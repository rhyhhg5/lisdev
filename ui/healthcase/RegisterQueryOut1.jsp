<%@page contentType="text/html;charset=GBK" %>
<%
//Name    ：ReportQueryOut1.jsp
//Function：在"立案"模块中将按照"报案号"查询出的数据显示在"立案"界面上
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
<%
//输出参数
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  LLReportSchema mLLReportSchema = new LLReportSchema();
  mLLReportSchema.setRptNo(request.getParameter("RptNo"));

  VData tVData = new VData();
  tVData.addElement(mLLReportSchema);
  RegisterQueryUI tRegisterQueryUI   = new RegisterQueryUI();
  if (!tRegisterQueryUI.submitData(tVData,"QUERY||MAIN"))
  {
    Content = " 查询失败，原因是: " + tRegisterQueryUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
  }
  else
  {
    tVData.clear();
    tVData = tRegisterQueryUI.getResult();
    LLReportSchema tLLReportSchema = new LLReportSchema();
    tLLReportSchema.setSchema((LLReportSchema)tVData.getObjectByObjectName("LLReportSchema",0));

    LLSubReportSet mLLSubReportSet = new LLSubReportSet();
    mLLSubReportSet.set((LLSubReportSet)tVData.getObjectByObjectName("LLSubReportSet",0));
    int n = mLLSubReportSet.size();
    System.out.println("get report "+n);
    //进行转意字符的处理
    String Path = application.getRealPath("config//Conversion.config");
    tLLReportSchema.setAccidentReason(StrTool.Conversion(tLLReportSchema.getAccidentReason(),Path));
    tLLReportSchema.setAccidentCourse(StrTool.Conversion(tLLReportSchema.getAccidentCourse(),Path));
    tLLReportSchema.setRemark(StrTool.Conversion(tLLReportSchema.getRemark(),Path));
  %>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <script language="javascript">
    var tAccidentReason = Conversion("<%=tLLReportSchema.getAccidentReason()%>");
    var tAccidentCourse = Conversion("<%=tLLReportSchema.getAccidentCourse()%>");
    var tRemark = Conversion("<%=tLLReportSchema.getRemark()%>");


    top.opener.fm.all("RptNo").value = "<%=tLLReportSchema.getRptNo()%>";
    top.opener.fm.all("AccidentDate").value = "<%=tLLReportSchema.getAccidentDate()%>";
    top.opener.fm.all("AccidentReason").value =tAccidentReason ;
    top.opener.fm.all("AccidentSite").value = "<%=tLLReportSchema.getAccidentSite()%>";
    top.opener.fm.all("AccidentCourse").value =tAccidentCourse;
    top.opener.fm.all("Remark").value =tRemark;
    top.opener.fm.all("MngCom").value = "<%=tLLReportSchema.getMngCom()%>";
    top.opener.fm.all("AgentCode").value = "<%=tLLReportSchema.getAgentCode()%>";
    top.opener.fm.all("AgentGroup").value = "<%=tLLReportSchema.getAgentGroup()%>";
    top.opener.fm.all("CaseState").value="<%=tLLReportSchema.getRptMode()%>";
    top.opener.fm.all("PeopleName").value = "<%=mLLSubReportSet.get(1).getCustomerName()%>";
    top.opener.fm.all("CustomerNo").value = "<%=mLLSubReportSet.get(1).getCustomerNo()%>";
    top.opener.emptyUndefined();
  </script>
  <script language="javascript">
    top.opener.CaseGrid.clearData();
  </script>
  <%
    System.out.println("for bgin");
    for (int i = 1; i <= n; i++)
    {
       LLSubReportSchema mLLSubReportSchema = mLLSubReportSet.get(i);
  %>
       <script language="javascript">
         top.opener.CaseGrid.addOne("CaseGrid");
         top.opener.CaseGrid.setRowColData(<%=i-1%>,1,"<%=mLLSubReportSchema.getCaseNo()%>");
         top.opener.CaseGrid.setRowColData(<%=i-1%>,2,"<%=mLLSubReportSchema.getCustomerNo()%>");
         top.opener.CaseGrid.setRowColData(<%=i-1%>,3,"<%=mLLSubReportSchema.getCustomerName()%>");
         top.opener.CaseGrid.setRowColData(<%=i-1%>,8,"<%=mLLSubReportSchema.getAccidentType()%>");
         top.opener.CaseGrid.setRowColData(<%=i-1%>,9,"<%=mLLSubReportSchema.getSubRptNo()%>");
         top.opener.CaseGrid.checkBoxAll();
         top.opener.emptyUndefined();
       </script>
  <%
    }
  }
    //如果在Catch中发现异常，则不从错误类中提取错误信息
    if (FlagStr == "Fail")
    {
      tError = tRegisterQueryUI.mErrors;
      if (!tError.needDealError())
      {
        Content = " 查询成功! ";
        FlagStr = "Succ";
      }
      else
      {
        Content = " 查询失败，原因是:" + tError.getFirstError();
        FlagStr = "Fail";
      }
    }
    System.out.println("------end------");
    System.out.println(FlagStr);
    System.out.println(Content);
    System.out.println("------end------");
    System.out.println(FlagStr);
    System.out.println(Content);
    out.println("<script language=javascript>");
    out.println("top.close();");
    out.println("</script>");
%>