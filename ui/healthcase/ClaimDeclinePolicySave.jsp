<%
//程序名称：LLClaimSave.jsp
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llhealthcase.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  System.out.println("sdfsdf");
  VData tVData = new VData();
  LLCasePolicySchema tLLCasePolicySchema   = new LLCasePolicySchema();
	LLCasePolicySet tLLCasePolicySet=new LLCasePolicySet();
  CasePolicyUI tCasePolicyUI   = new CasePolicyUI();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "UPDATE";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

  //读取分案保单明细信息
  String strCaseNo=request.getParameter("CaseNo");
  String[] strNumber=request.getParameterValues("CasePolicyGridNo");
  String[] strDeclineNo=request.getParameterValues("CasePolicyGrid1");
  String[] strCasePolNo=request.getParameterValues("CasePolicyGrid2");
  String[] strPolNo=request.getParameterValues("CasePolicyGrid3");
  String[] strRiskVer=request.getParameterValues("CasePolicyGrid4");
  String[] strRiskCode=request.getParameterValues("CasePolicyGrid5");
  String[] strAgentCode=request.getParameterValues("CasePolicyGrid6");
  String[] strInsuredName=request.getParameterValues("CasePolicyGrid7");
  String[] strInsuredSex=request.getParameterValues("CasePolicyGrid8");
  
  //保存分案保单明细信息
  int intLength=strNumber.length;
  for(int i=0;i<intLength;i++)
  {
  	tLLCasePolicySchema=new LLCasePolicySchema();
  	tLLCasePolicySchema.setCaseNo(strCaseNo);
  	tLLCasePolicySchema.setDeclineNo(strDeclineNo[i]);
  	tLLCasePolicySchema.setCasePolNo(strCasePolNo[i]);
  	tLLCasePolicySchema.setPolNo(strPolNo[i]);
  	tLLCasePolicySchema.setRiskVer(strRiskVer[i]);
  	tLLCasePolicySchema.setRiskCode(strRiskCode[i]);
  	tLLCasePolicySchema.setAgentCode(strAgentCode[i]);
  	tLLCasePolicySchema.setInsuredName(strInsuredName[i]);
  	tLLCasePolicySchema.setInsuredSex(strInsuredSex[i]);
  	tLLCasePolicySet.add(tLLCasePolicySchema);
  }
	
	
  try
  {
  // 准备传输数据 VData
   
   //此处需要根据实际情况修改
   tVData.addElement(tLLCasePolicySet);    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tCasePolicyUI.submitData(tVData,transact);

  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCasePolicyUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
//保存拒赔信息
%>            
         
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>