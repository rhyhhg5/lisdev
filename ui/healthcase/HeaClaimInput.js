//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var Action;
var tRowNo=0;

//提交，保存按钮对应操作
function submitForm()
{
  if (confirm("您确实想保存该记录吗?"))
  {
    if (ClaimDetailGrid.mulLineCount==0)
    {
      alert("请作赔付明细计算");
      return;
    }

    var i = 0;
    var tRealPay = 0;
    var tCalPay=0;
    var tTempPolPay=0;

    for (i=0;i<ClaimPolGrid.mulLineCount;i++)
    {
      tCalPay=parseFloat(ClaimPolGrid.getRowColData(i,7));
      tRealPay=parseFloat(ClaimPolGrid.getRowColData(i,8));
      tRemark=ClaimPolGrid.getRowColData(i,11);
      //add:
      //  核算金额〉实际金额
      //  核算金额〈实际金额
      //  	if (tRealPay>tCalPay)
      //{
      //  	   alert("实际赔付金额大于系统计算赔付金，不能保存！！")
      //  	   return false;
      // 	}
      if(tRealPay>tCalPay)
      {
        if(tRemark=="")
        {
          alert("实际赔付金额大于系统计算赔付金额，必须填写备注！！");
          return false;
        }
      }
      if (ClaimPolGrid.getRowColData(i,6)=="001") //检查临时保单赔付金额是否大于25万
      {
        tTempPolPay = tTempPolPay + tRealPay;
      }
    }

    if (tTempPolPay > 250000)
    {
      if (!confirm("临时保单赔付金额大于25万，系统将自动对赔付进行调整，您确实需要保存吗？"))
      {
        return;
      }
      else
      {
        var Ratio = 250000/tTempPolPay;
        for (i=0;i<ClaimPolGrid.mulLineCount;i++)
        {
          alert(ClaimPolGrid.getRowColData(i,6));
          if (ClaimPolGrid.getRowColData(i,6)=="001")
          {
            var tNowPay = ""+parseFloat(ClaimPolGrid.getRowColData(i,8))*parseFloat(Ratio);
            ClaimPolGrid.setRowColData(i,8,tNowPay);
            var tPolNo=ClaimPolGrid.getRowColData(i,1);
            alert(tPolNo);
            var j=0;
            for (j=0;j<ClaimDetailGrid.mulLineCount;j++)
            {
              alert(ClaimDetailGrid.getRowColData(j,1));
              if (ClaimDetailGrid.getRowColData(j,1)==""+tPolNo)
              {
                alert("ssss");
                var tNowPay1 = ""+parseFloat(ClaimDetailGrid.getRowColData(j,3))*parseFloat(Ratio);
                alert(tNowPay1);
                ClaimDetailGrid.setRowColData(j,3,tNowPay1);
                tNowPay1 = ""+parseFloat(ClaimDetailGrid.getRowColData(j,4))*parseFloat(Ratio);
                ClaimDetailGrid.setRowColData(j,4,tNowPay1);
              }
            }
            for (j=0;j<ClaimPayGrid.mulLineCount;j++)
            {
              if (ClaimPayGrid.getRowColData(j,1)==""+tPolNo)
              {
              ClaimPayGrid.setRowColData(j,2,""+parseFloat(ClaimPayGrid.getRowColData(j,2))*parseFloat(Ratio));
              ClaimPayGrid.setRowColData(j,3,""+parseFloat(ClaimPayGrid.getRowColData(j,3))*parseFloat(Ratio));
              ClaimPayGrid.setRowColData(j,4,""+parseFloat(ClaimPayGrid.getRowColData(j,4))*parseFloat(Ratio));
              ClaimPayGrid.setRowColData(j,5,""+parseFloat(ClaimPayGrid.getRowColData(j,5))*parseFloat(Ratio));
              ClaimPayGrid.setRowColData(j,6,""+parseFloat(ClaimPayGrid.getRowColData(j,6))*parseFloat(Ratio));
              ClaimPayGrid.setRowColData(j,7,""+parseFloat(ClaimPayGrid.getRowColData(j,7))*parseFloat(Ratio));
            }
            }
          }
        }
      }
    }

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    showSubmitFrame(mDebug);
    fm.Opt.value = "save"
    fm.submit(); //提交

  }
  else
  {

    alert("您取消了修改操作！");
  }
}
function showFeeInfo()
{
  if (CaseGrid.getSelNo()==0)
  {
    alert("请选择一个分案!");
    return;
  }
  if ((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
  {
    alert ("请您先执行保存操作!");
    return ;
  }
  else
  {
    fm.DisplayFlag.value="0";//标志是在立案界面中进入的费用明细
    var varSrc = "&RgtNo=" +fm.RgtNo.value;
    varSrc += "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
    varSrc += "&CustomerNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
    varSrc += "&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
    var newWindow = window.open("./FrameMainFAZL.jsp?Interface=ICaseCureInput.jsp"+varSrc,"ICaseCureInput",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
}


//提交，保存按钮对应操作
function submitForm1()
{

  initForm();
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	window.open("./HeaFrameCasePolicyQuery.jsp");

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    if (Action=="DELETE")
    {
    	resetForm();
    }
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  if (fm.ClmNo.value!="")
  {
  	alert("请保存修改信息");
  	return;
  }
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if (confirm("您确实想修改该记录吗?"))
  {
    //下面增加相应的代码
    if (fm.ClmNo.value=="")
    {
      alert("请用新增保存");
      return;
    }
    Action = "UPDATE";
    showDiv(operateButton,"false");
    showDiv(inputButton,"true");
  }
  else
  {
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  Action = "QUERY";
  //window.showModelessDialog("./ProposalQuery.jsp",window,"dialogWidth=15cm;dialogHeight=12cm");
  initForm();
  window.open("./FrameClaimQuery.jsp");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  alert("您不能执行删除操作！！！");
  return;
//  if (!confirm("您确实要删除该记录吗？"))
//  {
//  	return;
//  }
//  //下面增加相应的代码
//  var i = 0;
//  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//  showSubmitFrame(mDebug);
//  fm.Opt.value="del";
//  Action = "DELETE"
//  fm.submit();
}
//Click事件，当点击“选择责任”按钮时触发该函数
function chooseDuty()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  if(cPolNo=="")
  {
     alert("您必须先录入投保单号才能修改该投保单的责任项。");
     return false
  }
//  showModalDialog("./FrameMain.jsp?Interface=ChooseDutyInput.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
  window.open("./FrameMain.jsp?Interface=ChooseDutyInput.jsp&PolNo="+cPolNo);
  return true
}

//Click事件，当点击“责任信息”按钮时触发该函数
function showDuty()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  if(cPolNo=="")
  {
     alert("您必须先录入投保单号才能察看该投保单的责任项。");
     return false
  }
  showModalDialog("./ProposalDuty.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
//  window.open("./ProposalDuty.jsp?PolNo="+cPolNo);
}

//Click事件，当点击“暂交费信息”按钮时触发该函数
function showFee()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  //showModalDialog("./ProposalFee.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=16cm;dialogHeight=8cm");
  window.open("./ProposalFee.jsp?PolNo="+cPolNo);

}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function CalPay()
{
	fm.Opt.value = "cal";
	tRowNo = ClaimPolGrid.getSelNo();
	if (tRowNo==0)
	{
		alert("请选择需要计算的保单");
		return;
	}


		var i = 0;
  	var showStr="正在赔付计算，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//showSubmitFrame(mDebug);
	//fm.target="_blank";
  	fm.submit(); //提交
}
function submitFormSurvery()
{
    if (CaseGrid.getSelNo()==0)
    {
    		alert("请您选择一个分案！");
    		return;
    }

    if (CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1)=="")
    {
    	alert("分案号为空");
    	return;
    }


  var varSrc = "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
  varSrc += "&InsuredNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
  varSrc += "&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  varSrc += "&StartPhase=1";
  varSrc += "&Type=1";


  var newWindow = window.open("./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc,"RgtSurveyInput");

}

//分案收据明细
function showCaseReceipt()
{
    if (CaseGrid.getSelNo()==0)
    {
    		alert("请您选择一个分案！");
    		return;
    }
    if ((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
    {
    	alert("请您先执行保存操作");
    	return;
    }
    else
    {
  			var varSrc = "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
  			varSrc += "&CustomerNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
  			varSrc +="&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
  			var newWindow = window.open("./FrameMainClaimFASJ.jsp?Interface=ClaimCaseReceiptInput.jsp"+varSrc,"CaseReceiptInput",'width=700,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  	}
}

//分案诊疗明细
function showICaseCure()
{
  if (CaseGrid.getSelNo()==0)
  {
    alert("请选择一个分案!");
    return;
  }
  if ((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
  {
    alert ("请您先执行保存操作!");
    return ;
  }
  else
  {
			var varSrc = "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
      varSrc += "&CustomerNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
      varSrc += "&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
      var newWindow = window.open("./ClaimFrameMainFAZL.jsp?Interface=ClaimICaseCureInput.jsp"+varSrc,"ICaseCureInput",'width=700,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
}
//显示伤残明细信息的函数
function showAccident()
{
  if (CaseGrid.getSelNo()==0)
  {
    alert("请您选择一个分案！");
    return;
  }
  if ((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
  {
    alert("请您先执行保存操作");
    return;
  }
  else
  {
    fm.DisplayFlag.value="1";//0表示在立案中进入的事故明细
    var varSrc = "&RgtNo=" +fm.RgtNo.value;
    varSrc += "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
    varSrc += "&CustomerNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
    varSrc +="&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
    varSrc +="&DisplayFlag="+fm.DisplayFlag.value;
    var newWindow = window.open("./FrameMainAccident.jsp?Interface=CaseAccidentInput.jsp"+varSrc,"CaseAccidentInput");
  }
}
//结算利息的函数
function CalInterest()
{
	fm.Opt.value = "CalInt";
	tRowNo = ClaimPolGrid.getSelNo();
	if (tRowNo==0)
	{
		alert("请选择需要结算账户利息的保单");
		return;
	}
		var i = 0;
  	var showStr="正在结算账户利息，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	fm.action = "./CalInterest.jsp";
  	//fm.target = "_blank";
  	fm.submit(); //提交
}
