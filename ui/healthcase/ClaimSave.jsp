<%
//程序名称：LLClaimSave.jsp
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//           guoxiang   2003-8-1    1.根据客户的需求更改字段：添加全选框的初始化
//                                  2.保存ReMark 字段
//                                  3.多选框的选择保存
//           guoxiang   2003-8-7    1.保存给付类型编码
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llhealthcase.*"%>
<%@page import=" java.text.SimpleDateFormat"%>
<%@page import=" java.text.*"%>
<%@page import= "java.util.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//接收信息，并作校验处理。
//输入参数
  LLClaimSchema tLLClaimSchema   = new LLClaimSchema();

LLClaimSet tLLClaimSet=new LLClaimSet();
LLClaimDetailSet tLLClaimDetailSet=new LLClaimDetailSet();
LJSGetClaimSet tLJSGetClaimSet = new LJSGetClaimSet();
LJSGetSchema tLJSGetSchema = new LJSGetSchema();
String mTotalMoney = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String transact;
String FlagStr = "";
String Content = "";
CErrors tError = null;
String tOpt=request.getParameter("Opt");
System.out.println("opt=="+tOpt);
if (tOpt.equals("cal"))
{    //计算赔付
  System.out.println("开始执行ClaimSave.jsp");
  transact = "QUERY";
  //添加程序将账户的信息传入到后台
  //判断是否有值
  LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();//传递前台的数据
  LCInsureAccSet eLCInsureAccSet = new LCInsureAccSet();//接受后台传递的数据

  String tPolAccGridNo[] = request.getParameterValues("PolAccGridNo");
  int tAccSel = 0;
  if(!(tPolAccGridNo ==null||tPolAccGridNo.equals("")))
  {
    int  tAccCount = tPolAccGridNo.length;
    System.out.println("账户信息的长度是"+tAccCount);

    if(tAccCount>=1)
    {
      String tPolNo[] = request.getParameterValues("PolAccGrid1");
      String tInsuAccNo[] = request.getParameterValues("PolAccGrid2");
      String tAccType[] = request.getParameterValues("PolAccGrid3");//
      String tInsuAccBala[] = request.getParameterValues("PolAccGrid4");//账户原有金额
      String tSumPay[] = request.getParameterValues("PolAccGrid5");//利息值
      String tSumPaym[] = request.getParameterValues("PolAccGrid6");//利息金额
      String tMasterPolNo[] = request.getParameterValues("PolAccGrid12");//关联保单号码
      for(int aCount=0;aCount<tAccCount;aCount++)
      {
        tAccSel = tAccSel+1;
        LCInsureAccSchema mLCInsureAccSchema = new LCInsureAccSchema();
        //mLCInsureAccSchema.setPolNo(tPolNo[aCount].trim());
        mLCInsureAccSchema.setPolNo(tMasterPolNo[aCount]);
        System.out.println("关联的保单号码是"+mLCInsureAccSchema.getPolNo());
        mLCInsureAccSchema.setInsuredNo(tInsuAccNo[aCount].trim());
        mLCInsureAccSchema.setAccType(tAccType[aCount].trim());
        mLCInsureAccSchema.setInsuAccBala(tInsuAccBala[aCount].trim());
        mLCInsureAccSchema.setSumPay(tSumPay[aCount].trim());
        mLCInsureAccSchema.setSumPaym(tSumPaym[aCount].trim());
        mLCInsureAccSet.add(mLCInsureAccSchema);
      }
      System.out.println("账户信息的值是mLCInsureAccSet"+mLCInsureAccSet.size());
    }
  }

  LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
  //判断Radio被选中
  String tClaimPolNo[] = request.getParameterValues("ClaimPolGridNo"); //序号字段
  String tRadio[] = request.getParameterValues("InpClaimPolGridSel");
  //注意：radio字段的名字结构为Inp+GridName+Sel，这里GridName=ClaimPolGrid
  boolean flag = false;
  int ClaimPolCount = tClaimPolNo.length;   //纪录的总行数
  int tSelNo =0 ;
  for (int i = 0; i < ClaimPolCount; i++)
  {
    if (tRadio [i].equals("1"))
    {             //如果该行被选中
      tSelNo = i;
      flag = true;                     //置标记为真
    }
  }
  tLLClaimPolicySchema.setRgtNo(request.getParameter("RgtNo"));

  String[] strPolNo=request.getParameterValues("ClaimPolGrid1");
  String[] strRiskCode=request.getParameterValues("ClaimPolGrid2");
  String[] strCaseNo=request.getParameterValues("ClaimPolGrid3");
  String[] strInsuredNo=request.getParameterValues("ClaimPolGrid4");
  String[] strCValiDate=request.getParameterValues("ClaimPolGrid5");
  String[] strGetDutyKind=request.getParameterValues("ClaimPolGrid6");
  String[] strRemark=request.getParameterValues("ClaimPolGrid11");
  String[] strCasePolType=request.getParameterValues("ClaimPolGrid12");

  String[] strPolNoDetail=request.getParameterValues("ClaimDetailGrid1");
  String[] strStandPayDetail=request.getParameterValues("ClaimDetailGrid3");

  String[] strGetDutyCodeDetail=request.getParameterValues("ClaimDetailGrid8");

  String[] strPolNoClaimPay=request.getParameterValues("ClaimPayGrid1");

  tLLClaimPolicySchema.setPolNo(strPolNo[tSelNo]);
  tLLClaimPolicySchema.setRiskCode(strRiskCode[tSelNo]);
  tLLClaimPolicySchema.setCaseNo(strCaseNo[tSelNo]);
  tLLClaimPolicySchema.setInsuredNo(strInsuredNo[tSelNo]);
  tLLClaimPolicySchema.setCValiDate(strCValiDate[tSelNo]);
  tLLClaimPolicySchema.setGetDutyKind(strGetDutyKind[tSelNo]);
  tLLClaimPolicySchema.setRemark(strRemark[tSelNo]);
  tLLClaimPolicySchema.setCasePolType(strCasePolType[tSelNo]);

  System.out.println("PolNo=="+tLLClaimPolicySchema.getPolNo());
  System.out.println("RgtNo=="+tLLClaimPolicySchema.getRgtNo());
  System.out.println("CaseNo=="+tLLClaimPolicySchema.getCaseNo());
  System.out.println("InsureNo=="+tLLClaimPolicySchema.getInsuredNo());
  //添加理赔账户的信息

  LLCaseAccSet tLLCaseAccSet = new LLCaseAccSet();
  int mAccCount = 0;
  String arrCaseAccNo[] = request.getParameterValues("PolAccGridNo");
  if (!(arrCaseAccNo==null||arrCaseAccNo.equals("")))
  {
    mAccCount = arrCaseAccNo.length;
    System.out.println("PolAccGrid账户列表的记录数是"+mAccCount);
  }
  else
    mAccCount = 0;
  System.out.println("账户列表的记录数是"+mAccCount);
  if(mAccCount>0)
  {
    String[] strPolNoAcc = request.getParameterValues("PolAccGrid1");
    String[] strGetDutyCodeAcc = request.getParameterValues("PolAccGrid10");
    String[] strGetDutyKindAcc = request.getParameterValues("PolAccGrid11");
    String[] strAccType = request.getParameterValues("PolAccGrid3");
    String[] strInsuAccNo = request.getParameterValues("PolAccGrid2");
    String[] strAccBJMoney = request.getParameterValues("PolAccGrid4");
    String[] strInterestMoney = request.getParameterValues("PolAccGrid5");
    String[] strTotalMoney = request.getParameterValues("PolAccGrid6");
    String[] strBasePay = request.getParameterValues("PolAccGrid7");
    String[] strRealPayAcc = request.getParameterValues("PolAccGrid8");
    String[] strMasterPolNo = request.getParameterValues("PolAccGrid12");
    for(int i=0;i<mAccCount;i++)
    {
      LLCaseAccSchema tLLCaseAccSchema = new LLCaseAccSchema();
      // tLLCaseAccSchema.setCaseNo(strCaseNo[]);
      tLLCaseAccSchema.setPolNo(strPolNoAcc[i]);
      tLLCaseAccSchema.setGetDutyCode(strGetDutyCodeAcc[i]);
      tLLCaseAccSchema.setGetDutyKind(strGetDutyKindAcc[i]);
      tLLCaseAccSchema.setAccType(strAccType[i]);
      tLLCaseAccSchema.setInsuAccNo(strInsuAccNo[i]);
      tLLCaseAccSchema.setAccBJMoney(strAccBJMoney[i]);
      tLLCaseAccSchema.setInterestMoney(strInterestMoney[i]);
      tLLCaseAccSchema.setTotalMoney(strTotalMoney[i]);
      tLLCaseAccSchema.setBasePay(strBasePay[i]);
      tLLCaseAccSchema.setRealPay(strRealPayAcc[i]);
      tLLCaseAccSchema.setMasterPolNo(strMasterPolNo[i]);
      tLLCaseAccSet.add(tLLCaseAccSchema);
    }
  }
  ClaimCalUI tClaimCalUI   = new ClaimCalUI();
  // 准备传输数据 VData
  VData tVData = new VData();
  try{
    //此处需要根据实际情况修改
    tVData.addElement(tLLClaimPolicySchema);
    tVData.addElement(mLCInsureAccSet);
    tVData.addElement(tLLCaseAccSet);
    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    tClaimCalUI.submitData(tVData,transact);
  }
  catch(Exception ex){
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("cal end2004-4-21lys");
  if (FlagStr=="")
  {
    tError = tClaimCalUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 计算成功";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 计算失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
  if (FlagStr == "Succ")
  {
    tVData.clear();
    tVData = tClaimCalUI.getResult();
    tLLClaimPolicySchema = new LLClaimPolicySchema();
    tLLClaimPolicySchema=(LLClaimPolicySchema)tVData.getObjectByObjectName("LLClaimPolicySchema",0);
    tLLClaimDetailSet.set((LLClaimDetailSet)tVData.getObjectByObjectName("LLClaimDetailSet",0));
    tLJSGetClaimSet.set((LJSGetClaimSet)tVData.getObjectByObjectName("LJSGetClaimSet",0));
    eLCInsureAccSet.set((LCInsureAccSet)tVData.getObjectByObjectName("LCInsureAccSet",0));
    tLLClaimPolicySchema.setStandPay(tLLClaimPolicySchema.getStandPay());
    tLLClaimPolicySchema.setRealPay(tLLClaimPolicySchema.getRealPay());
    System.out.println("SumMoney==="+tLLClaimPolicySchema.getStandPay());
    System.out.println("账户信息返回值是"+eLCInsureAccSet.size());
    if(eLCInsureAccSet.size()>0)
    {
      for(int acount=1;acount<=eLCInsureAccSet.size();acount++)
      {
      %>
      <script language="javascript">
      //parent.fraInterface.PolAccGrid.setRowColData(<%=tAccSel-1%>,7,"<%=Double.parseDouble(new DecimalFormat("0.00").format(eLCInsureAccSet.get(acount).getInsuAccBala()))%>");
      //parent.fraInterface.PolAccGrid.setRowColData(<%=tAccSel-1%>,8,"<%=Double.parseDouble(new DecimalFormat("0.00").format(eLCInsureAccSet.get(acount).getInsuAccBala()))%>");
      </script>
      <%
        }
    }

  %>
    <script language="javascript">
      parent.fraInterface.ClaimPolGrid.setRowColData(<%=tSelNo%>,7,"<%=Double.parseDouble(new DecimalFormat("0.00").format(tLLClaimPolicySchema.getStandPay()))%>");
        parent.fraInterface.ClaimPolGrid.setRowColData(<%=tSelNo%>,8,"<%=Double.parseDouble(new DecimalFormat("0.00").format(tLLClaimPolicySchema.getRealPay()))%>");
  </script>
<%
  double t_sc_gf =0;
  double t_sw_gf =0;
  double t_yl_gf =0;
  double t_qt_gf =0;
  double t_tb_gf =0;
  double t_tf_gf =0;

  //得到已有的纪录数目
  int lineCount = 0;
  String arrCount[] = request.getParameterValues("ClaimDetailGridNo");
  if (arrCount!=null)
    lineCount = arrCount.length;
  else
    lineCount = 0;
  int i = 1;
  int lineNow=1;  //显示位置
  int lineAdd=0;  //新增加的行数
  String BeingFlag; //是否为新行
  //显示给付金信息
  int n = tLLClaimDetailSet.size();
  for ( i = 1; i <=n ;i++)
  {
    LLClaimDetailSchema tLLClaimDetailSchema = tLLClaimDetailSet.get(i);
    BeingFlag="1";
    //确定显示信息的行数
    lineAdd = lineAdd + 1;
    for (int j = 1 ;j<=lineCount;j++)
    {
      if (strPolNoDetail[j-1].trim().equals(tLLClaimDetailSchema.getPolNo().trim())&& strGetDutyCodeDetail[j-1].trim().equals(tLLClaimDetailSchema.getGetDutyCode().trim()))
      {
      lineNow = j;
      lineAdd = lineAdd - 1;
      BeingFlag = "0";  //已有记录
      break;
    }
    }

    if (BeingFlag == "1")
    {
      lineNow = lineCount + lineAdd;
 %>
     <script language="javascript">
       parent.fraInterface.ClaimDetailGrid.addOne("ClaimDetailGrid")
     parent.fraInterface.ClaimDetailGrid.checkBoxAll();
     </script>
 <%
   }

   if (tLLClaimDetailSchema.getStatType()!=null && tLLClaimDetailSchema.getStatType().trim().equals("SC"))
     t_sc_gf = t_sc_gf + tLLClaimDetailSchema.getStandPay();
   if (tLLClaimDetailSchema.getStatType()!=null && tLLClaimDetailSchema.getStatType().trim().equals("SW"))
     t_sw_gf = t_sw_gf + tLLClaimDetailSchema.getStandPay();
   if (tLLClaimDetailSchema.getStatType()!=null && tLLClaimDetailSchema.getStatType().trim().equals("YL"))
     t_yl_gf = t_yl_gf + tLLClaimDetailSchema.getStandPay();
   if (tLLClaimDetailSchema.getStatType()!=null && tLLClaimDetailSchema.getStatType().trim().equals("QT"))
     t_qt_gf = t_qt_gf + tLLClaimDetailSchema.getStandPay();
   if (tLLClaimDetailSchema.getStatType()!=null && tLLClaimDetailSchema.getStatType().trim().equals("TB"))
     t_tb_gf = t_tb_gf + tLLClaimDetailSchema.getStandPay();
   if (tLLClaimDetailSchema.getStatType()!=null && tLLClaimDetailSchema.getStatType().trim().equals("TF"))
     t_tf_gf = t_tf_gf + tLLClaimDetailSchema.getStandPay();

   String tGetDutycode=tLLClaimDetailSchema.getGetDutyCode();   //给付责任编码
   String tGetDutyKind=tLLClaimDetailSchema.getGetDutyKind();   //给付责任类型
   LMDutyGetClmSchema  tLMDutyGetClmSchema=new LMDutyGetClmSchema();
   tLMDutyGetClmSchema=CaseFunPub.getGetDutyName(tGetDutycode,tGetDutyKind);
   String tGetDutyName=tLMDutyGetClmSchema.getGetDutyName();        //给负责任名称
   String tAfterGet="";
   if(tLMDutyGetClmSchema.getAfterGet()==null){
     tAfterGet="000";
   }
   else{
     tAfterGet=tLMDutyGetClmSchema.getAfterGet();            //险种能力
   }
   System.out.println("tAfterGet="+tAfterGet);                      //为000从数据库得到的为NULL
   if(tAfterGet.equals("003")){
     tAfterGet="Y";

   }
   else{
     tAfterGet="N";
   }
 %>
    <script language="javascript">
        parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,1,"<%=tLLClaimDetailSchema.getPolNo()%>");
       parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,2,"<%=tGetDutyName%>");
          parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,3,"<%=Double.parseDouble(new DecimalFormat("0.00").format(tLLClaimDetailSchema.getStandPay()))%>");
       parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,4,"<%=Double.parseDouble(new DecimalFormat("0.00").format(tLLClaimDetailSchema.getRealPay()))%>");
       //parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,3,"<%=tLLClaimDetailSchema.getStandPay()%>");
       //parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,4,"<%=tLLClaimDetailSchema.getRealPay()%>");
       parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,5,"<%=tLLClaimDetailSchema.getStatType()%>");
       parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,6,"<%=tLLClaimDetailSchema.getGetDutyKind()%>");
       parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,7,"<%=tLLClaimDetailSchema.getDutyCode()%>");
        parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,8,"<%=tGetDutycode%>");
      parent.fraInterface.ClaimDetailGrid.setRowColData(<%=lineNow-1%>,9,"<%=tAfterGet%>");

      </script>
<%  		}
  double t_sum_gf = t_sc_gf+t_sw_gf+t_yl_gf+t_qt_gf;
  lineCount = 0;
  String arrCount1[] = request.getParameterValues("ClaimPayGridNo");
  if (arrCount1!=null)
    lineCount = arrCount1.length;
  else
    lineCount = 0;
  i = 1;
  lineNow=1;  //显示位置
  lineAdd=0;  //新增加的行数
  BeingFlag="1"; //是否为新行

  //确定显示信息的行数
  lineAdd = lineAdd + 1;
  for (int j = 1 ;j<=lineCount;j++)
  {
    if (strPolNoClaimPay[j-1].trim().equals(tLLClaimPolicySchema.getPolNo().trim()))
    {
    lineNow = j;
    lineAdd = lineAdd - 1;
    BeingFlag = "0";  //已有记录
    break;
  }
  }

  if (BeingFlag == "1")
  {
    lineNow = lineCount + lineAdd;
 %>
    <script language="javascript">

      parent.fraInterface.ClaimPayGrid.addOne("ClaimPayGrid")
    </script>
 <%
   }


 %>
    <script language="javascript">
          parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,1,"<%=tLLClaimPolicySchema.getPolNo()%>");
//		   			parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,2,"<%=t_sc_gf%>");
//		   			parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,3,"<%=t_sw_gf%>");
//		   			parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,4,"<%=t_yl_gf%>");
//		   			parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,5,"<%=t_tb_gf%>");
//		   			parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,6,"<%=t_tf_gf%>");
//		   			parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,7,"<%=t_qt_gf%>");
//		   			parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,8,"<%=t_sum_gf%>");
//

        parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,2,"<%=Double.parseDouble(new DecimalFormat("0.00").format(t_sc_gf))%>");
        parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,3,"<%=Double.parseDouble(new DecimalFormat("0.00").format(t_sw_gf))%>");
        parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,4,"<%=Double.parseDouble(new DecimalFormat("0.00").format(t_yl_gf))%>");
        parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,5,"<%=Double.parseDouble(new DecimalFormat("0.00").format(t_tb_gf))%>");
        parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,6,"<%=Double.parseDouble(new DecimalFormat("0.00").format(t_tf_gf))%>");
        parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,7,"<%=Double.parseDouble(new DecimalFormat("0.00").format(t_qt_gf))%>");
        parent.fraInterface.ClaimPayGrid.setRowColData(<%=lineNow-1%>,8,"<%=Double.parseDouble(new DecimalFormat("0.00").format(t_sum_gf))%>");

   </script>
<%


  }

}

if (tOpt.equals("del"))
{
  System.out.println("del begin..");
  String tClmNo = request.getParameter("ClmNo");
  transact = "DELETE";
  tLLClaimSchema =  new LLClaimSchema();
  tLLClaimSchema.setClmNo(request.getParameter("ClmNo"));
  tLLClaimSchema.setCasePayType(request.getParameter("CasePayType"));
  ClaimSaveUI tClaimSaveUI   = new ClaimSaveUI();
  // 准备传输数据 VData
  VData tVData = new VData();
  try
  {
    //此处需要根据实际情况修改
    tVData.addElement(tLLClaimSchema);
    tVData.addElement(tG);

    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    tClaimSaveUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    ex.printStackTrace();
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr=="")
  {
    tError = tClaimSaveUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 删除成功";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 删除失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }


}
//理算保存时的函数
if (tOpt.equals("save"))      //保存赔付信息
{
	String mCaseNo = "";
  System.out.println("save now.....");
  String tClmNo = request.getParameter("ClmNo");
  if (tClmNo.equals(""))
    transact = "INSERT";
  else
    transact = "UPDATE";
  //向后台传递理赔账户的信息的容器

  LLCaseAccSet tLLCaseAccSet = new LLCaseAccSet();
  int mAccCount = 0;
  String arrCaseAccNo[] = request.getParameterValues("PolAccGridNo");
  if (!(arrCaseAccNo==null||arrCaseAccNo.equals("")))
  {
    mAccCount = arrCaseAccNo.length;
    System.out.println("PolAccGrid账户列表的记录数是"+mAccCount);
  }
  else
    mAccCount = 0;
  System.out.println("账户列表的记录数是"+mAccCount);
  if(mAccCount>0)
  {
    String[] strPolNoAcc = request.getParameterValues("PolAccGrid1");
    String[] strGetDutyCodeAcc = request.getParameterValues("PolAccGrid10");
    String[] strGetDutyKindAcc = request.getParameterValues("PolAccGrid11");
    String[] strAccType = request.getParameterValues("PolAccGrid3");
    String[] strInsuAccNo = request.getParameterValues("PolAccGrid2");
    String[] strAccBJMoney = request.getParameterValues("PolAccGrid4");
    String[] strInterestMoney = request.getParameterValues("PolAccGrid5");
    String[] strTotalMoney = request.getParameterValues("PolAccGrid6");//账户余额
    String[] strBasePay = request.getParameterValues("PolAccGrid7");
    String[] strRealPayAcc = request.getParameterValues("PolAccGrid8");//实际给付金额
    String[] strMasterPolNo = request.getParameterValues("PolAccGrid12");
    for(int i=0;i<mAccCount;i++)
    {
      LLCaseAccSchema tLLCaseAccSchema = new LLCaseAccSchema();
      tLLCaseAccSchema.setPolNo(strPolNoAcc[i]);
      tLLCaseAccSchema.setGetDutyCode(strGetDutyCodeAcc[i]);
      tLLCaseAccSchema.setGetDutyKind(strGetDutyKindAcc[i]);
      tLLCaseAccSchema.setAccType(strAccType[i]);
      tLLCaseAccSchema.setInsuAccNo(strInsuAccNo[i]);
      tLLCaseAccSchema.setAccBJMoney(strAccBJMoney[i]);
      tLLCaseAccSchema.setInterestMoney(strInterestMoney[i]);
      tLLCaseAccSchema.setTotalMoney(strTotalMoney[i]);
      tLLCaseAccSchema.setBasePay(strBasePay[i]);
      tLLCaseAccSchema.setRealPay(strRealPayAcc[i]);
      tLLCaseAccSchema.setMasterPolNo(strMasterPolNo[i]);
      tLLCaseAccSet.add(tLLCaseAccSchema);
    }
  }
  tLLClaimSchema =  new LLClaimSchema();
  LLClaimPolicySet tLLClaimPolicySet= new LLClaimPolicySet();
  tLLClaimDetailSet = new LLClaimDetailSet();
  tLLClaimSchema.setRgtNo(request.getParameter("RgtNo"));
  tLLClaimSchema.setClmNo(request.getParameter("ClmNo"));
  tLLClaimSchema.setCasePayType(request.getParameter("CasePayType"));


  String[] strPolNo=request.getParameterValues("ClaimPolGrid1");
  String[] strRiskCode=request.getParameterValues("ClaimPolGrid2");
  String[] strCaseNo=request.getParameterValues("ClaimPolGrid3");
  String[] strInsuredNo=request.getParameterValues("ClaimPolGrid4");
  String[] strCValiDate=request.getParameterValues("ClaimPolGrid5");
  String[] strGetDutyKind=request.getParameterValues("ClaimPolGrid6");
  String[] strStandPay=request.getParameterValues("ClaimPolGrid7");
  String[] strRealPay=request.getParameterValues("ClaimPolGrid8");
  String[] strPolType=request.getParameterValues("ClaimPolGrid10");
  String[] strPolRemark=request.getParameterValues("ClaimPolGrid11");
  String[] strCasePolType=request.getParameterValues("ClaimPolGrid12");

  String[] strPolNoDetail=request.getParameterValues("ClaimDetailGrid1");
  String[] strStandPayDetail=request.getParameterValues("ClaimDetailGrid3");
  String[] strRealPayDetail=request.getParameterValues("ClaimDetailGrid4");
  String[] strStatTypeDetail=request.getParameterValues("ClaimDetailGrid5");
  String[] strGetDutyKindDetail=request.getParameterValues("ClaimDetailGrid6");
  String[] strDutyCodeDetail=request.getParameterValues("ClaimDetailGrid7");
  String[] strGetDutyCodeDetail=request.getParameterValues("ClaimDetailGrid8");

  String[] strPolNoPay=request.getParameterValues("ClaimPayGrid1");
  String[] strSCPay=request.getParameterValues("ClaimPayGrid2");
  String[] strSWPay=request.getParameterValues("ClaimPayGrid3");
  String[] strYLPay=request.getParameterValues("ClaimPayGrid4");
  String[] strTBPay=request.getParameterValues("ClaimPayGrid5");
  String[] strTFPay=request.getParameterValues("ClaimPayGrid6");
  String[] strQTPay=request.getParameterValues("ClaimPayGrid7");




  int PolicyCount = 0;
  String arrPolicyNo[] = request.getParameterValues("ClaimPolGridNo");
  if (arrPolicyNo!=null)
    PolicyCount = arrPolicyNo.length;
  else
    PolicyCount = 0;

  for (int i = 0;i<PolicyCount;i++){


    LLClaimPolicySchema tLLClaimPolicySchema = new LLClaimPolicySchema();
    tLLClaimPolicySchema.setPolNo(strPolNo[i]);
    tLLClaimPolicySchema.setRgtNo(request.getParameter("RgtNo"));
    tLLClaimPolicySchema.setClmNo(request.getParameter("ClmNo"));
    tLLClaimSchema.setCasePayType(request.getParameter("CasePayType"));
    tLLClaimPolicySchema.setRiskCode(strRiskCode[i]);
    tLLClaimPolicySchema.setCaseNo(strCaseNo[i]);
    tLLClaimPolicySchema.setInsuredNo(strInsuredNo[i]);
    tLLClaimPolicySchema.setCValiDate(strCValiDate[i]);
    tLLClaimPolicySchema.setStandPay(strStandPay[i]);
    tLLClaimPolicySchema.setRealPay(strRealPay[i]);
    tLLClaimPolicySchema.setGetDutyKind(strGetDutyKind[i]);
    tLLClaimPolicySchema.setPolType(strPolType[i]);
    tLLClaimPolicySchema.setRemark(strPolRemark[i]);
    tLLClaimPolicySchema.setCasePolType(strCasePolType[i]);
    tLLClaimPolicySet.add(tLLClaimPolicySchema);

  }//添加LLClaimPolicy

  int PayDetailCount = 0;
  String PayDetailNo[] = request.getParameterValues("ClaimDetailGridNo");
  String tCheck[] = request.getParameterValues("InpClaimDetailGridChk");
  //注意：Check字段的名字结构为Inp+GridName+Chk，这里GridName=ClaimPolGrid
  if (PayDetailNo!=null)
    PayDetailCount = PayDetailNo.length;
  else
    PayDetailCount = 0;
  for (int i = 0;i<PayDetailCount;i++){
    if (tCheck [i].equals("1")) {             //如果该行被选中
      LLClaimDetailSchema tLLClaimDetailSchema = new LLClaimDetailSchema();
      tLLClaimDetailSchema.setPolNo(strPolNoDetail[i]);
      tLLClaimDetailSchema.setRgtNo(request.getParameter("RgtNo"));
      tLLClaimDetailSchema.setClmNo(request.getParameter("ClmNo"));
      tLLClaimDetailSchema.setGetDutyCode(strGetDutyCodeDetail[i]);
      tLLClaimDetailSchema.setStandPay(strStandPayDetail[i]);
      tLLClaimDetailSchema.setRealPay(strRealPayDetail[i]);
      tLLClaimDetailSchema.setStatType(strStatTypeDetail[i]);
      tLLClaimDetailSchema.setGetDutyKind(strGetDutyKindDetail[i]);
      tLLClaimDetailSchema.setDutyCode(strDutyCodeDetail[i]);
      System.out.println("LLClaimDetial.DutyCode"+tLLClaimDetailSchema.getDutyCode());
      System.out.println("add claimdetail");
      tLLClaimDetailSet.add(tLLClaimDetailSchema);
    }
  }           //添加LLClaimDetail

  int PayCount = 0;
  String PayNo[] = request.getParameterValues("ClaimPayGridNo");
  System.out.println("get PayNo");
  //System.out.println("PolNo==="+strPolNoPay[0]);
  if (PayNo!=null)
    PayCount = PayNo.length;
  else
    PayCount = 0;
  System.out.println("PayCount==="+PayCount);
  System.out.println("PayDetailCount==="+PayDetailCount);
  for (int i = 0;i<PayCount;i++)
  {
    String tGetDutyKind="000000";
    for (int j = 0;j<PayDetailCount;j++)
    {

      if (strPolNoPay[i].equals(strPolNoDetail[j]))
      {
      System.out.println("strPolNoPay[i]"+strPolNoPay[i]);
      System.out.println("strPolNoDetail[j]"+strPolNoDetail[j]);
      tGetDutyKind=strGetDutyKindDetail[j];
      break;
    }

    }
    System.out.println("GetDutyKind==="+tGetDutyKind);


    LJSGetClaimSchema tLJSGetClaimSchema = new LJSGetClaimSchema();
    tLJSGetClaimSchema.setPolNo(strPolNoPay[i]);
    System.out.println("getPolNo end");
    System.out.println("strSCPay==="+strSCPay[i]);
    if (Double.parseDouble(strSCPay[i])>0)
    {
      tLJSGetClaimSchema.setPay(Double.parseDouble(strSCPay[i]));
      System.out.println("aaaaa");
      tLJSGetClaimSchema.setGetDutyKind(tGetDutyKind);
      tLJSGetClaimSchema.setFeeFinaType("GF");
      tLJSGetClaimSchema.setFeeOperationType(tGetDutyKind);
      tLJSGetClaimSet.add(tLJSGetClaimSchema);
    }
    System.out.println("SCPAY end");
    if (Double.parseDouble(strSWPay[i])>0)
    {
      tLJSGetClaimSchema.setPay(Double.parseDouble(strSWPay[i]));
      tLJSGetClaimSchema.setGetDutyKind(tGetDutyKind);
      tLJSGetClaimSchema.setFeeFinaType("GF");
      tLJSGetClaimSchema.setFeeOperationType(tGetDutyKind);
      tLJSGetClaimSet.add(tLJSGetClaimSchema);
    }

    if (Double.parseDouble(strYLPay[i])>0)
    {
      tLJSGetClaimSchema.setPay(strYLPay[i]);
      tLJSGetClaimSchema.setGetDutyKind(tGetDutyKind);
      tLJSGetClaimSchema.setFeeFinaType("GF");
      tLJSGetClaimSchema.setFeeOperationType(tGetDutyKind);
      tLJSGetClaimSet.add(tLJSGetClaimSchema);
    }

    if (Double.parseDouble(strTBPay[i])>0)
    {
      tLJSGetClaimSchema.setPay(strTBPay[i]);
      tLJSGetClaimSchema.setGetDutyKind(tGetDutyKind);
      tLJSGetClaimSchema.setFeeFinaType("TB");
      tLJSGetClaimSchema.setFeeOperationType(tGetDutyKind);
      tLJSGetClaimSet.add(tLJSGetClaimSchema);
    }

    if (Double.parseDouble(strTFPay[i])>0)
    {
      tLJSGetClaimSchema.setPay(strTFPay[i]);
      tLJSGetClaimSchema.setGetDutyKind(tGetDutyKind);
      tLJSGetClaimSchema.setFeeFinaType("TF");
      tLJSGetClaimSchema.setFeeOperationType(tGetDutyKind);
      tLJSGetClaimSet.add(tLJSGetClaimSchema);
    }

    if (Double.parseDouble(strQTPay[i])>0)
    {
      tLJSGetClaimSchema.setPay(strQTPay[i]);
      tLJSGetClaimSchema.setGetDutyKind(tGetDutyKind);
      tLJSGetClaimSchema.setFeeFinaType("TF");
      tLJSGetClaimSchema.setFeeOperationType(tGetDutyKind);
      tLJSGetClaimSet.add(tLJSGetClaimSchema);
    }

  }           //添加LJSGetClaim



  System.out.println("transfer end");
  System.out.println("tG==="+tG.Operator);
  ClaimSaveUI tClaimSaveUI   = new ClaimSaveUI();
  // 准备传输数据 VData
  VData tVData = new VData();
  try
  {
    //此处需要根据实际情况修改
    tVData.addElement(tLLClaimSchema);
    tVData.addElement(tLLClaimPolicySet);
    tVData.addElement(tLLClaimDetailSet);
    tVData.addElement(tLJSGetClaimSet);
    tVData.addElement(tLLCaseAccSet);//后添加
    tVData.addElement(tG);


    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    tClaimSaveUI.submitData(tVData,transact);

  }
  catch(Exception ex)
  {
    ex.printStackTrace();
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr=="")
  {
    tError = tClaimSaveUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

  if (FlagStr == "Succ") //保存成功，作自动核赔，置核赔错误信息表
  {
    tVData.clear();

    transact = "AutoChkPol";
    for (int i = 0;i<PolicyCount;i++)
    {
      LLClaimUnderwriteSchema tLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema();
      tLLClaimUnderwriteSchema.setClmNo(tLLClaimSchema.getClmNo());
      tLLClaimUnderwriteSchema.setRgtNo(tLLClaimSchema.getRgtNo());
      tLLClaimUnderwriteSchema.setPolNo(strPolNo[i]);
      ClaimAutoChkUI tClaimAutoChkUI   = new ClaimAutoChkUI();
      try
      {
        //此处需要根据实际情况修改

        tVData.clear();
        tVData.addElement(tLLClaimUnderwriteSchema);
        System.out.println("555555555");

        tVData.addElement(tG);
        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        System.out.println("4444444444");
        tClaimAutoChkUI.submitData(tVData,transact);
      }
      catch(Exception ex)
      {
        Content = transact+"失败，原因是:" + ex.toString() + "，保单号：" + tLLClaimUnderwriteSchema.getPolNo();
        FlagStr = "Fail";
        break;
      }
    }           //添加LLClaimUnderwrite


  }




  if (FlagStr == "Succ")
  {
    //显示赔案号码

  %>
    <script language="javascript">
      parent.fraInterface.fm.all("ClmNo").value="<%=tLLClaimSchema.getClmNo()%>";
  </script>
<%
  }

/*


*/



}




//如果在Catch中发现异常，则不从错误类中提取错误信息
//添加各种预处理

%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>