<%
//Name：ReportQueryInit.jsp
//Function：初始化报案的查询界面
//Author  ：LiuYansong
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script language="JavaScript">
  function reportDetailClick(cObj)
  {
    var ex,ey;
    ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
    ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
    divDetailInfo.style.left=ex;
    divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
  }

  // 输入框的初始化（单记录部分）
  function initInpBox()
  {
    try
    {
    }
    catch(ex)
    {
      alert("在ReportQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
  }

  function initSelBox()
  {
    try
    {
    }
    catch(ex)
    {
      alert("在ReportQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
    }
  }

  function initForm()
  {
    try
    {
      initInpBox();
      initSelBox();
      initReportGrid();
    }
    catch(re)
    {
      alert("ReportQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
  }

  var ReportGrid;
  function initReportGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="报案号";    	//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="事故者姓名";         			//列名
      iArray[2][1]="200px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;


      iArray[3]=new Array();
      iArray[3][0]="号码类型";         			//列名
      iArray[3][1]="0px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="号码";         			//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="报案受理日期";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;

      iArray[6]=new Array();
      iArray[6][0]="客户号码";         		//列名
      iArray[6][1]="160px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;


      ReportGrid = new MulLineEnter( "fm" , "ReportGrid" );
      ReportGrid.mulLineCount = 8;
      ReportGrid.displayTitle = 1;
      ReportGrid.canSel=1;
      ReportGrid.loadMulLine(iArray);
      ReportGrid.detailInfo="单击显示详细信息";
      ReportGrid.detailClick=reportDetailClick;
      }
      catch(ex)
      {
        alert(ex);
      }
  }
</script>