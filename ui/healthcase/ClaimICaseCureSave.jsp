<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llhealthcase.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>



<%
  LLCaseCureSchema tLLCaseCureSchema   = new LLCaseCureSchema();
  LLCaseCureSchema mLLCaseCureSchema = new LLCaseCureSchema();
  LLCaseCureSet tLLCaseCureSet=new LLCaseCureSet();
  CaseCureUI tCaseCureUI   = new CaseCureUI();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "INSERT";
  mLLCaseCureSchema.setCaseNo(request.getParameter("CaseNo"));

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
	//读取立案明细信息
  String strCaseNo=request.getParameter("CaseNo");


  String[] strNumber=request.getParameterValues("ICaseCureGridNo");
  String[] strHospitalCode=request.getParameterValues("ICaseCureGrid1");
  String[] strHospitalName=request.getParameterValues("ICaseCureGrid2");
  String[] strFeeItemCode=request.getParameterValues("ICaseCureGrid3");
  String[] strFeeItemName=request.getParameterValues("ICaseCureGrid4");
  String[] strApplyFee=request.getParameterValues("ICaseCureGrid5");
  String[] strFee=request.getParameterValues("ICaseCureGrid6");
  String[] strCureNo=request.getParameterValues("ICaseCureGrid7");
  String[] strDiseaseCode=request.getParameterValues("ICaseCureGrid8");
  String[] strDiseaseName=request.getParameterValues("ICaseCureGrid9");
  String[] strDiagnosis=request.getParameterValues("ICaseCureGrid10");

  int intLength=strNumber.length;
  for(int i=0;i<intLength;i++)
  {
  	tLLCaseCureSchema=new LLCaseCureSchema();
  	tLLCaseCureSchema.setCaseNo(strCaseNo);
  	tLLCaseCureSchema.setHospitalCode(strHospitalCode[i]);
  	tLLCaseCureSchema.setHospitalName(strHospitalName[i]);
    tLLCaseCureSchema.setFeeItemCode(strFeeItemCode[i]);
    tLLCaseCureSchema.setFeeItemName(strFeeItemName[i]);
    tLLCaseCureSchema.setApplyFee(strApplyFee[i]);
    tLLCaseCureSchema.setFee(strFee[i]);
  	tLLCaseCureSchema.setCureNo(strCureNo[i]);
  	tLLCaseCureSchema.setDiseaseCode(strDiseaseCode[i]);
  	tLLCaseCureSchema.setDiseaseName(strDiseaseName[i]);
  	tLLCaseCureSchema.setDiagnosis(strDiagnosis[i]);
    tLLCaseCureSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLLCaseCureSchema.setCustomerName(request.getParameter("CustomerName"));
    tLLCaseCureSchema.setAccidentType(request.getParameter("AccidentType"));
    tLLCaseCureSet.add(tLLCaseCureSchema);
  }

    GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");

  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLLCaseCureSet);
   tVData.addElement(mLLCaseCureSchema);
   tVData.addElement(tG);

   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tCaseCureUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCaseCureUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>