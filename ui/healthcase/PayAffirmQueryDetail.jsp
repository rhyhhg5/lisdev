<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：PayAffirmQueryDetail.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  LLClaimSchema tLLClaimSchema   = new LLClaimSchema();
  tLLClaimSchema.setRgtNo(request.getParameter("RgtNo"));
  System.out.println("rgtno="+tLLClaimSchema.getRgtNo());
//  tLLClaimSchema.setClmNo(request.getParameter("ClmNo"));


  VData tVData = new VData();
  tVData.addElement(tLLClaimSchema);

  System.out.println("RgtNo..."+tLLClaimSchema.getRgtNo());
  System.out.println("ClmNo..."+tLLClaimSchema.getClmNo());

  PayAffirmReturnUI tPayAffirmReturnUI = new PayAffirmReturnUI();
	if (!tPayAffirmReturnUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tPayAffirmReturnUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tPayAffirmReturnUI.getResult();
		System.out.println("calculate end");
		System.out.println(tVData.size());
		// 显示
		// 保单信息
		LLClaimSchema mLLClaimSchema = new LLClaimSchema();
		LLClaimSet mLLClaimSet=new LLClaimSet();
		mLLClaimSet=(LLClaimSet)tVData.getObjectByObjectName("LLClaimSet",0);
		LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
		tLLRegisterSchema=(LLRegisterSchema)tVData.getObjectByObjectName("LLRegisterSchema",0);

		LLClaimPolicySet mLLClaimPolicySet=new LLClaimPolicySet();
		mLLClaimPolicySet=(LLClaimPolicySet)tVData.getObjectByObjectName("LLClaimPolicySet",0);
		mLLClaimSchema=mLLClaimSet.get(1);
		String tState=CaseFunPub.getCaseStateByRgtNo(mLLClaimSchema.getRgtNo());

		
		System.out.println("llclaim.ClmNo=="+mLLClaimSchema.getClmNo());
		%>
    	<script language="javascript">

					top.opener.ClaimPolGrid.clearData("ClaimPolGrid");
					top.opener.fm.all("RgtNo").value = "<%=mLLClaimSchema.getRgtNo()%>";
					top.opener.fm.all("ClmNo").value = "<%=mLLClaimSchema.getClmNo()%>";
					top.opener.fm.all("CustomerName").value = "<%=mLLClaimPolicySet.get(1).getInsuredName()%>";
				        top.opener.fm.all("ClmState").value = "<%=tState%>";
				        top.opener.fm.all("CasePayType").value = "<%=mLLClaimSchema.getCasePayType()%>";
				        top.opener.fm.all("RealPay").value = "<%=mLLClaimSchema.getRealPay()%>";
				        top.opener.fm.all("CaseGetMode").value = "<%=tLLRegisterSchema.getCaseGetMode()%>";
				        top.opener.fm.all("BankCode").value = "<%=tLLRegisterSchema.getBankCode()%>";
				        top.opener.fm.all("AccName").value = "<%=tLLRegisterSchema.getAccName()%>";
				        top.opener.fm.all("BankAccNo").value = "<%=tLLRegisterSchema.getBankAccNo()%>";
				        top.opener.emptyUndefined();
				        top.opener.showCodeName();
	        <%
	      			LJSGetSchema mLJSGetSchema = new LJSGetSchema();
							LJSGetSet mLJSGetSet = new LJSGetSet();
							mLJSGetSet = (LJSGetSet)tVData.getObjectByObjectName("LJSGetSet",0);
	
							if (mLJSGetSet.size()!=0)
							mLJSGetSchema = mLJSGetSet.get(1);
  
	        %>
	      	 top.opener.fm.all("GetNoticeNo").value = "<%=mLJSGetSchema.getGetNoticeNo()%>";
					<%

  					System.out.println("policy.size==="+mLLClaimPolicySet.size());
  					System.out.println("PolNo==="+mLLClaimPolicySet.get(1).getPolNo());
  					for (int i = 1;i<=mLLClaimPolicySet.size();i++)
  					{
		%>
								top.opener.ClaimPolGrid.addOne("ClaimPolGrid");
						  	top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,1,"<%=mLLClaimPolicySet.get(i).getPolNo()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,2,"<%=mLLClaimPolicySet.get(i).getRiskCode()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,3,"<%=mLLClaimPolicySet.get(i).getCaseNo()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,4,"<%=mLLClaimPolicySet.get(i).getInsuredNo()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,5,"<%=mLLClaimPolicySet.get(i).getInsuredName()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,6,"<%=mLLClaimPolicySet.get(i).getCValiDate()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,7,"<%=mLLClaimPolicySet.get(i).getStandPay()%>");
  							top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,8,"<%=mLLClaimPolicySet.get(i).getRealPay()%>");
								top.opener.emptyUndefined();

		<%
  					}
  					
%>
    	</script>
	<%	
	}
 // end of if


  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tPayAffirmReturnUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  out.println("top.close();");
  out.println("</script>");

%>
