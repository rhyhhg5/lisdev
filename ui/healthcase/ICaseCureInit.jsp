<%
/*******************************************************************************
 * Name     :ICaseInit.jsp
 * Function :初始化“立案－费用明细信息”的程序
 * Author   :LiuYansong
 * Date     :2003-7-23
 */
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script language="JavaScript">
  var turnPage = new turnPageClass();
function initForm()
{
  try
  {
    initICaseCureGrid();
    initHospitalGrid();
    initFeeOperationGrid();
    initDegreeOperationGrid();
    initSuccorGrid();
    initOtherFeeGrid();
    showCaseCureInfo();
  }
  catch(re)
  {
    alert("ICaseCureInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//门诊费用明细信息
function initICaseCureGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="医院名称";    	//列名
    iArray[1][1]="170px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="费用项目类型";    	//列名
    iArray[2][1]="90px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=2;
    iArray[2][4]="FeeItemCode";

    iArray[3]=new Array();
    iArray[3][0]="费用项目类型代码";    	//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="费用项目类型名称";    	//列名
    iArray[4][1]="120px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=1;

    iArray[5]=new Array();
    iArray[5][0]="申请赔付费用金额";    	//列名
    iArray[5][1]="100px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=1;

    iArray[6]=new Array();
    iArray[6][0]="责任内费用金额";    	//列名
    iArray[6][1]="100px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=1;

    ICaseCureGrid = new MulLineEnter( "fm" , "ICaseCureGrid" );
    ICaseCureGrid.mulLineCount = 0;
    ICaseCureGrid.displayTitle = 1;
    ICaseCureGrid.canSel = 0;
    ICaseCureGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}

//住院费用明细信息
function initHospitalGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="医院名称";    	//列名
    iArray[1][1]="200px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="费用项目类型";    	//列名
    iArray[2][1]="90px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=2;  
    iArray[2][4]="FeeItemCode";            			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="费用项目类型代码";         			//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=1;

    iArray[4]=new Array();
    iArray[4][0]="费用项目类型名称";         			//列名
    iArray[4][1]="120px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=1;

    iArray[5]=new Array();
    iArray[5][0]="申请赔付费用金额";         			//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=60;            			//列最大值
    iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="责任内费用金额";         			//列名
    iArray[6][1]="60px";            		//列宽
    iArray[6][2]=60;            			//列最大值
    iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    HospitalGrid = new MulLineEnter( "fm" , "HospitalGrid" );
    HospitalGrid.mulLineCount = 0;
    HospitalGrid.displayTitle = 1;
    HospitalGrid.canSel = 0;
    HospitalGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
//初始化按费用录入手术信息
function initFeeOperationGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";

    iArray[1]=new Array();
    iArray[1][0]="手术名称";    	//列名
    iArray[1][1]="180px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;

    iArray[2]=new Array();
    iArray[2][0]="手术代码";    	//列名
    iArray[2][1]="60px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=2;

    iArray[3]=new Array();
    iArray[3][0]="手术费用";    	//列名
    iArray[3][1]="90px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=1;

    iArray[4]=new Array();
    iArray[4][0]="手术给付比例";    	//列名
    iArray[4][1]="90px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=1;

    FeeOperationGrid = new MulLineEnter( "fm" , "FeeOperationGrid" );
    FeeOperationGrid.mulLineCount = 0;
    FeeOperationGrid.displayTitle = 1;
    FeeOperationGrid.canSel = 0;
    FeeOperationGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在FeeOperationGrid中出错");
    alert(ex);
  }
}
//初始化按档次录入初始化信息
function initDegreeOperationGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";

    iArray[1]=new Array();
    iArray[1][0]="手术名称";    	//列名
    iArray[1][1]="180px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;

    iArray[2]=new Array();
    iArray[2][0]="手术代码";    	//列名
    iArray[2][1]="60px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=2;

    iArray[3]=new Array();
    iArray[3][0]="手术费用";    	//列名
    iArray[3][1]="90px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=1;

    iArray[4]=new Array();
    iArray[4][0]="手术档次";    	//列名
    iArray[4][1]="90px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=1;

    DegreeOperationGrid = new MulLineEnter( "fm" , "DegreeOperationGrid" );
    DegreeOperationGrid.mulLineCount = 0;
    DegreeOperationGrid.displayTitle = 1;
    DegreeOperationGrid.canSel = 0;
    DegreeOperationGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在DegreeOperation中出错");
    alert(ex);
  }
}

function initSuccorGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";

    iArray[1]=new Array();
    iArray[1][0]="救援医院名称";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="210px";         			//列宽
    iArray[1][2]=100;          			//列最大值
    iArray[1][3]=1;

    iArray[2]=new Array();
    iArray[2][0]="代理救援机构";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="180px";         			//列宽
    iArray[2][2]=100;          			//列最大值
    iArray[2][3]=1;

    iArray[3]=new Array();
    iArray[3][0]="费用项目类型";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="90px";         			//列宽
    iArray[3][2]=100;          			//列最大值
    iArray[3][3]=1;

    iArray[4]=new Array();
    iArray[4][0]="费用项目类型代码";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="90px";         			//列宽
    iArray[4][2]=100;          			//列最大值
    iArray[4][3]=2;
    iArray[4][4]="succorcode";
    iArray[4][5]="4|5";
    iArray[4][6]="0|1";

    iArray[5]=new Array();
    iArray[5][0]="费用项目类型名称";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[5][1]="200px";         			//列宽
    iArray[5][2]=100;          			//列最大值
    iArray[5][3]=1;


    iArray[6]=new Array();
    iArray[6][0]="申请赔付费用金额";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[6][1]="120px";         			//列宽
    iArray[6][2]=100;          			//列最大值
    iArray[6][3]=1;

    iArray[7]=new Array();
    iArray[7][0]="责任内费用金额";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[7][1]="120px";         			//列宽
    iArray[7][2]=100;          			//列最大值
    iArray[7][3]=1;

    SuccorGrid = new MulLineEnter( "fm" , "SuccorGrid" );
    SuccorGrid.mulLineCount = 0;
    SuccorGrid.displayTitle = 1;
    SuccorGrid.canSel = 0;
    SuccorGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在救援费用中出错");
    alert(ex);
  }
}
//其他费用信息
function initOtherFeeGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";

    iArray[1]=new Array();
    iArray[1][0]="费用类型";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="210px";         			//列宽
    iArray[1][2]=100;          			//列最大值
    iArray[1][3]=1;

    iArray[2]=new Array();
    iArray[2][0]="费用代码";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="180px";         			//列宽
    iArray[2][2]=100;          			//列最大值
    iArray[2][3]=1;

    iArray[3]=new Array();
    iArray[3][0]="费用名称";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="90px";         			//列宽
    iArray[3][2]=100;          			//列最大值
    iArray[3][3]=1;

    iArray[4]=new Array();
    iArray[4][0]="申请费用金额";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="90px";         			//列宽
    iArray[4][2]=100;          			//列最大值
    iArray[4][3]=1;
    
    iArray[5]=new Array();
    iArray[5][0]="责任内费用金额";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[5][1]="200px";         			//列宽
    iArray[5][2]=100;          			//列最大值
    iArray[5][3]=1;


    iArray[6]=new Array();
    iArray[6][0]="备注3";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[6][1]="120px";         			//列宽
    iArray[6][2]=100;          			//列最大值
    iArray[6][3]=1;

    iArray[7]=new Array();
    iArray[7][0]="备注4";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[7][1]="120px";         			//列宽
    iArray[7][2]=100;          			//列最大值
    iArray[7][3]=1;

    OtherFeeGrid = new MulLineEnter( "fm" , "OtherFeeGrid" );
    OtherFeeGrid.mulLineCount = 0;
    OtherFeeGrid.displayTitle = 1;
    OtherFeeGrid.canSel = 0;
    OtherFeeGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在救援费用中出错");
    alert(ex);
  }
}
</script>
