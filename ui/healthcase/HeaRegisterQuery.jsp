<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
  var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
  var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="RegisterQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="HeaRegisterQueryInit.jsp"%>
  <title>立案信息查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./RegisterQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 报案信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLLReport1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title> 立案号 </TD>
          <TD  class= input> <Input class= common name=RgtNo > </TD>
          <TD  class= title> 立案日期 </TD>
          <TD  class= input> <Input class= common name=RptNo> </TD>
        </TR>

        <TR  class= common>
          <TD  class= title> 团单号码 </TD> 
          <TD  class= input> <Input class= common name=No > </TD>
          <TD  class= title> 立案申请人姓名 </TD>
          <TD  class= input> <Input class= common name=PeopleName > </TD>
        </TR>
      </table>
    </Div>
          <INPUT class=common VALUE="查    询" TYPE=button onclick="submitForm();return false;">
          <INPUT class=common VALUE="返    回" TYPE=button onclick="returnParent();">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 立案信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanRegisterGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button class=common onclick="turnPage.firstPage();" >
      <INPUT VALUE="上一页" TYPE=button class=common onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页" TYPE=button class=common onclick="turnPage.nextPage();">
      <INPUT VALUE="尾  页" TYPE=button class=common onclick="turnPage.lastPage();">
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>