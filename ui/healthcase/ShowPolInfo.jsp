<%@page contentType="text/html;charset=GBK" %>
<%
//Name：ShowPolInfo.jsp
//Function：显示客户的保单信息
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
<%
  //输出参数
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
	String InsuredNo=request.getParameter("InsuredNo");

  VData tVData = new VData();
  tVData.addElement(InsuredNo);
  System.out.println("2003-12-30客户号码是"+InsuredNo);
  ShowPolInfoUI mShowPolInfoUI = new ShowPolInfoUI();
  if (!mShowPolInfoUI.submitData(tVData,"QUERY||MAIN"))
	{
    Content = " 查询失败，原因是: " + mShowPolInfoUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
  }
	else
	{
    tVData.clear();
    tVData = mShowPolInfoUI.getResult();
    LCPolSet mLCPolSet = new LCPolSet();
    LCPolSchema mLCPolSchema = new LCPolSchema();
    mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
		int n = mLCPolSet.size();
		System.out.println("查询的记录是 "+n);
		String Strtest = "0|" + n + "^" + mLCPolSet.encode();
    System.out.println("QueryResult: " + Strtest);
  %>
    <script language="javascript">
      try
      {
        parent.fraInterface.displayQueryResult('<%=Strtest%>');
      }
      catch(ex) {}
    </script>
  <%
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = mShowPolInfoUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 初始化成功！！！ ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 初始化失败，原因是:" + tError.getFirstError()+"！！！！";
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>