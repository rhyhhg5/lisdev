<%
//程序名称：LLReportInput.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="com.sinosoft.lis.llhealthcase.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");

  //接收信息，并作校验处理。
  //输入参数
  //报案信息
  String a_Name="";
  LLReportSchema tLLReportSchema   = new LLReportSchema();
  LLReportSet tLLReportSet=new LLReportSet();
  //分报案信息
  LLSubReportSchema tLLSubReportSchema=new LLSubReportSchema();
  LLSubReportSet tLLSubReportSet=new LLSubReportSet();

  ReportUI tReportUI   = new ReportUI();
  SubReportUI tSubReportUI=new SubReportUI();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "INSERT";


  String tRela  = "";
  String FlagStr = "";
  String Content = "";

  //获取报案信息(问题一：为什么//)
  String strRptNo=request.getParameter("RptNo");
  //处理转意字符
  String Path = application.getRealPath("config//Conversion.config");
  tLLReportSchema.setAccidentReason(StrTool.Conversion(request.getParameter("AccidentReason"),Path));
  tLLReportSchema.setAccidentCourse(StrTool.Conversion(request.getParameter("AccidentCourse"),Path));
  tLLReportSchema.setRemark(StrTool.Conversion(request.getParameter("Remark"),Path));
  tLLReportSchema.setRptObj(request.getParameter("RptObj"));
  tLLReportSchema.setRptObjNo(request.getParameter("RptObjNo"));
  tLLReportSchema.setRptorName(request.getParameter("RptorName"));
  tLLReportSchema.setRptorAddress(request.getParameter("RptorAddress"));
  tLLReportSchema.setRptorPhone(request.getParameter("RptorPhone"));
  tLLReportSchema.setRelation(request.getParameter("Relation"));
  tLLReportSchema.setRptDate(request.getParameter("RptDate"));
  tLLReportSchema.setRptMode(request.getParameter("RptMode"));
  tLLReportSchema.setAccidentSite(request.getParameter("AccidentSite"));
  tLLReportSchema.setAccidentDate(request.getParameter("AccidentDate"));
  tLLReportSchema.setMngCom(request.getParameter("MngCom"));
  tLLReportSchema.setCustomerType(request.getParameter("PeopleType"));
  tLLReportSet.add(tLLReportSchema);

  String[] tChk = request.getParameterValues("InpSubReportGridChk");
  String[] strNumber=request.getParameterValues("SubReportGridNo");
  String[] strCustomerNo=request.getParameterValues("SubReportGrid1");
  String[] strCustomerName=request.getParameterValues("SubReportGrid2");
  String[] strHospitalCode=request.getParameterValues("SubReportGrid3");
  String[] strHospitalName=request.getParameterValues("SubReportGrid4");
  String[] strInHospitalDate=request.getParameterValues("SubReportGrid5");
  String[] strOutHospitalDate=request.getParameterValues("SubReportGrid6");
  String[] strRemark=request.getParameterValues("SubReportGrid7");
  String[] strSubRptNo=request.getParameterValues("SubReportGrid8");
  String[] strAccidentType=request.getParameterValues("SubReportGrid9");

  int intLength=0;
  //对LLSubReport进行
  if(strNumber!=null)
    intLength=strNumber.length;
  for(int i=0;i<intLength;i++)
  {
    System.out.println("值是"+tChk[i]);
    if(tChk[i].equals("0")) //未选
      continue;
    System.out.println("当i等于"+i+"时，选中了");
    tLLSubReportSchema=new LLSubReportSchema();
    tLLSubReportSchema.setCustomerNo(strCustomerNo[i]);
    tLLSubReportSchema.setCustomerName(strCustomerName[i]);
    tLLSubReportSchema.setHospitalCode(strHospitalCode[i]);
    tLLSubReportSchema.setHospitalName(strHospitalName[i]);
    tLLSubReportSchema.setInHospitalDate(strInHospitalDate[i]);
    tLLSubReportSchema.setOutHospitalDate(strOutHospitalDate[i]);
    System.out.println("i 是"+i+"时ReportSave.jsp中的入院日期是===="+tLLSubReportSchema.getInHospitalDate());
    tLLSubReportSchema.setRemark(strRemark[i]);
    tLLSubReportSchema.setAccidentType(strAccidentType[i]);
    tLLSubReportSchema.setCaseNo("00000000000000000000");
    tLLSubReportSet.add(tLLSubReportSchema);
  }
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  VData tVData = new VData();
  try
  {
  // 准备传输数据 VData

   //保存报案信息
   tVData.addElement(tLLReportSet);
   tVData.addElement(tLLSubReportSet);
   tVData.addElement(tG);
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tReportUI.submitData(tVData,transact);

  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tReportUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
    	FlagStr = "Succ";
    	tVData.clear();
    	tVData = tReportUI.getResult();
	    //反写报案号的语句
	    LLReportSchema ttLLReportSchema = new LLReportSchema();
	    LLReportBLSet yLLReportBLSet = new LLReportBLSet();

	    yLLReportBLSet.set((LLReportBLSet)tVData.getObjectByObjectName("LLReportBLSet",0));
	    ttLLReportSchema.setSchema(yLLReportBLSet.get(1));
			a_Name = (String)tVData.get(0);
	    System.out.println("ReportNo==="+ttLLReportSchema.getRptNo());
	    %>
	    <script language="javascript">
	    parent.fraInterface.fm.all("RptNo").value="<%=ttLLReportSchema.getRptNo()%>";
			parent.fraInterface.fm.all("RptDate").value="<%=ttLLReportSchema.getRptDate()%>";
			parent.fraInterface.fm.all("Operator").value="<%=a_Name%>";
			parent.fraInterface.fm.all("MngCom").value="<%=ttLLReportSchema.getMngCom()%>";
      parent.fraInterface.fm.all("CaseState").value="待立案";
			</script>
			<%

      //反写分报案号的语句
      LLSubReportSet yLLSubReportSet = new LLSubReportSet();
      yLLSubReportSet.set((LLSubReportBLSet)tVData.getObjectByObjectName("LLSubReportBLSet",0));
      int m = yLLSubReportSet.size();
      for (int i =0;i<m;i++)
      {
      	LLSubReportSchema ttLLSubReportSchema = new LLSubReportSchema();
      	ttLLSubReportSchema.setSchema(yLLSubReportSet.get(i+1));

      System.out.println("SubRptNo=="+ttLLSubReportSchema.getSubRptNo());
      %>

	<script language="javascript">
		parent.fraInterface.SubReportGrid.setRowColData(<%=i%>,8,"<%=ttLLSubReportSchema.getSubRptNo()%>");
	</script>
<%
    }
    }

    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>