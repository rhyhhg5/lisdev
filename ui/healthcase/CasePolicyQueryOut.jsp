<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ReportQueryOut.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  System.out.println("---hahahahah---");
  // 保单信息部分
    LLRegisterSchema tLLRegisterSchema   = new LLRegisterSchema();  
    tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
    tLLRegisterSchema.setRgtObj(request.getParameter("RgtObj"));
    tLLRegisterSchema.setRgtObjNo(request.getParameter("RgtObjNo"));
//    tLLRegisterSchema.setAccidentDate(request.getParameter("AccidentDate"));
//    tLLRegisterSchema.setRgtDate(request.getParameter("RgtDate"));
    tLLRegisterSchema.setCalFlag("0");
    tLLRegisterSchema.setDeclineFlag("0");
    LLCaseSchema tLLCaseSchema = new LLCaseSchema();
    tLLCaseSchema.setCustomerName(request.getParameter("CustomerName"));
    GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
  // 准备传输数据 VData
    VData tVData = new VData();
    tVData.addElement(tLLRegisterSchema);
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tG);

  // 数据传输
  RegisterUI tRegisterUI   = new RegisterUI();
	if (!tRegisterUI.submitData(tVData,"QUERYALL"))
	{
      Content = " 查询失败，原因是: " + tRegisterUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tRegisterUI.getResult();
		
		// 显示
		String tReturnStr=(String)tVData.getObjectByObjectName("String",0);
		System.out.println("ReturnString is :"+tReturnStr);
		if (tReturnStr.length()!=0)
		{
		   	%>
		<script language="javascript">
		try {
		 parent.fraInterface.displayQueryResult('<%=tReturnStr%>');
		 } catch(ex) {}
		</script>
			<%
		}
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tRegisterUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

