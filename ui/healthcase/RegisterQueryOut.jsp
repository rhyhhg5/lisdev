<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
 * Name    :RegisterQueryOut.jsp
 * Function:1.向后台查询程序传递查询的条件
 *          2.接收查询结果并将其显示在MulLine列表中
 * Author  ：LiuYansong
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
<%
  //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    System.out.println("开始执行。jsp");
    String tNo = request.getParameter("No");//接收号码
    String tNoType = request.getParameter("NoType");//接收客户号码
    LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
    tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
    tLLRegisterSchema.setRptNo(request.getParameter("RptNo"));

    LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
    LLCaseSchema tLLCaseSchema = new LLCaseSchema();
    if(!(tNoType==null||tNoType.equals("")))
    {
      if(tNoType.equals("0"))
        tLLCasePolicySchema.setPolNo(tNo);//0是保单号码
      if(tNoType.equals("1"))
      {
        tLLCaseSchema.setCustomerNo(tNo);//1是客户号码
      }
    }
    tLLCaseSchema.setCustomerName(request.getParameter("PeopleName"));

    GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
    System.out.println("在jsp中传入的管理机构是"+tG.ManageCom);

    VData tVData = new VData();
    tVData.addElement(tLLRegisterSchema);
    tVData.addElement(tLLCasePolicySchema);
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tG);
    RegisterUI tRegisterUI = new RegisterUI();
    if (!tRegisterUI.submitData(tVData,"QueryRgtInfo"))
    {
    		Content = " 查询失败，原因是: " + tRegisterUI.mErrors.getError(0).errorMessage;
    		FlagStr = "Fail";
    }
    else
    {
      tVData.clear();
      tVData = tRegisterUI.getResult();
      LLRegisterSet mLLRegisterSet = new LLRegisterSet();
      mLLRegisterSet.set((LLRegisterSet)tVData.getObjectByObjectName("LLRegisterSet",0));
      int n = mLLRegisterSet.size();
      System.out.println("get Register "+n);
      String Strtest = "0|" + n + "^" + mLLRegisterSet.encode();
      System.out.println("Strtest==="+Strtest);
      %>
      <script language="javascript">
        try
        {
          parent.fraInterface.displayQueryResult('<%=Strtest%>');
        }
        catch(ex) {}
      </script>
      <%
        } // end of if

     //如果在Catch中发现异常，则不从错误类中提取错误信息
     if (FlagStr == "Fail")
     {
       tError = tRegisterUI.mErrors;
       if (!tError.needDealError())
       {
         Content = " 查询成功! ";
         FlagStr = "Succ";
       }
       else
       {
         Content = " 查询失败，原因是:" + tError.getFirstError();
         FlagStr = "Fail";
       }
     }
     System.out.println("------end------");
     System.out.println(FlagStr);
     System.out.println(Content);
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>