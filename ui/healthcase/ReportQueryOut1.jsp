<%@page contentType="text/html;charset=GBK" %>
<%
//Name    ：ReportQueryOut1.jsp(刘岩松修改)
//Function：根据“事故者类型”、“号码”、“号码类型”在LCPol表中查询出向对应的记录
//Author  ：LiuYansong
//创建日期：2002-09-22
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
<%
  //输出参数
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  SSRS ssrs = new SSRS();

  String RptObj = request.getParameter("RptObj");
  String RptObjNo = request.getParameter("RptObjNo");
  String PeopleType = request.getParameter("PeopleType");

  if( RptObjNo == null )
  {
    out.println("RptObjNo == null");
    return;
  }
  if((PeopleType==null)||(PeopleType.equals("")))
  {
    System.out.println("事故者类型是空，不能对此进行查询");
    return;
  }
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  VData tVData = new VData();
  tVData.addElement(RptObj);
  tVData.addElement(RptObjNo);
  tVData.addElement(PeopleType);
  tVData.addElement(tG);

  ReportQueryUI tReportQueryUI   = new ReportQueryUI();
	if (!tReportQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tReportQueryUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tReportQueryUI.getResult();
 		ssrs=(SSRS)tVData.getObjectByObjectName("SSRS",0);
		if(ssrs.getMaxRow()!=0)
    {
      int n = ssrs.getMaxRow();
      System.out.println("get report "+n);
	    %>
	    <script language="javascript">
		    parent.fraInterface.SubReportGrid.clearData();

	    </script>
	    <%
		  String tFlag = "0";
		  int tRow=0;
      for (int i = 1; i <= n; i++)
      {
        //对于团单的情况
        tRow=i;
        if(RptObj.equals("0"))
        {
          %>
          <script language="javascript">
            parent.fraInterface.SubReportGrid.addOne("SubReportGrid");
            parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,1,"<%=ssrs.GetText(i,1)%>");
            parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,2,"<%=ssrs.GetText(i,2)%>");
            parent.fraInterface.SubReportGrid.checkBoxAll ();
          </script>
          <%
        }
     //对个单和客户的情况
     if(RptObj.equals("1"))
     {
      //对于个单中的事故类型是被保险人的情况
       if(PeopleType.equals("0"))
       {
         %>
         <script language="javascript">
           parent.fraInterface.SubReportGrid.addOne("SubReportGrid");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,1,"<%=ssrs.GetText(i,1)%>");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,2,"<%=ssrs.GetText(i,2)%>");
           parent.fraInterface.SubReportGrid.checkBoxAll ();
         </script>
         <%
       }
       //对于个单中的事故类型是投保人的情况
       if(PeopleType.equals("1"))
       {
         %>
         <script language="javascript">
           parent.fraInterface.SubReportGrid.addOne("SubReportGrid");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,1,"<%=ssrs.GetText(i,3)%>");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,2,"<%=ssrs.GetText(i,4)%>");
           parent.fraInterface.SubReportGrid.checkBoxAll ();
         </script>
         <%
       }
       //对于个单中的事故类型是被保人配偶的情况
       if(PeopleType.equals("2"))
       {
         String People = ssrs.GetText(i,2)+"的配偶";
         %>
         <script language="javascript">
           parent.fraInterface.SubReportGrid.addOne("SubReportGrid");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,1,"<%=ssrs.GetText(i,1)%>");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,2,"<%=People%>");
           parent.fraInterface.SubReportGrid.checkBoxAll ();
         </script>
         <%
       }
       //对于个单中的事故者类型是被保人子女的情况
       if(PeopleType.equals("3"))
       {
         String People = ssrs.GetText(i,2)+"的子女";
         %>
         <script language="javascript">
           parent.fraInterface.SubReportGrid.addOne("SubReportGrid");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,1,"<%=ssrs.GetText(i,1)%>");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,2,"<%=People%>");
           parent.fraInterface.SubReportGrid.checkBoxAll ();
         </script>
         <%
       }
     }
 //对于是客户的情况
     if(RptObj.equals("2"))
     {
       //对于个单中的事故类型是被保险人的情况
       if(PeopleType.equals("0"))
       {
         %>
         <script language="javascript">
           parent.fraInterface.SubReportGrid.addOne("SubReportGrid");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,1,"<%=ssrs.GetText(i,1)%>");
           parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,2,"<%=ssrs.GetText(i,2)%>");
           parent.fraInterface.SubReportGrid.checkBoxAll ();
         </script>
         <%
        }
        //对于个单中的事故类型是投保人的情况
        if(PeopleType.equals("1"))
        {
          %>
          <script language="javascript">
            parent.fraInterface.SubReportGrid.addOne("SubReportGrid");
            parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,1,"<%=ssrs.GetText(i,1)%>");
            parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,2,"<%=ssrs.GetText(i,2)%>");
            parent.fraInterface.SubReportGrid.checkBoxAll ();
          </script>
          <%
        }
        //对于个单中的事故类型是被保人配偶的情况
        if(PeopleType.equals("2"))
        {
          String People = ssrs.GetText(i,2)+"的配偶";
          %>
          <script language="javascript">
            parent.fraInterface.SubReportGrid.addOne("SubReportGrid");
            parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,1,"<%=ssrs.GetText(i,1)%>");
            parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,2,"<%=People%>");
            parent.fraInterface.SubReportGrid.checkBoxAll ();
          </script>
          <%
        }
        //对于个单中的事故者类型是被保人子女的情况
        if(PeopleType.equals("3"))
        {
          String People = ssrs.GetText(i,2)+"的子女";
          %>
          <script language="javascript">
            parent.fraInterface.SubReportGrid.addOne("SubReportGrid");
            parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,1,"<%=ssrs.GetText(i,1)%>");
            parent.fraInterface.SubReportGrid.setRowColData(<%=tRow-1%>,2,"<%=People%>");
            parent.fraInterface.SubReportGrid.checkBoxAll ();
          </script>
          <%
        }
       }
      }
    }
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tReportQueryUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>