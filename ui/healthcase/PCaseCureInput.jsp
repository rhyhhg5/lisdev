<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：CrtHtml程序创建
//更新记录：  
//更新人:刘岩松
//更新日期:2002-09-24
//更新原因/内容:去掉＂附件序号 ＂录入对话框
//　　　　　　　　增加＂赔付标志＂双击选择框
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>


<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="PCaseCureInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PCaseCureInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./PCaseCureSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏PCaseCure1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPCaseInfo1);">
      </td>
      <td class= titleImg>
        分案诊疗主要信息
      </td>
    	</tr>
    </table>

    <Div  id= "divPCaseInfo1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            分案号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CaseNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            出险人客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
          <TD  class= title>
            出险人名称
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerName >
          </TD>
        </TR>
      </table>
      </DIV>
    <!--分案诊疗信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseInfo2);">
    		</td>
    		<td class= titleImg>
    			 分案疾病明细信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divCaseInfo2" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanPCaseInfoGrid" >
					</span> 
				</td>
			</tr>
		</table>
    
	</div>
	<!--分案伤残信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseInfo1);">
    		</td>
    		<td class= titleImg>
    			 分案伤残明细信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divCaseInfo1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanPCaseInjureGrid" >
					</span> 
				</td>
			</tr>
		</table>
		</Div>

         <INPUT VALUE="保存" class=common TYPE=button onclick="submitForm();"> 					
         <INPUT VALUE="返回" class=common TYPE=button onclick="top.close();"> 					

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
  fm.CustomerNo.value = '<%= request.getParameter("CustomerNo") %>';
  fm.CustomerName.value = '<%=StrTool.unicodeToGBK( request.getParameter("CustomerName")) %>';
}
catch(ex)
{
  alert(ex);
}
 
</script>

</html>
