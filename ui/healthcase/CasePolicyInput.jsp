<html>
<%
/*******************************************************************************
 * Name     :CasePolicyInput.jsp
 * Function :立案－立案险种明细的初始化页面程序
 * Date     :2003-7-21
 * Author   :LiuYansong
 */
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
	System.out.println(request.getParameter("CustomerName"));
	System.out.println("toGBK:"+StrTool.unicodeToGBK(request.getParameter("CustomerName")));
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<!--
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
-->

  <SCRIPT src="CasePolicyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CasePolicyInit.jsp"%>

</head>
<body  onload="initForm();" >
  <form action="./CasePolicySave.jsp" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏LLCase1的信息 -->
     <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
      </td>
      <td class= titleImg>
        立案分案信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLLCase1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD class= title>
            立案号
          </TD>
          <TD class= input>
            <Input class="readonly" readonly name = RgtNo>
          </TD>
          <TD class=input width="26%">
            <input class=common type=button value="查询" onclick="submitFormCasePolicy()">
          </TD>
          <!--
         <TD  class= title>
            分案号
          </TD>
          <TD  class= input>
          -->
            <Input class= "readonly" readonly name=CaseNo type= hidden >
<!--
          </TD>
-->
        </TR>

        <TR class= common>
          <TD  class= title>
            事故者客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=InsuredNo >
          </TD>
          <TD  class= title>
            事故者名称
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerName >
          </TD>
        </TR>
        <TR  class= common>
        	<TD  class= title>
            事故类型
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AccidentType >
          </TD>

        </TR>
      </table>
    </Div>
    <!--立案保单信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCasePolicy);">
    		</td>
    		<td class= titleImg>
    			 立案保单明细信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divCasePolicy" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanCasePolicyGrid" >
					</span>
				</td>
			</tr>
		</table>
  </div>
  <!--
    <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  	</Div>
  -->

         <INPUT VALUE="立案保单保存" TYPE=button class=common onclick="submitForm();">
        <!-- <INPUT VALUE="保单效力终止" TYPE=button class=common onclick="EndPolPower();">-->
        <!-- <INPUT VALUE="保单效力恢复" TYPE=button class=common onclick="StartPolPower();">-->
         <INPUT VALUE="返  回" TYPE=button class=common onclick="top.close();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
  fm.InsuredNo.value = '<%= request.getParameter("InsuredNo") %>';
  fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName")) %>';
  fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
  fm.AccidentType.value = '<%=request.getParameter("AccidentType")%>';
}
catch(ex)
{
  alert(ex);
}
</script>
</html>