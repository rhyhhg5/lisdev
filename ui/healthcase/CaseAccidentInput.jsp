<html>
<%
/*******************************************************************************
 * Name     :CaseAccidentInput.jsp
 * Function :立案－事故/伤残明细的主界面
 * company  :sinosoft
 * Author   :LiuYansong
 * Date     :2003-7-15
 */
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="CaseAccidentInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CaseAccidentInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./CaseAccidentSave.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;"
          OnClick= "showPage(this,divCaseReceipt1);">
      </td>
      <td class= titleImg>
        事故/伤残明细信息
      </td>
    	</tr>
    </table>

    <Div  id= "divCaseReceipt1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD class= title>
            立案号
          </TD>
          <TD class= input>
            <Input class="readonly" readonly name=RgtNo>
          </td>
          <TD class= title>
            残疾原因
          </TD>
          <TD class= input>
            <Input class="code" name=AccitentType verify="事故类型|NOTNULL"
              CodeData="0|^YW|意外事故^JB|疾病"
              ondblClick="showCodeListEx('AccidentTypeList',[this],[0,1]);"
              onkeyup="showCodeListKeyEx('AccidentTypeList',[this],[0,1]);">
          </TD>
          <!--
          <TD  class= title>
            分案号
          </TD>
          <TD  class= input>
          -->
            <Input class= "readonly" readonly name=CaseNo type=hidden >
      <!--
       </TD>
      -->
        </TR>

        <TR  class= common>
          <TD  class= title>
            事故者名称
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerName >
          </TD>
          <TD  class= title>
            事故者客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
            <Input class="common" type=hidden name = DisplayFlag>
        </TR>
        <tr class=common>
        <td>
         <INPUT VALUE = "查询代码" class=common type=button onclick="query_disease()">
        </td>
        </TR>
      </table>
    </Div>
     <!--
    <Div id = "divDisplayFlag_c" style= "display: 'none'">

      <table class=common>
        <TR class= common>
          <TD class= title>
            残疾类别
          </TD>
          <TD class= input>
            <Input class="code" name=DeformityType verify="事故类型|NOTNULL"
              CodeData="0|^0|普通残疾^1|烧伤残疾^2|重要器官切除"
              ondblClick="showCodeListEx('DeformityTypeList',[this],[0,1,2]);"
              onkeyup="showCodeListKeyEx('DeformityTypeList',[this],[0,1,2]);">
          </TD>
          <TD class= title>
            伤残给付比例
          </TD>
          <TD class= input>
            <Input class=common name = GiveDeformity >
          </TD>
        </TR>
      </table>

    </Div>
   -->

    <!--收据明细费用信息部分（列表） -->
    <Div id= "divAccidentInfo" style = "display: ''">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAccident);">
    		</td>
    		<td class= titleImg>
          意外事故明细信息
    		</td>
    	</tr>

    </table>

	<Div  id= "divAccident" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanCaseGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>
  </div>

  <Div id = "divDiseaseInfo" style = "display: ''">
    <Table>
      <TR>
        <TD>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDisease);">
        </TD>
        <TD class=titleImg>
          疾病明细
        </TD>
      </TR>
    </Table>

    <Div id = "divDisease" style = "display: ''">

      <Table class= common>
        <TR class= common>
          <TD text-align: left colSpan=1>
            <span id="spanDiseaseGrid" >
            </span>
          </TD>
        </TR>
      </Table>
    </Div>
  </Div>

  <Div id = "divDeformityInfo" style = "display: ''">
    <Table>
      <TR>
        <TD>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDeformity);">
        </TD>
        <TD class=titleImg>
          意外/疾病残疾明细
        </TD>

      </TR>
    </Table>

    <Div id = "divDeformity" style = "display: ''">
      <Table class= common>
        <TR class= common>
          <TD text-align: left colSpan=1>
            <span id="spanDeformityGrid" >
            </span>
          </TD>
        </TR>
      </Table>

    </Div>
  </Div>

         <INPUT VALUE="保存" class=common TYPE=button onclick="submitForm();">
         <INPUT VALUE="返回" class=common TYPE=button onclick="top.close();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
try
{
  fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
  fm.CustomerNo.value = '<%= request.getParameter("CustomerNo") %>';
  fm.CustomerName.value = '<%= StrTool.unicodeToGBK(request.getParameter("CustomerName")) %>';
  fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';
  fm.DisplayFlag.value = '<%= request.getParameter("DisplayFlag")%>';
}
catch(ex)
{
  alert(ex);
}

</script>

</html>
