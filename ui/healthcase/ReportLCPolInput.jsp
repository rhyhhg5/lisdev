<html>
<%
//Name：ReportLCPolInput.jsp
//Function：客户保单明细的查询界面
//Author  ：LiuYansong
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%

%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>

  <SCRIPT src="ReportLCPolInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ReportLCPolInit.jsp"%>

</head>
<body  onload="initForm();" >
<form name=fm>
        <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divReportLCPol);">
    		</td>
    		<td class= titleImg>
    			 出险人保单信息明细
    		</td>
    		<TD>
            <Input type=hidden name=InsuredNo >
         </TD>
    	</tr>
    </table>
	<Div  id= "divReportLCPol" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanReportLCPolGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>
      <INPUT VALUE="首  页" TYPE=button class=common onclick="getFirstPage();">
      <INPUT VALUE="上一页" TYPE=button class=common onclick="getPreviousPage();">
      <INPUT VALUE="下一页" TYPE=button class=common onclick="getNextPage();">
      <INPUT VALUE="尾  页" TYPE=button class=common onclick="getLastPage();">

      <INPUT VALUE="返  回" TYPE=button class=common onclick="top.close();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
	fm.InsuredNo.value='<%= request.getParameter("InsuredNo") %>';
</script>
</html>
