<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
  * Name    ：RegisterQueryDetail.jsp
  * Function：立案－查询－返回
  * Author  ：LiuYansong
 */
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llhealthcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String CaseState = "";
  //保单信息部分
  LLRegisterSchema tLLRegisterSchema   = new LLRegisterSchema();
  tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLLRegisterSchema);
	tVData.addElement(tG);
  // 数据传输
  RegisterUI tRegisterUI   = new RegisterUI();
	if (!tRegisterUI.submitData(tVData,"ReturnData"))
	{
      Content = " 查询失败，原因是: " + tRegisterUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tRegisterUI.getResult();
		System.out.println(tVData.size());
		String a_RgtName = "";
    String a_HandlerName = "";
    CaseState = (String)tVData.get(0);
		a_RgtName= (String)tVData.get(1);
		a_HandlerName=(String)tVData.get(2);
		// 显示
		// 保单信息
		LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
		LLRegisterSet mLLRegisterSet=new LLRegisterSet();
		mLLRegisterSet=(LLRegisterSet)tVData.getObjectByObjectName("LLRegisterSet",0);
		mLLRegisterSchema=mLLRegisterSet.get(1);
		//对转意字符进行处理
		String Path = application.getRealPath("config//Conversion.config");
		mLLRegisterSchema.setAccidentReason(StrTool.Conversion(mLLRegisterSchema.getAccidentReason(),Path));
    mLLRegisterSchema.setAccidentCourse(StrTool.Conversion(mLLRegisterSchema.getAccidentCourse(),Path));
		mLLRegisterSchema.setRemark(StrTool.Conversion(mLLRegisterSchema.getRemark(),Path));
		mLLRegisterSchema.setAccidentSite(StrTool.Conversion(mLLRegisterSchema.getAccidentSite(),Path));
    mLLRegisterSchema.setRgtReason(StrTool.Conversion(mLLRegisterSchema.getRgtReason(),Path));
		%>

    	<script language="javascript">
       var tAccidentReason = Conversion("<%=mLLRegisterSchema.getAccidentReason()%>");
    	 var tAccidentCourse = Conversion("<%=mLLRegisterSchema.getAccidentCourse()%>");
    	 var tRemark = Conversion("<%=mLLRegisterSchema.getRemark()%>");
			 var tAccidentSite = Conversion("<%=mLLRegisterSchema.getAccidentSite()%>");
       var tRgtReason = Conversion("<%=mLLRegisterSchema.getRgtReason()%>");
       top.opener.fm.all("RgtNo").value = "<%=mLLRegisterSchema.getRgtNo()%>";
       top.opener.fm.all("RptNo").value = "<%=mLLRegisterSchema.getRptNo()%>";
       top.opener.fm.all("RgtantName").value = "<%=mLLRegisterSchema.getRgtantName()%>";
       top.opener.fm.all("Sex").value = "<%=mLLRegisterSchema.getRgtantSex()%>";
       top.opener.fm.all("Relation").value = "<%=mLLRegisterSchema.getRelation()%>";
       top.opener.fm.all("RgtantAddress").value = "<%=mLLRegisterSchema.getRgtantAddress()%>";
       top.opener.fm.all("RgtantPhone").value = "<%=mLLRegisterSchema.getRgtantPhone()%>";
       top.opener.fm.all("RgtantMobile").value = "<%=mLLRegisterSchema.getBankAccNo()%>";
       top.opener.fm.all("RgtDate").value = "<%=mLLRegisterSchema.getRgtDate()%>";
       top.opener.fm.all("AccidentDate").value = "<%=mLLRegisterSchema.getAccidentDate()%>";
       top.opener.fm.all("AccidentReason").value =tAccidentReason;
       top.opener.fm.all("AccidentSite").value = tAccidentSite;
       top.opener.fm.all("AccidentCourse").value = tAccidentCourse;
       top.opener.fm.all("Remark").value = tRemark;
       top.opener.fm.all("RgtReason").value = tRgtReason;
       top.opener.fm.all("AgentCode").value = "<%=mLLRegisterSchema.getAgentCode()%>";
       top.opener.fm.all("AgentGroup").value = "<%=mLLRegisterSchema.getAgentGroup()%>";
       top.opener.fm.all("Handler").value = "<%=a_HandlerName%>";
       top.opener.fm.all("MngCom").value = "<%=mLLRegisterSchema.getMngCom()%>";
       top.opener.fm.all("ClmState").value = "<%=mLLRegisterSchema.getClmState()%>";
			 top.opener.fm.all("BankCode").value = "<%=mLLRegisterSchema.getBankCode()%>";
			 top.opener.fm.all("BankAccNo").value = "<%=mLLRegisterSchema.getBankAccNo()%>";
			 top.opener.fm.all("AccName").value = "<%=mLLRegisterSchema.getAccName()%>";
       top.opener.fm.all("IDNo").value = "<%=mLLRegisterSchema.getIDNo()%>";
       top.opener.fm.all("IDType").value = "<%=mLLRegisterSchema.getIDType()%>";
       top.opener.fm.all("ApplyType").value = "<%=mLLRegisterSchema.getApplyType()%>";
       top.opener.fm.all("Handler1").value = "<%=mLLRegisterSchema.getHandler1()%>";
       top.opener.fm.all("Handler1Phone").value = "<%=mLLRegisterSchema.getHandler1Phone()%>";
       top.opener.fm.all("CaseGetMode").value =  "<%=mLLRegisterSchema.getCaseGetMode()%>";
       top.opener.fm.all("CaseState").value =  "<%=CaseState%>";
       top.opener.fm.all("Operator").value="<%=a_RgtName%>";

       top.opener.emptyUndefined();
       </script>
		<%
  }
      // end of if
      LLCaseSchema tLLCaseSchema   = new LLCaseSchema();
      tLLCaseSchema.setRgtNo(request.getParameter("RgtNo"));
      tVData.removeAllElements();
      tVData.addElement(tLLCaseSchema);
      // 数据传输
      CaseUI tCaseUI   = new CaseUI();
      if (!tCaseUI.submitData(tVData,"QUERY||MAIN"))
      {
        Content = " 查询失败，原因是: " + tCaseUI.mErrors.getError(0).errorMessage;
        FlagStr = "Fail";
      }
      else
      {
				tVData.clear();
				tVData = tCaseUI.getResult();
				System.out.println(tVData.size());
				LLCaseSet mLLCaseSet=new LLCaseSet();
				mLLCaseSet.set((LLCaseSet)tVData.getObjectByObjectName("LLCaseBLSet",0));
				int m = mLLCaseSet.size();
				%>

					<script language="javascript">
			   	  top.opener.CaseGrid.clearData();
            top.opener.fm.all("PeopleName").value =  "<%=mLLCaseSet.get(1).getCustomerName()%>";
            top.opener.fm.all("CustomerNo").value =  "<%=mLLCaseSet.get(1).getCustomerNo()%>";
					</script>
				<%
				for(int i=1;i<=m;i++)
				{
			  	LLCaseSchema mLLCaseSchema = mLLCaseSet.get(i);
				%>
    			<script language="javascript">
       			top.opener.CaseGrid.addOne("CaseGrid")
    	 			top.opener.CaseGrid.setRowColData(<%=i-1%>,1,"<%=mLLCaseSchema.getCaseNo()%>");
    	 			top.opener.CaseGrid.setRowColData(<%=i-1%>,2,"<%=mLLCaseSchema.getCustomerNo()%>");
    	 			top.opener.CaseGrid.setRowColData(<%=i-1%>,3,"<%=mLLCaseSchema.getCustomerName()%>");
    	 			top.opener.CaseGrid.setRowColData(<%=i-1%>,4,"<%=mLLCaseSchema.getCustomerSex()%>");
    	 			top.opener.CaseGrid.setRowColData(<%=i-1%>,5,"<%=mLLCaseSchema.getIDType()%>");
    	 			top.opener.CaseGrid.setRowColData(<%=i-1%>,6,"<%=mLLCaseSchema.getIDNo()%>");
    	 			top.opener.CaseGrid.setRowColData(<%=i-1%>,7,"<%=String.valueOf(mLLCaseSchema.getCustomerAge())%>");
						top.opener.CaseGrid.setRowColData(<%=i-1%>,8,"<%=mLLCaseSchema.getAccidentType()%>");
            top.opener.CaseGrid.setRowColData(<%=i-1%>,9,"<%=mLLCaseSchema.getSubRptNo()%>");
            top.opener.CaseGrid.checkBoxAll();
    				top.opener.emptyUndefined();
    			</script>
				<%
				}
 //end of for
			}
//end of if

  //读取附件信息
 //保单信息部分
  LLRgtAffixSchema tLLRgtAffixSchema   = new LLRgtAffixSchema();
  tLLRgtAffixSchema.setRgtNo(request.getParameter("RgtNo"));
  // 准备传输数据 VData
  tVData.removeAllElements();
	tVData.addElement(tLLRgtAffixSchema);
  // 数据传输
  RgtAffixUI tRgtAffixUI   = new RgtAffixUI();
	if (!tRgtAffixUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tRgtAffixUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tRgtAffixUI.getResult();
		System.out.println(tVData.size());
		// 显示
		LLRgtAffixSchema mLLRgtAffixSchema = new LLRgtAffixSchema();
		LLRgtAffixSet mLLRgtAffixSet=new LLRgtAffixSet();
		mLLRgtAffixSet.set((LLRgtAffixSet)tVData.getObjectByObjectName("LLRgtAffixBLSet",0));
		%>

		<script language="javascript">
      top.opener.AffixGrid.clearData();
		</script>
		<%
		for(int j=1;j<=mLLRgtAffixSet.size();j++)
		{
			mLLRgtAffixSchema=mLLRgtAffixSet.get(j);
			%>
    	<script language="javascript">
    	 	top.opener.AffixGrid.addOne("AffixGrid");
    	 	top.opener.AffixGrid.setRowColData(<%=j-1%>,1,"<%=mLLRgtAffixSchema.getAffixCode()%>");
    	 	top.opener.AffixGrid.setRowColData(<%=j-1%>,3,"<%=mLLRgtAffixSchema.getAffixNo()%>");
    		top.opener.emptyUndefined();
    	</script>
		<%
		} //end of for
	} //end of if

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tRegisterUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println("------end------");
  System.out.println(FlagStr);
  System.out.println(Content);
  out.println("<script language=javascript>");
  out.println("top.close();");
  out.println("</script>");
%>