<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：CasePolicyQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
//        guoxiang     2003-8-7        添加：给付类型字段         
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llhealthcase.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String a_HandlerName="";
  
  //保单信息部分
  LLClaimSchema tLLClaimSchema   = new LLClaimSchema();
  LLRegisterSchema tLLRegisterSchema   = new LLRegisterSchema();
  tLLClaimSchema.setRgtNo(request.getParameter("RgtNo"));
  tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	tLLCaseSchema.setRgtNo(request.getParameter("RgtNo"));
  // 准备传输数据 VData
  CaseUI tCaseUI = new CaseUI();
  System.out.println("RgtNo===="+tLLCaseSchema.getRgtNo());
  VData tVData = new VData();
  tVData.addElement(tLLCaseSchema);
  if (!tCaseUI.submitData(tVData,"QUERY||MAIN"))
  {
      Content = " 查询失败，原因是: " + tCaseUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
  }
  else
	{
		tVData.clear();
		tVData = tCaseUI.getResult();
		System.out.println("LLCaseSet.size()===="+tVData.size());
		LLCaseSet mLLCaseSet=new LLCaseSet();
		mLLCaseSet.set((LLCaseSet)tVData.getObjectByObjectName("LLCaseBLSet",0));
		int m = mLLCaseSet.size();
		System.out.println("mLLCaseSet.size===="+m);
		%>
		
		<script language="javascript">
				   	  top.opener.CaseGrid.clearData();
		</script>
		
		<%
		
		for(int i=1;i<=m;i++)
		{
			  LLCaseSchema mLLCaseSchema = mLLCaseSet.get(i);
			%>
    	<script language="javascript">
	      top.opener.CaseGrid.addOne("CaseGrid")
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,1,"<%=mLLCaseSchema.getCaseNo()%>");
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,2,"<%=mLLCaseSchema.getCustomerNo()%>");
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,3,"<%=mLLCaseSchema.getCustomerName()%>");
    	 	top.opener.CaseGrid.setRowColData(<%=i-1%>,4,"<%=mLLCaseSchema.getSubRptNo()%>");
    	 	top.opener.emptyUndefined();
    	</script>
		<%	
		}
 //end of for	
	} 
//end of if

 	tVData.clear(); 
  
  tVData.addElement(tLLClaimSchema);
  // 数据传输
  
  
  String tState="0";
	RegisterUI tRegisterUI   = new RegisterUI();
	System.out.println("dddd");
	tVData.addElement(tG);
	tVData.addElement(tLLRegisterSchema);
	System.out.println("rhtno==="+tLLRegisterSchema.getRgtNo());
	if (!tRegisterUI.submitData(tVData,"QUERY||MAIN"))
	{
      		Content = " 查询失败，原因是: " + tRegisterUI.mErrors.getError(0).errorMessage;
      		FlagStr = "Fail";
		System.out.println("it's fail");
	}
	else
	{
		System.out.println("it's success!");
		tVData.clear();
		tVData = tRegisterUI.getResult();
		a_HandlerName=(String)tVData.get(0);
		LLRegisterBLSet mLLRegisterBLSet = new LLRegisterBLSet(); 
		mLLRegisterBLSet.set((LLRegisterBLSet)tVData.getObjectByObjectName("LLRegisterBLSet",0));
		LLCasePolicyBLSet mLLCasePolicyBLSet = new LLCasePolicyBLSet(); 
		System.out.println("end");
		mLLCasePolicyBLSet.set((LLCasePolicyBLSet)tVData.getObjectByObjectName("LLCasePolicyBLSet",0));
		System.out.println("end1");
		System.out.println("aaaa==="+mLLRegisterBLSet.get(1).getRgtNo());
		tState = CaseFunPub.getCaseStateByRgtNo(mLLRegisterBLSet.get(1).getRgtNo());
		String tCheckType=CaseFunPub.getCaseCheckType(mLLRegisterBLSet.get(1).getRgtNo());
		
		System.out.println("end2");
%>
		<script language="javascript">
			top.opener.fm.all("RgtNo").value = "<%=mLLRegisterBLSet.get(1).getRgtNo()%>";
			top.opener.fm.all("ClmState").value = "<%=tState%>";
			top.opener.fm.all("ClmUWer").value = "<%=a_HandlerName%>";
			top.opener.fm.all("MngCom").value = "<%=mLLRegisterBLSet.get(1).getMngCom()%>";
			top.opener.fm.all("CheckType").value =  "<%=tCheckType%>";
			top.opener.ClaimPolGrid.clearData("ClaimPolGrid");
			top.opener.emptyUndefined();
<%
  		System.out.println("num==="+mLLCasePolicyBLSet.size());
  		for (int i = 1;i<=mLLCasePolicyBLSet.size();i++)
  		{
  			LLCasePolicySchema tLLCasePolicySchema = new LLCasePolicySchema();
			tLLCasePolicySchema.setSchema(mLLCasePolicyBLSet.get(i));
			System.out.println("PolNo==="+tLLCasePolicySchema.getPolNo());
String tSql="#getdutykind# and code in(select getdutykind from LMDutyGetClm where getdutycode"+ 
" in(select getdutycode from lcget where polno=#"+tLLCasePolicySchema.getPolNo().trim()+
"# and #"+tLLCasePolicySchema.getPolType().trim()+
"#=#1#) or getdutycode in(select getdutycode from lbget where polno=#"+
tLLCasePolicySchema.getPolNo().trim()+"# and #"+tLLCasePolicySchema.getPolType().trim()+
"#=#1#)) or (code=#001# and #"+tLLCasePolicySchema.getPolType().trim()+"#=#0#)";
			
									
			System.out.println(tSql);
%>
			top.opener.ClaimPolGrid.addOne("ClaimPolGrid");
			top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,1,"<%=tLLCasePolicySchema.getPolNo()%>");					
			top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,2,"<%=tLLCasePolicySchema.getRiskCode()%>");
  		top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,3,"<%=tLLCasePolicySchema.getCaseNo()%>");
  		top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,4,"<%=tLLCasePolicySchema.getInsuredNo()%>");
  		top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,5,"<%=tLLCasePolicySchema.getCValiDate()%>");
			top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,9,"<%=tSql%>");
			top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,10,"<%=tLLCasePolicySchema.getPolType()%>");
			top.opener.ClaimPolGrid.setRowColData(<%=i-1%>,12,"<%=tLLCasePolicySchema.getCasePolType()%>");
			top.opener.emptyUndefined();
<%
  		}
%>
		</script>
<%
		Content = " 查询成功! ";
    		FlagStr = "Succ";
	}
  
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  out.println("top.close();");
  out.println("</script>");
%>
