/******************************************************************************
 * Name    ：RegisterQuery.js
 * Function:立案－查询的处理程序
 * Author  :LiuYansong
 */
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
function submitForm()
{
  //var i = 0;
  initRegisterGrid();
  /*
  if(fm.NoType.value!=""&&fm.No.value=="")
  {
    alert("请您录入查询号码");
    return;
  }
  if(fm.No.value!=""&&fm.NoType.value=="")
  {
    alert("请您录入查询号码的号码类型");
    return;
  }
  */
  var strSQL = "select LLRegister.RptNo,LLRegister.RgtNo,LLCase.CustomerName,LLCase.CustomerNo,LLRegister.MakeDate,LLRegister.CaseGetMode "
             +" from LLRegister,LLCase ,LLReport where LLRegister.RgtNo=LLCase.RgtNo and LLRegister.RptNo = LLReport.RptNo "
             + " and LLRegister.MngCom like '"+ComCode+"%%'"+
               getWherePart( 'LLRegister.RgtNo','RgtNo' )+
               getWherePart('LLRegister.RptNo','RptNo')+
               getWherePart('LLReport.RptObj','NoType')+
               getWherePart('LLReport.RptObjNo','No')+
               getWherePart('LLCase.CustomerName','PeopleName')+
               " order by LLRegister.MakeDate,LLRegister.MakeTime";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
    alert("在该管理机构下没有满足条件的立案信息记录");
    return "";
  }

  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.pageDisplayGrid = RegisterGrid;
  turnPage.strQuerySql     = strSQL;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;

  //  fm.action = "./RegisterQueryOut.jsp";
  //  fm.submit(); //提交
}

function displayQueryResult(strResult)
{
  var filterArray          = new Array(4,0,7,10,13,42);
  turnPage.strQueryResult  = strResult;
  var tArr                 = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  turnPage.useSimulation   = 1;
  turnPage.pageDisplayGrid = RegisterGrid;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  }
}
function returnParent()
{
  tRow=RegisterGrid.getSelNo();
  if(tRow==0)
  {
    alert("请您先选择要返回的数据！");
    return;
  }
  clearData();
  tCol=2;
  tRegisterNo = RegisterGrid.getRowColData(tRow-1,tCol);
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  top.location.href="./RegisterQueryDetail.jsp?RgtNo="+tRegisterNo;
  var showBankFlag = RegisterGrid.getRowColData(tRow-1,6);
//  if(showBankFlag=="4")
//  {
//    top.opener.fm.all('divShowBankInfo').style.display="";
//  }
//  else
//  {
//    top.opener.fm.all('divShowBankInfo').style.display="none";
//  }
}

function clearData()
{
  top.opener.fm.RgtNo.value="";
   top.opener.fm.RptNo.value="";
   top.opener.fm.ClmState.value="";
   top.opener.fm.DisplayFlag.value="";
   top.opener.fm.CaseState.value="";
   top.opener.fm.CaseGetMode.value="";
   top.opener.fm.PeopleName.value="";
   top.opener.fm.CustomerNo.value="";
   top.opener.fm.Handler1.value="";
   top.opener.fm.Handler1Phone.value="";
   top.opener.fm.RgtDate.value="";
   top.opener.fm.AccidentDate.value="";
   top.opener.fm.RgtantName.value="";
   top.opener.fm.Sex.value="";
   top.opener.fm.IDType.value="";
   top.opener.fm.IDNo.value="";
   top.opener.fm.ApplyType.value="";
   top.opener.fm.Relation.value="";
   top.opener.fm.RgtantPhone.value="";
   top.opener.fm.RgtantAddress.value="";
   top.opener.fm.BankCode.value="";
   top.opener.fm.AccName.value="";
   top.opener.fm.BankAccNo.value="";
   top.opener.fm.RgtantMobile.value="";
   top.opener.fm.Operator.value="";
   top.opener.fm.Handler.value="";
   top.opener.fm.MngCom.value="";
   top.opener.fm.AgentCode.value="";
   top.opener.fm.AgentGroup.value="";
   top.opener.fm.AccidentReason.value="";
   top.opener.fm.AccidentSite.value="";
   top.opener.fm.AccidentCourse.value="";
   top.opener.fm.Remark.value="";
  top.opener.fm.RgtReason.value="";
}
