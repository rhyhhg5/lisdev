<%
/*******************************************************************************
 * Name     :CaseAccidentSave.jsp
 * Function :事故/伤残信息的保存程序
 * Authorm  :LiuYansong
 * Date     :2003-7-16
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.llhealthcase.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  String Type = request.getParameter("AccitentType");//判断是意外还是疾病；
  System.out.println("类型是2003-7-28"+Type);
  String DisplayFlag = request.getParameter("DisplayFlag");//判断是在立案中进入的还是在理算中进入的；（0立案；1理算）
  LLCaseInfoSet tLLCaseInfoSet=new LLCaseInfoSet();
  CaseInfoUI tCaseInfoUI   = new CaseInfoUI();
  LLCaseInfoSchema mLLCaseInfoSchema = new LLCaseInfoSchema();
  mLLCaseInfoSchema.setCaseNo(request.getParameter("CaseNo"));

  CErrors tError = null;
  String transact = "INSERT";
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String strCaseNo=request.getParameter("CaseNo");
  if(DisplayFlag.equals("0"))
  {
    //在立案中进入的，则只有意外和疾病两种操作
    if(Type.equals("YW"))
    {
      //对事故原因总类是意外的进行操作
      String[] strNumber3=request.getParameterValues("CaseGridNo");
      if(strNumber3==null)
      {
        tLLCaseInfoSet.clear();
      }
      else
      {
        String[] strCode3=request.getParameterValues("CaseGrid1");
        String[] strName3=request.getParameterValues("CaseGrid2");

        int intLength=strNumber3.length;
        for(int k=0;k<intLength;k++)
        {
          LLCaseInfoSchema tLLCaseInfoSchema   = new LLCaseInfoSchema();
          tLLCaseInfoSchema.setCaseNo(strCaseNo);
          tLLCaseInfoSchema.setCode(strCode3[k]);
          tLLCaseInfoSchema.setName(strName3[k]);
          tLLCaseInfoSchema.setCustomerNo(request.getParameter("CustomerNo"));
          tLLCaseInfoSchema.setCustomerName(request.getParameter("CustomerName"));
          tLLCaseInfoSchema.setAffixSerialNo(Type+String.valueOf(k));
          tLLCaseInfoSchema.setType(Type);
          tLLCaseInfoSet.add(tLLCaseInfoSchema);
        }
      }
    }
    if(Type.equals("JB"))
    {
      //对疾病进行处理
      String[] strNumber1=request.getParameterValues("DiseaseGridNo");
      if(strNumber1==null)
      {
        tLLCaseInfoSet.clear();
      }
      else
      {
        String[] strCode1=request.getParameterValues("DiseaseGrid1");
        String[] strName1=request.getParameterValues("DiseaseGrid2");

        int intLength=strNumber1.length;
        for(int i=0;i<intLength;i++)
        {
          LLCaseInfoSchema tLLCaseInfoSchema   = new LLCaseInfoSchema();
          tLLCaseInfoSchema.setCaseNo(strCaseNo);
          tLLCaseInfoSchema.setCode(strCode1[i]);
          tLLCaseInfoSchema.setName(strName1[i]);
          tLLCaseInfoSchema.setCustomerNo(request.getParameter("CustomerNo"));
          tLLCaseInfoSchema.setCustomerName(request.getParameter("CustomerName"));
          tLLCaseInfoSchema.setAffixSerialNo(Type+String.valueOf(i));
          tLLCaseInfoSchema.setType(Type);
          tLLCaseInfoSet.add(tLLCaseInfoSchema);
        }
      }
    }
  }
  if(DisplayFlag.equals("1"))
  {
    //对理算-伤残进行附值
    String[] strNumber2=request.getParameterValues("DeformityGridNo");
    //2003-08-13添加来执行删除操作的程序段
    if(strNumber2==null)
    {
      tLLCaseInfoSet.clear();
      System.out.println("08-13中的操作是Set中的值是0");
    }
    else
    {
      String[] strDeformityGrade  = request.getParameterValues("DeformityGrid1");
      String[] strGradeName       = request.getParameterValues("DeformityGrid2");
      String[] strCode2           = request.getParameterValues("DeformityGrid3");
      String[] strName2           = request.getParameterValues("DeformityGrid4");
      String[] strAppDeformityRate= request.getParameterValues("DeformityGrid5");
      String[] strDeformityRate   = request.getParameterValues("DeformityGrid6");
      String[] strDiagnosis       = request.getParameterValues("DeformityGrid7");
      int intLength=strNumber2.length;
      for(int j=0;j<intLength;j++)
      {
        LLCaseInfoSchema tLLCaseInfoSchema   = new LLCaseInfoSchema();
        tLLCaseInfoSchema.setCaseNo(strCaseNo);
        tLLCaseInfoSchema.setCode(strCode2[j]);
        tLLCaseInfoSchema.setName(strName2[j]);
        tLLCaseInfoSchema.setCustomerNo(request.getParameter("CustomerNo"));
        tLLCaseInfoSchema.setCustomerName(request.getParameter("CustomerName"));
        tLLCaseInfoSchema.setAffixSerialNo("SC"+String.valueOf(j));
        tLLCaseInfoSchema.setType(Type);//记录是YW还是JB
        tLLCaseInfoSchema.setDeformityRate(strDeformityRate[j]);
        tLLCaseInfoSchema.setAppDeformityRate(strAppDeformityRate[j]);
        tLLCaseInfoSchema.setGradeName(strGradeName[j]);
        tLLCaseInfoSchema.setDeformityGrade(strDeformityGrade[j]);
        tLLCaseInfoSchema.setDiagnosis(strDiagnosis[j]);//备注
        tLLCaseInfoSet.add(tLLCaseInfoSchema);
      }
    }
  }
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  try
  {
    VData tVData = new VData();
    tVData.addElement(DisplayFlag);//记录是从立案进入还是从理算进入
    tVData.addElement(Type);//将是JB还上YW的信息向后台传递
    tVData.addElement(tG);
    tVData.addElement(tLLCaseInfoSet);
    tVData.addElement(mLLCaseInfoSchema);

    tCaseInfoUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCaseInfoUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>