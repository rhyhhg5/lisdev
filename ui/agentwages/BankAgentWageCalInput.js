   //               该文件中包含客户端需要处理的函数和事件

var showInfo;

var mDebug="0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}

//提数操作
function submitForm()
{
	if (fm.all('ManageCom').value==null||fm.all('ManageCom').value=='')
	{
		alert("管理机构不能为空!");
		return false;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
//  showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
  	
	  initForm();
	  
  }
  catch(re)
  {
  	alert("在AgentWageBankInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
 	
}

//执行提数         
function AgentWageBankSave()
{
  fm.target="fraSubmit";
  //首先检验录入框
//  if(!verifyInput()) return false;
  
	if (fm.all('ManageCom').value=='')
	{
		alert("管理机构不能为空!");
		return false;
	}
	if (fm.all('WageYear').value=='')
	{
		alert("薪资所属年不能为空!");
		return false;
	}
	if (fm.all('WageMonth').value=='')
	{
		alert("薪资所属月不能为空!");
		return false;
	} 
  //校验是否该月佣金已算过
  var tWageNo = fm.WageYear.value + fm.WageMonth.value;
  var strSql = "select AgentCode from LAWage " 
               +"where IndexCalNo = '"+tWageNo+"'"
               +getWherePart('ManageCom','ManageCom','like')
               +getWherePart('BranchType')
               +getWherePart('BranchType2');

  //查询SQL，返回结果字符串
  var strQueryResult = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (strQueryResult) 
  {  
    alert("该月佣金已计算过，无法再计算！");
     return false;
  }
  var strSql = "select min(indexcalno) from lawage "
                  +"where 1=1 and state = '0' and indexcalno >='201110'"
               +getWherePart('ManageCom','ManageCom','like')
               +getWherePart('BranchType')
               +getWherePart('BranchType2');

  var strQueryResult = easyQueryVer3(strSql, 1, 1, 1); 
   	 var arr = decodeEasyQueryResult(strQueryResult);
   	 var temp = arr[0][0];
   	 if(temp!=''&&temp!=null){
   	 if(temp<tWageNo)
   	 {
     alert("上个月薪资计算未确认,本月不能进行薪资计算");
     return false;
     }
  }
   	 
  
  var tsql = "select a.modifydate ,current date from  lacontfycrate a,lacommision b "
           +" where a.grpcontno=b.grpcontno "
           +" and a.riskcode = b.riskcode  and a.Flag='N' and b.wageno='"+tWageNo
           +"' and  a.modifydate= current date " 
           +getWherePart('b.ManageCom','ManageCom','like')           
           +getWherePart('b.BranchType','BranchType')
           +getWherePart('b.BranchType2','BranchType2');
           
  strQueryResult = easyQueryVer3(tsql, 1, 1, 1);   
  if(strQueryResult)
  { 	
    alert("非标准业务提奖比例为今天录入,明天才能够计算薪资!");
    return false; 
  }
  
    
     
  //divAgentQuery.style.display='none'; 
   //if (fm.all('BranchType').value=='3')
 	fm.action="./BankAgentWageCalSave.jsp"
   //else if (fm.all('BranchType').value=='2')
 	//fm.action="./AgentWageCorpSave.jsp";
   //else
   //	{
    //      alert("输入的展业机构类型有误，请重新输入！！！");
    //      return;	
   	//}
  submitForm();	
}  
                  
//执行打印
function AgentWageBankPrt()
{
  fm.target="f1Print";
//首先检验录入框
  if(!verifyInput()) return false;
//   window.open("./AgentWageBankQuery.jsp?WageYear="+WageYear+);
  if(fm.all('WageYear').value==''||fm.all('WageMonth').value==''||fm.all('BranchType').value=='')
   {
     alert("请填写打印条件");
     return ;	
   }
  if (fm.all('ManageCom').value==null||fm.all('ManageCom').value=='')
   {
	alert("管理机构不能为空!");
	return false;
   }
  if (fm.all('BranchType').value=='3')
 	fm.action="./AgentWBF1PSave.jsp";
  else if (fm.all('BranchType').value=='2')
 	fm.action="./AgentWCF1PSave.jsp";  
  fm.submit();	
}


/////////////////////////////////////////////////////////////////////////////////
////执行查询
function AgentWageBankQuery()
{

  fm.target="fraSubmit";
	    //首先检验录入框
//  if(!verifyInput()) return false; 
 
 if(fm.all('BranchType').value==null ||fm.all('BranchType').value=='')
  {
    alert("请填写查询条件");
    return ;	
  }
 divAgentQuery.style.display='';
 initAgentQueryGrid();
  if (fm.all('BranchType').value=='3')
 	showBankRecord();

}

/////////////////////////////////////////////////////////////
//显示数据的函数，和easyQuery及MulLine 一起使用
/////////////记得更改查询的列
function showBankRecord()
{
	if (fm.all('ManageCom1').value=='')
	{
		alert("管理机构不能为空!");
		return false;
	}
	if (fm.all('AgentType').value=='')
	{
		alert("销售人员类型不能为空!");
		return false;
	}  
	if (fm.all('WageYear1').value=='')
	{
		alert("薪资所属年不能为空!");
		return false;
	}
	if (fm.all('WageMonth1').value=='')
	{
		alert("薪资所属月不能为空!");
		return false;
	}  
  // 拼SQL语句，从页面采集信息
  var tReturn = getManageComLimitlike("a.ManageCom");
  var Year = fm.all('WageYear1').value;
  var Month = fm.all('WageMonth1').value;
  var AgentCode = fm.all('AgentCode').value;
  var AgentName = fm.all('AgentName').value;
  var AgentType = fm.all('AgentType').value;
  var tbranchtype = fm.BranchType.value;        
  var tbranchtype2 = fm.BranchType2.value; 
  
  var strSql = "select * from (select getUniteCode(a.AgentCode) ord,c.Name,a.managecom,b.branchattr,b.name,"
              +"a.agentgrade,(select gradename from laagentgrade where gradecode=a.agentgrade),"
              +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
              +" c.outworkdate" 
  	         +",(select cond1 from LAWageRadix2 where branchtype =a.branchtype and branchtype2=a.branchtype2 and wagecode ='YD001' and agentgrade=d.agentgrade and areatype = (select wageflag from LABankIndexRadix " +
  	          " where branchtype ='3' and branchtype2='01' and managecom=trim(substr(a.managecom,1,4))))"
	         +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
              +",a.F07,a.F11,a.F08,a.F20,a.F21,a.F13,a.F09,a.F30,a.K12,a.F16,a.F17,a.F14,a.F15,a.F18,a.F19,a.F06,a.CurrMoney,a.operator"
              +" from LAWage a,labranchgroup b,laagent c ,latree d"
              //+",labasewage e " 
              +" where a.agentcode = c.agentcode and b.agentgroup = a.agentgroup "
              +" and c.agentgroup=b.agentgroup "
              +" and c.agentcode=d.agentcode  and a.branchtype='3' "
              +" and a.state = '0'"              
              //+" and d.agentgrade=e.agentgrade"
              //+" and a.managecom=e.managecom and e.type='01' and e.branchtype='3'"
              +" and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null)"
              +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
              +" and c.agenttype = '"+AgentType+"'"
              +" and a.branchtype = '" + trim(tbranchtype)
			  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
              +getWherePart('c.groupagentcode','AgentCode')
              +getWherePart('c.name','AgentName')
               //+tReturn
              //+" order by a.AgentCode "
//              +"union"
//              +" select getUniteCode(a.AgentCode) ord,c.Name,a.managecom,b.branchattr,b.name,"
//              +"a.agentgrade,(select gradename from laagentgrade where gradecode=a.agentgrade),"
//              +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
//              +" c.outworkdate" 
//  	         +",(select wagemoney from labankstandradix where wagetype = (select wageflag from LABankIndexRadix where managecom = a.managecom ) and agentgrade = d.agentgrade and agenttype = c.agenttype)"
//	         +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
//              +",a.F07,a.F11,a.F08,a.F13,a.F09,a.F30,a.K12,a.F16,a.F17,a.F14,'a.F15',a.F18,a.F19,a.CurrMoney,a.operator"
//              +" from LAWage a,labranchgroup b,laagent c ,latree d "
//              +"where a.agentcode = c.agentcode and b.agentgroup = (select agentgroup from labranchchangetemp where agentcode =a.agentcode and cvalimonth <='"+Year+Month+"'  and cvaliflag='1' order by cvalimonth  desc  fetch first 1 rows only )"
//              +"and a.agentcode=d.agentcode and a.agentcode = c.agentcode and  a.agentgroup = b.agentgroup "
//              +"and a.branchtype='3'  and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"'"
//              +"and (b.state<>'1' or b.state is null)"
//              +" and a.state = '0'"              
//              +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
//              +" and c.agenttype = '"+AgentType+"'"
//              +" and a.branchtype = '" + trim(tbranchtype)
//			  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
//              +getWherePart('c.groupagentcode','AgentCode')
//              +getWherePart('c.name','AgentName')
//              +" select '合计','','','','','','','','','','',"
//              +"value(sum(a.F07),0),value(sum(a.F11),0),value(sum(a.F08),0),value(sum(a.F013),0),value(sum(a.F09),0),"
//              +"value(sum(a.F30),0),value(sum(a.K12),0),value(sum(a.F16),0),value(sum(a.F17),0),value(sum(a.F14),0),"
//              +"value(sum(a.F15),0),value(sum(a.F18),0),value(sum(a.F19),0),value(sum(a.CurrMoney),0),''"
//              +" from LAWage a,labranchgroup b,laagent c ,latree d"
//              +" where a.agentcode = c.agentcode and b.agentgroup = a.agentgroup "
//              +" and c.agentgroup=b.agentgroup "
//              +" and c.agentcode=d.agentcode  and a.branchtype='3' "
//              +" and a.state = '0'"              
//              +" and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null)"
//              +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
//              +" and c.agenttype = '"+AgentType+"'"
//              +" and a.branchtype = '" + trim(tbranchtype)
//			  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
//              +getWherePart('c.groupagentcode','AgentCode')
//              +getWherePart('c.name','AgentName')
              +") kk order by ord with ur";

  //alert(strSql);	    
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("没有符合条件的数据！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}

/////////////////////////////////////////////////////////////////////
//显示数据的函数，和easyQuery及MulLine 一起使用
//暂时没用
function showCorpRecord()
{
	
  // 拼SQL语句，从页面采集信息
  var tReturn = getManageComLimitlike("a.ManageCom");

  var Year = fm.all('WageYear').value;
  var Month = fm.all('WageMonth').value;	    
	//查询SQL，返回结果字符串
	var tbranchtype = fm.BranchType.value;        
  var tbranchtype2 = fm.BranchType2.value;      

	var strSql2 = "select a.AgentCode,c.Name,b.branchattr,a.ManageCom,a.f22,w02,a.k20,a.f12,a.W10,a.f07,a.f09,a.f23,a.f20,f13,a.f24,a.w08,a.ShouldMoney,a.LastMoney,a.CurrMoney,a.SumMoney,a.K12 from LAWage a,labranchgroup b,laagent c " 
               +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  and IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null)"
               +getWherePart('a.ManageCom','ManageCom','like')
               +" and a.branchtype = '" + trim(tbranchtype)
				       + "'  and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
               +" order by a.AgentCode";	
	
  turnPage.strQueryResult  = easyQueryVer3(strSql2, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql2; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}

function WageDownLoad()
{
  var tManageCom  = fm.all('ManageCom').value;
  
 if(AgentQueryGrid.mulLineCount==0)
 {
    alert("列表中没有数据可下载");
    return;
 }
 else {
 // 书写SQL语句
   // 拼SQL语句，从页面采集信息
   
   var tReturn = getManageComLimitlike("a.ManageCom");
   var Year = fm.all('WageYear1').value;
   var Month = fm.all('WageMonth1').value;
   var AgentCode = fm.all('AgentCode').value;
   var AgentName = fm.all('AgentName').value;
   var AgentType = fm.all('AgentType').value;
   var tbranchtype = fm.BranchType.value;        
   var tbranchtype2 = fm.BranchType2.value; 
  /*
  var checkSql = "select state from lawage where branchtype = '3' and branchtype2 = '01' and managecom like '"+ManageCom+"%' and indexcalno ='"+Year+Month+"' ";
  var strQueryResult  = easyQueryVer3(checkSql, 1, 1, 1);
  var arr = decodeEasyQueryResult(strQueryResult);
  if(arr[0][0]!="1"){
     alert("本月薪资计算未确认,不能下载当月银代薪资报表!");
     return false;
  }
  */
  var strSql1 = "select * from (select getUniteCode(a.AgentCode) ord,c.Name,a.managecom,b.branchattr,b.name,"
              +"a.agentgrade,(select gradename from laagentgrade where gradecode=a.agentgrade),"
              +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
              +" c.outworkdate" 
              +",(select cond1 from LAWageRadix2 where branchtype =a.branchtype and branchtype2=a.branchtype2 and wagecode ='YD001' and agentgrade=d.agentgrade and areatype = (select wageflag from LABankIndexRadix " +
  	          " where branchtype ='3' and branchtype2='01' and managecom=trim(substr(a.managecom,1,4))))"
	         +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
              +",a.F07,a.F09,";
              if(AgentType=='1'){
            	  strSql1 += "a.F11,a.F08,a.F13," ;
              }
              if(AgentType=='2')
              {
            	  strSql1 += "a.F20,a.F21," ;
              }
              strSql1 += "a.F30,a.K12,a.F16,a.F17,a.F14,a.F15,a.F18,a.F19,a.F06,a.CurrMoney,a.operator"
              +" from LAWage a,labranchgroup b,laagent c ,latree d "
              //+",labasewage e " 
              +" where a.agentcode = c.agentcode and b.agentgroup = a.agentgroup "
              +" and c.agentgroup=b.agentgroup "
              +" and c.agentcode=d.agentcode "
              +" and a.state = '0'"
              //+" and d.agentgrade=e.agentgrade"
              //+" and a.managecom=e.managecom and e.type='01' and e.branchtype='3'"
              +" and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null)"
              +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
              +" and c.agenttype = '"+AgentType+"'"
              +" and a.branchtype = '" + trim(tbranchtype)
			  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
              +getWherePart('c.groupagentcode','AgentCode')
              +getWherePart('c.name','AgentName')
              +"union "
//              +" select getUniteCode(a.AgentCode) ord,c.Name,a.managecom,b.branchattr,b.name,"
//              +"a.agentgrade,(select gradename from laagentgrade where gradecode=a.agentgrade),"
//              +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
//              +" c.outworkdate" 
// 	         +",(select wagemoney from labankstandradix where wagetype = (select wageflag from LABankIndexRadix where managecom = a.managecom ) and agentgrade = d.agentgrade and agenttype = c.agenttype)"
//	         +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
//              +",a.F07,a.F11,a.F08,a.F13," ;
//              if(AgentType=='1'){
//            	  strSql1 += "a.F09," ;
//              }
//              strSql1 += "a.F30,a.K12,a.F16,a.F17,a.F18,a.F19,a.CurrMoney,a.operator"
//              +" from LAWage a,labranchgroup b,laagent c ,latree d "
//              +"where a.agentcode = c.agentcode and b.agentgroup = (select agentgroup from labranchchangetemp where agentcode =a.agentcode and cvalimonth <='"+Year+Month+"'  and cvaliflag='1' order by cvalimonth  desc  fetch first 1 rows only )"
//              +"and a.agentcode=d.agentcode and a.agentcode = c.agentcode and  a.agentgroup = b.agentgroup "
//              +"and a.branchtype='3'  and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"'"
//              +"and (b.state<>'1' or b.state is null) "
//              +" and a.state = '0'"
//              +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
//              +" and c.agenttype = '"+AgentType+"'"
//              +" and a.branchtype = '" + trim(tbranchtype)
//			  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
//              +getWherePart('c.groupagentcode','AgentCode')
//              +getWherePart('c.name','AgentName')
              +" select '合计','','','','','','','',NULL,0,NULL,"
              +"value(sum(a.F07),0),value(sum(a.F09),0),"
              if(AgentType=='1')
              {
            	  strSql1 += " value(sum(a.F11),0),value(sum(a.F08),0),value(sum(a.F13),0), " 
              }
              if(AgentType=='2')
              {
            	  strSql1 += " value(sum(a.F20),0),value(sum(a.F21),0),"
              }
             
              strSql1+= "value(sum(a.F30),0),value(sum(a.K12),0),value(sum(a.F16),0),value(sum(a.F17),0),value(sum(a.F14),0),"
              +"value(sum(a.F15),0),value(sum(a.F18),0),value(sum(a.F19),0),value(sum(a.F06),0),value(sum(a.CurrMoney),0),'' "
              +" from LAWage a,labranchgroup b,laagent c ,latree d"
              +" where a.agentcode = c.agentcode and b.agentgroup = a.agentgroup "
              +" and c.agentgroup=b.agentgroup "
              +" and c.agentcode=d.agentcode  and a.branchtype='3' "
              +" and a.state = '0'"              
              +" and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null)"
              +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
              +" and c.agenttype = '"+AgentType+"'"
              +" and a.branchtype = '" + trim(tbranchtype)
			  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
              +getWherePart('c.groupagentcode','AgentCode')
              +getWherePart('c.name','AgentName')
              +") kk order by ord  with ur";

    fm.querySql.value = strSql1;
    var oldAction = fm.action;
    fm.action = "BankAgentWageXLS.jsp";
    fm.submit();
    fm.action = oldAction;
  }
}	