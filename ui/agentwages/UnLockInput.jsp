<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="UnLockInput.js"></SCRIPT>
  <%@include file="UnLockInit.jsp"%>
  <title>解锁 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./UnLockSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class=codeNo readonly=true name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true  elementtype=nacessary>
          </TD>
            <TD  class= title>
           薪资月
          </TD>
          <TD  class= input>
            <Input class= common  name=Wageno  elementtype=nacessary>
          </TD>
        </TR>          
      	<TR  class= common>   
          <TD  class= title>
            业务员代码
          </TD>
          <TD  class= input>
            <Input class= common  name=Groupagentcode >
          </TD>    		
          <TD  class= title>
            锁定状态
          </TD>
          <TD  class= input>
          	<Input NAME=CanSendBank VALUE=""  CLASS=codeNo CodeData="0|^0|未锁定^1|已锁定" MAXLENGTH=20  ondblclick="return showCodeListEx('CanSendBank',[this,CanSendBankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CanSendBank',[this,CanSendBankName],[0,1],null,null,null,1);"><input class=codename name=CanSendBankName readonly=true> 
          </TD>  
        </TR>
        
    </table>
          <INPUT VALUE="查 询" class= CssButton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="重 置" class= CssButton TYPE=button onclick="resetFm();"> 
          <input type=hidden name="strSQL">  
		  <INPUT VALUE="清单下载" class=cssButton TYPE=button onclick="submitForm()">
	 <p> <font color="#ff0000">注：此功能只用于状态为1锁定状态解锁 。</font></p> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 尚未付费成功的信息
    		</td>
    	</tr>
    </table>  
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: '' ">
      <INPUT CLASS=CssButton VALUE="首  页" class= CssButton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=CssButton VALUE="上一页" class= CssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=CssButton VALUE="下一页" class= CssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=CssButton VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage.lastPage();">
      </Div> 	
  	</div>
  	<p>        
    <INPUT VALUE="选择解锁" class= CssButton TYPE=button onclick="unlock()">   		
    <INPUT VALUE="全部解锁" class= CssButton TYPE=button onclick="unlockALL()"> 
  	</p>   
  	<INPUT TYPE="hidden" NAME="workType">  	
  	<INPUT VALUE="" TYPE=hidden name=gnNo>
    <INPUT VALUE="" TYPE=hidden name=pNo>  	
    
    <INPUT VALUE="" TYPE="hidden" name='PrtNo'> 
    <INPUT VALUE="" TYPE="hidden" name='CustAgent'>
    <INPUT VALUE="" TYPE="hidden" name='CustCom'>
    <INPUT VALUE="" TYPE="hidden" name='PayMode2'>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
