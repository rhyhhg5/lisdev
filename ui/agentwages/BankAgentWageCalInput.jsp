<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
 <html>  
<%
//程序名称：BankAgentWageCalInput.jsp
//程序功能：银代薪资计算
//创建日期：2008-03-01
//创建人  ：xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<script>
   var msql="1 and char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000# ";
</script>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="BankAgentWageCalInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BankAgentWageCalInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >    
  <form action="./BankAgentWageCalSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		薪资计算
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|notnull&code:comcode" 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);"
             onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);"
            ><Input name=ManageComName class="codename" elementtype=nacessary>
          </TD> 
           <TD  class= title>
            薪资所属年
          </TD>
          <TD  class= input>
            <Input class=common  name=WageYear verify="薪资所属年|NOTNULL"  elementtype=nacessary>
          </TD> 
          <!--TD  class= title>
            展业类型
          </TD>
          <TD  class= input>
            <Input class="code" name=BranchType verify="展业类型|NOTNULL" ondblclick="showCodeList('branchtype',[this],null,null,mcodeSql,'1',null);" onkeyup="return showCodeListKey('branchtype',[this],null,null,mcodeSql,'1',null);" >
          </TD-->        
        </TR>
        <TR  class= common>
         
          <TD  class= title>
            薪资所属月
          </TD>
          <TD  class= input>
            <Input class=common name=WageMonth verify="薪资所属月|NOTNULL"  elementtype=nacessary>
          </TD>
        </TR>
      </table>
          
          <input type =button class=cssButton value="计  算" onclick="AgentWageBankSave();"> 
 </Div>         
<!--          <input type =button class=cssButton value="查  询" onclick="AgentWageBankQuery();">-->
<!--          -->
<!--          <input type =button class=cssbutton value="下  载" onclick="AgentWageBankDownLoad();"> -->
<!--           (<font color='red'>注意：请先查询后下载！</font>)-->
         <!--TD class=common>
          <input type =button class=cssButton value="打印" onclick="AgentWageBankPrt();">    
        </TD-->
     <hr>  
     
           
     <Div  id= "divAgent2" style= "display: ''">
      <table  class= common>
        <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom1 verify="管理机构|notnull&code:comcode" 
             ondblclick="return showCodeList('ComCode',[this,ManageComName1],[0,1],null,msql,1,1);"
             onkeyup="return showCodeListKey('ComCode',[this,ManageComName1],[0,1],null,msql,1,1);"
            ><Input name=ManageComName1 class="codename"   elementtype=nacessary>
          </TD> 
           <TD  class= title>
            薪资所属年
          </TD>
          <TD  class= input>
            <Input class=common  name=WageYear1 verify="薪资所属年|NOTNULL"   elementtype=nacessary>
          </TD>       
        </TR>
        <TR  class= common>
          <TD  class= title>
            薪资所属月
          </TD>
          <TD  class= input>
            <Input class=common name=WageMonth1 verify="薪资所属月|NOTNULL"  elementtype=nacessary >
          </TD>
          <TD  class= title>
          业务员代码
          </TD>
          <TD  class= input>
            <Input class=common name=AgentCode>
          </TD>
        </TR>
        <TR>
         <TD  class= title>
         业务员名称
          </TD>
          <TD  class= input>
            <Input class=common name=AgentName>
          </TD>
         <TD class=title>销售人员类型</TD>
		<TD class=input><Input name=AgentType class='codeno'
			verify="销售人员类型|NOTNULL"
			ondblclick="return showCodeList('agenttypecode',[this,AgentTypeName],[0,1]);"
			onkeyup="return showCodeListKey('agenttypecode',[this,AgentTypeName],[0,1]);"><Input
			class=codename name=AgentTypeName readOnly elementtype=nacessary>
			</TD>
        </TR>
         </table>
        <table>
        <TR class=input>     
         <TD class=common>
          <input type =button class=cssButton value="查询" onclick="AgentWageBankQuery();">    
        </TD>
         <TD class=common>   
           <input type =button class=cssbutton value="下载" onclick="WageDownLoad();" />  
        </TD>
       </TR>          
     </table>
    </Div>
    <!--input type=hidden name=BranchType value=""-->
    <Div  id= "divAgentQuery" style= "display: 'none'">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span> 
  			</TD>
      </TR>
     </Table>	
     
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	     	<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
   </Div>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
    <input type=hidden id="fmAction" name="fmAction">  
    <input type=hidden class=Common name=querySql > 
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>     
  </form>   
</body>
</html>


<script>
  var mcodeSql = "1 and (code = #2# or code = #3#)";
</script>