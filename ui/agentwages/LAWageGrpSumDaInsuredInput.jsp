<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：AgentWageGatherInput.jsp
		//程序功能：
		//创建日期：2002-08-16 15:39:06
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<%
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
		String BranchType = request.getParameter("BranchType");
		String BranchType2 = request.getParameter("BranchType2");
		System.out.println("BranchType:" + BranchType);
		System.out.println("BranchType2:" + BranchType2);
		String msql = " 1 and branchtype='" + BranchType
				+ "' and branchtype2='" + BranchType2 + "'";
	%>

	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="LAWageGrpSumDaInsuredInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LAWageGrpSumDaInsuredInit.jsp"%>
		<%@include file="../common/jsp/ManageComLimit.jsp"%>
		<%@include file="../agent/SetBranchType.jsp"%>
		
		
<script>
   var msql1="1 and char(length(trim(comcode))) in (#4#,#2#)";
</script>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./LAWageGrpDaInsuredSave.jsp" method=post name=fm
			target="fraSubmit">
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divAgent1);">
					</td>
					<td class=titleImg>
						管理机构业务员月终薪资查询
					</td>
				</tr>
			</table>
			<Div id="divAgent1" style="display: ''">
				<table class=common>
					<TR class=common>
						<TD class=title>
							管理机构
						</TD>
						<TD class=input>
							<Input class="codeno" name=ManageCom
								ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,msql1,1);"
								onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,msql1,1);"><Input class=codename name=ManageComName
								readOnlyelementtype=nacessary>
						</TD>
						<td class=title>
							统计级别
						</td>
						<td class=input>
							<Input class=codeno name=Flag verify="统计级别|NOTNULL"
								CodeData="0|^0|机构级别^1|团队级别^2|人员级别"
								ondblClick="showCodeListEx('SXFlag1',[this,SXFlagName],[0,1]);"
								onkeyup="showCodeListKeyEx('SXFlag1',[this,SXFlagName],[0,1]);"><input class=codename name=SXFlagName readonly=true
								elementtype="nacessary">
						</td>
					</TR>
					<TR class=common>
						<TD class=title>
							起始薪资年月
						</td>
						<td class=input>
							<Input class='common' name=StartDate verify="起始薪资年月|NOTNULL&len=6"
								elementtype=nacessary><font color="red">(yyyymm) 
						</td>

						<TD class=title>
							终止薪资年月
						</td>
						<td class=input>
							<Input class='common' name=EndDate verify="终止薪资年月|NOTNULL&len=6"
								elementtype=nacessary><font color="red">(yyyymm) 
						</td>
					</TR>
					<TR class=common>
						<TD class=title>
							团队编码
						</TD>
						<TD class=input>
							<Input class=common name=AgentGroup>
						</TD>

						<TD class=title>
							业务员代码
						</TD>
						<TD class=input>
							<Input class=common name=GroupAgentCode onchange="return checkAgentCode()">
						</TD>
					</TR>
					 
					<TR class=input>
						<TD class=common>
							<input type=button class=cssbutton value="查询" onclick="AgentWageGatherQuery();">
							<input type=button class=cssbutton value="下载" onclick="AgentWageDownLoad();">
						</TD>
					</TR>

				</table>
			</Div>
			<font color='red'>提示：该功能只能统计薪资确认的数据</font>
			<Div id="divAgentQuery" style="display: 'none'">
				<Table class=common>
					<TR class=common>
						<TD text-align: left colSpan=1>
							<span id="spanAgentQueryGrid"></span>
						</TD>
					</TR>
				</Table>
				<INPUT CLASS=cssbutton VALUE="首页" TYPE=button
					onclick="turnPage.firstPage();">
				<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button
					onclick="turnPage.previousPage();">
				<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button
					onclick="turnPage.nextPage();">
				<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button
					onclick="turnPage.lastPage();">
			</Div>
			<input type=hidden name=BranchType value=''>
			<input type=hidden name=BranchType2 value=''>
			<input type=hidden id="fmAction" name="fmAction">
			<input type=hidden class=Common name=querySql>
			<input type=hidden class=Common name=querySqlTitle>
			<input type=hidden class=Common name=Title>
			<input type=hidden name=AgentCode value=''>

			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
