   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//修改人：解青青  时间：2014-11-24
function checkAgentCode()
{
//   var sql = "select * from laagent where agentstate<='02' and branchtype='1' and branchtype2='01'";
   var sql=" select agentcode  from laagent   where  groupagentcode='"+fm.GroupAgentCode.value+"' "
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
    
}




try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
//提数操作

function initEdorType(cObj)
{
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeList('comcode',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeListKey('comcode',[cObj], null, null, mEdorType, "1");
}


function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  
  fm.submit(); //提交
  

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

/*********************************************
  * 注: 执行薪资确认操作
  * 
  *******************************************/        
function AgentWageGatherSave()
{
 	if(!verifyInput()) { return false; }	
		
 //校验是否该月佣金已算过
  var tWageNo = fm.WageYear.value + fm.WageMonth.value;
  var strSql = "select AgentCode from LAWage " 
               +"where IndexCalNo = '"+tWageNo+"'"
               +" and state='1' "
               +getWherePart('ManageCom','ManageCom','like')
               +getWherePart('BranchType')
               +getWherePart('BranchType2')
               ;
  //alert(strSql);	    
	//查询SQL，返回结果字符串
  var strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (strQueryResult) {  
    alert("该月佣金已确认过，不能再确认！");
    return false;
  }	
  divAgentQuery.style.display='none'; 
  
  submitForm();	
  
}  


               

/*****************************************************
*  注: 薪资确认后查询
*  
******************************************************/
function AgentWageGatherQuery()
{
 if(fm.all('WageYear1').value==''||fm.all('WageMonth1').value=='' || fm.all('ManageCom1').value==''||fm.all('AgentType').value=='')
  {
    alert("*的为必录项请检查");
    return ;	
  }
 divAgentQuery.style.display='';
 initAgentQueryGrid();
 showRecord();
}

/*******************************************************
  * 注:查询调用函数
  *
  *******************************************************/
function showRecord()
{
  // 拼SQL语句，从页面采集信息
  var tReturn = getManageComLimitlike("a.ManageCom");

  var Year = fm.all('WageYear1').value;
  var Month = fm.all('WageMonth1').value;	    
	//查询SQL，返回结果字符串
	var tbranchtype = fm.BranchType.value;        
  var tbranchtype2 = fm.BranchType2.value;      
  var tagenttype = fm.all('AgentType').value;
	var strSql2 = "select getUniteCode(a.AgentCode),c.Name,b.branchattr,a.ManageCom,"
	             +"a.f22,a.k20,a.f24,"
	             +"a.K01,a.K03,a.K02,a.K04,a.K06,a.K05,a.F06,a.LastMoney,a.CurrMoney,a.SumMoney "
	             +" from LAWage a,labranchgroup b,laagent c " 
               +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  "
               +"and IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null) "
               +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
               +" and a.branchtype = '" + trim(tbranchtype)
			   + "' and  a.branchtype2 = '"+ trim(tbranchtype2) 
			   +"' and c.agenttype = '"+tagenttype+"'"
			   +getWherePart('a.AgentCode','AgentCode')
			   +getWherePart('c.Name','Name')
			   ;
               +" order by a.AgentCode";	
	//查询SQL，返回结果字符串
  turnPage.queryModal(strSql2, AgentQueryGrid);  
}


function AgentWageDownLoad()
{
  var tManageCom  = fm.all('ManageCom1').value;
  if(fm.all('WageYear1').value==''||fm.all('WageMonth1').value=='' || fm.all('ManageCom1').value==''||fm.all('AgentType').value=='')
  {
    alert("*的为必录项请检查");
    return false ;	
  }
  if(AgentQueryGrid == null){
	 alert("请先查询数据");
	 return ;
  }  
 if(AgentQueryGrid.mulLineCount==0)
 {
    alert("列表中没有数据可下载");
    return;
 }
 else {
 // 书写SQL语句
   // 拼SQL语句，从页面采集信息
  var tWageNo = fm.WageYear1.value + fm.WageMonth1.value;
  var tReturn = getManageComLimitlike("a.ManageCom");
  var tbranchtype = fm.BranchType.value;        
  var tbranchtype2 = fm.BranchType2.value; 
  var tagenttype = fm.all('AgentType').value;
  if(tbranchtype2=='01'){
	  var strSql = "select getUniteCode(a.AgentCode),c.Name,b.branchattr,a.ManageCom,"
		             +"a.f22,a.k20,a.f24,"
		             +"a.K01,a.K03,a.K02,a.K04,a.K06,a.K05,a.F06,a.LastMoney,a.CurrMoney,a.SumMoney "
		             +" from LAWage a,labranchgroup b,laagent c " 
	               +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  "
	               +"and IndexCalNo = '"+tWageNo+"' and (b.state<>'1' or b.state is null) "
	               +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
	               +" and a.branchtype = '" + trim(tbranchtype)
				   + "' and  a.branchtype2 = '"+ trim(tbranchtype2)
				   +"' and c.agenttype = '"+tagenttype+"'"
				   +getWherePart('a.AgentCode','AgentCode')
				   +getWherePart('c.Name','Name')			   
				   ;  			   
	               +" order by a.AgentCode";	
  }else{
	  var strSql = "select getUniteCode(a.AgentCode),c.Name,b.branchattr,a.ManageCom,"
          +"a.f22,a.k20,a.f24,"
          +"a.K01,a.K03,a.K02,a.K04,a.F06,a.LastMoney,a.CurrMoney,a.SumMoney "
          +" from LAWage a,labranchgroup b,laagent c " 
        +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode  "
        +"and IndexCalNo = '"+tWageNo+"' and (b.state<>'1' or b.state is null) "
        +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
        +" and a.branchtype = '" + trim(tbranchtype)
		   + "' and  a.branchtype2 = '"+ trim(tbranchtype2)
		   +"' and c.agenttype = '"+tagenttype+"'"
		   +getWherePart('a.AgentCode','AgentCode')
		   +getWherePart('c.Name','Name')			   
		   ;  			   
        +" order by a.AgentCode";	
  }
    fm.querySql.value = strSql;
    var oldAction = fm.action;
    fm.action = "AgentGrpWageDaGatherXLS.jsp?branchtype2="+trim(tbranchtype2);
    fm.submit(); //提交
    fm.action = oldAction;
  }
}	

