<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LATrainGrpInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LATrainInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<body  onload="initForm();" >
  <form action="./LATrainSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLATrain1);">
    <td class=titleImg>
      业务人员培训信息
    </td>
    </td>
    </tr>
    </table>
    
  <Div  id= "divLATrain1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  业务人员代码 
		</td>
        <td  class= input> 
		  <input class= common name=AgentCode onchange="return checkValid();"> 
		</td>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Name>
          </TD>
      </tr>
      <tr  class= common>
        <td  class= title> 
		  销售机构 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=AgentGroup verify="销售机构|notnull&code:AgentGroup"> 
		</td> 
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=ManageCom verify="管理机构|notnull&code:ManageCom"> 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  培训名称
		</td>
        <td  class= input> 
		  <input class= common name=TrainName > 
		</td>
        <td  class= title> 
		  培训类别
		</td>
        <td  class= input> 
		  <input name=AClass class='code' ondblclick="return showCodeList('trainaclass',[this]);" onkeyup="return showCodeListKey('trainaclass',[this]);" > 
		</td>
      </tr>
      <tr  class= common>
        <td  class= title>
		  培训起始时间
		</td>
        <td  class= input>
		  <input name=TrainStart class='coolDatePicker' dateFormat='short' >
		</td>
        <td  class= title>
		  培训中止时间
		</td>
        <td  class= input>
		  <input name=TrainEnd class='coolDatePicker' dateFormat='short' >
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  培训单位
		</td>
        <td  class= input> 
		  <input name=TrainUnit class= common > 
		</td>
        <td  class= title> 
		  负责人 
		</td>
        <td  class= input> 
		  <input name=Charger class= common  > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  成绩
		</td>
        <td  class= input> 
		  <input name=Result class= common > 
		</td>
        <td  class= title> 
		  成绩级别
		</td>
        <td  class= input> 
		  <input name=ResultLevel class='code' ondblclick="return showCodeList('ResultLevel',[this]);" onkeyup="return showCodeListKey('ResultLevel',[this]);" > 
		</td>
      </tr>
      <tr  class= common>
        <td  class= title>
		  培训通过标志
		</td>
        <td  class= input>
		  <input name=TrainPassFlag class= 'code' ondblclick="return showCodeList('yesno',[this]);";onkeyup="return showCodeListKey('yesno',[this]);" >
		</td>
        <td  class= title>
		  批注
		</td>
        <td  class= input>
		  <input name=Noti class= common >
		</td>
      </tr>
      <tr  class= common>
        <td  class= title>
		  执行日期
		</td>
        <td  class= input>
		  <input name=DoneDate class= common >
		</td>
	<td  class= title> 
		  纪录顺序号 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=Idx > 
		</td>
      </tr>
      <tr class=common>
      <td  class= title>
		  操作员代码
		</td>
        <td  class= input>
		  <input name=Operator class="readonly" readonly >
		</td>   
	</tr>	 
      </table>
  </Div>
    <input name=DoneFlag type = hidden class= 'code' ondblclick="return showCodeList('DoneFlag',[this]);";onkeyup="return showCodeListKey('DoneFlag',[this]);">
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=HiddenAgentGroup value=''>
    <input type=hidden name=BranchType value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
