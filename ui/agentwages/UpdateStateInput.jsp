<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
 <html>  
<%
//程序名称：UpdateStateInput.jsp
//程序功能：Input.jsp
//创建日期：2017-10-19
//创建人  ：yangjian

%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="UpdateStateInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UpdateStateInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >    
  <form action=""  method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		<td class= titleImg>查询条件</td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR  class= common> 
      	  <TD  class= title> 管理机构</TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="机构管理|NOTNULL" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" 
            ><Input class ='codename' name ='ManageComName' elementtype=nacessary>
          </TD>
                  
          <TD  class= title > 薪资月</TD>
          <TD  class= input>
            <Input class=common name=WageNo  >
          </TD>
        </TR>
        
		<TR class=common>
          <TD  class= title > 计算状态</TD>
           <TD  class= input>
            <Input class=common name=State  >
          </TD>
         
          <TD  class= title> 展业类型</TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType
		    verify="展业类型|notnull"
		    ondblclick="return showCodeList('branchtype',[this,BranchTypeName],[0,1]);"
			onkeyup="return showCodeListKey('branchtype',[this,BranchTypeName],[0,1]);"
			readonly><Input class=codename name=BranchTypeName elementtype=nacessary>
          </TD>
        </TR>
        
		<TR class=common>
	      <TD class=title> 销售渠道</TD>
		  <TD class=input>
		  	<Input class='codeno' name=BranchType2
		    verify="销售渠道|notnull"
		    ondblclick="return showCodeList('branchtype2',[this,BranchType2Name],[0,1]);"
			onkeyup="return showCodeListKey('branchtype2',[this,BranchType2Name],[0,1]);"
			readonly><Input class=codename name=BranchType2Name elementtype=nacessary>
	     </TD>
	   </TR>
	   
	   <table class='common'>
		 <p><font color="red">注：计算状态有四种状态，分别是计算结束（11）、计算中（12）、薪资确认（13）、薪资审核发放（14），此功能只针对状态是12（计算中）和薪资确认（13）、（14)审核发放修改为计算结束状态！</font></p>
	   </table>
	   <table class='common'>
		 <p><font color="red">注：薪资月格式为年加月（如：201407）！</font></p>
	   </table>

       <TR class=input>     
         <TD class=common>
          <input type =button class=cssbutton value="查询" onclick="UpdateStateQuery();">    
          <input type =button class=cssbutton value="修改" onclick="UpdateStateUpdate();">
          </td>
       </TR>          
      </table>
    </Div>  
    <Div  id= "divAgentQuery" style= "display: ''">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanUpdateStateGrid" ></span> 
  			</TD>
      </TR>
     </Table>	
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
   </Div>	
  </form>   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
<script>
  var mcodeSql = "#1# and (code = #2# or code = #3#)";
</script>