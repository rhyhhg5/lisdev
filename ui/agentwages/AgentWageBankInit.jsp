   <%
//程序名称：AgentWageBankInit.jsp
//程序功能：
//创建日期：2003-06-30
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                
    fm.all('ManageCom').value = '';
    fm.all('WageYear').value = '';
    fm.all('WageMonth').value = '';
    fm.all('querySql').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';  
    fm.all('BranchType2').value = '<%=BranchType2%>';    
    
    
  }
  catch(ex)
  {
    alert("在AgentWageBankInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
 var AgentQueryGrid ;
 
// 保单信息列表的初始化
function initAgentQueryGrid()
  {                               
    var iArray = new Array();
    try
	{ 
      if (fm.all('BranchType').value=='3')
	      {    
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            		//列最大值
	      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	 
	      iArray[1]=new Array();
	      iArray[1][0]="代理人编码";         	//列名
	      iArray[1][1]="80px";              	//列宽
	      iArray[1][2]=200;            	        //列最大值
	      iArray[1][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[2]=new Array();
	      iArray[2][0]="代理人姓名";         	//列名
	      iArray[2][1]="80px";              	//列宽
	      iArray[2][2]=120;            	        //列最大值
	      iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[3]=new Array();
	      iArray[3][0]="代理人销售单位";         	//列名
	      iArray[3][1]="100px";            		//列宽
	      iArray[3][2]=200;            	        //列最大值
	      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[4]=new Array();
	      iArray[4][0]="管理机构";         		//列名
	      iArray[4][1]="80px";            		//列宽
	      iArray[4][2]=200;            	        //列最大值
	      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[5]=new Array();
	      iArray[5][0]="基薪";         		//列名
	      iArray[5][1]="60px";            		//列宽
	      iArray[5][2]=200;            	        //列最大值
	      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[6]=new Array();
	      iArray[6][0]="业务";         		//列名
	      iArray[6][1]="60px";            		//列宽
	      iArray[6][2]=200;            	        //列最大值
	      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[7]=new Array();
	      iArray[7][0]="绩效奖金";         		//列名
	      iArray[7][1]="60px";            		//列宽
	      iArray[7][2]=200;            	        //列最大值
	      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[8]=new Array();
	      iArray[8][0]="上次余额";         		//列名
	      iArray[8][1]="60px";            		//列宽
	      iArray[8][2]=200;            	        //列最大值
	      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[9]=new Array();
	      iArray[9][0]="养老保险";         		//列名
	      iArray[9][1]="60px";            		//列宽
	      iArray[9][2]=200;            	        //列最大值
	      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[10]=new Array();
	      iArray[10][0]="失业保险";         		//列名
	      iArray[10][1]="60px";            		//列宽
	      iArray[10][2]=200;            	        //列最大值
	      iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[11]=new Array();
	      iArray[11][0]="医疗保险";         		//列名
	      iArray[11][1]="60px";            		//列宽
	      iArray[11][2]=200;            	        //列最大值
	      iArray[11][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[12]=new Array();
	      iArray[12][0]="工伤保险";         		//列名
	      iArray[12][1]="60px";            		//列宽
	      iArray[12][2]=200;            	        //列最大值
	      iArray[12][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	  
	      iArray[13]=new Array();
	      iArray[13][0]="住房公积金";         		//列名
	      iArray[13][1]="60px";            		//列宽
	      iArray[13][2]=200;            	        //列最大值
	      iArray[13][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[14]=new Array();
	      iArray[14][0]="生育金";         		//列名
	      iArray[14][1]="60px";            		//列宽
	      iArray[14][2]=200;            	        //列最大值
	      iArray[14][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	
	      iArray[15]=new Array();
	      iArray[15][0]="差勤加扣款";         		//列名
	      iArray[15][1]="60px";            		//列宽
	      iArray[15][2]=200;            	        //列最大值
	      iArray[15][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[16]=new Array();
	      iArray[16][0]="税前基薪加扣款";         		//列名
	      iArray[16][1]="60px";            		//列宽
	      iArray[16][2]=200;            	        //列最大值
	      iArray[16][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[17]=new Array();
	      iArray[17][0]="税前绩效加扣款";         		//列名
	      iArray[17][1]="60px";            		//列宽
	      iArray[17][2]=200;            	        //列最大值
	      iArray[17][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[18]=new Array();
	      iArray[18][0]="税前工资";         		//列名
	      iArray[18][1]="60px";            		//列宽
	      iArray[18][2]=200;            	        //列最大值
	      iArray[18][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	  
	      iArray[19]=new Array();
	      iArray[19][0]="个人所得税";         		//列名
	      iArray[19][1]="60px";            		//列宽
	      iArray[19][2]=200;            	        //列最大值
	      iArray[19][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[20]=new Array();
	      iArray[20][0]="税后基薪加扣款";         		//列名
	      iArray[20][1]="60px";            		//列宽
	      iArray[20][2]=200;            	        //列最大值
	      iArray[20][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[21]=new Array();
	      iArray[21][0]="应发金额";         		//列名
	      iArray[21][1]="60px";            		//列宽
	      iArray[21][2]=200;            	        //列最大值
	      iArray[21][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[22]=new Array();
	      iArray[22][0]="实发金额";         		//列名
	      iArray[22][1]="60px";            		//列宽
	      iArray[22][2]=200;            	        //列最大值
	      iArray[22][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
        }
        else if (fm.all('BranchType').value=='2')
        {    
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            		//列最大值
	      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	 
	      iArray[1]=new Array();
	      iArray[1][0]="业务员代码";         	//列名
	      iArray[1][1]="80px";              	//列宽
	      iArray[1][2]=200;            	        //列最大值
	      iArray[1][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[2]=new Array();
	      iArray[2][0]="业务员姓名";         	//列名
	      iArray[2][1]="80px";              	//列宽
	      iArray[2][2]=120;            	        //列最大值
	      iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[3]=new Array();
	      iArray[3][0]="业务员销售单位";        	//列名
	      iArray[3][1]="100px";            		//列宽
	      iArray[3][2]=200;            	        //列最大值
	      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[4]=new Array();
	      iArray[4][0]="管理机构";         		//列名
	      iArray[4][1]="80px";            		//列宽
	      iArray[4][2]=200;            	        //列最大值
	      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[5]=new Array();
	      iArray[5][0]="基本工资";         		//列名
	      iArray[5][1]="60px";            		//列宽
	      iArray[5][2]=200;            	        //列最大值
	      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[6]=new Array();
	      iArray[6][0]="基本工资补发";         		//列名
	      iArray[6][1]="60px";            		//列宽
	      iArray[6][2]=200;            	        //列最大值
	      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      	
	      iArray[7]=new Array();
	      iArray[7][0]="月度提奖";         		//列名
	      iArray[7][1]="60px";            		//列宽
	      iArray[7][2]=200;            	        //列最大值
	      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[8]=new Array();
	      iArray[8][0]="季度达成奖";         		//列名
	      iArray[8][1]="65px";            		//列宽
	      iArray[8][2]=200;            	        //列最大值
	      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[9]=new Array();
	      iArray[9][0]="效益提奖";         		//列名
	      iArray[9][1]="0px";            		//列宽
	      iArray[9][2]=200;            	        //列最大值
	      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[10]=new Array();
	      iArray[10][0]="年终奖";         		//列名
	      iArray[10][1]="60px";            		//列宽
	      iArray[10][2]=200;            	        //列最大值
	      iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许	

	      
	      iArray[11]=new Array();
	      iArray[11][0]="育成奖";         		//列名
	      iArray[11][1]="60px";            		//列宽
	      iArray[11][2]=200;            	        //列最大值
	      iArray[11][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[12]=new Array();
	      iArray[12][0]="岗位工资";         		//列名
	      iArray[12][1]="60px";            		//列宽
	      iArray[12][2]=200;            	        //列最大值
	      iArray[12][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	  
	      iArray[13]=new Array();
	      iArray[13][0]="绩效工资";         		//列名
	      iArray[13][1]="60px";            		//列宽
	      iArray[13][2]=200;            	        //列最大值
	      iArray[13][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[14]=new Array();
	      iArray[14][0]="半年绩效补发";         		//列名
	      iArray[14][1]="60px";            		//列宽
	      iArray[14][2]=200;            	        //列最大值
	      iArray[14][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      	
	      iArray[15]=new Array();
	      iArray[15][0]="月度管理津贴";         		//列名
	      iArray[15][1]="80px";            		//列宽
	      iArray[15][2]=200;            	        //列最大值
	      iArray[15][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[16]=new Array();
	      iArray[16][0]="年效益奖";         		//列名
	      iArray[16][1]="80px";            		//列宽
	      iArray[16][2]=200;            	        //列最大值
	      iArray[16][3]=0;                   	//是否允许输入,1表	
	
	
	      iArray[17]=new Array();
	      iArray[17][0]="半年岗位工资补发";         		//列名
	      iArray[17][1]="80px";            		//列宽
	      iArray[17][2]=200;            	        //列最大值
	      iArray[17][3]=0;                   	//是否允许输入,1表示允许，0
	      
	      iArray[18]=new Array();
	      iArray[18][0]="扣款";         		//列名
	      iArray[18][1]="80px";            		//列宽
	      iArray[18][2]=200;            	        //列最大值
	      iArray[18][3]=0;                   	//是否允许输入,1表示允许，0
	
	      iArray[19]=new Array();
	      iArray[19][0]="税前薪资";         		//列名
	      iArray[19][1]="0px";            		//列宽
	      iArray[19][2]=200;            	        //列最大值
	      iArray[19][3]=0;                   	//是否允许输入,1表示允许，0表示不允许	      
	
	      iArray[20]=new Array();
	      iArray[20][0]="上次薪资余额";         		//列名
	      iArray[20][1]="80px";            		//列宽
	      iArray[20][2]=200;            	        //列最大值
	      iArray[20][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[21]=new Array();
	      iArray[21][0]="提奖加款";         		//列名
	      iArray[21][1]="80px";            		//列宽
	      iArray[21][2]=200;            	        //列最大值
	      iArray[21][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[22]=new Array();
	      iArray[22][0]="其他扣款";         		//列名
	      iArray[22][1]="80px";            		//列宽
	      iArray[22][2]=200;            	        //列最大值
	      iArray[22][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[23]=new Array();
	      iArray[23][0]="提奖扣款";         		//列名
	      iArray[23][1]="80px";            		//列宽
	      iArray[23][2]=200;            	        //列最大值
	      iArray[23][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[24]=new Array();
	      iArray[24][0]="其他扣款";         		//列名
	      iArray[24][1]="80px";            		//列宽
	      iArray[24][2]=200;            	        //列最大值
	      iArray[24][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[25]=new Array();
	      iArray[25][0]="本期应发薪资";         		//列名
	      iArray[25][1]="80px";            		//列宽
	      iArray[25][2]=200;            	        //列最大值
	      iArray[25][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	      iArray[26]=new Array();
	      iArray[26][0]="本期实发薪资";         		//列名
	      iArray[26][1]="80px";            		//列宽
	      iArray[26][2]=200;            	        //列最大值
	      iArray[26][3]=0;                   	//是否允许输入,1表示允许，0表示不允许	
         
        

        }  
        else
        {
          divAgentQuery.style.display='none';
          alert("输入的展业机构类型有误，请重新输入！！！");
          return;
        }  
      AgentQueryGrid = new MulLineEnter( "fm" , "AgentQueryGrid" ); 
      //这些属性必须在loadMulLine前
      AgentQueryGrid.mulLineCount = 10;   
      AgentQueryGrid.displayTitle = 1;
      AgentQueryGrid.hiddenPlus = 1;
      AgentQueryGrid.hiddenSubtraction = 1;
      AgentQueryGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
 }
function initForm()
{
  try
  {
    initInpBox();
    initAgentQueryGrid();
  }
  catch(re)
  {
    alert("AgentWageBankInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>
