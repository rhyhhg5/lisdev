<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：UnLockList.jsp
	//程序功能：清单下载
	//创建日期：2018-05-24
	//创建人  ：zyy
	//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
	boolean errorFlag = false;
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");

	//生成文件名
	Calendar cal = new GregorianCalendar();
	String min = String.valueOf(cal.get(Calendar.MINUTE));
	String sec = String.valueOf(cal.get(Calendar.SECOND));
	String downLoadFileName = "尚未付费成功信息清单_" + tG.Operator + "_" + min + sec
			+ ".xls";
	String filePath = application.getRealPath("temp");
	String tOutXmlPath = filePath + File.separator + downLoadFileName;
	System.out.println("OutXmlPath:" + tOutXmlPath);
   System.out.println(request.getParameter("strSQL"));
	//隐藏字段提供查询sql
	String querySql = request.getParameter("strSQL");
	
	System.out.println(querySql);
	//自己在这里写一个sql,这个是可以执行的
	//String querySql = "select distinct a.actugetno,a.otherno,a.Drawer,(select codename from ldcode where '1357623350000'='1357623350000' and  codetype = 'paymode' and code = a.paymode ), a.sumgetmoney,a.bankcode,a.bankaccno,a.accname, case a.cansendbank when '1' then '已锁定' else '未锁定' end, a.sendbankcount,(select d.codename from lyreturnfrombankb c, ldcode1 d where c.paycode=a.actugetno and d.codetype='bankerror' and d.code=c.bankcode and d.code1=c.banksuccflag and c.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.actugetno)),ManageCom,ShouldDate,'' from ljaget a where 1=1 and a.confdate is null  and (CanSendBank = '0' Or CanSendBank is null) and ManageCom like '86%' Order by ShouldDate desc  fetch first 3000 rows only";
	//这个暂时不管
	querySql = querySql.replaceAll("%25", "%");
	//设置表头
	String[] tTitle = { "薪资月","管理机构","业务员代码","给付通知书号", "案件号/批单号","付款方式", "付款金额","银行代码",
			"银行帐号", "帐户姓名", "是否锁定", "发盘次数", "失败原因", "应付时间" };

	//数据的显示属性(指定对应列是否显示在清单中)
	int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14};
	//生成文件
	CreateExcelListEx createexcellist = new CreateExcelListEx("");//指定文件名
	createexcellist.createExcelFile();
	String[] sheetName = { "list" };
	createexcellist.addSheet(sheetName);
	if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
		errorFlag = true;
	}
	if (!errorFlag) {
		//写文件到磁盘
		try {
			createexcellist.write(tOutXmlPath);
		} catch (Exception e) {
			errorFlag = true;
			System.out.println(e);
		}
	}
	//返回客户端
	if (!errorFlag) {
		downLoadFile(response, filePath, downLoadFileName);
	}

	out.clear();
	out = pageContext.pushBody();
%>

