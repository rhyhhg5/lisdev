<%
//程序名称：LAWageConfirmInit.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {        
  fm.all('BranchType').value = '<%=BranchType%>';  
  fm.all('BranchType2').value = '<%=BranchType2%>';
                   
  //$initfield$
  }
  catch(ex)
  {
    alert("在LAWageConfirmInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAWageConfirmInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	  initWageGrid();
  }
  catch(re)
  {
    alert("LAWageConfirmInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化WageGrid
 ************************************************************
 */
function initWageGrid()
  {                               
    var iArray = new Array();
      
      try
      {
	   if (fm.all('AgentType').value=='2')
		  {   
	        iArray[0]=new Array();
	        iArray[0][0]="序号";         //列名
	        iArray[0][1]="30px";         //列名
	        iArray[0][2]=100;         //列名
	        iArray[0][3]=0;         //列名
	
	        iArray[1]=new Array();
	        iArray[1][0]="薪资年月";         //列名
	        iArray[1][1]="80px";         //宽度
	        iArray[1][2]=100;         //最大长度
	        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
	
	        iArray[2]=new Array();
	        iArray[2][0]="管理机构";         //列名
	        iArray[2][1]="80px";         //宽度
	        iArray[2][2]=100;         //最大长度
	        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
	
	        iArray[3]=new Array();
	        iArray[3][0]="团队代码";         //列名
	        iArray[3][1]="80px";         //宽度
	        iArray[3][2]=100;         //最大长度
	        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
	
	        iArray[4]=new Array();
	        iArray[4][0]="团队名称";         //列名
	        iArray[4][1]="60px";         //宽度
	        iArray[4][2]=100;         //最大长度
	        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[5]=new Array();
	        iArray[5][0]="业务员代码";         //列名
	        iArray[5][1]="60px";         //宽度
	        iArray[5][2]=100;         //最大长度
	        iArray[5][3]=0;
	
	        iArray[6]=new Array();
	        iArray[6][0]="业务员名称";         //列名
	        iArray[6][1]="60px";         //宽度
	        iArray[6][2]=100;         //最大长度
	        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
	  
	        iArray[7]=new Array();
	        iArray[7][0]="职级名称";         //列名
	        iArray[7][1]="60px";         //宽度
	        iArray[7][2]=100;         //最大长度
	        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
	 
	 		iArray[8]=new Array();
	        iArray[8][0]="员工类型";         //列名
	        iArray[8][1]="60px";         //宽度
	        iArray[8][2]=100;         //最大长度
	        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
	
	 		iArray[9]=new Array();
	        iArray[9][0]="入司时间";         //列名
	        iArray[9][1]="80px";         //宽度
	        iArray[9][2]=100;         //最大长度
	        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
	
	 		iArray[10]=new Array();
	        iArray[10][0]="在职状态";         //列名
	        iArray[10][1]="80px";         //宽度
	        iArray[10][2]=100;         //最大长度
	        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
	
	 		iArray[11]=new Array();
	        iArray[11][0]="离职（登记/确认）日期";         //列名
	        iArray[11][1]="80px";         //宽度
	        iArray[11][2]=100;         //最大长度
	        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
	
	 		iArray[12]=new Array();
	        iArray[12][0]="基本工资标准";         //列名
	        iArray[12][1]="80px";         //宽度
	        iArray[12][2]=100;         //最大长度
	        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许

	 		iArray[13]=new Array();
	        iArray[13][0]="月度业绩达成率";         //列名
	        iArray[13][1]="80px";         //宽度
	        iArray[13][2]=100;         //最大长度
	        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许

	 		iArray[14]=new Array();
	        iArray[14][0]="基本工资";         //列名
	        iArray[14][1]="80px";         //宽度
	        iArray[14][2]=100;         //最大长度
	        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
	
	 		iArray[15]=new Array();
	        iArray[15][0]="基本工资补发";         //列名
	        iArray[15][1]="80px";         //宽度
	        iArray[15][2]=100;         //最大长度
	        iArray[15][3]=0;         //是否允许录入，0--不能，1--允许
	
	        iArray[16]=new Array();
	        iArray[16][0]="绩效工资";         //列名
	        iArray[16][1]="80px";         //宽度
	        iArray[16][2]=100;         //最大长度
	        iArray[16][3]=0;         //是否允许录入，0--不能，1--允许

	 		iArray[17]=new Array();
	        iArray[17][0]="绩效工资补发";         //列名
	        iArray[17][1]="80px";         //宽度
	        iArray[17][2]=100;         //最大长度
	        iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
	       

	
	 		iArray[18]=new Array();
	        iArray[18][0]="基本工资加款";         //列名
	        iArray[18][1]="80px";         //宽度
	        iArray[18][2]=100;         //最大长度
	        iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
	
	 		iArray[19]=new Array();
	        iArray[19][0]="基本工资扣款";         //列名
	        iArray[19][1]="80px";         //宽度
	        iArray[19][2]=100;         //最大长度
	        iArray[19][3]=0;       //是否允许录入，0--不能，1--允许2
	 		
	        iArray[20]=new Array();
	        iArray[20][0]="绩效工资加款";         //列名
	        iArray[20][1]="80px";         //宽度
	        iArray[20][2]=100;         //最大长度
	        iArray[20][3]=0;       //是否允许录入，0--不能，1--允许3
	 		
	        iArray[21]=new Array();
	        iArray[21][0]="绩效工资扣款";         //列名
	        iArray[21][1]="80px";         //宽度
	        iArray[21][2]=100;         //最大长度
	        iArray[21][3]=0;       //是否允许录入，0--不能，1--允许
	 	
	        iArray[22]=new Array();
	        iArray[22][0]="奖励方案加款";         //列名
	        iArray[22][1]="80px";         //宽度
	        iArray[22][2]=100;         //最大长度
	        iArray[22][3]=0;       //是否允许录入，0--不能，1--允许
	 		
	        iArray[23]=new Array();
	        iArray[23][0]="奖励方案扣款";         //列名
	        iArray[23][1]="80px";         //宽度
	        iArray[23][2]=100;         //最大长度
	        iArray[23][3]=0;       //是否允许录入，0--不能，1--允许
	 		
	        iArray[24]=new Array();
	        iArray[24][0]="其它加款";         //列名
	        iArray[24][1]="80px";         //宽度
	        iArray[24][2]=100;         //最大长度
	        iArray[24][3]=0;       //是否允许录入，0--不能，1--允许
	        
	 		iArray[25]=new Array();
	        iArray[25][0]="其它扣款";         //列名
	        iArray[25][1]="80px";         //宽度
	        iArray[25][2]=100;         //最大长度
	        iArray[25][3]=0;       //是否允许录入，0--不能，1--允许
 	        
	        iArray[26]=new Array();
		    iArray[26][0]="交叉销售佣金";         		//列名
		    iArray[26][1]="80px";            		//列宽
		    iArray[26][2]=200;            	        //列最大值
		    iArray[26][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
		    
		    iArray[27]=new Array();
		    iArray[27][0]="实发金额";         		//列名
		    iArray[27][1]="80px";            		//列宽
		    iArray[27][2]=200;            	        //列最大值
		    iArray[27][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
            
            
		  }
		  else if(fm.all('AgentType').value=='1')
			  {
		        iArray[0]=new Array();
		        iArray[0][0]="序号";         //列名
		        iArray[0][1]="30px";         //列名
		        iArray[0][2]=100;         //列名
		        iArray[0][3]=0;         //列名
		
		        iArray[1]=new Array();
		        iArray[1][0]="薪资年月";         //列名
		        iArray[1][1]="80px";         //宽度
		        iArray[1][2]=100;         //最大长度
		        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
		
		        iArray[2]=new Array();
		        iArray[2][0]="管理机构";         //列名
		        iArray[2][1]="80px";         //宽度
		        iArray[2][2]=100;         //最大长度
		        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
		
		        iArray[3]=new Array();
		        iArray[3][0]="团队代码";         //列名
		        iArray[3][1]="80px";         //宽度
		        iArray[3][2]=100;         //最大长度
		        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
		
		        iArray[4]=new Array();
		        iArray[4][0]="团队名称";         //列名
		        iArray[4][1]="60px";         //宽度
		        iArray[4][2]=100;         //最大长度
		        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
		        
		        iArray[5]=new Array();
		        iArray[5][0]="业务员代码";         //列名
		        iArray[5][1]="60px";         //宽度
		        iArray[5][2]=100;         //最大长度
		        iArray[5][3]=0;
		
		        iArray[6]=new Array();
		        iArray[6][0]="业务员名称";         //列名
		        iArray[6][1]="60px";         //宽度
		        iArray[6][2]=100;         //最大长度
		        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
		  
		        iArray[7]=new Array();
		        iArray[7][0]="职级名称";         //列名
		        iArray[7][1]="60px";         //宽度
		        iArray[7][2]=100;         //最大长度
		        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
		 
		 		iArray[8]=new Array();
		        iArray[8][0]="员工类型";         //列名
		        iArray[8][1]="60px";         //宽度
		        iArray[8][2]=100;         //最大长度
		        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
		
		 		iArray[9]=new Array();
		        iArray[9][0]="入司时间";         //列名
		        iArray[9][1]="80px";         //宽度
		        iArray[9][2]=100;         //最大长度
		        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
		
		 		iArray[10]=new Array();
		        iArray[10][0]="在职状态";         //列名
		        iArray[10][1]="80px";         //宽度
		        iArray[10][2]=100;         //最大长度
		        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
		
		 		iArray[11]=new Array();
		        iArray[11][0]="离职（登记/确认）日期";         //列名
		        iArray[11][1]="80px";         //宽度
		        iArray[11][2]=100;         //最大长度
		        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许

		 		iArray[12]=new Array();
		        iArray[12][0]="基本报酬标准";         //列名
		        iArray[12][1]="80px";         //宽度
		        iArray[12][2]=100;         //最大长度
		        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许

		 		iArray[13]=new Array();
		        iArray[13][0]="月度业绩达成率";         //列名
		        iArray[13][1]="80px";         //宽度
		        iArray[13][2]=100;         //最大长度
		        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
		
		 		iArray[14]=new Array();
		        iArray[14][0]="基本报酬";         //列名
		        iArray[14][1]="80px";         //宽度
		        iArray[14][2]=100;         //最大长度
		        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
		
		 		iArray[15]=new Array();
		        iArray[15][0]="基本工资补发";         //列名
		        iArray[15][1]="80px";         //宽度
		        iArray[15][2]=100;         //最大长度
		        iArray[15][3]=0;         //是否允许录入，0--不能，1--允许
		
		 		iArray[16]=new Array();
		        iArray[16][0]="首期绩效提奖";         //列名
		        iArray[16][1]="80px";         //宽度
		        iArray[16][2]=100;         //最大长度
		        iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
		        
		        iArray[17]=new Array();
		        iArray[17][0]="续期提奖";         //列名
		        iArray[17][1]="80px";         //宽度
		        iArray[17][2]=100;         //最大长度
		        iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
		
		 		iArray[18]=new Array();
		        iArray[18][0]="主管间接佣金";         //列名
		        iArray[18][1]="80px";         //宽度
		        iArray[18][2]=100;         //最大长度
		        iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
		        
		        
		
		 		iArray[19]=new Array();
		        iArray[19][0]="基本报酬加款";         //列名
		        iArray[19][1]="80px";         //宽度
		        iArray[19][2]=100;         //最大长度
		        iArray[19][3]=0;         //是否允许录入，0--不能，1--允许
		
		 		iArray[20]=new Array();
		        iArray[20][0]="基本报酬扣款";         //列名
		        iArray[20][1]="80px";         //宽度
		        iArray[20][2]=100;         //最大长度
		        iArray[20][3]=0;       //是否允许录入，0--不能，1--允许

		 		iArray[21]=new Array();
		        iArray[21][0]="绩效提奖加款";         //列名
		        iArray[21][1]="80px";         //宽度
		        iArray[21][2]=100;         //最大长度
		        iArray[21][3]=0;       //是否允许录入，0--不能，1--允许        
		      
		        iArray[22]=new Array();
		        iArray[22][0]="绩效提奖扣款";         //列名
		        iArray[22][1]="80px";         //宽度
		        iArray[22][2]=100;         //最大长度
		        iArray[22][3]=0;       //是否允许录入，0--不能，1--允许     
		        
		        iArray[23]=new Array();
		        iArray[23][0]="奖励方案加款";         //列名
		        iArray[23][1]="80px";         //宽度
		        iArray[23][2]=100;         //最大长度
		        iArray[23][3]=0;       //是否允许录入，0--不能，1--允许        
		     
		        iArray[24]=new Array();
		        iArray[24][0]="奖励方案扣款";         //列名
		        iArray[24][1]="80px";         //宽度
		        iArray[24][2]=100;         //最大长度
		        iArray[24][3]=0;       //是否允许录入，0--不能，1--允许        
		     
		        iArray[25]=new Array();
			      iArray[25][0]="其它加款";         		//列名
			      iArray[25][1]="80px";            		//列宽
			      iArray[25][2]=200;            	        //列最大值
			      iArray[25][3]=0;  
			
			      iArray[26]=new Array();
			      iArray[26][0]="其它扣款";         		//列名
			      iArray[26][1]="80px";            		//列宽
			      iArray[26][2]=200;            	        //列最大值
			      iArray[26][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
			
			      iArray[27]=new Array();
			      iArray[27][0]="交叉销售佣金";         		//列名
			      iArray[27][1]="80px";            		//列宽
			      iArray[27][2]=200;            	        //列最大值
			      iArray[27][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
		
			      iArray[28]=new Array();
			      iArray[28][0]="实发金额";         		//列名
			      iArray[28][1]="80px";            		//列宽
			      iArray[28][2]=200;            	        //列最大值
			      iArray[28][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
		
			  } 
		       else{
//		    	  divWage.style.display='none';
//			     return;
		    }    
        WageGrid = new MulLineEnter( "fm" , "WageGrid" ); 

        //这些属性必须在loadMulLine前
        WageGrid.mulLineCount = 0;   
        WageGrid.displayTitle = 1;
        WageGrid.locked=1;
        WageGrid.canSel=0;
        WageGrid.canChk=0;
       	WageGrid.hiddenPlus = 1;
      	WageGrid.hiddenSubtraction = 1;
        WageGrid.loadMulLine(iArray);   

      }
      catch(ex)
      {
        alert("初始化WageGrid时出错："+ ex);
      }
    }


</script>