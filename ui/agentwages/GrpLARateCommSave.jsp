<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LARateCommSave.jsp
//程序功能：
//创建人  ：销售管理
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentwages.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LARateCommisionSchema tLARateCommisionSchema = new LARateCommisionSchema();
  LARateCommisionBSchema bLARateCommisionBSchema = new LARateCommisionBSchema();
  LARateCommUI tLARateCommUI = new LARateCommUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate = tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
  
  
  tLARateCommisionSchema.setF02("F");
  tLARateCommisionSchema.setF05(" ");
  tLARateCommisionSchema.setRiskCode(request.getParameter("RiskCode"));  
  tLARateCommisionSchema.setBranchType(request.getParameter("BranchType"));
  tLARateCommisionSchema.setAppAge(request.getParameter("AppAge"));
  tLARateCommisionSchema.setYear(request.getParameter("Years"));
  tLARateCommisionSchema.setPayIntv(request.getParameter("PayIntv"));
  tLARateCommisionSchema.setRate(request.getParameter("Rate"));
  tLARateCommisionSchema.setF01(request.getParameter("CalType"));
  tLARateCommisionSchema.setF06(request.getParameter("PolType"));
  tLARateCommisionSchema.setsex("0");
  tLARateCommisionSchema.setCurYear(request.getParameter("CurYear"));
  tLARateCommisionSchema.setOperator(request.getParameter("Operator"));
  tLARateCommisionSchema.setManageCom(request.getParameter("ManageCom"));
  
  bLARateCommisionBSchema.setF05(" ");
  bLARateCommisionBSchema.setRiskCode(request.getParameter("RiskCodeB"));
  bLARateCommisionBSchema.setF02("F");
  bLARateCommisionBSchema.setBranchType(request.getParameter("BranchTypeB"));
  bLARateCommisionBSchema.setAppAge(request.getParameter("AppAgeB"));
  bLARateCommisionBSchema.setYear(request.getParameter("YearsB"));
  bLARateCommisionBSchema.setPayIntv(request.getParameter("PayIntvB"));
  bLARateCommisionBSchema.setrate(request.getParameter("RateB"));
  bLARateCommisionBSchema.setF01(request.getParameter("CalTypeB"));
  bLARateCommisionBSchema.setF06(request.getParameter("PolTypeB"));
  bLARateCommisionBSchema.setsex("0");
  bLARateCommisionBSchema.setCurYear(request.getParameter("CurYearB"));
  bLARateCommisionBSchema.setOperator(request.getParameter("OperatorB"));
  bLARateCommisionBSchema.setManageCom(request.getParameter("ManageComB"));

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tLARateCommisionSchema);
  tVData.addElement(bLARateCommisionBSchema);
  tVData.add(tG);
  try
  {
    tLARateCommUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLARateCommUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.Operator.value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

