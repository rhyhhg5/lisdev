   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
//提数操作

function initEdorType(cObj)
{
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeList('comcode',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeListKey('comcode',[cObj], null, null, mEdorType, "1");
}


function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  
  fm.submit(); //提交
  

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行提数         


//新增：2004-06-08 LL
//功能：佣金计算结果查询
function AgentWageGatherResult()
{
 	if(!verifyInput()) 
  { return; }	
  //下面增加相应的代码
  showInfo=window.open("./LAWageCalResultQuery.html");
}
               
function checkvalid()
{
	var strSQL = "";
 
  if (getWherePart('BranchAttr')!='')
  {
     strSQL = "select AgentGroup from LAbranchgroup where 1=1 "
     + getWherePart('BranchType','BranchType')
      + getWherePart('BranchType2','BranchType2')
	     + getWherePart('BranchAttr','BranchAttr');
	     
     var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  
 // alert("2");
 
  //alert("3");
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此组别！");
    fm.all('BranchAttr').value="";
    fm.all('AgentGroup').value = "";
    return;
  }
  }
else
	{
		fm.all('BranchAttr').value="";
    fm.all('AgentGroup').value = "";
     return;
  }
  //alert("4");
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  //<rem>######//

  //</rem>######//
 // alert("5");
  fm.all('AgentGroup').value  =tArr[0][0];
 // alert(fm.all('AgentGroup').value);
 
	}
//执行查询
function AgentWageGatherQuery()
{
		if(!verifyInput()) 
  { return; }	
//   window.open("./AgentWageGatherQuery.jsp?WageYear="+WageYear+);
 if(fm.all('WageYear').value=='')
  {
    alert("请填写查询条件");
    return ;	
  }
 divAgentQuery.style.display='';
 //AgentQueryGrid.clearData();
 initAgentQueryGrid();
 showRecord();
}

//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord()
{
  // 拼SQL语句，从页面采集信息
  var tWageNo = fm.WageYear.value;
  var tReturn =getManageComLimitlike("a.ManageCom");
   
   var tAgentGroup=fm.AgentGroup.value;
 var strSql = "select AgentCode,(select laagent.name from laagent where laagent.AgentCode=a.AgentCode),branchattr,"
        +"managecom,contno,polno,transmoney,fyc,fycrate" 
     +" from LAcommision a where 1=1 and wageno='"+tWageNo+"' and branchtype = '" + fm.BranchType.value+"' and branchtype2 = '" + fm.BranchType2.value+"'  and branchseries like '%%"+tAgentGroup+"%25'"
     +getWherePart('a.ManageCom','ManageCom','like')
     +getWherePart('a.AgentCode','AgentCode','like')
     
 //    +getWherePart('a.BranchSeries','AgentGroup','like')
               + tReturn 
               +" order by a.AgentCode";
               
               
 
  
    
   //  (select b.branchattr from lacommision b where b.branchseries like '%"+a.agentgroup+"%')commisionsn
            
   //    strSql+="  order by a.AgentCode";      branchtype = '" + fm.BranchType.value+"'
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}