<%
//程序名称：LAWelfareRadixQueryInput.jsp
//程序功能：
//创建日期：2003-6-28
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAWelfareRadixQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAWelfareRadixQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <title>福利信息</title>
</head>
<body  onload="initForm();" >
  <form action="./LAWelfareRadixQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAWelfareRadixGrid1);">
      </td>
      <td class= titleImg>
        请您输入查询条件： 
      </td>
    	</tr>   
    </table>
  <Div  id= "divLAWelfareRadixGrid1" style= "display: ''">    
  <table  class= common>
        <TR  class= common>
          <TD  class= title>
            地区编码
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
          </TD>
          <TD  class= title>
            员工职级
          </TD>
          <TD  class= input>
            <!--Input class= common name=AgentGrade -->
            <Input class="code" name=AgentGrade ondblclick="showCodeList('agentgrade',[this],null,null,codeSql,'1',null);" onkeyup="return showCodeListKey('agentgrade',[this],null,null,codeSql,'1',null);" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            福利类别
          </TD>
          <TD  class= input>
            <!--Input class= common name=AClass --->
            <Input class="code" name=AClass ondblclick="showCodeList('AClass',[this],null,null,tcodeSql,'1',null);" onkeyup="return showCodeListKey('AClass',[this],null,null,tcodeSql,'1',null);" >
          </TD>
          <TD  class= title>
          	展业类型            
          </TD>
          <TD  class= input>
            <Input class="code" name=BranchType verify="展业机构类型|code:BranchType" ondblclick="return showCodeList('branchtype',[this],null,null,mcodeSql,'1',null);" onkeyup="return showCodeListKey('branchtype',[this],null,null,mcodeSql,'1',null);" >
            <!--Input class='code' name=BranchType verify="展业机构类型|code:BranchType" ondblclick="return showCodeList('BranchType',[this]);" 
                                                            onkeyup="return showCodeListKey('BranchType',[this]);"-->
          </TD>
        </TR>
</table>
 </Div>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAWelfareRadixGrid);">
    		</td>
    		<td class= titleImg>
    			 福利信息结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLAWelfareRadixGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLAWelfareRadixGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  var codeSql = "#1# and codetype=#agentgrade# and (code like #B%# or code like #C%# or code like #D%# or code like #E%#)";
  var tcodeSql = "#1# and othersign=#Y#";
  var mcodeSql = "#1# and (code = #2# or code = #3#)";
</script>
