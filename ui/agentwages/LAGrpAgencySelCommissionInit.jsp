<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  tmanagecom=" 1 and  char(length(trim(comcode))) = #8# and comcode<>#86000000# ";
//var tmanagecom =" 1 and   char(length(trim(comcode)))<=#4# ";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
	
	fm.all('ManageCom').value ='';
	fm.all('ManageComName').value='';
	fm.all('StartYearMonth').value='';
    fm.all('EndYearMonth').value='';  
    fm.all('GrpAgentCode').value='';
    fm.all('GrpAgentName').value='';
    fm.diskimporttype.value='LACrossWage';
    fm2.diskimporttype.value='LACrossWage';  
    fm.BranchType.value=getBranchType();
    fm2.BranchType.value=getBranchType();
    fm2.BranchType2.value="02";
    //alert(getBranchType());
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
    alert("在LAGrpAgencySelCommissionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initInpBox1()
{
  try
  {
	  fm2.all('queryType').value ="";
	  fm2.all('queryName').value="";
	  fm2.all('FileImportNo').value="";
    //alert(fm2.BranchType.value);
    //alert(getBranchType());
  
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
	  alert("在LAGrpAgencySelCommissionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[1][4]="comcodejx";
    iArray[1][5]="1|2|3";
    iArray[1][6]="0|1|2";
    iArray[1][9]="管理机构|code:comcodejx&NUM&len=8";    
    iArray[1][15]="1";
    iArray[1][16]=tmanagecom;
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称"; //列名
    iArray[2][1]="100px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="管理机构"; //列名
    iArray[3][1]="0px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[4]=new Array();
	iArray[4][0]="业务员代码";          		//列名
	iArray[4][1]="100px";      	      		//列宽
	iArray[4][2]=20;            			//列最大值
    iArray[4][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[4][4]="grpinsagentcode";
    iArray[4][5]="4|5";              	                //引用代码对应第几列，'|'为分割符
	iArray[4][6]="0|1";
	iArray[4][9]="业务员代码|notnull";  
	iArray[4][15]="managecom"
	iArray[4][17]="3";
//	iArray[1][15]="1";
 //   iArray[1][16]=triskcode;
    
    iArray[5]=new Array();
    iArray[5][0]="业务员名称"; //列名
    iArray[5][1]="100px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    
    iArray[6]=new Array();
    iArray[6][0]="健代产佣金金额"; //列名
    iArray[6][1]="100px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[6][9]="健代产佣金金额|NotNull&NUM";
     
    iArray[7]=new Array();
    iArray[7][0]="健代寿佣金金额"; //列名
    iArray[7][1]="100px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[7][9]="健代寿佣金金额|NotNull&NUM";
     
    iArray[8]=new Array();
    iArray[8][0]="佣金年月"; //列名
    iArray[8][1]="100px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=1;              //是否允许输入,1表示允许,0表示不允许 
    
    iArray[9]=new Array();
    iArray[9][0]="操作人"; //列名
    iArray[9][1]="0px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许 
    
    
    iArray[10] =new Array();
    iArray[10][0]="管理机构";
    iArray[10][1]="0px";
    iArray[10][2]=100;
    iArray[10][3]=0;
    
    iArray[11] =new Array();
    iArray[11][0]="业务员代码";
    iArray[11][1]="0px";
    iArray[11][2]=100;
    iArray[11][3]=0;
    
    iArray[12] =new Array();
    iArray[12][0]="所属年月";
    iArray[12][1]="0px";
    iArray[12][2]=100;
    iArray[12][3]=0;
    
   
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
   // SetGrid.hiddenSubtraction =1;
   // SetGrid.hiddenPlus=0;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAGrpAgencySelCommissionInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initImportResultGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
    iArray[1][0]="导入文件批次";          		//列名
	iArray[1][1]="120px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
	iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="所在行"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="日志信息"; //列名
    iArray[3][1]="160px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
      
    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
    ImportResultGrid.canChk = 0;
		//SetGrid.mulLineCount = 10;
    //ImportResultGrid.displayTitle = 1;
    ImportResultGrid.hiddenSubtraction =1;
    ImportResultGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    ImportResultGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAGrpAgencySelCommissionInit.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    initImportResultGrid();
  }
  catch(re)
  {
    alert("在LAGrpAgencySelCommissionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>