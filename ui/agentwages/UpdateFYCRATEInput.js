   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
 
//提数操作
function FYCRATEUpdate()
{
	var i;
	var selFlag = true;
	var rowNum = FYCRATEGrid.mulLineCount;
	if(!selFlag) return selFlag;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.action="./UpdateFYCRATESave.jsp";
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在UpdateFYCRATEInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
        
//执行查询
function FYCRATEQuery()
{
  fm.target="fraSubmit";
	    //首先检验录入框
  if(!verifyForm("fm")) return false;
 
  // 拼SQL语句，从页面采集信息
  var strSql="select wageno,commisionsn,managecom,(select name from ldcom where comcode = managecom ),db2inst1.getunitecode(agentcode),(select name from laagent where agentcode = lacommision.agentcode)," +
  		"grpcontno,contno,riskcode,transmoney,fycrate,fyc,branchtype,branchtype2" +
  		" from lacommision  where 1=1"
  		      + getWherePart('BranchType','BranchType')
			  + getWherePart('BranchType2','BranchType2')
              +getWherePart('ManageCom','ManageCom')
              +getWherePart('ContNo','ContNo')
              +getWherePart('GrpContNo','GrpContNo')
              +getWherePart('WageNo','WageNo')
              +getWherePart('CommisionSN','CommisionSN')
              +getWherePart('db2inst1.getunitecode(agentcode)','GroupAgentCode')
              +" order by commisionsn,wageno ";
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
 //alert("**:"+strSql);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    FYCRATEGrid.clearData('FYCRATEGrid');  
    alert("查询失败！");
    return false;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = FYCRATEGrid;    
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}



