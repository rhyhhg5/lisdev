//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var arrDataSet;

function submitForm()
{  
   var sql =fm.strSQL.value;
	  if(sql==""||sql==null)
	  {
		  alert("请先查询");
		  return false;
	  }
	fm.action='./UnLockList.jsp';
	fm.submit();
}
//选择解锁
function unlock()
{ 
    var i;
	var iCount = 0;
	var rowNum = PolGrid.mulLineCount;
	for(i=0;i<rowNum;i++){
		if(PolGrid.getChkNo(i)){
			iCount++;
			if(PolGrid.getRowColData(i,11) =='已锁定')
			{    
				PolGrid.setFocus(i,11,PolGrid);
			}
			else{
			   alert("此数据未锁定,不需要进行解锁操作");
				PolGrid.setFocus(i,11,PolGrid);
				return false;
			}
		} 
	}
	if(iCount == 0){
		alert("请选择要解锁的记录!");
		return false
	}
	fm.all("workType").value = "UNLOCK";  
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./UnLockSave.jsp";
   fm.submit();
}
//全部解锁
function unlockALL()
{
  if(!check())
   return false ;
  fm.all("workType").value = "UNLOCKALL";  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./UnLockSave.jsp";
	fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {   
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");     
  }
  else
  { 
  	
	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");   
 	initForm();
  }
}

function addInfo(addFlag,error)
{
	if (addFlag=="Fail")
	{
		alert("向打印队列添加数据失败!原因是："+error);
	}else
	{
		alert("成功添加进打印队列!");
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function resetFm()
{
	fm.reset();
}

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{	
   if(!check())
    return false ;
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
    strSQL = "select  b.indexcalno,a.ManageCom,getUniteCode(b.agentcode),a.actugetno,a.otherno,(select codename from ldcode where codetype = 'paymode' and code = a.paymode ), "
        + "a.sumgetmoney,a.bankcode,a.bankaccno,a.accname, "
        + "case when a.cansendbank='1' then '已锁定' else '未锁定' end,"
        + "a.sendbankcount,(select d.codename from lyreturnfrombankb c, ldcode1 d where c.paycode=a.actugetno and d.codetype='bankerror' and d.code=c.bankcode and d.code1=c.banksuccflag and c.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.actugetno)),a.ShouldDate";
 strSQL = strSQL + " from ljaget a,lawage b where 1=1 and a.confdate is null and a.paymode='4' and a.othernotype='24' and a.agentcode=b.agentcode and b.branchtype='1' and b.branchtype2='01' and b.indexcalno='"+fm.all('Wageno').value+"' and b.indexcalno=substr(a.otherno,length(a.otherno)-5,6) and a.managecom=b.managecom "
 if(fm.all('Groupagentcode').value!=""&& fm.all('Groupagentcode').value!=null ){
        strSQL =strSQL +" and a.agentcode=getagentcode('"+fm.all('Groupagentcode').value+"')" 
 }
 if(fm.CanSendBank.value == '1')
 {
 	strSQL = strSQL + " and a.CanSendBank = '1'";
 }
 else if(fm.CanSendBank.value == '0')
 {
 	strSQL = strSQL + " and (a.CanSendBank = '0' Or a.CanSendBank is null)";
 }
  strSQL = strSQL + getWherePart( 'a.ManageCom','ManageCom',"like" ) + " Order by ShouldDate desc ";
  fm.strSQL.value = strSQL;
  
  //查询SQL，返回结果字符串
  //alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL); 
  //alert(easyQueryVer3(strSQL));
     
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
    //alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
    
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  PolGrid.SortPage=turnPage;  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);   
}
//查询之前先检查数据
function check()
{   
    if(fm.all("ManageCom").value==null||fm.all("ManageCom").value==''){
	  alert("管理机构为必填项，不能为空!");
	  return false;
	}
	if(fm.all("Wageno").value==null||fm.all("Wageno").value==''){
	  alert("薪资月为必填项，不能为空!");
	  return false;
	}
	return true;
}
