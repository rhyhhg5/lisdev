<%@include file="../common/jsp/UsrCheck.jsp"%>
 <html>  
<%
//程序名称：RepeatCalInput.jsp
//程序功能：
//创建日期：2003-07-22
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="RepeatCalInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="RepeatCalInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form method=post name=fm target="fraSubmit" action="./RepeatCalSave.jsp">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		薪资重算
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            员工编码
          </TD>
          <TD  class= input>
            <Input class="codeno" name=AgentCode 
            ondblclick="return showCodeList('AgentCode',[this,AgentCodeName], [0,1],null,acodeSql,'1',null);" 
            onkeyup="return showCodeListKey('AgentCode', [this,AgentCodeName], [0,1],null,acodeSql,'1',null);"><input class=codename name=AgentCodeName readonly=true >
          </TD>
          <TD  class= title>
          </TD>
          <TD  class= input>
           </TD>
        </TR>
        <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
		  <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>  
          <TD  class= title>
            展业类型
          </TD>
          <TD  class= input>
            <Input class="codeno" name=BranchType verify="展业类型|NOTNULL" ondblclick="showCodeList('branchtype',[this,BranchTypeName],[0,1],null,mcodeSql,'1',null);" onkeyup="return showCodeListKey('branchtype',[this,BranchTypeName],[0,1],null,mcodeSql,'1',null);" ><input class=codename name=BranchTypeName readonly=true >
          </TD>        
        </TR>
        <TR  class= common>
          <TD  class= title>
            佣金所属年
          </TD>
          <TD  class= input>
            <Input class=common  name=WageYear verify="佣金所属年|NOTNULL" >
          </TD>
          <TD  class= title>
            佣金所属月
          </TD>
          <TD  class= input>
            <Input class=common name=WageMonth verify="佣金所属月|NOTNULL" >
          </TD>
        </TR>
        <TR class=input>              
         <TD class=common>
          <input type =button class=common value="查询" onclick="RepeatCalQuery();">    
        </TD>
        <TD class=common>
          <input type =button class=common value="重算" onclick="RepeatCalSave();">    
        </TD>
       </TR>          
      </table>
    </Div>  
    <!--input type=hidden name=BranchType value=""-->
    <Div  id= "divAgentQuery" style= "display: 'none'">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span> 
  			</TD>
      </TR>
     </Table>	
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	     	<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
   </Div>	     
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


<script>
  var mcodeSql = "1 and (code = #2# or code = #3#)";
  var acodeSql = "1 and (BranchType=#2# or BranchType=#3#) and (AgentState = #01# or AgentState = #02#)";
</script>