   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var tFlag = "0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
//提数操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
//  showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在RepeatCalInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行提数         
function RepeatCalSave()
{
  if (tFlag == "0")
  {
  	alert("请先点击查询按钮，选择需要调整的员工！");
  	return;	
  }
  tFlag = "0";
  //首先检验录入框
  if(!verifyInput()) return false;
  divAgentQuery.style.display='none'; 
   if (fm.all('BranchType').value=='3'||fm.all('BranchType').value=='2')
	submitForm();	
   else
   	{
          alert("输入的展业机构类型有误，请重新输入！！！");
          return;	
   	}
}  
                  


//执行查询
function RepeatCalQuery()
{
	    //首先检验录入框
  if(!verifyInput()) return false;

 if(fm.all('WageYear').value==''||fm.all('WageMonth').value==''||fm.all('BranchType').value=='')
  {
    alert("请填写查询条件");
    return ;	
  }
 divAgentQuery.style.display='';

 initAgentQueryGrid();
 if (fm.all('BranchType').value=='3')
 	showBankRecord();
 else if (fm.all('BranchType').value=='2')
 	showCorpRecord();
 tFlag = "1";
}



//显示数据的函数，和easyQuery及MulLine 一起使用
function showBankRecord()
{
  // 拼SQL语句，从页面采集信息
  var tReturn = getManageComLimitlike("a.ManageCom");
  var Sql = fm.all('WageYear').value+fm.all('WageMonth').value;  
  var strSql = "select a.AgentCode,c.Name,b.branchattr,a.ManageCom,a.f23,a.f24,a.LastMoney,a.f25,a.ShouldMoney,a.k02,a.CurrMoney,a.summoney from LAWage a,labranchgroup b,laagent c " 
               +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode and c.BranchType='3' and a.IndexCalNo = '"+Sql
               +"' and (b.state<>'1' or b.state is null) and a.state='0' "
               + getWherePart('a.AgentCode','AgentCode')
               + tReturn 
               + getWherePart('a.ManageCom','ManageCom','like')
               +" order by a.AgentCode";

  //alert(strSql);	    
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}


//显示数据的函数，和easyQuery及MulLine 一起使用
function showCorpRecord()
{
  // 拼SQL语句，从页面采集信息
  var tReturn = getManageComLimitlike("a.ManageCom");

  var Sql ="select distinct WageNo from LAWageLog where 1=1 "
	           + getWherePart( 'WageYear' )
	           + getWherePart( 'WageMonth' );
  var strSql = "select a.AgentCode,c.Name,b.branchattr,a.ManageCom,a.f23,a.LastMoney,a.f25,a.ShouldMoney,a.k02,a.CurrMoney,a.summoney from LAWage a,labranchgroup b,laagent c " 
               +" where a.agentcode = c.agentcode and b.agentgroup = c.branchcode and c.BranchType='2' and IndexCalNo = ("+Sql+") and (b.state<>'1' or b.state is null)"
               + getWherePart('a.AgentCode','AgentCode')
               + tReturn 
               + getWherePart('a.ManageCom','ManageCom','like')
               +" order by a.AgentCode";

  //alert(strSql);	    
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}



/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	//alert(cCodeName);
	try	{
		if( cCodeName == "AgentCode" )	
		{
			checkvalid();//loadFlag在页面出始化的时候声明
		}
	}
	catch( ex ) {
	}
}


function checkvalid()
{
  var strSQL = "";
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select BranchType,ManageCom from LAAgent where 1=1 "
	     + getWherePart('AgentCode')+" and ((AgentState  not like '03') or (AgentState is null))";
     //alert(strSQL);
     var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('BranchType').value = "";
    fm.all('ManageCom').value = "";

    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此代理人！");
    fm.all('AgentCode').value="";
    fm.all('BranchType').value = "";
    fm.all('ManageCom').value = "";

    return;
  }
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  //<rem>######//
  fm.all('BranchType').value = tArr[0][0];
  fm.all('ManageCom').value = tArr[0][1];
  
}