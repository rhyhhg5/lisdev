<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpLARateCommQuery.jsp
//程序功能：
//创建日期：2003-07-08 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./GrpLARateCommQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./GrpLARateCommQueryInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>银代提奖比例信息 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPlan1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAPlan1" style= "display: ''">
    <table  class= common>
      <tr  class= common> 
        
        <td  class= title> 险种	</td>
        <td  class= input> <input name=RiskCode class="code" verify="险种|code:Riskcode" 
              ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);"> </td>
        <TD  class= title width="25%">展业类型</TD>
          <TD  class= input>
            <Input class=common name=BranchType verify="展业类型|NOTNULL" >
          </TD>
        </tr>
      
      <tr  class= common>  
        <td  class= title>投保年龄</td>
        <td  class= input> <input class=common name=AppAge verify="投保年龄|notnull"> </td>
        <td  class= title>保险年期</td>
        <td  class= input><input class=common name=Years verify="保险年期|notnull"> 	</td>
      </tr>
      <tr  class= common> 
        <td  class= title>交费间隔</td>
        <td  class= input><input class="code" name=PayIntv  verify="交费间隔|notnull" CodeData="0|^0|趸交^1|月交^12|年交^-1|不定期交" ondblclick="showCodeListEx('PayIntv',[this],[0]);"  onkeyup="showCodeListKeyEx('PayIntv',[this],[0]);"> 	</td>
        <td  class= title>提奖比例</td>
        <td  class= input><input name=Rate class= common verify = "手续费比率|notnull"> </td>
      </tr>
      <tr  class= common> 
        <td  class= title>保单年度</td>
        <td  class= input><input class=common name=CurYear verify="保单年度|notnull"> 	</td>
        <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom verify="管理机构|code:comcode&NOTNULL" 
            ondblclick="initEdor(this);"  onkeyup="actKey(this);"> 
          </TD>
      </tr>
      <tr  class= common> 
        <td  class= title>提奖类型</td>
        <TD  class= input><Input class="code" name=CalType verify="提奖类型|notnull"  CodeData="0|^01|组提奖计算^02|部提奖计算^09|银代个人提奖^10|团体提奖" ondblclick="showCodeListEx('CalType',[this],[0]);"  onkeyup="showCodeListKeyEx('CalType',[this],[0]);">
          </TD>
        <td  class= title>保单类型</td>
        <td  class= input><input class='code' name=PolType verify="计划类型|notnull"
		         CodeData="0|^0|优惠业务^1|正常业务" 
		         ondblclick="showCodeListEx('PolType',[this],[0]);"  onkeyup="showCodeListKeyEx('PolType',[this],[0]);"</td>
      </tr>
      <tr  class= common> 
        <td  class= title>众悦管理费水平上限</td>
        <TD  class= input><Input class=common name=F03 ></TD>
        <td  class= title>众悦管理费水平下限</td>
        <td  class= input><input class=common name=F04 ></td>
      </tr>
      <tr  class= common>         
        <td  class= title> 操作员代码</td>
        <td  class= input> <input name=Operator class= 'readonly'readonly > </td>		    
      </tr>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPlanGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPlanGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPlanGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 				
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
