<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-06-28 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LAWelfareRadixInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAWelfareRadixInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LAWelfareRadixSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <%@include file="../common/jsp/ManageComLimit.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAWelfareRadix1);">
    		</td>
    		 <td class= titleImg>
        		 福利基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLAWelfareRadix1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            地区编码
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom verify="地区编码|notnull&code:comcode"  
            ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" 
            onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
          </TD>
          <TD  class= title>
            员工职级
          </TD>
          <TD  class= input>
            <!--Input class= common name=AgentGrade -->
            <Input class="code" name=AgentGrade ondblclick="showCodeList('agentgrade',[this],null,null,codeSql,'1',null);" onkeyup="return showCodeListKey('agentgrade',[this],null,null,codeSql,'1',null);" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            福利类别
          </TD>
          <TD  class= input>
            <!--Input class= common name=AClass -->
            <Input class="code" name=AClass ondblclick="showCodeList('AClass',[this],null,null,tcodeSql,'1',null);" onkeyup="return showCodeListKey('AClass',[this],null,null,tcodeSql,'1',null);" >
          </TD>
          <TD  class= title>
            展业类型            
          </TD>
          <TD  class= input>
            <!--Input class='code' name=BranchType verify="展业机构类型|code:BranchType" ondblclick="return showCodeList('BranchType',[this]);" 
                                                            onkeyup="return showCodeListKey('BranchType',[this]);"-->
            <Input class= readonly name=BranchType readonly >	
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            提取基数
          </TD>
          <TD  class= input>
            <Input class= common name=DrawBase >
          </TD>
          <TD  class= title>
            提取比例            
          </TD>
          <TD  class= input>
            <Input class= common name=DrawRate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            提取金额
          </TD>
          <TD  class= input>
            <Input class= common name=DrawMoney >
          </TD>
          <TD  class= title>          
          </TD>
          <TD  class= input>
          </TD>
        </TR>
      </table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  var codeSql = "#1# and codetype=#agentgrade# and (code like #B%# or code like #C%# or code like #D%# or code like #E%#)";
  var tcodeSql = "#1# and othersign=#Y#";
</script>
