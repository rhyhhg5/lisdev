<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-06-28 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LABaseWageInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LABaseWageInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LABaseWageSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABaseWage1);">
    		</td>
    		 <td class= titleImg>
        		 基薪基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLABaseWage1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            员工编码
          </TD>
          <TD  class= input>
            <!--Input class= common name=AgentCode -->
            <Input class="codeno" name=AgentCode readonly verify="代理人编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this,AgentCodeName], [0,1],null,acodeSql,'1',null);" onkeyup="return showCodeListKey('AgentCode', [this,AgentCodeName], [0,1],null,acodeSql,'1',null);"><input class=codename name=AgentCodeName readonly=true >
          </TD>
          <TD  class= title>
            展业类型
          </TD>
          <TD  class= input>
            <Input class="readonly" name=BranchType readonly >
            <!--Input class='code' name=BranchType verify="展业机构类型|code:BranchType" ondblclick="return showCodeList('BranchType',[this]);" 
                                                            onkeyup="return showCodeListKey('BranchType',[this]);"-->
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            员工基薪
          </TD>
          <TD  class= input>
            <Input class= common name=BaseWage >
          </TD>
          <TD  class= title>            
          </TD>
          <TD  class= input>
          </TD>
        </TR>
      </table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  var acodeSql = "1 and (BranchType=#2# or BranchType=#3#) and (AgentState = #01# or AgentState = #02#)";
</script>
