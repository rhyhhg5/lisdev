//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
// 使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
// 提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    // initArchieveGrid();

    // showDiv(operateButton,"true");
    // showDiv(inputButton,"false");
    // 执行下一步操作
    var comstyle=fm.all('RadixType').value;
    if(comstyle=="WP1017")
    {
 	   divArchieveWP1017.style.display='none';
    }
    initForm();
    
  }
}
// 重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("LAWageRadix2Input.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
// 取消按钮对应操作
function cancelForm()
{
// window.location="../common/html/Blank.html";
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

// 提交前的校验、计算
function beforeSubmit()
{
if(!chkMulLine()) return false;
return true;

}
// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
// Click事件，当点击增加图片时触发该函数
function addClick()
{
  if(!beforeSubmit())
  {
    return false;
  }

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}
// 显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function easyQueryClick() {
  if (verifyInput() == false)
  return false;
  // initArchieveGrid();
  var radixtype=fm.all('RadixType').value;
  var branchtype=fm.all('BranchType').value;
  var branchtype2=fm.all('BranchType2').value;
  var wagecode;
  var strSql;
  strSql = " select a.agentgrade,b.gradename ,a.cond1 ,a.cond2,a.drawrate,a.idx "
		 + " from LAWAGERADIX2 a ,laagentgrade b "
		 + " where a.agentgrade=b.gradecode and a.branchtype='"+branchtype+"' and a.branchtype2='"+branchtype2+"'"
		 + " and wagecode='"+radixtype+"' "
//		 + " and managecom='"+fm.all('ManageCom').value+"'"
		 + " and areatype='"+fm.all('ComStyle').value+"'"
     + " order by a.agentgrade  with ur" ;
   var arrResult = new Array();
   arrResult = easyExecSql(strSql);
   if (!arrResult)
    {
    alert("没有符合条件的数据！");
    return false;
    }
   if(radixtype=="WP1017")
   {
	   // 查询成功则拆分字符串，返回二维数组
	   displayMultiline(arrResult,ArchieveWP1017Grid);
   }
}



// 提交，修改按钮对应操作
function DoSave()
{

	fm.fmtransact.value = "UPDATE";
	if (verifyInput() == false)
  return false;
 if((fm.all('ManageCom').value=="")||(  fm.all('ManageCom').value==null))
 {
 	alert("管理机构不能为空！");
 	return false;
 }
 if((fm.all('ComStyle').value=="")||(  fm.all('ComStyle').value==null))
 {
 	alert("地区类型不能为空！");
 	return false;
 }
 if((fm.all('RadixType').value=="")||(  fm.all('RadixType').value==null))
 {
 	alert("指标类型不能为空！");
 	return false;
 }

  if(!beforeSubmit())
  {
    return false;
  }
  // 下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); // 提交
  }
  else
  {
    alert("您取消了修改操作！");
  }

}

// 提交，保存按钮对应操作
function DoInsert()
{
	// alert(fm.all('ManageCom').value);
	 if((fm.all('ManageCom').value=="")||(  fm.all('ManageCom').value==null))
 {
 	alert("管理机构不能为空！");
 	return false;
 }
  fm.fmtransact.value = "INSERT" ;
  if (verifyInput() == false)
  return false;
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); // 提交

}

// 提交，修改按钮对应操作
function DoDelete()
{
  fm.fmtransact.value = "DELETE";
  // 下面增加相应的删除代码
  if(!beforeSubmit())
  {
    return false;
  }
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  fm.submit(); // 提交
  initForm();// ???????????????
  }
  else
  {
    alert("您取消了删除操作！");
  }

}

							// 判断是否选择了要增加、修改或删除的行
function chkMulLine()
{
	var i;
	var selFlag = true;
	var iCount = 0;
	var iCount1 = 0;
	var manageCom=fm.all('ManageCom').value;
	var comstyle=fm.all('ComStyle').value;
	var radixtype=fm.all('RadixType').value;
	if(radixtype=="WP1017")
    {
		return checkMullineWP1017();
    }
	return true;
}

function afterCodeSelect( cCodeName, Field ) {
  if( cCodeName == "radixtype"){
   var strValue  = Field.value;
   if(strValue=="WP1017")
   {
	   divArchieveWP1017.style.display='';
   }
  }
}
//岗位津贴每一条校验
function checkMullineWP1017()
{
	var i;
	var rowNum = ArchieveWP1017Grid.mulLineCount;
	var iCount =0;
	var selFlag = true;
	for(i=0;i<rowNum;i++)
	{
		if(ArchieveWP1017Grid.getChkNo(i))
		{
			iCount++;
			if((ArchieveWP1017Grid.getRowColData(i,1) == null)||(ArchieveWP1017Grid.getRowColData(i,1) == ""))
			{
				alert("第"+(i+1)+"行的职级代码不能为空");
				ArchieveWP1017Grid.setFocus(i,1,ArchieveWP1017Grid);
				selFlag = false;
				break;
			}
			if((ArchieveWP1017Grid.getRowColData(i,3) == null)||(ArchieveWP1017Grid.getRowColData(i,3) == ""))
			{
				alert("第"+(i+1)+"行的FYC上限不能为空");
				ArchieveWP1017Grid.setFocus(i,3,ArchieveWP1017Grid);
				selFlag = false;
				break;
			}
			if((ArchieveWP1017Grid.getRowColData(i,4) == null)||(ArchieveWP1017Grid.getRowColData(i,4) == ""))
			{
				alert("第"+(i+1)+"行的FYC上限不能为空");
				ArchieveWP1017Grid.setFocus(i,4,ArchieveWP1017Grid);
				selFlag = false;
				break;
			}
			if(ArchieveWP1017Grid.getRowColData(i,3)> ArchieveWP1017Grid.getRowColData(i,4))
			{
				alert("第"+(i+1)+"行的FYC上限不能大于FYC下限!");
				ArchieveWP1017Grid.setFocus(i,3,ArchieveWP1017Grid);
				selFlag = false;
				break;
			}
			if((ArchieveWP1017Grid.getRowColData(i,4) == null)||(ArchieveWP1017Grid.getRowColData(i,4) == ""))
			{
				alert("第"+(i+1)+"行的岗位津贴提取比例不能为空");
				ArchieveWP1017Grid.setFocus(i,4,ArchieveWP1017Grid);
				selFlag = false;
				break;
			}
			// 如果idx为空不能修改
			if((ArchieveWP1017Grid.getRowColData(i,6) == null)||(ArchieveWP1017Grid.getRowColData(i,6)=="") 
					|| (ArchieveWP1017Grid.getRowColData(i,6)==0))
			{
				if(fm.fmtransact.value == "UPDATE")
				{
					alert("有未保存的新增纪录！不可修改，请先保存记录。");
					ArchieveWP1017Grid.checkBoxAllNot();
					ArchieveWP1017Grid.setFocus(i,1,ArchieveWP1017Grid);
					selFlag = false;
					break;
				}
			}
			else
			{	// 如果idx不为空不能插入
				if(fm.fmtransact.value == "INSERT")
				{
					alert("序号"+(i+1)+"纪录已存在，不可保存！");
					ArchieveWP1017Grid.checkBoxAllNot();
					ArchieveWP1017Grid.setFocus(i,1,ArchieveWP1017Grid);
					selFlag = false;
					break;
				}
			}
		}
		else
		{//不是选中的行
			if((ArchieveWP1017Grid.getRowColData(i,6) == null)||(ArchieveWP1017Grid.getRowColData(i,6)==""))
				{
					alert("有未保存的新增纪录！请先保存记录。");
					ArchieveWP1017Grid.checkBoxAllNot();
					ArchieveWP1017Grid.setFocus(i,1,ArchieveWP1017Grid);
					selFlag = false;
					break;
				}
		} 
	}
	if(!selFlag) return selFlag;
    if(iCount == 0)
	{
    	var title="";
    	if(fm.fmAction.value=="UPDATE")
    	{
    		title="修改";
    	}
    	if(fm.fmAction.value=="DELETE")
    	{
    		title="删除";
    	}
    	if(fm.fmAction.value=="INSERT")
    	{
    		title="新增";
    	}
		alert("请选择要"+title+"的记录!");
		return false
	}
    
	return checkMullinesWP1017();
}

//岗位津贴选中条数的校验
function checkMullinesWP1017()
{
	var i,j;
	var rowNum = ArchieveWP1017Grid.mulLineCount;
	var selFlag = true;
	for(i=0;i<rowNum;i++)
	{
		if(ArchieveWP1017Grid.getChkNo(i))
		{
			var agentgrade = ArchieveWP1017Grid.getRowColData(i,1);
			var fyc1 = ArchieveWP1017Grid.getRowColData(i,3);
			var fyc2 = ArchieveWP1017Grid.getRowColData(i,4);
			for(j=i+1;j<rowNum;j++)
			{
				if(ArchieveWP1017Grid.getChkNo(j))
				{
					if(agentgrade == ArchieveWP1017Grid.getRowColData(j,1))
					{
						if(!(fyc1>=ArchieveWP1017Grid.getRowColData(j,4)||fyc2<=ArchieveWP1017Grid.getRowColData(j,3)))
						{
							alert("第"+(i+1)+"行与第"+(j+1)+"行"+agentgrade+"职级的fyc区间有重复");
							selFlag= false;
							break;
						}
					}
				}
			}
		}
	}
	if(!selFlag) return selFlag;
	return true;
}

