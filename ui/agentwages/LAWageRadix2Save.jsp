<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAWageRadix2Save.jsp
//程序功能：
//创建日期：2008-07-31 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentwages.*"%>


<%
  CErrors  tError = null;
  String tRela  = "";
  String Content = "";
  String FlagStr = "Fail";
  String transact = "";
  String tOperate ="INSERT||MAIN";
  String mTest ="";
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");

  LAWageRadix2Schema tLABaseWageSchema   = new LAWageRadix2Schema();
  LAWageRadix2Set tLAWageRadix2Set = new LAWageRadix2Set();
  LAWageRadix2Set tSetU = new LAWageRadix2Set();	//用于修改
  LAWageRadix2Set tSetI = new LAWageRadix2Set();	//用于保存 新增
  LAWageRadix2Set tSetD = new LAWageRadix2Set();	//用于保存 新增
  LAWageRadix2Set ttSetU = new LAWageRadix2Set();	//用于修改
  LAWageRadix2Set ttSetI = new LAWageRadix2Set();	//用于保存 新增
  LAWageRadix2Set ttSetD = new LAWageRadix2Set();	//用于保存 新增
  
  LAWageRadix2UI tLAWageRadix2UI= new LAWageRadix2UI();

  try
  {
  //接收信息，并作校验处理。
  //输入参数
  //执行动作：insert 添加纪录，update 修改纪录
  transact = request.getParameter("fmtransact");
  

  String tManageCom = request.getParameter("ManageCom");
  String tComStyle = request.getParameter("ComStyle");
  String tRadixType = request.getParameter("RadixType");
  String tRadixTypeName = request.getParameter("RadixTypeName");
  String tBranchType =  request.getParameter("BranchType");
  String tBranchType2 =  request.getParameter("BranchType2");
  
   	//获取最大的ID号
  ExeSQL tExe = new ExeSQL();
  String tSql = "select int(max(idx)) from lawageradix2 order by 1 desc ";
  String strIdx = "";
  int tMaxIdx = 0;

  strIdx = tExe.getOneValue(tSql);
  if (strIdx == null || strIdx.trim().equals(""))
  {
      tMaxIdx = 1;
  }
  else
  {
      tMaxIdx = Integer.parseInt(strIdx);
      System.out.println(tMaxIdx);
  }
  int lineCount = 0;
  String tChk[] = null;
  String[] tAgentGrade  = null;
  String[] tChannelType = null;
  String[] tCond1       = null;
  String[] tCond2       = null;
  String[] tDrawRate    = null;
  String[] tLimitPeriod = null;
  String[] tRewardMoney = null;
  String[] tDrawStart   = null;
  String[] tDrawEnd     = null;
  String[] tDrawRateOth = null;
  String tIdx[] = null;
  if(tRadixType.equals("WP1017"))
  {
	tChk = request.getParameterValues("InpArchieveWP1017GridChk");
	tAgentGrade = request.getParameterValues("ArchieveWP1017Grid1");
	tCond1 = request.getParameterValues("ArchieveWP1017Grid3");
	tCond2 = request.getParameterValues("ArchieveWP1017Grid4");
	tDrawRate = request.getParameterValues("ArchieveWP1017Grid5");
	tIdx = request.getParameterValues("ArchieveWP1017Grid6");
  }
   for(int i=0;i<tChk.length;i++)
  {
  	if(tChk[i].equals("1"))
  	{
  		System.out.println("savejsp"+i);
  		//创建一个新的Schema
  		LAWageRadix2Schema tSch = new LAWageRadix2Schema();
		if(null!=tAgentGrade&&tAgentGrade.length!=0)
		{
			tSch.setAgentGrade(tAgentGrade[i]);
		}
		else
		{
			tSch.setAgentGrade("AAA");
		}
		if(null!=tChannelType&&tChannelType.length!=0)
		{
			tSch.setChannelType(tChannelType[i]);
		}
		else
		{
			tSch.setChannelType("1");
		}
		if(null!=tCond1&&tCond1.length!=0)
		{
			tSch.setCond1(tCond1[i]);
		}
		else
		{
			tSch.setCond1("0");
		}
		if(null!=tCond2&&tCond2.length!=0)
		{
			tSch.setCond2(tCond2[i]);
		}
		else
		{
			tSch.setCond2("0");
		}
		if(null!=tDrawRate&&tDrawRate.length!=0)
		{
			tSch.setDrawRate(tDrawRate[i]);
		}
		else
		{
			tSch.setDrawRate("0");
		}
		if(null!=tLimitPeriod&&tLimitPeriod.length!=0)
		{
			tSch.setLimitPeriod(tLimitPeriod[i]);
		}
		else
		{
			tSch.setLimitPeriod("0");
		}
		if(null!=tRewardMoney&&tRewardMoney.length!=0)
		{
			tSch.setRewardMoney(tRewardMoney[i]);
		}
		else
		{
			tSch.setRewardMoney("0");
		}
		if(null!=tDrawStart&&tDrawStart.length!=0)
		{
			tSch.setDrawStart(tDrawStart[i]);
		}
		else
		{
			tSch.setDrawStart("0");
		}
		if(null!=tDrawEnd&&tDrawEnd.length!=0)
		{
			tSch.setDrawEnd(tDrawEnd[i]);
		}
		else
		{
			tSch.setDrawEnd("0");
		}
		if(null!=tDrawRateOth&&tDrawRateOth.length!=0)
		{
			tSch.setDrawRateOth(tDrawRateOth[i]);
		}
		else
		{
			tSch.setDrawRateOth("0");
		}
		tSch.setBranchType(tBranchType);
		tSch.setBranchType2(tBranchType2);
		//tSch.setManageCom(tManageCom);
		tSch.setAreaType(tComStyle);
      	tSch.setWageCode(tRadixType);
      	tSch.setWageName(tRadixTypeName);

  		if((tIdx[i] == null)||(tIdx[i].equals(""))||(tIdx[i].equals("0")))
  		{
  			//需要插入记录
			tMaxIdx++;
			tSch.setIdx(String.valueOf(tMaxIdx));
			tSetI.add(tSch);
   		}
        else if(transact.trim().equals("UPDATE"))
  		{
	  		tSch.setIdx(tIdx[i]);
	  		tSetU.add(tSch);
  		}
  		else if(transact.trim().equals("DELETE"))
  		{
	  		tSch.setIdx(tIdx[i]);
	  		tSetD.add(tSch);
  		}
  	}
  }
  
  
  
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  Content="";

  tVData.add(tGI);
  tVData.addElement(tRadixType);
  //没有更新或删除或插入的数据
  if ((tSetU.size() == 0)&&(tSetI.size() == 0)&&(tSetD.size() == 0)
  &&(ttSetU.size() == 0)&&(ttSetI.size() == 0)&&(ttSetD.size() == 0))
  {
    	FlagStr = "Fail";
    	Content = FlagStr + "未选中要处理的数据！";
  }
  else if (tSetI.size() != 0||ttSetI.size() != 0)
  {
        	//只有插入数据
        	tVData.add(tSetI);
        	tVData.add(ttSetI);
        	System.out.println("Start LAWageRadix2UI Submit...INSERT");
        	tLAWageRadix2UI.submitData(tVData,"INSERT");
  }
  else if (tSetU.size() != 0||ttSetU.size() != 0)
  {
        	//只有修改数据
        	tVData.add(tSetU);
        	tVData.add(ttSetU);
        	System.out.println("Start LAWageRadix2UI Submit...UPDATE");
        	tLAWageRadix2UI.submitData(tVData,"UPDATE");
   }
    else if (tSetD.size() != 0||ttSetD.size() != 0)
     {
        	//只有删除数据
        	tVData.add(tSetD);
        	tVData.add(ttSetD);
        	System.out.println("Start LAWageRadix2UI Submit...DELETE");
        	tLAWageRadix2UI.submitData(tVData,"DELETE");        	    	
     }

  }
  catch (Exception ex)
  {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
   }

   if(!FlagStr.equals("Fail"))
  {
    System.out.println("nihao");
    tError = tLAWageRadix2UI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
  <script language="javascript" >
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>


