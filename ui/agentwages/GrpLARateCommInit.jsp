<%
//程序名称：GrpLARateCommInit.jsp
//程序功能：
//创建人  ：销售管理
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG=(GlobalInput)session.getValue("GI");
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                               
    fm.all('F03').value = '';    
    fm.all('RiskCode').value = '';
    fm.all('F04').value = '';    
    fm.all('BranchType').value = getBranchType();   
    fm.all('AppAge').value = 0;
    fm.all('Years').value = 0;
    fm.all('PayIntv').value = '';
    fm.all('Rate').value = '';
    fm.all('CalType').value = '10';
    fm.all('PolType').value = '1';    
    fm.all('CurYear').value = '';
    fm.all('ManageCom').value = '';
    fm.all('Operator').value = '<%=tG.Operator%>' ;

  }
  catch(ex)
  {
    alert("在GrpLARateCommInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm()
{
  try
  {
    initInpBox();    
  }
  catch(re)
  {
    alert("在GrpLARateCommInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>