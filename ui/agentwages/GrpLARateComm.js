//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var sql="";
var sqlField="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function initEdorType(cObj)
{
	mEdorType = " #1# and branchtype="+fm.all('BranchType').value+"";
	showCodeList('riskbank',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and branchtype="+fm.all('BranchType').value+"";
	showCodeListKey('riskbank',[cObj], null, null, mEdorType, "1");
}


function initEdor(cObj)
{
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeList('comcode',[cObj], null, null, mEdorType, "1");
}

function actKey(cObj)
{	
	mEdorType = " #1# and length(trim(ComCode))=4 ";
	showCodeListKey('comcode',[cObj], null, null, mEdorType, "1");
}


//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value = mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);  
  fm.submit(); //提交  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在GrpLARateComm.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  if (!verifyInput()) 
    return false;  
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  mOperate = "INSERT||MAIN" ;
  //initForm();
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	beforeSubmit();
  //下面增加相应的代码  
   if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码  
  showInfo=window.open("./GrpLARateCommQueryMain.jsp");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{  
  //下面增加相应的删除代码
  beforeSubmit();
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
    initForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }  
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('F03').value = arrResult[0][11];                                        
		fm.all('RiskCode').value = arrResult[0][0];                                          
		fm.all('F04').value = arrResult[0][12];   
		fm.all('BranchType').value = arrResult[0][1];                                               
		fm.all('AppAge').value = arrResult[0][2];                                                  
		fm.all('Years').value = arrResult[0][3];
		fm.all('PayIntv').value = arrResult[0][4];    
		fm.all('Rate').value = arrResult[0][5];   
		fm.all('CalType').value = arrResult[0][6];    
		fm.all('PolType').value = arrResult[0][7];		    
		fm.all('CurYear').value = arrResult[0][9];    		                                                
		fm.all('Operator').value = arrResult[0][8]; 
		fm.all('ManageCom').value = arrResult[0][10]; 
		
    fm.all('F03B').value = arrResult[0][11];                                        
		fm.all('RiskCodeB').value = arrResult[0][0];                                          
		fm.all('F04B').value = arrResult[0][12];   
		fm.all('BranchTypeB').value = arrResult[0][1];                                               
		fm.all('AppAgeB').value = arrResult[0][2];                                                  
		fm.all('YearsB').value = arrResult[0][3];
		fm.all('PayIntvB').value = arrResult[0][4];    
		fm.all('RateB').value = arrResult[0][5];   
		fm.all('CalTypeB').value = arrResult[0][6];    
		fm.all('PolTypeB').value = arrResult[0][7];   		                                                
		fm.all('OperatorB').value = arrResult[0][8];		 
		fm.all('CurYearB').value = arrResult[0][9];
		fm.all('ManageComB').value = arrResult[0][10];                                                   		                                                                                                                                                    	
	}       
}               
