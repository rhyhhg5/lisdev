<%
//程序名称：LARateCommQueryInit.jsp
//程序功能：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
    fm.all('AgentCom').value = '';    
    fm.all('RiskCode').value = '';
    fm.all('ChannelType').value = '';
    fm.all('BranchType').value = top.opener.fm.all('BranchType').value;
    fm.all('AppAge').value = '';
    fm.all('Years').value = '';
    fm.all('PayIntv').value = '';
    fm.all('Rate').value = '';
    fm.all('CalType').value = '';
    fm.all('PolType').value = '';
    fm.all('Sex').value = '';
    fm.all('CurYear').value = '';
    fm.all('Operator').value = '' ; 
  }
  catch(ex)
  {
    alert("在LARateCommQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initPlanGrid();
  }
  catch(re)
  {
    alert("LARateCommQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化PlanGrid
 ************************************************************
 */
function initPlanGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="代理机构";         //列名
        iArray[1][1]="180px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="险种";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="渠道类型";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="展业类型";         //列名
        iArray[4][1]="80px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="投保年龄";         //列名
        iArray[5][1]="80px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="保险年期";         //列名
        iArray[6][1]="80px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[7]=new Array();
        iArray[7][0]="交费间隔";         //列名
        iArray[7][1]="80px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="手续费比率";         //列名
        iArray[8][1]="80px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="计算类型";         //列名
        iArray[9][1]="80px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[10]=new Array();
        iArray[10][0]="保单类型";         //列名
        iArray[10][1]="80px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[11]=new Array();
        iArray[11][0]="操作员代码";         //列名
        iArray[11][1]="80px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[12]=new Array();
        iArray[12][0]="性别";         //列名
        iArray[12][1]="80px";         //宽度
        iArray[12][2]=100;         //最大长度
        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[13]=new Array();
        iArray[13][0]="保单年度";         //列名
        iArray[13][1]="80px";         //宽度
        iArray[13][2]=100;         //最大长度
        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[14]=new Array();
        iArray[14][0]="管理机构";         //列名
        iArray[14][1]="80px";         //宽度
        iArray[14][2]=100;         //最大长度
        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
  
        PlanGrid = new MulLineEnter( "fm" , "PlanGrid" ); 

        //这些属性必须在loadMulLine前
        PlanGrid.mulLineCount = 0;   
        PlanGrid.displayTitle = 1;
        PlanGrid.locked=1;
        PlanGrid.canSel=1;
        PlanGrid.canChk=0;
        PlanGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化PlanGrid时出错："+ ex);
      }
    }


</script>