<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  tmanagecom=" 1 and  char(length(trim(comcode))) = #8# and comcode<>#86000000# ";
//var tmanagecom =" 1 and   char(length(trim(comcode)))<=#4# ";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
	
	fm.all('ManageCom').value ='';
	fm.all('ManageComName').value='';
	fm.all('StartYearMonth').value='';
    fm.all('EndYearMonth').value='';  
    fm.all('GrpAgentCode').value='';
    fm.all('GrpAgentName').value='';
    fm.diskimporttype.value='LACrossWage';
    fm2.diskimporttype.value='LACrossWage';  
    fm.BranchType.value=getBranchType();
    fm2.BranchType.value=getBranchType();
    fm2.BranchType2.value="01";
    //alert(getBranchType());
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
    alert("在LACrossSelCommissionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initInpBox1()
{
  try
  {
	  fm2.all('queryType').value ="";
	  fm2.all('queryName').value="";
	  fm2.all('FileImportNo').value="";
    //alert(fm2.BranchType.value);
    //alert(getBranchType());
  
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
	  alert("在LACrossSelCommissionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[1][4]="ComCode";
    iArray[1][5]="1|2";
    iArray[1][6]="0|1";
    iArray[1][9]="管理机构|code:comcode&NUM&len=8";    
    iArray[1][15]="1";
    iArray[1][16]=tmanagecom;
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称"; //列名
    iArray[2][1]="100px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    
    iArray[3]=new Array();
	iArray[3][0]="业务员代码";          		//列名
	iArray[3][1]="100px";      	      		//列宽
	iArray[3][2]=20;            			//列最大值
    iArray[3][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
//	iArray[3][5]="3|4";              	                //引用代码对应第几列，'|'为分割符
//	iArray[3][6]="0|1";
	iArray[3][7]="getAgentName"
	iArray[3][9]="业务员代码|notnull";  
//	iArray[1][15]="1";
 //   iArray[1][16]=triskcode;

    
    
    iArray[4]=new Array();
    iArray[4][0]="健代产佣金金额"; //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[4][9]="健代产佣金金额|NotNull&NUM";
     
    iArray[5]=new Array();
    iArray[5][0]="健代寿佣金金额"; //列名
    iArray[5][1]="100px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[5][9]="健代寿佣金金额|NotNull&NUM";
     
    iArray[6]=new Array();
    iArray[6][0]="佣金年月"; //列名
    iArray[6][1]="100px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=1;              //是否允许输入,1表示允许,0表示不允许 
    
    iArray[7]=new Array();
    iArray[7][0]="操作人"; //列名
    iArray[7][1]="0px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许 
    
    
    iArray[8] =new Array();
    iArray[8][0]="管理机构";
    iArray[8][1]="0px";
    iArray[8][2]=100;
    iArray[8][3]=0;
    
    iArray[9] =new Array();
    iArray[9][0]="业务员代码";
    iArray[9][1]="0px";
    iArray[9][2]=100;
    iArray[9][3]=0;
    
    iArray[10] =new Array();
    iArray[10][0]="所属年月";
    iArray[10][1]="0px";
    iArray[10][2]=100;
    iArray[10][3]=0;
    
   
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
   // SetGrid.hiddenSubtraction =1;
   // SetGrid.hiddenPlus=0;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LACrossSelCommissionInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initImportResultGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
    iArray[1][0]="导入文件批次";          		//列名
	iArray[1][1]="120px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
	iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="所在行"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="日志信息"; //列名
    iArray[3][1]="160px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
      
    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
    ImportResultGrid.canChk = 0;
		//SetGrid.mulLineCount = 10;
    //ImportResultGrid.displayTitle = 1;
    ImportResultGrid.hiddenSubtraction =1;
    ImportResultGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    ImportResultGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LACrossSelCommissionInit.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    initImportResultGrid();
  }
  catch(re)
  {
    alert("在LACommisionAwardRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>