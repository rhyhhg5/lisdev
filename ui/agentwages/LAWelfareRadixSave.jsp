<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAWelfareRadixInput.jsp
//程序功能：
//创建日期：2003-6-28
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentwages.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LAWelfareRadixSchema tLAWelfareRadixSchema   = new LAWelfareRadixSchema();

  LAWelfareRadixUI tLAWelfareRadixUI   = new LAWelfareRadixUI();
  
  GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("------transact:"+transact);


    tLAWelfareRadixSchema.setManageCom(request.getParameter("ManageCom"));
    tLAWelfareRadixSchema.setAgentGrade(request.getParameter("AgentGrade"));
    tLAWelfareRadixSchema.setAClass(request.getParameter("AClass"));
    tLAWelfareRadixSchema.setBranchType(request.getParameter("BranchType"));
    tLAWelfareRadixSchema.setDrawBase(request.getParameter("DrawBase"));    
    tLAWelfareRadixSchema.setDrawRate(request.getParameter("DrawRate"));
    tLAWelfareRadixSchema.setDrawMoney(request.getParameter("DrawMoney"));
		tLAWelfareRadixSchema.setOperator(tG.Operator);

  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
		tVData.addElement(tLAWelfareRadixSchema);
  
    tLAWelfareRadixUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLAWelfareRadixUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

