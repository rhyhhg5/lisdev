//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var tOrphanCode="";
var queryFlag= false;
window.onfocus=myonfocus;

 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,500,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}


//提交，保存按钮对应操作
function DoInsert()
{
	mOperate = "INSERT";
	fm.hideOperate.value=mOperate;
	if( verifyInput() == false ) return false;
	if(!chkMulLine()) return false;
//	chkMulLineData();
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
function DoSave()
{
	mOperate = "UPDATE";
	fm.hideOperate.value=mOperate;
	if( verifyInput() == false ) return false;
	if(!chkMulLine()) return false;
//	chkMulLineData();
	if(!chkPrimaryKey()) return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
//磁盘模板下载
function moduleDownload(){
	var tSQL="";
	fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "CrossSelCommisionModule.jsp";
    fm.submit();
    fm.action = oldAction;
}
//磁盘导入
function diskImport(){
	 
	    //alert("111111111111111");
			//alert("====="+fm.all("diskimporttype").value);
	   if(fm2.FileName.value==null||fm2.FileName.value==""){
	   	 alert("请选择要导入的文件！");
	   	 return false;
	   }
	   var showStr="正在导入数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	   fm2.submit();
}
//重置
function clearImport(){
	initInpBox1();
	document.getElementById('FileName').outerHTML=document.getElementById('FileName').outerHTML;
	initImportResultGrid();
}
//查询文件批次
function queryImportResult(){

	var tbatchno=fm2.FileImportNo.value;
	var tqueryType=fm2.queryType.value;
	if(tbatchno==null||tbatchno==""){
		alert("请输入查询文件批次号！");
		return false;
	}
	if(tqueryType==null||tqueryType==""){
		alert("请选择查询类型！");
		return false;
	}
	strSQL = "select batchno,contid,errorinfo"
	+ " from lcgrpimportlog"
	+ " where batchno='"+tbatchno+"' ";
	if(tqueryType==1){
		strSQL+=" and errorstate='1'";
	}else if(tqueryType==2){
		strSQL+=" and errorstate='0'";
	}
	strSQL+=" order by contid";
  //执行查询并返回结果
	turnPage2.queryModal(strSQL, ImportResultGrid);
	
}
//删除
function DoDel(){
	
	mOperate = "DELETE";
	fm.hideOperate.value=mOperate;
	if( verifyInput() == false ) return false;
	if(!chkMulLine()) return false;
//	chkMulLineData();
	if(!chkPrimaryKey()) return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
		queryFlag = false;
	}
	
}
//标准查询
function easyQueryClick()
{   
	if(fm.all('ManageCom').value==""||fm.all('ManageCom').value==null) 
	{
		alert("管理机构不能为空！");
		return false;
	}
	if(fm.all('StartYearMonth').value==""||fm.all('StartYearMonth').value==null) 
	{
		alert("佣金年月起期不能为空！");
		return false;
	}
	if(fm.all('EndYearMonth').value==""||fm.all('EndYearMonth').value==null) 
	{
		alert("佣金年月止期不能为空！");
		return false;
	}
		var tEndYearMonth = fm.all('EndYearMonth').value;
		var tStartYearMonth = fm.all('StartYearMonth').value;
		if(tEndYearMonth.length != 6||tStartYearMonth.length != 6){
		alert("佣金年月起止日期不符合格式！");
		return false;
		}
	
	var sql = "select ManageCom,(select name from ldcom where comcode=lacrosswage.managecom),ManageCom,getUniteCode(AgentCode),(select name from laagent where agentcode=lacrosswage.agentcode),PropertyFYC,LifeFYC,WageNo,operator" +
			", ManageCom,getUniteCode(AgentCode),WageNo " +
			" from lacrosswage where 1=1 and branchtype ='3' and branchtype2 = '01' " +
			" and wageno between '"+fm.all('StartYearMonth').value+"' and '"+fm.all('EndYearMonth').value+"' " 
			+getWherePart("managecom","ManageCom","like")
			+getWherePart("getUniteCode(agentcode)","GrpAgentCode");
	turnPage1.queryModal(sql, SetGrid);
	  if (!turnPage1.strQueryResult) {
	   alert("未查询到交叉销售佣金的信息！");
	   return false;
	  }
}

function DoReset(){
	initInpBox();
	initSetGrid();
}

//下载
function DoNewDownload(){

	 if(SetGrid.mulLineCount==0)
	 {
	    alert("列表中没有数据可下载");
	    return;
	 }
	 else {
	 // 书写SQL语句
		var stSQL = "";
		stSQL = "select managecom,(select name from ldcom where comcode =lacrosswage.managecom),getUniteCode(AgentCode),(select name from laagent where agentcode =lacrosswage.agentcode),PropertyFYC,LifeFYC,WageNo"
		+ " from LACrossWage "
		+ " where 1=1  and  wageno between '"+fm.all('StartYearMonth').value+"' and '"+fm.all('EndYearMonth').value+"' and branchtype ='3' and branchtype2='01'  "
		+getWherePart("managecom","ManageCom","like")
		+getWherePart("getUniteCode(agentcode)","GrpAgentCode");
	    fm.querySql.value = stSQL;
	    var oldAction = fm.action;
	    fm.action = "LAGrpCrossSelCommisionDownloadXLS.jsp";
	    fm.submit();
	    fm.action = oldAction;
	  }
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}
function chkMulLine()
{
	//alert("enter chkmulline");
	if(!SetGrid.checkValue("SetGrid")) return false;
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = SetGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(SetGrid.getChkNo(i))
		{
			iCount++;
			if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
			{
				alert("管理机构不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}

			if((SetGrid.getRowColData(i,4) == null)||(SetGrid.getRowColData(i,4) == ""))
			{
				alert("业务员不能为空");
				SetGrid.setFocus(i,4,SetGrid);
				selFlag = false;
				break;
			}
			var wageno = SetGrid.getRowColData(i,8);
			var managecom = SetGrid.getRowColData(i,1);
			if((wageno == null)||(wageno == ""))
			{
				alert("薪资月不能为空");
				SetGrid.setFocus(i,8,SetGrid);
				selFlag = false;
				break;
			}
			if(wageno.length!=6)
			{
				alert("输入的所属年月非法");
				SetGrid.setFocus(i,8,SetGrid);
				selFlag = false;
				break;
			}
			if(parseInt(wageno.substr(0,4))<=2014)
			{
				if (!confirm('输入的薪资月中年份为2015年之前，请确认您的操作'))
				{
					return false;
				}
//				SetGrid.setFocus(i,3,SetGrid);
//				selFlag = false;
//				break;
			}
			if(parseInt(wageno.substr(4,6),10)<1 ||parseInt(wageno.substr(4,6),10)>12 )
			{
				alert("所属年月中的月份为非法月份");
				SetGrid.setFocus(i,8,SetGrid);
				selFlag = false;
				break;
			}

//			if((SetGrid.getRowColData(i,7) == null)||(SetGrid.getRowColData(i,7) == ""))
//			{
//				alert("交叉销售佣金不能为空");
//				SetGrid.setFocus(i,7,SetGrid);
//				selFlag = false;
//				break;
//			}
  			//如果Serialno为空不能删除和更新
			if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9)==""))
			{
				if((fm.hideOperate.value == "DELETE") || (fm.hideOperate.value == "UPDATE"))
				{
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
			else if ((SetGrid.getRowColData(i,9) !== null)||(SetGrid.getRowColData(i,9)!==""))
			{	//如果Serialno不为空不能插入
				if(fm.hideOperate.value == "INSERT")
				{
					alert("此纪录已存在，不可插入！");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
		}
		else
		{//不是选中的行
			if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9)==""))
				{
					alert("有未保存的新增纪录！请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
		} 
		if(!selFlag) return selFlag;
	    if(iCount == 0)
		{
	    	var title="";
	    	if(fm.hideOperate.value=="UPDATE")
	    	{
	    		title="修改";
	    	}
	    	if(fm.hideOperate.value=="DELETE")
	    	{
	    		title="删除";
	    	}
	    	if(fm.hideOperate.value=="INSERT")
	    	{
	    		title="新增";
	    	}
			alert("请选择要"+title+"的记录!");
			return false
		}
		return true;
	
		
}
function chkPrimaryKey()
{
	var rowNum = SetGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(SetGrid.getChkNo(i))
		{
			if(SetGrid.getRowColData(i,1)!=SetGrid.getRowColData(i,10))
			{
				alert("不能修改管理机构！");
				SetGrid.setFocus(i,1,SetGrid);
				return false;
			}
			if(SetGrid.getRowColData(i,4)!=SetGrid.getRowColData(i,11))
			{
				alert("不能修改业务员！");
				SetGrid.setFocus(i,4,SetGrid);
				return false;
			}
			if(SetGrid.getRowColData(i,8)!=SetGrid.getRowColData(i,12))
			{
				alert("不能修改薪资月！");
				SetGrid.setFocus(i,8,SetGrid);
				return false;
			}
		}
	}
	return true;
}

