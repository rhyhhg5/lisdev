   //               该文件中包含客户端需要处理的函数和事件
//程序名称：UpdateStateInput.js
//程序功能：Input.js
//创建日期：2017-10-19
//创建人  ：yangjian
var showInfo;
var mDebug="0";

try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
 
//修改操作
function UpdateStateUpdate()
{
	var i;
	var selFlag = true;
	
	var iCount = 0;
	
	var rowNum = UpdateStateGrid.mulLineCount;
	if(!selFlag) return selFlag;
	
	
	for(i=0;i<rowNum;i++)
	{
		if(UpdateStateGrid.getChkNo(i))
		{
			iCount++;
			var newState = UpdateStateGrid.getRowColData(i,5);
			if((newState == null)||(newState == ""))
			{
				alert("第"+iCount+"行计算状态（改）不能为空");
				UpdateStateGrid.setFocus(i,5,UpdateStateGrid);
				selFlag = false;
				break;
			}
			
			
			if((newState >14)||(newState<11))
			{
				alert("行计算状态（改）应在11-14范围中");
				UpdateStateGrid.setFocus(i,5,UpdateStateGrid);
				selFlag = false;
				break;
			}
			
			
			
		}
	}
	if(!selFlag) return selFlag;
	if(iCount == 0)
	{
		alert("请选择要修改计算状态的记录!");
		return false
	}
	
	
  var i = 0;
  var showStr="正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.action="./UpdateStateSave.jsp";
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close(); 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
} 
//执行查询
function UpdateStateQuery()
{
  fm.target="fraSubmit";
  //首先检验录入框
  if(!verifyForm("fm")) return false;
  // 拼SQL语句，从页面采集信息
  var strSQL = "select ManageCom,(select Name from LDCom where ComCode=lawagehistory.ManageCom),WageNo,State,''" +
  		",BranchType,(select CodeName from ldcode where codetype = 'branchtype' and Code=lawagehistory.BranchType)," +
  		"BranchType2,(select CodeName from ldcode where codetype = 'branchtype2' and Code=lawagehistory.BranchType2) " +
  		"from lawagehistory where 1=1 "    
		+ getWherePart('ManageCom','ManageCom','like')
		+ getWherePart('WageNo','WageNo')
		+ getWherePart('State','State')
		+ getWherePart('BranchType','BranchType')
		+ getWherePart('BranchType2','BranchType2')
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    UpdateStateGrid.clearData('UpdateStateGrid');  
    alert("查询失败！");
    return false;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = UpdateStateGrid;    
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}

