//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet; 
var turnPage = new turnPageClass();

//修改人：解青青  时间：2014-11-24
function checkAgentCode()
{
    var sql = "select getagentcode('" + fm.GroupAgentCode.value + "') from dual ";
    var rs = easyExecSql(sql);
    if(rs != null && rs.length > 0){
    	fm.all('AgentCode').value  = rs[0][0];
    }
    else{
    	fm.all('AgentCode').value  = "";  //若录入代理人编码错误，则置vv，避免把原来的代理人做薪资确认
    }
}


//提交，保存按钮对应操作
function submitForm()
{
//	if(!verifyInput()) 
//  { return; }
	if (fm.all('ManageCom').value=='')
	{
		alert("管理机构不能为空!");
		return false;
	}
	if (fm.all('WageYear').value=='')
	{
		alert("薪资所属年不能为空!");
		return false;
	}
	if (fm.all('WageMonth').value=='')
	{
		alert("薪资所属月不能为空!");
		return false;
	} 
	
  if(!beforeSubmit()) return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 // alert(fm.all('ManageCom').value);  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

//  initPolGrid();
//  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LABranchGroupQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
//	var sql = "select getagentcode('" + fm.GroupAgentCode.value + "') from dual ";
//    var rs = easyExecSql(sql);
//    if(rs != null && rs.length > 0){
//    	fm.all('AgentCode').value  = rs[0][0];
//    }
//    else{
//    	fm.all('AgentCode').value  = ""; 
//    }
    
    //若录入集团工号，但没有成功转换成内部工号，则报错。转换详见checkAgentCode()
//	if(fm.all('AgentCode').value  == ""
//		&& fm.GroupAgentCode.value != ""){
//		alert("您录入的代理人不存在");
//		return false;
//	}
//	
  //添加操作	
//   var tAgentGroup =fm.all('AgentGroup').value;
   //alert("easyQueryClick1");
   // 书写SQL语句
   var strSQL = "";
   var tReturn = getManageComLimitlike("a.ManageCom");
   var tWageNo = fm.WageYear.value + fm.WageMonth.value;
   strSQL = "select getunitecode(a.agentcode) from LAWage a,labranchgroup b,laagent c where 1=1 and a.agentcode=c.agentcode and a.agentgroup = b.agentgroup and a.state='0'  and (b.state<>'1' or b.state is null)"
             +" and indexcalno = '"+tWageNo+"' "
	        + getWherePart('a.branchtype','BranchType')
//            + getWherePart('b.branchattr','AgentGroup')
//           + getWherePart('a.indexcalno','IndexCalNo')
            + getWherePart('a.BranchType2','BranchType2')           
            + getWherePart('a.managecom','ManageCom')
//            + getWherePart('c.name','name')
//            + getWherePart('c.GroupAgentCode','GroupAgentCode');	
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);   
   //判断是否查询成功
   if (!turnPage.strQueryResult)
   {
     alert("本月没有进行薪资计算或者已经进行审核发放，不需要审核发放！");
     return false;
   }	
   return true;	
    
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,500,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
/*
function returnParent()
{
  var arrReturn = new Array();
	var tSel = BranchGroupGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = BranchGroupGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");	
	arrSelected = new Array();
	tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	//arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

*/

// 查询按钮
function easyQueryClick()
{
//	if(!verifyInput()) 
//  { return; }
	// 初始化表格
	initWageGrid();
//	var tAgentGroup =fm.all('AgentGroup').value;
	//alert("easyQueryClick1");
	// 书写SQL语句
	var strSQL = "";
//    var tReturn = getManageComLimitlike("a.ManageCom");
//        //zyy 修改
//     var BranchType=fm.all("BranchType").value;
//     var BranchType2=fm.all("BranchType2").value;
//	
//if(BranchType=='2'&&BranchType2=='01'){
//strSQL = "select a.indexcalno,getUniteCode(a.agentcode),c.name,a.managecom,b.branchattr, a.summoney,case when a.state='0' then '未发放' when a.state='1' then '已发放' else '财务已确认' end" 
//	
//}else{
//	strSQL += "select a.indexcalno,getUniteCode(a.agentcode),c.name,a.managecom,b.branchattr, a.summoney,case when a.state='0' then '未发放' when a.state='1' then '已发放' else '财务已确认' end, a.agentcode "
//}
	if (fm.all('ManageCom1').value=='')
	{
		alert("管理机构不能为空!");
		return false;
	}
	if (fm.all('AgentType').value=='')
	{
		alert("销售人员类型不能为空!");
		return false;
	}  
	if (fm.all('WageYear1').value=='')
	{
		alert("薪资所属年不能为空!");
		return false;
	}
	if (fm.all('WageMonth1').value=='')
	{
		alert("薪资所属月不能为空!");
		return false;
	} 
	 divWage.style.display='';
	 initWageGrid();
	  var Year = fm.all('WageYear1').value;
	  var Month = fm.all('WageMonth1').value;
	  var AgentCode = fm.all('GroupAgentCode').value;
	  var AgentName = fm.all('AgentName').value;
	  var AgentType = fm.all('AgentType').value;
	  var tbranchtype = fm.BranchType.value;        
	  var tbranchtype2 = fm.BranchType2.value; 
	  var strSql = "select * from (select a.indexcalno,a.managecom,b.branchattr,b.name,getUniteCode(a.AgentCode) ord,c.Name,"
          +"(select gradename from laagentgrade where gradecode=a.agentgrade),"
          +" case when c.agenttype='1' then '营销人员' when c.agenttype = '2' then '直销人员' else '人员类型' end,c.employdate,"
          +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
          +" c.outworkdate"
	      +",(select cond1 from LAWageRadix2 where branchtype =a.branchtype and branchtype2=a.branchtype2 and wagecode ='YD001' and agentgrade=d.agentgrade and areatype = (select wageflag from LABankIndexRadix " +
  	          " where branchtype ='3' and branchtype2='01' and managecom=trim(substr(a.managecom,1,4))))"
	      +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
	      +",a.F07,a.F09 "
	      if(AgentType=='1'){
	    	  strSql += ",a.F11,a.F08,a.F13," ;
          }
          if(AgentType=='2')
          {
        	  strSql += ",a.F20,a.F21," ;
          }
          strSql += "a.F30,a.K12,a.F16,a.F17,a.F14,a.F15,a.F18,a.F19,a.F06,a.CurrMoney "
          +" from LAWage a,labranchgroup b,laagent c ,latree d"
          //+",labasewage e " 
          +" where a.agentcode = c.agentcode and b.agentgroup = a.agentgroup "
          +" and c.agentgroup=b.agentgroup "
          +" and c.agentcode=d.agentcode  and a.branchtype='3' "
          +" and a.state = '1'"              
          //+" and d.agentgrade=e.agentgrade"
          //+" and a.managecom=e.managecom and e.type='01' and e.branchtype='3'"
          +" and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null)"
          +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
          +" and c.agenttype = '"+AgentType+"'"
          +" and a.branchtype = '" + trim(tbranchtype)
		  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
          +getWherePart('c.groupagentcode','GroupAgentCode')
          +getWherePart('c.name','AgentName')
           //+tReturn
          //+" order by a.AgentCode "
//          +"union"
//          +" select a.indexcalno,a.managecom,b.branchattr,b.name,getUniteCode(a.AgentCode) ord,c.Name,"
//          +"(select gradename from laagentgrade where gradecode=a.agentgrade),"
//          +" case when c.agenttype='1' then '营销人员' when c.agenttype = '2' then '直销人员' else '人员类型' end,c.employdate,"
//          +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
//          +" c.outworkdate" 
//	      +",(select wagemoney from labankstandradix where wagetype = (select wageflag from LABankIndexRadix where managecom = a.managecom ) and agentgrade = d.agentgrade and agenttype = c.agenttype)"
//	      +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
//          +",a.F07,(a.F11+a.F08+a.F13),(F09+F30+F16+F18-K12-F17-F19),a.CurrMoney,a.CurrMoney"
//          +" from LAWage a,labranchgroup b,laagent c ,latree d "
//          +"where a.agentcode = c.agentcode and b.agentgroup = (select agentgroup from labranchchangetemp where agentcode =a.agentcode and cvalimonth <='"+Year+Month+"'  and cvaliflag='1' order by cvalimonth  desc  fetch first 1 rows only )"
//          +"and a.agentcode=d.agentcode and a.agentcode = c.agentcode and  a.agentgroup = b.agentgroup "
//          +"and a.branchtype='3'  and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"'"
//          +"and (b.state<>'1' or b.state is null)"
//          +" and a.state = '1'"              
//          +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
//          +" and c.agenttype = '"+AgentType+"'"
//          +" and a.branchtype = '" + trim(tbranchtype)
//		  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
//          +getWherePart('c.groupagentcode','GroupAgentCode')
//          +getWherePart('c.name','AgentName')
          +") kk order by ord  with ur"; 
 
	turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败，原因是：本月没有进行薪资确认或者该机构未有人员参与薪资计算！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = WageGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex   = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}




function WageDownLoad()
{
  var tManageCom  = fm.all('ManageCom').value;
  
 if(WageGrid.mulLineCount==0)
 {
    alert("列表中没有数据可下载");
    return;
 }
 else {
 // 书写SQL语句
   // 拼SQL语句，从页面采集信息
   
   var tReturn = getManageComLimitlike("a.ManageCom");
   var Year = fm.all('WageYear1').value;
   var Month = fm.all('WageMonth1').value;
   var AgentCode = fm.all('GroupAgentCode').value;
   var AgentName = fm.all('AgentName').value;
   var AgentType = fm.all('AgentType').value;
   var tbranchtype = fm.BranchType.value;        
   var tbranchtype2 = fm.BranchType2.value; 
  /*
  var checkSql = "select state from lawage where branchtype = '3' and branchtype2 = '01' and managecom like '"+ManageCom+"%' and indexcalno ='"+Year+Month+"' ";
  var strQueryResult  = easyQueryVer3(checkSql, 1, 1, 1);
  var arr = decodeEasyQueryResult(strQueryResult);
  if(arr[0][0]!="1"){
     alert("本月薪资计算未确认,不能下载当月银代薪资报表!");
     return false;
  }
  */
	  var strSql = "select * from (select a.indexcalno,a.managecom,b.branchattr,b.name,getUniteCode(a.AgentCode) ord,c.Name,"
          +"(select gradename from laagentgrade where gradecode=a.agentgrade),"
          +" case when c.agenttype='1' then '营销人员' when c.agenttype = '2' then '直销人员' else '人员类型' end,c.employdate,"
          +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
          +" c.outworkdate" 
	      +",(select cond1 from LAWageRadix2 where branchtype =a.branchtype and branchtype2=a.branchtype2 and wagecode ='YD001' and agentgrade=d.agentgrade and areatype = (select wageflag from LABankIndexRadix " +
  	          " where branchtype ='3' and branchtype2='01' and managecom=trim(substr(a.managecom,1,4))))"
	      +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
          +",a.F07,a.F09"
          if(AgentType=='1'){
        	  strSql += ",a.F11,a.F08,a.F13," ;
          }
          if(AgentType=='2')
          {
        	  strSql += ",a.F20,a.F21," ;
          }
          strSql += "a.F30,a.K12,a.F16,a.F17,a.F14,a.F15,a.F18,a.F19,a.F06,a.CurrMoney "
          +" from LAWage a,labranchgroup b,laagent c ,latree d"
          //+",labasewage e " 
          +" where a.agentcode = c.agentcode "
          +" and c.agentgroup=b.agentgroup "
          +" and c.agentcode=d.agentcode  and a.branchtype='3' "
          +" and a.state = '1'"              
          //+" and d.agentgrade=e.agentgrade"
          //+" and a.managecom=e.managecom and e.type='01' and e.branchtype='3'"
          +" and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null)"
          +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
          +" and c.agenttype = '"+AgentType+"'"
          +" and a.branchtype = '" + trim(tbranchtype)
		  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
          +getWherePart('c.groupagentcode','GroupAgentCode')
          +getWherePart('c.name','AgentName')
           //+tReturn
          //+" order by a.AgentCode "
          +"union"
//          +" select a.indexcalno,a.managecom,b.branchattr,b.name,getUniteCode(a.AgentCode) ord,c.Name,"
//          +"(select gradename from laagentgrade where gradecode=a.agentgrade),"
//          +" case when c.agenttype='1' then '营销人员' when c.agenttype = '2' then '直销人员' else '人员类型' end,c.employdate,"
//          +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
//          +" c.outworkdate" 
//	      +",(select wagemoney from labankstandradix where wagetype = (select wageflag from LABankIndexRadix where managecom = a.managecom ) and agentgrade = d.agentgrade and agenttype = c.agenttype)"
//	      +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
//	      +",a.F07,(a.F07+a.F09+a.F30-a.K12),(a.F11+a.F08+a.F16-a.F17),a.F13,(a.F14-a.F15),(a.F18-a.F19),a.CurrMoney,a.CurrMoney"
//          +" from LAWage a,labranchgroup b,laagent c ,latree d "
//          +"where a.agentcode = c.agentcode and b.agentgroup = (select agentgroup from labranchchangetemp where agentcode =a.agentcode and cvalimonth <='"+Year+Month+"'  and cvaliflag='1' order by cvalimonth  desc  fetch first 1 rows only )"
//          +"and a.agentcode=d.agentcode and a.agentcode = c.agentcode and  a.agentgroup = b.agentgroup "
//          +"and a.branchtype='3'  and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"'"
//          +"and (b.state<>'1' or b.state is null)"
//          +" and a.state = '1'"              
//          +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
//          +" and c.agenttype = '"+AgentType+"'"
//          +" and a.branchtype = '" + trim(tbranchtype)
//		  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
//          +getWherePart('c.groupagentcode','GroupAgentCode')
//          +getWherePart('c.name','AgentName')
          +" select '合计','','','','','','','',NULL,'',NULL,0,NULL"
//          +",(sum(value(a.F07,0))+sum(value(a.F09,0))+sum(value(a.F30,0))-sum(value(a.K12,0)))" 
//          +",(sum(value(a.F11,0))+sum(value(a.F08,0))+sum(value(a.F16,0))-sum(value(a.F17,0))),sum(value(a.F13,0))" 
//          +",(sum(value(a.F14,0))-sum(value(a.F15,0))),(sum(value(a.F18,0))-sum(value(a.F19,0)))" 
//          +",sum(value(a.CurrMoney,0)),sum(value(a.CurrMoney,0))"
          +",value(sum(a.F07),0),value(sum(a.F09),0),"
          if(AgentType=='2')
          {
        	  strSql+=" value(sum(a.F20),0),value(sum(a.F21),0)," 
          }
          if(AgentType=='1')
          {
        	  strSql+=" value(sum(a.F11),0),value(sum(a.F08),0),value(sum(a.F13),0),"
          }
          strSql+=" value(sum(a.F30),0),value(sum(a.K12),0),value(sum(a.F16),0),value(sum(a.F17),0),value(sum(a.F14),0), "
          +" sum(a.F15),value(sum(a.F18),0),value(sum(a.F19),0),value(sum(a.F06),0),value(sum(a.CurrMoney),0) "
          +" from LAWage a,labranchgroup b,laagent c ,latree d"
          +" where a.agentcode = c.agentcode "
          +" and c.agentgroup=b.agentgroup "
          +" and c.agentcode=d.agentcode  and a.branchtype='3' "
          +" and a.state = '1'"              
          +" and c.BranchType='3' and a.IndexCalNo = '"+Year+Month+"' and (b.state<>'1' or b.state is null)"
          +" and a.ManageCom like '"+fm.all('ManageCom1').value+"%' "
          +" and c.agenttype = '"+AgentType+"'"
          +" and a.branchtype = '" + trim(tbranchtype)
		  + "' and  a.branchtype2 = '"+ trim(tbranchtype2) + "'"
          +getWherePart('c.groupagentcode','GroupAgentCode')
          +getWherePart('c.name','AgentName')
          +") kk order by ord desc with ur"; 
    fm.querySql.value = strSql;
    var oldAction = fm.action;
    fm.action = "LABankWageConfirmXLS.jsp";
    fm.submit();
    fm.action = oldAction;
  }
}