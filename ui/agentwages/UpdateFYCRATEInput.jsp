<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
 <html>  
<%
//程序名称：UpdateFYCRATEInput.jsp
//程序功能：
//创建日期：2017-04-26
//创建人  ：zyy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="UpdateFYCRATEInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UpdateFYCRATEInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >    
  <form action="./UpdateFYCRATESave.jsp"  method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		提奖比例维护
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input 
            class ='codename' name ='ManageComName' elementtype=nacessary>
          </TD>          
          <TD  class= title >
            薪资月
          </TD>
           <TD  class= input>
            <Input class=common name=WageNo >
          </TD>
          <TD  class= title >
            业务员代码
          </TD>
           <TD  class= input>
            <Input class=common name=GroupAgentCode >
          </TD>
          </tr>
          <tr class= common>
          <TD  class= title>
            团单号
          </TD>
          <TD  class= input>
            <Input class=common name=GrpContNo >
          </TD>
          <TD  class= title>
            个单号
          </TD>
          <TD  class= input>
            <Input class=common  name=ContNo  >
          </TD>
          <TD  class= title>
            主键commisionsn
          </TD>
          <TD  class= input>
            <Input class=common  name=CommisionSN  >
          </TD>
          <td></td>
        </TR>
         <TR  class= common>			
		<TD class=title>展业类型</TD>
		<TD class=input><Input class='codeno' name=BranchType
		    verify="展业类型|notnull"
		    ondblclick="return showCodeList('branchtype',[this,BranchTypeName],[0,1]);"
			onkeyup="return showCodeListKey('branchtype',[this,BranchTypeName],[0,1]);"
			readonly><Input class=codename name=BranchTypeName elementtype=nacessary>
	   </TD>
	    <TD class=title>销售渠道</TD>
		<TD class=input><Input class='codeno' name=BranchType2
		    verify="销售渠道|notnull"
		    ondblclick="return showCodeList('branchtype2',[this,BranchType2Name],[0,1]);"
			onkeyup="return showCodeListKey('branchtype2',[this,BranchType2Name],[0,1]);"
			readonly><Input class=codename name=BranchType2Name elementtype=nacessary>
	   </TD>
	   </TR>
        <TR class=input>     
         <TD class=common>
          <input type =button class=cssbutton value="查 询" onclick="FYCRATEQuery();">    
          <input type =button class=cssbutton value="修 改" onclick="FYCRATEUpdate();">
          </td>
       </TR>          
      </table>
    </Div>  
    <Div  id= "divAgentQuery" style= "display: ''">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanFYCRATEGrid" ></span> 
  			</TD>
      </TR>
     </Table>	
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	     	<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
   </Div>	
  </form>   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
<script>
  var mcodeSql = "#1# and (code = #2# or code = #3#)";
</script>