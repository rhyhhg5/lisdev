
<%
	//程序名称：CreatePersonInit.jsp
	//程序功能：
	//创建日期：2018-04-12 
	//创建人  ：zxs
	//
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
%>

<script language="JavaScript">
	function getQueryResult(tRow) {
		var turnPage = new turnPageClass();
		var arrSelected = null;
		arrSelected = new Array();
		// 书写SQL语句
		var strSQL = "";
		strSQL = "select Name,Sex,Birthday,Idtype,idno,Makedate,Maketime,Remark,serialno from newpeopletasktable a where a.serialno ='"
				+ tRow + "'";
		turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
		//判断是否查询成功
		if (!turnPage.strQueryResult) {
			alert("查询失败！");
			return false;
		}
		//查询成功则拆分字符串，返回二维数组
		arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);

		return arrSelected;
	}

	function initData() {
		try {
			var data = new Array();
			var no = <%=request.getParameter("no")%>;
	        var mOperate =<%=request.getParameter("hideOperate")%>;
			//alert(mOperate);
			fm.all('no').value = no;
			if (mOperate == 'insert') {
				fm.all('hideOperate').value = 'INSERT||MAIN';
			} else if (mOperate == 'update') {
				fm.all('hideOperate').value = 'UPDATE||MAIN';
			}
			if (no != null) {
				data = getQueryResult(no);
				//alert(data);
				fm.all('name').value = data[0][0];
				if (data[0][1] == 0) {
					fm.all('sexTypeName').value = '男';
				} else {
					fm.all('sexTypeName').value = '女';
				}
				fm.all('sexType').value = data[0][1];
				fm.all('birthday').value = data[0][2];
				fm.all('RgtType').value = data[0][3];
				if (data[0][3] == 1) {
					fm.all('idtype').value = '身份证';
				} else if (data[0][3] == 2) {
					fm.all('idtype').value = '户口本';
				} else {
					fm.all('idtype').value = '其他';
				}
				fm.all('idno').value = data[0][4];
				fm.all('comment').value = data[0][7];
				fm.all('makeDate').value = data[0][5];
				fm.all('makeTime').value = data[0][6];
			}
		} catch (ex) {
			alert("在CreatePersonInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
		}
	}

	function initForm() {
		try {
			//initInpBox();
			initData();
		} catch (re) {
			alert("在CreatePersonInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
</script>