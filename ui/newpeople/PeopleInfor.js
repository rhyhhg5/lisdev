//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var mSwitch = parent.VD.gVSwitch;
var mOperate;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
try{
	var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
}
catch(ex)
{}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";  
	}
}

function submitForm()
{
	if(!verifyInput()) 
	{ return; }	
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
	//showSubmitFrame(mDebug);
	fm.submit(); //提交
}

//新建人员
function createNewPerson(){
	//window.open("./CreatePersonInput.jsp?hideOperate='insert'",'_blank');
	showInfo = window.open("./PeopleInforHtml.jsp?hideOperate='insert'",'_blank');
}

//修改人员信息
function updatePerson(){
	checkLine();

}

//查看选中哪一行
function checkLine(){
	var flag = -1;
	var count = PersonGrid.mulLineCount;
	for(var i=0;i<count;i++){
		if(PersonGrid.getChkNo(i)){
			flag = i ;
			break;
		}
	}
	if(flag!=-1){
		//var s = PersonGrid.getRowColDataByName(flag,"serialno"); 
		//alert(s);
		var data = new Array();
		var personData = PersonGrid.getRowData(flag);
		alert(PersonGrid.getRowColData(flag,7));
		if(personData[personData.length-1]!=null){
			//window.open("./CreatePersonInput.jsp?no='"+personData[personData.length-1]+"'&hideOperate='update'",'_blank');
			showInfo = window.open("./PeopleInforHtml.jsp?no='"+personData[personData.length-1]+"'&hideOperate='update'",'_blank');
		}	
	}else{
		alert("请选中记录进行修改");
	}
}

//校验字段信息
function beforeSumbit(){
	if(fm.name.value == null || fm.name.value == "" || fm.name.value == "null"){
		alert("姓名不可以为空！");
		fm.name.focus();
		return false;
	}
	if(fm.sexTypeName.value == null || fm.sexTypeName.value == "" || fm.sexTypeName.value == "null"){
		alert("性别不可以为空！");
		fm.sexTypeName.focus();
		return false;
	}

	if(fm.birthday.value == null || fm.birthday.value == "" || fm.birthday.value == "null"){
		alert("出生日期不可以为空！");
		fm.birthday.focus();
		return false;
	}

	if(fm.idtype.value == null || fm.idtype.value == "" || fm.idtype.value == "null"){
		alert("证件类型不可以为空！");
		fm.idtype.focus();
		return false;
	}
	if(fm.idno.value == null || fm.idno.value == "" || fm.idno.value == "null"){
		alert("证件号码不可以为空！");
		fm.idno.focus();
		return false;
	}
	return true;
}

/*
//保存人员
function saveClick(){
	if (verifyInput() == false) {
		return false;
	}
	if(!beforeSumbit()){
		return false;
	}
	  mOperate = fm.hideOperate.value;
	  if(mOperate=='INSERT||MAIN'){
		fm.submit();
	   }else{
		 alert("请规范操作数据！");
	   }
		if (fm.hideOperate.value==""){
			alert("操作控制数据丢失！");
		}	
}

//修改人员
function updateClick(){
	if (verifyInput() == false) {
		return false;
	}
	if(!beforeSumbit()){
		return false;
	}
	mOperate = fm.hideOperate.value;
	if(mOperate=='UPDATE||MAIN'){
		 if (confirm("您确实想修改该记录吗?"))
		    {
			 fm.submit();
		    }
		    else
		    {
		      alert("您取消了修改操作！");
		    }
	}else{
		alert("先选择要修改的记录！");
	}
		if (fm.hideOperate.value==""){
			alert("操作控制数据丢失！");
		}


}

//删除人员
function deleteClick(){
	if (verifyInput() == false) {
		return false;
	}
	if(!beforeSumbit()){
		return false;
	}	
	mOperate = fm.hideOperate.value;
	if(mOperate=='UPDATE||MAIN'){
		fm.hideOperate.value = 'DELETE||MAIN';
		mOperate=='DELETE||MAIN';
		fm.submit();
	}else{
		alert("先选择要删除的记录！");
	}
		if (fm.hideOperate.value==""){
			alert("操作控制数据丢失！");
		}	
}
 */
//查询功能
function easyQueryClick(){		
	if(!verifyInput()) 
	{ return; }	
	// 初始化表格
	initPersonGrid();	

	var name  = fm.name.value;
	var sexType = fm.sexType.value;
	var birthday = fm.birthday.value;
	var RgtType = fm.RgtType.value;
	var idno = fm.idno.value;
	var createdate = fm.createdate.value;

	// 书写SQL语句
	var strSQL = ""; 
	strSQL = "select Name,Sex,Birthday,Idtype,idno,Makedate,serialno from newpeopletasktable a where a.name like'"+name+"%'" 
	+"and a.Sex like'"+sexType+"%'"
	+"and a.Birthday like'"+birthday+"%'"
	+"and a.Idtype like'"+RgtType+"%'"
	+"and a.idno like'"+idno+"%'"
	+"and a.Makedate like'"+createdate+"%'";
	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}

	//查询成功则拆分字符串，返回二维数组  
	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);

	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = PersonGrid;    

	//保存SQL语句
	turnPage.strQuerySql     = strSQL; 

	//设置查询起始位置
	turnPage.pageIndex = 0;  

	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	arrGrid = arrDataSet;
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

}
/*
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
 */
//执行确认         
function WageConfirm()
{   

	submitForm();	   
}  

//显示frmSubmit框架，用来调试
/*function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}*/