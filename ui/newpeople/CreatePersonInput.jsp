<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="CreatePersonInit.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="CreatePeron.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<title>个人信息录入</title>
</head>
<script>
</script>
<body onload="initForm();initElementtype();">
	<%
String data=request.getParameter("data");
//System.out.println("data="+data);
%>
	<form action="./PeopleInforSave.jsp" method=post name=fm
		target="fraSubmit">
		<table>
			<TR>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,personInput);">
				</td>
				<td class=titleImg>个人信息</td>
			</TR>
		</table>
		<table class=common id="personInput" style="display: ''">
			<tr class=common>
				<td class=title>姓名</td>
				<td class=input><input class=common type=text name=name
					elementtype=nacessary verify="姓名|notnull"></td>
				<TD class=title8>性别</TD>
				<TD class=input8><input class="codeno" CodeData="0|^0|男^1|女"
					verify="性别|notnull" name=sexType
					ondblclick="return showCodeListEx('sexType',[this,sexTypeName],[0,1],null,null,null,1);"
					onkeydown="QueryOnKeyDown();"
					onkeyup="return showCodeListKeyEx('sexType',[this,sexTypeName],[0,1]);"><input
					class=codename name=sexTypeName elementtype=nacessary></TD>

				<td class=title8>出生日期</td>
				<td class=input><Input class="coolDatePicker" name=birthday
					elementtype=nacessary verify="出生日期|notnull"></td>

			</tr>
			<!-- elementtype=nacessary -->
			<tr class=common>
				<TD class=title8>证件类型</TD>
				<TD class=input8><input class="codeno" verify="证件类型|notnull"
					CodeData="0|3^1|身份证^2|户口本^3|其他" name=RgtType
					ondblclick="return showCodeListEx('RgtType',[this,idtype],[0,1],null,null,null,1);"
					onkeydown="QueryOnKeyDown();"
					onkeyup="return showCodeListKeyEx('RgtType',[this,idtype],[0,1]);"><input
					class=codename name=idtype elementtype=nacessary></TD>
				<td class=title>证件号码</td>
				<td class=input><input verify="证件号码|notnull"
					elementtype=nacessary class=common type=text name=idno></td>
			</tr>

			<tr class=common>
				<TD class=title8>备注</TD>
				<TD class=input8><textarea cols="" rows="3" id="comment"
						name="comment"></textarea></TD>
			</tr>
		</table>
		<table class=common>
			<tr class=common>
				<input type=button value="保存" class=cssButton onclick="saveClick()">
				</td>
				<input type=button value="修改" class=cssButton
					onclick="updateClick()">
				</td>
				<input type=button value="删除" class=cssButton
					onclick="deleteClick()">
				</td>
			</tr>
		</table>
		<Input type=hidden name=hideOperate value=''> <Input
			type=hidden name=no value=''> <Input type=hidden
			name=makeDate value=''> <Input type=hidden name=makeTime
			value=''>
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>

</body>
</html>