<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//返回按钮初始化
var str = "";
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('BankCode').value = '';
    fm.all('ZoneNo').value = '';
    fm.all('BankNode').value = '';
    fm.all('StartTransDate').value = '';
    fm.all('EndTransDate').value = '';
    fm.all('ManageCom').value = '';
    
  }
  catch(ex)
  {
    alert("在xbtBalanceQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在xbtBalanceQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initxbtPolGrid();
	
	
  }
  catch(re)
  {
    alert("xbtBalanceQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initxbtPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="银行名称";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[2]=new Array();
      iArray[2][0]="地区编码";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      
      iArray[3]=new Array();
      iArray[3][0]="银行网点";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="对帐日期";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="对账结果";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;  
      

      
      iArray[6]=new Array();
      iArray[6][0]="分公司名称";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;  
      
      xbtPolGrid = new MulLineEnter( "fm" , "xbtPolGrid" ); 
      //这些属性必须在loadMulLine前
      xbtPolGrid.mulLineCount = 10;   
      xbtPolGrid.displayTitle = 1;
      xbtPolGrid.locked = 1;
      xbtPolGrid.canSel = 1;
      xbtPolGrid.hiddenPlus = 1;
      xbtPolGrid.hiddenSubtraction = 1;
      xbtPolGrid.loadMulLine(iArray); 
      
      
      //这些操作必须在loadMulLine后面
      //divLCPol1.setRowColData(7,7,"","divLCPol1"); 
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>     