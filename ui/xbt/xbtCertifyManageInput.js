var mOperate="";
var showInfo;

window.onfocus=myonfocus;
var turnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}

function resetForm()
{
  try {
    initForm();
  } catch(re) {
    alert("在LAComInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//Click事件，当点击增加图片时触发该函数
function addClick() {
	fm.updateSubmit.disabled=true;


	
	if (verifyInput() == false) return false;
	

			if (('86'!=fm.ManageCom.value) //不是总公司(86)
			&& (4!=fm.ManageCom.value.length && 6!=fm.ManageCom.value.length && 8!=fm.ManageCom.value.length)) {	//也不是分公司(86xx)
			alert("单证对应的管理机构有误！");
			return false;
		}
		
		/**
		 * 存在单证类型定义记录，提示报错。
		 */
		var tSQLStr = "select * from LKCertifyMapping where 1=1 "
			+ "and bankcode='" + fm.BankCode.value + "' "
			+ "and RiskWrapCode='" + fm.RiskWrapCode.value + "' "
			+ "and managecom='" + fm.ManageCom.value + "' ";
		var tSQLResultStr  = easyQueryVer3(tSQLStr, 1, 1, 1);
		if (tSQLResultStr) {
			alert("此管理机构和险种下的单证类型定义信息已经存在！");
			return false;
		}
	

    //判断加入的主键是否已经存在
    turnPage.queryAllRecordCount=0;
    queryClick('flag')
    //alert(turnPage.queryAllRecordCount)
    if(turnPage.queryAllRecordCount>0) {
        alert('该网点已经存在!添加失败.')
        return;
    }
   
 //if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
  //下面增加相应的代码
 
  fm.hideOperate.value="INSERT||MAIN";
  
  
  if (!confirm("您确实添加改该记录吗?"))return;
  //alert();
  submitForm();

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,tBankNode)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    if(fm.hideOperate.value=="DEL||MAIN")content="删除成功!";
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
          //parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   // if(fm.hideOperate.value.length>0&&fm.hideOperate.value!="DEL||MAIN")
    queryClick();//提交后自动查询
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}

function delClick()
{fm.updateSubmit.disabled=true;
 if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
  //下面增加相应的代码
   fm.hideOperate.value="DEL||MAIN";
  //alert(mOperate);
  if (!confirm("您确实删除改该记录吗?"))return;
   var rowArr=null;
    n=checkedRowNo("LKCodeGridSel");
    //alert('n '+n)
    if(n!=-1)
    {
      rowArr=LKCodeGrid.getRowData(n);
      if(rowArr!=null)
      {
        fm.hideBankCode.value=rowArr[0];
        fm.hideManageCom.value=rowArr[1];
        fm.hideRiskWrapCode.value=rowArr[2];
//        //fm.Operator.value=rowArr[3];
//        fm.UpAgentCom.value=rowArr[3];
//        fm.ComCode.value=rowArr[4];
//        fm.ManageCom.value=rowArr[5];
//        fm.Remark.value=rowArr[6];
        //BankCode=rowArr[0];
      }
    }
  submitForm();

}


function updateSubmitForm()
{ if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
        

    

    //下面增加相应的代码
    if (verifyInput() == false) return false;

      
    if (confirm("您确实想修改该记录吗?"))
    {
      fm.hideOperate.value="UPDATE||MAIN";
      //alert("aaaa");
      submitForm();
      fm.updateSubmit.disabled=true;
    }
    else
    {
      fm.hideOperate.value="";
      alert("您取消了修改操作！");
      return;
    }

}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{//alert('.value.value.value')
  
   if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
   
  var rowArr=null;
    n=checkedRowNo("LKCodeGridSel");
    //alert('n '+n)
    if(n==-1)
    {
      alert('请先选中一条记录!');
      return ;
    }
    fm.updateSubmit.disabled=false;
    if(n!=-1)
    {
      rowArr=LKCodeGrid.getRowData(n);
      if(rowArr!=null)
      {

        fm.BankCode.value=rowArr[0];
        fm.ManageCom.value=rowArr[1];
        fm.RiskWrapCode.value=rowArr[2];
        //fm.Operator.value=rowArr[3];
        fm.CertifyCode.value=rowArr[3];
        fm.CertifyClass.value=rowArr[4];
       


        //修改前主键值
        fm.hideBankCode.value=rowArr[0];
        fm.hideManageCom.value=rowArr[1];
        fm.hideRiskWrapCode.value=rowArr[2];
      }
    }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick(flag)
{fm.updateSubmit.disabled=true;
 initLKCodeGrid();
 var strSQL = "";
 var strOperate="like";
 strSQL = "select BankCode,ManageCom,RiskWrapCode,CertifyCode,CertifyClass from LKCertifyMapping  where 1 =1 "
 

         
         strSQL+=  getWherePart( 'BankCode' )
        strSQL+=  getWherePart( 'ManageCom' )
        strSQL+=  getWherePart( 'RiskWrapCode' )

        if(flag==null)
        {
        strSQL+=  getWherePart( 'CertifyCode' )
        strSQL+= getWherePart( 'CertifyClass' )
 
        }
        strSQL+=' order by BankCode,ManageCom,RiskWrapCode ';

//updateClickalert("11111");
turnPage.pageLineNum=20;
turnPage.queryModal(strSQL, LKCodeGrid);

        return true;
}

//提交前的校验、计算
function beforeSubmit()
{
        return true;
}

//提交，保存按钮对应操作
function submitForm()
{
 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交
  //initForm();
}

function showDiv(cDiv,cShow)
{
  if(cShow=="true")
  {
   cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}





//返回选中的行号,这对radio button
function checkedRowNo(name)
{
  //var isOneMore=false;
  obj=document.all[name];

  if(typeof(obj.length)!='undefined')
  {
      for(i=0;i<obj.length;i++)
      {
        if(obj[i].checked)return i;
       //alert(isOneMore)
      }
  }
  else
  {
    if(obj.checked)return 0;
  }


  return -1;
}



