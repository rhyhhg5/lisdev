<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：xbtFeeRateManageInput.jsp
//程序功能：信保通费率配置
//创建日期：2010-04-25
//创建人  ：yinjj程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<head>
<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
<script src="../common/javascript/Common.js"></script>
<script src="../common/cvar/CCodeOperate.js"></script>
<script src="../common/javascript/MulLine.js"></script>
<script src="../common/javascript/VerifyInput.js"></script>
<script src="xbtFeeRateManageInput.js"></script>
<link href="../common/css/Project.css" rel=stylesheet type=text/css>
<link href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="xbtFeeRateManageInputInit.jsp"%>
<title></title>
</head>
<body onload="initForm();initElementtype();">
	<form action="./xbtFeeRateManageSave.jsp" method=post name=fm target="fraSubmit">
		<span id="operateButton">
			<table class="common" align=center>
				<tr align=right>
					<td class=button>&nbsp;&nbsp;</td>
					<td class=button width="10%">
						<INPUT class=cssButton VALUE="查  询" name="query" TYPE=button onclick="return queryClick();">
					</td>
					<td class=button width="10%">
						<INPUT class=cssButton VALUE="增  加" name="add" TYPE=button onclick="return addClick();">
					</td>
					<td class=button width="20%">
						<INPUT class=cssButton VALUE="修  改" name="update" TYPE=button onclick="return updateClick();">
						<INPUT class=cssButton VALUE="保  存" name="updateSubmit" disabled="disabled" TYPE=button onclick="return updateSubmitForm();">
					</td>
					<td class=button width="10%">
						<INPUT class=cssButton VALUE="删  除" name="del" TYPE=button onclick="return delClick();">
					</td>
					<td class=button width="10%">
						<INPUT class=cssButton VALUE="重  置" TYPE=button onclick="return resetForm();">
					</td>
				</tr>
			</table>
		</span>
		<table>
			<tr class=common>
				<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLACom1);">
				</td>
				<td class=titleImg>信保通费率&nbsp;&nbsp;&nbsp;</td>
			</tr>
		</table>

	<Div id="divLACom1" style="display: ''">
		<table class=common>
			<tr class=common>
				<td class=title>费率标志</td>
				<td class=input>
					<input class='codeno'  verify="费率标志|NotNull" name=CommonFeeRateFlag CodeData="0|^1|分公司默认费率^0|中介机构费率" ondblclick="return showCodeListEx('FeeRateFlag',[this,FeeRateFlag],[0,1]);" onkeyup="return showCodeListKeyEx('FeeRateFlag',[this,FeeRateFlag],[0,1]);"  ><input class=codename name=FeeRateFlag readonly=true elementtype=nacessary>
				</td> 
				<td class=title>套餐编码</td>
				<td class=input>
					<input class=common elementtype=nacessary verify="套餐编码|NotNull" name=RiskWrapCode>
				</td>
			</tr>
			<tr class=common>
				<td class=title>管理机构(4位机构)</td>
				<td class=input >
					<!--  <input readonly name="ManageCom" value="<%=globalInput.ManageCom%>" class="readonly"/> -->
					<% 
					if("86".equals(globalInput.ManageCom)){
					%>
						<input class=common name='ManageCom'  elementtype=nacessary/>
					<%}else{
						%>
						<input readonly name="ManageCom" value="<%=globalInput.ManageCom.substring(0,4)%>" class="readonly"/>
						
					<%
					}
					%>
				</td>
				<td class=title>银行代码</td>
				<td class=input>
					<input class=codeno  verify="银行代码|NotNull" name=BankCode ondblclick="return showCodeList('banknum',[this,BankCodeName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('ybtbank',[this,BankCodeName],[0,1],null,null,null,1,null,1);">
        	<input class=codename  name=BankCodeName readonly=true elementtype=nacessary>
				</td>
			</tr>
		</table> 
	</Div>

	<Div id="divLACom2" style="display: ''" class="readonly">
		<table class=common>
			<tr class=common>
				<td class=title>费率标志</td>
				<td class=input>
					<input readonly name="CommonFeeRateFlag2" value="" class="readonly"/>
				</td> 
				<td class=title>套餐编码</td>
				<td class=input>
					<input readonly name="RiskWrapCode2" value="" class="readonly"/>
				</td>
			</tr>
			<tr class=common>
				<td class=title>管理机构(4位机构)</td>
				<td class=input >
					<input readonly name="ManageCom2" value="" class="readonly"/>
				</td>
				<td class=title>银行代码</td>
				<td class=input >
					<input readonly name="BankCode2" value="" class="readonly"/>
				</td>
			</tr>
		</table> 
	</Div>

	<Div id="divState" style="display: 'none'" align=left> 
		<table class=common>
			<tr class=common>
				<td class=title>可用状态</td>
				<td class=input>
					<input class='codeno'  name=AvailableState CodeData="0|^0|禁用^1|启用" ondblclick="return showCodeListEx('stateType',[this,stateType],[0,1]);" onkeyup="return showCodeListKeyEx('stateType',[this,stateType],[0,1]);"  ><input class=codename name=stateType readonly=true elementtype=nacessary>  
				</td>
				<td class=title></td>
				<td class=input>
</td>		      
			</tr>
		</table>
	</Div>  

	<Div id="divState2" style="display: ''" align=left> 
		<table class=common>
		<tr class=common>		
			<td class=title>可用状态</td>
			<td class=input>
				<input readonly name="AvailableState2" value="" class="readonly"/>
			</td>
			<td class=title></td>
			<td class=input></td>		      
		</tr>
		</table>
	</Div>  

	<Div id="divAgent" style="display: 'none'" align=left> 
		<table class=common>
			<tr class=common>
				<td class=title>中介机构</td>
				<td class=input>
					<input class="common" name=AgentCom elementtype=nacessary >
				</td>
				<td class=title>中介机构名称</td>
				<td class=input>
					<input class="common" name=AgentComName>
				</td>
			</tr>
		</table>
	</Div>

	<Div id="divAgent2" style="display: 'none'" align=left> 
		<table class=common>
			<tr class=common>
				<td class=title>中介机构</td>
				<td class=input>
					<input readonly name="AgentCom2" value="" class="readonly"/>  
				</td>
				<td class=title>中介机构名称</td>
				<td class=input>
					<input readonly name="AgentComName2" value="" class="readonly"/>
				</td>
	
			</tr>

		</table>
	</Div>

	<Div id="divFee" style="display: 'none'" align=left> 
		<table class=common>
			<tr class=common>
				<td class=title>费率(每万元保费)</td>
				<td class=input>
					<input class=codename name=FeeRate  elementtype=nacessary>
				</td>
				<td class=title>费率类型</td>
				<td class=input>
					<input class='codeno'  verify="费率类型|NotNull" name=FeeRateType CodeData="0|^M|月费率 ^Y|年费率 ^L|阶梯费率" ondblclick="return showCodeListEx('FeeRateTypeName',[this,FeeRateTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeRateTypeName',[this,FeeRateTypeName],[0,1]);"  ><input class=codename name=FeeRateTypeName readonly=true elementtype=nacessary>  
				</td>
			</tr>
		</table>
	</Div>

	<Div id="divFeeRateYear" style="display: 'none'" align=left> 
		<table class=common>
			<tr class=common>
				<td class=title>费率期间(1-100之间的整数)</td>
				<td class=input>
					<input class="codename" name=FeeRateYear  elementtype=nacessary >
				</td>
				<td class=title></td>
				<td class=input></td>	
			</tr>
		</table>
	</Div>

	<input type=hidden name=hideOperate value=''>
	<input type=hidden name=hideType value=''>
	<input type=hidden name=FeeRateYearFlag value=''>
	<input type=hidden name=ProtocolNo value=''>
	<input type=hidden name=hideFeeRate value=''>
	<input type=hidden name=hideFeeRateType value=''>
	<input type=hidden name=hideFeeRateYear value=''>

	
	<table>
		<tr>
			<td class=common>
				<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLACom);">
			</td>
			<td class=titleImg>信保通费率信息</td>
		</tr>
	</table>

	<div id="divLACom" style="display: ''">
		<table class=common>
			<tr class=common>
				<td text-align: left colSpan=1>
					<span id="spanLKFeeRateDetail"> </span>
				</td>
			</tr>
		</table>
	</div>

	<Div id="divPage" align=center style="display: '' ">
		<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
		<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
 </form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>