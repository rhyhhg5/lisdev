<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
  //程序名称：xbtNodeManageSave.jsp
  //程序功能：
  //创建日期：2005-08-29
  //创建人  ：liuyx
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@ page import="org.apache.log4j.Logger"%>
<%@page import="com.sinosoft.utility.TransferData"%>
<%@page import="com.sinosoft.lis.schema.LKCodeMappingSchema"%>
<%@page import="com.sinosoft.midplat.kernel.management.NodeManageUI"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
Logger cLogger = Logger.getLogger("midplat.xbtNodeManageSave_jsp");
cLogger.info("into xbtNodeManageSave.jsp...");

  //接收信息，并作校验处理。输入参数
  LKCodeMappingSchema tLKCodeMappingSchema = new LKCodeMappingSchema();
  //输出参数
  String tOperate = request.getParameter("hideOperate").trim();
  GlobalInput tG = (GlobalInput) session.getValue("GI");
  String BankCode = request.getParameter("BankCode");
  String ZoneNo = request.getParameter("ZoneNo");
  String BankNode = request.getParameter("BankNode");
  //fm.Operator.value=rowArr[3];
  String UpAgentCom = request.getParameter("UpAgentCom");
  String ComCode = request.getParameter("ComCode");
  String ManageCom = request.getParameter("ManageCom");
 String Remark = request.getParameter("Remark");
 String Operator = request.getParameter("Operator");
  String Remark5 = request.getParameter("Remark5");
  if(Remark5.equals("1"))
  {
  ManageCom = request.getParameter("ManageCom1");
  }
  System.out.println("********"+ManageCom);
  String Remark1 = request.getParameter("Remark1");
   String Remark2 = "01";
  String Remark3 = request.getParameter("Remark3");
  String FTPAddress = request.getParameter("FTPAddress");
  String FTPDir = request.getParameter("FTPDir");
  String FTPUser = request.getParameter("FTPUser");
  String FTPPassWord = request.getParameter("FTPPassWord");
  //修改前的主键值
  String hideBankCode = request.getParameter("hideBankCode");
  String hideZoneNo = request.getParameter("hideZoneNo");
  String hideBankNode = request.getParameter("hideBankNode");
  if (BankCode == null) BankCode = "";
  if (ZoneNo == null) ZoneNo = ""; //ZoneNo ;
  if (BankNode == null) BankNode = ""; //BankNode ;
  if (UpAgentCom == null) UpAgentCom = ""; //UpAgentCom ;
  if (ComCode == null) ComCode = ""; //ComCode ;
  if (ManageCom == null) ManageCom = ""; //ManageCom ;
  if (Remark == null) Remark = ""; //fRemark ;
  if (Operator == null) Operator = ""; //Operator ;
  if (Remark1 == null) Remark1 = ""; //Remark1 ;
  if(Remark5 == null) Remark5 = "";
  if (Remark3 == null) Remark3 = ""; //Remark1 ;
  if (FTPAddress == null) FTPAddress = "";
  if (FTPDir == null) FTPDir = "";
  if (FTPUser == null) FTPUser = "";
  if (FTPPassWord == null) FTPPassWord = "";
  //修改前的主键值
  if (hideBankCode == null) hideBankCode = ""; //hideBankCode ;
  if (hideZoneNo == null) hideZoneNo = ""; //hideZoneNo ;
  if (hideBankNode == null) hideBankNode = ""; //hideBankNode ;
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("BankCode", BankCode);
  tTransferData.setNameAndValue("ZoneNo", ZoneNo);
  tTransferData.setNameAndValue("BankNode", BankNode);
  tTransferData.setNameAndValue("UpAgentCom", UpAgentCom);
  tTransferData.setNameAndValue("ComCode", ComCode);
  tTransferData.setNameAndValue("ManageCom", ManageCom);
  tTransferData.setNameAndValue("Remark", Remark);
  tTransferData.setNameAndValue("Operator", Operator);
  tTransferData.setNameAndValue("Remark1", Remark1);
  tTransferData.setNameAndValue("Remark2", Remark2);
  tTransferData.setNameAndValue("Remark5",Remark5);
  tTransferData.setNameAndValue("Remark3", Remark3);
  tTransferData.setNameAndValue("FTPAddress", FTPAddress);
  tTransferData.setNameAndValue("FTPDir", FTPDir);
  tTransferData.setNameAndValue("FTPUser", FTPUser);
  tTransferData.setNameAndValue("FTPPassWord", FTPPassWord);
  tTransferData.setNameAndValue("hideBankCode", hideBankCode);
  tTransferData.setNameAndValue("hideZoneNo", hideZoneNo);
  tTransferData.setNameAndValue("hideBankNode", hideBankNode);
  //
  //  tLKCodeMappingSchema.setBankCode(request.getParameter("BankCode"));
  //  tLKCodeMappingSchema.setZoneNo(request.getParameter("ZoneNo"));
  //  tLKCodeMappingSchema.setBankNode(request.getParameter("BankNode"));
  //  tLKCodeMappingSchema.setAgentCom(request.getParameter("UpAgentCom"));
  //  tLKCodeMappingSchema.setComCode(request.getParameter("ComCode"));
  //  tLKCodeMappingSchema.setManageCom(request.getParameter("ManageCom"));
  //tLKCodeMappingSchema.setOperator(request.getParameter("Operator"));
  //  tLKCodeMappingSchema.setRemark(request.getParameter("Remark"));

	String cFlagStr = null;
	String cContent = null;
	try {
  		NodeManageUI tNodeManageUI = 
  			new NodeManageUI(tTransferData, tG, tOperate);
  		tNodeManageUI.deal();
  		cFlagStr = "Succ";
  		cContent = "操作成功！";
  	} catch (Exception ex) {
  		cLogger.error("操作失败！", ex);
  		cFlagStr = "Fail";
  		cContent = "操作失败：" + ex.getMessage();
  }

cLogger.info("out xbtNodeManageSave.jsp!");
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=cFlagStr%>","<%=cContent%>","");
</script></html>