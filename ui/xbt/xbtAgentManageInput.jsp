<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
  //程序名称：xbtAgentManageInput.jsp
  //程序功能：
  //创建日期：2005-08-29
  //创建人  ：liuyx程序创建
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<head>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="xbtAgentManageInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="xbtAgentManageInputInit.jsp"%>
	<%@include file="../agent/SetBranchType.jsp"%>
	<title></title>
</head>
<body onload="initForm();initElementtype();">
<form action="./xbtAgentManageSave.jsp" method=post name=fm target="fraSubmit">
<span id="operateButton">
  <table class="common" align=center>
    <tr align=right>
      <td class=button>&nbsp;&nbsp;</td>
      <td class=button width="10%">
        <INPUT class=cssButton VALUE="查  询" name="query" TYPE=button onclick="return queryClick();">
      </td>
      <td class=button width="10%">
        <INPUT class=cssButton VALUE="增  加" name="add" TYPE=button onclick="return addClick();">
      </td>
      <td class=button width="20%">
        <INPUT class=cssButton VALUE="修  改" name="update" TYPE=button onclick="return updateClick();">
        <INPUT class=cssButton VALUE="保  存" name="updateSubmit" disabled="disabled" TYPE=button onclick="return updateSubmitForm();">
      </td>
      <td class=button width="10%">
        <INPUT class=cssButton VALUE="删  除" name="del" TYPE=button onclick="return delClick();">
      </td>
      <td class=button width="10%">
        <INPUT class=cssButton VALUE="重  置" TYPE=button onclick="return resetForm();">
      </td>
    </tr>
  </table>
</span>
<table>
  <tr class=common>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLACom1);">
    </td>
    <td class=titleImg>信保通机构&nbsp;&nbsp;&nbsp;</td>
      </tr>
</table>
<Div id="divLACom1" style="display: ''">
  <table class=common>
    <tr class=common>
     <td class=title>网点类别</td>
      <td class=input>
        <input class='codeno'  verify="网点类别|NotNull" name=Remark5 CodeData="0|^0|普通网点^1|对帐网点" ondblclick="return showCodeListEx('NodeType',[this,NodeType],[0,1]);" onkeyup="return showCodeListKeyEx('NodeType',[this,NodeType],[0,1]);"  ><input class=codename name=NodeType readonly=true elementtype=nacessary>  
      </td> 
      <td class=title>地区代码</td>
      <td class=input>
        <input class=common elementtype=nacessary verify="地区代码|NotNull" name=ZoneNo>
      </td>
    </tr>
    <tr class=common>
       <td class=title>银行代码</td>
      <td class=input>
        <input class=codeno  verify="银行代码|NotNull" name=BankCode ondblclick="return showCodeList('banknum',[this,BankCodeName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('ybtbank',[this,BankCodeName],[0,1],null,null,null,1,null,1);">
        <input class=codename  name=BankCodeName readonly=true elementtype=nacessary>
      </td>  
        
      <td class=title>网点代码</td>
      <td class=input>
        <input class=common elementtype=nacessary verify="网点代码|NotNull" name=BankNode>
      </td>
      
    </tr>
   </table>   
     <Div id="divYes" style="display: 'none'" align=center> 
        <table class=common>
      <tr class=common>
      <td class=title>代理机构代码</td>
      <td class=input>
        <input class="common" name=UpAgentCom elementtype=nacessary  onchange="getComName(ComCode,ManageCom)" >
      </td>
      <td class=title>代理机构名称</td>
      <td class=input>
        <input class="readonly"  name=UpAgentName  readonly=true>
      </td>
      </td>
    </tr>
    <tr class=common>
       <td class=title>对帐地区编码</td>
      <td class=input>
        <input class=common  name="Remark3" readonly=true >
      </td>
      <td class=title>登陆机构</td>
      <td class=input>
        <input name=ComCode class="readonly"   readonly=true>
      </td>
    </tr>
    <tr class=common>
      
      <td class=title>对帐网点编码</td>
      <td class=input>
        <input class=common  name="Remark1" readonly=true>
      </td>
      <td class=title>管理机构</td>
      <td class=input>
        <input name=ManageCom class="readonly"   readonly=true>
      </td>
    </tr>
   
</table>
</Div>
		<Div id="divNo" style="display: 'none'" align=center> 
		    <table class=common>
		    <tr class=common>
		        
		        
		        <td class=title>管理机构</td>
		      <td class=input>
		        <input name=ManageCom1 class=codeno   ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
		
		      </td>
		      
		      

		  </table>
		</Div>
</Div>
<input type=hidden name=hideBankCode value=''>
<input type=hidden name=hideZoneNo value=''>
<input type=hidden name=hideBankNode value=''>
<input type=hidden name=hideOperate value=''>
<input type=hidden name=HiddenAgentGroup value=''>
<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLACom);">
    </td>
    <td class=titleImg>信保通机构信息</td>
  </tr>
</table>
<Div id="divLACom" style="display: ''">
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanLKCodeGrid">        </span>
      </td>
    </tr>
  </table>
</div>
<Div id="divPage" align=center style="display: '' ">
  <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
  <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
</Div>
</form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
