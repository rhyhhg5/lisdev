<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
  //程序名称：xbtNodeManageSave.jsp
  //程序功能：
  //创建日期：2010-3-3
  //创建人  ：yinjj
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="com.sinosoft.utility.TransferData"%>

<%@ page import="com.sinosoft.midplat.kernel.management.CertifyManageUI"%>
<%@ page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
Logger cLogger = Logger.getLogger("midplat.xbtCertifyManageSave_jsp");
cLogger.info("into xbtCertifyManageSave.jsp...");

  //接收信息，并作校验处理。输入参数

  //输出参数
  String tOperate = request.getParameter("hideOperate").trim();
  GlobalInput tG = (GlobalInput) session.getValue("GI");
  String BankCode = request.getParameter("BankCode");
	String RiskWrapCode = request.getParameter("RiskWrapCode");
  String ManageCom = request.getParameter("ManageCom");
	String CertifyCode = request.getParameter("CertifyCode");
	String CertifyClass = request.getParameter("CertifyClass");
	String Remark1 = "";
	String Remark2 = "";
	String Operator = request.getParameter("Operator");
  System.out.println("********"+ManageCom);


  //修改前的主键值
  String hideBankCode = request.getParameter("hideBankCode");
  String hideManageCom = request.getParameter("hideManageCom");
  String hideRiskWrapCode = request.getParameter("hideRiskWrapCode");
  if (BankCode == null) BankCode = "";
  if (RiskWrapCode == null) RiskWrapCode = "";
  if (ManageCom == null) ManageCom = ""; //ManageCom ;
  if (CertifyCode == null) CertifyCode = ""; //CertifyCode ;
  if (CertifyClass == null) CertifyClass = "";//CertifyClass;

  if (Remark1 == null) Remark1 = ""; //Remark1 ;
 
  if (Remark2 == null) Remark2 = ""; //Remark2 ;
 
  //修改前的主键值
  if (hideBankCode == null) hideBankCode = ""; //hideBankCode ;
  if (hideManageCom == null) hideManageCom = ""; //hideManageCom ;
  if (hideRiskWrapCode == null) hideRiskWrapCode = ""; //hideRiskWrapCode ;
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("BankCode", BankCode);
  tTransferData.setNameAndValue("RiskWrapCode", RiskWrapCode);
  tTransferData.setNameAndValue("ManageCom", ManageCom);
	tTransferData.setNameAndValue("CertifyCode", CertifyCode);
	tTransferData.setNameAndValue("CertifyClass", CertifyClass);
  tTransferData.setNameAndValue("Operator", Operator);
  tTransferData.setNameAndValue("Remark1", Remark1);
  tTransferData.setNameAndValue("Remark2", Remark2);

  tTransferData.setNameAndValue("hideBankCode", hideBankCode);
  tTransferData.setNameAndValue("hideManageCom", hideManageCom);
  tTransferData.setNameAndValue("hideRiskWrapCode", hideRiskWrapCode);
  String cFlagStr = null;
	String cContent = null;
	try {
  		CertifyManageUI tCertifyManageUI = 
  			new CertifyManageUI(tTransferData, tG, tOperate);
  		tCertifyManageUI.deal();
  		cFlagStr = "Succ";
  		cContent = "操作成功！";
  	} catch (Exception ex) {
  		cLogger.error("操作失败！", ex);
  		cFlagStr = "Fail";
  		cContent = "操作失败：" + ex.getMessage();
  }

cLogger.info("out xbtCertifyManageSave.jsp!");
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=cFlagStr%>","<%=cContent%>","");
</script></html>