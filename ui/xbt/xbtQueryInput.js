var showInfo;
var mDebug = "0";
var mSwitch = parent.VD.gVSwitch;
var arrDataSet;

var turnPage = new turnPageClass();
  
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}

// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,0,0,*";
	} else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
	parent.fraMain.rows = "0,0,0,0,*";
}
function resetForm() {
	try {
		// showDiv(operateButton,"true");
		// showDiv(inputButton,"false");
		initForm();

	} catch (re) {
		alert("在LAComInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}
// 查询按钮
function easyQueryClick() {

	if (verifyInput() == false)
		return false;
	// 初始化表格
	initybtPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var ManageCom = fm.ManageCom.value;
	var ContNo = fm.ContNo.value;
	var AgentCom = fm.AgentComNo.value;
	var ProposalcontNo = fm.ProposalcontNo.value;
	var ContType = fm.ContType.value;
	var RiskCode = fm.RiskCode.value;
	var BankCode = fm.BankCode.value;
	strSQL = "select distinct lc.contno, lc.proposalcontno, lc.appntname, lc.insuredname,lc.cvalidate,lc. cinvalidate,lc.paytodate,lc.prem,"
          +"case lc.stateflag when 0 then '投保' when 1 then '承保有效' when 2 then '失效中止' when 3 then '终止' end "+"stateflag"
          +",ld.name,lg.name,lc.agentcom,la.name,lm.banknode,la.name " 
          +" from lktransstatus lk,lkcodemapping lm,lacom la,ldcom ld,laagent lg,lccont lc "
          +" where ld.comcode= lc.managecom and "
          +" lg.agentcode= lc.agentcode and "
          +" la.agentcom= lc.agentcom and "
          +" lk.bankcode = lm.bankcode "
          +" and lk.bankbranch = lm.zoneno "
          +" and lk.banknode = lm.banknode "
          +" and lk.polno = lc.contno "
          +" and la.agentcom  =  lm.agentcom "
          + getWherePart("lk.bankcode", "BankCode")	
          + getWherePart("lc.managecom", "ManageCom")
          + getWherePart("lc.contno", "ContNo")
          + getWherePart("lc.agentcom", "AgentComNo")
          + getWherePart("lc.proposalcontno", "ProposalcontNo")
          + getWherePart("lc.signdate", "StartDate", ">=")
          + getWherePart("lc.signdate", "EndDate", "<=")
          + getWherePart("lc.cvalidate", "startcvalidate", ">=")
          + getWherePart("lc.cvalidate", "endcvalidate", "<=")
          + getWherePart("lc.stateflag", "ContType")
          + getWherePart("lk.riskcode", "RiskCode")
          + "order by lc.proposalcontno with ur";
     turnPage.queryModal(strSQL, ybtPolGrid);
}