<%
  //程序名称：xbtAmntUpLimitInit.jsp
  //程序功能：
  //创建日期：2010-3-2 
  //创建人  ：yinjj
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">
var manageCom = '<%= globalInput.ManageCom %>';
var comcode = '<%= globalInput.ComCode %>';
function initInpBox()
{
  try
  {
    fm.all('BankCode').value = '';
    fm.all('BankCodeName').value = '';
    fm.all('ManageCom').value = '';
    fm.all('RiskWrapCode').value = '';
    fm.all('AgentCom').value = '';
    fm.all('Amnt').value = '';
//   fm.all('Operator').value = '';
  
       

  

  }
  catch(ex)
  {
    alert("在xbtAmntUpLimitInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  
  }
  catch(ex)
  {
  alert("在xbtAmntUpLimitInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    
    initInpBox();    
    
    initSelBox();
    initLKCodeGrid();
    
    
  }
  catch(re)
  {
    alert("xbtAmntUpLimitInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LKCodeGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initLKCodeGrid()
{
    var iArray = new Array();
    //var i11Array = getAgentGradeStr();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="银行代码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="管理机构";         		//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="套餐编码";         		//列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="中介机构";         		//列名
      iArray[4][1]="40px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="保额上限值";         		//列名
      iArray[5][1]="40px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      
      LKCodeGrid = new MulLineEnter( "fm" , "LKCodeGrid" );
      //这些属性必须在loadMulLine前
      LKCodeGrid.displayTitle = 1;
      LKCodeGrid.locked = 0;
      LKCodeGrid.mulLineCount = 1;
      LKCodeGrid.hiddenPlus = 1;
      LKCodeGrid.hiddenSubtraction = 1;
      LKCodeGrid.canSel = 1;

      LKCodeGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //LAComGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
