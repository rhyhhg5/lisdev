<%
//程序名称：GetAdvanceBatchSave.jsp
//程序功能：
//创建日期：2009-06-19 09:25:18
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%
    String FlagStr = "";
    String Content = "";
    String type = request.getParameter("type");
    
    try{
    if(type.equals("1")){
        System.out.println("-----月报表发送-----");
        PremReceivableRptM tBatchSendDataTask = new PremReceivableRptM();
        FlagStr = "";
        Content = "批处理程序触发成功";
        tBatchSendDataTask.run();
    }else if(type.equals("2")){
        System.out.println("-----日报表发送-----");
        PremReceivableRptD tBatchSendDataTask = new PremReceivableRptD();
        FlagStr = "";
        Content = "批处理程序触发成功";
        tBatchSendDataTask.run();
    }else if(type.equals("3")){
        System.out.println("-----高端医疗报表发送（团）-----");
        HighendMedicalDockTask tBatchSendDataTask = new HighendMedicalDockTask();
        FlagStr = "";
        Content = "批处理程序触发成功";
        tBatchSendDataTask.run();
    }else if(type.equals("4")){
        System.out.println("-----高端医疗报表发送（个）-----");
        HighendMedicalDockITask tBatchSendDataTask = new HighendMedicalDockITask();
        FlagStr = "";
        Content = "批处理程序触发成功";
        tBatchSendDataTask.run();
    }else if(type.equals("5")){
        System.out.println("-----集团交叉补报数据-----");
        MixedContNTaskSub tBatchSendDataTask = new MixedContNTaskSub();
        FlagStr = "";
        Content = "批处理程序触发成功";
        tBatchSendDataTask.run();
    }else if(type.equals("6")){
        System.out.println("-----北分外包数据发送-----");
        BFCBXmlTransTask tBatchSendDataTask = new BFCBXmlTransTask();
        FlagStr = "";
        Content = "批处理程序触发成功";
        tBatchSendDataTask.run();
    }
    } catch (Exception ex){
        ex.printStackTrace();
        FlagStr = "Fail";
        Content = "批处理程序运行失败";
    }
%>                      
<html>
<script language="javascript">

parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>

