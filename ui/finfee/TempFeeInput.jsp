

<%
//程序名称：TempFeeInput.jsp
//程序功能：财务收费的输入
//创建日期：2002-07-12 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT> 
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
<SCRIPT src="TempFeeInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="TempFeeInit.jsp"%>
</head>
<body  onload="initForm();" >
<Form action="./TempFeeQuery.jsp" method=post name=fm target="fraSubmit">
	<!--<%@include file="../common/jsp/OperateButton.jsp"%> -->
    <!--<%@include file="../common/jsp/InputButton.jsp"%> -->
<!--框架一：公有信息 --> 
     <Table>
    	<tr>
         <td class=common>
         <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,Frame1);">    	 
          <td class= titleImg>
           公共信息
           ( <input type =button class=cssButton value="查询交费" onclick="queryTempfee();">  )
          </td>
            	  
    	 </td>
    	</tr>
    </Table>
  <Div  id= "Frame1" style= "display: ''">          
     <Table class= common border=0>
       <TR  class= common> 
          <TD  class= title>
            管理机构
          </TD>          
          <TD  class= input>
          <Input name=GlobalManageCom  type="hidden">
	     <Input class=codeNo name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  
	  </TD>             
          <TD class= title>
            交费日期
          </TD>
          <TD class= input >
            <Input class="coolDatePicker" verify="交费日期|DATE&NOTNULL" name=PayDate >
          </TD>  
                                        
       </TR>         
       <TR  class= common>
          <TD  class= title>
            业务员
          </TD>
         <td class="input" width="25%">
           <!--Input class="code" name=AgentCode verify="代理人编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');" onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');"-->     
         <Input class=common name=GroupAgentCode MAXLENGTH=0>
         <Input class=common name=AgentCode VALUE="" MAXLENGTH=0 type='hidden'>
         <input class=cssButton type="button" value="查  询" style="width:60" onclick="queryAgent()"></td>     
        </TD> 
        <!--
          <TD  class= title>
            代理人组别
          </TD>
          -->
          <TD class=title>
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly tabindex=-1 name=AgentGroup  type=hidden>
          </TD>
          
      </TR>           	                 	 
     </TABLE>
 </Div>
<!--框架一  -->     

<!--框架二：选择交费类型并输入 -->  
     <Table>
    	<tr>
         <td class=common>
         <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,Frame2);">    	 
          <td class= titleImg>
           交费录入
           ( <input type =button class=cssButton value="待收费查询" onclick="PayQueryClick();">  )
          </td>    	 
    	 </td>
    	</tr>
    </Table>
   <Div  id= "Frame2" style= "display: ''">
     <table class= common> 
       <TR  class= common>
          <TD  class= title>
            交费类型
          </TD>          
          <TD  class= input>
            <Input class=codeNo name=TempFeeType  ondblclick="return showCodeList('TempFeeType1',[this,TempFeeTypeName],[0,1]);" onkeyup="return showCodeListKey('TempFeeType1',[this,TempFeeTypeName],[0,1]);"><input class=codename name=TempFeeTypeName readonly=true >
          </TD>        
          <td class=title></td>
          <td class=input></td>
      </TR> 
     </table>
    
    <!-- 团体保单新单交费 -->  
    <Div id="TempFeeType1" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
            投保单印刷号码
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo1 onblur="getInfo1(this)">
          </TD>  
				<TD  class= input>
					<Input name=InputNoName CLASS="code" type="hidden">
					号码类型 <Input class="code" name=InputNoType CodeData="0|^1|缴费通知书号码|^2|暂收收据号码" ondblclick="showCodeListEx('InputNoType',[this,InputNoName],[1,0],null,null,null,1);" onkeyup="showCodeListEx('InputNoType',[this,InputNoName],[1,0],null,null,null,1);">
				</TD>                 
          <TD  class= input>
            <Input class=common name=InputNo2 >
          </TD>                            
      </TR>
    </table> 
    </Div>
    
    <!-- 续期催收交费 --> 
    <Div id="TempFeeType2" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title >
            合同号码
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo3 onblur="getInfo3(this)">
          </TD>       
	   <TD  class= title>
            交费通知书号码
          </TD>
	 <TD  class= input>
            <Input class=common name=InputNo4 >
          </TD>
          
               <TD  class= input>
            <Input class=cssButton type=button value="查  询" onclick="queryLJSPay();">
          </TD>           
      
      </TR>
    </table>
    <Div id = "divErrorMes" ></Div>  
    </Div>
    
    <!-- 续期非催收交费 --> 
    <Div id="TempFeeType3" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
            保单号码
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo5 >
          </TD>        
                   
      </TR>
    </table>  
    </Div>
          
    <!-- 保全交费 --> 
    <Div id="TempFeeType4" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
            批单号码
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo7  onblur="getInfo5(this)">
          </TD>        
          <TD  class= title>
            交费收据号码
          </TD>
          <TD  class= input>
            <Input class=common name=InputNo8 >
          </TD>          
      </TR>
    </table>  
    <Div id = "divErrorMesb" ></Div>  
    </Div>
         <!-- 理赔预付回收 --> 
    <Div id="TempFeeType9" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
            预付赔款号
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo24  onblur="getInfo9(this)">
          </TD>        
          <TD  class= title>
            交费收据号码
          </TD>
          <TD  class= input>
            <Input class=common name=InputNo25 >
          </TD>          
      </TR>
    </table>  
    </Div>
    
    <!-- 首期银行代扣交费 -->  
    <Div id="TempFeeType5" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
            投保单印刷号码
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo9 >
          </TD>        
          <TD  class= title>
            投保单印刷号码
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo10 >
          </TD>         
      </TR>
    </table>  
    </Div>
    
    <!-- 定额单交费 -->  
    <Div id="TempFeeType6" style="display:none">
    <table class= common> 
      <TR  class= common>
          <!--TD  class= title>
            单证编码 
          </TD>          
          <TD  class= input>
            <Input class=code name=InputNo11 ondblclick="showCodeList('CardCode',[this]);" onkeyup="return showCodeListKey('CardCode',[this]);" >
          </TD-->        
          <TD  class= title>
            结算单号
          </TD>
          <TD  class= input>
            <Input class=common name=InputNo12 onblur="getInfo6(this)">
          </TD>
          <TD  class= title>
          	暂收收据号
          </TD>
          <TD  class= input>
          	<Input class=common name=InputNo18>
          </TD>          
      </TR>
    </table>  
    </Div>
    
    <!-- 保全受理交费 --> 
    <Div id="TempFeeType7" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
            保全受理号
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo13 >
          </TD>        
          <TD  class= title>
            交费收据号码
          </TD>
          <TD  class= input>
            <Input class=common name=InputNo14 >
          </TD>          
      </TR>
    </table>  
    </Div>
    
    <!-- 预打保单交费 -->  
    <Div id="TempFeeType8" style="display:none">
    <table class= common> 
      <!--<TR  class= common>
          <TD  class= title >
            <Input type=radio class=cssButton name=radionButton >
            个人保单
          </TD>        	
          <TD  class= title >
            <Input type=radio class=cssButton name=radionButton >
            团体保单
          </TD>
          <TD  class= title >
          </TD>      
          <TD  class= title >
          </TD>                          
      </TR>-->
      <TR  class= common>
          <TD  class= title>
            投保单印刷号码
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo15 >
          </TD>        
          <TD  class= title>
            交费收据号码
          </TD>
          <TD  class= input>
            <Input class=common name=InputNo16 >
          </TD>          
      </TR>
    </table>  
    </Div>      
    <!-- 代理点定期结算 -->  
    <Div id="TempFeeType10" style="display:none">
    <table class= common> 
      <TR  class= common>
				  <TD  class= input>
				  	结算单号
				  </TD>                 
          <TD class= input>
            <Input class=common  name=InputNo17 onblur='getInfo7(this)'>
          </TD>
          <TD  class= title>
          </TD>
          <TD  class= input>   
          </TD>   
           <TD  class= title>
          </TD>
          <TD  class= input>   
          </TD>
       </TR>  
    </table> 
     </Div> 
     <!-- 个人保单新单交费 --> 
    <Div id="TempFeeType11" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
            投保单印刷号码
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo22 onblur="getInfo2(this)">
          </TD>  
				<TD  class= input>
					<Input name=InputNoName2 CLASS="code" type="hidden">
					号码类型 <Input class="code" name=InputNoType2 CodeData="0|^1|缴费通知书号码|" ondblclick="showCodeListEx('InputNoType2',[this,InputNoName2],[1,0],null,null,null,1);" onkeyup="showCodeListEx('InputNoType2',[this,InputNoName2],[1,0],null,null,null,1);">
				</TD>                   
          <TD  class= input>
            <Input class=common name=InputNo23 >
          </TD>                            
      </TR>
    </table> 
    </Div>
     <!-- 健管交费 -->  
    <Div id="TempFeeType20" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
           申请书编号
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo20 onblur="getInfo20(this);">
          </TD>  
		      <TD  class= title>
            交费收据号码
          </TD>
          <TD  class= input>
            <Input class=common name=InputNo21 >
          </TD>                
      </TR>
    </table> 
    </Div>      
       <!-- 意外险社保报销交费 -->  
    <Div id="TempFeeType25" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
           保单号
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo26 onblur="getInfo24(this);">
          </TD>  
		      <TD  class= title>
            交费收据号码
          </TD>
          <TD  class= input>
            <Input class=common name=InputNo27 >
          </TD>                
      </TR>
    </table> 
    </Div> 
     <!-- 健康管理服务交费 -->  
    <Div id="TempFeeType30" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title>
           服务合同号
          </TD>          
          <TD  class= input>
            <Input class=common name=InputNo31 onblur="getInfo30(this);">
          </TD>  
              <TD  class= title>
            交费收据号码
          </TD>
          <TD  class= input>
            <Input class=common name=InputNo32 >
          </TD>                
      </TR>
    </table> 
    </Div>      
    <table  align=right>    
     <TR>  
     <TD   class=common > 
             <Input type=button class= cssButton value="收费批量导入" onclick="batchGet();">
    </TD>   
    </table></tr>
    <table  align=right>    
     <TR  >    
      <TD   class=common align="Left"> 
         <font color="#FF0000" align="Left">
         交费类型为“11-个人保单新单收费”数据，缴费通知书号不会自动带出，请进行手工录入，谢谢！
         </font>
    </TD>   
      <TD>
       <Input type=button class= cssButton value="确  认" onclick="confirm();">
      </TD> 
      <!--保存交费号码 type=hidden--> 
      <TD class= common>
       <input  class= common  name="TempFeeNo" type="hidden" readonly >
      <TD>                                                
     </TR>
    </table> 
    <br><br>           
                 
  <Div  id= "divTempFeeInput" style= "display: ''">
  <!--录入的交费表部分 -->
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanTempGrid" >
	 </span> 
	</td>
       </tr>
    </Table>
  <!--录入的交费分类表部分 -->
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanTempClassGrid" >
	 </span> 
	</td>
       </tr>
    </Table>      
    <table align=right>
    <TR class=common>     
     <TD class=common>
     	<input type =button class=cssButton value="收付方式说明" onclick="payModeHelp();">
      <input type =button class=cssButton value="添加一笔" onclick="addRecord();">      
     </TD>
    </TR> 
    </table>
    <br><br>
    <Table class=common>
      <TR>
        <TD  class= title>
          交费共缴纳
        <input width='10px' class=common  name=TempFeeCount readonly tabindex=-1 >
          笔
       </TD>
       <TD  class= title>
          交费总计
        <input width='10px' class=common  name=SumTempFee readonly tabindex=-1 >
       </TD>
    </TR>
   </Table>       
    </Div>
   </Div>  
   <input type="hidden" name="OtherNoType" >
   <Input type="hidden"  name="TempFeeTypeCheck">
</Form>
<!--框架二 -->

<!--框架三：保存提交的数据 -->
<Form action="./TempFeeSave.jsp" method=post name=fmSave target="fraSubmit">
     <Table>
    	<tr>
         <td class=common>
         <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divTempFeeSave);">    	 
          <td class= titleImg>
           存储数据
          </td>    	 
    	 </td>
    	</tr>
    </Table>          
  <Div  id= "divTempFeeSave" style= "display:'none'">
  <!--交费表部分 -->  
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanTempToGrid" >
	 </span> 
	</td>
       </tr>
    </Table>
    
    <Table>
    <tr>  </tr>
    </Table>
    
  <!--交费分类表部分 -->
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanTempClassToGrid" >
	 </span> 
	</td>
       </tr>
    </Table>
    <table align=right>
    <TR class=input>
     <TD   class=common align="Left"> 
         <font color="#FF0000" align="Left">
        		 交费方式为“2-现金支票，3-转账支票”数据，请在提交数据之后进行到账确认！
         </font>
    </TD>      
     <TD class=common>
      <input type =button class=cssButton value="全部提交" onclick="submitForm();">        
      <input type =button class=cssButton value="撤销全部数据" onclick="clearFormData();">    
     </TD>
    </TR>
    </table>
  </Div>
</Form> 
<!--框架三 -->   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

<!--隐藏-存放查询数据 -->
<Form action="./TempFeeTypeQuery.jsp" method=post name=fmTypeQuery target="fraSubmit">
  <input type=hidden name=QueryNo>
  <input type=hidden name=QueryType>
</Form>

<!--隐藏-存放卡折数据 -->
<form action="../certifyapp/CertifySignAfterPay.jsp" method=post name=fmCertify target="fraSubmit">
    <input type=hidden name=GrpContNo>
    <input type=hidden name=MissionID>
    <input type=hidden name=SubMissionID>
</form>
</body>
</html>
