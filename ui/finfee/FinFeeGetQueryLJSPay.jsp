<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<title>实收总表查询 </title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>   
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <script src="./FinFeePayQueryLJSPay.js"></script> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FinFeePayQueryLJSPayInit.jsp"%>


</head>

<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm action=./FinFeePayQueryLJAGetResult.jsp target=fraSubmit method=post>
    <table  class= common align=center>
      	<TR  class= common>
          <!--
          <TD  class= title>
          通知书号码:
          </TD>
          <TD  class= input>
            <Input class= common name=GetNoticeNo >
          </TD>
          -->
         <TD  class= title>
          保单号码:
          </TD>
          <TD  class= input>
         <Input class="common" name=OtherNo >
          </TD>   
          <TD  class= title>
          险种编码:
          </TD>
          <TD  class= input>
            <Input NAME=RiskCode class=code ondblclick="return showCodeList('RiskCode', [this]);" onkeyup="return showCodeListKey('RiskCode', [this]);">
          </TD>
      	<TR  class= common>
          <TD  class= title>
          投保人姓名:
          </TD>
          <TD  class= input>
         <Input class="common" name=AppntName >
          </TD>      	      
          <TD  class= title>
          被保人姓名:
          </TD>
          <TD  class= input>
         <Input class="common" name=InsuredName >
          </TD>      	               	      
       </TR>                     
   </Table>  
      <INPUT VALUE="查询" Class=cssButton TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="返回" Class=cssButton TYPE=button onclick="returnParent();">   
      <INPUT VALUE="关闭" Class=cssButton TYPE=button onclick="top.close();">   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</TD>
    		<TD class= titleImg>
    			 实收总表信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divFinFee1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanQueryLJAGetGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
 </Div>					

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
</html>
