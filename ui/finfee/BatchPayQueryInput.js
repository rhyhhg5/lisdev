  //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var bankflag="";
//提交，保存按钮对应操作
function submitForm()
{
  fm.submit(); //提交
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function easyQueryClick() {
       if(fm.all('TempFeeType').value==""){
        alert("请输入查询类型！！");
        return false;
       }
       if(fm.all('InputDataNo1').value==""||fm.all('InputDataNo2').value==""){
        alert("请输入查询日期！！");
        return false;
       }
        var sDate=fm.all('InputDataNo1').value;
         var eDate=fm.all('InputDataNo2').value;
        if ((eDate.split("-")[0]>sDate.split("-")[0])||((eDate.split("-")[0]==sDate.split("-")[0])&&(eDate.split("-")[1]-sDate.split("-")[1])>=3)){
          alert("起始日期和終止日期月份之间不能大於三個月！！");
          return false;
         }
        if(fm.all('TempFeeType').value == "1"){
          var arrp="select trim(prtno)||'801',prtno,'新单交费',(case paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' when '5' then '内部转账' when '11' then '银行汇款' when '12' then '银行代收' end),prem,bankcode,bankaccno,accname,appntname,AppntNo " 
				+"from lccont  where uwflag in ('4','9') " 
				+"and Conttype = '1'  " 
				+"and GrpContNo = '00000000000000000000' " 
				+"and CardFlag = '0' " 
				+"and appflag in ('0', '9') " 
				+"and prem<>0 and signdate is null " 
				+"and (select count(1) from lcrnewstatelog where prtno=lccont.prtno)=0 " 
				+"and paymode  in ('1','2','3','5','6','11','12')   " 
				+"and managecom like '"+fm.all('ComCode').value+"%' " 
				+"and makedate>='"+fm.all('InputDataNo1').value+"' " 
				+"and makedate<='"+fm.all('InputDataNo2').value+"' " 
				+"and not exists (select 1 from ljtempfee where otherno=lccont.prtno and othernotype = '4' ) " 
				+"union all " 
				+"select trim(prtno)||'801',prtno,'新单交费',(case paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' when '5' then '内部转账' when '11' then '银行汇款' when '12' then '银行代收' end),prem, '', '', '',grpname,AppntNo " 
				+"from lcgrpcont  where uwflag in ('4','9') " 
				+"and appflag in ('0', '9') " 
				+"and (cardflag is null or cardflag='4') " 
				+"and prem<>0 and signdate is null " 
				+"and (select count(1) from lcrnewstatelog where prtno=lcgrpcont.prtno)=0 " 
				+"and paymode in ('1','2','3','5','6','11','12') " 
				+"and managecom like '"+fm.all('ComCode').value+"%' " 
				+"and makedate>='"+fm.all('InputDataNo1').value+"' " 
				+"and makedate<='"+fm.all('InputDataNo2').value+"' " 
				+"and not exists (select 1 from ljtempfee where otherno=lcgrpcont.prtno and othernotype = '5') " 
				+"with ur";
          }
//日期需要修改
     if(fm.all('TempFeeType').value == "2"){

         var arrp="select distinct a.GetNoticeNo,a.OtherNo,'续期催收交费','', "
            +"a.SumDuePayMoney,a.BankCode,a.BankAccNo,a.AccName,b.AppntName,b.AppntNo "
            +"from  LJSPay a,LCCont b "
            +"where a.OtherNoType = '2' "
            +"and b.ContType = '1' "
            +"and a.OtherNo = b.ContNO "
            +"and a.BankAccNo is null  "
            +"and a.GetNoticeNo like '31%' "
            +"and a.ManageCom like '"+fm.all('ComCode').value+"%' " 
            +"and a.MakeDate>='"+fm.all('InputDataNo1').value+"'  "
            +"and a.MakeDate<='"+fm.all('InputDataNo2').value+"'  "
            +"union all "
            +"select distinct a.GetNoticeNo,a.OtherNo,'续期催收交费','', "
            +"a.SumDuePayMoney,a.BankCode,a.BankAccNo,a.AccName,b.GrpName,b.AppntNo "
            +"from LJSPay a,LCGrpCont b "
            +"where a.OtherNoType = '1' "
            +"and a.OtherNo = b.GrpContNo "
            +"and a.BankAccNo is null  "
            +"and a.GetNoticeNo like '31%' "
            +"and a.ManageCom like '"+fm.all('ComCode').value+"%'   "
            +"and a.MakeDate>='"+fm.all('InputDataNo1').value+"' " 
            +"and a.MakeDate<='"+fm.all('InputDataNo2').value+"' " 
            +"with ur";
          
    }
    if(fm.all('TempFeeType').value == "4"){

        var arrp = "select distinct a.GetNoticeNo,a.OtherNo,'保全交费','',"
    	   +"a.SumDuePayMoney,a.BankCode,a.BankAccNo,a.AccName,c.GrpName,c.AppntNo "
    	   +"from LJSPay a,LPGrpEdorItem b,LCGrpCont c "
    	   +"where a.OtherNo = b.EdorAcceptNo "
    	   +"and b.GrpContNo = c.GrpContNo "
    	   +"and a.OtherNoType in ('3','13') "
    	   +"and a.BankAccNo is null "
    	   +"and a.SumDuePayMoney <> 0 "
    	   +"and a.ManageCom like '"+fm.all('ComCode').value+"%' " 
       	   +"and a.MakeDate>='"+fm.all('InputDataNo1').value+"'  "
       	   +"and a.MakeDate<='"+fm.all('InputDataNo2').value+"'  "
    	   +"union all "
    	   +"select distinct a.GetNoticeNo,a.OtherNo,'保全交费','', "
    	   +"a.SumDuePayMoney,a.BankCode,a.BankAccNo,a.AccName,c.AppntName,c.AppntNo "
    	   +"from LJSPay a,LPEdorItem b,LCCont c "
    	   +"where a.OtherNo = b.EdorAcceptNo " 
    	   +"and b.ContNo = c.ContNo "
    	   +"and a.OtherNoType = '10' "
    	   +"and c.ContType = '1' "
    	   +"and a.BankAccNo is null "
    	   +"and a.SumDuePayMoney <> 0  "
    	   +"and a.ManageCom like '"+fm.all('ComCode').value+"%' " 
       	   +"and a.MakeDate>='"+fm.all('InputDataNo1').value+"'  "
       	   +"and a.MakeDate<='"+fm.all('InputDataNo2').value+"'  "
    	   +"with ur";
          
    }
    if(fm.all('TempFeeType').value == "6"){

        var arrp="select b.payno,b.payno,'6','',b.duepaymoney,'','','' from lzcard a,lzcardpay b where a.Startno=b.startno and a.Endno=b.Endno and a.subcode=b.cardtype and b.state='1' and not exists (select 1 from ljtempfee where tempfeeno=b.payno) and b.managecom like '"+fm.all('ComCode').value+"%' and b.makedate>='"+fm.all('InputDataNo1').value+"' and b.makedate<='"+fm.all('InputDataNo2').value+"' with ur";
          
    }
    if(fm.all('TempFeeType').value == "10"){
    	
       var arrp="Select Distinct a.Getnoticeno,a.Otherno,'10','',a.Sumduepaymoney,a.Bankcode,a.Accname,a.Bankaccno,c.grpname,c.appntno " +
       		"From Ljspay a " +
       		"Inner Join Lpgrpedoritem b On a.Otherno = b.EdorAcceptNo " +
       		"Inner Join Lcgrpcont c On c.Grpcontno = b.Grpcontno " +
       		"Where a.Othernotype = '13' " +
       	    "and a.ManageCom like '"+fm.all('ComCode').value+"%' " +
        	"and a.MakeDate>='"+fm.all('InputDataNo1').value+"' " + 
        	"and a.MakeDate<='"+fm.all('InputDataNo2').value+"' " +
       		"And a.Bankaccno Is Null " +
       		"And a.Otherno Is Not Null With Ur";
          
    }
    if(fm.all('TempFeeType').value == "20"){
   
        var arrp="select trim(a.GrpPrtNo)||'801',a.GrpPrtNo,'健管交费',(case a.paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' when '5' then '内部转账' when '11' then '银行汇款' when '12' then '银行代收' end),decimal(b.Prem,12,2),a.BankCode,a.BankAccNo,a.AccName,a.grpname,a.customerno "
				+"from HCGrpCont a,HCGrpProduct b "
				+"where 1=1 "
				+"and a.GrpContNo=b.GrpContNo "
				+"and a.ContState='2' "
				+"and a.ContKind='01' "
				+"and a.signdate is null "
				+"and a.managecom like '"+fm.all('ComCode').value+"%'  "
				+"and a.makedate>='"+fm.all('InputDataNo1').value+"'  "
				+"and a.makedate<='"+fm.all('InputDataNo2').value+"'  "
				+"union all "
				+"select trim(a.PrtNo)||'801',a.PrtNo,'健管交费',(case a.paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转账支票' when '5' then '内部转账' when '11' then '银行汇款' when '12' then '银行代收' end),decimal(b.Prem,12,2),a.BankCode,a.BankAccNo,a.AccName,a.buyername,a.buyerno "
				+"from HCCont a,HCProduct b "
				+"where 1=1 "
				+"and a.ContNo=b.ContNo "
				+"and a.ContState='2' "
				+"and a.ContKind='01' "
				+"and a.managecom like '"+fm.all('ComCode').value+"%' "
				+"and a.makedate>='"+fm.all('InputDataNo1').value+"'  "
				+"and a.makedate<='"+fm.all('InputDataNo2').value+"'  "
				+"with ur" ;
          
    }
    if(fm.all('TempFeeType').value == "30"){
   
        var arrp=" select getnoticeno,otherno,'30','',sum(SumDuePayMoney),bankcode,accname,bankaccno from LJSPay a where  a.othernotype='30' and bankaccno is null and a.otherno is not null and managecom like '"+fm.all('ComCode').value+"%' and makedate>='"+fm.all('InputDataNo1').value+"' and makedate<='"+fm.all('InputDataNo2').value+"' group by getnoticeno,otherno,bankcode,accname,bankaccno with ur";
          
    }
    if(fm.all('TempFeeType').value == "9"){
   
        var arrp=" Select Distinct a.Getnoticeno,a.Otherno,'9','',a.Sumduepaymoney,a.Bankcode,a.Accname,a.Bankaccno,c.Grpname,c.Appntno " +
        		"From Ljspay a " +
        		"Inner Join Llprepaidtrace b On a.Otherno = b.Otherno " +
        		"Inner Join Lcgrpcont c On c.Grpcontno = b.Grpcontno " +
        		"Where a.Othernotype = 'Y' " +
        		"And a.Otherno Is Not Null " +
        		"And a.Managecom Like '"+fm.all('Comcode').value+"%' " +
        		"And a.Makedate >= '"+fm.all('Inputdatano1').value+"' " +
        		"And a.Makedate <= '"+fm.all('Inputdatano2').value+"' " +
        		"And Not Exists (Select 1 From Ljtempfee Where Tempfeeno = a.Getnoticeno) With Ur";
    }
  fm.arrpSql.value = arrp;
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(arrp); 
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	CheckTempGrid.clearData();
  	alert("没有查询到数据！");
    return false;
  }
   //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = CheckTempGrid;    
     
  //保存SQL语句
  turnPage.strQuerySql     = arrp; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果

  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);   
    
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  fm.type.value=fm.TempFeeType.value;
}


function afterSubmit( FlagStr, content )
{
  showInfo.close(); 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  LJAGetGrid.clearData('LJAGetGrid');  
}


function checkImportDate(tDate,tActugetno) 
			{ 
				var dateStr=tDate;
				if(dateStr==""){
					return;
				}
				var   datePat=/^((\d{2}(([02468][048])|([13579][26]))[\-]?((((0?[13578])|(1[02]))[\-]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-]?((((0?[13578])|(1[02]))[\-]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[0-9])|([1-2][0-3]))\:([0-5]?[0-9])((\s)|(\:([0-5]?[0-9])))))?$/;  
				var   matchArray=dateStr.match(datePat);   //   is   the   format   ok?   
				if   (matchArray==null)   {   
					alert("请正确输入实付号"+tActugetno+"的到帐日期格式 yyyy-mm-dd");   
					return   false;   
				}   
				if(tDate.length!=10){
					alert("请正确输入实付号"+tActugetno+"的到帐日期格式 yyyy-mm-dd"); 
					return   false;
				} 
				month=dateStr.split('-')[1];   //   parse   date   into   variables   
				day=dateStr.split('-')[2];   
				year=dateStr.split('-')[0];   
				if   (month   <   1   ||   month   >   12)   {   //   check   month   range   
					alert("请正确输入实付号"+tActugetno+"的到帐日期,月份必须在1到12之间");   
					return   false;   
				}   
				if   (day   <   1   ||   day   >   31)   {   
					alert("请正确输入实付号"+tActugetno+"的到帐日期,日期必须在1到31之间");
					return   false;   
				}   
				if((month=="04" || month=="06" || month=="09" || month=="11") && day=="31"){   
					alert("请正确输入实付号"+tActugetno+"的到帐日期,"+month+"没有31天");  
					return false;   
				}   
				if(month=="02"){  
					var isleap=(year%4==0&&(year%100!=0||year%400==0));  
					if(day>29 ||(day==29&&(!isleap))){   
						alert("请正确输入实付号"+tActugetno+"的到帐日期,二月 "+year+"没有"+day+" 天!"); 
						return false;   
					}   
				} 
				return  true;  
			} 
