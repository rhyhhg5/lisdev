<html> 
<%
//程序名称：FinVerifyColl.jsp
//程序功能：应收个人交费表的输入
//创建日期：2002-07-12 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="FinVerifyColl.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body >
<Form action="./FinVerifyCollOperation.jsp" method=post name=fm target=fraSubmit>
     <TABLE class=common>
       <TR  class= common>
          <TD  class= title>
            暂交费收据号码
          </TD>
          <TD  class= input>
            <Input class= common name=TempFeeNo >
          </TD>
          <TD  class= input>
            <Input class= cssButton type=Button value="核销" onclick="submitForm();">
          </TD>
        </TR>
      </TABLE>   
</Form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
