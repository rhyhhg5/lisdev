<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2009-09-10
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>
<%@page import="com.sinosoft.lis.pubfun.*" %>

<%
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  XmlExport txmlExport = null; 
  String outname = ""; 
  String outpathname = ""; 
    
	String tChk[]=request.getParameterValues("InpContInfoGridChk");
  String tTempfeeNo[] = request.getParameterValues("ContInfoGrid7");
  LJTempFeeSet tLJTempFeeSet=new LJTempFeeSet();
  for(int i=0;i<tChk.length;i++){
			if(tChk[i]!=null && tChk[i].equals("1")){
				LJTempFeeSchema tLJTempFeeSchema=new LJTempFeeSchema();
				tLJTempFeeSchema.setTempFeeNo(tTempfeeNo[i]);
				tLJTempFeeSet.add(tLJTempFeeSchema);
			}
	}
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
  tTransferData.setNameAndValue("StartDate", request.getParameter("StartDate"));
  tTransferData.setNameAndValue("EndDate", request.getParameter("EndDate"));
  
  VData tVData = new VData();
  tVData.addElement(tLJTempFeeSet);
  tVData.addElement(tG);
  tVData.addElement(tTransferData);
         
  PrtUnSignPayedListUI tPrtUnSignPayedListUI = new PrtUnSignPayedListUI(); 
  if(!tPrtUnSignPayedListUI.submitData(tVData,"PRINT"))
  {          
     	operFlag = false;
     	Content = tPrtUnSignPayedListUI.mErrors.getFirstError(); 
  }
  else
  {             
    VData mResult = tPrtUnSignPayedListUI.getResult();			
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    
    if(txmlExport==null)
    {
    	operFlag=false;
    	Content="没有得到要显示的数据文件";	  
    }
  }
	
	if (operFlag==true)
	{
	  //
	  //String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    //ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    //CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    //tcombineVts.output(dataStream);
    //session.putValue("PrintVts", dataStream);
	  
		//session.putValue("PrintStream", txmlExport.getInputStream());
		//response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
		
	  Readhtml rh = new Readhtml();
	  rh.XmlParse(txmlExport.getInputStream());
		
	  String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址  
	  String temp=realpath.substring(realpath.length()-1,realpath.length());
	  if(!temp.equals("/"))
	  {
		  realpath=realpath+"/"; 
	  }
	  
	  String templatename=rh.getTempLateName();//模板名字
	  String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
	  //System.out.println("*********************templatepathname= " + templatepathname);
	  //System.out.println("************************realpath="+realpath);
	  
	  String date=PubFun.getCurrentDate().replaceAll("-","");
	  String time=PubFun.getCurrentTime().replaceAll(":","");
	  outname="未签单已收费打印"+tG.Operator+date+time+".xls";
	  outpathname=realpath+"vtsfile/"+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	 
	  rh.setReadFileAddress(templatepathname);
	  rh.setWriteFileAddress(outpathname);
	  rh.start("vtsmuch");
	  try {
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
			outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
			outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	
%>
	<a href="../f1print/download.jsp?filename=<%=outname%>&filenamepath=<%=outpathname%>">点击下载</a>
<%	
	}
	else
	{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>