<%

%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<%
    GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
var ManageCom = "<%=tGI.ManageCom%>";  //操作员编号

function initForm()
{
    try
    {
        initInputBox();
        initElementtype();
    }
    catch(e)
    {
        alert("初始化界面错误!");
    }
}

function initInputBox()
{
    if(ManageCom.length < 4 || ManageCom.substring(0,4) != "8612"){
        alert("非天津分公司，无法按发票代码导出！");
    } else {
        document.getElementById("ext").style.display = "";
        fm.all('ComCode').value = ManageCom;
        fm.all('InvoiceStartDate').value = "";                                  
        fm.all('InvoiceEndDate').value = "";

        var tSql = "select TaxpayerNo,TaxpayerName from LJInvoiceInfo where comcode = '"
                + ManageCom + "' "
                + " order by makedate desc with ur ";
        var arr = easyExecSql(tSql);
        if(arr != null)
        {
            fm.all('TaxpayerNo').value = arr[0][0];
            fm.all('TaxpayerName').value = arr[0][1];
        }
        fm.all("SFState").value = "0";
        fm.all("SFStateName").value = "收费发票";
    }
}
</script>