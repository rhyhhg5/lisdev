//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 申请汇总批次信息号。
 */
function applySerialnoHZ() {
	// 申请机构 申请机构页面录入什么机构就是什么机构
	var mManageCom = fm.ManageCom.value;
	// 申请日期
	var mApplyDate = fm.ApplyDate.value;
	// 批次名 可录入不是必须项
	var mSerialnoHZName = fm.SerialnoHZName.value;
	// 申请机构
	if (mManageCom == "" || mManageCom == null) {
		alert("申请机构不能为空");
		return;
	}
	// 申请日期
	if (mApplyDate == "" || mApplyDate == null) {
		alert("申请日期不能为空");
		return;
	}
	var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./PayUnionserialnoApplySave.jsp?ApplyDate=" + mApplyDate
			+ "&ManageCom=" + mManageCom + "&SerialnoHZName" + mSerialnoHZName;
	fm.submit();
}
/**
 * 查询已申请汇总批次号。
 */
function querySerialnoHzList() {
	// 汇总批次号
	var mSerialnoHZ = fm.SerialnoHZ.value;
	// 申请机构  页面录入的机构
	var mManageCom = fm.ManageCom.value;
	// 申请日期
	var mApplyDate = fm.ApplyDate.value;
	// 批次名 可以通过批次名称模糊查询
	var mSerialnoHZName = fm.SerialnoHZName.value;

	if (!verifyInput2()) {
		return false;
	}

	var tStrSql = "select TranBatch,TranBatchName,ApplyDate,ManageCom,Operator,SerialnoHZNum,cast(SerialnoHZMoney as decimal(30,2)) from LCSerialnoHZ where 1=1 ";
	if (mSerialnoHZ != "" && mSerialnoHZ != null) {
		tStrSql = tStrSql + " and TranBatch='" + mSerialnoHZ + "'";
	}
	if (mManageCom != "" && mManageCom != null) {
		tStrSql = tStrSql + " and ManageCom='" + mManageCom + "'";
	}
	if (mApplyDate != "" && mApplyDate != null) {
		tStrSql = tStrSql + " and ApplyDate='" + mApplyDate + "'";
	}
	if (mSerialnoHZName != "" && mSerialnoHZName != null) {
		tStrSql = tStrSql + " and TranBatchName like '%" + mSerialnoHZName
				+ "%'";
	}

	turnPage1.pageDivName = "divPaySerialnoHZListGridPage";
	turnPage1.queryModal(tStrSql, PaySerialnoHZListGrid);

	if (!turnPage1.strQueryResult) {
		alert("没有满足条件的批次信息！");
		return false;
	}

	return true;
}

/**
 * 进入汇总批次信息录入。
 */
function addPay() {
	var tRow = PaySerialnoHZListGrid.getSelNo() - 1;
	if (tRow == null || tRow < 0) {
		alert("请选择一条记录。");
		return false;
	}

	var tRowDatas = PaySerialnoHZListGrid.getRowData(tRow);

	var tSerialnoHZ = tRowDatas[0];

	var tStrUrl = "./PayUnionSerialnoTJInput.jsp" + "?SerialnoHZ=" + tSerialnoHZ;

	window.location = tStrUrl;
}

/**
 * 进入汇总批次信息修改。
 */
function updatePay() {
	var tRow = PaySerialnoHZListGrid.getSelNo() - 1;
	if (tRow == null || tRow < 0) {
		alert("请选择一条记录。");
		return false;
	}

	var tRowDatas = PaySerialnoHZListGrid.getRowData(tRow);

	var tSerialnoHZ = tRowDatas[0];

	var tStrUrl = "./PayUnionSerialnoUPInput.jsp" + "?SerialnoHZ=" + tSerialnoHZ;

	window.location = tStrUrl;
}

function afterSubmit(FlagStr, content, HZSerialno, mSerialnoHZName) {
	showInfo.close();
	fm.SerialnoHZ.value = HZSerialno;
	fm.SerialnoHZName.value = mSerialnoHZName;
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {

		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content+"批次号为："+HZSerialno;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		showDiv(operateButton, "true");
		showDiv(inputButton, "false");

	}
}