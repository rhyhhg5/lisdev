var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 
var turnPage4 = new turnPageClass(); 
var turnPage5 = new turnPageClass(); 

/*********************************************************************
 *  查询按钮实现
 *  参数  ：  1 为收费  2 为付费
 *
 **********************************************************************/
function easyQueryClick(method)
{
	if(method == 1){
		fm.feeFlag.value = "S";
	} else {
		fm.feeFlag.value = "F";
	}
	var no = fm.PayOrGetCode.value;
	var otherNo = fm.Otherno.value;
	var bankAccNo = fm.Bankaccno.value;
	
	if(isNull(no) && isNull(otherNo) && isNull(bankAccNo)){
  		alert("收付费号、业务号、客户开户银行至少录入一项！");
	  	return false;
	}
	if(method == 1){
		
		var spayPath = "";
		var tempfeePath = "";
		var contPath = "";
		
		if(!isNull(no)){
			spayPath = " and getnoticeno='" + no + "' ";
			tempfeePath = " and tempfeeno='" + no + "' ";
			if(no.indexOf("801") != -1 && no.length > 3){
				contPath = " and prtno='" + no.substring(0 ,no.length - 3) + "'";
			} else {
				contPath = " and 1=2 ";
			}
		}
		if(!isNull(otherNo)){
			spayPath += " and otherno='" + otherNo + "' ";
			tempfeePath += " and exists (select 1 from ljtempfee where tempfeeno=tfc.tempfeeno and otherno='" + otherNo + "') ";
			contPath += " and prtno='" + otherNo + "'";
		}
		if(!isNull(bankAccNo)){
			spayPath += " and bankaccno='" + bankAccNo + "' ";
			tempfeePath += " and bankaccno='" + bankAccNo + "' ";
			contPath += " and bankaccno='" + bankAccNo + "'";
		}
		
		var shouldSQL = "select getnoticeno, (case when bankcode is not null and bankaccno is not null and accname is not null then '银行转账' else '非银行转账' end), otherno, (case when othernotype in ('5', '9') then '首期缴费' when othernotype in ('1', '2') then '续期缴费' when othernotype in ('3', '10') then '保全缴费' when othernotype = 'Y' then '预付赔款回收' when othernotype = '16' then '先收费' else '其他' end), sumduepaymoney, bankcode, bankaccno, accname, case when cansendbank = '0' then '未加锁' when cansendbank is null then '未加锁' when cansendbank = '1' then '已加锁' else '其他' end, case when bankonthewayflag = '0' then '未在途' when bankonthewayflag is null then '未在途' when bankonthewayflag='1' then '在途' else '其他' end, varchar(startpaydate), varchar(paydate), serialno, (select bankcode from lybanklog where serialno = x.serialno), (Case When Exists (Select 1 From Lybanklog Where Senddate Is Null And Serialno = x.Serialno) Then '待发盘' When Exists (Select 1 From Lybanklog Where Dealstate Is Null And Serialno = x.Serialno) Then '待回盘' When Exists (Select 1 From Lybanklog Where Dealstate Is Not Null And Serialno = x.Serialno) Then '已回盘' Else '' End), (Select Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End From Lyreturnfrombank Rfb Inner Join Lybanklog bl On Rfb.Serialno = bl.Serialno Where bl.Serialno = x.Serialno And Rfb.Paycode = x.Getnoticeno Union Select Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End From Lyreturnfrombankb rfb Inner Join Lybanklog bl On Rfb.Serialno = bl.Serialno Where bl.Serialno = x.Serialno And rfb.Paycode = x.Getnoticeno) from (select getnoticeno, otherno, othernotype, sumduepaymoney, bankcode, bankaccno, accname, cansendbank, bankonthewayflag, startpaydate, paydate, send_query(getnoticeno) serialno " 
					+ " from ljspay " 
					+ " where 1=1 " + spayPath
					+ " and not exists (select 1 from ljtempfee where tempfeeno = ljspay.getnoticeno and confmakedate is not null) And Not Exists (Select 1 From Ljtempfeeclass Where Tempfeeno = Ljspay.Getnoticeno And paymode In ('2','3'))) as x " 
					+ " union all " 
					+ " select tempfeeno, (select codename from ldcode where codetype = 'paymode' and code = tfc.paymode), (select otherno from ljtempfee where tempfeeno = tfc.tempfeeno fetch first 1 rows only), '首期缴费', paymoney, bankcode, bankaccno, accname, '未加锁', '未在途', paydate, paydate + 1 year, '', '', '', '' " 
					+ " from ljtempfeeclass tfc " 
					+ " where 1=1 " + tempfeePath
					+ " and not exists (select 1 from ljspay where getnoticeno = tfc.tempfeeno) "
					+ " and paymode='4' and confmakedate is null and enteraccdate is null and confdate is null " 
					+ " union all " 
					+ " select trim(prtno) || '801', (select codename from ldcode where codetype = 'paymode' and code = lccont.paymode), prtno, '首期缴费', (select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0) from LCPol lcp where 1 = 1 and lcp.UWFlag in ('4', '9') and lcp.ContNo = lccont.contno), bankcode, bankaccno, accname,'未加锁','未在途', cvalidate, cvalidate + 1 year, '', '', '', '' " 
					+ " from lccont " 
					+ " where 1=1 " + contPath
					+ " and uwflag in ('4', '9') and conttype = '1' and appflag<>'1' and makedate > current date - 1 year and not exists (select 1 from ljspay where otherno = lccont.prtno) and not exists (select 1 from ljtempfee where otherno = lccont.prtno) " 
					+ " union all " 
					+ " select prtno || '801', (select codename from ldcode where codetype = 'paymode' and code = lcgrpcont.paymode), prtno, '首期缴费', prem, bankcode, bankaccno, accname, '未加锁', '未在途', cvalidate, cvalidate + 1 year, '', '', '', '' " 
					+ " from lcgrpcont " 
					+ " where 1=1 " + contPath
					+ " and appflag<>'1' and makedate > current date - 1 year and not exists (select 1 from ljspay where otherno = lcgrpcont.prtno) and not exists (select 1 from ljtempfee where otherno = lcgrpcont.prtno) with ur";
		
		var confSQL = "Select x.Tempfeeno, (Select Codename From Ldcode Where Codetype = 'paymode' And Code = x.Paymode), x.Otherno, (case x.tempfeetype when'1' then '首期收费' when'11' then '首期收费' when'2' then '续期收费' when'4' then '保全收费' when'7' then '保全收费' when'16' then '先收费' when'Y' then '预付赔款回收' when'YS' then '预收保费' when'17' then '银保通' when'20' then '健康管理' when'30' then '健康管理服务' when'31' then '复杂产品网销收费' else '其他' end ), x.Paymoney, x.Confmakedate, Case When Ap.Confdate Is Null Then '未核销' Else '已核销' End, Ap.Confdate, Case When Opm.Makedate Is Null Then '未打印' Else '已打印' End, Opm.Makedate, x.Bankaccno, x.Insbankaccno From (Select Tempfeeno, Paymode, (Select Otherno From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Otherno Is Not Null Fetch First 1 Rows Only) Otherno, (Select Tempfeetype From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Otherno Is Not Null Fetch First 1 Rows Only) Tempfeetype, Paymoney, Confmakedate, Confdate, Bankaccno, Insbankaccno " 
			+ " From Ljtempfeeclass Tfc " 
			+ " Where Confmakedate Is Not Null And Not Exists (Select 1 From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Tempfeetype In ('1', '11' ,'17','16','4','7')) " 
			+ tempfeePath
			+ " ) As x Left Join Ljapay Ap On Ap.Getnoticeno = x.Tempfeeno And Ap.sumactupaymoney >= 0 And Ap.Incomeno = x.Otherno Left Join Loprtmanager2 Opm On Opm.Otherno = Ap.Payno And Code = '35' And Stateflag = '1' " 
			+ " Union All " 
			+ " Select x.Tempfeeno, (Select Codename From Ldcode Where Codetype = 'paymode' And Code = x.Paymode), x.Otherno, (case x.tempfeetype when'1' then '首期收费' when'11' then '首期收费' when'2' then '续期收费' when'4' then '保全收费' when'7' then '保全收费' when'16' then '先收费' when'Y' then '预付赔款回收' when'YS' then '预收保费' when'17' then '银保通' when'20' then '健康管理' when'30' then '健康管理服务' when'31' then '复杂产品网销收费' else '其他' end ), x.Paymoney, x.Confmakedate, Case When Ap.Confdate Is Null Then '未核销' Else '已核销' End, Ap.Confdate, Case When Opm.Makedate Is Null Then '未打印' Else '已打印' End, Opm.Makedate, x.Bankaccno, x.Insbankaccno From (Select Tempfeeno, Paymode, (Select Otherno From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Otherno Is Not Null Fetch First 1 Rows Only) Otherno, (Select Tempfeetype From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Otherno Is Not Null Fetch First 1 Rows Only) Tempfeetype, Paymoney, Confmakedate, Confdate, Bankaccno, Insbankaccno " 
			+ " From Ljtempfeeclass Tfc " 
			+ " Where Confmakedate Is Not Null And Exists (Select 1 From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Tempfeetype In ('1', '11','17','16')) " 
			+ tempfeePath 
			+ " ) As x Left Join Ljapay Ap On Ap.Incomeno = x.Otherno And Ap.sumactupaymoney >= 0 And Duefeetype = '0' Left Join Loprtmanager2 Opm On Opm.Otherno = Ap.Payno And Code = '35' And Stateflag = '1' " 
			+ " Union All " 
			+ " Select x.Tempfeeno, (Select Codename From Ldcode Where Codetype = 'paymode' And Code = x.Paymode), x.Otherno, (case x.tempfeetype when'1' then '首期收费' when'11' then '首期收费' when'2' then '续期收费' when'4' then '保全收费' when'7' then '保全收费' when'16' then '先收费' when'Y' then '预付赔款回收' when'YS' then '预收保费' when'17' then '银保通' when'20' then '健康管理' when'30' then '健康管理服务' when'31' then '复杂产品网销收费' else '其他' end ), x.Paymoney, x.Confmakedate, Case When Ap.Confdate Is Null Then '未核销' Else '已核销' End, Ap.Confdate, Case When Opm.Makedate Is Null Then '未打印' Else '已打印' End, Opm.Makedate, x.Bankaccno, x.Insbankaccno From (Select Tempfeeno, Paymode, (Select Otherno From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Otherno Is Not Null Fetch First 1 Rows Only) Otherno, (Select Tempfeetype From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Otherno Is Not Null Fetch First 1 Rows Only) Tempfeetype, Paymoney, Confmakedate, Confdate, Bankaccno, Insbankaccno " 
			+ " From Ljtempfeeclass Tfc " 
			+ " Where Confmakedate Is Not Null And Exists (Select 1 From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Tempfeetype In ('4','7')) " 
			+ tempfeePath 
			+ " ) As x Left Join Ljapay Ap On Ap.Incomeno = x.Otherno And Ap.sumactupaymoney >= 0 Left Join Loprtmanager2 Opm On Opm.Otherno = Ap.Payno And Code = '35' And Stateflag = '1' " 
			+ "Union All "
			+ " Select Tempfeeno, (Select Codename From Ldcode Where Codetype = 'paymode' And Code = Tfc.Paymode), (Select Otherno From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Otherno Is Not Null Fetch First 1 Rows Only) Otherno, case (Select Tempfeetype From Ljtempfee Where Tempfeeno = Tfc.Tempfeeno And Otherno Is Not Null Fetch First 1 Rows Only) when'1' then '首期收费' when'11' then '首期收费' when'2' then '续期收费' when'4' then '保全收费' when'7' then '保全收费' when'16' then '先收费' when'Y' then '预付赔款回收' when'YS' then '预收保费' when'17' then '银保通' when'20' then '健康管理' when'30' then '健康管理服务' when'31' then '复杂产品网销收费' else'其他' end Tempfeetype, Paymoney, (Select Current Date From Dual Where 1 = 2), '未核销', (Select Current Date From Dual Where 1 = 2), '未打印', (Select Current Date From Dual Where 1 = 2), Bankaccno, Insbankaccno " 
			+ " From Ljtempfeeclass Tfc " 
			+ " Where Confmakedate Is Null And Paymode in ('3','2') " 
			+ tempfeePath  
			+ " With Ur";
					
		
		initYingGrid();
		initZanGrid();
		initPastGrid();
		initPolStatuGrid();
		initZongGrid();
		initMingGrid();
		fm.serialNo.value = "";
		
		turnPage1.queryModal(confSQL, ZanGrid); 
		turnPage.queryModal(shouldSQL, YingGrid);
		
		      
	} else if(method == 2) {
	
		var wherePath = "";
		
		if(!isNull(no)){
			wherePath = " and actugetno='" + no + "' ";
		}
		if(!isNull(otherNo)){
			wherePath += " and otherno='" + otherNo + "' ";
		}
		if(!isNull(bankAccNo)){
			wherePath += " and bankaccno='" + bankAccNo + "' ";
		}
		
		var strSQL3 = "select actugetno, (select codename from ldcode where codetype = 'paymode' and code = x.paymode), otherno, case when othernotype = '10' then '保全付费' when othernotype = '3' then '保全付费' when othernotype = '20' then '满期给付' when othernotype = 'Y' then '预付赔款' when othernotype in ('5', 'C') then '理赔' else '其他' end, sumgetmoney, bankcode, bankaccno, AccName, case when cansendbank = '0' then '未加锁' when cansendbank is null then '未加锁' when cansendbank = '1' then '已加锁' else '其他' end, case when bankonthewayflag = '0' then '未在途' when bankonthewayflag is null then '未在途' when bankonthewayflag='1' then '在途' else '其他' end, shoulddate, '', serialno, (select bankcode from lybanklog where serialno = x.serialno), (Case When Exists (Select 1 From Lybanklog Where Senddate Is Null And Serialno = x.Serialno) Then '待发盘' When Exists (Select 1 From Lybanklog Where Dealstate Is Null And Serialno = x.Serialno) Then '待回盘' When Exists (Select 1 From Lybanklog Where Dealstate Is Not Null And Serialno = x.Serialno) Then '已回盘' Else '' End), (Select Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End From Lyreturnfrombank Rfb Inner Join Lybanklog bl On Rfb.Serialno = bl.Serialno Where bl.Serialno = x.Serialno And Rfb.Paycode = x.Actugetno Union Select Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End From Lyreturnfrombankb rfb Inner Join Lybanklog bl On Rfb.Serialno = bl.Serialno Where bl.Serialno = x.Serialno And rfb.Paycode = x.Actugetno) from (select actugetno, paymode, otherno , othernotype, sumgetmoney, bankcode, bankaccno, AccName,cansendbank, bankonthewayflag, shoulddate ,send_query(actugetno) serialno " 
				+ " from ljaget " 
				+ " where 1=1 " 
				+ wherePath
				+ " and confdate is null " 
				+ ") as x with ur" ; 
		      
		var strSQL4 = "select actugetno, (select codename from ldcode where codetype = 'paymode' and code = ljfiget.paymode), otherno, case when othernotype = '10' then '保全付费' when othernotype = '3' then '保全付费' when othernotype = '20' then '满期给付' when othernotype = 'Y' then '预付赔款' when othernotype in ('5', 'C') then '理赔' else '其他' end, getmoney, confdate, '已核销', confdate, '', '', bankaccno, (select insbankaccno from ljaget where actugetno = ljfiget.actugetno) " 
				+ " from ljfiget "
				+ " where 1=1 " 
				+ wherePath
				+ "with ur" ;
	  
	  initYingGrid();
	  initZanGrid();
	  initPastGrid();
	  initPolStatuGrid();
	  initZongGrid();
	  initMingGrid();
	  
	  fm.serialNo.value = "";
	  
	  turnPage.queryModal(strSQL3, YingGrid);
	  turnPage1.queryModal(strSQL4, ZanGrid);
	}
}

/*********************************************************************
 *  既往批次信息查询
 *
 **********************************************************************/
 
function historyQuery(payCode) {
	//MODIFY BY JL  金联查询

	/*var sql = "select bl.serialno,bl.bankcode,rfb.paycode,rfb.polno,rfb.paymoney,rfb.accno,rfb.accname,bl.senddate,bl.returndate,'已回盘',Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End from lyreturnfrombank rfb"
			+ " inner join lybanklog bl on rfb.serialno=bl.serialno"
			+ " where rfb.paycode='" + payCode + "' and exists (select 1 from ldbank where bankcode=bl.bankcode and operator='sys') and not exists (select 1 from lysendtobank where serialno=rfb.serialno and paycode=rfb.paycode)"
			+ " union all"
			+ " select bl.serialno,bl.bankcode,rfb.paycode,rfb.polno,rfb.paymoney,rfb.accno,rfb.accname,bl.makedate,bl.modifydate,'已回盘',Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End from lyreturnfrombank rfb"
			+ " inner join lybanklog bl on rfb.serialno=bl.serialno"
			+ " where rfb.paycode='" + payCode + "' and exists (select 1 from ldbank where bankcode=bl.bankcode and operator='ybt') and not exists (select 1 from lysendtobank where serialno=rfb.serialno and paycode=rfb.paycode)"
			+ " union all"
			+ " select bl.serialno,bl.bankcode,rfb.paycode,rfb.polno,rfb.paymoney,rfb.accno,rfb.accname,bl.senddate,bl.returndate,'已回盘',Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End from lyreturnfrombankb rfb"
			+ " inner join lybanklog bl on rfb.serialno=bl.serialno"
			+ " where rfb.paycode='" + payCode + "' with ur";*/
	var sql = "select bl.serialno,bl.bankcode,rfb.paycode,rfb.polno,rfb.paymoney,rfb.accno,rfb.accname,bl.senddate,bl.returndate,'已回盘',Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End from lyreturnfrombank rfb"
		+ " inner join lybanklog bl on rfb.serialno=bl.serialno"
		+ " where rfb.paycode='" + payCode + "' and exists (select 1 from ldbank where bankcode=bl.bankcode and operator='sys') and not exists (select 1 from lysendtobank where serialno=rfb.serialno and paycode=rfb.paycode)"
		+ " union all"
		+ " select bl.serialno,bl.bankcode,rfb.paycode,rfb.polno,rfb.paymoney,rfb.accno,rfb.accname,bl.makedate,bl.modifydate,'已回盘',Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End from lyreturnfrombank rfb"
		+ " inner join lybanklog bl on rfb.serialno=bl.serialno"
		+ " where rfb.paycode='" + payCode + "' and exists (select 1 from ldbank where bankcode=bl.bankcode and operator='system') and not exists (select 1 from lysendtobank where serialno=rfb.serialno and paycode=rfb.paycode)"
		+ " union all"
		+ " select bl.serialno,bl.bankcode,rfb.paycode,rfb.polno,rfb.paymoney,rfb.accno,rfb.accname,bl.makedate,bl.modifydate,'已回盘',Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End from lyreturnfrombank rfb"
		+ " inner join lybanklog bl on rfb.serialno=bl.serialno"
		+ " where rfb.paycode='" + payCode + "' and exists (select 1 from ldbank where bankcode=bl.bankcode and operator='ybt') and not exists (select 1 from lysendtobank where serialno=rfb.serialno and paycode=rfb.paycode)"
		+ " union all"
		+ " select bl.serialno,bl.bankcode,rfb.paycode,rfb.polno,rfb.paymoney,rfb.accno,rfb.accname,bl.senddate,bl.returndate,'已回盘',Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = Rfb.Banksuccflag), '失败') End from lyreturnfrombankb rfb"
		+ " inner join lybanklog bl on rfb.serialno=bl.serialno"
		+ " where rfb.paycode='" + payCode + "' with ur";
	
	turnPage2.queryModal(sql, PastGrid);
}

/*********************************************************************
 *  查询应收、应付数据暂付数据点击触发函数
 *
 **********************************************************************/
function queryShouldOldMsg(){

	var payCode = YingGrid.getRowColData(YingGrid.getSelNo()-1 , 1);
	historyQuery(payCode);
	
	fm.feeState.value = "Should";
	feeStateQuery();
}

/*********************************************************************
 *  查询暂收、暂付数据点击触发函数
 *
 **********************************************************************/
function queryTrueOldMsg(){

	var payCode = ZanGrid.getRowColData(ZanGrid.getSelNo()-1 , 1);
	historyQuery(payCode);
	
	fm.feeState.value = "Conf";
	feeStateQuery();
}

/*********************************************************************
 *  明细信息查询
 *
 **********************************************************************/
function feeStateQuery(){
	initPolStatuGrid();
	fm.action  = "./FeeStateQuery.jsp";
	fm.submit();
}

/********************
 * 既往明细说明
 * 查询lyreturnfrombank 或者lyreturnfrombankb
 * ******************
 */
function JQueryDetailMsg(){

	var rowNum = PastGrid.getSelNo();
	var serialNo = PastGrid.getRowColData(rowNum-1 , 1); 
	
	fm.serialNo.value = serialNo;
	
	var sql = "Select Ly.Paycode, (Case Dealtype When 'S' Then (Select Otherno From Ljspay Where Getnoticeno = Ly.Paycode) When 'F' Then (Select Otherno From Ljaget Where Actugetno = Ly.Paycode) End), Ly.Paymoney, Ly.Bankcode, Ly.Accno, Ly.Accname, Ly.Comcode, (Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Ly.Bankcode And Code1 = Ly.Banksuccflag) From Lyreturnfrombank Ly Where 1 = 1 " 
			+ " And Serialno = '" + serialNo + "'" 
			+ " Union All "
			+ " Select Ly.Paycode, (Case Dealtype When 'S' Then (Select Otherno From Ljspay Where Getnoticeno = Ly.Paycode) When 'F' Then (Select Otherno From Ljaget Where Actugetno = Ly.Paycode) End), Ly.Paymoney, Ly.Bankcode, Ly.Accno, Ly.Accname, Ly.Comcode, (Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Ly.Bankcode And Code1 = Ly.Banksuccflag) From Lyreturnfrombankb Ly Where 1 = 1 " 
			+ " And Serialno = '" + serialNo + "' with ur";
			
	var logSql = "select " 
			+ "'" + serialNo + "'," 
			+ "'" + PastGrid.getRowColData(rowNum-1 , 2) + "'," 
			+ "'" + PastGrid.getRowColData(rowNum-1 , 8) + "'," 
			+ "'" + PastGrid.getRowColData(rowNum-1 , 9) + "'," 
			+ "'" + PastGrid.getRowColData(rowNum-1 , 10) + "'" 
			+ " from dual where 1=1";
	
	turnPage4.queryModal(logSql , ZongGrid);
	turnPage5.queryModal(sql , MingGrid);
	
}

function SerialOnClick(){
	var rowNum = ZongGrid.getSelNo();
	var serialNo = ZongGrid.getRowColData(rowNum-1 , 1); 
	
	fm.serialNo.value = serialNo;
}

/********************
 * 批次查询
 * ******************
 */
function findSeria(){

	initYingGrid();
	initZanGrid();
	initPastGrid();
	initPolStatuGrid();
	initZongGrid();
	initMingGrid();
	
	fm.PayOrGetCode.value = "";
	fm.Otherno.value = "";
	fm.Bankaccno.value = "";
	
	if(fm.serialNo.value === null || fm.serialNo.value == ""){
		alert("请录入待查询的批次号！");
		return false;
	}
	
	//分析：给一个批次号首先显示汇总信息
	var serialNo = fm.serialNo.value;
	
	var logSql = "select serialno,bankcode,senddate,returndate, Case When Senddate Is Null Then '待发盘' When Returndate Is Null Then '待回盘' Else '已回盘' End from lybanklog where serialno='" + serialNo + "' and exists (select 1 from ldbank where bankcode=lybanklog.bankcode and operator='sys')"
			+ " union all"
			+ " select serialno,bankcode,makedate,case when dealstate is null then (select current date from dual where 1=2) else modifydate end ,Case When Dealstate Is Null Then '待回盘' Else '已回盘' End from lybanklog where serialno='" + serialNo + "' and exists (select 1 from ldbank where bankcode=lybanklog.bankcode and operator='ybt')"
			+ " union all"
			+ " select serialno,bankcode,senddate,returndate,Case When Outfile Is Null Then '待生成文件' When Infile Is Null Then '待文件返回' When Dealstate Is Null Then  '待返回处理' Else '已回盘' End from lybanklog where serialno='" + serialNo + "' and not exists (select 1 from ldbank where bankcode=lybanklog.bankcode and operator in ('sys','ybt')) with ur";
	
	var sql = "Select Ly.Paycode, (Case Dealtype When 'S' Then (Select Otherno From Ljspay Where Getnoticeno = Ly.Paycode) When 'F' Then (Select Otherno From Ljaget Where Actugetno = Ly.Paycode) End), Ly.Paymoney, Ly.Bankcode, Ly.Accno, Ly.Accname, Ly.Comcode, Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = ly.Banksuccflag), '失败') End From Lyreturnfrombank Ly inner join lybanklog bl on ly.serialno=bl.serialno Where 1 = 1 " 
			+ " And bl.Serialno = '" + serialNo + "' and not exists (select 1 from lysendtobank where serialno='" + serialNo + "') " 
			+ " Union All "
			+ " Select Ly.Paycode, (Case Dealtype When 'S' Then (Select Otherno From Ljspay Where Getnoticeno = Ly.Paycode) When 'F' Then (Select Otherno From Ljaget Where Actugetno = Ly.Paycode) End), Ly.Paymoney, Ly.Bankcode, Ly.Accno, Ly.Accname, Ly.Comcode, Case When Exists (Select 1 From Ldbank Where Bankcode = Bl.Bankcode And (Case Bl.Logtype When 'S' Then Agentpaysuccflag When 'F' Then Agentgetsuccflag End) = trim(Banksuccflag) || ';') Then '成功' Else Nvl((Select Codename From Ldcode1 Where Codetype = 'bankerror' And Code = Bl.Bankcode And Code1 = ly.Banksuccflag), '失败') End From Lyreturnfrombankb Ly inner join lybanklog bl on ly.serialno=bl.serialno Where 1 = 1 " 
			+ " And bl.Serialno = '" + serialNo + "' "
			+ " Union All "
			+ " Select Ly.Paycode, (Case Dealtype When 'S' Then (Select Otherno From Ljspay Where Getnoticeno = Ly.Paycode) When 'F' Then (Select Otherno From Ljaget Where Actugetno = Ly.Paycode) End), Ly.Paymoney, Ly.Bankcode, Ly.Accno, Ly.Accname, Ly.Comcode, '' From lysendtobank Ly Where 1 = 1 " 
			+ " And Serialno = '" + serialNo + "' with ur";

	turnPage4.queryModal(logSql , ZongGrid);
	turnPage5.queryModal(sql , MingGrid);
}

/********************
 * 发盘报文下载
 * ******************
 */
function sendFileDownload() {
	var serialNo = fm.serialNo.value;
	
	if(fm.serialNo.value === null || fm.serialNo.value == ""){
		alert("请录入待查询的批次号！");
		return false;
	}
	
	var strSql = "select outfile from lybanklog where serialno = '"+serialNo+"' and bankcode in ('7705','7706','7707')";
	
	var arrResult = easyExecSql(strSql);
	if(!arrResult){
		alert("该批次非集中代收付批次，无法下载发盘报文");
		return false;
	} else if(arrResult[0][0] == null || arrResult[0][0] == "" || arrResult[0][0].indexOf("#") == -1){
		alert("该批次尚未发盘，无法下载发盘报文");
		return false;
	}
	
		
	if (!(serialNo == "" || serialNo == null)) { 
		
	    fm.all('filenamepath').value = arrResult[0][0];
	    
	    fm.action  = "./FinanceDownload.jsp";
	    fm.submit();
	  }
	  else {
	    alert("请输入下载批次的相关批次号！"); 
	  } 
}

/********************
 * 回盘报文下载
 * ******************
 */
function returnFileDownload() {
	var serialNo = fm.serialNo.value;
	var strSql = "select infile from lybanklog where serialno = '"+serialNo+"' and bankcode in ('7705','7706','7707')";
	
	var arrResult = easyExecSql(strSql);
	if(!arrResult){
		alert("该批次非集中代收付批次，无法下载回盘报文");
		return false;
	} else if(arrResult[0][0] == null || arrResult[0][0] == "" || arrResult[0][0].indexOf("#") == -1){
		alert("该批次尚未回盘，无法下载回盘报文");
		return false;
	}
	
		
	if (!(serialNo == "" || serialNo == null)) { 
		
	    fm.all('filenamepath').value = arrResult[0][0];
	    
	    fm.action  = "./FinanceDownload.jsp";
	    fm.submit();
	  }
	  else {
	    alert("请输入下载批次的相关批次号！"); 
	  } 
}

function isNull(value) {
	if(value == null || value == "" || value == "null"){
		return true;
	}
	return false;
}

/********************
 * 状态明细说明
 * ******************
 */
function stateMsghelp(){
	window.open("./StateMsgHelp.jsp");
}
/********************
 * 提盘规则说明
 * ******************
 */

function drawRulehelp(){
	window.open("./DrawRuleHelp.jsp");
}

function afterSubmit(info){
	PolStatuGrid.addOne("PolStatuGrid");
	var rowNum = PolStatuGrid.mulLineCount;
	PolStatuGrid.setRowColData(rowNum - 1, 1, info);
}