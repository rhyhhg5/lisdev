<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2013-11-26
		//创建人  ：y
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="AuditReportPrintInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<%@include file="AuditReportPrintInit.jsp"%>
	</head>
	<body onload="initElementtype();initForm();">
		<form method=post name=fm target="fraSubmit">

				<strong><IMG id="a1" src="../common/images/butExpand.gif"
						style="cursor:hand;" OnClick="showPage(this,divOperator);" />非现场业务审计数据表打印
				</strong>

			<Div id="divOperator" style="display: ''">
			   <font color="#FF0000" size="3">
    		   注意：因数据量较大原因，可能导致报表打印时间较长，请耐心等待，不要重复点击打印按钮！
    		   </font>
    		    <p>
			</Div>

			<p>
				<strong><IMG id="a2" src="../common/images/butExpand.gif"
						style="cursor:hand;" OnClick="showPage(this,divMonthPrint);" />请输入查询范围
				</strong>
			</p>

			<Div id="divMonthPrint" style="display: ''">
				<table class="common">
						<TR class=common>
							<TD class=title>
								机构代码
							</TD>
							<TD class=input>
								<Input class= "codeno"  name=ManageCom readonly="readonly" ondblclick="return showCodeList('comcode',[this,CompanyCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,CompanyCodeName],[0,1],null,null,null,1);" ><Input class=codename readonly="readonly" name=CompanyCodeName elementtype=nacessary>
							</TD>
							<TD class=title>
								起始日期
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="StartDate" name="StartDate" verify="起始日期|NOTNULL" elementtype=nacessary>
							</TD>
							<TD class=title>
								结束日期
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="EndDate" name="EndDate" verify="结束日期|NOTNULL" elementtype=nacessary>
							</TD>
						</TR>
						<TR class=common>
							<TD class=title>
								险种代码
							</TD>
							<TD class=input colspan="5">
								<Input class= "codeno"  name=RiskCode readonly="readonly" ondblclick="return showCodeList('riskprop2',[this,RiskCodeName],[0,1],null,null,'riskprop',1,400);" onkeyup="return showCodeListKey('riskprop2',[this,RiskCodeName],[0,1],null,null,'riskprop',1,400);" ><Input class=codename readonly="readonly" name=RiskCodeName >
							</TD>
				</table>
				<p>
				</p>

				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="PremIncomePrint" value="保费收入数据表"
					onclick="PremIncome_Print()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="ClaimPrint" value="赔款支出数据表"
					onclick="Claim_Print()" />
				<br />
				<br />	
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="MaturePayPrint" value="满期给付数据表"
					onclick="MaturePay_Print()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="CasualtyPayPrint" value="死伤医疗给付数据表"
					onclick="CasualtyPay_Print()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="SurrenderChargePrint" value="退保金数据表"
					onclick=" SurrenderCharge_Print()" />
				<br />
				<br />
			</Div>

			<input type="hidden" id="fmtransact" name="fmtransact" />
			<input type="hidden" name="Opt" />
		</form>

		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
