<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ModifyLJAGetSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  
<%
  System.out.println("\n\n---ModifyLJAGetSave Start---");
  System.out.println("ActuGetNo2:" + request.getParameter("ActuGetNo2"));
  System.out.println("PayMode:" + request.getParameter("PayMode"));
  System.out.println("BankCode:" + request.getParameter("BankCode"));
  System.out.println("AccName:" + request.getParameter("AccName"));
  System.out.println("AccNo:" + request.getParameter("AccNo"));

  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  
  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
  tLJAGetSchema.setActuGetNo(request.getParameter("ActuGetNo2"));
  tLJAGetSchema.setPayMode(request.getParameter("PayMode"));
  tLJAGetSchema.setBankCode(request.getParameter("BankCode"));
  tLJAGetSchema.setBankAccNo(request.getParameter("AccNo"));
  tLJAGetSchema.setAccName(request.getParameter("AccName"));
  LJAGetSet inLJAGetSet = new LJAGetSet();
  inLJAGetSet.add(tLJAGetSchema);

  VData inVData = new VData();
  inVData.add(inLJAGetSet);
  inVData.add(tGlobalInput);
  
  String Content = "";
  String FlagStr = "";
  
  ModifyLJAGetUI ModifyLJAGetUI1 = new ModifyLJAGetUI();

  if (!ModifyLJAGetUI1.submitData(inVData, "INSERT")) {
    VData rVData = ModifyLJAGetUI1.getResult();
    Content = " 处理失败，原因是:" + (String)rVData.get(0);
  	FlagStr = "Fail";
  }
  else {
    Content = " 处理成功! ";
  	FlagStr = "Succ";
  }

	System.out.println(Content + "\n" + FlagStr + "\n---ModifyLJAGetSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.easyQueryClick();
</script>
</html>
