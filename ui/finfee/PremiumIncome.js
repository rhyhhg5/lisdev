//               该文件中包含客户端需要处理的函数和事件
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if (CheckDate(fm.StartDay.value) && CheckDate(fm.EndDay.value))
	{
	  var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.submit(); //提交
	} 
}

//财务接口日结以方式来打印财务收费日结单 新增
function Print()
{
    fm.action="../finfee/PremiumIncomeSave.jsp";
    fm.target="finfee";
    fm.fmtransact.value="PRINT";
    submitForm();
    showInfo.close();
}

// ----日期格式的验证------------------
function CheckDate(strDate)
{
    var reg=/^(\d{4})([-])(\d{2})([-])(\d{2})/;
    if(!reg.test(strDate))
    {
        alert("日期格式不正确!\n 正确格式为:yyyy-mm-dd");
        return false;
    }
    return true;
}
 