<html> 
<%
//程序名称：BatchPayInput.jsp
//程序功能：财务批量付费
//创建日期：2008-07-23 
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
	<SCRIPT src="BatchPayInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BatchPayInit.jsp"%>
</head>
<body  onload="initForm();" >
<Form action="" method=post name=fm target="fraSubmit">
		<Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</TD>
    		<TD class= titleImg>
    			 查询条件
    		</TD>
    	</TR>
    </Table> 
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>业务类型</TD>
          <TD  class= input>
          	<Input class=codeNo name=paytype  CodeData="0|^1|理赔给付^2|保全给付^3|生存领取^4|暂交费退费^5|红利给付^6|其他退费" ondblclick="return showCodeListEx('paytype',[this,paytypeName],[0,1]);" onkeyup="return showCodeListKeyEx('paytype',[this,paytypeName],[0,1]);"><input class=codename name=paytypeName readonly=true>       
          </TD>
          <TD  class= title>付费方式</TD>
          <TD  class= input>
          	<Input class=codeNo name=PayMode  ondblclick="return showCodeList('paymode2',[this,PayModeName],[0,1]);" onkeyup="return showCodeListKey('paymode2',[this,PayModeName],[0,1]);"><input class=codename name=PayModeName readonly=true > 
          </TD>
          <TD  class= title>对方银行编码</TD>
          <TD  class= input>
          	<Input class=codeNo name=bankcode  ondblclick="return showCodeList('bank',[this,bankname],[0,1]);" onkeyup="return showCodeListKey('bank',[this,bankname],[0,1]);"><input class=codename name=bankname readonly=true >
          </TD> 
        </TR>
        <TR>
        	<TD  class= title> 起始日期</TD>
	        <TD  class= input>	<Input class= "coolDatePicker" dateFormat="short" name=StartDate >  </TD>        
	        <TD  class= title> 截至日期</TD> 
	        <TD  class= input> <Input class= "coolDatePicker" dateFormat="short" name=EndDate > </TD>
          <TD  class= title>给付批次号</TD>
          <TD  class= input>
          	<Input class=common name="HCNo" >
          </TD> 
      	</TR>
    </Table>  
      <INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="下  载" Class=cssButton TYPE=button onclick="download();"> 
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</TD>
    		<TD class= titleImg>
    			 应付总表信息
    		</TD>
    	</TR>
    </Table>  
  <input type=hidden name=ComCode> 
  <input type=hidden name=downloadSql>	
 <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanLJAGetGrid" ></span> 
  	    </TD>
      </TR>
    </Table>					
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div>					
  <Div id='Savebutton' style='display:' >   
      <INPUT VALUE="保  存" Class=cssButton TYPE=button onclick="submitForm();">
 	</Div>
</Form>    
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
 