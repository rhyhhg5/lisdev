
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body >
<Form method=post name=fm target="fraSubmit">
	<br>
	<br> 
	<font size=3>
	<b>收付费方式说明</b> 
		<p></p>
		&nbsp;&nbsp;（一）现金（核心业务系统中的方式代码为1）：主要是指现钞的概念，在实际操作中，将客户直接在我公司柜面用现金投保或领取现金退保金及理赔金的行为定义为现金交易方式。使用填明“现金”字样的银行汇票、银行本票和用于支取现金的支票等现金票据的解付行为也包含在此类中。
			<p></p>
		&nbsp;&nbsp;（二）支票（核心业务系统中的方式为2、3）：包括使用现金支票和转帐支票以银行为中介进行转帐的收付费方式。。	
			<p></p>
		&nbsp;&nbsp;（三）POS机方式（核心业务系统中的方式代码为6）：是指客户持个人银行卡到我公司柜台在POS机终端刷卡进行缴费的方式。
			<p></p>
		&nbsp;&nbsp;（四）银行批量代收代付方式（核心业务系统中的方式代码为4）：是指我公司向银行提供报盘文件和接收银行回盘文件进行收付费的方式（直接在银行接口操作，不需要在财务收付费界面选择该方式）。
		<p></p>
		&nbsp;&nbsp;（五）银行汇款（核心业务系统中的方式代码为11）：包括信汇、电汇。
		<p></p>
		&nbsp;&nbsp;（六）其他银行代收代付方式（核心业务系统中的方式代码为12）：包括 ：<p></p>
    &nbsp;&nbsp;&nbsp;&nbsp;  1、先将现金送存银行，持现金送款单到我公司进行缴费的方式。<p></p>
    &nbsp;&nbsp;&nbsp;&nbsp;  2、 通过向银行提供客户勾连信息，使用网上银行路径进行直接收费和付费的方式。<p></p>
    &nbsp;&nbsp;&nbsp;&nbsp;  3、以上未涵盖且以银行为中介进行存取的其他方式。
    <p></p>
		&nbsp;&nbsp;（七）赠送保费（核心业务系统中的方式代码为13）
		<p></p>
		&nbsp;&nbsp;（八）内部转账（核心业务系统中的方式代码为5）
		<p></p>
		&nbsp;&nbsp;（九）应收保费（核心业务系统中的方式代码为10）
		<p></p>
		&nbsp;&nbsp;（十）其他方式（核心业务系统中的方式代码为9） 

</font>
	<table class= common>
		<tr><td align=center><input type =button class=cssButton value="关 闭" onclick="window.close();"></td></tr>
</table>

</body>
</html>
