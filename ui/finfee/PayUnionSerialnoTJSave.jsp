<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<%@page import="com.sinosoft.lis.pubfun.PubSubmit"%>
<%@page import="com.sinosoft.lis.pubfun.MMap"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<title>Insert title here</title>
</head>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.certifybusiness.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.LCCoInsuranceParamSchema"%>
<%@page import="com.sinosoft.lis.vschema.LCCoInsuranceParamSet"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.finfee.*"%>

<%
	System.out.println("PayUnionSerialnoTJSave.jsp Begin ...");

	String FlagStr = "Succ";
	String Content = "";
	LCPaySerialnoHZSet mLCPaySerialnoHZSet = new LCPaySerialnoHZSet();
	PubSubmit tPubSubmit = new PubSubmit();
	

	CErrors tError = null;

	GlobalInput tG = (GlobalInput) session.getValue("GI");
	VData cInputData = new VData();

	String mSerialno = request.getParameter("Serialno");
	String tChk[] = request.getParameterValues("InpLJFIGetGridChk");
	String tActuGetNo[] = request.getParameterValues("LJFIGetGrid1");
	String tOtherno[] = request.getParameterValues("LJFIGetGrid2");
	String tGetMoney[] = request.getParameterValues("LJFIGetGrid4");
	String tPayMode[] = request.getParameterValues("LJFIGetGrid5");
	String tGetBankAccName[] = request.getParameterValues("LJFIGetGrid6");//对方银行账户名
	String tDrawer[] = request.getParameterValues("LJFIGetGrid7");//领取人
	String tConfdate[] = request.getParameterValues("LJFIGetGrid8");//到账日期
	String tAgentComCode[] = request.getParameterValues("LJFIGetGrid9");//给付机构编码
	String tManagecom[] = request.getParameterValues("LJFIGetGrid10");//机构编码
	String tMakeDate[] = request.getParameterValues("LJFIGetGrid11");//结算日期
	String tRiskCode[] = request.getParameterValues("LJFIGetGrid12");//产品代码，险种编码
	double sumMoney = 0.0;

	 for (int i = 0; i < tChk.length; i++) {
		if (tChk[i] != null && tChk[i].equals("1")) {
			LCPaySerialnoHZSchema mLCPaySerialnoHZSchema = new LCPaySerialnoHZSchema();
			mLCPaySerialnoHZSchema.setTranSerial(tActuGetNo[i]);
			mLCPaySerialnoHZSchema.setTranBatch(mSerialno);
			//第二版机构编码
			//mLCPaySerialnoHZSchema.setPk_Org(tAgentComCode[i]);
			mLCPaySerialnoHZSchema.setPk_Org(tManagecom[i]);
			mLCPaySerialnoHZSchema.setVDate(tMakeDate[i]);
			mLCPaySerialnoHZSchema.setSrcSysItem("01 核心系统");
			mLCPaySerialnoHZSchema.setBusiDate(tConfdate[i]);
			mLCPaySerialnoHZSchema.setCustName(tDrawer[i]);
			mLCPaySerialnoHZSchema.setLocalTotalAmt(tGetMoney[i]);
			String Riskcode ;
			if(tRiskCode[i].length()>4){
				Riskcode =  tRiskCode[i].substring(0, 4);
			} else {
				Riskcode =  tRiskCode[i];
			}
		System.out.println("险种为："+Riskcode);
			mLCPaySerialnoHZSchema.setProCode(Riskcode);
			mLCPaySerialnoHZSchema.setOperator(tG.Operator);
			mLCPaySerialnoHZSchema.setMakeDate(PubFun.getCurrentDate());
			mLCPaySerialnoHZSchema.setMakeTime(PubFun.getCurrentTime());
			mLCPaySerialnoHZSchema.setState("N");
			mLCPaySerialnoHZSchema.setBusiNo("22");//收支项目22代表手续费
			sumMoney = sumMoney + mLCPaySerialnoHZSchema.getLocalTotalAmt();
			mLCPaySerialnoHZSet.add(mLCPaySerialnoHZSchema);
		}
	} 
	System.out.println(mLCPaySerialnoHZSet.size() + "mLCPaySerialnoHZSet的长度");
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("mLCPaySerialnoHZSet", mLCPaySerialnoHZSet);
	VData tVData = new VData();
	tVData.add(tTransferData);
	PayUnionSerialnoTJUI tPayUnionserialnoTJUI = new PayUnionSerialnoTJUI();
	if (!tPayUnionserialnoTJUI.submitData(tVData, "")) {
		Content = " 批次关联失败，原因是: "
				+ tPayUnionserialnoTJUI.mErrors.getFirstError();
		FlagStr = "Fail";
	} else {
		Content = " 批次关联成功！";
		FlagStr = "Succ";

	}

	System.out.println("PayUnionSerialnoTJSave.jsp End ...");
%>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
