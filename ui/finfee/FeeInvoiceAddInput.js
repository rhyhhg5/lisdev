var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

function QueryBankButt()
{
	initQueryLDBankGrid();
	
	var sql="";
	if(fm.mInvoiceName.value!=""){
	sql = "select distinct riskwrapcode,riskwrapname,riskwrapname from ldriskwrap where riskwrapcode='"+fm.mInvoiceName.value+"' with ur";
	}
	           
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(sql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    QueryLDBankGrid.clearData('QueryLDBankGrid');  
    alert("没有查询到数据！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = QueryLDBankGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = sql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
}

//显示修改明细
function ShowDetail(){
	
	 var tSel = 0;
	 tSel = QueryLDBankGrid.getSelNo();
		try
		{
			if (tSel !=0 )
			   divLLMainAskInput5.style.display = "";
			else
				 divLLMainAskInput5.style.display = "none";			 
	      fm.all('repWrapName').value=QueryLDBankGrid.getRowColData(tSel-1,1);
		  fm.all('repInvoiceName').value=QueryLDBankGrid.getRowColData(tSel-1,2);
		}
		catch(ex)
		{
			alert( "没有发现父窗口的接口" + ex );
		}
}

//修改数据
function RepBankButt()
{   
	if (fm.repWrapName.value ==""||fm.repWrapName.value == null)
	{
		alert('套餐编码不能为空！');
		return;
	}
	if (fm.repInvoiceName.value ==""||fm.repInvoiceName.value == null)
	{
		alert('发票打印套餐名称不能为空！');
		return;
	}		
	fm.submit();	
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, tCode )
{
	  if (FlagStr == "Fail" )
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }
	  else
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
	  	//parent.fraInterface.initForm();
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	    //执行下一步操作
	    fm.repWrapName.value="";
	    fm.repInvoiceName.value="";
	  }
	
}