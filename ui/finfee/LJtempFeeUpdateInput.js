var showInfo;

var turnPage = new turnPageClass(); 

function easyQueryClick()
{
	if(fm.StartDate.value=='' || fm.EndDate.value==''){
		alert('起始日期或截至日期不能为空!');
		return false;
	}
	
	var strWhere="";
 	if(fm.CertificateNo.value!=''){
 		strWhere+=" and  b.tempfeeno='"+fm.CertificateNo.value+"' ";
 	}
 	if(trim(fm.ThisOtherNo.value)!=''){
 		strWhere+=" and  a.otherno='"+fm.ThisOtherNo.value+"' ";
 	}
 	if(fm.ByPayMode.value!=''){
 		strWhere+=" and  b.PayMode='"+fm.ByPayMode.value+"' ";
 	}

 	strWhere+=" and b.confmakedate>='"+fm.StartDate.value+"' and b.confmakedate<='"+fm.EndDate.value+"' ";

	var strSQL="select distinct a.tempfeeno,a.otherno,b.paymode,b.bankcode,b.bankaccno,b.insbankcode,b.insbankaccno,b.confmakedate,a.agentcode from ljtempfee a,ljtempfeeclass b where a.tempfeeno=b.tempfeeno  and b.confmakedate is not null and b.confmakedate<=current date and b.paymoney>=0 "
			+strWhere;	
			strSQL+=getWherePart('b.ManageCom','ComCode',"like");	
			
	turnPage.queryModal(strSQL, RecoilGrid); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

function save(){
	var tSel = RecoilGrid.getSelNo();
	if(tSel==0){
		alert('请选择一条记录');
		return false;
	}
	if(fm.TempfeeNo.value==''){
		alert('暂收号不能为空！');
		return false;
	} 
    if(fm.AgentCode.value==''){
	 alert('业务员不能为空！');
		return false;
     }
	
	if(confirm('是否确定对该交费信息进行修改')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.OperateType.value="GET";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}


function getInfo(){
	var tSel = RecoilGrid.getSelNo();
	var tOtherNo=RecoilGrid.getRowColData(tSel-1,1);
	var tBankCode=RecoilGrid.getRowColData(tSel-1,4);
	var tBankAccNo=RecoilGrid.getRowColData(tSel-1,5);
	var tAgentCode=RecoilGrid.getRowColData(tSel-1,9);
	fm.TempfeeNo.value=tOtherNo;
	fm.BankCode.value=tBankCode;
	fm.AccNo.value=tBankAccNo;
	fm.AgentCode.value=tAgentCode;
	fm.OtherNo.value=tOtherNo;
}
        

