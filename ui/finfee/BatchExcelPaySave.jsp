
<%
//程序功能：
//创建日期：2012-2-14
//创建人  ：zcx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.finfee.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%
    //接收信息，并作校验处理。
    //输入参数
    // Variables
int count=0;      
String FileName = request.getParameter("ImportFile");       
String ImportPath = request.getParameter("ImportPath");
System.out.println("...FileName:" + FileName );
System.out.println("...ImportPath:" + ImportPath );
String path = application.getRealPath("").replace('\\','/')+'/';    
ImportPath= path + ImportPath  ;
    
System.out.println("...pathL:" + path );
System.out.println("...开始上载文件");        
System.out.println("ImportPath .." + ImportPath );
// Initialization
DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
fu.setSizeMax(10000000);
// maximum size that will be stored in memory?
// 设置最多只允许在内存中存储的数据,单位:字节
fu.setSizeThreshold(4096);
// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
fu.setRepositoryPath(path+"temp");
//开始读取上传信息
System.out.println(path);
List fileItems = null;
try{
 fileItems = fu.parseRequest(request);
}
catch(Exception ex)
{
    ex.printStackTrace();
}
// 依次处理每个上传的文件
Iterator iter = fileItems.iterator();
System.out.println("进入银行编码信息导入");
while (iter.hasNext()) {
  FileItem item = (FileItem) iter.next();
  //忽略其他不是文件域的所有表单信息
  if (!item.isFormField()) {
    String name = item.getName();
    System.out.println(name);
    long size = item.getSize();
    if((name==null||name.equals("")) && size==0)
      continue;
    //ImportPath= path + ImportPath;
    FileName = name.substring(name.lastIndexOf("\\") + 1);
    System.out.println("importpath:"+ImportPath + FileName);
    //保存上传的文件到指定的目录
    try {
      item.write(new File(ImportPath + FileName));
      count = 1;
    } catch(Exception e) {
        e.printStackTrace();
      System.out.println("upload file error ...");
    }
  }
}
System.out.println("end 上传");
    
    //输出参数
    CErrors tError = null;
    String tRela  = "";                
    String FlagStr = "Fail";
    String Content = "";
    String Result="";
    
    TransferData tTransferData = new TransferData();             
  
  System.out.println("----FileName:" + FileName);
  boolean res = true;

 //读文件，存数据库
 BatchPayReadExcelUI BatchPayReadExcelUI=new BatchPayReadExcelUI();
 FileName=ImportPath+FileName;
 TransferData transferData1 = new TransferData();
  transferData1.setNameAndValue("fileName", FileName);
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");

  VData inVData = new VData();
  inVData.add(transferData1);
  inVData.add(tGlobalInput);
  
   Content = "";
   FlagStr = "";

  if (!BatchPayReadExcelUI.submitData(inVData, "READ")) {
    System.out.println("BatchPayExcelBL处理失败");
    VData rVData = BatchPayReadExcelUI.getResult();
    Content = " 处理失败，原因是:" + (String)rVData.get(0);
    FlagStr = "Fail";
  }
  else {
    Content = " 处理成功! ";
    FlagStr = "Succ";
  }
  Result=BatchPayReadExcelUI.getActuGetNo();
  System.out.println(Result);
  System.out.println(Content + "\n" + FlagStr + "\n---BatchExcelPaySave End---\n\n");
%>
<html>
    <script language="javascript">

  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");

</script>
</html>

