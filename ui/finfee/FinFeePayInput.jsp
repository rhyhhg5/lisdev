<html> 
<%
//程序名称：FinFeePayInput.jsp
//程序功能：财务付费表的输入
//创建日期：2002-07-12 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="FinFeePayInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FinFeePayInit.jsp"%>
</head>
<body  onload="initForm();" >
<Form action="./FinFeePaySave.jsp" method=post name=fm target="fraSubmit">
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>给付号码</TD>
          <TD  class= input><Input class= common name=qActuGetNo ></TD>
          <TD  class= title>业务序号</TD>
          <TD  class= input><Input class="common" name=qOtherNo ></TD>
          <TD  class= title>付费方式</TD>
          <TD  class= input><Input class=codeNo name=qPayMode  ondblclick="return showCodeList('paymode2',[this,qPayModeName],[0,1]);" onkeyup="return showCodeListKey('PayMode2',[this,qPayModeName],[0,1]);"><input class=codename name=qPayModeName readonly=true ></TD> 
        </TR>
      	<TR  class= common>
          <TD  class= title colspan=2><INPUT TYPE="checkBox" NAME="OtherCom" value="01">异地代付</TD>
        </TR>
    </Table>  
     <Div  id= "ujsDiv"  style= "display: ''">
     <INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="批量给付" Class=cssButton TYPE=button onclick="BatchPay();"> 
      <INPUT VALUE="待付费查询" Class=cssButton TYPE=button onclick="PayQueryClick();"> 
      
      <!-- 2010年4月20日添加  -->
      <INPUT VALUE="手续费付费" Class=cssButton TYPE=button onclick="BatchPoundagePay();">
      <!-- 2013年4月24日添加  --> 
      <INPUT VALUE="批量导入" Class=cssButton TYPE=button onclick="batchPay();"> 
      </Div>
      <Div  id= "jsdiv"  style= "display: ''">
      <INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="批量给付" Class=cssButton TYPE=button onclick="BatchPay();"> 
      <INPUT VALUE="待付费查询" Class=cssButton TYPE=button onclick="PayQueryClick();"> 
      
      <!-- 2010年4月20日添加  -->
      <INPUT VALUE="手续费付费" Class=cssButton TYPE=button onclick="BatchPoundagePay();">
      <!-- 2013年4月24日添加  --> 
      <INPUT VALUE="批量导入" Class=cssButton TYPE=button onclick="batchPay();"> 
     
      <INPUT VALUE="江苏平台手续费付费" Class=cssButton TYPE=button onclick="JSBatchPoundagePay();">
      </div>
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</TD>
    		<TD class= titleImg>
    			 应付总表信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanQueryLJAGetGrid" ></span> 
  	    </TD>
      </TR>
    </Table>					
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div>					
<hr>
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</TD>
    		<TD class= titleImg>
    			 付费明细
    		</TD>
    	</TR>
    </Table>    	
     <Table class= common>                             
          <TD  class= title>
            给付号码
          </TD>  
          <TD  class= input>
            <Input class= common name=ActuGetNo id="ActuG" readonly >
          </TD>
          <td class= title></td ><td class= input></td>
     </Table>   
     <Table class= common>                 
       <TR  class= common> 
          <TD  class= title>
            业务序号
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo id="PolNoId" verify="批单号码|notnull" readonly >
          </TD>                       
          <TD  class= title>
            付费方式
          </TD>          
          <TD  class= input>
            <Input class=codeNo name=PayMode verify="付费方式|notnull" ondblclick="return showCodeList('PayMode2',[this,PayModeName],[0,1]);" onkeyup="return showCodeListKey('PayMode2',[this,PayModeName],[0,1]);" onchange="showBankAccNo();"><input class=codename name=PayModeName readonly=true ><input type =button class=cssButton value="方式说明" onclick="payModeHelp();"> 
          </TD>                     
       </TR>
       <TR  class= common>                    
          <TD  class= title>
            给付金额
          </TD>           
          <TD  class= input>
            <Input class= common name=GetMoney verify="给付金额|notnull&num" readonly >
          </TD>          
          <TD  class= title>
          	<div id="divEnterAccTitle" style="display:">
            	财务到帐日期
          	</div>
          </TD>
          <TD  class= input>
          	<div id="divEnterAccDate" style="display:">
            	<Input class="coolDatePicker"   name=EnterAccDate 
<%	
	if(updateFlag==null)
	{
%>            	
            	verify="财务到帐日期|notnull&date" 
<%
	}
%> 
							>
          	</div>
          </TD>  
        </TR> 
                                                   	                  	 
     </TABLE> 
        <Div id='LinQuDiv' style='display:' >  
        <table class=common>                       
      <TR  class= common>
           <TD  class= title>
            领取人
          </TD>  
          <TD  class= input>
            <Input class= common name=Drawer id=Drawer_Id  >
          </TD>                                   
          <TD  class= title>
            领取人身份证号
          </TD>
          <TD  class= input>
            <Input class= common  name=DrawerID id=DrawerID_Id >
          </TD>
        </TR>   
        </table>
       </Div> 
                                      
     <Div id='divBankAccNo' style='display:' >
       <table class=common>
      <TR  class= common>
         <TD  class= title>
            对方开户银行
         </TD>  
         <TD  class= input>
	       <Input class=codeNo name=BankCode  ondblclick="querybank();" onkeyup="querybank();"><input class=codename name=BankCodeName readonly=true >                        
         </TD> 
         <TD class= title>
           票据号
         </TD>
         <TD class= input>
           <Input class = common name=ChequeNo>
         </TD>
       </TR>   
       <TR class=common>                                  
         <TD  class= title>
            对方银行帐号
         </TD>
          <TD  class= input>
            <Input class= common  name=BankAccNo >
          </TD>
          <TD class=title>
            对方银行帐户名
          </TD>
          <TD class=input>
            <Input class= common  name=AccName  id=AccName_Id >
          </TD>
        </TR> 
        <TR class=common>  
          <TD  class= title>
              本方银行
          </TD>
          <TD  class= input>
          <Input NAME=getbankcode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('paybankcode',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('paybankcode',[this,BankCodeName],[0,1]);" readonly=true><input class=codename name=BankCodeName readonly=true >
          </TD>
          <TD  class= title>
             本方银行账号
          </TD>
          <TD  class= input>
            <Input NAME=getbankaccno CodeData="" MAXLENGTH=20 CLASS=code ondblclick="return showCodeList('paybankaccno',[this],[0],null,fm.getbankcode.value,'bankcode');" onkeyup="return showCodeListKey('paybankaccno',[this],[0],null,fm.getbankcode.value,'bankcode');" readonly=true>
          </TD>
        </TR> 
        </table> 
      </Div>
      <Div id='Savebutton' style='display:' >   
      <INPUT VALUE="保  存" Class=cssButton TYPE=button onclick="submitForm();">
      </Div>
      <Div id='Updatebutton' style='display:none' >   
      <INPUT VALUE="修  改" Class=cssButton TYPE=button onclick="updateClick();"> 
      </Div>
       <hr>
       <br>
       <div align=right>
            <Input class=cssButton style="width:120px" type=button value="既往给付信息查询" onclick="queryLJAGet();">
      </div>   
                 
     <Div style='display:none'>
       <TR  class= common>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
             <Input class="code" name=AgentCode >
          </TD>           
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly tabindex=-1 name=AgentGroup  >
          </TD>
      </TR>      
        <TR  class= common>          
          <TD  class= title>
            操作员
          </TD> 
          <TD  class= input>
            <Input class="readonly"  readonly tabindex=-1 name=Operator  verify="操作员|notnull">
          </TD>                             
          <TD  class= title>
            操作时间
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly tabindex=-1 name=ModifyDate   verify="操作时间|notnull">
          </TD>
       </TR>
       
     </Div>
          <TD  class= input>
            <input type=hidden name=ModifyTime>
            <input type=hidden name=ComCode>
          </TD>   
            
          
	<input name="UpFlag" type="hidden" value = "<%=updateFlag%>">
		<input name="InsBankAccNo" type="hidden" value='1'>
	    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</TD>
    		<TD class= titleImg>
    			 给付信息打印
    		</TD>
    	</TR>
    </Table> 
    <table  class= common >
	   <TR>
          <TD  class= title>
            开始日期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=SDate  verify="起期|DATE"   >  
          </TD>
           <TD  class= title>
            截至日期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=EDate  verify="止期|DATE"   >  
          </TD>
          <TD  class= input>
          <INPUT VALUE="打印给付信息" Class=cssButton TYPE=button onclick="printfCurrentData();">
          </TD> 
       </TR>
       </table>
</Form>    

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 