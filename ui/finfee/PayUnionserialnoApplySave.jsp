<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<title>Insert title here</title>
</head>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>  
<%
System.out.println("PayUnionserialnoApplySave.jsp start ...");

CErrors tError = null;
String FlagStr = "Fail";
String Content = "";
String HZSerialno = "";
GlobalInput tG = (GlobalInput)session.getValue("GI");

String tApplyDate = request.getParameter("ApplyDate");
String tManageCom = request.getParameter("ManageCom");
String tSerialnoHZName = request.getParameter("SerialnoHZName");
//外侧和本地不一致
//String str = new String(tSerialnoHZName.getBytes("iso8859-1"),"GBK");
String str = tSerialnoHZName;
String tSerialnoHZ = null;
String chk = "";

try
{

    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ApplyDate", tApplyDate);
    tTransferData.setNameAndValue("ManageCom", tManageCom);
    tTransferData.setNameAndValue("SerialnoHZName", tSerialnoHZName);
    tTransferData.setNameAndValue("Operator", tG.Operator);
    
    tVData.add(tTransferData);
    tVData.add(tG);
    
    PayUnionserialnoApplyUI tPayUnionserialnoApplyUI = new PayUnionserialnoApplyUI();
    if(!tPayUnionserialnoApplyUI.submitData(tVData, ""))
    {
        Content = " 批次申请失败，原因是: " + tPayUnionserialnoApplyUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 批次申请成功！";
        FlagStr = "Succ";
        HZSerialno = tPayUnionserialnoApplyUI.getHZSerialno();
        
    }
}
catch (Exception e)
{
    Content = " 批次申请失败.";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("PayUnionserialnoApplySave.jsp end ...");

%>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=HZSerialno%>","<%=str%>");
</script>
</html>