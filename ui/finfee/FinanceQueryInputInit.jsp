
<%
	//程序名称：GrpPolQueryInit.jsp
	//程序功能：
	//创建日期：2003-03-14 
	//创建人  ：lh
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>

<script language="JavaScript">


function initInpBox()
{ 

  try
  {                                   

    fm.all('PayOrGetCode').value ='';	
    fm.all('Otherno').value = '';   
    fm.all('Bankaccno').value = '';
    fm.all('Serialno').value = '';
  }
  catch(ex)
  {
    alert("在FinanceQueryInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm()
{
  try
  {
    initInpBox();
    initYingGrid();
    initZanGrid();
	initPastGrid();
	initPolStatuGrid();
	initZongGrid();
	initMingGrid();
	
  }
  catch(re)
  {
    alert("FinanceQueryInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 应付显示数据初始化
function initYingGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="收/付费号";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[2]=new Array();
      iArray[2][0]="收/付费方式";         		    //列名
      iArray[2][1]="70px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="业务号";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[4]=new Array();
      iArray[4][0]="业务类型";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[5]=new Array();
      iArray[5][0]="金额";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="客户开户银行";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[7]=new Array();
      iArray[7][0]="开户账号";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0; 

      iArray[8]=new Array();
      iArray[8][0]="开户名";         		//列名
      iArray[8][1]="50px";            			//列最大值
      iArray[8][3]=0; 


	  iArray[9]=new Array();
      iArray[9][0]="是否加锁";         		//列名
      iArray[9][1]="50px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0; 

      iArray[10]=new Array();
      iArray[10][0]="是否在途";         		//列名
      iArray[10][1]="50px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0; 

      iArray[11]=new Array();
      iArray[11][0]="应收/付费日期";         		//列名
      iArray[11][1]="80px";            		   //列宽
      iArray[11][2]=100;            		//列最大值
      iArray[11][3]=0; 


      iArray[12]=new Array();
      iArray[12][0]="最晚缴费日期"; 
      iArray[12][1]="80px"      		//列名
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0; 

      iArray[13]=new Array();
      iArray[13][0]="批次号";         		//列名
      iArray[13][1]="60px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0; 
 
      iArray[14]=new Array();
      iArray[14][0]="提盘银行";         		//列名
      iArray[14][1]="60px";            		//列宽
      iArray[14][2]=100;            		//列最大值
      iArray[14][3]=0; 
      
      iArray[15]=new Array();
      iArray[15][0]="批次状态";         		//列名
      iArray[15][1]="60px";            		//列宽
      iArray[15][2]=100;            		//列最大值
      iArray[15][3]=0; 
      
      iArray[16]=new Array();
      iArray[16][0]="失败原因";         		//列名
      iArray[16][1]="60px";            		//列宽
      iArray[16][2]=100;            		//列最大值
      iArray[16][3]=0; 
      
      YingGrid = new MulLineEnter( "fm" , "YingGrid" ); 
      //这些属性必须在loadMulLine前
      YingGrid.mulLineCount = 0;   
      YingGrid.displayTitle = 1;
      YingGrid.locked = 1;
      YingGrid.canSel = 1;
      YingGrid.hiddenPlus = 1;
      YingGrid.hiddenSubtraction = 1;
      YingGrid.loadMulLine(iArray); 
      YingGrid.selBoxEventFuncName = "queryShouldOldMsg";
      //点选这个按钮的时候，会操作这个函数，对批次的既往批次进行查询
      
      //这些操作必须在loadMulLine后面
      //GrpPolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("FinanceQueryInputInit.jsp-->initYingGrid函数中发生异常:初始化界面错误!");
      }
}
//暂付显示数据初始化
function initZanGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="收/付费号";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[2]=new Array();
      iArray[2][0]="收/付费方式";         		    //列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="业务号";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[4]=new Array();
      iArray[4][0]="业务类型";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[5]=new Array();
      iArray[5][0]="金额";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="收/付费日期";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[7]=new Array();
      iArray[7][0]="是否核销";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0; 

      iArray[8]=new Array();
      iArray[8][0]="核销日期";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0; 


	  iArray[9]=new Array();
      iArray[9][0]="发票是否打印";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0; 

      iArray[10]=new Array();
      iArray[10][0]="发票打印日期";         		//列名
      iArray[10][1]="70px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0; 
 
   	  iArray[11]=new Array();
      iArray[11][0]="客户开户账号";         		//列名
      iArray[11][1]="70px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;
     
      iArray[12]=new Array();
      iArray[12][0]="本方银行账号";         		//列名
      iArray[12][1]="70px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0;

	
      ZanGrid = new MulLineEnter( "fm" , "ZanGrid" ); 
      //这些属性必须在loadMulLine前
      ZanGrid.mulLineCount = 0;   
      ZanGrid.displayTitle = 1;
      ZanGrid.locked = 1;
      ZanGrid.canSel = 1;
      ZanGrid.hiddenPlus=1;
      ZanGrid.hiddenSubtraction=1;
      ZanGrid.loadMulLine(iArray); 
	  ZanGrid.selBoxEventFuncName = "queryTrueOldMsg";
      
      //这些操作必须在loadMulLine后面
      //ClientGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("FinanceQueryInputInit.jsp-->initZanGrid函数中发生异常:初始化界面错误!");
      }
}
//既往批次信息显示初始化
function initPastGrid()
{     
	 var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="提盘银行";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="收/付费号";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[4]=new Array();
      iArray[4][0]="业务号";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=70;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
	  iArray[5]=new Array();
      iArray[5][0]="金额";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[6]=new Array();
      iArray[6][0]="开户账号";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=60;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="开户名";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0; 

      iArray[8]=new Array();
      iArray[8][0]="发盘日期";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0; 


	  iArray[9]=new Array();
      iArray[9][0]="回盘日期";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0; 

      iArray[10]=new Array();
      iArray[10][0]="批次状态";         		//列名
      iArray[10][1]="70px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0; 
 
   	  iArray[11]=new Array();
      iArray[11][0]="失败原因";         		//列名
      iArray[11][1]="100px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;
     
	  
      PastGrid = new MulLineEnter( "fm" , "PastGrid" ); 
      //这些属性必须在loadMulLine前
      PastGrid.mulLineCount = 0;   
      PastGrid.displayTitle = 1;
      PastGrid.locked = 1;
      PastGrid.canSel = 1;
      PastGrid.hiddenPlus=1;
      PastGrid.hiddenSubtraction=1;
      PastGrid.loadMulLine(iArray); 
	  PastGrid.selBoxEventFuncName = "JQueryDetailMsg";
      
      //这些操作必须在loadMulLine后面
      //ClientGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("FinanceQueryInputInit.jsp-->initPastGrid函数中发生异常:初始化界面错误!");
      }
}
//状态明细显示初始化
function initPolStatuGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="状态明细";         		//列名
      iArray[1][1]="300px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      PolStatuGrid = new MulLineEnter( "fm" , "PolStatuGrid" ); 
      //这些属性必须在loadMulLine前
      PolStatuGrid.mulLineCount = 0;   
      PolStatuGrid.displayTitle = 1;
      PolStatuGrid.locked = 1;
      PolStatuGrid.canSel = 0;
      PolStatuGrid.canChk = 0;
      PolStatuGrid.hiddenPlus = 1;
      PolStatuGrid.hiddenSubtraction = 1;
      PolStatuGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
//批次汇总信息初始化
function initZongGrid()
{     
	 var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="提盘银行";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="发盘日期";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[4]=new Array();
      iArray[4][0]="回盘日期";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=70;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[5]=new Array();
      iArray[5][0]="批次状态";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=70;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

	 
      ZongGrid = new MulLineEnter( "fm" , "ZongGrid" ); 
      //这些属性必须在loadMulLine前
      ZongGrid.mulLineCount = 0;   
      ZongGrid.displayTitle = 1;
      ZongGrid.locked = 1;
      ZongGrid.canSel = 1;
      ZongGrid.hiddenPlus=1;
      ZongGrid.hiddenSubtraction=1;
      ZongGrid.loadMulLine(iArray);
      ZongGrid.selBoxEventFuncName = "SerialOnClick";
      }
      catch(ex)
      {
        alert("FinanceQueryInputInit.jsp-->initZongGrid函数中发生异常:初始化界面错误!");
      }
}
//批次明细信息初始化
function initMingGrid()
{     
	 var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="收/付费号";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="业务号码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="金额";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[4]=new Array();
      iArray[4][0]="银行编码";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=70;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[5]=new Array();
      iArray[5][0]="银行账号";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="银行户名";         		//列名
      iArray[6][1]="50px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[7]=new Array();
      iArray[7][0]="管理机构";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0; 

      iArray[8]=new Array();
      iArray[8][0]="失败原因";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0; 

	 
      MingGrid = new MulLineEnter( "fm" , "MingGrid" ); 
      //这些属性必须在loadMulLine前
      MingGrid.mulLineCount = 0;   
      MingGrid.displayTitle = 1;
      MingGrid.locked = 1;
      MingGrid.canSel = 1;
      MingGrid.hiddenPlus=1;
      MingGrid.hiddenSubtraction=1;
      MingGrid.loadMulLine(iArray); 
      }
      catch(ex)
      {
        alert("FinanceQueryInputInit.jsp-->initMingGrid函数中发生异常:初始化界面错误!");
      }
}

</script>
