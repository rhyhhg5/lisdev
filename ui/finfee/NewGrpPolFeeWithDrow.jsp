<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2002-07-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.finfee.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%
CErrors tError = null;
String FlagStr = "";
String Content = "";
//处理类
NewGrpContFeeWithdrawBL tNewGrpContFeeWithdrawBL = new NewGrpContFeeWithdrawBL();
try
{
	//session
	GlobalInput tGI = new GlobalInput();
	tGI=(GlobalInput)session.getValue("GI");

	//合同号
	String GrpContNo=request.getParameter("GrpContNo");

	LCGrpContSchema tLCGrpContSchema =new LCGrpContSchema();
	tLCGrpContSchema.setGrpContNo(GrpContNo);

	LCGrpContSet tLCGrpContSet =new LCGrpContSet();
	tLCGrpContSet.add(tLCGrpContSchema);

	TransferData tTransferData= new TransferData();
	tTransferData.setNameAndValue("SpecWithDraw","1");

	VData tVData = new VData();
	tVData.addElement(tLCGrpContSet);
	tVData.addElement(tGI);
	tVData.addElement(tTransferData);

	tNewGrpContFeeWithdrawBL.submitData(tVData);
}
catch(Exception ex)
{
	Content = "失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}

if (FlagStr=="")
{
	tError = tNewGrpContFeeWithdrawBL.mErrors;
	if (!tError.needDealError())
	{
		Content = " 完成";
		FlagStr = "Succ";
	}
	else
	{
		Content =" 失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>