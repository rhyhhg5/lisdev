<%@page contentType="text/html;charset=GBK"%>
<%
	//程序名称：GetSendToBankSave.jsp                                         
	//程序功能：                                                              
	//创建日期：2006-02-18                                         
	//创建人  ：zhangbin                                                        
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bank.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.finfee.*"%>

<%@page import="java.text.*"%>
<%@page import="java.util.*"%>

<%
	CErrors tError = null;
	String Content = "";
	String FlagStr = "";
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	TransferData tTransferData = new TransferData();
	VData tVData = new VData();
	FirstHastenUI tFirstHastenUI = new FirstHastenUI();
	
	String tPrtNo[] = request.getParameterValues("PolGrid2");
	String tGetNoticeNo[] = request.getParameterValues("PolGrid1");
	String tChecks[] = request.getParameterValues("InpPolGridChk");
	int nIndex = 0;
	//循环
	for (nIndex = 0; nIndex < tChecks.length; nIndex++) {
		//If this line isn't selected, continue，如果没有选中当前行，则继续
		if (!tChecks[nIndex].equals("1")) {
			continue;
		}
		//将数据放入合同保单集合

		tTransferData = new TransferData();
		tTransferData.setNameAndValue("PRTNO", tPrtNo[nIndex]);
		tVData.clear();
		tVData.add(tTransferData);
		tVData.add(tG);

		try {
			if (!tFirstHastenUI.submitData(tVData, "INSERT")) {
		          Content += "投保单：" + tPrtNo[nIndex] +" "+ tFirstHastenUI.mErrors.getFirstError()+ " <br>";
		          FlagStr = "Fail";
		           tFirstHastenUI.mErrors.clearErrors();
			}
		} catch (Exception ex) {
			Content += "投保单：" + tPrtNo[nIndex] + "发催缴通知书失败，原因是:"+ ex.toString() + " <br>";
			FlagStr = "Fail";
			break;
		}

	}

	if (!FlagStr.equals("Fail")) {
		Content = "成功添加进打印队列!";
		FlagStr = "Succ";
	}
%>
<script language="javascript">	
	parent.fraInterface.addInfo1("<%=FlagStr%>","<%=Content%>");	
</script>
<html>


</html>
