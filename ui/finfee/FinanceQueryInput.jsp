<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="FinanceQueryInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

		<%@include file="FinanceQueryInputInit.jsp"%>
		<title>财务查询</title>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit">
			<!-- 保单信息部分 -->
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输入查询条件：
					</td>
				</tr>
			</table>
			<table class=common align=center>
				<TR class=common>
					<TD class=title>
						收/付费号
					</TD>
					<TD class=input>
						<Input class="common1" name=PayOrGetCode>
					</TD>
					<TD class=title>
						业务号
					</TD>
					<TD class=input>
						<Input class="common1" name=Otherno>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						客户开户账号
					</TD>
					<TD class=input>
						<Input class="common1" name=Bankaccno>
					</TD>
				</TR>
			</table>
			<table>
				<TR>
					<td>
						<INPUT VALUE="收费信息查询" class=cssbutton TYPE=button
							onclick="easyQueryClick(1);">
						<INPUT VALUE="付费信息查询" class=cssbutton TYPE=button
							onclick="easyQueryClick(2);">
					</td>
					<TD class=common align="right">
						<font color="#FF0000" align="right">
							业务号：首期签单前、先收费为印刷号，首期签单后、续期为保单号，保全为批单号，理赔为案件号或者团体批次号。 </font>
					</TD>
				</TR>
			</table>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCClent);">
					</td>
					<td class=titleImg>
						应收应付信息
					</td>
				</tr>
			</table>
			<Div id="divLCClent" style="display: ''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanYingGrid"> </span>
						</td>
					</tr>
				</table>
				<INPUT VALUE="首  页" class=cssbutton TYPE=button
					onclick="turnPage.firstPage();showCodeName();">
				<INPUT VALUE="上一页" class=cssbutton TYPE=button
					onclick="turnPage.previousPage();showCodeName();">
				<INPUT VALUE="下一页" class=cssbutton TYPE=button
					onclick="turnPage.nextPage();showCodeName();">
				<INPUT VALUE="尾  页" class=cssbutton TYPE=button
					onclick="turnPage.lastPage();showCodeName();">
			</Div>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCGrpClent);">
					</td>
					<td class=titleImg>
						收付信息
					</td>
				</tr>
			</table>
			<Div id="divLCGrpClent" style="display: ''" align=center>
				<div align=left>
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanZanGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
				<INPUT VALUE="首  页" class=cssbutton TYPE=button
					onclick="turnPage1.firstPage();">
				<INPUT VALUE="上一页" class=cssbutton TYPE=button
					onclick="turnPage1.previousPage();">
				<INPUT VALUE="下一页" class=cssbutton TYPE=button
					onclick="turnPage1.nextPage();">
				<INPUT VALUE="尾  页" class=cssbutton TYPE=button
					onclick="turnPage1.lastPage();">
			</Div>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCPol1);">
					</td>
					<td class=titleImg>
						既往批次信息
					</td>
				</tr>
			</table>
			<Div id="divLCPol1" style="display: ''" align=center>
				<div align=left>
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanPastGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
				<INPUT VALUE="首  页" class=cssbutton TYPE=button
					onclick="turnPage2.firstPage();">
				<INPUT VALUE="上一页" class=cssbutton TYPE=button
					onclick="turnPage2.previousPage();">
				<INPUT VALUE="下一页" class=cssbutton TYPE=button
					onclick="turnPage2.nextPage();">
				<INPUT VALUE="尾  页" class=cssbutton TYPE=button
					onclick="turnPage2.lastPage();">
			</Div>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCPol2);">
					</td>
					<td class=titleImg>
						状态明细
					</td>
				</tr>
			</table>
			<Div id="divLCPol2" style="display: ''" align=center>
				<div align=left>
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanPolStatuGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
			</Div>
			<p />
				<tr>
				<td>
					<INPUT VALUE="状态明细说明" class=cssbutton TYPE=button
						onclick="stateMsghelp();">
				</td>
				<td>
					<INPUT VALUE="提盘规则说明" class=cssbutton TYPE=button
						onclick="drawRulehelp();">
				</td>
				</tr>
			<p />
			<table class=common align=center>
				<TR class=common>
					<TD class=title>
						批次号
					</TD>
					<TD class=input>
						<Input class="common1" name=serialNo>
					</TD>
				</TR>
			</table>
			<table class=common align=center>
			<tr>
				<td>
					<INPUT  VALUE="批次查询" class=cssbutton TYPE=button
						onclick="findSeria();">
					<INPUT VALUE="发盘报文下载" class=cssbutton TYPE=button
						onclick="sendFileDownload();">
					<INPUT VALUE="回盘报文下载" class=cssbutton TYPE=button
						onclick="returnFileDownload();">
				</td>
			</tr>
			</table>
			<Div id=DivFileDownload style="display:'none'">
      				<A id=fileUrl href=""></A>
    		</Div>
			<INPUT VALUE="" TYPE=hidden name=filenamepath>
			<INPUT VALUE="" TYPE=hidden name=filename>
			<br>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCPol3);">
					</td>
					<td class=titleImg>
						批次汇总信息
					</td>
				</tr>
			</table>
			<Div id="divLCPol3" style="display: ''" align=center>
				<div align=left>
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanZongGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
				<INPUT VALUE="首  页" class=cssbutton TYPE=button
					onclick="turnPage4.firstPage();">
				<INPUT VALUE="上一页" class=cssbutton TYPE=button
					onclick="turnPage4.previousPage();">
				<INPUT VALUE="下一页" class=cssbutton TYPE=button
					onclick="turnPage4.nextPage();">
				<INPUT VALUE="尾  页" class=cssbutton TYPE=button
					onclick="turnPage4.lastPage();">
			</Div>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCPol4);">
					</td>
					<td class=titleImg>
						批次明细信息
					</td>
				</tr>
			</table>
			<Div id="divLCPol4" style="display: ''" align=center>
				<div align=left>
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanMingGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
				<INPUT VALUE="首  页" class=cssbutton TYPE=button
					onclick="turnPage5.firstPage();">
				<INPUT VALUE="上一页" class=cssbutton TYPE=button
					onclick="turnPage5.previousPage();">
				<INPUT VALUE="下一页" class=cssbutton TYPE=button
					onclick="turnPage5.nextPage();">
				<INPUT VALUE="尾  页" class=cssbutton TYPE=button
					onclick="turnPage5.lastPage();">
			</Div>
			<input type="hidden" name="feeFlag">
			<input type="hidden" name="feeState">
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
