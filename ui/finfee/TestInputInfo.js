var showInfo;
var turnPage = new turnPageClass();

function easyQueryClick()
{
    var strSql = "select TempFeeNo, TempFeeType,  RiskCode, BackUpSerialNo,  MakeTime,  MakeDate, ModifyDate, ModifyTime, Operator " 
        + " from LbTempfee "
        + " where 1 = 1 "
        + getWherePart('TempFeeNo', 'TempFeeNo')
        + getWherePart('TempFeeType', 'TempFeeType')
        + getWherePart('RiskCode', 'RiskCode')
        + getWherePart('BackUpSerialNo', 'BackUpSerialNo');
       
    turnPage.queryModal(strSql, FFInvoiceBaseInfoGrid);
    divUpdate.style.display = "none";
}

function easyQueryCom()
{
    var strSql = "select OutComCode, LetterServiceName, ShortName from ldcom where ComCode = '" + ManageCom + "'";
    var arr = easyExecSql(strSql);
    if(arr != null)
    {
        fm.all('OutComCode').value = arr[0][0];
        fm.all('LetterServiceName').value = arr[0][1];
        fm.all('ShortName').value = arr[0][2];
        fm.all('Nsrsbh').value = "";
        fm.all('outputTemplate').value = "";
    }
}

function addIBaseInfo()
{

    fm.fmtransact.value = "INSERT" ;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.all("method").value="save";
    fm.submit(); //提交
    return true;
}

function afterSubmit(FlagStr, content, transact)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        if(transact == "UPDATE"){
            easyQueryClick();
        }
		//showDiv(operateButton,"true"); 
		//showDiv(inputButton,"false"); 
		//执行下一步操作
	}
}

function onclkSelBox(){
    var tSel = FFInvoiceBaseInfoGrid.getSelNo();
    fmUpdate.upTempFeeNo.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,1);
    fmUpdate.upTempFeeType.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,2);
    fmUpdate.upRiskCode.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,3);
    fmUpdate.upBackUpSerialNo.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,4);
    fmUpdate.upMakeTime.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,5);
    fmUpdate.upMakeDate.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,6);
    fmUpdate.upModifyDate.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,7);
    fmUpdate.upModifyTime.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,8);
    fmUpdate.upOperator.value = FFInvoiceBaseInfoGrid.getRowColData(tSel-1,9);
    divUpdate.style.display = "";
}

function updateInvoice(){
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fmUpdate.all("method1").value="change";
    fmUpdate.submit(); 
    return true;
}

function deleteInvoice(){
	fmUpdate.all("method1").value="delete";
    fmUpdate.submit(); 
    return true;
}