//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  if(!check())	return ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	 //divDifShow.style.display = 'none';
	 fm.all('Dif').value=0;
  }
}

function check()
{
	if(fm.all('GrpContNo').value==null||fm.all('GrpContNo').value=='')
	{
		alert("请先查询");
		fm.all('GrpContNo').focus();
		return false;
	}
	
	if(fm.Dif.value==0)
	{
		alert("退费额为零");
		return false;
	}
	
	
	
	return true;
}

function queryClick(){
	if(fm.all('GrpContNo').value==null||fm.all('GrpContNo').value=='')
	{
		alert("请录入合同单号");
		fm.all('GrpContNo').focus();
		return false;
	}
	var strSQL = "select Dif from LCGrpCont where GrpContNo = '"+fm.all('GrpContNo').value+"'";
	var tResult = easyExecSql(strSQL, 1, 0, 1);
	if(!tResult)
	{
		alert("录入的合同号可能有误，数据库中无记录信息！");
		return false;
	}
	//divDifShow.style.display = '';
	fm.all('Dif').value=tResult;
}

function print(){
	
	if(fm.GrpContNo.value.length!=0)
	{ 
		fm.action="../f1print/NBCF2PSave.jsp";
	  fm.target="f1print";
	  fm.submit();
	}
  else 
  { 
  	 alert("请输入合同单号");
  }
}