<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2007-05-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GB2312"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.finfee.*"%>
<%@page import="java.util.List"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%
  System.out.println("start FFInvoiceBaseInfoSave ----");

  GlobalInput tGI = (GlobalInput) session.getValue("GI");

  String FlagStr = "";
  String Content = "";

  String tInvoiceNo = (String) request.getParameter("InvoiceNo");
  String tMonty = (String) request.getParameter("Money");
    
  LOPRTInvoiceManagerSchema tLOPRTInvoiceManagerSchema = new LOPRTInvoiceManagerSchema();
  TransferData tTransferData = new TransferData();
  
  String tComCode[] = request.getParameterValues("InvoiceGrid1");
  String tInvoiceCode[] = request.getParameterValues("InvoiceGrid4");
  String tInvoiceCodeExp[] = request.getParameterValues("InvoiceGrid5");
  String tInvoiceEndNo[] = request.getParameterValues("InvoiceGrid7");
  String tInvoiceStartNo[] = request.getParameterValues("InvoiceGrid6");
  String tTaxpayerName[] = request.getParameterValues("InvoiceGrid3");
  String tTaxpayerNo[] = request.getParameterValues("InvoiceGrid2");
  String tSel[] = request.getParameterValues("InpInvoiceGridSel"); 
  
  for(int i = 0 ; i<tSel.length ; i++){
      if(tSel[i].equals("1")) {
        tLOPRTInvoiceManagerSchema.setInvoiceNo(tInvoiceNo);
        tLOPRTInvoiceManagerSchema.setXSumMoney(tMonty);
        tLOPRTInvoiceManagerSchema.setStateFlag("2");
        tLOPRTInvoiceManagerSchema.setComCode(tComCode[i]);
        tLOPRTInvoiceManagerSchema.setInvoiceCode(tInvoiceCode[i]);
        tLOPRTInvoiceManagerSchema.setInvoiceCodeEx(tInvoiceCodeExp[i]);
        tLOPRTInvoiceManagerSchema.setPayeeName(tTaxpayerName[i]);
        tLOPRTInvoiceManagerSchema.setPayerName(tTaxpayerName[i]);
        tTransferData.setNameAndValue("InvoiceStartNo", tInvoiceStartNo[i]);
        tTransferData.setNameAndValue("InvoiceEndNo", tInvoiceEndNo[i]);
      }
  }

    VData tVData = new VData();
    tVData.add(tLOPRTInvoiceManagerSchema);
    tVData.add(tGI);
    tVData.add(tTransferData);

   FFInvoiceNullifyUI tFFInvoiceNullifyUI = new FFInvoiceNullifyUI();

    try
    {
        if(!tFFInvoiceNullifyUI.submitData(tVData,"INSERT"))
        {
            FlagStr = "Fail";
            Content = tFFInvoiceNullifyUI.mErrors.getFirstError().toString();
        } else {
            FlagStr = "";
            Content = "保存成功";
        }
    }
    catch(Exception ex)
    {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
    }

    System.out.println("end FFInvoiceBaseInfoSave ----");
%>

<html>
    <script language="javascript">
    try
    {
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
    }
    catch(e)
    {
    }
</script>
</html>
