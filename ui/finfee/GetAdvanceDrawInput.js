//TempFeeWithdrawInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var tFees;
//提交，保存按钮对应操作
function submitForm()
{ 
  var i = 0;
  var checkFlag = 0;
  if(fm.qPayMode.value == null || fm.qPayMode.value == ""){
      alert("请选择付费方式！");
      return;
  }
  if(fm.qPayMode.value == 4){
      alert("不能使用银行转账方式进行退费！");
      return;
  }
  for (i=0; i<FeeGrid.mulLineCount; i++) {
    if (FeeGrid.getChkNo(i)) { 
      checkFlag = 1;
      break;
    }
  }
    if (checkFlag) { 
		var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
       showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
       fm.submit(); //提交
  }
  else {
    alert("请先选择一条暂交费信息！"); 
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

var queryBug = 0;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//暂交费号码查询按钮
function tempFeeNoQuery(afterQuery) {
  //已核销、退费（ConfFlag）的不显示出来
  var moneySql = "sum(PayMoney)<>0";
    if(fm.PayMoney.value != null && fm.PayMoney.value != ""){
       moneySql = " sum(PayMoney)=" + fm.PayMoney.value;
    }
    var strSql = "select '', OtherNo, TempFeeType, RiskCode, APPntName, getUniteCode(agentcode),OtherNo, sum(PayMoney), PayDate, EnterAccDate, sum(PayMoney) from LJTempFee where ConfMakedate is not null "
               + " and Confdate is null and TempFeeType='YS' and agentcode like '"+fm.AgentCode.value+"%'  "
               + getWherePart('EnterAccDate')
               + getWherePart('ManageCom','ManageCom','like')
               + getWherePart('APPntName','APPntName','like')
               + " group by TempFeeType, RiskCode, APPntName, OtherNo, PayDate, EnterAccDate,agentcode having " + moneySql;
            

  
  var strSqlTemp=easyQueryVer3(strSql, 1, 0, 1); 
     turnPage.strQueryResult=strSqlTemp;
     
     if(!turnPage.strQueryResult)
     {
        window.alert("没有查询记录!");
        FeeGrid.clearData();
        return false;
     }
     else
     {
          turnPage.queryModal(strSql, FeeGrid);
     }
}

function queryAgent()
{
    if(fm.all('ManageCom').value==""){
         alert("请先录入管理机构信息！");
         return;
    }
    if(fm.all('AgentCode').value == "")
    {
          var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
      }
    if(fm.all('AgentCode').value != "")
    {
      var cAgentCode = fm.AgentCode.value;  //保单号码
      var strSql = "select (select getUniteCode('" + cAgentCode +"') from dual),Name from LAAgent where AgentCode='" + cAgentCode +"'";
      var arrResult = easyExecSql(strSql);
      if (arrResult != null) {
        alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
      }
    }
}

function afterQuery2(arrReturn)
{
   fm.all('AgentCode').value = arrReturn[0][0];
   var rsSQL = "select getUniteCode('"+arrReturn[0][0]+"') from dual";
   var rs = easyExecSql(rsSQL);
   fm.GroupAgentCode.value = rs;
}
