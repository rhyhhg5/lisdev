<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GetAdvanceDateInput.jsp
//程序功能：清单下载
//创建日期：2011-11-24
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "预收保费查询清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  String querySql = request.getParameter("sql");
  
  ExeSQL tExeSQL = new ExeSQL();
  SSRS tSSRS = tExeSQL.execSQL(querySql.replaceAll("%25","%"));
  System.out.println("-----sql------" + querySql.replaceAll("%25","%"));
  System.out.println("----" + tSSRS.getMaxRow());
  
  String manageCom = request.getParameter("ManageCom");
  String sql = "select name from ldcom where comcode='" + manageCom + "' ";
  SSRS mSSRS = tExeSQL.execSQL(sql);
  
  
  String[][] mToExcel = new String[4000][30];
        mToExcel[0][0] = "预收保费查询清单";
        mToExcel[1][0] = "机构:" + mSSRS.GetText(1,1);
        mToExcel[2][0] = "起始日期：" + request.getParameter("StartDate");
        mToExcel[2][3] = "终止日期：" + request.getParameter("EndDate");
        mToExcel[3][0] = "预收保费号码";
        mToExcel[3][1] = "业务类型";
        mToExcel[3][2] = "日期";
        mToExcel[3][3] = "金额";
        mToExcel[3][4] = "剩余金额";
        mToExcel[3][5] = "投保单位";
        mToExcel[3][6] = "业务员代码";
        
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row + 3][col - 1] = tSSRS.GetText(row, col);
            }
        }        
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }
  //返回客户端
  if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
  out.clear();
    out = pageContext.pushBody();
%>