
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var bankflag="";

function easyQueryClick() {
	var Serialno= fm.HCNo.value;
	 // 实付号、业务号、给付机构、含税金额、收款方名称 、险种
	var strSql = "select lcp.TranSerial,lja.otherno,lcp.Pk_Org,cast(lcp.LocalTotalAmt as decimal(30,2)),lcp.CustName,lcp.ProCode,case when Receive_State='1' then '已读取' else '未读取' end from LCPaySerialnoHZ lcp,ljaget lja where TranBatch='"
	+ Serialno
	+"' and lcp.TranSerial=lja.actugetno with ur";

	// 查询SQL，返回结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSql, 1, 1, 1);

	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		// 清空MULTILINE，使用方法见MULTILINE使用说明
		LCPaySerialnoHZGrid.clearData('LCPaySerialnoHZGrid');
		alert("没有查询到数据！");
		return false;
	}

	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	// 设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = LCPaySerialnoHZGrid;

	// 保存SQL语句
	turnPage.strQuerySql = strSql;

	// 设置查询起始位置
	turnPage.pageIndex = 0;

	turnPage.pageLineNum = 15;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,
			turnPage.pageIndex, turnPage.pageLineNum);
	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	// 控制是否显示翻页按钮
	if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
		try {
			window.divPage.style.display = "";
		} catch (ex) {
		}
	} else {
		try {
			window.divPage.style.display = "none";
		} catch (ex) {
		}
	} 
	}

// 提交，保存按钮对应操作
function submitForm()
{
  // 汇总批次号
  var mSerialnoHZ = fm.HCNo.value;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.action="./PayUnionSerialnoUPSave.jsp?SerialnoHZ=" + mSerialnoHZ;
  fm.submit(); // 提交
}
function afterSubmit(FlagStr,content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	easyQueryClick();
}