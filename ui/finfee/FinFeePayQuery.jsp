 <%
//程序名称：FinFeePayQuery.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>    
  
<%@page contentType="text/html;charset=GBK" %>
<%
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
   String ActuGetNo = "";
   ActuGetNo = request.getParameter("ActuGetNo");
   
   LJAGetSchema  tLJAGetSchema  ; //实付总表
   LJAGetSet     tLJAGetSet     ;

//2-查询实付总表
    tLJAGetSchema = new LJAGetSchema();
    tLJAGetSchema.setActuGetNo(ActuGetNo);
    VData tVData = new VData();
    tVData.add(tLJAGetSchema);
System.out.println("Start 查询实付总表");
    LJAGetQueryUI tLJAGetQueryUI = new LJAGetQueryUI();
    if(!tLJAGetQueryUI.submitData(tVData,"QUERY"))
    {
        Content = " 查询实付总表失败，原因是: " + tLJAGetQueryUI.mErrors.getError(0).errorMessage;
        FlagStr = "Fail";
    }  
    else
    {
//3-提取并显示数据
    tVData.clear();    
    tLJAGetSet = new LJAGetSet();
    tLJAGetSchema = new LJAGetSchema();     
    tVData = tLJAGetQueryUI.getResult();
    tLJAGetSet.set((LJAGetSet)tVData.getObjectByObjectName("LJAGetSet",0));  
    tLJAGetSchema =(LJAGetSchema)tLJAGetSet.get(1);
    String PayMode= tLJAGetSchema.getPayMode();
    if(PayMode==null) PayMode="";
 System.out.println("PayMode:"+PayMode);       
System.out.println("显示数据");
%>
<Script language="javascript">
    parent.fraInterface.fmSave.all('PolNo').value="<%=tLJAGetSchema.getOtherNo()%>";
    parent.fraInterface.fmSave.all('PayMode').value="<%=PayMode%>";
    parent.fraInterface.fmSave.all('GetMoney').value="<%=tLJAGetSchema.getSumGetMoney()%>";                           
		parent.fraInterface.fmSave.all('BankCode').value="<%=tLJAGetSchema.getBankCode()%>";
		parent.fraInterface.fmSave.all('AccName').value="<%=tLJAGetSchema.getAccName()%>";
		parent.fraInterface.fmSave.all('BankAccNo').value="<%=tLJAGetSchema.getBankAccNo()%>";
</Script>
<% 
  if (FlagStr=="")
  {
    tError = tLJAGetQueryUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 查询成功";
      FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }              
    }//对应3                  
%> 
<HTML>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</HTML> 
