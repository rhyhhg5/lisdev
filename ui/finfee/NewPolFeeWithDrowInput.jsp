 <html> 
<%
//程序名称：NewPolFeeWithDrowInput.jsp
//程序功能：个人保费催收，实现数据从保费项表到应收个人表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="NewPolFeeWithDrowInput.js"></SCRIPT>  
  <%@include file="NewPolFeeWithDrowInit.jsp"%>
</head>
<body  onload="initForm();">
<form name=fm action=./NewPolFeeWithDrow.jsp target=fraSubmit method=post>
<!-- 显示或隐藏信息 -->
    
    <!-- 查询信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
	
    <table  class= common align=center>
    	<TR  class= common>
        <TD  class= title>
          险种编码
        </TD>
        <TD  class= input>
          <Input NAME=RiskCode class=common>
        </TD>
        <TD  class= title>
          印刷号
        </TD>
        <TD  class= input>
          <Input NAME=PrtNo class=common>
        </TD>
        <TD  class= title>
          保单号 
        </TD>
        <TD  class= input>
          <Input NAME=PolNo2 class=common>
        </TD>
      </TR>
      
      <TR  class= common>  
        <TD  class= title>
          投保人 
        </TD>
        <TD  class= input>
          <Input NAME=AppntName class=common>
        </TD>
        <TD  class= title>
          银行代码
        </TD>
        <TD  class= input>
          <Input NAME=BankCode class=codeNo ondblclick="return showCodeList('bank', [this,BankName],[0,1]);" onkeyup="return showCodeListKey('bank', [this,BankName],[0,1]);"><input class=codename name=BankName readonly=true >
        </TD>
        <TD  class= title>
          余额
        </TD>
        <TD  class= input>
          <Input NAME=LeavingMoney class=common>
        </TD>
      </TR>
      
      <TR  class= common>  
        <TD  class= title>
          <INPUT VALUE="查询" TYPE=button class=cssButton onclick="easyQueryClick();">
        </TD>

      </TR>
    </table>
        
    <hr>   
        
    <!-- 续期缴费通知书信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </Div>
    <hr>
    <table  class= common align=center>
      <INPUT VALUE="退 费" class= cssButton TYPE=button onclick="submitForm()">
    </table>
    <br>
    <Input type=hidden class= common name=ProPosalNo  >   
    <Input type=hidden class= common name=PolNo >
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
