<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2007-05-18
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="FFInvoiceExport.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FFInvoiceExportInit.jsp"%>
</head>
<body onload="initForm();" >
    <form action="FFInvoiceExportSave.jsp" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFFIExport);">
                </td>
                <td class="titleImg">机构信息</td>
            </tr>
        </table>
        <div id="divFFIExport" style= "display:''">
            <table class="common" align='center' >
                <tr class="common">
                    <td class="title">机构编号</td>
                    <td class="input"><input class="readonly" name="ComCode" readonly="readonly" /></td>
                    <td class="title"></td>
                    <td class="input"></td>
                </tr>
                <tr class="common" id="TaxDiv">
                    <td class="title">纳税人标识号</td>
                    <td class="input"><input class="common" name="TaxpayerNo" readonly="readonly" /></td>
                    <td class="title">纳税人名称</td>
                    <td class="input"><input class="common" name="TaxpayerName" readonly="readonly" /></td>
                </tr>
                <tr class="common">
                    <td class="title">所属日期起</td>
                    <td class="input"><input class="coolDatePicker" name="InvoiceStartDate" verify="扫描时间|date&notnull" elementtype="nacessary" /></td>
                    <td class="title">所属日期止</td>
                    <td class="input"><input class="coolDatePicker" name="InvoiceEndDate" verify="扫描时间|date&notnull" elementtype="nacessary" /></td>
                </tr>
                <tr class="common">
                    <td class="title">收付费发票标记</td>
                    <td class="input8"><input class="codeno" CodeData="0|8^0|收费发票^1|付费发票" name="SFState"  ondblclick="return showCodeListEx('SFState',[this,SFStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('SFState',[this,SFStateName],[0,1],null,null,null,1);" /><input class=codename name=SFStateName elementtype="nacessary"></td>
                    <td class="title">
                    	<Div id="TypeDiv" style="display:none">导出类型</Div>
                    	<Div id="Certify" style="display:none">单证编码</Div>
                    </td>
                    <td class="input8">
                    	<Div id="TypeDiv2" style="display:none"><input class="codeno" CodeData="0|8^0|发票开具信息^1|发票明细信息" name="Type"  ondblclick="return showCodeListEx('Type',[this,TypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Tpye',[this,TpyeName],[0,1],null,null,null,1);" /><input class=codename name=TypeName elementtype="nacessary"></Div>
                    	<Div id="CertifyCode" style="display:none"><input class="code" name="CertifyCode"  ondblclick="return showCodeList('InvoiceCertify',[this]);" onkeyup="return showCodeListKey('InvoiceCertify',[this]);" /></Div>
                    </td>
                </tr>
            </table>
        </div>
        <INPUT VALUE="导  出" class = cssButton TYPE=button onclick="Export();">
        <div id="filesList" style="margin-top:10px;"></div>
    </form>
    <span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>