<%
//程序名称：TempFeeWithdrawInit.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>  
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox() { 
}                                     

function initForm() {
  try {
  	initInpBox();
  	initFeeGrid();
  	fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
  }
  catch(re) {
    alert("TempFeeWithdrawInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 领取项信息列表的初始化
var FeeGrid;
function initFeeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            	//列宽
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="退费原因";         		//列名
    iArray[1][1]="40px";            		//列宽
    iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="bz_cancel";            

    iArray[2]=new Array();
    iArray[2][0]="预收费收据号";      //列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="收费类型";                //列名
    iArray[3][1]="40px";            		//列宽
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="险种编码";         		//列名
    iArray[4][1]="0px";            		//列宽
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="投保单位名称";         		//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="业务员代码";               //列名
    iArray[6][1]="60px";                    //列宽
    iArray[6][3]=0;                         //是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="投保单印刷号";         		//列名
    iArray[7][1]="0px";            		//列宽
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="交费金额";         			//列名
    iArray[8][1]="60px";            		//列宽
    iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="交费日期";         		//列名
    iArray[9][1]="60px";            		//列宽
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[10]=new Array();
    iArray[10][0]="到帐日期";         		//列名
    iArray[10][1]="0px";            		//列宽
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[11]=new Array();
    iArray[11][0]="退费金额";         		//列名
    iArray[11][1]="50px";            		//列宽
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    FeeGrid = new MulLineEnter( "fm" , "FeeGrid" ); 
    //这些属性必须在loadMulLine前
    FeeGrid.mulLineCount = 0;   
    FeeGrid.displayTitle = 1;
    FeeGrid.hiddenPlus = 1;
    FeeGrid.hiddenSubtraction = 1;
    FeeGrid.canChk = 1;
    FeeGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
    //FeeGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>

	
