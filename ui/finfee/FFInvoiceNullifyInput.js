var showInfo;
var turnPage = new turnPageClass();

function easyQueryClick()
{
    var strSql = "select ComCode, TaxpayerNo, TaxpayerName, InvoiceCode, InvoiceCodeExp, InvoiceStartNo, InvoiceEndNo, Operator " 
        + " from LJInvoiceInfo "
        + " where ComCode like '" + ManageCom + "%' "
        + getWherePart('TaxpayerNo', 'TaxpayerNo')
        + getWherePart('TaxpayerName', 'TaxpayerName','like')
        + getWherePart('InvoiceCode', 'InvoiceCode','like')
        + getWherePart('InvoiceCodeExp', 'InvoiceCodeExp','like')
    turnPage.queryModal(strSql, InvoiceGrid);
}

function addIBaseInfo()
{
    if (!verifyInput2()) 
    {
        return false;
    }
    
    var i = 0;
    var checkFlag = 0;
    for (i=0; i<InvoiceGrid.mulLineCount; i++) {
      if (InvoiceGrid.getSelNo(i)) { 
        checkFlag = 1;
        break;
      }
    }
    if (checkFlag==1) { 
      fm.fmtransact.value = "INSERT" ;
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      fm.submit(); //提交
      return true;
    }
    else {
      alert("请先选择发票信息记录！"); 
    }
}

function afterSubmit(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}