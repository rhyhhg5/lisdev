<html>
<%
	//程序名称：PayUnionSerialnoTJ.jsp
	//程序功能：手续费批量付费添加批次
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<%
	String mSerialnoHZ = request.getParameter("SerialnoHZ");
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	String ManageCom = tGI.ManageCom;
%>
<SCRIPT src="PayUnionSerialnoTJ.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="PayUnionSerialnoTJInit.jsp"%>
</head>

<body onload="initForm();">
	<Form action="" method=post name=fm target="fraSubmit">
		<Table>
			<TR>
				<TD class=common>
					<IMG src="../common/images/butExpand.gif" style="cursor: hand;" OnClick="showPage(this,queryCondition);">
				</TD>
				<TD class=titleImg>查询条件</TD>
			</TR>
		</Table>
		<div id="queryCondition" align=center style="display: ''">
			<table class=common>
				<TR class=common>
					<TD class=title8>业务类型</TD>
					<TD class=input8>
						<Input class=codeNo name=paytype value="AC/BC"><input class=codename name=paytypeName value="手续费付费" readonly=true>
					</TD>
					<TD class=title8>付费方式</TD>
					<TD class=input8>
						<Input class=codeNo name=PayMode ondblclick="return showCodeList('paymode',[this,PayModeName],[0,1]);" onkeyup="return showCodeListKey('paymode',[this,PayModeName],[0,1]);"><input class=codename name=PayModeName>
					</TD>
					<TD class=title8>支付对象</TD>
					<TD class=input8>
						<Input class="codeno" name=AgentCom ondblclick="return showCodeList('chargecom',[this,AgentComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('chargecom',[this,AgentComName],[0,1],null,null,null,1);"><input class=codename name=AgentComName>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title8>起始日期</TD>
					<TD class=input8>
						<Input class="coolDatePicker" dateFormat="short" name=StartDate>
					</TD>
					<TD class=title8>截至日期</TD>
					<TD class=input8>
						<Input class="coolDatePicker" dateFormat="short" name=EndDate>
					</TD>
				</TR>
			</Table>
			<Input class=common name="ManageCom" value="<%=ManageCom%>" type=hidden>
		</div>
		<INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();">
		<Table>
			<TR class=common>
				<TD class=title>汇总批次号</TD>
				<TD class=input>
					<Input class=common name="HZNo" disabled="disabled" value="<%=mSerialnoHZ%>" style="border-color: #C0C0C0;">
				</TD>
			</TR>
		</Table>

		<Table>
			<TD class=common>
				<IMG src="../common/images/butExpand.gif" style="cursor: hand;" OnClick="showPage(this,divFinFee1);">
			</TD>
			<TD class=titleImg>手续费实付表信息</TD>
		</Table>
		<Div id="divFinFee1" align=center style="display: ''">
			<Table class=common>
				<TR class=common>
					<TD text-align: left colSpan=1>
						<span id="spanLJFIGetGrid"></span>
					</TD>
				</TR>
			</Table>
			<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
			<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
			<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
			<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
		<Div id='Savebutton' style='display:'>
			<INPUT VALUE="保  存" Class=cssButton TYPE=button onclick="submitForm();">
		</Div>
	</Form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>