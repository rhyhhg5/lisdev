 <%
//程序名称：BatchPayQuereyInit.jsp
//程序功能：
//创建日期：2009-06-15 
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<SCRIPT src="Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
    
%>                            

<script language="JavaScript">
                                     

function initForm()
{
  try
  {  
    initQueryGrid();  
  }
  catch(re)
  {
    alert("menuGrpInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  } 
}

 

function initQueryGrid()
{

    var iArray = new Array();
      
      try
      { 
      
      iArray[0]=new Array();
      iArray[0][0]="序号";                    //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";                  //列宽
      iArray[0][2]=10;                      //列最大值
      iArray[0][3]=0;              
      
      iArray[1]=new Array();
      iArray[1][0]="发票代码";                    //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="110px";                     //列宽
      iArray[1][2]=100;                     //列最大值
      iArray[1][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="发票号码";                      //列名
      iArray[2][1]="60px";                         //列宽
      iArray[2][2]=100;                     //列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
            
      iArray[3]=new Array();
      iArray[3][0]="保单号";                      //列名
      iArray[3][1]="110px";                          //列宽
      iArray[3][2]=100;                     //列最大值
      iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

 
      iArray[4]=new Array();
      iArray[4][0]="金额";                        //列名
      iArray[4][1]="60px";                         //列宽
      iArray[4][2]=100;                     //列最大值
      iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
 
      iArray[5]=new Array();
      iArray[5][0]="发票状态";                      //列名
      iArray[5][1]="50px";                         //列宽
      iArray[5][2]=100;                     //列最大值
      iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏
      
      iArray[6]=new Array();
      iArray[6][0]="付费方名称";                      //列名
      iArray[6][1]="110px";                         //列宽
      iArray[6][2]=100;                     //列最大值
      iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      iArray[7]=new Array();
      iArray[7][0]="开票人";                      //列名
      iArray[7][1]="80px";                         //列宽
      iArray[7][2]=100;                     //列最大值
      iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      iArray[8]=new Array();
      iArray[8][0]="开票日期";                      //列名
      iArray[8][1]="80px";                         //列宽
      iArray[8][2]=100;                     //列最大值
      iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
      
      
      iArray[9]=new Array();
      iArray[9][0]="管理机构";                    //列名
      iArray[9][1]="80px";                         //列宽
      iArray[9][2]=100;                     //列最大值
      iArray[9][3]=0;  
      
      iArray[10]=new Array();
      iArray[10][0]="单证编码";                    //列名
      iArray[10][1]="80px";                         //列宽
      iArray[10][2]=100;                     //列最大值
      iArray[10][3]=0;  
       
      QueryGrid = new MulLineEnter( "fm" , "QueryGrid" ); 
         
      //这些属性必须在loadMulLine前
      QueryGrid.mulLineCount = 0;     
      QueryGrid.displayTitle = 1;
      QueryGrid.canChk =0;
      QueryGrid.canSel =0;
      QueryGrid.locked =1;            //是否锁定：1为锁定 0为不锁定
      QueryGrid.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
      QueryGrid.hiddenSubtraction=1; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
      QueryGrid.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
        
      QueryGrid.loadMulLine(iArray);     
      }
      catch(ex)
      { 
        alert(ex);
      }
  }     
</script>
 