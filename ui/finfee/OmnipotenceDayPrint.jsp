<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//OmnipotenceDayPrint.jsp
//程序功能：
//创建日期：2007-10-22
//创建人  ：张建宝
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%		
    System.out.println("in to OmnipotenceDayPrint.jsp");	
    boolean operFlag = true;
    String FlagStr = "";
    String Content = "";
    String[] title = {"机构编码", "机构", "险种代码", "险种名称", "保费收入", "持续奖金", 
    	"保单利息", "初始费用", "犹豫期退保", "退保金", "退保费用", "保单管理费","风险保费", 
    	"部分领取", "重疾保险金", "身故保险金", "可转投资净额"};
    XmlExport xmlExport = null;   
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    String sql = request.getParameter("querySql");
    //sql.replaceAll("a.PrtSeq,", "");
    System.out.println(request.getParameter("endDate"));
    
    ArrayList arrayList = new ArrayList();
    arrayList.add("startDate");
    arrayList.add(request.getParameter("startDate"));
    arrayList.add("endDate");
    arrayList.add(request.getParameter("endDate"));
    
    TransferData tTransferData= new TransferData();
    tTransferData.setNameAndValue("sql", sql);	//查询的 SQL 
    tTransferData.setNameAndValue("vtsName", "OmnipotenceDay.vts");//模板名
    tTransferData.setNameAndValue("printerName", "printer");//打印机名
    tTransferData.setNameAndValue("title", title);//表头
    tTransferData.setNameAndValue("tableName", "BB");//表名
    tTransferData.setNameAndValue("arrayList", arrayList);//表名
    
    VData tVData = new VData();
    tVData.addElement(tG);
    tVData.addElement(tTransferData);
          
    PrintList printList = new PrintList(); 
    if(!printList.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = printList.mErrors.getErrContent();                
    }
    else
    {    
        VData result = printList.getResult();			
        xmlExport=(XmlExport)result.getObjectByObjectName("XmlExport",0);
        
        if(xmlExport==null)
        {
        	operFlag=false;
        	Content="没有得到要显示的数据文件";	  
        }
    }
    //System.out.println(operFlag);
    if (operFlag==true)
    {
        ExeSQL exeSQL = new ExeSQL();
        //获取临时文件名
        String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
        String strFilePath = exeSQL.getOneValue(strSql);
        String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
        //获取存放临时文件的路径
        //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
        //String strRealPath = exeSQL.getOneValue(strSql);
        String strRealPath = application.getRealPath("/").replace('\\','/');
        String strVFPathName = strRealPath + "//" +strVFFileName;
        
        CombineVts tcombineVts = null;	
        
        String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
    	tcombineVts = new CombineVts(xmlExport.getInputStream(),strTemplatePath);
    
    	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    	tcombineVts.output(dataStream);
        	
    	//把dataStream存储到磁盘文件
    	System.out.println("存储文件到"+strVFPathName);
    	AccessVtsFile.saveToFile(dataStream,strVFPathName);
        System.out.println("==> Write VTS file to disk ");
            
    		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
    }
    else
    {
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	//alert("<%= Content %>");
	//top.close();
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
<%
  	}
%>