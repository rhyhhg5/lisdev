<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="PayManageConfirm.js"></SCRIPT>
  <%@include file="PayManageConfirmInit.jsp"%>
  <title>付费管理 </title>
</head>

<body  onload="initForm();" >
  <form action="./SendToBankConfirmSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
            <TD  class= title>
           给付号码
          </TD>
          <TD  class= input>
            <Input class= common  name=ActuGetNo >
          </TD>
            <TD  class= title>
           案件号/批单号
          </TD>
          <TD  class= input>
            <Input class= common  name=CaseNo> 
          </TD> 
          </TD>
            <TD  class= title>
           付费方式
          </TD>
          <TD  class= input>
            <Input NAME=PayMode VALUE=""  CLASS=codeNo MAXLENGTH=20  ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,'1 and code not in (#8#)','1',1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName],[0,1],null,null,null,1);"><input class=codename name=PayModeName readonly=true elementtype=nacessary>  
          </TD>                   
        </TR>          
      	<TR  class= common>   
          <TD  class= title>
            领取人
          </TD>
          <TD  class= input>
            <Input class= common  name=Drawer >
          </TD>    		       
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class=codeNo readonly=true name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >
          </TD>
          <TD  class= title>
            锁定状态
          </TD>
          <TD  class= input>
          	<Input NAME=CanSendBank VALUE=""  CLASS=codeNo CodeData="0|^0|未锁定^1|已锁定" MAXLENGTH=20  ondblclick="return showCodeListEx('CanSendBank',[this,CanSendBankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CanSendBank',[this,CanSendBankName],[0,1],null,null,null,1);"><input class=codename name=CanSendBankName readonly=true> 
          </TD>  
        </TR>
        
    </table>
          <INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="重  置" class= CssButton TYPE=button onclick="resetFm();"> 
          <input type=hidden name="strSQL">  
		  <INPUT VALUE="清单下载" class=cssButton TYPE=button onclick="submitForm()">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 尚未付费成功的信息
    		</td>
    	</tr>
    </table>  
  	 
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	
      <Div  id= "divPage" align=center style= "display: '' ">
      <INPUT CLASS=CssButton VALUE="首  页" class= CssButton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=CssButton VALUE="上一页" class= CssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=CssButton VALUE="下一页" class= CssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=CssButton VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage.lastPage();">
      </Div> 	
      				
  	</div>
    <br> 
  

  	<Div  id= "divLCPol2"  name=BankInfo style= "display: 'none'">  
    <table>
    	<tr>
    		<td class= titleImg>
    			 修改银行信息
    		</td>
    	</tr>
    </table>          	
    <table  class= common align=center>    
    	<!--TR CLASS=common>
        <TD CLASS=title>
          付费方式 
        </TD>
        <TD CLASS=input COLSPAN=1>
           <!--Input NAME=PayMode2 VALUE=""  CLASS=codeNo CodeData="0|^1|现金^4|银行转账" MAXLENGTH=20  ondblclick="return showCodeListEx('PayMode',[this,PayModeName2],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode',[this,PayModeName2],[0,1],null,null,null,1);" verify="code:PayMode"><input class=codename name=PayModeName2 readonly=true-->
           <!--Input NAME=PayMode2 VALUE=""  CLASS=codeNo  ondblclick="return showCodeList('PayMode',[this,PayModeName2],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName2],[0,1],null,null,null,1);" verify="code:PayMode"><input class=codename name=PayModeName2 readonly=true>  
        </TD>    		
        <TD CLASS=title> 
        </TD>
        <TD CLASS=input COLSPAN=1>
        </TD>
        <TD CLASS=title>
        </TD>
        <TD CLASS=input COLSPAN=1>
        </TD>
      </TR-->    	  
    	<TR CLASS=common>
        <TD CLASS=title>
          银行代码 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=BankCode VALUE="" MAXLENGTH=10 CLASS=codeNo ondblclick="return showCodeList('sendbank', [this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank', [this,BankCodeName],[0,1]);" verify="code:bank" ><input class=codename name=BankCodeName readonly=true >
        </TD>    		
        <TD CLASS=title>
          账户名称 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20 readonly=true>
        </TD>
        <TD CLASS=title>
          银行账号 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=AccNo VALUE="" CLASS=common MAXLENGTH=40>
        </TD>
      </TR>
    </table>
    <br>
    
  	</div>     
  	<p>        
    <INPUT VALUE="付费方式修改" class= cssButton TYPE=button onclick="changePaymode()">
    <INPUT VALUE="账户信息修改" class= cssButton NAME=BankInfoButt TYPE="button" style="display: 'none'" onclick="changeAcc()">
    <INPUT VALUE="付费锁定" class= CssButton TYPE=button onclick="lock()">   		
    <INPUT VALUE="解除锁定" class= CssButton TYPE=button onclick="unlock()"> 
    
  	</p>   
  	<INPUT TYPE="hidden" NAME="workType">  	
  	<INPUT VALUE="" TYPE=hidden name=gnNo>
    <INPUT VALUE="" TYPE=hidden name=pNo>  	
    
    <INPUT VALUE="" TYPE="hidden" name='PrtNo'> 
    <INPUT VALUE="" TYPE="hidden" name='CustAgent'>
    <INPUT VALUE="" TYPE="hidden" name='CustCom'>
    <INPUT VALUE="" TYPE="hidden" name='PayMode2'>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
