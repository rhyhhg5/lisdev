<html>
	<%
	//Name：LLMainAskInput.jsp
	//Function：登记界面的初始化
	//Date：2005-07-23 16:49:22
	//Author：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="FinBankInput.js"></SCRIPT>
		<%@include file="FinBankInputInit.jsp"%>
	</head>
	<body  onload="initForm();">
		<form action="./FinBankSave.jsp" method=post name=fm target="fraSubmit">
				<table>
					<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLMainAskInput);">
					</TD>
					<TD class= titleImg>添加已有银行分行或支行</TD>		
					<td>
		    		  <font color="#FF0000" size="2">
		    		    	&nbsp;&nbsp;配置在四位机构下的银行认定为分公司省级分行，一个四位机构下一个银行仅允许配置一个省级分行，支公司下配置的银行认定为支行。
		    		  </font>
		    		</td>							
					</TR>
				</table>
        <Div  id= "divLLMainAskInput" style= "display: ''">
				<table class= common>
					<TR class= common8>
						<TD  class= title8>选择银行</TD>
						<TD  class= input8><Input NAME=BankCode VALUE="" MAXLENGTH=10 CLASS=codeNo  readonly=true ondblclick="return showCodeList('banknum', [this,BankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('banknum', [this,BankCodeName],[0,1]);" verify="code:banknum" ><input class=codename name=BankCodeName readonly=true ></TD>
						<TD  class= title8>所属机构</TD>
						<TD  class= input8 > <Input class=codeNo readonly=true name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true ></TD>
					</TR>
					<TR class= common8>
						<TD  class= title8>录入所需银行名称</TD>
						<TD  class= input8> <input class="common"  name=ChildBankName></TD>
            <TD  class= title8>所加银行代码</TD>
						<TD  class= input8><input class="common" readonly=true name=ChildBankCode>
						<!-- 省级分行标志 -->
						<input class="common"  type = "hidden"  name=mProvincialBanch>
						</TD>
					</TR>
					<TR class= common8>
						<!--  <TD  class= title8>收费帐号</TD>  -->
						<TD  class= input8> 
						  <input class="common" TYPE=hidden name=GetBankAccNo></TD>
                       <!--  <TD  class= title8>付费帐号</TD>  -->
						<TD  class= input8>
						  <input class="common"  TYPE=hidden name=PayBankAccNo></TD>
					</TR>
				</table>
						<INPUT VALUE="确  定" class= cssButton TYPE=button onclick="ChildBankButt();">
						<INPUT VALUE="批量导入" class= cssButton TYPE=button onclick="BatchInput();">
				<br>
			</div>
				<!--HR>
				<table>
					<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLMainAskInput1);">
					</TD>
					<TD class= titleImg>添加新银行</TD>									
					</TR>
				</table>
				<Div  id= "divLLMainAskInput1" style= "display: ''">
				<table class= common>
					<TR class= common8>
						<TD  class= title8 style= "width: '5%'">银行名称</TD>
						<TD  class= input8> <input class="common"  name=HeadBankName  ></TD>
						<TD  class= title8 style= "width: '5%'">所加银行代码</TD>
						<TD  class= input8> <input class="common"  readonly= true name=HeadBankCode  ></TD>
					</TR>
				</table>
				<INPUT VALUE="确  定" class= cssButton TYPE=button onclick="HeadBankButt();">
				<br>
			</div-->
				<HR>
				<table>
					<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLMainAskInput2);">
					</TD>
					<TD class= titleImg>打印银行列表清单</TD>									
					</TR>
				</table>
				<Div  id= "divLLMainAskInput2" style= "display: ''">
				<table class= common>
					<TR class= common8>
			      <input type="radio" value=1 checked name=RgtState onclick="SetFlag()">所有银行</input>
			      <input type="radio" value=2 name=RgtState onclick="SetFlag()">可转账银行</input>
					</TR>
				</table>
				<INPUT VALUE="打  印" class= cssButton TYPE=button onclick="PrintList();">
				<br>
			</div>	
					
				<HR>
				<table>
					<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLMainAskInput3);">
					</TD>
					<TD class= titleImg>银行查询及修改</TD>									
					</TR>
				</table>
				<Div  id= "divLLMainAskInput3" style= "display: ''">
				<table class= common>
					<TR class= common8>
						<TD  class= title8 style= "width: '10%'">所加银行代码</TD>
						<TD  class= input8> <input class="common"  name=mBankCode  ></TD>
						<TD  class= title8 style= "width: '10%'">银行名称</TD>
						<TD  class= input8> <input class="common"   name=mBankName  ></TD>	
					</TR>
				</table>
				<INPUT VALUE="查  询" class= cssButton TYPE=button onclick="QueryBankButt();">
				<TR class= common8><TD  class= input8> <input class="common"   name=mComCode  TYPE=hidden  ></TD></TR>
				<br>
			</div>	
      <Div  id= "divLLMainAskInput4" align=center style= "display: ''">
        <Table  class= common>
            <TR  class= common>
             <TD text-align: left colSpan=1>
                 <span id="spanQueryLDBankGrid" ></span> 
       	    </TD>
           </TR>
         </Table>					
           <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
           <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
           <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
           <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div>				
				<HR>
				<Div  id= "divLLMainAskInput5" style= "display: 'none'">
				<table class= common>
					<TR class= common8>
						<TD class=title8 style= "width: '5%'">银行名称</TD>
				    <TD class= input8><input class="common"  name=repBankName style= "width: '80%'"></TD>
						<TD class=title8 style= "width: '5%'">所属机构</TD>
				    <TD class= input8><input class="common"  name=repComCode style= "width: '80%'"></TD>
					</TR>
					<TR class= common8 style= "display: 'none'">
						<TD class=title8 style= "width: '5%'">收费帐号</TD>
				    <TD class= input8><input class="common"  name=repGetAccNo style= "width: '80%'"></TD>
				    <TD class=title8 style= "width: '5%'">付费帐号</TD>
				    <TD class= input8><input class="common"  name=repPayAccNo style= "width: '80%'"></TD>
					</TR>
				</table>
				<INPUT VALUE="修  改" class= cssButton TYPE=button onclick="RepBankButt();">
				<INPUT VALUE="删  除" class= cssButton TYPE=button onclick="DelBankButt();">
				<TR class= common8><TD  class= input8> <input class="common"   name=repBankCode  TYPE=hidden  ></TD></TR>
			</div>				
				<!--table class= common>
					<TR>
          <TD>
          	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLMainAskInput2);">
          </TD>
				  <TD class= titleImg>添加银行转帐参数</TD>		
					</TR>				
				</table>
				<Div  id= "divLLMainAskInput2" style= "display: 'none'">
				<table class= common>
          <TR class= common8>
						<TD  class= title8>选择银行</TD>
						<TD  class= input8><Input NAME=DealBankCode VALUE="" MAXLENGTH=10 CLASS=codeNo ondblclick="return showCodeList('sendbank', [this,DealBankName],[0,1]);" onkeyup="return showCodeListKey('sendbank', [this,DealBankName],[0,1]);" verify="code:sendbank" ><input class=codename name=DealBankName readonly=true ></TD>
						<TD  class= title8>所属机构</TD>
						<TD  class= input8 > <Input class=codeNo readonly=true name=ComCode ondblclick="return showCodeList('comcode',[this,ComCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComCodeName],[0,1]);"><input class=codename name=ComCodeName readonly=true ></TD>
					</TR>
					<TR class= common8>
						<TD class=title8 style= "width: '5%'">代收发送配置文件名称</TD>
				    <TD class= input8><input class="common"  name=GetSendFileName style= "width: '80%'"></TD>
						<TD class=title8 style= "width: '5%'">代收成功参数</TD>
				    <TD class= input8><input class="common"  name=GetSuccFlag style= "width: '80%'"></TD>
					</TR>
					<TR class= common8>
						<TD class=title8 style= "width: '5%'">代收回盘配置文件名称</TD>
				    <TD class= input8><input class="common"  name=GetReturnFileName style= "width: '80%'"></TD>
					</TR>
					<TR class= common8>
						<TD class=title8 style= "width: '5%'">代扣发送配置文件名称</TD>
				    <TD class= input8><input class="common"  name=PaySendFileName style= "width: '80%'"></TD>
				    <TD class=title8 style= "width: '5%'">代扣成功参数</TD>
				    <TD class= input8><input class="common"  name=PaySuccFlag style= "width: '80%'"></TD>
					</TR>
					<TR class= common8>
						<TD class=title8 style= "width: '5%'">代扣回盘配置文件名称</TD>
				    <TD class= input8><input class="common"  name=PayReturnFileName style= "width: '80%'"></TD>
					</TR>
				</table>
				<INPUT VALUE="确  定" class= cssButton TYPE=button onclick="DealBankButt();">
			</div-->
			<br>
			<input name= sql type=hidden class=common>
			<input name= mOperate type = hidden class=common>
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
