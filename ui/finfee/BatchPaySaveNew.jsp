 <%
//程序名称：BatchPaySave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>    
  <%@page import="com.sinosoft.lis.pubfun.*"%> 
  <%@page import="java.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%

   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {    	
	
	String getBankCode = request.getParameter("tBankCode"); //本方银行
	String getBankAccNo = request.getParameter("tBankAccNo");//本方账号
	String otherBankCode = request.getParameter("otherBankCode");//对方银行
	String otherBankAccNo = request.getParameter("otherBankAccNo");//对方账号
	
	System.out.println("对方银行::"+otherBankCode);
	System.out.println("对方账号::"+otherBankAccNo);
	System.out.println("本方银行::"+getBankCode);
	System.out.println("本方账号::"+getBankAccNo);
	
	String tChk[]=request.getParameterValues("InpLJAGetGridChk");
  	String tActuGetNo[] = request.getParameterValues("LJAGetGrid1");
  	String tPolNo[] = request.getParameterValues("LJAGetGrid2");
  	String tGetMoney[] = request.getParameterValues("LJAGetGrid4");
  	String tPayMode[] = request.getParameterValues("LJAGetGrid5");
  	String tGetBankCode[] = request.getParameterValues("LJAGetGrid6");//本方银行编码
  	String tGetBankAccNo[] = request.getParameterValues("LJAGetGrid7");//本方银行账号
  	String tBankCode[] = request.getParameterValues("LJAGetGrid8"); //对方银行
  	String tBankAccNo[] = request.getParameterValues("LJAGetGrid9");//对方账号
  	String tAccName[] = request.getParameterValues("LJAGetGrid10");
  	String tDrawer[] = request.getParameterValues("LJAGetGrid11");
  	String tChequeNo[] = request.getParameterValues("LJAGetGrid12");
  	String tEnterAccDate[] = request.getParameterValues("LJAGetGrid13");
  	String tDrawerID[] = request.getParameterValues("LJAGetGrid14");
    String tRiskcode[] = request.getParameterValues("LJAGetGrid20");
   	LJFIGetSchema tLJFIGetSchema ; //财务给付表      
   	LJAGetSchema  tLJAGetSchema  ; //实付总表
   	LJAGetSet  mLJAGetSet = new LJAGetSet();
	 LJFIGetSet mLJFIGetSet = new LJFIGetSet();
	 	VData mVData = new VData();
	 	HashMap mapO=new HashMap();
	 	for(int i=0;i<tChk.length;i++){
			if(tChk[i]!=null && tChk[i].equals("1")){
				tLJAGetSchema=new LJAGetSchema();
				tLJAGetSchema.setActuGetNo(tActuGetNo[i]);
				tLJAGetSchema.setPayMode(tPayMode[i]);
				VData tVData = new VData();
				tVData.add(tLJAGetSchema);
				LJAGetQueryUI tLJAGetQueryUI = new LJAGetQueryUI();
   			if(!tLJAGetQueryUI.submitData(tVData,"QUERY")){
   				Content = " 查询实付总表失败，原因是: " + tLJAGetQueryUI.mErrors.getError(0).errorMessage;
       		FlagStr = "Fail";
   			}else{
   			    if( mapO.containsKey(tActuGetNo[i].trim())){
			    LJFIGetSchema FIGetSchema=(LJFIGetSchema) mapO.get(tActuGetNo[i].trim());
			    double money= FIGetSchema.getGetMoney();
       	 		money=money+Double.parseDouble(tGetMoney[i]);
       	 		FIGetSchema.setGetMoney(money);
			 	 }else{ 
   				tVData.clear();      
			    tVData = tLJAGetQueryUI.getResult();
			    tLJAGetSchema=null;
			    tLJAGetSchema=((LJAGetSet)tVData.getObjectByObjectName("LJAGetSet",0)).get(1);
			    tLJAGetSchema.setEnterAccDate(tEnterAccDate[i]);
    			tLJAGetSchema.setDrawer(tDrawer[i]);
    			tLJAGetSchema.setDrawerID(tDrawerID[i]);
    			tLJAGetSchema.setOperator(tGI.Operator); 
    			tLJAGetSchema.setChequeNo(tChequeNo[i]);
    			tLJAGetSchema.setAccName(tAccName[i]);
    			//对方银行
    			if(!"".equals(otherBankCode) && otherBankCode!=null){
    				tLJAGetSchema.setBankCode(otherBankCode);
    			}else{
    				tLJAGetSchema.setBankCode(tBankCode[i]);
    			}
    			if(!"".equals(otherBankAccNo)&& otherBankAccNo!=null){
    				tLJAGetSchema.setBankAccNo(otherBankAccNo);
    			}else{
    				tLJAGetSchema.setBankAccNo(tBankAccNo[i]);
    			} 
    			//本方银行
    			if(!"".equals(getBankCode) && getBankCode!=null){
    				tLJAGetSchema.setInsBankCode(getBankCode);
    			}else{
    				tLJAGetSchema.setInsBankCode(tGetBankCode[i]);
    			}
    			if(!"".equals(getBankAccNo) && getBankAccNo!=null){
    				tLJAGetSchema.setInsBankAccNo(getBankAccNo);
    			}else{
    				tLJAGetSchema.setInsBankAccNo(tGetBankAccNo[i]);
    			}
    			tLJFIGetSchema=new LJFIGetSchema();
			    tLJFIGetSchema.setActuGetNo(tActuGetNo[i]);
			    tLJFIGetSchema.setPayMode(tPayMode[i]);
			    tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
			    tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
			    tLJFIGetSchema.setGetMoney(tGetMoney[i]);
			    tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
			    tLJFIGetSchema.setEnterAccDate(tEnterAccDate[i]);
			    tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
			    tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
			    tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
			    tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
			    tLJFIGetSchema.setDrawer(tDrawer[i]);
			    tLJFIGetSchema.setDrawerID(tDrawerID[i]);
			    tLJFIGetSchema.setOperator(tGI.Operator);
			    tLJFIGetSchema.setBankCode(tLJAGetSchema.getBankCode());
			    tLJFIGetSchema.setBankAccNo(tLJAGetSchema.getBankAccNo());
			    tLJFIGetSchema.setChequeNo(tChequeNo[i]);    
			    tLJFIGetSchema.setAccName(tAccName[i]); 	
    			
				mapO.put(tActuGetNo[i].trim(), tLJFIGetSchema);
			    mLJAGetSet.add(tLJAGetSchema);
			    }
   			}
			}
	 	}
	 	Collection c=mapO.values();
        Iterator it=c.iterator();
        for(;it.hasNext();){
        	mLJFIGetSet.add((LJFIGetSchema) it.next());
        	
        }
        mapO.clear();
	 	System.out.println("mLJFIGetSet="+mLJFIGetSet.size());
	 	 System.out.println("mLJAGetSet="+mLJAGetSet.size());
	 	 for(int i=1;i<=mLJAGetSet.size();i++){
	 	 if(mLJAGetSet.get(i).getSumGetMoney()!=mLJFIGetSet.get(i).getGetMoney()){
	 	   FlagStr="Fail";
	 	   Content="实付号"+mLJAGetSet.get(i).getActuGetNo()+"金额不一致，可以原因：存在多个险种，但只对其中部分险种进行了付费，请选择该实付数据下的所有险种记录进行付费操作";
	 	   System.out.println("mLJAGetSet.get(i).getSumGetMoney():"+mLJAGetSet.get(i).getSumGetMoney());
	 	   System.out.println("mLJFIGetSet.get(i).getGetMoney():"+mLJFIGetSet.get(i).getGetMoney());
	 	   }
	 	 }
	 	mVData.add(mLJFIGetSet);
		mVData.add(mLJAGetSet);
		mVData.add(tGI);
		System.out.println("FlagStr:"+FlagStr);
		if(FlagStr!="Fail"){
		BatchPayUI tBatchPayUI=new BatchPayUI();
		if(tBatchPayUI.submitData(mVData,"VERIFY")==true){
			tBatchPayUI.submitData(mVData,"CLAIM");
			tError = tBatchPayUI.mErrors;
		}else{
			tError = tBatchPayUI.mErrors;
		}
		}  
    if(FlagStr=="Fail"){
       Content=" 失败，" +Content;
    }else{
   	   if (tError.needDealError()){
   	   	    Content = " 失败，原因:" + tError.getFirstError();
   	    	FlagStr = "Fail";
     	}else{
  	    	Content = " 操作成功";
     	    FlagStr = "Succ"; 
     	}
     }
%> 
<HTML>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</HTML>   
<%}%> 