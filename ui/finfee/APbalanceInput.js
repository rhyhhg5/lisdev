//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var arrDataSet;

function initGlobalData()
{
//垫付金额汇总 
 tPayNo = 0.0;
//社保报销金额汇总
 tGetNo = 0.0;
}

// 查询按钮
function easyQueryClick()
{	
	// 初始化表格
	initPolGrid();
	if(fm.all("ContNo").value==""){
	alert("保单号码不能为空！");
	return false;
	}
	if(fm.all("StartDate").value==""||fm.all("EndDate").value==""){
	alert("起始日期和截止日期不能为空！");
	return false;
	}
   var ContNo=fm.all("ContNo").value;
   var ManageCom=fm.all("ManageCom").value;
	// 书写SQL语句
	var strSQL = "";
     strSQL = "select a.grpcontno, b.otherno,(select grpname from lcgrpcont where grpcontno=a.grpcontno union all select grpname from lbgrpcont where grpcontno=a.grpcontno),(select name from lcinsured where grpcontno=a.grpcontno and contno=a.contno and insuredno=a.insuredno union all select name from lbinsured where grpcontno=a.grpcontno and contno=a.contno and insuredno=a.insuredno),money,(select money*double(replace(othersign,'%',''))/100 from ldcode d where a.grpcontno=d.codealias and d.codetype='TJAccident' and d.code=(select trim(CHAR(YEAR(cinvalidate))) from lcgrpcont where grpcontno=a.grpcontno fetch first 1 rows only)||'GrpContNo' ),(select othersign from ldcode d where a.grpcontno=d.codealias and d.codetype='TJAccident' and d.code=(select trim(CHAR(YEAR(cinvalidate))) from lcgrpcont where grpcontno=a.grpcontno fetch first 1 rows only)||'GrpContNo' ),a.actugetno  from LLCasePayAdvancedTrace a,ljaget b where a.actugetno=b.actugetno and a.dealstate='02' and b.othernotype='D' and b.confdate is not null and exists (select 1 from ldcode d where a.grpcontno=d.codealias and d.codetype='TJAccident' and d.code=(select trim(CHAR(YEAR(cinvalidate))) from lcgrpcont where grpcontno=a.grpcontno fetch first 1 rows only)||'GrpContNo' and d.othersign is not null) "
        + "and  b.confdate between '"+fm.all("StartDate").value+"' and '"+fm.all("EndDate").value+"' "
        + getWherePart('a.grpcontno','ContNo');
     strSQL = strSQL + getWherePart( 'a.ManageCom','ManageCom','like' ) ;
     strSQL = strSQL + " order by b.otherno" ;

	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL); 

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
   // alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
    
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  PolGrid.SortPage=turnPage;  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);   
}

function showdivLCPol2() {
    if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1)=='')
  {
    	 fm.all("divLCPol2").style.display = "none";
  }
else
{    var fee=PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3);
	  fm.all("divLCPol2").style.display = "";
     fm.all("tContNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
     fm.all("Feevalue").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3);
	 
}	  
}
function ok()
{  initGlobalData();
   var haveCheck = false;
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getChkNo(i)) {
      haveCheck = true;
      break;
    }
  }
if (haveCheck) {
    fm.all("divLCPol2").style.display = "";
    tPayNo=0.0;
    var i = 0;
    PolGridCount=PolGrid.mulLineCount;//PolGrid的行数
    
    for(i=0;i<PolGridCount;i++)
  {
      if (PolGrid.getChkNo(i)) {
       tPayNo = tPayNo+parseFloat(PolGrid.getRowColData(i,5));//垫付金额汇总累计
       tGetNo = tGetNo+parseFloat(PolGrid.getRowColData(i,6));//社保报销金额汇总
      }
  }
    fm.all('tContNo').value=PolGrid.getRowColData(0,1);
    fm.all('Feevalue').value=PolGrid.getRowColData(0,7);
    fm.all('tPayNo').value=tPayNo;
    fm.all('tGetNo').value=tGetNo;
  }
  else {
    alert("请先选择待结算案件，在选择框中打钩！");
    fm.all("divLCPol2").style.display = "none";
   
  }
}

function save()
{  
fm.all('WorkType').value="save";
    //首先检验录入框


  if(fm.all('tContNo').value!=""&&fm.all('tGetNo').value!="")
  {  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交
  showInfo.close();
  }
  else
   alert("请先查询！");
}






//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content  )
{    
      if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,
                    "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
     resetFm();
    //执行下一步操作
  }
}
function resetFm()
{
  fm.all('WorkType').value="";
  fm.all('tContNo').value="";
  fm.all('Feevalue').value="";
  fm.all('ContNo').value = '';
  initForm();
}
