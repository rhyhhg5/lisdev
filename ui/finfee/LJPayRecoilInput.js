var showInfo;

var turnPage = new turnPageClass(); 

function easyQueryClick()
{
	if(fm.StartDate.value=='' || fm.EndDate.value==''){
		alert('起始日期或截至日期不能为空!');
		return false;
	}
	
	var strWhere="";
 	if(fm.CertificateNo.value!=''){
 		strWhere+=" and  actugetno='"+fm.CertificateNo.value+"' ";
 	}
 	if(fm.OtherNo.value!=''){
 		strWhere+=" and  otherno='"+fm.OtherNo.value+"' ";
 	}
 	if(fm.ByPayMode.value!=''){
 		strWhere+=" and  PayMode='"+fm.ByPayMode.value+"' ";
 	}

 	strWhere+=" and confdate>='"+fm.StartDate.value+"' and confdate<='"+fm.EndDate.value+"' ";

	var strSQL="select actugetno,otherno,paymode,bankcode,bankaccno,insbankcode,insbankaccno,confdate from ljaget where confdate is not null and confdate<current date and sumgetmoney>=0 "
			+strWhere;		
			strSQL+=getWherePart('ManageCom','ComCode',"like");	
			
	turnPage.queryModal(strSQL, RecoilGrid); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

function save(){
	var tSel = RecoilGrid.getSelNo();
	if(tSel==0){
		alert('请选择一条记录');
		return false;
	}
	if(fm.paymode.value==''){
		alert('交费方式不能为空！');
		return false;
	} 
	if(fm.paymode.value=='2' || fm.paymode.value=='3' || fm.paymode.value=='11' || fm.paymode.value=='12' || fm.paymode.value=='4' ){
		if(fm.getbankaccno.value=='' || fm.getbankcode.value==''  ){
			alert('本方银行和本方银行帐号不能为空！');
			return false;
		}
	}
 if(fm.paymode.value=='4'){
	  if(fm.BankCode.value=='' || fm.AccNo.value==''){
		 alert('对方银行和对方银行帐号不能为空！');
		 return false;
	  }
 }
	
	if(confirm('是否确定对该交费信息进行修改')==true){
		try 
		{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.ActuGetNo.value=RecoilGrid.getRowColData(tSel-1,1);
				fm.OperateType.value="PAY";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close( );
  		alert(ex);
  	}
	}
}


function getInfo(){
	var tSel = RecoilGrid.getSelNo();
	var tPayMode=RecoilGrid.getRowColData(tSel-1,3);
	var tBankCode=RecoilGrid.getRowColData(tSel-1,4);
	var tBankAccNo=RecoilGrid.getRowColData(tSel-1,5);
	var tInsBankCode=RecoilGrid.getRowColData(tSel-1,6);
	var tInsBankAccNo=RecoilGrid.getRowColData(tSel-1,7);
	fm.paymode.value=tPayMode;
	fm.BankCode.value=tBankCode;
	fm.AccNo.value=tBankAccNo;
	fm.getbankcode.value=tInsBankCode;
	fm.getbankaccno.value=tInsBankAccNo;
}
        

