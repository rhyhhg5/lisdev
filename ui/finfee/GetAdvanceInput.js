
function submitForm() {
  //检验录入框
    if (!verifyInput()){
        return false;
    }
    if (!checkValue()) {
        return false;
    }
    var i = 0;
    var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交
}

function querybank()
{
	bankflag="0";
	showInfo = window.open("../bq/LDBankQueryMain.jsp");
}

function payModeHelp(){
    window.open("./PayModeHelp.jsp");
}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  clearFormData();
}
function clearFormData(){

}

function queryAgent()
{
    if(fm.all('ManageCom').value==""){
         alert("请先录入管理机构信息！");
         return;
    }
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function afterQuery(arrReturn)
{
    fm.all('AccBank').value = arrReturn[0];
    fm.all('AccBankName').value = arrReturn[1];
}

function afterQuery2(arrReturn)
{
   fm.all('AgentCode').value = arrReturn[0][0];
   var rsSQL = "select getUniteCode('"+arrReturn[0][0]+"') from dual";
   var rs = easyExecSql(rsSQL);
   fm.GroupAgentCode.value = rs;
}

function BatchGet(){   
    window.open("./GetAdvanceBatchMain.jsp");
}

//如果付费方式选择“转帐支票”，则显示银行帐号
function showBankAccNo()
{
  if(fm.all('PayMode').value=='2'||fm.all('PayMode').value=='3'||fm.all('PayMode').value=='4'||fm.all('PayMode').value=='11' || fm.all('PayMode').value=='12')
  {
    divBankAccNo.style.display="";
  }
  else  
  {
    divBankAccNo.style.display="none";
    fm.all('AccBank').value = "";
    fm.all('ChequeNo').value = "";
    fm.all('BankAccNo').value = "";
    fm.all('AccName').value = "";
    fm.all('getbankcode').value = "";
    fm.all('getbankaccno').value = "";
  } 
}

function afterCodeSelect(){
  showBankAccNo();  
}


function checkValue()
{
     if(fm.all('PayMoney').value>0){
     } else {
        alert("收费金额格式错误或金额小于零！");
        return false ;
     };
    //不允许为9的收费方式
    if(fm.all('PayMode').value == '9'){
        alert("请重新选择付费方式！");
        return false ;
    }
  //如果收费方式是支票类
  if(fm.all('PayMode').value=='2'||fm.all('PayMode').value=='3')
  {
    if(fm.all('AccBank').value==''||fm.all('ChequeNo').value=='')  
      {
        alert("收费方式是支票:开户银行和票据号码不能为空！");
        return false;
      }
  }
  
    if(fm.all('PayMode').value!='1' && fm.all('PayMode').value!='5')
  {
    if(fm.all('getbankcode').value==''||fm.all('getbankaccno').value=='')   
      {
        alert("由于收费方式不是现金:本方银行编码和本方银行帐号不能为空！");
        return false;
      }
  }
 
  return true;
    
}