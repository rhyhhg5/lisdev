<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2013-10-12
		//创建人  ：yan
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="GrpRiskPrePrint.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<%@include file="GrpRiskPrePrintInit.jsp"%>
	</head>
	<body onload="initElementtype();initForm();">
		<form method=post name=fm target="fraSubmit">

				<strong><IMG id="a1" src="../common/images/butExpand.gif"
						style="cursor:hand;" OnClick="showPage(this,divOperator);" />集团风险保费报表打印
				</strong>

			<Div id="divOperator" style="display: ''">
			   <font color="#FF0000" size="3">
    		   注意：因数据量较大原因，可能导致报表打印时间较长，请耐心等待，不要重复点击打印按钮！
    		   </font>
    		    <p>
			</Div>

			<p>
				<strong><IMG id="a2" src="../common/images/butExpand.gif"
						style="cursor:hand;" OnClick="showPage(this,divMonthPrint);" />请输入查询范围
				</strong>
			</p>

			<Div id="divMonthPrint" style="display: ''">
				<table class="common">
						<TR class=common>
							<TD class=title>
								公司代码
							</TD>
							<TD class=input>
								<Input class= "codeno"  name=ManageCom readonly="readonly" ondblclick="return showCodeList('comcode',[this,CompanyCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,CompanyCodeName],[0,1],null,null,null,1);" ><Input class=codename readonly="readonly" name=CompanyCodeName elementtype=nacessary>
							    
							</TD>
							<TD class=title>
								起始日期
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="StartDate" name="StartDate" verify="起始时间|NOTNULL" elementtype=nacessary>
							</TD>
							<TD class=title>
								结束日期
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="EndDate" name="EndDate" verify="结束时间|NOTNULL" elementtype=nacessary>
							</TD>
						</TR>
				</table>
				<p>
				</p>

				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="PreReceivePrint" value="应收保费报表(集团风险)"
					onclick="PreReceive_Print()" />
				<br />
				<br />
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="PreBudgetPrint" value="保费收支报表(集团风险)"
					onclick="PreBudget_Print()" />
				<br />
				<br />
			</Div>

			<input type="hidden" id="fmtransact" name="fmtransact" />
			<input type="hidden" name="Opt" />
		</form>

		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
