<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：FirstPayInput.jsp
//程序功能：银行转账先收费录入界面
//创建日期：2010-01-18 
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="FirstPayInput.js"></SCRIPT>
  <%@include file="FirstPayInit.jsp"%>
  
  <title>银行转账先收费信息录入 </title>
</head>

<body  onload="initForm();initElementtype();" >
  <form action="./FirstPaySave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 暂交费信息部分 fraSubmit-->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请录入相关信息：</td>
  		</tr>
  	</table>
  	
    <table  class= common>
    <TR  class= common>
          <TD  class= title>
        管理机构
      </TD>
      <TD  class= input>
      	<Input class="codeNo"  name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary >
      </TD>
      
            
      <TD  class= title>
            业务员
      </TD>
      <TD class="input" width="20%">
           <!--Input class="code" name=AgentCode verify="代理人编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');" onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');"-->     
         <Input class=common name=AgentCode elementtype=nacessary /><input class=cssButton type="button" value="查  询" style="width:60" onclick="queryAgent()"/>    
      </TD> 
     
    </TR>
    <TR  class= common>
      <TD  class= title>
        印刷号
      </TD>
      <TD  class= input>
        <Input class=common name=PrtNo elementtype=nacessary >
      </TD>
      <TD  class= title>
            开户银行
      </TD>  
      <TD  class= input>
        <Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno  ondblclick="return showCodeList('sendbank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank" ><input class=codename name=BankCodeName readonly=true elementtype=nacessary>                  
      </TD> 
      <TD  class= title>
        银行帐号
      </TD>
      <TD  class= input>
        <Input class= common name=BankaccNo elementtype=nacessary>
      </TD>
    </TR>
      <TR  class= common>
      <TD  class= title>
        银行帐户名
      </TD>
      <TD  class= input>
        <Input class= common name=BankaccName elementtype=nacessary>
      </TD>
      <TD  class= title>
        交费金额
      </TD>
      <TD  class= input>
        <Input class= common name=PayMoney elementtype=nacessary>
      </TD>
    </TR>
   
 
    </table>
    <table align=left>
      <TR>
        <TD>
          <INPUT VALUE="添加一条" class= cssButton name = "AddButton" TYPE=button onclick="fillFeeGrid();">
        </TD>
        
      </TR>
    </table>
    <br><br>
    
    <!-- 暂交费信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJTempFee1);">
    		</td>
    		<td class= titleImg>
    			 银行转账信息 
    		</td>
    	</tr>
    </table>
	<Div  id= "divLJTempFee1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanFeeGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
	
  <Div id= "divPage" align=center style= "display: 'none' ">
  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  <table align=left>
    <tr>
      <td>
        <INPUT VALUE="录入确认" class= cssButton name = "ConfirmButton" TYPE=button onclick="submitForm();">
      </td>
    </tr>
  </table>         										
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    
  <form action="./TempFeeWithdrawPrintMain.jsp" method=post name=fm2 target="_blank">
    <Input type=hidden name=PrtData >
  </form>
  
</body>
</html>
