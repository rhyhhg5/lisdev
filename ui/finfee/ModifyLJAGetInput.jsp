<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：ModifyLJAGetInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head > 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="ModifyLJAGetInput.js"></SCRIPT> 
  <%@include file="ModifyLJAGetInit.jsp"%>
  
  <title>变更付款方式</title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  comCode = "<%=tGlobalInput.ComCode%>";
</script>

<body  onload="initForm();" >
  <form action="./ModifyLJAGetSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
	
    <table  class= common align=center>
    	<TR  class= common>
        <TD  class= title>
          实付通知书号
        </TD>
        <TD  class= input>
          <Input NAME=ActuGetNo class=common>
        </TD>
        <TD  class= title>
          其它号码
        </TD>
        <TD  class= input>
          <Input NAME=OtherNo class=common>
        </TD>
        <TD  class= input>
          <INPUT VALUE="查询" TYPE=button class=cssButton onclick="easyQueryClick();">
        </TD>
      </TR>
    </table>
    
    <hr>   
        
    <!-- 付款信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 付款信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </Div>
    
    <hr>
    
    <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>录入要修改的信息：</td>
		</tr>
	  </table> 
	     		  								
    <table  class= common align=center>
      <TR CLASS=common>
        <TD  class= title>
          实付通知书号
        </TD>
        <TD  class= input>
          <Input NAME=ActuGetNo2 class=common verify="实付通知书号|notnull">
        </TD>
        <TD  class= title>
          付款方式
        </TD>
        <TD  class= input>
          <Input NAME=PayMode CLASS=codeNo CodeData="0|2^1|现金^2|现金支票^3|转账支票^4|银行转账" ondblclick="return showCodeListEx('PayMode', [this,PayModeName],[0,1]);" onkeyup="return showCodeListKeyEx('PayMode', [this,PayModeName],[0,1]);" verify="银行代码|notnull&code:PayMode"><input class=codename name=PayModeName readonly=true >
        </TD>
        
      </TR>
      
    	<TR CLASS=common>
        <TD CLASS=title>
          银行代码 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=BankCode VALUE="" MAXLENGTH=10 CLASS=codeNo ondblclick="return showCodeList('bank', [this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('bank', [this,BankCodeName],[0,1]);" verify="" ><input class=codename name=BankCodeName readonly=true>
        </TD>
        <TD CLASS=title>
          银行账号 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=AccNo VALUE="" CLASS=common MAXLENGTH=40 verify="">
        </TD>
        <TD CLASS=title>
          账户名称 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20 verify="">
        </TD>
      </TR>
    </table>
    <br>
    <table align=right>
      <tr>
        <td>         
    <INPUT VALUE="保存数据" class= cssButton TYPE=button onclick="submitForm()">
        </td>
        <td>
    <INPUT VALUE="读取保单银行信息" class= cssButton TYPE=button onclick="readBankInfo()">
        </td>
      </tr>
    </table>
    <INPUT VALUE="" TYPE=hidden name=serialNo>
  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>

