<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TempFeeWithdrawSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  
<%
  System.out.println("\n\n---GetAdvanceDrawSave Start---");
  
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String strResult = "";

  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  
  TransferData tTransferData = new TransferData();
  LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
  
  String tTempFeeNo[] = request.getParameterValues("FeeGrid2");
  String tTempFeeType[] = request.getParameterValues("FeeGrid3");
  String tChk[] = request.getParameterValues("InpFeeGridChk"); 
  String tPayMode = request.getParameter("qPayMode");
  tTransferData.setNameAndValue("PayMode",tPayMode);
  
  for(int i = 0; i< tChk.length; i ++){
      if(tChk[i].equals("1")){
          LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
          tLJTempFeeSchema.setOtherNo(tTempFeeNo[i]);
          tLJTempFeeSchema.setTempFeeType(tTempFeeType[i]);    
          tLJTempFeeSet.add(tLJTempFeeSchema);
      }
  }
        
  // 准备传输数据 VData
	VData tVData = new VData();
	tVData.add(tLJTempFeeSet);
	tVData.add(tG);
    tVData.add(tTransferData);

  // 数据传输
	GetAdvanceDrawUI tGetAdvanceDrawUI = new GetAdvanceDrawUI();
	tGetAdvanceDrawUI.submitData(tVData, "INSERT");
  
  tError = tGetAdvanceDrawUI.mErrors;
  System.out.println("needDeal:"+tError.needDealError());
  if (!tError.needDealError()) {                          
  	Content = " 退费成功! ";
  	FlagStr = "Succ";
  	System.out.println("----------after submitData");
  }
  else {
  	Content = " 退费失败，原因是:" + tError.getFirstError();
  	FlagStr = "Fail";
  }

	System.out.println(Content + "\n" + FlagStr + "\n---GetAdvanceDrawSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.tempFeeNoQuery(1);
</script>
</html>
