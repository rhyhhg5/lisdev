<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<title>暂交费信息修改</title>
  
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT> 

  <script src="./TempFeeUpdate.js"></script>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="TempFeeUpdateInit.jsp"%>

</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm action=./TempFeeUpdateResult.jsp target=fraSubmit method=post>
    <table  class= common align=center>
      	
      	<TR  class= common>
          <TD  class= title>
          暂收据号:
          </TD>
          <TD  class= input>
            <Input class= common name=TempFeeNo >
          </TD>
        </TR>  
        <TR  class= common>
          <TD  class= title>
          印刷号:
          </TD>
          <TD  class= input>
            <Input class= common name=OtherNo >
          </TD>
        </TR>  
        <TR>
        <TD  class= input width="26%"> 
        	<Input class= cssButton type=Button value="查询" onclick="submitForm()">
      	</TD>  
      	</TR>
    </table>

      	
  <Div  id= "Frame1" style= "display: 'none'">    
      <table  class= common align=center>    	
      <TR  class= common>
          <TD  class= title>
            暂交费类型
          </TD>          
          <TD  class= input>
            <Input class="code" name=TempFeeType verify="暂交费类型|code:TempFeeType" ondblclick="return showCodeList('TempFeeType',[this]);" onkeyup="return showCodeListKey('TempFeeType',[this]);">
          </TD>        
      </TR>	 
      
      <TR  class= common> 
          <TD  class= title>
            管理机构
          </TD>          
          <TD  class= input>
	     <Input class="code" name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);" > 
	  </TD>
          <TD  class= title>
            交费日期
          </TD>
          <TD  class= input>
            <Input class="common" verify="交费日期|DATE&NOTNULL" name=PayDate >
          </TD>                                         
       </TR>         
       <TR  class= common>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
             <Input class="code" name=AgentCode verify="代理人编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');" onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');">          </TD>           
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly tabindex=-1 name=AgentGroup  >
          </TD>
          <input type=hidden name="Opt">
      </TR>  
       <TR  class= common>
          <TD  class= title>
            其它号码
          </TD>
          <TD  class= input>
             <Input class="common" name=OtherNo>    </TD>           
          <TD  class= title>
      </TR>                 	                 	 
   	   
   </Table> 
   </Div>
    
    
  <Div  id= "Frame2" style= "display: 'none'">     
  <!--暂交费表部分 -->  
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanTempGrid" >
	 </span> 
	</td>
       </tr>
    </Table>
    <Table>
    <tr>  </tr>
    </Table>




  <!--暂交费分类表部分 -->
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanTempClassToGrid" >
	 </span> 
	</td>
       </tr>
    </Table>
    <table>
    <TR class=input>     
     <TD class=common>
      <input type =button class=cssButton value="提交修改" onclick="submitForm1();">        
      <input type =button class=cssButton value="撤销修改" onclick="clearFormData();">    
     </TD>
    </TR>
    </table>
</Div>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</Form>
</body>
</html>










