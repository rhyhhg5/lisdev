  //               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




function easyQueryClick()
{
	  //判断关键定是否为空
	var strAgentCom=trim(document.fm.all("AgentCom").value);
	var strPayMode=trim(document.fm.all("PayMode").value);
	var strStartDate=trim(document.fm.all("StartDate").value);
	var strEndDate=trim(document.fm.all("EndDate").value);
	if(dateDiff(strStartDate,strEndDate,"M")>3)
	{
		window.alert("月份不得超过三个月");
		return false;
	}
	  if((strAgentCom!=""&&strAgentCom!=null)&&(strStartDate!=""&&strStartDate!=null)&&(strEndDate!=""&&strEndDate!=null))
	  {
			  //查询strSql语句如下
			  strSql="select (select name from ldcom where comcode=a.managecom) as managecom,"+
			  "(case a.conttype when '1' then a.contno else a.grpcontno end) as contno,"+
			  "a.appntname as appntname,"+
			  "a.insuredname as insuredname,"+
			  "a.cvalidate as cvalidate,"+
			  "a.cinvalidate as cinvalidate,"+
			  "(case a.conttype when '1' then a.prem else (select prem from lcgrpcont where grpcontno= a.grpcontno) end) as prem,"+
			  "(select paymode from ljtempfeeclass where tempfeeno in (select tempfeeno from ljtempfee where otherno=a.contno or otherno=a.grpcontno) fetch first 1 rows only) as paymode,"+
			  "(select confmakedate from ljtempfee where otherno=a.contno or otherno=a.grpcontno fetch first 1 rows only) as paydate,"+
			  "a.cvalidate as cvalidate,"+
			  "(select modifydate from LCContPrint where otherno=a.contno or otherno=a.grpcontno fetch first 1 rows only) as printdate,"+
			  "'' as posno,"+
			  "(select name from labranchgroup where agentgroup=a.agentgroup) as agentgroupname,"+
			  "(select name from laagent where agentcode=a.agentcode) as agentname,"+
			  "(select name from laagent where agentcode=a.agentcode) as agentname,"+
			  "(select insbankaccno from ljtempfeeclass where tempfeeno in (select tempfeeno from ljtempfee where otherno=a.contno or otherno=a.grpcontno) fetch first 1 rows only) as insbankaccno,"+
			  "'' as posadd "+
			  "from lccont a where a.signdate between '"+strStartDate+"' and '"+strEndDate+
			  "'and a.managecom like '"+strAgentCom+"%' ";
			  if(strPayMode!='null'&&strPayMode!=''&&strPayMode!=null){
			   strSql=strSql+" and exists(select 1 from ljtempfeeclass where tempfeeno in (select tempfeeno from ljtempfee where otherno=a.contno or otherno=a.grpcontno) and paymode='"+strPayMode+"' )";
			  }
			  strSql=strSql+"union "+
			  "select (select name from ldcom where comcode=b.managecom) as managecom,"+
			  "(case b.conttype when '1' then b.contno else b.grpcontno end) as contno,"+
			  "b.appntname as appntname,"+
			  "b.insuredname as insuredname,"+
			  "b.cvalidate as cvalidate,"+
			  "b.cinvalidate as cinvalidate,"+
			  "(case b.conttype when '1' then b.prem else (select prem from lcgrpcont where grpcontno= b.grpcontno) end) as prem,"+
			  "(select paymode from ljtempfeeclass where tempfeeno in (select tempfeeno from ljtempfee where otherno=b.contno or otherno=b.grpcontno) fetch first 1 rows only) as paymode,"+
			  "(select confmakedate from ljtempfee where otherno=b.contno or otherno=b.grpcontno fetch first 1 rows only) as paydate,"+
			  "b.cvalidate as cvalidate,"+
			  "(select modifydate from LCContPrint where otherno=b.contno or otherno=b.grpcontno fetch first 1 rows only) as printdate,"+
			  "'' as posno,"+
			  "(select name from labranchgroup where agentgroup=b.agentgroup ) as agentgroupname,"+
			  "(select name from laagent where agentcode=b.agentcode) as agentname,"+
			  "(select name from laagent where agentcode=b.agentcode) as agentname,"+
			  "(select insbankaccno from ljtempfeeclass where tempfeeno in(select tempfeeno from ljtempfee where otherno=b.contno or otherno=b.grpcontno) fetch first 1 rows only) as insbankaccno,"+
			  "'' as posadd "+
			  "from lccont b where b.signdate between '"+strStartDate+"' and '"+strEndDate+
			  "'and b.managecom like '"+strAgentCom+"%'";
			  if(strPayMode!='null'&&strPayMode!=''&&strPayMode!=null){
			   strSql=strSql+" and exists(select 1 from ljtempfeeclass where tempfeeno in (select tempfeeno from ljtempfee where otherno=b.contno or otherno=b.grpcontno) and paymode='"+strPayMode+"' )";
			  }
			  strSql=strSql+"with ur";	 
	  }
	  else//当出现为空的情况时
	  {
  		  window.alert("请输入必填信息!");
  		  return false;
	  }
	 
	  
	 //查询SQL，返回结果字符串
	 var strSqlTemp=easyQueryVer3(strSql, 1, 0, 1); 
	 turnPage.strQueryResult=strSqlTemp;
	 fm.all("arrpSql").value=strSql;//不明白为什么隐藏此控件
	 
	 if(!turnPage.strQueryResult)
	 {
	 	window.alert("没有查询记录!");
	 	return false;
	 }
	 else
	 {
	 	//var arrDataSet=decodeEasyQueryResult(turnPage.strQueryResult);
	 	turnPage.queryModal(strSql,LJAGetGrid);
	 }
	 
}



function checkImportDate(tDate,tActugetno) 
{ 
	var dateStr=tDate;
	if(dateStr==""){
		return;
	}
	var   datePat=/^((\d{2}(([02468][048])|([13579][26]))[\-]?((((0?[13578])|(1[02]))[\-]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-]?((((0?[13578])|(1[02]))[\-]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[0-9])|([1-2][0-3]))\:([0-5]?[0-9])((\s)|(\:([0-5]?[0-9])))))?$/;  
	var   matchArray=dateStr.match(datePat);   //   is   the   format   ok?   
	if   (matchArray==null)   
	{   
		window.alert("请正确输入实付号"+tActugetno+"的到帐日期格式 yyyy-mm-dd");   
		return   false;   
	}   
	if(tDate.length!=10)
	{
		window.alert("请正确输入实付号"+tActugetno+"的到帐日期格式 yyyy-mm-dd"); 
		return   false;
	} 
	month=dateStr.split('-')[1];   //   parse   date   into   variables   
	day=dateStr.split('-')[2];   
	year=dateStr.split('-')[0];   
	if(month   <   1   ||   month   >   12)   
	{   //   check   month   range   
		alert("请正确输入实付号"+tActugetno+"的到帐日期,月份必须在1到12之间");   
		return   false;   
	}   
	if(day<   1   ||   day   >   31)   
	{   
		alert("请正确输入实付号"+tActugetno+"的到帐日期,日期必须在1到31之间");
		return   false;   
	}   
	if((month=="04" || month=="06" || month=="09" || month=="11") && day=="31")
	{   
		alert("请正确输入实付号"+tActugetno+"的到帐日期,"+month+"没有31天");  
		return false;   
	}   
	if(month=="02")
	{  
		var isleap=(year%4==0&&(year%100!=0||year%400==0));  
		if(day>29 ||(day==29&&(!isleap))){   
			alert("请正确输入实付号"+tActugetno+"的到帐日期,二月 "+year+"没有"+day+" 天!"); 
			return false;   
		}   
	} 
	return  true;  
} 

