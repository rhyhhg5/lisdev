<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：按险种打印操作员日结
//程序功能：
//创建日期：2002-12-12
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%
	System.out.println("start");
  String mDay[]=new String[2];
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  //输出参数
  CError cError = new CError( );
  //后面要执行的动作：添加，修改，删除
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  boolean operFlag=true;
  String strOperation = "";//用来判断是收费还是付费
  String strOpt = "";//LYS 用来记录是暂收还是预收
  strOperation = request.getParameter("fmtransact");
  strOpt = request.getParameter("Opt");
  System.out.println("收费类型是"+strOpt);
  mDay[0]=request.getParameter("StartDay");
  mDay[1]=request.getParameter("EndDay");
  System.out.println("start"+mDay[0]);
  System.out.println("end"+mDay[1]);
  System.out.println(tG.Operator);
  System.out.println(tG.ManageCom);
  VData tVData = new VData();
  VData mResult = new VData();
  CErrors mErrors = new CErrors();
  tVData.addElement(mDay);
  tVData.addElement(strOpt); //传递收费的类型 暂收OR预收
  tVData.addElement(tG);
  XmlExport txmlExport = new XmlExport();
  if(strOpt.equals("HeBao"))
  {
    System.out.println("strOpt的标志是"+strOpt);
    FinDayCheckPremiumUI tFinDayCheckPremiumUI = new FinDayCheckPremiumUI();
    if(!tFinDayCheckPremiumUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tFinDayCheckPremiumUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "FinDayCheckUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tFinDayCheckPremiumUI.getResult();
   
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  }
  else if(strOpt.equals("BQPrem"))
  {
    System.out.println("strOpt的标志是"+strOpt);
    FinDayBQPremUI tFinDayBQPremUI = new FinDayBQPremUI();
    if(!tFinDayBQPremUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tFinDayBQPremUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "FinDayBQPremUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tFinDayBQPremUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  } 
  else if(strOpt.equals("DJPrem"))
  {
    System.out.println("strOpt的标志是"+strOpt);
    FinDayDJPremUI tFinDayDJPremUI = new FinDayDJPremUI();
    if(!tFinDayDJPremUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tFinDayDJPremUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "FinDayDJPremUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tFinDayDJPremUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  }
  else if(strOpt.equals("DJSF"))
  {
    System.out.println("strOpt的标志是"+strOpt);
    FinDayDJSFUI tFinDayDJSFUI = new FinDayDJSFUI();
    if(!tFinDayDJSFUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tFinDayDJSFUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "FinDayDJSFUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tFinDayDJSFUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  }       
  else if(strOpt.equals("YingFu"))
  {
    System.out.println("strOpt的标志是"+strOpt);
    FinDayCheckYFUI tFinDayCheckYFUI = new FinDayCheckYFUI();
    if(!tFinDayCheckYFUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tFinDayCheckYFUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "FinDayCheckUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tFinDayCheckYFUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  }
   else if(strOpt.equals("BQYingFu"))
  {
    System.out.println("strOpt的标志是"+strOpt);
    FinDayCheckBQYFUI tFinDayCheckBQYFUI = new FinDayCheckBQYFUI();
    if(!tFinDayCheckBQYFUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tFinDayCheckBQYFUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "FinDayCheckBQYFUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tFinDayCheckBQYFUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  } 
  else if (strOpt.equals("Claim"))
  {
    System.out.println("strOpt的标志是"+strOpt);
    ClaimDayBalanceUI tClaimDayBalanceUI = new ClaimDayBalanceUI();
    if(!tClaimDayBalanceUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tClaimDayBalanceUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "tClaimDayBalanceUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tClaimDayBalanceUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  }
    else if (strOpt.equals("YDClaim"))
  {
    System.out.println("strOpt的标志是"+strOpt);
    ClaimDayBalanceUI tClaimDayBalanceUI = new ClaimDayBalanceUI();
    if(!tClaimDayBalanceUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tClaimDayBalanceUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "tClaimDayBalanceUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tClaimDayBalanceUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  }  
  else if (strOpt.equals("ShiFu"))
  {
    System.out.println("strOpt的标志是"+strOpt);
    ActuPayDayCheckUI tActuPayDayCheckUI = new ActuPayDayCheckUI();
    if(!tActuPayDayCheckUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tActuPayDayCheckUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "tActuPayDayCheckUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tActuPayDayCheckUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  } 
  else if(strOpt.equals("TeXu")){
  	System.out.println("strOpt的标志是"+strOpt);
    TeXuDayCheckUI tTeXuDayCheckUI = new TeXuDayCheckUI();
    if(!tTeXuDayCheckUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tTeXuDayCheckUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "tActuPayDayCheckUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tTeXuDayCheckUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  }
  else
  {
    FinDayCheckUI tFinDayCheckUI = new FinDayCheckUI();
    if(!tFinDayCheckUI.submitData(tVData,strOperation))
    {
      operFlag= false;
      mErrors.copyAllErrors(tFinDayCheckUI.mErrors);
      cError.moduleName = "FinDayCheckSave";
      cError.functionName = "submitData";
      cError.errorMessage = "FinDayCheckUI发生错误，但是没有提供详细的出错信息";
      mErrors.addOneError(cError);
    }
    mResult = tFinDayCheckUI.getResult();
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
      System.out.println("null");
      return;
    }
  }
	ExeSQL tExeSQL = new ExeSQL();
	
	//获取临时文件名
	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	String strFilePath = tExeSQL.getOneValue(strSql);
	String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
	//获取存放临时文件的路径
	String strRealPath = application.getRealPath("/").replace('\\','/');
	String strVFPathName = strRealPath +"/"+ strVFFileName;
	
	CombineVts tcombineVts = null;
	
	if (operFlag==true)
	{
		//合并VTS文件
		String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
	
		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);
	
		//把dataStream存储到磁盘文件
		System.out.println("存储文件到"+strVFPathName);
		AccessVtsFile.saveToFile(dataStream,strVFPathName);
		System.out.println("==> Write VTS file to disk ");
	
		System.out.println("===strVFFileName : "+strVFFileName);
		//本来打算采用get方式来传递文件路径
		//缺点是文件路径被暴露
		System.out.println("---passing params by get method");
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="+strVFPathName);
	}
%>