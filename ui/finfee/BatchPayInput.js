  //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var bankflag="";
//提交，保存按钮对应操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!checkValue()) return false; //附加检验
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
 	fm.action="./BatchPaySave.jsp";
  fm.submit(); //提交
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//#2191 社保通查勘费报销问题：按原始需求，仅支持现金结算
function checkValue()
{
	var rowNum=LJAGetGrid.mulLineCount ; 
	if(rowNum==null || rowNum<1){
		alert('请先查询！');
		return false;
	}
	var ChkFlag=0;
	for(var i=0;i<rowNum;i++){
		if(LJAGetGrid.getChkNo(i)){
			ChkFlag=1;
		}
	}
	if(ChkFlag==0){
		alert('至少选择一项');
		return false;
	}
	for(var i=0;i<rowNum;i++){
		if(!LJAGetGrid.getChkNo(i)) continue;
		var tActugetno=LJAGetGrid.getRowColData(i,1);//实付号
		var tPayMode=LJAGetGrid.getRowColData(i,5);//缴费方式
		var tInsBankCode=LJAGetGrid.getRowColData(i,6);//本方银行
		var tInsBankAccno=LJAGetGrid.getRowColData(i,7);//本方银行帐号
		var tBankCode=LJAGetGrid.getRowColData(i,8);//对方银行编码
		var tBankAccno=LJAGetGrid.getRowColData(i,9);//对方银行帐号
		var tDrawer=LJAGetGrid.getRowColData(i,11);//对方银行编码
		var tChequeNo=LJAGetGrid.getRowColData(i,12);//票据号
		var tEnterAccDate=LJAGetGrid.getRowColData(i,13);//到帐日期
		var tOthernoType=LJAGetGrid.getRowColData(i,3);//号码类型
		
		if(tOthernoType=='23' && (tPayMode!='1' && tPayMode!='11')){
			alert("实付号"+tActugetno+"为社保通调查费，付费方式请选择现金或银行汇款进行给付！");
      		return false;
		}
		
		if(tPayMode==''){
			alert("实付号"+tActugetno+"的付费方式不能为空！");
      return false;
		}
		if(tEnterAccDate==''){
			alert("实付号"+tActugetno+"的到帐日期不能为空！");
			return false;
		}else{
			if(!checkImportDate(tEnterAccDate,tActugetno))
				return false;
		}
		if(tPayMode=='2' || tPayMode=='3'){
			if(tBankCode==''||tChequeNo==''){
				alert("实付号"+tActugetno+"的付费方式是支票:对方开户银行和票据号码不能为空！");
      	return false;
			}
		}
		if(tPayMode=='11'){
			if(tBankCode=='' || tBankAccno==''){
				alert("实付号"+tActugetno+"的付费方式是银行汇款:对方开户银行和帐号不能为空！");
      	return false;
			}
		}
		if(tPayMode=='4'){
			if(tBankCode==''){
				alert("实付号"+tActugetno+"的付费方式是银行转账:开户银行不能为空！");
      	return false;
			}
		}
        
        if(tPayMode=='9'){
            alert("实付号"+tActugetno+"的付费方式为其他，请重新选择付费方式！");
            return false;
        }
  
		if(tPayMode!='1' && tPayMode!='5')
  	{
    	if(tInsBankCode==''||tInsBankAccno=='') 	
      {
      	alert("实付号"+tActugetno+"的付费方式不是现金:本方银行编码和本方银行帐号不能为空！");
      	return false;
      }
      var accsql = "select 1 from ldfinbank where bankcode ='" + tInsBankCode + "' and bankaccno='" + tInsBankAccno +"'";
      var arrBankResult = easyExecSql(accsql);
      if(arrBankResult==null){
        alert("实付号"+tActugetno+"的归集账户不存在，请确认录入的归集账户");
        return false ;
      }
  	}
  
	  if(tPayMode!='4' && tPayMode!='5'){
			  if(tDrawer == null || tDrawer== "")
			  {
			  	 alert("实付号"+tActugetno+"的领取人不能为空！");
			  	 return false;
			  }
		}
		
		if(tPayMode!=''){
			var sql = "SELECT '1' FROM ldcode WHERE codetype='paymode' and code='"+tPayMode+"' with ur";
	    var rs = easyExecSql(sql);
	    if(!rs)
	    {
	      alert("付费方式不在输入的范围内");
	      return false;
	    }
  	}
    
    if(tInsBankCode!=''){
	    sql = "select '1' from LDFinBank where FinFlag='F' and BankCode='"+tInsBankCode+"' with ur";
	    rs=null;
	    rs = easyExecSql(sql);
	    if(!rs)
	    {
	      alert("本方银行不在输入的范围内");
	      return false;
	    }
  	}
    
    if(tInsBankAccno!=''){
	    sql = "select '1' from LDFinBank where bankaccno='"+tInsBankAccno+"' with ur";
	    rs=null;
	    rs = easyExecSql(sql);
	    if(!rs)
	    {
	      alert("本方银行帐号不在输入的范围内");
	      return false;
	    }
  	}
    
    if(tBankCode!=''){
	    sql = "select '1' from LDBank  where (BankUniteFlag is null or BankUniteFlag<>'1') and bankcode='"+tBankCode+"' with ur";
	    rs=null;
	    rs = easyExecSql(sql);
	    if(!rs)
	    {
	      alert("对方银行不在输入的范围内");
	      return false;
	    } 
  	}
	}
	return true;
}



function easyQueryClick() {
  var strSql = "select ActuGetNo, OtherNo, OtherNoType,SumGetMoney,PayMode,insbankcode,InsBankAccNo,BankCode,BankAccNo,AccName, "
  + " drawer,chequeno,current date,drawerid "
  + " from LJAGet where 1=1 and (finstate!='FC' or finstate is null) and ConfDate is null and EnterAccDate is null and (bankonthewayflag='0' or bankonthewayflag is null) "
 // + " and sumgetmoney<>0 "
	+ getWherePart( 'PayMode'  ,'PayMode'  )
	+ getWherePart( 'BankCode'  ,'bankcode'  )
	+getWherePart('ManageCom','ComCode',"like");
	
	if(fm.StartDate.value!=''){
			strSql +=" and makedate>='"+fm.StartDate.value+"' ";
	}
	if(fm.EndDate.value!=''){
			strSql +=" and makedate<='"+fm.EndDate.value+"' ";
	}
	
	if (fm.HCNo.value!=null&&fm.HCNo.value!="") {
		 strSql += " and exists (select 1 from llhospcase where caseno=LJAGet.otherno and confirmstate='1' and hcno='"+fm.HCNo.value+"')";
	}
	
	if(fm.paytype.value=='1'){
		strSql +=" and OtherNoType in ('5','F','C','Y') ";
	}else if(fm.paytype.value=='2'){
		strSql +=" and OtherNoType in ('3','10') ";
	}else if(fm.paytype.value=='3'){
		strSql +=" and OtherNoType in ('1','2','0') ";
	}else if(fm.paytype.value=='4'){
		strSql +=" and OtherNoType in ('4') ";
	}else if(fm.paytype.value=='5'){
		strSql +=" and OtherNoType in ('7') ";
	}else if(fm.paytype.value=='6'){
		strSql +=" and OtherNoType in ('6','8','23') ";
	}
  strSql +=" order by ActuGetNo,otherno with ur ";
  
  setDownLoadSql();
 
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    LJAGetGrid.clearData('LJAGetGrid');  
    alert("没有查询到数据！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = LJAGetGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  turnPage.pageLineNum=15;
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
}


function afterSubmit( FlagStr, content )
{
  showInfo.close(); 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  LJAGetGrid.clearData('LJAGetGrid');  
}


function checkImportDate(tDate,tActugetno) 
			{ 
				var dateStr=tDate;
				if(dateStr==""){
					return;
				}
				var   datePat=/^((\d{2}(([02468][048])|([13579][26]))[\-]?((((0?[13578])|(1[02]))[\-]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-]?((((0?[13578])|(1[02]))[\-]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s(((0?[0-9])|([1-2][0-3]))\:([0-5]?[0-9])((\s)|(\:([0-5]?[0-9])))))?$/;  
				var   matchArray=dateStr.match(datePat);   //   is   the   format   ok?   
				if   (matchArray==null)   {   
					alert("请正确输入实付号"+tActugetno+"的到帐日期格式 yyyy-mm-dd");   
					return   false;   
				}   
				if(tDate.length!=10){
					alert("请正确输入实付号"+tActugetno+"的到帐日期格式 yyyy-mm-dd"); 
					return   false;
				} 
				month=dateStr.split('-')[1];   //   parse   date   into   variables   
				day=dateStr.split('-')[2];   
				year=dateStr.split('-')[0];   
				if   (month   <   1   ||   month   >   12)   {   //   check   month   range   
					alert("请正确输入实付号"+tActugetno+"的到帐日期,月份必须在1到12之间");   
					return   false;   
				}   
				if   (day   <   1   ||   day   >   31)   {   
					alert("请正确输入实付号"+tActugetno+"的到帐日期,日期必须在1到31之间");
					return   false;   
				}   
				if((month=="04" || month=="06" || month=="09" || month=="11") && day=="31"){   
					alert("请正确输入实付号"+tActugetno+"的到帐日期,"+month+"没有31天");  
					return false;   
				}   
				if(month=="02"){  
					var isleap=(year%4==0&&(year%100!=0||year%400==0));  
					if(day>29 ||(day==29&&(!isleap))){   
						alert("请正确输入实付号"+tActugetno+"的到帐日期,二月 "+year+"没有"+day+" 天!"); 
						return false;   
					}   
				} 
				return  true;  
			} 

function setDownLoadSql(){
	var strSql = "select ActuGetNo, OtherNo, OtherNoType,SumGetMoney,PayMode,insbankcode,InsBankAccNo,BankCode,BankAccNo,AccName,drawer,chequeno "
			+ " from LJAGet where 1=1 and (finstate!='FC' or finstate is null) and ConfDate is null and EnterAccDate is null and (bankonthewayflag='0' or bankonthewayflag is null) "
			+ getWherePart( 'PayMode'  ,'PayMode'  )
			+ getWherePart( 'BankCode'  ,'bankcode'  )
			+ getWherePart('ManageCom','ComCode',"like");
	
	if(fm.StartDate.value!=''){
			strSql +=" and makedate>='"+fm.StartDate.value+"' ";
	}
	if(fm.EndDate.value!=''){
			strSql +=" and makedate<='"+fm.EndDate.value+"' ";
	}
	
	if (fm.HCNo.value!=null&&fm.HCNo.value!="") {
		 strSql += " and exists (select 1 from llhospcase where caseno=LJAGet.otherno and confirmstate='1' and hcno='"+fm.HCNo.value+"')";
	}
	
	if(fm.paytype.value=='1'){
		strSql +=" and OtherNoType in ('5','F','C','Y') ";
	}else if(fm.paytype.value=='2'){
		strSql +=" and OtherNoType in ('3','10') ";
	}else if(fm.paytype.value=='3'){
		strSql +=" and OtherNoType in ('1','2','0') ";
	}else if(fm.paytype.value=='4'){
		strSql +=" and OtherNoType in ('4') ";
	}else if(fm.paytype.value=='5'){
		strSql +=" and OtherNoType in ('7') ";
	}else if(fm.paytype.value=='6'){
		strSql +=" and OtherNoType in ('6','8') ";
	}
	
	strSql +=" order by ActuGetNo,otherno with ur ";
	
	fm.downloadSql.value = strSql;
}

function download(){

	if(fm.downloadSql.value == null || fm.downloadSql.value == ""){
		alert("请先进行查询！");
	} else {
		var oldAction = fm.action;
    	fm.action = "BatchPayDownload.jsp";
    	fm.submit();
   		fm.action = oldAction;
	}
}