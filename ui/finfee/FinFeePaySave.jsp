 <%
//程序名称：FinFeePaySave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>    
  <%@page import="com.sinosoft.lis.pubfun.*"%> 
  
<%@page contentType="text/html;charset=GBK" %>
<%
// 输出参数
	CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {   
   String ActuGetNo 		= request.getParameter("ActuGetNo");;
   String PolNo 				= request.getParameter("PolNo");
   String PayMode 			= request.getParameter("PayMode");
   String GetMoney 			= request.getParameter("GetMoney");
   String AgentCode 		= request.getParameter("AgentCode");
   String Drawer 				= request.getParameter("Drawer");
   String AgentGroup 		= request.getParameter("AgentGroup");   
   String DrawerID 			= request.getParameter("DrawerID");   
   String EnterAccDate 	= request.getParameter("EnterAccDate");   
   //String ConfDate 		= request.getParameter("ConfDate"); 
   String Operator 			= request.getParameter("Operator");   
   String ModifyDate 		= request.getParameter("ModifyDate");   
   String BankCode 			= request.getParameter("BankCode");        
   String ChequeNo 			= request.getParameter("ChequeNo");
   String BankAccNo 		= request.getParameter("BankAccNo");
   String AccName 			= request.getParameter("AccName");   
   String getbankcode 	= request.getParameter("getbankcode"); 
   String getbankaccno 	= request.getParameter("getbankaccno");     


   LJFIGetSchema tLJFIGetSchema ; //财务给付表      
   LJAGetSchema  tLJAGetSchema  ; //实付总表
   LJAGetSet     tLJAGetSet     ;

// 准备传输数据 VData
   VData tVData = new VData();
   tLJAGetSchema = new LJAGetSchema();
   tLJAGetSchema.setActuGetNo(ActuGetNo);
   //亓莹莹添加
   tLJAGetSchema.setPayMode(PayMode);
   tLJAGetSchema.setBankCode(getbankcode);
   tLJAGetSchema.setBankAccNo(getbankaccno);
   //添加结束
   tVData.clear();
   tVData.add(tLJAGetSchema);
System.out.println("Start 查询实付总表");
   LJAGetQueryUI tLJAGetQueryUI = new LJAGetQueryUI();
   if(!tLJAGetQueryUI.submitData(tVData,"QUERY"))
   {
       Content = " 查询实付总表失败，原因是: " + tLJAGetQueryUI.mErrors.getError(0).errorMessage;
       FlagStr = "Fail";
   }  
   else
   {//第一个括号
    tVData.clear();    
    tLJAGetSet = new LJAGetSet();
    tLJAGetSchema = new LJAGetSchema();     
    tVData = tLJAGetQueryUI.getResult();
    tLJAGetSet.set((LJAGetSet)tVData.getObjectByObjectName("LJAGetSet",0));  
    tLJAGetSchema =(LJAGetSchema)tLJAGetSet.get(1);
System.out.println("更新实付总表Schema");
    //tLJAGetSchema.setSumGetMoney(Double.parseDouble(GetMoney)+tLJAGetSchema.getSumGetMoney());
    tLJAGetSchema.setEnterAccDate(EnterAccDate);
    //tLJAGetSchema.setConfDate(ConfDate);
    tLJAGetSchema.setDrawer(Drawer);
    tLJAGetSchema.setDrawerID(DrawerID);
    tLJAGetSchema.setOperator(Operator); 
    tLJAGetSchema.setChequeNo(ChequeNo);
    tLJAGetSchema.setBankCode(BankCode);
    tLJAGetSchema.setBankAccNo(BankAccNo);
    tLJAGetSchema.setAccName(AccName);
    tLJAGetSchema.setInsBankCode(getbankcode);
     tLJAGetSchema.setInsBankAccNo(getbankaccno); 

    
//2-构造财务给付表的新纪录

    tLJFIGetSchema = new LJFIGetSchema();
    tLJFIGetSchema.setActuGetNo(ActuGetNo);
    tLJFIGetSchema.setPayMode(PayMode);
    tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
    tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
    tLJFIGetSchema.setGetMoney(GetMoney);
    tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
    tLJFIGetSchema.setEnterAccDate(EnterAccDate);
    //tLJFIGetSchema.setConfDate(ConfDate);
    tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
    tLJFIGetSchema.setAgentGroup(AgentGroup);
    tLJFIGetSchema.setAgentCode(AgentCode);
    tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
    tLJFIGetSchema.setDrawer(Drawer);
    tLJFIGetSchema.setDrawerID(DrawerID);
    tLJFIGetSchema.setOperator(Operator);
    tLJFIGetSchema.setBankCode(BankCode);
    tLJFIGetSchema.setChequeNo(ChequeNo);    
    tLJFIGetSchema.setBankAccNo(BankAccNo);
    tLJFIGetSchema.setAccName(AccName);
    
//入机时间等在BL层设置
//3-事务操作 插入纪录到财务给付表  更新实付总表
System.out.println("start 事务操作");
   tVData.clear();
   tVData.add(tLJFIGetSchema);
   tVData.add(tLJAGetSchema);   
   tVData.add(tGI);
   OperFinFeeGetUI tOperFinFeeGetUI = new OperFinFeeGetUI();
   if(tOperFinFeeGetUI.submitData(tVData,"VERIFY")==true){
   		BatchPayUI tBatchPayUI=new BatchPayUI();
   		LJAGetSet  mLJAGetSet = new LJAGetSet();
	 	  LJFIGetSet mLJFIGetSet = new LJFIGetSet();
	 	  mLJFIGetSet.add(tLJFIGetSchema);
			mLJAGetSet.add(tLJAGetSchema);
			tVData.add(mLJFIGetSet);
   		tVData.add(mLJAGetSet);   
   		tBatchPayUI.submitData(tVData,"CLAIM");

   }
   tError = tOperFinFeeGetUI.mErrors;   
   if (tError.needDealError())
   {                          
   Content = " 失败，原因:" + tError.getFirstError();
   FlagStr = "Fail";
   }  
   else
   {// 第二个括号 
     Content = " 操作成功";
     FlagStr = "Succ";   
%>
<script language="javascript">
	parent.fraInterface.initForm();
</script>
<%    
    }//对应第二个括号     
   }// 对应第一个括号
 }//页面有效区    
%> 
<HTML>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</HTML>    