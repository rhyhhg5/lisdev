<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  //程序名称：TempFeeSave.jsp
  //程序功能：
  //创建日期：2002-07-22
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--SCRIPT src="TempFeeInput.js"></SCRIPT-->
<!--用户校验类-->
<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.finfee.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%
  // 输出参数
  CErrors tError = new CErrors();
  String FlagStr = "";
  String Content = "";
  String ShowFinance = "N"; //显示缴纳金额信息的标记（如果财务收费完全正确，则显示金额的详细信息）
  double CashValue = 0;
  double ChequeValue = 0;
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI = (GlobalInput) session.getValue("GI"); //参见loginSubmit.jsp
  if (tGI == null) {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else { //页面有效
    String Operator = tGI.Operator; //保存登陆管理员账号
    String ManageCom = tGI.ComCode; //保存登陆区站,ManageCom=内存中登陆区站代码
    LJTempFeeSchema tLJTempFeeSchema;
    // 暂收表信息记录集
    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    String tTempNum[] = request.getParameterValues("TempToGridNo");
    String tTempFeeNo[] = request.getParameterValues("TempToGrid1");
    String tTempFeeType[] = request.getParameterValues("TempToGrid2");
    String tPayDate[] = request.getParameterValues("TempToGrid3");
    String tPayMoney[] = request.getParameterValues("TempToGrid4");
    String tEnterAccDate[] = request.getParameterValues("TempToGrid5");
    String tManageCom[] = request.getParameterValues("TempToGrid6");
    String tRiskCode[] = request.getParameterValues("TempToGrid7");
    String tAgentGroup[] = request.getParameterValues("TempToGrid8");
    String tAgentCode[] = request.getParameterValues("TempToGrid9");
    String tOtherNo[] = request.getParameterValues("TempToGrid10");
    String tOtherNoType[] = request.getParameterValues("TempToGrid11");
    
    int TempCount = tTempNum.length;
    for (int i = 0; i < TempCount; i++) {
      tLJTempFeeSchema = new LJTempFeeSchema();
      tLJTempFeeSchema.setTempFeeNo(tTempFeeNo[i].trim());
      tLJTempFeeSchema.setTempFeeType(tTempFeeType[i]);
      //如果是预打保单交费，交费类型改成‘a’
      if (tTempFeeType[i].equals("8")) {
        tLJTempFeeSchema.setTempFeeType("a");
      }
      //如果是健管交费，判断是个单还是团单
      if (tTempFeeType[i].equals("20")) {
         if((tTempFeeNo[i].substring(0,2)).equals("12")){
           tLJTempFeeSchema.setTempFeeType("21");
         }        
      }
        //如果是个单收费，险种为定值
      if (tTempFeeType[i].equals("11")) {  
           tLJTempFeeSchema.setRiskCode("000000");
      }else if(tTempFeeType[i].equals("30")){
      tLJTempFeeSchema.setRiskCode("0000");
      }else{
      tLJTempFeeSchema.setRiskCode(tRiskCode[i]);
      }
      tLJTempFeeSchema.setAgentGroup(tAgentGroup[i]);
      tLJTempFeeSchema.setAgentCode(tAgentCode[i]);
      tLJTempFeeSchema.setPayDate(tPayDate[i]);
      tLJTempFeeSchema.setEnterAccDate(tEnterAccDate[i]); //到帐日期
      tLJTempFeeSchema.setPayMoney(tPayMoney[i]);
      tLJTempFeeSchema.setManageCom(tManageCom[i]);
      tLJTempFeeSchema.setOtherNo(tOtherNo[i]);
      tLJTempFeeSchema.setOtherNoType(tOtherNoType[i]);
      tLJTempFeeSchema.setOperator(Operator);
      tLJTempFeeSchema.setCashier(Operator);
      tLJTempFeeSet.add(tLJTempFeeSchema);
    }
    // 暂收分类表记录集
    LJTempFeeClassSchema tLJTempFeeClassSchema;
    LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    String tTempClassNum[] = request.getParameterValues("TempClassToGridNo");
    String tCTempFeeNo[] = request.getParameterValues("TempClassToGrid1");
    String tCPayMode[] = request.getParameterValues("TempClassToGrid2");
    String tCPayDate[] = request.getParameterValues("TempClassToGrid3");
    String tCPayMoney[] = request.getParameterValues("TempClassToGrid4");
    String tCEnterAccDate[] = request.getParameterValues("TempClassToGrid5");
    String tCManageCom[] = request.getParameterValues("TempClassToGrid6");
    String tCChequeNo[] = request.getParameterValues("TempClassToGrid7");
    String tCChequeDate[] = request.getParameterValues("TempClassToGrid8");
    String tBankcode[] = request.getParameterValues("TempClassToGrid9");
    String tBankaccno[] = request.getParameterValues("TempClassToGrid10");
    String tAccname[] = request.getParameterValues("TempClassToGrid11");
    String tInsBankcode[] = request.getParameterValues("TempClassToGrid12");
    String tInsBankAccNo[] = request.getParameterValues("TempClassToGrid13");
    TempCount = tTempClassNum.length;
    for (int i = 0; i < TempCount; i++) {
      tLJTempFeeClassSchema = new LJTempFeeClassSchema();
      tLJTempFeeClassSchema.setTempFeeNo(tCTempFeeNo[i].trim());
      tLJTempFeeClassSchema.setPayMode(tCPayMode[i]);
      tLJTempFeeClassSchema.setPayDate(tCPayDate[i]);
      tLJTempFeeClassSchema.setPayMoney(tCPayMoney[i]);
      tLJTempFeeClassSchema.setChequeNo(tCChequeNo[i]);
      tLJTempFeeClassSchema.setChequeDate(tCChequeDate[i]);
      if ((tCPayMode[i].equals("11") || tCPayMode[i].equals("12"))&& !tCPayDate[i].equals("")){
      	tLJTempFeeClassSchema.setEnterAccDate(tCPayDate[i]);
      }else{
      	tLJTempFeeClassSchema.setEnterAccDate(tCEnterAccDate[i]);
      }
      tLJTempFeeClassSchema.setManageCom(tCManageCom[i]);
      tLJTempFeeClassSchema.setBankCode(tBankcode[i]);
      tLJTempFeeClassSchema.setBankAccNo(tBankaccno[i]);
      tLJTempFeeClassSchema.setAccName(tAccname[i]);
      tLJTempFeeClassSchema.setInsBankCode(tInsBankcode[i]);
      tLJTempFeeClassSchema.setInsBankAccNo(tInsBankAccNo[i]);
      tLJTempFeeClassSchema.setOperator(Operator);
      tLJTempFeeClassSchema.setCashier(Operator);
      tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
      if (tCPayMode[i].equals("2") || tCPayMode[i].equals("3")){ //支票
        ChequeValue = ChequeValue + Double.parseDouble(tCPayMoney[i]);
      }else{ //现金
      	CashValue = CashValue + Double.parseDouble(tCPayMoney[i]);
      }
      System.out.println("Cash:" + CashValue);
      System.out.println("cheque:" + ChequeValue);
    }
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.addElement(tLJTempFeeSet);
    tVData.addElement(tLJTempFeeClassSet);
    tVData.addElement(tGI);
    // 数据传输
    TempFeeUI tTempFeeUI = new TempFeeUI();
    tTempFeeUI.submitData(tVData, "INSERT");
    //调用保全确认
    if ((tTempFeeType != null) && (tTempFeeType[0] != null) && (tTempFeeType[0].equals("4"))) {
        for (int i = 0; i < TempCount; i++) {
	      BqFinishTask tBqFinishTask = new BqFinishTask(ManageCom,tOtherNo[i]);
	      tBqFinishTask.run();
        }
    }
    //调用理赔确认
    if ((tTempFeeType != null) && (tTempFeeType[0] != null) && ((tTempFeeType[0].equals("9") || tTempFeeType[0].equals("Y") ) )) {
      LLClaimFinishTask tLLClaimFinishTask = new LLClaimFinishTask(ManageCom);
      tLLClaimFinishTask.run();
    }
    /*
      //调用保全确认
      for (int i=0; i < tTempNum.length; i++)
      {
     if (tTempFeeType[i] == null)
     {
      continue;
     }
     if ((tTempFeeType[i].equals("4")) || (tTempFeeType[i].equals("7")))
     {
      String edorAcceptNo = tOtherNo[i];
      //判断是个险还是团险
      LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
      tLPGrpEdorMainDB.setEdorAcceptNo(edorAcceptNo);
      LPGrpEdorMainSet tLPGrpEdorMainSet = tLPGrpEdorMainDB.query();
      //个单
      if (tLPGrpEdorMainSet.size() == 0)
      {
       LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
       tLPEdorAppSchema.setEdorAcceptNo(edorAcceptNo);
       String strTemplatePath = application.getRealPath("xerox/printdata") + "/";
       VData bqVData = new VData();
       bqVData.addElement(strTemplatePath);
       bqVData.addElement(tLPEdorAppSchema);
       bqVData.addElement(tGI);
       PEdorAcceptConfirmUI tPEdorAcceptConfirmUI   = new PEdorAcceptConfirmUI();
       if (!tPEdorAcceptConfirmUI.submitData(bqVData, "INSERT||EDORACPTCONFIRM"))
       {
        System.out.println("保全确认失败！");
        continue;
       }
       tError = tPEdorAcceptConfirmUI.mErrors;
      }
      else
      {
         LPGrpEdorMainSchema tLPGrpEdorMainSchema = tLPGrpEdorMainSet.get(1);
            //保存个人保单信息(保全)
           String strTemplatePath = application.getRealPath("xerox/printdata") + "/";
           VData bqVData = new VData();
           bqVData.addElement(strTemplatePath);
           bqVData.addElement(tLPGrpEdorMainSchema);
           bqVData.addElement(tGI);
           PGrpEdorConfirmUI tPGrpEdorConfirmUI = new PGrpEdorConfirmUI();
           if (!tPGrpEdorConfirmUI.submitData(bqVData, "INSERT||GRPEDORCONFIRM"))
           {
        System.out.println("保全确认失败！");
        continue;
           }
           tError = tPGrpEdorConfirmUI.mErrors;
      }
      if (tError.getErrType().equals(CError.TYPE_NONEERR))
      {
       //工单结案
       LGWorkSchema tLGWorkSchema = new LGWorkSchema();
       tLGWorkSchema.setDetailWorkNo(edorAcceptNo);
       tLGWorkSchema.setTypeNo("03");
       VData tTaskVData = new VData();
       tTaskVData.add(tGI);
       tTaskVData.add(tLGWorkSchema);
       TaskAutoFinishBL tTaskAutoFinishBL = new TaskAutoFinishBL();
       if (!tTaskAutoFinishBL.submitData(tTaskVData, ""))
       {
        System.out.println("工单结案失败！");
       }
      }
     }
      }
     */
    //如果在Catch中发现异常，则不从错误类中提取错误信息
    try {
      tError = tTempFeeUI.mErrors;
      String[] errTempFeeNo = tTempFeeUI.getResult(); //
      if (!tError.needDealError()) {
        Content = " 操作成功! ";
        FlagStr = "Succ";
        if (errTempFeeNo != null) {
          for (int n = 0; n < errTempFeeNo.length; n++) {
            if (errTempFeeNo[n] == null) {
              if (n == 0) { //如果没有纪录直接退出
                ShowFinance = "Y"; //显示缴纳金额信息的标记为真
                break;
              }
              Content = Content + " )  原因可能是：1-该纪录您已经录入；2-暂交费号对应的险种编码选择有误";
              Content = Content + ". 请您确认或查询后重新操作这些纪录!";
              break; //退出
            }
            if (n == 0) {
              Content = "操作完成,但是以下暂交费纪录已经存在,不能保存 ( ";
            }
            Content = Content + (n + 1) + ": " + errTempFeeNo[n] + " | ";
            if (n == errTempFeeNo.length - 1) { //如果是正常结束
              Content = Content + " ) 请您确认或查询后重新操作这些纪录! ";
            }
          } //end for
        } //end if
        else {
          ShowFinance = "Y"; //显示缴纳金额信息的标记为真
        }
        //调用签单begin
        System.out.println("缴费方式:" + tCPayMode.length + "是否为现金:" + tCPayMode[0]);
        if (tCPayMode[0].equals("1") || tCPayMode[0].equals("10") ||
            tCPayMode[0].equals("11") || tCPayMode[0].equals("12")|| tCPayMode[0].equals("13")|| tCPayMode[0].equals("16")|| tCPayMode[0].equals("17")) {
          String cExeSQL = "select * from lccont where prtno='"
              + tOtherNo[0] + "' and conttype='1' ";
          LCContDB tLCContDB = new LCContDB();
          LCContSchema tLCContSchema = new LCContSchema();
          LCContSet tLCContSet = new LCContSet();
          tLCContSet = tLCContDB.executeQuery(cExeSQL);
          if (tLCContSet.size() == 1) {
            tLCContSchema = tLCContSet.get(1).getSchema();
            String tExeSQL =
                "select  missionprop1, missionprop2, missionid, submissionid,activityid " +
                "from lwmission where 1=1 and processid = '0000000003' and " +
                "activityid = '0000001150' and missionprop2 in " +
                "(select otherno  from ljtempfee where othernotype = '4' " +
                "and confflag='0' and enteraccdate is not null) and MissionProp2='" + tLCContSchema.getPrtNo() + "'" +
                " union " +
                "select  missionprop1, missionprop2, missionid, submissionid,activityid " +
                "from lwmission where 1=1 and processid = '0000000007' and " +
                "activityid = '0000007002' and missionprop2 in " +
                "(select otherno  from ljtempfee where othernotype = '4' " +
                "and confflag='0' and enteraccdate is not null) and MissionProp2='" + tLCContSchema.getPrtNo() + "'";
            LWMissionDB tLWMissionDB = new LWMissionDB();
            LWMissionSchema tLWMissionSchema = new LWMissionSchema();
            LWMissionSet tLWMissionSet = new LWMissionSet();
            tLWMissionSet = tLWMissionDB.executeQuery(tExeSQL);
            if (tLWMissionSet.size() == 1) {
              tLWMissionSchema = tLWMissionSet.get(1).getSchema();
              System.out.println("begin to sign");
              TransferData tTransferData = new TransferData();
              LCContSchema cLCContSchema = new LCContSchema();
              LCContSet cLCContSet = new LCContSet();
              cLCContSchema.setContNo(tLCContSchema.getProposalContNo());
              cLCContSchema.setPrtNo(tLCContSchema.getPrtNo());
              cLCContSet.add(cLCContSchema);
              tTransferData.setNameAndValue("MissionID", tLWMissionSchema.getMissionID());
              tTransferData.setNameAndValue("SubMissionID", tLWMissionSchema.getSubMissionID());
              VData cVData = new VData();
              cVData.add(tGI);
              cVData.add(cLCContSet);
              cVData.add(tTransferData);
             /* if (tLCContSchema.getCardFlag() == null || tLCContSchema.getCardFlag().equals("0")) {
                TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
                if (tTbWorkFlowUI.submitData(cVData, "0000001150") == false) {
                  Content = Content + "但是投保单签单失败";
                }
                else {
                  Content = Content + "且签单成功!";
                }
              }
              if (tLCContSchema.getCardFlag() != null && (tLCContSchema.getCardFlag().equals("2")||tLCContSchema.getCardFlag().equals("6")) ) {
              	cVData.add(tLCContSchema);
                BriefTbWorkFlowUI tTbWorkFlowUI = new BriefTbWorkFlowUI();
                boolean bl = tTbWorkFlowUI.submitData(cVData, "0000007002");
                if (bl == false) {
                  Content = Content + "但是投保单签单失败";
                }
                else {
                  Content = Content + "且签单成功!";
                }
              }*/
            }
          }
          //续期应收数据变更为待核销  2011-11-4 需要使用续期自动核销 该段代码移至TempFeeBL中
          /*boolean flag = true;
          for (int i = 1; i <= tLJTempFeeSet.size(); i++) {
            if (!tLJTempFeeSet.get(i).getTempFeeType().equals("2")) {
              continue;
            }
            LJSPaySchema tLJSPaySchema = new LJSPaySchema();
            tLJSPaySchema.setGetNoticeNo(tLJTempFeeSet.get(i).getTempFeeNo());
            VData ttVdata = new VData();
            ttVdata.add(tLJSPaySchema);
            ttVdata.add(tGI);
            ChangeFinFeeStateBL bl = new ChangeFinFeeStateBL("4");
            if (!bl.submitData(ttVdata, "")) {
              flag = false;
              Content += bl.mErrors.getFirstError() + "||";
              System.out.println(bl.mErrors.getErrContent());
            }
          }
          if (!flag) {
            FlagStr = "Fail";
            Content = "收费成功，但是改变更应收状态为待核销失败:" + Content;
          }*/
        }
        //调用签单end
      }
      else {
        Content = " 失败，原因:" + tError.getFirstError();
        FlagStr = "Fail";
      }
    }
    catch (Exception ex) {
      Content = " 失败，原因:" + ex.toString();
      FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
       if("<%=FlagStr%>"=="Succ")//操作成功
       {
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
       }
       else//操作失败
       {
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
       }
         window.focus;
</script>
</html>
