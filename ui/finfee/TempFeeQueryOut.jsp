<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<title>暂交费信息查询</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>   
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <script src="./TempFeeQuery.js"></script> 
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="TempFeeQueryInit.jsp"%>


</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm action=./TempFeeQueryResult.jsp target=fraSubmit method=post>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
          暂交费状态:
          </TD>
          <TD  class= input>
            <Input class= codeNo name=TempFeeStatus verify="号码类型|NOTNULL" CodeData="0|^0|未到帐^1|未核销^2|已核销^3|已退费^4|新保未核销^5|续保未核销^6|全部" ondblClick="showCodeListEx('TempFeeStatus',[this,TempFeeStatusName],[0,1,2,3,4,5,6]);" onkeyup="showCodeListKeyEx('TempFeeStatus',[this,TempFeeStatusName],[0,1,2,3,4,5,6]);" ><input class=codename name=TempFeeStatusName readonly=true >
          </TD>
         
         
         
          <TD  class= title>
            险种
          </TD>
          <TD  class= input>
            <Input class= codeNo name=RiskCode ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('RiskCode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true >
          </TD>         
       </TR>                     
      	<TR  class= common>
          <TD  class= title>
          开始日期:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>       	
          <TD  class= title>
          结束日期:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD>
       </TR>
       <TR  class= common>
          <TD  class= title>
          操作员:
          </TD>
          <TD  class= input>
            <Input class=common name=Operator >
          </TD>       	
         <TD  class= title>
          业务员:
          </TD>
          <TD  class= input>
            <Input class=common name=AgentCode >
          </TD>       	
       </TR>
       
       <TR  class= common>
          <TD  class= title>
          暂收据号:
          </TD>
          <TD  class= input>
            <Input class=common name=TempFeeNo >
          </TD>  
          <TD class = title>
          投保单印刷号:
          </TD>
          <TD class=input>
            <Input class=common name=PrtNo>
          </TD>
        </TR>
      </table>
      <div id=divChequeNo style='display:none'>
      <table class=common align=center>
        <TR class=common>
          <TD class=title>
            支票号码:
          </TD>
          <TD class=input>
            <Input class=common name=ChequeNo>
          </TD>
          <TD class=title>
          </TD>
          <TD class=input>
          </TD>
          
        </TR>
      </table>
      </div>
      <table align=right>
        <TR >
          
          <TD >
            <INPUT VALUE="查询" Class=cssButton TYPE=button onclick="submitForm();"> 
          </TD> 
           	
       </TR>         
   </Table> 
   <BR><BR>
     <!--INPUT VALUE="返回" Class=cssButton TYPE=button onclick="returnParent();"-->   
    
   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTempFee1);">
    		</TD>
    		<TD class= titleImg>
    			 暂交费信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divTempFee1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanTempQueryGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    <table align=right>
      <tr>
        <td>
          <INPUT CLASS=cssButton VALUE="打印" TYPE=button onclick="easyQueryPrint(2,'TempQueryGrid','turnPage');">
        </td>
      </tr>
    </table>
 <input type=hidden id="fmAction" name="fmAction">
 </Div>					
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
</html>

