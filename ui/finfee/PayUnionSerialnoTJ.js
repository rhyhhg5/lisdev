var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();
var bankflag = "";
var mArray = new Array();

function easyQueryClick() {
	var tManageCom = fm.ManageCom.value;
	var strSql = "select lja.ActuGetNo,"
			+ "lja.OtherNo, "
			+ "'手续费',"
			+ "lja.sumGetMoney,"
			+ "lja.PayMode,"
			+ "lja.AccName,"
			+ "lja.drawer,"
			+ "lja.confdate,"
			//+ "(select name from lacom where agentcom =lja.agentcom)"
			+ "lja.agentcom,"
			+ "lja.ManageCom,"
			+ "lja.makedate,"
			+ "lac.riskcode"
			+ " from ljfiget ljf,lacharge lac,ljaget lja"
			+ " where lja.actugetno=ljf.actugetno and lja.othernotype in ('BC','AC') and lac.CommisionSN=lja.getnoticeno and lja.confdate is not null"
			+ " and not exists (select 1 from LCPaySerialnoHZ where TranSerial=lja.actugetno)";
	//页面的开始时间和结束时间是对应手续费结算时间来说的，即生成ljaget的时间
	if (fm.StartDate.value != '' && fm.StartDate.value != null) {
		strSql += " and lja.makedate>='" + fm.StartDate.value + "' ";
	}
	if (fm.EndDate.value != '' && fm.EndDate.value != null) {
		strSql += " and lja.makedate<='" + fm.EndDate.value + "' ";
	}

	if (fm.PayMode.value != '' && fm.PayMode.value != null) {
		strSql += " and lja.PayMode='" + fm.PayMode.value + "' ";
	}
	if (fm.AgentCom.value != '' && fm.AgentCom.value != null) {
		strSql += " and lja.AgentCom='" + fm.AgentCom.value + "' ";
	}
//登录机构模糊查询付费信息
	strSql += " and  lja.ManageCom like '" + tManageCom
			+ "%' order by ActuGetNo,otherno with ur ";

	// 查询SQL，返回结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSql, 1, 1, 1);

	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		// 清空MULTILINE，使用方法见MULTILINE使用说明
		LJFIGetGrid.clearData('LJFIGetGrid');
		alert("没有查询到数据！");
		return false;
	}

	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	// 设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = LJFIGetGrid;

	// 保存SQL语句
	turnPage.strQuerySql = strSql;

	// 设置查询起始位置
	turnPage.pageIndex = 0;

	turnPage.pageLineNum = 50;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,
			turnPage.pageIndex, turnPage.pageLineNum);
	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	// 控制是否显示翻页按钮
	if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
		try {
			window.divPage.style.display = "";
		} catch (ex) {
		}
	} else {
		try {
			window.divPage.style.display = "none";
		} catch (ex) {
		}
	}
}

// 提交，保存按钮对应操作
function submitForm() {
	// 首先检验录入框
	var Serialno = fm.HZNo.value;
	if (!verifyInput())
		return false;
	if (!checkValue())
		return false; //附加检验
	var i = 0;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./PayUnionSerialnoTJSave.jsp?Serialno=" + Serialno;
	fm.submit(); // 提交
}

function checkValue() {
	var rowNum = LJFIGetGrid.mulLineCount;
	if (rowNum == null || rowNum < 1) {
		alert('请先查询！');
		return false;
	}
	var ChkFlag = 0;
	for (var i = 0; i < rowNum; i++) {
		if (LJFIGetGrid.getChkNo(i)) {
			ChkFlag = 1;
		}
	}
	if (ChkFlag == 0) {
		alert('至少选择一项');
		return false;
	}
	return true;
}

function afterSubmit(FlagStr,content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	easyQueryClick();
}
