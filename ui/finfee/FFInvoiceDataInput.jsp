<html> 
<%
//程序名称：TempFeeBatchInput.jsp
//程序功能：财务收费查询
//创建日期：2012-2-14 
//创建人  ：zcx
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="FFInvoiceDataInput.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FFInvoiceDataInit.jsp"%>
</head>
<body  onload="initForm();" >
<form action="" method=post 
    name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
<table class = common>
    <TR  class= common>
      <TD class = title >
        选择导入的文件：
      </TD>
      <TD class = common width=30%>
        <Input  type="file" name=FileName size=30>
      </TD>
    </TR>
    
    <TR class = common >
      <td class = common colspan=2>
        <INPUT class=cssButton VALUE="导入" TYPE=button onclick = "submitForm();" > 
               
      </td>
    </TR>
   </table>
    <input type=hidden name=ImportFile>
     <Div  id= "divQuery" style= "display: ''">
    <table>
    <tr>
      <td class= titleImg>
         导入数据列表
      </td></tr>
    </table>
     <table  class= common>
        <tr>
            <td text-align: left colSpan=1>
             <span id="spanQueryGrid" ></span>
        </td>
    </tr>
     </table>
        <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">                  
        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
</div>

  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
 

 