
 <%
//程序名称：BatchPayQuereyInit.jsp
//程序功能：
//创建日期：2009-06-15 
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<SCRIPT src="Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
	
%>                            

<script language="JavaScript">
                                     

function initForm()
{
  try
  {  
  	fm.all('BankaccNo').value = '';
    fm.all('BankaccName').value = '';
  	fm.all('PrtNo').value = '';
    fm.all('PayMoney').value = '';
    
    initFeeGrid();  
  }
  catch(re)
  {
    alert("menuGrpInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  } 
}

 

function initFeeGrid()
{

    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              	
      
      iArray[1]=new Array();
      iArray[1][0]="印刷号";    	                //列名
      iArray[1][1]="130px";            		        //列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      iArray[2]=new Array();
      iArray[2][0]="金额";    	                //列名
      iArray[2][1]="150px";            		        //列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      iArray[3]=new Array();
      iArray[3][0]="银行编码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[3][1]="100px";         			//列宽
      iArray[3][2]=10;          			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="银行帐号";    	                //列名
      iArray[4][1]="180px";            		        //列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      iArray[5]=new Array();
      iArray[5][0]="银行帐户名";    	                //列名
      iArray[5][1]="180px";            		        //列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
        	
      iArray[6]=new Array();
      iArray[6][0]="机构编码";    	                //列名
      iArray[6][1]="100px";            		        //列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

 
      iArray[7]=new Array();
      iArray[7][0]="业务员";    	                //列名
      iArray[7][1]="230px";            		        //列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
 
      FeeGrid = new MulLineEnter( "fm" , "FeeGrid" ); 
         
      //这些属性必须在loadMulLine前
      FeeGrid.mulLineCount = 0;     
      FeeGrid.displayTitle = 1;
      FeeGrid.canChk =1;
      //FeeGrid.canSel =1;
      FeeGrid.locked =0;            //是否锁定：1为锁定 0为不锁定
      FeeGrid.hiddenPlus=1;        //是否隐藏"+"添加一行标志：1为隐藏；0为不隐藏       
      FeeGrid.hiddenSubtraction=0; //是否隐藏"-"添加一行标志：1为隐藏；0为不隐藏
     // FeeGrid.recordNo=0;         //设置序号起始基数为10，如果要分页显示数据有用
        
      FeeGrid.loadMulLine(iArray);     
  
      }
      catch(ex)
      { 
        alert(ex);
      }
  }     
</script>
 