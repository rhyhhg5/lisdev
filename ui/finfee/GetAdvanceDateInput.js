var turnPage = new turnPageClass();
// 查询按钮
function easyQueryClick()
{
    if (!verifyInput()){
        return false;
    }
	// 书写SQL语句
	var strSQL = "";
    var sSql1 = " and confmakedate>='" + fm.all("StartDate").value + "' ";
    var sSql2 = " and confdate>='" + fm.all("StartDate").value + "' ";
    var eSql1 = " and confmakedate<='" + fm.all("EndDate").value + "' ";
    var eSql2 = " and confdate<='" + fm.all("EndDate").value + "' ";
    
    if (fm.all("StartDate").value == null || fm.all("StartDate").value == "") {
         sSql1 = "";
         sSql2 = "";
    }
    
    if (fm.all("EndDate").value == null || fm.all("EndDate").value == "") {
         eSql1 = "";
         eSql2 = "";
    }
    strSQL1 = "select otherno,'收费',confmakedate,paymoney,(select case when sum(paymoney) is null then 0 else sum(paymoney) end from ljtempfee where otherno=a.otherno and confdate is null),appntname,(select getUniteCode(agentCode) from dual) from ljtempfee a where tempfeetype='YS' and paymoney>0 "
              + getWherePart( 'AppntName','AppntName','like') 
              + getWherePart( 'ManageCom','ManageCom','like') 
              + getWherePart( 'AgentCode','AgentCode') 
              + sSql1 + eSql1;
    strSQL2 = "select otherno,'退费',confdate,sum(paymoney),0,appntname,(select getUniteCode(agentCode) from dual) from ljtempfee where tempfeetype='YS' and confdate is not null "
              + getWherePart( 'AppntName','AppntName','like') 
              + getWherePart( 'ManageCom','ManageCom','like') 
              + getWherePart( 'AgentCode','AgentCode') 
              + sSql2 + eSql2
              + " group by otherno,confdate,appntname,agentcode";
    strSQL3 = "select otherno,'转实收保费',confmakedate,-paymoney,(select case when sum(paymoney) is null then 0 else sum(paymoney) end from ljtempfee where otherno=a.otherno and confdate is null),appntname,(select getUniteCode(agentCode) from dual) from ljtempfee a where tempfeetype='YS' and paymoney<0"
             + getWherePart( 'AppntName','AppntName','like') 
             + getWherePart( 'ManageCom','ManageCom','like') 
             + getWherePart( 'AgentCode','AgentCode') 
             + sSql1 + eSql1;
             
    if(fm.Type.value == 0) {
        strSQL = "select * from (" + strSQL1 + ") a order by appntname ";
    } else if (fm.Type.value == 1) {
        strSQL = "select * from (" + strSQL2 + ") a order by appntname ";
    } else if (fm.Type.value == 2) {
        strSQL = "select * from (" + strSQL3 + ") a order by appntname ";
    } else {
        strSQL = "select * from (" + strSQL1 + " union all "  + strSQL2 + " union all " + strSQL3 + ") a order by appntname ";
    }
     //查询SQL，返回结果字符串
     var strSqlTemp=easyQueryVer3(strSQL, 1, 0, 1); 
     turnPage.strQueryResult=strSqlTemp;
     //fm.all("arrSql").value=strSql;
     
     if(!turnPage.strQueryResult)
     {
        window.alert("没有查询记录!");
        LJAGetGrid.clearData();
        return false;
     }
     else
     {
        fm.sql.value = strSQL;
        turnPage.queryModal(strSQL,LJAGetGrid);
     }
  
  return true;
}

function download(){
    if(LJAGetGrid.mulLineCount == 0 || LJAGetGrid.mulLineCount == null)

  {
    alert("没有需要下载的数据，请先查询！");
    return false;
  }
  submitForm();
}

function queryAgent()
{
    if(fm.all('ManageCom').value==""){
         alert("请先录入管理机构信息！");
         return;
    }
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function afterQuery2(arrReturn)
{
   fm.all('AgentCode').value = arrReturn[0][0];
   var rsSQL = "select getUniteCode('"+arrReturn[0][0]+"') from dual";
   var rs = easyExecSql(rsSQL);
   fm.GroupAgentCode.value = rs;
}

function submitForm() {
  //检验录入框
    if (!verifyInput()){
        return false;
    }
    fm.submit(); //提交
}