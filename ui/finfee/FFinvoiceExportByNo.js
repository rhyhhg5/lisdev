var showInfo;

function Export()
{
    if (!verifyInput2()) 
    {
        return false;
    }
    if(fm.SFState.value == null || fm.SFState.value == "") {
       alert("收付费发票标记为空");
       return false;
     }
    document.getElementById("filesList").innerHTML = "<font color='red'>正在生成数据文件......</font>";
    fm.submit();
    return true;
}

function afterSubmit(FlagStr, content, transact)
{
    if(showInfo != null && showInfo != "undefined")
        showInfo.close();
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    else
    {
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //showDiv(operateButton,"true"); 
        //showDiv(inputButton,"false"); 
        //执行下一步操作
        var tfilesList = document.getElementById("filesList");
        tfilesList.innerHTML = "";
        if(transact != null && transact != "undefined")
        {
            var filename = transact.split("|");
            for(index in filename)
            {
                if(filename[index] != "")
                {
                    var tSkip = filename[index].lastIndexOf("/")==-1?0:filename[index].lastIndexOf("/")+1;
                    tfilesList.innerHTML +=  "<br /> 下载 [<a href='../" + filename[index] + "' >" + filename[index].substring(tSkip) + "</a>]";
                }
            }
        }
    }
}