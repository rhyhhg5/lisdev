<%
//程序名称：download.jsp
//程序功能：公用下载组件
//创建日期：2008-1-15 18:13
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<%@page import="java.io.*"%>
<%@page import="java.net.URLEncoder"%>
<%!
private boolean downLoadFile(HttpServletResponse aResponse,String aPath,String aFileName)
{ 
			boolean flag =true;
		  File file = new File(aPath+File.separator+aFileName);

      aResponse.reset();
      aResponse.setContentType("application/octet-stream"); 
      try{
      	aResponse.setHeader("Content-Disposition","attachment; filename="+URLEncoder.encode(aFileName,"UTF-8"));
      }catch(Exception e)
      {
      	flag = false;
      }
      aResponse.setContentLength((int) file.length());
      
      byte[] buffer = new byte[4096];
      BufferedOutputStream output = null;
      BufferedInputStream input = null;    
      //写缓冲区
      try 
      {
        output = new BufferedOutputStream(aResponse.getOutputStream());
        input = new BufferedInputStream(new FileInputStream(file));
        
        int len = 0;
        while((len = input.read(buffer)) >0)
        {
          output.write(buffer,0,len);
        }
        input.close();
        output.close();
      }
      catch (Exception e) 
      {
      	flag = false;
        e.printStackTrace();
      }
      finally 
      {
      	try{
        if (input != null) input.close();
        }catch(Exception e)
        {
        	 e.printStackTrace();
        }
        try{
        if (output != null) output.close();
        }catch(Exception e)
        {
        	 e.printStackTrace();
        }
      }
      return flag;
}
%>