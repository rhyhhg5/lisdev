//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  //if(!check())	return ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    easyQueryClick();
  }
} 
//原来的财务退费存在问题，首先PolNo不是保单号，退费的确应该针对险种退
//但是需要支持整单退费，因此将muiline改成复选退费，因此此处校验多余
//function check()
//{
//  if(fm.all('PolNo').value==null||fm.all('PolNo').value=='')	
//   {
//   	alert("保单号码不能为空");
//    return false;
//   }
//      
//   var tSql = "select ProPosalNo from LCPol where PolNo='"+fm.all('PolNo').value+"'";          
//   var tResult = easyExecSql(tSql, 1, 1, 1);
//   if(!tResult)
//   {
//     alert("没有查到对应的保单！");	
//     return false;
//   }   
//  fm.all('ProPosalNo').value=tResult;     
//  return true;
//}

// 查询按钮
function easyQueryClick() {
	// 书写SQL语句			     
	var strSql = "select a.ContNo, a.RiskCode, b.SumActuPayMoney, a.prem, a.amnt, a.SignDate, c.OutPayFlag, c.BankCode, a.PayLocation ,a.polno"
	           + " from lcpol a,ljapayperson b,lccont c where a.contno=b.contno and a.polno=b.polno and b.contno=c.contno and c.dif>0 and b.PayType='YET'"
          	 + getWherePart("RiskCode", "RiskCode")
          	 + getWherePart("PrtNo", "PrtNo")
          	 + getWherePart("PolNo", "PolNo2")
          	 + getWherePart("AppntName", "AppntName")
          	 + getWherePart("BankCode", "BankCode")
          	 + getWherePart("LeavingMoney", "LeavingMoney");
  
  if (fm.AppntName.value != "") strSql = strSql + " and appntno in (select c.customerno from ldperson c where name='" + fm.AppntName.value + "')"; 
  strSql = strSql + " Order by SignDate";
				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
	fm.PolNo.value = fm.PolNo2.value;
}

function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
	  fm.PolNo.value = turnPage.arrDataCacheSet[index][0];
  }
}
