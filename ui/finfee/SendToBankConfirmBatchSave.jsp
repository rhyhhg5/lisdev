<%@page contentType="text/html;charset=GBK" %>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>  
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String workType = request.getParameter("workType");
  String tfailinfo = "";
  String tsuccinfo = "";
  GlobalInput tGI = new GlobalInput();
  TransferData tTransferData = new TransferData();
  LCContSchema tLCContSchema = new LCContSchema();
  SendToBankConfirmUI tSendToBankConfirmUI  = new SendToBankConfirmUI(); 
  VData tVData = new VData(); 
  tGI=(GlobalInput)session.getValue("GI");
  if(tGI == null) {
		out.println("session has expired");
		return;
  }
  
  if("UNLOCK".equals(workType)){
     tfailinfo="解锁失败！";
     tsuccinfo="解锁成功！";
  }else{
     tfailinfo="锁定失败！";
     tsuccinfo="锁定成功！";
  }
  
  String tPrtNo[] = request.getParameterValues("PolGrid2");
  String tGetNoticeNo[] = request.getParameterValues("PolGrid1");  
  String tChecks[] = request.getParameterValues("InpPolGridChk");     
  int nIndex = 0;
//循环
for(nIndex = 0; nIndex < tChecks.length; nIndex++ )
{
	//If this line isn't selected, continue，如果没有选中当前行，则继续
	if( !tChecks[nIndex].equals("1") )
	{
		continue;
	}
	//将数据放入合同保单集合
	tLCContSchema = new LCContSchema();
	tLCContSchema.setPrtNo(tPrtNo[nIndex]);
	tTransferData = new TransferData();
	tTransferData.setNameAndValue( "GetNoticeNo",tGetNoticeNo[nIndex]);
    tTransferData.setNameAndValue("SaveUnSuccreason","");
    tVData.clear();
	tVData.add(tTransferData);
	tVData.add(tLCContSchema);			
	tVData.add( tGI );
	
	try
	{
		if (!tSendToBankConfirmUI.submitData(tVData,workType)){
			Content += "投保单："+tPrtNo[nIndex]+tfailinfo+"，原因是:" + tSendToBankConfirmUI.mErrors.getFirstError()+"<BR>";
			FlagStr = "Fail";
			tSendToBankConfirmUI.mErrors.clearErrors();
		}
	}
	catch(Exception ex)
	{
		Content += "投保单："+tPrtNo[nIndex]+tfailinfo+"，原因是:" + ex.toString() +"<BR>";
		FlagStr = "Fail";
		break;
	}
	
	
}

if (!FlagStr.equals("Fail"))
{
	Content = tsuccinfo;
	FlagStr = "Succ";
}
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>