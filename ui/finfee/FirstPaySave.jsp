<%
//程序名称：BatchInputListSave.jsp
//程序功能：
//创建日期：2009-06-19 09:25:18
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>    
  <%@page import="com.sinosoft.lis.pubfun.*"%> 
  
<%@page contentType="text/html;charset=GBK" %>
<%

   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  VData mVData = new VData();
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {   
  	String tChk[]=request.getParameterValues("InpFeeGridChk");
  	String tTempFeeNo[] = request.getParameterValues("FeeGrid1");
  	String tPayMoney[] = request.getParameterValues("FeeGrid2");
  	String tPayBankCode[] = request.getParameterValues("FeeGrid3");
  	String tPayBankAccNo[] = request.getParameterValues("FeeGrid4");
  	String tGetBankAccName[] = request.getParameterValues("FeeGrid5");
  	String tComCode[] = request.getParameterValues("FeeGrid6");
  	String tAgentCode[] = request.getParameterValues("FeeGrid7");


   	LJTempFeeSchema tLJTempFeeSchema ; //财务给付表      
   	LJTempFeeClassSchema  tLJTempFeeClassSchema  ; //实付总表
   	LJTempFeeSet  mLJTempFeeSet = new LJTempFeeSet();
	 	LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
	 
	 
	 	
	 	for(int i=0;i<tChk.length;i++){
			if(tChk[i]!=null && tChk[i].equals("1")){
				tLJTempFeeSchema=new LJTempFeeSchema();
    			tLJTempFeeSchema.setTempFeeType("16");
    			tLJTempFeeSchema.setOtherNo(tTempFeeNo[i]);
    			tLJTempFeeSchema.setOtherNoType("4");
    			tLJTempFeeSchema.setOperator(tGI.Operator);
    			tLJTempFeeSchema.setPayMoney(tPayMoney[i]);
    			tLJTempFeeSchema.setManageCom(tComCode[i]);
    			tLJTempFeeSchema.setAgentCode(tAgentCode[i]);
    	
			    
			    tLJTempFeeClassSchema=new LJTempFeeClassSchema();
			   
			    tLJTempFeeClassSchema.setOperator(tGI.Operator);
			    tLJTempFeeClassSchema.setPayMoney(tPayMoney[i]);
			    tLJTempFeeClassSchema.setManageCom(tComCode[i]);
			   
			    tLJTempFeeClassSchema.setBankCode(tPayBankCode[i]);
			    tLJTempFeeClassSchema.setBankAccNo(tPayBankAccNo[i]);
			    tLJTempFeeClassSchema.setAccName(tGetBankAccName[i]);
			  
			    
			    mLJTempFeeClassSet.add(tLJTempFeeClassSchema);
			    mLJTempFeeSet.add(tLJTempFeeSchema);
			    mVData.add(mLJTempFeeClassSet);
	 			mVData.add(mLJTempFeeSet);
   			}
			}
	 	}

	 	
	 	
	 	mVData.add(tGI);
		
		FirstPayUI tFirstPayUI=new FirstPayUI();
		if(tFirstPayUI.submitData(mVData,"VERIFY")==true){
			//tBatchPayUI.submitData(mVData,"CLAIM");
			tError = tFirstPayUI.mErrors;
		}else{
			tError = tFirstPayUI.mErrors;
		}
		   
   	if (tError.needDealError()){
   		Content = " 失败，原因:" + tError.getFirstError();
   		FlagStr = "Fail";
  	}else{
  		Content = " 操作成功";
     	FlagStr = "Succ"; 
  	}

%> 
<HTML>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</HTML>    