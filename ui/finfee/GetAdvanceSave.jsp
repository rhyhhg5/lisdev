 <%
//程序名称：PayAdvanceSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>    
  <%@page import="com.sinosoft.lis.pubfun.*"%> 
  
<%@page contentType="text/html;charset=GBK" %>
<%
// 输出参数
    CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {   
   String AgentCode    = request.getParameter("AgentCode");
   String PayMoney     = request.getParameter("PayMoney");
   String PayMode      = request.getParameter("PayMode");
   String AppntName    = request.getParameter("AppntName"); 
   String EnterAccDate = request.getParameter("EnterAccDate");     
   String AccBank      = request.getParameter("AccBank");        
   String ChequeNo     = request.getParameter("ChequeNo");
   String BankAccNo    = request.getParameter("BankAccNo");
   String AccName      = request.getParameter("AccName");   
   String getbankcode  = request.getParameter("getbankcode"); 
   String getbankaccno = request.getParameter("getbankaccno");     
   
   LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
   LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();

   LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
   tLJTempFeeSchema.setPayMoney(PayMoney);
   tLJTempFeeSchema.setAPPntName(AppntName);
   tLJTempFeeSchema.setPayDate(EnterAccDate);
   if(!PayMode.trim().equals("3")){
     tLJTempFeeSchema.setEnterAccDate(PubFun.getCurrentDate());
     tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
   }
   tLJTempFeeSchema.setAgentCode(AgentCode);
   tLJTempFeeSet.add(tLJTempFeeSchema);
      
   LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
   tLJTempFeeClassSchema.setPayMoney(PayMoney);
   tLJTempFeeClassSchema.setPayMode(PayMode);
   tLJTempFeeClassSchema.setAppntName(AppntName);
   tLJTempFeeClassSchema.setPayDate(EnterAccDate);
   if(!PayMode.trim().equals("3")){
     tLJTempFeeClassSchema.setEnterAccDate(PubFun.getCurrentDate());
     tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
   }
   tLJTempFeeClassSchema.setBankCode(AccBank);
   tLJTempFeeClassSchema.setChequeNo(ChequeNo);
   tLJTempFeeClassSchema.setBankAccNo(BankAccNo);
   tLJTempFeeClassSchema.setAccName(AccName);
   tLJTempFeeClassSchema.setInsBankCode(getbankcode);
   tLJTempFeeClassSchema.setInsBankAccNo(getbankaccno);
   tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
   
// 准备传输数据 VData
   VData tVData = new VData();
   tVData.clear();
   tVData.add(tLJTempFeeSet);
   tVData.add(tLJTempFeeClassSet); 
   tVData.add(tGI);
   GetAdvanceUI tGetAdvanceUI = new GetAdvanceUI();
   if(!tGetAdvanceUI.submitData(tVData,"INSERT")){
       Content = " 失败，原因:" + tGetAdvanceUI.mErrors.getFirstError();
       FlagStr = "Fail";
   }
   else {
     Content = " 操作成功";
     FlagStr = "Succ";   
%>
<script language="javascript">
    parent.fraInterface.initForm();
</script>
<%    
    }     
   } 
%> 
<HTML>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</HTML>    