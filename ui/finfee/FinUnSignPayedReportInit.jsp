 <%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initForm()
{
  try
  {
 		initContInfoGrid();
 		var arrResult = easyExecSql("select current date from dual");
		if(arrResult!=null){
			fm.all('EndDate').value=arrResult[0][0];
		}
		arrResult = easyExecSql("select current date- 3 MONTHS from dual");
		if(arrResult!=null){
			fm.all('StartDate').value=arrResult[0][0];
		}
		fm.all('ManageCom').value=<%=strManageCom%>;
  }
  catch(re)
  {
    alert("InitForm函数中发生异常:初始化界面错误!");
  }
}

// 暂收费信息列表的初始化
function initContInfoGrid()
  {                               
    var iArray = new Array();      
    try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="管理机构";      	   		//列名
      iArray[1][1]="80px";            			//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="印刷号";      	   		//列名
      iArray[2][1]="80px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="财务收费日期";      	   		//列名
      iArray[3][1]="100px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="投保人号";      	   		//列名
      iArray[4][1]="100px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="投保人姓名";      	   		//列名
      iArray[5][1]="100px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="业务员代码";      	   		//列名
      iArray[6][1]="100px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="暂收号";      	   		//列名
      iArray[7][1]="100px";            			//列宽
      iArray[7][2]=20;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许


      ContInfoGrid = new MulLineEnter( "fm" , "ContInfoGrid" ); 
      //这些属性必须在loadMulLine前
      ContInfoGrid.mulLineCount = 0;   
      ContInfoGrid.displayTitle = 1;
      ContInfoGrid.locked=1;     
      ContInfoGrid.canChk = 1;
      ContInfoGrid.loadMulLine(iArray);  
    }
    catch(ex)
    {
      alert(ex);
    }
}
</script>
