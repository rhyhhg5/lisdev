<%@ page contentType="text/html;charset=GBK"%>
<%@ include file="../common/jsp/UsrCheck.jsp"%>

<%
	//程序名称：
	//程序功能：
	//创建日期：2017-04-06
	//创建人  ：JiangLi
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";
	var strManageCom = "<%=tGI.ComCode%>
	";
</script>
<head>
<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
<script src="../common/javascript/Common.js"></script>
<script src="../common/cvar/CCodeOperate.js"></script>
<script src="../common/javascript/MulLine.js"></script>
<script src="../common/Calendar/Calendar.js"></script>
<script src="../common/javascript/VerifyInput.js"></script>

<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

<script src="PayUnionSerialnoApply.js"></script>
<%@include file="PayUnionSerialnoApplyInit.jsp"%>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
	<p style="color:red">申请批次号时: 汇总批次号系统自动生成不需要手动录入</p>
		<table>
			<tr>
				<td class="titleImg">请输入查询条件：</td>
			</tr>
		</table>
		<div id="divCondCode" style="display: ''">
			<table class="common">
				<tr class="common">
					<td class="title8">汇总批次号</td>
					<td class="input8"><input class="common" name="SerialnoHZ" /></td>
					<td class="title8">汇总批次名</td>
					<td class="input8"><input class="common" name="SerialnoHZName" /></td>
				</tr>
				<tr class="common">
					<td class="title8">管理机构</td>
					<td class="input8"><input class="codeNo" name="ManageCom"
						verify="管理机构|code:comcode&notnull"
						ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
						onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
						readonly="readonly" /><input class="codename"
						name="ManageComName" readonly="readonly" elementtype="nacessary" />
					</td>
					<td class="title8">申请日期</td>
					<td class="input8"><input class="coolDatePicker"
						dateFormat="short" name="ApplyDate" verify="申请日期|date" /></td>
				</tr>
			</table>
		</div>

		<table>
			<tr>
				<td class="common"><input class="cssButton" type="button"
					value="查  询" onclick="querySerialnoHzList();" /></td>
				<td class="common"><input class="cssButton" type="button"
					id="btnApplySerialnoHzSettlement"
					name="btnApplySerialnoHzSettlement" value="申  请"
					onclick="applySerialnoHZ();" /></td>
			</tr>

		</table>

		<br />

		<table>
			<tr>
				<td class="common"><img src="../common/images/butExpand.gif"
					style="cursor: hand;"
					onclick="showPage(this, divPaySerialnoHZListGrid);" /></td>
				<td class="titleImg">汇总批次信息</td>
			</tr>
		</table>
		<div id="divPaySerialnoHZListGrid" style="display: ''">
			<table class="common">
				<tr class="common">
					<td><span id="spanPaySerialnoHZListGrid"></span></td>
				</tr>
			</table>

			<div id="divPaySerialnoHZListGridPage" style="display: ''"
				align="center">
				<input type="button" class="cssButton" value="首  页"
					onclick="turnPage1.firstPage();" /> <input type="button"
					class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />
				<input type="button" class="cssButton" value="下一页"
					onclick="turnPage1.nextPage();" /> <input type="button"
					class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />
			</div>
		</div>

		<table>
			<tr>
				<td class="common"><input class="cssButton" type="button"
					id="btnAdd" value="添加该批次的付费手续费" onclick="addPay();" /></td>
				<td class="common"><input class="cssButton" type="button"
					id="btnUpdate" value="修改该批次的付费手续费" onclick="updatePay();" /></td>
			</tr>
		</table>
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
