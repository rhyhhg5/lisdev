<html> 
<%
//程序名称：BatchPayQueryInput.jsp
//程序功能：财务收费查询
//创建日期：2009-06-15 
//创建人  ：yanjing
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
	<SCRIPT src="BatchPayQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BatchPayQueryInit.jsp"%>
</head>
<body  onload="initForm();" >
<Form action="./BatchPayQueryList.jsp" method=post name=fm target="fraSubmit">
		<Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</TD>
    		<TD class= titleImg>
    			 查询条件
    		</TD>
    	</TR>
    <table  class= common align=center>
      	 <TR  class= common>
          <TD  class= title>
            交费类型
          </TD>          
          <TD  class= input>
            <Input class=codeNo name=TempFeeType  ondblclick="return showCodeList('TempFeeType2',[this,TempFeeTypeName],[0,1]);" onkeyup="return showCodeListKey('TempFeeType2',[this,TempFeeTypeName],[0,1]);"><input class=codename name=TempFeeTypeName readonly=true >
          </TD>        
          <td class=title></td>
          <td class=input></td>
        </TR> 
        <TR  class= common> 
          <TD  class= title>
            起始日期
          </TD>          
          <TD  class= input>
          <Input class="coolDatePicker"  name=InputDataNo1 >
          </TD>        
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" name=InputDataNo2 >
          </TD>          
      </TR>
    </Table>  
      <INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();">  
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFinFee1);">
    		</TD>
    		<TD class= titleImg>
    			待收费信息
    		</TD>
    	</TR>
    </Table>
   <input type=hidden name="type">
  <input type=hidden name=ComCode>  	
 <Div  id= "divFinFee1" align=center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
           <span id="spanCheckTempGrid" ></span> 
  	    </TD>
      </TR>
    </Table>					
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
 </Div>					
  <Div id='Savebutton' style='display:' > 
      <input type=hidden name="arrpSql">  
      <INPUT VALUE="清单下载" Class=cssButton TYPE=button onclick="submitForm();">
 	</Div>
</Form>    
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
 