<html>
<%
	//程序名称：BatchPoundagePayInput.jsp
	//程序功能：手续费批量付费
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="PayUnionSerialnoUP.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="PayUnionSerialnoUPInit.jsp"%>
<%
	String mSerialnoHZ = request.getParameter("SerialnoHZ");
System.out.println(mSerialnoHZ);
%>
</head>
<body onload="initForm();">
	<Form action="" method=post name=fm target="fraSubmit">
		<Table>
			<TR>
				<TD class=common>
					<IMG src="../common/images/butExpand.gif" style="cursor: hand;" OnClick="showPage(this,queryCondition);">
				</TD>
				<TD class=titleImg>查询条件</TD>
			</TR>
		</Table>

		<INPUT type=hidden name=sql>

		<Table>
			<TR class=common>
				<TD class=title>汇总批次号</TD>
				<TD class=input>
					<Input class=common name="HCNo"  style="border-color: #C0C0C0;" value="<%=mSerialnoHZ%>">
				</TD>
			</TR>
			<TR class=common>
				<TD class=input>
					<INPUT VALUE="查  询" Class=cssButton TYPE=button onclick="easyQueryClick();">
				</TD>
			</TR>
		</Table>

		<Table>
			<TD class=common>
				<IMG src="../common/images/butExpand.gif" style="cursor: hand;" OnClick="showPage(this,divFinFee);">
			</TD>
			<TD class=titleImg>汇总批次号已经关联的付费信息</TD>
		</Table>
		<input type=hidden name=ComCode>
		<input type=hidden name=Operate>
		<Div id="divFinFee" align=center style="display: ''">
			<Table class=common>
				<TR class=common>
					<TD text-align: left colSpan=1>
						<span id="spanLCPaySerialnoHZGrid"></span>
					</TD>
				</TR>
			</Table>
			<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
			<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
			<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
			<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
		<Div id='Savebutton' style='display:'>
			<INPUT VALUE="删   除" Class=cssButton TYPE=button onclick="submitForm();">
		</Div>
	</Form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>