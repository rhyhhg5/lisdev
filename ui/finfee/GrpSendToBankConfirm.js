//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass(); 
var arrDataSet;

function changePaymode()
{
    if (PolGrid.getSelNo())
    {
        fm.all("workType").value = "PAYMODE";
        fm.all("gnNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
        fm.all("pNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
        var accname=PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3);
        if (fm.all("PayMode2").value=="")
        {
            alert("请选择缴费方式");
            return false;
        }
        if(fm.all("PayMode").value == fm.all("PayMode2").value)
        {
            alert("缴费方式没有变更，请选择帐户信息修改！");
            return false;
        }
        if ((fm.all("PayMode2").value=="4"|| fm.all("PayMode2").value=="6"||fm.all("PayMode2").value=="8") && (fm.all("BankCode").value=="" || fm.all("AccName").value=="" || fm.all("AccNo").value==""))
        {
            alert("请录入银行代码、账户、账号");
            return false;
        }
        if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 7)=="银行在途")
        {
            alert("银行在途不能修改缴费方式！");
            return false;
        }
        
      //如果银行非锁定状态不允许修改
  	  var sql = "select cansendbank from ljspay where otherno ='"+fm.all("OtherNo").value+"'";
  	  var sql2 = "select paymode from lcgrpcont where prtno ='"+fm.all("OtherNo").value+"'";
  	  var cansendbank = easyExecSql(sql);
  	  var paymode = easyExecSql(sql2);
  	  if (cansendbank != 1 && paymode == 4) {
  	  	alert("该保单非银行锁定状态，不能进行修改！");
        	return false;
        }
        
        if(fm.all("AccName").value!=""&&fm.all("AccName").value!=accname){
            alert("账户名需与投保人名称相同！");
            return false;
        }

        //如果收费方式非银行转账则其他项为空
        if (fm.all("PayMode2").value!="4") {
        	fm.all("BankCode").value = "";
        	fm.all("BankCodeName").value = "";
        	fm.all("AccName").value = "";
        	fm.all("AccNo").value = "";
		}
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        fm.action="./GrpSendToBankConfirmSave.jsp";
        fm.submit();
    }else{
       alert('请选择需要修改的保单.');
    }
}

function changeAcc()
{
  if (PolGrid.getSelNo()) 
 {  
  fm.all("gnNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  fm.all("pNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
  var accname=PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3);  
  if (fm.all("BankCode").value=="" || fm.all("AccName").value=="" || fm.all("AccNo").value=="")
  {
  	alert("请录入银行代码、账户、账号");
  	return false;
  }
  if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 7)=="银行在途")
  {
  	alert("银行在途不能修改缴费信息！");
  	return false;
  }  
  if (fm.all("PayMode2").value=="1" || PolGrid.getRowColData(PolGrid.getSelNo() - 1, 6)=="现金")
  {
  	alert("现金方式缴费无需修改账户信息");
  	return false;
  }
  if(fm.all("AccName").value!=""&&fm.all("AccName").value!=accname){
    alert("账户名需与投保人名称相同！");
    return false;
  }  
  
  //如果银行非锁定状态不允许修改
  var sql = "select cansendbank from ljspay where otherno ='"+fm.all("OtherNo").value+"'";
  var sql2 = "select paymode from lcgrpcont where prtno ='"+fm.all("OtherNo").value+"'";
  var cansendbank = easyExecSql(sql);
  var paymode = easyExecSql(sql2);
  if (cansendbank != 1 && paymode == 4) {
  	alert("该保单非银行锁定状态，不能进行修改！");
	return false;
  }
	  
	fm.all("workType").value = "ACC";  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./GrpSendToBankConfirmSave.jsp";
	fm.submit();
 }else{
    alert('请选择需要修改的保单.');
 }
}

function lock()
{
  if (PolGrid.getSelNo()) 
 {  
  fm.all("gnNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  fm.all("pNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
    
  if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 9)=="无法锁定")
  {
  	alert("此数据无法锁定");
  	return false;
  }
    if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 9)=="锁定")
  {
  	alert("此数据已经锁定");
  	return false;
  }
	fm.all("workType").value = "LOCK";  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./GrpSendToBankConfirmSave.jsp";
	fm.submit();
 }else{
    alert('请选择需要锁定的保单.');
 }
}

function unlock()
{
  if (PolGrid.getSelNo()) 
 {  
  fm.all("gnNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  fm.all("pNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
  if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 9)=="无法锁定")
  {
  	alert("此数据无法解除锁定");
  	return false;
  }
    if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 9)=="未锁定")
  {
  	alert("此数据未锁定");
  	return false;
  }
	fm.all("workType").value = "UNLOCK";  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./GrpSendToBankConfirmSave.jsp";
	fm.submit();
 }else{
    alert('请选择需要解除锁定的保单.');
 }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {   
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");     
  }
  else
  { 
  	
	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");   
 	initForm();
  }
  fm.PayMode2.value="";
  fm.PayModeName2.value="";  
  fm.BankCode.value="";
  fm.BankCodeName.value="";  
  fm.AccName.value="";
  fm.AccNo.value="";
}

function addInfo(addFlag,error)
{
	if (addFlag=="Fail")
	{
		alert("向打印队列添加数据失败!原因是："+error);
	}else
	{
		alert("成功添加进打印队列!");
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function resetFm()
{
	fm.reset();
	fm.PayMode.value="4";
	fm.PayModeName.value="银行转账";
}

// 查询按钮
function easyQueryClick()
{
    if(fm.OtherNo.value == "" && !verifyInput())
    {
        return false;
    }
	if(dateDiff(fm.StartCValiDate.value, fm.EndCValiDate.value, "M") > 3)
	{
		alert("统计期最多为三个月！");
		return false;
	}
	
    // 初始化表格
    initPolGrid();

    // 书写SQL语句
    var strSql = "";

//    if (fm.all("PayMode").value=='1')
//    {
//        strSql = "select '',prtno,appntname,cvalidate,(select sum(prem) from lcgrppol a where LCGrpCont.prtno=a.prtno and a.uwflag not in('a','8','1')),"
//            + "'现金','未交费','','无法锁定',getUniteCode(agentcode),managecom,0,'','', SaleChnl, AgentCom ,'' "
//            + "from LCGrpCont "
//            + "where conttype='2' and appflag<>'1' and approveflag='9' and uwflag not in('a','8','1') and paymode='1' "
//            //+ "and not exists (select 1 from LJTempFee where PrtNo = OtherNo and EnterAccDate is not null) "
//            + "and ManageCom like '" + fm.ManageCom.value + "%' "
//            + getWherePart( 'prtno','OtherNo' )
//            + getWherePart( 'AppntName' )
//            + getWherePart('CValiDate', 'StartCValiDate', '>=')
//            + getWherePart('CValiDate', 'EndCValiDate', '<=')
//            + getWherePart('getUniteCode(agentcode)', 'AgentCode' )
//            + getWherePart("SaleChnl","SaleChnlCode")
//            + getWherePart("AgentCom","AgentComBank")
//            + " and CardFlag not in ('9','a') "
//            + "order by CValiDate with ur";
//    }
//    else
//    {
        strSql = " select * from "
            + " ( "
            + " select case when A is not null then A else '' end,B,C,D,E,F,case when L is not null then L when N is not null then N when O is not null then O when R is not null then R "
            + " else '未转账' end  V,'',case when M is not null then M when Q is not null then Q else '无法锁定' end,I,J,case when P is not null then P else '' end,case when S is not null then S else '' end , "
            + " AA,FD "
            + " from "
            + " ( "
            + " select Z.prtno B,Z.grpName C,Z.cvalidate D,(select sum(prem) from lcgrppol a where a.prtno=Z.prtno and a.uwflag not in('a','8','1')) E,'银行转账' F, "
            + " Z.agentcode I,Z.managecom J, "
            + " (select '未转账' from ljspay where getnoticeno = Y.getnoticeno and BankOnTheWayFlag='0' and SendBankCount=0) L, "
            + " (select case cansendbank when '0' then '未锁定' when '1' then '锁定' when '2' then '未锁定' end from ljspay where getnoticeno = Y.getnoticeno and BankOnTheWayFlag='0' and SendBankCount=0) M, "
            + " (select '银行在途' from ljspay where getnoticeno = Y.getnoticeno and BankOnTheWayFlag='1') N, "
            + " (select '转账失败' from ljspay where getnoticeno = Y.getnoticeno and BankOnTheWayFlag='0' and SendBankCount>=1 and BankSuccFlag in('0','2')) O, "
            //+" (select  d.codename from lyreturnfrombankb c, ldcode1 d,ljspay a where c.paycode=a.getnoticeno and d.codetype='bankerror' and d.code=c.bankcode and d.code1=c.banksuccflag and c.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.getnoticeno and a.OtherNo=Z.PrtNo)) P, "
            + " (select  d.codename from lyreturnfrombankb c, ldcode1 d,ljspay a where c.paycode=a.getnoticeno and d.codetype='bankerror' and d.code=c.bankcode and d.code1=c.banksuccflag and a.getnoticeno = Y.getnoticeno order by c.serialno desc fetch first 1 rows only) P, "
            + " (select case cansendbank when '0' then '未锁定' when '2' then '未锁定' else '锁定' end from ljspay where getnoticeno = Y.getnoticeno and BankOnTheWayFlag='0' and SendBankCount>=1 and BankSuccFlag in('0','2')) Q, "
            + " (select getnoticeno from ljspay where getnoticeno = Y.getnoticeno )  A, "
            + " (select distinct '转账成功' from ljtempfee where OtherNo=Z.PrtNo and enteraccdate is not null and othernotype='4') R, "
            + " (select  c.bankunsuccreason from lyreturnfrombankb c,ljspay a where c.paycode=a.getnoticeno and c.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.getnoticeno and a.OtherNo=Z.PrtNo)and a.getnoticeno = Y.getnoticeno) S, "
            + " Z.SaleChnl AA,  "
            + " (select startpaydate from ljspay where getnoticeno = Y.getnoticeno) FD "
            + " from lcgrpcont Z inner join ljspay Y on Z.prtno = Y.otherno and  Y.OtherNoType in ('9', '16') where Z.appflag<>'1' and Z.approveflag='9' "
            + " and  Z.ManageCom like '" + fm.ManageCom.value + "%' "
            + getWherePart('Z.PayMode', 'PayMode')
            + getWherePart('Z.PrtNo', 'OtherNo')
            + getWherePart('Z.AppntNo', 'AppntName')
            + getWherePart('Z.CValiDate', 'StartCValiDate', '>=')
            + getWherePart('Z.CValiDate', 'EndCValiDate', '<=')
//            + getWherePart('getUniteCode(Z.agentcode)','AgentCode' )
            + getWherePart('Z.agentcode','AgentCode' )
            + getWherePart("Z.SaleChnl","SaleChnlCode")
//            + getWherePart("Z.AgentCom","AgentComBank")
            + " and Z.UWFlag not in('a','8','1') "
            + " and Z.CardFlag is null "
            + " )as Y "
            + " ) as X where X.V<>'转账成功' ";
//        if (fm.HastenPrtNo.value=="1")
//        {
//            strSql = strSql + "and K>0";
//        }
//        else if(fm.HastenPrtNo.value=="2")
//        {
//            strSql = strSql + "and K=0";
//        }
        strSql =  strSql + " order by D,B with ur " ;
//    }
    
    turnPage1.pageLineNum = 10;
    //查询SQL，返回结果字符串
    turnPage1.queryModal(strSql,PolGrid,0,0,1);
    fm.querySql.value = strSql;
}

function showBankInfo(parm1, parm2) {
	var strSql = "select paymode,CodeName('paymode', PayMode),BankCode,"
	         + "case when paymode = '4' then (select BankName from LDBank where lcgrpcont.BankCode=ldbank.BankCode)" 
	         + " when paymode = '8' then (select medicalcomName from ldmedicalcom where lcgrpcont.BankCode=ldmedicalcom.medicalcomCode) end" 
	         +		",accname,bankaccno from lcgrpcont where prtno = '" 
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2)
             + "'";
             //+ "' and paymode<>'1' union select paymode,'现金','','','','' from lccont where prtno='"
             //+ PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2)
             //+ "' and paymode='1' ";
    var arrResult = easyExecSql(strSql);

    fm.all("PayMode2").value = arrResult[0][0];
    fm.all("PayModeName2").value = arrResult[0][1];
    fm.all("BankCode").value = arrResult[0][2];
    fm.all("BankCodeName").value = arrResult[0][3];
    fm.all("AccName").value = arrResult[0][4];
    fm.all("AccNo").value = arrResult[0][5];

    if(fm.PayMode2.value=="1")
    {
        //fm.all('BankCode').style.display="none";
        //fm.all('BankCodeName').style.display="none";
        //fm.all('AccName').style.display="none";
        //fm.all('AccNo').style.display="none";
        fm.all('BankCode').style.display="";
        fm.all('BankCodeName').style.display="";
        fm.all('AccName').style.display="";
        fm.all('AccNo').style.display="";
    }
    else
    {
        fm.all('BankCode').style.display="";
        fm.all('BankCodeName').style.display="";
        fm.all('AccName').style.display="";
        fm.all('AccNo').style.display="";
        //判断是否需要录入转帐失败原因
        var tstrSql="select 1 from ljspay a,lyreturnfrombankb b where b.paycode=a.getnoticeno  and OtherNoType='9' and BankOnTheWayFlag='0' and SendBankCount>=1 and a.BankSuccFlag in('0','2') and a.otherno='"+PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2)+"'";
        var tarrResult = easyExecSql(tstrSql);
        if(tarrResult){
            divLCPol3.style.display="";
            var tstrSql1="select bankunsuccreason from ljspay a,lyreturnfrombankb b where b.paycode=a.getnoticeno  and OtherNoType='9' and BankOnTheWayFlag='0' and SendBankCount>=1 and a.BankSuccFlag in('0','2') and a.otherno='"
                    +PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2)+"' and b.serialno=(select max(e.serialno) from lyreturnfrombankb e where e.paycode=a.getnoticeno)";
            var tarrResult1 = easyExecSql(tstrSql1);
        	if(tarrResult1){
        		fm.Content.value=tarrResult1[0][0];
        	}
        }else{
            divLCPol3.style.display="none";
            return false;
        }
    }
}

function firstHasten()
{		
	if (PolGrid.getSelNo())
	{
		if (fm.PayMode.value=="1")
		{
			alert('现金缴费方式的保单不能添加进"首期保费催缴通知书"打印队列!');
			return false;
		}
		fm.all('PrtNo'		).value 	= PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
		fm.all('CustAgent').value		= PolGrid.getRowColData(PolGrid.getSelNo() - 1, 10);
		fm.all('CustCom'	).value  	= PolGrid.getRowColData(PolGrid.getSelNo() - 1, 11);
		
		var sta = PolGrid.getRowColData(PolGrid.getSelNo()-1,7);
		if (sta!='转账失败')
		{
			alert('"'+sta+'"'+"状态的保单不能被添加到催缴打印队列!");
			return false;
		}
		
		var strSql = "select * from loprtmanager where code='75' and otherno = '"+fm.all('PrtNo').value+"' and StateFlag='0'";
		var result = easyExecSql(strSql);	
		if (result!=null)
		{
			alert("该保单的首期保费催缴通知书打印已添加进打印队列并且未首次打印,不能重复添加!");
			return false;
		}
/*	*/	
		strSql="select PrtSeq from loprtmanager where code = '07' "
			+ " and otherno in ( select proposalcontno from lcgrpcont a where  a.prtno='"+fm.all('PrtNo').value+"')"
			;
		 var prtSeq = easyExecSql(strSql);	
		 if (prtSeq)
		{
			var pSeq = prtSeq[0][0];
		}
		
		fm.action="./FirstHastenSave.jsp?PSeq="+pSeq;
		fm.submit();
	}
	else
	{
		alert('请查询后选择转账失败状态的保单.');
	}
} 


function afterCodeSelect( cCodeName, Field )
{
	if(cCodeName=="PayModeInd")
    {
        //2012-5-3 张成轩 去点修改为现金收费时银行信息的录入限制
        /*
	    if(fm.PayMode2.value=="1") 
	    {
	 	    fm.all('BankCode').value="";
	 	    fm.all('BankCode').style.display="none";
            fm.all('BankCodeName').value="";
            fm.all('BankCodeName').style.display="none";
            fm.all('AccName').value= "";
            fm.all('AccName').style.display="none";
            fm.all('AccNo').value= "";
            fm.all('AccNo').style.display="none";
	    }      
	    else// if(fm.PayMode2.value=="4") 
	    {*/
	 	 	fm.all('BankCode').style.display="";
            fm.all('BankCodeName').style.display="";
            fm.all('AccName').style.display="";
            fm.all('AccNo').style.display="";
	    //}
    }
//    if (cCodeName == "lcsalechnl")
//    {
//        var tSaleChnl = fm.SaleChnlCode.value;
//        displayAgentComBank(tSaleChnl == "04");
//    }
}

function SaveUnSuccreason()
{
  if (PolGrid.getSelNo()) 
 {  
  fm.all("gnNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
  fm.all("pNo").value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
  
  if(fm.all("gnNo").value==""||fm.all("pNo").value==""){
  alert("请先选中一条记录!");
  	return false;  
  }
  
    
  if (fm.all("Content").value=="")
  {
  	alert("没有录入转帐失败原因,不能保存!");
  	return false;
  }
	fm.all("workType").value = "SaveUnSuccreason";  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./GrpSendToBankConfirmSave.jsp";
	fm.submit();
 }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
//function displayAgentComBank(isDisplay)
//{
//    if(isDisplay == true)
//    {
//        fm.AgentComBank.style.display = "";
//    }
//    else
//    {
//        fm.AgentComBank.value = "";
//        fm.AgentComBank.style.display = "none";        
//    }
//}

/**
 * 查询银行网点
 */
//function queryAgentComBank()
//{
//    showInfo=window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
//    //alert(showInfo.fm);
//}

/**
 * 查询结构返现
 */
//function afterQuery(arrQueryResult)
//{
//    if(arrQueryResult)
//    {
//        fm.AgentComBank.value = arrQueryResult[0][0];
//    }
//}

function downloadClick()
{
    if(PolGrid.mulLineCount==0)
    {
        alert("列表中没有数据可下载");
    }
    var oldAction = fm.action;
    fm.action = "GrpSendToBankDownLoad.jsp";
    fm.submit();
    fm.action = oldAction;
}

function batchlock()
{
    var rowNum=PolGrid.mulLineCount ; 
	if(rowNum==null || rowNum<1){
		alert('请先查询！');
		return false;
	}
	var ChkFlag=0;
	for(var i=0;i<rowNum;i++){
		if(PolGrid.getChkNo(i)){
			ChkFlag=1;
		}
	}
	if(ChkFlag==0){
		alert('至少选择一项');
		return false;
	}
	var failinfo1="";
	var failinfo2="";
	var lockflag ="";
    for(var i=0;i<rowNum;i++){
		if(!PolGrid.getChkNo(i)) continue;
		lockflag = PolGrid.getRowColData(i,9);
		if(lockflag=="无法锁定"){
		   if(failinfo1==""){
		       failinfo1 = PolGrid.getRowColData(i,2);
		   }else{
		       failinfo1 = failinfo1 + "、" + PolGrid.getRowColData(i,2);
		   }
		}
		
		if(lockflag=="锁定"){
		   if(failinfo2==""){
		       failinfo2 = PolGrid.getRowColData(i,2);
		   }else{
		       failinfo2 = failinfo2 + "、" + PolGrid.getRowColData(i,2);
		   }
		}
				
    }
    
    if(failinfo1!=""){
	  failinfo1 = failinfo1 + "无法锁定！\n";
	}
  
    if(failinfo2!=""){
	  failinfo2 = failinfo2 + "已经锁定！";
	}
    if(failinfo1!="" ||failinfo2!=""){
      alert(failinfo1+failinfo2);
      return false;
    }
  
	fm.all("workType").value = "LOCK";  
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./GrpSendToBankConfirmBatchSave.jsp";
	fm.submit();
}

function batchunlock()
{
    var rowNum=PolGrid.mulLineCount ; 
	if(rowNum==null || rowNum<1){
		alert('请先查询！');
		return false;
	}
	var ChkFlag=0;
	for(var i=0;i<rowNum;i++){
		if(PolGrid.getChkNo(i)){
			ChkFlag=1;
		}
	}
	if(ChkFlag==0){
		alert('至少选择一项');
		return false;
	}
	var failinfo1="";
	var failinfo2="";
	var lockflag ="";
    for(var i=0;i<rowNum;i++){
		if(!PolGrid.getChkNo(i)) continue;
		lockflag = PolGrid.getRowColData(i,9);
		if(lockflag=="无法锁定"){
		   if(failinfo1==""){
		       failinfo1 = PolGrid.getRowColData(i,2);
		   }else{
		       failinfo1 = failinfo1 + "、" + PolGrid.getRowColData(i,2);
		   }
		}
		
		if(lockflag=="未锁定"){
		   if(failinfo2==""){
		       failinfo2 = PolGrid.getRowColData(i,2);
		   }else{
		       failinfo2 = failinfo2 + "、" + PolGrid.getRowColData(i,2);
		   }
		}
				
    }
    
    if(failinfo1!=""){
		   failinfo1 = failinfo1 + "无法解除锁定！\n";
	}
    
    if(failinfo2!=""){
	  failinfo2 = failinfo2 + "未锁定！";
    }
  
    if(failinfo1!="" ||failinfo2!=""){
      alert(failinfo1+failinfo2);
      return false;
    }
	fm.all("workType").value = "UNLOCK";  
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.action="./GrpSendToBankConfirmBatchSave.jsp";
	fm.submit();
}

function batchfirstHasten()
{		
    var rowNum=PolGrid.mulLineCount ; 
	if(rowNum==null || rowNum<1){
		alert('请查询后选择转账失败状态的保单！');
		return false;
	}
	var ChkFlag=0;
	for(var i=0;i<rowNum;i++){
		if(PolGrid.getChkNo(i)){
			ChkFlag=1;
		}
	}
	if(ChkFlag==0){
		alert('至少选择一项');
		return false;
	}
	
	var failinfo1="";
	var failinfo2="";
	var failinfo3="";
	var failflag ="";
	var strSql="";
	var result;
    for(var i=0;i<rowNum;i++){
		if(!PolGrid.getChkNo(i)) continue;
		failflag = PolGrid.getRowColData(i,6);
		if(failflag=="现金"){
		   failinfo1=failinfo1+"1";
		}
		failflag = PolGrid.getRowColData(i,7);		
		if(failflag!="转账失败"){
		   failinfo2 = failinfo2 + "2";
		}
		strSql = "select * from loprtmanager where code='75' and otherno = '"+PolGrid.getRowColData(i,2)+"' and StateFlag='0'";
		result = easyExecSql(strSql);	
		if (result!=null)
		{
		    if(failinfo3==""){
		       failinfo3 = PolGrid.getRowColData(i,2);
		   }else{
		       failinfo3 = failinfo3 + "、" + PolGrid.getRowColData(i,2);
		   }
		}
						
    }
    
    if(failinfo1!=""){
		failinfo1 ='现金缴费方式的保单不能添加进"首期保费催缴通知书"打印队列！\n';
	}
	if(failinfo2!=""){
		failinfo2 ='非转账失败状态的保单不能被添加到催缴打印队列!\n';
	}
	if(failinfo3!=""){
	    failinfo3 ="投保单"+failinfo3 + "的首期保费催缴通知书打印已添加进打印队列并且未首次打印,不能重复添加！";
	}
  
   if(failinfo1!="" ||failinfo2!=""||failinfo3!=""){
     alert(failinfo1+failinfo2+failinfo3);
     return false;
   }
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	
	fm.action="./FirstHastenBatchSave.jsp";
	fm.submit();


} 

function addInfo1(FlagStr,content)
{
	
  showInfo.close();
  if (FlagStr == "Fail" )
  {   
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=向打印队列添加数据失败!原因为：<br>" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");     
  }
  else
  { 
  	
	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");   
  }
	
}
