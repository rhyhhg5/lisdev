//程序名称：ScanContInput.js
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约扫描的EasyQuery
 *  描述:查询显示对象是扫描件.显示条件:扫描件已上载成功
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	if(!verifyInput2())
	return false;
	// 书写SQL语句
	var strSQL = "";
	if(type =="1")
	{
		initGrpGrid1();
		strSQL = "select lwmission.missionprop1,lwmission.missionprop3,(select char(max(makedate))||' '||char(max(maketime)) from es_doc_main where doccode=lwmission.MissionProp1 and subtype='TB01'),lwmission.missionid,lwmission.submissionid from lwmission where 1=1 "
				 + " and activityid = '0000001099' "
				 + " and processid = '0000000003'"
				   +" and missionprop5 = '1' "
				 + " and exists(select 1 from ES_Doc_Main where (State is null or State = ''  or State = '01') and DocCode = MissionProp1) "
				 + getWherePart('missionprop1','PrtNo')
				 + getWherePart('missionprop2','InputDate')
				 + getWherePart('missionprop3','ManageCom','like')
				 //+ " and missionprop3 like '"+manageCom+"%%'"
				 //+ getWherePart('missionprop4','ScanOperator')
				 + " order by lwmission.missionprop1"
				 ;
				 	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有需要录入的扫描件！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid1;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;	 
	}
	if(type == "2")
	{
		initGrpGrid();
		strSQL = "select * "
						+" from( "
						+" select lwmission.missionprop1,lwmission.missionprop3,lwmission.missionprop2,lwmission.missionid, "
						+" lwmission.submissionid, "
						+" (select case when count(1)<>0 then '有问题件' else '没有问题件' end from LCGrpIssuePol where grpcontno in (select grpcontno from lcgrpcont where PrtNo =lwmission.missionprop1) and state<>'5' ) "
						+" from lwmission where 1=1 "
						+" and activityid = '0000002099' "
						 +" and missionprop5 = '1' "
						+" and processid = '0000000004' "
						 +	getWherePart('missionprop1','PrtNo')
						 +	getWherePart('missionprop2','InputDate')
						 +	getWherePart('missionprop3','ManageCom','like')
										 //+ " and missionprop3 like '"+manageCom+"%%'"
										 //+ getWherePart('missionprop4','ScanOperator')
						+" order by lwmission.missionprop1  ) as X "
				 	;
				 		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有需要录入的扫描件！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;	
	}

}

/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入扫描随动页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GoToInput()
{
	if(type=="2")
  {
  var i = 0;
  var checkFlag = 0;
  var state = "0";
  
  for (i=0; i<GrpGrid.mulLineCount; i++) {
    if (GrpGrid.getSelNo(i)) { 
      checkFlag = GrpGrid.getSelNo();
      break;
    }
  }
  if (checkFlag) { 
  	prtNo = GrpGrid.getRowColData(checkFlag - 1, 1); 	
    var	ManageCom = GrpGrid.getRowColData(checkFlag - 1, 2);
    var MissionID =GrpGrid.getRowColData(checkFlag - 1, 4);
    var SubMissionID =GrpGrid.getRowColData(checkFlag - 1, 5);
	var PolApplyDate = GrpGrid.getRowColData(checkFlag - 1, 3);
	
	//校验印刷号是否符合规则
    if (prtNo != "") {
    	var srtPrtno = CheckPrtno(prtNo);
     	if (srtPrtno != "") {
     		alert(srtPrtno);
    	  return false;
     	}
	}
    var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;

    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    var strReturn = window.showModalDialog(urlStr, "", sFeatures);

    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    var strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and PolState=1002 ";
	  var arrResult = easyExecSql(strSql);
	  if (arrResult!=null && arrResult[0][1]!=operator) {
	    alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
	    return;
	  }
    
    var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
    
    if (strReturn == "1") 
 			{
 				window.open("./GrpContInputScanMain.jsp?ScanFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&scantype=scan&PolApplyDate="+PolApplyDate, "", sFeatures); 
 			}
  }
  else {
    alert("请先选择一条保单信息！"); 
  			}
	}else if(type=="1"){
	  var i = 0;
  var checkFlag = 0;
  var state = "0";
  
  for (i=0; i<GrpGrid1.mulLineCount; i++) {
    if (GrpGrid1.getSelNo(i)) { 
      checkFlag = GrpGrid1.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	prtNo = GrpGrid1.getRowColData(checkFlag - 1, 1); 	
    var	ManageCom = GrpGrid1.getRowColData(checkFlag - 1, 2);
    var MissionID =GrpGrid1.getRowColData(checkFlag - 1, 4);
    var SubMissionID =GrpGrid1.getRowColData(checkFlag - 1, 5);
		var PolApplyDate = GrpGrid1.getRowColData(checkFlag - 1, 3);
    var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;

    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    var strReturn = window.showModalDialog(urlStr, "", sFeatures);

    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    /*
    var strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and PolState=1002 ";
	  var arrResult = easyExecSql(strSql);
	  if (arrResult!=null && arrResult[0][1]!=operator) {
	    alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
	    return;
	  }

    var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
    */
    if (strReturn == "1") 
 			{
 					window.open("./ContInputScanMain.jsp?ScanFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&scantype=scan&PolApplyDate="+PolApplyDate, "", sFeatures); 
 			}
  }
  else {
    alert("请先选择一条保单信息！"); 
  			}	
	}
  
}
