 <%
//程序名称：TempFeeInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String CurrentDate = PubFun.getCurrentDate();
  String CurrentTime = PubFun.getCurrentTime();
%>  
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="ContInsuredInput.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
  	fm.SelPolNo.value='';
  	try{mSwitch.deleteVar("PolNo");}catch(ex){};
  }
  catch(ex)
  {
    alert("在TempFeeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
   if(checktype=="1")
   {
     param="121";
     fm.pagename.value="121";     
   }
   if(checktype=="2")
   {
     param="22";
     fm.pagename.value="22";     
   }   
  }
  catch(ex)
  {
    alert("在TempFeeInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
	//alert(ContNo);
  try
  { 
    fm.EdorType.value = EdorType;
    fm.EdorAcceptNo.value = EdorAcceptNo;
    fm.EdorNo.value = EdorNo;
    
    fm.PolTypeFlag.CodeData="0|^0|记名^1|不记名^3|不记名(有档案清单)^4|不记名(事后补名单)";
    initInpBox();
    initSelBox();    
    initInsuredGrid(); 
    initImpartGrid();
    initImpartDetailGrid();
    initPolGrid();
    initGrpInfo();
    if (scantype== "scan") 
    {  
    setFocus(); 
    }
//    alert(acctype);

    
    if (acctype=="2" || acctype=="5"){//2公共账户;5公共保额
		if(acctype=="2"){
			fm.all('PolTypeFlag').value="2";
			fm.all('Name').value="公共账户";
			fm.all('PolTypeFlagName').value="公共账户";
			document.getElementById('addInsuredButton').value = "添加公共账户";
			document.getElementById('modifyInsuredButton').style.display = "none";
			document.getElementById('deleteInsuredButton').value = "删除公共账户";
			document.getElementById('nametitle').style.display = "none";
			document.getElementById('nameinput').style.display = "none";
			//chenwm20070820 显示公共帐户的工作单位 不强制添加.
			//fm.GrpName.elementtype="";
		}else{
			fm.all('PolTypeFlag').value="5";
			fm.all('Name').value="公共保额";
			fm.all('PolTypeFlagName').value="公共保额";
            fm.ContPlanCode.style.display="none";
            fm.ContPlanCodeName.style.display="none";
            PubAmnType1.style.display="";
            PubAmnType2.style.display="";
            fm.PubAmnType.style.display="";
            fm.PubAmnTypeName.style.display="";		  
			LblCompanyName1.style.display="none";
			LblCompanyName2.style.display="none";
			fm.OrganComCode.style.display="none";
			fm.GrpName.style.display="none";
 		}
		ContPlanCode1.style.display="none";
        ContPlanCode2.style.display="none";
		fm.all('Sex').value="0"; 
		fm.all('Birthday').value=getBirthday();
		fm.all('IDType').value = "4";
		fm.all('OccupationCode').value="";
		fm.all('InsuredAppAge').value="30";
		fm.all('OccupationType').value="1";
		fm.all('InsuredPeoples').value="";
		fm.all('InsuredPeoples').readOnly=false;
		fm.all('InsuredAppAge').readOnly=false;
		DivGrpNoname.style.display="none";
		DivAccInsured.style.display="none";
		DivAddress.style.display="none";
		divSalary.style.display="none";
		
		
		//bieming1.style.display="";
		//bieming2.style.display="";
		
		DivLCInsuredButton.style.display="none";
		MName1.style.display="none";
		MName2.style.display="none";
		fm.MainName.style.display="none";
		
		EnglishName1.style.display="none";
		EnglishName2.style.display="none";
		fm.EnglishName.style.display="none";
		
		ContPlanCode5.style.display="";
	    ContPlanCode6.style.display="";
		
		document.all("IDTypeT").style.display = "none";  
		document.all("IDTypeI").style.display = "none";
		document.all("IDNoT").style.display = "none";  
		document.all("IDNoI").style.display = "none";
		
		document.all("TempT").style.display = "none";  
		document.all("TempI").style.display = "none";
		document.all("Temp1T").style.display = "none";  
		document.all("Temp1I").style.display = "none";
         
		showCodeName();
     }
     

    if(Auditing=="1")
    {
    	var tmSwitch = top.opener.parent.VD.gVSwitch;
			mSwitch.deleteVar( "ContNo" );
  		mSwitch.addVar( "ContNo", "", tmSwitch.getVar("ContNo") );
  		mSwitch.updateVar("ContNo", "", tmSwitch.getVar("ContNo"));
  		mSwitch.deleteVar( "ProposalContNo" );
  		mSwitch.addVar( "ProposalContNo", "", tmSwitch.getVar("ContNo") );
  		mSwitch.updateVar("ProposalContNo", "", tmSwitch.getVar("ContNo"));
  		mSwitch.deleteVar( "PrtNo" );
  		mSwitch.addVar( "PrtNo", "", tmSwitch.getVar("PrtNo") );
  		mSwitch.updateVar("PrtNo", "", tmSwitch.getVar("PrtNo"));
  		mSwitch.deleteVar( "GrpContNo" );
  		mSwitch.addVar( "GrpContNo", "", tmSwitch.getVar("GrpContNo") );
  		mSwitch.updateVar("GrpContNo", "", tmSwitch.getVar("GrpContNo"));
    }

    getContInfo(); 
    getInsuredInfo(); 
    //如果是无名单的话，显示无名单代表的被保人的人数
//    alert(fm.all('PolTypeFlag').value);
    if(fm.all('PolTypeFlag').value=="1"||fm.all('PolTypeFlag').value=="3"||fm.all('PolTypeFlag').value=="4")
    {
    	DivGrpNoname.style.display="";	
    	//无名单被保人人数默认为1
    	//fm.all('InsuredPeoples').value="1";
    }   
    
    if(fm.all('FamilyType').value==''||fm.all('FamilyType').value==null||fm.all('FamilyType').value=='0'||fm.all('FamilyType').value=='false'){  
      fm.all('FamilyType').value='0';
      divTempFeeInput.style.display="none";
      getProposalInsuredInfo();          //获得个人单信息，填写被保人表
        if(acctype=="5")
       {
           var tContNo=fm.all("ContNo").value;
           var tSql1 = "select a.pubamnttype,a.pubamntname from lccont a where a.contno = '"+tContNo+"'";
           //alert(tSql);
           var resultgg=easyExecSql(tSql1,1,1);
       
           try{fm.all('PubAmnType').value= resultgg[0][0];}catch(ex){};
           try{fm.all('bieming').value= resultgg[0][1];}catch(ex){};           
           //alert(tContNo);
       }
    }
  }
  catch(re)
  {
  	//alert(re.number+"\n"+re.description);
    alert("TempFeeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  if (ContType=="2")
  {
  	//initContPlanCode();
  	//modify by winnie ASR20093243 统括保单的服务机构初始化问题
	  //initExecuteCom(); 
	//if((acctype=="null"||acctype=="0")&&TKBillTag=="1"){

	//end modify
  
  }

  if(LoadFlag=="1"){ 
       divLCInsuredPerson.style.display="none";
       //divFamilyType.style.display="";
       divTempFeeInput.style.display="";
       fm.FamilyType.value = "1";
       divPrtNo.style.display="";       
       divSamePerson.style.display="";       
       DivGrpNoname.style.display="";  
       fm.PolTypeFlag.value='0'; 
	   //添加对客户内部号码的初始化，杨红于20050608添加
	   fm.SequenceNo.value=InsuredGrid.mulLineCount+1;
	   if(fm.SequenceNo.value=='1')
	  {
		   fm.InsuredSequencename.value="第一被保险人资料";
	  }
	  if(fm.SequenceNo.value=='2')
	  {
		   fm.InsuredSequencename.value="第二被保险人资料";
	  }
	  if(fm.SequenceNo.value=='3')
	  {
		   fm.InsuredSequencename.value="第三被保险人资料";
	  }
       showCodeName();
	   //杨红添加测试代码
	  // alert(InsuredGrid.mulLineCount+1);
  }
  if(LoadFlag=="2"||LoadFlag=="23" || LoadFlag=="4"){ 
   fm.InsuredSequencename.value="被保险人资料";   
   divFamilyType.style.display="none";
   divSamePerson.style.display="none";
   divPrtNo.style.display="none";
   divTempFeeInput.style.display="none";
   divInputContButton.style.display="none";
   divGrpInputContButton.style.display="";
   divLCInsuredPerson.style.display="none";   
   DivRelation.style.display="";
	   if(fm.all('PolTypeFlag').value=="2" || fm.all('PolTypeFlag').value=="5"){
	   		DivAddress.style.display="none";
	   		DivRelation.style.display="none";
	   }else{
	   	DivAddress.style.display="";
	   }
   showCodeName(); 
  }
  if(LoadFlag=="3"){
    divTempFeeInput.style.display="";
    getInsuredInfo();
  }
 
  if(LoadFlag=="5"||LoadFlag=="25"){
    divTempFeeInput.style.display="";
    getInsuredInfo();
    divFamilyType.style.display="";
    divAddDelButton.style.display="";
    fm.butBack.style.display="none";
    divSamePerson.style.display="none";   
    divInputContButton.style.display="none";
    divApproveContButton.style.display="";
    divLCInsuredPerson.style.display="none";
    showCodeName();
  }

  if(LoadFlag=="6"){
    divTempFeeInput.style.display="";
    divPrtNo.style.display="";    
    divInputContButton.style.display="none";
    divQueryButton.style.display="";
    divFamilyType.style.display="";
    divAddDelButton.style.display="none";
    DivGrpNoname.style.display="none"; 
    fm.butBack.style.display="none";
    divSamePerson.style.display="none"; 
    showCodeName();      
  }
  if(LoadFlag=="7"){
   divFamilyType.style.display="none";
   divSamePerson.style.display="none";
   divPrtNo.style.display="none";
   divTempFeeInput.style.display="none";
   divInputContButton.style.display="none";
   divGrpInputContButton.style.display="";
   divLCInsuredPerson.style.display="none";   
   fm.InsuredSequencename.value="被保险人资料"; 
   showCodeName();
  }
  if(LoadFlag=="9"){
    fm.InsuredSequencename.value="被保险人资料";     
    divTempFeeInput.style.display="none";
    divInputContButton.style.display="none";
    divQueryButton.style.display="";
    divFamilyType.style.display="none";
    fm.butBack.style.display="none";
    divSamePerson.style.display="none"; 
    divLCInsuredPerson.style.display="none";
    DivRelation.style.display="";
    DivAddress.style.display="none";
    divPrtNo.style.display=""; 
    divAddDelButton.style.display="none";  
     //fm.ContNo.value=ContNo;
     //alert(fm.ContNo.value);
     //fm.FamilyType.value="1";     
    showCodeName();     
  }  
  //alert(LoadFlag);
  //if(LoadFlag=="16"){
  //	LoadFlag="GQ";
  //}
  //从综合查询--投保信息查询--》被保人人清单-->选被保人传入的LoadFlag=="GQ"
  
  if(LoadFlag=="GQ"){
       fm.InsuredSequencename.value="被保险人资料";   
       divFamilyType.style.display="none";
       divSamePerson.style.display="none";
       divPrtNo.style.display="none";
       divTempFeeInput.style.display="none";
       divInputContButton.style.display="none";
       divQueryButton.style.display="";
       divLCInsuredPerson.style.display="none";   
       DivRelation.style.display="";
       divSalary.style.display="none";
       divAddDelButton.style.display="none";
       showCodeName(); 
       var strSQL ="select InsuredNo,Name,Sex,Birthday,RelationToMainInsured,SequenceNo ,mainInsuredNo "
                  +" ,(select codename from ldcode where codetype='relation' and code=a.RelationToMainInsured)"
                  +" ,a.IDNo,a.Salary ,a.OrganComCode "
                  +" ,(select grpname from lcorgan where OrganComCode=a.OrganComCode and grpcontno=a.grpcontno)"
                  +" ,a.ContPlanCode "
                  +" ,(select contplanname  from lccontplan n where n.grpcontno=a.grpcontno and n.contplancode=a.contplancode )"
                  +" ,a.OccupationCode ,(select occupationname  from LDOccupation o where o.occupationcode=a.OccupationCode)"
                  +" ,a.OccupationType "
                  +" ,(case a.OccupationType when '1' then '一类' when '2' then '二类' when '3' then '三类'  "
                  +" when '4' then '四类' when '5' then '五类' when '6' then '六类'  else '' end ) "
                  +",a.PluralityType ,a.WorkType"
                  +" ,a.JoinCompanyDate"
                  +" from LCInsured  a where ContNo='"+fm.all("ContNo").value+"'"
                  +" union"
                  +" select InsuredNo,Name,Sex,Birthday,RelationToMainInsured,SequenceNo,mainInsuredNo  "
                  +" ,(select codename from ldcode where codetype='relation' and code=a.RelationToMainInsured)"
                  +" ,a.IDNo, a.Salary,a.OrganComCode "
                  +" ,(select grpname from lcorgan where OrganComCode=a.OrganComCode and grpcontno=a.grpcontno)"
                  +" ,a.ContPlanCode "
                  +" ,(select contplanname  from lccontplan n where n.grpcontno=a.grpcontno and n.contplancode=a.contplancode )"
                  +" ,a.OccupationCode ,(select occupationname  from LDOccupation o where o.occupationcode=a.OccupationCode)"
                  +" ,a.OccupationType "
                  +" ,(case a.OccupationType when '1' then '一类' when '2' then '二类' when '3' then '三类'  "
                  +" when '4' then '四类' when '5' then '五类' when '6' then '六类'  else '' end ) "
                  +",a.PluralityType ,a.WorkType"
                  +" ,a.JoinCompanyDate"
                  +" from LBInsured a where ContNo='"+fm.all("ContNo").value+"'"
                  ;
       var arrInfo=new Array;
       arrInfo=easyExecSql(strSQL,1,1);
       //alert(arrInfo);
       fm.all("InsuredNo").value=arrInfo[0][0];
       //alert(fm.all("InsuredNo").value);
       fm.all("Name").value=arrInfo[0][1];
       fm.all("Sex").value=arrInfo[0][2];
       fm.all("Birthday").value=arrInfo[0][3];
       fm.all("RelationToMainInsured").value=arrInfo[0][4];
       fm.all("SequenceNo").value=arrInfo[0][5];
       fm.all("MainInsuredNo").value=arrInfo[0][6];
       fm.all("RelationToMainInsuredName").value=arrInfo[0][7];
       fm.all("IDNo").value=arrInfo[0][8];
       fm.all("Salary").value=arrInfo[0][9];
       fm.all("OrganComCode").value=arrInfo[0][10];
       fm.all("GrpName").value=arrInfo[0][11];
       fm.all("ContPlanCode").value=arrInfo[0][12];
       fm.all("ContPlanCodeName").value=arrInfo[0][13];
       fm.all("OccupationCode").value=arrInfo[0][14];
       fm.all("OccupationCodeName").value=arrInfo[0][15];
       fm.all("OccupationType").value=arrInfo[0][16];
       fm.all("OccupationTypeName").value=arrInfo[0][17];
       fm.all("PluralityType").value=arrInfo[0][18];
       fm.all("WorkType").value=arrInfo[0][19];
       divShowJoinComPanyDate.style.display="";
       fm.all("QJoinCompanyDate").value=arrInfo[0][20];
       var tSql2 = "select distinct  DepartmentId,InsuredID, InsuredStat, "
              +" case a.InsuredStat when '0' then '在职' when '1' then '退休' end,"
              +" (select d.PostalAddress from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.ZipCode from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.Phone from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.Mobile from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.EMail from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select min(v.CValiDate) from LCPol v where v.ContNo=a.ContNo and a.InsuredNo=v.InsuredNo),"
              +" (select max(v.EndDate)-1 from LCPol v where v.ContNo=a.ContNo and a.InsuredNo=v.InsuredNo)"
              +" from LCInsured a  "
              +" where a.insuredno='"+fm.all('InsuredNo').value+"' and a.contno='"+oldContNo+"'"
              +" union"
              +" select distinct  DepartmentId,InsuredID, InsuredStat, "
              +" case a.InsuredStat when '0' then '在职' when '1' then '退休' end,"
              +" (select d.PostalAddress from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.ZipCode from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.Phone from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.Mobile from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.EMail from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select min(v.CValiDate) from LBPol v where v.ContNo=a.ContNo and a.InsuredNo=v.InsuredNo),"
              +" (select max(m.EdorValiDate)-1 from LBPol v ,LPEdorItem m where m.edorno=v.edorno and m.contno=v.contno and m.insuredno=v.insuredno and v.ContNo=a.ContNo and a.InsuredNo=v.InsuredNo)"
              +" from LbInsured a  "
              +" where a.insuredno='"+fm.all('InsuredNo').value+"' and a.contno='"+oldContNo+"'"
              ;
       //alert(oldContNo);
       var customerArr2=new Array;
       customerArr2=easyExecSql(tSql2,1,1);
       //alert(customerArr2);
       if(customerArr2!=""&&customerArr2!="null"&&customerArr2!=null)
       {
       	  fm.all("DepartmentId").value=customerArr2[0][0];
          fm.all("InsuredID").value=customerArr2[0][1];
          fm.all("RetireFlag").value=customerArr2[0][2];
          fm.all("RetireFlagName").value=customerArr2[0][3];
          fm.all("PostalAddress").value=customerArr2[0][4];
          fm.all("ZipCode").value=customerArr2[0][5];
          fm.all("Phone").value=customerArr2[0][6];
          fm.all("Mobile").value=customerArr2[0][7];
          fm.all("EMail").value=customerArr2[0][8];
          fm.all("InsureDate").value=customerArr2[0][9];
          fm.all("StopDate").value=customerArr2[0][10];
       }
       var tSql = "select BankCode,BankInfo, "
              +" BankPrv,(select codename  from ldcode where codetype='nativeplacebak' and code=BankPrv),"
              +" BankCity,(select codename  from ldcode where codetype='provincecity' and code=BankCity)"
              +" ,AccName ,BankAccNo,"
              +"(select  BankName from LDBank b where b.BankCode=t.BankCode )"
              +" from insured_view t where insuredno='"+fm.all('InsuredNo').value+"' and t.contno='"+oldContNo+"'"
              ;
      //alert(tSql);
       var customerArr3=easyExecSql(tSql,1,1);
       if(customerArr3!=""&&customerArr3!="null"&&customerArr3!=null)
       {
          fm.all("BankCode").value=customerArr3[0][0];
          fm.all("BankInfo").value=customerArr3[0][1];
          fm.all("BankPrv").value=customerArr3[0][2];
          fm.all("BankPrvName").value=customerArr3[0][3];
          fm.all("BankCity").value=customerArr3[0][4];
          fm.all("BankCityName").value=customerArr3[0][5];
          fm.all("AccName").value=customerArr3[0][6];
          fm.all("BankAccNo").value=customerArr3[0][7];
          fm.all("AppntBankCodeName").value=customerArr3[0][8];
      }
      var tSql4 = " select distinct name from LCInsured where insuredno='"+fm.all('MainInsuredNo').value+"' "
                +" union"
                +" select distinct name from LBInsured where insuredno='"+fm.all('MainInsuredNo').value+"'"
        ;
      var MNameI=easyExecSql(tSql4,1,1);
      if(MNameI!=""&&MNameI!="null"&&MNameI!=null)
      {
        	fm.all("MainName").value=MNameI;
      }
      var InsuredNo=fm.all("InsuredNo").value;
      var ContNo=fm.all("ContNo").value;
      if(InsuredNo!=null&&InsuredNo!="")
     {
        var strSQL ="select PolNo,RiskCode,Prem,Amnt from LCPol "
                   +" where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and AppFlag<>'2'"//ASR20081744-处理人员变更-系统错误只有已签单（投保，承保状态）或是已保全确认的时候才显示其对应已有效的险种信息
                   +" union"
                   +" select PolNo,RiskCode,Prem,Amnt from LBPol "
                   +" where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and AppFlag<>'2'"//ASR20081744-处理人员变更-系统错误只有已签单（投保，承保状态）或是已保全确认的时候才显示其对应已有效的险种信息
                   ;
        turnPage.queryModal(strSQL,PolGrid); 
        //turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        ////判断是否查询成功
        //if (!turnPage.strQueryResult)
        //{
        //    return false;
        //}
        ////查询成功则拆分字符串，返回二维数组
        //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        ////设置初始化过的MULTILINE对象
        //turnPage.pageDisplayGrid = PolGrid;
        ////保存SQL语句
        //turnPage.strQuerySql = strSQL;
        ////设置查询起始位置
        //turnPage.pageIndex = 0;
        ////在查询结果数组中取出符合页面显示大小设置的数组
        //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        ////调用MULTILINE对象显示查询结果
        //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
     }  
   }   
   if(LoadFlag=="16"){
   fm.InsuredSequencename.value="被保险人资料";   
   divFamilyType.style.display="none";
   divSamePerson.style.display="none";
   divPrtNo.style.display="none";
   	   if(fm.all('PolTypeFlag').value=="2" || fm.all('PolTypeFlag').value=="5"){
	   		DivRelation.style.display="none";
	   }else{
	  	DivRelation.style.display="";
	   }
   divTempFeeInput.style.display="none";
   divInputContButton.style.display="none";
   divQueryButton.style.display="";
   divLCInsuredPerson.style.display="none";   
   divSalary.style.display="none";
   divAddDelButton.style.display="none";
   showCodeName(); 
    var strSQL ="select InsuredNo,Name,Sex,Birthday,RelationToMainInsured,SequenceNo ,mainInsuredNo "
                  +" ,(select codename from ldcode where codetype='relation' and code=a.RelationToMainInsured)"
                  +" ,a.IDNo,a.Salary ,a.OrganComCode "
                  +" ,(select grpname from lcorgan where OrganComCode=a.OrganComCode and grpcontno=a.grpcontno)"
                  +" ,a.ContPlanCode "
                  +" ,(select contplanname  from lccontplan n where n.grpcontno=a.grpcontno and n.contplancode=a.contplancode )"
                  +" ,a.OccupationCode ,(select occupationname  from LDOccupation o where o.occupationcode=a.OccupationCode)"
                  +" ,a.OccupationType "
                  +" ,(case a.OccupationType when '1' then '一类' when '2' then '二类' when '3' then '三类'  "
                  +" when '4' then '四类' when '5' then '五类' when '6' then '六类'  else '' end ) "
                  +",a.PluralityType ,a.WorkType"
                  +" ,a.JoinCompanyDate"
                  +" from LCInsured  a where ContNo='"+fm.all("ContNo").value+"'"
                  +" union"
                  +" select InsuredNo,Name,Sex,Birthday,RelationToMainInsured,SequenceNo,mainInsuredNo  "
                  +" ,(select codename from ldcode where codetype='relation' and code=a.RelationToMainInsured)"
                  +" ,a.IDNo, a.Salary,a.OrganComCode "
                  +" ,(select grpname from lcorgan where OrganComCode=a.OrganComCode and grpcontno=a.grpcontno)"
                  +" ,a.ContPlanCode "
                  +" ,(select contplanname  from lccontplan n where n.grpcontno=a.grpcontno and n.contplancode=a.contplancode )"
                  +" ,a.OccupationCode ,(select occupationname  from LDOccupation o where o.occupationcode=a.OccupationCode)"
                  +" ,a.OccupationType "
                  +" ,(case a.OccupationType when '1' then '一类' when '2' then '二类' when '3' then '三类'  "
                  +" when '4' then '四类' when '5' then '五类' when '6' then '六类'  else '' end ) "
                  +",a.PluralityType ,a.WorkType"
                  +" ,a.JoinCompanyDate"
                  +" from LBInsured a where ContNo='"+fm.all("ContNo").value+"'"
                  ;
       var arrInfo=new Array;
       arrInfo=easyExecSql(strSQL,1,1);
       //alert(arrInfo);
       fm.all("InsuredNo").value=arrInfo[0][0];
       //alert(fm.all("InsuredNo").value);
       fm.all("Name").value=arrInfo[0][1];
       fm.all("Sex").value=arrInfo[0][2];
       fm.all("Birthday").value=arrInfo[0][3];
       fm.all("RelationToMainInsured").value=arrInfo[0][4];
       fm.all("SequenceNo").value=arrInfo[0][5];
       fm.all("MainInsuredNo").value=arrInfo[0][6];
       fm.all("RelationToMainInsuredName").value=arrInfo[0][7];
       fm.all("IDNo").value=arrInfo[0][8];
       fm.all("Salary").value=arrInfo[0][9];
       fm.all("OrganComCode").value=arrInfo[0][10];
       fm.all("GrpName").value=arrInfo[0][11];
       fm.all("ContPlanCode").value=arrInfo[0][12];
       fm.all("ContPlanCodeName").value=arrInfo[0][13];
       fm.all("OccupationCode").value=arrInfo[0][14];
       fm.all("OccupationCodeName").value=arrInfo[0][15];
       fm.all("OccupationType").value=arrInfo[0][16];
       fm.all("OccupationTypeName").value=arrInfo[0][17];
       fm.all("PluralityType").value=arrInfo[0][18];
       fm.all("WorkType").value=arrInfo[0][19];
       divShowJoinComPanyDate.style.display="";
       fm.all("QJoinCompanyDate").value=arrInfo[0][20];
       var tSql2 = "select distinct  DepartmentId,InsuredID, InsuredStat, "
              +" case a.InsuredStat when '0' then '在职' when '1' then '退休' end,"
              +" (select d.PostalAddress from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.ZipCode from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.Phone from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.Mobile from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.EMail from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select min(v.CValiDate) from LCPol v where v.ContNo=a.ContNo and a.InsuredNo=v.InsuredNo),"
              +" (select max(v.EndDate)-1 from LCPol v where v.ContNo=a.ContNo and a.InsuredNo=v.InsuredNo)"
              +" from LCInsured a  "
              +" where a.insuredno='"+fm.all('InsuredNo').value+"' and a.contno='"+oldContNo+"'"
              +" union"
              +" select distinct  DepartmentId,InsuredID, InsuredStat, "
              +" case a.InsuredStat when '0' then '在职' when '1' then '退休' end,"
              +" (select d.PostalAddress from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.ZipCode from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.Phone from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.Mobile from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select d.EMail from lcaddress d where d.AddressNo=a.AddressNo and a.InsuredNo=d.CustomerNo),"
              +" (select min(v.CValiDate) from LBPol v where v.ContNo=a.ContNo and a.InsuredNo=v.InsuredNo),"
              +" (select max(m.EdorValiDate)-1 from LBPol v ,LPEdorItem m where m.edorno=v.edorno and m.contno=v.contno and m.insuredno=v.insuredno and v.ContNo=a.ContNo and a.InsuredNo=v.InsuredNo)"
              +" from LbInsured a  "
              +" where a.insuredno='"+fm.all('InsuredNo').value+"' and a.contno='"+oldContNo+"'"
              ;
       //alert(oldContNo);
       var customerArr2=new Array;
       customerArr2=easyExecSql(tSql2,1,1);
       //alert(customerArr2);
       if(customerArr2!=""&&customerArr2!="null"&&customerArr2!=null)
       {
       	  fm.all("DepartmentId").value=customerArr2[0][0];
          fm.all("InsuredID").value=customerArr2[0][1];
          fm.all("RetireFlag").value=customerArr2[0][2];
          fm.all("RetireFlagName").value=customerArr2[0][3];
          fm.all("PostalAddress").value=customerArr2[0][4];
          fm.all("ZipCode").value=customerArr2[0][5];
          fm.all("Phone").value=customerArr2[0][6];
          fm.all("Mobile").value=customerArr2[0][7];
          fm.all("EMail").value=customerArr2[0][8];
          fm.all("InsureDate").value=customerArr2[0][9];
          fm.all("StopDate").value=customerArr2[0][10];
       }
       var tSql = "select BankCode,BankInfo, "
              +" BankPrv,(select codename  from ldcode where codetype='nativeplacebak' and code=BankPrv),"
              +" BankCity,(select codename  from ldcode where codetype='provincecity' and code=BankCity)"
              +" ,AccName ,BankAccNo,"
              +"(select  BankName from LDBank b where b.BankCode=t.BankCode )"
              +" from insured_view t where insuredno='"+fm.all('InsuredNo').value+"' and t.contno='"+oldContNo+"'"
              ;
      //alert(tSql);
       var customerArr3=easyExecSql(tSql,1,1);
       if(customerArr3!=""&&customerArr3!="null"&&customerArr3!=null)
       {
          fm.all("BankCode").value=customerArr3[0][0];
          fm.all("BankInfo").value=customerArr3[0][1];
          fm.all("BankPrv").value=customerArr3[0][2];
          fm.all("BankPrvName").value=customerArr3[0][3];
          fm.all("BankCity").value=customerArr3[0][4];
          fm.all("BankCityName").value=customerArr3[0][5];
          fm.all("AccName").value=customerArr3[0][6];
          fm.all("BankAccNo").value=customerArr3[0][7];
          fm.all("AppntBankCodeName").value=customerArr3[0][8];
      }
      var tSql4 = " select distinct name from LCInsured where insuredno='"+fm.all('MainInsuredNo').value+"' "
                +" union"
                +" select distinct name from LBInsured where insuredno='"+fm.all('MainInsuredNo').value+"'"
        ;
      var MNameI=easyExecSql(tSql4,1,1);
      if(MNameI!=""&&MNameI!="null"&&MNameI!=null)
      {
        	fm.all("MainName").value=MNameI;
      }
      var InsuredNo=fm.all("InsuredNo").value;
      var ContNo=fm.all("ContNo").value;
      if(InsuredNo!=null&&InsuredNo!="")
     {
        var strSQL ="select PolNo,RiskCode,Prem,Amnt from LCPol "
                   +" where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and AppFlag<>'2'"//ASR20081744-处理人员变更-系统错误只有已签单（投保，承保状态）或是已保全确认的时候才显示其对应已有效的险种信息
                   +" union"
                   +" select PolNo,RiskCode,Prem,Amnt from LBPol "
                   +" where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and AppFlag<>'2'"//ASR20081744-处理人员变更-系统错误只有已签单（投保，承保状态）或是已保全确认的时候才显示其对应已有效的险种信息
                   ;
        //alert(strSQL);
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult)
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = PolGrid;
        //保存SQL语句
        turnPage.strQuerySql = strSQL;
        //设置查询起始位置
        turnPage.pageIndex = 0;
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
     }  
  }    
  if(LoadFlag=="13"){ 
   divFamilyType.style.display="none";
   divSamePerson.style.display="none";
   divPrtNo.style.display="none";
   divTempFeeInput.style.display="none";
   divInputContButton.style.display="none";
   divLCInsuredPerson.style.display="none"; 
   divApproveModifyContButton.style.display="";
   
   getInsuredInfo();  
   showCodeName();
  }
  if(this.LoadFlag == "99"&&checktype=="1"){
       divAddDelButton.style.display="none";
       divInputContButton.style.display="none";
       autoMoveButton.style.display=""; 
       showCodeName();   
 }
   if(this.LoadFlag == "99"&&checktype=="2"){
       divAddDelButton.style.display="none";
       divInputContButton.style.display="none";
       autoMoveButton.style.display=""; 
       showCodeName();   
 }    
	showAllCodeName(); 
}


// 暂收费信息列表的初始化
function initInsuredGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="客户号码";          		//列名
      iArray[1][1]="80px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="姓名";         			//列名
      iArray[2][1]="60px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="性别";      	   		//列名
      iArray[3][1]="40px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=2;              	//是否允许输入,1表示允许，0表示不允许
      iArray[3][4]="Sex"; 


      iArray[4]=new Array();
      iArray[4][0]="出生日期";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0; 
      
      iArray[5]=new Array();
      iArray[5][0]="与第一被保险人关系";      	   		//列名
      iArray[5][1]="100px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=2; 
      iArray[5][4]="Relation";              	        //是否引用代码:null||""为不引用
      iArray[5][9]="与主被保险人关系|code:Relation&NOTNULL";
      //iArray[5][18]=300;
      
      iArray[6]=new Array();
      iArray[6][0]="客户内部号";      	   		//列名
      iArray[6][1]="80px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=2; 
      iArray[6][10]="MutiSequenceNo";
      iArray[6][11]="0|^1|第一被保险人 ^2|第二被保险人 ^3|第三被保险人";      

      InsuredGrid = new MulLineEnter( "fm" , "InsuredGrid" ); 
      //这些属性必须在loadMulLine前
      InsuredGrid.mulLineCount = 0;   
      InsuredGrid.displayTitle = 1;
      InsuredGrid.canSel =1;
      InsuredGrid. selBoxEventFuncName ="getInsuredDetail" ;     //点击RadioBox时响应的JS函数
      InsuredGrid. hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      InsuredGrid. hiddenSubtraction=1;
      InsuredGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 告知信息列表的初始化
function initImpartGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="告知版别";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="ImpartVer";
      iArray[1][9]="告知版别|len<=5";
      iArray[1][18]=300;

      iArray[2]=new Array();
      iArray[2][0]="告知编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ImpartCode";
      iArray[2][5]="2|3";
      iArray[2][6]="0|1";
      //iArray[2][7]="getImpartCode";
      iArray[2][9]="告知编码|len<=4";
      iArray[2][15]="ImpartVer";
      iArray[2][17]="1";
      iArray[2][18]=700;

      iArray[3]=new Array();
      iArray[3][0]="告知内容";         		//列名
      iArray[3][1]="250px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="填写内容";         		//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=150;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][9]="填写内容|len<=200";

//      iArray[5]=new Array();
//      iArray[5][0]="告知客户类型";         		//列名
//      iArray[5][1]="90px";            		//列宽
//      iArray[5][2]=90;            			//列最大值
//      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
//      iArray[5][4]="CustomerType";
//      iArray[5][9]="告知客户类型|len<=18";
//      
//      iArray[6]=new Array();
//      iArray[6][0]="告知客户号码";         		//列名
//      iArray[6][1]="90px";            		//列宽
//      iArray[6][2]=90;            			//列最大值
//      iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
//      iArray[6][4]="CustomerNo";
//      iArray[6][9]="告知客户号码";
//      iArray[6][15]="Cont";

      ImpartGrid = new MulLineEnter( "fm" , "ImpartGrid" ); 
      //这些属性必须在loadMulLine前
      ImpartGrid.mulLineCount = 1;   
      ImpartGrid.displayTitle = 1;
      //ImpartGrid.tableWidth   ="500px";
      ImpartGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ImpartGrid.setRowColData(1,1,"asdf");
    }
    catch(ex) {
      alert(ex);
    }
}
// 告知明细信息列表的初始化
function initImpartDetailGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="告知版别";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="ImpartVer";
      iArray[1][9]="告知版别|len<=5";
      iArray[1][18]=300;

      iArray[2]=new Array();
      iArray[2][0]="告知编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ImpartCode";
      iArray[2][5]="2|3";
      iArray[2][6]="0|1";
      //iArray[2][7]="getImpartCode";
      iArray[2][9]="告知编码|len<=4";
      iArray[2][15]="ImpartVer";
      iArray[2][17]="1";
      iArray[2][18]=700;

      iArray[3]=new Array();
      iArray[3][0]="告知内容";         		//列名
      iArray[3][1]="250px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="疾病内容";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许      

      iArray[5]=new Array();
      iArray[5][0]="开始时间";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=150;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][9]="开始时间|date";

      iArray[6]=new Array();
      iArray[6][0]="结束时间";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=90;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][9]="结束时间|date";
      
      iArray[7]=new Array();
      iArray[7][0]="证明人";         		//列名
      iArray[7][1]="75px";            		//列宽
      iArray[7][2]=90;            			//列最大值
      iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="目前情况";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=90;            			//列最大值
      iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许


      iArray[9]=new Array();
      iArray[9][0]="能否证明";         		//列名
      iArray[9][1]="50px";            		//列宽
      iArray[9][2]=90;            			//列最大值
      iArray[9][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[9][4]="yesno";
    
 
      ImpartDetailGrid = new MulLineEnter( "fm" , "ImpartDetailGrid" ); 
      //这些属性必须在loadMulLine前
      ImpartDetailGrid.mulLineCount = 1;   
      ImpartDetailGrid.displayTitle = 1;
      ImpartDetailGrid.loadMulLine(iArray);  
      
    }
    catch(ex) {
      alert(ex);
    }
}
//被保人险种信息列表初始化
function initPolGrid(){
    var iArray = new Array();
      
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		        //列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保险单险种号码";         		//列名
      iArray[1][1]="60px";            		        //列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]=" 险种编码";         		//列名
      iArray[2][1]="60px";            		        //列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[2][4]="RiskCode";              			//是否允许输入,1表示允许，0表示不允许           
      
      iArray[3]=new Array();
      iArray[3][0]="保费(元)";         		//列名
      iArray[3][1]="60px";            		        //列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="保额(元)";         		//列名
      iArray[4][1]="60px";            		        //列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
          
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.canSel =1;
      PolGrid. selBoxEventFuncName ="getPolDetail";
      PolGrid. hiddenPlus=1;
      PolGrid. hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);  
    }
    catch(ex) {
      alert(ex);
    }
}


function getContInfo()

{ //alert(123);
    try { fm.all( 'ContNo' ).value = mSwitch.getVar( "ContNo" ); if(fm.all( 'ContNo' ).value=="false"){fm.all( 'ContNo' ).value="";} } catch(ex) { };   
    try { fm.all( 'PrtNo' ).value = mSwitch.getVar( "PrtNo" ); } catch(ex) { };
    try { fm.all( 'ProposalContNo' ).value = mSwitch.getVar( "ProposalContNo" ); } catch(ex) { };   
    try { fm.all( 'GrpContNo' ).value = mSwitch.getVar( "GrpContNo" ); } catch(ex) { };
    try { fm.all( 'FamilyType' ).value = mSwitch.getVar( "FamilyType" ); } catch(ex) {};
    //try { fm.all( 'PolTypeFlag' ).value = mSwitch.getVar( "PolType" );if(fm.all( 'PolTypeFlag' ).value=="false"){fm.all( 'PolTypeFlag' ).value="0";} } catch(ex) {};
    //try { fm.all( 'InsuredPeoples' ).value = mSwitch.getVar( "InsuredPeoples" );if(fm.all( 'InsuredPeoples' ).value=="false"){fm.all( 'InsuredPeoples' ).value="";} } catch(ex) {};
//    alert("ContNo"+fm.all( 'ContNo' ).value);
//    alert("PrtNo"+fm.all( 'PrtNo' ).value);
//	alert(fm.all('PolTypeFlag').value);
// chenweiming@sinosoft.com.cn 取消sql中的 'poltype=1' 2007-06-19
    var strSql = "select poltype,Peoples,(select nativeplace from lcinsured where contno=lccont.contno and insuredno=lccont.insuredno) From lccont where prtno='" + fm.all( 'PrtNo' ).value+"' and contno='"+fm.all( 'ContNo' ).value+"'";
    var arrResult = easyExecSql(strSql);
//  alert("strSql="+strSql+" arrResult='"+arrResult+"'");
    if (arrResult != null) {
      fm.all( 'PolTypeFlag' ).value = arrResult[0][0];
      fm.all( 'InsuredPeoples' ).value= arrResult[0][1];
     // fm.all( 'NoticeWay' ).value= arrResult[0][2];
      fm.all( 'NativePlace' ).value= arrResult[0][2];
    }
    else
    {
    	fm.all( 'InsuredPeoples' ).value="1";
    }
}
function initContPlanCode()
{
	 //alert(mSwitch.getVar( "ProposalGrpContNo" ));
	 fm.all("ContPlanCode").CodeData=getContPlanCode(mSwitch.getVar( "ProposalGrpContNo" ));	
}
function initExecuteCom()
{
	 fm.all("ExecuteCom").CodeData=getExecuteCom(mSwitch.getVar( "ProposalGrpContNo" ));	
}

function initGrpInfo()
{
	fm.PrtNo.value=mSwitch.getVar('PrtNo');
	fm.SaleChnl.value=mSwitch.getVar('SaleChnl');
	fm.ManageCom.value=mSwitch.getVar('ManageCom');
	fm.AgentCode.value=mSwitch.getVar('AgentCode');
	fm.AgentGroup.value=mSwitch.getVar('AgentGroup');
	//fm.GrpName.value=mSwitch.getVar('GrpName');
	
	fm.CValiDate.value=mSwitch.getVar('CValiDate');
	fm.ProposalGrpContNo.value = mSwitch.getVar('ProposalGrpContNo');
}
function chaxungong()
{


}
</script>
