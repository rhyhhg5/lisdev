<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var LoadFlag = "<%=request.getParameter("LoadFlag")%>";//记录登陆机构
	var operator="<%=tGI.Operator%>";//记录操作员
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="GroupPolApprove.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GroupPolApproveInit.jsp"%>
  <title>团体投保单复核 </title>
</head>
<body onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 团体信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <!--<TD  class= title>
            团体保单号码
          </TD-->
          <!--TD  class= input-->
            <Input class= common name=GrpProposalNo type="hidden">
          <!--/td-->
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo verify="印刷号码|int">
          </TD>
           <TD  class= title>
            销售渠道
          </TD>
          <TD  class= input>
            <Input class=codeNo name=SaleChnl verify="销售渠道|code:SaleChnl" ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName readonly=true >
          </TD>        
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
          </TD>
         </TR>
        <TR  class= common>         
	      	<TD  class= title8>
            业务员代码
          </TD>
          <TD  class= input8>
      			<Input class=codeNo name=AgentCode  ondblclick="return showCodeList('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
         </TD>
          <TD  class= title8 style="display:'none'">
            业务员组别
          </TD>
          <TD  class= input8 style="display:'none'">
            <Input class="codeNo" name=AgentGroup verify="代理人组别|code:branchcode" ondblclick="return showCodeList('agentgroup2',[this,AgentGroupName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('agentgroup2',[this,AgentGroupName],[0,1]);"><input class=codename name=AgentGroupName readonly=true >
          </TD>  
        </TR>
       
    </table>
          <INPUT VALUE="查  询" class=cssButton  TYPE=button onclick="easyQueryClick();">
          
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divGrpGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssButton TYPE=button  onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button  onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button  onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class=cssButton TYPE=button  onclick="getLastPage();"> 					
  	</div>
  	<p>
  	<INPUT class=cssButton VALUE="投保单复核" TYPE=button onclick="showApproveDetail();">
      <!--INPUT VALUE="查询团体下个人投保单" Class=Common  TYPE=button onclick="showPol();"--> 
      <!--INPUT VALUE="团体投保单复核" Class=Common TYPE=button onclick="approveGrpPol();"--> 
  	</p>
  </form>
         
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
