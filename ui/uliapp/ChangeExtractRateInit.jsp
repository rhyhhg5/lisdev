<%

//@NAME:ChangeExtractRateInit.jsp
//@DESC:ChangeExtractRate.jsp页面的初始化页面
//@AUTHOR:_LaoChen_ ;)
//@SINCE:20070517

%>

<script language="JavaScript"  type="text/javascript" >
function initInpBox()
{
  try
  {
	//alert(this.tLoadFlag);
	if(this.tLoadFlag=="16"){
		divRiskPlanSave.style.display="none";
	}
	//add by cjg
	 
   if(this.LoadFlag == "99"){
        //显示代码选择中文
   autoMoveButton.style.display="";   

    }  
    //for extension
  }
  catch(ex)
  {
  	 alert("在ChangeExtractRateInit.jsp-->InitInpBox()函数中发生异常:初始化界面错误!"+ex.message);
  }
}
function initForm()
{
  try
  {
    initInpBox();
    initRiskInfoGrid();
	initChangeExtractRateGrid();

    initQuery();
if(scantype=="scan")
  {
    setFocus();
  } 

  }
  catch(ex)
  {
    //alter("在ChangeExtractRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ex.message);
  }
}

//初始化RiskInfoGrid
function initRiskInfoGrid()
{
   
    var iArray = new Array();

      try
      {
         
      	iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=10;
		iArray[0][3]=0;
 	  
 	  iArray[1] = new Array();
	  iArray[1][0]="险种号码";         //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="40px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
 
 	  iArray[2] = new Array();
	  iArray[2][0]="险种名称";         //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="40px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
   	  iArray[3] = new Array();
	  iArray[3][0]="集体险种编码";         //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[3][1]="40px";         			//列宽
      iArray[3][2]=10;          			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      
      
      RiskInfoGrid = new MulLineEnter( "fm" , "RiskInfoGrid" );
      RiskInfoGrid.mulLineCount = 0;
	  RiskInfoGrid.displayTitle = 1;
	 RiskInfoGrid.hiddenPlus = 1;
	 RiskInfoGrid.hiddenSubtraction = 1;
	 RiskInfoGrid.canSel=1;
	 RiskInfoGrid.selBoxEventFuncName = "ShowExtratRate";
	 RiskInfoGrid.loadMulLine(iArray);
		
 
       // alert("aa");
      }
      catch(ex)
      {
    alter("在ChangeExtractRateInit.jsp-->initRiskInfoGrid函数中发生异常:初始化界面错误!"+ex.message);
      }
  }


function initChangeExtractRateGrid()
{
      var iArray = new Array();  
      try
      {
      iArray[0]=new Array();
	  iArray[0][0]="序号";
	  iArray[0][1]="30px";
	  iArray[0][2]=10;
	  iArray[0][3]=0;
	  
      iArray[1] = new Array();
	  iArray[1][0]="被保险人参保起始年期";         //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="40px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;    

      iArray[2] = new Array();
	  iArray[2][0]="被保险人参保终止年期";         //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="40px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=0;    

                   			//是否允许输入,1表示允许，0表示不允许
      iArray[3] = new Array();
	  iArray[3][0]="费用提取比例";         //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[3][1]="40px";         			//列宽
      iArray[3][2]=10;          			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4] = new Array();
	  iArray[4][0]="费率保存状态";         //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[4][1]="40px";         			//列宽
      iArray[4][2]=10;          			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      ChangeExtractRateGrid = new MulLineEnter( "fm" , "ChangeExtractRateGrid" );
      ChangeExtractRateGrid.mulLineCount = 0;
      ChangeExtractRateGrid.displayTitle = 1;
      ChangeExtractRateGrid.canChk = 0;
      ChangeExtractRateGrid.hiddenPlus=1;
      ChangeExtractRateGrid.hiddenSubtraction=1;
      ChangeExtractRateGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
   		 alter("在ChangeExtractRateInit.jsp-->initChangeExtractRateGrid函数中发生异常:初始化界面错误!"+ex.message);
      }
 	
  }
 </script>
