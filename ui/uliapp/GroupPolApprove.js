//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var k=0;
var mSwitch = parent.VD.gVSwitch;
/*********************************************************************
 *  执行待新单复核单的EasyQuery
 *  描述:查询显示对象是主险保单.显示条件:主险或任一附加险未新单复核
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
var turnPage = new turnPageClass(); 
function easyQueryClick()
{
	k++;
	// 初始化表格
	initGrpGrid();
	if(!verifyInput2())
	return false;
	
	// 书写SQL语句
	var strSQL = "";

	//strSQL = "select ProposalGrpContNo,PrtNo,SaleChnl,GrpName,CValiDate from LCGrpCont where 1=1 "
	//			 +" and ApproveFlag=0 "
	//			 + getWherePart( 'PrtNo' )
	//			 + getWherePart( 'ProposalGrpContNo','GrpProposalNo' )
	//			 + getWherePart( 'ManageCom' )
	//			 + getWherePart( 'AgentCode' )
	//			 + getWherePart( 'AgentGroup' )
	//			 + getWherePart( 'SaleChnl' );
	//			 + "order by makedate,maketime";

	strSQL = "select lwmission.missionprop1,lwmission.missionprop2,(select codename from ldcode where codetype='salechnl' and code=lwmission.missionprop3),lwmission.missionprop7,lwmission.missionprop8,lwmission.missionid,lwmission.submissionid,case lwmission.missionprop20 when 'Y' then '问题件未修改' when  'N' then '问题件已修改' when 'N1' then '有新的问题件' else '没有问题件' end from lwmission where 1=1 "
				 + " and activityid = '0000002001' "
				  + " and   exists (select GrpProposalNo  from lcgrppol a where a.prtno=missionprop2 and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G') and riskcode !='370301' ) "
				 + " and processid = '0000000004' "
				 //+ " and missionprop3 like '"+comcode+"%%'"
				 + getWherePart('missionprop1','GrpProposalNo')
				 + getWherePart('missionprop2','PrtNo')
				 + getWherePart('missionprop3','SaleChnl')
				 + getWherePart('getUniteCode(missionprop5)','AgentCode')
				 + getWherePart('missionprop4','ManageCom','like')
				 + getWherePart('missionprop6','AgentGroup')
						 
				 + " order by lwmission.missionprop2"						//根据印刷号排序
				 ;		
			
	if(LoadFlag == "17"){
		strSQL = "select lwmission.missionprop1,lwmission.missionprop2,(select codename from ldcode where codetype='salechnl' and code=lwmission.missionprop3),lwmission.missionprop7,lwmission.missionprop8,lwmission.missionid,lwmission.submissionid from lwmission where 1=1 "
				 + " and activityid = '0000002002' "
				   + " and exists (select GrpProposalNo  from lcgrppol a where a.prtno=missionprop2 and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G') and riskcode !='370301' ) "
				 + " and processid = '0000000004'"
				 //+ " and missionprop3 like '"+comcode+"%%'"
				 + getWherePart('missionprop1','GrpProposalNo')
				 + getWherePart('missionprop2','PrtNo')
				 + getWherePart('missionprop3','SaleChnl')
				 + getWherePart('getUniteCode(missionprop5)','AgentCode')
				 + getWherePart('missionprop4','ManageCom','like')
				 + getWherePart('missionprop6','AgentGroup')
						 
				 + " order by lwmission.missionprop2"						//根据印刷号排序
				 ;		
	}		 	
	turnPage.queryModal(strSQL, GrpGrid);
}

/*********************************************************************
 *  显示EasyQuery的结果
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{       
		// 初始化表格
		initGrpGrid();
		//HZM 到此修改
		GrpGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpGrid.loadMulLine(GrpGrid.arraySave);		
		//HZM 到此修改		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpGrid.setRowColData( i, j+1, arrResult[i][j] );
			} 
		}
		GrpGrid.delBlankLine();
	} 
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function showApproveDetail() { 
  if (GrpGrid.getSelNo() == 0) {
    alert("请先选择一条保单信息！");
    return false;
  } 
  var polNo = GrpGrid.getRowColData(GrpGrid.getSelNo() - 1, 1);
  var prtNo = GrpGrid.getRowColData(GrpGrid.getSelNo() - 1, 2);
  var cMissionID=GrpGrid.getRowColData(GrpGrid.getSelNo() - 1, 6);
  var cSubMissionID = GrpGrid.getRowColData(GrpGrid.getSelNo() - 1, 7);
var strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and  CreatePos='承保复核' and  PolState=1003 ";
  var arrResult = easyExecSql(strSql);
  if (arrResult!=null && arrResult[0][1]!=operator) {
    alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
    return;
  }
  //锁定该印刷号
  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保复核&PolState=1003&Action=INSERT";
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
  //配合以前实现过的页面功能，源于ProposalMain.jsp
  mSwitch.deleteVar("PolNo");
	mSwitch.addVar("PolNo", "", polNo);
	mSwitch.updateVar("PolNo", "", polNo);
	//alert(polNo);
	
	mSwitch.deleteVar("GrpContNo");
	mSwitch.addVar("GrpContNo", "", polNo);
	mSwitch.updateVar("GrpContNo", "", polNo);
	//alert(mSwitch.getVar("ApprovePolNo"));
	if(LoadFlag == "17"){
		easyScanWin = window.open("./GroupPolApproveInfo.jsp?ScanFlag=1&LoadFlag="+LoadFlag+"&polNo="+polNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&scantype=scan&prtNo="+prtNo,"");    
	}else{
  	easyScanWin = window.open("./GroupPolApproveInfo.jsp?ScanFlag=1&LoadFlag=4&polNo="+polNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&scantype=scan&prtNo="+prtNo,"");    
  }
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
    if(fm.all('AgentCode').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=2","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	  }
	if(fm.all('AgentCode').value != "")	 {

	var cAgentCode = fm.AgentCode.value;  //保单号码	
	var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
     }
	}	
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];
  	fm.AgentGroup.value = arrResult[0][1];fm.all('AgentGroupName').value = easyExecSql("select name from labranchgroup where  AgentGroup='"+arrResult[0][1]+"'");
  }
}
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }
 if(cCodeName=="agentgroup2"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
 
}
