<%
//*******************************************************
//* 程序名称：inputButton.jsp
//* 程序功能：页面通用按钮
//* 创建日期：2002-05-20
//* 更新记录：  更新人    更新日期     更新原因/内容
//*             Minim   2002-05-20    新建
//******************************************************
%>
<div id="inputButton" style="display: none">
	<table class="common" align=center>
		<tr>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="重  置"  TYPE=button onclick="return resetForm();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="this.disabled=true; setEnable('submitButton');return submitForm();" id="submitButton">
			</td>
		</tr>
	</table>
</div>
<div id="inputQuest" style="display: 'none'">
	        <input type="button" class=cssButton name="Input" value="上一步" onClick="returnparent()" class=cssButton>
	        
      <div id="inputApproveConFirm" style="display: 'none'">
<INPUT class=cssButton id="Donextbutton4" VALUE="复核完毕" TYPE=button onclick="this.disabled=true; setEnable('Donextbutton4');inputConfirm(2);"> 
      </div>
      <div id="inputQuestIn" style="display: 'none'" style="float: right">
		<input type="button" class=cssButton name="Input" value="错误字段标记" onClick="QuestInput()" class=cssButton>
      </div>	
		<!--<INPUT class=cssButton VALUE=问题件查询 TYPE=button onclick="QuestQuery();">-->
		<!--input type="button" class=cssButton name="Input" value="强制解除锁定" onClick="unLockTable();" class=cssButton-->
</div>
<div id="inputConFirm" style="display: 'none'">
<INPUT class=cssButton VALUE=录入完毕 TYPE=button onclick=" this.disabled=true; setEnable('copyEnd');GrpConfirm(0);" id="copyEnd">
<div id="SpecPol" style="float: right" style="display: ''">
<INPUT class=cssButton VALUE=标记特殊件  TYPE=button onclick="IssuePol();">
</div>
</div>
<div id="modifyButton" style="display: 'none'">
	  	<INPUT class=cssButton VALUE="修  改" name = "modify9" TYPE=button onclick="this.disabled=true; setEnable('updateButton');return updateClick();" style="float: right" id="updateButton">
			<INPUT class=cssButton VALUE="删  除"  TYPE=button onclick="this.disabled=true; setEnable('deletButton');return deleteClick();" style="float: right" id="deletButton">
		<!--input type="button" class=cssButton name="Input" value="强制解除锁定" onClick="unLockTable();" class=cssButton-->
</div>

<%
//*******************************************************
//* 程序名称：queryButton.jsp
//* 程序功能：页面通用按钮
//* 创建日期：2002-05-20
//* 更新记录：  更新人    更新日期     更新原因/内容
//*             Minim   2002-05-20    新建
//******************************************************
%>
<div id="queryButton" style="display: none">
	<table class="common" align=center>
		<tr>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="return queryClick();">
			</td>
		</tr>
	</table>
</div>

<%
//*******************************************************
//* 程序名称：updateButton.jsp
//* 程序功能：页面通用按钮
//* 创建日期：2002-05-20
//* 更新记录：  更新人    更新日期     更新原因/内容
//*             Minim   2002-05-20    新建
//******************************************************
%>
<div id="updateButton" style="display: none">
	<table class="common" align=center>
		<tr>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="this.disabled=true; setEnable('updateButton');return updateClick();" id="updateButton">
			</td>
		</tr>
	</table>
</div>

<%
//*******************************************************
//* 程序名称：deleteButton.jsp
//* 程序功能：页面通用按钮
//* 创建日期：2002-05-20
//* 更新记录：  更新人    更新日期     更新原因/内容
//*             Minim   2002-05-20    新建
//******************************************************
%>
<div id="deleteButton" style="display: none">
	<table class="common" align=center>
		<tr>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="删  除"  TYPE=button onclick="this.disabled=true; setEnable('deleteButton');return deleteClick();" id="deleteButton">
			</td>
		</tr>
	</table>
</div>

<%
//*******************************************************
//* 程序名称：autoMoveButton.jsp
//* 程序功能：页面通用按钮
//* 创建日期：2002-05-20
//* 更新记录：  更新人    更新日期     更新原因/内容
//*             Minim   2002-05-20    新建
//******************************************************
%>
<div id="autoMoveButton" style="display: none">
	<input type="button" name="autoMoveInput" value="随动定制确定" onclick="submitAutoMove(''+param+'');" class=cssButton>
	<input type="button" name="autoMoveInput" value="上一步" onclick="returnparent();" class=cssButton>	
        <input type=hidden  id=""  name="autoMoveFlag">
        <input type=hidden  id=""  name="autoMoveValue">
        <input type=hidden id="" name="pagename" value="">             
</div>

<%
//*******************************************************
//* 程序名称：autoMoveButton.jsp
//* 程序功能：页面通用按钮
//* 创建日期：2002-05-20
//* 更新记录：  更新人    更新日期     更新原因/内容
//*             Minim   2002-05-20    新建
//******************************************************
%>
<div id="inputQuestButton" style="float: right" style="display: none">
	<input type="button" name="Input" value="问题件录入" onClick="QuestInputC()" class=cssButton>
</div>
<input type=hidden id="UrgencyFlag" name="UrgencyFlag" value="0">
<input type=hidden name="EdorType">

</body>
</html>