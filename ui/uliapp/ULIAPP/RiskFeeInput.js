//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var QueryCount = 0;
var mulLineCount = 0;

var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
	if(showInfo!=null){
		try{
			showInfo.focus();
		}
		catch(ex){
			showInfo=null;
		}
	}
}

//提交，保存按钮对应操作
function submitForm(){
	if (!beforeSubmit()){
		return false;
	}
	//by gzh 20110104
	if(fm.RiskFeeMode.value == "" || fm.RiskFeeMode.value == null){
		alert("管理费收取方式不能为空，请选择！");
		return false;
	}
	//------ by gzh end
    if(!verifySubmit()) //投连校验
    {
    	return false;
    }
	if(!verifyInput2()){
		return false;
	}
	if(RiskFeeGrid.checkValue2(RiskFeeGrid.FeeCalMode2,RiskFeeGrid)== false){
		return false;
	}
	if (!confirm('确认您的操作')){
		return false;
	}
	
	fm.all('mOperate').value = "INSERT||MAIN";
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
	showInfo.close();
	if( FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	easyQueryClick();
}

/*********************************************************************
 *  选择险种后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	try	{
		//if( cCodeName == "GrpRisk" )
		if( cCodeName == "GrpRisk1" )
		{
			fm.LimitNum.value = "";
			fm.LimitFee.value = "";
			initRiskFeeGrid();
			//对于万能投连需要录入C4投资标志
			var strSql = "select 1 from lmriskapp where risktype4 in ('3','4') and riskcode = '"+Field.value+"'";
			var aResult = easyExecSql(strSql,1,0,1);
			if(aResult){
			    divGrpLimit.style.display ='';
				divInverstFlag.style.display ='';
				fm.C4Flag.verify="投资标志|NOTNULL";
				strSql = "select RiskProp1, decode(RiskProp1,'0','非直接投资','1','直接投资') from LCGrpPolSpec where grpcontno = '"+
				fm.GrpContNo.value+"' and riskcode = '"+Field.value+"' and GrpPolSpecCode = 'C4investflag'";
				aResult = easyExecSql(strSql,1,0,1);
				if(aResult){
					fm.C4Flag.value = aResult[0][0];
					fm.C4FlagName.value = aResult[0][1];
				}
				else{
					fm.C4Flag.value = "0";
					fm.C4FlagName.value = "非直接投资";
				}
				if(Field.value=="SEK2"||Field.value=="UEK2"||Field.value=="SCK01"||Field.value=="UEK3") //Amended by Fang on 2008-06-27; 增加UEK3 2009-01-11
				{
							divTQRulesInput.style.display ='';
							strSql = "select RiskProp1, RiskProp3,RiskProp5 ,RiskProp6,RiskProp7 from LCGrpPolSpec where grpcontno = '"+
							fm.GrpContNo.value+"' and riskcode = '"+Field.value+"' and GrpPolSpecCode = 'TQRules'";
							aResult = easyExecSql(strSql,1,0,1);
							if(aResult){
							fm.MinGetIntv.value = aResult[0][0];
							fm.GetIntvF.value = aResult[0][1];
							fm.GetIntvWT.value = aResult[0][2];
							if(aResult[0][3]=="Y")
								fm.GetIntvWT.value = "60";
							}
							fm.TotalGetTime.value = aResult[0][4];
							
							fm.MinGetIntv.verify="最低起领年限|NOTNULL&int";
							fm.GetIntvF.verify="领取间隔|NOTNULL&int"
							fm.GetIntvWT.verify="领取宽限期|NOTNULL&int"
							fm.TotalGetTime.verify="领取次数|NOTNULL&int"
				}
				else
				{
						divTQRulesInput.style.display ='none';
						fm.MinGetIntv.value = "";
						fm.MinGetIntv.verify="";
						fm.GetIntvF.verify="";
						fm.GetIntvWT.verify="";
						fm.TotalGetTime.verify="";
				}
				//转换信息
				strSql = " select riskprop1,riskprop2,riskprop3,riskprop4 From LCGrpPolSpec "
					+ " where grpcontno='"+fm.GrpContNo.value+"' and riskcode='"+Field.value+"' and GrpPolSpecCode='ZHLimit' ";
				aResult = easyExecSql(strSql,1,0,1);
				if(aResult){
					fm.LimitNum.value = aResult[0][1];
					fm.LimitFee.value = aResult[0][3];	
				}
			} else {
				divInverstFlag.style.display ='none';
				divTQRulesInput.style.display ='none';
				divGrpLimit.style.display ='none';
				fm.MinGetIntv.value = "";
				fm.C4Flag.value = "";
				fm.C4Flag.verify="";
				fm.MinGetIntv.verify="";
				fm.GetIntvF.verify="";
				fm.GetIntvWT.verify="";
				fm.TotalGetTime.verify="";
			}
		}
	}
	catch( ex ) {
	    
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm(){
	try{
		initForm();
	}
	catch(re){
		alert("在LAAgent.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//取消按钮对应操作
function cancelForm(){
	showDiv(operateButton,"true");
	showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit(){
	//检查险种管理费明细
	var lineCount = 0;
	var selLineCount=0;
	var O9=0,O0=0;//09及其它
	
	RiskFeeGrid.delBlankLine("RiskFeeGrid");
	lineCount = RiskFeeGrid.mulLineCount;
	//alert("lineCount="+lineCount);
	for (i=0;i<lineCount;i++){
//		alert(RiskFeeGrid.getChkNo(i));
//		if (RiskFeeGrid.getChkNo(i)){
//		}
		//alert(i+"/"+lineCount);
		//alert(RiskFeeGrid.getChkNo(i));
		if (RiskFeeGrid.getChkNo(i)){
			selLineCount=selLineCount+1;
			if(!isNumeric(RiskFeeGrid.getRowColData(i,10))){
				alert("序号为"+(i+1)+"的对应值不是数字!");
				return false;
			}
			
			if(RiskFeeGrid.getRowColData(i,8)!="01"&&RiskFeeGrid.getRowColData(i,3)=="000000"){
				alert("序号为"+(i+1)+",每月管理费只能选择的计算方式是01-固定值(内扣)!");
				return false;
			}
			if(RiskFeeGrid.getRowColData(i,8)!="02"&&RiskFeeGrid.getRowColData(i,3)=="111111"){
				alert("序号为"+(i+1)+",所缴纳的初始费用只能选择的计算方式是02-固定比例(内扣)!");
				return false;
			}
			
			if(RiskFeeGrid.getRowColData(i,8)=="02" || RiskFeeGrid.getRowColData(i,8)=="09"){
				if(RiskFeeGrid.getRowColData(i,10)>1){
					alert("序号为"+(i+1)+",选择的计算方式是固定比例,对应的比例应该在0到1的范围内!");
					return false;
				}
		//add by liyane			
				if(RiskFeeGrid.getRowColData(i,3)=="111111"&&RiskFeeGrid.getRowColData(i,10)>0.05){
					alert("初始费用最高不超过5%!");
					return false;
				}
			
				
		/*	
		var  sql1="select CommissionRate+tFeeRate  from  lcgrppol where  GrpPolNo = '"+fm.all('GrpPolNo').value+"' ";
	
				aResult = easyExecSql(sql1,1,0,1);
			//alert(aResult+""+RiskFeeGrid.getRowColData(i,10)*100);
				if(aResult!=null&&aResult[0][0]!=""&&aResult[0][0]>RiskFeeGrid.getRowColData(i,10)*100){
				alert("初始费用不能小于佣金＋推动费用比例!");
		  	return false;
					}
					//end
					*/
			}
			if(RiskFeeGrid.getRowColData(i,3)=="000000"&&RiskFeeGrid.getRowColData(i,10)>5)
				{
					alert("保单管理费用最高不超过5元!");
					return false;
				}
			if(RiskFeeGrid.getRowColData(i,8)=="09"){
				O9=O9+1;
			}else if(RiskFeeGrid.getRowColData(i,3)!="000000"){
				O0=O0+1;
			}
		}
	}
	//alert(O9+' '+O0);
	if(O9>0&&O0>0){
		alert("如果有一个缴费代码的计算方式是09-按固定比例一笔收取,则除了每月管理费的其它也必须选择09!");
		return false;
		
	}
	if(selLineCount==0){
		alert("请选择险种管理费用！");
		return false;
	}
	return true;
}
function verifySubmit()
{
	//对于投连
	var strSql = "select 1 from lmriskapp where risktype4 in ('3') and riskcode = '"+fm.RiskCode.value+"'";
	var aResult = easyExecSql(strSql,1,0,1);
	//alert(aResult);
	if(aResult)
	{
		if(fm.LimitNum.value=="" || !isNumeric(fm.LimitNum.value)){
				alert("产品/投资转换次数不能为空且必须是数字!");
				return false;
		}
		if(fm.LimitFee.value=="" || !isNumeric(fm.LimitFee.value)){
				alert("超转换次数扣除费用不能为空且必须是数字!");
				return false;
		}
	}
	else if(fm.LimitNum.value!="" ||fm.LimitFee.value!="" )
	{
	   alert("请将转换次数及扣除费用信息录入在投连险下!");
	   fm.LimitNum.value="";
	   fm.LimitFee.value="";
	}
	return true;
}

//Click事件，当点击增加图片时触发该函数
function addClick(){
	//下面增加相应的代码
	if (fm.all('initOperate').value == 'INSERT'){
		mOperate="INSERT||MAIN";
		showDiv(operateButton,"false");
		showDiv(inputButton,"true");
		//fm.all('AgentCode').value = '';
		if (fm.all('AgentCode').value !='')
			resetForm();
	}
	else
		alert('在此不能新增！');
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick(){
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick(){
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick(){
	if (!beforeSubmit()){
		return false;
	}
	fm.all('mOperate').value = "DELETE||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
	if (cShow=="true"){
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

function easyQueryClick(){
	divRiskFeeParam.style.display='none';	
	initRiskFeeGrid();
	if (fm.all('RiskCode').value ==""){
		alert("请选择险种信息！");
		fm.all('RiskCode').focus();
		return false;
	}
	var strSQL = "";
	
	/*modify  by  liyane针对：初始费用的录入格式,初始费用录一条需求，初始费用由select '','',b.PayPlanCode,c.PayPlanName||'所缴纳的初始费用',改为select distinct '','','111111', '所缴纳的初始费用'*/
	strSQL = "select distinct '','','111111', '所缴纳的初始费用','','',b.PayInsuAccName,"
	       +" a.FeeCalMode,a.FeeCalCode,a.FeeValue,a.CompareValue,b.FeeCalModeType,b.FeePeriod,b.MaxTime,b.DefaultFlag,'已存' "
		     +" from LCGrpFee a,LMRiskFee b,LMDutyPay c,LMRiskToAcc d where a.RiskCode = d.RiskCode and "
		     +" b.PayPlanCode = c.PayPlanCode and b.InsuAccNo = d.InsuAccNo and a.FeeCode = b.FeeCode and a.InsuAccNo = "
		     +" b.InsuAccNo and a.PayPlanCode = b.PayPlanCode "
		     +" and a.GrpPolNO = '"+fm.all('GrpPolNo').value+"' and b.payplancode in (select payplancode from lmdutypayrela "
		     +" where dutycode in (select dutycode from lmriskduty where riskcode = '"+fm.all('RiskCode').value+"'))"
	       //增加对万能和投连的处理
		     + " union  "
		     +" select b.insuaccno,'', b.PayPlanCode, b.feename, b.feecode,'', b.PayInsuAccName, a.FeeCalMode,"
		     +"  b.FeeCalCode, a.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, "
		     +" b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '已存' from LCGrpFee a,LMRiskFee b "
		     +" where a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and a.riskcode = b.riskcode and a.feecode=b.feecode and "
		     +" a.payplancode=b.payplancode and a.insuaccno=b.insuaccno and (a.payplancode = '000000' or  a.insuaccno = '000000')"
		     +" union"
		     +" select  distinct '','','111111', '所缴纳的初始费用','','',b.PayInsuAccName,"
	       +" b.FeeCalMode,a.FeeCalCode,a.FeeValue,a.CompareValue,b.FeeCalModeType,b.FeePeriod,b.MaxTime,b.DefaultFlag,'已存' "
		     +" from LBGrpFee a,LMRiskFee b,LMDutyPay c,LMRiskToAcc d where a.RiskCode = d.RiskCode and "
		     +" b.PayPlanCode = c.PayPlanCode and b.InsuAccNo = d.InsuAccNo and a.FeeCode = b.FeeCode and a.InsuAccNo = "
		     +" b.InsuAccNo and a.PayPlanCode = b.PayPlanCode "
		     +" and a.GrpPolNO = '"+fm.all('GrpPolNo').value+"' and b.payplancode in (select payplancode from lmdutypayrela "
		     +" where dutycode in (select dutycode from lmriskduty where riskcode = '"+fm.all('RiskCode').value+"'))"
	       //增加对万能和投连的处理
		     + " union  "
		     +" select b.insuaccno,'', b.PayPlanCode, b.feename, b.feecode,'', b.PayInsuAccName, a.FeeCalMode,"
		     +"  b.FeeCalCode, a.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, "
		     +" b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '已存' from LBGrpFee a,LMRiskFee b "
		     +" where a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and a.riskcode = b.riskcode and a.feecode=b.feecode and "
		     +" a.payplancode=b.payplancode and a.insuaccno=b.insuaccno and (a.payplancode = '000000' or  a.insuaccno = '000000')"
		     ;
	//prompt('',strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		//alert("险种管理费明细查询失败！");
		//return false;
	}
	else{
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = RiskFeeGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
		//by gzh 20101222
		mulLineCount = RiskFeeGrid.mulLineCount;
		for(i=0;i<mulLineCount;i++){
			if("01"==RiskFeeGrid.getRowColData(i,8)){
				RiskFeeGrid.setRowColData(i,6,"固定值(内扣)");
			}else if("02"==RiskFeeGrid.getRowColData(i,8)){
				RiskFeeGrid.setRowColData(i,6,"固定比例(内扣)");
			}
		}
	}
	
//"+fm.all('GrpPolNo').value+"  "+fm.all('RiskCode').value+"
	var tFcode = " and b.FeeCode not in (select FeeCode from LCGrpFee where GrpPolNo = '"+fm.all('GrpPolNo').value+"')"
	           ;
	var tFcode2 = " and b.FeeCode not in (select FeeCode from LBGrpFee where GrpPolNo = '"+fm.all('GrpPolNo').value+"')"
	           ;
	
	 strSQL = ""
	       +" select distinct '','', '111111', '所缴纳的初始费用', '','', b.PayInsuAccName, b.FeeCalMode, b.FeeCalCode, b.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '未存' from LCGrpPol a, LMRiskFee b, LMDutyPay c, LMRiskToAcc d where a.RiskCode = d.RiskCode and b.PayPlanCode = c.PayPlanCode and b.InsuAccNo = d.InsuAccNo and a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and   b.payplancode in (select payplancode from lmdutypayrela where dutycode in (select dutycode from lmriskduty where riskcode = '"+fm.all('RiskCode').value+"')) "+tFcode
	       +" union  select b.insuaccno,'', b.PayPlanCode, b.feename, b.feecode,'', b.PayInsuAccName, b.FeeCalMode, b.FeeCalCode, b.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '未存' from lcgrppol a,LMRiskFee b   where b.payplancode = '000000' and b.insuaccno='000000' and b.FeeItemType<>'06' and a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and a.riskcode=b.riskcode "+tFcode   //b.FeeItemType='06'风险保费
	       +" union  select b.insuaccno,'', b.PayPlanCode, b.feename, b.feecode,'', b.PayInsuAccName, b.FeeCalMode, b.FeeCalCode, b.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '未存' from lcgrppol a,LMRiskFee b   where b.payplancode = '000000' and b.insuaccno<>'000000' and a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and a.riskcode=b.riskcode "+tFcode
	       +" union  select b.insuaccno,'', b.PayPlanCode, (select payplanname from lmdutypay where payplancode=b.payplancode), b.feecode,'', b.PayInsuAccName, b.FeeCalMode, b.FeeCalCode, b.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '未存' from lcgrppol a,LMRiskFee b   where b.payplancode <> '000000' and b.insuaccno='000000' and a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and a.riskcode=b.riskcode"+" and b.payplancode not in (select payplancode from LCGrpFee where GrpPolNo = '"+fm.all('GrpPolNo').value+"')"
	       +" union"
	       +" select distinct '','','111111', '所缴纳的初始费用', '','', b.PayInsuAccName, b.FeeCalMode, b.FeeCalCode, b.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '未存' from LBGrpPol a, LMRiskFee b, LMDutyPay c, LMRiskToAcc d where a.RiskCode = d.RiskCode and b.PayPlanCode = c.PayPlanCode and b.InsuAccNo = d.InsuAccNo and a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and b.FeeCode not in (select FeeCode from LCGrpFee where GrpPolNo = '"+fm.all('GrpPolNo').value+"') and b.payplancode in (select payplancode from lmdutypayrela where dutycode in (select dutycode from lmriskduty where riskcode = '"+fm.all('RiskCode').value+"')) "+tFcode2
	       +" union  select b.insuaccno,'', b.PayPlanCode, b.feename, b.feecode,'', b.PayInsuAccName, b.FeeCalMode, b.FeeCalCode, b.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '未存' from lbgrppol a,LMRiskFee b   where b.payplancode = '000000' and b.insuaccno='000000' and b.FeeItemType<>'06' and a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and a.riskcode=b.riskcode "+tFcode2  //b.FeeItemType='06'风险保费
	       +" union  select b.insuaccno,'', b.PayPlanCode, b.feename,b.feecode,'', b.PayInsuAccName, b.FeeCalMode, b.FeeCalCode, b.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '未存' from lbgrppol a,LMRiskFee b   where b.payplancode = '000000' and b.insuaccno<>'000000' and a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and a.riskcode=b.riskcode "+tFcode2
	       +" union  select b.insuaccno,'', b.PayPlanCode, (select payplanname from lmdutypay where payplancode=b.payplancode),b.feecode,'', b.PayInsuAccName, b.FeeCalMode, b.FeeCalCode, b.FeeValue, case when b.CompareValue is null then 0 else b.CompareValue end, b.FeeCalModeType, b.FeePeriod, b.MaxTime, b.DefaultFlag, '未存' from lbgrppol a,LMRiskFee b   where b.payplancode <> '000000' and b.insuaccno='000000' and a.GrpPolNo = '"+fm.all('GrpPolNo').value+"' and a.riskcode=b.riskcode"+" and b.payplancode not in (select payplancode from LBGrpFee where GrpPolNo = '"+fm.all('GrpPolNo').value+"')"
	       ;
//prompt('',strSQL);
//alert(strSQL);
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		//alert(turnPage.strQueryResult);
	if (!turnPage.strQueryResult) {
	}
	else{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		mulLineCount = RiskFeeGrid.mulLineCount;
		for(i=0; i<turnPage.arrDataCacheSet.length; i++){
			RiskFeeGrid.addOne("RiskFeeGrid");
			RiskFeeGrid.setRowColData(mulLineCount+i,1,turnPage.arrDataCacheSet[i][0]);
			RiskFeeGrid.setRowColData(mulLineCount+i,2,turnPage.arrDataCacheSet[i][1]);
			RiskFeeGrid.setRowColData(mulLineCount+i,3,turnPage.arrDataCacheSet[i][2]);
			RiskFeeGrid.setRowColData(mulLineCount+i,4,turnPage.arrDataCacheSet[i][3]);
			RiskFeeGrid.setRowColData(mulLineCount+i,5,turnPage.arrDataCacheSet[i][4]);
			//RiskFeeGrid.setRowColData(mulLineCount+i,6,turnPage.arrDataCacheSet[i][5]);
			if("01"==turnPage.arrDataCacheSet[i][7]){
				RiskFeeGrid.setRowColData(mulLineCount+i,6,"固定值(内扣)");
			}else if("02"==turnPage.arrDataCacheSet[i][7]){
				RiskFeeGrid.setRowColData(mulLineCount+i,6,"固定比例(内扣)");
			}
			RiskFeeGrid.setRowColData(mulLineCount+i,7,turnPage.arrDataCacheSet[i][6]);
			RiskFeeGrid.setRowColData(mulLineCount+i,8,turnPage.arrDataCacheSet[i][7]);
			RiskFeeGrid.setRowColData(mulLineCount+i,9,turnPage.arrDataCacheSet[i][8]);
			RiskFeeGrid.setRowColData(mulLineCount+i,10,turnPage.arrDataCacheSet[i][9]);
			RiskFeeGrid.setRowColData(mulLineCount+i,11,turnPage.arrDataCacheSet[i][10]);
			RiskFeeGrid.setRowColData(mulLineCount+i,12,turnPage.arrDataCacheSet[i][11]);
			RiskFeeGrid.setRowColData(mulLineCount+i,13,turnPage.arrDataCacheSet[i][12]);
			RiskFeeGrid.setRowColData(mulLineCount+i,14,turnPage.arrDataCacheSet[i][13]);
			RiskFeeGrid.setRowColData(mulLineCount+i,15,turnPage.arrDataCacheSet[i][14]);
			RiskFeeGrid.setRowColData(mulLineCount+i,16,turnPage.arrDataCacheSet[i][15]);
		}
	}
  if(scantype=="scan")
  {
    setFocus();
  } 
	var riskFeeModeSql = "select riskfeemode from lcgrpfee where grpcontno = '"+fm.GrpContNo.value+"' and riskcode = '"+fm.RiskCode.value+"' and payplancode = '000000' ";
	var RiskFeeModeResult = easyExecSql(riskFeeModeSql,1,0,1);
	if(RiskFeeModeResult == null){
		fm.RiskFeeMode.value="";
		fm.RiskFeeModeName.value="";
		return;
	}
	fm.RiskFeeMode.value = RiskFeeModeResult[0][0];
	if("1"==fm.RiskFeeMode.value){
		fm.RiskFeeModeName.value = "从个人账户收取";
	}else if("2"==fm.RiskFeeMode.value){
		fm.RiskFeeModeName.value = "从公共账户收取";
	}

}

function returnparent(){

mSwitch = parent.VD.gVSwitch; 
top.fraInterface.window.location = "./ContPolInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+LoadFlag+"&scantype="+scantype+"&polNo="+mSwitch.getVar( "GrpContNo" )+"&oldContNo="+oldContNo;
//alert(top.fraInterface.window.location+"====123");

}

function QueryRiskFeeParam(parm1,parm2){
	//if(fm.all(parm1).all('InpRiskFeeGridSel').value == '1'){
	if(fm.all(parm1).all('InpRiskFeeGridChk').value == '1'){
		//当前行第1列的值设为：选中
		var cFeeCalMode = fm.all(parm1).all('RiskFeeGrid8').value;	//计算方式
		var cFeeCode = fm.all(parm1).all('RiskFeeGrid5').value;
		var cInsuAccNo = fm.all(parm1).all('RiskFeeGrid1').value;
		var cPayPlanCode = fm.all(parm1).all('RiskFeeGrid3').value;
		/*
		如果交费方式
		07-分档计算
		08-累计分档计算
		需要初始化参数录入界面
		*/
		if(cFeeCalMode == '07' || cFeeCalMode == '08'){
			divRiskFeeParam.style.display='';
			initRiskFeeParamGrid();
			var strSQL = "";
			strSQL = "select FeeMin,FeeMax,FeeRate,FeeID,'已存' "
				     +" from LCGrpFeeParam "
				     +" where 1=1 "
				     +" and FeeCode = '"+cFeeCode+"' and InsuAccNo = '"+cInsuAccNo+"' "
				     +" and PayPlanCode = '"+cPayPlanCode+"' "
				     +" and GrpPolNo = '"+fm.all('GrpPolNo').value+"'"
				     +" union"
				     +" select FeeMin,FeeMax,FeeRate,FeeID,'已存' "
				     +" from LBGrpFeeParam "
				     +" where 1=1 "
				     +" and FeeCode = '"+cFeeCode+"' and InsuAccNo = '"+cInsuAccNo+"' "
				     +" and PayPlanCode = '"+cPayPlanCode+"' "
				     +" and GrpPolNo = '"+fm.all('GrpPolNo').value+"'"
				     ;
			turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
			//判断是否查询成功
			if (!turnPage.strQueryResult) {
			}
			else{
				//查询成功则拆分字符串，返回二维数组
				turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
				//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
				turnPage.pageDisplayGrid = RiskFeeParamGrid;
				//保存SQL语句
				turnPage.strQuerySql = strSQL;
				//设置查询起始位置
				turnPage.pageIndex = 0;
				//在查询结果数组中取出符合页面显示大小设置的数组
				//调用MULTILINE对象显示查询结果
				displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
			}

			strSQL = "select FeeMin,FeeMax,FeeRate,FeeID,'未存' "
				+ "from LMRiskFeeParam "
				+ "where 1=1 "
				+ "and FeeCode = '"+cFeeCode+"' and InsuAccNo = '"+cInsuAccNo+"' "
				+ "and PayPlanCode = '"+cPayPlanCode+"' and FeeCalMode = '"+cFeeCalMode+"' "
				+ "and FeeID not in (select FeeID from LCGrpFeeParam where GrpPolNo = '"+fm.all('GrpPolNo').value+"')";
			turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
			if (!turnPage.strQueryResult) {
			}
			else{
				turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
				mulLineCount = RiskFeeParamGrid.mulLineCount;
				for(i=0; i<turnPage.arrDataCacheSet.length; i++){
					RiskFeeParamGrid.addOne("RiskFeeParamGrid");
					RiskFeeParamGrid.setRowColData(mulLineCount+i,1,turnPage.arrDataCacheSet[i][0]);
					RiskFeeParamGrid.setRowColData(mulLineCount+i,2,turnPage.arrDataCacheSet[i][1]);
					RiskFeeParamGrid.setRowColData(mulLineCount+i,3,turnPage.arrDataCacheSet[i][2]);
					RiskFeeParamGrid.setRowColData(mulLineCount+i,4,turnPage.arrDataCacheSet[i][3]);
					RiskFeeParamGrid.setRowColData(mulLineCount+i,5,turnPage.arrDataCacheSet[i][4]);
				}
			}
		}
		else{
			divRiskFeeParam.style.display='none';
			initRiskFeeParamGrid();
		}
	}
}

function changeFeeModeName(GridIntdex){
	if("02" == RiskFeeGrid.getRowColData(GridIntdex.substring(15),8)){
		RiskFeeGrid.setRowColData(GridIntdex.substring(15),9,'固定比例(内扣)');
		return;
	}
	if("01" == RiskFeeGrid.getRowColData(GridIntdex.substring(15),8)){
		RiskFeeGrid.setRowColData(GridIntdex.substring(15),9,'固定值(内扣)');
		return;
	}
}

function goback(){
	    location.href="ContPolInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype;
	    return;
}
