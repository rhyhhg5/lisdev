<DIV id=DivLCInsuredButton STYLE="display:''">
<!-- 被保人信息部分 -->
	<table>
		<tr>
			<td class=common>
				<IMG id=img4 src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCInsured);">
			</td>
			<td class=titleImg>
				<Input class=mulinetitle value="第一被保险人资料"  name=InsuredSequencename readonly>（客户号：<Input class=common name=InsuredNo>
				<INPUT id="butBack" VALUE="查  询" class=cssButton TYPE=button onclick="queryInsuredNo();"> 首次投保客户无需填写客户号）
				<Div id= "divSamePerson" style="display:'none'">
					<font color=red>
						如投保人为被保险人本人，可免填本栏，请选择
						<INPUT TYPE="checkbox" NAME="SamePersonFlag" onclick="isSamePerson();">
					</font>
				</div>
			</td>
		</tr>
	</table>
</DIV>

<DIV id=DivLCInsured STYLE="display:''">
	<table class=common>
		<TR class=common>
			<TD class=title>保单类型</TD>
				<TD class=input>
					<Input name=PolTypeFlag VALUE="0" CLASS=codeno  readonly verify="保单类型标记"><input name=PolTypeFlagName class=codename value="记名"readonly=true>
				</TD>
			<TD class=title id=nametitle>姓名</TD>
			<TD class=input id=nameinput>
				<Input class=common name=Name elementtype=nacessary verify="被保险人姓名|notnull&len<=20" onblur="getallinfo();">
			
			<TD class=title id=EnglishName1 style="display:'none'">英文名字</TD>
			<TD class=input id=EnglishName2 style="display:'none'">
				<input class=common name=EnglishName>
			</TD>
				<TD CLASS=title></TD>
				<TD class=input></TD>
			<TD class=title id=bieming1 style="display:'none'">别名</TD>
			<TD class=input id=bieming2 style="display:'none'">
		    <input class=common name=bieming>
			</TD>	            

		</TR>
		
		<TR class=common>
			<TD class=title id="MName1" style="display:'none'">主被保险人姓名</TD>
			<TD class=input id="MName2" style="display:'none'">
				<input class=common name=MainName style="display:''">
			</TD>
			<TD class=title id="TempT" STYLE="display:'none'" ></TD>
			<TD class=input id="TempI" STYLE="display:'none'" ></TD>
			<TD class=title id="Temp1T" STYLE="display:'none'" ></TD>
			<TD class=input id="Temp1I" STYLE="display:'none'" ></TD>

		</TR>

		<TR class=common>

		</TR>
	</Table>
	<DIV id=DivRelation STYLE="display:''">
	<table class=common>
		<TR class=common>
			<TD class=title style="display: none">主被保险人客户号</TD>
			<TD class=input style="display: none">
				<Input class=code8 name="MainInsuredNo" type='text'  verify="" ondblclick="getMainInsuredID();" >
			</TD>
			<!--<TD class=title>与投保人关系</TD>
			<!--TD class=input>
				<Input class="codeno" name="RelationToAppnt" verify="与投保人关系|notnull" ondblclick="return showCodeList('Relation', [this,RelationToAppntName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,RelationToAppntName],[0,1]);"><input name=RelationToAppntName class=codename readonly=true elementtype=nacessary>
			</TD-->
		<!--	<TD class=input>
				<Input class="codeno" name="RelationToAppnt" type=hidden  ondblclick="return showCodeList('Relation', [this,RelationToAppntName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,RelationToAppntName],[0,1]);"><input name=RelationToAppntName class=codename readonly=true type=hidden>
			</TD> -->
			<TD class=title>与主被保险人关系</TD>
			<TD class=input>
				<Input class="codeno" name="RelationToMainInsured" verify="与第一被保险人关系|code:Relation" ondblclick="return showCodeList('Relation', [this,RelationToMainInsuredName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,RelationToMainInsuredName],[0,1]);"><input name=RelationToMainInsuredName class=codename readonly=true elementtype=nacessary>
			</TD>
			<TD class=title>出生日期</TD>
			<TD class=input>
				<input class="coolDatePicker" elementtype=nacessary dateFormat="short" name="Birthday" verify="被保险人出生日期|notnull&date">
			</TD>
				<TD class=title>性别</TD>
			<TD class=input>
				<Input class="codeno" name=Sex verify="被保险人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input name=SexName class=codename readonly=true elementtype=nacessary>
			</TD>
		
			<TD CLASS=title style="display:'none'">部门编码</TD>
			<TD class=input style="display:'none'">
			    <Input class=code8 name="DepartmentId" type='text'  verify="" >
				<Input class="code" name="SequenceNo" type=hidden>
				<Input class="codeno" name="RelationToAppnt" type=hidden >
			</TD>
		</TR>
			<TR class=common>
			<TD class=title id="IDTypeT">证件类型</TD>
			<TD class=input id="IDTypeI">
				<Input class="codeno" name="IDType" value="0" verify="被保险人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1]);" onblur="getallinfo();"><input name=IDTypeName class=codename readonly=true elementtype=nacessary>
			</TD>
			
			<TD class=title id="IDNoT">证件号码</TD>
			<TD class=input id="IDNoI">				
				<Input class=common name="IDNo" onblur="getBirthdaySexByIDNo(this.value);showCodeName();" verify="被保险人证件号码|len<=20" elementtype=nacessary>
				<Input class=common  name=ConfirmIDNo elementtype=firm verify="重复帐号|len<=20" STYLE="display:'none'">   
			</TD>
			<TD class=title>服务年数起始日</TD>
			<TD class=input>
				<Input name="JoinCompanyDate" class="coolDatePicker" dateFormat="short">
			</TD>
			
		</TR>
		
		<TR class=common>

		<Input  style="display: none"class=common name="PluralityType">
			
			
			<TD class=title>职业代码</TD>
			<TD class=input>
				<Input class="codeno" name="OccupationCode" verify="被保险人职业代码|code:OccupationCode" ondblclick="return showCodeList('OccupationCode',[this,OccupationCodeName],[0,1]);" onkeyup="return showCodeListKey('OccupationCode',[this,OccupationCodeName],[0,1]);" onfocus="getdetailwork();showCodeName();"><input name=OccupationCodeName class=codename readonly=true>
			</TD>
				<Input  style="display: none"class=common name="WorkType">
			<TD class=title>职业类别</TD>
			<TD class=input>
				<Input class="codeno" name="OccupationType"  verify="被保险人职业类别|code:OccupationType&notnull" ondblclick="return showCodeList('OccupationType',[this,OccupationTypeName],[0,1]);" onkeyup="return showCodeListKey('OccupationType',[this,OccupationTypeName],[0,1]);"><input name=OccupationTypeName class=codename readonly=true elementtype=nacessary>
			</TD>
			
			<TD class=title>级别</TD>
			<TD class=input>
				<Input class="codeno" name="Position" verify="被保险人级别|code:PositionCode" ondblclick="return showCodeList('PositionCode',[this,PositionCodeName],[0,1],null,fm.GrpContNo.value,'grpcontno','1');" onkeyup="return showCodeListKey('PositionCode',[this,PositionCodeName],[0,1],null,fm.GrpContNo.value,'grpcontno','1');" onfocus="getdetailwork();showCodeName();"><input name=PositionCodeName class=codename readonly=true>
			</TD>
		</TR>
	</table>
	<table>
		<tr>
			<td>
			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivClaimBankInfo);">
			</td>
			<td class="titleImg">理赔账户信息
			</td>
		</tr>
</table>
<DIV id=DivClaimBankInfo STYLE="display:''">
	 <table  class= common>         
   
          <TR CLASS=common>
	    <TD CLASS=title  >
	      开户银行 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input class=codeno name=BankCode MAXLENGTH=20 verify="开户行|code:bank" ondblclick="return showCodeList('bank',[this,bankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bank',[this,bankName],[0,1],null,null,null,1);"><input class=codename name=bankName readonly=true >	
      </TD>
	    <TD CLASS=title >
	      户名 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20 verify="户名|len<=20" >
	    </TD>
            <TD CLASS=title width="109" >
	      账号</TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=BankAccNo class="code" VALUE="" CLASS=common ondblclick="return showCodeList('accint',[this],null,null,fm.AppntNo.value,'CustomerNo');" onkeyup="return showCodeListKey('accint',[this],null,null,fm.AppntNo.value,'CustomerNo');" verify="银行帐号|len<=40" onfocus="getdetail();">
	    </TD>
	  </TR>   
    </Table> 
    </div>	
    <table>
		<tr>
			<td>
			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivPayBankInfo);">
			</td>
			<td class="titleImg">个人账户直接交费银行信息
			</td>
		</tr>
</table>    
<DIV id=DivPayBankInfo STYLE="display:''">
     <table  class= common>         
          <TR CLASS=common>
	    <TD CLASS=title  >
	      开户银行 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input class=codeno name=PerBankCode MAXLENGTH=20 verify="开户行|code:bank" ondblclick="return showCodeList('bank',[this,PerBankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bank',[this,PerBankName],[0,1],null,null,null,1);"><input class=codename name=PerBankName readonly=true >	
      </TD>
	    <TD CLASS=title >
	      户名 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PerAccName VALUE="" CLASS=common MAXLENGTH=20 verify="户名|len<=20" >
	    </TD>
            <TD CLASS=title width="109" >
	      账号</TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PerBankAccNo class="code" VALUE="" CLASS=common ondblclick="return showCodeList('accint',[this],null,null,fm.AppntNo.value,'CustomerNo');" onkeyup="return showCodeListKey('accint',[this],null,null,fm.AppntNo.value,'CustomerNo');" verify="银行帐号|len<=40" onfocus="getdetail();">
	    </TD>
	  </TR>   
    </Table> 
    </div>	    
</DIV>
	<DIV id=DivAccInsured STYLE="display:'none'">
	<table class=common>
			
	</table>
</DIV>
	<DIV id=DivGrpNoneed STYLE="display:'none'">
		<table class=common>
			<TR class=common>
				<TD class=title>婚姻状况</TD>
				<TD class=input>
					<Input class="codeno" name="Marriage"  ondblclick="return showCodeList('Marriage',[this,MarriageName],[0,1]);" onkeyup="return showCodeListKey('Marriage',[this,MarriageName],[0,1]);"><input name=MarriageName class=codename readonly=true>
				</TD>
				<TD class=title>学历</TD>
				<TD class=input>
					<Input class="codeno" name="Degree"  ondblclick="return showCodeList('Degree',[this,DegreeName],[0,1]);" onkeyup="return showCodeListKey('Degree',[this,DegreeName],[0,1]);"><input name=DegreeName class=codename readonly=true>
				</TD>
				<TD class=title>是否吸烟</TD>
				<TD class=input>
					<Input class="codeno" name="SmokeFlag"  ondblclick="return showCodeList('YesNo',[this,SmokeFlagName],[0,1]);" onkeyup="return showCodeListKey('YesNo',[this,SmokeFlagName],[0,1]);"><input name=SmokeFlagName class=codename readonly=true>
				</TD>
			</TR>
			<TR class=common>				
				<TD class=title>户口所在地</TD>
				<TD class=input>
					<Input class=common name="RgtAddress">
				</TD>
				<TD class=title>民族</TD>
				<TD class=input>
					<input class="codeno" name="Nationality"  ondblclick="return showCodeList('Nationality',[this,NationalityName],[0,1]);" onkeyup="return showCodeListKey('Nationality',[this,NationalityName],[0,1]);"><input name=NationalityName class=codename readonly=true>
				</TD>
			</TR>
		</table>
		</DIV>
	<DIV id=DivGrpNoname STYLE="display:'none'">
		<table class=common>
			<TR class=common>
				<TD CLASS=title>被保人人数</TD>
				<TD class=input>
					<Input name=InsuredPeoples  CLASS=common verify="被保人人数|num&notnull" elementtype=nacessary>
				</TD>
				<TD class=title>被保人年龄</TD>
				<TD class=input>
					<Input name=InsuredAppAge VALUE="30" readonly=true CLASS=common verify="被保人年龄|num">
				</TD>
				<TD CLASS=title></TD>
				<TD class=input></TD>
			</TR>
		</table>
	</Div>
	<DIV id=DivAddress STYLE="display:''">
		<table class=common>
			<TR class=common>
<!--				<TD class=title>工作单位</TD>
				<TD class=input colspan=3>
					<Input class=common3 name="GrpName">
				</TD>
			</TR>
			<TR class=common>
				<TD class=title>地址代码</TD>
          <TD  class= input>
            <Input class="codeno" name="AddressNo"  ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this,AddressNoName],[0,1],'', '', '', true);" onkeyup="getaddresscodedata();return showCodeListKeyEx('GetAddressNo',[this,AddressNoName],[0,1],'', '', '', true);"><input class=codename name=AddressNoName readonly=true>
          </TD> 
			</TR> -->
<!--
			<TD class=title><span id="LblCompanyName">工作单位</span></TD>
      <TD class=input>
          <Input class="codeno" name="OrganComCode" readOnly type='text'  verify="" onchange="querySendORGComCode();" ondblclick="getORGComCode();" ><input name=GrpName class=codename readonly=true  elementtype=nacessary >
					<input name="OrganInnerCode" type="hidden">

      </TD>  
-->      
			<TD class=title style="display: none">被保人自编号码</TD>
			<TD class=input style="display: none">
				<Input class=common name="InsuredID" >
				<Input class="codeno" name="AddressNo" type="hidden" ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this,AddressNoName],[0,1],'', '', '', true);" onkeyup="getaddresscodedata();return showCodeListKeyEx('GetAddressNo',[this,AddressNoName],[0,1],'', '', '', true);"><input class=codename name=AddressNoName readonly=true type="hidden">
			</TD>	
			<TD class=title style="display: none">工资（元）</TD>
			<TD class=input style="display: none">
			<Input name="Salary" class=common>
			</TD>
		  <TD class=title></TD>
			<TD class=input>
			
			<Input class=common name="GrpAddress" type="hidden"><!-- 单位地址 -->
			<Input class=common name="GrpZipCode" type="hidden"><!-- 单位邮编 -->
			<Input class=common name="GrpPhone" type="hidden"><!--  单位电话-->

			<Input name="HomeAddress" class=common type="hidden"><!-- 家庭地址 -->
			<Input name="HomeZipCode" class=common type="hidden"><!-- 家庭邮编 -->
			<Input name="HomePhone" class=common type="hidden"><!-- 家庭电话 -->
			<Input class="codeno" name="CheckPostalAddress" type="hidden" value='3' CodeData="0|^1|单位地址^2|家庭地址^3|其它"  ondblclick="return showCodeListEx('CheckPostalAddress',[this,CheckPostalAddressName],[0,1]);" onkeyup="return showCodeListKeyEx('CheckPostalAddress',[this,CheckPostalAddressName],[0,1]);"><input name=CheckPostalAddressName type="hidden" class=codename readonly=true><!--联系地址选择-->
			</TD>
			<TR class=common style="display: none">
				<TD class=title>联系地址</TD>
				<TD class=input>
					<Input class=common name="PostalAddress"  verify="被保险人联系地址|len<=80">
				</TD>
				<TD class=title>邮政编码</TD>
				<TD class=input>
					<Input class=common name="ZipCode"  MAXLENGTH="6" verify="被保险人邮政编码|zipcode">
				</TD>
				<TD class=title>联系电话</TD>
				<TD class=input>
					<input class=common name="Phone"  verify="被保险人家庭电话|len<=18">
				</TD>
			</TR>
				
			<TR class=common style="display: none">
				<TD class=title>移动电话</TD>
				<TD class=input>
					<Input class=common name="Mobile" verify="被保险人移动电话len<=15">
				</TD>
				<TD class=title>电子邮箱</TD>
				<TD class=input>
					<Input class=common name="EMail" verify="被保险人电子邮箱|len<=20&Email">
				</TD>	
				<TD class=title>退休和在职</TD>
				<TD class=input>
					<input class="codeno" name=RetireFlag CodeData="0|^0|在职^1|退休" onDblClick="showCodeListEx('RetireFlag',[this,RetireFlagName],[0,1]);" onKeyUp="showCodeListKeyEx('RetireFlag',[this,RetireFlagName],[0,1]);"><input class=codename name=RetireFlagName  readonly=true >            
				</TD>		
			</TR>
			
		<TR>
			<TD class=title style="display: none">生效日期</TD>
			<TD class=input style="display: none">
				<Input class="coolDatePicker" name=InsureDate dateFormat="short"  name=CValiDate verify="" >
			</TD>	
			<TD class=title style="display: none">终止日期</TD>
					<TD class=input style="display: none">
						<Input class="coolDatePicker" name=StopDate dateFormat="short" name=PolApplyDate verify="">
			</TD>	
			<TD class=title style="display: none">银行具体信息</TD>
				<TD class=input style="display: none">
					<Input class=common name=BankInfo>	
			</TD>
		</TR>	
		
	 <TR CLASS=common>
	  
	 
	    <TD class=title ></TD>
			<TD class=input >
				<Input class=code8 type=hidden >
			</TD>	
			<TD class=title ></TD>
			<TD class=input >
				<Input class=code8 type=hidden >
			</TD>	 
	  </TR>
	  
	  <TR class=common style="display: none">
				<TD  class= title> 开户行所在省 </TD>
	    <TD  class= input> <Input class= codeno name=BankPrv ondblclick="showCodeList('nativeplacebak',[this,BankPrvName],[0,1]);" onkeyup="showCodeListKey('nativeplacebak',[this,BankPrvName],[0,1]);"><input class=codename name=BankPrvName readonly=true></TD>
	    <TD  class= title> 开户行所在地（市） </TD>
	    <TD  class= input> <Input class= codeno name=BankCity ondblclick="showCodeList('provincecity',[this,BankCityName],[0,1],null,fm.BankPrv.value,'codealias','1');" onkeyup="showCodeListKey('provincecity',[this,BankCityName],[0,1],null,fm.BankPrv.value,'codealias','1');"><input class=codename name=BankCityName readonly=true></TD>
		<!--modify  by winnie PIR20092538 国籍 变更为 国家或地区 -->
		<TD class=title>国家或地区</TD>
		<TD class=input>
				<Input class="codeno" name=NativePlace  ondblclick="return showCodeList('nativeplace', [this,NativePlaceName],[0,1]);" onkeyup="return showCodeListKey('nativeplace', [this,NativePlaceName],[0,1]);" verify="国家或地区|code:nativeplace"><input class=codename name=NativePlaceName readonly=true >
		</TD>
		</TR>	  
	  	<TR class=common style="display: none">			
				<TD class=title>是否有医保</TD>
				<TD class=input>
					<input class="codeno" name=DoctorEnsure value=2 CodeData="0|^1|有医保^2|无医保" onDblClick="showCodeListEx('DoctorEnsure',[this,DoctorEnsureName],[0,1]);" onKeyUp="showCodeListKeyEx('DoctorEnsure',[this,DoctorEnsureName],[0,1]);"><input class=codename name=DoctorEnsureName  readonly=true >            
				</TD>	
				<TD class=title>特殊政策标记</TD>
				<TD class=input>
					<input class="codeno" name=EspecialTag value=2 CodeData="0|^1|有标记^2|无标记" onDblClick="showCodeListEx('EspecialTag',[this,EspecialTagName],[0,1]);" onKeyUp="showCodeListKeyEx('EspecialTag',[this,EspecialTagName],[0,1]);"><input class=codename readonly=true name=EspecialTagName>            
				</TD>	
				<TD class=title>理赔通知方式</TD>
				<TD class=input>
					<Input class="codeno" name=NoticeWay MAXLENGTH=1 ondblclick="return showCodeList('noticeway', [this,NoticeWayName],[0,1]);" onkeyup="return showCodeListKey('noticeway', [this,NoticeWayName],[0,1]);" verify="理赔通知方式|code:noticeway"><input class=codename name=NoticeWayName readonly=true >
				</TD>

			</TR>				
		</TABLE>
	</Div>
</DIV>
<TABLE class=common>
	<TR class=common >
			<DIV id="divContPlan" style="display:'none'">
				<TABLE class=common style="display: none">
					<TR class=common>
					<input name="OrganInnerCode" type="hidden">	
						

					<TD class=title id="LblCompanyName1">工作单位</TD>
      				<TD class=input id="LblCompanyName2">
          				<Input class="codeno" name="OrganComCode" readOnly type='text'  verify="" 
          					onchange="querySendORGComCode();" ondblclick="getORGComCode();" 
          					><input name=GrpName class=codename readonly=true  elementtype=nacessary 
          					varify="工作单位|notnull">
					</TD>
						<TD class=title id=ContPlanCode1 style="display:''">保障层级</TD>
						<TD class=input id=ContPlanCode2 style="display:''">
							<Input class="codeno" name="ContPlanCode" ondblclick="showCodeListEx('ContPlanCode',[this,ContPlanCodeName],[0,1],'', '', '', true);" onkeyup="showCodeListKeyEx('ContPlanCode',[this,ContPlanCodeName],[0,1],'', '', '', true);"><input name=ContPlanCodeName class=codename readonly=true>
						</TD>

					    <TD class=title id=PubAmnType1 style="display:'none'">公共保额属性</TD>
						<TD class=input id=PubAmnType2 style="display:'none'">
							<Input class="codeno" name="PubAmnType"  CodeData="0|^1|一般的公共保额^2|多险种共享的公共保额" ondblclick="showCodeListEx('PubAmnType',[this,PubAmnTypeName],[0,1],'', '', '', true);" onkeyup="showCodeListKeyEx('PubAmnTypeName',[this,PubAmnTypeName],[0,1],'', '', '', true);"><input name=PubAmnTypeName class=codename readonly=true>
						</TD>
		
						<TD class=title id=ContPlanCode3 style="display:''"></TD>
						<TD class=input id=ContPlanCode4 style="display:''"></TD>
						
						<TD class=title id=ContPlanCode5 style="display:'none'"></TD>
						<TD class=input id=ContPlanCode6 style="display:'none'"></TD>
						
						
					</TR>
				</TABLE>
			</DIV>
			<BR>
		<TD class=title>
			<DIV id="divExecuteCom" style="display:'none'">
				<TABLE class=common>
					<TR class=common>
						<TD class=title>处理机构</TD>
						<!-- modify by winnie ASR20093243 -->						
						<TD class=input>
							<Input class="codeno" name="ExecuteCom" ondblclick="showCodeListEx('ExecuteCom',[this,ExecuteComName],[0,1],'', '', '', true);" onkeyup="showCodeListKeyEx('ExecuteCom',[this,ExecuteComName],[0,1],'', '', '', true);"><input name=ExecuteComName class=codename readonly=true>
						</TD>
						<TD class=title />
						<TD class=input />
						<TD class=title />
						<TD class=input />
						<!-- end modify -->
					</TR>
				</TABLE>
			</DIV>
		</TD>
	</TR>
</TABLE>
	<Div id="DivAppointHospital" style="display: 'none'">
		<table class=common>
				<TR class=common>
          <TD class=title>医保指定医院</TD>
				  <TD class=input>
					 <input class=common name="AppointHospital">
				  </TD>
				<TD CLASS=title></TD>
				<TD class=input></TD>
				<TD CLASS=title></TD>
				<TD class=input></TD>			
				</TR>			
		</table>
	</Div>
	<Div id="DivEspecial" style="display: 'none'">
		<table class=common>
				<TR class=common>
					<TD class=title8>特殊政策标记原因(最多100个字)</TD>
				</TR>
				
				<TR class=common>
					<TD class=title8>
						<textarea name="TagReason" maxlength="100" class="common4"></textarea>
					</TD>
				</TR>			
		</table>
	</Div>
<DIV id=divShowJoinComPanyDate style="display:'none'">
	<table class=common>
		<TR class=common>
			<TD class=title>入职日期</TD>
			<TD class=input>
				<Input class="coolDatePicker" name=QJoinCompanyDate style="width:160"  dateFormat="short" >
			</TD>
			<TD class=title></TD>
			<TD class=input>
			</TD>
			<TD class=title></TD>
			<TD class=input>
			</TD>
		</TR>
	</table>
</DIV>
<DIV id=divSalary style="display:''">
	
</DIV>
<DIV id=divLCInsuredPerson style="display:'none'">
	<table class=common>
		<TR class=common>
			<TD class=title>家庭传真</TD>
			<TD class=input>
				<Input name="HomeFax">
			</TD>
			<TD class=title>单位传真</TD>
			<TD class=input>
				<Input name="GrpFax" class=common>
			</TD>
			<TD class=title>联系传真</TD>
			<TD class=input>
				<Input name="Fax" class=common>
			</TD>
		</TR>
	</table>
</DIV>