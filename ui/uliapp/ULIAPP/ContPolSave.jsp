<%--
    保存集体保单信息 2004-11-16 wzw
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.ulitb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%	         
	System.out.println("...................save here0:");
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = new LCCustomerImpartDetailSet();
  //团体告知信息
  String tImpartNum[] = request.getParameterValues("ImpartGridNo");
	String tImpartVer[] = request.getParameterValues("ImpartGrid1");            //告知版别
	String tImpartCode[] = request.getParameterValues("ImpartGrid2");           //告知编码
	String tImpartContent[] = request.getParameterValues("ImpartGrid3");        //告知内容
	String tImpartParamModle[] = request.getParameterValues("ImpartGrid4"); 
  //基本告知的详细告知
  String tImpartDetailNum2[] = request.getParameterValues("BaseImpartDetailGridNo");
	String tImpartDetailVer2[] = request.getParameterValues("BaseImpartDetailGrid1");            //告知版别
	String tImpartDetailCode2[] = request.getParameterValues("BaseImpartDetailGrid2");           //告知编码
	String tImpartDetailContent2[] = request.getParameterValues("BaseImpartDetailGrid3");        //告知内容
	String tImpartDetailDiseaseContent2[] = request.getParameterValues("BaseImpartDetailGrid4");  
	String tImpartDetailStartDate2[] = request.getParameterValues("BaseImpartDetailGrid5");      
	String tImpartDetailEndDate2[] = request.getParameterValues("BaseImpartDetailGrid6");
	String tImpartDetailProver2[] = request.getParameterValues("BaseImpartDetailGrid7");    
	String tImpartDetailCurrCondition2[] = request.getParameterValues("BaseImpartDetailGrid8");  
	String tImpartDetailIsProved2[] = request.getParameterValues("BaseImpartDetailGrid9");  		        		        		
	int ImpartDetailCount2 = 0;
	if (tImpartDetailNum2 != null) ImpartDetailCount2 = tImpartDetailNum2.length;
		for (int i = 0; i < ImpartDetailCount2; i++)	{
	            LCCustomerImpartDetailSchema tLCCustomerImpartDetailSchema = new LCCustomerImpartDetailSchema();						
				tLCCustomerImpartDetailSchema.setCustomerNoType("0");
				tLCCustomerImpartDetailSchema.setImpartVer(tImpartDetailVer2[i]) ;				
				tLCCustomerImpartDetailSchema.setImpartCode(tImpartDetailCode2[i]);
				tLCCustomerImpartDetailSchema.setImpartDetailContent(tImpartDetailContent2[i]);
				tLCCustomerImpartDetailSchema.setDiseaseContent(tImpartDetailDiseaseContent2[i]);
				tLCCustomerImpartDetailSchema.setStartDate(tImpartDetailStartDate2[i]);
				tLCCustomerImpartDetailSchema.setEndDate(tImpartDetailEndDate2[i]);
				tLCCustomerImpartDetailSchema.setProver(tImpartDetailProver2[i]);
				tLCCustomerImpartDetailSchema.setCurrCondition(tImpartDetailCurrCondition2[i]);								
				tLCCustomerImpartDetailSchema.setIsProved(tImpartDetailIsProved2[i]);					
				tLCCustomerImpartDetailSet.add(tLCCustomerImpartDetailSchema);
		}
  //健康告知的详细告知
  String tImpartDetailNum1[] = request.getParameterValues("HealthImpartDetailGridNo");
	String tImpartDetailVer1[] = request.getParameterValues("HealthImpartDetailGrid1");            //告知版别
	String tImpartDetailCode1[] = request.getParameterValues("HealthImpartDetailGrid2");           //告知编码
	String tImpartDetailContent1[] = request.getParameterValues("HealthImpartDetailGrid3");        //告知内容
	String tImpartDetailDiseaseContent1[] = request.getParameterValues("HealthImpartDetailGrid4");  
	String tImpartDetailStartDate1[] = request.getParameterValues("HealthImpartDetailGrid5");      
	String tImpartDetailEndDate1[] = request.getParameterValues("HealthImpartDetailGrid6");
	String tImpartDetailProver1[] = request.getParameterValues("HealthImpartDetailGrid7");    
	String tImpartDetailCurrCondition1[] = request.getParameterValues("HealthImpartDetailGrid8");  
	String tImpartDetailIsProved1[] = request.getParameterValues("HealthImpartDetailGrid9");  		        		        		
	System.out.println("...................save here2:");
	int ImpartDetailCount1 = 0;
	if (tImpartDetailNum1 != null) ImpartDetailCount1 = tImpartDetailNum1.length;
		for (int i = 0; i < ImpartDetailCount1; i++)	{
	            LCCustomerImpartDetailSchema tLCCustomerImpartDetailSchema = new LCCustomerImpartDetailSchema();						
				tLCCustomerImpartDetailSchema.setCustomerNoType("0");
				tLCCustomerImpartDetailSchema.setImpartVer(tImpartDetailVer1[i]) ;				
				tLCCustomerImpartDetailSchema.setImpartCode(tImpartDetailCode1[i]);
				tLCCustomerImpartDetailSchema.setImpartDetailContent(tImpartDetailContent1[i]);
				tLCCustomerImpartDetailSchema.setDiseaseContent(tImpartDetailDiseaseContent1[i]);
				tLCCustomerImpartDetailSchema.setStartDate(tImpartDetailStartDate1[i]);
				tLCCustomerImpartDetailSchema.setEndDate(tImpartDetailEndDate1[i]);
				tLCCustomerImpartDetailSchema.setProver(tImpartDetailProver1[i]);
				tLCCustomerImpartDetailSchema.setCurrCondition(tImpartDetailCurrCondition1[i]);								
				tLCCustomerImpartDetailSchema.setIsProved(tImpartDetailIsProved1[i]);					
				tLCCustomerImpartDetailSet.add(tLCCustomerImpartDetailSchema);
		}
  //既往告知
  String tImpartNum2[] = request.getParameterValues("HistoryImpartGridNo");
	String tInsuStartYear[] = request.getParameterValues("HistoryImpartGrid1");
	String tInsuEndYear[] = request.getParameterValues("HistoryImpartGrid2");
	String tInsuContent[] = request.getParameterValues("HistoryImpartGrid3");           //告知编码
	String tRate[] = request.getParameterValues("HistoryImpartGrid4");        //告知内容
	String tEnsureContent[] = request.getParameterValues("HistoryImpartGrid5");
	String tPeoples[] = request.getParameterValues("HistoryImpartGrid6");	 
	String tRecompensePeoples[] = request.getParameterValues("HistoryImpartGrid7"); 
	String tOccurMoney[] = request.getParameterValues("HistoryImpartGrid8"); 
	String tRecompenseMoney[] = request.getParameterValues("HistoryImpartGrid9"); 
	String tPendingMoney[] = request.getParameterValues("HistoryImpartGrid10");
	String tSerialNo1[] = request.getParameterValues("HistoryImpartGrid11"); 
	//新历史保障情况告知
	String tImpartNum4[] = request.getParameterValues("NewHistoryImpartGridNo");
	String tInsuYear[] = request.getParameterValues("NewHistoryImpartGrid1");
	String tInsuContent2[] = request.getParameterValues("NewHistoryImpartGrid2");           //告知编码
	String tEnsureContent2[] = request.getParameterValues("NewHistoryImpartGrid3");
	String tRate2[] = request.getParameterValues("NewHistoryImpartGrid4");        //告知内容
	String tPeoples2[] = request.getParameterValues("NewHistoryImpartGrid5");	 
	String tRecompensePeoples2[] = request.getParameterValues("NewHistoryImpartGrid6"); 
	String tOccurMoney2[] = request.getParameterValues("NewHistoryImpartGrid7"); 
	String tRecompenseMoney2[] = request.getParameterValues("NewHistoryImpartGrid8"); 
	String tPendingMoney2[] = request.getParameterValues("NewHistoryImpartGrid9");
	String tSerialNo21[] = request.getParameterValues("NewHistoryImpartGrid10");
	//严重疾病情况告知
	String tImpartNum3[] = request.getParameterValues("DiseaseGridNo");
	String tOcurTime[] = request.getParameterValues("DiseaseGrid1");
	String tDiseaseName[] = request.getParameterValues("DiseaseGrid2");            //告知版别
	String tDiseasePepoles[] = request.getParameterValues("DiseaseGrid3");           //告知编码
	String tCureMoney[] = request.getParameterValues("DiseaseGrid4");        //告知内容
	String tRemark[] = request.getParameterValues("DiseaseGrid5");
	String tSerialNo2[] = request.getParameterValues("DiseaseGrid6"); 
	//客户需求服务
	String tServInfoGridNum[] = request.getParameterValues("ServInfoGridNo");
	String tServKind[] = request.getParameterValues("ServInfoGrid1");
	System.out.println("客户服务类型 :"+tServKind);
	String tServDetail[] = request.getParameterValues("ServInfoGrid3");            //告知版别
	String tServChoose[] = request.getParameterValues("ServInfoGrid6");           //告知编码
	String tServRemark[] = request.getParameterValues("ServInfoGrid7"); 	
	
	//约定缴费方式
	String tBookingPayIntyNum[] = request.getParameterValues("BookingPayIntyGridNo");
	String tBookingDate[] = request.getParameterValues("BookingPayIntyGrid1");
	String tBookingMoney[] = request.getParameterValues("BookingPayIntyGrid2");            //告知版别

  VData tVData = new VData();
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();      //集体保单
  LCGrpAppntSchema tLCGrpAppntSchema = new LCGrpAppntSchema();   //团单投保人
  LDGrpSchema tLDGrpSchema   = new LDGrpSchema();                //团体客户
  LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema(); //团体客户地址
  LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();	//客户告知
  LCHistoryImpartSet tLCHistoryImpartSet = new LCHistoryImpartSet(); //既往告知          //既往告知
  LCDiseaseImpartSet tLCDiseaseImpartSet = new LCDiseaseImpartSet(); //严重疾病告知
  LCGrpServInfoSet tLCGrpServInfoSet = new LCGrpServInfoSet();     //客户服务信息		 
  GlobalInput tG = new GlobalInput();
  LCGrpSpecFeeSet tLCGrpSpecFeeSet = new LCGrpSpecFeeSet();
  tG=(GlobalInput)session.getValue("GI");
  
  tAction = request.getParameter( "fmAction" );
            //集体保单信息  LCGrpCont
	    tLCGrpContSchema.setProposalGrpContNo(request.getParameter("ProposalGrpContNo"));  //集体投保单号码
	    tLCGrpContSchema.setGrpContNo(request.getParameter("GrpContNo"));
	    tLCGrpContSchema.setPrtNo(request.getParameter("PrtNo"));                  //印刷号码
	    tLCGrpContSchema.setManageCom(request.getParameter("ManageCom"));          //管理机构
	    tLCGrpContSchema.setSaleChnl(request.getParameter("SaleChnl"));            //销售渠道
	    tLCGrpContSchema.setSaleChnlDetail(request.getParameter("SaleChnlDetail")); 
	    tLCGrpContSchema.setAgentCom(request.getParameter("AgentCom"));            //代理机构
	    tLCGrpContSchema.setAgentType(request.getParameter("AgentType"));           //代理机构分类
	    tLCGrpContSchema.setAgentCode(request.getParameter("AgentCode"));          //代理人编码
	    System.out.println("代理人编码ewe :"+request.getParameter("AgentCode"));	    
	    tLCGrpContSchema.setAgentGroup(request.getParameter("AgentGroup"));        //代理人组别
	    tLCGrpContSchema.setAgentCode1(request.getParameter("AgentCode1"));        //联合代理人代码
	    tLCGrpContSchema.setAgentSaleCode(request.getParameter("AgentSaleCode"));  //代理销售人员编码
	    tLCGrpContSchema.setGrpSpec(request.getParameter("GrpSpec"));              //集体特约
	    tLCGrpContSchema.setAppntNo(request.getParameter("GrpNo"));                //客户号码
	    tLCGrpContSchema.setPayMode(request.getParameter("PayMode"));                // 缴费方式
	    //tLCGrpContSchema.setAddressNo(request.getParameter("GrpAddressNo"));       //地址号码
	    tLCGrpContSchema.setGrpName(request.getParameter("GrpName"));              //单位名称
	    tLCGrpContSchema.setGetFlag(request.getParameter("GetFlag"));              //付款方式
	    tLCGrpContSchema.setPayIntv(request.getParameter("GrpContPayIntv")); 	    
	    tLCGrpContSchema.setBankCode(request.getParameter("BankCode"));            //银行编码
	    tLCGrpContSchema.setBankAccNo(request.getParameter("BankAccNo"));          //银行帐号
	    tLCGrpContSchema.setAccName(request.getParameter("AccName"));          //银行帐号	    
	    tLCGrpContSchema.setCurrency(request.getParameter("Currency"));            //币别
	    String CValiDate="";
	    if(StrTool.cTrim(request.getParameter("CValiDate")).equals("")){
	    	CValiDate = PubFun.getCurrentDate();
	    }else{
	    	CValiDate = request.getParameter("CValiDate");
	    }
	    tLCGrpContSchema.setCValiDate(CValiDate);          //保单生效日期
	    tLCGrpContSchema.setPolApplyDate(request.getParameter("HandlerDate"));    //保单投保日期
	    tLCGrpContSchema.setOutPayFlag(request.getParameter("OutPayFlag"));        //溢交处理方式
	    tLCGrpContSchema.setEnterKind(request.getParameter("EnterKind"));          //参保形式
	    tLCGrpContSchema.setAmntGrade(request.getParameter("AmntGrade"));          //保额等级
	    tLCGrpContSchema.setPeoples3(request.getParameter("Peoples3"));		//单位可投保人数
        tLCGrpContSchema.setOnWorkPeoples(request.getParameter("OnWorkPeoples"));		//单位可投保人数	    
        tLCGrpContSchema.setOffWorkPeoples(request.getParameter("OffWorkPeoples"));		//单位可投保人数	    
        tLCGrpContSchema.setOtherPeoples(request.getParameter("OtherPeoples"));		//单位可投保人数
        tLCGrpContSchema.setRelaPeoples(request.getParameter("RelaPeoples"));		//连带被保人人数
        tLCGrpContSchema.setRelaMatePeoples(request.getParameter("RelaMatePeoples"));		//配偶人数	    
        tLCGrpContSchema.setRelaYoungPeoples(request.getParameter("RelaYoungPeoples"));		//子女人数
        tLCGrpContSchema.setRelaOtherPeoples(request.getParameter("RelaOtherPeoples"));		//连带其他人员数
        tLCGrpContSchema.setFirstTrialOperator(request.getParameter("FirstTrialOperator"));
        tLCGrpContSchema.setReceiveDate(request.getParameter("ReceiveDate"));
        tLCGrpContSchema.setPrem(request.getParameter("Prem"));	
        tLCGrpContSchema.setAmnt(request.getParameter("Amnt"));	
         
       tLCGrpContSchema.setTempFeeNo(request.getParameter("TempFeeNo"));
       tLCGrpContSchema.setBusinessBigType(request.getParameter("BusinessBigType"));
       
       // 共保标志
       tLCGrpContSchema.setCoInsuranceFlag(request.getParameter("CoInsuranceFlag"));
       // ----------------------------
       //tLCGrpContSchema.setCTCount(request.getParameter("CTCount"));
       //tLCGrpContSchema.setCityInfo(request.getParameter("CityInfo"));
       
       // 集团交叉业务标识。
       tLCGrpContSchema.setCrs_SaleChnl(request.getParameter("Crs_SaleChnl"));
       tLCGrpContSchema.setCrs_BussType(request.getParameter("Crs_BussType"));
       // --------------------
               
                                	    
	    tLCGrpContSchema.setGrpNature(request.getParameter("GrpNature"));         //单位性质
	    tLCGrpContSchema.setBusinessType(request.getParameter("BusinessType"));   //行业类别
	    tLCGrpContSchema.setPeoples(request.getParameter("Peoples"));             //总人数
	    tLCGrpContSchema.setRgtMoney(request.getParameter("RgtMoney"));           //注册资本
	    tLCGrpContSchema.setAsset(request.getParameter("Asset"));                 //资产总额
	    tLCGrpContSchema.setNetProfitRate(request.getParameter("NetProfitRate")); //净资产收益率
	    tLCGrpContSchema.setMainBussiness(request.getParameter("MainBussiness")); //主营业务
	    tLCGrpContSchema.setCorporation(request.getParameter("Corporation"));     //法人
	    tLCGrpContSchema.setComAera(request.getParameter("ComAera"));             //机构分布区域
	    tLCGrpContSchema.setPhone(request.getParameter("Phone"));             		//总机
	    tLCGrpContSchema.setFax(request.getParameter("Fax"));             				//传真
	    tLCGrpContSchema.setFoundDate(request.getParameter("FoundDate"));         //成立时间
	    tLCGrpContSchema.setRemark(request.getParameter("Remark"));			//备注	    
	    tLCGrpContSchema.setHandlerName(request.getParameter("HandlerName"));	
	    tLCGrpContSchema.setHandlerDate(request.getParameter("HandlerDate"));	
	    tLCGrpContSchema.setHandlerPrint(request.getParameter("HandlerPrint"));	
	    tLCGrpContSchema.setAgentDate(request.getParameter("AgentDate"));	
	    tLCGrpContSchema.setMarketType(request.getParameter("MarketType"));	     //市场类型
	    tLCGrpContSchema.setCInValiDate(request.getParameter("CInValiDate"));	
	    tLCGrpContSchema.setAskGrpContNo(request.getParameter("AskGrpContNo"));	
	    tLCGrpContSchema.setPremScope(request.getParameter("GrpContSumPrem"));
	    tLCGrpContSchema.setBigProjectFlag(request.getParameter("BigProjectFlag"));	  //大项目标识      
	    tLCGrpContSchema.setContPrintType(request.getParameter("ContPrintType"));	  //保单打印类型
	    tLCGrpContSchema.setGrpAgentCom(request.getParameter("GrpAgentCom"));
	    tLCGrpContSchema.setGrpAgentIDNo(request.getParameter("GrpAgentIDNo"));
	    tLCGrpContSchema.setGrpAgentCode(request.getParameter("GrpAgentCode"));
	    tLCGrpContSchema.setGrpAgentName(request.getParameter("GrpAgentName"));
	    String tInputOperator=request.getParameter("InputOperator");
	    System.out.println("进入了!"+tG.Operator);
			if(StrTool.cTrim(tInputOperator).equals(""))
			{
	    tLCGrpContSchema.setInputOperator(tG.Operator); 
	  	}else
	  	{
	  	tLCGrpContSchema.setInputOperator(request.getParameter("InputOperator")); 
	  	}
	  	tLCGrpContSchema.setHandlerIDNo(request.getParameter("HandlerIDNo"));//业务经办人身份证号
	  	//System.out.println("InputOperator"+tInputOperator);
	    //团单投保人信息  LCGrpAppnt
	    tLCGrpAppntSchema.setGrpContNo(request.getParameter("ProposalGrpContNo"));     //集体投保单号码
	    tLCGrpAppntSchema.setCustomerNo(request.getParameter("GrpNo"));                //客户号码
	    tLCGrpAppntSchema.setPrtNo(request.getParameter("PrtNo"));                 	   //印刷号码
	    tLCGrpAppntSchema.setName(request.getParameter("GrpName"));
	    tLCGrpAppntSchema.setOrganComCode(request.getParameter("OrgancomCode"));   	   //组织机构
	    tLCGrpAppntSchema.setPostalAddress(request.getParameter("GrpAddress"));
	    tLCGrpAppntSchema.setZipCode(request.getParameter("GrpZipCode"));
	    //tLCGrpAppntSchema.setAddressNo(request.getParameter("GrpAddressNo"));
	    tLCGrpAppntSchema.setPhone(request.getParameter("Phone"));
	    //反洗钱新增
	    tLCGrpAppntSchema.setTaxNo(request.getParameter("TaxNo"));
	    tLCGrpAppntSchema.setLegalPersonName(request.getParameter("LegalPersonName"));
	    tLCGrpAppntSchema.setLegalPersonIDNo(request.getParameter("LegalPersonIDNo"));
        
        // 在团单投保人信息层，加入人员相关基本信息的采集。
        tLCGrpAppntSchema.setPeoples(request.getParameter("Peoples"));
        tLCGrpAppntSchema.setOnWorkPeoples(request.getParameter("AppntOnWorkPeoples"));
        tLCGrpAppntSchema.setOffWorkPeoples(request.getParameter("AppntOffWorkPeoples"));
        tLCGrpAppntSchema.setOtherPeoples(request.getParameter("AppntOtherPeoples"));
        // --------------------
        
	    //团体客户信息  LDGrp
	    tLDGrpSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	    tLDGrpSchema.setGrpName(request.getParameter("GrpName"));             //单位名称
	    tLDGrpSchema.setOrganComCode(request.getParameter("OrgancomCode"));   //组织机构
	    tLDGrpSchema.setGrpNature(request.getParameter("GrpNature"));         //单位性质
	    tLDGrpSchema.setBusinessType(request.getParameter("BusinessType"));   //行业类别
	    tLDGrpSchema.setPeoples(request.getParameter("Peoples"));             //总人数
	    tLDGrpSchema.setOnWorkPeoples(request.getParameter("AppntOnWorkPeoples"));             //总人数
	    tLDGrpSchema.setOffWorkPeoples(request.getParameter("AppntOffWorkPeoples"));             //总人数
	    tLDGrpSchema.setOtherPeoples(request.getParameter("AppntOtherPeoples"));             //总人数	    	    	    
	    tLDGrpSchema.setRgtMoney(request.getParameter("RgtMoney"));           //注册资本
	    tLDGrpSchema.setAsset(request.getParameter("Asset"));                 //资产总额
	    tLDGrpSchema.setNetProfitRate(request.getParameter("NetProfitRate")); //净资产收益率
	    tLDGrpSchema.setMainBussiness(request.getParameter("MainBussiness")); //主营业务
	    tLDGrpSchema.setCorporation(request.getParameter("Corporation"));     //法人
	    tLDGrpSchema.setComAera(request.getParameter("ComAera"));             //机构分布区域
	    tLDGrpSchema.setPhone(request.getParameter("Phone"));             //总机
	    tLDGrpSchema.setFax(request.getParameter("Fax"));             //传真
	    tLDGrpSchema.setFoundDate(request.getParameter("FoundDate"));             //成立时间
	    tLDGrpSchema.setGrpAppntNum(request.getParameter("GrpAppNum"));
	    //团体客户地址  LCGrpAddress	    
	    tLCGrpAddressSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	    //tLCGrpAddressSchema.setAddressNo(request.getParameter("GrpAddressNo"));      //地址号码
	    tLCGrpAddressSchema.setGrpAddress(request.getParameter("GrpAddress"));       //单位地址
	    System.out.println("*******************"+request.getParameter("GrpAddress"));
	    tLCGrpAddressSchema.setGrpZipCode(request.getParameter("GrpZipCode"));       //单位邮编
	    //保险联系人一
	    tLCGrpAddressSchema.setLinkMan1(request.getParameter("LinkMan1"));
	    tLCGrpAddressSchema.setDepartment1(request.getParameter("Department1"));
	    tLCGrpAddressSchema.setHeadShip1(request.getParameter("HeadShip1"));
	    tLCGrpAddressSchema.setPhone1(request.getParameter("Phone1"));
	    tLCGrpAddressSchema.setE_Mail1(request.getParameter("E_Mail1"));
	    tLCGrpAddressSchema.setFax1(request.getParameter("Fax1"));
	    //保险联系人二
	    tLCGrpAddressSchema.setLinkMan2(request.getParameter("LinkMan2"));
	    tLCGrpAddressSchema.setDepartment2(request.getParameter("Department2"));
	    tLCGrpAddressSchema.setHeadShip2(request.getParameter("HeadShip2"));
	    tLCGrpAddressSchema.setPhone2(request.getParameter("Phone2"));
	    tLCGrpAddressSchema.setE_Mail2(request.getParameter("E_Mail2"));
	    tLCGrpAddressSchema.setFax2(request.getParameter("Fax2"));
			//客户告知
			int ImpartCount = 0;
			if (tImpartNum != null) ImpartCount = tImpartNum.length;
	        
			for (int i = 0; i < ImpartCount; i++)	
			{
			LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
			  tLCCustomerImpartSchema.setProposalContNo(request.getParameter("ContNo"));
			  	tLCCustomerImpartSchema.setGrpContNo(request.getParameter("GrpContNo"));
				tLCCustomerImpartSchema.setCustomerNo(tLDGrpSchema.getCustomerNo());
				tLCCustomerImpartSchema.setCustomerNoType("0");
				tLCCustomerImpartSchema.setImpartCode(tImpartCode[i]);
				tLCCustomerImpartSchema.setImpartContent(tImpartContent[i]);
				tLCCustomerImpartSchema.setImpartParamModle(tImpartParamModle[i]);
				System.out.println(tImpartParamModle[i]+"=====tImpartParamModle[i]");
				tLCCustomerImpartSchema.setImpartVer(tImpartVer[i]) ;
				tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
			}
			//既往情况告知
	    int ImpartCont2 = 0;
	    if (tImpartNum2 != null) ImpartCont2 = tImpartNum2.length;
	    				//System.out.println("三大法阿飞诉讼"+ImpartCont2);    
	    			for (int i = 0; i < ImpartCont2; i++)	
			{	    				//System.out.println("三大法阿飞诉讼琐琐碎碎ssss");    
			LCHistoryImpartSchema tLCHistoryImpartSchema = new LCHistoryImpartSchema();
			  //tLCHistoryImpartSchema.setPrtNo(request.getParameter("PrtNo"));
				//tLCHistoryImpartSchema.setGrpContNo(request.getParameter("GrpContNo"));
				//tLCHistoryImpartSchema.setSerialNo(request.getParameter("SerialNo"));
                                                                                                               
				tLCHistoryImpartSchema.setInsuContent(tInsuContent[i]);                                                             
				tLCHistoryImpartSchema.setRate(tRate[i]) ;                                                             
				tLCHistoryImpartSchema.setEnsureContent(tEnsureContent[i]) ;                                                             
				tLCHistoryImpartSchema.setPeoples(tPeoples[i]) ;                                                             
				tLCHistoryImpartSchema.setRecompensePeoples(tRecompensePeoples[i]) ;                                                     
				tLCHistoryImpartSchema.setOccurMoney(tOccurMoney[i]) ;                                                             
				tLCHistoryImpartSchema.setRecompenseMoney(tRecompenseMoney[i]) ;                                                         
				tLCHistoryImpartSchema.setPendingMoney(tPendingMoney[i]) ;
				tLCHistoryImpartSchema.setSerialNo(tSerialNo1[i]) ;	
				if(!tInsuStartYear[i].equals("")||tInsuStartYear[i]!=null)
				{
					tInsuStartYear[i] = tInsuStartYear[i] + "-01";
				}
				if(!tInsuEndYear[i].equals("")||tInsuEndYear[i]!=null)
				{
					tInsuEndYear[i] = tInsuEndYear[i] + "-01";
				}
				tLCHistoryImpartSchema.setInsuStartYear(tInsuStartYear[i]);
				tLCHistoryImpartSchema.setInsuEndYear(tInsuEndYear[i]);
				
				tLCHistoryImpartSet.add(tLCHistoryImpartSchema);
			}
			//新既往情况告知
		int ImpartCont4 = 0;
		if (tImpartNum4 != null) ImpartCont4 = tImpartNum4.length;
	    				//System.out.println("三大法阿飞诉讼"+ImpartCont2);    
	    			for (int i = 0; i < ImpartCont4; i++)	
			{	    				//System.out.println("三大法阿飞诉讼琐琐碎碎ssss");    
			LCHistoryImpartSchema tLCHistoryImpartSchema = new LCHistoryImpartSchema();
			  //tLCHistoryImpartSchema.setPrtNo(request.getParameter("PrtNo"));
				//tLCHistoryImpartSchema.setGrpContNo(request.getParameter("GrpContNo"));
				//tLCHistoryImpartSchema.setSerialNo(request.getParameter("SerialNo"));
                                                                                                               
				tLCHistoryImpartSchema.setInsuContent(tInsuContent2[i]);                                                             
				tLCHistoryImpartSchema.setRate(tRate2[i]) ;                                                             
				tLCHistoryImpartSchema.setEnsureContent(tEnsureContent2[i]) ;                                                             
				tLCHistoryImpartSchema.setPeoples(tPeoples2[i]) ;                                                             
				tLCHistoryImpartSchema.setRecompensePeoples(tRecompensePeoples2[i]) ;                                                     
				tLCHistoryImpartSchema.setOccurMoney(tOccurMoney2[i]) ;                                                             
				tLCHistoryImpartSchema.setRecompenseMoney(tRecompenseMoney2[i]) ;                                                         
				tLCHistoryImpartSchema.setPendingMoney(tPendingMoney2[i]) ;
				tLCHistoryImpartSchema.setSerialNo(tSerialNo21[i]) ;	
				tLCHistoryImpartSchema.setInsuYear(tInsuYear[i]);
				tLCHistoryImpartSet.add(tLCHistoryImpartSchema);
			}
			//严重疾病情况告知
	    int ImpartCont3 = 0;
	    if (tImpartNum3 != null) ImpartCont3 = tImpartNum3.length;
	     
	    for (int i = 0; i < ImpartCont3; i++)	
			{  //System.out.println("三大法阿飞诉讼"+tOcurTime[i]);  
			  LCDiseaseImpartSchema tLCDiseaseImpartSchema = new LCDiseaseImpartSchema();
 				tLCDiseaseImpartSchema.setOcurTime(tOcurTime[i]);                                                             
				tLCDiseaseImpartSchema.setDiseaseName(tDiseaseName[i]);                                                             
				tLCDiseaseImpartSchema.setDiseasePepoles(tDiseasePepoles[i]) ;                                                             
				tLCDiseaseImpartSchema.setCureMoney(tCureMoney[i]) ;                                                             
				tLCDiseaseImpartSchema.setRemark(tRemark[i]) ; 
				tLCDiseaseImpartSchema.setSerialNo(tSerialNo2[i]) ;	                                                            
				
				tLCDiseaseImpartSet.add(tLCDiseaseImpartSchema);
			}


		int m = 0;
	    if (tServKind != null) m = tServKind.length;
	     System.out.println("----开始客户服务信息----");
	     System.out.println("Mulline 行数 ："+m);
	    for (int i = 0; i < m; i++)	
			{  
			   LCGrpServInfoSchema tLCGrpServInfoSchema = new LCGrpServInfoSchema();
 				//存放Mulline中的数据
 				tLCGrpServInfoSchema.setServKind(tServKind[i]);                                                             
				tLCGrpServInfoSchema.setServDetail(tServDetail[i]);                                                             
				tLCGrpServInfoSchema.setServChoose(tServChoose[i]) ;                                                             
				tLCGrpServInfoSchema.setServRemark(tServRemark[i]) ;  
				//从页面上获取的数据
				tLCGrpServInfoSchema.setPrtNo(request.getParameter("PrtNo"));
				//其他数据从后台获取
				tLCGrpServInfoSet.add(tLCGrpServInfoSchema);
			}
			
	int Booking = 0;
	    if (tBookingPayIntyNum != null) Booking = tBookingPayIntyNum.length;

	    for (int i = 0; i < Booking ; i++)	
			{  
			    //将约定缴费信息保存到团单约定缴费表中
			    LCGrpSpecFeeSchema tLCGrpSpecFeeSchema = new LCGrpSpecFeeSchema();
			    tLCGrpSpecFeeSchema.setGrpContNo(request.getParameter("GrpContNo"));
			    tLCGrpSpecFeeSchema.setPeriodNo(String.valueOf(i+1));     //约定缴费期次
			    tLCGrpSpecFeeSchema.setPayToDate(tBookingDate[i]);      //约定缴费日期
			    tLCGrpSpecFeeSchema.setPayToAmount(tBookingMoney[i]);   //约定缴费金额
			    tLCGrpSpecFeeSchema.setPayState("0");                   //约定缴费状态
			    System.out.println("tBookingDate[i]="+tBookingDate[i]+"  tBookingMoney[i]="+tBookingMoney[i]);
				tLCGrpSpecFeeSet.add(tLCGrpSpecFeeSchema);
			}
			
	    //by zlx 20130408 增加对综合开拓数据的处理
	  	String tAssistSaleChnl = request.getParameter("AssistSaleChnl");
	  	String tAssistAgentCode = request.getParameter("AssistAgentCode");
		LCExtendSchema tLCExtendSchema = new LCExtendSchema();
		tLCExtendSchema.setPrtNo(request.getParameter("PrtNo"));
		tLCExtendSchema.setAssistSalechnl(tAssistSaleChnl);
		tLCExtendSchema.setAssistAgentCode(tAssistAgentCode);
		tVData.add(tLCExtendSchema);
	  
  //by zlx 20130408 增加对综合开拓数据的处理 end
  System.out.println("end setSchema:");
  // 准备传输数据 VData
  tVData.add( tLCGrpContSchema );
  tVData.add( tLCGrpAppntSchema );
  tVData.add( tLDGrpSchema );
  tVData.add( tLCGrpAddressSchema );
  tVData.add( tLCCustomerImpartSet );   //团体告知信息
  tVData.add( tLCHistoryImpartSet );			//既往告知
  tVData.add( tLCDiseaseImpartSet );			//严重疾病告知
  tVData.add( tLCGrpServInfoSet );
  tVData.add( tLCGrpSpecFeeSet );
  tVData.add( tLCCustomerImpartDetailSet );
  System.out.println("tVData"+tVData);
  tVData.add( tG );

  	
  //传递非SCHEMA信息
  TransferData tTransferData = new TransferData();
  String SavePolType="";
  String BQFlag=request.getParameter("BQFlag");
  if(BQFlag==null) SavePolType="0";
  else if(BQFlag.equals("")) SavePolType="0";
  else  SavePolType=BQFlag;
  
  //传递LoadFlag
  mLoadFlag=request.getParameter("LoadFlag"); 
  tTransferData.setNameAndValue("LoadFlag",mLoadFlag);
  System.out.println("LoadFlag is : " + request.getParameter("LoadFlag"));

  
  tTransferData.setNameAndValue("SavePolType",SavePolType); //保全保存标记，默认为0，标识非保全
  System.out.println("SavePolType，BQ is 2，other is 0 : " + request.getParameter("BQFlag"));
  tVData.addElement(tTransferData);
  
  
  
	if( tAction.equals( "INSERT" )) tOperate = "INSERT||GROUPPOL";
	if( tAction.equals( "UPDATE" )) tOperate = "UPDATE||GROUPPOL";
	if( tAction.equals( "DELETE" )) tOperate = "DELETE||GROUPPOL";
	GroupContUI tGroupContUI = new GroupContUI();
	if( tGroupContUI.submitData( tVData, tOperate ) == false )
	{
		Content = " 保存失败，原因是: " + tGroupContUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";

		tVData.clear();
		tVData = tGroupContUI.getResult();

		// 显示
		if(( tAction.equals( "INSERT" ))||( tAction.equals( "UPDATE" )))
		{
			// 保单信息
			LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); 
			mLCGrpContSchema.setSchema(( LCGrpContSchema )tVData.getObjectByObjectName( "LCGrpContSchema", 0 ));
			%>
			   <script language="javascript">
			    	parent.fraInterface.fm.all("ProposalGrpContNo").value = "<%=mLCGrpContSchema.getProposalGrpContNo()%>";
			    	parent.fraInterface.fm.all("GrpContNo").value = "<%=mLCGrpContSchema.getGrpContNo()%>";
			     parent.fraInterface.fm.all("GrpNo").value = "<%=mLCGrpContSchema.getAppntNo()%>";
			     //parent.fraInterface.fm.all("GrpAddressNo").value = "<%=mLCGrpContSchema.getAddressNo()%>"; 
			         	
			   </script>
			<%		
		}
		else
		{
			%>
			   <script language="javascript">
			     parent.fraInterface.emptyFormElements();
			    </script>
			<%
		
		}
	}
        System.out.println("Content:"+Content);	

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>