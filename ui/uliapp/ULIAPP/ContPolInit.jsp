<%
//程序名称：GroupPolInput.jsp
//程序功能：
//创建日期：2002-08-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ProposalGrpContNo').value = '';
    fm.all('GrpContNo').value = '';
    fm.all('PrtNo').value = prtNo;
    fm.all('ManageCom').value = ManageCom;
    fm.all('MissionID').value = MissionID;
    fm.all('SubMissionID').value = SubMissionID;  
    //初始化界面的销售渠道
    fm.all('SaleChnl').value = '02';
    fm.all('Remark').value = "";
    fm.all('SaleChnlDetail').value = '01';
    fm.all('ContPrintType').value = '0';
    
    //保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
    if (BQFlag=="2") {
      addClick();
    }

  }
  catch(ex)
  {
    alert("在GroupPolInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在GroupPolInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    fm.querybutton.disabled=true;  
    initMissionID();
    initInpBox();
    initRiskGrid();
    initHistoryImpartGrid();
    initNewHistoryImpartGrid();
    initDiseaseGrid();
    initImpartGrid();
    initServInfoGrid();
    initBaseImpartDetailGrid();
    initHealthImpartDetailGrid();
    initBookingPayIntyGrid();
    fm.all('AgentCom').className = "readonly";
	  fm.all('AgentCom').readOnly = true;
	  fm.all('AgentCom').disable = true;
	  //fm.all('AgentCom').ondblclick = "";
	  fm.all('AgentComName').className = "readonly";
	  fm.all('AgentComName').readOnly = true;
	  fm.all('AgentComName').ondblclick = "";
	  //alert(LoadFlag);
	  fm.LoadFlag.value=LoadFlag;
 
	fm.all('AgentSaleCode').disable = true;
	fm.all('AgentSaleName').readOnly = true;
    
    if(this.ScanFlag == "1"||this.ScanFlag == "0"){ 
            fm.PrtNo.readOnly=true;
            var arrResult = easyExecSql("select * from LCGrpCont where PrtNo = '" + prtNo + "'", 1, 0);                        
            //显示代码选择中文
            if (arrResult != null) {
            polNo=arrResult[0][0];                     
            LoadFlag=2;
            }       
    }
    if(this.LoadFlag == "2"){
        if(polNo!=""){
            //显示代码选择中文
            //parent.fraInterface.showCodeName();  
            getapproveinfo();
         }
        divnormalquesbtn.style.display="";     
    }
    if(this.LoadFlag == "4"){
        //显示代码选择中文
       //parent.fraInterface.showCodeName();
       divRiskDeal.style.display="";
       divButton.style.display="";
       divnormalbtn.style.display="";
       divchangplanbtn.style.display="none";
       divGroupPol4.style.display="";
       divapprovenormalbtn.style.display="none";   
       divapprovebtn.style.display=""; 
       fm.all('Query').style.display="none";
       fm.saveButton.disabled=true; 
       getapproveinfo();       
    }
    if(this.LoadFlag == "6"){
        //显示代码选择中文
       //parent.fraInterface.showCodeName();
       divButton.style.display="none";
        divchangplanbtn.style.display="none";
       divnormalbtn.style.display="none";
       divapprovenormalbtn.style.display=""; 
       getapproveinfo();      
    }
    //9是无名单补名单0
    if(this.LoadFlag == "9")
		{
			//显示代码选择中文
			//parent.fraInterface.showCodeName();
			divButton.style.display="none";
			divchangplanbtn.style.display="none";
			divnormalbtn.style.display="none";
			divRiskDeal.style.display="none";
			divapprovenormalbtn.style.display="none";
			divfilllistbtn.style.display="";
			getapproveinfo();
		}	
    if(this.LoadFlag == "13"){
        //显示代码选择中文
       
      // parent.fraInterface.showCodeName();  
        divchangplanbtn.style.display="none";
       divButton.style.display="none";
       divnormalbtn.style.display="none";
       divapprovenormalbtn.style.display="";  
       divapproveupdatebtn.style.display=""; 
       getapproveinfo();    
    }
    
     if(this.LoadFlag == "14"){
        //显示代码选择中文
       
      // parent.fraInterface.showCodeName();  
        divchangplanbtn.style.display="none";
       divButton.style.display="";
       divnormalbtn.style.display="";
       divuwupdatebtn.style.display="";
       //divapprovenormalbtn.style.display="";  
       //divapproveupdatebtn.style.display=""; 
       getapproveinfo();      
    }
    if(this.LoadFlag == "16"){
        //显示代码选择中文
       divRiskDeal.style.display="none";
       //parent.fraInterface.showCodeName();
       divButton.style.display="none";
       //divnormalbtn.style.display="none";
       divchangplanbtn.style.display="none";
       divnormalbtn.style.display=""; 
       divquerybtn.style.display="";
       fm.all('Query').style.display="none";
       fm.Resource.value=this.Resource;
       fm.LoadFlag.value=this.LoadFlag;
       getapproveinfo();    
    }
    if(this.LoadFlag == "99"){
        //显示代码选择中文
        getapproveinfo();
       divRiskDeal.style.display="none";
       divButton.style.display="none";
       divnormalbtn.style.display="none";
       divchangplanbtn.style.display="none";
       autoMoveButton.style.display="";     
    }  
    
    if(this.LoadFlag =="23")
    {
       divButton.style.display="none";
       //divnormalbtn.style.display="none";
       divnormalbtn.style.display="";  
       divapproveupdatebtn.style.display="none"; 
       divchangplanbtn.style.display="";
       
       getapproveinfo();   
     } 
     
     if(this.LoadFlag == "17"){
		  //显示代码选择中文
		  //parent.fraInterface.showCodeName();
		  divRiskDeal.style.display="none";
		  //divButton.style.display="";
		  //divnormalbtn.style.display="";
		  //divchangplanbtn.style.display="none";
		  //divGroupPol4.style.display="";
		  divapprovenormalbtn.style.display="none";
		  //divRiskDeal
		  //divapprovebtn.style.display="";
		  fm.all('Query').style.display="none";
		  fm.saveButton.disabled=true;
		  divButton.style.display="none";
      divchangplanbtn.style.display="none";
      divnormalbtn.style.display="none";
      divBigProjectApprove.style.display="";
		  getapproveinfo();
		}
	initHandlerIDNo();
    initLegalPerson();	
    initCoInsuranceParamInput();
    showAllCodeName();
    
    if(fm.SaleChnl.value!="03")
  	{
	  	fm.all('AgentCom').style.display="none";
  		fm.all('AgentComName').style.display="none";
  		fm.all('AgentSaleCode').style.display="none";
  		fm.all('AgentSaleName').style.display="none";
  	}else{
  		initAgentSaleCode();
  	}
  	if (fm.SaleChnl.value=="11"||fm.SaleChnl.value=="12"){
	  	fm.all("AgentCom").style.display = "none";
	    fm.all("AgentComName").style.display = "none";
	    fm.all("AgentCom1").style.display = "";
	    fm.all("AgentComName1").style.display = "";
	    fm.all("GrpAgentTitleID").style.display = "";
  	}
    
     if(fm.PayMode.value=="2"||fm.PayMode.value=="4") 
	  
	  { //alert(fm.PayMode.value);
	  	 fm.BankCode.style.display=''; 		
  		fm.BankCodeName.style.display='';
  		fm.BankAccNo.style.display='';
    fm.AccName.style.display='';}
  }
  catch(re)
  {
    alert("GroupPolInputInit.jsp-->InitForm1函数中发生异常:初始化界面错误!"+re);
  }
  try
  {
  	
	getImpartbox(); 
  }
  catch(re)
  {
    alert("GroupPolInputInit.jsp-->InitForm函数中发生异常:初始化告知信息错误!");
  }  
  try
  {
    initSelBox();    
  }
  catch(re)
  {
    alert("GroupPolInputInit.jsp-->InitForm2函数中发生异常:初始化界面错误!");
  }
  try
  {
  if(scantype=="scan")
  {
    setFocus();
  } 
  }
  catch(re)
  {
    alert("GroupPolInputInit.jsp-->InitForm3函数中发生异常:初始化界面错误!");
  }
    
}
// 告知明细信息列表的初始化
function initBaseImpartDetailGrid() {                               
    var iArray = new Array();
 
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="告知版别";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="NewImpartVer";
      iArray[1][9]="告知版别|len<=5";
      iArray[1][18]=300;

      iArray[2]=new Array();
      iArray[2][0]="告知编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ImpartCode";
      iArray[2][5]="2|3";
      iArray[2][6]="0|1";
      iArray[2][9]="告知编码|len<=4";
      iArray[2][15]="ImpartVer";
      iArray[2][17]="1";
      iArray[2][18]=700;

      iArray[3]=new Array();
      iArray[3][0]="告知项目";         		//列名
      iArray[3][1]="400px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="说明内容";         		//列名
      iArray[4][1]="400px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="开始时间";         		//列名
      iArray[5][1]="150px";            		//列宽
      iArray[5][2]=150;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][9]="开始时间|date";

      iArray[6]=new Array();
      iArray[6][0]="结束时间";         		//列名
      iArray[6][1]="90px";            		//列宽
      iArray[6][2]=90;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][9]="结束时间|date";
      
      iArray[7]=new Array();
      iArray[7][0]="证明机构或医生";         		//列名
      iArray[7][1]="90px";            		//列宽
      iArray[7][2]=90;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="目前情况或结果";         		//列名
      iArray[8][1]="90px";            		//列宽
      iArray[8][2]=90;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许


      iArray[9]=new Array();
      iArray[9][0]="能否证明";         		//列名
      iArray[9][1]="90px";            		//列宽
      iArray[9][2]=90;            			//列最大值
      iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[9][4]="yesno";
    
      BaseImpartDetailGrid = new MulLineEnter( "fm" , "BaseImpartDetailGrid" ); 
      //这些属性必须在loadMulLine前
      BaseImpartDetailGrid.mulLineCount = 0;   
      BaseImpartDetailGrid.displayTitle = 1;
      BaseImpartDetailGrid.loadMulLine(iArray);  
      //divLCImpart3.style.display='';
    }
    catch(ex) {
      alert(ex);
    }
}

// 告知明细信息列表的初始化
function initHealthImpartDetailGrid() {                               
    var iArray = new Array();
 
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="告知版别";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="NewImpartVer";
      iArray[1][9]="告知版别|len<=5";
      iArray[1][18]=300;

      iArray[2]=new Array();
      iArray[2][0]="告知编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ImpartCode";
      iArray[2][5]="2|3";
      iArray[2][6]="0|1";
      iArray[2][9]="告知编码|len<=4";
      iArray[2][15]="ImpartVer";
      iArray[2][17]="1";
      iArray[2][18]=700;

      iArray[3]=new Array();
      iArray[3][0]="告知项目";         		//列名
      iArray[3][1]="400px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="说明内容";         		//列名
      iArray[4][1]="400px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="开始时间";         		//列名
      iArray[5][1]="150px";            		//列宽
      iArray[5][2]=150;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][9]="开始时间|date";

      iArray[6]=new Array();
      iArray[6][0]="结束时间";         		//列名
      iArray[6][1]="90px";            		//列宽
      iArray[6][2]=90;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][9]="结束时间|date";
      
      iArray[7]=new Array();
      iArray[7][0]="证明机构或医生";         		//列名
      iArray[7][1]="90px";            		//列宽
      iArray[7][2]=90;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="目前情况或结果";         		//列名
      iArray[8][1]="90px";            		//列宽
      iArray[8][2]=90;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许


      iArray[9]=new Array();
      iArray[9][0]="能否证明";         		//列名
      iArray[9][1]="90px";            		//列宽
      iArray[9][2]=90;            			//列最大值
      iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[9][4]="yesno";
    
      HealthImpartDetailGrid = new MulLineEnter( "fm" , "HealthImpartDetailGrid" ); 
      //这些属性必须在loadMulLine前
      HealthImpartDetailGrid.mulLineCount = 0;   
      HealthImpartDetailGrid.displayTitle = 1;
      HealthImpartDetailGrid.loadMulLine(iArray);  
      //divLCImpart3.style.display='';
    }
    catch(ex) {
      alert(ex);
    }
}
// 险种信息列表的初始化
function initRiskGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种代码";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      //iArray[1][19]=1;

      iArray[2]=new Array();
      iArray[2][0]="险种名称";         			//列名
      iArray[2][1]="160px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//      iArray[2][9]="险种名称|NOTNULL";

      iArray[3]=new Array();
      iArray[3][0]="保费计算方式";      	   		//列名
      iArray[3][1]="0px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=1; 
      iArray[3][9]="保费合计|NUM&NOTNULL";             			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="应保人数";      	   		//列名
      iArray[4][1]="60px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][9]="应保人数|NUM&NOTNULL";
      
      iArray[5]=new Array();
      iArray[5][0]="参保人数";      	   		//列名
      iArray[5][1]="60px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][9]="参保人数|NUM&NOTNULL";

      iArray[6]=new Array();
      iArray[6][0]="保费合计";      	   		//列名
      iArray[6][1]="120px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0; 
      iArray[6][9]="保费合计|NUM&NOTNULL";             			//是否允许输入,1表示允许，0表示不允许

      RiskGrid = new MulLineEnter( "fm" , "RiskGrid" ); 
      //这些属性必须在loadMulLine前
      RiskGrid.mulLineCount = 0;   
      RiskGrid.displayTitle = 1;
      RiskGrid.canChk =1;
      RiskGrid. hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      RiskGrid. hiddenSubtraction=1;

      RiskGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
//团体告知参数
function initImpartGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="告知版别";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="GrpImpartVer";
      iArray[1][9]="告知版别|len<=5";
      iArray[1][18]=300;

      iArray[2]=new Array();
      iArray[2][0]="告知编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ImpartCode";
      iArray[2][5]="2|3";
      iArray[2][6]="0|1";
      //iArray[2][7]="getImpartCode";
      iArray[2][9]="告知编码|len<=4";
      iArray[2][15]="ImpartVer";
      iArray[2][17]="1";
      iArray[2][18]=700;

      iArray[3]=new Array();
      iArray[3][0]="告知内容";         		//列名
      iArray[3][1]="250px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="填写内容";         		//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=150;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][9]="填写内容|len<=200";
      ImpartGrid = new MulLineEnter( "fm" , "ImpartGrid" ); 
      //这些属性必须在loadMulLine前
      ImpartGrid.mulLineCount = 1;   
      ImpartGrid.displayTitle = 1;
      ImpartGrid.addEventFuncName="setFocus";
      //ImpartGrid.tableWidth   ="500px";
      ImpartGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ImpartGrid.setRowColData(1,1,"asdf");
    }
    catch(ex) {
      alert(ex);
    }
}

function initHistoryImpartGrid()
{
			var iArray = new Array();
			try {
			      iArray[0]=new Array();
			      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
			      iArray[0][1]="30px";            		//列宽
			      iArray[0][2]=10;            			//列最大值
			      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			      iArray[1]=new Array();
			      iArray[1][0]="保障起始时间";         		//列名
			      iArray[1][1]="100px";            		//列宽
			      iArray[1][2]=60;            			//列最大值
			      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			      
			      iArray[2]=new Array();
			      iArray[2][0]="保障终止时间";         		//列名
			      iArray[2][1]="100px";            		//列宽
			      iArray[2][2]=60;            			//列最大值
			      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			      iArray[3]=new Array();
			      iArray[3][0]="保障内容";         		//列名
			      iArray[3][1]="75px";            		//列宽
			      iArray[3][2]=300;            			//列最大值
			      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			
			      iArray[4]=new Array();
			      iArray[4][0]="保险费";         		//列名
			      iArray[4][1]="30px";            		//列宽
			      iArray[4][2]=200;            			//列最大值
			      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			      iArray[5]=new Array();
			      iArray[5][0]="保障提供者";         		//列名
			      iArray[5][1]="30px";            		//列宽
			      iArray[5][2]=300;            			//列最大值
			      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			      iArray[6]=new Array(); 
						iArray[6][0]="参加人数";   
						iArray[6][1]="50px";   
						iArray[6][2]=20;        
						iArray[6][3]=0;
						
						iArray[7]=new Array(); 
						iArray[7][0]="报销人数";   
						iArray[7][1]="50px";   
						iArray[7][2]=20;        
						iArray[7][3]=0;
						
						iArray[8]=new Array(); 
						iArray[8][0]="发生金额";   
						iArray[8][1]="50px";   
						iArray[8][2]=20;        
						iArray[8][3]=0;
						
						iArray[9]=new Array(); 
						iArray[9][0]="报销金额";   
						iArray[9][1]="50px";   
						iArray[9][2]=20;        
						iArray[9][3]=0;
						
						iArray[10]=new Array(); 
						iArray[10][0]="未决金额";   
						iArray[10][1]="50px";   
						iArray[10][2]=20;        
						iArray[10][3]=0;
						
						iArray[11]=new Array(); 
						iArray[11][0]="流水号";   
						iArray[11][1]="0px";   
						iArray[11][2]=20;        
						iArray[11][3]=3;
			      
			      HistoryImpartGrid = new MulLineEnter( "fm" , "HistoryImpartGrid" ); 
			      //这些属性必须在loadMulLine前
			      HistoryImpartGrid.mulLineCount = 0;
			      HistoryImpartGrid.hiddenSubtraction=1;  
				  HistoryImpartGrid.hiddenPlus   = 1 ;   
			      HistoryImpartGrid.displayTitle = 1;
			      //ImpartGrid.tableWidth   ="500px";
			      HistoryImpartGrid.addEventFuncName="setFocus";
			      HistoryImpartGrid.loadMulLine(iArray);  
			      
			      //这些操作必须在loadMulLine后面
			      //ImpartGrid.setRowColData(1,1,"asdf");
    }
    catch(ex) {
      alert(ex);
      }
}
function initNewHistoryImpartGrid()
{
			var iArray = new Array();
			try {
			      iArray[0]=new Array();
			      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
			      iArray[0][1]="30px";            		//列宽
			      iArray[0][2]=10;            			//列最大值
			      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			      iArray[1]=new Array();
			      iArray[1][0]="保障期间";         		//列名
			      iArray[1][1]="60px";            		//列宽
			      iArray[1][2]=60;            			//列最大值
			      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
			      
			      iArray[2]=new Array();
			      iArray[2][0]="保障内容";         		//列名
			      iArray[2][1]="75px";            		//列宽
			      iArray[2][2]=300;            			//列最大值
			      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

			
			      iArray[3]=new Array();
			      iArray[3][0]="保障提供者";         		//列名
			      iArray[3][1]="60px";            		//列宽
			      iArray[3][2]=200;            			//列最大值
			      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
			
			      iArray[4]=new Array();
			      iArray[4][0]="保险费";         		//列名
			      iArray[4][1]="60px";            		//列宽
			      iArray[4][2]=300;            			//列最大值
			      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

			      iArray[5]=new Array(); 
				  iArray[5][0]="参加人数";   
				  iArray[5][1]="50px";   
				  iArray[5][2]=20;        
				  iArray[5][3]=1;
						
						iArray[6]=new Array(); 
						iArray[6][0]="报销人数";   
						iArray[6][1]="50px";   
						iArray[6][2]=20;        
						iArray[6][3]=1;
						
						iArray[7]=new Array(); 
						iArray[7][0]="发生金额";   
						iArray[7][1]="50px";   
						iArray[7][2]=20;        
						iArray[7][3]=1;
						
						iArray[8]=new Array(); 
						iArray[8][0]="报销金额";   
						iArray[8][1]="50px";   
						iArray[8][2]=20;        
						iArray[8][3]=1;
						
						iArray[9]=new Array(); 
						iArray[9][0]="未决金额";   
						iArray[9][1]="50px";   
						iArray[9][2]=20;        
						iArray[9][3]=1;
						
						iArray[10]=new Array(); 
						iArray[10][0]="流水号";   
						iArray[10][1]="0px";   
						iArray[10][2]=20;        
						iArray[10][3]=3;
			      
			      NewHistoryImpartGrid = new MulLineEnter( "fm" , "NewHistoryImpartGrid" ); 
			      //这些属性必须在loadMulLine前
			      NewHistoryImpartGrid.mulLineCount = 1;
			      NewHistoryImpartGrid.hiddenSubtraction=0;  
				  NewHistoryImpartGrid.hiddenPlus   = 0 ;   
			      NewHistoryImpartGrid.displayTitle = 1;
			      //ImpartGrid.tableWidth   ="500px";
			      NewHistoryImpartGrid.addEventFuncName="setFocus";
			      NewHistoryImpartGrid.loadMulLine(iArray);  
			      
			      //这些操作必须在loadMulLine后面
			      //ImpartGrid.setRowColData(1,1,"asdf");
    }
    catch(ex) {
      alert(ex);
      }
}

function initDiseaseGrid()
{
			var iArray = new Array();
			try {
			      iArray[0]=new Array();
			      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
			      iArray[0][1]="30px";            		//列宽
			      iArray[0][2]=10;            			//列最大值
			      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			      iArray[1]=new Array();
			      iArray[1][0]="年份";         		//列名
			      iArray[1][1]="100px";            		//列宽
			      iArray[1][2]=60;            			//列最大值
			      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			      
			      iArray[2]=new Array();
			      iArray[2][0]="疾病名称";         		//列名
			      iArray[2][1]="100px";            		//列宽
			      iArray[2][2]=60;            			//列最大值
			      iArray[2][3]=0;

			      iArray[3]=new Array();
			      iArray[3][0]="患病人数";         		//列名
			      iArray[3][1]="100px";            		//列宽
			      iArray[3][2]=300;            			//列最大值
			      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			
			      iArray[4]=new Array();
			      iArray[4][0]="当年发生医疗费用总额";         		//列名
			      iArray[4][1]="100px";            		//列宽
			      iArray[4][2]=200;            			//列最大值
			      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			      iArray[5]=new Array(); 
						iArray[5][0]="备注";   
						iArray[5][1]="100px";   
						iArray[5][2]=20;        
						iArray[5][3]=0;
						
						iArray[6]=new Array(); 
						iArray[6][0]="流水号";   
						iArray[6][1]="30px";   
						iArray[6][2]=20;        
						iArray[6][3]=3;
						
									      
			      DiseaseGrid = new MulLineEnter( "fm" , "DiseaseGrid" ); 
			      DiseaseGrid.mulLineCount = 0;   
			      DiseaseGrid.displayTitle = 1;
			      DiseaseGrid.hiddenSubtraction=1;  
				  DiseaseGrid.hiddenPlus   = 1 ;
			      DiseaseGrid.addEventFuncName="setFocus";
			      DiseaseGrid.loadMulLine(iArray);  
			      
    }
    catch(ex) {
      alert(ex);
      }
}

function initServInfoGrid()
{
	var iArray = new Array();
	try {
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
		iArray[1]=new Array();
		iArray[1][0]="服务类型";         		//列名
		iArray[1][1]="130px";            		//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许          

		iArray[2]=new Array();
		iArray[2][0]="服务内容";         		//列名
		iArray[2][1]="130px";            		//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


                                			
		iArray[3]=new Array(); 
		iArray[3][0]="服务明细";   
		iArray[3][1]="130px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;         		
		
		iArray[4]=new Array(); 
		iArray[4][0]="服务明细内容";   
		iArray[4][1]="130px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;	
		
                
		iArray[5]=new Array(); 
		iArray[5][0]="条件值";   
		iArray[5][1]="130px";   
		iArray[5][2]=20;        
		iArray[5][3]=3;
		
		iArray[6]=new Array(); 
		iArray[6][0]="服务选择值";   
		iArray[6][1]="50px";   
		iArray[6][2]=20;        
		iArray[6][3]=2;
		iArray[6][4]="ServerInfoChooseCode";	
		iArray[6][5]="6|8"; 
		iArray[6][6]="0|1|";
    iArray[6][15]="servkind";
    iArray[6][17]="5";	
    
     
                
		iArray[7]=new Array(); 
		iArray[7][0]="条件值";   
		iArray[7][1]="130px";   
		iArray[7][2]=20;        
		iArray[7][3]=3; 			
		
		iArray[8]=new Array(); 
		iArray[8][0]="服务名称";   
		iArray[8][1]="190px";   
		iArray[8][2]=20;        
		iArray[8][3]=0;       
						    	      
		ServInfoGrid = new MulLineEnter( "fm" , "ServInfoGrid" ); 
		//这些属性必须在loadMulLine前
		ServInfoGrid.mulLineCount = 0;   
		ServInfoGrid.displayTitle = 1;
		ServInfoGrid.hiddenSubtraction=1;  
		ServInfoGrid.hiddenPlus   = 1 ;             
		ServInfoGrid.addEventFuncName="setFocus";            
		ServInfoGrid.loadMulLine(iArray);  
		               
		//这些操作必须在loadMulLine后面
		//ImpartGrid.setRowColData(1,1,"asdf");
		fillintigrid();
    }                  
    catch(ex) {        
      alert(ex);
      }
}
function initBookingPayIntyGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="缴费期次";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="60px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="缴费日期";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="缴费金额";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      BookingPayIntyGrid = new MulLineEnter( "fm" , "BookingPayIntyGrid" ); 
      //这些属性必须在loadMulLine前
      BookingPayIntyGrid.mulLineCount = 0;   
      BookingPayIntyGrid.displayTitle = 1;
      BookingPayIntyGrid.addEventFuncName="ChkMulLineCount";

      //ImpartGrid.tableWidth   ="500px";
      BookingPayIntyGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ImpartGrid.setRowColData(1,1,"asdf");
    }
    catch(ex) {
      alert(ex);
    }
}


function getGrpCont(){


    try { fm.all( 'PrtNo' ).value = mSwitch.getVar( "PrtNo" ); } catch(ex) { };
    try { fm.all( 'GrpContNo' ).value = mSwitch.getVar( "GrpContNo" ); } catch(ex) { };

}
function fillintigrid()
{
   var strservSql="select distinct a.ServKind,b.ServKindRemark,a.ServDetail,c.ServDetailRemark,trim(a.servkind)||'-'||trim(a.servdetail),'' from LDServChooseInfo a,LDServKindInfo b,LDServDetailInfo c where a.ServKind=b.ServKind and a.ServKind=c.ServKind and a.ServDetail=c.ServDetail order by a.ServKind,a.ServDetail"
      turnPage.queryModal(strservSql, ServInfoGrid);
}
</script>