<!--
@NAME:ChangeExtractRate.jsp
@DESC:这个页面是用来维护退保费率的
@AUTHOR:_LaoChen_ ;)
@SINCE:20070517
-->

<html>
 <%@page contentType="text/html;charset=GBK" %>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <head >
   <SCRIPT src="../common/javascript/Common.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js" type="text/javascript" ></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="ChangeRate.js" type="text/javascript" ></SCRIPT>
 <%@include file="ChangeRateInit.jsp"%>
 </head>
<body  onload="initForm();" >

<form action="./ChangeRateSave.jsp" method=post name=fm target='fraSubmit'>
<br />
<br />
     <table>
     
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskInfoGrid);">
         </TD>
         <TD class= titleImg>
         费率比例：
         </TD>
       </TR>
      </table>
      <br />
      <table  class= common>
	        <TR  class= common>
	          <TD colSpan=1>
	            <span id="spanChangeRateGrid" >
	            </span>
	          </TD>
	        </TR>
	      </table>
	      <div><font color="red">退保及部分领取比例不能大于2%，录入规则如：当比例为1%时，录入数字0.01。</font></div>
	      <div><font color="red">退保及部分领取比例值最多包括3位小数。</font></div>
	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="GrpPolNo" name="GrpPolNo">
	<input type=hidden id="PrtNo" name="PrtNo">
	<br>
	<input class=cssButton type=button value="保  存" onclick="this.disabled=true; setEnable('submitButton');Submit('INSERT');" id="submitButton" >
	<input class=cssButton type=button value="修  改" onclick="this.disabled=true; setEnable('submitButton2');Submit('UPDATE');" id="submitButton2" > 	
	<input class=cssButton type=button value="删  除" onclick="this.disabled=true; setEnable('submitButton3');Submit('DELETE');" id="submitButton3" > 	
  </form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
