<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.intltb.*"%>    
  <%@page import="com.sinosoft.lis.pubfun.*"%> 

<%

  CErrors tError = new CErrors();
  String FlagStr = "";
  String Content = "";
try{
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput) session.getValue("GI");
  if (tGI == null) {
    System.out.println("登录信息为空");
    FlagStr = "Fail";
    Content = "登录校验失败，请重新登录";
  }
  else {
    String tPrtNo = request.getParameter("PrtNo");
    String tOperate = request.getParameter("Operate");
    LCRiskFeeRateSet tLCRiskFeeRateSet = new LCRiskFeeRateSet();
    String tChangeRateNum[] = request.getParameterValues("ChangeRateGridNo");
    String tFeeYear[] = request.getParameterValues("ChangeRateGrid1");
    String tDrawRate[] = request.getParameterValues("ChangeRateGrid2");
    String tCancelRate[] = request.getParameterValues("ChangeRateGrid3");
    
    for(int num=0; num < tChangeRateNum.length; num++){
   	    LCRiskFeeRateSchema tLCRiskFeeRateSchema = new LCRiskFeeRateSchema();
   	    tLCRiskFeeRateSchema.setPrtNo(tPrtNo);
   	    tLCRiskFeeRateSchema.setFeeYear(tFeeYear[num]);
   	    tLCRiskFeeRateSchema.setDrawRate(tDrawRate[num]);
   	    tLCRiskFeeRateSchema.setCancelRate(tDrawRate[num]);
   	    tLCRiskFeeRateSet.add(tLCRiskFeeRateSchema);
    }
    
    System.out.println(tLCRiskFeeRateSet.size());
    
    VData tVData = new VData();
    tVData.clear();
    tVData.add(tLCRiskFeeRateSet); 
    tVData.add(tGI);
    
    ChangeRateUI tChangeRateUI = new ChangeRateUI();
    if(!tChangeRateUI.submitData(tVData,tOperate)){
       Content = " 失败，原因:" + tChangeRateUI.mErrors.getFirstError();
       FlagStr = "Fail";
     }
     else {
       Content = " 操作成功";
       FlagStr = "Succ"; 
    }
  }
 }catch (Exception ex){
 	   Content = "程序处理失败，请确认录入的比例是否正确";
       FlagStr = "Fail"; 
 }
%>
<html>
<script language="javascript">
       if("<%=FlagStr%>"=="Succ")
       {
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
       }
       else
       {
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
       }
         window.focus;
</script>
</html>
