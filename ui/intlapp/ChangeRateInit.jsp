
<%
	
%>

<script language="JavaScript" type="text/javascript">

    var tPrtNo=<%=request.getParameter("prtno")%>;	
	
	function initForm() {
		try {
			initChangeExtractRateGrid();
			initQuery();
		} catch (ex) {
			alert(ex.message);
		}
	}

	function initChangeExtractRateGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;

			iArray[1] = new Array();
			iArray[1][0] = "年份";
			iArray[1][1] = "100px";
			iArray[1][2] = 10;
			iArray[1][3] = 0;

			iArray[2] = new Array();
			iArray[2][0] = "退保/部分领取比例";
			iArray[2][1] = "100px";
			iArray[2][2] = 10;
			iArray[2][3] = 1;

			iArray[3] = new Array();
			iArray[3][0] = "退保费用比例";
			iArray[3][1] = "100px";
			iArray[3][2] = 10;
			iArray[3][3] = 3;
			
			iArray[4] = new Array();
			iArray[4][0] = "不用";
			iArray[4][1] = "100px";
			iArray[4][2] = 10;
			iArray[4][3] = 3;
			
			iArray[5] = new Array();
			iArray[5][0] = "不用";
			iArray[5][1] = "100px";
			iArray[5][2] = 10;
			iArray[5][3] = 3;

			ChangeRateGrid = new MulLineEnter("fm", "ChangeRateGrid");
			ChangeRateGrid.mulLineCount = 0;
			ChangeRateGrid.displayTitle = 1;
			ChangeRateGrid.canChk = 0;
			ChangeRateGrid.hiddenPlus = 1;
			ChangeRateGrid.hiddenSubtraction = 1;
			ChangeRateGrid.loadMulLine(iArray);
		} catch (ex) {
			alter("初始化ChangeExtractRateInit.jsp-->initChangeExtractRateGrid异常!"
					+ ex.message);
		}

	}

	function initQuery() {
		var sql = "select feeyear,DECIMAL(drawrate,4,3),DECIMAL(cancelrate,4,3) from LCRiskFeeRate where prtno='" + tPrtNo + "'";
		var strSqlTemp = easyQueryVer3(sql, 1, 0, 1);
		turnPage.strQueryResult = strSqlTemp;
		if (!turnPage.strQueryResult) {
			ChangeRateGrid.addOne("ChangeRateGrid");
			ChangeRateGrid.addOne("ChangeRateGrid");
			ChangeRateGrid.addOne("ChangeRateGrid");
			ChangeRateGrid.addOne("ChangeRateGrid");
			//显示年份
			ChangeRateGrid.setRowColData(0, 1, "2");
			ChangeRateGrid.setRowColData(1, 1, "3");
			ChangeRateGrid.setRowColData(2, 1, "4");
			ChangeRateGrid.setRowColData(3, 1, "5");
			//显示初始比例
			ChangeRateGrid.setRowColData(0, 2, "0");
			ChangeRateGrid.setRowColData(1, 2, "0");
			ChangeRateGrid.setRowColData(2, 2, "0");
			ChangeRateGrid.setRowColData(3, 2, "0");
		} else {
			turnPage.queryModal(sql, ChangeRateGrid);
		}
	}
</script>
