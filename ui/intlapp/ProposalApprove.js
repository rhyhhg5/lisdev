//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var mSwitch = parent.VD.gVSwitch;

/*********************************************************************
 *  调用EasyQuery查询保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	initPolGrid();
	
 	var hasBPOErr = "";
 	if(fm.HaseBPOErr.value == "0")                 
 	{
 	  hasBPOErr = " = ";
 	}
 	else if(fm.HaseBPOErr.value == "1")                 
 	{
 	  hasBPOErr = " > ";
 	}
 	
 	var sql = "" 
 	        + "select lwmission.missionprop1,lwmission.missionprop2,lwmission.missionprop4,lwmission.missionprop5,"
 	        + "  lwmission.missionprop6 missionprop6,lwmission.Missionid,lwmission.submissionid,"
 	        + "  (select name from ldcom where comcode=lwmission.MissionProp8),"
 	        + "  (select max(char(makedate)||' '||char(maketime)) from es_doc_main where doccode=lwmission.MissionProp2), "
 	        + "  case when (select count(1) from BPOIssue a, BPOLCPol b where a.BPOBatchNo = b.BPOBatchNo and a.ContID = b.ContID and b.PrtNo = lwmission.MissionProp2) > 0 then '有' else '无' end, "
 	        + "  ActivityID, ProcessID "
 	        + "from lwmission where 1=1 "
		      + (hasBPOErr == "" ? "" : "  and (select count(1) from BPOIssue a, BPOLCPol b where a.BPOBatchNo = b.BPOBatchNo and a.ContID = b.ContID and b.PrtNo = lwmission.MissionProp2) " + hasBPOErr + " 0 ")
          + " and activityid = '0000009002' "  //万能险
          + " and processid = '0000000009' "
			    + getWherePart('lwmission.MissionProp1','ProposalNo')
		      + getWherePart('lwmission.MissionProp2','PrtNo')
		      + getWherePart('lwmission.MissionProp6','MakeDate')
		      + getWherePart('getUniteCode(lwmission.MissionProp7)','AgentCode')
		      + getWherePart('lwmission.MissionProp8','ManageCom','like') 
          
          + "union all "
          
          + "select distinct b.ProposalContNo, a.missionprop1, b.AppntName, b.InputOperator,"
          + "  b.InputDate missionprop6,a.Missionid,a.submissionid,"
          + "  (select name from ldcom where comcode=a.MissionProp3),"
          + "  (select max(char(makedate)||' '||char(maketime)) from es_doc_main where doccode=a.MissionProp1), "
          + "  case when (select count(1) from BPOIssue a, BPOLCPol b where a.BPOBatchNo = b.BPOBatchNo and a.ContID = b.ContID and b.PrtNo = a.MissionProp1) > 0 then '有' else '无' end, "
          + "  ActivityID, ProcessID "
          + "from lwmission a left join LCCont b "
          + "on a.MissionProp1 = b.PrtNo "
          + "where 1=1 "
          + (hasBPOErr == "" ? "" : "  and (select count(1) from BPOIssue a, BPOLCPol b where a.BPOBatchNo = b.BPOBatchNo and a.ContID = b.ContID and b.PrtNo = a.MissionProp1) " + hasBPOErr + " 0 ")
 			    + " and activityid = '0000007003' "  //银保
          + " and processid = '0000000007' "
          + " and exists(select 1 from BPOMissionState where BussNoType = 'TB' and BussNo = MissionProp1) "
          + getWherePart('missionprop1','PrtNo')
          + getWherePart('b.MakeDate','MakeDate')
		      + getWherePart('getUniteCode(b.AgentCode)','AgentCode')
          + getWherePart('missionprop3','ManageCom','like')
          
 			    + "order by MissionProp6 "
 			    + "with ur ";
	turnPage.queryModal(sql, PolGrid);
}

function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode")
 {
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }
 if(cCodeName=="agentgroup1")
 {
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
}


function showApproveDetail() 
{ 
  if (PolGrid.getSelNo() == 0) 
  {
    alert("请先选择一条保单信息！");
    return false;
  } 
  
  var tMissionID = PolGrid.getRowColDataByName(PolGrid.getSelNo() - 1, "MissionID");
  var tSubMissionID= PolGrid.getRowColDataByName(PolGrid.getSelNo() - 1, "SubMissionID");
  var polNo = PolGrid.getRowColDataByName(PolGrid.getSelNo() - 1, "ContNo");
  var prtNo = PolGrid.getRowColDataByName(PolGrid.getSelNo() - 1, "PrtNo");
  var contNo = PolGrid.getRowColDataByName(PolGrid.getSelNo() - 1, "ContNo");
  var tProcessID = PolGrid.getRowColDataByName(PolGrid.getSelNo() - 1, "ProcessID");
  var tActivityID = PolGrid.getRowColDataByName(PolGrid.getSelNo() - 1, "ActivityID");
  
  //by gzh 增加保单录入人员与复核人员不能为同一人的校验 20110817
  var OperatorSql = "select inputoperator from lccont where prtno = '"+prtNo+"' ";
  var OperatorResult = easyExecSql(OperatorSql);
  if(OperatorResult == null){
  	alert("获取保单录入人员信息失败！");
  	return false;
  }else if(OperatorResult[0][0]==Operator){
  	alert("该印刷号的投保单由您（"+OperatorResult[0][0]+"）录入，您不能进行复核操作!");
  	return false;
  }
  
  var strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and  CreatePos='承保复核' and  PolState=1003";
  var arrResult = easyExecSql(strSql);
  if (arrResult!=null && arrResult[0][1]!=Operator) 
  {
    alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
    return;
  }
  
  //锁定该印刷号
  var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保复核&PolState=1003&Action=INSERT";
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 


  //配合以前实现过的页面功能，源于ProposalMain.jsp
  mSwitch.deleteVar("PolNo");
	mSwitch.addVar("PolNo", "", polNo);
	mSwitch.updateVar("PolNo", "", polNo);
	//alert(polNo);
	
	mSwitch.deleteVar("ApprovePolNo");
	mSwitch.addVar("ApprovePolNo", "", polNo);
	mSwitch.updateVar("ApprovePolNo", "", polNo);

  easyScanWin = window.open("../app/ProposalEasyScan.jsp?LoadFlag=5&ContNo="+contNo+"&prtNo="+prtNo+"&MissionID="+tMissionID+"&SubMissionID="+tSubMissionID+"&scantype=scan&ProcessID=" + tProcessID + "&ActivityID=" + tActivityID, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");    
  initPolGrid();
}