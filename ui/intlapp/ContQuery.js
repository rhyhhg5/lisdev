//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//----------------------------------初始化区------------------------------------------

//输入框的初始化
function initInpBox()
{
	try
	{
    fm.all('ManageCom').value = manageCom;
    fm.EndDate.value = tCurrentDate;
    var sql = "select Current Date - 3 month from dual ";
    fm.StartDate.value = easyExecSql(sql);
	}
	catch(ex)
	{
		alert("在ProposalPrintInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}


//----------------------------------事件相应区----------------------------------------

//查询保单信息
function queryCont()
{
  if(!verifyInput2())
  {
    return false;
  }
  
  var sql = "select a.PrtNo, a.ContNo, (select Name from LDCom where ComCode = a.ManageCom), "
          + "   b.CodeName, (select Name from LAAgent where AgentCode = a.AgentCode), "
          + "   a.AppntName, a.CValidate, CInvalidate, Prem, "
          + "   (select UserName from LDUser where UserCode = a.InputOperator) "
          + "from LCCont a, LDCode b "
          + "where a.SaleChnl = b.Code "
          + "   and a.ContType = '1' "
          + "   and a.IntlFlag = '1' "
          + "   and b.CodeType in('lcsalechnl', 'salechnl') "
          + "   and AppFlag = '1' "
          + "   and ManageCom like '" + manageCom + "%' "
          + ((fm.ManageCom.value != "") ? ("    and a.ManageCom like '" + fm.ManageCom.value + "%' ") : (""))
          + getWherePart("a.PrtNo", "PrtNo")
          + getWherePart("a.ContNo", "ContNo")
          + getWherePart("a.SaleChnl", "SaleChnl")
          + getWherePart("a.AgentCode", "AgentCode")
          + getWherePart("a.AppntNo", "AppntNo")
          + "   and CValidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' "
          + ((fm.InsuredNo.value != "") ? ("   and exists (select 1 from LCInsured where ContNo = a.ContNo and InsuredNo ='" + fm.InsuredNo.value + "')") : "");
  
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, ContGrid);
	
	if(ContGrid.mulLineCount == 0)
	{
	  alert("没有查询到数据");
	  return false;
	}
}

//保单下载
function downloadCont()
{
  if(ContGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }
  
  fm.Sql.value = turnPage2.strQuerySql;
  
  fm.submit();
}



//----------------------------------校验区-------------------------------------------