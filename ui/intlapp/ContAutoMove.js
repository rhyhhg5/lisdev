//程序名称：AutoMove.js
//程序功能：外包录入随动
//创建日期：2007-11-5
//创建人  ：xiaoxin
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var k = 0;
var turnPage = new turnPageClass();
var cflag = "";
var canReplyFlag = false;

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
 
/**
 * 为每个界面录入控件增加相应随动的事件，在ProposalInput.js中调用 
 **/
function setFocus() {
  for (var elementsNum=0; elementsNum<window.document.forms[0].elements.length; elementsNum++) {
  	if (typeof(LoadFlag)!="undefined" && LoadFlag==99) {
    	//if(window.document.forms[0].elements[elementsNum].name=="BnfGrid1"){
    		
    	//}
      if (window.document.forms[0].elements[elementsNum].type == "text" 
          || window.document.forms[0].elements[elementsNum].type == "textarea" ) {
        window.document.forms[0].elements[elementsNum].onclick = custom;
        window.document.forms[0].elements[elementsNum].onfocus = debugShow;  
      }
    }
    else {
  	window.document.forms[0].elements[elementsNum].onfocus = goToArea;        
    }		
  } 
}


/** 
 * 隐藏红框
 **/
function hiddenPosition() {
  top.fraPic.spanPosition.style.display = "none";
}


/** 
 * 显示红框，将控件框起来 
 **/
function showPosition(l, t, w, h) {
  //alert(l + " " + t + " " + w + " " + h);
  top.fraPic.spanPosition.style.display = "";
  top.fraPic.spanPosition.style.left = l;
  top.fraPic.spanPosition.style.top = t;
  top.fraPic.Rect.width = w;
  top.fraPic.Rect.height = h;
}

/*** 随动 ***/
function goToArea() {
	var goToLock = true;
  var objName = this.name;
  var hx = 0;
  var hy = 0;
  try { hiddenPosition(); } catch(e) {}
  
  try { if (objName == "PrtNo") { goToPic(0); top.fraPic.scrollTo(0, 39); showPosition(863+hx, 152+hy, 343, 38); } } catch(e) {} 
  try { if (objName == "AgentCom") { goToPic(0); top.fraPic.scrollTo(0, 39); showPosition(152+hx, 172+hy, 228, 37); } } catch(e) {} 
  try { if (objName == "AgentCodeG") { goToPic(0); top.fraPic.scrollTo(0, 54); showPosition(150+hx, 222+hy, 228, 35); } } catch(e) {} 
  try { if (objName == "AgentNameG") { goToPic(0); top.fraPic.scrollTo(0, 54); showPosition(150+hx, 222+hy, 228, 35); } } catch(e) {} 
  try { if (objName == "AgentCode") { goToPic(0); top.fraPic.scrollTo(0, 54); showPosition(150+hx, 222+hy, 228, 35); } } catch(e) {} 
  try { if (objName == "AgentName") { goToPic(0); top.fraPic.scrollTo(0, 54); showPosition(150+hx, 222+hy, 228, 35); } } catch(e) {} 
  try { if (objName == "InputDate") { goToPic(0); top.fraPic.scrollTo(0, 1366); showPosition(779+hx, 1641+hy, 396, 53); } } catch(e) {} 
  try { if (objName == "ReceiveDate") { goToPic(0); top.fraPic.scrollTo(0, 39); showPosition(154+hx, 248+hy, 214, 33); } } catch(e) {} 
  try { if (objName == "CValiDate") { goToPic(0); top.fraPic.scrollTo(0, 1000); showPosition(230+hx, 1200+hy, 614, 50); } } catch(e) {}
  try { if (objName == "PolApplyDate") { goToPic(0); top.fraPic.scrollTo(0, 1361); showPosition(648+hx, 1650+hy, 560, 60); } } catch(e) {}
  
	//投保人信息
	
  try { if (objName == "AppntName") { goToPic(0); top.fraPic.scrollTo(0, 282); showPosition(324+hx, 341+hy, 250, 35); } } catch(e) {} 
  try { if (objName == "AppntIDType") { goToPic(0); top.fraPic.scrollTo(0, 282); showPosition(323+hx, 367+hy, 258, 35); } } catch(e) {} 
  try { if (objName == "AppntIDNo") { goToPic(0); top.fraPic.scrollTo(0, 282); showPosition(625+hx, 365+hy, 579, 39); } } catch(e) {} 
  try { if (objName == "AppntSex") { goToPic(0); top.fraPic.scrollTo(0, 135); showPosition(773+hx, 327+hy, 120, 33); } } catch(e) {} 
  try { if (objName == "AppntMarriage") { goToPic(0); top.fraPic.scrollTo(0, 150); showPosition(888+hx, 327+hy, 274, 34); } } catch(e) {} 
  try { if (objName == "AppntNativePlace") { goToPic(0); top.fraPic.scrollTo(0, 165); showPosition(943+hx, 359+hy, 222, 23); } } catch(e) {} 
  try { if (objName == "AppntSalary") { goToPic(0); top.fraPic.scrollTo(0, 180); showPosition(1017+hx, 384+hy, 145, 28); } } catch(e) {} 
  try { if (objName == "AppntBirthday") { goToPic(0); top.fraPic.scrollTo(0, 282); showPosition(626+hx, 340+hy, 331, 35); } } catch(e) {} 
  try { if (objName == "OccupationCode") { goToPic(0); top.fraPic.scrollTo(0, 282); showPosition(861+hx, 392+hy, 193, 38); } } catch(e) {} 
  try { if (objName == "GrpName") { goToPic(0); top.fraPic.scrollTo(0, 282); showPosition(322+hx, 394+hy, 266, 36); } } catch(e) {} 
  try { if (objName == "Position") { goToPic(0); top.fraPic.scrollTo(0, 268); showPosition(627+hx, 393+hy, 176, 36); } } catch(e) {} 
  try { if (objName == "PostalAddress") { goToPic(0); top.fraPic.scrollTo(0, 268); showPosition(324+hx, 420+hy, 675, 35); } } catch(e) {} 
  try { if (objName == "ZipCode") { goToPic(0); top.fraPic.scrollTo(0, 268); showPosition(1008+hx, 420+hy, 180, 36); } } catch(e) {} 
  try { if (objName == "Phone") { goToPic(0); top.fraPic.scrollTo(0, 268); showPosition(323+hx, 447+hy, 262, 37); } } catch(e) {} 
  try { if (objName == "Mobile") { goToPic(0); top.fraPic.scrollTo(0, 268); showPosition(625+hx, 447+hy, 233, 36); } } catch(e) {} 
  try { if (objName == "EMail") { goToPic(0); top.fraPic.scrollTo(0, 268); showPosition(917+hx, 447+hy, 270, 38); } } catch(e) {} 
  

	//缴费信息
	
	try { if (objName == "PayIntv") { goToPic(0); top.fraPic.scrollTo(0, 956); showPosition(826+hx, 1066+hy, 286, 149); } } catch(e) {} 
	try { if (objName == "PayMode") { goToPic(0); top.fraPic.scrollTo(0, 1145); showPosition(258+hx, 1223+hy, 146, 42); } } catch(e) {} 
	try { if (objName == "BankCode") { goToPic(0); top.fraPic.scrollTo(0, 1122); showPosition(330+hx, 1255+hy, 329, 34); } } catch(e) {} 
	try { if (objName == "AccName") { goToPic(0); top.fraPic.scrollTo(0, 1137); showPosition(995+hx, 1280+hy, 185, 37); } } catch(e) {} 
	try { if (objName == "BankAccNo") { goToPic(0); top.fraPic.scrollTo(0, 1152); showPosition(329+hx, 1280+hy, 638, 36); } } catch(e) {} 
	
}

function custom() {
  try {
    objName = this.name;
    //alert(objName);
    
    try { hiddenPosition(); } catch(e) {}
    
    //top.fraPic.centerPic.innerHTML = "try { if (objName == \"\") { goToPic(1); top.fraPic.scrollTo(0, 0); showPosition(192+hx, 83+hy, 285, 115); } } catch(e) {} ";
    path = top.fraPic.centerPic.innerHTML;
    this.value = "try { if (objName == \"" + objName + path.substring(path.indexOf("=") + 4);
    //alert(path);  
  }
  catch(e) {}
}

function debugShow() {
  try {   
    objName = this.name;
    
    try { hiddenPosition(); } catch(e) {}
    
    eval(this.value);
  } catch(e) {}
}

