<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ContInsuredSave.jsp
//程序功能：
//创建日期：2007-7-13 9:52
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.intltb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
  //输出参数
  String FlagStr = "";
  String Content = "";
  CErrors tError = null;

  LCContSchema tLCContSchema = new LCContSchema();
  LDPersonSchema tLDPersonSchema   = new LDPersonSchema();
  LCInsuredDB tOLDLCInsuredDB=new LCInsuredDB();
  LCAddressSchema tLCAddressSchema = new LCAddressSchema();
  LCInsuredSchema tmLCInsuredSchema =new LCInsuredSchema();
  LCRiskDutyWrapSet tLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
 	LCDiseaseResultSet tLCDiseaseResultSet = new LCDiseaseResultSet();
   
  ContInsuredIntlUI tContInsuredIntlUI   = new ContInsuredIntlUI();
   
  GlobalInput tGI = (GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  String fmAction=request.getParameter("fmAction");  //后面要执行的动作：添加，修改，删除
  System.out.println("fmAction:"+fmAction); 
  
  //得到合同信息
  tLCContSchema.setContNo(request.getParameter("ContNo"));
  tLCContSchema.setPrtNo(request.getParameter("PrtNo"));
  //String tBonusgetmode = request.getParameter("BonusType");
  
  //被保人信息
  tmLCInsuredSchema.setInsuredNo(request.getParameter("InsuredNo"));
  tmLCInsuredSchema.setRelationToMainInsured(request.getParameter("RelationToMainInsured"));
  tmLCInsuredSchema.setRelationToAppnt(request.getParameter("RelationToAppnt"));
  tmLCInsuredSchema.setContNo(request.getParameter("ContNo"));
  tmLCInsuredSchema.setGrpContNo(request.getParameter("GrpContNo"));
  tmLCInsuredSchema.setContPlanCode(request.getParameter("ContPlanCode"));
  tmLCInsuredSchema.setExecuteCom(request.getParameter("ExecuteCom"));
  tmLCInsuredSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
  tmLCInsuredSchema.setInsuredPeoples(request.getParameter("InsuredPeoples"));
  tmLCInsuredSchema.setSalary(request.getParameter("Salary"));
  tmLCInsuredSchema.setNativePlace(request.getParameter("NativePlace"));
  tmLCInsuredSchema.setMarriage(request.getParameter("Marriage"));
  tmLCInsuredSchema.setBankCode(request.getParameter("BankCode"));
  tmLCInsuredSchema.setBankAccNo(request.getParameter("BankAccNo"));                
  tmLCInsuredSchema.setAccName(request.getParameter("AccName"));
  tmLCInsuredSchema.setOthIDNo(request.getParameter("OthIDNo"));
  tmLCInsuredSchema.setEnglishName(request.getParameter("EnglishName"));
  tmLCInsuredSchema.setOccupationType(request.getParameter("OccupationType"));
  tmLCInsuredSchema.setOccupationCode(request.getParameter("OccupationCode"));
  tmLCInsuredSchema.setPosition(request.getParameter("Position"));
  tmLCInsuredSchema.setIDStartDate(request.getParameter("IDStartDate"));
  tmLCInsuredSchema.setIDEndDate(request.getParameter("IDEndDate"));
  tmLCInsuredSchema.setNativeCity(request.getParameter("NativeCity"));
  //modify by zxs 
  tmLCInsuredSchema.setAuthorization(request.getParameter("InsuredAuth"));
  if(request.getParameter("IDType").equals("a")||request.getParameter("IDType").equals("b")){
  tmLCInsuredSchema.setOriginalIDNo(request.getParameter("InsuredPassIDNo"));
  }
  //客户人信息
  tLDPersonSchema.setCustomerNo(request.getParameter("InsuredNo"));
  tLDPersonSchema.setName(request.getParameter("Name"));
  tLDPersonSchema.setSex(request.getParameter("Sex"));   
  tLDPersonSchema.setBirthday(request.getParameter("Birthday"));
  tLDPersonSchema.setIDType(request.getParameter("IDType"));
  tLDPersonSchema.setIDNo(request.getParameter("IDNo"));
  tLDPersonSchema.setPassword(request.getParameter("Password"));
  tLDPersonSchema.setNativePlace(request.getParameter("NativePlace"));
  tLDPersonSchema.setNationality(request.getParameter("Nationality"));
  tLDPersonSchema.setRgtAddress(request.getParameter("RgtAddress"));
  tLDPersonSchema.setMarriage(request.getParameter("Marriage"));
  tLDPersonSchema.setMarriageDate(request.getParameter("MarriageDate"));
  tLDPersonSchema.setHealth(request.getParameter("Health"));
  tLDPersonSchema.setStature(request.getParameter("Stature"));
  tLDPersonSchema.setAvoirdupois(request.getParameter("Avoirdupois"));
  tLDPersonSchema.setDegree(request.getParameter("Degree"));
  tLDPersonSchema.setCreditGrade(request.getParameter("CreditGrade"));
  tLDPersonSchema.setOthIDType(request.getParameter("OthIDType"));
  tLDPersonSchema.setOthIDNo(request.getParameter("OthIDNo"));
  tLDPersonSchema.setICNo(request.getParameter("ICNo"));
  tLDPersonSchema.setGrpNo(request.getParameter("GrpNo"));
  tLDPersonSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
  tLDPersonSchema.setStartWorkDate(request.getParameter("StartWorkDate"));
  tLDPersonSchema.setPosition(request.getParameter("Position"));
  tLDPersonSchema.setSalary(request.getParameter("Salary"));
  tLDPersonSchema.setOccupationType(request.getParameter("OccupationType"));
  tLDPersonSchema.setOccupationCode(request.getParameter("OccupationCode"));
  tLDPersonSchema.setWorkType(request.getParameter("WorkType"));
  tLDPersonSchema.setPluralityType(request.getParameter("PluralityType"));
  tLDPersonSchema.setDeathDate(request.getParameter("DeathDate"));
  tLDPersonSchema.setSmokeFlag(request.getParameter("SmokeFlag"));
  tLDPersonSchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));
  tLDPersonSchema.setProterty(request.getParameter("Proterty"));
  tLDPersonSchema.setRemark(request.getParameter("Remark"));
  tLDPersonSchema.setState(request.getParameter("State"));
	tLDPersonSchema.setGrpName(request.getParameter("GrpName"));
	tLDPersonSchema.setEnglishName(request.getParameter("EnglishName"));
	tLDPersonSchema.setNativeCity(request.getParameter("NativeCity"));
	//modify by zxs
	tLDPersonSchema.setAuthorization(request.getParameter("InsuredAuth"));
	if(request.getParameter("IDType").equals("a")||request.getParameter("IDType").equals("b")){
	tLDPersonSchema.setOriginalIDNo(request.getParameter("InsuredPassIDNo"));
	}
						
	//地址信息
	tLCAddressSchema.setCustomerNo(request.getParameter("InsuredNo"));
  //tLCAddressSchema.setAddressNo(request.getParameter("AddressNo"));  
  tLCAddressSchema.setPostalAddress(request.getParameter("PostalAddress"));
  tLCAddressSchema.setZipCode(request.getParameter("ZipCode"));
  tLCAddressSchema.setPhone(request.getParameter("Phone"));
  tLCAddressSchema.setFax(request.getParameter("Fax"));          
  tLCAddressSchema.setMobile(request.getParameter("Mobile"));        
  tLCAddressSchema.setEMail(request.getParameter("EMail"));
  tLCAddressSchema.setHomeAddress(request.getParameter("HomeAddress"));        
  tLCAddressSchema.setHomeZipCode(request.getParameter("HomeZipCode"));
  tLCAddressSchema.setHomePhone(request.getParameter("HomePhone"));
  tLCAddressSchema.setHomeFax(request.getParameter("HomeFax"));  
  tLCAddressSchema.setGrpName(request.getParameter("GrpName")); 
  tLCAddressSchema.setCompanyPhone(request.getParameter("GrpPhone"));
  tLCAddressSchema.setCompanyAddress(request.getParameter("GrpAddress"));
  tLCAddressSchema.setCompanyZipCode(request.getParameter("GrpZipCode"));
  tLCAddressSchema.setCompanyFax(request.getParameter("GrpFax"));
  tLCAddressSchema.setPostalCity(request.getParameter("City"));
  tLCAddressSchema.setPostalCommunity(request.getParameter("PostalCommunity"));
  tLCAddressSchema.setPostalCounty(request.getParameter("County"));
  tLCAddressSchema.setPostalProvince(request.getParameter("Province"));
  tLCAddressSchema.setPostalStreet(request.getParameter("PostalStreet"));
  
  //抵达国家
	LCNationSet tLCNationSet = new LCNationSet();
  String[] tNationNo = request.getParameterValues("NationGrid1");//国家代码
  String[] tNationName = request.getParameterValues("NationGrid2");
  int MulCount = 0;
  if (tNationNo != null)    MulCount = tNationNo.length;
  for (int n = 0; n < MulCount; n++) {
    LCNationSchema tLCNationSchema = new LCNationSchema();
    tLCNationSchema.setGrpContNo("00000000000000000000");
    tLCNationSchema.setNationNo(tNationNo[n]);
    tLCNationSchema.setContNo(request.getParameter("ContNo"));
    tLCNationSchema.setChineseName(tNationName[n]);
    tLCNationSet.add(tLCNationSchema);
  }
  
  //套餐信息
  String chkRiskWrapGrid[] = request.getParameterValues("InpRiskWrapGridChk");
  if(chkRiskWrapGrid != null && chkRiskWrapGrid.length > 0)
  {
    int tWrapCol = Integer.parseInt(StrTool.cTrim(request.getParameter("RiskWrapCol")));
    int tWrapColFix = Integer.parseInt(StrTool.cTrim(request.getParameter("RiskWrapColFix")));
    
    String tRiskWrapCode[] = request.getParameterValues("RiskWrapGrid1");
    String tRiskCode[] = request.getParameterValues("RiskWrapGrid3");
    String tDutyCode[] = request.getParameterValues("RiskWrapGrid5");
    
    //循环行
    for (int index = 0; index < chkRiskWrapGrid.length; index++) 
    {
      //若行选中
	    if(chkRiskWrapGrid[index].equals("1")) 
	    {
	      //循环列，获取计算要素
	      for(int i = tWrapColFix; i < tWrapCol; i++) 
	      {
	        //从标志位Factor开始
	        if(StrTool.cTrim(request.getParameterValues("RiskWrapGrid" + i)[index]).equals("Factor")) 
	        {
	          LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
	          tLCRiskDutyWrapSchema.setRiskWrapCode(tRiskWrapCode[index]);
	          tLCRiskDutyWrapSchema.setCalFactor(request.getParameterValues("RiskWrapGrid" + (i + 1))[index]);
	          tLCRiskDutyWrapSchema.setCalFactorValue(request.getParameterValues("RiskWrapGrid" + (i + 2))[index]);
	          tLCRiskDutyWrapSchema.setRiskCode(tRiskCode[index]);
	          tLCRiskDutyWrapSchema.setDutyCode(tDutyCode[index]);
	          tLCRiskDutyWrapSet.add(tLCRiskDutyWrapSchema);
	        }
	      }
	    }
	  }
  }
  
  LCRiskDutyWrapSchema tLCRiskDutyWrapSchema = new LCRiskDutyWrapSchema();
  
  
  
  //告知信息  
  int lineCount = 0;
	String arrCount[] = request.getParameterValues("DisDesbGridNo");

	if(arrCount!=null)
	{
		String tCustomerNo[]=request.getParameterValues("DisDesbGrid1");
		String tDisDesb[]=request.getParameterValues("DisDesbGrid3");
		String tDisResult[]=request.getParameterValues("DisDesbGrid4");
		String tICDCode[]=request.getParameterValues("DisDesbGrid5");
		String tSerialNo[]=request.getParameterValues("DisDesbGrid6");
		String tDiseaseCode[]=request.getParameterValues("DisDesbGrid8");
		String tRemark[]=request.getParameterValues("DisDesbGrid10");
		
		lineCount = arrCount.length;
		System.out.println("lineCount="+lineCount);
		for(int i = 0;i<lineCount;i++)
		{
			LCDiseaseResultSchema tLCDiseaseResultSchema = new LCDiseaseResultSchema();
			tLCDiseaseResultSchema.setContNo(request.getParameter("ContNo"));
			tLCDiseaseResultSchema.setProposalContNo(request.getParameter("ProposalContNo"));
			tLCDiseaseResultSchema.setDiseaseSource("1");          //告知整理信息
			tLCDiseaseResultSchema.setCustomerNo(tCustomerNo[i]);
			tLCDiseaseResultSchema.setDisDesb(tDisDesb[i]);
			tLCDiseaseResultSchema.setSerialNo(tSerialNo[i]);
			System.out.println("tDisDesb="+tDisDesb[i]);
			System.out.println("tDisResult="+tDisResult[i]);
			System.out.println("tICDCode="+tICDCode[i]);
			tLCDiseaseResultSchema.setDisResult(tDisResult[i]);
			tLCDiseaseResultSchema.setICDCode(tICDCode[i]);
			tLCDiseaseResultSchema.setRemark(tRemark[i]);
			tLCDiseaseResultSchema.setDiseaseCode(tDiseaseCode[i]);
			tLCDiseaseResultSet.add(tLCDiseaseResultSchema);
		}
	}
    
    //客户告知
    String tImpartNum[] = request.getParameterValues("ImpartGridNo");
    String tImpartVer[] = request.getParameterValues("ImpartGrid1");            //告知版别
    String tImpartCode[] = request.getParameterValues("ImpartGrid2");           //告知编码
    String tImpartContent[] = request.getParameterValues("ImpartGrid3");        //告知内容
    String tImpartParamModle[] = request.getParameterValues("ImpartGrid4");    //告知客户号码
    LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
    if(tImpartCode != null && tImpartCode.length > 0)
    {
        for (int i = 0; i < tImpartNum.length; i++)    
        {
            LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
            
            tLCCustomerImpartSchema.setProposalContNo(request.getParameter("ContNo"));
            tLCCustomerImpartSchema.setCustomerNoType("I");
            //tLCCustomerImpartSchema.setCustomerNo(tInsuredID);
            //tLCCustomerImpartSchema.setImpartVer(tImpartVer[i]);
            tLCCustomerImpartSchema.setImpartVer("001");
            tLCCustomerImpartSchema.setImpartCode(tImpartCode[i]);
            tLCCustomerImpartSchema.setImpartContent(tImpartContent[i]);
            tLCCustomerImpartSchema.setImpartParamModle(tImpartParamModle[i]);
            //tLCCustomerImpartSchema.setDiseaseContent(tImpartContent[i]);
            tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
        }
    }

    //客户告知明细
    String tImpartDetailNum[] = request.getParameterValues("ImpartDetailGridNo");
    String tImpartDetailVer[] = request.getParameterValues("ImpartDetailGrid1");            //告知版别
    String tImpartDetailCode[] = request.getParameterValues("ImpartDetailGrid2");           //告知编码
    String tImpartDetailContent[] = request.getParameterValues("ImpartDetailGrid3");        //告知内容
    String tImpartDetailDiseaseContent[] = request.getParameterValues("ImpartDetailGrid4");    //告知客户号码  
                    String tImpartDetailStartDate[] = request.getParameterValues("ImpartDetailGrid5");      
                    String tImpartDetailEndDate[] = request.getParameterValues("ImpartDetailGrid6");
                String tImpartDetailProver[] = request.getParameterValues("ImpartDetailGrid7");    
                String tImpartDetailCurrCondition[] = request.getParameterValues("ImpartDetailGrid8");  
                String tImpartDetailIsProved[] = request.getParameterValues("ImpartDetailGrid9");          
    LCCustomerImpartDetailSet tLCCustomerImpartDetailSet = new LCCustomerImpartDetailSet();
    if(tImpartDetailNum != null && tImpartDetailNum.length > 0)
    {
        for (int i = 0; i < tImpartDetailNum.length; i++)  
        {
            LCCustomerImpartDetailSchema tLCCustomerImpartDetailSchema = new LCCustomerImpartDetailSchema();

            tLCCustomerImpartDetailSchema.setProposalContNo(request.getParameter("ContNo"));
            tLCCustomerImpartDetailSchema.setCustomerNoType("I");
            //tLCCustomerImpartDetailSchema.setCustomerNo(tInsuredID);
            //tLCCustomerImpartDetailSchema.setImpartVer(tImpartDetailVer[i]);
            tLCCustomerImpartDetailSchema.setImpartVer("001");
            tLCCustomerImpartDetailSchema.setImpartCode(tImpartDetailCode[i]);
            tLCCustomerImpartDetailSchema.setImpartDetailContent(tImpartDetailContent[i]);
            //tLCCustomerImpartDetailSchema.setImpartParamModle(tImpartDetailParamModle[i]);
            tLCCustomerImpartDetailSchema.setDiseaseContent(tImpartDetailDiseaseContent[i]);
            tLCCustomerImpartDetailSet.add(tLCCustomerImpartDetailSchema);
        }
    }
  
  // 受益人信息
  LCBnfSet tLCBnfSet = new LCBnfSet();
  String tBnfNum[] = request.getParameterValues("BnfGridNo");
  String tBnfType[] = request.getParameterValues("BnfGrid1");
  String tName[] = request.getParameterValues("BnfGrid2");
  String tIDType[] = request.getParameterValues("BnfGrid3");
  String tIDNo[] = request.getParameterValues("BnfGrid4");
  String tSex[] = request.getParameterValues("BnfGrid5");
  String tBnfRelationToInsured[] = request.getParameterValues("BnfGrid6");
  String tBnfLot[] = request.getParameterValues("BnfGrid7");
  String tBnfGrade[] = request.getParameterValues("BnfGrid8");
  String tNativePlace[] = request.getParameterValues("BnfGrid9");
  String tOccupationCode[] = request.getParameterValues("BnfGrid10");
  String tPhone[] = request.getParameterValues("BnfGrid11");
  String tAddress[] = request.getParameterValues("BnfGrid12");
  String tIDStartDate[] = request.getParameterValues("BnfGrid13");
  String tIDEndDate[] = request.getParameterValues("BnfGrid14");
  int BnfCount = 0;
  if (tBnfNum != null)
    BnfCount = tBnfNum.length;
  for (int i = 0; i < BnfCount; i++) {
    if (tName[i] == null || tName[i].equals(""))
      break;
    LCBnfSchema xLCBnfSchema = new LCBnfSchema();
    xLCBnfSchema.setBnfType(tBnfType[i]);
    xLCBnfSchema.setName(tName[i].trim());
    xLCBnfSchema.setSex(tSex[i]);
    xLCBnfSchema.setIDType(tIDType[i]);
    xLCBnfSchema.setIDNo(tIDNo[i]);
    
    //将受益人的身份证转换成出生日期                    by zhangyang 2011-04-08
    if(tIDType[i] != null && !tIDType[i].equals("") && tIDType[i].equals("0")
    	&& tIDNo[i] != null && !tIDNo[i].equals(""))
    {
    	String tBnfBirthDay = PubFun.getBirthdayFromId(tIDNo[i]);
    	System.out.println("受益人的出生日期为：" + tBnfBirthDay);
    	xLCBnfSchema.setBirthday(tBnfBirthDay);
    } else {
    	xLCBnfSchema.setBirthday("");
    }
    //---------------------------------------------------------
    
    xLCBnfSchema.setRelationToInsured(tBnfRelationToInsured[i]);
    xLCBnfSchema.setBnfLot(tBnfLot[i]);
    xLCBnfSchema.setBnfGrade(tBnfGrade[i]);
    xLCBnfSchema.setNativePlace(tNativePlace[i]);
    xLCBnfSchema.setOccupationCode(tOccupationCode[i]);
    xLCBnfSchema.setPhone(tPhone[i]);
    xLCBnfSchema.setPostalAddress(tAddress[i]);
    xLCBnfSchema.setIDStartDate(tIDStartDate[i]);
    xLCBnfSchema.setIDEndDate(tIDEndDate[i]);
    tLCBnfSet.add(xLCBnfSchema);
  }
          
  String SavePolType="";
  String BQFlag=request.getParameter("BQFlag");
  if(BQFlag==null) 
  {
    SavePolType="0";
  }
  else if(BQFlag.equals("")) 
  {
    SavePolType="0";
  }
  else  
  {
    SavePolType=BQFlag;  
  }
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("SavePolType",SavePolType); //保全保存标记，默认为0，标识非保全  
  tTransferData.setNameAndValue("ContNo",request.getParameter("ContNo")); 
  tTransferData.setNameAndValue("FamilyType",request.getParameter("FamilyType"));//家庭单标记 
  tTransferData.setNameAndValue("ContType",request.getParameter("ContType"));//团单，个人单标记
  tTransferData.setNameAndValue("PolTypeFlag",request.getParameter("PolTypeFlag"));//无名单标记
  tTransferData.setNameAndValue("InsuredPeoples",request.getParameter("InsuredPeoples"));//被保险人人数
  tTransferData.setNameAndValue("InsuredAppAge",request.getParameter("InsuredAppAge"));//被保险人年龄
  tTransferData.setNameAndValue("SequenceNo",request.getParameter("SequenceNo"));//
  tTransferData.setNameAndValue("OldContNo",request.getParameter("oldContNo"));
  tTransferData.setNameAndValue("LoadFlag",request.getParameter("LoadFlag"));
  tTransferData.setNameAndValue("Bonusgetmode",request.getParameter("BonusGetMode"));
  //modify by zxs
  tTransferData.setNameAndValue("InsuredAuth",request.getParameter("InsuredAuth"));
  if(request.getParameter("IDType").equals("a")||request.getParameter("IDType").equals("b")){
  tTransferData.setNameAndValue("InsuredPassIDNo",request.getParameter("InsuredPassIDNo"));
  } else{
	  tTransferData.setNameAndValue("InsuredPassIDNo","");  
  }
  System.out.println("ContType"+request.getParameter("ContType"));
  if (fmAction.equals("UPDATE||CONTINSURED")||fmAction.equals("DELETE||CONTINSURED"))
  {
    String tRadio[] = request.getParameterValues("InpInsuredGridSel");
    String tInsuredNo[] = request.getParameterValues("InsuredGrid1");
    System.out.println("**************2"+tRadio);
    System.out.println("**************1"+tInsuredNo);
    
    if (tRadio!=null)
    {
      for (int index=0; index< tRadio.length;index++)
      {
        if(tRadio[index].equals("1"))
        {
          tOLDLCInsuredDB.setContNo(request.getParameter("ContNo"));
          tOLDLCInsuredDB.setGrpContNo(request.getParameter("GrpContNo"));
          tOLDLCInsuredDB.setInsuredNo(tInsuredNo[index]);
          System.out.println("tInsuredNo" + tInsuredNo[index]);
        }
        if (tOLDLCInsuredDB.getInsuredNo()==null)
        {
          tOLDLCInsuredDB.setContNo(request.getParameter("ContNo"));
          tOLDLCInsuredDB.setGrpContNo(request.getParameter("GrpContNo"));
          tOLDLCInsuredDB.setInsuredNo(tInsuredNo[0]);
          System.out.println("tInsuredNo" + tInsuredNo[0]);
        }
      }
    }
    else
    {
      tOLDLCInsuredDB.setContNo(request.getParameter("ContNo"));
      tOLDLCInsuredDB.setGrpContNo(request.getParameter("GrpContNo"));
      tOLDLCInsuredDB.setInsuredNo(tInsuredNo[0]);
      System.out.println("tInsuredNo" + tInsuredNo[0]);
    }
  }
            
  try
  {
    // 准备传输数据 VData
    System.out.println("tLDPersonSchema2"+tLDPersonSchema);
    VData tVData = new VData();
    tVData.add(tLCContSchema);
    tVData.add(tLDPersonSchema);         
    tVData.add(tmLCInsuredSchema);
    tVData.add(tLCAddressSchema);
    tVData.add(tOLDLCInsuredDB);
  	tVData.add( tLCNationSet );
    tVData.add(tLCRiskDutyWrapSet);
    tVData.add(tLCDiseaseResultSet);
    tVData.add(tLCBnfSet);
    
    tVData.add(tLCCustomerImpartSet);
    tVData.add(tLCCustomerImpartDetailSet);
    
    tVData.add(tTransferData);
    tVData.add(tGI);
    
      
     //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    if (!tContInsuredIntlUI.submitData(tVData,fmAction))
    {
      FlagStr = "Fail";
      Content = "保存失败，原因是:" + tContInsuredIntlUI.mErrors.getFirstError();
    }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tContInsuredIntlUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println("FlagStr:"+FlagStr+"Content:"+Content);
  
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

