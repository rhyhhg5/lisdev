<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>	  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="ProposalApprove.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ProposalApproveInit.jsp"%>
  <title>投保单复核 </title>
  <!-- 
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT LANGUAGE="javascript" FOR="document" EVENT="onkeydown">
    document_onkeydown();
  </SCRIPT>
  -->
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  Operator = "<%=tGlobalInput.Operator%>";
  ComCode = "<%=tGlobalInput.ComCode%>";
</script>

<body  onload="initForm();" >
  <form action="./ProposalApproveSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      <!--    <TD  class= title>
            投保单号码
          </TD>-->
            <Input class= common name=ProposalNo type="hidden">
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo  verify="印刷号码|int">
          </TD>
          <TD  class= title>
            录入日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=MakeDate verify=" 录入日期|date">
          </TD>
        </TR>
        
        <TR  class= common>         
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
						<Input class="codeNo"  name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
          </TD>
          <TD  class= title>
            业务员代码
          </TD>
          <TD  class= input>
            <Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCodet" ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
          </TD>
          <!--
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
          	-->
            <Input type=hidden class="codeNo" name=AgentGroup verify="代理人组别|code:branchcode" ondblclick="return showCodeList('agentgroup1',[this,AgentGroupName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('agentgroup1',[this,AgentGroupName],[0,1]);"><input type=hidden class=codename name=AgentGroupName readonly=true >
          </TD>
        </TR>
        
        <TR  class= common>         
          <TD  class= title>
            外包错误反馈
          </TD>
          <TD  class= input>
						<input class=codeno name=HaseBPOErr verify="|len<=20"  CodeData="0|^0|无^1|有" ondblclick="return showCodeListEx('HaseBPOErr',[this,HaseBPOErrName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('HaseBPOErr',[this,HaseBPOErrName],[0,1],null,null,null,1);"><input class=codename name=HaseBPOErrName>
          </TD>
          <TD  class= title>
          </TD>
          <TD  class= input>
          </TD>
        </TR>

    </table>
          <INPUT class=cssButton VALUE="查  询" TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>			
  	</div>
    <p>
      <INPUT class=cssButton VALUE="投保单复核" TYPE=button onclick="showApproveDetail();"> 
    </p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
