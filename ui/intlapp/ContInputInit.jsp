<%
//程序名称：ContInputInit.jsp
//程序功能：
//创建日期：2007-6-18 14:02
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。

  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
  
  var agentcomCode = "agentcombank";
  
	var tCurrentDate = "<%=tCurrentDate%>";
	var tCurrentTime = "<%=tCurrentTime%>";
	
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var scantype = "<%=request.getParameter("scantype")%>";
	var tProcessID = "<%=request.getParameter("ProcessID")%>";
	var tActivityID = "<%=request.getParameter("ActivityID")%>";
	var tMissionID = "<%=request.getParameter("MissionID")%>";
	var tSubMissionID = "<%=request.getParameter("SubMissionID")%>";
	var ManageCom = "<%=request.getParameter("ManageCom")%>";
	var PolApplyDate = "<%=request.getParameter("PolApplyDate")%>";
	var CurrentDate = "<%=PubFun.getCurrentDate()%>";
  var AgentType = "<%=request.getParameter("AgentType")%>";
	var AgentCom = "<%=request.getParameter("AgentCom")%>";
	var type = "<%=request.getParameter("type")%>";
	var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
	if (ScanFlag == "null") 
	{
	  ScanFlag = "1";
	}
	var scantype = "<%=request.getParameter("scantype")%>";		

	var LoadFlag = "<%=request.getParameter("LoadFlag")%>";
	if( LoadFlag == null || LoadFlag == "" || LoadFlag == "null")
	{
		LoadFlag = "1";
	}
	var tCardFlag = "<%=request.getParameter("CardFlag")%>";  //7：国际业务；8：万能险
	var tIntlFlag = "<%=request.getParameter("IntlFlag")%>";  //0：非国际业务；1：国际业务
	if(tIntlFlag == null || tIntlFlag == "" || tIntlFlag == "null")
	{
	  tIntlFlag = "0";  
	}
	else if(tIntlFlag == "1")
	{
	  tCardFlag = "7";
	}
	var tVideoFlag = "<%=request.getParameter("VideoFlag")%>";
	var tXSFlag = "<%=request.getParameter("XSFlag")%>";
	
function initForm()
{
  try
  {
    //initGlobalvary();
    fm.AppntAuth.value='1';
    initButton();
    initInputBox();
    queryContInfo();
    initExtend();
    var LoadFlag1 = "<%=request.getParameter("LoadFlag")%>";
    if(LoadFlag1=="5")
    {
    	fm.all("AppntAuth").verify="授权使用客户信息|code:Auth";
    	fm.all("AppntAuth").elementtype='';
    	fm.all("AppntAuthName").elementtype='';
    }
    
    showAllCodeName();
  }
  catch(ex)
  {
    alert("在初始化过程中出错："+ex.message)
  }
}
</script>
