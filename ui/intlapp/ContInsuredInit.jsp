
<%
//程序名称：ContInsuredInit.jsp
//程序功能：
//创建日期：2007-6-21 10:06
//创建人  ：yangyalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String CurrentDate = PubFun.getCurrentDate();
  String CurrentTime = PubFun.getCurrentTime();
%>
                      
<script language="JavaScript">

var intlContInsuredInput = 1;

var tIntlFlag = "";  //国际业务标志：0：菲国际业务；1：国际业务
var tCardFlag = "";  //卡单标志：6：银行保险；7：国际业务；8：简易万能险
var tWrapType = "";  //套餐类型，详见LDWrap描述
  
function initInpBox()
{ 
  try
  { 
  	fm.SelPolNo.value='';
  	
  	var sql = "select AppntNo, AppntName, CardFlag, IntlFlag "
  	        + "from LCCont "
  	        + "where PrtNo = '" + prtNo + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      fm.AppntNo.value = rs[0][0];
      fm.AppntName.value = rs[0][1];
      tCardFlag = rs[0][2];
      tIntlFlag = rs[0][3];
      
      tWrapType = tCardFlag;
      if("1" == tIntlFlag)
      {
        tWrapType = "9";
      }
    }
    setSubType();
  }
  catch(ex)
  {
    alert("在ContInsuredInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
	 
   if(checktype=="1")<!--默认是第一被保险-->
   {
     param="121";
     fm.pagename.value="121";     
   }
   if(checktype=="2")
   {
     param="22";
     fm.pagename.value="22";     
   }   
  }
  catch(ex)
  {
    alert("在ContInsuredInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  { 
	  //modify by zxs
	fm.InsuredAuth.value = '1';
    initGlobalvary();
    initInpBox();
    initSelBox();    
    initGrpInfo();
    initInsuredGrid();
    initWrapGrid();
    initRiskWrapGrid("");
    initDisDesbGrid();
    
    // initImpartGrid();
    // initImpartDetailGrid();
    
    //抵达国家
    initNationGrid();
    //getNationInfo();  如果境外救援也走本平台，则需要放开
    
    
    
    queryWrapGrip();
    
    if (scantype== "scan") 
    {  
      //setFocus(); 
    }
    
    getContInfo(); 
    getInsuredInfo();
    
    // getImpartbox();
    getImpartAll();
    
    //查询受益人必须在被保人查询之后
    initBnfGrid();
    queryBnfGrid();
    initButton();
    
    if(fm.all('FamilyType').value==''||fm.all('FamilyType').value==null||fm.all('FamilyType').value=='0'||fm.all('FamilyType').value=='false'){  
      fm.all('FamilyType').value='0';
    }
  }
  
  catch(re)
  {
    alert("ContInsuredInit.jsp-->InitForm函数中发生异常:初始化界面错误!" + re.message);
  }
  
  if(LoadFlag=="1")
  {
    fm.all('FamilyType').value='1';     
    divSamePerson.style.display="";
    fm.PolTypeFlag.value='0';    
  }
  if(LoadFlag=="5")
  {
	  //modify by zxs
	  fm.all("InsuredAuth").verify="授权使用客户信息|code:Auth";
	  fm.all("InsuredAuth").elementtype='';
	  fm.all("InsuredAuthName").elementtype='';
    fm.all("inputConfirmButtonID").style.display = "none";
    fm.all("approveConfirmButtonID").style.display = "";
    if(fm.all('ManageCom').value.length>=4){
       if(fm.all('ManageCom').value.substring(0,4)=='8644'){
      	  fm.all("AudioAndVideo").style.display="";
      	  fm.VideoFlag.value=tVideoFlag;
       }
    }
  }
  showAllCodeName();   
	
	
	initBonusgetmode();//初始化红利领取方式
}


// 暂收费信息列表的初始化
function initInsuredGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="被保险人客户号";          		//列名
      iArray[1][1]="80px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      iArray[1][21]="InsuredNo"; 

      iArray[2]=new Array();
      iArray[2][0]="姓名";         			//列名
      iArray[2][1]="60px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="性别";      	   		//列名
      iArray[3][1]="40px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=2;              	//是否允许输入,1表示允许，0表示不允许
      iArray[3][4]="Sex"; 


      iArray[4]=new Array();
      iArray[4][0]="出生日期";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0; 
      
      iArray[5]=new Array();
      iArray[5][0]="与第一被保险人关系";      	   		//列名
      iArray[5][1]="100px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=2; 
      iArray[5][4]="Relation";              	        //是否引用代码:null||""为不引用
      iArray[5][9]="与主被保险人关系|code:Relation&NOTNULL";
      //iArray[5][18]=300;
      
      iArray[6]=new Array();
      iArray[6][0]="客户内部号码";      	   		//列名
      iArray[6][1]="80px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=2; 
      iArray[6][10]="MutiSequenceNo";
      iArray[6][11]="0|^1|第一被保险人 ^2|第二被保险人 ^3|第三被保险人";
            

      InsuredGrid = new MulLineEnter( "fm" , "InsuredGrid" ); 
      //这些属性必须在loadMulLine前
      InsuredGrid.mulLineCount = 0;   
      InsuredGrid.displayTitle = 1;
      InsuredGrid.canSel =1;
      InsuredGrid. selBoxEventFuncName ="queryInsrued" ;     //点击RadioBox时响应的JS函数
      InsuredGrid. hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      InsuredGrid. hiddenSubtraction=1;
      InsuredGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

//被保人险种信息列表初始化
function initWrapGrid(){
    var iArray = new Array();
      
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		        //列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="套餐编码";         		//列名
      iArray[1][1]="30px";            		        //列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  	  iArray[1][21]="RiskWrapCode";
      
      iArray[2]=new Array();
      iArray[2][0]=" 套餐名称";         		//列名
      iArray[2][1]="80px";            		        //列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
  	  iArray[2][21]="WrapName";           			//是否允许输入,1表示允许，0表示不允许           
      
      WrapGrid = new MulLineEnter( "fm" , "WrapGrid" ); 
      WrapGrid.mulLineCount = 0;   
      WrapGrid.displayTitle = 1;
      WrapGrid.canChk =1;
      WrapGrid. hiddenPlus=1;
      WrapGrid. hiddenSubtraction=1;
      WrapGrid.loadMulLine(iArray);  
    }
    catch(ex) {
      alert(ex);
    }
}

// 保障计划列表的初始化
function initRiskWrapGrid(riskWrapCodes)
{
  riskWrapCodes = ((riskWrapCodes == null || riskWrapCodes == "")? "''" : riskWrapCodes);
  
  try{
    var strsql = "select distinct 'Factor',ChooseFlag,CalFactor,CalFactorName,function,factorType "
                +"from LDRiskDutyWrap a,LDWrap b "
                +"where a.RiskWrapCode=b.RiskWrapCode "
                + "   and b.WrapType = '" + tWrapType + "' "
                + "   and b.RiskWrapCode in (" + riskWrapCodes + ") ";

  	var arr = easyExecSql(strsql);
  	if(!arr){
  		arr = new Array();
  		arr[0] = new Array();
  	}
  	
  	//var displayFlag = ("8" == tCardFlag ? "3" : "0");
  	var displayFlag = (tIntlFlag == 1 ? "0" : "3");
  	
  	var iArray = new Array();
  	iArray[0]=new Array();
  	iArray[0][0]="序号";
  	iArray[0][1]="30px";
  	iArray[0][2]=10;
  	iArray[0][3]=0;
  	
  	iArray[1]=new Array();
  	iArray[1][0]="套餐编码";
  	iArray[1][1]="50px";
  	iArray[1][2]=10;
  	//iArray[1][3]=displayFlag;
  	iArray[1][3]=0;
  	iArray[1][21]="RiskWrapCode";
  	
  	iArray[2]=new Array();
  	iArray[2][0]="套餐名称";
  	iArray[2][1]="150px";
  	iArray[2][2]=10;
  	//iArray[2][3]=displayFlag;
  	iArray[2][3]=0;
  	iArray[2][21]="WrapName";
  	
  	iArray[3]=new Array();
  	iArray[3][0]="险种编码";
  	iArray[3][1]="50px";
  	iArray[3][2]=10;
  	//iArray[3][3]= ("6" == tWrapType || "8" == tWrapType ? 3 : 0);
  	iArray[3][3]= displayFlag;
  	iArray[3][21]="RiskCode";
  	
  	iArray[4]=new Array();
  	iArray[4][0]="险种名称";
  	iArray[4][1]="250px";
  	iArray[4][2]=10;
  	//iArray[4][3]= ("6" == tWrapType || "8" == tWrapType ? 3 : 0);
  	iArray[4][3]= displayFlag;
  	iArray[4][21]="RiskName";
  	
  	iArray[5]=new Array();
  	iArray[5][0]="责任编码";
  	iArray[5][1]="50px";
  	iArray[5][2]=10;
  	//iArray[5][3]= ("6" == tWrapType || "8" == tWrapType ? 3 : displayFlag);
  	iArray[5][3]= displayFlag;
  	iArray[5][21]="DutyCode";
  	
  	iArray[6]=new Array();
  	iArray[6][0]="责任名称";
  	iArray[6][1]="150px";
  	iArray[6][2]=10;
  	//iArray[6][3]= ("6" == tWrapType || "8" == tWrapType ? 3 : displayFlag);
  	iArray[6][3]= displayFlag;
  	iArray[6][21]="DutyName";
  	
  	iArray[7]=new Array();
  	iArray[7][0]="折扣";
  	iArray[7][1]="50";
  	iArray[7][2]=10;
  	//iArray[7][3]= ("6" == tWrapType || "8" == tWrapType ? 3 : displayFlag);
  	iArray[7][3]= displayFlag;
  	iArray[7][21]="FloatRate";
  	
  	fm.RiskWrapColFix.value = 7;  //固定列数
  	
  	//if("9" == tWrapType || "8" == tWrapType)
  	if("1" == tIntlFlag)
  	{
    	strSqlMain = "select distinct a.RiskWrapCode,a.WrapName, "
    	            + "   b.RiskCode, (select RiskName from LMRisk where RiskCode = b.RiskCode), "
    	            + "   b.DutyCode, (select DutyName from LMDuty where DutyCode = b.DutyCode), "
    	            + "   (select replace(varchar(decimal(round(Prem/StandPrem, 2), 10, 2)), 'null', '') from LCPol where PrtNo = '" + fm.PrtNo.value + "'   and InsuredNo = '" + fm.InsuredNo.value + "'   and RiskCode = b.RiskCode) "
    }
    else
    {
      //除了国际业务，其他的套餐险种、责任和折扣都不需要
      strSqlMain = "select a.RiskWrapCode,a.WrapName, '000000' RiskCode, '000000', '000000' DutyCode, '000000', '000000' ";
    }
  	
  	var i = fm.RiskWrapColFix.value;
  	for(var m=0 ; m<arr.length ; m++)
  	{
  		for(var n=0 ; n<arr[m].length ; n++)
  		{
  			if(arr[m][n]=="Factor")
  			{
  				var display = 0;
  				if(arr[m][n+1] == "6")
  				{
  					display = 1;
  				}
  				if(arr[m][n+1] == "7")
  				{
  					display = 3;
  				}
  			}
  			else
  			{
  				break
  			}
  			var ChooseFlag = arr[m][n+1];
  			var CalFactor = arr[m][n+2];
  			var CalFactorName = arr[m][n+3];
  			var tFunction = arr[m][n+4];
  			var FactorType = arr[m][n+5];
  
  			i++;
  			iArray[i]=new Array();
  			iArray[i][0]="标志位";
  			iArray[i][1]="30px";
  			iArray[i][2]=10;
  			iArray[i][3]=3;
  			iArray[i][21]=ChooseFlag;
  			
  			i++;
  			iArray[i]=new Array();
  			iArray[i][0]="要素";
  			iArray[i][1]="30px";
  			iArray[i][2]=10;
  			iArray[i][3]=3;
  			iArray[i][21]=CalFactor;
  			
  			i++;
  			iArray[i]=new Array();
  			iArray[i][0]=CalFactorName
  			iArray[i][1]="60px";
  			iArray[i][2]=10;
  			iArray[i][3]=display;//控制是否显示此要素
  			iArray[i][21]=CalFactorName;//控制是否显示此要素
  			
  		  if(FactorType == "2")
  		  {
  		    //险种要素
  		  	strSqlMain += ",'Factor','"+CalFactor+"',''";
  		  }
  		  else if(FactorType == "1")
  		  {
  		  	strSqlMain += ",'Factor','"+CalFactor+"',(select ";
  				strSqlMain += tFunction;
  				strSqlMain += "(d."+CalFactor+") ";  //sql中可能查不到数据，导致包含null，此处转换为空串
  				
  		    //if("9" == tWrapType || "8" == tWrapType)
  		    if("1" == tIntlFlag)
  		    {
    		    strSqlMain +=  " from LCPol c, LCDuty d "
    		                   + "where c.PolNo = d.PolNo "
    		                   + "    and d.DutyCode = b.DutyCode "
    		                   + "    and c.prtno = '" + prtNo + "' "
    		                   + "    and c.InsuredNo = '" + fm.InsuredNo.value + "' "
    		                   + ") ";
  		    }
  		    else
  		    {
                if(CalFactor == "SupplementaryPrem")
                {
                    strSqlMain +=  ""
                        + " from LCPol d "
                        + " inner join LCRiskDutyWrap lcrdw on lcrdw.RiskCode = d.riskcode and lcrdw.ContNo = d.ContNo "
                        + " where lcrdw.CalFactor='SupplementaryPrem' "
                        + " and d.prtno='" + prtNo + "' "
                        + " and lcrdw.RiskWrapCode=a.RiskWrapCode "
                        + " and d.Riskcode in (select Riskcode from LDRiskWrap where RiskWrapCode=a.RiskWrapCode) "
                        + " ) "
                        ;
                }
                else if(CalFactor == "InitFeeRate")
                {
                    strSqlMain +=  ""
                        + " from LCPol d "
                        + " inner join LCRiskDutyWrap lcrdw on lcrdw.RiskCode = d.riskcode and lcrdw.ContNo = d.ContNo "
                        + " where lcrdw.CalFactor='InitFeeRate' "
                        + " and d.prtno='" + prtNo + "' "
                        + " and lcrdw.RiskWrapCode=a.RiskWrapCode "
                        + " and d.Riskcode in (select Riskcode from LDRiskWrap where RiskWrapCode=a.RiskWrapCode) "
                        + " ) "
                        ;
                }
                else
                {
    		      strSqlMain +=  " from LCPol c,LCRiskDutyWrap b,LCDuty d where c.ContNo=d.ContNo and b.DutyCode=d.DutyCode and b.CalFactor='"+CalFactor+"' and c.prtno='"+prtNo+"' and b.RiskWrapCode=a.RiskWrapCode and c.ContNo=b.ContNo and c.Riskcode=b.RiskCode and c.Riskcode in (select Riskcode from LDRiskWrap where RiskWrapCode=a.RiskWrapCode))";
                }
  		    }
  		  }
  		}
  	}
  	
  	//if("9" == tWrapType || "8" == tWrapType)
  	if("1" == tIntlFlag)
    {
  		strSqlMain += " from LDWrap a, LDRiskDutyWrap b "
                		+ "where a.RiskWrapCode = b.RiskWrapCode "
                		+ "   and a.WrapType ='" + tWrapType + "' "
                		+ "   and a.WrapProp='Y' ";
    }
    else
    {
      strSqlMain += " from LDWrap a where a.WrapType = '" + tWrapType + "' and a.WrapProp='Y' and RiskWrapCode<>'WR0350' ";
    }
              		
		fm.RiskWrapCol.value=i;  //最大列数
		
		RiskWrapGrid = new MulLineEnter( "fm" , "RiskWrapGrid" );
		//这些属性必须在loadMulLine前
		RiskWrapGrid.mulLineCount = 0;
		RiskWrapGrid.displayTitle = 1;
		RiskWrapGrid.locked = 0;
		RiskWrapGrid.canSel = 0;
		RiskWrapGrid.canChk = 1;
		RiskWrapGrid.hiddenPlus = 1;
		RiskWrapGrid.hiddenSubtraction = 1;
		RiskWrapGrid.loadMulLine(iArray);
	}
	catch (ex)
	{
		alert(ex.message);
	}
}

// 受益人信息列表的初始化
function initBnfGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";		//列宽
    iArray[0][2]=10;			//列最大值
    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="受益人类别"; 		//列名
    iArray[1][1]="50px";		//列宽
    iArray[1][2]=40;			//列最大值
    iArray[1][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="BnfType";
    iArray[1][9]="受益人类别|notnull&code:BnfType";
    iArray[1][21]="BnfType";

    iArray[2]=new Array();
    iArray[2][0]="姓名"; 	//列名
    iArray[2][1]="50px";		//列宽
    iArray[2][2]=30;			//列最大值
    iArray[2][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[2][9]="姓名|notnull&len<=20";//校验

    iArray[3]=new Array();
    iArray[3][0]="证件类型"; 		//列名
    iArray[3][1]="45px";		//列宽
    iArray[3][2]=40;			//列最大值
    iArray[3][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="IDType1";
    iArray[3][9]="证件类型|notnull&code:IDType1";

    iArray[4]=new Array();
    iArray[4][0]="证件号码"; 		//列名
    iArray[4][1]="120px";		//列宽
    iArray[4][2]=80;			//列最大值
    iArray[4][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[4][9]="证件号码|notnull&len<=20";
    
    iArray[5]=new Array();
    iArray[5][0]="性别"; 	//列名
    iArray[5][1]="25px";		//列宽
    iArray[5][2]=30;			//列最大值
    iArray[5][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[5][4]="sex";
    iArray[5][9]="性别|notnull&code:sex";//校验

    iArray[6]=new Array();
    iArray[6][0]="是被保险人的"; 	//列名
    iArray[6][1]="60px";		//列宽
    iArray[6][2]=60;			//列最大值
    iArray[6][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[6][4]="Relation";
    iArray[6][9]="与被保人关系|notnull&code:Relation";

    iArray[7]=new Array();
    iArray[7][0]="受益比例"; 		//列名
    iArray[7][1]="45px";		//列宽
    iArray[7][2]=40;			//列最大值
    iArray[7][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[7][9]="受益比例|notnull&Decimal&len<=10";
    iArray[7][21]="BnfLot";

    iArray[8]=new Array();
    iArray[8][0]="受益顺序"; 		//列名
    iArray[8][1]="45px";		//列宽
    iArray[8][2]=40;			//列最大值
    iArray[8][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[8][4]="OccupationType";
    iArray[8][9]="受益顺序|notnull&int&code:OccupationType";
    iArray[8][21]="BnfGrade";

    iArray[9]=new Array();
    iArray[9][0]="国籍"; 	//列名
    iArray[9][1]="30px";		//列宽
    iArray[9][2]=30;			//列最大值
    iArray[9][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[9][4]="NativePlace";
    iArray[9][9]="国籍|code:NativePlace";//校验
    
    iArray[10]=new Array();
    iArray[10][0]="职业"; 	//列名
    iArray[10][1]="40px";		//列宽
    iArray[10][2]=100;			//列最大值
    iArray[10][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[10][4]="occupationcode";
    iArray[10][9]="职业|code:occupationcode";
    iArray[10][18]=300;
    

    iArray[11]=new Array();
    iArray[11][0]="联系电话"; 		//列名
    iArray[11][1]="80px";		//列宽
    iArray[11][2]=100;			//列最大值
    iArray[11][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[11][9]="联系电话|len<=18";
    
    iArray[12]=new Array();
    iArray[12][0]="联系地址"; 		//列名
    iArray[12][1]="160px";		//列宽
    iArray[12][2]=100;			//列最大值
    iArray[12][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[12][9]="联系地址|len<=80";
    
    iArray[13]=new Array();
    iArray[13][0]="证件生效日期"; 		//列名
    iArray[13][1]="65px";		//列宽
    iArray[13][2]=100;			//列最大值
    iArray[13][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[13][9]="证件生效日期|Date&len<=80";
    
    iArray[14]=new Array();
    iArray[14][0]="证件失效日期"; 		//列名
    iArray[14][1]="65px";		//列宽
    iArray[14][2]=100;			//列最大值
    iArray[14][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[14][9]="证件失效日期|Date&len<=80";
    

    iArray[15]=new Array();
    iArray[15][0]="速填"; 		//列名
    iArray[15][1]="30px";		//列宽
    iArray[15][2]=30;			//列最大值
    iArray[15][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[15][4]="customertype";


    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" );
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 0;
    BnfGrid.displayTitle = 1;
    BnfGrid.addEventFuncName="addInit";
    BnfGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在ProposalInit.jsp-->initBnfGrid函数中发生异常:初始化界面错误!");
  }
}

function getContInfo()

{
    try { fm.all( 'ContNo' ).value = mSwitch.getVar( "ContNo" ); if(fm.all( 'ContNo' ).value=="false"){fm.all( 'ContNo' ).value="";} } catch(ex) { };
    try { fm.all( 'PrtNo' ).value = mSwitch.getVar( "PrtNo" ); } catch(ex) { };
    try { fm.all( 'ProposalContNo' ).value = mSwitch.getVar( "ProposalContNo" ); } catch(ex) { };   
    try { fm.all( 'GrpContNo' ).value = mSwitch.getVar( "GrpContNo" ); } catch(ex) { };
    try { fm.all( 'FamilyType' ).value = mSwitch.getVar( "FamilyType" ); } catch(ex) {};
    try { fm.all( 'PolTypeFlag' ).value = mSwitch.getVar( "PolType" );if(fm.all( 'PolTypeFlag' ).value=="false"){fm.all( 'PolTypeFlag' ).value="0";} } catch(ex) {};
    try { fm.all( 'InsuredPeoples' ).value = mSwitch.getVar( "InsuredPeoples" );if(fm.all( 'InsuredPeoples' ).value=="false"){fm.all( 'InsuredPeoples' ).value="";} } catch(ex) {};
}

function initGrpInfo()
{
	fm.PrtNo.value=mSwitch.getVar('PrtNo');
	fm.SaleChnl.value=mSwitch.getVar('SaleChnl');
	fm.ManageCom.value=mSwitch.getVar('ManageCom');
	fm.AgentCode.value=mSwitch.getVar('AgentCode');
	fm.AgentGroup.value=mSwitch.getVar('AgentGroup');

	fm.CValiDate.value=mSwitch.getVar('CValiDate');
	fm.ProposalGrpContNo.value = mSwitch.getVar('ProposalGrpContNo');
}

//抵达国家
function initNationGrid()
{
  var iArray = new Array();

  iArray[0]=new Array();
  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  iArray[0][1]="30px";         			//列宽
  iArray[0][2]=10;          			//列最大值
  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[1]=new Array();
  iArray[1][0]="国家代码";    	//列名
  iArray[1][1]="100px";            		//列宽
  iArray[1][2]=100;            			//列最大值
  iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
  iArray[1][4]="ldnation";
  iArray[1][5]="1|2";
  iArray[1][6]="0|1";
  iArray[1][9]="国家代码|len<=20";
  iArray[1][15]="chinesename";
  iArray[1][17]="2";
  iArray[1][18]=250;

  iArray[2]=new Array();
  iArray[2][0]="国家名称";    	//列名
  iArray[2][1]="100px";            		//列宽
  iArray[2][2]=100;            			//列最大值
  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


  NationGrid = new MulLineEnter( "fm" , "NationGrid" );
  //这些属性必须在loadMulLine前
  NationGrid.mulLineCount = 0;
  NationGrid.displayTitle = 1;
  NationGrid.locked = 0;
  NationGrid.canSel = 0;
  NationGrid.canChk = 0;
  NationGrid.hiddenPlus = 0;
  NationGrid.hiddenSubtraction = 0;
  NationGrid.loadMulLine(iArray);
}

</script>
