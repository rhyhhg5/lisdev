
//程序名称：EsPicDownload.js
//程序功能：扫描单证下载的主页面JS
//创建日期：2005-08-25
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容


//               该文件中包含客户端需要处理的函数和事件

var arrDataSet 
var tArr;
var turnPage = new turnPageClass();

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //fm.hideOperate.value=mOperate;
  //if (fm.hideOperate.value=="")
  //{
  //  alert("操作控制数据丢失！");
  //}
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    window.focus();
  showInfo.close();
  fm.all("DownloadButton").disabled=false;
	fm.all("DownloadAllButton").disabled=false;
 	if (FlagStr == "Fail" )
	{
		//如果失败，则返回错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		//alert();
	}
	else
	{
		//如果提交成功，则执行查询操作
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		//easyQueryClick();
		//alert(1);
	}
  easyqueryClick();
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在EsPicDownload.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

function easyqueryClick()
{
	initCodeGrid();
	var strSQL = "select docid,doccode,numpages,makedate,maketime,scanoperator,"
					+ "managecom "
					+ " from ES_DOC_MAIN where 1=1 "
					+ getWherePart( 'ManageCom','ManageCom','like' )
					+ getWherePart( 'MakeDate' )
					+ getWherePart( 'ScanOperator' )
					+ getSubtype()
					+ getWherePart( 'DocCode' )
                    
                    // 仅保留8612（天津分公司及下属三级机构）信息
                    //+ " and ManageCom not like '8653%' "
                    //+ " and subStr(ManageCom, 1, 4) not in ('8653', '8621', '8694', '8635', '8637', '8632') "
                    //+ " and ManageCom like '8612%' "
                    + " and 1 = 2 "
                    // -----------------------------                    
					+ " and (State is null or State = '01') "  //已发送前置机待外包录入的扫描件不能修改
					+ " and docid not in (select docid from lcscandownload) and docid not in (select docid from es_doc_qc_main where qcflag<>'2' )"
					;	
	var tstrSQL = "select *"
					+ " from ES_DOC_MAIN where 1=1 "
					+ getMngCom()
					+ getWherePart( 'MakeDate' )
					+ getWherePart( 'ScanOperator' )
					+ getSubtype()
					+ getWherePart( 'DocCode' )
                    
                    // 仅保留8612（天津分公司及下属三级机构）信息
                    //+ " and ManageCom not like '8653%' "
                    //+ " and subStr(ManageCom, 1, 4) not in ('8653', '8621', '8694', '8635', '8637', '8632') "
                    //+ " and ManageCom like '8612%' "
                    + " and 1 = 2 "
                    // -----------------------------
                    
					+ " and (State is null or State = '01') "  //已发送前置机待外包录入的扫描件不能修改
					+ " and docid not in (select docid from lcscandownload) and docid not in (select docid from es_doc_qc_main where qcflag<>'2' ) "
					;	
	//alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  if (!turnPage.strQueryResult) {
    alert("未查询到满足条件的数据！");
    fm.EXESql.value="";
    fm.DownloadFlag.value="";
     return false;
  }	
	//设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  turnPage.pageLineNum = 15;
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = CodeGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL 
  
  
  //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,MAXSCREENLINES);
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    fm.EXESql.value=tstrSQL;
    fm.DownloadFlag.value="1";

} 
function easyqueryClick2()
{
	initCodeGrid();
	var strSQL = "select docid,doccode,numpages,makedate,maketime,scanoperator,"
					+ "managecom,(select operator from lcscandownload where docid =ES_DOC_MAIN.docid),(select makedate from lcscandownload where docid =ES_DOC_MAIN.docid) b "
					+ " from ES_DOC_MAIN where 1=1 "
					+ getWherePart( 'ManageCom','ManageCom','like' )
					+ getWherePart( 'MakeDate' )
					+ getWherePart( 'ScanOperator' )
					+ getSubtype()
					+ getWherePart( 'DocCode' )
                    
                    // 仅保留8612（天津分公司及下属三级机构）信息
                    //+ " and ManageCom not like '8653%' "
                    //+ " and subStr(ManageCom, 1, 4) not in ('8653', '8621', '8694', '8635', '8637', '8632') "
                    //+ " and ManageCom like '8612%' "
                    + " and 1 = 2 "
                    // -----------------------------
                    
					+ " and (State is null or State = '01') "  //已发送前置机待外包录入的扫描件不能修改
					+ " and docid in (select docid from lcscandownload) and makedate>'2006-02-01' order by b"	 
					;
	var tstrSQL = "select * "
					+ " from ES_DOC_MAIN where 1=1 "
					+ getMngCom()
					+ getWherePart( 'MakeDate' )
					+ getWherePart( 'ScanOperator' )
					+ getSubtype()
					+ getWherePart( 'DocCode' )
                    
                    // 仅保留8612（天津分公司及下属三级机构）信息
                    //+ " and ManageCom not like '8653%' "
                    //+ " and subStr(ManageCom, 1, 4) not in ('8653', '8621', '8694', '8635', '8637', '8632') "
                    //+ " and ManageCom like '8612%' "
                    + " and 1 = 2 "
                    // -----------------------------
                    
					+ " and (State is null or State = '01') "  //已发送前置机待外包录入的扫描件不能修改
					+ " and docid in (select docid from lcscandownload) and makedate>'2006-02-01'"	  
					;
	//alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  if (!turnPage.strQueryResult) {
    alert("未查询到满足条件的数据！");
    fm.EXESql.value="";
    fm.DownloadFlag.value="";
     return false;
  }	
	//设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  turnPage.pageLineNum = 15;
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = CodeGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL 
  
  
  //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,MAXSCREENLINES);
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    fm.EXESql.value=tstrSQL;
    fm.DownloadFlag.value="2";
} 
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
//获取扫描件类型
function getSubtype()
{
	var tSubType=fm.conttype.value;
	var tGetSubtype="";
  if (tSubType=="1")
  {
    tGetSubtype=" and subtype='TB01' ";
  }else
  	{
  if (tSubType=="2")
  {
    tGetSubtype=" and subtype='TB02' ";
  }
  else
  {
    tGetSubtype=" and (subtype='TB01' or subtype='TB02' )";  
  }
}
  return tGetSubtype;
}

function DownloadPic()
{
	var i = 0;
	var flag = 0;
	//判定是否有选择打印数据
	for( i = 0; i < CodeGrid.mulLineCount; i++ )
	{
		if( CodeGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}
	//如果没有打印数据，提示用户选择
	if( flag == 0 )
	{
		alert("请先选择一条记录，再下载单证");
		return false;
	}
	var showStr="正在生成下载数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	fm.DownloadType.value="1";
	fm.all("DownloadButton").disabled=true;
	fm.submit();
}
function DownloadAllPic()
{
	if (fm.EXESql.value==""){
		alert("请先进行查询");
		return false;
		}
	var showStr="正在生成下载数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	fm.DownloadType.value="2";
	fm.all("DownloadAllButton").disabled=true;
	fm.submit();
}
function getMngCom()
{
	var tManageCom=fm.ManageCom.value;
	var cManageCom="";
  if (tManageCom=="")
  {
    cManageCom="";
  }
  else
  {
    cManageCom=" and ManageCom like '"+tManageCom+"%' ";  
  }
  return cManageCom;
}