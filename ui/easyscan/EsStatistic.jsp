<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：AppStatistic.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
PubFun tpubFun = new PubFun();
String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="EsStatistic.js"></SCRIPT>   
<script language="JavaScript">
	function initManageCom()
{
  try
  {
    fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
  }
  catch(re)
  {
    alert("EsStatistic.jsp-->initManageCom函数中发生异常:初始化界面错误!");
  }
}
	</script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initManageCom();">    
  <form action="./EsStatisticSub.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>          
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
    </table>

    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 