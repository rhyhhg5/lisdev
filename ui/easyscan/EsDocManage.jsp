<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：EsDocManage.jsp
//程序功能：
//创建日期：2004-06-02
//创建人  ：LiuQiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="EsDocManage.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="EsDocManageInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./EsDocManageSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%//@include file="../common/jsp/InputButton.jsp"%>
    
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 扫描单证信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divCode1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            DOC_ID
          </TD>
          <TD  class= text>
            <Input class= common name=DOC_ID >
          </TD>
          <TD  class= title>
            单证号码
          </TD>
          <TD  class= input>
            <Input class= common name=DOC_CODE >
          </TD>
          <TD  class= title>
            页数
          </TD>
          <TD  class= input>
            <Input class= common name=NUM_PAGES >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            扫描日期
          </TD>
          <TD  class= input>
            <Input class= common name=INPUT_DATE >
          </TD>
          <TD  class= title>
            扫描时间
          </TD>
          <TD  class= input>
            <Input class= common name=Input_Time >
          </TD>
          <TD  class= title>
            扫描员
          </TD>
          <TD  class= input>
            <Input class= common name=ScanOperator >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class= common name=ManageCom >
          </TD>
          <TD  class= title>
            单证状态
          </TD>
          <TD  class= input>
            <Input class= common name=DOC_FLAGE >
          </TD>
          <TD  class= title>
            状态_EX
          </TD>
          <TD  class= input>
            <Input class= common name=Doc_Ex_Flag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            录单员
          </TD>
          <TD  class= input>
            <Input class= common name=Operator >
          </TD>
          <TD  class= title>
            录单开始日期
          </TD>
          <TD  class= input>
            <Input class= common name=InputStartDate >
          </TD>
          <TD  class= title>
            录单开始时间
          </TD>
          <TD  class= input>
            <Input class= common name=InputStartTime >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            录单状态
          </TD>
          <TD  class= input>
            <Input class= common name=InputState >
          </TD>
          <TD  class= title>
            录单结束日期
          </TD>
          <TD  class= input>
            <Input class= common name=InputEndDate >
          </TD>
          <TD  class= title>
            录单结束时间
          </TD>
          <TD  class= input>
            <Input class= common name=InputEndTime >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单证备注
          </TD>
          <TD  class= input>
            <Input class= common name=DOC_REMARK >
          </TD>
        </TR>
        
      </table>
      <!--Input class=code name=ComCode ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);" >
      <!--Input class= common name=ComCode -->
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    
    <!-- ES_DOC_PAGES数据区 MultiLine -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCodeGrid);">
    		</td>
    		<td class= titleImg>
    			 扫描件单证页信息
    		</td>
    	</tr>
    </table>
    
    <INPUT VALUE="保存修改" TYPE=button onclick="saveUpdate()"> 	
    <INPUT VALUE="删除选中" TYPE=button onclick="deleteChecked()">
    
  	<Div  id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCodeGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      	<INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      	<INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      	<INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      	<INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
