<%
//程序名称：EsDocManageInit.jsp
//程序功能：
//创建日期：2004-06-02
//创建人  ：LiuQiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
String DocCode = request.getParameter("DocCode");
String SubType = request.getParameter("SubType");
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = '';
    fm.all('ScanOperator').value = '';
    fm.all('DocCode').value = '<%=request.getParameter("DocCode")%>';
    var DocCode='<%=DocCode%>';
    if(DocCode == "null" ||DocCode == ''||DocCode == null ){
      fm.all('DocCode').value ='';   
    }else{
      fm.all('DocCode').value =DocCode ;
    }
    
    var SubType='<%=SubType%>';
    if(SubType=='LP01'){
       fm.all('SubType').value= SubType;
       fm.all('SubTypeName').value="理赔申请书";   
    }
  }
  catch(ex)
  {
    alert("在CodeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在CodeInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initCodeGrid();

    fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value ==86){
    	fm.all('ManageCom').readOnly=false;
    }
    else{
    	fm.all('ManageCom').readOnly=true;
    }
  }
  catch(re)
  {
    alert("CodeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化CodeGrid
 ************************************************************
 */
var CodeGrid;          //定义为全局变量，提供给displayMultiline使用
function initCodeGrid()
{                               
    var iArray = new Array();
      
    try
    {
		iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列宽
        iArray[0][2]=100;            //列最大值
        iArray[0][3]=3;              //是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="扫描号";         //列名
        iArray[1][1]="40px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        iArray[1][21]="docId";
        
        iArray[2]=new Array();
        iArray[2][0]="单证号码";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        iArray[2][21]="docCode";

        iArray[3]=new Array();
        iArray[3][0]="页数";         //列名
        iArray[3][1]="25px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=1;         //是否允许录入，0--不能，1--允许
        iArray[3][21]="numPages";

        iArray[4]=new Array();
        iArray[4][0]="扫描日期";         //列名
        iArray[4][1]="60px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=1;         //是否允许录入，0--不能，1--允许
        iArray[4][21]="makeDate";
    
        iArray[5]=new Array();
        iArray[5][0]="扫描时间";         //列名
        iArray[5][1]="55px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        iArray[5][21]="makeTime";

		iArray[6]=new Array();
        iArray[6][0]="扫描员";         //列名
        iArray[6][1]="50px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        iArray[6][21]="scanOperator";
        
        iArray[7]=new Array();
        iArray[7][0]="机构";         //列名
        iArray[7][1]="50px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        iArray[7][21]="manageCom";

		iArray[8]=new Array();
        iArray[8][0]="状态";         //列名
        iArray[8][1]="60px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        iArray[8][21]="inputState";
        
		iArray[9]=new Array();
        iArray[9][0]="操作员";         //列名
        iArray[9][1]="50px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=1;         //是否允许录入，0--不能，1--允许
        iArray[9][21]="operator";
        
        iArray[10]=new Array();
        iArray[10][0]="录入日期";         //列名
        iArray[10][1]="60px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=1;         //是否允许录入，0--不能，1--允许
        iArray[10][21]="inputStartDate";
        
        iArray[11]=new Array();
        iArray[11][0]="录入时间";         //列名
        iArray[11][1]="55px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=1;         //是否允许录入，0--不能，1--允许
        iArray[11][21]="inputStartTime";
        
        iArray[12]=new Array();
        iArray[12][0]="单证类型";         //列名
        iArray[12][1]="50px";         //宽度
        iArray[12][2]=100;         //最大长度
        iArray[12][3]=1;         //是否允许录入，0--不能，1--允许
        iArray[12][21]="subType";
        
        iArray[13]=new Array();
        iArray[13][0]="单证类型描述";         //列名
        iArray[13][1]="75px";         //宽度
        iArray[13][2]=100;         //最大长度
        iArray[13][3]=1;         //是否允许录入，0--不能，1--允许
        iArray[13][21]="subTypeName";
        
        iArray[14]=new Array();
        iArray[14][0]="Busstype";         //列名
        iArray[14][1]="0px";         //宽度
        iArray[14][2]=100;         //最大长度
        iArray[14][3]=2;         //是否允许录入，0--不能，1--允许
        iArray[14][21]="bussType";
        
        iArray[15]=new Array();
        iArray[15][0]="归档号";         //列名
        iArray[15][1]="130px";         //宽度
        iArray[15][2]=100;         //最大长度
        iArray[15][3]=1;         //是否允许录入，0--不能，1--允许
        iArray[15][21]="ArchiveNo";
  
        CodeGrid = new MulLineEnter( "fm" , "CodeGrid" ); 

        //这些属性必须在loadMulLine前
        CodeGrid.mulLineCount = 10;   
        CodeGrid.displayTitle = 1;
        CodeGrid.canSel=1;
        CodeGrid.hiddenSubtraction=1;
        CodeGrid.hiddenPlus=1;
        CodeGrid.canChk=0;
        CodeGrid.loadMulLine(iArray);  

    }
    catch(ex)
    {
        alert("初始化CodeGrid时出错："+ ex);
    }
}
</script>