<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：EsPicDownload.jsp
//程序功能：扫描单证下载的Save页面
//创建日期：2005-08-22
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.easyscan.EsPicDownloadUI"%>
<%@page contentType="text/html;charset=GBK" %>
<%
String FlagStr = "";
String Content = "";
String tOperate = "";
//获得mutline中的数据信息
int nIndex = 0;
String tChecks[] = request.getParameterValues("InpCodeGridChk");
String tDocid[] = request.getParameterValues("CodeGrid1");
String tDocCode[] = request.getParameterValues("CodeGrid2");
String tManagecom[] = request.getParameterValues("CodeGrid7");
String tDownloadType = request.getParameter("DownloadType");
String tDownloadFlag=request.getParameter("DownloadFlag");
String tEXESql = request.getParameter("EXESql");
if(tDownloadFlag.equals("1"))
{
System.out.println("下载");
tOperate="DOWNLOAD";
}
if(tDownloadFlag.equals("2"))
{
System.out.println("下载2");
tOperate="REDOWNLOAD";
}
//获取操作人员信息
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
System.out.println("下载3");
EsPicDownloadUI tEsPicDownloadUI =new EsPicDownloadUI();
VData tVData = new VData();
ES_DOC_MAINSet tES_DOC_MAINSet=new ES_DOC_MAINSet();
ES_DOC_MAINSet mES_DOC_MAINSet=new ES_DOC_MAINSet();

if(tDownloadType.equals("1"))
{      System.out.println("下载4");
    for(nIndex = 0; nIndex < tChecks.length; nIndex++ )
    {
    System.out.println("下载33");
	   //If this line isn't selected, continue，如果没有选中当前行，则继续
	    if( !tChecks[nIndex].equals("1") )
	      {
	      continue;
	      }
	      ES_DOC_MAINSchema tES_DOC_MAINSchema=new ES_DOC_MAINSchema();
	      tES_DOC_MAINSchema.setDocID(tDocid[nIndex]);
	      tES_DOC_MAINSet.add(tES_DOC_MAINSchema);
		if(tES_DOC_MAINSet.size()>0)
		{
		System.out.println("下载");
		try
		{	
		tVData.add(tG);
		tVData.addElement(tES_DOC_MAINSet);
		if (!tEsPicDownloadUI.submitData(tVData,tOperate)){
			Content += "扫描件下载失败，原因是:" + tEsPicDownloadUI.mErrors.getFirstError()+"<BR>";
			FlagStr = "Fail";
			tEsPicDownloadUI.mErrors.clearErrors();
		}else{
			Content="扫描件下载成功！";			
			}	
	}
	catch(Exception ex)
	{
	System.out.println("下载失败"+ex.toString());
	Content = ex.toString();
	FlagStr = "Fail";
		}
	}
	else{
	Content = "下载失败";
	FlagStr = "Fail";	
		}	      
	 }
}
else{
    if(tDownloadType.equals("2"))
     {
    ES_DOC_MAINDB tES_DOC_MAINDB=new ES_DOC_MAINDB();
    tES_DOC_MAINSet=tES_DOC_MAINDB.executeQuery(tEXESql);
     }
     else
    	{
    	FlagStr = "Fail";
      Content = "不支持的下载类型";
    	}

System.out.println("下载");
		try
	{	

	if(tES_DOC_MAINSet.size()>0){
		for(int count=1;count<=tES_DOC_MAINSet.size();count++){
			ES_DOC_MAINSchema tES_DOC_MAINSchema=new ES_DOC_MAINSchema();
	    tES_DOC_MAINSchema=tES_DOC_MAINSet.get(count);
	    mES_DOC_MAINSet.add(tES_DOC_MAINSchema);
			tVData.add(tG);
			tVData.addElement(mES_DOC_MAINSet);
		if (!tEsPicDownloadUI.submitData(tVData,tOperate)){
			Content += "扫描件下载失败，原因是:" + tEsPicDownloadUI.mErrors.getFirstError()+"<BR>";
			FlagStr = "Fail";
			tEsPicDownloadUI.mErrors.clearErrors();
		}else{
			Content="扫描件下载成功！";			
				}
			}		
		}
	}
	catch(Exception ex)
	{
	System.out.println("下载失败"+ex.toString());
	Content = ex.toString();
	FlagStr = "Fail";
		}
	}

%>
<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	//	alert();
	var intPageWidth=screen.availWidth;
	var intPageHeight=screen.availHeight;
	var w=(intPageWidth-300)/2;
	var h=(intPageHeight-200)/2;
	//alert();     
	window.open("./PicDownload.jsp?&prtNo=downloadfile","print", "height=50,width=300,top="+h+",left="+w+",toolbar=yes,status=no,menubar=no,resizable=no,z-look=no");
		</script>
	<table>
		<tr>
			<td>
			</td>
		</tr>
	</table>

</html>