<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：EsDocManage.jsp
//程序功能：
//创建日期：2004-06-02
//创建人  ：LiuQiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="EsDocPagesSortMain.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="EsDocPagesSortMainInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./EsDocManageSave.jsp" method=post name=fm target="fraSubmit">
    <%//@include file="../common/jsp/OperateButton.jsp"%>
    <%//@include file="../common/jsp/InputButton.jsp"%>
    
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 扫描单证信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divCode1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= text>
            <Input class= common name=ManageCom >
          </TD>
          <TD  class= title>
            扫描日期
          </TD>
          <TD  class= input>
            <Input class= common name=MakeDate >
          <TD  class= title>
            扫描员
          </TD>
          <TD  class= input>
            <Input class= common name=ScanOperator >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单证类型
          </TD>
          <TD  class= input>
            <Input class= common name=SubType >
          </TD>
          <TD  class= title>
            单证号码
          </TD>
          <TD  class= input>
            <Input class= common name=DocCode >
          </TD>
          <TD>
          </TD>
          <TD>
          </TD>
        </TR>       
      </table>
      <!--Input class=code name=ComCode ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);" >
      <!--Input class= common name=ComCode -->
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <table>
    	<tr>
        	<td class=common>
			        <INPUT VALUE="理赔单证查询" TYPE=button class=cssButton onclick="easyqueryClick()"> 	
    		</td>
    	</tr>
    </table>
    
    <!-- ES_DOC_PAGES数据区 MultiLine -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCodeGrid);">
    		</td>
    		<td class= titleImg>
    			 扫描件单证页信息
    		</td>
    	</tr>
    </table>
        
    <INPUT VALUE="保存修改" TYPE=hidden class=cssButton onclick="saveUpdate()"> 	
    <INPUT VALUE="删除选中" TYPE=hidden class=cssButton onclick="deleteChecked()">
    
  	<Div align=center id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCodeGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<br>
      	<INPUT VALUE="首页" TYPE=button class=cssButton onclick="getFirstPage();"> 
      	<INPUT VALUE="上一页" TYPE=button class=cssButton onclick="getPreviousPage();"> 					
      	<INPUT VALUE="下一页" TYPE=button class=cssButton onclick="getNextPage();"> 
      	<INPUT VALUE="尾页" TYPE=button class=cssButton onclick="getLastPage();"> 					
  	</div>
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
    <INPUT VALUE="查看" TYPE=button class=cssButton onclick="ShowPagesDetails()"> 
  			  	</td>
  			</tr>
    	</table>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
