<%
//程序名称：LLScanSortInit.jsp
//程序功能：理赔排序
//创建日期：2005-09-15
//创建人  ：dongjianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   //
  }
  catch(ex)
  {
    alert("在CodeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm()
{
  try
  {
    initEsDocPagesGrid();
    initInpBox();
  }
  catch(re)
  {
    alert("EsDocPagesInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化CodeGrid
 ************************************************************
 */
  var EsDocPagesGrid;          //定义为全局变量，提供给displayMultiline使用
function initEsDocPagesGrid()
{                               
    var iArray = new Array();
      
      try
      {
        
		    iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列宽
        iArray[0][2]=50;            //列最大值
        iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="页码";         //列名
        iArray[1][1]="15px";         //宽度
        iArray[1][2]=50;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="管理机构";         //列名
        iArray[2][1]="25px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="操作员";         //列名
        iArray[3][1]="25px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="创建日期";         //列名
        iArray[4][1]="25px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
    
        iArray[5]=new Array();
        iArray[5][0]="创建时间";         //列名
        iArray[5][1]="20px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

		    iArray[6]=new Array();
        iArray[6][0]="修改日期";         //列名
        iArray[6][1]="25px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="修改时间";         //列名
        iArray[7][1]="20px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="理赔分类";         //列名
        iArray[8][1]="20px";         //列宽
        iArray[8][2]=100;            //列最大值
        iArray[8][3]=2;              //是否允许输入,1表示允许，0表示不允许
        
        iArray[9]=new Array();
        iArray[9][0]="理赔页码";         //列名
        iArray[9][1]="20px";         //列宽
        iArray[9][2]=100;            //列最大值
        iArray[9][3]=2;              //是否允许输入,1表示允许，0表示不允许
        
        iArray[10]=new Array();
        iArray[10][0]="理赔事件号";         //列名
        iArray[10][1]="20px";         //列宽
        iArray[10][2]=100;            //列最大值
        iArray[10][3]=2;              //是否允许输入,1表示允许，0表示不允许 
        
        iArray[11]=new Array();
        iArray[11][0]="pageid";         //列名
        iArray[11][1]="0px";         //列宽
        iArray[11][2]=100;            //列最大值
        iArray[11][3]=0;              //是否允许输入,1表示允许，0表示不允许 
        
        iArray[12]=new Array();
        iArray[12][0]="docid";         //列名
        iArray[12][1]="0px";         //列宽
        iArray[12][2]=100;            //列最大值
        iArray[12][3]=0;              //是否允许输入,1表示允许，0表示不允许 
        
        iArray[13]=new Array();
        iArray[13][0]="pagename";         //列名
        iArray[13][1]="0px";         //列宽
        iArray[13][2]=100;            //列最大值
        iArray[13][3]=0;              //是否允许输入,1表示允许，0表示不允许 
        
        iArray[14]=new Array();
        iArray[14][0]="pageflag";         //列名
        iArray[14][1]="0px";         //列宽
        iArray[14][2]=100;            //列最大值
        iArray[14][3]=0;              //是否允许输入,1表示允许，0表示不允许 
          
                       
        EsDocPagesGrid = new MulLineEnter( "fm" , "EsDocPagesGrid" ); 

        //这些属性必须在loadMulLine前
        EsDocPagesGrid.mulLineCount = 1;   
        EsDocPagesGrid.displayTitle = 1;
        EsDocPagesGrid.canSel=1;
        EsDocPagesGrid.hiddenSubtraction=1;
        EsDocPagesGrid.hiddenPlus=1;
        EsDocPagesGrid.canChk=1;
        EsDocPagesGrid.loadMulLine(iArray);  
        EsDocPagesGrid.selBoxEventFuncName ="ChangePic";

      }
      catch(ex)
      {
        alert("初始化EsDocPagesGrid时出错："+ ex);
      }
}
    
    
</script>