
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}

//查询
function queryClick(){
	if(fm.printNo.value==null||fm.printNo.value==""){
		alert("请输入业务号！");
		return false;
	}
	initInfoGrid();
	var strSql = "select DOCCODE, DOCID, NUMPAGES,MAKEDATE, MAKETIME from ES_DOC_MAIN lcpi where 1=1 and DOCCODE like '"+fm.printNo.value+"%'"
		       + " order by lcpi.DOCCODE desc ";
	turnPage1.queryModal(strSql, InfoGrid);
}
//更新最新
function InfoUpdate() {
	var checkFlag = 0;
  for (i=0; i<InfoGrid.mulLineCount; i++)
  {
    if (InfoGrid.getSelNo(i))
    {
      checkFlag = InfoGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	var	DOCCODE = InfoGrid.getRowColData(checkFlag - 1, 1);
  	if(DOCCODE == null || DOCCODE == "" || DOCCODE =="null"){
  		alert("请先查询影像件信息！");
  		return false;
  	}
  	fm.DOCCODE.value=DOCCODE;
  	fm.fmtransact.value = "UPDATE";
	//
  	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
  	//showInfo = window.open("./infoMain.jsp?transact=update&DOCCODE="+DOCCODE);
  }else{
  	alert("请选择一条要修改的影像件！");
  	return false;
  }
}
//删除
function InfoDelete(){
	var checkFlag = 0;
	  for (i=0; i<InfoGrid.mulLineCount; i++)
	  {
	    if (InfoGrid.getSelNo(i))
	    {
	      checkFlag = InfoGrid.getSelNo();
	      break;
	    }
	  }
	  if(checkFlag){
	  	var	DOCCODE = InfoGrid.getRowColData(checkFlag - 1, 1);
	  	if(DOCCODE == null || DOCCODE == "" || DOCCODE =="null"){
	  		alert("请先查询影像件信息！");
	  		return false;
	  	}
	  	fm.DOCCODE.value=DOCCODE;
	  	fm.fmtransact.value = "DELETE";
		//
	  	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit();
	  }else{
	  	alert("请选择一条要删除的影像件信息！");
	  	return false;
	  }
}

function afterSubmit(FlagStr, content ) {
	  try { showInfo.close(); } catch(e) {}
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");
	  queryClick();
	} 

