//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  // showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
	var i = 0;
	var flag = 0;
	flag = 0;
	//判定是否有选择打印数据
	for( i = 0; i < ContGrid.mulLineCount; i++ )
	{
		if( ContGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}
	//如果没有打印数据，提示用户选择
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	fm.all("printButton").disabled=true;
	fm.submit();
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//无论打印结果如何，都重新激活打印按钮
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果打印失败，展现错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}
function easyQueryClick()
{
	resetButton();
	// 初始化表格
	if(Flag==1){
	initQCerGrid();

	var strSQL = "";
	strSQL =  "select DocID, DocCode,BussType,SubType,"
	        + "(select codename from ldcode where codetype='scantype' and code=SubType),"
	        + "ScanOperator,ScanManagecom,ScanLastApplyDate,QCFlag,"
	        + "(case QCFlag when '0' then '待审核' when '1' then '审核不通过' else 'd' end),"
	        + "QCsuggest,"
	        + " unpassreason ,scanopestate, "
	        + "(case scanopestate when '' then '' when '1' then '修改中' when '2' then '删除重扫中' when '3' then '修改完毕' when '4' then '删除重扫完毕' else ' ' end) "
	        + "from ES_DOC_QC_MAIN"
	        + " where (QCFlag='0' or (QCFlag='1' and scanopestate='3')) "
	        + getWherePart('ScanManagecom', 'ManageCom', 'like')
	        + getWherePart('DocCode', 'DocCode')
	        + getWherePart('SubTpye', 'SubTpye')
	        + " order by  ScanLastApplyDate ";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}

	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	turnPage.pageLineNum = 10 ;
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = QCerGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL ;
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}
if(Flag==2)
{
	initScanerGrid();
	var strSQL = "";
	strSQL =  "select DocID, DocCode,BussType,SubType,"
	        + "(select codename from ldcode where codetype='scantype' and code=SubType),"
	        + "ScanOperator,ScanManagecom,ScanLastApplyDate,QCFlag,"
	        + "(case QCFlag when '0' then '待审核' when '1' then '审核不通过' else 'd' end),"
	        + "QCsuggest,"
	        + " unpassreason ,scanopestate, "
	        + "(case scanopestate when '' then '' when '1' then '修改中' when '2' then '删除重扫中' when '3' then '修改完毕' when '4' then '删除重扫完毕' else ' ' end) "
	        + "from ES_DOC_QC_MAIN"
	        + " where (QCFlag='1' and scanopestate not in ('3','4')) "
	        + getWherePart('ScanManagecom', 'ManageCom', 'like')
	        + getWherePart('DocCode', 'DocCode')
	        + getWherePart('SubTpye', 'SubTpye')
	        + " and ScanOperator ='"+Operator+"' "
	        + " order by  ScanLastApplyDate ";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}

	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	turnPage.pageLineNum = 10 ;
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = ScanerGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL ;
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	}
}

function queryBranch()
{
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
	if(arrResult!=null)
	{
		fm.BranchGroup.value = arrResult[0][3];
	}
}
function QCerinitButton()
{
 resetButton();
 var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<QCerGrid.mulLineCount; i++) {
    if (QCerGrid.getSelNo(i)) { 
      checkFlag = QCerGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) {
  	var tQDocID=QCerGrid.getRowColData(checkFlag-1,1);
  	var tQDocCode=QCerGrid.getRowColData(checkFlag-1,2);
  	var tQBussTpye=QCerGrid.getRowColData(checkFlag-1,3);
  	var tQSubTpye=QCerGrid.getRowColData(checkFlag-1,4);
  	var tQCFlag=QCerGrid.getRowColData(checkFlag-1,9);
  	var tUnpassReason=QCerGrid.getRowColData(checkFlag-1,12);
  	var tScanOpeState=QCerGrid.getRowColData(checkFlag-1,13);
  
  	fm.all("QDocID").value=tQDocID;
  	fm.all("QDocCode").value=tQDocCode;
  	fm.all("QBussTpye").value=tQBussTpye;
  	fm.all("QSubTpye").value=tQSubTpye;
  	fm.all("UnpassReason").value=tUnpassReason;

  	
  }
  else {
    alert("请先选择一条保单信息！"); 
  }

}
function ScanerinitButton()
{
 resetButton();
 var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<ScanerGrid.mulLineCount; i++) {
    if (ScanerGrid.getSelNo(i)) { 
      checkFlag = ScanerGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) {
  	var tQDocID=ScanerGrid.getRowColData(checkFlag-1,1);
  	var tQDocCode=ScanerGrid.getRowColData(checkFlag-1,2);
  	var tQBussTpye=ScanerGrid.getRowColData(checkFlag-1,3);
  	var tQSubTpye=ScanerGrid.getRowColData(checkFlag-1,4);
  	var tQCFlag=ScanerGrid.getRowColData(checkFlag-1,9);
  	var tUnpassReason=ScanerGrid.getRowColData(checkFlag-1,12);
  	var tScanOpeState=ScanerGrid.getRowColData(checkFlag-1,13);
  
  	fm.all("QDocID").value=tQDocID
  	fm.all("QDocCode").value=tQDocCode
  	fm.all("QBussTpye").value=tQBussTpye
  	fm.all("QSubTpye").value=tQSubTpye
  	fm.all("UnpassReason").value=tUnpassReason;
  	if(tScanOpeState==3){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=false;
         fm.all("Scanermodify").disabled=false;
         fm.all("Scanerdelete").disabled=false;
         fm.all("ScanermodifyOK").disabled=false;
         fm.all("ScanerdeleteOK").disabled=true;
  		}
  	if(tScanOpeState==2){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=true;
         fm.all("Scanermodify").disabled=false;
         fm.all("Scanerdelete").disabled=true;
         fm.all("ScanermodifyOK").disabled=false;
         fm.all("ScanerdeleteOK").disabled=false;
  		}
  	if(tScanOpeState==1){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=false;
         fm.all("Scanermodify").disabled=true;
         fm.all("Scanerdelete").disabled=true;
         fm.all("ScanermodifyOK").disabled=false;
         fm.all("ScanerdeleteOK").disabled=true;
  		}
  	if(tScanOpeState==""||tScanOpeState==null||tScanOpeState=="null"){
         fm.all("QCershowpic").disabled=false;
         fm.all("QCerpass").disabled=false;
         fm.all("QCermodify").disabled=false;
         fm.all("QCerdelete").disabled=false;
         fm.all("Scanershowpic").disabled=false;
         fm.all("Scanermodify").disabled=false;
         fm.all("Scanerdelete").disabled=false;
         fm.all("ScanermodifyOK").disabled=true;
         fm.all("ScanerdeleteOK").disabled=true;
  		}
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}
function resetButton()
{
fm.all("QCershowpic").disabled=false;
fm.all("QCerpass").disabled=false;
fm.all("QCermodify").disabled=false;
fm.all("QCerdelete").disabled=false;
fm.all("Scanershowpic").disabled=false;
fm.all("Scanermodify").disabled=false;
fm.all("Scanerdelete").disabled=false;
fm.all("ScanermodifyOK").disabled=false;
fm.all("ScanerdeleteOK").disabled=false;
fm.all("QDocID").value="";
fm.all("QDocCode").value="";
fm.all("QBussTpye").value="";
fm.all("QSubTpye").value="";
fm.all("QOperate").value="";
fm.all("UnpassReason").value="";

}
function temp()
{
resetButton();

resetButton();
 var i = 0;
  var checkFlag = 0;
  var state = "0";
  
  for (i=0; i<QCerGrid.mulLineCount; i++) {
    if (QCerGrid.getSelNo(i)) { 
      checkFlag = QCerGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) {
  	prtNo = GrpGrid.getRowColData(checkFlag - 1, 1);
    var	ManageCom = GrpGrid.getRowColData(checkFlag - 1, 2);
    var MissionID =GrpGrid.getRowColData(checkFlag - 1, 4);
    var SubMissionID =GrpGrid.getRowColData(checkFlag - 1, 5);
		var PolApplyDate = GrpGrid.getRowColData(checkFlag - 1, 3);
    var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;

    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    var strReturn = window.showModalDialog(urlStr, "", sFeatures);

    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    if (strReturn == "1") 
    	if(type=="1")
    	{
    		window.open("./ContInputScanMain.jsp?ScanFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&scantype=scan&PolApplyDate="+PolApplyDate, "", sFeatures);        
 			}
 			if(type=="2")
 			{
 				window.open("./GrpContInputScanMain.jsp?ScanFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&scantype=scan&PolApplyDate="+PolApplyDate, "", sFeatures); 
 			}
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

function showPic()
{
	if(fm.all("QDocID").value==""||fm.all("QDocCode").value==""||fm.all("QBussTpye").value==""||fm.all("QSubTpye").value=="")
	{
		alert("请选择一条记录！");
		return;
		}
		var cDocID=fm.all("QDocID").value;
  	var cDocCode = fm.all("QDocCode").value;
    var	cBussTpye = fm.all("QBussTpye").value;
    var cSubTpye =fm.all("QSubTpye").value;
    
    window.open("./QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        

}
function QCsubmit(SubFlag)
{
	//SubFlag=1  质检通过
	//SubFlag=2  质检建议修改
	//SubFlag=3  质检建议删除
  //SubFlag=4  扫描员修改中
	//SubFlag=5  扫描员删除
	//SubFlag=6  扫描员修改完毕
	//
	if(fm.all("QDocID").value==""||fm.all("QDocCode").value==""||fm.all("QBussTpye").value==""||fm.all("QSubTpye").value=="")
	{
		alert("请选择一条记录！");
		return;
		}
	var tFlag="1";
 if(SubFlag==1){
 	fm.all("QOperate").value="QCPASS";
 	}
else if(SubFlag==2){
  fm.all("QOperate").value="QCMODIFY";
	}
else if(SubFlag==3){
	if(fm.all("UnpassReason").value==""){
		alert("请输入未通过原因！");
		return;
		}
  fm.all("QOperate").value="QCDELETE";
  }
else if(SubFlag==4){
  fm.all("QOperate").value="SCANMODIFY";
	}
else if(SubFlag==5){
  fm.all("QOperate").value="SCANDELETE";
	}
else if(SubFlag==6){
  fm.all("QOperate").value="SCANMODIFYOK";
	}
else if(SubFlag==7){
  fm.all("QOperate").value="SCANDELETEOK";
	}
else{
	fm.all("QOperate").value="";
	tFlag="0";
	}
if(tFlag==1){
	submitForm();
	}
else{
	alert("不允许该操作");
	}
}