<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：ProposalDownload.jsp
//程序功能：保单下载
//创建日期：2005-08-26
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ProposalDownload.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ProposalDownloadInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ProposalDownloadSave.jsp" method=post name=fm target="fraSubmit">
    <%//@include file="../common/jsp/OperateButton.jsp"%>
    <%//@include file="../common/jsp/InputButton.jsp"%>
    
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 请输入查询条件
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divCode1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
		<TD class= title>管理机构</TD>
		<TD class= input>
		<Input class="codeNo" name=ManageCom verify="管理机构|code:comcode&NOTNULL" elementtype=nacessary  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true > </TD>
 				
		<TD  class= title>
            保险合同号
          </TD>
          <TD  class= input>
            <Input class= common name=ContNo >
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>
        </TR>
        <TR  class= common>
		<TD class= title>保单类型</TD>
		<TD class= input>
		<Input class="codeNo"  name=ContType ondblclick="return showCodeList('conttype',[this,conttypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('conttype',[this,conttypeName],[0,1],null,null,null,1);"><input class=codename name=conttypeName readonly=true > </TD>
 	         
          <TD  class= title>
            打印日期  自
          </TD>
          <TD  class= input>
            <input class=coolDatePicker  name=StartDate verify="起始日期|NOTNULL"  value="">
          </TD>
          <TD  class= title>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;至
          </TD>
          <TD  class= input>
            <input class=coolDatePicker  name=EndDate verify="结束日期|NOTNULL" value="">
          </TD>
        </TR>  
  			<TR class= common id="PrintStateTRID" style="display: ''">
  				<TD class= title>个单类型</TD>
  				<TD class= input colspan="5">
  				  <Input class=codeNo name=CardFlag value="" CodeData="0|^0|其他保单^8|银保万能险^9|个险万能险" ondblclick="return showCodeListEx('ReCardFlag',[this,CardFlagName],[0,1]);" onkeyup="return showCodeListKey('ReCardFlag',[this,CardFlagName],[0,1]);"><input class=codename name=CardFlagName value="" readonly=true> 不选择即查询所有类型保单</TD>
  			</TR>     
      </table>
      <!--Input class=code name=ComCode ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);" >
      <!--Input class= common name=ComCode -->
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <table>
    	<tr>
        	<td class=common>
			        <INPUT VALUE="未下载过的保单" TYPE=button class=cssButton onclick="easyqueryClick()"> 
			        <INPUT VALUE="已下载过的保单" TYPE=button class=cssButton onclick="easyqueryClick2()"> 	
			        <INPUT VALUE="" type=hidden name=DownloadType> 
			        <INPUT VALUE="" type=hidden name=EXESql> 
    		</td>
    	</tr>
    </table>
    
    <!-- ES_DOC_PAGES数据区 MultiLine -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			 保单打印下载信息
    		</td>
    	</tr>
    </table>    
  	<Div align=center id= "divContGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
  			</table>
    	<br>
      	<INPUT VALUE="首页" TYPE=button class=cssButton onclick="getFirstPage();"> 
      	<INPUT VALUE="上一页" TYPE=button class=cssButton onclick="getPreviousPage();"> 					
      	<INPUT VALUE="下一页" TYPE=button class=cssButton onclick="getNextPage();"> 
      	<INPUT VALUE="尾页" TYPE=button class=cssButton onclick="getLastPage();"> 					
  	</div>
  	    <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
			        <INPUT VALUE="下载选中保单" class="cssButton" TYPE=button onclick="DownloadPic();" name='DownloadButton' >	
			        <INPUT VALUE="下载所有保单" class="cssButton" TYPE=button onclick="DownloadAllPic();" name='DownloadAllButton' >
  			  	</td>
  			</tr>
  			  </table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
