//程序名称：ProposalDownload.js
//程序功能：保单下载
//创建日期：2005-08-26
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
var arrDataSet
var tArr;
var turnPage = new turnPageClass();

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//个单卡单类型
var tCardFlagCondition = "";

function setCardFlagCondition()
{
    //若选择了个单卡单类型，则ContType默认为1
    if(fm.CardFlag.value != "")
    {
        fm.ContType.value = "1";
    }
    else
    {
        tCardFlagCondition = "";
    }

	if(fm.ContType.value == "1")
	{
        if(fm.CardFlag.value == "8")
        {
            tCardFlagCondition = "  and exists(select 1 from LCCont where PrtNo = a.PrtNo and CardFlag is not null and CardFlag = '" + fm.CardFlag.value + "') "
                + "  and exists (select 1 from LDBusinessRule where ComCode = substr(a.ManageCom, 1, 4) and RuleType = '01' and BussType = 'TB04' and State = '1') ";
        }
        else if(fm.CardFlag.value == "0")
  	    {
            tCardFlagCondition = " and exists (select 1 from LCCont b, LDCode1 c where b.PrtNo = trim(a.PrtNo) and (CardFlag is null or CardFlag != '8')"
                + " and c.CodeType = 'printchannel' and c.Code = substr(b.ManageCom, 1, 4) and Code1 = b.ContType and c.CodeName = '0') "//分公司保单打印数据不进行下载
  	            + " and not exists (select 1 from LCPol b, LMRiskApp c where b.PrtNo = a.PrtNo and b.RiskCode = c.RiskCode and c.RiskType4 = '4') ";  //万能险保单不进行下载
  	    }
        else if(fm.CardFlag.value == "9")
  	    {
            tCardFlagCondition = " and exists (select 1 from LCPol b, LMRiskApp c where b.PrtNo = a.PrtNo and b.RiskCode = c.RiskCode and c.RiskType4 = '4') ";  //非万能险保单
  	    }
    }
}

function easyqueryClick()
{
    if(fm.ManageCom.value == "")
    {
        alert("管理机构不能为空！");
        return false;
    }
    initContGrid();
    setCardFlagCondition();

    fm.DownloadType.value="";
    var strSQL = "select distinct(PrtNo),OtherNo,CValiDate,managecom"
        + " from lccontprint a where 1=1 and PrintCount > 0 and downloadcount<1 "
        + " and OtherNoType in('1', '2') and ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart( 'MakeDate','StartDate','>=' )
        + getWherePart( 'MakeDate','EndDate','<=' )
        + getWherePart( 'OtherNo','ContNo' )
        + getWherePart( 'PrtNo' )
        + getWherePart( 'othernotype','ContType' )
        + tCardFlagCondition
        + " with ur ";

    var tstrSQL = "select * "
        + " from lccontprint a where 1=1 and PrintCount > 0 and downloadcount<1 "
        + " and OtherNoType in('1', '2') and ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart( 'MakeDate','StartDate','>=' )
        + getWherePart( 'MakeDate','EndDate','<=' )
        + tCardFlagCondition
        + getWherePart( 'OtherNo','ContNo' )
        + getWherePart( 'PrtNo' )
        + getWherePart( 'othernotype','ContType' );

    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (!turnPage.strQueryResult) {
        alert("未查询到满足条件的数据！");
        fm.EXESql.value="";
        return false;
    }

    //设置查询起始位置
    turnPage.pageIndex = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    turnPage.pageLineNum = 20;
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = ContGrid;

    //保存SQL语句
    turnPage.strQuerySql     = strSQL


    //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,MAXSCREENLINES);
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);

    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    fm.EXESql.value=tstrSQL;

}
function easyqueryClick2()
{
    if(fm.ManageCom.value == "")
    {
        alert("管理机构不能为空！");
        return false;
    }
    initContGrid();
    setCardFlagCondition();

    fm.DownloadType.value="";
    var strSQL = "select distinct(PrtNo),OtherNo,CValiDate,managecom, downoperator,downloaddate "
        + " from lccontprint a where 1=1 and PrintCount > 0 and downloadcount>=1 "
        + " and OtherNoType in('1', '2') and ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart( 'MakeDate','StartDate','>=' )
        + getWherePart( 'MakeDate','EndDate','<=' )
        + getWherePart( 'OtherNo','ContNo' )
        + getWherePart( 'PrtNo' )
        + tCardFlagCondition
        + getWherePart( 'othernotype','ContType' )
        + " with ur ";

    var tstrSQL = "select * "
        + " from lccontprint a where 1=1 and PrintCount > 0 and downloadcount>=1 "
        + " and OtherNoType in('1', '2') and ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart( 'MakeDate','StartDate','>=' )
        + getWherePart( 'MakeDate','EndDate','<=' )
        + tCardFlagCondition
        + getWherePart( 'OtherNo','ContNo' )
        + getWherePart( 'PrtNo' )
        + getWherePart( 'othernotype','ContType' );

    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (!turnPage.strQueryResult) {
        alert("未查询到满足条件的数据！");
        fm.EXESql.value="";
        return false;
    }

    //设置查询起始位置
    turnPage.pageIndex = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    turnPage.pageLineNum = 20;
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = ContGrid;

    //保存SQL语句
    turnPage.strQuerySql     = strSQL


    //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,MAXSCREENLINES);
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);

    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    fm.EXESql.value=tstrSQL;

}
function DownloadPic()
{
	var i = 0;
	var flag = 0;
	flag = 0;
	//判定是否有选择打印数据
	for( i = 0; i < ContGrid.mulLineCount; i++ )
	{
		if( ContGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}
	//如果没有打印数据，提示用户选择
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	var showStr="正在生成下载数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	fm.DownloadType.value="1";
	fm.all("DownloadButton").disabled=true;
	fm.submit();
}
function DownloadAllPic()
{
	if (fm.EXESql.value==""){
		alert("请先进行查询");
		return false;
		}
	var showStr="正在生成下载数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	fm.DownloadType.value="2";
	fm.all("DownloadAllButton").disabled=true;
	fm.submit();
}
function afterSubmit( FlagStr, content )
{
	//无论打印结果如何，都重新激活打印按钮
	showInfo.close();
	fm.all("DownloadButton").disabled=false;
	fm.all("DownloadAllButton").disabled=false;

	if (FlagStr == "Fail" )
	{
		//如果打印失败，展现错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	easyqueryClick();
}