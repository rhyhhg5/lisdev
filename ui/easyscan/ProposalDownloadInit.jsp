<%
//程序名称：ProposalDownloadInit.jsp
//程序功能：保单下载
//创建日期：2005-08-26
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">

function initInputBox()
{
    try
    {
        fm.reset();
        
        //初始化管理机构
        fm.all('ManageCom').value = <%=strManageCom%>;
        if(fm.all('ManageCom').value==86){
            fm.all('ManageCom').readOnly=false;
        }
        else{
            fm.all('ManageCom').readOnly=true;
        }
        //初始化保单打印日期
        var dateSql = "select (Current Date - 5 day), (Current Date) from dual";
        var arr = easyExecSql(dateSql);
        if(arr){
            fm.all('StartDate').value = arr[0][0];
            fm.all('EndDate').value = arr[0][1];
        }
    }
    catch(ex)
    {
        alert("在ProposalDownloadInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initContGrid();
    }
    catch(re)
    {
        alert("CodeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

var ContGrid;          //定义为全局变量，提供给displayMultiline使用
function initContGrid()
{
    var iArray = new Array();

    try
    {

        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="40px";         //列宽
        iArray[0][2]=100;            //列最大值
        iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="印刷号";         //列名
        iArray[1][1]="60px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="保险合同号";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="生效日期";         //列名
        iArray[3][1]="50px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="管理机构";         //列名
        iArray[4][1]="50px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="下载操作员";         //列名
        iArray[5][1]="50px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="下载日期";         //列名
        iArray[6][1]="50px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        ContGrid = new MulLineEnter( "fm" , "ContGrid" );

        //这些属性必须在loadMulLine前
        ContGrid.mulLineCount = 0;
        ContGrid.displayTitle = 1;
        ContGrid.canSel=0;
        ContGrid.hiddenSubtraction=1;
        ContGrid.hiddenPlus=1;
        ContGrid.canChk=1;
        ContGrid.loadMulLine(iArray);

    }
    catch(ex)
    {
        alert("初始化ContGrid时出错："+ ex);
    }
}
</script>