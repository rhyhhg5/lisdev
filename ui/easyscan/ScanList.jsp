<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ScanList.jsp
//程序功能：扫描件查询页面
//创建日期：2006-11-20 9:10
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="ScanList.js"></SCRIPT>
  <%@include file="ScanListInit.jsp"%>
  <title>扫描件查询</title>
</head>
<body  onload="initForm();">
  <form action="./ScanListPrint.jsp" method=post name=fm target="fraSubmit">
    <br>
    <br>
    <table class=common>
      <tr class=common>
        <TD class= title> 扫描类型 </TD>
        <TD class= input>
	    	  <input class="codeNo" name="BussType" id="BussType" readonly ondblclick="return showCodeList('scanbusstype', [this,BussTypeName], [0,1]);" onkeyup="return showCodeListKey('scanbusstype', [this,BussTypeName],[0,1])";><input class="codeName" name="BussTypeName" readonly >
	      </TD>
	      <TD class= title> 扫描操作机构 </TD>
        <TD class= input>
	    	  <input class="codeNo" name="ManageCom" readonly ondblclick="return showCodeList('comcode', [this,ManageComName], [0,1]);" onkeyup="return showCodeListKey('comcode', [this,ManageComName],[0,1])"; verify="扫描操作机构|notnull"><input class="codeName" name="ManageComName" readonly >
	      </TD>
        <TD class= title> 扫描人 </TD>
        <TD class= input>
          <Input class=common name="ScanOperator">
        </TD>
      </tr>
      <tr>
        <TD class= title> 扫描日期起始 </TD>
        <TD class= input>
          <Input class="coolDatePicker" name="StartDate" value="" DateFormat="short" verify="扫描日期|Date">
        </TD> 
        <TD class= title> 扫描日期截止 </TD>
        <TD class= input>
          <Input class="coolDatePicker" name="EndDate" value="" DateFormat="short" verify="扫描日期|Date">
        </TD> 
        </tr>
      <tr>
        <TD class= title> 扫描时间起始 </TD>
        <TD class= input>
          <input class="common" name="StartTime">
        </TD> 
        <TD class= title> 扫描时间截止 </TD>
        <TD class= input>
        	<input class="common" name="EndTime">
        </TD>
      </tr>
      <tr>
        <TD class= title> 总扫描份数 </TD>
        <TD class= input>
          <input class="readonly" readonly name="SumPages">
        </TD> 
        <TD class= title> 受理件数 </TD>
        <TD class= input>
          <input class="readonly" readonly name="SumBuss">
        </TD> 
        <TD class= title> 打印日期 </TD>
        <TD class= input>
        	<input class="readonly" readonly name="PrintDate">
        </TD>
      </tr>
      <tr>
        <TD class= input colspan="6">
        	<input type="button" class=cssButton name="queryButton" value="查  询" id="queryID" onclick="queryList();" onMouseOver=";">
        </TD>    
      </tr>
    </table>
    <Div  id= "divScanlist" style= "display: ''"> 
	  	<table class= common>
	   		<tr class= common>
	  	  		<td text-align: left colSpan=1>
					<span id="spanScanGrid" >
					</span> 
			  	</td>
			</tr>
	    </table>
  	  <Div id= "divPage2" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
    	</Div>
  	</Div>
  	<table>
  	  <tr>
        <TD class= input colspan="6">
        	<input type="button" class=cssButton name="printButton" value="打  印" id="printID" onclick="prinScantList();">
        </TD>    
      </tr>
    </table>
    <input class="common" name="sql" type="hidden">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
