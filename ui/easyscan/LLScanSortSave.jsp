<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLScanSortSave.jsp
//程序功能：
//创建日期：2005-09-16
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.easyscan.*"%>
  <%@page import="java.sql.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//接收信息，并作校验处理。
  LLScanSortUI tLLScanSortUI   = new LLScanSortUI();

  //输入参数
 LLScanSortSet tLLScanSortSet   = new LLScanSortSet();

  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";  
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("--LLScanSortSave.jsp--transact:"+transact);
  
  String tGridNo[] = request.getParameterValues("EsDocPagesGridNo");  //得到序号列的所有值
  String tChk[] = request.getParameterValues("InpEsDocPagesGridChk");    //参数格式="MulLine对象名+Chk"
  String tGrid1[] = request.getParameterValues("EsDocPagesGrid11");  //得到pageid的所有值
  String tGrid2[] = request.getParameterValues("EsDocPagesGrid12");  //得到docid的所有值
  String tGrid3[] = request.getParameterValues("EsDocPagesGrid1");  //得到pagecode的所有值
  String tGrid4[] = request.getParameterValues("EsDocPagesGrid13");  //得到pagename的所有值
  String tGrid5[] = request.getParameterValues("EsDocPagesGrid14");  //得到pageflag的所有值
  String tGrid6[] = request.getParameterValues("EsDocPagesGrid8");  //得到sorttype的所有值
  String tGrid7[] = request.getParameterValues("EsDocPagesGrid9");  //得到sortpage的所有值
  String tGrid8[] = request.getParameterValues("EsDocPagesGrid10");  //得到sortno的所有值
  String tGrid9[] = request.getParameterValues("EsDocPagesGrid4");  //得到makedate的所有值
  String tGrid10[] = request.getParameterValues("EsDocPagesGrid5");  //得到maketime的所有值
  
  int count = tChk.length; //得到接收到的记录数
  
  System.out.println("--LLScanSortSave.jsp--20:" + count + ":" + tGridNo.length);
  
  
  for(int index=0; index< count; index++)
  {
  int index2=index+1;
  	System.out.println("--LLScanSortSave.jsp--30");
  	System.out.println("sdfsadfsaf"+tGrid9[index]);
  	System.out.println(PubFun.getCurrentTime());
  	
    if(tGrid9[index].equals(""))
    {
    tGrid9[index]=PubFun.getCurrentDate();
    System.out.println("sdfsadfsaf"+tGrid9[index]);
    }
    if(tGrid10[index].equals(""))
    {
    tGrid10[index]=PubFun.getCurrentTime();
    System.out.println(tGrid10[index]);
    }
    
    LLScanSortSchema tLLScanSortSchema = new LLScanSortSchema();
    
    tLLScanSortSchema.setPageID(tGrid1[index]);
    tLLScanSortSchema.setDocID(tGrid2[index]);
    tLLScanSortSchema.setPageCode(tGrid3[index]);
    tLLScanSortSchema.setPageName(tGrid4[index]);
    tLLScanSortSchema.setPageFlag(tGrid5[index]);
    tLLScanSortSchema.setSortType(tGrid6[index]);
    tLLScanSortSchema.setSortPage(tGrid7[index]);
    tLLScanSortSchema.setSortNo(tGrid8[index]);
    tLLScanSortSchema.setMakeDate(tGrid9[index]);
    tLLScanSortSchema.setMakeTime(tGrid10[index]);
    
    System.out.println(tGrid1[index]);
    System.out.println("--EsDocManageSave.jsp--增加第:"+index2+"条记录");
    
    tLLScanSortSet.add(tLLScanSortSchema);

  }

  System.out.println("--LLScanSortSave.jsp--40");
  
  try
  {
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.add(tLLScanSortSet);
  	tVData.add(tG);

    System.out.println("--LLScanSortSave.jsp--before submitData");
    tLLScanSortUI.submitData(tVData,transact);
    System.out.println("--LLScanSortSave.jsp--after  submitData");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLLScanSortUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功! ";
      FlagStr = "Success";
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>