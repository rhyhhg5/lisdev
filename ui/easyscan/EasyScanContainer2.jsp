<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>测试EasyScan4.0影像系统</title>
    <script src="../common/javascript/Common.js" type="text/javascript"></script>
    <script src="../common/cvar/CCodeOperate.js" type="text/javascript"></script>
    <!-- 页面样式  -->
    <link rel='stylesheet' type='text/css' href='../common/css/Project.css'>
      <%
      try
      {//获取session
    	String  username  =(String) request.getSession().getAttribute("esUser");
    	System.out.print("........username............."+username+".............username...............");
          if (username == null) {
            session.putValue("username",null);
        	System.out.print("........username............."+username+".............username...............");
            out.println("加载失败，用户名密码错误");
            return;
          }
    	String password= request.getParameter("password"); 
    	System.out.print("....................."+username+"............................");
    	System.out.print("....................."+password+"............................"); 
        String clientURL = "http:/"+"/10.136.10.101:900/logon/main.jsp";
    	System.out.print("....................."+clientURL+"............................");
        int iEnd = clientURL.indexOf("logon/main.jsp");
        clientURL = clientURL.substring(0,iEnd);
        //获取影像保存类型
        String imageType = EasyScanConfig.getInstance().imageType;
        //其它的传入参数（预留）
        String otherConfig = "";
        %>
        <script language="JavaScript" type="text/javascript">
          var objWUpdate;
          var objEasyScan;
          var strRet;
          var strVersion;
          //EasyScan当前版本
          var CON_CURRENT_VERSION;
          CON_CURRENT_VERSION="V4.0.4.7";
          try {
            //创建在线更新程序对象
            objWUpdate = new ActiveXObject ("Update.WUpdate");
            //创建成功，则启动在线更新程序对象
            strRet=objWUpdate.sfUpdate("<%= clientURL%>", "EasyScan单证扫描系统");
            if(objWUpdate!=null){
              objWUpdate=null;
            }
          }
          catch(e) {
            alert( "自动更新程序启动失败！！\n" + e);
            if(objWUpdate!=null){
              objWUpdate=null;
            }
          }
          try {
            //创建EasyScan对象
            objEasyScan = new ActiveXObject ("EasyScan.SinoEasyScan");
            strVersion=objEasyScan.dfGetVersion();
            //如果客户端与服务器端的版本一致
            if(strVersion == CON_CURRENT_VERSION)
            {
              //创建成功，则初始化EasyScan
              objEasyScan.dfControlInit("<%=username%>", "86", "<%=clientURL%>", "<%=imageType%>", "<%=otherConfig%>");
              //启动EasyScan
              objEasyScan.dfShowMain();
              //              alert(objEasyScan.dfGetVersion());
            }
            else
            {
              //如果客户端版本比服务器端的版本旧
              if(strVersion < CON_CURRENT_VERSION)
              {
                alert( "您使用的不是最新版本的EasyScan程序 [" + strVersion + "]，请下载安装最新版本的EasyScan程序 [" + CON_CURRENT_VERSION + "] ！");
              }
              //如果客户端版本比服务器端的版本新
              else if(strVersion > CON_CURRENT_VERSION)
              {
                alert( "您使用的EasyScan程序版本是 [" + strVersion + "] ，服务器端的版本是 [" + CON_CURRENT_VERSION + "] ，两个版本不匹配，请与系统管理员联系！");
              }
            }
            if(objEasyScan!=null){
              objEasyScan=null;
            }
          }
          catch(e) {
          alert( "EasyScan扫描影像系统启动失败，可能是您未安装EasyScan最新版本 [" + CON_CURRENT_VERSION + "] 的程序，或者没有正确安装。\n" + e);
            if(objEasyScan!=null){
              objEasyScan=null;
            }
          }
          </script>
          </head>
          <body   onload='init()'>
            <%
            }
            catch(Exception exception)
            {
              out.println("网页超时，请您重新登录");
              return;
            }
            %>
            </body>
          </html>