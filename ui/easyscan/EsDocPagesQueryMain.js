
//程序名称：EsDocManage.js
//程序功能：
//创建日期：2004-06-02
//创建人  ：LiuQiang
//更新记录：  更新人    更新日期     更新原因/内容


//               该文件中包含客户端需要处理的函数和事件

var arrDataSet
var tArr;
var turnPage = new turnPageClass();

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //fm.hideOperate.value=mOperate;
  //if (fm.hideOperate.value=="")
  //{
  //  alert("操作控制数据丢失！");
  //}
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
    queryPages(fm.all( 'DOC_ID' ).value);
  }

}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
	  initForm();
  }
  catch(re)
  {
  	alert("在OLDCode.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  alert("增加功能暂时不提供，请使用EasyScan上载单证数据!");
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
//  showDiv(operateButton,"false");
//  showDiv(inputButton,"true");
//  fm.fmtransact.value = "INSERT||CODE" ;
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  	var i = 0;
  	var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	//showSubmitFrame(mDebug);
  	fm.fmtransact.value = "UPDATE||MAIN";

  	fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./EsDocMainQuery.html");
}
//点击查询按钮，页面中显示出在es_doc_pages中查询出的数据。
function easyqueryClick()
{
    var strSQL = "select docid,doccode,numpages,makedate,maketime,scanoperator,"
        + "managecom,inputstate,operator,inputstartdate,inputstarttime,subtype,"
        + "(select SubTypeName from ES_Doc_Def b where a.SubType = b.SubType) SubTypeName,"
        + "busstype,"
        + "ArchiveNo "
        + " from ES_DOC_MAIN a where 1=1 "
        + getWherePart( 'ManageCom','ManageCom','like' )
        + getWherePart('MakeDate','startMakeDate','>=')
        + getWherePart('MakeDate','endMakeDate','<=')
        + getWherePart('ScanOperator')
        + getWherePart('SubType','SubType')
        + getWherePart('DocCode')
        + " fetch first 2000 rows only with ur";

	turnPage.strQueryResult  = easyQueryVer3(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有记录");
    return false;
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = CodeGrid;

  //保存SQL语句
  turnPage.strQuerySql     = strSQL;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //tArr = chooseArray(arrDataSet,[0,1,3,4])
  //调用MULTILINE对象显示查询结果

  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
    fm.querySql.value = strSQL;
}
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
	var i = 0;
  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	//showSubmitFrame(mDebug);
  	fm.fmtransact.value = "DELETE||MAIN";

  	fm.submit(); //提交
  	initForm();
  }
  else
  {
    //mOperate="";
    alert("您取消了删除操作！");
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}




/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();

	//alert("afterQuery");
	// 书写SQL语句
	var strSQL = " select doc_id,doc_code,num_pages,input_date,input_time,scanoperator,"
					+ "managecom,inputstate,operator,inputstartdate,inputstarttime "
					+ ",doc_flage,doc_remark,doc_ex_flag,inputenddate,inputendtime "
					+ " from ES_DOC_MAIN where 1=1 "
					+ " "+ " and DOC_ID = " + arrQueryResult[0][0] + " ";

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("查询失败！");
		return false;
    }
	//查询成功则拆分字符串，返回二维数组
	arrResult = decodeEasyQueryResult(turnPage.strQueryResult);

	if( arrResult != null )
	{
		fm.all( 'DOC_ID' ).value = 			arrResult[0][0];
		fm.all( 'DOC_CODE' ).value = 		arrResult[0][1];
		fm.all('NUM_PAGES').value = 		arrResult[0][2];
		fm.all('INPUT_DATE').value = 		arrResult[0][3];
		fm.all('Input_Time').value = 		arrResult[0][4];
		fm.all('ScanOperator').value = 		arrResult[0][5];
		fm.all('ManageCom').value = 		arrResult[0][6];
		fm.all('InputState').value = 		arrResult[0][7];
		fm.all('Operator').value = 			arrResult[0][8];
		fm.all('InputStartDate').value = 	arrResult[0][9];
		fm.all('InputStartTime').value = 	arrResult[0][10];
		fm.all('DOC_FLAGE').value = 		arrResult[0][11];
		fm.all('DOC_REMARK').value = 		arrResult[0][12];
		fm.all('Doc_Ex_Flag').value = 		arrResult[0][13];
		fm.all('InputEndDate').value = 		arrResult[0][14];
		fm.all('InputEndTime').value = 		arrResult[0][15];

		queryPages(arrResult[0][0]);
	}
	else
		alert("arrQueryResult is null!");
}


// 查询按钮
function queryPages(strDoc_id)
{
	// 书写SQL语句
	var strSQL = "select page_id,doc_id,page_code,Server_host,page_name,page_flage,"
					+ "page_path,picpath,operator,makedate,maketime,doc_type "
					+ " from ES_DOC_PAGES where 1=1 "
					+ " "+ " and DOC_ID = " + strDoc_id + " ";
	//alert(strSQL);
	turnPage.queryModal(strSQL, CodeGrid);
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
}



//Click事件 点击'保存修改'按钮时触发，实现保存MultiLine当前修改的数据 非通用
function saveUpdate()
{
  if (confirm("您确实想保存修改吗?"))
  {
	var i = 0;
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	fm.fmtransact.value = "UPDATE||PAGES";

  	//选中所有行
  	CodeGrid.checkBoxAll ()

  	fm.submit(); //提交
  	//initForm();
  }
}
//Click事件 点击'删除选中'按钮时触发，实现删除选中的MultiLine行数据 非通用
function deleteChecked()
{
  if (confirm("您确实想删除选中的单证页数据吗?"))
  {
	var i = 0;
  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	fm.fmtransact.value = "DELETE||PAGES";

  	fm.submit(); //提交
  	//重新查询
  	//queryPages(fm.all( 'DOC_ID' ).value);
  	//initForm();
  }
}
function ShowPagesDetails()
{
  var i = 0;
  var checkFlag = 0;

    for (i=0; i<CodeGrid.mulLineCount; i++) {
    if (CodeGrid.getSelNo(i)) {
      checkFlag = CodeGrid.getSelNo();
      break;
    }
  }
  if (checkFlag) {
  	var prtNo = CodeGrid.getRowColDataByName(checkFlag - 1, "docCode");
    var BussType = CodeGrid.getRowColDataByName(checkFlag - 1, "bussType");
    var SubType =CodeGrid.getRowColDataByName(checkFlag - 1, "subType");
    var BussNoType="";
    var docid=CodeGrid.getRowColDataByName(checkFlag - 1, "docId");

if  (SubType=="TB01"){
	SubType="TB1001";
	BussNoType="11";
	BussType="TB";
}
if  (SubType=="TB02"){
	SubType="TB1002";
	BussNoType="12";
	BussType="TB";
}
if (SubType=="LP01"){
	SubType="LP1001";
	BussNoType="21";
	BussType="LP";
	}
	if (SubType=="LP02"){
	SubType="LP1002";
	//BussNoType="22";
	BussType="LP";
	}
	if (SubType=="LP03"){
	SubType="LP1003";
	BussType="LP";
	}
	if (SubType=="LP04"){
	SubType="LP1004";
	BussType="LP";
	}
if (SubType=="TB15"){
	SubType="TB1015";
	BussType="TB";
	}
if (SubType=="TB22"){
	SubType="TB1022";
	BussType="TB";
	}
if  (SubType=="TB29"){
	SubType="TB1029";
	//BussNoType="11";
	BussType="TB";
}

 window.open("./EsDocPagesQueryShow.jsp?&prtNo="+prtNo+"&BussType="+BussType+"&BussNoType="+BussNoType+"&SubType="+SubType+"&docid="+docid, "");
}
  else {
    alert("请先选择一条保单信息！");
  }
}
function DownloadPic()
{
  var i = 0;
  var checkFlag = 0;

    for (i=0; i<CodeGrid.mulLineCount; i++) {
    if (CodeGrid.getSelNo(i)) {
      checkFlag = CodeGrid.getSelNo();
      break;
    }
  }
  if (checkFlag) {
  	var prtNo = CodeGrid.getRowColDataByName(checkFlag - 1, "docCode");
    var BussType = CodeGrid.getRowColDataByName(checkFlag - 1, "bussType");
    var SubType =CodeGrid.getRowColDataByName(checkFlag - 1, "subType");
    var BussNoType="";
    var docid=CodeGrid.getRowColDataByName(checkFlag - 1, "docId");

if  (SubType=="TB01"){
	SubType="TB1001";
	BussNoType="11";
	BussType="TB";
}
if  (SubType=="TB02"){
	SubType="TB1002";
	BussNoType="12";
	BussType="TB";
}
if (SubType=="LP01"){
	SubType="LP1001";
	BussNoType="21";
	BussType="LP";
	}
//var strSQL = "select a.pagecode,a.managecom,a.operator,a.makedate,"
//					+ "a.maketime,a.modifydate,a.modifytime,a.pageid,a.docid,a.hostname,a.pagename,a.PageSuffix,a.PicPathFTP,a.PageFlag,a.PicPath,a.PageType "
//					+ " from es_doc_pages a, es_doc_main b where 1=1 "
//					+ "and a.docid=b.docid "
//					+"and a.docid="+docid
//					+" order by a.pagecode";
//	//alert(strSQL);
//		var arr = easyExecSql(strSQL);
//	  displayMultiline(arr, PicPageGrid , turnPage);
	  fm.all.docid.value=docid;
	  fm.all.prtNo.value=prtNo;
	  fm.all.SubType.value=SubType;
	  fm.all.BussNoType.value=BussNoType;
	  fm.all.BussType.value=BussType;
    fm.submit(); //提交
 //window.transfer("./EsPicDownload.jsp?&prtNo="+prtNo+"&docid="+docid, "");

}
  else {
    alert("请先选择一条保单信息！");
  }

}

//扫描件清单下载
function downloadList()
{
    if(CodeGrid.mulLineCount == 0)
    {
        alert("没有需要下载的数据");
        return false;
    }
    fm.target = '_blank';
    var formAction = fm.action;
    fm.action = "EsDocPagesDownload.jsp";
    fm.submit();
    fm.action = formAction;
}

//单证影印件打包下载
function downloadPicture()
{
    var sel = CodeGrid.getSelNo();
    if(sel == 0 || sel == null)
    {
        alert( "请先选择一条记录。" );
    }
    else
    {
        fm.prtNo.value = CodeGrid.getRowColDataByName(sel - 1, "docCode");
        fm.docid.value = CodeGrid.getRowColDataByName(sel - 1, "docId");
        fm.target = '_blank';
        var formAction = fm.action;
        fm.action = "./EsDocPagesPicDownload.jsp";
        fm.submit();
        fm.action = formAction;
    }
}
