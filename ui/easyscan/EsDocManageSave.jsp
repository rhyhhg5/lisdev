<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：EsDocManageSave.jsp
//程序功能：
//创建日期：2004-06-02
//创建人  ：LiuQiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.easyscan.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//接收信息，并作校验处理。
  EsDocManageUI tManageUI   = new EsDocManageUI();

  //输入参数
  ES_DOC_MAINSchema tES_DOC_MAINSchema   = new ES_DOC_MAINSchema();
  ES_DOC_PAGESSet tES_DOC_PAGESSet   = new ES_DOC_PAGESSet();

  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String transact = "";

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("--EsDocManageSave.jsp--transact:"+transact);

  tES_DOC_MAINSchema.setDOC_ID(request.getParameter("DOC_ID"));
  tES_DOC_MAINSchema.setDOC_CODE(request.getParameter("DOC_CODE"));
  tES_DOC_MAINSchema.setNUM_PAGES(request.getParameter("NUM_PAGES"));
  tES_DOC_MAINSchema.setINPUT_DATE(request.getParameter("INPUT_DATE"));
  tES_DOC_MAINSchema.setInput_Time(request.getParameter("Input_Time"));
  tES_DOC_MAINSchema.setScanOperator(request.getParameter("ScanOperator"));
  tES_DOC_MAINSchema.setManageCom(request.getParameter("ManageCom"));
  tES_DOC_MAINSchema.setInputState(request.getParameter("InputState"));
  tES_DOC_MAINSchema.setOperator(request.getParameter("Operator"));
  tES_DOC_MAINSchema.setInputStartDate(request.getParameter("InputStartDate"));
  tES_DOC_MAINSchema.setInputStartTime(request.getParameter("InputStartTime"));
  tES_DOC_MAINSchema.setInputEndDate(request.getParameter("InputEndDate"));
  tES_DOC_MAINSchema.setInputEndTime(request.getParameter("InputEndTime"));
  tES_DOC_MAINSchema.setDOC_FLAGE(request.getParameter("DOC_FLAGE"));
  tES_DOC_MAINSchema.setDOC_EX_FLAG(request.getParameter("Doc_Ex_Flag"));
  tES_DOC_MAINSchema.setDOC_REMARK(request.getParameter("DOC_REMARK"));

  System.out.println("--EsDocManageSave.jsp--10");
  
  String tGridNo[] = request.getParameterValues("CodeGridNo");  //得到序号列的所有值
  String tChk[] = request.getParameterValues("InpCodeGridChk");    //参数格式="MulLine对象名+Chk"
  String tGrid1  [] = request.getParameterValues("CodeGrid1");  //得到第1列的所有值
  String tGrid2  [] = request.getParameterValues("CodeGrid2");  //得到第2列的所有值
  String tGrid3  [] = request.getParameterValues("CodeGrid3");  //得到第2列的所有值
  String tGrid4  [] = request.getParameterValues("CodeGrid4");  //得到第2列的所有值
  String tGrid5  [] = request.getParameterValues("CodeGrid5");  //得到第2列的所有值
  String tGrid6  [] = request.getParameterValues("CodeGrid6");  //得到第2列的所有值
  String tGrid7  [] = request.getParameterValues("CodeGrid7");  //得到第2列的所有值
  String tGrid8  [] = request.getParameterValues("CodeGrid8");  //得到第2列的所有值
  String tGrid9  [] = request.getParameterValues("CodeGrid9");  //得到第2列的所有值
  String tGrid10 [] = request.getParameterValues("CodeGrid10"); //得到第2列的所有值
  String tGrid11 [] = request.getParameterValues("CodeGrid11"); //得到第2列的所有值
  String tGrid12 [] = request.getParameterValues("CodeGrid12"); //得到第2列的所有值
  
  int count = tChk.length; //得到接受到的记录数
  
  System.out.println("--EsDocManageSave.jsp--20:" + count + ":" + tGridNo.length);
  
  
  for(int index=0; index< count; index++)
  {
  	System.out.println("--EsDocManageSave.jsp--30");
    if(tChk[index].equals("1"))
    {
    System.out.println("--EsDocManageSave.jsp--31");
    //选中的行
    ES_DOC_PAGESSchema tPageSchema = new ES_DOC_PAGESSchema();
    tPageSchema.setPAGE_ID(tGrid1[index]);
    tPageSchema.setDOC_ID(tGrid2[index]);
    tPageSchema.setPAGE_CODE(tGrid3[index]);
    tPageSchema.setSERVER_HOST(tGrid4[index]);
    tPageSchema.setPAGE_NAME(tGrid5[index]);
    tPageSchema.setPAGE_FLAGE(tGrid6[index]);
    tPageSchema.setPAGE_PATH(tGrid7[index]);
    tPageSchema.setPicPath(tGrid8[index]);
    tPageSchema.setOperator(tGrid9[index]);
    tPageSchema.setMakeDate(tGrid10[index]);
    tPageSchema.setMakeTime(tGrid11[index]);
    tPageSchema.setDoc_Type(tGrid12[index]);
	System.out.println("--EsDocManageSave.jsp--32");
    tES_DOC_PAGESSet.add(tPageSchema);
    }
  }

  System.out.println("--EsDocManageSave.jsp--40");
  
  try
  {
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.add(tES_DOC_MAINSchema);
    tVData.add(tES_DOC_PAGESSet);

    System.out.println("--EsDocManageSave.jsp--before submitData");
    tManageUI.submitData(tVData,transact);
    System.out.println("--EsDocManageSave.jsp--after  submitData");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tManageUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功! ";
      FlagStr = "Success";
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>