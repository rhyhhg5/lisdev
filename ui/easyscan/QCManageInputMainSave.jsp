<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QCManagerInputMainSave.jsp
//程序功能：
//创建日期：2005-12-30 11:10:36
//创建人  ：Dongjb程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<%
String FlagStr = "";
String Content = "";

//获得input页面中取得单证号码和未通过原因
String tDocID = request.getParameter("QDocID");
String tUnpassReason = request.getParameter("UnpassReason");
String tOperate =request.getParameter("QOperate");

//获得session中的人员信息
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//操作对象及容器
ES_DOC_QC_MAINSchema tES_DOC_QC_MAINSchema = new ES_DOC_QC_MAINSchema();
tES_DOC_QC_MAINSchema.setDocID(tDocID);
tES_DOC_QC_MAINSchema.setUnpassReason(tUnpassReason);

QCManageUI tQCManageUI = new QCManageUI();
VData vData = new VData();

	vData = new VData();
	vData.add(tG);
	vData.add(tES_DOC_QC_MAINSchema);
	//执行后台操作
	try
	{
		if (!tQCManageUI.submitData(vData, tOperate)){
			Content = "操作失败，原因是:" + tQCManageUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}


//如果没有失败，则返回打印成功
if (!FlagStr.equals("Fail"))
{
	Content = "操作成功! ";
	FlagStr = "Succ";
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>