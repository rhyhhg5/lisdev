<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QCManagerInputMainSave.jsp
//程序功能：
//创建日期：2005-12-30 11:10:36
//创建人  ：Dongjb程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<%
String FlagStr = "";
String Content = "";

//获得input页面中取得单证号码和未通过原因
String tOperate =request.getParameter("Operate");  
String tDocID =request.getParameter("mDocID");  
String tDocCode =request.getParameter("mDocCode");  
String tBussTpye =request.getParameter("mBussTpye");  
String tSubTpye =request.getParameter("mSubTpye");  
String tApplyReason =request.getParameter("ApplyReason");  //申请原因
String tApplyResponse =request.getParameter("ApplyResponse");  //回复信息
String tIssueType =request.getParameter("IssueType");  //申请原因
System.out.println("tIssueType: " + tIssueType);

//获得session中的人员信息
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//操作对象及容器
Es_IssueDocSchema tEs_IssueDocSchema = new Es_IssueDocSchema();
tEs_IssueDocSchema.setIssueDocID(tDocID);
tEs_IssueDocSchema.setBussNo(tDocCode);
tEs_IssueDocSchema.setBussType(tBussTpye);
tEs_IssueDocSchema.setSubType(tSubTpye);
tEs_IssueDocSchema.setIssueDesc(tApplyReason);
tEs_IssueDocSchema.setCheckContent(tApplyResponse);
tEs_IssueDocSchema.setIssueType(tIssueType);

EsModifyUI tEsModifyUI = new EsModifyUI();
VData vData = new VData();

	vData = new VData();
	vData.add(tG);
	vData.add(tEs_IssueDocSchema);
	//执行后台操作
	try
	{
		if (!tEsModifyUI.submitData(vData, tOperate)){
			Content = "操作失败，原因是:" + tEsModifyUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}
        
        // 对于某些可以直接进行修改的扫描件，进行自动审核通过。
        if("Apply".equals(tOperate) && "".equals(FlagStr))
        {
            boolean isAutoPass = false;
            String[] strAutoPassType = {"LP", "HM", "BQ"};
            System.out.println("tSubTpye: " + tSubTpye);
            for(int i = 0; i < strAutoPassType.length; i++)
            {
                if(tSubTpye != null)
                {
                    if(tSubTpye.substring(0, 2).equals(strAutoPassType[i]))
                    {
                        isAutoPass = true;
                        break;
                    }
                }
            }
            
            String tPrtNo = tDocCode;
            String tbAutoPassFlag = "";
            
            if(!isAutoPass && tDocCode != null)
            {
                if(!"TB01".equals(tSubTpye) && !"TB02".equals(tSubTpye) && !"TB04".equals(tSubTpye)&& !"TB29".equals(tSubTpye)&&!"TB28".equals(tSubTpye)&&!"TB31".equals(tSubTpye)&&!"TB33".equals(tSubTpye))
                {
                    tPrtNo = tDocCode.substring(0, tDocCode.length() - 3);
                }
                
                String tStrSql = " select count(1) from ("
                + " select 1 "
                + " from LCCont lcc "
                + " where (uwflag != '0' or appflag != '0') and ContType = '1' "
                + " and lcc.PrtNo = '" + tPrtNo + "' "
                + " union all "
                + " select 1 "
                + " from LCGrpCont lgc "
                + " where (uwflag != '0' or appflag != '0') "
                + " and lgc.PrtNo = '" + tPrtNo + "' "
                + " union all "
                + " select 1 "
                + " from LCScanDownload lcsd "
                + " where Busstype = 'TB' and SubType in ('TB01', 'TB02') "
                + " and lcsd.DocCode = '" + tPrtNo + "' "
                + " ) as temp ";
                
                tbAutoPassFlag = new ExeSQL().getOneValue(tStrSql);
            }
            
            if(isAutoPass || "0".equals(tbAutoPassFlag))
            {
                if (!tEsModifyUI.submitData(vData, "ApplyPass|Auto"))
                {
                    Content = "操作失败，原因是:" + tEsModifyUI.mErrors.getFirstError();
                    FlagStr = "Fail";
                }
                else
                {
                    Content = "您的申请不需要审核，请直接进行操作。";
                }
            }
            else
            {
                Content = "您的申请，需要相关人员审核。";
            }
        }
        // -----------------------------------------------
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}


//如果没有失败，则返回打印成功
if (!FlagStr.equals("Fail"))
{
	Content += "操作成功! ";
	FlagStr = "Succ";
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>