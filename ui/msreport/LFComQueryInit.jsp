<%
//程序名称：LFComQueryInit.jsp
//程序功能：
//创建日期：2004-06-05 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = '';
    fm.all('Name').value = '';    
    fm.all('Operator').value = '';
    fm.all('FoundDate').value = '';
    fm.all('Address').value = '';
    fm.all('EndFlag').value = '';
    fm.all('Zipcode').value ='';
    fm.all('EndDate').value ='';
    fm.all('Phone').value = '';
    fm.all('Attribute').value = '';    
    fm.all('Fax').value = '';
    fm.all('Sign').value = '';
    fm.all('EMail').value = '';
    fm.all('calFlag').value = '';
    fm.all('WebAddress').value ='';
    fm.all('ApproveDate').value ='';
    fm.all('ManagerName').value ='';
    fm.all('PassDate').value ='';
    
  }
  catch(ex)
  {
    alert("在LFComQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}


function initForm()
{
  try
  {
    initInpBox(); 
    initLAEmployeeGrid();        
  }
  catch(re)
  {
    alert("LFComQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LAEmployeeGrid;
function initLAEmployeeGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="管理机构代码";         		//列名
    iArray[0+1][1]="80px";         		//列名
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="管理机构名称";         		//列名
    iArray[1+1][1]="100px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="成立日期";         		//列名
    iArray[2+1][1]="100px";         		//列名
 
    iArray[3+1]=new Array();
    iArray[3+1][0]="机构属性";         		//列名
    iArray[3+1][1]="50px";         		//列名
    
    iArray[4+1]=new Array();
    iArray[4+1][0]="统计标志";         		//列名
    iArray[4+1][1]="50px";         		//列名 
    
 
    LAEmployeeGrid = new MulLineEnter( "fm" , "LAEmployeeGrid" ); 
    LAEmployeeGrid.mulLineCount = 1; 
    LAEmployeeGrid.displayTitle = 1; 
    LAEmployeeGrid.locked = 1; 
 
    LAEmployeeGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LAEmployeeGrid.mulLineCount = 0;   
    LAEmployeeGrid.displayTitle = 1;
    LAEmployeeGrid.hiddenPlus = 1;
    LAEmployeeGrid.hiddenSubtraction = 1;
    LAEmployeeGrid.canSel = 1;
    LAEmployeeGrid.canChk = 0;
    LAEmployeeGrid.selBoxEventFuncName = "showOne";
*/
    LAEmployeeGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LAEmployeeGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
