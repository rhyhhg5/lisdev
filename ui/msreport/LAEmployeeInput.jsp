<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2004-06-04 12:04:37
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAEmployeeInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAEmployeeInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LAEmployeeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 人员信息表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLAEmployee1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      人员编码
    </TD>
    <TD  class= input>
      <Input class= 'readonly' readonly name=EmployeeCode >
    </TD>
    <TD  class= title>
      管理机构
    </TD>
    <TD  class= input>
      <Input class="code" name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Name >
    </TD>
    <TD  class= title>
      出生日期
    </TD>
    <TD  class= input>
      <Input class='coolDatePicker' dateFormat='short' name=Birthday >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      身份证号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDNo >
    </TD>
    <TD  class= title>
      政治面貌
    </TD>
    <TD  class= input>
      <Input name=PolityVisage class="code" id="polityvisage" 
		ondblclick="return showCodeList('polityvisage',[this]);" 
		onkeyup="return showCodeListKey('polityvisage',[this]);" > 
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      民族
    </TD>
    <TD  class= input>
      <Input name=Nationality class="code" id="Nationality" ondblclick="return showCodeList('Nationality',[this]);" 			onkeyup="return showCodeListKey('Nationality',[this]);" > 
    </TD>
    <TD  class= title>
      性别
    </TD>
    <TD  class= input>
      <Input name=Sex class="code" verify="性别|code:Sex" ondblclick="return showCodeList('Sex',[this]);" 
		onkeyup="return showCodeListKey('Sex',[this]);" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      户口所在地
    </TD>
    <TD  class= input> 
          <Input name=RgtAddress class="code" ondblclick="return showCodeList('NativePlaceBak',[this]);" 
		onkeyup="return showCodeListKey('NativePlaceBak',[this]);"> 
        </TD>
    <TD  class= title>
      学历
    </TD>
    <TD  class= input>
      <Input name=Degree class="code" id="Degree" 
		ondblclick="return showCodeList('Degree',[this]);" 
		onkeyup="return showCodeListKey('Degree',[this]);"> 
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      专业技术职称
    </TD>
    <TD  class= input>
      <Input name=PostTitle class='code' 
		ondblclick="return showCodeList('posttitle',[this]);" 
		onkeyup="return showCodeListKey('posttitle',[this]);" > 
    </TD>
    <TD  class= title>
      入司日期
    </TD>
    <TD  class= input>
      <Input class='coolDatePicker' dateFormat='short' name=EmployeeDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      离司日期
    </TD>
    <TD  class= input>
      <Input class='coolDatePicker' dateFormat='short' name=OutWorkDate >
    </TD>
    <TD  class= title>
      高管标志
    </TD>
    <TD  class= input>
      <Input class="code" name=ManageFlag verify="高管标志|code:ManageFlag&NOTNULL"  CodeData="0|^0|普通员工^1|高级管理人员" 
            ondblclick="showCodeListEx('ManageFlag',[this],[0]);"  onkeyup="showCodeListKeyEx('ManageFlag',[this],[0]);">

    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      人员状态
    </TD>
    <TD  class= input>      
      <Input class="code" name=EmployeeCodeState  CodeData="0|^0|在职^1|离职" 
            ondblclick="showCodeListEx('EmployeeCodeState',[this],[0]);"  onkeyup="showCodeListKeyEx('EmployeeCodeState',[this],[0]);">
    </TD>
    <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
      <Input class=readonly readonly name=Operator >
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
