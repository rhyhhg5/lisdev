//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{  
	
    if (!beforeSubmit())
      return false;
 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LFCom.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{

  //添加操作	
  if( verifyInput() == false )   
  	return false;  
  else
  	return true;  
  
  }           



//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 else 
 {
  	parent.fraMain.rows = "0,0,0,82,*";
 }
}
       

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
if((fm.all('EndFlag').value=="Y")&&(fm.all('EndDate').value==null||fm.all("EndDate").value == ''))
  {
  	showInfo.close();
  	alert("如果该机构停业请置停业日期！");
  	}
  else
	{ 
   //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {  	
      mOperate="UPDATE||MAIN";      
      submitForm();      
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
}
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./LFComQuery.html");
}           


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;		                
		fm.all('ManageCom').value = arrResult[0][0];
        fm.all('Name').value = arrResult[0][1];
    fm.all('Operator').value = arrResult[0][17];
    fm.all('FoundDate').value = arrResult[0][2];
    fm.all('Address').value = arrResult[0][8];
    fm.all('EndFlag').value=arrResult[0][3];
   	fm.all('Zipcode').value = arrResult[0][9];    
    fm.all('EndDate').value = arrResult[0][4];  
    fm.all('Phone').value = arrResult[0][10];
    fm.all('Attribute').value = arrResult[0][5];
    fm.all('Fax').value = arrResult[0][11];
    fm.all('Sign').value = arrResult[0][6];
    fm.all('EMail').value = arrResult[0][12];
    fm.all('calFlag').value=arrResult[0][7];
   	fm.all('WebAddress').value = arrResult[0][13];    
    fm.all('ApproveDate').value = arrResult[0][15]; 
    fm.all('PassDate').value = arrResult[0][16]; 
    fm.all('ManagerName').value = arrResult[0][14]; 
     
    
     
   }
}

