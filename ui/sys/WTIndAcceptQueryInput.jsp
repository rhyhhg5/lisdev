<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：TBIndAcceptQueryInput.jsp
//程序功能：新承保数据每日生成
//创建日期：2006-6-13 16:49
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="WTIndAcceptQueryInput.js"></SCRIPT>   
<%@include file="WTIndAcceptQueryInputInit.jsp"%>
</head>
<body onload="initForm();">
	<form action="./WTIndAcceptQuerySave.jsp" method=post name=fm target="f1print">
		<table class= common border=0 width=100%>
	    <TR  class= common>
	        <TD  class= title>管理机构</TD>
	        <TD  class= input><Input class="codeno" name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class = codename name =ManageComName elementtype=nacessary></TD>  
	        <TD  class= title>统计起期</TD>
	        <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
	        <TD  class= title>统计止期</TD>
	        <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
        <TR class=common>
			<TD class=title>
				销售渠道
			</TD>
			<TD class=input>
				<Input class=codeNo name=SaleChnl verify="销售渠道"
					ondblclick="return showCodeList('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);"
			    	onkeyup="return showCodeListKey('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true>
			</TD>
		</TR>
		</table>
		<INPUT VALUE="查  询" class="cssButton" TYPE="button" onclick="easyQuery()">
		<input class=cssButton type=button value="下载清单" onclick="easyPrint();">
		
		
		
	 <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanWTGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		  <Div  id= "divPage2" align=center style= "display: '' ">     
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
		 </Div>
  	</Div>	
		
		
	<input type=hidden id="sql" name="sql" value="">	
		
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>