//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var strSQL = "";

// 查询按钮
var turnPage = new turnPageClass();
function easyQueryClick()
{
	// 初始化表格
	initCollectivityGrid();
	if(!verifyInput2())
	return false;
	// 书写SQL语句
	strSQL = "select customerno,GrpName,GrpNature,organcomcode from LDGrp  "
	         +" where 1=1 "
				 + getWherePart( 'customerno','GrpNo' )
				 + getWherePart( 'GrpName' )
				 + getWherePart( 'GrpNature' );
	turnPage.queryModal(strSQL, CollectivityGrid);
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = CollectivityGrid.getSelNo();

	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
			top.close();
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		
	}
}

function getQueryResult()
{
	var selNo = CollectivityGrid.getSelNo();
	var	GrpNo = CollectivityGrid.getRowColData(selNo - 1, 1);
	var strSQL = "select * from LDGrp where CustomerNo = '" + GrpNo + "' ";
	var arrResult = easyExecSql(strSQL);
	return arrResult;
}