<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDPersonInput.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="LDPersonInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LDPersonInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDPersonSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LDPerson1的信息 -->  

    <Div  id= "divLDPerson1" style= "display: ''">
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonBasic);">
	      </td>
	      <td class= titleImg>
	        客户基本信息
	      </td>
	  </tr>
    </table>

    <Div  id= "divLDPersonBasic" style= "display: ''">    
      <table  class= common>
        <TR  class= common>           
    <TD  class= title>
      客户号码
    </TD>
    <TD  class= input>
      <Input class="readonly" name=CustomerNo readonly>
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= "readonly" name=Name elementtype=nacessary verify="客户姓名|NOTNULL" readonly>
    </TD>
    <TD  class= title>
      客户性别
    </TD>
    <TD  class= input>
      <Input class="readonly" name=Sex elementtype=nacessary verify="客户性别|code:Sex&NOTNULL" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);" readonly>
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      客户出生日期
    </TD>
    <TD  class= common>
      <Input class="readonly" elementtype=nacessary  verify="客户出生日期|DATE&NOTNULL" dateFormat="short" name=Birthday readonly>
    </TD>
     <TD  class= title>
      证件类型
    </TD>
    <TD  class= input>
      <Input class="readonly" name=IDType verify="证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);" readonly>
    </TD>
    <TD  class= title>
      证件号码
    </TD>
    <TD  class= input>
      <Input class= readonly name=IDNo readonly>
    </TD>
  </TR>
  <TR>
      <TD  class= title> 黑名单标记</TD><TD  class= input>
      <Input class= "codeno"  name=BlacklistFlag  ondblclick="return showCodeList('YesNo',[this,BlacklistFlagname],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('YesNo',[this,BlacklistFlagname],[0,1],null,null,null,1);" ><Input class=codename  name=BlacklistFlagname>
  
        <TD  class= title> VIP值</TD><TD  class= input>
      <Input class= "codeno"  name=VIPValue  ondblclick="return showCodeList('VIPValue',[this,VIPValuename],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('VIPValue',[this,VIPValuename],[0,1],null,null,null,1);" ><Input class=codename  name=VIPValuename>

     </TR>
        </Table>
    </Div>
 <Div  id= "divLDPersonBasic1" style= "display: 'none'">    
  <TABLE class=common>
  <TR  class= common>
    <TD  class= title>
      密码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Password >
    </TD>
    <TD  class= title>
      国籍
    </TD>
    <TD  class= input>
      <Input class="code" name=NativePlace ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);" >
    </TD>
    <TD  class= title>
      民族
    </TD>
    <TD  class= input>
      <Input class="code" name=Nationality ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);" >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      户口所在地
    </TD>
    <TD  class= input>
      <Input class="code" name=RgtAddress ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);" >
    </TD>
    <TD  class= title>
      婚姻状况
    </TD>
    <TD  class= input>
      <Input class="code" name=Marriage ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);" >
    </TD>
    <TD  class= title>
      结婚日期
    </TD>
    <TD  class= input>
      <Input class="coolDatePicker" name=MarriageDate >
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      健康状况
    </TD>
    <TD  class= input>
       <Input class="code" name=Health ondblclick="return showCodeList('Health',[this]);" onkeyup="return showCodeListKey('Health',[this]);">                                                                          
    </TD>
    <TD  class= title>
      身高
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Stature >
    </TD>
     <TD  class= title>
      体重
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Avoirdupois >
    </TD>
  </TR>
  <TR  class= common>
   
    <TD  class= title>
      学历
    </TD>
    <TD  class= input>
      <Input class="code" name=Degree ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">
    </TD>
    <TD  class= title>
      信用等级
    </TD>
    <TD  class= input>
            <Input class="code" name=CreditGrade ondblclick="return showCodeList('CreditGrade',[this]);" onkeyup="return showCodeListKey('CreditGrade',[this]);">                        
    </TD>
    <TD  class= title>
      其它证件类型
    </TD>
    <TD  class= input>
            <Input class="code" name=OthIDType verify="证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">                                                                                                                    
    </TD>
  </TR>
 
  <TR  class= common>
    <TD  class= title>
      其它证件号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OthIDNo >
    </TD>
    <TD  class= title>
      ic卡号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICNo >
    </TD><u></u>
     <TD  class= title>
      单位编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GrpNo >
    </TD>
  </TR>
  <TR  class= common>
   
    <TD  class= title>
      入司日期
    </TD>
    <TD  class= input>
      <Input class="coolDatePicker" name=JoinCompanyDate >
    </TD>
    <TD  class= title>
      参加工作日期
    </TD>
    <TD  class= input>
      <Input class="coolDatePicker" name=StartWorkDate >
    </TD>
    <TD  class= title>
      职位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Position >
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      工资
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Salary >
    </TD>
    <TD  class= title>
      职业类别
    </TD>
    <TD  class= input>
       <Input class="code" name=OccupationType ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
    </TD>
    <TD  class= title>
      职业代码
    </TD>
    <TD  class= input>
	<Input class="code" name=OccupationCode ondblclick="return showCodeList('OccupationCode',[this]);" onkeyup="return showCodeListKey('OccupationCode',[this]);">
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      职业（工种）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WorkType >
    </TD>
    <TD  class= title>
      兼职（工种）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PluralityType >
    </TD>
    <TD  class= title>
      死亡日期
    </TD>
    <TD  class= input>
      <Input class="coolDatePicker" dateFormat="short" name=DeathDate >
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      是否吸烟标志
    </TD>
    <TD  class= input>
            <Input class="code" name=SmokeFlag ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
    </TD>
    
     <TD  class= title>
      属性
    </TD>
    <TD  class= input>
      <Input class="code" name=Proterty ondblclick="return showCodeList('Proterty',[this]);" onkeyup="return showCodeListKey('Proterty',[this]);">                                                                                                        
    </TD>
  </TR>
  <TR  class= common>
   
    <TD  class= title>
      备注
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Remark >
    </TD>
    <TD  class= title>
      状态
    </TD>
    <TD  class= input>
      <Input class= 'common' name=State >
    </TD>
    
  </TR>
  <TR  class= common>
    
  </TR>   
   </Table>                  
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonAddress);">
	      </td>
	      <td class= titleImg>
	        客户联系方式
	      </td>
	  </tr>
    </table>
    </Div>

    <Div  id= "divLDPersonAddress" style= "display: 'none'">    
      <table  class= common>
       <TR  class= common>               
          <TD  class= title>
            地址编码<br><font color=red size=1>(选此查看地址信息)</font>
          </TD>
          <TD  class= input>
            <Input class= code name=AddressNo ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this],[0],'', '', '', true);" onkeyup="return showCodeListKeyEx('GetAddressNo',[this],[0],'', '', '', true);" onfocus="getdetailaddress();">
          </TD>
        </TR>
<TR  class= common>
    <TD  class= title>
      通讯地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PostalAddress >
    </TD>
    <TD  class= title>
      通讯邮编
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ZipCode >
    </TD>
      <TD  class= title>
      通讯电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Phone >
    </TD>
  </TR>
  <TR  class= common>
  
    <TD  class= title>
      通讯传真
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Fax >
    </TD>
     <TD  class= title>
      家庭地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HomeAddress >
    </TD>
    <TD  class= title>
      家庭邮编
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HomeZipCode >
    </TD>
  </TR>
 
  <TR  class= common>
    <TD  class= title>
      家庭电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HomePhone >
    </TD>
    <TD  class= title>
      家庭传真
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HomeFax >
    </TD>
    <TD  class= title>
      单位地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CompanyAddress >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      单位邮编
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CompanyZipCode >
    </TD>
    <TD  class= title>
      单位电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CompanyPhone >
    </TD>
    <TD  class= title>
      单位传真
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CompanyFax >
    </TD>
  </TR>
 
  <TR  class= common>
    <TD  class= title>
      手机
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Mobile >
    </TD>
    <TD  class= title>
      手机中文标示
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MobileChs >
    </TD>
    <TD  class= title>
      e_mail
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EMail >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      传呼
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BP >
    </TD>
    <TD  class= title>
      手机2
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Mobile2 >
    </TD>
    <TD  class= title>
      手机中文标示2
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MobileChs2 >
    </TD>
  </TR>
 
  <TR  class= common>
    <TD  class= title>
      e_mail2
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EMail2 >
    </TD>
    <TD  class= title>
      传呼2
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BP2 >
    </TD>
  </TR>
     </table>
    </Div>  
 
   <Div  id= "divLDPersonAccount" style= "display: 'none'">  
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonAccount);">
	      </td>
	      <td class= titleImg>
	        客户账户信息
	      </td>
	  </tr>
    </table>
      
      <table  class= common>  
  <TR  class= common>      
    <TD  class= title>
      是否默认账户
    </TD>
    <TD  class= input>
      <Input class= 'code' name=AccKind  elementtype=nacessary value="N" readonly >
    </TD>
    <TD  class= title>
      银行编码
    </TD>    
    <TD  class= input>
      <Input  name=BankCode CLASS="code" elementtype=nacessary > <!--verify="银行账号|notnull" MAXLENGTH=20 verify="开户行|notnull&code:bank" ondblclick="return showCodeList('bank',[this]);" onkeyup="return showCodeListKey('bank',[this]);"--> 
    </TD>
    <TD  class= title>
      银行账号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BankAccNo elementtype=nacessary ><!--verify="银行账号|notnull&len<=40"-->
    </TD>
  </TR>     
  <TR  class= common>      
    
    <TD  class= title>
      银行账户名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccName  elementtype=nacessary  ><!--verify="银行账户名|notnull&len<=20"-->
    </TD>
  </TR>       
        </TR>  
          <TD>
          <input type=hidden name=Transact >
          </TD>        
      </table>
    </Div>      
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
