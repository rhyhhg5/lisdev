<%
//程序名称：AgentDetailInit.jsp
//程序功能：
//创建日期：2003-4-8 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            
<script> 
	var tAgentCode = "<%=tAgentCode%>"; 	
</script>
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value = '';
    fm.all('Password').value = '';
    fm.all('EntryNo').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('Birthday').value = '';
    fm.all('NativePlace').value = '';
    fm.all('Nationality').value = '';
    fm.all('Marriage').value = '';
    fm.all('CreditGrade').value = '';
    fm.all('HomeAddressCode').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('PostalAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('BP').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('MarriageDate').value = '';
    fm.all('IDNo').value = '';
    fm.all('Source').value = '';
    fm.all('BloodType').value = '';
    fm.all('PolityVisage').value = '';
    fm.all('Degree').value = '';
    fm.all('GraduateSchool').value = '';
    fm.all('Speciality').value = '';
    fm.all('PostTitle').value = '';
    fm.all('ForeignLevel').value = '';
    fm.all('WorkAge').value = '';
    fm.all('OldCom').value = '';
    fm.all('OldOccupation').value = '';
    fm.all('HeadShip').value = '';
    //fm.all('RecommendAgent').value = '';
    fm.all('Business').value = '';
    fm.all('SaleQuaf').value = '';
    fm.all('QuafNo').value = '';
    fm.all('QuafStartDate').value = '';
    fm.all('QuafEndDate').value = '';
    fm.all('DevNo1').value = '';
    fm.all('DevNo2').value = '';
    fm.all('RetainContNo').value = '';
    fm.all('AgentKind').value = '';
    fm.all('DevGrade').value = '';
    fm.all('InsideFlag').value = '';
    fm.all('FullTimeFlag').value = '';
    fm.all('NoWorkFlag').value = '';
    fm.all('TrainDate').value = '';
    fm.all('EmployDate').value = '';
    //fm.all('InDueFormDate').value = '';
    fm.all('OutWorkDate').value = '';
    //fm.all('Approver').value = '';
    //fm.all('ApproveDate').value = '';
    fm.all('AssuMoney').value = '';
    fm.all('AgentState').value = '';  //增员状态
    //fm.all('QualiPassFlag').value = '';
    fm.all('SmokeFlag').value = '';
    fm.all('RgtAddress').value = '';
    fm.all('BankCode').value = '';
    fm.all('BankAccNo').value = '';
    fm.all('Remark').value = '';
    fm.all('Operator').value = '';
    //行政信息
    fm.all('UpAgent').value = '';
    fm.all('IntroAgency').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value = '';
    fm.all('AgentSeries').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('RearAgent').value = '';
    fm.all('RearDepartAgent').value = '';
    fm.all('RearSuperintAgent').value = '';
//    fm.all('BranchType').value = getBranchType();     
  }
  catch(ex)
  {
    alert("在AgentDetailInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AgentDetailInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initWarrantorGrid(); 
    afterQuery(tAgentCode);   
  }
  catch(re)
  {
    alert("AgentDetailInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  
// 担保人信息的初始化
function initWarrantorGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="名称";          		        //列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="性别";         		        //列名
      iArray[2][1]="30px";            			//列宽
      iArray[2][2]=10;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="身份证号码";      	   		//列名
      iArray[3][1]="130px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="出生日";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=10;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="单位";      	   		//列名
      iArray[5][1]="110px";            			//列宽
      iArray[5][2]=10;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="家庭地址编码";      	   	//列名
      iArray[6][1]="80px";            			//列宽
      iArray[6][2]=10;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="家庭地址";      	   		//列名
      iArray[7][1]="120px";            			//列宽
      iArray[7][2]=10;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="邮政编码";      	   		//列名
      iArray[8][1]="60px";            			//列宽
      iArray[8][2]=6;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="电话";      	   		//列名
      iArray[9][1]="80px";            			//列宽
      iArray[9][2]=10;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="关系";      	   		//列名
      iArray[10][1]="50px";            			//列宽
      iArray[10][2]=10;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      

      WarrantorGrid = new MulLineEnter( "fm" , "WarrantorGrid" ); 
      //这些属性必须在loadMulLine前
      WarrantorGrid.mulLineCount = 1;   
      WarrantorGrid.displayTitle = 1;
      //WarrantorGrid.locked=1;  
      WarrantorGrid.hiddenPlus=1;
	    WarrantorGrid.hiddenSubtraction=1;    
      WarrantorGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
}

function afterQuery(tAgentCode)
{	
	var arrSelected = new Array();	
	var strSQL = "";
	strSQL = "select a.*,c.BranchManager,b.IntroAgency,b.AgentSeries,b.AgentGrade,c.BranchAttr,b.AscriptSeries from LAAgent a,LATree b,LABranchGroup c where a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup and a.AgentCode='" + tAgentCode + "'"; 
    
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }

//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);


	var arrResult = new Array();
	
	if( arrSelected != null )
	{
		arrResult = arrSelected;		                                  
                fm.all('AgentCode').value = arrResult[0][0];
                fm.all('hideAgentGroup').value = arrResult[0][1];
                fm.all('hideManageCom').value = arrResult[0][2];
                fm.all('ManageCom').value = arrResult[0][2];
                fm.all('Password').value = arrResult[0][3];
                fm.all('EntryNo').value = arrResult[0][4];
                fm.all('Name').value = arrResult[0][5];
                fm.all('Sex').value = arrResult[0][6];
                fm.all('Birthday').value = arrResult[0][7];
                fm.all('NativePlace').value = arrResult[0][8];
                fm.all('Nationality').value = arrResult[0][9];
                fm.all('Marriage').value = arrResult[0][10];
                fm.all('CreditGrade').value = arrResult[0][11];
                fm.all('HomeAddressCode').value = arrResult[0][12];
                fm.all('HomeAddress').value = arrResult[0][13];
                fm.all('PostalAddress').value = arrResult[0][14];
                fm.all('ZipCode').value = arrResult[0][15];
                fm.all('Phone').value = arrResult[0][16];
                fm.all('BP').value = arrResult[0][17];
                fm.all('Mobile').value = arrResult[0][18];
                fm.all('EMail').value = arrResult[0][19];
                fm.all('MarriageDate').value = arrResult[0][20];
                fm.all('IDNo').value = arrResult[0][21];
                fm.all('Source').value = arrResult[0][22];
                fm.all('BloodType').value = arrResult[0][23];
                fm.all('PolityVisage').value = arrResult[0][24];
                fm.all('Degree').value = arrResult[0][25];
                fm.all('GraduateSchool').value = arrResult[0][26];
                fm.all('Speciality').value = arrResult[0][27];
                fm.all('PostTitle').value = arrResult[0][28];
                fm.all('ForeignLevel').value = arrResult[0][29];
                fm.all('WorkAge').value = arrResult[0][30];
                fm.all('OldCom').value = arrResult[0][31];
                fm.all('OldOccupation').value = arrResult[0][32];
                fm.all('HeadShip').value = arrResult[0][33];
                //fm.all('RecommendAgent').value = arrResult[0][34];
                fm.all('Business').value = arrResult[0][35];
                fm.all('SaleQuaf').value = arrResult[0][36];
                fm.all('QuafNo').value = arrResult[0][37];
                fm.all('QuafStartDate').value = arrResult[0][38];
                fm.all('QuafEndDate').value = arrResult[0][39];
                fm.all('DevNo1').value = arrResult[0][40];
                fm.all('DevNo2').value = arrResult[0][41];
                fm.all('RetainContNo').value = arrResult[0][42];
                fm.all('AgentKind').value = arrResult[0][43];
                fm.all('DevGrade').value = arrResult[0][44];
                fm.all('InsideFlag').value = arrResult[0][45];
                fm.all('FullTimeFlag').value = arrResult[0][46];
                fm.all('NoWorkFlag').value = arrResult[0][47];
                fm.all('TrainDate').value = arrResult[0][48];
                fm.all('EmployDate').value = arrResult[0][49];
                //fm.all('InDueFormDate').value = arrResult[0][50];
                fm.all('OutWorkDate').value = arrResult[0][51];
                //fm.all('Approver').value = arrResult[0][57];
                //fm.all('ApproveDate').value = arrResult[0][58];
                fm.all('AssuMoney').value = arrResult[0][59];
                fm.all('AgentState').value = arrResult[0][61];
                //fm.all('QualiPassFlag').value = arrResult[0][62];
                fm.all('SmokeFlag').value = arrResult[0][63];
                fm.all('RgtAddress').value = arrResult[0][64];
                fm.all('BankCode').value = arrResult[0][65];
                fm.all('BankAccNo').value = arrResult[0][66];
                fm.all('Remark').value = arrResult[0][60];
                fm.all('Operator').value = arrResult[0][67];
                //行政信息
                fm.all('UpAgent').value = arrResult[0][73];
                fm.all('IntroAgency').value = arrResult[0][74];
                fm.all('AgentSeries').value = arrResult[0][75];
                fm.all('AgentGrade').value = arrResult[0][76];
                var arrRear = arrResult[0][78].split("^");
                fm.all('RearAgent').value = arrRear.length>0?arrRear[0]:'';
                fm.all('RearDepartAgent').value = arrRear.length>1?arrRear[0]:'';
                fm.all('RearSuperintAgent').value = arrRear.length>2?arrRear[0]:'';
                //显式机构代码
                fm.all('AgentGroup').value = arrResult[0][77];
                
        }

        WarrantorGrid.clearData("WarrantorGrid");
        easyQuery();
}
</script>