 //               该文件中包含客户端需要处理的函数和事件
var mOperate = "";
var showInfo1;
var mDebug="0";
var turnPage = new turnPageClass();
var arrResult;			
var checkapply="1";
//存放添加动作执行的次数
var addAction = 0;
//暂交费总金额
var sumTempFee = 0.0;
//暂交费信息中交费金额累计
var tempFee = 0.0;
//暂交费分类信息中交费金额累计
var tempClassFee = 0.0;
//单击确定后，该变量置为真，单击添加一笔时，检验该值是否为真，为真继续，然后再将该变量置假
var confirmFlag=false;
//
var arrCardRisk;
//window.onfocus=focuswrap;
var mSwitch = parent.VD.gVSwitch;
//工作流flag
var mWFlag = 0;
//使得从该窗口弹出的窗口能够聚焦
function focuswrap()
{
	myonfocus(showInfo1);
}

//提交，保存按钮对应操作
function submitForm()
{
 }


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  try { showInfo.close(); } catch(e) {}
  checkapply="1";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  if (FlagStr=="Succ" && mWFlag == 0)
  {

  	if((acctype==null || acctype=="null" || acctype=="" || acctype!="2") && confirm("是否继续录入其他客户？"))
  	{
  	  emptyInsured();

  	   if(acctype=="5")
       {
                fm.Name.value="公共保额";
                fm.all('PolTypeFlag').value="5";
                fm.PubAmnType.value="";
                fm.PubAmnTypeName.value="";
                fm.all('Sex').value="0"; 
                fm.bieming.value = "";
		        fm.all('Birthday').value=getBirthday();
		        fm.all('IDType').value = "4";
		        fm.all('OccupationCode').value="";
		        fm.all('InsuredAppAge').value="30";
		        fm.all('OccupationType').value="1";
		        fm.all('InsuredPeoples').value="";
       }
       
         	   if(acctype=="2")
       {
                fm.Name.value="公共账户";
                fm.all('PolTypeFlag').value="2";
                fm.PubAmnType.value="";
                fm.PubAmnTypeName.value="";
                fm.all('Sex').value="0"; 
                fm.bieming.value = "";
                fm.OrganComCode.value = "";
		        fm.all('Birthday').value=getBirthday();
		        fm.all('IDType').value = "4";
		        fm.all('OccupationCode').value="";
		        fm.all('InsuredAppAge').value="30";
		        fm.all('OccupationType').value="1";
		        fm.all('InsuredPeoples').value="";
       }
       
  	  if (fm.ContType.value==2)
  	  {
  		fm.ContNo.value="";
  		fm.ProposalContNo.value="";
  	  }
  	  else
  	  {
	       if(fm.InsuredSequencename.value=="第一被保险人资料")
	       {
	       	     //emptyFormElements();
	       	     param="122";
	       	     fm.pagename.value="122";
	       	     fm.SequenceNo.value="2";
                            fm.InsuredSequencename.value="第二被保险人资料";
                            if (scantype== "scan")
                            {
                            setFocus();
                            }
                            noneedhome();
                            return false;
	       }
	       if(fm.InsuredSequencename.value=="第二被保险人资料")
	       {
	       	     //emptyFormElements();
	       	     param="123";
	       	     fm.pagename.value="123";
	       	     fm.SequenceNo.value="3";
                            fm.InsuredSequencename.value="第三被保险人资料";
                            if (scantype== "scan")
                            {
                            setFocus();
                            }
                            noneedhome();
                            return false;
	       }
	       	if(fm.InsuredSequencename.value=="第三被保险人资料")
	       {
                            alert("暂时只允许添加三个被保人");
                            return false;
	       }
	   }
      }
      else
      {
      var strli = "select Birthday from LCInsured where InsuredNo='"+fm.all('InsuredNo').value+"' and ContNo='"+fm.all('ContNo').value+"'";
      var arrResult1 = easyExecSql(strli);
      try{fm.all('Birthday').value=arrResult1[0][0]}catch(ex){}
      }
//chenwm20070828 添加完被保险人后 直接添加险种 acctype为"null"?  导致后面添加个人账户或者公共帐户显示有问题.      
//      alert("acctype="+acctype);
      if (acctype==null||acctype=="null"){
//      	alert(fm.all("PolTypeFlag").value);
      	acctype=fm.all("PolTypeFlag").value;
     
      }  
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//取消按钮对应操作
function cancelForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
//	alert("reach beforesubmit");
//	if(!verifyInput2()){
//		return false;
//	}
	if(fm.PolTypeFlag.value == 0){
	
	
		if(!verifyElementWrap2("被保人邮政编码|zipcode",fm.all("ZipCode").value,"fm.ZipCode")){
			return false;;
		}
		if(!verifyElementWrap2("被保人联系电话|num&len<18",fm.all("Phone").value,"fm.Phone")){
			return false;;
		}
		if(!verifyElementWrap2("被保险人级别|code:PositionCode",fm.all("Position").value,"fm.Position")){
			return false;;
		}
		if(!verifyElementWrap2("被保人移动电话|num&len<15",fm.all("Mobile").value,"fm.Mobile")){
			return false;;
		}
	
		if(!verifyElementWrap2("电子邮箱|EMAIL",fm.all("EMail").value,"fm.EMail")){
			return false;;
		}
	
		if(!verifyElementWrap2("银行帐号|num&len<=40",fm.all("BankAccNo").value,"fm.BankAccNo")){
			return false;;
		}
		if(null!=fm.all("BankInfo").value && fm.all("BankInfo").value!="")
        {
			reg= /[^A-Za-z]*[A-Za-z]+[^A-Za-z]*/ ;     
			if(reg.test(fm.all("BankInfo").value)){    
			    alert("对不起，您输入银行具体信息类型格式不正确!");
			    return;   
			}
        }
       
	}
	//alert("beforesubmit over...");
	return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
        parent.fraMain.rows = "0,0,50,82,*";
    }
 	else
 	{
        parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//问题件录入
function QuestInput()
{
    cContNo = fm.ContNo.value;  //保单号码
    if(LoadFlag=="2"||LoadFlag=="4"||LoadFlag=="13")
    {
        if(mSwitch.getVar( "ProposalGrpContNo" )=="")
        {
            alert("尚无集体合同投保单号，请先保存!");
        }
		else
		{
            window.open("./GrpQuestInputMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+LoadFlag);
        }
    }
    else
    {
        if(cContNo == "")
        {
            alert("尚无合同投保单号，请先保存!");
        }
        else
        {
            window.open("../uw/QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+ LoadFlag,"window1");
        }
    }
}
//问题件查询
function QuestQuery()
{
   cContNo = fm.all("ContNo").value;  //保单号码
    if(LoadFlag=="2"||LoadFlag=="4"||LoadFlag=="13")
    {
         if(mSwitch.getVar( "ProposalGrpContNo" )==""||mSwitch.getVar( "ProposalGrpContNo" )==null)
         {
          	alert("请先选择一个团体主险投保单!");
          	return ;
         }
         else
         {
              window.open("./GrpQuestQueryMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+LoadFlag);
         }
         }
    else
    {
        if(cContNo == "")
        {
	       alert("尚无合同投保单号，请先保存!");
        }
	    else
	    {
            window.open("../uw/QuestQueryMain.jsp?ContNo="+cContNo+"&Flag="+LoadFlag,"window1");
        }
    }
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{

}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{

}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{

}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{

}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{

}

/*********************************************************************
 *  进入险种信息录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoRiskInfo()
{
	if(fm.InsuredNo.value==""||fm.ContNo.value=="")
	{
		alert("请先添加，选择被保人");
		return false;
	}
	if(checkapply=="0"&&LoadFlag=="1")
	{
		alert("请为初审单添加必填信息！");
		return false;
	}
	//mSwitch =parent.VD.gVSwitch;
	delInsuredVar();
	addInsuredVar();

  try{mSwitch.addVar('SelPonNo','',fm.SelPolNo.value);}catch(ex){}; //选择险种单进入险种界面带出已保存的信息
	if ((LoadFlag=='5'||LoadFlag=='6'||LoadFlag=='16'||LoadFlag=='GQ')&&(mSwitch.getVar( "PolNo" ) == null || mSwitch.getVar( "PolNo" ) == ""))
	{
		alert("请先选择被保险人险种信息！");
		return;
	}
	try{mSwitch.addVar('SelPolNo','',fm.SelPolNo.value);}catch(ex){};
	try{mSwitch.deleteVar('ContNo');}catch(ex){};
	try{mSwitch.addVar('ContNo','',fm.ContNo.value);}catch(ex){};
	try{mSwitch.updateVar('ContNo','',fm.ContNo.value);}catch(ex){};
	try{mSwitch.deleteVar('InsuredNo');}catch(ex){};
	try{mSwitch.addVar('InsuredNo','',fm.InsuredNo.value);}catch(ex){};
	try{mSwitch.updateVar('InsuredNo','',fm.InsuredNo.value);}catch(ex){};
	try{mSwitch.deleteVar('mainRiskPolNo');}catch(ex){}; 

	parent.fraInterface.window.location = "./UliProposalInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag=" + LoadFlag+"&ContType="+ContType+"&acctype="+acctype+"&scantype=" + scantype+ "&MissionID=" + MissionID+ "&SubMissionID=" + SubMissionID+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&oldContNo="+oldContNo+"&SysType=02&PolTypeFlag="+fm.PolTypeFlag.value+"&InsuredNo="+fm.InsuredNo.value;
}

/*********************************************************************
 *  删除缓存中被保险人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delInsuredVar()
{
    try{mSwitch.deleteVar('ContNo');}catch(ex){};
    try{mSwitch.deleteVar('InsuredNo');}catch(ex){};
    try{mSwitch.deleteVar('PrtNo');}catch(ex){};
    try{mSwitch.deleteVar('GrpContNo');}catch(ex){};
 //   try{mSwitch.deleteVar('AppntNo');}catch(ex){};
 //   try{mSwitch.deleteVar('ManageCom');}catch(ex){};
    try{mSwitch.deleteVar('ExecuteCom');}catch(ex){};
    try{mSwitch.deleteVar('FamilyType');}catch(ex){};
    try{mSwitch.deleteVar('RelationToMainInsure');}catch(ex){};
    try{mSwitch.deleteVar('RelationToAppnt');}catch(ex){};
    try{mSwitch.deleteVar('AddressNo');}catch(ex){};
    try{mSwitch.deleteVar('SequenceNo');}catch(ex){};
    try{mSwitch.deleteVar('Name');}catch(ex){};
    try{mSwitch.deleteVar('Sex');}catch(ex){};
    try{mSwitch.deleteVar('Birthday');}catch(ex){};
    try{mSwitch.deleteVar('IDType');}catch(ex){};
    try{mSwitch.deleteVar('IDNo');}catch(ex){};
    try{mSwitch.deleteVar('ConfirmIDNo');}catch(ex){};
    try{mSwitch.deleteVar('RgtAddress');}catch(ex){};
    try{mSwitch.deleteVar('Marriage');}catch(ex){};
    try{mSwitch.deleteVar('MarriageDate');}catch(ex){};
    try{mSwitch.deleteVar('Health');}catch(ex){};
    try{mSwitch.deleteVar('Stature');}catch(ex){};
    try{mSwitch.deleteVar('Avoirdupois');}catch(ex){};
    try{mSwitch.deleteVar('Degree');}catch(ex){};
    try{mSwitch.deleteVar('CreditGrade');}catch(ex){};
    try{mSwitch.deleteVar('BankCode');}catch(ex){};
    try{mSwitch.deleteVar('BankAccNo');}catch(ex){};
    try{mSwitch.deleteVar('AccName');}catch(ex){};
    try{mSwitch.deleteVar('JoinCompanyDate');}catch(ex){};
    try{mSwitch.deleteVar('StartWorkDate');}catch(ex){};
    try{mSwitch.deleteVar('Position');}catch(ex){};
    try{mSwitch.deleteVar('Salary');}catch(ex){};
    try{mSwitch.deleteVar('OccupationType');}catch(ex){};
    try{mSwitch.deleteVar('OccupationCode');}catch(ex){};
    try{mSwitch.deleteVar('WorkType');}catch(ex){};
    try{mSwitch.deleteVar('PluralityType');}catch(ex){};
    try{mSwitch.deleteVar('SmokeFlag');}catch(ex){};
    try{mSwitch.deleteVar('ContPlanCode');}catch(ex){};
    try{mSwitch.deleteVar('Operator');}catch(ex){};
    try{mSwitch.deleteVar('MakeDate');}catch(ex){};
    try{mSwitch.deleteVar('MakeTime');}catch(ex){};
    try{mSwitch.deleteVar('ModifyDate');}catch(ex){};
    try{mSwitch.deleteVar('ModifyTime');}catch(ex){};
    try{mSwitch.deleteVar('PolTypeFlag');}catch(ex){};
    try{mSwitch.deleteVar('InsuredAppAge');}catch(ex){};
    try{mSwitch.deleteVar('InsuredPeoples');}catch(ex){};
       
    try{mSwitch.deleteVar('RetireFlag');}catch(ex){};
    try{mSwitch.deleteVar('DoctorEnsure');}catch(ex){};
    try{mSwitch.deleteVar('AppointHospital');}catch(ex){};
    try{mSwitch.deleteVar('InsureDate');}catch(ex){};
    try{mSwitch.deleteVar('StopDate');}catch(ex){};
    try{mSwitch.deleteVar('EspecialTag');}catch(ex){};
    try{mSwitch.deleteVar('TagReason');}catch(ex){};
    try{mSwitch.deleteVar('EnglishName');}catch(ex){}; 
    try{mSwitch.deleteVar('BankInfo');}catch(ex){};
    try{mSwitch.deleteVar('BankPrv');}catch(ex){};
    try{mSwitch.deleteVar('BankCity');}catch(ex){};

}

/*********************************************************************
 *  将被保险人信息加入到缓存中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addInsuredVar()
{
    try{mSwitch.addVar('ContNo','',fm.ContNo.value);}catch(ex){};
    //alert("ContNo:"+fm.ContNo.value);
    try{mSwitch.addVar('InsuredNo','',fm.InsuredNo.value);}catch(ex){};
    try{mSwitch.addVar('PrtNo','',fm.PrtNo.value);}catch(ex){};
    try{mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);}catch(ex){};
 //   try{mSwitch.addVar('AppntNo','',fm.AppntNo.value);}catch(ex){};
 //   try{mSwitch.addVar('ManageCom','',fm.ManageCom.value);}catch(ex){};
    try{mSwitch.addVar('ExecuteCom','',fm.ExecuteCom.value);}catch(ex){};
    try{mSwitch.addVar('FamilyType','',fm.FamilyType.value);}catch(ex){};
    try{mSwitch.addVar('RelationToMainInsure','',fm.RelationToMainInsure.value);}catch(ex){};
    try{mSwitch.addVar('RelationToAppnt','',fm.RelationToAppnt.value);}catch(ex){};
    try{mSwitch.addVar('AddressNo','',fm.AddressNo.value);}catch(ex){};
    try{mSwitch.addVar('SequenceNo','',fm.SequenceNo.value);}catch(ex){};
    try{mSwitch.addVar('Name','',fm.Name.value);}catch(ex){};
    try{mSwitch.addVar('Sex','',fm.Sex.value);}catch(ex){};
    try{mSwitch.addVar('Birthday','',fm.Birthday.value);}catch(ex){};
    try{mSwitch.addVar('IDType','',fm.IDType.value);}catch(ex){};
    try{mSwitch.addVar('IDNo','',fm.IDNo.value);}catch(ex){};
    try{mSwitch.addVar('ConfirmIDNo','',fm.ConfirmIDNo.value);}catch(ex){};
    try{mSwitch.addVar('RgtAddress','',fm.RgtAddress.value);}catch(ex){};
    try{mSwitch.addVar('Marriage','',fm.Marriage.value);}catch(ex){};
    try{mSwitch.addVar('MarriageDate','',fm.MarriageDate.value);}catch(ex){};
    try{mSwitch.addVar('Health','',fm.Health.value);}catch(ex){};
    try{mSwitch.addVar('Stature','',fm.Stature.value);}catch(ex){};
    try{mSwitch.addVar('Avoirdupois','',fm.Avoirdupois.value);}catch(ex){};
    try{mSwitch.addVar('Degree','',fm.Degree.value);}catch(ex){};
    try{mSwitch.addVar('CreditGrade','',fm.CreditGrade.value);}catch(ex){};
    try{mSwitch.addVar('BankCode','',fm.BankCode.value);}catch(ex){};
    try{mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);}catch(ex){};
     try{mSwitch.addVar('BankInfo','',fm.BankInfo.value);}catch(ex){};
    try{mSwitch.addVar('AccName','',fm.AccName.value);}catch(ex){};
    try{mSwitch.addVar('JoinCompanyDate','',fm.JoinCompanyDate.value);}catch(ex){};
    try{mSwitch.addVar('StartWorkDate','',fm.StartWorkDate.value);}catch(ex){};
    try{mSwitch.addVar('Position','',fm.Position.value);}catch(ex){};
    try{mSwitch.addVar('Salary','',fm.Salary.value);}catch(ex){};
    try{mSwitch.addVar('OccupationType','',fm.OccupationType.value);}catch(ex){};
    try{mSwitch.addVar('OccupationCode','',fm.OccupationCode.value);}catch(ex){};
    try{mSwitch.addVar('WorkType','',fm.WorkType.value);}catch(ex){};
    try{mSwitch.addVar('PluralityType','',fm.PluralityType.value);}catch(ex){};
    try{mSwitch.addVar('SmokeFlag','',fm.SmokeFlag.value);}catch(ex){};
    try{mSwitch.addVar('ContPlanCode','',fm.ContPlanCode.value);}catch(ex){};
    try{mSwitch.addVar('InsuredPeoples','',fm.InsuredPeoples.value);}catch(ex){};
    try{mSwitch.addVar('Operator','',fm.Operator.value);}catch(ex){};
    try{mSwitch.addVar('MakeDate','',fm.MakeDate.value);}catch(ex){};
    try{mSwitch.addVar('MakeTime','',fm.MakeTime.value);}catch(ex){};
    try{mSwitch.addVar('ModifyDate','',fm.ModifyDate.value);}catch(ex){};
    try{mSwitch.addVar('ModifyTime','',fm.ModifyTime.value);}catch(ex){};
    try{mSwitch.addVar('PolTypeFlag','',fm.PolTypeFlag.value);}catch(ex){};
    try{mSwitch.addVar('InsuredAppAge','',fm.InsuredAppAge.value);}catch(ex){};
   
    try{mSwitch.addVar('RetireFlag',fm.RetireFlag.value);}catch(ex){};
    try{mSwitch.addVar('DoctorEnsure',fm.DoctorEnsured.value);}catch(ex){};
    try{mSwitch.addVar('AppointHospital',fm.AppointHospital.value);}catch(ex){};
    try{mSwitch.addVar('InsureDate',fm.InsuredDate.value);}catch(ex){};
    try{mSwitch.addVar('StopDate',fm.StopDate.value);}catch(ex){};
    try{mSwitch.addVar('EspecialTag',fm.EspecialTag.value);}catch(ex){};
    try{mSwitch.addVar('TagReason',fm.TagReason.value);}catch(ex){};
    try{mSwitch.addVar('EnglishName',fm.EnglishName.value);}catch(ex){};
    try{mSwitch.addVar('BankPrv',fm.BankPrv.value);}catch(ex){};
    try{mSwitch.addVar('BankCity',fm.BankCity.value);}catch(ex){};

}

/*********************************************************************
 *  添加被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addRecord()
{
//2005.03.18 chenhq 对此进行修改
	if(!beforeSubmit()){
		return false;
	}
 if(LoadFlag==1||LoadFlag==3||LoadFlag==5||LoadFlag==6){
	    var tPrtNo=fm.all("PrtNo").value;
       var sqlstr="select SequenceNo from LCInsured where PrtNo='"+tPrtNo+"' unino select SequenceNo from LBInsured where PrtNo='"+tPrtNo+"'";
      arrResult=easyExecSql(sqlstr,1,0);
      if(arrResult!=null){
      for(var sequencenocout=0; sequencenocout<arrResult.length;sequencenocout++ )
      {
      if(fm.SequenceNo.value==arrResult[sequencenocout][0]){
	  	alert("已经存在该客户内部号");
	  	fm.SequenceNo.focus();
	  	return false;
	  	}
	    }
	  }
	}

	if(acctype != "2" && acctype != "5"){
		 if(!chenckInfo())
			 return false;
		 checkInsuredNo();
	}
    if(fm.PolTypeFlag.value == 0){
   
		if(!verifyElementWrap2("被保险人证件号码|notnull&len<=20",fm.all("IDNo").value,"fm.IDNo"))
	     {
		return;
	     }
	    //chenwm071113 
	    var idno=fm.all("IDNo").value;
		if(idno.length=18&&idno.substr(17,1)=='x'){
			alert("被保险人身份证号最后一位不能是小写的x,只能录入大写的X.");
			return false;
		}	     
	}
    if(fm.PolTypeFlag.value==1||fm.PolTypeFlag.value==3||fm.PolTypeFlag.value==4)
    {
       if(fm.InsuredPeoples.value=="")
       {
       alert("被保人人数不能为空");
       return false;
       }
    }
    //chenwm20070827 公共保额属性不填 后面添加险种的时候会出错.
    if(fm.PolTypeFlag.value==5){
         if(!verifyElementWrap2("姓名|notnull",fm.all("Name").value,"fm.Name"))
	     {
		return;
	     }
         if(!verifyElementWrap2("公共保额属性|notnull",fm.all("PubAmnType").value,"fm.PubAmnType"))
	     {
		return;
	     }
		    	
    }
    if(fm.PolTypeFlag.value==2){
     var tPrtNo=fm.all("PrtNo").value;
       var sqlstr="select 1 from LCInsured c where PrtNo='"+tPrtNo+"' and exists (select 1 from lccont where contno=c.contno and poltype='2')";
      arrResult1=easyExecSql(sqlstr,1,0);
      if(arrResult1!=null){
      alert("已存在公共账户，不能再次添加，请确认！");
      return false;
      }
      if(arrResult!=null){
       //  if(!verifyElementWrap2("姓名|notnull",fm.all("Name").value,"fm.Name"))
	   //  {
		//return;
	   //  }
       //  if(!verifyElementWrap2("工作单位|notnull",fm.all("OrganComCode").value,"fm.OrganComCode"))
	    // {
		//return;
	   //  }
		    	
    }
    }
    //chenwm20070918 同一个末级机构只能添加一个公共账户
    //if(fm.PolTypeFlag.value==2){
   // 	var tSQL="SELECT COUNT(*) FROM lccont a,lcinsured b WHERE a.grpcontno='"+fm.all('GrpContNo').value
    //			+"' AND a.poltype='2' AND a.insuredno=b.insuredno AND b.organcomcode='"
    //			+fm.all("OrganComCode").value+"'";
    //	var isNull=easyExecSql(tSQL);
    //	if(isNull!=null&&isNull>0){
    //		alert("同一个末级机构只能有一个公共帐户.");
    //	return false;
    //	}
   // }
    //hm ASR20093596 理赔通知方式
    if(!verifyElementWrap2("理赔通知方式|code:noticeway",fm.all("NoticeWay").value,"fm.NoticeWay"))
	{
		return;
	}
	//modify by winnie PIR20092538 国籍 变更为 国家或地区
	//guoxh TASK000033-上传国家或地区处理
    if(!verifyElementWrap2("国家或地区|code:nativeplace",fm.all("NativePlace").value,"fm.NativePlace"))
	{
		return;
	}
	
    if(LoadFlag==1)
    {
    	/*yangh于2005-06-03修改
		if(fm.Marriage.value=="")
    	{
    		alert("请填写婚姻状况！");
    		return false;
    	}
    	if(fm.RelationToMainInsured.value=="")
    	{
    		alert("请填写与主被保险人关系！");
    		return false;
    	}
    	if(fm.RelationToAppnt.value=="")
    	{
    		alert("请填写与投保人关系！！");
    		return false;
    	}  */
        //杨红于20050608添加对与被保险人和与投保人关系的校验，达到效果：可以错误定位，并以黄色提示
		//提示； 修改被保人时也要添加与此类似的校验
		//20050608添加开始

		if(!verifyElementWrap2("客户内部号码|notnull",fm.all("SequenceNo").value,"fm.SequenceNo"))
	     {
		return;
	     }
         if(!verifyElementWrap2("与投保人关系|notnull",fm.all("RelationToAppnt").value,"fm.RelationToAppnt"))
	     {
		return;
	     }
         if(!verifyElementWrap2("与第一被保险人关系|notnull",fm.all("RelationToMainInsured").value,"fm.RelationToMainInsured"))
	     {
		return;
	     }

        //20050608添加结束
		//if( verifyInput2() == false ) return false;
    }
   if(fm.PolTypeFlag.value=='1'||fm.PolTypeFlag.value=="3" || fm.PolTypeFlag.value=="4")
   {
         if(!verifyElementWrap2("保障层级|notnull",fm.all("ContPlanCode").value,"fm.ContPlanCode"))
	     {
		return;
	     }
   

   }
    //Added by AppleWood 2005/8/23
//    if(LoadFlag==7&&BQFlag=='2'&&fm.all('ContPlanCode').value=='')
//    {
//    	alert("保障层级不能为空！");
//    	return false;
//    }

   // if (fm.all('PolTypeFlag').value==0)
   // {
   //     if( verifyInput2() == false ) return false;
   // }
   if (fm.all('PolTypeFlag').value==0)
   {
		if(!verifyElementWrap2("生日|date",fm.all("Birthday").value,"fm.Birthday"))
	     {
		   return;
	     }
   }

	if(fm.PolTypeFlag.value != "2"){
	    if(fm.all('IDType').value=="0")
	    {
	       var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
	       if (strChkIdNo != "")
	       {
	             alert(strChkIdNo);
		     return false;
	       }
	    }
    }
    //if(!checkself())
    //return false;

    if(!checkrelation())
    return false;
    if(ImpartGrid.checkValue2(ImpartGrid.name,ImpartGrid)== false)return false;
    if(ImpartDetailGrid.checkValue2(ImpartDetailGrid.name,ImpartDetailGrid)== false)return false;
	ImpartGrid.delBlankLine();
	ImpartDetailGrid.delBlankLine();
     if (fm.InsuredNo.value==''&&fm.AddressNo.value!='')
    {
        alert("被保险人客户号为空，不能有地址号码");
        return false;
    }
    if(LoadFlag==9)
    {
    	if(!checkifnoname())
        return false;
        if(!checkenough())
        return false;
    }
    
    var strli = fm.EnglishName.value.trim();    
    if(strli.length!=0){    
    reg=/^[a-z A-Z]+$/;     
    if(!reg.test(strli)){    
         alert("对不起，您输入的英文名字类型格式不正确!");//请将“英文字母类型”改成你需要验证的属性名称! 
         return;   
    }
    }
    fm.all('ContType').value=ContType;
    fm.all( 'BQFlag' ).value = BQFlag;
    fm.all('fmAction').value="INSERT||CONTINSURED";
    fm.submit();
 }

/*********************************************************************
 *  修改被选中的被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function modifyRecord()
{
/*
		    var tPrtNo=fm.all("PrtNo").value;
      arrResult=easyExecSql("select SequenceNo from LCInsured where PrtNo='"+tPrtNo+"'",1,0);
      for(var sequencenocout=0; sequencenocout<arrResult[0].length;sequencenocout++ )
      {
      if(fm.SequenceNo.value==arrResult[0][sequencenocout]){
	  	alert("已经存在该客户内部号");
	  	fm.SequenceNo.focus();
	  	return false;
	  	}
	    }
*/
	if(!beforeSubmit()){
		return false;
	}
  if(acctype != "2" && acctype != "5"){
		if(!chenckInfo())
			return false;
		checkInsuredNo();
	}
	
  //  if (fm.all('PolTypeFlag').value==0)
  //  {
  //      if( verifyInput2() == false ) return false;
   // }
    //if(!checkself())
     //   return false; 

    //alert("SelNo:"+InsuredGrid.getSelNo());
    var SQL="select RelationToMainInsured,name,sex,Birthday,IDType,IDNo from LCInsured where InsuredNo='"+fm.all('InsuredNo').value+"' and ContNo='"+fm.all('ContNo').value+"'";
    arrResult = easyExecSql(SQL);
  
    if(arrResult != null){
    	if(fm.all('Name').value!=arrResult[0][1]||fm.all('RelationToMainInsured').value!=arrResult[0][0]|| fm.all('IDType').value!=arrResult[0][4]||fm.all('IDNo').value!=arrResult[0][5]|| 
    			fm.all('Sex').value!=arrResult[0][2]||fm.all('Birthday').value!=arrResult[0][3]){
    			alert("基本信息不允许改动!");
    			return ;
    	}
    }
    if (InsuredGrid.mulLineCount==0)
    {
        alert("该被保险人还没有保存，无法进行修改！");
        return false;
    }
    if (fm.InsuredNo.value==''&&fm.AddressNo.value!='')
    {
        alert("被保险人客户号为空，不能有地址号码");
        return false;
    }
    if (fm.all('PolTypeFlag').value==0)
   {
		if(!verifyElementWrap2("生日|date",fm.all("Birthday").value,"fm.Birthday"))
	     {
		   return;
	     }
   }
  
    //hm ASR20093596 理赔通知方式
    if(!verifyElementWrap2("理赔通知方式|code:noticeway",fm.all("NoticeWay").value,"fm.NoticeWay"))
	{
		return;
	}
	//modify by winnie PIR20092538 国籍 变更为 国家或地区 
	//guoxh TASK000033-上传国家或地区处理
  
    var strli = fm.EnglishName.value.trim();    
    if(strli.length!=0){    
    reg=/^[a-z A-Z]+$/;     
    if(!reg.test(strli)){    
         alert("对不起，您输入的英文名字类型格式不正确!");//请将“英文字母类型”改成你需要验证的属性名称! 
         return;   
    }
    }
	//杨红于2005-06-15添加职业代码的校验逻辑
	//添加开始
	/*
    var selno=InsuredGrid.getSelNo();//选中列
	var custumerNo=InsuredGrid.getRowColData(selno-1,1);
	//alert(custumerNo);
	//return;
    arrResult=easyExecSql("select occupationcode from LCAppnt where prtno='"+fm.PrtNo.value+"' and appntno='"+custumerNo+"'",1,0);
	if(arrResult!=null)
	{
      //alert(arrResult[0][0]);
	  //return;
	  if(fm.OccupationCode.value!=arrResult[0][0])
		{
		  alert("被保人和投保人为同一人，请将被保人职业代码更改为"+arrResult[0][0]);
		  return;
		}
	}
	*/
	//添加结束
	

	
    if(ImpartGrid.checkValue2(ImpartGrid.name,ImpartGrid)== false)return false;
    if(ImpartDetailGrid.checkValue2(ImpartDetailGrid.name,ImpartDetailGrid)== false)return false;
	ImpartGrid.delBlankLine();
	ImpartDetailGrid.delBlankLine(); 

    fm.all('ContType').value=ContType;
    fm.all('fmAction').value="UPDATE||CONTINSURED";
 
    fm.submit();
}
/*********************************************************************
 *  删除被选中的被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function deleteRecord()
{
    if (fm.InsuredNo.value=='')
    {
        alert("请选中需要删除的客户！")
        return false;
    }
    if (InsuredGrid.mulLineCount==0)
    {
        alert("该被保险人还没有保存，无法进行修改！");
        return false;
    }
     if (fm.InsuredNo.value==''&&fm.AddressNo.value!='')
    {
        alert("被保险人客户号为空，不能有地址号码");
        return false;
    }
    
    var strSQL = " select 'x' from dual where exists (select '' from lcinsured where MainInsuredNo = '"+fm.all('InsuredNo').value+"' and MainInsuredNo!=insuredno and grpcontno='"+fm.all('GrpContNo').value+"')";
    if(easyExecSql(strSQL)!= null){
    	 alert("该被保人有附属被保人请先删除");
    	 return false;
    }
    if(acctype=="5")
    {
        var strSQL = "select * from lccont where grpcontno= '"+fm.all('GrpContNo').value+"' and pubamncontno = '"+fm.all('ContNo').value+"'";
        if(easyExecSql(strSQL)!= null)
        {
    	 alert("该公共保额有被保险人，不能删除");
    	 return false;
        }
        strSQL = "select '' from lccontplan where grpcontno = '"+fm.all('GrpContNo').value+"' and pubamncontno = '"+fm.all('ContNo').value+"'";
        if(easyExecSql(strSQL)!= null)
        {
    	 alert("该公共保额已绑定了计划，不能删除");
    	 return false;
        }
    }
    fm.all('ContType').value=ContType;
    fm.all('fmAction').value="DELETE||CONTINSURED";
    fm.submit();
}
/*********************************************************************
 *  返回上一页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function returnparent()
{

if(fm.EdorType.value!=null&&fm.EdorType.value=="PB")
{
parent.fraInterface.window.location="../bq/GedorTypePB.jsp";
}
else if(fm.EdorType.value!=null&&fm.EdorType.value=="PE")
{
parent.fraInterface.window.location="../bq/GEdorTypePE.jsp";                                           
}
else
{
  	var backstr=fm.all("ContNo").value;

	mSwitch.addVar("PolNo", "", backstr);
	mSwitch.updateVar("PolNo", "", backstr);
	try
	{
	    mSwitch.deleteVar('ContNo');
	}
	catch(ex){};
	//alert(LoadFlag);
	if(LoadFlag=="1"||LoadFlag=="3")
	{
		//alert(fm.all("PrtNo").value);
  	    location.href="./ContInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag + "&prtNo=" + fm.all("PrtNo").value;
        }
        if(LoadFlag=="5"||LoadFlag=="25")
	{
		//alert(fm.all("PrtNo").value);
  	    location.href="./ContInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag + "&prtNo=" + fm.all("PrtNo").value;
        }

	if(LoadFlag=="2")
	{
  	    location.href="./ContGrpInsuredInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag + "&polNo=" + fm.all("GrpContNo").value+"&scantype="+scantype+ "&GrpContNo=" + fm.all("GrpContNo").value;
        }

	else if (LoadFlag=="6")
	{
	    location.href="ContInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag + "&ContNo=" + backstr+"&prtNo="+fm.all("PrtNo").value+"&Resource="+Resource;
	    return;
	}
	else if (LoadFlag=="7")
	{
	    location.href="../bq/GEdorTypeNI.jsp?BQFlag="+BQFlag;
	    return;
	}
	else if (LoadFlag=="9")
	{
  	    location.href="./ContPolInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag + "&polNo=" + fm.all("GrpContNo").value+"&scantype="+scantype+"&oldContNo="+oldContNo;
	    return;
	}
	else if(LoadFlag=="4"||LoadFlag=="16"||LoadFlag=="13"||LoadFlag=="14"||LoadFlag=="23")
	{
	    if(Auditing=="1")
	    {
	    	top.close();
	    }
	    else
	    {
	    //modify by winnie ASR20093243 +"&GrpContNo=" + fm.all("GrpContNo").value;
     // parent.fraInterface.window.location = "./ContGrpInsuredInput.jsp?LoadFlag="+LoadFlag+"&scantype="+scantype;
            parent.fraInterface.window.location = "./ContGrpInsuredInput.jsp?LoadFlag="+LoadFlag+"&scantype="+scantype+"&GrpContNo=" + fm.all("GrpContNo").value+"&Resource="+Resource;
	    //end modify 20090625
	    }
	}
	else if (LoadFlag=="99")
	{
	    location.href="ContPolInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype;
	    return;
	}
	else if (LoadFlag=="GQ")//从综合查询传入的值
	{
	    if(Auditing=="1")
	    {
	    	top.close();
	    }
	    else
	    {
	    	  //modify by winnie ASR20093243 &GrpContNo=" + fm.all("GrpContNo").value;
          //  parent.fraInterface.window.location = "../sys/ContGrpInsuredInput.jsp?LoadFlag="+LoadFlag+"&scantype="+scantype;
            parent.fraInterface.window.location = "../sys/UliContGrpInsuredInput.jsp?LoadFlag="+LoadFlag+"&scantype="+scantype+"&GrpContNo=" + fm.all("GrpContNo").value;
          //end modify 20090625
	    }
	}
/*    else
    {
        location.href="ContInput.jsp?LoadFlag="+ LoadFlag;
	}  针对	各种情况的不同暂不支持else方式
*/
}
}
/*********************************************************************
 *  进入险种计划界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function grpRiskPlanInfo()
{
    var newWindow = window.open("./GroupRiskPlan.jsp");
}
function getBirthday(){
  var d,s;
  d = new Date();
  s= (d.getYear()-30)+"-";
  s+= (d.getMonth()+1)+"-";
  s+= d.getDate();
  return(s);
  }
/*********************************************************************
 *  代码选择后触发时间
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function fCodeSelect( cCodeName, Field )
{

	if(fm.all('EspecialTag').value=="1"){
 		document.all("DivEspecial").style.display = "";
  }else{
  	document.all("DivEspecial").style.display = "none";
  }
  
  if(fm.all('DoctorEnsure').value=="1"){
 		document.all("DivAppointHospital").style.display = "";
  }else{
  	document.all("DivAppointHospital").style.display = "none";
  }
  
	 if(cCodeName=="AddressNo"){
         getdetailaddress();
  }


    try
    {
        //如果是无名单
        if( cCodeName == "PolTypeFlag")
        {
            if (Field.value=='0')
            {
            		fm.all('InsuredPeoples').value="1";
                fm.all('InsuredPeoples').readOnly=true;
                fm.all('InsuredAppAge').readOnly=true;
                document.all("IDTypeT").style.display = "";
								document.all("IDTypeI").style.display = "";
								document.all("IDNoT").style.display = "";
								document.all("IDNoI").style.display = "";
								
								DivGrpNoname.style.display="none";
								DivAccInsured.style.display="";
            }
            else if (Field.value=='1'||Field.value=="3" || Field.value=="4")
            {
                fm.all('Name').value="无名单";
                fm.all('Sex').value="0";
                fm.all('Birthday').value=getBirthday();
                fm.all('IDType').value = "8";
                fm.all('OccupationCode').value="0";
                fm.all('InsuredAppAge').value="30";
                fm.all('OccupationType').value="1";
                fm.all('InsuredPeoples').value="";
                fm.all('InsuredPeoples').readOnly=false;
                fm.all('InsuredAppAge').readOnly=false;
                
                if(fm.ContPlanCodeName.elementtype=="nacessary")
                {
                }
                else
                {
                fm.ContPlanCodeName.elementtype="nacessary";
                //initElementtype();
                fm.ContPlanCodeName.insertAdjacentHTML("afterEnd","<font id='"+fm.ContPlanCodeName.name+"n' color=red>&nbsp;*</font>");
                }
                var strSQL = "select max(idno) from lcinsured where name='无名单' and idtype='8'";
								arrResult=easyExecSql(strSQL);
								//如果存在无名单的最大流水号，则顺序加一
								//请注意Int型最大65535
								if(arrResult!=null)
								{
										var mIdno = arrResult[0][0];
										if(mIdno.length>6)
										{
												mIdno=mIdno.substring(mIdno.length-6,mIdno.length);
										}
										mIdno="00000"+(parseFloat(mIdno)+1);
									
										fm.all('IDNo').value = mIdno.substring(mIdno.length-6,mIdno.length);

								}
								else
								{
								//如果不存在，即第一次录入无名单，则默认为000001
										fm.all('IDNo').value = "00000"+1;
								}
                DivGrpNoname.style.display="";
                DivAccInsured.style.display="";
                document.all("IDTypeT").style.display = "";
								document.all("IDTypeI").style.display = "";
								document.all("IDNoT").style.display = "";
								document.all("IDNoI").style.display = "";

                showCodeName();
            }
            else if (Field.value=='2')
            {

                fm.all('Name').value="公共账户";
                fm.all('Sex').value="0";
                fm.all('Birthday').value=getBirthday();
                fm.all('IDType').value = "8";
                fm.all('OccupationCode').value="0";
                fm.all('InsuredAppAge').value="30";
                fm.all('OccupationType').value="1";
                fm.all('InsuredPeoples').value="";
                fm.all('InsuredPeoples').readOnly=false;
                fm.all('InsuredAppAge').readOnly=false;
                DivGrpNoname.style.display="none";
                DivAccInsured.style.display="none";
                document.all("IDTypeT").style.display = "none";
								document.all("IDTypeI").style.display = "none";
								document.all("IDNoT").style.display = "none";
								document.all("IDNoI").style.display = "none";

								document.all("TempT").style.display = "";
								document.all("TempI").style.display = "";
								document.all("Temp1T").style.display = "";
								document.all("Temp1I").style.display = "";

                showCodeName();
            }
        }
         if( cCodeName == "ImpartCode")
         {

         }
         if( cCodeName == "SequenceNo")
         {
	   if(Field.value=="1"&&fm.SamePersonFlag.checked==false)
	   {
	   	     emptyInsured();
		     param="121";
		     fm.pagename.value="121";
                     fm.InsuredSequencename.value="第一被保险人资料";
                     fm.RelationToMainInsured.value="00";
	   }
	   if(Field.value=="2"&&fm.SamePersonFlag.checked==false)
	   {
	   	if(InsuredGrid.mulLineCount==0)
	   	{
	   		alert("请先添加第一被保人");
	   		fm.SequenceNo.value="1";
	   		return false;
	   	}
	   	     emptyInsured();
                     noneedhome();
		     param="122";
		     fm.pagename.value="122";
                     fm.InsuredSequencename.value="第二被保险人资料";
	   }
	   if(Field.value=="3"&&fm.SamePersonFlag.checked==false)
	   {
	   	if(InsuredGrid.mulLineCount==0)
	   	{
	   		alert("请先添加第一被保人");
	   		Field.value="1";
	   		return false;
	   	}
	   	if(InsuredGrid.mulLineCount==1)
	   	{
	   		alert("请先添加第二被保人");
	   		Field.value="1";
	   		return false;
	   	}
	   	     emptyInsured();
                     noneedhome();
		     param="123";
		     fm.pagename.value="123";
                     fm.InsuredSequencename.value="第三被保险人资料";
	   }
           if (scantype== "scan")
           {
           setFocus();
           }
         }
         if( cCodeName == "CheckPostalAddress")
         {
	 if(fm.CheckPostalAddress.value=="1")
	 {
	 	fm.all('PostalAddress').value=fm.all('GrpAddress').value;
                fm.all('ZipCode').value=fm.all('GrpZipCode').value;
                fm.all('Phone').value= fm.all('GrpPhone').value;

	 }
	 else if(fm.CheckPostalAddress.value=="2")
	 {
	 	fm.all('PostalAddress').value=fm.all('HomeAddress').value;
                fm.all('ZipCode').value=fm.all('HomeZipCode').value;
                fm.all('Phone').value= fm.all('HomePhone').value;
	 }
	 else if(fm.CheckPostalAddress.value=="3")
	 {
	 	fm.all('PostalAddress').value="";
                fm.all('ZipCode').value="";
                fm.all('Phone').value= "";
	 }
        }
         if( cCodeName == "OccupationCode")
         {
         	getdetailwork();
         }
    }
    catch(ex) {}

//理赔金帐户带来投保人信息
	if(fm.AccountNo.value=="1")
	{					
           fm.all('BankAccNo').value=mSwitch.getVar("AppntBankAccNo");
           fm.all('BankCode').value=mSwitch.getVar("AppntBankCode");
           fm.all('AccName').value=mSwitch.getVar("AppntAccName");
	}
	if(fm.AccountNo.value=="2")
	{	
           fm.all('BankAccNo').value="";
           fm.all('BankCode').value="";
           fm.all('AccName').value="";
	}
}
/*********************************************************************
 *  显示家庭单下被保险人的信息
 *  返回值：  无
 *********************************************************************
 */
function getInsuredInfo()
{
    var ContNo=fm.all("ContNo").value;
    if(ContNo!=null&&ContNo!="")
    {
        var strSQL ="select InsuredNo,Name,Sex,Birthday,RelationToMainInsured,SequenceNo from LCInsured where ContNo='"+ContNo+"'and InsuredNo='"+InsuredNo+"'";
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult)
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = InsuredGrid;
        //保存SQL语句
        turnPage.strQuerySql = strSQL;
        //设置查询起始位置
        turnPage.pageIndex = 0;
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}

/*********************************************************************
 *  获得个单合同的被保人信息
 *  返回值：  无
 *********************************************************************
 */
function getProposalInsuredInfo()
{
    var tContNo=fm.all("ContNo").value;
   
    arrResult=easyExecSql("select * from insured_view where ContNo='"+tContNo+"'and InsuredNo='"+InsuredNo+"'",1,0);
    if(arrResult==null||InsuredGrid.mulLineCount>1)
    {
        return;
    }
    else
    {
    	if(InsuredGrid.mulLineCount=1){
        DisplayInsured();//该合同下的被投保人信息
        //alert(arrResult[0][33]+fm.all('salary').value);
        var tCustomerNo = arrResult[0][2];		// 得到投保人客户号
        var tAddressNo = arrResult[0][10]; 		// 得到投保人地址号
        arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo='"+tCustomerNo+"'",1,0);
       }
        if(arrResult==null)
        {
            //alert("未得到用户信息");
            //return;
        }
        else
        {
            displayAppnt();       //显示投保人详细内容
            emptyUndefined();
            fm.AddressNo.value=tAddressNo;
            getdetailaddress();//显示投保人地址详细内容
        }
    }
    if(fm.RelationToAppnt.value==""||fm.RelationToMainInsured.value==""||fm.Marriage.value==""||fm.OccupationCode.value=="")
    {
        checkapply="0";                         //针对初审信息填充不足
    }
    else
    {
    	checkapply="1";
    }
    getPerBankinfor();
    getInsuredPolInfo();
    showCodeName();
}
function getPerBankinfor(){
    var tContNo=fm.all("ContNo").value; 
    arrResult=easyExecSql("select Bankcode,(select BankName from ldbank where Bankcode=l.Bankcode ),AccName ,BankAccNo from lccont l where ContNo='"+tContNo+"' union select Bankcode,(select BankName from ldbank where Bankcode=l.Bankcode ),AccName ,BankAccNo from lbcont l where ContNo='"+tContNo+"'",1,0);
   if(arrResult==null)
    {
        return;
    }
    else
    {
    
        fm.PerBankCode.value = arrResult[0][0];		// 得到投保人客户号
        fm.PerBankName.value = arrResult[0][1];	
        fm.PerAccName.value = arrResult[0][2];	
        fm.PerBankAccNo.value = arrResult[0][3];	
              
}
}
/*********************************************************************
 *  MulLine的RadioBox点击事件，获得被保人详细信息，填入被保人信息表
 *  返回值：  无
 *********************************************************************
 */
function getInsuredDetail(parm1,parm2)
{
    var InsuredNo=fm.all(parm1).all('InsuredGrid1').value;
    var ContNo = fm.ContNo.value;
    //被保人详细信息
    var strSQL ="select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo='"+InsuredNo+"'";
    arrResult=easyExecSql(strSQL);
    if(arrResult!=null)
    {
        displayAppnt();
    }
    strSQL ="select * from LCInsured where ContNo = '"+ContNo+"' and InsuredNo='"+InsuredNo+"'";
    arrResult=easyExecSql(strSQL);
    if(arrResult!=null)
    {
        DisplayInsured();
		//alert(arrResult[0][33]);
		var arrResult2=easyExecSql("select codename from ldcode where codetype= 'occupationtype' and code='"+arrResult[0][34]+"'",1,0);
	    try{fm.OccupationTypeName.value=arrResult2[0][0];}catch(ex){}
    }
    if(fm.RelationToAppnt.value==""||fm.RelationToMainInsured.value==""||fm.Marriage.value==""||fm.OccupationCode.value=="")
    {
        checkapply="0";                         //针对初审信息填充不足
    }
    else
    {
    	checkapply="1";
    }
    var tAddressNo = arrResult[0][10]; 		// 得到被保人地址号
    fm.AddressNo.value=tAddressNo;
    getdetailaddress();
    getInsuredPolInfo();
	 if(fm.SequenceNo.value=='1')
	  {
		   fm.InsuredSequencename.value="第一被保险人资料";
	  }
	  if(fm.SequenceNo.value=='2')
	  {
		   fm.InsuredSequencename.value="第二被保险人资料";
	  }
	  if(fm.SequenceNo.value=='3')
	  {
		   fm.InsuredSequencename.value="第三被保险人资料";
	  }
    getImpartInfo();
    getImpartDetailInfo();
    showCodeName();
}
/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayCustomer()
{
	try{fm.all('Nationality').value= arrResult[0][8]; }catch(ex){};

}
/*********************************************************************
 *  把查询返回的客户地址数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayAddress()
{
	try{fm.all('PostalAddress').value= arrResult[0][2]; }catch(ex){};
	try{fm.all('ZipCode').value= arrResult[0][3]; }catch(ex){};
	try{fm.all('Phone').value= arrResult[0][4]; }catch(ex){};
	try{fm.all('Mobile').value= arrResult[0][14]; }catch(ex){};
	try{fm.all('EMail').value= arrResult[0][16]; }catch(ex){};
	//try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){};
	try{fm.all('GrpPhone').value= arrResult[0][12]; }catch(ex){};
	try{fm.all('CompanyAddress').value= arrResult[0][10]; }catch(ex){};
	try{fm.all('GrpZipCode').value= arrResult[0][11]; }catch(ex){};
}
/*********************************************************************
 *  显示被保人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayInsured()
{
    try{fm.all('GrpContNo').value=arrResult[0][0];}catch(ex){};
    try{fm.all('ContNo').value=arrResult[0][1];}catch(ex){};
    try{fm.all('InsuredNo').value=arrResult[0][2];}catch(ex){};
    try{fm.all('PrtNo').value=arrResult[0][3];}catch(ex){};
    try{fm.all('AppntNo').value=arrResult[0][4];}catch(ex){};
    try{fm.all('ManageCom').value=arrResult[0][5];}catch(ex){};
    try{fm.all('ExecuteCom').value=arrResult[0][6];}catch(ex){};
    try{fm.all('FamilyID').value=arrResult[0][7];}catch(ex){};
    try{fm.all('RelationToMainInsured').value=arrResult[0][8];}catch(ex){};
    try{fm.all('RelationToAppnt').value=arrResult[0][9];}catch(ex){};
    try{fm.all('AddressNo').value=arrResult[0][10];}catch(ex){};
    try{fm.all('SequenceNo').value=arrResult[0][11];}catch(ex){};
    try{fm.all('Name').value=arrResult[0][12];}catch(ex){};
    try{fm.all('Sex').value=arrResult[0][13];}catch(ex){};
    try{fm.all('Birthday').value=arrResult[0][14];}catch(ex){};
    try{fm.all('IDType').value=arrResult[0][15];}catch(ex){};
    try{fm.all('IDNo').value=arrResult[0][16];}catch(ex){};
    try{fm.all('ConfirmIDNo').value=arrResult[0][16];}catch(ex){};
    try{fm.all('NativePlace').value=arrResult[0][17];}catch(ex){};
    try{fm.all('Nationality').value=arrResult[0][18];}catch(ex){};
    try{fm.all('RgtAddress').value=arrResult[0][19];}catch(ex){};
    try{fm.all('Marriage').value=arrResult[0][20];}catch(ex){};
    try{fm.all('MarriageDate').value=arrResult[0][21];}catch(ex){};
    try{fm.all('Health').value=arrResult[0][22];}catch(ex){};
    try{fm.all('Stature').value=arrResult[0][23];}catch(ex){};
    try{fm.all('Avoirdupois').value=arrResult[0][24];}catch(ex){};
    try{fm.all('Degree').value=arrResult[0][25];}catch(ex){};
    try{fm.all('CreditGrade').value=arrResult[0][26];}catch(ex){};
    try{fm.all('BankCode').value=arrResult[0][27];}catch(ex){};
    try{fm.all('BankAccNo').value=arrResult[0][28];}catch(ex){};
    try{fm.all('AccName').value=arrResult[0][29];}catch(ex){};
    try{fm.all('JoinCompanyDate').value=arrResult[0][30];}catch(ex){};
    try{fm.all('StartWorkDate').value=arrResult[0][31];}catch(ex){};
    try{fm.all('Position').value=arrResult[0][32];}catch(ex){};
    try{fm.all('Salary').value=arrResult[0][33];}catch(ex){};
    try{fm.all('OccupationType').value=arrResult[0][34];}catch(ex){};
    try{fm.all('OccupationCode').value=arrResult[0][35];}catch(ex){};
    try{fm.all('WorkType').value=arrResult[0][36];}catch(ex){};
    try{fm.all('PluralityType').value=arrResult[0][37];}catch(ex){};
    try{fm.all('SmokeFlag').value=arrResult[0][38];}catch(ex){};
    try{fm.all('ContPlanCode').value=arrResult[0][39];}catch(ex){};
    try{fm.all('Operator').value=arrResult[0][40];}catch(ex){};
    try{fm.all('MakeDate').value=arrResult[0][41];}catch(ex){};
    try{fm.all('MakeTime').value=arrResult[0][42];}catch(ex){};
    try{fm.all('ModifyDate').value=arrResult[0][43];}catch(ex){};
    try{fm.all('ModifyTime').value=arrResult[0][44];}catch(ex){};
    try{fm.all('BankInfo').value= arrResult[0][83];}catch(ex){};
    try{fm.all('BankPrv').value= arrResult[0][85];}catch(ex){};
    try{fm.all('BankCity').value= arrResult[0][86];}catch(ex){};
    try{fm.all('MainInsuredNo').value= arrResult[0][87];}catch(ex){};
    try{fm.all('DepartmentId').value= arrResult[0][89];}catch(ex){};
    try{fm.all('OrganComCode').value= arrResult[0][57];}catch(ex){};
    try{fm.all('OrganInnerCode').value= arrResult[0][70];}catch(ex){};
    try{fm.all('InsuredPeoples').value= arrResult[0][51];}catch(ex){};
    try{fm.all('InsuredID').value= arrResult[0][67];}catch(ex){}; 
    //chenwm012208 退休在职标记取lcinsured里面的,不从ldperson里面取,因为每个保单可能录入的不一样.   
    try{fm.all('RetireFlag').value= arrResult[0][84];}catch(ex){};     
    if(arrResult[0][87]!="")
    {
    arrResult1=easyExecSql("select name from LCInsured where insuredno='"+arrResult[0][87]+"'",1,0);
    try{fm.all('MainName').value= arrResult1[0][0];}catch(ex){};
    }
	
    displayLDPerson();
}
function displayissameperson()
{
    try{fm.all('InsuredNo').value= mSwitch.getVar( "AppntNo" ); }catch(ex){};
    try{fm.all('Name').value= mSwitch.getVar( "AppntName" ); }catch(ex){};
    try{fm.all('Sex').value= mSwitch.getVar( "AppntSex" ); }catch(ex){};
    try{fm.all('Birthday').value= mSwitch.getVar( "AppntBirthday" ); }catch(ex){};
    try{fm.all('IDType').value= mSwitch.getVar( "AppntIDType" ); }catch(ex){};
    try{fm.all('IDNo').value= mSwitch.getVar( "AppntIDNo" ); }catch(ex){};
    try{fm.all('Password').value= mSwitch.getVar( "AppntPassword" ); }catch(ex){};
    try{fm.all('NativePlace').value= mSwitch.getVar( "AppntNativePlace" ); }catch(ex){};
    try{fm.all('Nationality').value= mSwitch.getVar( "AppntNationality" ); }catch(ex){};
    try{fm.all('AddressNo').value= mSwitch.getVar( "AddressNo" ); }catch(ex){};
    try{fm.all('RgtAddress').value= mSwitch.getVar( "AppntRgtAddress" ); }catch(ex){};
    try{fm.all('Marriage').value= mSwitch.getVar( "AppntMarriage" );}catch(ex){};
    try{fm.all('MarriageDate').value= mSwitch.getVar( "AppntMarriageDate" );}catch(ex){};
    try{fm.all('Health').value= mSwitch.getVar( "AppntHealth" );}catch(ex){};
    try{fm.all('Stature').value= mSwitch.getVar( "AppntStature" );}catch(ex){};
    try{fm.all('Avoirdupois').value= mSwitch.getVar( "AppntAvoirdupois" );}catch(ex){};
    try{fm.all('Degree').value= mSwitch.getVar( "AppntDegree" );}catch(ex){};
    try{fm.all('CreditGrade').value= mSwitch.getVar( "AppntDegreeCreditGrade" );}catch(ex){};
    try{fm.all('OthIDType').value= mSwitch.getVar( "AppntOthIDType" );}catch(ex){};
    try{fm.all('OthIDNo').value= mSwitch.getVar( "AppntOthIDNo" );}catch(ex){};
    try{fm.all('ICNo').value= mSwitch.getVar( "AppntICNo" );}catch(ex){};
    try{fm.all('GrpNo').value= mSwitch.getVar( "AppntGrpNo" );}catch(ex){};
    try{fm.all( 'JoinCompanyDate' ).value = mSwitch.getVar( "JoinCompanyDate" ); if(fm.all( 'JoinCompanyDate' ).value=="false"){fm.all( 'JoinCompanyDate' ).value="";} } catch(ex) { };
    try{fm.all('StartWorkDate').value= mSwitch.getVar( "AppntStartWorkDate" );}catch(ex){};
    try{fm.all('Position').value= mSwitch.getVar( "AppntPosition" );}catch(ex){};
    try{fm.all( 'Position' ).value = mSwitch.getVar( "Position" ); if(fm.all( 'Position' ).value=="false"){fm.all( 'Position' ).value="";} } catch(ex) { };
    try{fm.all('Salary').value= mSwitch.getVar( "AppntSalary" );}catch(ex){};
    try{fm.all( 'Salary' ).value = mSwitch.getVar( "Salary" ); if(fm.all( 'Salary' ).value=="false"){fm.all( 'Salary' ).value="";} } catch(ex) { };
    try{fm.all('OccupationType').value= mSwitch.getVar( "AppntOccupationType" );}catch(ex){};
    try{fm.all('OccupationCode').value= mSwitch.getVar( "AppntOccupationCode" );}catch(ex){};
    try{fm.all('WorkType').value= mSwitch.getVar( "AppntWorkType" );}catch(ex){};
    try{fm.all('PluralityType').value= mSwitch.getVar( "AppntPluralityType" );}catch(ex){};
    try{fm.all('DeathDate').value= mSwitch.getVar( "AppntDeathDate" );}catch(ex){};
    try{fm.all('SmokeFlag').value= mSwitch.getVar( "AppntSmokeFlag" );}catch(ex){};
    try{fm.all('BlacklistFlag').value= mSwitch.getVar( "AppntBlacklistFlag" );}catch(ex){};
    try{fm.all('Proterty').value= mSwitch.getVar( "AppntProterty" );}catch(ex){};
    try{fm.all('Remark').value= mSwitch.getVar( "AppntRemark" );}catch(ex){};
    try{fm.all('State').value= mSwitch.getVar( "AppntState" );}catch(ex){};
    try{fm.all('Operator').value= mSwitch.getVar( "AppntOperator" );}catch(ex){};
    try{fm.all('MakeDate').value= mSwitch.getVar( "AppntMakeDate" );}catch(ex){};
    try{fm.all('MakeTime').value= mSwitch.getVar( "AppntMakeTime" );}catch(ex){};
    try{fm.all('ModifyDate').value= mSwitch.getVar( "AppntModifyDate" );}catch(ex){};
    try{fm.all('ModifyTime').value= mSwitch.getVar( "AppntModifyTime" );}catch(ex){};
    try{fm.all('PostalAddress').value= mSwitch.getVar( "AppntPostalAddress" );}catch(ex){};
    try{fm.all('PostalAddress').value= mSwitch.getVar( "AppntPostalAddress" );}catch(ex){};
    try{fm.all('ZipCode').value= mSwitch.getVar( "AppntZipCode" );}catch(ex){};
    try{fm.all('Phone').value= mSwitch.getVar( "AppntPhone" );}catch(ex){};
    try{fm.all('Fax').value= mSwitch.getVar( "AppntFax" );}catch(ex){};
    try{fm.all('Mobile').value= mSwitch.getVar( "AppntMobile" );}catch(ex){};
    try{fm.all('EMail').value= mSwitch.getVar( "AppntEMail" );}catch(ex){};
    try{fm.all('GrpName').value= mSwitch.getVar( "AppntGrpName" );}catch(ex){};
    try{fm.all('GrpPhone').value= mSwitch.getVar( "AppntGrpPhone" );}catch(ex){};
    try{fm.all('GrpAddress').value= mSwitch.getVar( "CompanyAddress" );}catch(ex){};
    try{fm.all('GrpZipCode').value= mSwitch.getVar( "AppntGrpZipCode" );}catch(ex){};
    try{fm.all('GrpFax').value= mSwitch.getVar( "AppntGrpFax" );}catch(ex){};
    try{fm.all('HomeAddress').value= mSwitch.getVar( "AppntHomeAddress" );}catch(ex){};
    try{fm.all('HomePhone').value= mSwitch.getVar( "AppntHomePhone" );}catch(ex){};
    try{fm.all('HomeZipCode').value= mSwitch.getVar( "AppntHomeZipCode" );}catch(ex){};
    try{fm.all('HomeFax').value= mSwitch.getVar( "AppntHomeFax" );}catch(ex){};
    try{fm.all('BankAccNo').value= mSwitch.getVar( "AppntBankAccNo" );}catch(ex){};
    try{fm.all('BankCode').value= mSwitch.getVar( "AppntBankCode" );}catch(ex){};
    try{fm.all('AccName').value= mSwitch.getVar( "AppntAccName" );}catch(ex){};
}
/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartInfo()
{
    initImpartGrid();
    var InsuredNo=fm.all("InsuredNo").value;
    var ContNo=fm.all("ContNo").value;
    //告知信息初始化
    if(InsuredNo!=null&&InsuredNo!="")
    {
        var strSQL ="select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LCCustomerImpart where CustomerNo='"+InsuredNo+"' and ProposalContNo='"+ContNo+"' and CustomerNoType='I'";
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult)
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = ImpartGrid;
        //保存SQL语句
        turnPage.strQuerySql = strSQL;
        //设置查询起始位置
        turnPage.pageIndex = 0;
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}
/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartDetailInfo()
{
    initImpartDetailGrid();
    var InsuredNo=fm.all("InsuredNo").value;
    var ContNo=fm.all("ContNo").value;
    //告知信息初始化
    if(InsuredNo!=null&&InsuredNo!="")
    {
        var strSQL ="select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where CustomerNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and CustomerNoType='I'";
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult)
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = ImpartDetailGrid;
        //保存SQL语句
        turnPage.strQuerySql = strSQL;
        //设置查询起始位置
        turnPage.pageIndex = 0;
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}
/*********************************************************************
 *  获得被保人险种信息，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getInsuredPolInfo()
{  
    initPolGrid();
    var InsuredNo=fm.all("InsuredNo").value;
    var ContNo=fm.all("ContNo").value;
    //险种信息初始化
    if(InsuredNo!=null&&InsuredNo!="")
    {
        var strSQL ="select PolNo,RiskCode,Prem,Amnt from LCPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'";
        //alert(strSQL);
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult)
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = PolGrid;
        //保存SQL语句
        turnPage.strQuerySql = strSQL;
        //设置查询起始位置
        turnPage.pageIndex = 0;
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}
/*********************************************************************
 *  MulLine的RadioBox点击事件，获得被保人险种详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolDetail(parm1,parm2)
{
    var PolNo=fm.all(parm1).all('PolGrid1').value
    try{mSwitch.deleteVar('PolNo')}catch(ex){};
    try{mSwitch.addVar('PolNo','',PolNo);}catch(ex){};
    fm.SelPolNo.value=PolNo;
}
/*********************************************************************
 *  根据家庭单类型，隐藏界面控件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function choosetype(){
	if(fm.FamilyType.value=="1")
	divTempFeeInput.style.display="";
	if(fm.FamilyType.value=="0")
	divTempFeeInput.style.display="none";
}
/*********************************************************************
 *  校验被保险人与主被保险人关系
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function checkself()
{
	if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value=="")
	{
	    fm.RelationToMainInsured.value="00";
	    return true;
    }
	else if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value!="00")
	{
	    alert("个人单中'与主被保险人关系'只能是'本人'");
	    fm.RelationToMainInsured.value="00";
	    return false;
    }
	else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value==""&&InsuredGrid.mulLineCount==0)
	{
	    fm.RelationToMainInsured.value="00";
	    return true;
    }
    else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value!="00"&&InsuredGrid.mulLineCount==0)
    {
	    alert("家庭单中第一位被保险人的'与主被保险人关系'只能是'本人'");
	    fm.RelationToMainInsured.value="00";
	    return false;
    }
    else
        return true;
}
/*********************************************************************
 *  校验保险人
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function checkrelation()
{
	if(LoadFlag==2||LoadFlag==7)
	{    
        if (fm.all('ContNo').value != "")
        {
            //alert("团单的个单不能有多被保险人");
            alert("该被保险人已经保存，不能重复添加！");
            return false;
        }
        else
            return true;
    }
    else
    {
        if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="0")
        {
            var strSQL="select * from LCInsured where contno='"+fm.all('ContNo').value +"'";
            turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
            if(turnPage.strQueryResult)
            {
                alert("个单不能有多被保险人");
                return false;
            }
            else
                return true;
        }
        else if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="1"&&InsuredGrid.mulLineCount>0&&fm.RelationToMainInsured.value=="00")
        {
            alert("家庭单只能有一个主被保险人");
            return false;
        }
        else if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="1"&&fm.RelationToAppnt.value=="00")
        {
            var strSql="select * from LCInsured where contno='"+fm.all('ContNo').value +"' and RelationToAppnt='00' ";
            turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);
            if(turnPage.strQueryResult)
		    {
                alert("投保人已经是该合同号下的被保险人");
                return false;
            }
		    else
		        return true;
        }
        else
            return true;
    }
	//select count(*) from ldinsured

}
/*********************************************************************
 *  投保人与被保人相同选择框事件
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function isSamePerson()
{
    //对应未选同一人，又打钩的情况
    if ( fm.SamePersonFlag.checked==true&&fm.RelationToAppnt.value!="00")
    {
      fm.all('DivLCInsured').style.display = "none";
	  fm.img4.src="../common/images/butCollapse.gif";
      divLCInsuredPerson.style.display = "none";
      divSalary.style.display = "none";
      fm.SamePersonFlag.checked = true;
      fm.RelationToAppnt.value="00"
      displayissameperson();
    }
    //对应是同一人，又打钩的情况
    else if (fm.SamePersonFlag.checked == true)
    {
      fm.all('DivLCInsured').style.display = "none";
	  fm.img4.src="../common/images/butCollapse.gif";
      divLCInsuredPerson.style.display = "none";
      divSalary.style.display = "none";
      displayissameperson();
    }
    //对应不选同一人的情况
    else if (fm.SamePersonFlag.checked == false)
    {
    fm.all('DivLCInsured').style.display = "";
	fm.img4.src="../common/images/butExpand.gif";
    divLCInsuredPerson.style.display = "none";
    divSalary.style.display = "none";
	try{fm.all('InsuredNo').value="";}catch(ex){};
    try{fm.all('Name').value=""; }catch(ex){};
    try{fm.all('Sex').value= ""; }catch(ex){};
    try{fm.all('Birthday').value= ""; }catch(ex){};
    try{fm.all('IDType').value= "0"; }catch(ex){};
    try{fm.all('IDNo').value= ""; }catch(ex){};
    try{fm.all('Password').value= ""; }catch(ex){};
    try{fm.all('NativePlace').value= ""; }catch(ex){};
    try{fm.all('Nationality').value=""; }catch(ex){};
    try{fm.all('RgtAddress').value= ""; }catch(ex){};
    try{fm.all('Marriage').value= "";}catch(ex){};
    try{fm.all('MarriageDate').value= "";}catch(ex){};
    try{fm.all('Health').value= "";}catch(ex){};
    try{fm.all('Stature').value= "";}catch(ex){};
    try{fm.all('Avoirdupois').value= "";}catch(ex){};
    try{fm.all('Degree').value= "";}catch(ex){};
    try{fm.all('CreditGrade').value= "";}catch(ex){};
    try{fm.all('OthIDType').value= "";}catch(ex){};
    try{fm.all('OthIDNo').value= "";}catch(ex){};
    try{fm.all('ICNo').value="";}catch(ex){};
    try{fm.all('GrpNo').value= "";}catch(ex){};
    try{fm.all('JoinCompanyDate').value= "";}catch(ex){}
    try{fm.all('StartWorkDate').value= "";}catch(ex){};
    try{fm.all('Position').value= "";}catch(ex){};
    try{fm.all('Salary').value= "";}catch(ex){};
    try{fm.all('OccupationType').value= "";}catch(ex){};
    try{fm.all('OccupationCode').value= "";}catch(ex){};
    try{fm.all('WorkType').value= "";}catch(ex){};
    try{fm.all('PluralityType').value= "";}catch(ex){};
    try{fm.all('DeathDate').value= "";}catch(ex){};
    try{fm.all('SmokeFlag').value= "";}catch(ex){};
    try{fm.all('BlacklistFlag').value= "";}catch(ex){};
    try{fm.all('Proterty').value= "";}catch(ex){};
    try{fm.all('Remark').value= "";}catch(ex){};
    try{fm.all('State').value= "";}catch(ex){};
    try{fm.all('Operator').value= "";}catch(ex){};
    try{fm.all('MakeDate').value= "";}catch(ex){};
    try{fm.all('MakeTime').value="";}catch(ex){};
    try{fm.all('ModifyDate').value= "";}catch(ex){};
    try{fm.all('ModifyTime').value= "";}catch(ex){};
    try{fm.all('PostalAddress').value= "";}catch(ex){};
    try{fm.all('PostalAddress').value= "";}catch(ex){};
    try{fm.all('ZipCode').value= "";}catch(ex){};
    try{fm.all('Phone').value= "";}catch(ex){};
    try{fm.all('Mobile').value= "";}catch(ex){};
    try{fm.all('EMail').value="";}catch(ex){};
    try{fm.all('GrpName').value= "";}catch(ex){};
    try{fm.all('GrpPhone').value= "";}catch(ex){};
    try{fm.all('GrpAddress').value="";}catch(ex){};
    try{fm.all('GrpZipCode').value= "";}catch(ex){};

    }
}
/*********************************************************************
 *  投保人客户号查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryInsuredNo()
{
    if (fm.all("InsuredNo").value == "")
    {
      showAppnt1();
    }
    else
    {
        //arrResult = easyExecSql("select a.*,b.AddressNo,b.PostalAddress,b.ZipCode,b.HomePhone,b.Mobile,b.EMail,a.GrpNo,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode from LDPerson a,LCAddress b where 1=1 and a.CustomerNo=b.CustomerNo and a.CustomerNo = '" + fm.all("InsuredNo").value + "'", 1, 0);
        var inSql="select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + fm.all("InsuredNo").value + "'";
        arrResult = easyExecSql(inSql, 1, 0);
        //alert(arrResult);
        if (arrResult == null)
        {
          alert("未查到投保人信息");
          //displayAppnt(new Array());
          emptyUndefined();
        }
        else
        {
          displayAppnt11(arrResult[0]);
        }

    }
}
/*********************************************************************
 *  投保人查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt1()
{
	if( mOperate == 0 )
	{
		mOperate = 2;
		showInfo = window.open( "../sys/LDPersonQueryNew.html" );
	}
}
/*********************************************************************
 *  显示投保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt()
{
    //modified by zhangjq on 2008-5-12 将大部分已经在DisplayInsured()函数中置值了元素置值语句的进行注释
    //try{fm.all('InsuredNo').value= arrResult[0][0]; }catch(ex){};
    //try{fm.all('Name').value= arrResult[0][1]; }catch(ex){};
    //try{fm.all('Sex').value= arrResult[0][2]; }catch(ex){};
    //try{fm.all('Birthday').value= arrResult[0][3]; }catch(ex){};
    //try{fm.all('IDType').value= arrResult[0][4]; }catch(ex){};
    //try{fm.all('IDNo').value= arrResult[0][5]; }catch(ex){};
    //try{fm.all('ConfirmIDNo').value= arrResult[0][5]; }catch(ex){};
    try{fm.all('Password').value= arrResult[0][6]; }catch(ex){};
    //try{fm.all('NativePlace').value= arrResult[0][7]; }catch(ex){};
    //try{fm.all('Nationality').value= arrResult[0][8]; }catch(ex){};
    //try{fm.all('RgtAddress').value= arrResult[0][9]; }catch(ex){};
    //try{fm.all('Marriage').value= arrResult[0][10];}catch(ex){};
    //try{fm.all('MarriageDate').value= arrResult[0][11];}catch(ex){};
    //try{fm.all('Health').value= arrResult[0][12];}catch(ex){};
    //try{fm.all('Stature').value= arrResult[0][13];}catch(ex){};
    //try{fm.all('Avoirdupois').value= arrResult[0][14];}catch(ex){};
    //try{fm.all('Degree').value= arrResult[0][15];}catch(ex){};
    //try{fm.all('CreditGrade').value= arrResult[0][16];}catch(ex){};
    try{fm.all('OthIDType').value= arrResult[0][17];}catch(ex){};
    try{fm.all('OthIDNo').value= arrResult[0][18];}catch(ex){};
    try{fm.all('ICNo').value= arrResult[0][19];}catch(ex){};
    try{fm.all('GrpNo').value= arrResult[0][20];}catch(ex){};
//chenwm071126 隐藏掉来自ldperson中的JoinCompanyDate,Position,Salary因为新契约只存在lcinsured
//    try{fm.all('JoinCompanyDate').value= arrResult[0][21];}catch(ex){};
    //try{fm.all('StartWorkDate').value= arrResult[0][22];}catch(ex){};
//    try{fm.all('Position').value= arrResult[0][23];}catch(ex){};
//    try{fm.all('Salary').value= arrResult[0][24];}catch(ex){};
//    try{fm.all('OccupationType').value= arrResult[0][25];}catch(ex){};
    //try{fm.all('OccupationCode').value= arrResult[0][26];}catch(ex){};
    //try{fm.all('WorkType').value= arrResult[0][27];}catch(ex){};
    //try{fm.all('PluralityType').value= arrResult[0][28];}catch(ex){};
    try{fm.all('DeathDate').value= arrResult[0][29];}catch(ex){};
    //try{fm.all('SmokeFlag').value= arrResult[0][30];}catch(ex){};
    try{fm.all('BlacklistFlag').value= arrResult[0][31];}catch(ex){};
    try{fm.all('Proterty').value= arrResult[0][32];}catch(ex){};
    try{fm.all('Remark').value= arrResult[0][33];}catch(ex){};
    try{fm.all('State').value= arrResult[0][34];}catch(ex){};
    //try{fm.all('Operator').value= arrResult[0][35];}catch(ex){};
    //try{fm.all('MakeDate').value= arrResult[0][36];}catch(ex){};
    //try{fm.all('MakeTime').value= arrResult[0][37];}catch(ex){};
    //try{fm.all('ModifyDate').value= arrResult[0][38];}catch(ex){};
    //try{fm.all('ModifyTime').value= arrResult[0][39];}catch(ex){};
//chenwm0116 隐藏掉来自查询被保险人返回的工作单位信息,因为有可能是其它保单的.
//    try{fm.all('GrpName').value= arrResult[0][41];}catch(ex){};
 //   try{fm.all('OrganComCode').value= arrResult[0][44];}catch(ex){};
//   try{fm.all('InsuredID').value= arrResult[0][45];}catch(ex){};
//   try{fm.all('RetireFlag').value= arrResult[0][42];}catch(ex){};
   //try{fm.all('DoctorEnsure').value= arrResult[0][70];}catch(ex){};
  // try{fm.all('AppointHospital').value= arrResult[0][71];}catch(ex){};
   try{fm.all('InsureDate').value= arrResult[0][72];}catch(ex){};
   try{fm.all('StopDate').value= arrResult[0][73];}catch(ex){};
   
     if(arrResult[0][70]==""){
		  fm.all('DoctorEnsure').value=2;
	  }else{
		  try {fm.all('DoctorEnsure').value= arrResult[0][70];} catch(ex){};
	  }
   	if(fm.all('DoctorEnsure').value=="1"){
 		  document.all("DivAppointHospital").style.display = "";		  
      try{fm.all('AppointHospital').value= arrResult[0][71];}catch(ex){};
    }else{
  	  document.all("DivAppointHospital").style.display = "none";
    }
    
   
   	if(arrResult[0][74]==""){
		  fm.all('EspecialTag').value=2;
	  }else{
		  try {fm.all('EspecialTag').value= arrResult[0][74];} catch(ex){};
	  }
   	if(fm.all('EspecialTag').value=="1"){
 		  document.all("DivEspecial").style.display = "";		  
      try{fm.all('TagReason').value= arrResult[0][75];}catch(ex){};
    }else{
  	  document.all("DivEspecial").style.display = "none";
    }
   try{fm.all('EnglishName').value= arrResult[0][76];}catch(ex){};
    //地址显示部分的变动
    
    var strSQL="select * from LCAddress where CustomerNo='"+fm.all('InsuredNo').value+"' and addressno='"+fm.all('AddressNo').value+"'";
	  arrResult=easyExecSql(strSQL);
    if(arrResult!=null){
  		DisplayAddress();
    }
/*
    try{fm.all('AddressNo').value= "";}catch(ex){};
    try{fm.all('PostalAddress').value=  "";}catch(ex){};
    try{fm.all('ZipCode').value=  "";}catch(ex){}; 
    try{fm.all('Phone').value=  "";}catch(ex){}; 
    try{fm.all('Mobile').value=  "";}catch(ex){}; 
    try{fm.all('EMail').value=  "";}catch(ex){}; 
    //try{fm.all('GrpName').value= arrResult[0][46];}catch(ex){}; 
    try{fm.all('GrpPhone').value=  "";}catch(ex){}; 
    try{fm.all('GrpAddress').value=  "";}catch(ex){}; 
    try{fm.all('GrpZipCode').value=  "";}catch(ex){}; */
}

function displayAppnt11() {
  try {
    fm.all('InsuredNo').value= arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value= arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value= arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value= arrResult[0][21];
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= arrResult[0][23];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= arrResult[0][25];
  } catch(ex) {}
  ;
  //alert( arrResult[0][25]);
  try {
    fm.all('OccupationCode').value= arrResult[0][26];
  } catch(ex) {}
  ;
  showAllCodeName(); 
}

function displayAppnt(arrResult)
{
    //modified by zhangjq on 2008-5-12 将大部分已经在DisplayInsured()函数中置值了元素置值语句的进行注释
    try{fm.all('InsuredNo').value= arrResult[0][0]; }catch(ex){};
    try{fm.all('Name').value= arrResult[0][1]; }catch(ex){};
    try{fm.all('Sex').value= arrResult[0][2]; }catch(ex){};
   try{fm.all('Birthday').value= arrResult[0][3]; }catch(ex){};
    try{fm.all('IDType').value= arrResult[0][4]; }catch(ex){};
    try{fm.all('IDNo').value= arrResult[0][5]; }catch(ex){};
    try{fm.all('ConfirmIDNo').value= arrResult[0][5]; }catch(ex){};
    try{fm.all('Password').value= arrResult[0][6]; }catch(ex){};
    //try{fm.all('NativePlace').value= arrResult[0][7]; }catch(ex){};
    //try{fm.all('Nationality').value= arrResult[0][8]; }catch(ex){};
    //try{fm.all('RgtAddress').value= arrResult[0][9]; }catch(ex){};
    //try{fm.all('Marriage').value= arrResult[0][10];}catch(ex){};
    //try{fm.all('MarriageDate').value= arrResult[0][11];}catch(ex){};
    //try{fm.all('Health').value= arrResult[0][12];}catch(ex){};
    //try{fm.all('Stature').value= arrResult[0][13];}catch(ex){};
    //try{fm.all('Avoirdupois').value= arrResult[0][14];}catch(ex){};
    //try{fm.all('Degree').value= arrResult[0][15];}catch(ex){};
    //try{fm.all('CreditGrade').value= arrResult[0][16];}catch(ex){};
    try{fm.all('OthIDType').value= arrResult[0][17];}catch(ex){};
    try{fm.all('OthIDNo').value= arrResult[0][18];}catch(ex){};
    try{fm.all('ICNo').value= arrResult[0][19];}catch(ex){};
    try{fm.all('GrpNo').value= arrResult[0][20];}catch(ex){};
//chenwm071126 隐藏掉来自ldperson中的JoinCompanyDate,Position,Salary因为新契约只存在lcinsured
//    try{fm.all('JoinCompanyDate').value= arrResult[0][21];}catch(ex){};
    //try{fm.all('StartWorkDate').value= arrResult[0][22];}catch(ex){};
//    try{fm.all('Position').value= arrResult[0][23];}catch(ex){};
//    try{fm.all('Salary').value= arrResult[0][24];}catch(ex){};
//    try{fm.all('OccupationType').value= arrResult[0][25];}catch(ex){};
    //try{fm.all('OccupationCode').value= arrResult[0][26];}catch(ex){};
    //try{fm.all('WorkType').value= arrResult[0][27];}catch(ex){};
    //try{fm.all('PluralityType').value= arrResult[0][28];}catch(ex){};
   // try{fm.all('DeathDate').value= arrResult[0][29];}catch(ex){};
    //try{fm.all('SmokeFlag').value= arrResult[0][30];}catch(ex){};
    //try{fm.all('BlacklistFlag').value= arrResult[0][31];}catch(ex){};
    //try{fm.all('Proterty').value= arrResult[0][32];}catch(ex){};
    //try{fm.all('Remark').value= arrResult[0][33];}catch(ex){};
    //try{fm.all('State').value= arrResult[0][34];}catch(ex){};
    //try{fm.all('Operator').value= arrResult[0][35];}catch(ex){};
    //try{fm.all('MakeDate').value= arrResult[0][36];}catch(ex){};
    //try{fm.all('MakeTime').value= arrResult[0][37];}catch(ex){};
    //try{fm.all('ModifyDate').value= arrResult[0][38];}catch(ex){};
    //try{fm.all('ModifyTime').value= arrResult[0][39];}catch(ex){};
//chenwm0116 隐藏掉来自查询被保险人返回的工作单位信息,因为有可能是其它保单的.
//    try{fm.all('GrpName').value= arrResult[0][41];}catch(ex){};
 //   try{fm.all('OrganComCode').value= arrResult[0][44];}catch(ex){};
//   try{fm.all('InsuredID').value= arrResult[0][45];}catch(ex){};
//   try{fm.all('RetireFlag').value= arrResult[0][42];}catch(ex){};
   //try{fm.all('DoctorEnsure').value= arrResult[0][70];}catch(ex){};
  // try{fm.all('AppointHospital').value= arrResult[0][71];}catch(ex){};
   //try{fm.all('InsureDate').value= arrResult[0][72];}catch(ex){};
  // try{fm.all('StopDate').value= arrResult[0][73];}catch(ex){};

    
   
 
  
   try{fm.all('EnglishName').value= arrResult[0][76];}catch(ex){};
    //地址显示部分的变动
    
    var strSQL="select * from LCAddress where CustomerNo='"+fm.all('InsuredNo').value+"' and addressno='"+fm.all('AddressNo').value+"'";
	  arrResult=easyExecSql(strSQL);
    if(arrResult!=null){
  		DisplayAddress();
    }
/*
    try{fm.all('AddressNo').value= "";}catch(ex){};
    try{fm.all('PostalAddress').value=  "";}catch(ex){};
    try{fm.all('ZipCode').value=  "";}catch(ex){}; 
    try{fm.all('Phone').value=  "";}catch(ex){}; 
    try{fm.all('Mobile').value=  "";}catch(ex){}; 
    try{fm.all('EMail').value=  "";}catch(ex){}; 
    //try{fm.all('GrpName').value= arrResult[0][46];}catch(ex){}; 
    try{fm.all('GrpPhone').value=  "";}catch(ex){}; 
    try{fm.all('GrpAddress').value=  "";}catch(ex){}; 
    try{fm.all('GrpZipCode').value=  "";}catch(ex){}; */
}
/*********************************************************************
 *  查询返回后触发
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */        
function afterQuery( arrQueryResult ) 
{
//	alert("here:" + arrQueryResult + "\n" + mOperate);
    if( arrQueryResult != null ) 
    {
        arrResult = arrQueryResult;
        
        if( mOperate == 1 )	
        {		// 查询投保单
            fm.all( 'ContNo' ).value = arrQueryResult[0][0];

            arrResult = easyExecSql("select ProposalContNo,PrtNo,ManageCom,SaleChnl,AgentCode,AgentGroup,AgentCode1,AgentCom,AgentType,Remark from LCCont where LCCont = '" + arrQueryResult[0][0] + "'", 1, 0);

            if (arrResult == null)
            {
                alert("未查到投保单信息");
            }
            else
            {
                displayLCContPol(arrResult[0]);
            }
        }
        if( mOperate == 2 )
        {		
        	// 投保人信息
        	//arrResult = easyExecSql("select a.*,b.AddressNo,b.PostalAddress,b.ZipCode,b.HomePhone,b.Mobile,b.EMail,a.GrpNo,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode from LDPerson a,LCAddress b where 1=1 and a.CustomerNo=b.CustomerNo and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
        	arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
        	if (arrResult == null)
        	{
        	    alert("未查到投保人信息");
        	}
        	else
        	{
               // alert(arrResult[0][26]);
        	    displayAppnt(arrResult);
        	}
        }
         if( mOperate == 3 )
        {		
        	// 工作单位
        	if (arrResult == null)
        	{
        	    alert("未查到投保人信息");
        	}
        	else
        	{
        		if(ComFlag=="ORG"){
	        		fm.all("OrganComCode").value = arrQueryResult[0][2];
			        fm.all("OrganInnerCode").value = arrQueryResult[0][3];
							fm.all("GrpName").value = arrQueryResult[0][6];
							if(arrQueryResult[0][27]!=0){
								alert("所选的工作单位不是末级工作单位!");
								fm.all("OrganComCode").focus();
				        		fm.all("OrganComCode").value = "";
				        		fm.all("OrganInnerCode").value = "";
								fm.all("GrpName").value = "";
								return;
							}
						 }
        	}
        }
//        showCodeName();
    }
	mOperate = 0;		// 恢复初态
}
/*********************************************************************
 *  查询职业类别
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getdetailwork()
{
    var strSql = "select OccupationType from LDOccupation where OccupationCode='" + fm.OccupationCode.value+"'";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
        fm.OccupationType.value = arrResult[0][0];
    }
    else
    {
        fm.OccupationType.value = '';
    }
}
/*获得个人单信息，写入页面控件
function getProposalInsuredInfo(){
  var ContNo = fm.ContNo.value;
  //被保人详细信息
  var strSQL ="select * from ldperson where CustomerNo in (select InsuredNo from LCInsured where ContNo='"+ContNo+"')";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null){
  	DisplayCustomer();
  }

  strSQL ="select * from LCInsured where ContNo = '"+ContNo+"'";
  arrResult=easyExecSql(strSQL);

  if(arrResult!=null){
  	   DisplayInsured();
  }else{


    return;
  }

  var tAddressNo = arrResult[0][10]; 		// 得到被保人地址号
  var InsuredNo=arrResult[0][2];
  var strSQL="select * from LCAddress where AddressNo='"+tAddressNo+"' and CustomerNo='"+InsuredNo+"'";
  arrResult=easyExecSql(strSQL);
    if(arrResult!=null){
  	DisplayAddress();
    }

    getInsuredPolInfo();

}*/


/*********************************************************************
 *  把合同所有信息录入结束确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function inputConfirm(wFlag)
{
	//alert("LoadFlag=="+LoadFlag);


    if (wFlag ==1 ) //录入完毕确认
    {
        var tStr= "	select * from lwmission where 1=1 and lwmission.missionprop1 = '"+fm.ContNo.value+"'";
        turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
        if (turnPage.strQueryResult)
        {
		    alert("该合同已经做过保存！");
		    return;
		}
		fm.AppntNo.value = AppntNo;
		fm.AppntName.value = AppntName;
		fm.WorkFlowFlag.value = "7999999999";
    }
    else if (wFlag ==2)//复核完毕确认
    {
  	    if(fm.all('ProposalContNo').value == "")
        {
            alert("未查询出合同信息,不容许您进行 [复核完毕] 确认！");
            return;
        }
		fm.WorkFlowFlag.value = "0000001001";
		fm.MissionID.value = tMissionID;
		fm.SubMissionID.value = tSubMissionID;
    }
    else if (wFlag ==3)
    {
        if(fm.all('ProposalContNo').value == "")
        {
            alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
            return;
        }
		fm.WorkFlowFlag.value = "0000001002";
		fm.MissionID.value = tMissionID;
		fm.SubMissionID.value = tSubMissionID;
	}
	else
		return;

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./InputConfirm.jsp";
    fm.submit(); //提交
}
/*********************************************************************
 *  查询被保险人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getdetailaddress()
{
    var strSQL="select b.AddressNo,b.PostalAddress,b.ZipCode,b.Phone,b.Mobile,b.EMail,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode,b.HomeAddress,b.HomeZipCode,b.HomePhone from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.InsuredNo.value+"'";
    arrResult=easyExecSql(strSQL);
    try{fm.all('AddressNo').value= arrResult[0][0];}catch(ex){};
    try{fm.all('PostalAddress').value= arrResult[0][1];}catch(ex){};
    try{fm.all('ZipCode').value= arrResult[0][2];}catch(ex){};
    try{fm.all('Phone').value= arrResult[0][3];}catch(ex){};
    try{fm.all('Mobile').value= arrResult[0][4];}catch(ex){};
    try{fm.all('EMail').value= arrResult[0][5];}catch(ex){};
    //try{fm.all('GrpName').value= arrResult[0][6];}catch(ex){};
    try{fm.all('GrpPhone').value= arrResult[0][6];}catch(ex){};
    try{fm.all('GrpAddress').value= arrResult[0][7];}catch(ex){};
    try{fm.all('GrpZipCode').value= arrResult[0][8];}catch(ex){};
    try{fm.all('HomeAddress').value= arrResult[0][9];}catch(ex){};
    try{fm.all('HomeZipCode').value= arrResult[0][10];}catch(ex){};
    try{fm.all('HomePhone').value= arrResult[0][11];}catch(ex){};

}

/*********************************************************************
 *  查询保险计划
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getContPlanCode(tProposalGrpContNo)
{
    var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select ContPlanCode,ContPlanName from LCContPlan where ContPlanCode!='00' and ProposalGrpContNo='"+tProposalGrpContNo+"'";
    //alert("strsql :" + strsql);
    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
    if (turnPage.strQueryResult != "")
    {
    	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	m = turnPage.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    	}
    	 divContPlan.style.display="";
    }
    else
    {
      //alert("保险计划没查到");
        divContPlan.style.display="none";
    }
    //alert ("tcodedata : " + tCodeData);
    return tCodeData;
}

/*********************************************************************
 *  查询处理机构
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getExecuteCom(tProposalGrpContNo)
{
    	//alert("1");
    var i = 0;
	var j = 0;
	var m = 0;
	var n = 0;
	var strsql = "";
	var tCodeData = "0|";
	//modify by winnie ASR20093243 统括保单添加服务机构给被保人
	var mOrganComCode=fm.all("OrganComCode").value;
	if(mOrganComCode!=null&&mOrganComCode!=''){
		strsql = "select distinct ExecuteCom,Name from LCGeneraltoRisk a,LDCom b where a.GrpContNo='"+tProposalGrpContNo+"' and a.ExecuteCom=b.ComCode and a.organcomcode='"+mOrganComCode+"'";
	}else{
		strsql = "select distinct ExecuteCom,Name from LCGeneraltoRisk a,LDCom b where a.GrpContNo='"+tProposalGrpContNo+"' and a.ExecuteCom=b.ComCode";
	}
	//strsql = "select ExecuteCom,Name from LCGeneral a,LDCom b where a.GrpContNo='"+tProposalGrpContNo+"' and a.ExecuteCom=b.ComCode";
	//end modify
	//alert("strsql :" + strsql);
	turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
	if (turnPage.strQueryResult != "")
	{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		m = turnPage.arrDataCacheSet.length;
		for (i = 0; i < m; i++)
		{
			j = i + 1;
//			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
			tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
		}
		divExecuteCom.style.display="";
	}
	else
	{
		 //modify by winnie ASR20093243  20090625
		//被保人对应分支机构，现在其分支机构不对应比例配置信息，查找其上级机构对应的分支机构
		var curCom=mOrganComCode;
		while(curCom!=null&&curCom!=''){
			var upComSql="select nvl(upcomcode,'') from lcorgan where grpcontno='"+tProposalGrpContNo+"' and organcomcode='"+curCom+"'";
			var arrResult =  easyExecSql(upComSql);
			var upCom="";
			if(arrResult!=null&&arrResult[0][0]!=null&&arrResult[0][0]!=''){
				upCom=arrResult[0][0];
			}
			strsql = "select distinct ExecuteCom,Name from LCGeneraltoRisk a,LDCom b where a.GrpContNo='"+tProposalGrpContNo+"' and a.ExecuteCom=b.ComCode and a.organcomcode='"+curCom+"'";
			turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
			if (turnPage.strQueryResult != "")
			{
				turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
				m = turnPage.arrDataCacheSet.length;
				for (i = 0; i < m; i++)
				{
					j = i + 1;
					tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
				}
				divExecuteCom.style.display="";
				break;
			}else{
				curCom=upCom;
			}
		}
		// divExecuteCom.style.display="none";
		//end modify
    }
	//alert ("tcodedata : " + tCodeData);

	return tCodeData;
}

function emptyInsured()
{

	try{fm.all('InsuredNo').value= ""; }catch(ex){};
	try{fm.all('ExecuteCom').value= ""; }catch(ex){};
	try{fm.all('FamilyID').value= ""; }catch(ex){};
	try{fm.all('RelationToMainInsured').value= ""; }catch(ex){};
    try{fm.all('RelationToMainInsuredName').value= ""; }catch(ex){};
	try{fm.all('RelationToAppnt').value= ""; }catch(ex){};
	try{fm.all('AddressNo').value= ""; }catch(ex){};
	//try{fm.all('SequenceNo').value= ""; }catch(ex){};
	try{fm.all('Name').value= ""; }catch(ex){};
	try{fm.all('Sex').value= ""; }catch(ex){};
    try{fm.all('SexName').value= ""; }catch(ex){};
	try{fm.all('Birthday').value= ""; }catch(ex){};
	try{
		if(fm.all('PolTypeFlag').value == "2"){
			fm.all('IDType').value= "4";
		}else{
			fm.all('IDType').value= "0";
		}
	}catch(ex){};
    try{fm.all('IDTypeName').value= ""; }catch(ex){};
	try{fm.all('IDNo').value= ""; }catch(ex){};
	try{fm.all('MainName').value= ""; }catch(ex){};	
	try{fm.all('ConfirmIDNo').value= ""; }catch(ex){};
	try{fm.all('NativePlace').value= ""; }catch(ex){};
	try{fm.all('Nationality').value= ""; }catch(ex){};
    try{fm.all('NativePlaceName').value= ""; }catch(ex){};
	try{fm.all('RgtAddress').value= ""; }catch(ex){};
	try{fm.all('Marriage').value= ""; }catch(ex){};
    try{fm.all('MarriageName').value= ""; }catch(ex){};
	try{fm.all('MarriageDate').value= ""; }catch(ex){};
	try{fm.all('Health').value= ""; }catch(ex){};
	try{fm.all('Stature').value= ""; }catch(ex){};
	try{fm.all('Avoirdupois').value= ""; }catch(ex){};
	try{fm.all('Degree').value= ""; }catch(ex){};
	try{fm.all('CreditGrade').value= ""; }catch(ex){};
	try{fm.all('BankCode').value= ""; }catch(ex){};
	try{fm.all('BankAccNo').value= ""; }catch(ex){};
	try{fm.all('DoctorEnsure').value= ""; }catch(ex){};
	try{fm.all('DoctorEnsureName').value= ""; }catch(ex){};
	try{fm.all('EspecialTag').value= ""; }catch(ex){};
	try{fm.all('EspecialTagName').value= ""; }catch(ex){};
	try{fm.all('OrganComCode').value= ""; }catch(ex){};
    try{fm.all('GrpName').value= ""; }catch(ex){};
	try{fm.all('AccName').value= ""; }catch(ex){};
	try{fm.all('JoinCompanyDate').value= ""; }catch(ex){};
	try{fm.all('StartWorkDate').value= ""; }catch(ex){};
	try{fm.all('Position').value= ""; }catch(ex){};
	try{fm.all('Salary').value= ""; }catch(ex){};
	try{fm.all('OccupationType').value= ""; }catch(ex){};
    try{fm.all('OccupationTypeName').value= ""; }catch(ex){};
	try{fm.all('OccupationCode').value= ""; }catch(ex){};
    try{fm.all('OccupationCodeName').value= ""; }catch(ex){};
	try{fm.all('WorkType').value= ""; }catch(ex){};
	try{fm.all('PluralityType').value= ""; }catch(ex){};
	try{fm.all('SmokeFlag').value= ""; }catch(ex){};
	try{fm.all('ContPlanCode').value= ""; }catch(ex){};
	try{fm.all('ContPlanCodeName').value= ""; }catch(ex){};	
    try{fm.all('HomeAddress').value= ""; }catch(ex){};
    try{fm.all('HomeZipCode').value= ""; }catch(ex){};
    try{fm.all('HomePhone').value= ""; }catch(ex){};
    try{fm.all('HomeFax').value= ""; }catch(ex){};
    try{fm.all('GrpFax').value= ""; }catch(ex){};
    try{fm.all('Fax').value= ""; }catch(ex){};
    try{fm.all('NoticeWay').value= ""; }catch(ex){};
    try{fm.all('NoticeWayName').value= ""; }catch(ex){};
    try{fm.all('NativePlace').value= ""; }catch(ex){};
    try{fm.all('NativePlaceName').value= ""; }catch(ex){};
	emptyAddress();
	ImpartGrid.clearData();
	ImpartGrid.addOne();
	ImpartDetailGrid.clearData();
	ImpartDetailGrid.addOne();
	showCodeName();
}

/*********************************************************************
 *  清空客户地址数据
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function emptyAddress()
{
	try{fm.all('PostalAddress').value= "";  }catch(ex){};
	try{fm.all('ZipCode').value= "";  }catch(ex){};
	try{fm.all('Phone').value= "";  }catch(ex){};
	try{fm.all('Mobile').value= "";  }catch(ex){};
	try{fm.all('EMail').value= "";  }catch(ex){};
	//try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){};
	try{fm.all('GrpPhone').value= "";  }catch(ex){};
	try{fm.all('GrpAddress').value= ""; }catch(ex){};
	try{fm.all('GrpZipCode').value= "";  }catch(ex){};
}
/*********************************************************************
 *  根据身份证号取得出生日期和性别
 *  参数  ：  身份证号
 *  返回值：  无
 *********************************************************************
 */

function getBirthdaySexByIDNo(iIdNo)
{
	if(fm.all('IDType').value=="0")
	{ 
		if(checkIdCard(iIdNo)== true){
			fm.all('Birthday').value=getBirthdatByIdNo(iIdNo);
			fm.all('Sex').value=getSexByIDNo(iIdNo);
		}
	}
}
/*********************************************************************
 *  合同信息录入完毕确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpInputConfirm(wFlag)
{
	mWFlag = 1;
	if (wFlag ==1 ) //录入完毕确认
	{
	    var tStr= "	select * from lwmission where 1=1 "
	    					+" and lwmission.processid = '0000000004'"
	    					+" and lwmission.activityid = '0000002001'"
	    					+" and lwmission.missionprop1 = '"+fm.ProposalGrpContNo.value+"'";
	    turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
	    if (turnPage.strQueryResult)
	    {
	        alert("该团单合同已经做过保存！");
	        return;
	    }
		if(fm.all('ProposalGrpContNo').value == "")
	    {
	        alert("团单合同信息未保存,不容许您进行 [录入完毕] 确认！");
	        return;
	    }
		fm.WorkFlowFlag.value = "6999999999";			//录入完毕
    }
    else if (wFlag ==2)//复核完毕确认
    {
        if(fm.all('ProposalGrpContNo').value == "")
	    {
	        alert("未查询出团单合同信息,不容许您进行 [复核完毕] 确认！");
	        return;
	    }
		fm.WorkFlowFlag.value = "0000002002";					//复核完毕
		fm.MissionID.value = MissionID;
		fm.SubMissionID.value = SubMissionID;
	}
	else if (wFlag ==3)
    {
  	    if(fm.all('ProposalGrpContNo').value == "")
	    {
	        alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
	        return;
	    }
		fm.WorkFlowFlag.value = "0000001002";					//复核修改完毕
		fm.MissionID.value = MissionID;
		fm.SubMissionID.value = SubMissionID;
	}
	else if(wFlag == 4)
	{
		if(fm.all('ProposalGrpContNo').value == "")
	    {
	       alert("未查询出合同信息,不容许您进行 [修改完毕] 确认！");
	       return;
	    }
	    fm.WorkFlowFlag.value = "0000001021";					//问题修改
	    fm.MissionID.value = MissionID;
	    fm.SubMissionID.value = SubMissionID;
	}
	else
		return;

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GrpInputConfirm.jsp";
    fm.submit(); //提交
}
function getaddresscodedata()
{
    var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select AddressNo,PostalAddress from LCAddress where CustomerNo ='"+fm.InsuredNo.value+"'";
    //alert("strsql :" + strsql);
    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
    if (turnPage.strQueryResult != "")
    {
    	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	m = turnPage.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    	}
    }
    //alert ("tcodedata : " + tCodeData);
    //return tCodeData;
    fm.all("AddressNo").CodeData=tCodeData;
}

function getImpartCode(parm1,parm2){
  //alert("hehe:"+fm.all(parm1).all('ImpartGrid1').value);
  var impartVer=fm.all(parm1).all('ImpartGrid1').value;
  window.open("./ImpartCodeSel.jsp?ImpartVer="+impartVer);
}
function checkidtype()
{
	if(fm.IDType.value=="")
	{
		alert("请先选择证件类型！");
		fm.IDNo.value="";
		fm.ConfirmIDNo.value="";

        }
}
function getallinfo()
{
 	if(fm.Name.value!=""&&fm.IDType.value!=""&&fm.IDNo.value!="")
 	{
	    strSQL = "select a.CustomerNo, a.Name, a.Sex, a.Birthday, a.IDType, a.IDNo from LDPerson a where 1=1 "
                +"  and Name='"+fm.Name.value
                +"' and IDType='"+fm.IDType.value
                +"' and IDNo='"+fm.IDNo.value
		+"' order by a.CustomerNo";
             turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
             if (turnPage.strQueryResult != "")
             {
 		  mOperate = 2;
 		  window.open("../sys/LDPersonQueryAll.html?Name="+fm.Name.value+"&IDType="+fm.IDType.value+"&IDNo="+fm.IDNo.value,"newwindow","height=10,width=1090,top=180,left=180, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no,status=no");
 	     }
 	     else
 	     return;
 	}
}
function DelRiskInfo()
{
	if(fm.InsuredNo.value=="")
	{
		alert("请先选择被保人");
		return false;
	}
	var tSel =PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert("该客户没有险种或者您忘记选择了？");
		return false;
	}
	var tRow = PolGrid.getSelNo() - 1;
	var tpolno=PolGrid.getRowColData(tRow,1)
	fm.all('fmAction').value="DELETE||INSUREDRISK";
	fm.action="./DelIsuredRisk.jsp?polno="+tpolno;
	fm.submit(); //提交

}
function InsuredChk()
{
	var tSel =InsuredGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert("请先选择被保险人！");
		return false;
	}
	var tRow = InsuredGrid.getSelNo() - 1;
	var tInsuredNo=InsuredGrid.getRowColData(tRow,1);
	var tInsuredName=InsuredGrid.getRowColData(tRow,2);
	var tInsuredSex=InsuredGrid.getRowColData(tRow,3);
	var tBirthday=InsuredGrid.getRowColData(tRow,4);
        var sqlstr="select *from ldperson where Name='"+tInsuredName+"' and Sex='"+tInsuredSex+"' and Birthday='"+tBirthday+"' and CustomerNo<>'"+tInsuredNo+"'";
        arrResult = easyExecSql(sqlstr,1,0);
        if(arrResult==null)
        {
	   alert("该没有与该被保人保人相似的客户,无需校验");
	   return false;
        }

	window.open("../uw/InsuredChkMain.jsp?ProposalNo1="+fm.ContNo.value+"&InsuredNo="+tInsuredNo+"&Flag=I","window1");
}

function AutoMoveForNext()
{
	if(fm.AutoMovePerson.value=="定制第二被保险人")
	{
		     //emptyFormElements();
		     param="122";
		     fm.pagename.value="122";
                     fm.AutoMovePerson.value="定制第三被保险人";
                     return false;
	}
	if(fm.AutoMovePerson.value=="定制第三被保险人")
	{
		     //emptyFormElements();
		     param="123";
		     fm.pagename.value="123";
                     fm.AutoMovePerson.value="定制第一被保险人";
                     return false;
	}
		if(fm.AutoMovePerson.value=="定制第一被保险人")
	{
		     //emptyFormElements();
		     param="121";
		     fm.pagename.value="121";
                     fm.AutoMovePerson.value="定制第二被保险人";
                     return false;
	}
}
function noneedhome()
{
    var insuredno="";
    if(InsuredGrid.mulLineCount>=1)
    {
        for(var personcount=0;personcount<=InsuredGrid.mulLineCount;personcount++)
        {
        	if(InsuredGrid.getRowColData(personcount,5)=="00")
        	{
        		insuredno=InsuredGrid.getRowColData(personcount,1);
        		break;
        	}
        }
       var strhomea="select HomeAddress,HomeZipCode,HomePhone from lcaddress where customerno='"+insuredno+"' and addressno=(select addressno from lcinsured where contno='"+fm.ContNo.value+"' and insuredno='"+insuredno+"')";
       arrResult=easyExecSql(strhomea,1,0);
       try{fm.all('HomeAddress').value= arrResult[0][0];}catch(ex){};
       try{fm.all('HomeZipCode').value= arrResult[0][1];}catch(ex){};
       try{fm.all('HomePhone').value= arrResult[0][2];}catch(ex){};
    }
}
function getdetail()
{
var strSql = "select BankCode,AccName from LCAccount where BankAccNo='" + fm.BankAccNo.value+"'";
arrResult = easyExecSql(strSql);
if (arrResult != null) {
      fm.BankCode.value = arrResult[0][0];
      fm.AccName.value = arrResult[0][1];
    }
}
function checkifnoname()
{
    var strSql = "select ContPlanCode from LCInsured where ContNo='" + oldContNo+"' and Name='无名单'";
    arrResult = easyExecSql(strSql);
    if(fm.ContPlanCode.value!=""&&fm.ContPlanCode.value!=arrResult[0][0])
    {
    	alert("所选计划与原无名单所选不一致！");
    	return false;
    }
    else
    {
    	return true;
    }
}
function checkenough()
{
    var strSql1 = " select * from lcpol where appflag='4' and MasterPolNo in (select  proposalNo from lcpol where Contno='" + oldContNo+"')";
    var arrResult1 = easyExecSql(strSql1);
    var strSql2 = " select * from lccont where appflag<>'1' and GrpContNo = (select  GrpContNo from lcCont where Contno='" + oldContNo+"')";
    var arrResult2 = easyExecSql(strSql2);
    if(arrResult1!=null||arrResult2!=null)
    {
    	alert("尚有补名单未签单,请先签单再补其他名单!");
    	return false;
    }
    else
    {
    	return true;
    }
}

var ComFlag = null;
function getORGComCode(){
	ComFlag = "ORG";
		window.open("ORGQuery.jsp?PrtNo="+fm.all("PrtNo").value);
	  return true;
}

function getSendORGComCode(){
	
}

function querySendORGComCode()
{
}

function displayLDPerson(){
}

function chenckInfo(){
	if(fm.all('Name').value == ""){
		alert("姓名不能为空！");
		fm.all('Name').focus ;
		return false;
	}
	
	
		

	//连带被保险人
	if(fm.all('MainInsuredNo').value != "" && fm.all('MainInsuredNo').value != fm.all("InsuredNo").value){
		 if(fm.all('RelationToMainInsured').value == "00"){
		 		alert("当为附属被保险人的时候,与主被保险人的关系不能为本人!");
		 		fm.all('RelationToMainInsured').focus;
		 		return false;
		 }else if (fm.all('RelationToMainInsured').value == ""){
		 	  alert("与主被保险人的关系不能为空!");
		 	  fm.all('RelationToMainInsured').focus;
		 	  return false;	
		 }
		//校验主附被保险人的机构代码是否一致
		var SQL = "select organcomcode from lcinsured where insuredno = '" + fm.all('MainInsuredNo').value + "' and prtno = '" + fm.all("PrtNo").value + "'";
	 	arrResult = easyExecSql(SQL);
	 	if(arrResult != null){
	 		if(fm.all('OrganComCode').value != arrResult[0][0]){
	 				alert("与主被保险人的机构"+arrResult[0][0]+"不一致!");
	 				fm.all('OrganComCode').focus ;
	 				return false;
	 		}
	 	}
	 	//校验主附被保险人的理赔通知方式是否一致  guoxh 
	 	var SQLNW = "select noticeway from lccont where insuredno = '" + fm.all('MainInsuredNo').value + "' and prtno = '" + fm.all("PrtNo").value + "'";
	 	var arrResultNW = easyExecSql(SQLNW);
	 	if(arrResultNW != null){
	 		if(fm.all('NoticeWay').value != arrResultNW[0][0]){
	 				alert("与主被保险人的理赔通知方式"+arrResultNW[0][0]+"不一致!");
	 				fm.all('NoticeWay').focus ;
	 				return false;
	 		}
	 	}
	 	
	}else{//主被保险人
		if(fm.all('RelationToMainInsured').value != "00"){
		 		alert("当为主保险人的时候,与主被保险人的关系只能为00!");
		 		fm.all('RelationToMainInsured').focus;
		 		return false;
		 }
	}
	
	if(fm.all('PolTypeFlag').value!="2"){
	
		if(fm.all('IDType').value == ""){
			alert("证件类型不能为空！");
			fm.all('IDType').focus;
			return false;
		}
		if(fm.all('IDNo').value == ""){
			alert("证件号码不能为空！");
			fm.all('IDNo').focus;
			return false;
		}
		if(fm.all('Sex').value == ""){
			alert("性别不能为空！");
			fm.all('Sex').focus;
			return false;
		}
		if(fm.all('Birthday').value == ""){
			alert("出生日期不能为空！");
			fm.all('Birthday').focus;
			return false;
		}
		if(fm.all('OccupationType').value == ""){
			alert("职业类别不能为空！");
			fm.all('OccupationType').focus;
			return false;
		}
		
	 	if(fm.all('ZipCode').value.length !=6 && fm.all('ZipCode').value !=""){
	 		alert("邮编长度应该为6!");
	 		fm.all('ZipCode').focus;
	 		return false;
	 	}
	 	if(fm.all('Phone').value >18){
	 		alert("联系电话书写格式不正确");
	 		fm.all('Phone').focus;
	 		return false;
	 	}
	 	 if(fm.all('Mobile').value.length >15){
	 		alert("移动电话长度应该小于15");
	 		fm.all('Mobile').focus;
	 		return false;
	 	}
	 	
	}
	return true ;
}

// 验证被保人的联系地址是否改动，如果改动则被保人的AddressNo加1 
function checkInsuredNo(){
			if(fm.all('InsuredNo').value != ""){
			var sql = "select * from LcAddress where CustomerNo='"+fm.all('InsuredNo').value+"'and AddressNo='"+fm.all('AddressNo').value+"' ";
			arrResult = easyExecSql(sql);
			if (arrResult != null){ 
				if(fm.all('PostalAddress').value != arrResult[0][2]|| fm.all('ZipCode').value != arrResult[0][3]
				   || fm.all('Phone').value != arrResult[0][4] || fm.all('Mobile').value != arrResult[0][14]
				   || fm.all('EMail').value != arrResult[0][16] ){ 
					arrResult = easyExecSql("select max(to_number(addressno)) from lcaddress where customerno='"+fm.all('InsuredNo').value+"'");
					var a1 =  parseInt(arrResult[0][0])+ 1;
					fm.all('AddressNo').value = a1.toString(); 
				}
			}
		}
}

//查询主被保险人
function getMainInsuredID(){
		window.open("QueryMainInsuredID.jsp?GrpContNo="+fm.all("GrpContNo").value);
}

//校验是否是附属被保险人,如果是的话,不允许在做主被保险人
function afterQueryInsured(tInsuredNo){
	var SQL ="select * from LCinsured where InsuredNo='"+tInsuredNo+"' and MainInsuredNo=InsuredNo";
	arrResult = easyExecSql(SQL);

	if (arrResult == null)
	{
	    alert("附属被保险人不允许做主被保险人!");
	    return ;
	}else{
		fm.all('MainInsuredNo').value = tInsuredNo ;
	}
}


//计划定制
function showPInvest()
{
	if (PolGrid.getSelNo()==0)
	{
		alert("请先选择一条记录！");
		return;
	}

	var tContNo = PolGrid.getRowColData(PolGrid.getSelNo()-1, 1);
  showInfo = window.open("../acc/PInvestPlanMain.jsp?ContNo=" + tContNo + "&LoadFlag="+LoadFlag);	
	
}

//选择团体计划定制
function showGInvest()
{
	if (PolGrid.getSelNo()==0)
	{
		alert("请先选择一条记录！");
		return;
	}

	var tPolNo = PolGrid.getRowColData(PolGrid.getSelNo()-1, 1);
	var tGrpContNo = fm.GrpContNo.value;
  showInfo = window.open("../acc/GrpInvestUseMain.jsp?PolNo=" + tPolNo + "&LoadFlag="+LoadFlag+"&GrpContNo="+tGrpContNo);	
	
}



String.prototype.trim=function(){   
        return this.replace(/(^\s*)|(\s*$)/g, "");    
    }   
