//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var mCodeName;
var tPremBegin;
var tPremEnd;
var tAgeBegin;
var tAgeEnd;
var turnPage = new turnPageClass();
//提交，保存按钮对应操作
function submitForm()
{
	if(!verifyInput2())
	return false;
	if(fm.QYType_ch.value==""){
	alert("类型不能为空！");
	return false;
  }
  tPremBegin=fm.PremBegin.value;
  tPremEnd=fm.PremEnd.value;
  if(tPremBegin!=""&&tPremEnd!=""){
		if(!isNumeric(tPremBegin)||!isNumeric(tPremEnd)){
		alert("保费范围不是有效数字！");
		return false;
  	}
  	    if(tPremBegin-tPremEnd>0){
	alert("开始保费不应大于终止保费！");
	return false;  
  }
	}else if(tPremBegin!=""&&tPremEnd==""){
	alert("终止保费不能为空！");
		return false;	
	}else if(tPremBegin==""&&tPremEnd!=""){
	alert("开始保费不能为空！");
		return false;	
	}
  tAgeBegin=fm.AgeBegin.value;
//  alert(tAgeBegin);
  tAgeEnd=fm.AgeEnd.value;
//  alert(tAgeEnd);
  if(tAgeBegin!=""&&tAgeEnd!=""){
  	if(!isNumeric(tAgeBegin)||!isNumeric(tAgeEnd)){
	alert("年龄范围不是有效数字！");
	return false;
  		}
  if(tAgeBegin-tAgeEnd>0){
	alert("开始年龄不应大于终止年龄！");
	return false;  
  }
  }else if(tAgeBegin!=""&&tAgeEnd==""){
	alert("终止年龄不能为空！");
		return false;	
	}else if(tAgeBegin==""&&tAgeEnd!=""){
	alert("开始年龄不能为空！");
		return false;	
	} 
  if(fm.all('SignDateBegin').value!=""&&fm.all('SignDateEnd').value!=""){	
    if(dateDiff( fm.all('SignDateBegin').value,fm.all('SignDateEnd').value,"D")< 0) {
    alert("承保起始日期不应在承保终止日期之前!");
    fm.all('CInValiDate').focus();
    return false;
  	}
	}else if(fm.all('SignDateBegin').value!=""&&fm.all('SignDateEnd').value==""){
		alert("承保结束日期不能为空！");
		return false;		
	}else if(fm.all('SignDateBegin').value==""&&fm.all('SignDateEnd').value!=""){
		alert("承保开始日期不能为空！");
		return false;		
	}   
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function getRisk(Obj)
{
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('I','A','C','D')"
           + " order by RiskCode";
  ;
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "")
  {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++)
    {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
  }
  Obj.CodeData=tCodeData;
}