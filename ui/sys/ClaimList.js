//该文件中包含客户端需要处理的函数和事件
//程序名称：ClaimList.js
//程序功能：
//创建日期：2007-3-18 21:54
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();


//----------------------全局变量区------------------------------

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();


//----------------------初始化程序区-----------------------------

//查询保全清单
function queryClaimList()
{
  var contNoFieldType = "";
  if(tAppObj == "G")
  {
    contNoFieldType = "GrpContNo";
  }
  else
  {
    contNoFieldType = "ContNo";
  }
  
  if(tContNo == "" || tContNo == null || tContNo == "null"){
  	alert("请核实前台传入的保单号/团单号，此处不许为空，请返回上级页面重新操作！");
  	return false;
  }else{
  	fm.ContNo.value=tContNo;
  }
  
  var sql = "select a.CaseNo, a.RgtDate, a.rigister, "
          + "   a.CustomerName, "
          + "   (select min(GiveTypeDesc) from LLClaim where CaseNo = a.CaseNo), "
          + "   sum(b.RealPay), a.EndCaseDate, "
          + "   (select Name from LDCom where ComCode = a.MngCom), "
          + "   CodeName('llrgtstate', a.RgtState) "
          + "from LLCase a, LLClaimPolicy b "
          + "where a.CaseNo = b.CaseNo "
          + "   and a.CustomerNo = b.InsuredNo "
          //+ " and grpcontno='00004328000001' "
          + "   and b." + contNoFieldType + " = '" + tContNo + "' "
          + getWherePart("a.CustomerNo", "InsuredNo")
          + "group by a.CaseNo, a.RgtDate, a.CustomerName, a.EndCaseDate, a.MngCom, a.RgtState ,a.rigister "
          + "order by a.CaseNo ";
  
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(sql, ClaimListGrid);
  
  fm.all("divPage2").style.display = "";
  
  fm.ClaimListSql.value = sql;
}

function ScanQuery() {
	var getCol =  ClaimListGrid.getSelNo()-1;
	if(getCol>=0){
        var checkCaseno = ClaimListGrid.getRowColData(ClaimListGrid.getSelNo()-1,1);
	    var pathStr="../case/ClaimUnderwriteEasyScan.jsp?RgtNo="+checkCaseno+"&SubType=LP1001&BussType=LP&BussNoType=21";
	    var newWindow=OpenWindowNew(pathStr,"查看扫描件","left");	
	  }else{
		  alert("请选中案件进行查询。");
		  return false;
	  }
	}

//------------------打印------------------
function printContList(contType)
{
  if(contType == "Vali" && ClaimListGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }else if(tContNo == "" || tContNo == null || tContNo == "null"){
  	alert("请核实前台传入的保单号/团单号，此处不许为空，请返回上级页面重新操作！");
  	return false;
  }
  
  fm.action = "printClaimListSave.jsp?ContType=" + contType;
  fm.target = "_blank";
  fm.submit();
}


//------------------事件响应区----------------
//function printList()
//{
//  if(ClaimListGrid.mulLineCount == 0)
//  {
//    alert("没有需要打印的数据");
//    return false;
//  }
//  
//  fm.action = "PrintClaimListSave.jsp";
//	fm.target = "_blank";
//	fm.submit();
//  
//  return true;
//}

//察看工单
//function viewClaimInfo()
//{
//  var row = EdorListGrid.getSelNo();
//  
//  if(row == null || row == 0)
//  {
//    alert("请选择一条记录");
//    return false;
//  }
//  
//  var WorkNo = EdorListGrid.getRowColDataByName(row - 1, "EdorNo");
//  var CustomerNo = EdorListGrid.getRowColDataByName(row - 1, "CustomerNo");
//  
//	var width = screen.availWidth - 10;
//  var height = screen.availHeight - 28;
//	win = window.open("../task/TaskViewMain.jsp?WorkNo="+WorkNo +"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + CustomerNo, 
//					  "view", "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
//	win.focus();
//}