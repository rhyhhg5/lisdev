<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称:PEfficiencySave.jsp
//程序功能：打印效率清单程序的向后台传递参数的程序
//创建日期：2003-04-04
//创建人  ：刘岩松
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.f1print.*"%>
<%
  System.out.println("开始执行打印操作");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";

  String strStartDate = request.getParameter("StartDate");
  String strEndDate = request.getParameter("EndDate");
  String StrOperateFlag = request.getParameter("OperateFlag");
  System.out.println("您录入的开始日期是===="+strStartDate);
  System.out.println("您录入的结束日期是===="+strEndDate);
  System.out.println("您的操作标志是===="+StrOperateFlag);

  PEfficiencyUI tPEfficiencyUI = new PEfficiencyUI();
  VData tVData = new VData();
  VData mResult = new VData();
  try
  {
    tVData.addElement(strStartDate);
    tVData.addElement(strEndDate);
    tVData.addElement(StrOperateFlag);
    tPEfficiencyUI.submitData(tVData,"PRINT");
  }
  catch(Exception ex)
  {
    Content = "PRINT"+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  mResult = tPEfficiencyUI.getResult();
  XmlExport txmlExport = new XmlExport();
  txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
  if (txmlExport==null)
  {
    System.out.println("null没有正确的打印数据！！！");
    tError = tPEfficiencyUI.mErrors;
    Content = "打印失败,原因是＝＝"+tError.getFirstError();
    FlagStr = "Fail";
  }
  else
  {
    session.putValue("PrintStream", txmlExport.getInputStream());
  	System.out.println("put session value");
  	response.sendRedirect("../f1print/GetF1Print.jsp");
  }
  %>
<html>
  <script language="javascript">
 	  alert("<%=Content%>");
    top.opener.focus();
    top.close();
	</script>
</html>