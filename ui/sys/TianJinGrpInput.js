//程序名称：TianJinGrpInput.js
//程序功能：天津社保补充业务数据采集统计分析
//创建日期：2017-12-5 
//创建人  ：YangJian

var turnPage = new turnPageClass();
var showInfo = null;

//-------------------------------------事件响应区----------------------------------
function TianJinGrpQuery()
{
	var urlStr="select FileName,MakeDate,FilePath from LCFileManage where FileType='9' and FileDetailType='9' ";
	turnPage.queryModal(urlStr,TianJinGrpGrid);	
	
}



function TianJinGrpDownload	(){	
	var i;
	var selFlag = true;
	
	if(!selFlag) return selFlag;
		
	var rowNum = TianJinGrpGrid.mulLineCount;
	
	//校验是否选定要下载的选项
	if(!TianJinGrpGrid.getSelNo()){
		alert("未选定要下载的数据");
		return false;
	}

	fm.action = "TianJinGrpSave.jsp";
	fm.submit();
}

function afterSubmit(FlagStr, content )
{
	
	if(FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}
