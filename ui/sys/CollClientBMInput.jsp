<html> 
<%
//程序名称：CollClientBMInput.jsp
//程序功能：
//创建日期：2002-08-07 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="CollClientBMInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CollClientBMInit.jsp"%>
</head>
<body  onload="initForm();">
  <form action="./CollClientBMSave.jsp" method=post name=fm target="fraSubmit">

    <!-- 一些操作按钮 -->
	<table>
      <tr>
        <td width="80%">&nbsp;&nbsp;</td>
        <td><input type="button" value="&nbsp;批&nbsp;&nbsp;改&nbsp;" onclick="updateClick( )"></td>
        <td><input type="button" value="&nbsp;查&nbsp;&nbsp;询&nbsp;" onclick="queryClick( )"></td></tr>
    </table>

    <!-- 显示或隐藏CollectiveClient的信息 -->
    <table>
      <tr>
        <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCollectiveClient);"></td>
        <td class= titleImg>集体客户信息</td></tr>
    </table>

    <div id= "divCollectiveClient" style= "display: ''">
      <table class="common">
        <tr class="common">
          <td class="title">单位号码</td>
          <td class="input"><input class="common" name=GrpNo></td>
          
          <td class="title">单位名称</td>
          <td class="input"><input class="common" name=GrpName></td></tr>
          
        <tr class="common">
          <td class="title">单位地址编码</td>
          <td class="input"><input class="common" name=GrpAddressCode></td>
          
          <td class="title">单位地址</td>
          <td class="input"><input class="common" name=GrpAddress></td></tr>
          
        <tr class="common">
          <td class="title">单位邮编</td>
          <td class="input"><input class="common" name=GrpZipCode></td>
          
          <td class="title">单位电话</td>
          <td class="input"><input class="common" name=Phone></td></tr>
          
        <tr class="common">
          <td class="title">单位传真</td>
          <td class="input"><input class="common" name=Fax></td>
          
          <td class="title">负责人</td>
          <td class="input"><input class="common" name=Satrap></td></tr>
          
        <tr class="common">
          <td class="title">法人</td>
          <td class="input"><input class="common" name=Corporation></td>
          
          <td class="title">e_mail</td>
          <td class="input"><input class="common" name=EMail></td></tr>
          
        <tr class="common">
          <td class="title">联系人</td>
          <td class="input"><input class="common" name=LinkMan></td>
          
          <td class="title">总人数</td>
          <td class="input"><input class="common" name=Peoples></td></tr>
          
        <tr class="common">
          <td class="title">银行编码</td>
          <td class="input"><input class="common" name=BankCode></td>
          
          <td class="title">银行账号</td>
          <td class="input"><input class="common" name=BankAccNo></td></tr>
          
        <tr class="common">
          <td class="title">行业分类</td>
          <td class="input"><Input class="codeNo" name=BusinessType ondblclick="return showCodeList('BusinessType',[this,BusinessName],[0,1]);"><input class="codename" name="BusinessName" ></td>
          
          <td class="title">单位性质</td>
          <td class="input"><Input class="codeNo" name=GrpNature ondblclick="return showCodeList('GrpNature',[this,GrpNatureName],[0,1]);"><input class="codename" name="GrpNatureName" ></td></tr>
          
        <tr class="common">
          <td class="title">成立日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="FoundDate"></td>
          
          <td class="title">客户组号码</td>
          <td class="input"><input class="common" name=GrpGroupNo></td></tr>
          
        <tr class="common">
          <td class="title">状态</td>
          <td class="input"><input class="common" name=State></td>
          
          <td class="title">备注</td>
          <td class="input"><input class="common" name=Remark></td></tr>
          
        <tr class="common">
          <td class="title">黑名单标记</td>
          <td class="input"><input class="common" name=BlacklistFlag></td>
          
          <td class="title">操作员代码</td>
          <td class="input"><input class="common" name=Operator></td></tr>

          <input type=hidden name=Transact>
      </table>
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
