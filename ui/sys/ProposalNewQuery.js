//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}


function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	// 书写SQL语句
  if( trim(fm.PrtNo.value) == ''&& trim(fm.AppntName.value)=='' && trim(fm.InsuredName.value)=='' && trim(fm.PolApplyDate.value) == '' && trim(fm.CValiDate.value) == ''&& trim(fm.AgentCode.value)=='')
  {
  	alert("请输入查询条件");
  	return false;
  }

	var strSql = "select l.ContNo,l.PrtNo,l.PolApplyDate,l.CValiDate,l.AppntName,l.AgentCode,l.ManageCom from LCCont l where 1=1 "
    				+" and conttype='1'"
    				+ " and l.manageCom like '"+manageCom+"%%'"
    				+ " and (l.AppFlag='0' or l.AppFlag='9')"
    				+ getWherePart( 'l.PrtNo','PrtNo' )	
    				+ getWherePart( 'l.AppntName' ,'AppntName' )
    				+ getWherePart( 'l.InsuredName','InsuredName' )
    				+ getWherePart( 'l.AgentCode','AgentCode' )
    				+ getWherePart( 'l.PolApplyDate','PolApplyDate' ) 
    				+ getWherePart( 'l.CValiDate','CValiDate' )
    				;   
	
	turnPage.strQueryResult  = easyQueryVer3(strSql);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未查询到符合条件的信息，请重新输入查询条件！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //tArr = chooseArray(arrDataSet,[0,1,3,4]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

}

var mSwitch = parent.VD.gVSwitch;


function ChkState()
{
	fm.action =  "../sys/PolStatusChk.jsp";
	fm.submit(); //提交
}
function afterSubmit(FlagStr,Content)
{
  var i = 0;
  var tstatue="";
  for (i=0; i<PolStatuGrid.mulLineCount; i++) {
  	var j=i+1;
    if (PolStatuGrid.getRowColData(i,1)!=null||PolStatuGrid.getRowColData(i,1)!=""||PolStatuGrid.getRowColData(i,1)!="null") { 
     tstatue+=j+"、"+PolStatuGrid.getRowColData(i,1)+" ";
    }
  }
  fm.ProState.value=tstatue;
}


function queryStateClick() {
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	var cProposalContNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	var cprtno = PolGrid.getRowColData(checkFlag - 1, 2); 	
//  	mSwitch.deleteVar( "fm.value('PrtNo')" );
  	
//  	mSwitch.addVar( "fm.value('PrtNo')", "", cPrtNo);
  	
    urlStr = "./ProposalQueryStateMain.jsp?ContNo="+cProposalContNo+"&prtNo="+cprtno ,"status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";   
  
    window.open(urlStr);
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}