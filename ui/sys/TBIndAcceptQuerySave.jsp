<%
//程序名称：TBIndAcceptQuerySave.jsp
//程序功能：F1报表生成
//创建日期：2006-6-15 11:33
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
新承保统计
</title>
<head>
</head>
<body>
<%
	String flag = "0";
	String FlagStr = "";
	String Content = "";
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	String FileName = "TBIndAcceptQuery";
	String ManageCom = request.getParameter("ManageCom");
	System.out.println(ManageCom);
	String StartDate = request.getParameter("StartDate");
	System.out.println(StartDate);
	String EndDate = request.getParameter("EndDate");
	System.out.println(EndDate);
	
	String SQL = "";
	String SaleChnl = request.getParameter("SaleChnl");
	if (!"".equals(SaleChnl)) {
		SQL = SQL + " and d.SaleChnl = '" + SaleChnl + "' ";
	}
	if ("".equals(SQL)) {
		SQL = "and 1=1";
	}
	String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
	String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");
	String OperatorManagecom=tG.ManageCom;
	System.out.println(OperatorManagecom);
	if(sd.compareTo(ed) > 0){
		flag = "1";
		FlagStr = "Fail";
		Content = "操作失败，原因是:统计止期比统计统计起期早";
	}
	JRptList t_Rpt = new JRptList();
	String tOutFileName = "";
	if(flag.equals("0")){
		t_Rpt.m_NeedProcess = false;
		t_Rpt.m_Need_Preview = false;
		t_Rpt.mNeedExcel = true;
		StartDate = AgentPubFun.formatDate(StartDate, "yyyy-MM-dd");
		EndDate = AgentPubFun.formatDate(EndDate, "yyyy-MM-dd");
		String CurrentDate = PubFun.getCurrentDate();
		CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
		t_Rpt.AddVar("StartDate", StartDate);
		t_Rpt.AddVar("EndDate", EndDate);
		t_Rpt.AddVar("ManageCom", ManageCom);
		t_Rpt.AddVar("SQL", SQL);
		t_Rpt.Prt_RptList(pageContext,FileName);
		tOutFileName = t_Rpt.mOutWebReportURL;
		String strVFFileName = 
		FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
		String strRealPath = 
		application.getRealPath("/web/Generated").replace('\\','/');
		String strVFPathName = strRealPath +"/"+ strVFFileName; 
		System.out.println("strVFPathName : "+ strVFPathName); 
		System.out.println("=======Finshed in JSP==========="); 
		System.out.println(tOutFileName);
		response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+
		"&RealPath="+strVFPathName);
	}
	%>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script>