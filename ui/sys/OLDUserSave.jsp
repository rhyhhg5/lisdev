<%
//程序名称：OLDUserInput.jsp
//程序功能：
//创建日期：2002-08-16 17:44:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LDUserSchema tLDUserSchema   = new LDUserSchema();

  OLDUserUI tOLDUser   = new OLDUserUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLDUserSchema.setUserCode(request.getParameter("UserCode"));
    tLDUserSchema.setUserName(request.getParameter("UserName"));
    tLDUserSchema.setComCode(request.getParameter("ComCode"));
    tLDUserSchema.setPassword(request.getParameter("Password"));
    tLDUserSchema.setUserDescription(request.getParameter("UserDescription"));
    tLDUserSchema.setUserState(request.getParameter("UserState"));
    tLDUserSchema.setUWPopedom(request.getParameter("UWPopedom"));
    tLDUserSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLDUserSchema.setOtherPopedom(request.getParameter("OtherPopedom"));
    tLDUserSchema.setPopUWFlag(request.getParameter("PopUWFlag"));
    tLDUserSchema.setSuperPopedomFlag(request.getParameter("SuperPopedomFlag"));
    tLDUserSchema.setOperator(request.getParameter("Operator"));
    tLDUserSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDUserSchema.setMakeTime(request.getParameter("MakeTime"));
    tLDUserSchema.setValidStartDate(request.getParameter("ValidStartDate"));
    tLDUserSchema.setValidEndDate(request.getParameter("ValidEndDate"));


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLDUserSchema);
	tVData.add(tG);
  try
  {
    tOLDUser.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tOLDUser.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

