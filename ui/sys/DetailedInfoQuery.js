var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var tInsuredNo="";

/*********************************************************************
 *  查询个单客户险种信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setRiskPolValue() {

  initRiskPolGrid();
  var arrReturn1 = new Array();
  if(tContType=="1") {
    var ContType=easyExecSql("select ContType from lccont where contno='"+tContNo+"' and appflag='1'");
  } else if(tContType=="2") {
    var ContType=easyExecSql("select ContType from lbcont where contno='"+tContNo+"' and appflag='1'");
  }
  try {
    var strSQL="";
    if(tContType=="1" && ContType=="1") {
      strSQL = "select a.RiskCode,(select RiskName from LMRisk  where RiskCode=a.RiskCode),a.InsuredName,a.InsuredNo,a.polno from lcpol a where a.contno='"+tContNo+"' and a.appflag='1'"
   						+" union all "
   						+"select a.RiskCode,(select RiskName from LMRisk  where RiskCode=a.RiskCode),a.InsuredName,a.InsuredNo,a.polno from lbpol a where a.contno='"+tContNo+"' and a.appflag='1'";
    } else if(tContType=="2"&& ContType=="1") {
      var strSQL = "select a.RiskCode,(select RiskName from LMRisk  where RiskCode=a.RiskCode),a.InsuredName,a.InsuredNo,a.polno from lbpol a where a.contno='"+tContNo+"' and a.appflag='1'";
          
    }
    if (strSQL == "") {
      return false;
    } else {
      turnPage.queryModal(strSQL,RiskPolGrid);
    }
  } catch(ex) {
    alert("在DetailedInfoQueryInit.jsp-->setRiskPolValue函数中发生异常:初始化界面错误!");
  }

}
function DetailedRiskpol() {
	var tSel = RiskPolGrid.getSelNo();
  var cPolNo = RiskPolGrid.getRowColData( tSel - 1, 5 );
  var arrReturn1 = new Array();
  try {
    var strSQl="";
    strSQL ="select a.Amnt,a.StandPrem,a.Mult,c.paycount,a.InsuredName,a.InsuredAppAge,"
            +" case a.InsuredSex when '0' then '男' when '1' then '女' end,b.Occupationcode,a.InsuYear,a.InsuredNo from lcpol a,lcinsured b,ljapayperson c "
            +"where a.polno='"+cPolNo+"' and a.insuredno=b.insuredno and a.contno=b.contno and a.appflag='1' and a.polno=c.polno"
            +" union all "
            +"select a.Amnt,a.StandPrem,a.Mult,c.paycount,a.InsuredName,a.InsuredAppAge,"
            +"case a.InsuredSex when '0' then '男' when '1' then '女' end,b.Occupationcode,a.InsuYear,a.InsuredNo from lbpol a,lbinsured b,ljapayperson c "
            +"where a.polno='"+cPolNo+"' and a.insuredno=b.insuredno and a.contno=b.contno and a.appflag='1' and a.polno=c.polno";

    if(strSQL != "") {
      arrReturn1 =easyExecSql(strSQL);
    }
    if (arrReturn1 == ""||arrReturn1 == null) {
      return false;
    } else {
      display(arrReturn1[0]);
    }
  } catch(ex) {
    alert("在DetailedInfoQueryInit.jsp-->DetailedRiskpol函数中发生异常:初始化界面错误!");
  }
}

/*********************************************************************
 *  设置保单详细信息
 *  参数  ：  cArr 数组
 *  返回值：  无
 *********************************************************************
 */
function display(cArr) {
	var tSel = RiskPolGrid.getSelNo();
	var cPolNo = RiskPolGrid.getRowColData( tSel - 1, 5 );
	var strSql = "select polstate from lcpol where appflag='1' and polno='"+cPolNo+"' union all select polstate from lbpol where polno='"+cPolNo+"'";
  var tRiskstate=easyExecSql(strSql);
  var aa=tRiskstate[0][0].substring(0,4);                  
  try {
    fm.all('Riskstate').value =easyExecSql("select codename from ldcode where code='"+aa+"' and codetype='polstatereason'");                                                                                                                                                                           ;
  } catch(ex) { }                                                                                                                                                                                                                                                                              
  try {
    fm.all('Amnt').value = cArr[0];
  } catch(ex) { }
  try {
    fm.all('StandPrem').value = cArr[1];
  } catch(ex) { }
  try {
    fm.all('Mult').value = cArr[2];
  } catch(ex) { }
  try {
    fm.all('PayCount').value = cArr[3];
  } catch(ex) { }
  try {
    fm.all('InsuredName').value = cArr[4];
  } catch(ex) { }
  try {
    fm.all('InsuredAppAge').value = cArr[5];
  } catch(ex) { }
  try {
    fm.all('InsuredSex').value = cArr[6];
  } catch(ex) { }
  try {
    fm.all('Occupationcode').value = cArr[7];
  } catch(ex) { }
  fm.all('OccupationName').value = easyExecSql("select trim(OccupationName) from LDOccupation where OccupationCode ='"+cArr[7]+"'");
  try {
    fm.all('InsuYear').value = cArr[8];
  } catch(ex) {}
   try {
    tInsuredNo = cArr[9];
  } catch(ex) {}	
}
/*********************************************************************
 *  应缴保费信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function PayQueryClick()  
 {  
		window.open("../sys/PayInfo.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);
 }
/*********************************************************************
 *  保费历史资料查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function PayhistoryQueryClick()  
 {  
		window.open("../sys/PayHistoryInfo.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);
 }
 
/*********************************************************************
 *  业务员资料查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function AggentQueryClick()  
 {  
		window.open("../sys/PhoAgent.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);
 }
 
 /*********************************************************************
 *  保全历史资料查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function EndorhistoryQueryClick()  
 {  
		window.open("../sys/bqHistoryQuery.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);
 }
 

 /*********************************************************************
 *  保单理赔历史查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function ClaimhistoryQueryClick()  
 {  
		window.open("../sys/LLClaimQuery.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);
 } 
 
 /*********************************************************************
 *  保单保单杂项信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function MiscellaneouspolQueryClick()  
 {  
		window.open("../sys/PhoOtherQuery.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);
 } 

  /*********************************************************************
 *  保单受益人资料查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function BnfQueryClick()  
 {  
		window.open("../sys/PhoProposalQuery.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);
 } 
 
 
 
 

