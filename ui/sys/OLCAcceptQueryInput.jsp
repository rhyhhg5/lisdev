<%
//程序名称：OLCAcceptQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 12:55:37
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./OLCAcceptQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./OLCAcceptQueryInit.jsp"%>
  <title>接单表 </title>
</head>
<body  onload="initForm();" >
  <form action="./OLCAcceptQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <table  class= common>
  <TR  class= common>
    <TD  class= title>
      印刷号码
    </TD>
    <TD  class= input>
      <Input class= common name=PrtNo >
    </TD>
    <TD  class= title>
      管理机构
    </TD>
    <TD  class= input>
      <Input class='code' name=ManageCom ondblclick="return showCodeList('com',[this]);" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      险种编码
    </TD>
    <TD  class= input>
      <Input class='code' name=RiskCode ondblclick="return showCodeList('riskcode',[this]);" >
    </TD>
    <TD  class= title>
      险种版本
    </TD>
    <TD  class= input>
      <Input class= common name=RiskVersion >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      团单标志
    </TD>
    <TD  class= input>
      <Input class= common name=GrpFlag >
    </TD>
    <TD  class= title>
      体件标志
    </TD>
    <TD  class= input>
      <Input class= common name=MEflag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      交费金额
    </TD>
    <TD  class= input>
      <Input class= common name=PayMoney >
    </TD>
    <TD  class= title>
      投保人名称
    </TD>
    <TD  class= input>
      <Input class= common name=AppntName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      被保人数目
    </TD>
    <TD  class= input>
      <Input class= common name=InsuredPeoples >
    </TD>
    <TD  class= title>
      代理人编码
    </TD>
    <TD  class= input>
      <Input class='code' name=AgentCode ondblclick="return showCodeList('agentcode',[this]);" >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      附件份数
    </TD>
    <TD  class= input>
      <Input class= common name=AffixNum >
    </TD>
    <TD  class= title>
      附件页数
    </TD>
    <TD  class= input>
      <Input class= common name=AffixPageNum >
    </TD>
  </TR>
</table>

    </table>
          <INPUT VALUE="查询" TYPE=button onclick="submitForm();return false;"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAcceptGrid);">
    		</td>
    		<td class= titleImg>
    			 接单表结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAcceptGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAcceptGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button > 
      <INPUT VALUE="上一页" TYPE=button > 					
      <INPUT VALUE="下一页" TYPE=button > 
      <INPUT VALUE="尾页" TYPE=button > 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
