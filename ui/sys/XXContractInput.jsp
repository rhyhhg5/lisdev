<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<%@include file="./XXContractInit.jsp"%>
	<SCRIPT src="./XXContractInput.js"></SCRIPT>
</head>
<body  onload="initForm();initElementtype();" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="#" method="post" name="fm" target="fraSubmit">
  
    <table>
	   	  <tr>
	        <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCase);"></td>
	    	<td class= titleImg>请输入查询条件:</td>
	      </tr>
    </table>
    
	<div  id= "divCase" style= "display: ''">
	<table  class= common>
	  <tr  class= common>  
		    <td  class="title">
		      	 险种<!--对应险种-->
		    </td>
		    <td  class= input>
		      	<input class="common" name="RiskCode">       
		    </td>
		    <td class="title">
		    	保单号
		    </td>
		    <td  class="input">
		      	<input class="common" name="ContNo">       
		    </td>
    </tr>
    <tr class="common">
	    <td  class="title">
		 	起始日期
		</td>
		<td  class= input>
		     <input class= "coolDatePicker" dateFormat="short" name="ValidateDate" verify="生效日期|NOTNULL" elementtype="nacessary">
		</td>

		<td  class="title">
			 终止日期
		</td>
		<td  class= input>
		     <input class= "coolDatePicker" dateFormat="short" name="InValidateDate" verify="终止日期|NOTNULL" elementtype="nacessary">
		</td>
    </tr>
    </table>
    </div>
    
    <center>
		<table  class= "common">
		    <tr class= "common" align="left"> 
			    <td class=button>
	        	 <INPUT class=cssButton VALUE="新 单 数 据 下 载 " TYPE=button onClick="DownLoadnew()">	
	        	</td>  
	        	</tr>  
	        	<tr class= "common" align="left"> 
	        	<td class=button>
	        	 <INPUT class=cssButton VALUE="理 赔  数 据 下 载" TYPE=button onClick="DownLoadclm()">	
	        	</td>  
		    </tr>    
	    </table>
    </center>
    
	 <br><hr>
    <!--  table>
 	  <tr>
 	  	<td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTable);"></td>
  		<td class= titleImg>新单数据查询结果如下所示:</td>
  	  </tr>
    </table>
    
    <div  id= "divTable" style= "display: ''">
	 	<span id="spanDataGrid"></span>
	 	<center>
	 	<input value="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
	    <input value="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
	    <input value="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
	    <input value="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
	    </center>
    </div>
    

    <br><hr>
	   <table>
		  <tr>
		  	<td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClaim);"></td>
			<td class= titleImg>理赔数据查询结果如下所示:</td>
		  </tr>
	    </table>
  
	  <div  id= "divClaim" style= "display: ''">
		 	<span id="spanDataTable"></span>
		 	<center>
		 	<input value="首  页" TYPE=button onclick="turnPage1.firstPage();" class="cssButton">
		    <input value="上一页" TYPE=button onclick="turnPage1.previousPage();" class="cssButton">
		    <input value="下一页" TYPE=button onclick="turnPage1.nextPage();" class="cssButton">
		    <input value="尾  页" TYPE=button onclick="turnPage1.lastPage();" class="cssButton">
		    </center>
	  </div-->
   	  <input type=hidden class=Common name=querySqlnew >
  	  <input type=hidden class=Common name=querySqlclm >
  <Div id=DivFileDownload style="display:'none'">
      <A id=fileUrl href=""></A>
    </Div>
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>