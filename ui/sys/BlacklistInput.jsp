<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-1-7
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//            陈海强    2004-12-31
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="BlacklistInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="BlacklistInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./BlacklistSave.jsp" method=post name=fm target="fraSubmit">
    <!--%@include file="../common/jsp/OperateButton.jsp"%-->
    <!--%@include file="../common/jsp/InputButton.jsp"%-->
    <table>
      <tr>
      <td>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBlacklist1);">
      </td>
      <td class= titleImg>
        特殊客户信息
      </td>
    	</tr>
    </table>
    <Div  id= "divBlacklist1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            黑名单客户号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly  name=BlacklistNo >
          </TD>
		  <TD  class= title>
          	<Input class=cssbutton type=button value="查  询" onclick="ComQuery()">
		  </TD>
		  <td class=input></td>
          <TD  class= title>
            黑名单客户类型
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly  name=BlacklistType >
          </TD>
        <TR class=common>
          <TD  class= title>
            黑名单名称
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly  name=BlackName >
          </TD>
          <TD  class= title>
            黑名单日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=BlacklistMakeDate >
          </TD>
          <TD  class= title>
            黑名单操作员
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=BlacklistOperator >
          </TD>
        </TR>
      </table>
      <table class= common>
        <TR class= common>
			<TD class= title>黑名单原因描述</TD>
		</TR>					
		<TR  class= common>
		  	<TD  class= input>
	    		<textarea name=BlacklistReason cols="85%" rows="3"  class="common"></textarea>
		    </TD>
  		</TR>
      </table>
		<Input class=cssbutton type=button value="增加黑名单"  onclick="submitForm()" disabled=true name="addBlack">
		<Input class=cssbutton type=button value="黑名单查询"  onclick="EspQuery()">
		<Input class=cssbutton type=button value="修改黑名单"  onclick="UpdateForm()"  disabled=true name="uptBlack">
    </Div>
    <input type=hidden name=hideOperate value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
