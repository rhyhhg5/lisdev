<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//修改：刘岩松
//修改日期：2004-2-17
%>
<%
	String tContNo = "";
	String tIsCancelPolFlag = "";
	String tContType = "";
	try
	{
		tContNo = request.getParameter("ContNo");
		tIsCancelPolFlag = request.getParameter("IsCancelPolFlag");
		tContType = request.getParameter("ContType");
		if(tContType == null)
		tContType = "1";//用于测试；
		System.out.println("tContNodfdf="+tContNo);
	}
	catch( Exception e )
	{
		tContNo = "";
		tIsCancelPolFlag = "0";
	}
%>
<head >
<script>
	var tContNo = "<%=tContNo%>"; 
	var tIsCancelPolFlag = "<%=tIsCancelPolFlag%>";
	var tContType = "<%=tContType%>";	
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="BaseInfoQuery.js"></SCRIPT>
	<!--<SCRIPT src="AllProposalQuery.js"></SCRIPT>-->
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="BaseInfoQueryInit.jsp"%>
	
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>保单查询 </title>
</head>

<body  onload="initForm();" >
  <form  name=fm >   
    <table>
    	<tr>
        <td class=common>
			
    		</td>
    		<td class= titleImg>
    			保单基本信息
    		</td>
    	</tr>
    </table>
	<table class="common" id="table2">
		<tr CLASS="common">
			<td CLASS="title">保单号</td>    		       
				<td CLASS="input" COLSPAN="1">
					<input NAME="prtNo" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" READONLY="true">
				</td>	
			<td CLASS="title">投保类型</td>    		         
				<td CLASS="input" COLSPAN="1">
					<input NAME="conttype" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20"READONLY="true">
				</td>		
			<td CLASS="title">期缴保费合计</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="SumPrem" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
				</td>
</tr>
<tr CLASS="common">
			<td CLASS="title">
				收费方式 </td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="PayIntv" type=hidden VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
    	<input NAME="PayIntvName" VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
    	</td>	
		<TD CLASS=title  >
	      缴费方式 
	   </TD>
	   <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PayMode type=hidden VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
	   		<Input NAME=PayModeName  VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
	   </TD>	

			<TD  class= title>
      投保日期
			</TD>
    <TD  class= input>
      <Input name=PolApplyDate VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>
</tr>
<tr CLASS="common">
		<TD  class= title>
      承保日期
			</TD>
    <TD  class= input>
      <Input name=SignDate VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>	
    <TD  class= title>
        生效日期
    </TD>
    <TD  class= input>
       <Input  name=CValiDate VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD> 
  <TD  class= title>
        缴至日期
    </TD>
    <TD  class= input>
       <Input  name=PaytoDate VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>
</tr>
<tr CLASS="common">             			
<td CLASS="title">代理人名称</td>
		<td CLASS="input" COLSPAN="1">
			<input NAME="AgentName" VALUE CLASS="common" READONLY="true"TABINDEX="-1" MAXLENGTH="20" >
		</td>
<td CLASS="title">代理人编码</td>    		           
		<td CLASS="input" COLSPAN="1">
			<input NAME="AgentCode" VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
		</td>
<TD  class= title>
		投保人名称
	  </TD>
	  <TD  class= input>
		<Input class= common name=AppntName elementtype=nacessary verify="投保人姓名|notnull&len<=20" readonly=true>
	  </TD>
</tr>
<tr CLASS="common">
<TD  class= title>
        被保人名称
    </TD>
    <TD  class= input>
       <Input  name=InsuredName VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>		  
<TD  class= title>
        与被保人关系
    </TD>
    <TD  class= input>
       <Input  name=RelationToMainInsured type=hidden VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
       <Input  name=RelationToMainInsuredName VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>
    <td CLASS="title">保单状态 </td>    		       
			<td CLASS="input" COLSPAN="1">
			<input name=ContState VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
 </tr>
 	<Div  id= "divPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
   </table>
    	<table>
         <INPUT class=CssButton VALUE="详细信息" TYPE=button onclick="DetailedInfoQueryClick();"> 
		 <INPUT class=CssButton VALUE="客户资料" TYPE=button onclick="CustomInfoQueryClick();"> 
		 <INPUT class=CssButton VALUE="保单试算" TYPE=button onclick="NumerationQueryClick();">
	     <INPUT class=CssButton VALUE="取 消" TYPE=button onclick="top.close()"> 
  		</table>		
  	</Div>			
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script>

</script>
</html>


	