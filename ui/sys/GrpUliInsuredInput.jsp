<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = "";
	String tModeFlag = "";
	String tPlanCode = "";
	try
	{
		tContNo = request.getParameter("ContNo" );
		tModeFlag = request.getParameter("ModeFlag" );
		tPlanCode = request.getParameter("PlanCode" );
		
		//默认情况下为集体保单
		if( tContNo == null || tContNo.equals( "" ))
			tContNo = "00000000000000000000";
	}
	catch( Exception e1 )
	{
		System.out.println("---contno:"+tContNo);

	}
	System.out.println("---contno:"+tContNo);
	
%>

<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   
<script>
	var contNo = "<%=tContNo%>";  //个人单的查询条件.
	var modeFlag = "<%=tModeFlag%>";
	var planCode = "<%=tPlanCode%>";
	var tContType = "<%=request.getParameter("ContType" )%>"
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpUliInsuredInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="GrpUliInsuredInit.jsp"%>
  <title>被保人清单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
  			<td class= titleImg align= center>请输入查询被保人条件：</td>
  			<td><INPUT VALUE="查  询" class = cssbutton TYPE=button onclick="easyQueryClick();"></td> 
  		</tr>
  	</table>
  	<br>
      <table  class= common align=center>
        <TR  class= common>
    		  <TD  class= title> 被保人姓名 </TD>
              <TD  class= input> <Input class="common" name=Name> </TD> 
    		  <TD  class= title> 客户号 </TD>  
    		  <TD  class= input> <Input class="common" name=InsuredNo > </TD>
          <TD  class= title> 证件号码 </TD>
          <TD  class= input> <Input class="common" name=IDNo> </TD>
          <TD  class= title> 被保人状态 </TD>
          <TD  class= input><Input class="codeNo" name="InsuredStat" value="0" readonly CodeData="0|^0|有效^1|无效^2|全部" verify="客户状态|&code:InsuredStat"  ondblclick="return showCodeListEx('InsuredStat',[this,StatName],[0,1]);" onkeyup="return showCodeListEx('InsuredStat',[this,StatName],[0,1]);"><Input class="codeName" name="StatName"  value="有效" readonly></TD> 
        </TR>
        <TR  class= common>
    		  <TD  class= title> 保障计划 </TD>
          <TD  class= input colspan=7><Input class="codeNo" name="ContPlanCode" readonly CodeData=""  ondblclick="return showCodeListEx('grpcontplanrisk',[this,ContPlanName],[0,1], null, null, null, 1);"><Input class="codeName" name="ContPlanName" readonly></TD> 
        </TR>
      </table>
  	<br>
	
	<!--按保障计划或状态查询的被保人信息-->
  	<Div  id= "divLCInsure" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpInsuredSumGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
  	  <Div id= "divPage5" align="center" style= "display: '' ">
        <INPUT VALUE="首  页" class = cssbutton  TYPE=button onclick="turnPage5.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton  TYPE=button onclick="turnPage5.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssbutton  TYPE=button onclick="turnPage5.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton  TYPE=button onclick="turnPage5.lastPage();">				
  	  </div>
  	</div>
	
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsure);">
    		</td>
    		<td class= titleImg>
    			 被保人信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCInsure" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpInseGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div id= "divPage4" align="center" style= "display: '' ">
        <INPUT VALUE="首  页" class = cssbutton  TYPE=button onclick="turnPage4.firstPage();setSumActuPayMoney();"> 
        <INPUT VALUE="上一页" class = cssbutton  TYPE=button onclick="turnPage4.previousPage();setSumActuPayMoney();"> 					
        <INPUT VALUE="下一页" class = cssbutton  TYPE=button onclick="turnPage4.nextPage();setSumActuPayMoney();"> 
        <INPUT VALUE="尾  页" class = cssbutton  TYPE=button onclick="turnPage4.lastPage();setSumActuPayMoney();">				
        <INPUT VALUE="打印全部清单" onclick="printList();" id="PrintListID" class = cssbutton TYPE=button>
    	</div>
    </div>
  	
  	<!--无名单状态列表-->
  	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divNoNameCont);">
    		</td>
    		<td class= titleImg>
    			 无名单状态列表(选择无名单察看增减人操作记录)
    		</td>
    	</tr>
    </table>
  	<Div  id= "divNoNameCont" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanNoNameCont" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div id= "divPage2" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();	showCodeName();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage(); showCodeName();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage(); showCodeName();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage(); showCodeName();"> 
    	</Div>
  	</div>
  	
  	
  	<!--无名单操作轨迹: NNCOperateTrace:NoNameContOperateTrace-->
  	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divNNCOperateTrace);">
    		</td>
    		<td class= titleImg>
    			 无名单增减人操作记录
    		</td>
    	</tr>
    </table>
  	<Div  id= "divNNCOperateTrace" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanNNCOperateTrace" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div id= "divPage3" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();	showCodeName();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage(); showCodeName();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage(); showCodeName();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage(); showCodeName();"> 
    	</Div>
  	</div>
    <input name= "InsuredSql" type=hidden>
    <input name="GrpContNo" value="<%=tContNo%>" type=hidden>
    
    <br>
    <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="returnParent();"> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
