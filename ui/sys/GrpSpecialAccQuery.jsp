<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String cContNo = "";
	String cContLoadFlag = "";
	String cContType = "";
	try
	{
		cContNo = request.getParameter("ContNo");
		cContLoadFlag = request.getParameter("ContLoadFlag");
		cContType = request.getParameter("ContType");
	}
	catch( Exception e )
	{
		cContNo = "";
	}	
%>
<head >
<script> 
	var tContNo = "<%=cContNo%>"; 
	var tContType =  "<%=cContType%>";
	var tGrpPolNo =  "<%=request.getParameter("GrpPolNo")%>";
	var tRiskCode =  "<%=request.getParameter("RiskCode")%>";
	var tPlanCode =  "<%=request.getParameter("PlanCode")%>";
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="GrpSpecialAccQuery.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="GrpSpecialAccQueryInit.jsp"%>
	
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>特需事项查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="f1print"> 
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCGrpCont);">
    		</td>
    		<td class= titleImg>
    			 投保账户事项：
    		</td>
    	</tr>
    </table>
   <div id="DivLCGrpCont" STYLE="display:''">
	 <table class="common" id="table1">
		<tr CLASS="common">
			<td CLASS="title">管理费收取方式 </td>    		       
			<td CLASS="input" COLSPAN="1">
			  <input NAME="ManageFeeType" VALUE CLASS="common1" TABINDEX="-1"MAXLENGTH="20"READONLY="true">
      </td>  
			<td CLASS="title">投保账户保费</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="GrpAccPrem" VALUE MAXLENGTH="10" CLASS="common1" TABINDEX="-1" READONLY="true">
    	</td>		
			<td CLASS="title"></td>
			<td CLASS="input" COLSPAN="1"></td>		 
		</tr>
		<tr CLASS="common" style="display:none">
			<td CLASS="title">团体帐户管理费金额</td>    		           
			<td CLASS="input" COLSPAN="1">
			<input NAME="ManageFee" VALUE MAXLENGTH="10" CLASS="common1" READONLY="true" TABINDEX="-1">
    		         </td>
			<td CLASS="title">团体医疗帐户金额</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="GrpPubAcc" VALUE CLASS="common1" READONLY="true"TABINDEX="-1" MAXLENGTH="20" >
    	</td>
			<td ></td>    		         
			<td ></td>           
		</tr>
	  </table>
   </div> 
   
   <!--账户管理费金额-->
   <table>
	  <tr>
			<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivGrpAccFee);">
			</td>
			<td class= titleImg>
				 账户管理费金额：
			</td>
	  </tr>
    </table>
    <div id="DivGrpAccFee" STYLE="display:''">
	   <table class=common>	   
        <tr CLASS="common">
					<td CLASS="title">公共账户管理费 </td>
					<td CLASS="input" COLSPAN="1">
					  <input NAME="GrpAccFee" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
					</td>
					<td CLASS="title">固定账户管理费 </td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="GrpFixAccFee" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
					</td>
		      <td CLASS="title">个人账户管理费</td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="InsuredAccFee" VALUE MAXLENGTH="10" CLASS="common1" READONLY="true"TABINDEX="-1"></td>
					<td CLASS="title">管理费合计</td>
					<td CLASS="input" COLSPAN="1">
					<input NAME="SumAccFee" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
		    		</td>
        </tr>
      </table>
    </div>  
   
    <table>
	    <tr>
  			<td class=common>
  				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivGrpAcc);">
  			</td>
  			<td class= titleImg>
  				 团体公共账户信息：
  			</td>
	    </tr>
    </table>
    <div id="DivGrpAcc" STYLE="display:''">
	   <table class=common>	   
        <tr CLASS="common">
					<td CLASS="title">账户成立时间 </td>
					<td CLASS="input" COLSPAN="1">
					  <input NAME="GrpAccFoundDate" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
					</td>
					<td CLASS="title">账户余额</td>
					<td CLASS="input" COLSPAN="1">
					  <input NAME="GrpInsuAccBala" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
		    	</td>
		      <td CLASS="title">最后修改日期 </td>
					<td CLASS="input" COLSPAN="1">
					  <input NAME="GrpAccModifyDate" VALUE MAXLENGTH="10" CLASS="common1" READONLY="true"TABINDEX="-1">
					</td>
		      <td CLASS="title"></td>
					<td CLASS="input" COLSPAN="1"></td>
        </tr>
      </table>
    </div> 
   
    
	  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivPerPayAcc);">
    		</td>
    		<td class= titleImg>
    			 个人账户信息
    		</td>
    	</tr>
    </table>
	  <div id="DivPerPayAcc" STYLE="display:''">
      <table class="common" id="table4">
  		  <tr CLASS="common">
    			<td CLASS="title">有效被保人总数</td>
    			<td CLASS="input" COLSPAN="1">
    			  <input NAME="InsuerCount" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
    			</td>
    			<td CLASS="title">账户总额 </td>
    			<td CLASS="input" COLSPAN="1">
    			  <input NAME="AccInsuAccBala" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
    			</td>
    			<td CLASS="title">最后修改日期 </td>
    			<td CLASS="input" COLSPAN="1">
    			  <input NAME="AccInsuModifyDate" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
    			</td>
		      <td CLASS="title"></td>
					<td CLASS="input" COLSPAN="1"></td>
        </tr>
		  </table>
	  </div>
	  
	  <table>
    	 <tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivGrpFixAcc);">
    		</td>
    		<td class= titleImg>
    			 固定账户信息：</td>
    	 </tr>
    </table> 
	  <div id="DivGrpFixAcc" STYLE="display:''">
  	  <table class=common>	   
        <tr CLASS="common">
					<td CLASS="title">账户成立时间 </td>
					<td CLASS="input" COLSPAN="1">
					  <input NAME="FixAccFoundDate" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
					</td>
    			<td CLASS="title">账户余额</td>
    			<td CLASS="input" COLSPAN="1">
    			  <input NAME="FixAccBala" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
        	</td>
    			<td CLASS="title">最后修改日期 </td>
    			<td CLASS="input" COLSPAN="1">
    			  <input NAME="FixAccModifyDate" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
    			</td>
    			<td CLASS="title"> </td>
    			<td CLASS="input" COLSPAN="1"></td>
        </tr>
      </table>
   </div>  
   
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivGrpHealthAcc);">
    		</td>
    		  <td class= titleImg>健康管理帐户信息: </td>
    	</tr>
    </table> 
	  <div id="DivGrpFixAcc" STYLE="display:''">
  	  <table class=common>	   
        <tr CLASS="common">
					<td CLASS="title">账户成立时间 </td>
					<td CLASS="input" COLSPAN="1">
					  <input NAME="HealthAccFoundDate" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
					</td>
    			<td CLASS="title">账户余额</td>
    			<td CLASS="input" COLSPAN="1">
    			  <input NAME="HealthAccBala" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
        	</td>
    			<td CLASS="title">最后修改日期 </td>
    			<td CLASS="input" COLSPAN="1">
    			  <input NAME="HealthAccModifyDate" VALUE CLASS="common1" READONLY="true"TABINDEX="-1">
    			</td>
    			<td CLASS="title"> </td>
    			<td CLASS="input" COLSPAN="1"></td>
        </tr>
      </table>
    </div>  
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpAccFee);">
    		</td>
    		<td class= titleImg>
    			 管理费定义清单
    		</td>
    	</tr>
    </table>
    <Div  id= "divGrpAccFee" style= "display: "> 
      	<table  class= two>
       		<tr class=common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanManageFeeGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    </DIV>  
   
    <table class=common>	   
      <tr CLASS="common">
  			<td CLASS="title">特需预定利率：</td>
  			<td CLASS="input" COLSPAN="1">
  			  <input NAME="Rate" VALUE CLASS="common1" READONLY="true"TABINDEX="-1"></td>
  			<td CLASS="title"></td>
  			<td CLASS="input" COLSPAN="1"></td>
        <td CLASS="title"> </td>
  			<td CLASS="input" COLSPAN="1"></td>
  			<td CLASS="title"></td>
  			<td CLASS="input" COLSPAN="1"></td>
      </tr>
    </table>
   
    <br>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPol1);">
    		</td>
    		<td class= titleImg>
    			 账户交易查询:
    		</td>		
    	</tr>
    </table>
  	<Div  id= "divPol1" style= "display: ''" align = left>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanSpecGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<INPUT VALUE="账户交易信息下载"  class="cssButton" TYPE="button" onclick="printf();">
  	  <Div id= "divPage2" align="center" style= "display: '' ">
        <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage1.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage1.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage1.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage1.lastPage();"> 					
      </Div>			
  	</Div>	
    
    <br>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHealthTrace);">
    		</td>
    		<td class= titleImg>
    			 健康管理账户交易查询:
    		</td>		
    	</tr>
    </table>
  	<Div  id= "divHealthTrace" style= "display: ''" align = left>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanHealthTraceGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		    <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage2.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage2.lastPage();"> 			
  	</Div>	
	<Input class="common" name="LLAccSql" type=hidden>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>


