<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：TBIndAcceptQueryNewInput.jsp
		//程序功能：新承保数据每日生成
		//创建日期：2009-9-1 16:49
		//创建人  ：guzg	
		//更新记录：更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="java.util.*"%>
	<%%>
	
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="TBIndAcceptQueryNewInput.js"></SCRIPT>
	</head>
	<body>
		<form action="./TBIndAcceptQueryNewSave.jsp" method=post name=fm target="f1print">
			<table class=common border=0 width=100%>
				<TR class=common>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class="codeno" name=ManageCom elementtype=nacessary
							verify="管理机构|code:comcode&NOTNULL"
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);"
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName>
					</TD>
					<TD class=title>
						回执回销起期
					</TD>
					<TD class=input>
						<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary>
					</TD>
					<TD class=title>
						回执回销止期
					</TD>
					<TD class=input>
						<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						销售渠道
					</TD>
					<TD class=input>
						<Input class=codeNo name=SaleChnl verify="销售渠道"
							ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true>
					</TD>
				</TR>
			</table>
			<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
