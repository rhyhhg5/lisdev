
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();



//***************************************************
//* 单击“查询”进行的操作
//***************************************************

function easyQueryClick()

{  
  if( verifyInput() == false )   
  	return false;  
	var tSQL = "";

	tSQL = "select BlacklistCode,Name,"
	+ " Name1,Name8,Name9,Name10,Remark"
    + " FROM "
    + " LCBlackList" 
    + " where  1=1 and type = '1' "
    + getWherePart('BlacklistCode', 'BlacklistCode')
	+ getWherePart('Name', 'Name')
  	+ " ORDER BY BlacklistCode" ;

	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		turnPage.queryModal(tSQL, EvaluateGrid); 
		return false;
		}
    else turnPage.queryModal(tSQL, EvaluateGrid);
}

function gotoAdd()
{
  //下面增加相应的代码
  showInfo=window.open("./BlackListGroupMain.jsp?transact=insert");
}
function gotoUpdate()
{
  var checkFlag = 0;
  for (i=0; i<EvaluateGrid.mulLineCount; i++)
  {
    if (EvaluateGrid.getSelNo(i))
    {
      checkFlag = EvaluateGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	var	tBlacklistCode = EvaluateGrid.getRowColData(checkFlag - 1, 1);
  	showInfo=window.open("./BlackListGroupMain.jsp?transact=update&BlacklistCode="+tBlacklistCode);
  }else{
  	alert("请选择需要修改的黑名单信息！");
  	return false;
  }
  return true;
}
//***************************************************
//* 点击“删除”进行的操作
//***************************************************
function deleteClick()
{
  var checkFlag = 0;
  for (i=0; i<EvaluateGrid.mulLineCount; i++)
  {
    if (EvaluateGrid.getSelNo(i))
    {
      checkFlag = EvaluateGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	 var tBlacklistCode = EvaluateGrid.getRowColData(checkFlag - 1, 1);
  	 if(tBlacklistCode== null || tBlacklistCode == ""){
  	 	alert("删除时，获取黑名单人员编码失败！");
  		return false;
  	 }
  	 if (confirm("您确实想删除该记录吗?"))
	  {
	  var i = 0;
	  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.fmtransact.value = "DELETE||MAIN";
	  fm.action = "./BlackListPersonQuerySave.jsp?BlacklistCode="+tBlacklistCode;
	  fm.submit(); //提交
	  fm.action="";
	  initForm();
	  }
	  else
	  {
	    alert("您取消了删除操作！");
	  }
  }else{
  	alert("请选择需要删除的黑名单信息！");
  	return false;
  }
}
//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content,aBlacklistCode)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  easyQueryClick();
} 