<%
//程序名称：LOPolLuckNoQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-08-01 20:54:01
//创建人  ：yangtao
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('PrtNo').value = "";
    fm.all('AppntName').value = "";
    fm.all('Remark').value = "";
  }
  catch(ex) {
    alert("在LOPolLuckNoQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLOPolLuckNoGrid();  
  }
  catch(re) {
    alert("LOPolLuckNoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LOPolLuckNoGrid;
function initLOPolLuckNoGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名

    iArray[0+1]=new Array();
    iArray[0+1][0]="印刷号";         		//列名
    iArray[0+1][1]="150px";         		//列名
    iArray[1][3]=0;         		//只读
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="投保人名称";         		//列名
    iArray[1+1][1]="100px";         		//列名
    iArray[2][3]=0;         		//只读
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="保单状态";         		//列名
    iArray[2+1][1]="50px";         		//列名
    iArray[3][3]=0;         		//只读
 
    iArray[3+1]=new Array();
    iArray[3+1][0]="签单日期";         		//列名
    iArray[3+1][1]="100px";         		//列名
     iArray[4][3]=0;         		//只读

    iArray[4+1]=new Array();
    iArray[4+1][0]="当前主险保单号";         		//列名
    iArray[4+1][1]="200px";         		//列名
     iArray[5][3]=0;         		//只读

    iArray[5+1]=new Array();
    iArray[5+1][0]="备注";         		//列名
    iArray[5+1][1]="150px";         		//列名
     iArray[6][3]=0;         		//只读

    LOPolLuckNoGrid = new MulLineEnter( "fm" , "LOPolLuckNoGrid" ); 
    LOPolLuckNoGrid.mulLineCount = 1; 
    LOPolLuckNoGrid.displayTitle = 1; 
    LOPolLuckNoGrid.locked = 1; 
 
    LOPolLuckNoGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LOPolLuckNoGrid.mulLineCount = 0;   
    LOPolLuckNoGrid.displayTitle = 1;
    LOPolLuckNoGrid.hiddenPlus = 1;
    LOPolLuckNoGrid.hiddenSubtraction = 1;
    LOPolLuckNoGrid.canSel = 1;
    LOPolLuckNoGrid.canChk = 0;
    LOPolLuckNoGrid.selBoxEventFuncName = "showOne";
*/
    LOPolLuckNoGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LOPolLuckNoGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
