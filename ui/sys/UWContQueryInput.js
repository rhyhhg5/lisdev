var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false) return false;
    var tStartDate = fm.startDate.value;
    var tEndDate = fm.endDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>3)
	{
		alert("统计期最多为三个月！");
		return false;
	}
	setSQL();
  
	fm.action = "UWContQuery.jsp";
	fm.target = "_blank";
	fm.submit();
}

//
function setSQL()
{
	var companyCode = fm.companyCode.value;
	var strSQL = "select c.contno, c.managecom, CodeName('lcsalechnl', c.salechnl), c.uwdate, current date, c.prem, c.AppntName, CodeName('sex', c.AppntSex), "
						+ "(select d.mobile from lcaddress d where c.appntno = d.customerno and d.addressno = (select addressno from lcappnt b where c.contno = b.contno)), "
						+ "a.name, CodeName('sex', a.sex), a.mobile "
						+ "from lccont c, laagent a "
						+ "where c.agentcode = a.agentcode and c.conttype = '1' and c.uwflag in ('4', '9') and c.managecom like '" + companyCode + "%' "
						+ getWherePart('c.uwdate + 1 day','startDate', '>')
						+ getWherePart('c.uwdate - 1 day','endDate', '<')
						+ " order by c.uwdate"
						;
	fm.querySql.value = strSQL;
}