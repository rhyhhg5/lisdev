//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
var times = 0;
function easyQueryClick()
{
	var strSQL = "";
	
	
	if( tflag == "0" )
 	{
 		strSQL = "select p.EdorNo,min(p.GrpContNo),min(c.RiskCode),sum(distinct p.GetMoney),  case p.EdorState when '0' then '保全确认' when '9' then '待收费'  from LCGrpPol c,LPGrpEdorMain p "

		       + " where p.GrpContNo=c.GrpContNo and p.EdorState in ('0','9')"
			   + " and p.GrpContNo = '" + GrpContNo + "'"
			   + " group by p.EdorNo"
			   + " Union all "
			   + " select p.EdorNo,min(p.GrpContNo),min(b.RiskCode),sum(p.GetMoney), case p.EdorState when '0' then  '保全确认' when '9'  then '待收费'  from LPGrpEdorMain p,LBGrpPol b "
			   + " where p.GrpContNo=b.GrpContNo and p.EdorState in ('0','9')"
			   + " and p.GrpContNo = '" + GrpContNo + "'"
			   + " group by p.EdorNo";
	}
	else
	{	
		
		//当工单号有值时取消三个月的校验,【OoO?】杨天政修改20111020
		if(fm.EdorNo.value ==""||fm.EdorNo.value ==null||fm.EdorNo.value == undefined)
		{
			
	        var tStartDate = fm.all("StartDate").value;
		    var tEndDate = fm.all("EndDate").value;
		    //保全申请日期起期校验
		    if(tStartDate == "" || tStartDate == null)
		    {
		    	alert("保全申请日期起期不能为空！");
		    	return false;
		    }
		    //保全申请日期起期止期校验
		    if(tEndDate == "" || tEndDate == null)
		    {
		    	alert("保全申请日期起期止期不能为空！");
		    	return false;
		    }
		    //保全申请日期起止时间不能超过3个月
		    var t1 = new Date(tStartDate.replace(/-/g,"\/")).getTime();
		    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
		    var tMinus = (t2-t1)/(24*60*60*1000);  
		    if(tMinus>92)
		    {
				alert("保全申请日期起止时间不能超过3个月！");
				return false;
		    }
		}
		else
		{
			var tStartDate = "1900-01-01";
	        var tEndDate = "2100-01-01";
	 
		}
	    var condition = "";
	    if(fm.EdorAppName.value != "")
	    {
	        condition = " and a.edorAppName like '%%" + fm.EdorAppName.value + "%%' "
	    }
	    if(fm.EdorType.value != "")
	    {
	        condition = condition + " and exists(select 1 from LPGrpEdorItem where EdorNo = a.EdorAcceptNo and EdorType = '" + fm.EdorType.value + "') "
	    }
	    if(fm.ManageCom.value != "")
	    {
	        condition = condition + " and exists(select 1 from LPGrpEdorItem where EdorNo = a.EdorAcceptNo) and a.ManageCom like '" + fm.ManageCom.value + "%' "
	    }
	    
		strSQL = "  select distinct a.EdorAcceptNo, a.otherno, '团单客户号', a.EdorAppName, "
		         + "    (select sum(getMoney) "
		         + "    from LPGrpEdorMain c "
		         + "    where c.edorAcceptNo =  a.edorAcceptNo), "
		         + "    (select userName "
		         + "    from LDUser "
		         + "    where LDUser.userCode =b.acceptorno), "
		         + "    (select userName "
		         + "    from LDUser d "
		         + "    where d.userCode = b.Operator), "
		         + "    (select CodeName from LDCode where CodeType = 'taskstatusno' and Code = b.statusNo), "
		         + "    case "
    		     + "        when edorState='0' then '保全确认' "
    		     + "        when edorState='2' then '申请确认' "
    		     + "        when (edorState='9' and not exists(select 1 from lpedorespecialdata where edortype='DJ' and edorno=a.edorAcceptNo and edorvalue='1' ) and exists (select 1 from lcgrpbalplan where balIntv!=0 and  grpcontno=(select grpcontno from LPGrpEdorMain where edoracceptno=a.edorAcceptNo)))  then '正在处理' "
    		     + "        when edorState='9' then '待交费' "
    		     + "        else '正在申请' "
    		     + "    end "
    		     + "from LPEdorApp a, LGWork b "
    		     + "where a.edorAcceptNo = b.workNo "
    		     + "    and otherNoType = '2' and a.EdorState in ('0','9')"
    		     + "    and a.EdorAppDate between '" + tStartDate + "' and '" + tEndDate + "' "
                 + getWherePart('a.EdorAcceptNo','EdorNo')
                 + getWherePart('a.OtherNo','GrpContNo')
                 + condition
                 + getWherePart('a.AppType','AppType')
                 + getWherePart('b.AcceptorNo','Acceptor')
                 + "order by edorAcceptNo desc ";
		}
	
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(strSQL,PolGrid);
  
  setEdorName();
  
  if(times == 0)
  {
    times = times + 1;
    easyQueryClick();
  }
}

function setEdorName()
{
  for(var i = 0; i < PolGrid.mulLineCount; i++)
  {
    var edorNo = PolGrid.getRowColDataByName(i, "EdorAcceptNo");
    var sql = "select EdorType from LPGrpEdorItem where EdorNo = '" + edorNo + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      var edorCode = "";
      for(var j = 0; j < rs.length; j++)
      {
        edorCode = edorCode + rs[j][0] + " ";
      }
      PolGrid.setRowColDataByName(i, "EdorCode", edorCode);
    }
  }
}

// 项目明细查询
function ItemQuery()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击明细查询按钮。" );
	else
	{
	    var cEdorNo = PolGrid.getRowColData(tSel - 1,1);	
	    var cSumGetMoney = 	PolGrid.getRowColData(tSel - 1,6);			
//		parent.VD.gVSwitch.deleteVar("PayNo");				
//		parent.VD.gVSwitch.addVar("PayNo","",cPayNo);
		
		if (cEdorNo == "")
		    return;
		    
		//window.open("../sys/AllGPBqQueryMain.jsp?EdorNo=" + cEdorNo + "&SumGetMoney=" + cSumGetMoney);		
		window.open("../sys/AllGBqItemQueryMain.jsp?EdorNo=" + cEdorNo + "&SumGetMoney=" + cSumGetMoney);		
										
	}
}


	//打印批单
function PrtEdor()
{
	var arrReturn = new Array();
				
		var tSel = PolGrid.getSelNo();
	   
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击查看按钮。" );
		else
		{
			var state=PolGrid. getRowColData(tSel-1,5);
			
			if (state=="正在申请")
				alert ("所选批单处于正在申请状态,请于工单内查询！");
			else{
				fm.all('EdorNo').value = PolGrid. getRowColData(tSel-1,1);
				fm.all('GrpContNo').value = PolGrid. getRowColData(tSel-1,2);
				parent.fraInterface.fm.action = "../f1print/GrpEndorsementF1PJ1.jsp?EdorNo=" + fm.all('EdorNo').value;
				parent.fraInterface.fm.target="f1print";		 	
				// showSubmitFrame(mDebug);
				fm.submit();
				//fm.all('EdorNo').value ="";
				//fm.all('GrpContNo').value="";	
			}
		}
}

function GPBqQueryClick()
{
	
	var tSel = PolGrid.getSelNo();
    fm.all('EdorNo').value = PolGrid. getRowColData(tSel-1,1);
    fm.all('GrpContNo').value = PolGrid. getRowColData(tSel-1,2);
}


//显示被保人清单
function showInsuredList()
{
	var sql = "select EdorType from LPGrpEdorItem " +
	          "where EdorNo = '" + fm.all('EdorNo').value + "' ";
	var arrResult = easyExecSql(sql, 1, 0);
	if (!arrResult)
	{
		alert("没有找到保全信息！");
		return false;
	}
	
	var hasNI = false;
	var hasZT = false;
	var hasCT = false;
	var hasLP = false;
	var hasZB = false;
	var hasGA = false;
	var hasGD = false;
	var hasWS = false;
	var hasWD = false;
	var hasWT = false;
	//BY GZH 20110210
	var hasTY = false;
	//BY XP 20110317
	var hasTZ = false;
	var hasTL = false;
	//刘鑫 保全 20110513
	var hasTQ = false;
	var hasTA = false;
	var hasCM = false;
	var hasZA = false;
	var hasZE = false;
	for (var i = 0; i < arrResult.length; i++)
	{
		if (arrResult[i][0] == "NI")
		{
			hasNI = true;
		}
		if (arrResult[i][0] == "ZT")
		{
			hasZT = true;
		}
		if(arrResult[i][0] == "CT")
		{
		  hasCT = true;
		}
		if(arrResult[i][0] == "LP")
		{
		  hasLP = true;
		}
		if(arrResult[i][0] == "ZB")
		{
		  hasZB = true;
		}
		//BY GZH 20110210
		if(arrResult[i][0] == "TY")
		{
		  hasTY = true;
		}
		if(arrResult[i][0] == "TL")
		{
		  hasTL = true;
		}
		if(arrResult[i][0] == "TZ")
		{
		  hasTZ = true;
		}
		if(arrResult[i][0] == "GA")
		{
		  hasGA = true;
		}
		if(arrResult[i][0] == "GD")
		{
		  hasGD = true;
		}
		if(arrResult[i][0] == "WS")
		{
		  hasWS = true;
		}
		if(arrResult[i][0] == "WD")
		{
		  hasWD = true;
		}
		if(arrResult[i][0] == "WT")
		{
		  hasWT = true;
		}
		if(arrResult[i][0] == "TQ")
		{
		  hasTQ = true;
		}
		if(arrResult[i][0] == "TA")
		{
		  hasTA = true;
		}
		if(arrResult[i][0] == "CM")
		{
		  hasCM = true;
		}
		if(arrResult[i][0] == "ZA")
		{
		  hasZA = true;
		}
		if(arrResult[i][0] == "ZE")
		{
		  hasZE = true;
		}
		if(hasNI && hasZT && hasCT && hasLP && hasZB && hasGA && hasGD && hasWS && hasWD && hasTY && hasTL && hasTZ && hasTQ && hasTA && hasCM && hasZA && hasZE)
		{
			break;
		}
	}
	
	if (hasNI == true)
	{
		window.open("../bq/ShowInsuredList.jsp?EdorAcceptNo="+fm.all('EdorNo').value);
	}
	if(hasZT == true)
	{
		window.open("../bq/GInsuredListZTMain.jsp?edorType=ZT&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasCT == true)
	{
		window.open("../bq/GInsuredListZTMain.jsp?edorType=CT&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasLP == true)
	{
		window.open("../bq/GInsuredListLPMain.jsp?edorType=LP&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasZB == true)
	{
		window.open("../bq/GInsuredListZBMain.jsp?edorType=ZB&edorAcceptNo=" + fm.EdorNo.value);
	}
	//BY GZH 20110210
	if(hasTY == true)
	{
		window.open("../bq/GInsuredListTYMain.jsp?edorType=TY&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasTL == true)
	{
		window.open("../bq/GInsuredListTLMain.jsp?edorType=TL&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasTZ == true)
	{
		window.open("../bq/GInsuredListTZMain.jsp?edorType=TZ&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasGA == true)
	{
		window.open("../bq/GInsuredListGAMain.jsp?edorType=GA&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasGD == true)
	{
		window.open("../bq/GInsuredListGDMain.jsp?edorType=GD&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasWS == true)
	{
		window.open("../bq/GInsuredListWSMain.jsp?edorType=WS&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasWD == true)
	{
		window.open("../bq/GInsuredListWDMain.jsp?edorType=WD&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasTQ == true)
	{
		window.open("../bq/GInsuredListTQMain.jsp?edorType=TQ&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasTA == true)
	{
		window.open("../bq/GInsuredListTAMain.jsp?edorType=TA&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasWT == true)
	{
		window.open("../bq/GInsuredListWTMain.jsp?edorType=WD&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasCM == true)
	{
		window.open("../bq/GInsuredListCMMain.jsp?edorType=CM&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasZA == true)
	{
		window.open("../bq/GInsuredListZAMain.jsp?edorType=GA&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasZE == true)
	{
		window.open("../bq/GInsuredListZEMain.jsp?edorType=GA&edorAcceptNo=" + fm.EdorNo.value);
	}
	if(hasNI == false && hasZT == false && hasCT == false&& hasLP == false && hasZB == false && hasGA == false && hasGD == false && hasWS == false && hasWD == false && hasWT == false && hasTY == false && hasTZ == false && hasTL == false && hasTQ == false && hasTA == false && hasCM == false && hasZA == false && hasZE == false  )
	{
		alert("该受理没有被保人清单！");
	}
}