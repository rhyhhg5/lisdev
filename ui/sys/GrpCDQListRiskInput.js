//该文件中包含客户端需要处理的函数和事件
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
// 查询按钮
function easyQueryClick()
{
	//alert ("start easyQueryClick");
	// 初始化表格
	initSetGrid();
	if( verifyInput() == false ) return false;
	if(!checkData()){
		return false;
	}

	//获取复选的市场类型
	var mt = document.getElementsByName("MarketType");
	var mtsql = "";
	var fxsql1="";
	var j=0;
	var arrmt = [];
	for(var i=0;i<mt.length;i++){
		if(mt[i].checked){
			arrmt[j] = mt[i].value;
			j++;
		}
	}
//    alert(arrmt);
    if(arrmt.length>0){
//    	mtsql += " and a.markettype in (";
    	for(var i=0;i<arrmt.length;i++){
    		if(mtsql==""){
    			mtsql += "'"+arrmt[i]+"'";
    		}else{
    			mtsql += ",'"+arrmt[i]+"'";
    		}
    	}
    	fxsql1 = " and a.markettype in ("+ mtsql +") ";
    	
    }
    //获取复选的销售渠道
    var sc = document.getElementsByName("SaleChnl");
    var scsql = "";
    var fxsql2 = "";
    var k=0;
    var arrsc = [];
    for(var i=0;i<sc.length;i++){
    	if(sc[i].checked){
    		arrsc[k] = sc[i].value;
    		k++;
    	}
    }
//    alert(arrsc);
    if(arrsc.length>0){
//    	mtsql += " and a.markettype in (";
    	for(var i=0;i<arrsc.length;i++){
    		if(scsql==""){
    			scsql += "'"+arrsc[i]+"'";
    		}else{
    			scsql += ",'"+arrsc[i]+"'";
    		}
    	}
    	fxsql2 = " and a.salechnl in ("+ scsql +") ";
    	
    }
    //获取复选的出单平台
    var cf = document.getElementsByName("CardFlag");
    var cfsql = "";
    var fxsql3 = "";
    var f = false;
    var l=0;
    var arrcf = [];
    for(var i=0;i<cf.length;i++){
    	if(cf[i].checked){
    		if(cf[i].value=="standardlgc"){
    			f = true;
    		}else{
    			arrcf[l] = cf[i].value;
    			l++;
    		}
    	}
    }
//    alert(arrcf);
    if(arrcf.length>0){
//    	mtsql += " and a.markettype in (";
    	for(var i=0;i<arrcf.length;i++){
    		if(cfsql==""){
    			cfsql += "'"+arrcf[i]+"'";
    		}else{
    			cfsql += ",'"+arrcf[i]+"'";
    		}
    	}
    	if(f){
    		fxsql3 = " and (a.cardflag in ("+ cfsql +") or a.cardflag is null) ";
    	}else{
    		fxsql3 = " and a.cardflag in ("+ cfsql +") ";
    	}
    	
    }
    //获取险种
    var riskStr = fm.all('RiskCode').value;
    riskStr = riskStr.replace(/^\s+/, '');
    for (var i = riskStr.length - 1; i >= 0; i--) {
    	if (/\S/.test(riskStr.charAt(i))) {
    		riskStr = riskStr.substring(0, i + 1);
    		break;
		}
    }
//    alert(riskStr);
    var risksql="";
    if(riskStr!="" && riskStr!=null){
    	var riskarr = riskStr.replace(/,/g,"','");
    	riskarr = "'" + riskarr + "'";
//    	alert(riskarr);
    	risksql=" and lgp.riskcode in (" + riskarr + ")";
    }
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.ManageCom"
		+",(select name from ldcom where comcode=a.managecom)"
		+",a.grpcontno,a.prtno"
		+",(select distinct codename from ldcode where (codetype='salechnl' or codetype='lcsalechnl') and code=a.salechnl)"
		+",(select distinct codename from ldcode where codetype='markettype' and code=a.markettype)"
		+",getUniteCode(a.agentcode),(select name from laagent where agentcode=a.agentcode)"
		+",(select branchattr from labranchgroup where agentgroup=a.agentgroup)"
		+",(select name from labranchgroup where agentgroup=a.agentgroup)"
		+",a.grpname,a.polapplydate,a.inputdate,a.cvalidate,a.cinvalidate"
		+",a.signdate"
		+",(select codename from ldcode where code=a.paymode and codetype='paymode')"
		+",(select codename from ldcode where code=char(a.payintv) and codetype='payintv')"
		+",(case when a.payintv = -1 then (select sum(prem) from lcgrppayplandetail where prtno = a.prtno and riskcode = lgp.riskcode) else ( case when a.appflag = '1' then (select nvl(sum(ljag.sumactupaymoney),0) from ljapay lja,ljapaygrp ljag where lja.incomeno = a.grpcontno and lja.duefeetype = '0' and lja.payno = ljag.payno and ljag.grppolno = lgp.grppolno) else lgp.prem end) end) "
		+",lgp.peoples2"
		+",case when a.uwflag = 'a' then '已撤销' when a.uwflag = '1' then '拒保' else (select codename from ldcode where code=a.appflag and codetype='appflag') end "
		+",case when a.appflag = '0' then '0' else varchar((select nvl(sum(ljag.sumactupaymoney),0) from ljapay lja,ljapaygrp ljag where lja.incomeno = a.grpcontno and lja.duefeetype = '0' and lja.payno = ljag.payno and ljag.grppolno = lgp.grppolno)) end "
		+",case when a.appflag = '0' then '0' else varchar((select (select nvl(sum(ljag.sumactupaymoney),0) from ljapay lja,ljapaygrp ljag where lja.incomeno = a.grpcontno and ljag.grppolno = lgp.grppolno and lja.payno = ljag.payno) + (select nvl(sum(getmoney),0) from ljagetendorse where grpcontno = a.grpcontno and grppolno = lgp.grppolno and feeoperationtype<>'MJ' ) from dual)) end "
		+ ",lgp.riskcode "
		+ ",(select bigprojectno from lcbigprojectcont where prtno=a.prtno)"
		+ ",(select bigprojectname from lcbigprojectinfo where bigprojectno = (select bigprojectno from lcbigprojectcont where prtno=a.prtno)) " 
		+ ",case when a.cardflag is null then '标准平台' else (select b.codename from ldcode b where b.codetype='cardflag2' and b.code=a.cardflag) end "
		+ " from lcgrpcont a "
		+ " inner join lcgrppol lgp on a.grpcontno = lgp.grpcontno "
		+ " where  1=1 "
		+ fxsql1 
		+ fxsql2 
		+ fxsql3 
		+ risksql 
		+ getWherePart('a.ManageCom', 'ManageCom', 'like')
		+ getWherePart('a.InputDate', 'StartDate', '>=')
		+ getWherePart('a.InputDate', 'EndDate', '<=')
		+ getWherePart('a.CValiDate', 'StartCvaliDate', '>=')
		+ getWherePart('a.CValiDate', 'EndCvaliDate', '<=')
		+ getWherePart('a.SignDate', 'StartSignDate', '>=')
		+ getWherePart('a.SignDate', 'EndSignDate', '<=')
		+ getWherePart('getUniteCode(a.agentcode)','AgentCode')
		+" and not exists (select 'X' from lcrnewstatelog where newgrpcontno = a.grpcontno)"
		+" Order by a.managecom ";
    turnPage.queryModal(strSQL, SetGrid);
    if(SetGrid.mulLineCount==0){
    	alert("没有查询到任何满足条件的数据！");
    	return false;
 	}
 	fm.querySql.value = strSQL;
}
function DoNewDownload()
{
	if( verifyInput() == false ) return false;
 // 书写SQL语句
	if(fm.querySql.value != null && fm.querySql.value != "" && SetGrid.mulLineCount > 0)
    {
        fm.action = "GrpCDQListRiskReport.jsp";
    	fm.submit();
    }
    else
    {
        alert("请先执行查询操作，再下载！");
        return ;
    }
}					
//校验查询条件
function checkData()
{
	var tStartDate = fm.all('StartDate').value;
	var tEndDate = fm.all('EndDate').value;
	var tStartCValiDate = fm.all('StartCValiDate').value;
	var tEndCValiDate = fm.all('EndCValiDate').value;
	var tStartSignDate = fm.all('StartSignDate').value;
	var tEndSignDate = fm.all('EndSignDate').value;
	
	if((tStartDate == null || tStartDate == "") && (tEndDate == null || tEndDate == "")
		&&(tStartCValiDate == null || tStartCValiDate == "")&&(tEndCValiDate == null || tEndCValiDate == "")
		&&(tStartSignDate == null || tStartSignDate == "")&&(tEndSignDate == null || tEndSignDate == ""))
	{
		alert("录入日期起止、生效日期起止和签单日期起止三组条件，必须填写其中一组才能进行查询！");
		return false;
	}
	
	if((tStartDate != null && tStartDate != "") 
		&& (tEndDate == null || tEndDate == ""))
	{
		alert("请选择录入日期止期！");
		return false;
	}
	
	if((tStartDate == null || tStartDate == "") 
		&& (tEndDate != null && tEndDate != ""))
	{
		alert("请选择录入日期起期！");
		return false;
	}
	
	// 因原有报表无时间段校验，经与IT商定，暂时不加时间段校验处理。
	//后查询时间太长，增加时间校验。3个时间有一个在3个月内即可
	var tStartDateFlag = false;
	if((tStartDate != null && tStartDate != "") 
		&& (tEndDate != null && tEndDate != ""))
	{
		if(dateDiff(tStartDate,tEndDate,"M") > 3)
		{
			tStartDateFlag = true;
		}
	}
	
	if((tStartCValiDate != null && tStartCValiDate != "") 
		&& (tEndCValiDate == null || tEndCValiDate == ""))
	{
		alert("请选择生效日期止期！");
		return false;
	}
	
	if((tStartCValiDate == null || tStartCValiDate == "") 
		&& (tEndCValiDate != null && tEndCValiDate != ""))
	{
		alert("请选择生效日期起期！");
		return false;
	}
	
	// 因原有报表无时间段校验，经与IT商定，暂时不加时间段校验处理。
	var tStartCValiDateFlag = false;
	if((tStartCValiDate != null && tStartCValiDate != "") 
		&& (tEndCValiDate != null && tEndCValiDate != ""))
	{
		if(dateDiff(tStartCValiDate,tEndCValiDate,"M") > 3)
		{
			tStartCValiDateFlag = true;
		}
	}
	
	if((tStartSignDate != null && tStartSignDate != "") 
		&& (tEndSignDate == null || tEndSignDate == ""))
	{
		alert("请选择签单日期止期！");
		return false;
	}
	
	if((tStartSignDate == null || tStartSignDate == "") 
		&& (tEndSignDate != null && tEndSignDate != ""))
	{
		alert("请选择签单日期起期！");
		return false;
	}
	
	// 因原有报表无时间段校验，经与IT商定，暂时不加时间段校验处理。
	var tStartSignDateFlag = false;
	if((tStartSignDate != null && tStartSignDate != "") 
		&& (tEndSignDate != null && tEndSignDate != ""))
	{
		if(dateDiff(tStartSignDate,tEndSignDate,"M") > 3)
		{
			tStartSignDateFlag = true;
		}
	}
	
	if(tStartDateFlag && tStartCValiDateFlag && tStartSignDateFlag){
		alert("选择的时间段，必须有一个时间段在3个月内！");
		return false;
	}
	
	if(tStartDate == null || tStartDate == ""){
		if(tStartCValiDate == null || tStartCValiDate == ""){
			if(tStartSignDateFlag){
				alert("查询时间间隔最长为3个月！");
				return false;
			}
		}else{
			if(tStartSignDate == null || tStartSignDate == ""){
				if(tStartCValiDateFlag){
					alert("查询时间间隔最长为3个月！");
					return false;
				}
			}else{
				if(tStartCValiDateFlag && tStartSignDateFlag){
					alert("选择的时间段，必须有一个时间段在3个月内！");
					return false;
				}
			}
		}
	}else{
		if(tStartCValiDate == null || tStartCValiDate == ""){
			if(tStartSignDate == null || tStartSignDate == ""){
				if(tStartDateFlag){
					alert("查询时间间隔最长为3个月！");
					return false;
				}
			}else{
				if(tStartDateFlag && tStartSignDateFlag){
					alert("选择的时间段，必须有一个时间段在3个月内！");
					return false;
				}
			}
		}else{
			if(tStartSignDate == null || tStartSignDate == ""){
				if(tStartDateFlag && tStartCValiDateFlag){
					alert("选择的时间段，必须有一个时间段在3个月内！");
					return false;
				}
			}
		}
	}
	
	return true;
}