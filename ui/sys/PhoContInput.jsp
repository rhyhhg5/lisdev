<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = "";
	String tCustomerNo="";
	try
	{
		tContNo = request.getParameter( "ContNo" );
		
		//默认情况下为集体保单
		if( tContNo == null || tContNo.equals( "" ))
			tContNo = "00000000000000000000";

		tCustomerNo = request.getParameter( "CustomerNo" );
		if( tCustomerNo == null )
	       tCustomerNo="";
	}
	
	
	catch( Exception e1 )
	{
		tContNo = "00000000000000000000";
			System.out.println("---contno:"+tContNo);

	}
	System.out.println("---contno:"+tContNo);
        String tDisplay = "";
	try
	{
		tDisplay = request.getParameter("display");
		if(tDisplay == null || tDisplay.equals( "" ))
		 { tDisplay = "0";}
	}
	catch( Exception e )
	{
		tDisplay = "";
	}
%>

<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   
<script>
	var contNo = "<%=tContNo%>";  //个人单的查询条件.
	var comCode = "<%=tG.ComCode%>";
	var tDisplay = "<%=tDisplay%>";
	var customerNo = "<%=tCustomerNo%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="PhoContInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="PhoContInputInit.jsp"%>
  <title>保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
			<td><INPUT VALUE="查  询" class = cssbutton TYPE=button onclick="easyQueryClick();"></td> 
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
		  <TD  class= title> 客户号码 </TD>  
		  <TD  class= input> <Input class="common1" name=CustomerNo > </TD>
		  <TD  class= title> 客户名称 </TD>
          <TD  class= input> <Input class="common1" name=Name> </TD>          
          <TD  class= title> 出生月日 </TD>
          <TD  class= input> <Input class= common1 name=Birthday MAXLENGTH="5">格式必须为：MM-DD</TD>
          <TD  class= title> 性别 </TD>
           <TD  class= input><Input class=codeNo name=Sex verify="代理人编码|code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);showAllCodeName();"><input class=codename name=SexName readonly=true >
          </TD>
        </TR>
        <TR  class= common>
		  <TD  class= title> 证件号码 </TD>
          <TD  class= input> <Input class="common1" name=IDNo> </TD>
		  <TD  class= title> 保单号码 </TD>
          <TD  class= input> <Input class= common1 name=ContNo >  </TD>		 
		  <TD  class= title> 管理机构 </TD>
          <TD  class= input> <Input class="common1" name=ManageCom> </TD>
		  <TD  class= title> 单位名称 </TD>
          <TD  class= input> <Input class="common1" name=AppntName> </TD>
        </TR>
    </table>
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCClent);">
    		</td>
    		<td class= titleImg>
    			 个单客户信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCClent" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPersonGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage1.firstPage();showCodeName();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage1.previousPage();showCodeName();"> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage1.nextPage();showCodeName();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage1.lastPage();showCodeName();"> 		
    </div>
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpClent);">
    		</td>
    		<td class= titleImg>
    			 团单客户信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCGrpClent" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpPersonGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage2.previousPage();"> 		
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage2.lastPage();"> 		
    </div>     
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="getLastPage();"> 					
  	</div>
	<br>
	 <table class= common border=0 width=100%>
    	<tr>
            <td><INPUT VALUE="保单明细" class = cssbutton TYPE=button onclick="returnParent();"> </td> 
		</tr>
	</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
