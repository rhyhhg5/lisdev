<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskCommonView.jsp
//程序功能：工单管理工单基本信息显示页面
//创建日期：2005-03-20 15:52:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<%@include file="TaskCommonViewInit.jsp"%>
	<Input type="hidden" class= common name="WorkNo">
    <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTaskInfo);">
	      </td>
      <td class= titleImg>工单信息&nbsp;</td>
	  </tr>
    </table>
    <Div id="divTaskInfo" style="display: ''"> 
    <table class=common>
      <tr class=common>
        <TD class= title> 受理号 </TD>
        <TD class= input>
      		<Input class="readonly" name="AcceptNo" readonly >
        </TD>
        <TD class= title> 客户号 </TD>
        <TD class= input>
	    	<Input class="readonly" name="CustomerNo" readonly > 
	    </TD>  
	    <TD class= title> 优先级别 </TD>
        <TD class= input>
        	<input class="readonly" name="PriorityNo" ondblclick="return showCodeList('PriorityNo', [this]);" onkeyup="return showCodeListKey('PriorityNo', [this]);">
        </TD>
      </tr>
      <tr>
        <TD class= title> 业务类型 </TD>
        <TD class= input>
        	<input class="readonly" name="TypeNo" ondblclick="return showCodeList('TaskTypeNo', [this]);" onkeyup="return showCodeListKey('TaskTypeNo', [this]);">
        </TD>
        <TD class= title> 子业务类型 </TD>
        <TD class= input>
        	<!--<input class="readonly" name="TypeNo" ondblclick="return showCodeList('TaskTypeNo', [this]);" onkeyup="return showCodeListKey('TaskTypeNo', [this]);" verify="业务类型|notnull&code:TaskTypeNo">-->
        </TD>
        <TD class= title> 时限 </TD>
        <TD class= input>
        	<input class="readonly" name="DateLimit" readonly >
        </TD>
      </tr>
      <tr>
        <TD class= title> 申请人类型 </TD>
        <TD class= input>
        	<input class="readonly" name="ApplyTypeNo" ondblclick="return showCodeList('ApplyTypeNo', [this]);" onkeyup="return showCodeListKey('ApplyTypeNo', [this]);">
        </TD>
        <TD class= title> 申请人姓名 </TD>
        <TD class= input>
        	<input class="readonly" name="ApplyName" readonly >
         </TD> 
        <TD class= title> 受理途径 </TD>
        <TD class= input>
        	<input class="readonly" name="AcceptWayNo" ondblclick="return showCodeList('AcceptWayNo', [this]);" onkeyup="return showCodeListKey('AcceptWayNo', [this]);">
        </TD>    
      </tr>
      <tr>
        <TD class= title> 受理机构 </TD>
        <TD class= input>
            <input class="readonly" name="AcceptCom" ondblclick="return showCodeList('AcceptCom', [this]);" onkeyup="return showCodeListKey('AcceptCom', [this]);">
        </TD>
        <TD class= title> 受理人 </TD>
        <TD class= input>
        	<Input class="readonly" name="AcceptorNo" readonly >
        </TD>
        <TD class= title> 受理日期 </TD>
        <TD class= input>
        	<Input class="readonly" name="AcceptDate" readonly >
        </TD>        
      </tr>
      <tr>
        <TD  class= title> 备注 </TD>
        <TD  colspan="7"  class= input>
        	<textarea class="common" name="RemarkContent" cols="90%" rows="3"></textarea> 
        </TD>
      </tr>
    </table>
    </Div>