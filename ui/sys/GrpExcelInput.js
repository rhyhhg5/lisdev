var showInfo;
var turnPage = new turnPageClass();

function Print()
{	
	if(!checkData()){
		return false;
	}
	
	var tSQL1 = "select lgc.managecom 管理机构,"
		+" (select name from ldcom where comcode=lgc.managecom) 机构名称,"
		+" lgc.grpcontno 保单号,"
		+" lgc.salechnl 销售渠道,"
		+" (select codename from ldcode where codetype='salechnl' and code=lgc.salechnl) 渠道名称,"
		+"  lgc.grpname 投保单位名称,"
		+" lgc.cvalidate 生效日期,"
		+" lgc.cinvalidate 失效日期,"
		+" lgc.signdate 签单日期,"
		+" lgc.prem 保费,"
		+" lgp.riskcode 险种,"
//		+" (SELECT a.feevalue FROM lcgrpfee a WHERE a.grpcontno = lgc.grpcontno AND feecode = '000001') 费率,"
//		+" (CASE WHEN (lgp.riskcode = '162501') THEN trim(replace(strip(replace(char(cast(("
//		+" SELECT a.feevalue FROM lcgrpfee a WHERE a.grpcontno = lgc.grpcontno AND feecode = '000001'"
 //       +" ) as decimal(8,2))),'.','#'),L,'0'),'#','0.'))  else '' END ) 费率,"
		+" (select calfactorvalue from lccontplandutyparam c where c.grpcontno=lgc.grpcontno and  calfactor='Mult' fetch first 1 rows only) 档次"
		+" from lcgrpcont lgc,lcgrppol lgp"
		+" where lgc.grpcontno=lgp.grpcontno"
		+" and lgc.appflag='1'"
		+" and lgc.stateflag='1'"
		+" and lgp.riskcode in ('162501','162401')"		
		+ getWherePart("lgc.managecom","ManageCom","like")	
		+ getWherePart("lgc.signdate","SignStartDate",">=")					 
		 + getWherePart("lgc.signdate","SignEndDate","<=");
	var tSQL2 = "select lgc.managecom 管理机构,"
		+" (select name from ldcom where comcode=lgc.managecom) 机构名称,"
		+" lgc.grpcontno 保单号,"
		+" lgc.salechnl 销售渠道,"
		+" (select codename from ldcode where codetype='salechnl' and code=lgc.salechnl) 渠道名称,"
		+"  lgc.grpname 投保单位名称,"
		+" lgc.cvalidate 生效日期,"
		+" lgc.cinvalidate 失效日期,"
		+" lgc.signdate 签单日期,"
		+" lgc.prem 保费,"
		+" lgp.riskcode 险种,"
		+" (SELECT a.feevalue FROM lcgrpfee a WHERE a.grpcontno = lgc.grpcontno AND feecode = '000001') 管理费比例,"
		+" (select defaultrate from lcgrpinterest b where b.grpcontno=lgc.grpcontno) 利率"
		+" from lcgrpcont lgc,lcgrppol lgp"
		+" where lgc.grpcontno=lgp.grpcontno"
		+" and lgc.appflag='1'"
		+" and lgc.stateflag='1'"
		+" and lgp.riskcode in ('170301','170401')"	
		+ getWherePart("lgc.managecom","ManageCom","like")	
		+ getWherePart("lgc.signdate","SignStartDate",">=")					 
		 + getWherePart("lgc.signdate","SignEndDate","<=");
	var tSQL3 = "select lgc.managecom 管理机构,"
		+" (select name from ldcom where comcode=lgc.managecom) 机构名称,"
		+" lgc.grpcontno 保单号,"
		+" lgc.salechnl 销售渠道,"
		+" (select codename from ldcode where codetype='salechnl' and code=lgc.salechnl) 渠道名称,"
		+"  lgc.grpname 投保单位名称,"
		+" lgc.cvalidate 生效日期,"
		+" lgc.cinvalidate 失效日期,"
		+" lgc.signdate 签单日期,"
		+" lgc.prem 保费,"
		+" lgp.riskcode 险种,"
//		+" (select a.feevalue from lcgrpfee a where a.grpcontno=lgc.grpcontno and feecode='000001') 费率,"
		+"  (case when (select 1 from lpgrpedoritem a where edortype = 'GD' and a.grpcontno=lgc.grpcontno and exists (select 1 from lpedorapp b where b.edoracceptno=a.edorno and b.edorstate='0') fetch first 1 rows only)=1" 
		+" then '是' else '否' end  ) 是否已分配保额,"
		+" (select calfactorvalue from lccontplandutyparam c where c.grpcontno=lgc.grpcontno and  calfactor='Mult' fetch first 1 rows only) 档次"	
		+" from lcgrpcont lgc,lcgrppol lgp"
		+" where lgc.grpcontno=lgp.grpcontno"
		+" and lgc.appflag='1'"
		+" and lgc.stateflag='1'"
		+" and lgp.riskcode = '170501'"	
		+ getWherePart("lgc.managecom","ManageCom","like")	
		+ getWherePart("lgc.signdate","SignStartDate",">=")					 
		 + getWherePart("lgc.signdate","SignEndDate","<=");
	
	
    fm.querySql1.value=tSQL1;  
    fm.querySql2.value=tSQL2;  
    fm.querySql3.value=tSQL3;  
    fm.action = "GrpExcelDownload.jsp";
    fm.submit();
    fm.action = "";
    fm.querySql1.value="";  
    fm.querySql2.value="";  
    fm.querySql3.value="";  
}


function checkData(){
	if(fm.ManageCom.value == ""){
		alert("管理机构不能为空");
		return false;
	}
	var signdate = fm.SignStartDate.value == "" || fm.SignEndDate.value == "";	
	if(signdate ){
		
		alert("签单日起期和签单日止期为保单签单日，必录。");
		return false;
	}
	
	
	return true;
}