
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">

</head>

<body  onload="" >
  <form name=fm target="fraTitle">
	<Div  id= "divButton" style= "display: ''">
    <table class=common>
       <tr>
          <TD  class= title>  险种编码 </TD>
          <TD  class= input> <Input class="code" name=RiskCode ondblclick=" showCodeList('RiskInd',[this]);" onkeyup="return showCodeListKey('RiskInd',[this]);"> </TD>
          <TD  class= title> 主险保单号码 </TD>
          <TD  class= input> <Input class="readonly" readonly name=MainPolNo verify="主险投保单号码|notnull" >  </TD>     
          <TD  class= title>   合同号  </TD>
          <TD  class= input>  <Input class= readonly name=ContNo > </TD>
		</tr>        
     </table>
    </Div>
	 	<table>
    		<tr>
    		<td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskInfo);"></td>
    		<td class= titleImg> 险种信息 </td>
    		</tr>
    	</table>
    <Div  id= "divRiskInfo" style= "display: ''">
	 <table class= common>
	
	   	<TR class=common>	
			<TD  class= title> 保单号码 </TD>
        	<TD  class= input> <Input class="readonly" readonly name=PolNo > </TD>
        	<TD  class= title> 集体保单号码  </TD>
            <TD  class= input> <Input class="readonly" readonly name=GrpPolNo > </TD>
            <TD  class= title> 总单/合同号码 </TD>
            <TD  class= input> <Input class="readonly" readonly name=ContNo  > </TD>
        </TR>
        <TR class= common>
          <TD  class= title> 印刷号码 </TD>
          <TD  class= input> <Input name=PrtNo class="readonly" readonly > </TD>
          <TD  class= title> 代理人编码 </TD>
          <TD  class= input> <Input class="code" name=AgentCode verify="代理人编码|notnull&code:AgentCode" ondblclick="return showCodeList('AgentCode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('AgentCode',[this],null,null,null,null,1);"> </TD>  
          <TD  class= title> 代理人组别 </TD>
          <TD  class= input> <Input class="readonly" readonly name=AgentGroup  > </TD>  
        </TR>       
        <TR class= common>
          <TD  class= title> 管理机构 </TD>
          <TD  class= input> <Input class="code"  name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> </TD>        
          <TD  class= title> 代理机构 </TD>
          <TD  class= input> <Input name=AgentCom class="readonly" readonly > </TD>
          <TD  class= title>  销售渠道 </TD>
          <TD  class= input> <Input class="code" name=SaleChnl ondblclick="return showCodeList('SaleChnl',[this]);"  onkeyup="return showCodeListKey('SaleChnl',[this]);"> </TD>
        </TR>
        <!--TR class= common>
          <TD  class= title>  是否孤儿单标示?  </TD>
          <TD  class= input> <Input class= readonly name=SignDateFlag > </TD>
          <TD  class= title> 续期收费员编号? </TD>
          <TD  class= input> <Input class= readonly name=FirstPayDateFlag > </TD>
          <TD  class= title>  续期收费员?  </TD>
          <TD  class= input>  <Input class= readonly name=PayEndDateFlag >  </TD>
        </TR-->
        <TR class= common>
          <TD  class= title> 保单状态 </TD>
          <TD  class= input> <Input class="code" name=PolState verify="保单状态|NOTNULL" CodeData="0|^0|有效^1|失效^2|解约" ondblClick="showCodeListEx('PolState_1',[this],[0,1,2]);" onkeyup="showCodeListKeyEx('PolState_1',[this],[0,1,2]);"></TD>
          <TD  class= title>  签单日期  </TD>
          <TD  class= input> <Input class= readonly name=SignDate > </TD>
        </TR>
        <TR class= common>
          <TD  class= title> 保单生效日期 </TD>
          <TD  class= input> <input class="readonly" dateFormat="short" name="CValiDate" verify="保单生效日期|NOTNULL" ></TD>	
          <TD  class= title>  最近复效日期   </TD>
          <TD  class= input>  <Input class= readonly name=LastRevDate >  </TD>           
          <TD  class= title> 保单送达日期 </TD>
          <TD  class= input> <Input class= readonly name=CustomGetPolDate > </TD>
        </TR>

        <TR class= common>
          <TD  class= title> 交费年期/(年) </TD>
          <TD  class= input> <Input class= readonly name=PayYears > </TD> 
          <TD  class= title> 首期交费日期 </TD>
          <TD  class= input> <Input class= readonly name=FirstPayDate > </TD>                           
          <TD  class= title> 交至日期 </TD>
          <TD  class= input> <Input class= readonly name=PaytoDate > </TD>          
        </TR>
        
        <TR class= common>
          <TD  class= title>  终交日期  </TD>
          <TD  class= input>  <Input class= readonly name=PayEndDate >  </TD>
          <TD  class= title>  终交期间 </TD>
          <TD  class= input> <input class="readonly" name="PayEndYear"></TD>
          <TD  class= title>  终交期间单位 </TD>
          <TD  class= input> <Input class="code" name=PayEndYearFlag ondblclick="return showCodeList('PeriodUnit',[this]);" onkeyup="return showCodeListKey('PeriodUnit',[this]);"> </TD>
        </TR>
      <TR class= common>
		 <TD  class= title> 交费间隔 </TD>
         <TD  class= input> <Input class="code" name=PayIntv ondblclick="return showCodeList('PayIntv',[this]);" onkeyup="return showCodeListKey('PayIntv',[this]);"> </TD>
         <TD  class= title>  收费方式 </TD>
         <TD  class= input> <Input class="code" name=PayLocation verify="收费方式|code:PayLocation" ondblclick="return showCodeList('PayLocation',[this]);" onkeyup="return showCodeListKey('PayLocation',[this]);"> </TD>       
		  <!--TD  class= title> 银行委托书号码 </TD>
          <TD  class= input> <Input class="readonly" name=ConsignNo> </TD-->	        
       </TR>  
        <TR CLASS=common>
          <TD CLASS=title> 开户行   </TD>
          <TD CLASS=input COLSPAN=1> <Input NAME=BankCode VALUE="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('bank', [this]);" onkeyup="return showCodeListKey('bank', [this]);" verify="开户行|code:bank" > </TD>
          <TD CLASS=title> 银行帐号   </TD>
          <TD CLASS=input COLSPAN=1>  <Input NAME=BankAccNo class="readonly" MAXLENGTH=40 > </TD>
          <TD CLASS=title> 户名  </TD>
          <TD CLASS=input COLSPAN=1> <Input NAME=AccName class="readonly" MAXLENGTH=20 > </TD>
        </TR>
        <TR class=common>
          <TD  class= title>  保险期间 </TD>
          <TD  class= input> <Input class= readonly name=InsuYear > </TD>         
          <TD  class= title> 保险期间单位 </TD>
          <TD  class= input>  <Input class="code" name=InsuYearFlag ondblclick="return showCodeList('PeriodUnit1',[this]);" onkeyup="return showCodeListKey('PeriodUnit1',[this]);"> </TD>
          <TD  class= title>  保险责任终止日期 </TD>
          <TD  class= input>  <Input class="readonly" name=EndDate > </TD>
        </TR>
        <TR class= common>
          <TD  class= title> 保费  </TD>
          <TD  class= input>  <Input class= readonly name=Prem >  </TD>
          <TD  class= title>  保额 </TD>
          <TD  class= input>  <Input class= readonly name=Amnt > </TD>
          <TD  class= title>  份数  </TD>
          <TD  class= input>  <Input class= readonly name=Mult > </TD>
        </TR>
        <TR class= common>
          <TD class= title> 累计保费 </TD>
          <TD  class= input> <Input class="readonly" name=SumPrem >  </TD>
          <TD  class= title> 余额 </TD>
          <TD  class= input> <Input class= readonly name=LeavingMoney >   </TD>
          <TD  class= title> 红利领取方式</TD>
          <TD  class= input> <Input class="code" name=BonusGetMode ondblclick="return showCodeList('livegetmode',[this]);" onkeyup="return showCodeListKey('livegetmode',[this]);"> </TD>
        </TR>
        <TR class= common>
		  <TD  class= title> 生存金领取方式 </TD>
          <TD  class= input><Input class="code" name=LiveGetMode verify="生存|NOTNULL" CodeData="0|^1|累积生息^2|领取现金^3|抵缴保费^4|其他^5|增额交清" ondblClick="showCodeListEx('SC_1',[this],[1,2,3,4,9]);" onkeyup="showCodeListKeyEx('SC_1',[this],[1,2,3,4,9]);"></TD>
          <TD  class= title>  利差返还方式  </TD>
          <TD  class= input> <Input class=code name=InterestDifFlag ondblclick="return showCodeList('InterestDifFlag',[this]);" onkeyup="return showCodeListKey('InterestDifFlag',[this]);"> </TD>
          <TD class= title> 年金领取方式</TD>
          <TD class= input> <Input class= "code" name=GetDutyKind verify="给付方法|NOTNULL" CodeData="0|^1|一次领取型|^2|年领定额型|^3|月领定额型|^4|年领十年固定定额型|^5|月领十年固定定额型|^6|年领算术增额型|^7|月领算术增额型|^8|年领几何增额型|^9|月领几何增额型" ondblClick="showCodeListEx('GetIntv212401',[this],[0]);" onkeyup="showCodeListKeyEx('GetIntv212401',[this],[0]);">  </TD>        
        </TR> 
        <TR class = common>
          <TD  class= title>年金起领日期 </TD>
          <TD  class= input><Input class= readonly name=GetYear > </TD>	
          <TD  class= title>年金起领日期标志 </TD>
          <TD  class= input> <Input class="code" name=GetYearFlag ondblclick="return showCodeList('PeriodUnit',[this]);" onkeyup="return showCodeListKey('PeriodUnit',[this]);"> </TD>  
		  <TD  class= title>年金起领日期参照 </TD>
          <TD  class= input> <Input class="code" name=GetStartType CodeData="0|^S|起保日期对应日|^B|出生日期对应日" ondblClick="showCodeListEx('StartDateCalRef212401',[this],[0]);" onkeyup="showCodeListKeyEx('StartDateCalRef212401',[this],[0]);" > </TD>  
         </TR>
        <TR class= common>
		  <TD  class= title> 自动垫交标志 </TD>
          <TD  class= input> <Input class="code" name=AutoPayFlag verify="垫交标志|NOTNULL" CodeData="0|^0|正常^1|垫交" ondblClick="showCodeListEx('AutoPayFlag_1',[this],[0,1]);" onkeyup="showCodeListKeyEx('AutoPayFlag_1',[this],[0,1]);"> </TD>        
          <!--TD  class= title> 保单借款标志? </TD>
          <TD  class= input> <Input class="readonly" name=ConsignNoflag> </TD-->	
          <TD  class= title> 减额交清标志  </TD>
          <TD  class= input>  <Input class="code" name=SubFlag verify="减额|NOTNULL" CodeData="0|^0|正常^1|减额" ondblClick="showCodeListEx('JE_1',[this],[0,1]);" onkeyup="showCodeListKeyEx('JE_1',[this],[0,1]);">  </TD>
         <TD  class= title>  体检标志 </TD>
          <TD  class= input> <Input class="code" name=HealthCheckFlag verify="体检|NOTNULL" CodeData="0|^0|不体检^1|体检" ondblClick="showCodeListEx('TJ_1',[this],[0,1]);" onkeyup="showCodeListKeyEx('TJ_1',[this],[0,1]);"> </TD>                          
        </TR>
        <TR class= common>
          <TD  class= title>  保单打印次数 </TD>
		  <TD  class= input>  <Input class="readonly" name=PrintCount >  </TD>
          <TD  class= title>  保单补打次数 </TD>
		  <TD  class= input>  <Input class="readonly" name=LostTimes >  </TD>		  
        </TR>   
      </table>    
    </Div>
    <!-- 被保人信息部分 -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured1);">
    		</td>
    		<td class= titleImg>
    			 被保人信息（客户号：<Input class="readonly" readonly name=CustomerNo > ）
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCInsured1" style= "display: ''">
      <table  class= common>
        <TR  class= common>        
          <TD  class= title> 姓名 </TD>
          <TD  class= input> <Input class="readonly" readonly name=Name verify="被保人姓名|notnull&len<=20" >  </TD>
          <TD  class= title>  性别  </TD>
          <TD  class= input>  <Input class="code" readonly name=Sex verify="被保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);"> </TD>
          <TD  class= title> 出生日期 </TD>
          <TD  class= input>  <input class="readonly" readonly name="Birthday" verify="被保人出生日期|notnull&date" > </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>  投保年龄  </TD>
          <TD  class= input> <input class="readonly" readonly name="Age" >  </TD>
          <TD  class= title>   证件类型  </TD>
          <TD  class= input>  <Input class="code" readonly name="IDType" verify="被保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">  </TD>
          <TD  class= title>  证件号码 </TD>
          <TD  class= input>  <Input class="readonly" readonly name="IDNo" verify="被保人证件号码|len<=20" >  </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <input class="code" readonly name="NativePlace" verify="被保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">
          </TD>
          <TD  class= title>
            户口所在地
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="RgtAddress" verify="被保人户口所在地|len<=80" >
          </TD>
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class="code"  name="Marriage" verify="被保人婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);">
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            民族
          </TD>
          <TD  class= input>
          <input class="code" readonly name="Nationality" verify="被保人民族|code:Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">
          </TD>
          <TD  class= title>
            学历
          </TD>
          <TD  class= input>
            <Input class="code"  name="Degree" verify="被保人学历|code:Degree" ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">
          </TD>
          <TD  class= title>
            是否吸烟
          </TD>
          <TD  class= input>
            <Input class="code" readonly name="SmokeFlag" verify="被保人是否吸烟|code:YesNo" ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class="readonly3" readonly name="PostalAddress" verify="被保人联系地址|len<=80" >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="ZipCode" verify="被保人邮政编码|zipcode" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            家庭电话
          </TD>
          <TD  class= input>
          <input class="readonly" readonly name="Phone" verify="被保人家庭电话|len<=18" >
          </TD>
          <TD  class= title>
            移动电话
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="Mobile" verify="被保人移动电话|len<=15" >
          </TD>
          <TD  class= title>
            电子邮箱
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="EMail" verify="被保人电子邮箱|len<=20" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            工作单位
          </TD>
          <TD  class= input colspan=3 >
            <Input class="readonly3" readonly name="GrpName" verify="被保人工作单位|len<=60" >
          </TD>
          <TD  class= title>
            单位电话
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="GrpPhone" verify="被保人单位电话|len<=18" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class="readonly3" readonly name="GrpAddress" verify="被保人单位地址|len<=80" >
          </TD>
          <TD  class= title>
            单位邮政编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="GrpZipCode" verify="被保人单位邮政编码|zipcode" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            职业（工种）
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="WorkType" verify="被保人职业（工种）|len<=10" >
          </TD>
          <TD  class= title>
            兼职（工种）
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="PluralityType" verify="被保人兼职（工种）|len<=10" >
          </TD>
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input>
            <Input class="code"  name="OccupationType" verify="被保人职业类别|notnull&code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            职业代码
          </TD>
          <TD  class= input>
            <Input class="code" readonly name="OccupationCode" verify="被保人职业代码|code:OccupationCode" ondblclick="return showCodeList('OccupationCode',[this]);" onkeyup="return showCodeListKey('OccupationCode',[this]);">
          </TD>
          <TD  class= title>
            联系电话2
          </TD>
          <TD  class= input>
          <input class="readonly" readonly name="Phone2" verify="联系电话2|len<=18" >
          </TD>
        </TR>
      </table>
    </Div>    
    <!-- 隐藏信息 -->
    <Div  id= "divLCPol01" style= "display: ''">
          <TD  class= title>
            健康状况
          </TD>
          <TD  class= input>
            <Input class="readonly" name=Health >
          </TD>
    </Div> 
    
    <!-- 投保人信息部分 -->
    <table>
    	<tr>
        	<td>
    	  		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCAppntInd1);">
    		</td>
    		<td class= titleImg>
    			 投保人信息（客户号：<Input class="readonly" readonly  name=AppntCustomerNo > ）
 			 
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCAppntInd1" style= "display: ''">
      <table  class= common>
        <TR  class= common>        
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntName verify="投保人姓名|len<=20" >
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class="code" name=AppntSex verify="被保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
          </TD>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
          <input class="readonly" readonly name="AppntBirthday" verify="投保人出生日期|date" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            年龄
          </TD>
          <TD  class= input>
          <input class="readonly" readonly name="AppntAge" >
          </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class="code" readonly name="AppntIDType" verify="被保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="AppntIDNo" verify="投保人证件号码|len<=20" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <input class="code" readonly name="AppntNativePlace" verify="被保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">
          </TD>
          <TD  class= title>
            户口所在地
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="AppntRgtAddress" verify="投保人户口所在地|len<=80" >
          </TD>
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class="code" readonly name="AppntMarriage" verify="被保人婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);">
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            民族
          </TD>
          <TD  class= input>
          <input class="code" readonly name="AppntNationality" verify="被保人民族|code:Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">
          </TD>
          <TD  class= title>
            学历
          </TD>
          <TD  class= input>
            <Input class="code" readonly name="AppntDegree" verify="被保人学历|code:Degree" ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">
          </TD>
          <TD  class= title>
            与被保险人关系
          </TD>
          <TD  class= input>
            <Input class="code"  name="AppntRelationToInsured"  verify="被保人关系|code:Relation" ondblclick="return showCodeList('Relation',[this]);" onkeyup="return showCodeListKey('Relation',[this]);">
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class="readonly3" readonly name="AppntPostalAddress" verify="投保人联系地址|len<=80" >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="AppntZipCode" verify="投保人邮政编码|zipcode" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            家庭电话
          </TD>
          <TD  class= input>
          <input class="readonly" readonly name="AppntPhone" verify="投保人家庭电话|len<=18" >
          </TD>
          <TD  class= title>
            移动电话
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="AppntMobile" verify="投保人移动电话|len<=15" >
          </TD>
          <TD  class= title>
            电子邮箱
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="AppntEMail" verify="投保人电子邮箱|len<=20" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            工作单位
          </TD>
          <TD  class= input colspan=3 >
            <Input class="readonly3" readonly name="AppntGrpName" verify="投保人工作单位|len<=60" >
          </TD>
          <TD  class= title>
            单位电话
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="AppntGrpPhone" verify="投保人单位电话|len<=18" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class="readonly3" readonly name="AppntGrpAddress" verify="投保人单位地址|len<=80" >
          </TD>
          <TD  class= title>
            单位邮政编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="AppntGrpZipCode" verify="投保人单位邮政编码|zipcode" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            职业（工种）
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="AppntWorkType" verify="投保人职业（工种）|len<=10" >
          </TD>
          <TD  class= title>
            兼职（工种）
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name="AppntPluralityType" verify="投保人兼职（工种）|len<=10" >
          </TD>
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input>
            <Input class="code" readonly name="AppntOccupationType" verify="被保人职业类别|notnull&code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            职业代码
          </TD>
          <TD  class= input>
            <Input class="code" readonly name="AppntOccupationCode" verify="被保人职业代码|code:OccupationCode" ondblclick="return showCodeList('OccupationCode',[this]);" onkeyup="return showCodeListKey('OccupationCode',[this]);">
          </TD>
          
          <TD  class= title>
            是否吸烟
          </TD>
          <TD  class= input>
            <Input class="code" readonly name="AppntSmokeFlag" verify="被保人是否吸烟|code:YesNo" ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
          </TD>
          
           <TD  class= title>
            联系电话2
          </TD>
          <TD  class= input>
          <input class="readonly" readonly name="Phone2" verify="联系电话2|len<=18" >
          </TD>

        </TR>
      </table>   
    </Div>
    
    <!-- 集体投保人信息部分 修改所有字段名称-->
    <Div  id= "divLCAppntGrp0" style= "display: none ">
    <table>
    	<tr>
        	<td>
    	  		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCAppntGrp1);">
    		</td>
    		<td class= titleImg>
    			 投保人信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCAppntGrp1" style= "display: ''">
      <table  class= common>
        <TR class= common>
          <TD  class= title>
            投保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ColGrpNo >
          </TD>
          <TD  class= title>
            投保人名称
          </TD>
          <TD  class= input>
            <Input class= readonly name=ColGrpName >
          </TD>
          <TD  class= title>
            联系人
          </TD>
          <TD  class= input>
            <input class= readonly name=ColLinkMan >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            与被保人关系
          </TD>
          <TD  class= input>
            <Input class="readonly" name=ColGrpRelation >
          </TD>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= readonly name=ColGrpPhone >
          </TD>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input class= readonly name=ColGrpFax >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class= readonly name=ColGrpEMail >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= readonly name=ColGrpZipCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= readonly name=ColGrpAddress >
          </TD>
        </TR>
      </table>
    </Div>
    </Div>

      </Div>
      <!-- 隐藏 -->
    <Div  id= "divLCKind0" style= "display: ''">
      <table  class= common>


	      
    <!-- 连带被保人信息部分（列表） -->
	<Div  id= "divLCInsured0" style= "display: ''">
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured2);">
    		</td>
    		<td class= titleImg>
    			 连带被保人信息
    		</td>
    	</tr>
      </table>
	  <Div  id= "divLCInsured2" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanSubInsuredGrid" >
					</span> 
				</td>
			</tr>
		</table>
	  </div>
	</div>
	
    <!-- 受益人信息部分（列表） -->
	<Div  id= "divLCSubInsured0" style= "display: ''">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCBnf1);">
    		</td>
    		<td class= titleImg>
    			 受益人信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCBnf1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanBnfGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
	</div>
    <!-- 告知信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
    		</td>
    		<td class= titleImg>
    			 告知信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCImpart1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanImpartGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
    <!-- 特约信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCSpec1);">
    		</td>
    		<td class= titleImg>
    			 特约信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCSpec1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanSpecGrid">
					</span> 
				</td>
			</tr>
		</table>
	</div>

    <!--可以选择的责任部分，该部分始终隐藏-->
	<Div  id= "divChooseDuty0" style= "display: ''">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDutyGrid);">
    		</td>
    		<td class= titleImg>
    			 可选责任信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divDutyGrid" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanDutyGrid" >
					</span> 
				</td>
			</tr>
		</table>
		<!--确定是否需要责任信息-->
	</div>
	</div>
	   <table  class= common>
        <TR  class= common>
          <TD  class= title>
            录入人员
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=Operator >
          </TD>
          <TD  class= title>
            复核人员
          </TD>
          <TD  class= input>
            <Input class= readonly readonly name=ApproveCode >
          </TD>
          <TD  class= title>
            核保人员
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=UWCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            录入日期
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=MakeDate >
          </TD>
          <TD  class= title>
            复核日期
          </TD>
          <TD  class= input>
            <Input class= readonly readonly name=ApproveDate >
          </TD>
          <TD  class= title>
            核保日期
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=UWDate >
          </TD>
        </TR>
    	</table>
		<input type=hidden id="inpNeedDutyGrid" name="inpNeedDutyGrid" value="0">
		<input type=hidden id="fmAction" name="fmAction">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
<script>
function returnParent()
{
	var isDia=0;
	var haveMenu=0;
	var callerWindowObj;
	try
	{
		callerWindowObj = dialogArguments; 
		isDia=1;
	}
	catch(ex1)
	{ isDia=0;}
	try
	{
		if(isDia==0)//如果是打开一个新的窗口，则执行下面的代码
		{
			top.opener.parent.document.body.innerHTML=window.document.body.innerHTML;
			top.opener.parent.showCodeName();
		}
		else//如果打开一个模态对话框，则调用下面的代码
		{
			callerWindowObj.document.body.innerHTML=window.document.body.innerHTML;
			haveMenu = 1;
			callerWindowObj.parent.frames("fraMenu").Ldd = 0;
			callerWindowObj.parent.frames("fraMenu").Go();
			callerWindowObj.parent.showCodeName();
		}
	}
	catch(ex)
	{
		if( haveMenu != 1 ) 
		{
			alert("Riskxxx.jsp:发生错误："+ex.name);
		}
	}
	top.close();
}  

returnParent();
</script>


