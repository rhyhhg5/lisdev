 //               该文件中包含客户端需要处理的函数和事件
var mOperate = "";
var showInfo1;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 

var arrResult;

//提交，保存按钮对应操作
function submitForm()
{
}



/*********************************************************************
 *  返回上一页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */  
function returnparent()
{    	
		
  	window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + ContNo +"&IsCancelPolFlag=0");  
}

/*********************************************************************
 *  代码选择后触发时间
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
    try	
    {
   	    if(cCodeName == "Relation")
   	    {
	          if(fm.all('RelationToAppnt').value=="00")
	          {		
	          	fm.all('SamePersonFlag').checked=true;
	          	displayissameperson();
	          	isSamePerson();
		        }	
   	    }
        //如果是无名单
        if( cCodeName == "PolTypeFlag")
        {
            if (Field.value!='0')
            {
                fm.all('InsuredPeoples').readOnly=false;
                fm.all('InsuredAppAge').readOnly=false;
            }
            else
            {
                fm.all('InsuredPeoples').readOnly=true;
                fm.all('InsuredAppAge').readOnly=true;
            }
        }     
         if( cCodeName == "ImpartCode")
         {
	   
         }  
         if( cCodeName == "SequenceNo")
         {
	   if(Field.value=="1"&&fm.SamePersonFlag.checked==false)
	   {
	   	     emptyInsured();
		     param="121";
		     fm.pagename.value="121"; 
                     fm.InsuredSequencename.value="第一被保险人资料";	
                     fm.RelationToMainInsured.value="00";	   	
	   }
	   if(Field.value=="2"&&fm.SamePersonFlag.checked==false)
	   {
	   	if(InsuredGrid.mulLineCount==0)
	   	{	   		
	   		alert("请先添加第一被保人");
	   		fm.SequenceNo.value="1";	   		
	   		return false;
	   	}	   	
	   	     emptyInsured();
                     noneedhome();   	     
		     param="122";
		     fm.pagename.value="122"; 
                     fm.InsuredSequencename.value="第二被保险人资料";		   	
	   }
	   if(Field.value=="3"&&fm.SamePersonFlag.checked==false)
	   {
	   	if(InsuredGrid.mulLineCount==0)
	   	{	   		
	   		alert("请先添加第一被保人");
	   		Field.value="1";
	   		return false;
	   	}
	   	if(InsuredGrid.mulLineCount==1)
	   	{	   		
	   		alert("请先添加第二被保人");
	   		Field.value="1";
	   		return false;
	   	}	   		   	
	   	     emptyInsured();
                     noneedhome();	   	     
		     param="123";
		     fm.pagename.value="123"; 
                     fm.InsuredSequencename.value="第三被保险人资料";	   	
	   }	
           if (scantype== "scan") 
           {  
           setFocus(); 
           }	      	   	
         } 
         
    if(cCodeName=="OccupationCode")
    {
    	//alert();
    	var t = Field.value;
    	var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+t+"'";
    	//alert(strSql);
    	var arr = easyExecSql(strSql);
    	if( arr )
    	{
    		fm.OccupationType.value = arr[0][0];
    	}
    }       
    }
    catch(ex) {}
      
}


/*********************************************************************
 *  获得个单合同的被保人信息
 *  返回值：  无
 *********************************************************************
 */
function getProposalInsuredInfo()
{  
    var tContNo=fm.all("ContNo").value;
    arrResult=easyExecSql("select * from LCInsured where ContNo='"+tContNo+"'",1,0);
    if(arrResult==null)
    {
      //alert("未得到被投保人信息");
        return;
    }
    else
    {
        DisplayInsured();//该合同下的被投保人信息
        var tCustomerNo = arrResult[0][2];		// 得到投保人客户号
        var tAddressNo = arrResult[0][10]; 		// 得到投保人地址号
        arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo='"+tCustomerNo+"'",1,0);
        if(arrResult==null)
        {
            //alert("未得到用户信息");
            //return;
        }
        else
        {
            //displayAppnt();       //显示投保人详细内容
            emptyUndefined();
            fm.AddressNo.value=tAddressNo;
            getdetailaddress();//显示投保人地址详细内容
        } 
    } 
    getInsuredPolInfo();
    getImpartInfo();
    getImpartDetailInfo();
}

/*********************************************************************
 *  MulLine的RadioBox点击事件，获得被保人详细信息，填入被保人信息表
 *  返回值：  无
 *********************************************************************
 */
function getInsuredDetail(parm1,parm2)
{
    fm.SequenceNo.value=fm.all(parm1).all('InsuredGrid6').value;
    if(fm.SequenceNo.value=="1")
    {
    	fm.pagename.value="121"; 
    	fm.InsuredSequencename.value="第一被保险人资料";
    }	
    if(fm.SequenceNo.value=="2")
    {
    	fm.pagename.value="121";     	
    	fm.InsuredSequencename.value="第二被保险人资料";
    } 
    if(fm.SequenceNo.value=="3")
    {
    	fm.pagename.value="121";     	
    	fm.InsuredSequencename.value="第三被保险人资料";
    }
    if (scantype== "scan") 
    {  
    setFocus(); 
    }           
    var InsuredNo=fm.all(parm1).all('InsuredGrid1').value;
    var ContNo = fm.ContNo.value;
    //被保人详细信息
    var strSQL ="select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo='"+InsuredNo+"'";
    arrResult=easyExecSql(strSQL);  
    if(arrResult!=null)
    {
        displayAppnt();
    }
    strSQL ="select * from LCInsured where ContNo = '"+ContNo+"' and InsuredNo='"+InsuredNo+"'";
    arrResult=easyExecSql(strSQL);
    if(arrResult!=null)
    {
        DisplayInsured();
    }
    var tAddressNo = arrResult[0][10]; 		// 得到被保人地址号 
    fm.AddressNo.value=tAddressNo;   
    getdetailaddress();  	  
    getInsuredPolInfo();
    getImpartInfo();
    getImpartDetailInfo();
}
/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayCustomer()
{
	try{fm.all('Nationality').value= arrResult[0][8]; }catch(ex){}; 
	
}
/*********************************************************************
 *  把查询返回的客户地址数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayAddress()
{	
	try{fm.all('Phone').value= arrResult[0][4]; }catch(ex){}; 
	try{fm.all('Mobile').value= arrResult[0][14]; }catch(ex){}; 
	try{fm.all('EMail').value= arrResult[0][16]; }catch(ex){}; 
  //try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){}; 
	try{fm.all('GrpPhone').value= arrResult[0][12]; }catch(ex){}; 
	try{fm.all('CompanyAddress').value= arrResult[0][10]; }catch(ex){}; 
	try{fm.all('GrpZipCode').value= arrResult[0][11]; }catch(ex){}; 
}
/*********************************************************************
 *  显示被保人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayInsured()
{
    try{fm.all('GrpContNo').value=arrResult[0][0];}catch(ex){};            
    try{fm.all('ContNo').value=arrResult[0][1];}catch(ex){};               
    try{fm.all('InsuredNo').value=arrResult[0][2];}catch(ex){};            
    try{fm.all('PrtNo').value=arrResult[0][3];}catch(ex){};                
    try{fm.all('AppntNo').value=arrResult[0][4];}catch(ex){};              
    try{fm.all('ManageCom').value=arrResult[0][5];}catch(ex){};            
    try{fm.all('ExecuteCom').value=arrResult[0][6];}catch(ex){};           
    try{fm.all('FamilyID').value=arrResult[0][7];}catch(ex){};             
    try{fm.all('RelationToMainInsured').value=arrResult[0][8];}catch(ex){};
    try{fm.all('RelationToAppnt').value=arrResult[0][9];}catch(ex){};      
    try{fm.all('AddressNo').value=arrResult[0][10];}catch(ex){};           
    try{fm.all('SequenceNo').value=arrResult[0][11];}catch(ex){};          
    try{fm.all('Name').value=arrResult[0][12];}catch(ex){};                
    try{fm.all('Sex').value=arrResult[0][13];}catch(ex){};                 
    try{fm.all('Birthday').value=arrResult[0][14];}catch(ex){};            
    try{fm.all('IDType').value=arrResult[0][15];}catch(ex){};              
    try{fm.all('IDNo').value=arrResult[0][16];}catch(ex){};
    try{fm.all('NativePlace').value=arrResult[0][17];}catch(ex){};
    try{fm.all('Nationality').value=arrResult[0][18];}catch(ex){};                  
    try{fm.all('RgtAddress').value=arrResult[0][19];}catch(ex){};          
    try{fm.all('Marriage').value=arrResult[0][20];}catch(ex){};            
    try{fm.all('MarriageDate').value=arrResult[0][21];}catch(ex){};        
    try{fm.all('Health').value=arrResult[0][22];}catch(ex){};              
    try{fm.all('Stature').value=arrResult[0][23];}catch(ex){};             
    try{fm.all('Avoirdupois').value=arrResult[0][24];}catch(ex){};         
    try{fm.all('Degree').value=arrResult[0][25];}catch(ex){};              
    try{fm.all('CreditGrade').value=arrResult[0][26];}catch(ex){};         
    try{fm.all('BankCode').value=arrResult[0][27];}catch(ex){};            
    try{fm.all('BankAccNo').value=arrResult[0][28];}catch(ex){};           
    try{fm.all('AccName').value=arrResult[0][29];}catch(ex){};             
    try{fm.all('JoinCompanyDate').value=arrResult[0][30];}catch(ex){};     
    try{fm.all('StartWorkDate').value=arrResult[0][31];}catch(ex){};       
    try{fm.all('Position').value=arrResult[0][32];}catch(ex){};            
    try{fm.all('Salary').value=arrResult[0][33];}catch(ex){};              
    try{fm.all('OccupationType').value=arrResult[0][34];}catch(ex){};      
    try{fm.all('OccupationCode').value=arrResult[0][35];}catch(ex){};
  //  try {fm.all('OccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][35]);} catch(ex) { };             
    try{fm.all('WorkType').value=arrResult[0][36];}catch(ex){};            
    try{fm.all('PluralityType').value=arrResult[0][37];}catch(ex){};       
    try{fm.all('SmokeFlag').value=arrResult[0][38];}catch(ex){}; 
    try{fm.all('ContPlanCode').value=arrResult[0][39];}catch(ex){};          
    try{fm.all('Operator').value=arrResult[0][40];}catch(ex){};            
    try{fm.all('MakeDate').value=arrResult[0][41];}catch(ex){};            
    try{fm.all('MakeTime').value=arrResult[0][42];}catch(ex){};            
    try{fm.all('ModifyDate').value=arrResult[0][43];}catch(ex){};          
    try{fm.all('ModifyTime').value=arrResult[0][44];}catch(ex){};   
}

function displayissameperson()
{
    try{fm.all('InsuredNo').value= mSwitch.getVar( "AppntNo" ); }catch(ex){};         
    try{fm.all('Name').value= mSwitch.getVar( "AppntName" ); }catch(ex){};          
    try{fm.all('Sex').value= mSwitch.getVar( "AppntSex" ); }catch(ex){};           
    try{fm.all('Birthday').value= mSwitch.getVar( "AppntBirthday" ); }catch(ex){};        
    try{fm.all('IDType').value= mSwitch.getVar( "AppntIDType" ); }catch(ex){};        
    try{fm.all('IDNo').value= mSwitch.getVar( "AppntIDNo" ); }catch(ex){};          
    try{fm.all('Password').value= mSwitch.getVar( "AppntPassword" ); }catch(ex){};      
    try{fm.all('NativePlace').value= mSwitch.getVar( "AppntNativePlace" ); }catch(ex){};   
    try{fm.all('Nationality').value= mSwitch.getVar( "AppntNationality" ); }catch(ex){}; 
    try{fm.all('AddressNo').value= mSwitch.getVar( "AddressNo" ); }catch(ex){};       
    try{fm.all('RgtAddress').value= mSwitch.getVar( "AppntRgtAddress" ); }catch(ex){};    
    try{fm.all('Marriage').value= mSwitch.getVar( "AppntMarriage" );}catch(ex){};      
    try{fm.all('MarriageDate').value= mSwitch.getVar( "AppntMarriageDate" );}catch(ex){};  
    try{fm.all('Health').value= mSwitch.getVar( "AppntHealth" );}catch(ex){};        
    try{fm.all('Stature').value= mSwitch.getVar( "AppntStature" );}catch(ex){};       
    try{fm.all('Avoirdupois').value= mSwitch.getVar( "AppntAvoirdupois" );}catch(ex){};   
    try{fm.all('Degree').value= mSwitch.getVar( "AppntDegree" );}catch(ex){};        
    try{fm.all('CreditGrade').value= mSwitch.getVar( "AppntDegreeCreditGrade" );}catch(ex){};   
    try{fm.all('OthIDType').value= mSwitch.getVar( "AppntOthIDType" );}catch(ex){};     
    try{fm.all('OthIDNo').value= mSwitch.getVar( "AppntOthIDNo" );}catch(ex){};       
    try{fm.all('ICNo').value= mSwitch.getVar( "AppntICNo" );}catch(ex){};          
    try{fm.all('GrpNo').value= mSwitch.getVar( "AppntGrpNo" );}catch(ex){};         
    try{fm.all( 'JoinCompanyDate' ).value = mSwitch.getVar( "JoinCompanyDate" ); if(fm.all( 'JoinCompanyDate' ).value=="false"){fm.all( 'JoinCompanyDate' ).value="";} } catch(ex) { };
    try{fm.all('StartWorkDate').value= mSwitch.getVar( "AppntStartWorkDate" );}catch(ex){}; 
    try{fm.all('Position').value= mSwitch.getVar( "AppntPosition" );}catch(ex){};      
    try{fm.all( 'Position' ).value = mSwitch.getVar( "Position" ); if(fm.all( 'Position' ).value=="false"){fm.all( 'Position' ).value="";} } catch(ex) { }; 
    try{fm.all('Salary').value= mSwitch.getVar( "AppntSalary" );}catch(ex){};        
    try{fm.all( 'Salary' ).value = mSwitch.getVar( "Salary" ); if(fm.all( 'Salary' ).value=="false"){fm.all( 'Salary' ).value="";} } catch(ex) { }; 
    try{fm.all('OccupationType').value= mSwitch.getVar( "AppntOccupationType" );}catch(ex){};
    try{fm.all('OccupationCode').value= mSwitch.getVar( "AppntOccupationCode" );}catch(ex){};
    try{fm.all('WorkType').value= mSwitch.getVar( "AppntWorkType" );}catch(ex){};      
    try{fm.all('PluralityType').value= mSwitch.getVar( "AppntPluralityType" );}catch(ex){}; 
    try{fm.all('DeathDate').value= mSwitch.getVar( "AppntDeathDate" );}catch(ex){};     
    try{fm.all('SmokeFlag').value= mSwitch.getVar( "AppntSmokeFlag" );}catch(ex){};     
    try{fm.all('BlacklistFlag').value= mSwitch.getVar( "AppntBlacklistFlag" );}catch(ex){}; 
    try{fm.all('Proterty').value= mSwitch.getVar( "AppntProterty" );}catch(ex){};      
    try{fm.all('Remark').value= mSwitch.getVar( "AppntRemark" );}catch(ex){};        
    try{fm.all('State').value= mSwitch.getVar( "AppntState" );}catch(ex){};         
    try{fm.all('Operator').value= mSwitch.getVar( "AppntOperator" );}catch(ex){};      
    try{fm.all('MakeDate').value= mSwitch.getVar( "AppntMakeDate" );}catch(ex){};      
    try{fm.all('MakeTime').value= mSwitch.getVar( "AppntMakeTime" );}catch(ex){};      
    try{fm.all('ModifyDate').value= mSwitch.getVar( "AppntModifyDate" );}catch(ex){};    
    try{fm.all('ModifyTime').value= mSwitch.getVar( "AppntModifyTime" );}catch(ex){};      
    try{fm.all('ZipCode').value= mSwitch.getVar( "AppntZipCode" );}catch(ex){}; 
    try{fm.all('Phone').value= mSwitch.getVar( "AppntPhone" );}catch(ex){}; 
    try{fm.all('Fax').value= mSwitch.getVar( "AppntFax" );}catch(ex){};     
    try{fm.all('Mobile').value= mSwitch.getVar( "AppntMobile" );}catch(ex){}; 
    try{fm.all('EMail').value= mSwitch.getVar( "AppntEMail" );}catch(ex){}; 
    try{fm.all('GrpName').value= mSwitch.getVar( "AppntGrpName" );}catch(ex){}; 
    try{fm.all('GrpPhone').value= mSwitch.getVar( "AppntGrpPhone" );}catch(ex){}; 
    try{fm.all('GrpAddress').value= mSwitch.getVar( "CompanyAddress" );}catch(ex){}; 
    try{fm.all('GrpZipCode').value= mSwitch.getVar( "AppntGrpZipCode" );}catch(ex){};     
    try{fm.all('GrpFax').value= mSwitch.getVar( "AppntGrpFax" );}catch(ex){};     
    try{fm.all('HomeAddress').value= mSwitch.getVar( "AppntHomeAddress" );}catch(ex){}; 
    try{fm.all('HomePhone').value= mSwitch.getVar( "AppntHomePhone" );}catch(ex){}; 
    try{fm.all('HomeZipCode').value= mSwitch.getVar( "AppntHomeZipCode" );}catch(ex){}; 
    try{fm.all('HomeFax').value= mSwitch.getVar( "AppntHomeFax" );}catch(ex){};        	
}

/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function getImpartInfo()
{
    initImpartGrid();	
     var InsuredNo=cInsuredNo;
     var tContNo=ContNo;
    //告知信息初始化
    if(InsuredNo!=null&&InsuredNo!="")
    {
        var strSQL ="select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LCCustomerImpart where CustomerNo='"+InsuredNo+"' and ProposalContNo='"+tContNo+"' and CustomerNoType='I'";
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = ImpartGrid;    
        //保存SQL语句
        turnPage.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}

/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function getImpartDetailInfo()
{
    initImpartDetailGrid();	
     var InsuredNo=cInsuredNo;
    var tContNo=ContNo;
    //告知信息初始化
    if(InsuredNo!=null&&InsuredNo!="")
    {
        var strSQL ="select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where CustomerNo='"+InsuredNo+"' and ContNo='"+tContNo+"' and CustomerNoType='I'";
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = ImpartDetailGrid;    
        //保存SQL语句
        turnPage.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}

/*********************************************************************
 *  获得被保人险种信息，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function getInsuredPolInfo()
{
    initPolGrid();	
    var InsuredNo=cInsuredNo;
    var tContNo=ContNo;
    //险种信息初始化
    if(InsuredNo!=null&&InsuredNo!="")
    {
        var strSQL ="select PolNo,RiskCode,Prem,Amnt,PolNo,RiskSeqNo from LCPol where InsuredNo='"+InsuredNo+"' and ContNo='"+tContNo+"'";
     
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = PolGrid;    
        //保存SQL语句
        turnPage.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}
/*********************************************************************
 *  MulLine的RadioBox点击事件，获得被保人险种详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function getPolDetail(parm1,parm2)
{
    var PolNo=fm.all(parm1).all('PolGrid1').value
    try{mSwitch.deleteVar('PolNo')}catch(ex){};
    try{mSwitch.addVar('PolNo','',PolNo);}catch(ex){};
    fm.SelPolNo.value=PolNo;
}
/*********************************************************************
 *  根据家庭单类型，隐藏界面控件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */    
function choosetype(){
	if(fm.FamilyType.value=="1")
	divTempFeeInput.style.display="";
	if(fm.FamilyType.value=="0")
	divTempFeeInput.style.display="none";
}
/*********************************************************************
 *  校验被保险人与主被保险人关系
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */    
function checkself()
{
	if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value=="")
	{		  
	    fm.RelationToMainInsured.value="00";
	    return true;	   
    }
	else if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value!="00")
	{	
	    alert("个人单中'与第一被保险人关系'只能是'本人'");
	    fm.RelationToMainInsured.value="00";
	    return false;	   
    }
	else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value==""&&InsuredGrid.mulLineCount==0)
	{	  
	    fm.RelationToMainInsured.value="00";
	    return true;
    }
    else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value!="00"&&InsuredGrid.mulLineCount==0)
    {
	    alert("家庭单中第一位被保险人的'与第一被保险人关系'只能是'本人'");
	    fm.RelationToMainInsured.value="00";
	    return false;
    }
    else 
        return true;
}

/*********************************************************************
 *  校验保险人
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */   
function checkrelation()
{
	if(LoadFlag==2||LoadFlag==7)
	{
        if (fm.all('ContNo').value != "") 
        {
            alert("团单的个单不能有多被保险人");
            return false;
        }
        else 
            return true;
    }
    else
    {
        if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="0") 
        {
            var strSQL="select * from LCInsured where contno='"+fm.all('ContNo').value +"'";
            turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
            if(turnPage.strQueryResult)
            {
                alert("个单不能有多被保险人");
                return false;
            }
            else
                return true;
        }
        else if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="1"&&InsuredGrid.mulLineCount>0&&fm.RelationToMainInsured.value=="00") 
        {
            alert("家庭单只能有一个第一被保险人");
            return false;
        }                 
        else if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="1"&&fm.RelationToAppnt.value=="00") 
        {
            var strSql="select * from LCInsured where contno='"+fm.all('ContNo').value +"' and RelationToAppnt='00' ";
            turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);
            if(turnPage.strQueryResult)
		    {
                alert("投保人已经是该合同号下的被保险人");
                return false;
            }
		    else
		        return true;
        }	
        else 
            return true;
    }
	//select count(*) from ldinsured
	
}
/*********************************************************************
 *  投保人与被保人相同选择框事件
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */   
function isSamePerson() 
{
    //对应未选同一人，又打钩的情况     
    if ( fm.SamePersonFlag.checked==true&&fm.RelationToAppnt.value!="00") 
    {
      fm.all('DivLCInsured').style.display = "none";
      divLCInsuredPerson.style.display = "none";
      divSalary.style.display = "none";   
      fm.SamePersonFlag.checked = true;
      fm.RelationToAppnt.value="00" 
      displayissameperson();      
    }
    //对应是同一人，又打钩的情况
    else if (fm.SamePersonFlag.checked == true) 
    {
      fm.all('DivLCInsured').style.display = "none";
      divLCInsuredPerson.style.display = "none";  
      divSalary.style.display = "none";    
      displayissameperson();     
    }
    //对应不选同一人的情况
    else if (fm.SamePersonFlag.checked == false) 
    {
    fm.all('DivLCInsured').style.display = "";   
    try{fm.all('Name').value=""; }catch(ex){};          
    try{fm.all('Sex').value= ""; }catch(ex){};           
    try{fm.all('Birthday').value= ""; }catch(ex){};        
    try{fm.all('IDType').value= "0"; }catch(ex){};        
    try{fm.all('IDNo').value= ""; }catch(ex){};          
    try{fm.all('Password').value= ""; }catch(ex){};      
    try{fm.all('NativePlace').value= ""; }catch(ex){};   
    try{fm.all('Nationality').value=""; }catch(ex){};   
    try{fm.all('RgtAddress').value= ""; }catch(ex){};    
    try{fm.all('Marriage').value= "";}catch(ex){};      
    try{fm.all('MarriageDate').value= "";}catch(ex){};  
    try{fm.all('Health').value= "";}catch(ex){};     
    try{fm.all('Stature').value= "";}catch(ex){};       
    try{fm.all('Avoirdupois').value= "";}catch(ex){};   
    try{fm.all('Degree').value= "";}catch(ex){};        
    try{fm.all('CreditGrade').value= "";}catch(ex){};   
    try{fm.all('OthIDType').value= "";}catch(ex){};     
    try{fm.all('OthIDNo').value= "";}catch(ex){};       
    try{fm.all('ICNo').value="";}catch(ex){};          
    try{fm.all('GrpNo').value= "";}catch(ex){};         
    try{fm.all('JoinCompanyDate').value= "";}catch(ex){}
    try{fm.all('StartWorkDate').value= "";}catch(ex){}; 
    try{fm.all('Position').value= "";}catch(ex){};      
    try{fm.all('Salary').value= "";}catch(ex){};        
    try{fm.all('OccupationType').value= "";}catch(ex){};
    try{fm.all('OccupationCode').value= "";}catch(ex){};
    try{fm.all('OccupationName').value= "";}catch(ex){};    
    try{fm.all('WorkType').value= "";}catch(ex){};      
    try{fm.all('PluralityType').value= "";}catch(ex){}; 
    try{fm.all('DeathDate').value= "";}catch(ex){};     
    try{fm.all('SmokeFlag').value= "";}catch(ex){};     
    try{fm.all('BlacklistFlag').value= "";}catch(ex){}; 
    try{fm.all('Proterty').value= "";}catch(ex){};      
    try{fm.all('Remark').value= "";}catch(ex){};        
    try{fm.all('State').value= "";}catch(ex){};         
    try{fm.all('Operator').value= "";}catch(ex){};      
    try{fm.all('MakeDate').value= "";}catch(ex){};      
    try{fm.all('MakeTime').value="";}catch(ex){};      
    try{fm.all('ModifyDate').value= "";}catch(ex){};    
    try{fm.all('ModifyTime').value= "";}catch(ex){};       
    try{fm.all('Phone').value= "";}catch(ex){}; 
    try{fm.all('Mobile').value= "";}catch(ex){}; 
    try{fm.all('EMail').value="";}catch(ex){}; 
    try{fm.all('GrpName').value= "";}catch(ex){}; 
    try{fm.all('GrpPhone').value= "";}catch(ex){}; 
    try{fm.all('GrpAddress').value="";}catch(ex){}; 
    try{fm.all('GrpZipCode').value= "";}catch(ex){};  
    try{fm.all('HomeAddress').value= ""; }catch(ex){}; 	
    try{fm.all('HomeZipCode').value= ""; }catch(ex){}; 	
    try{fm.all('HomePhone').value= ""; }catch(ex){}; 	
    try{fm.all('HomeFax').value= ""; }catch(ex){}; 	
    try{fm.all('GrpFax').value= ""; }catch(ex){}; 
    try{fm.all('Fax').value= ""; }catch(ex){};         
        
    }  
}
/*********************************************************************
 *  投保人客户号查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */                                                      
function queryInsuredNo() 
{    
	alert(tInsuredNo);
    if (tInsuredNo == "" ||tInsuredNo == null ) 
    {                    
    } 
    else 
    {                                                                   
        arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + tInsuredNo + "'", 1, 0);
        if (arrResult == null) 
        {                                                 
        } 
        else 
        {                                                                 
          displayAppnt(arrResult[0]);                                            
        }                                                                        
    }                                                                          
}                                                                            

/*********************************************************************
 *  查询返回后触发
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */        
function afterQuery( arrQueryResult ) 
{
  //alert("here:" + arrQueryResult + "\n" + mOperate);
    if( arrQueryResult != null ) 
    {
        arrResult = arrQueryResult;
        
        if( mOperate == 1 )	
        {		// 查询投保单	
            fm.all( 'ContNo' ).value = arrQueryResult[0][0];
            
            arrResult = easyExecSql("select ProposalContNo,PrtNo,ManageCom,SaleChnl,AgentCode,AgentGroup,AgentCode1,AgentCom,AgentType,Remark from LCCont where LCCont = '" + arrQueryResult[0][0] + "'", 1, 0);
            
            if (arrResult == null) 
            {
                alert("未查到投保单信息");
            } 
            else 
            {
                displayLCContPol(arrResult[0]);
            }
        }
        
        if( mOperate == 2 )	
        {		// 投保人信息
        	//arrResult = easyExecSql("select a.*,b.AddressNo,b.PostalAddress,b.ZipCode,b.HomePhone,b.Mobile,b.EMail,a.GrpNo,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode from LDPerson a,LCAddress b where 1=1 and a.CustomerNo=b.CustomerNo and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
        	arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
        	if (arrResult == null) 
        	{
        	    alert("未查到投保人信息");
        	} 
        	else 
        	{
        	    displayAppnt(arrResult[0]);
        	}
        }
    }
	
	mOperate = 0;		// 恢复初态
}
/*********************************************************************
 *  查询职业类别
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */        
function getdetailwork()
{
    var strSql = "select OccupationType from LDOccupation where OccupationCode='" + fm.OccupationCode.value+"'";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null) 
    {
        fm.OccupationType.value = arrResult[0][0];
    }
    else
    {  
        fm.OccupationType.value = '';
    }	
}  
/*获得个人单信息，写入页面控件
function getProposalInsuredInfo(){
  var ContNo = fm.ContNo.value;
  //被保人详细信息
  var strSQL ="select * from ldperson where CustomerNo in (select InsuredNo from LCInsured where ContNo='"+ContNo+"')";
  arrResult=easyExecSql(strSQL);  
  if(arrResult!=null){
  	DisplayCustomer();
  }
  
  strSQL ="select * from LCInsured where ContNo = '"+ContNo+"'";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null){
    DisplayInsured();
  }else{
    return;
  }

  var tAddressNo = arrResult[0][10]; 		// 得到被保人地址号 
  var InsuredNo=arrResult[0][2]; 
  var strSQL="select * from LCAddress where AddressNo='"+tAddressNo+"' and CustomerNo='"+InsuredNo+"'";
  arrResult=easyExecSql(strSQL);
    if(arrResult!=null){
  	DisplayAddress();
    }
     	
    getInsuredPolInfo();
    
}*/


/*********************************************************************
 *  把合同所有信息录入结束确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function inputConfirm(wFlag)
{
	//alert("LoadFlag=="+LoadFlag);
	
	
    if (wFlag ==1 ) //录入完毕确认
    {
        var tStr= "	select * from lwmission where 1=1 and lwmission.missionprop1 = '"+fm.ContNo.value+"'";
        turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
        if (turnPage.strQueryResult) 
        {
		    alert("该合同已经做过保存！");
		    return;
		}
		fm.AppntNo.value = AppntNo;
		fm.AppntName.value = AppntName;
		fm.WorkFlowFlag.value = "7999999999";
    }
    else if (wFlag ==2)//复核完毕确认
    {
  	    if(fm.all('ProposalContNo').value == "") 
        {
            alert("未查询出合同信息,不容许您进行 [复核完毕] 确认！");
            return;
        }
		fm.WorkFlowFlag.value = "0000001001";
		fm.MissionID.value = tMissionID;
		fm.SubMissionID.value = tSubMissionID;
    }
    else if (wFlag ==3)
    {
        if(fm.all('ProposalContNo').value == "") 
        {
            alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
            return;
        }
		fm.WorkFlowFlag.value = "0000001002";
		fm.MissionID.value = tMissionID;
		fm.SubMissionID.value = tSubMissionID;
	}
	else
		return;
		
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
    fm.action = "./InputConfirm.jsp";
    fm.submit(); //提交
} 
/*********************************************************************
 *  查询被保险人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getdetailaddress()
{
    var strSQL="select b.AddressNo,b.PostalAddress,b.ZipCode,b.Phone,b.Mobile,b.EMail,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode,b.HomeAddress,b.HomeZipCode,b.HomePhone,b.GrpName from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.InsuredNo.value+"'";
    arrResult=easyExecSql(strSQL);
    try{fm.all('AddressNo').value= arrResult[0][0];}catch(ex){};     
    try{fm.all('Phone').value= arrResult[0][3];}catch(ex){}; 
    try{fm.all('Mobile').value= arrResult[0][4];}catch(ex){}; 
    try{fm.all('EMail').value= arrResult[0][5];}catch(ex){};     
    try{fm.all('GrpPhone').value= arrResult[0][6];}catch(ex){}; 
    try{fm.all('GrpAddress').value= arrResult[0][7];}catch(ex){}; 
    try{fm.all('GrpZipCode').value= arrResult[0][8];}catch(ex){}; 
    try{fm.all('HomeAddress').value= arrResult[0][9];}catch(ex){}; 
    try{fm.all('HomeZipCode').value= arrResult[0][10];}catch(ex){}; 
    try{fm.all('HomePhone').value= arrResult[0][11];}catch(ex){}; 
    try{fm.all('GrpName').value= arrResult[0][12];}catch(ex){};       
}
/*********************************************************************
 *  查询保险计划
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getContPlanCode(tProposalGrpContNo)
{
    var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select ContPlanCode,ContPlanName from LCContPlan where ContPlanCode!='00' and ProposalGrpContNo='"+tProposalGrpContNo+"'";
    //alert("strsql :" + strsql);
    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
    if (turnPage.strQueryResult != "")
    {
    	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	m = turnPage.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    	}
    	 divContPlan.style.display="";   
    }
    else
    {
      //alert("保险计划没查到");
        divContPlan.style.display="none";   
    }
    //alert ("tcodedata : " + tCodeData);
    return tCodeData;
}       


function emptyInsured()
{
	
	try{fm.all('InsuredNo').value= ""; }catch(ex){}; 
	try{fm.all('ExecuteCom').value= ""; }catch(ex){}; 
	try{fm.all('FamilyID').value= ""; }catch(ex){}; 
	try{fm.all('RelationToMainInsured').value= ""; }catch(ex){}; 
	try{fm.all('RelationToAppnt').value= ""; }catch(ex){}; 
	try{fm.all('AddressNo').value= ""; }catch(ex){}; 
	//try{fm.all('SequenceNo').value= ""; }catch(ex){}; 
	try{fm.all('Name').value= ""; }catch(ex){}; 
	try{fm.all('Sex').value= ""; }catch(ex){}; 
	try{fm.all('Birthday').value= ""; }catch(ex){}; 
	try{fm.all('IDType').value= "0"; }catch(ex){}; 
	try{fm.all('IDNo').value= ""; }catch(ex){}; 
	try{fm.all('NativePlace').value= ""; }catch(ex){}; 
	try{fm.all('Nationality').value= ""; }catch(ex){}; 
	try{fm.all('RgtAddress').value= ""; }catch(ex){}; 
	try{fm.all('Marriage').value= ""; }catch(ex){}; 
	try{fm.all('MarriageDate').value= ""; }catch(ex){}; 
	try{fm.all('Health').value= ""; }catch(ex){}; 
	try{fm.all('Stature').value= ""; }catch(ex){}; 
	try{fm.all('Avoirdupois').value= ""; }catch(ex){}; 
	try{fm.all('Degree').value= ""; }catch(ex){}; 
	try{fm.all('CreditGrade').value= ""; }catch(ex){}; 
	try{fm.all('BankCode').value= ""; }catch(ex){}; 
	try{fm.all('BankAccNo').value= ""; }catch(ex){}; 
	try{fm.all('AccName').value= ""; }catch(ex){}; 
	try{fm.all('JoinCompanyDate').value= ""; }catch(ex){}; 
	try{fm.all('StartWorkDate').value= ""; }catch(ex){}; 
	try{fm.all('Position').value= ""; }catch(ex){}; 
	try{fm.all('Salary').value= ""; }catch(ex){}; 
	try{fm.all('OccupationType').value= ""; }catch(ex){}; 
	try{fm.all('OccupationCode').value= ""; }catch(ex){}; 
	try{fm.all('OccupationName').value= ""; }catch(ex){}; 	
	try{fm.all('WorkType').value= ""; }catch(ex){}; 
	try{fm.all('PluralityType').value= ""; }catch(ex){}; 
	try{fm.all('SmokeFlag').value= ""; }catch(ex){}; 
	try{fm.all('ContPlanCode').value= ""; }catch(ex){}; 
        try{fm.all('GrpName').value= ""; }catch(ex){}; 	
        try{fm.all('HomeAddress').value= ""; }catch(ex){}; 	
        try{fm.all('HomeZipCode').value= ""; }catch(ex){}; 	
        try{fm.all('HomePhone').value= ""; }catch(ex){}; 	
        try{fm.all('HomeFax').value= ""; }catch(ex){}; 	
        try{fm.all('GrpFax').value= ""; }catch(ex){}; 
        try{fm.all('Fax').value= ""; }catch(ex){}; 	                                       
	emptyAddress();
	ImpartGrid.clearData(); 
	ImpartGrid.addOne();
	ImpartDetailGrid.clearData(); 
	ImpartDetailGrid.addOne();	
}      



/*********************************************************************
 *  险种明细查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function riskQueryClick()  
 {  
	 var tSel = PolGrid.getSelNo();	
	 if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	 else
	{
		 var cPolNo = PolGrid.getRowColData( tSel - 1, 5 );
		 var cRiskSeqNo = PolGrid.getRowColData( tSel - 1, 6);
		  parent.fraInterface.window.location = "./LCProposalInput.jsp?InsuredNo=" 
			  + cInsuredNo + "&ContNo=" + ContNo+"&PolNo" + "cPolNo" + "&RiskSeqNo" 
			  + cRiskSeqNo + "&LogFlag=2";
	}
 }

/*********************************************************************
 *  体检信息查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function queryPE()  
 {  
     initPEGrid();	
    var tContNo=ContNo;
    //体检信息初始化
    if(tContNo!=null&&tContNo!="")
    {
		var strSql = "select PEDate,'' from LCPENotice where ContNo='" + ContNo + "'" ;
        turnPage3.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage3.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage3.arrDataCacheSet = decodeEasyQueryResult(turnPage3.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage3.pageDisplayGrid = PEGrid;    
        //保存SQL语句
        turnPage3.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage3.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage3.getData(turnPage3.arrDataCacheSet, turnPage3.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage3.pageDisplayGrid);
    }
 }

 /*********************************************************************
 *  被保人信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */                                                      
function queryInsuredNo() 
{    
	alert();
    if (tInsuredNo == "" || tInsuredNo == null) 
    {                    
    } 
    else 
    {                                                                   
        arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + fm.all("InsuredNo").value + "'", 1, 0);
        if (arrResult == null) 
        {                                                 
          alert("未查到被保人信息");                                             
        } 
        else 
        {                                                                 
          displayAppnt(arrResult[0]);                                            
        }                                                                        
    }                                                                          
}    

/*********************************************************************
 *  显示被保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */        
function displayAppnt()
{
    //try{fm.all('InsuredNo').value= arrResult[0][0]; }catch(ex){}; 
    try{fm.all('Name').value= arrResult[0][1]; }catch(ex){}; 
    try{fm.all('Sex').value= arrResult[0][2]; }catch(ex){}; 
    try{fm.all('Birthday').value= arrResult[0][3]; }catch(ex){}; 
    try{fm.all('IDType').value= arrResult[0][4]; }catch(ex){}; 
    try{fm.all('IDNo').value= arrResult[0][5]; }catch(ex){}; 
    try{fm.all('Password').value= arrResult[0][6]; }catch(ex){}; 
    try{fm.all('NativePlace').value= arrResult[0][7]; }catch(ex){}; 
    try{fm.all('Nationality').value= arrResult[0][8]; }catch(ex){}; 
    try{fm.all('RgtAddress').value= arrResult[0][9]; }catch(ex){}; 
    try{fm.all('Marriage').value= arrResult[0][10];}catch(ex){}; 
    try{fm.all('MarriageDate').value= arrResult[0][11];}catch(ex){}; 
    try{fm.all('Health').value= arrResult[0][12];}catch(ex){}; 
    try{fm.all('Stature').value= arrResult[0][13];}catch(ex){}; 
    try{fm.all('Avoirdupois').value= arrResult[0][14];}catch(ex){}; 
    try{fm.all('Degree').value= arrResult[0][15];}catch(ex){}; 
    try{fm.all('CreditGrade').value= arrResult[0][16];}catch(ex){}; 
    try{fm.all('OthIDType').value= arrResult[0][17];}catch(ex){}; 
    try{fm.all('OthIDNo').value= arrResult[0][18];}catch(ex){}; 
    try{fm.all('ICNo').value= arrResult[0][19];}catch(ex){}; 
    try{fm.all('GrpNo').value= arrResult[0][20];}catch(ex){}; 
    try{fm.all('JoinCompanyDate').value= arrResult[0][21];}catch(ex){}; 
    try{fm.all('StartWorkDate').value= arrResult[0][22];}catch(ex){}; 
    try{fm.all('Position').value= arrResult[0][23];}catch(ex){}; 
    try{fm.all('Salary').value= arrResult[0][24];}catch(ex){}; 
    try{fm.all('OccupationType').value= arrResult[0][25];}catch(ex){}; 
    try{fm.all('OccupationCode').value= arrResult[0][26];}catch(ex){}; 
    try {fm.all('OccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][26]);} catch(ex) { };      
    try{fm.all('WorkType').value= arrResult[0][27];}catch(ex){}; 
    try{fm.all('PluralityType').value= arrResult[0][28];}catch(ex){}; 
    try{fm.all('DeathDate').value= arrResult[0][29];}catch(ex){}; 
    try{fm.all('SmokeFlag').value= arrResult[0][30];}catch(ex){}; 
    try{fm.all('BlacklistFlag').value= arrResult[0][31];}catch(ex){}; 
    try{fm.all('Proterty').value= arrResult[0][32];}catch(ex){}; 
    try{fm.all('Remark').value= arrResult[0][33];}catch(ex){}; 
    try{fm.all('State').value= arrResult[0][34];}catch(ex){}; 
    try{fm.all('Operator').value= arrResult[0][35];}catch(ex){}; 
    try{fm.all('MakeDate').value= arrResult[0][36];}catch(ex){}; 
    try{fm.all('MakeTime').value= arrResult[0][37];}catch(ex){}; 
    try{fm.all('ModifyDate').value= arrResult[0][38];}catch(ex){}; 
    try{fm.all('ModifyTime').value= arrResult[0][39];}catch(ex){}; 
    try{fm.all('GrpName').value= arrResult[0][41];}catch(ex){}; 
    //地址显示部分的变动
    try{fm.all('AddressNo').value= "";}catch(ex){};    
    try{fm.all('Phone').value=  "";}catch(ex){}; 
    try{fm.all('Mobile').value=  "";}catch(ex){}; 
    try{fm.all('EMail').value=  "";}catch(ex){}; 
    //try{fm.all('GrpName').value= arrResult[0][46];}catch(ex){}; 
    try{fm.all('GrpPhone').value=  "";}catch(ex){}; 
    try{fm.all('GrpAddress').value=  "";}catch(ex){}; 
    try{fm.all('GrpZipCode').value=  "";}catch(ex){}; 
    try{fm.all('HomeAddress').value=  "";}catch(ex){}; 
    try{fm.all('HomeZipCode').value=  "";}catch(ex){}; 
    try{fm.all('HomePhone').value=  "";}catch(ex){};     
}

