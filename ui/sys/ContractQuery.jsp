<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ContractQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="ContractQueryInit.jsp"%>
  <title>保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
 
	
    <table  class= common align=center>
      	<TR  class= common>
		  <TD  class= title> 投保人姓名 </TD>  
		  <TD  class= input> <Input class="common1" name=AppntName > </TD>
	     <TD  class= title>
          投保人性别 
        </TD>
             <TD  class= input>
            <Input class=codeNo name=AppntSex  verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true elementtype=nacessary>    
          </TD>
				<TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntIDType" MAXLENGTH=1
            ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1]);" 
            onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1]);"><input class=codename name=AppntIDTypeName readonly=true>    
          </TD>
          <TD  class= title> 证件号码 </TD>
          <TD  class= input><Input class= common1 name=AppntIDNo ></TD> </TD>
        </TR>
        <TR  class= common>
		  <TD  class= title> 被保人姓名 </TD>
          <TD  class= input> <Input class="common1" name=InsuredName> </TD>
	     <TD  class= title>
          被保人性别 
        </TD>
        <TD  class= input>
          <Input name=InsuredSex class="codeNo" MAXLENGTH=1 
           ondblclick="showCodeListEx('Sex',[this,InsuredSexName],[0,1]);" 
           onkeyup="showCodeListKeyEx('Sex',[this,InsuredSexName],[0,1]);" 
           ><Input class=codename name=InsuredSexName readOnly> 
        </TD>    
				<TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class=codeNo name="InsuredIDType" MAXLENGTH=1
            ondblclick="return showCodeList('IDType',[this,InsuredIDTypeName],[0,1]);" 
            onkeyup="return showCodeListKey('IDType',[this,InsuredIDTypeName],[0,1]);"><input class=codename name=InsuredIDTypeName readonly=true>    
          </TD>
          <TD  class= title>证件号码 </TD>
          <TD  class= input> <Input class="common1" name=InsuredIDNo> </TD>
        </TR>
      <TR  class= common>
         <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          	<Input class=codeNo name=ManageCom  verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('station2',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station2',[this,ManageComName],[0,1]);" readonly><input class=codename name=ManageComName readonly=true elementtype=nacessary>
          </TD>
		  <TD  class= title> 投保申请时间 </TD>
          <TD  class= input> <Input name=PolApplyDate VALUE class="coolDatePicker"  >  </TD>
		  <TD  class= title> 单位名称 </TD>
          <TD  class= input> <Input class="common1" name=CompanyName> </TD>
        </TR>
    </table>


 	<Div  id= "divLCClent" style= "display: ''" align = center>
 			<table class= common border=0 align=center>
		<tr>
	   <td><INPUT VALUE="查  询" class = cssbutton TYPE=button onclick="easyQueryClick();"></td> 
	</tr>	
	</table>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
				<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage.previousPage();"> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage.lastPage();"> 		
    </div>
    <Div  id= "divLCPol2" style= "display: ''">
    	<table  class= common>
    		<tr  class= common>
    	  		<td text-align: left colSpan=1>
							<span id="spanPolStatuGrid" >
							</span> 
							<span id="spanAddfeeInfoGrid" >
							</span>
							<span id="spanSpecInfoGrid" >
							</span>  
	  				</td>
	  		</tr>
    	</table>
    </div>
	<table class="common" id="table2">
		<tr CLASS="common">
			<td CLASS="title">印刷号码</td>    		       
				<td CLASS="input" COLSPAN="1">
					<input NAME="prtno" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" READONLY="true">
				</td>	
			<td CLASS="title">投保类型</td>    		         
				<td CLASS="input" COLSPAN="1">
					<input NAME="Conttype" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20"READONLY="true">
				</td>		
			<td CLASS="title">投保人</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="AppntName1" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
				</td>
			<td CLASS="title">投保日期</td>    		       
				<td CLASS="input" COLSPAN="1">
					<input NAME="PolApplyDate1" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" READONLY="true">
				</td>		
</tr>
		<tr CLASS="common">

			<td CLASS="title">期缴保费</td>    		         
				<td CLASS="input" COLSPAN="1">
					<input NAME="Prem" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20"READONLY="true" READONLY="true">
				</td>		
			<td CLASS="title">首期保费</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="FirstPrem" VALUE CLASS="common" READONLY="true"TABINDEX="-1" READONLY="true">
				</td>
			<td CLASS="title">暂收余额</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="Dif" VALUE CLASS="common" READONLY="true"TABINDEX="-1" READONLY="true">
				</td>
			<td CLASS="title">付费人</td>    		       
				<td CLASS="input" COLSPAN="1">
					<input NAME="PayPerson" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" READONLY="true">
				</td>		
</tr>
		<tr CLASS="common">

			<td CLASS="title">业务员代码</td>    		         
				<td CLASS="input" COLSPAN="1">
					<input NAME="AgentCode" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" READONLY="true">
				</td>		
			<td CLASS="title">业务员</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="AgentName" VALUE CLASS="common" READONLY="true"TABINDEX="-1" READONLY="true">
				</td>
			<td CLASS="title">承保保单号</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="ContNo" VALUE CLASS="common" READONLY="true"TABINDEX="-1" READONLY="true">
				</td>
			<td CLASS="title">承保日期</td>    		       
				<td CLASS="input" COLSPAN="1">
					<input NAME="SignDate" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" READONLY="true">
				</td>		
</tr>
		<tr CLASS="common">

			<td CLASS="title">保单状态</td>    		         
				<td CLASS="input" COLSPAN="1">
					<input NAME="ContState" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" READONLY="true">
				</td>		
			<td CLASS="title">生效日期</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="CValiDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1" READONLY="true">
				</td>
</tr>
</table>
	<br>
	  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <input type =button class=cssButton value="复核险种信息" onclick="showComplexRiskInfo();">
  <INPUT VALUE="扫描件查询" class=cssButton  TYPE=button onclick="ScanQuery();">   
</body>
</html>