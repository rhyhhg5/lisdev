//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在Agent.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  fm.all('ManageCom').value = fm.all('hideManageCom').value;
  if( verifyInput() == false ) return false;  
  /*
  if ((trim(fm.all('Name').value) == '')||(fm.all('Name').value == null))
  {
  	alert("请输入姓名！");
  	fm.all('Name').focus();
  	return false;
  }
  if ((trim(fm.all('Sex').value) == '')||(fm.all('Sex').value == null))
  {
  	alert("请输入性别！");
  	fm.all('Sex').focus();
  	return false;
  }
  if ((trim(fm.all('Birthday').value) == '')||(fm.all('Birthday').value == null))
  {
  	alert("请输入出生日期！");
  	fm.all('Birthday').focus();
  	return false;
  }
  if ((trim(fm.all('IDNo').value) == '')||(fm.all('IDNo').value == null))
  {
  	alert("请输入身份证号！");
  	fm.all('IDNo').focus();
  	return false;       	
  }
  if ((trim(fm.all('AssuMoney').value) == '')||(fm.all('AssuMoney').value == null))
  {
  	alert("请输入保证金额！");
  	fm.all('AssuMoney').focus();
  	return false;
  }
  if ((trim(fm.all('hideAgentGroup').value) == '')||(fm.all('AgentGroup').value == null))
  {
  	alert("请输入销售机构！");
  	fm.all('AgentGroup').focus();
  	return false;
  }
  */
  var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value))
  if (strChkIdNo != "")
  {  	
        alert(strChkIdNo);
	return false;
  }
  if ((trim(fm.all('UpAgent').value)=='')&&(trim(fm.all('AgentSeries').value)=='A'))
  {
     alert('请先确定该销售机构的负责人！');
     return false;	
  }
  //检查应有的育成人是否录入完全
  /*
  if ((fm.all('AgentSeries').value > 'A')&&(trim(fm.all('RearAgent').value)==''))
  {
  	alert('请录入育成代理人！');
  	return false;
  }else if (fm.all('AgentSeries').value>'A')
  {
  	if ((fm.all('RearAgent').value==null)||(trim(fm.all('RearAgent').value)==''))
  	{
  		alert('请录入育成代理人！');
  		return false;
  	}else
  	if ((fm.all('AgentSeries').value>'B')&&(trim(fm.all('RearDepartAgent').value)==''))
  	{
  		alert('请录入增部代理人！');
  		return false;
  	}else  	
  	if ((fm.all('AgentSeries').value > 'C')&&(trim(fm.all('RearDepartAgent').value)==''))
  	{
  		alert('请录入育成督导代理人！'); 
  		return false;
  	}
  }*/
  if (!checkRearAgent())
    return false;
    
  //检查担保人信息是否录入  
    var lineCount = 0;
    var tempObj = fm.all('WarrantorGridNo'); //假设在表单fm中
    if (tempObj == null)
    {
      alert("请填写担保人信息！");
      return false;
    }
    WarrantorGrid.delBlankLine("WarrantorGrid");
    lineCount = WarrantorGrid.mulLineCount;
    if (lineCount == 0)
    {
      alert("请填写担保人信息！");
      return false;
    }else
    {
      var sValue;
      var strChkIdNo;	
      for(var i=0;i<lineCount;i++)
      {
      	sValue = WarrantorGrid.getRowColData(i,1);
      	if ((trim(sValue)=='')||(sValue==null))
      	{
      	   alert('请输入担保人名称！');
      	   return false;
      	}
      	sValue = WarrantorGrid.getRowColData(i,2);
      	if ((trim(sValue)=='')||(sValue==null))
      	{
      	   alert('请输入担保人性别！');
      	   return false;
      	}
      	sValue = WarrantorGrid.getRowColData(i,3);
      	if ((trim(sValue)=='')||(sValue==null))
      	{
      	   alert('请输入担保人身份证！');
      	   return false;
      	}
      	sValue = WarrantorGrid.getRowColData(i,4);
      	if ((trim(sValue)=='')||(sValue==null))
      	{
      	   alert('请输入担保人出生日期！');
      	   return false;
      	}
        strChkIdNo = chkIdNo(trim(WarrantorGrid.getRowColData(i,3)),trim(WarrantorGrid.getRowColData(i,4)),trim(WarrantorGrid.getRowColData(i,2)))
        if (strChkIdNo != "")
        {  	
          alert(strChkIdNo);
	  return false;
        }
      }	//end of for
    }
    
    return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.all('AgentCode').value = '';
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  alert('在此不能修改！');	
  /*
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的代理人记录！');    
  }else
  {
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {  	
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }*/
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./AgentQuery.html");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  alert('在此不能删除！');	
  /*
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要删除的代理人记录！');
  }else
  {	
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }*/
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function changeGroup()
{
   if (getWherePart('AgentGroup')=='')
     return false;
   var strSQL = "";
   strSQL = "select BranchAttr,ManageCom,BranchManager,AgentGroup,BranchLevel from LABranchGroup where 1=1 "
           +" and EndFlag <> 'Y' "
           + getWherePart('BranchAttr','AgentGroup')
           + getWherePart('ManageCom');			
     	 //alert(strSQL);
   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该销售机构！');
   	//fm.all('AgentGroup').focus();
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('hideManageCom').value = '';
   	fm.all('hideAgentGroup').value = '';
        fm.all('BranchLevel').value = '';
   	return false;
   }  
   var arr = decodeEasyQueryResult(strQueryResult);
   if ((arr[0][2]==null)||(trim(arr[0][2])==''))
   {
   	//增员非经理级的的代理人不能操作 -- 待定
   	if (trim(fm.all('AgentSeries').value)=='A')
   	{
   		alert('必须增员该销售机构的管理人员！');
   		return false;
        }
        //增员经理级的代理人若该人的职级与销售机构的级别一致则提示将该代理人设为管理人员-- 待定
        else
        {     
        	//营业组
        	if ((arr[0][4]=='01')&&(trim(fm.all('AgentSeries').value)>'A'))
        	{
        	    if (confirm("是否指定该代理人为该销售单位的管理人员？"))
        	       fm.all('hideIsManager').value='true';
        	    else
        	    {
        	       fm.all('hideIsManager').value='false';
        	       fm.all('AgentGroup').value = '';
        	    }
        	}  
        	//营业部 
        	if ((arr[0][4]>'02')&&(trim(fm.all('AgentSeries').value)>'B'))
        	{
        	    
        	    if (confirm("是否指定该代理人为该销售单位的管理人员？"))
        	       fm.all('hideIsManager').value='true';
        	    else
        	    {
        	       fm.all('hideIsManager').value='false';
        	       fm.all('AgentGroup').value = '';
        	    }
        	} 
        }
   }else
   {
   	switch(arr[0][4])
   	{
   	  case "01":
   	      if (fm.all('AgentSeries').value == 'B')
   	      {
   	      	alert('该销售单位已存在业务经理，不能再增员业务经理级的代理人！');
   	      	fm.all('AgentGroup').value = '';
   	        fm.all('UpAgent').value = '';
   	        fm.all('hideManageCom').value = '';
   	        fm.all('hideAgentGroup').value = '';
                fm.all('BranchLevel').value = '';
   	      	return false;
   	      }
   	      break;
   	  case "02":
   	      if (fm.all('AgentSeries').value == 'C')
   	      {
   	      	alert('该销售单位已存在高级业务经理，不能再增员高级业务经理级的代理人！');
   	      	fm.all('AgentGroup').value = '';
   	        fm.all('UpAgent').value = '';
   	        fm.all('hideManageCom').value = '';
   	        fm.all('hideAgentGroup').value = '';
                fm.all('BranchLevel').value = '';
   	      	return false;
   	      }
   	      break;
   	  case "03":
   	      if (fm.all('AgentSeries').value == 'C')
   	      {
   	      	alert('该销售单位已存在督导长，不能再增员督导长！');
   	      	fm.all('AgentGroup').value = '';
   	        fm.all('UpAgent').value = '';
   	        fm.all('hideManageCom').value = '';
   	        fm.all('hideAgentGroup').value = '';
                fm.all('BranchLevel').value = '';
   	      	return false;
   	      }
   	      break;
   	  case "04":	
   	      if (fm.all('AgentSeries').value == 'C')
   	      {
   	      	alert('该销售单位已存在区域督导长，不能再增员区域督导长！');
   	      	fm.all('AgengGroup').value = '';
   	        fm.all('UpAgent').value = '';
   	        fm.all('hideManageCom').value = '';
   	        fm.all('hideAgentGroup').value = '';
                fm.all('BranchLevel').value = '';
   	      	return false;
   	      }
   	      break;
   	} 
   }
   /*
   if (trim(arr[0][4])!='01')
   {
   	alert('只能录入营业组级别的机构代码！');
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('hideManageCom').value = '';
   	fm.all('hideAgentGroup').value = '';
   	return false;
   }*/
   fm.all('UpAgent').value = arr[0][2];
   fm.all('ManageCom').value = arr[0][1];
   fm.all('hideManageCom').value = arr[0][1];
   fm.all('hideAgentGroup').value = arr[0][3];
   fm.all('BranchLevel').value = arr[0][4];
   fm.all('IntroAgency').value = '';
   return true;
}

function changeIntroAgency()
{	
   if (getWherePart('IntroAgency')=='')
     return false;
   var strSQL = "";
   strSQL = "select a.AgentCode,b.BranchAttr,a.ManageCom, a.AgentGroup from LAAgent a,LABranchGroup b where 1=1 "
           + "and (a.AgentState is null or a.AgentState < '03') "
           + "and a.AgentGroup = b.AgentGroup "
           + getWherePart('a.AgentGroup','hideAgentGroup')
           + getWherePart('a.ManageCom','ManageCom')
           + getWherePart('a.AgentCode','IntroAgency');		
   //alert(strSQL);
   var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该代理人！');
   	fm.all('IntroAgency').value = '';
   	return false;
   }  
   var arr = decodeEasyQueryResult(strQueryResult);
   fm.all('AgentGroup').value = arr[0][1]
   fm.all('ManageCom').value = arr[0][2];
   fm.all('hideManageCom').value = arr[0][2];
   fm.all('hideAgentGroup').value = arr[0][3];
   return true;
}

function afterQuery(tAgentCode)
{	
	var arrSelected = new Array();	
	var strSQL = "";
	strSQL = "select a.*,c.BranchManager,b.IntroAgency,b.AgentSeries,b.AgentGrade,c.BranchAttr,b.AscriptSeries from LAAgent a,LATree b,LABranchGroup c where a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup and a.AgentCode='" + tAgentCode + "'"; 
	     
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;		                                  
                fm.all('AgentCode').value = arrResult[0][0];
                fm.all('hideAgentGroup').value = arrResult[0][1];
                fm.all('hideManageCom').value = arrResult[0][2];
                fm.all('ManageCom').value = arrResult[0][2];
                fm.all('Password').value = arrResult[0][3];
                fm.all('EntryNo').value = arrResult[0][4];
                fm.all('Name').value = arrResult[0][5];
                fm.all('Sex').value = arrResult[0][6];
                fm.all('Birthday').value = arrResult[0][7];
                fm.all('NativePlace').value = arrResult[0][8];
                fm.all('Nationality').value = arrResult[0][9];
                fm.all('Marriage').value = arrResult[0][10];
                fm.all('CreditGrade').value = arrResult[0][11];
                fm.all('HomeAddressCode').value = arrResult[0][12];
                fm.all('HomeAddress').value = arrResult[0][13];
                fm.all('PostalAddress').value = arrResult[0][14];
                fm.all('ZipCode').value = arrResult[0][15];
                fm.all('Phone').value = arrResult[0][16];
                fm.all('BP').value = arrResult[0][17];
                fm.all('Mobile').value = arrResult[0][18];
                fm.all('EMail').value = arrResult[0][19];
                fm.all('MarriageDate').value = arrResult[0][20];
                fm.all('IDNo').value = arrResult[0][21];
                fm.all('Source').value = arrResult[0][22];
                fm.all('BloodType').value = arrResult[0][23];
                fm.all('PolityVisage').value = arrResult[0][24];
                fm.all('Degree').value = arrResult[0][25];
                fm.all('GraduateSchool').value = arrResult[0][26];
                fm.all('Speciality').value = arrResult[0][27];
                fm.all('PostTitle').value = arrResult[0][28];
                fm.all('ForeignLevel').value = arrResult[0][29];
                fm.all('WorkAge').value = arrResult[0][30];
                fm.all('OldCom').value = arrResult[0][31];
                fm.all('OldOccupation').value = arrResult[0][32];
                fm.all('HeadShip').value = arrResult[0][33];
                //fm.all('RecommendAgent').value = arrResult[0][34];
                fm.all('Business').value = arrResult[0][35];
                fm.all('SaleQuaf').value = arrResult[0][36];
                fm.all('QuafNo').value = arrResult[0][37];
                fm.all('QuafStartDate').value = arrResult[0][38];
                fm.all('QuafEndDate').value = arrResult[0][39];
                fm.all('DevNo1').value = arrResult[0][40];
                fm.all('DevNo2').value = arrResult[0][41];
                fm.all('RetainContNo').value = arrResult[0][42];
                fm.all('AgentKind').value = arrResult[0][43];
                fm.all('DevGrade').value = arrResult[0][44];
                fm.all('InsideFlag').value = arrResult[0][45];
                fm.all('FullTimeFlag').value = arrResult[0][46];
                fm.all('NoWorkFlag').value = arrResult[0][47];
                fm.all('TrainDate').value = arrResult[0][48];
                fm.all('EmployDate').value = arrResult[0][49];
                //fm.all('InDueFormDate').value = arrResult[0][50];
                fm.all('OutWorkDate').value = arrResult[0][51];
                //fm.all('Approver').value = arrResult[0][57];
                //fm.all('ApproveDate').value = arrResult[0][58];
                fm.all('AssuMoney').value = arrResult[0][59];
                fm.all('AgentState').value = arrResult[0][61];
                //fm.all('QualiPassFlag').value = arrResult[0][62];
                fm.all('SmokeFlag').value = arrResult[0][63];
                fm.all('RgtAddress').value = arrResult[0][64];
                fm.all('BankCode').value = arrResult[0][65];
                fm.all('BankAccNo').value = arrResult[0][66];
                fm.all('Remark').value = arrResult[0][60];
                fm.all('Operator').value = arrResult[0][67];
                //行政信息
                fm.all('UpAgent').value = arrResult[0][73];
                fm.all('IntroAgency').value = arrResult[0][74];
                fm.all('AgentSeries').value = arrResult[0][75];
                fm.all('AgentGrade').value = arrResult[0][76];
                var arrRear = arrResult[0][78].split("^");
                fm.all('RearAgent').value = arrRear.length>0?arrRear[0]:'';
                fm.all('RearDepartAgent').value = arrRear.length>1?arrRear[0]:'';
                fm.all('RearSuperintAgent').value = arrRear.length>2?arrRear[0]:'';
                //显式机构代码
                fm.all('AgentGroup').value = arrResult[0][77];
                
        }
        WarrantorGrid.clearData("WarrantorGrid");
        easyQuery();
}
function easyQuery()
{
   // 书写SQL语句
   var strSQL = "";
   strSQL = "select * from LAWarrantor where 1=1 "
           + getWherePart('AgentCode');		
     	 //alert(strSQL);
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("担保人信息查询失败！");
    return false;
    }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.arrDataCacheSet = chooseArray(tArr,[0,1,2,3,4,5]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = WarrantorGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = chooseArray(arrDataSet,[2,3,4,5,6,7,8,9,10,11]); 
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function changeIDNo()
{
   if (getWherePart('IDNo')=='')
     return false;
   var strSQL = "";
   strSQL = "select * from LAAgent where 1=1 "
           + getWherePart('IDNo');		
     	 //alert(strSQL);
   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
   if (strQueryResult)
   {
   	alert('该身份证号已存在!');
   	fm.all('IDNo').value = '';
   	return false;
   }
   return true; 
}
//校验育成代理人
function checkRearAgent()
{
   var strSQL = "",str = "";
   var strQueryResult = null;
   
   strSQL = "select AgentCode from LAAgent where (AgentState < '03' or AgentState is not null) "
   //育成代理人
   if (trim(fm.all('RearAgent').value)=='')
     return true;
   str = getWherePart('AgentCode','RearAgent');
            
   alert(strSQL+str);
   strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该育成代理人！');
   	fm.all('RearAgent').value = '';
   	return false;
   }  
   //增部代理人
   if (trim(fm.all('RearDepartAgent').value)=='')
     return true;
   str = getWherePart('AgentCode','RearDepartAgent');
            
   alert(strSQL+str);
   strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该增部代理人！');
   	fm.all('RearDepartAgent').value = '';
   	return false;
   } 
   //增部代理人
   if (trim(fm.all('RearSuperintAgent').value)=='')
     return true;
   str = getWherePart('AgentCode','RearSuperintAgent');
            
   alert(strSQL+str);
   strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该育成督导代理人！');
   	fm.all('RearSuperintAgent').value = '';
   	return false;
   }
}
