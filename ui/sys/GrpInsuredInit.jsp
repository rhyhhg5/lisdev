<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 被保人查询条件
    fm.all('Name').value = '';
    fm.all('InsuredNo').value = '';
    fm.all('IDNo').value = '';
	  fm.all('InsuredStat').value = '0';
	  
	  setContPlanCode();
  }
  catch(ex)
  {
    alert("在GrpInsuredInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
     initInpBox();
     initGrpInsuredSumGrid();
     initGrpInseGrid();
     initNoNameCont();
     initNNCOperateTrace();
	   //easyQueryClick();
	   queryNoNameCont();
	   queryOperateTrace();
  }
  catch(re)
  {
    alert("在GrpInsuredInit.jsp-->InitForm函数中发生异常:初始化界面错误!" + re.message);
  }
}

//被保人信息汇总，如按被保人状态或保障计划查询时显示
function initGrpInsuredSumGrid()
  {                               
    var iArray = new Array();
      
      try
      {
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            			//列最大值
	      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[1]=new Array();
	      iArray[1][0]="总人数";         		//列名
	      iArray[1][1]="50px";            		//列宽
	      iArray[1][2]=100;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[2]=new Array();
	      iArray[2][0]="有效人数";         		//列名
	      iArray[2][1]="50px";            		//列宽
	      iArray[2][2]=100;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      
		    iArray[3]=new Array();
	      iArray[3][0]="失效人数";         		//列名
	      iArray[3][1]="50px";            		//列宽
	      iArray[3][2]=100;            			//列最大值
	      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
          			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[4]=new Array();
	      iArray[4][0]="生效日期";         		//列名
	      iArray[4][1]="60px";            		//列宽
	      iArray[4][2]=100;            			//列最大值
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	       
	      
	      iArray[5]=new Array();
	      iArray[5][0]="满期日";         		//列名
	      iArray[5][1]="60px";            		//列宽
	      iArray[5][2]=200;            			//列最大值
	      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	
		    iArray[6]=new Array();
	      iArray[6][0]="保费交至日";         		//列名
	      iArray[6][1]="60px";            		//列宽
	      iArray[6][2]=200;            			//列最大值
	      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	
	      iArray[7]=new Array();
	      iArray[7][0]="总保费合计";         		//列名
	      iArray[7][1]="50px";            		//列宽
	      iArray[7][2]=200;            			//列最大值
		    iArray[7][3]=0; 
		    
	     
	      iArray[8]=new Array();
	      iArray[8][0]="有效保费合计";         		//列名
	      iArray[8][1]="50px";            		//列宽
	      iArray[8][2]=100;            			//列最大值
	      iArray[8][3]=0;        

	      GrpInsuredSumGrid = new MulLineEnter( "fm" , "GrpInsuredSumGrid" ); 
	      //这些属性必须在loadMulLine前
	      GrpInsuredSumGrid.mulLineCount = 0;   
	      GrpInsuredSumGrid.displayTitle = 1;
	      GrpInsuredSumGrid.locked = 1;
	      GrpInsuredSumGrid.canSel = 1;
	      //GrpInsuredSumGrid.hiddenPlus = 1;
	      //GrpInsuredSumGrid.hiddenSubtraction = 1;
	      GrpInsuredSumGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert("在GrpInsuredInit.jsp-->initGrpInsuredSumGrid函数中发生异常:初始化界面错误!");
      }
}

// 保单信息列表的初始化
function initGrpInseGrid()
  {                               
    var iArray = new Array();
      
      try
      {
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            			//列最大值
	      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[1]=new Array();
	      iArray[1][0]="客户号";         		//列名
	      iArray[1][1]="70px";            		//列宽
	      iArray[1][2]=100;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      iArray[1][21]="InsuredNo";
	
	      iArray[2]=new Array();
	      iArray[2][0]="被保人姓名";         		//列名
	      iArray[2][1]="80px";            		//列宽
	      iArray[2][2]=100;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      
		    iArray[3]=new Array();
	      iArray[3][0]="员工姓名";         		//列名
	      iArray[3][1]="80px";            		//列宽
	      iArray[3][2]=100;            			//列最大值
	      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
          			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[4]=new Array();
	      iArray[4][0]="与员工关系";         		//列名
	      iArray[4][1]="60px";            		//列宽
	      iArray[4][2]=100;            			//列最大值
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	       
	      
	      iArray[5]=new Array();
	      iArray[5][0]="性别";         		//列名
	      iArray[5][1]="35px";            		//列宽
	      iArray[5][2]=200;            			//列最大值
	      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	
		    iArray[6]=new Array();
	      iArray[6][0]="出生日期";         		//列名
	      iArray[6][1]="70px";            		//列宽
	      iArray[6][2]=200;            			//列最大值
	      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	
	      iArray[7]=new Array();
	      iArray[7][0]="证件类型";         		//列名
	      iArray[7][1]="55px";            		//列宽
	      iArray[7][2]=200;            			//列最大值
		    iArray[7][3]=0; 
		    
	     
	      iArray[8]=new Array();
	      iArray[8][0]="证件号码";         		//列名
	      iArray[8][1]="140px";            		//列宽
	      iArray[8][2]=100;            			//列最大值
	      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[9]=new Array();
	      iArray[9][0]="职业类别";         		//列名
	      iArray[9][1]="55px";            		//列宽
	      iArray[9][2]=100;            			//列最大值
	      iArray[9][3]=0;     
	     
	    
		    iArray[10]=new Array();
	      iArray[10][0]="保障计划";         		//列名
	      iArray[10][1]="55px";            		//列宽
	      iArray[10][2]=100;            			//列最大值
	      iArray[10][3]=0; 
	
	      iArray[11]=new Array();
	      iArray[11][0]="国籍";         //列名
	      iArray[11][1]="70px";            		//列宽
	      iArray[11][2]=110;            			//列最大值
	      iArray[11][3]=0;  
	      
	      iArray[12]=new Array();
	      iArray[12][0]="国家/地区";         //列名
	      iArray[12][1]="70px";            		//列宽
	      iArray[12][2]=110;            			//列最大值
	      iArray[12][3]=0;  
	      
		  iArray[13]=new Array();
	      iArray[13][0]="期交保费";         		//列名
	      iArray[13][1]="60px";            		//列宽
	      iArray[13][2]=100;            			//列最大值
	      iArray[13][3]=0; 
	      
	      iArray[14]=new Array();
	      iArray[14][0]="当期交费";         		//列名
	      iArray[14][1]="60px";            		//列宽
	      iArray[14][2]=100;            			//列最大值
	      iArray[14][3]=0; 
	      iArray[14][21]="SumActuPayMoney"; 
	
		  iArray[15]=new Array();
	      iArray[15][0]="生效日期";         		//列名
	      iArray[15][1]="70px";            		//列宽
	      iArray[15][2]=110;            			//列最大值
	      iArray[15][3]=0; 
	      
	      iArray[16]=new Array();
	      iArray[16][0]="终止日期";         		//列名
	      iArray[16][1]="70px";            		//列宽
	      iArray[16][2]=110;            			//列最大值
	      iArray[16][3]=0; 
	      
		  iArray[17]=new Array();
	      iArray[17][0]="账户余额信息";         		//列名
	      iArray[17][1]="70px";            		//列宽
	      iArray[17][2]=110;            			//列最大值
	      iArray[17][3]=0; 
	      
	      iArray[18]=new Array();
	      iArray[18][0]="理赔金账号";         		//列名
	      iArray[18][1]="70px";            		//列宽
	      iArray[18][2]=110;            			//列最大值
	      iArray[18][3]=0; 
	      
	      iArray[19]=new Array();
	      iArray[19][0]="保单号";         //列名
	      iArray[19][1]="70px";            		//列宽
	      iArray[19][2]=110;            			//列最大值
	      iArray[19][3]=3; 
	      iArray[19][21]="ContNo";    
	      
	      iArray[20]=new Array();
	      iArray[20][0]="学校";         		//列名
	      iArray[20][1]="70px";            		//列宽
	      iArray[20][2]=110;            			//列最大值
	      iArray[20][3]=0; 
	      
	      iArray[21]=new Array();
	      iArray[21][0]="班级";         //列名
	      iArray[21][1]="70px";            		//列宽
	      iArray[21][2]=110;            			//列最大值
	      iArray[21][3]=0; 
   
	      iArray[22]=new Array();
	      iArray[22][0]="授权使用客户信息";         //列名
	      iArray[22][1]="70px";            		//列宽
	      iArray[22][2]=110;            			//列最大值
	      iArray[22][3]=0;
	      
	      iArray[23]=new Array();
	      iArray[23][0]="岗位";         //列名
	      iArray[23][1]="70px";            		//列宽
	      iArray[23][2]=110;            			//列最大值
	      iArray[23][3]=0;
	      
	      iArray[24]=new Array();
	      iArray[24][0]="职业";         //列名
	      iArray[24][1]="70px";            		//列宽
	      iArray[24][2]=110;            			//列最大值
	      iArray[24][3]=0;

	      GrpInseGrid = new MulLineEnter( "fm" , "GrpInseGrid" ); 
	      //这些属性必须在loadMulLine前
	      GrpInseGrid.mulLineCount = 0;   
	      GrpInseGrid.displayTitle = 1;
	      GrpInseGrid.locked = 1;
	      GrpInseGrid.canSel = 1;
	      //GrpInseGrid.hiddenPlus = 1;
	      //GrpInseGrid.hiddenSubtraction = 1;
	      GrpInseGrid.loadMulLine(iArray);
		    GrpInseGrid.selBoxEventFuncName = "fromListReturn";
      }
      catch(ex)
      {
        alert("在GrpInsuredInit.jsp-->initGrpInseGrid函数中发生异常:初始化界面错误!");
      }
}

//无名单状态列表
function initNoNameCont()
{                               
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保障计划";         		//列名
    iArray[1][1]="70px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="contPlanCode";

    iArray[2]=new Array();
    iArray[2][0]="人员类别";         		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="contPlanName";
    
    iArray[3]=new Array();
    iArray[3][0]="被保人数";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="peoples2";
    
    iArray[4]=new Array();
    iArray[4][0]="保费";         		//列名
    iArray[4][1]="60px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="prem"; 
    
    iArray[5]=new Array();
    iArray[5][0]="保额";         		//列名
    iArray[5][1]="35px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="amnt";
    
    iArray[6]=new Array();
    iArray[6][0]="保单号";         		//列名
    iArray[6][1]="35px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="contNo";
    
    iArray[7]=new Array();
    iArray[7][0]="被保人号";         		//列名
    iArray[7][1]="35px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="insuredNo";

    NoNameCont = new MulLineEnter( "fm" , "NoNameCont" ); 
    //这些属性必须在loadMulLine前
    NoNameCont.mulLineCount = 0;   
    NoNameCont.displayTitle = 1;
    NoNameCont.locked = 1;
    NoNameCont.canSel = 1;
    NoNameCont.hiddenPlus = 1;
    NoNameCont.hiddenSubtraction = 1;
    NoNameCont.selBoxEventFuncName = "queryOperateTrace";
    NoNameCont.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在GrpInsuredInit.jsp-->initGrpInseGrid函数中发生异常:初始化界面错误!");
  }
}

//初始化无名单操作轨迹
function initNNCOperateTrace()
{                               
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=3;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="工单号";         		//列名
    iArray[1][1]="50px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="workNo";

    iArray[2]=new Array();
    iArray[2][0]="项目";         		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="edorName";
    
    iArray[3]=new Array();
    iArray[3][0]="保障计划";         		//列名
    iArray[3][1]="40px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="contPlanCode";
    
    iArray[4]=new Array();
    iArray[4][0]="人员类别";         		//列名
    iArray[4][1]="60px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="contPlanName"; 
    
    iArray[5]=new Array();
    iArray[5][0]="增减人数";         		//列名
    iArray[5][1]="35px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="peoplesChange";
    
    iArray[6]=new Array();
    iArray[6][0]="补退费";         		//列名
    iArray[6][1]="50px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="getMoney";
    
    iArray[7]=new Array();
    iArray[7][0]="生效日期";         		//列名
    iArray[7][1]="35px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="edorValiDate";

    NNCOperateTrace = new MulLineEnter( "fm" , "NNCOperateTrace" ); 
    //这些属性必须在loadMulLine前
    NNCOperateTrace.mulLineCount = 0;   
    NNCOperateTrace.displayTitle = 1;
    NNCOperateTrace.locked = 1;
    //NNCOperateTrace.canSel = 1;
    NNCOperateTrace.hiddenPlus = 1;
    NNCOperateTrace.hiddenSubtraction = 1;
    NNCOperateTrace.selBoxEventFuncName = "queryOperateTrace";
    NNCOperateTrace.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在GrpInsuredInit.jsp-->initGrpInseGrid函数中发生异常:初始化界面错误!");
  }
}

</script>