<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="AllGetQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AllGetQueryInit.jsp"%>
  <title>给付查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入给付查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 实付号码 </TD>
          <TD  class= input> <Input class= common name=ActuGetNo > </TD>
          <TD  class= title>  其它号码  </TD>
          <TD  class= input>  <Input class= common name=OtherNo > </TD>
          <TD  class= title> 其它号码类型 </TD>
          <TD  class= input>	<Input class=codeno name=OtherNoType verify="其它号码类型" CodeData="0|^0|生存领取合同号^1|生存领取集体保单号^2|生存领取个人保单号^3|批改号^4|暂交费退费给付通知书号^5|赔付应收给付通知书号^6|其他退费给付通知书号^7|红利个人保单号" ondblClick="showCodeListEx('OtherNoType',[this,OtherNoTypeName],[0,1,2,3,4,5,6,7]);" onkeyup="showCodeListKeyEx('OtherNoType',[this,OtherNoTypeName],[0,1,2,3,4,5,6,7]);"><input class = className name = OtherNoTypeName ></TD>          
        </TR>
        <TR  class= common>
          <TD  class= title>  应付日期</TD>
          <TD  class= input>	<Input class= "coolDatePicker" dateFormat="short" name=ShouldDate > </TD>
          <TD  class= title>  管理机构 </TD>
          <TD  class= input><Input class="codeno" name=MngCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,MngComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this.MngComName],[0,1],null,null,null,1);"><input class = className name = MngComName></TD>
          <TD  class= title> 代理人编码  </TD> 
          <TD  class= input> <Input class="codeno" name=AgentCode verify="代理人编码|notnull&code:AgentCode" ondblclick="return showCodeList('AgentCode',[this,AgentCodeName], [0,1], null, MngCom, 'ManageCom');" onkeyup="return showCodeListKey('AgentCode', [this,AgentCodeName], [0,1], null, MngCom, 'ManageCom');"><input class = className name = AgentCodeName></TD>
        </TR>
    </table>
          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="给付明细" class= common TYPE=button onclick="getQueryDetail();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJAGet1);">
    		</td>
    		<td class= titleImg>
    			 给付信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLJAGet1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
