<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectGrid();
	divPage.style.display="";                 
       
}
var ProjectGrid;
function initProjectGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         	//列名
    iArray[0][1]="30px";         	//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="险种编码";         	 //列名
    iArray[1][1]="120px";            //列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
            		
    iArray[2]=new Array();
    iArray[2][0]="险种名称";         	 //列名
    iArray[2][1]="120px";            //列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    var out=easyExecSql("select distinct edorcode,edorname from lmedoritem with ur");
    for(i=0;i<out.length;i++){
		iArray[i+3]=new Array();
	    iArray[i+3][0]=out[i][1]+"("+out[i][0]+")";         	
	    iArray[i+3][1]="120px";            	
	    iArray[i+3][2]=200;            		 
	    iArray[i+3][3]=0;
	}
     
    ProjectGrid = new MulLineEnter("fm", "ProjectGrid"); 
    //设置Grid属性
    ProjectGrid.mulLineCount = 0;
    ProjectGrid.displayTitle = 1;
    ProjectGrid.locked = 1;
    ProjectGrid.canSel = 0;
    ProjectGrid.canChk = 0;
    ProjectGrid.hiddenSubtraction = 1;
    ProjectGrid.hiddenPlus = 1;
 	ProjectGrid.selBoxEventFuncName = "getQueryResult";
    ProjectGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
                                      

</script>
