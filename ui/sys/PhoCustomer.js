//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var arrAllDataSet;

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//王珑修改

/*********************************************************************
 *  查询被保人信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	
	// 初始化表格
	initInsuredGrid();	
	// 书写SQL语句RiskCode
	var strSQL = "";
	if (tContType == 1)
	{
		 strSQL = "select a.InsuredNo,a.Name "
	   +" from LCInsured a  where a.ContNo='" + tContNo + "'  ";
	}else
	{
		 strSQL = "select a.InsuredNo,a.Name "
	   +" from LBInsured a  where a.ContNo='" + tContNo + "'  ";
	}	   
	  
	//查询SQL，返回结果字符串
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	InsuredGrid.clearData(); 
    return false;
  }
 
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  arrAllDataSet = turnPage.arrDataCacheSet;  
 
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = InsuredGrid;   
         
  //保存SQL语句
  turnPage.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //setAmntMult();
  var arrDataSet = turnPage.getData(arrAllDataSet, turnPage.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}  

 
/*********************************************************************
 *  查询合同信息及投保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function returnParent()
 {
 	               
	var arrReturn1 = new Array();
	var arrReturn2 = new Array();
	var arrReturn3 = new Array();
	var cDate = new Date();
	var cYear = cDate.getYear();	
	try{
		
		//投保人信息
		if(tContType=="1"){
			var strSql2 = "select LDPerson.CustomerNo,LDPerson.Name,LDPerson.Birthday,LDPerson.OccupationCode,'',LCAppnt.AddressNo" 
			 + ",LDPerson.Sex,"+ cYear +" - year(LDPerson.Birthday),LDPerson.NativePlace,LDPerson.IDType,LDPerson.IDNo,LDPerson.GrpNo,LCAddress.GrpName,LCAddress.CompanyPhone" 
			 + ",LCAddress.CompanyAddress,LCAddress.CompanyZipCode,LCAddress.HomeAddress,LCAddress.HomeZipCode,LCAddress.HomePhone" 
			 + ",LCAddress.Mobile"
			 + " from LCAppnt,LDPerson,LCAddress"
			 + " where LCAppnt.ContNo= '"+tContNo+"'"
			 + " and LDPerson.CustomerNo=LCAppnt.AppntNo and LDPerson.CustomerNo = LCAddress.CustomerNo and LCAppnt.AddressNo=LCAddress.AddressNo";
		}else if(tContType=="2")
		{
			 var strSql2 = "select LDPerson.CustomerNo,LDPerson.Name,LDPerson.Birthday,LDPerson.OccupationCode,'',LCAddress.AddressNo" 
			 + ",LDPerson.Sex,"+ cYear +" - year(LDPerson.Birthday),LDPerson.NativePlace,LDPerson.IDType,LDPerson.IDNo,LDPerson.GrpNo,LCAddress.GrpName,LCAddress.CompanyPhone" 
			 + ",LCAddress.CompanyAddress,LCAddress.CompanyZipCode,LCAddress.HomeAddress,LCAddress.HomeZipCode,LCAddress.HomePhone" 
			 + ",LCAddress.Mobile"
			 + " from LBAppnt,LDPerson,LCAddress"
			 + " where LBAppnt.ContNo= '"+tContNo+"'"
			 + " and LDPerson.CustomerNo=LBAppnt.AppntNo and LDPerson.CustomerNo = LCAddress.CustomerNo and LBAppnt.AddressNo=LCAddress.AddressNo";

		}
		arrReturn2 =easyExecSql(strSql2);
		
		if (arrReturn2 == null) {
		} else {
		  
		   var strSql3="select  trim(OccupationName)||'-'||trim(workname) from LDOccupation" 
			   + " where OccupationCode='" + arrReturn2[0][3]+ "'"                   
			   + " order by OccupationCode"
			 ;
			arrReturn3 =easyExecSql(strSql3);
			if (arrReturn3 == null) {
				arrReturn2[0][4]='';
			} else {
				 arrReturn2[0][4]=arrReturn3[0][0];
			}
		   displayAppnt(arrReturn2[0]);
		}
	}
	catch(ex)
	{
		alert( "请先选择一条非空记录。");			
	}		
	showCodeName();
 } 
  

/*********************************************************************
 *  设置投保人信息
 *  参数  ：  cArr 数组
 *  返回值：  无
 *********************************************************************
 */
  function displayAppnt(cArr)
  {
   	try { fm.all('AppntNo').value = cArr[0]; } catch(ex) { };
  	try { fm.all('ClietName').value = cArr[1]; } catch(ex) { };
  	try { fm.all('AppntBirthday').value = cArr[2]; } catch(ex) { };
  	try { fm.all('AppntOccupationCode').value = cArr[3]; } catch(ex) { };
  	try { fm.all('AppntOccupationName').value = cArr[4]; } catch(ex) { };
  	try { fm.all('AddressNo').value = cArr[5]; } catch(ex) { };
  	try { fm.all('AppntSex').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntYear').value = cArr[7]; } catch(ex) { };
  	try { fm.all('AppntNativePlace').value = cArr[8]; } catch(ex) { };
  	try { fm.all('AppntIDType').value = cArr[9]; } catch(ex) { };
  	try { fm.all('AppntIDNo').value = cArr[10]; } catch(ex) { };
  	try { fm.all('AppntGrpNo').value = cArr[11]; } catch(ex) { };
  	try { fm.all('AppntGrpName').value = cArr[12]; } catch(ex) { };
    try { fm.all('AppntGrpPhone').value = cArr[13]; } catch(ex) { };
	try { fm.all('CompanyAddress').value = cArr[14]; } catch(ex) { };
  	try { fm.all('AppntGrpZipCode').value = cArr[15]; } catch(ex) { };
  	try { fm.all('AppntHomeAddress').value = cArr[16]; } catch(ex) { };
  	try { fm.all('AppntHomeZipCode').value = cArr[17]; } catch(ex) { };	
  	try { fm.all('AppntMobile').value = cArr[19]; } catch(ex) { };
	var cRelation = new Array();
	cRelation= easyExecSql("select RelationToMainInsured from LCInsured where contno='"+ tContNo+"' and AppntNo='"+fm.all('AppntNo').value+"'"); 

	try { fm.all('RelationToMainInsured').value=easyExecSql("select  CodeName from ldcode where codetype = 'relation' and Code='"+cRelation[0][0]+"'");} catch(ex) { };
  	
 }


/*********************************************************************
 *  设置被保人信息
 *  参数  ：  cArr 数组
 *  返回值：  无
 *********************************************************************
 */
  function displayInsured(cArr)
  {
   	try { fm.all('InsuredNo').value = cArr[0]; } catch(ex) { };
  	try { fm.all('Name').value = cArr[1]; } catch(ex) { };
  	try { fm.all('Sex').value = cArr[2]; } catch(ex) { };
  	try { fm.all('Birthday').value = cArr[3]; } catch(ex) { };
  	try { fm.all('INsuredYear').value = cArr[4]; } catch(ex) { };
  	try { fm.all('IDType').value = cArr[5]; } catch(ex) { };
  	try { fm.all('IDNo').value = cArr[6]; } catch(ex) { };
  	try { fm.all('RelationToAppnt').value = easyExecSql("select  CodeName from ldcode where codetype = 'relation' and Code='"+cArr[7]+"'");} catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[8]; } catch(ex) { };
	try { fm.all('OccupationName').value = cArr[9]; } catch(ex) { };
	try { fm.all('InsuredAddressNo').value = cArr[10]; } catch(ex) { };  	
  	try { fm.all('HomeAddress').value = cArr[11]; } catch(ex) { };
  	try { fm.all('HomeZipCode').value = cArr[12]; } catch(ex) { };
    try { fm.all('Mobile').value = cArr[13]; } catch(ex) { };
	try { fm.all('GrpName').value = cArr[14]; } catch(ex) { };

 }


 /*********************************************************************
 *  险种明细查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function riskQueryClick()  
 {  
	 var tSel = InsuredGrid.getSelNo();	
	 if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击险种明细按钮。" );
	 else
	{
		 var cInsuredNo = InsuredGrid.getRowColData( tSel - 1, 1 );				
		  window.open("./DetailedInfoQuery.jsp?InsuredNo=" 
			  + cInsuredNo + "&ContNo=" + tContNo  + "&ContType=" + tContType);
	}
 }




/*********************************************************************
 *  返回
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GoBack(){
	//此方法工单复用，修改请慎重,yangyalin
	
	//工单用，修改请慎重
	top.close();
	top.opener.focus();
	
}


/*********************************************************************
 *  设置被保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setInsValue()
{
    var tInsNo=""; 

	var arrReturn4 = new Array();
	var arrReturn5 = new Array();

	var cDate = new Date();
	var cYear = cDate.getYear();	

   //个单客户选中客户号码
	var tSel = InsuredGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{	
    }else
    {	
	    tInsNo = InsuredGrid.getRowColData( tSel - 1, 1 );
		//投保人信息
            if(tContType=="1"){
				var strSql2 = "select a.InsuredNo,a.Name,a.Sex,a.Birthday,"+ cYear +" - year(a.Birthday),a.IDType" 
				 + ",a.IDNo,a.RelationToAppnt,a.OccupationCode,'',a.AddressNo,'','','',''"
				 + " from LCInsured a"
				 + " where a.ContNo= '"+tContNo+"' and a.InsuredNo='" + tInsNo + "'"
			}else if(tContType=="2")
			{
                 var strSql2 = "select a.InsuredNo,a.Name,a.Sex,a.Birthday,"+ cYear +" - year(a.Birthday),a.IDType" 
				 + ",a.IDNo,a.RelationToAppnt,a.OccupationCode,'',a.AddressNo,'','',''"
				 + " from LBInsured a"
				 + " where a.ContNo= '"+tContNo+"' and a.InsuredNo='" + tInsNo + "'"

			}
			arrReturn2 =easyExecSql(strSql2);
			
			if (arrReturn2 == null) {
			} else {
			   var strSql3="select  trim(OccupationName)||'-'||trim(workname) from LDOccupation" 
                   + " where OccupationCode='" + arrReturn2[0][8]+ "'"                   
                   + " order by OccupationCode"
			     ;
			    arrReturn3 =easyExecSql(strSql3);
			    if (arrReturn3 == null) {
                    arrReturn2[0][9]='';
			    } else {
			         arrReturn2[0][9]=arrReturn3[0][0];
			    }   
				
				var strSql4="select  a.HomeAddress,a.HomeZipCode,a.Mobile,a.GrpName from LCAddress a" 
                   + " where CustomerNo='" + tInsNo+ "' and AddressNo='" +arrReturn2[0][10]  + "'"                   
			     ;
			    arrReturn4 =easyExecSql(strSql4);
			    if (arrReturn4 == null) {
                    arrReturn2[0][11]='';
					arrReturn2[0][12]='';
					arrReturn2[0][13]='';
					arrReturn2[0][14]='';
			    } else {
			        arrReturn2[0][11]=arrReturn4[0][0];
					arrReturn2[0][12]=arrReturn4[0][1];
					arrReturn2[0][13]=arrReturn4[0][2];
					arrReturn2[0][14]=arrReturn4[0][3];
			    } 
				
			   displayInsured(arrReturn2[0]);
			}
		
	     showCodeName();
	}

	
}

//王珑修改  