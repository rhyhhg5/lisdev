<%
//程序名称：ClientConjoinQuerySave.jsp
//程序功能：
//创建日期：2002-08-19
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%

  //接收信息，并作校验处理。
  //输入参数
  ClientConjoinQuerySchema tClientConjoinQuerySchema   = new ClientConjoinQuerySchema();
  ClientConjoinQueryUI tClientConjoinQueryUI   = new ClientConjoinQueryUI();

  //输出参数
  CErrors tError = null;
  String tBmCert = "";
  //后面要执行的动作：添加，修改，删除
  String transact = "";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
//    if(request.getParameter("CustomerNo").length()>0)
    tClientConjoinQuerySchema.setCustomerNo(request.getParameter("CustomerNo"));
//    if(request.getParameter("Password").length()>0)
    tClientConjoinQuerySchema.setPassword(request.getParameter("Password"));
//    if(request.getParameter("Name").length()>0)
    tClientConjoinQuerySchema.setName(request.getParameter("Name"));
//    if(request.getParameter("Sex").length()>0)
    tClientConjoinQuerySchema.setSex(request.getParameter("Sex"));
//    if(request.getParameter("Birthday").length()>0)
    tClientConjoinQuerySchema.setBirthday(request.getParameter("Birthday"));
//    if(request.getParameter("NativePlace").length()>0)
    tClientConjoinQuerySchema.setNativePlace(request.getParameter("NativePlace"));
//    if(request.getParameter("Nationality").length()>0)
    tClientConjoinQuerySchema.setNationality(request.getParameter("Nationality"));
//    if(request.getParameter("Marriage").length()>0)
    tClientConjoinQuerySchema.setMarriage(request.getParameter("Marriage"));
//    if(request.getParameter("MarriageDate").length()>0)
    tClientConjoinQuerySchema.setMarriageDate(request.getParameter("MarriageDate"));
//    if(request.getParameter("OccupationType").length()>0)
    tClientConjoinQuerySchema.setOccupationType(request.getParameter("OccupationType"));
//    if(request.getParameter("StartWorkDate").length()>0)
    tClientConjoinQuerySchema.setStartWorkDate(request.getParameter("StartWorkDate"));
//    if(request.getParameter("Salary").length()>0)
    tClientConjoinQuerySchema.setSalary(request.getParameter("Salary"));
//    if(request.getParameter("Health").length()>0)
    tClientConjoinQuerySchema.setHealth(request.getParameter("Health"));
//    if(request.getParameter("Stature").length()>0)
    tClientConjoinQuerySchema.setStature(request.getParameter("Stature"));
//    if(request.getParameter("Avoirdupois").length()>0)
    tClientConjoinQuerySchema.setAvoirdupois(request.getParameter("Avoirdupois"));
//    if(request.getParameter("CreditGrade").length()>0)
    tClientConjoinQuerySchema.setCreditGrade(request.getParameter("CreditGrade"));
//    if(request.getParameter("IDType").length()>0)
    tClientConjoinQuerySchema.setIDType(request.getParameter("IDType"));
//    if(request.getParameter("Proterty").length()>0)
    tClientConjoinQuerySchema.setProterty(request.getParameter("Proterty"));
//    if(request.getParameter("IDNo").length()>0)
    tClientConjoinQuerySchema.setIDNo(request.getParameter("IDNo"));
//    if(request.getParameter("OthIDType").length()>0)
    tClientConjoinQuerySchema.setOthIDType(request.getParameter("OthIDType"));
//    if(request.getParameter("OthIDNo").length()>0)
    tClientConjoinQuerySchema.setOthIDNo(request.getParameter("OthIDNo"));
//    if(request.getParameter("ICNo").length()>0)
    tClientConjoinQuerySchema.setICNo(request.getParameter("ICNo"));
//    if(request.getParameter("HomeAddressCode").length()>0)
    tClientConjoinQuerySchema.setHomeAddressCode(request.getParameter("HomeAddressCode"));
//    if(request.getParameter("HomeAddress").length()>0)
    tClientConjoinQuerySchema.setHomeAddress(request.getParameter("HomeAddress"));
//    if(request.getParameter("PostalAddress").length()>0)
    tClientConjoinQuerySchema.setPostalAddress(request.getParameter("PostalAddress"));
//    if(request.getParameter("ZipCode").length()>0)
    tClientConjoinQuerySchema.setZipCode(request.getParameter("ZipCode"));
//    if(request.getParameter("Phone").length()>0)
    tClientConjoinQuerySchema.setPhone(request.getParameter("Phone"));
//    if(request.getParameter("BP").length()>0)
    tClientConjoinQuerySchema.setBP(request.getParameter("BP"));
//    if(request.getParameter("Mobile").length()>0)
    tClientConjoinQuerySchema.setMobile(request.getParameter("Mobile"));
//    if(request.getParameter("EMail").length()>0)
    tClientConjoinQuerySchema.setEMail(request.getParameter("EMail"));
//    if(request.getParameter("BankCode").length()>0)
    tClientConjoinQuerySchema.setBankCode(request.getParameter("BankCode"));
//    if(request.getParameter("BankAccNo").length()>0)
    tClientConjoinQuerySchema.setBankAccNo(request.getParameter("BankAccNo"));
//    if(request.getParameter("JoinCompanyDate").length()>0)
    tClientConjoinQuerySchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
//    if(request.getParameter("Position").length()>0)
    tClientConjoinQuerySchema.setPosition(request.getParameter("Position"));
//    if(request.getParameter("GrpNo").length()>0)
    tClientConjoinQuerySchema.setGrpNo(request.getParameter("GrpNo"));
//    if(request.getParameter("GrpName").length()>0)
    tClientConjoinQuerySchema.setGrpName(request.getParameter("GrpName"));
//    if(request.getParameter("GrpPhone").length()>0)
    tClientConjoinQuerySchema.setGrpPhone(request.getParameter("GrpPhone"));
//    if(request.getParameter("GrpAddressCode").length()>0)
    tClientConjoinQuerySchema.setGrpAddressCode(request.getParameter("GrpAddressCode"));
//    if(request.getParameter("GrpAddress").length()>0)
    tClientConjoinQuerySchema.setGrpAddress(request.getParameter("GrpAddress"));
//    if(request.getParameter("DeathDate").length()>0)
    tClientConjoinQuerySchema.setDeathDate(request.getParameter("DeathDate"));
//    if(request.getParameter("Remark").length()>0)
    tClientConjoinQuerySchema.setRemark(request.getParameter("Remark"));
//    if(request.getParameter("State").length()>0)
    tClientConjoinQuerySchema.setState(request.getParameter("State"));
//    if(request.getParameter("BlacklistFlag").length()>0)
    tClientConjoinQuerySchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));
//    if(request.getParameter("Operator").length()>0)
    tClientConjoinQuerySchema.setOperator(request.getParameter("Operator"));

    transact=request.getParameter("Transact");

  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tClientConjoinQuerySchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tClientConjoinQueryUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tClientConjoinQueryUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = transact+" 成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = transact+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

