<%
//程序名称：TempFeeInput.jsp
//程序功能：财务收费的输入
//创建日期：2002-07-12 
//创建人  ：
//更新记录：  更新人     更新日期     更新原因/内容
%>
<!--%@include file="../common/jsp/UsrCheck.jsp"%-->
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<script>
	var cContNo = "<%=request.getParameter("ContNo")%>";	
    var cInsuredNo = "<%=request.getParameter("cInsuredNo")%>";
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var tContType = "<%=request.getParameter("ContType")%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>	
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	 <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="ProposalAutoMove.js"></SCRIPT> 	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="ContInsuredInput.js"></SCRIPT>
  <%@include file="ContInsuredInit.jsp"%>
  <%if(request.getParameter("scantype")!=null&&request.getParameter("scantype").equals("scan")){%>  
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  <%}%>   
</head>
<body  onload="initForm();initElementtype();" >
<Form action="./ContInsuredSave.jsp" method=post name=fm target="fraSubmit">
<input type=hidden id="fmAction" name="fmAction">
<input type=hidden id="ContType" name="ContType">
<input type=hidden id="GrpContNo" name="GrpContNo">

    <Div  id= "divFamilyType" style="display:'none'">
    <table class=common>
       <tr class=common>
          <TD  class= title>
            个人合同类型
          </TD>
          <TD  class= input>
            <Input class="code" name=FamilyType ondblclick="showCodeList('FamilyType',[this]);" onkeyup="return showCodeListKey('FamilyType',[this]);" onfocus="choosetype();">
          </TD>
    			<TD  class= title>
    			  投保单号码
    			</TD>
    			<TD  class= input>
    			  <Input class="common" readonly name=ProposalContNo >
    			</TD>
       </tr>
    </table>
    </Div>       
    <Input type= "hidden" class="common" readonly name=ContNo >

       
    <DIV id="divPrtNo" style="display:'none'">
    	<table class=common>
    		<TR>
    			<TD  class= title>
    			  印刷号码
    			</TD>
    			<TD  class= input>
    			  <Input class="common" readonly name=PrtNo verify="印刷号码|notnull" >
    			</TD>
     			<TD  class= title>
    			</TD>
    			<TD  class= input>
    			</TD>   			
    		</TR>
    	</table>
    </DIV>
    <Div  id= "divTempFeeInput" style= "display: ''">
    	<Table  class= common>
    		<tr>
    			<td text-align: left colSpan=1>
	  				<span id="spanInsuredGrid" >
	  				</span> 
	  			</td>
    		</tr>
    	</Table>
    </div>

   <%@include file="ComplexInsured.jsp"%> 
       
<DIV id=DivLCBody STYLE="display:''">
	<table>
		<tr>
			<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
			</td>
			<td class= titleImg>
			体检信息：
			</td>
		</tr>		
	</table>
</DIV>
 <Div  id= "divPEQuery" style= "display: ''">
    	<Table  class= common>
    		<tr>
    			<td text-align: left colSpan=1>
	  				<span id="spanPEGrid" >
	  				</span> 
	  			</td>
    		</tr>
    	</Table>
 </DIV>
<DIV id=DivLCImpart STYLE="display:'none'">
<!-- 告知信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
</td>
<td class= titleImg>
被保险人告知信息
</td>
</tr>
</table>

<div  id= "divLCImpart1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartGrid" >
</span>
</td>
</tr>
</table>
</div>
</DIV>
<DIV id=DivLCImpart STYLE="display:'none'">
<!-- 告知信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart2);">
</td>
<td class= titleImg>
被保险人告知明细信息
</td>
</tr>
</table>

<div  id= "divLCImpart2" style= "display: 'none'">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartDetailGrid" >
</span>
</td>
</tr>
</table>
</div>
</DIV>
<hr/>
<!-- 被保人险种信息部分 -->
<DIV id=DivLCPol STYLE="display:''">
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
</td>
<td class= titleImg>
被保险人险种信息
</td>
</tr>
</table>

<div  id= "divLCPol1" style= "display: ''">
	<table  class= common>
	<tr  class= common>
	<td text-align: left colSpan=1>
	<span id="spanPolGrid" >
	</span>
	</td>
	</tr>
	</table>
</div>
</DIV>
 <hr>
      
<Div  id= "divInputContButton" style= "display: ''" style="float: left">    
  <INPUT class=cssButton id="riskbutton1" VALUE="返  回" TYPE=button onclick="returnparent();"> 
  <INPUT class=cssButton id="riskbutton2" VALUE="险种明细" TYPE=button onclick="riskQueryClick();"> 
	   
</DIV>            
	<input type=hidden id="WorkFlowFlag" name="WorkFlowFlag">
	<INPUT  type= "hidden" class= Common name= MissionID value= ""><!-- 工作流任务编码 -->
  <INPUT  type= "hidden" class= Common name= SubMissionID value= "">
  <INPUT  type= "hidden" class= Common name= ProposalGrpContNo value= "">
  <INPUT  type= "hidden" class= Common name= AppntNo value= "">
  <INPUT  type= "hidden" class= Common name= AppntName value= "">
  <INPUT  type= "hidden" class= Common name= SelPolNo value= "">
  <INPUT  type= "hidden" class= Common name= SaleChnl value= "">
  <INPUT  type= "hidden" class= Common name= ManageCom value= "">
  <INPUT  type= "hidden" class= Common name= AgentCode value= "">
  <INPUT  type= "hidden" class= Common name= AgentGroup value= "">
  <INPUT  type= "hidden" class= Common name= CValiDate value= "">
  <input type=hidden name=BQFlag>
  <input type=hidden name=AddressNo>
</Form> 
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>