<%
//程序名称：OLCAcceptQuery.js
//程序功能：
//创建日期：2002-08-16 12:55:37
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  LCAcceptSchema tLCAcceptSchema   = new LCAcceptSchema();

  OLCAcceptUI tOLCAcceptQueryUI   = new OLCAcceptUI();
  //读取Session中的全局类
	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

    tLCAcceptSchema.setPrtNo(request.getParameter("PrtNo"));
    tLCAcceptSchema.setManageCom(request.getParameter("ManageCom"));
    tLCAcceptSchema.setRiskCode(request.getParameter("RiskCode"));
    tLCAcceptSchema.setRiskVersion(request.getParameter("RiskVersion"));
    tLCAcceptSchema.setGrpFlag(request.getParameter("GrpFlag"));
    tLCAcceptSchema.setMEflag(request.getParameter("MEflag"));
    tLCAcceptSchema.setPayMoney(request.getParameter("PayMoney"));
    tLCAcceptSchema.setAppntName(request.getParameter("AppntName"));
    tLCAcceptSchema.setInsuredPeoples(request.getParameter("InsuredPeoples"));
    tLCAcceptSchema.setAgentCode(request.getParameter("AgentCode"));
    tLCAcceptSchema.setAffixNum(request.getParameter("AffixNum"));
    tLCAcceptSchema.setAffixPageNum(request.getParameter("AffixPageNum"));



  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLCAcceptSchema);
	tVData.add(tG);

  FlagStr="";
  // 数据传输
	if (!tOLCAcceptQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " +  tError.getFirstError();
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tOLCAcceptQueryUI.getResult();
		
		// 显示
		LCAcceptSet mLCAcceptSet = new LCAcceptSet();
		mLCAcceptSet.set((LCAcceptSet)tVData.getObjectByObjectName("LCAcceptSet",0));
		int n = mLCAcceptSet.size();
		LCAcceptSchema mLCAcceptSchema;
		for (int i = 1; i <= n; i++)
		{
		  	mLCAcceptSchema = mLCAcceptSet.get(i);
		   	%>
		   	<script language="javascript">
        parent.fraInterface.AcceptGrid.addOne("AcceptGrid")
parent.fraInterface.fm.AcceptGrid1[<%=i-1%>].value="<%=mLCAcceptSchema.getPrtNo()%>";
parent.fraInterface.fm.AcceptGrid2[<%=i-1%>].value="<%=mLCAcceptSchema.getManageCom()%>";
parent.fraInterface.fm.AcceptGrid3[<%=i-1%>].value="<%=mLCAcceptSchema.getRiskCode()%>";
parent.fraInterface.fm.AcceptGrid4[<%=i-1%>].value="<%=mLCAcceptSchema.getRiskVersion()%>";
parent.fraInterface.fm.AcceptGrid5[<%=i-1%>].value="<%=mLCAcceptSchema.getAppntName()%>";
parent.fraInterface.fm.AcceptGrid6[<%=i-1%>].value="<%=mLCAcceptSchema.getPayMoney()%>";

			</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (!FlagStr.equals("Fail"))
  {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

