<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-12-19 11:10:36
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String tPayNo = "";
	try
	{
		tPayNo = request.getParameter("PayNo");
	}
	catch( Exception e )
	{
		tPayNo = "";
	}
	String tSumActuPayMoney = "";
	try 
	{
		tSumActuPayMoney = request.getParameter("SumActuPayMoney");
		System.out.println(tSumActuPayMoney);
	}
	catch( Exception e )
	{
		tSumActuPayMoney = "";
	}
%>
<head >
<script> 
	var tPayNo = "<%=tPayNo%>";  
	var tSumActuPayMoney = "<%=tSumActuPayMoney%>";
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="AllFeeQueryGDetail.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="AllFeeQueryGDetailInit.jsp"%>
	<title>交费明细查询 </title>
</head>

<body  onload="initForm();easyQueryClick();" >
  <form  name=fm >
  <table >
    	<tr>
    	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJAPay1);">
    	</td>
			<td class= titleImg>
				费用整体信息
			</td>
		</tr>
	</table>
	<Div  id= "divLJAPay1" style= "display: ''">
    <table  class= common align=center>
      	<TR>
        <TD  class= title>
            集体保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpPolNo >
          </TD>
          <TD  class= title>
            总单/合同号码
          </TD>
          <TD  class= input>
            <Input class= common name=ContNo >
          </TD>
          <TD  class= title>
            交费收据号码
          </TD>
          <TD  class= input>
            <Input class= common name=PayNo >
          </TD>
          </TR>
      	<TR  class= common>
          
          <TD  class= title>
            总实交金额
          </TD>
          <TD  class= input>
            <Input class= common name=SumActuPayMoney >
          </TD>
        </TR>
     </table>
  </Div>
  
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJAPPerson1);">
    		</td>
    		<td class= titleImg>
    			 费用详细信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLJAPPerson1" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button class= cssbutton onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button class= cssbutton onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button class= cssbutton onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" TYPE=button class= cssbutton onclick="turnPage.lastPage();">				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script>
<!--
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select PolNo,PayAimClass,SumActuPayMoney,PayIntv,PayDate,CurPayToDate,PayType,ContNo,GrpPolNo from LJAPayPerson where PayNo='" + tPayNo + "'";			 
	//alert(strSQL);
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
	//对上述控件名称进行附值
	var tGrpPolNo=PolGrid. getRowColData(1,9);
	var tContNo=PolGrid. getRowColData(1,8);
	
	fm.GrpPolNo.value = tGrpPolNo;
	fm.ContNo.value = tContNo;
	
	}
/*
function showOtherInfo()
{
		var tGrid7 [] = request.getParameterValues("PolGrid7"); 
    var tGrid8 [] = request.getParameterValues("PolGrid8");
    var tGrid9 [] = request.getParameterValues("PolGrid9");
    var yGrpPolNo = tGrid7[1];
    var yContNo = tGrid8[1];
    var yPayType = tGrid9[1];
    alert("集体保单号码是"+yGrpPolNo);
    fm.GrpPolNo.value = yGrpPolNo;
    fm.ContNo.value = yContNo;
    fm.PayType.value = yPayType;
} 
*/
</script>-->
</html>


