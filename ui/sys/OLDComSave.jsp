<%
//程序名称：OLDComInput.jsp
//程序功能：
//创建日期：2002-08-16 17:44:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LDComSchema tLDComSchema   = new LDComSchema();

  OLDComUI tOLDCom   = new OLDComUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLDComSchema.setComCode(request.getParameter("ComCode"));
    tLDComSchema.setOutComCode(request.getParameter("OutComCode"));
    tLDComSchema.setName(request.getParameter("Name"));
    tLDComSchema.setShortName(request.getParameter("ShortName"));
    tLDComSchema.setAddress(request.getParameter("Address"));
    tLDComSchema.setZipCode(request.getParameter("ZipCode"));
    tLDComSchema.setPhone(request.getParameter("Phone"));
    tLDComSchema.setFax(request.getParameter("Fax"));
    tLDComSchema.setEMail(request.getParameter("EMail"));
    tLDComSchema.setWebAddress(request.getParameter("WebAddress"));
    tLDComSchema.setSatrapName(request.getParameter("SatrapName"));
    tLDComSchema.setSign(request.getParameter("Sign"));


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLDComSchema);
	tVData.add(tG);
  try
  {
    tOLDCom.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tOLDCom.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

