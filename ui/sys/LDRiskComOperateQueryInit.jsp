<%
//程序名称：LDRiskComOperateQuery.js
//程序功能：
//创建日期：2003-10-28 15:15:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
 
  }
  catch(ex)
  {
    alert("在LDRiskComOperateQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
 
  }
  catch(ex)
  {
    alert("在LDRiskComOperateQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
   
  try
  {
     
     initLDRiskComOperateGrid();
    
  }
  catch(re)
  {
    alert("LDRiskComOperateQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化LDRiskComOperateGrid
 ************************************************************
 */
var LDRiskComOperateGrid;          //定义为全局变量，提供给displayMultiline使用
function initLDRiskComOperateGrid()
  {  
                         
    var iArray = new Array();     
      try
      {
        
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=0; 
        
        iArray[1]=new Array();
        iArray[1][0]="险种编码";
        iArray[1][1]="80px";
        iArray[1][2]=100;
        iArray[1][3]=2; 
        iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
        iArray[1][5]="4";              	                //引用代码对应第几列，'|'为分割符
        iArray[1][9]="险种编码|code:RiskCode&NOTNULL";
        iArray[1][18]=250;
        iArray[1][19]= 0 ;
        
        iArray[2]=new Array();
        iArray[2][0]="管理机构";
        iArray[2][1]="120px";
        iArray[2][2]=100;
        iArray[2][3]=2; 
        iArray[2][4]="station";              	        //是否引用代码:null||""为不引用
        iArray[2][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[2][9]="管理机构|code:station&NOTNULL";
        iArray[2][18]=250;
        iArray[2][19]= 0 ;
        
        iArray[3]=new Array();
        iArray[3][0]="授权机构";
        iArray[3][1]="120px";
        iArray[3][2]=100;
        iArray[3][3]=2; 
        iArray[3][4]="station";              	        //是否引用代码:null||""为不引用
        iArray[3][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[3][9]="授权机构|code:station&NOTNULL";
        iArray[3][18]=250;
        iArray[3][19]= 0 ;
        
        iArray[4]=new Array();
        iArray[4][0]="功能标识";
        iArray[4][1]="80px";
        iArray[4][3]=2; 
        iArray[4][4]="operatetype";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]="功能标识|code:operatetype&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;
        
        
        iArray[5]=new Array();
        iArray[5][0]="功能描述";
        iArray[5][1]="200px";
        iArray[5][2]=100;
        iArray[5][3]=0; 
        
  
        LDRiskComOperateGrid = new MulLineEnter( "fm" , "LDRiskComOperateGrid" ); 
        //这些属性必须在loadMulLine前
        LDRiskComOperateGrid.mulLineCount = 10;   
        LDRiskComOperateGrid.displayTitle = 1;
        LDRiskComOperateGrid.canSel=1;
        LDRiskComOperateGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化LDRiskComOperateGrid时出错："+ ex);
      }
    }
</script>
