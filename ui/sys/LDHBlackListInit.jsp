<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

if(globalInput == null) {
	out.println("session has expired");
	return;
}

String strOperator = globalInput.Operator;
%>
<script language="JavaScript">

var tAgentTitleGrid = "业务员";

function initInpBox()
{
  try
  {
    fm.all('BlacklistNo').value = '';
    fm.all('Type').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('IDType').value = '';
    fm.all('IDNo').value = '';
    fm.all('Birthday').value = '';
    fm.all('PublishCom').value = '';
    fm.all('Cause').value = '';
    fm.all('Remark').value = '';
    fm.all('Nationality').value = '';
    
  }
  catch(ex)
  {
    alert("在LDHBlackListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initUpdate();
   
  }
  catch(re)
  {
    alert("LDHBlackListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>
