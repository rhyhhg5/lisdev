<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
	//程序名称：LAAgentInput.jsp
	//程序功能：个人代理增员管理
	//创建日期：2002-08-16 15:39:06
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String transact = request.getParameter("transact");
    String tBlacklistCode = request.getParameter("BlacklistCode");
	%>
	<script type="text/javascript">
	var transact = "<%=transact%>";
	var tBlacklistCode = "<%=tBlacklistCode%>";
	</script>
	<%@page contentType="text/html;charset=GBK" %>
	<head >
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="BlackListGroupInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="BlackListGroupInit.jsp"%>
	</head>
	<body  onload="initForm();initElementtype();" >
	<form action="./BlackListPersonSave.jsp" method=post name=fm target="fraSubmit">
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
					<td class=titleImg>
						黑名单组织信息
					</td>
				</td>
			</tr>
		</table>
		<table  class= common>
			<TR  class= common>
				<TD class= title>
					黑名单组织编号
				</TD>
				<TD  class= input>
					<Input class= 'readonly' readonly name=BlacklistCode >
				</TD>
				<TD  class= title>
					名称
				</TD>
				<TD  class= input>
					<Input name=Name class= common  elementtype=nacessary>
				</TD>
				<TR  class= common>
				<TD  class= title>
					曾用名
				</TD>
				<TD  class= input>
					<Input name=Name1 class= common >
				</TD>
				<TD  class= title>
					风险等级
				</TD>
				<TD  class= input>
					<Input name=Name8 class= common >
				</TD>
				</TR>
				<TR  class= common>
				<TD  class= title>
					发布机构
				</TD>
				<TD  class= input>
					<Input name=Name9 class= common >
				</TD>
				<TD  class= title>
					类别
				</TD>
				<TD  class= input>
					<Input name=Name10 class= common >
				</TD>
				</TR>
		</table>
	    <table class= common>
	        <TR class= common>
				<TD class= title>备注</TD>
			</TR>					
			<TR  class= common>
			  	<TD  class= input>
		    		<textarea name=Remark cols="85%" rows="5"  class="common"></textarea>
			    </TD>
	  		</TR>
     	</table>
      <INPUT VALUE="保  存" TYPE=button onclick="submitForm();" class=cssbutton >
      <INPUT VALUE="修  改" TYPE=button onclick="updateClick();" class=cssbutton >
      <input type=hidden name=Type value='1'>
      <input type=hidden id="fmtransact" name="fmtransact">
      
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>