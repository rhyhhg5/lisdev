<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

function initForm()
{
  try
  {
	//体检信息
    initPEGrid();
	queryPE();

	//被保人信息
	queryInsuredNo();

	//保障信息
	initPolGrid();
	setGrpValue();

	//理赔信息
	//claimQuery();
  }
  catch(re)
  {
    alert("GroupPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//被保人体检信息列表初始化
function initPEGrid(){
    var iArray = new Array();
      
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		        //列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="体检时间";         		//列名
      iArray[1][1]="60px";            		        //列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
          
      iArray[2]=new Array();
      iArray[2][0]="体检费用";         		//列名
      iArray[2][1]="60px";            		        //列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

	  iArray[3]=new Array();
      iArray[3][0]="业务号";         		//列名
      iArray[3][1]="60px";            		        //列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
  
      PEGrid = new MulLineEnter( "fm" , "PEGrid" ); 
      PEGrid.mulLineCount = 0;   
      PEGrid.displayTitle = 1;
      PEGrid.canSel =1;
      PEGrid. hiddenPlus=1;
      PEGrid. hiddenSubtraction=1;
      PEGrid.loadMulLine(iArray);  
    }
    catch(ex) {
      alert("ContInsuredQueryInit.jsp-->iinitPolGrid函数中发生异常:初始化界面错误!");
    }
}
//团单保障信息
function initPolGrid()
{     
	 var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[2]=new Array();
      iArray[2][0]="投保时间";         		    //列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="险种";         		//列名
      iArray[3][1]="130px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[4]=new Array();
      iArray[4][0]="险种号码";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[5]=new Array();
      iArray[5][0]="档次";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="保额";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="保费";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="生效时间";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;       
    
	  iArray[9]=new Array();
      iArray[9][0]="终止时间";         		//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0; 

	  iArray[10]=new Array();
      iArray[10][0]="原因";         		//列名
      iArray[10][1]="50px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0; 

      iArray[11]=new Array();
      iArray[11][0]="险种状态";         		//列名
      iArray[11][1]="50px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0; 

      iArray[12]=new Array();
      iArray[12][0]="失效原因";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0;
      
      iArray[13]=new Array();
      iArray[13][0]="交至日期";         		//列名
      iArray[13][1]="80px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[14]=new Array();
      iArray[14][0]="保单险种号";         		//列名
      iArray[14][1]="80px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[15]=new Array();
      iArray[15][0]="险种序号";         		//列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
	  PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.canSel =1;
      PolGrid. hiddenPlus=1;
      PolGrid. hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ClientGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("ContInsuredQueryInit.jsp-->initPolGrid函数中发生异常:初始化界面错误!");
      }
}
</script>