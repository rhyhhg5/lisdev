<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-12-17 15:01:19
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LOGrpPolDataInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LOGrpPolDataInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LOGrpPolDataSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLOGrpPolDataGrid1);">
    		</td>
    		 <td class= titleImg>
        		 团体保单数据维护
       		 </td>   		 
    	</tr>
    </table>
  <Div  id= "divLOGrpPolDataGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      保单号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PolNo maxlength=20 verify="保单号码|num&len=20&notnull">
    </TD>
    <TD  class= title>
      约定执行日期
    </TD>
    <TD  class= input>
	  <input class="coolDatePicker" dateFormat="short" name="SpecExeDate" verify="约定执行日期|NOTNULL" >
    </TD>	
  </TR>
  <TR  class= common>
    <TD  class= title>
      类型
    </TD>
    <TD  class= input>
      <Input class= 'codeno' verify="类型|len=1&notnull"  CodeData="0|2^0|分红比例^1|退保比例" name="PolType" ondblclick="return showCodeListEx('poltype;',[this,PolTypeName],[0,1]);" ><input class = codename name =PolTypeName>
    </TD>
    <TD  class= title>
      比例
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Rate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      保单年度末
    </TD>
    <TD  class= input>
      <Input class= 'common' verify="保单年度末|notnull" name=PolCurYear >
    </TD>
    <TD  class= title>
      状态
    </TD>
    <TD  class= input>
      <Input class= 'codeno'  verify="状态|len=1" name=State CodeData="0|^0|失效^1|有效" ondblclick="return showCodeListEx('statetype;',[this,StateName],[0,1]);"><input class = codename name =StateName>
    </TD>
  </TR>
</table>
  </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
