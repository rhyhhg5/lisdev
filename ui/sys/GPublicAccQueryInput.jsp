<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：GPublicAccQueryInput.jsp
//程序功能：投连万能账户查询
//创建日期：2007-08-24
//创建人  ：guoly
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  String GrpContNo=request.getParameter("GrpContNo");
  String PrtNo=request.getParameter("PrtNo");
%>
<head >
<script>
var tGrpContNo = "<%=GrpContNo%>"; 
var tPrtNo="<%=PrtNo%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="GPublicAccQueryInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="GPublicAccQueryInit.jsp"%>
<title>万能账户信息查询 </title>
</head>
<body  onload="initForm();" >
<form name=fm  target=fraSubmit method=post>
<Div id="divBussChose" style="display:'none'">
  <table class=common width=90%>
    <tr>
      <TD class=title>公共账户信息查询<input type="radio" name="QueryType" id=QueryType_0 value="0" onclick="chooseQueryType()" ></TD>
      <TD class=title>个人账户信息查询<input type="radio" name="QueryType" id=QueryType_1 checked value="1" onclick="chooseQueryType()" ></TD>
    </tr>
  </table>
</Div>
<Div id="divGroupPol1" style="display: ''">
<table class=common>
   <TR class=common>
      <TD class=title8>保单号码</TD>
          <TD class=input8>
            <Input class=readonly name=GrpContNo readonly >
          </TD>

      <TD class=title8>投保单位名称</TD>
          <TD class=input8>
            <input class=readonly name=GrpName readonly=true >
          </TD>
      <TD class=title8>印刷号</TD>
          <TD class=input8>
            <Input class=readonly name=PrtNo readonly >
          </TD>
   </TR>
    <TR class=common>
      <TD class=title8>生效日期</TD>
          <TD class=input8>
            <Input class=readonly name=CValiDate readonly >
          </TD>

      <TD class=title8>服务代理人</TD>
          <TD class=input8>
            <input class=readonly name=AgentInfo readonly=true >
          </TD>
      <TD class=title8>被保人数</TD>
          <TD class=input8>
            <Input class=readonly name=Peoples readonly >
          </TD>
   </TR>   
  </table>
<DIV id=ShowPublicAcc STYLE="display:''">
<table id="table1">
	<tr>
          <td class="titleImg">团险公共账户信息</td>
   </tr>
</table>
</DIV> 
<DIV id=ShowInsuredAcc STYLE="display:'none'">
<table id="table1">
	<tr>
          <td class="titleImg">团险个人账户信息</td>
        </tr>
</table>
 <Div  id= "divLAAgent1" style= "display: ''">
    <table  class= common>
    	<TR class=common>
    	<TD class=title>被保险人姓名</TD>
       <TD class=input>
          <input class="common" name=InsuredName>
       </TD>
         	<TD  class= title>
       被保人性别
    </TD>
        <TD  class= input>
        <Input class="codeno" name=InsuredSex verify="性别|code:Sex" CodeData= "0|^0|男^1|女" ondblclick="return showCodeListEx('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input name=SexName class=codename readonly=true >
    </TD>
    	    <td CLASS="title">客户号码 </td>    
       <td CLASS="input">
           <input NAME="InsuredNo"  CLASS="common" style="width:180px">
       </td>
        
    </tr>
    <TR class=common>
        <TD class=title>证件类型</td>
  	    <td class=input>
  			   <input class="codeno"  name=IDType CodeData="0|^0|身份证^1|护照^2|军官证^3|工作证^4|其它"  ondblclick="return showCodeListEx('idtype', [this,IDTypeName],[0,1]);" onKeyUp="showCodeListKeyEx('idtype',[this,IDTypeName],[0,1]);"><input name=IDTypeName class=codename  readonly=true>
  	    </td>
      
       <td CLASS="title">证件号码</td>
     <td CLASS="input">
        <input NAME="IDNo"  CLASS="common">
     </td>
 
     <TD class=title>个人保单号码</TD>
       <TD class=input>
          <input class="common" name=ContNo>
       </TD>
    </tr>
      <tr  STYLE="display:'none'">
<td  class= title>统计起期</td>
         <TD  class= input> <Input class="coolDatePicker" dateFormat="short"  style="width:160px" name=StartDate verify="统计起期|DATE" ></TD>
         <td  class= title>统计止期</td>
         <TD  class= input><Input class="coolDatePicker" dateFormat="short"  style="width:160px" name=EndDate verify="统计止期|DATE"></TD>
</TR>  
 </tr>
    </table>
  </div>
  <input class=cssbutton type="button" align=lift value=" 个人账户信息查询 " onclick="queryClick()">
   <input class=cssbutton type="button" align=lift value=" 公共账户信息查询 " onclick="queryPubClick()">
</DIV>
</div>   
<DIV id=ShowAccInfo STYLE="display:''">
	<table id="table1">
	<tr>
          <td>
            <img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ShowAccBaseInfo);">
          </td>
          <td class="titleImg">保险账户信息</td>
        </tr>
    </table> 
    <DIV id=ShowAccBaseInfo STYLE="display:''">
    <table class=common>        
       <tr class=common>
            <td text-align: left colSpan=6 id="AgentGrid">
              <span id="spanAccGrid"></span>
            </td>
       </tr>
    </table>
    <center>
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">           
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">   
   </center>
  </div>
</div>
</div>
     
<Div id="divGroupPol2" style="display: 'none'">
	<table>   
<td class="titleImg" type=hidden>账户合计</td>
</table>
<table class=common>
   <TR>
          <TD class=title8>公共账户累计金额</TD>
          <TD class=input8>
            <Input class=readonly name=AccTotal readonly>
          </TD>
          <TD class=input8>
          </td>
          <TD class=input8>
          </TD>
          <!--<TD class=title8>公共账户对应红利账户金额</TD>
          <TD class=input8>-->
            <Input class=readonly type=hidden name=AccHTotal  readonly >
          <!--</TD>-->
    </TR>
    <TR>
          <!--<TD class=title8>所有个人账户单位交费部分累计金额合计</TD>
          <TD class=input8>-->
            <Input class=readonly type=hidden name=PAccBPTotal readonly>
          <!--</TD>-->
          <!--<TD class=title8>个人账户单位交费部分对应红利账户累计金额合计</TD>
          <TD class=input8>-->
            <Input name=HAccBPTotal type=hidden class=readonly readonly>
   </TR>
   <TR>
          <!--<TD class=title8>所有个人账户个人交费部分累计金额合计</TD>
          <TD class=input8>-->
            <Input class=readonly type=hidden name=PAccPPTotal readonly>
          <!--</TD>-->
          <!--<TD class=title8>个人账户个人交费部分对应红利账户累计金额合计</TD>
          <TD class=input8>-->
            <Input name=HAccPPTotal type=hidden class=readonly readonly>
          <!--</TD>-->
   </TR>
</table>
</div>
    
<Div id="divGroupPol3" style="display: 'none'">    
<table>
<td class="titleImg">保单账户信息</td>
</table>
<table class=common>
   <TR class=common>
          <TD class=title8>单位交费部分累计金额合计</TD>
          <TD class=input8>
            <Input class=readonly name=BPayTotal readonly>
          </TD>
          <TD class=title8>单位交费部分对应红利账户累计金额合计</TD>
          <TD class=input8>
            <Input name=BPayHAcc  class=readonly readonly>
          </TD>
   </tr>
   <TR class=common>
          <TD class=title8>个人交费部分累计金额合计</TD>
          <TD class=input8>
            <Input class=readonly  name=PPayTotal readonly>
          </TD>
          <TD class=title8>个人交费部分对应红利账户累计金额合计</TD>
          <TD class=input8>
            <Input name=PPayHAcc  class=readonly readonly>
          </TD>
   </tr>
</table>
</div>
<Div id="divAccClassInfo" style="display: 'none'"> 
<table id="table1">
	<tr>
          <td>
            <img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divShowAccClass);">
          </td>
          <td class="titleImg">账户分类信息</td>
        </tr>
    </table> 
<Div id="divShowAccClass" style="display: ''">  
<table class=common>
   <TR class=common>
            <td text-align: left colSpan=6 id="ClassGrid">
              <span id="spanAccClassGrid"></span>
            </td>
       </tr>
</table> 
 <center>
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">           
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">   
   </center>
</div> 
</div> 
<Div id="divAccTraceInfo" style="display: 'none'">  
<table id="table1">
	<tr>
          <td>
            <img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divShowAccTrace);">
          </td>
          <td class="titleImg">账户轨迹信息</td>
        </tr>
    </table> 
<Div id="divShowAccTrace" style="display: ''">  
<table class=common>
   <TR class=common>
            <td text-align: left colSpan=6 id="TraceGrid">
              <span id="spanAccTraceGrid"></span>
            </td>
       </tr>
</table>
 <center>
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">           
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">   
   </center> 
</div>  
</div>
 <input type=hidden   name = "CurrentDate">
  <br>
  <INPUT class=cssButton id="Donextbutton9" VALUE="返回上级" TYPE=button onclick="top.close();">
 </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>