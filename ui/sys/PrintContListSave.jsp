<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PrintContListSave.jsp
//程序功能：
//创建日期：2007-3-16 11:07
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>

<%@page import="com.sinosoft.lis.pubfun.*" %>
<%
  boolean operFlag = true;
  String flagStr = "";
  String content = "";
  String outname = "";
  String outpathname = "";
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  String contType = request.getParameter("ContType");
  
  String sql = null;
  String templateName = null;
  
  TransferData tTransferData = new TransferData();
  
  if("Vali".equals(contType))
  {
    templateName = "PrtContListVali.vts";
    sql = request.getParameter("ContSql");
  }
  else
  {
    templateName = "PrtContListInVali.vts";
    sql = request.getParameter("InvaliContSql");
  }
  
  tTransferData.setNameAndValue(PrtCoreBL.TemplateName, templateName);
  tTransferData.setNameAndValue(PrtCoreBL.SQL, sql);
  
  TextTag tTextTag = new TextTag();
  tTextTag.add("ContNo", request.getParameter("ContNo"));
  tTextTag.add("PrtNo", request.getParameter("PrtNo"));
  tTextTag.add("StateFlag", request.getParameter("StateFlag"));
  tTextTag.add("StateFlagName", StrTool.unicodeToGBK(request.getParameter("StateFlagName")));
  tTextTag.add("ContKind", request.getParameter("ContKind"));
  tTextTag.add("ContKindName", StrTool.unicodeToGBK(request.getParameter("ContKindName")));
  tTextTag.add("ManageCom", request.getParameter("ManageCom"));
  tTextTag.add("ManageComName", StrTool.unicodeToGBK(request.getParameter("ManageComName")));
  tTextTag.add("Group03", request.getParameter("Group03"));
  tTextTag.add("Group03Name", StrTool.unicodeToGBK(request.getParameter("Group03Name")));
  tTextTag.add("AgentCode", request.getParameter("AgentCode"));
  tTextTag.add("AgentName", request.getParameter("AgentName"));
  tTextTag.add("RiskCode1", request.getParameter("RiskCode1"));
  tTextTag.add("RiskCode1Name", StrTool.unicodeToGBK(request.getParameter("RiskCode1Name")));
  tTextTag.add("RiskCode2", request.getParameter("RiskCode2"));
  tTextTag.add("RiskCode2Name", StrTool.unicodeToGBK(request.getParameter("RiskCode2Name")));
  tTextTag.add("SaleChnlCode", request.getParameter("SaleChnlCode"));
  tTextTag.add("SaleChnlName", StrTool.unicodeToGBK(request.getParameter("SaleChnlName")));
  tTextTag.add("AgentComBank", request.getParameter("AgentComBank"));
  tTextTag.add("AgentComBankName", StrTool.unicodeToGBK(request.getParameter("AgentComBankName")));
  tTextTag.add("ApplyDateStart", request.getParameter("ApplyDateStart"));
  tTextTag.add("ApplyDateEnd", request.getParameter("ApplyDateEnd"));
  tTextTag.add("PayToDateStart", request.getParameter("PayToDateStart"));
  tTextTag.add("PayToDateEnd", request.getParameter("PayToDateEnd"));
  

  
  
  
  VData d = new VData();
  d.add(tG);
  d.add(tTransferData);
  d.add(tTextTag);
         
  PrtCoreUI ui = new PrtCoreUI();
  XmlExport txmlExport = ui.getXmlExport(d, "");
  System.out.println("\n\n\n\n\nsssssssss" + sql);
  if(txmlExport == null)
  {
    operFlag = false;
    content = ui.mErrors.getFirstError().toString();                 
  }
  
	if (operFlag==true)
	{
//	  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
//    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();  
//    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
//    tcombineVts.output(dataStream);  
//    session.putValue("PrintVts", dataStream);
//	  session.putValue("PrintStream", txmlExport.getInputStream());
//		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");

	          Readhtml rh=new Readhtml();
	          System.out.println(txmlExport.getInputStream());
			  rh.XmlParse(txmlExport.getInputStream()); //相当于XmlExport.getInputStream();
			  
			  String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
			  String temp=realpath.substring(realpath.length()-1,realpath.length());
			  if(!temp.equals("/"))
			  {
				  realpath=realpath+"/"; 
			  }
			  String templatename=rh.getTempLateName();//模板名字
			  String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
			  System.out.println("*********************templatepathname= " + templatepathname);
			  System.out.println("************************realpath="+realpath);
			  String date=PubFun.getCurrentDate().replaceAll("-","");
			  String time=PubFun.getCurrentTime().replaceAll(":","");
			  outname="清单打印"+tG.Operator+date+time+".xls";
			  outpathname=realpath+"vtsfile/"+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
				System.out.println("outpathname"+outpathname);
				System.out.println("templatepathname为空？"+templatepathname);
				System.out.println("**++**"+realpath+"f1print/picctemplate/");
				System.out.println("templatename："+templatename);
			  rh.setReadFileAddress(templatepathname);
			  rh.setWriteFileAddress(outpathname);
			  rh.start("vtsmuch");
			  try {
					outname = java.net.URLEncoder.encode(outname, "UTF-8");
					outname = java.net.URLEncoder.encode(outname, "UTF-8");
					outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
					outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	else
	{
    	flagStr = "Fail";
    	}
%>

<a href="../f1print/download.jsp?filename=<%=outname%>&filenamepath=<%=outpathname%>">点击下载</a>