<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%  
 
//得到界面的调用位置,默认为1,表示个人保单直接录入.
// 1 -- 个人投保单直接录入
// 2 -- 团体下个人投保单录入
// 3 -- 个人投保单明细查询
// 4 -- 团体复核
// 5 -- 复核
// 6 -- 查询
// 7 -- 保全新保加人
// 8 -- 保全新增附加险
// 9 -- 无名单补名单
// 10-- 浮动费率
// 13-- 团单复核修改
// 14-- 团单核保修改
// 16-- 团单明细查询
// 99-- 随动定制

	String tLoadFlag = "";
	String tGrpContNo = "";
	
	try
	{
		tLoadFlag = request.getParameter( "LoadFlag" );
		tGrpContNo = request.getParameter( "GrpContNo" );
		//默认情况下为个人保单直接录入
		if( tLoadFlag == null || tLoadFlag.equals( "" ))
			tLoadFlag = "2";//LoadFlag本身的 意义关键就在于个单部分
	}
	catch( Exception e1 )
	{
		tLoadFlag = "2";
	}
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
        System.out.println("LoadFlag:" + tLoadFlag);
         System.out.println("扫描类型:" + request.getParameter("scantype"));       
%> 
<script>
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var polNo = "<%=request.getParameter("polNo")%>";
	var cContType = "<%=request.getParameter("ContType")%>";
	var scantype = "<%=request.getParameter("scantype")%>";
	var MissionID = "<%=request.getParameter("MissionID")%>";
	var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
	var ManageCom = "<%=request.getParameter("ManageCom")%>";
	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
	var Resource = "<%=request.getParameter("Resource")%>";
        var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
        if (ScanFlag == null||ScanFlag=="null") 
        ScanFlag="0";
 	//用于无名单补名单
  var oldContNo ="<%=request.getParameter("oldContNo")%>";
	if (polNo == "null") polNo = "";
	if (prtNo == "null") prtNo = "";
	var LoadFlag ="<%=tLoadFlag%>";
	var tLoadFlag ="<%=tLoadFlag%>";
	//保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
	var BQFlag = "<%=request.getParameter("BQFlag")%>";
	if (BQFlag == null||BQFlag=="null") 
	BQFlag = "0";
	//保全调用会传险种过来
	var BQRiskCode = "<%=request.getParameter("riskCode")%>";
  var tsql=" 1 and code in (select code from ldcode where codetype=#grppaymode#) ";
  		
</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ContPolInit.jsp"%>
  <SCRIPT src="ContPolLCImpart.js"></SCRIPT> <!--保障状况告知 ＆ 健康状况告知 引用函数-->
  <SCRIPT src="ContPolInput.js"></SCRIPT>
  <SCRIPT src="ProposalAutoMove.js"></SCRIPT>
  <%if(request.getParameter("scantype")!=null&&request.getParameter("scantype").equals("scan")){%>
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  <%}%> 
</head>

<body  onload="initForm();initElementtype();" >
  <form action="./ContPolSave.jsp" method=post name=fm target="fraSubmit">

    <!-- 合同信息部分 GroupPolSave.jsp-->
  <DIV id=DivLCContButton STYLE="display:''">
  <table id="table1">
  		<tr>
  			<td>
  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroupPol1);">
  			</td>
  			<td class="titleImg">内控作业栏
  			</td>
  		</tr>
  </table>
</DIV>
     
    <Div  id= "divGroupPol1" style= "display: ''">
      <table  class= common>
      	<tr CLASS="common">
			<td CLASS="title">保单状态2 </td>    		       
			<td CLASS="input" COLSPAN="1">
			  <input NAME="AppFlag" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20"READONLY="true">
      </td>
			<td CLASS="title">最后处理人</td>    		         
			<td CLASS="input" COLSPAN="1">
			<input NAME="Operator" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="14" READONLY="true"></td>
    	<td CLASS="title">最后处理时间</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="ModifyDate" VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    	</td>
			</tr>
        <TR  class= common>	 
          <!--TD  class= title8>
            团体投保单号码
          </TD>
          <TD  class= input8>
            <Input class="common" name=ProposalGrpContNo readonly TABINDEX="-1"  MAXLENGTH="40">
          </TD-->
          <TD  class= title8>
            印刷号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=PrtNo elementtype=nacessary TABINDEX="-1" MAXLENGTH="11" verify="印刷号码|notnull&len=11" >
          </TD>
          <TD  class= title8>
            管理机构
          </TD>
          <TD  class= input8>
            <!--<Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">-->
            <Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
          </TD>
          <TD  class= title8>
            销售渠道
          </TD> 
          <TD  class= input8>
            <!--<Input class="readonly" readonly name=SaleChnl verify="销售渠道|code:SaleChnl&notnull" >-->
          	<Input class=codeNo name=SaleChnl verify="销售渠道|notnull" ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>
          </TD>
          
        </TR>
        <!--tr>
            <td colspan="6"><font color="red">对于非银行渠道的交叉销售业务，销售渠道选择“中介”，交叉销售渠道选择“财代健”或者“寿代健”；对于银行渠道的交叉销售，则填写“银行代理”渠道</font></td>
        </tr>
        <tr>
            <td colspan="6"><font color="red">以下“交叉销售渠道”，“交叉销售业务类型”，“对方机构代码”，“对方业务员代码”，“对方业务员姓名”信息，仅集团交叉销售业务需要填写。</font></td>
        </tr-->
        <tr>
            <td colspan="6"><font color="black">如果是交叉销售,请选择</font><INPUT TYPE="checkbox" NAME="MixComFlag" onclick="isMixCom();"></td>
        </tr>
        <tr class="common8" id="GrpAgentComID" style="display: none">
            <td class="title8">交叉销售渠道</td>
            <td class="input8">
                <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" elementtype=nacessary/>
            </td>
            <td class="title8">交叉销售业务类型</td>
            <td class="input8">
                <input class="codeNo" name="Crs_BussType" id="Crs_BussType" verify="交叉销售业务类型|code:crs_busstype" ondblclick="return showCodeList('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_BussTypeName" readonly="readonly" elementtype=nacessary/>
            </td>
            <td class="title8">&nbsp;</td>
            <td class="input8">&nbsp;</td>
        </tr>	
		<tr class=common id="GrpAgentTitleID" style="display: 'none'">
    		<td CLASS="title" >对方机构代码</td>    		
			<td CLASS="input" COLSPAN="1" >
    	    <Input class="code" name="GrpAgentCom" elementtype=nacessary  ondblclick="return queryGrpAgentCom();">
    	    </td>   	    
    	    <td  class= title>对方机构名称</td>
	        <td  class= input>
	          <Input class="common" name="GrpAgentComName" elementtype=nacessary  TABINDEX="-1"  readonly>
	        </td>  
			<td CLASS="title">对方业务员代码</td>
    		<td CLASS="input" COLSPAN="1">
			<input NAME="GrpAgentCode" VALUE MAXLENGTH="20" CLASS="code" elementtype=nacessary ondblclick="return queryGrpAgent();" >
    		</td>
        </tr>
   	 	<tr class=common id="GrpAgentTitleIDNo" style="display: 'none'">
            <td  class="title" >对方业务员姓名</td>
	        <td  class="input" COLSPAN="1">
	            <Input  name=GrpAgentName CLASS="common" elementtype=nacessary >
	        </td>
	        <td CLASS="title">对方业务员身份证</td>
	    	<td CLASS="input" COLSPAN="1">
			<input NAME="GrpAgentIDNo" VALUE MAXLENGTH="18" CLASS="code" elementtype=nacessary  ondblclick="return queryGrpAgent();" >
    	    </td>
    	</tr>
        <!--tr class="common8">
            <td class="title8">交叉销售渠道</td>
            <td class="input8">
                <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" />
            </td>
            <td class="title8">交叉销售业务类型</td>
            <td class="input8">
                <input class="codeNo" name="Crs_BussType" id="Crs_BussType" verify="交叉销售业务类型|code:crs_busstype" ondblclick="return showCodeList('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_BussTypeName" readonly="readonly" />
            </td>
            <td class="title8">&nbsp;</td>
            <td class="input8">&nbsp;</td>
        </tr-->
        <TR  class= common8>
        	<!--TD  class= title8>
            销售渠道明细
          </TD>
          <TD  class= input8-->
            <Input class=common VALUE="01" type="hidden" name=SaleChnlDetail  ondblclick="return showCodeList('SaleChnlDetail',[this,SaleChnlDetailName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AgentType',[this,SaleChnlDetailName],[0,1],null,null,null,1);">
          <!--/TD-->
    	   <TD  class= title8>
            经办人签名
          </TD>
          <TD  class= input8>
          <Input name=HandlerName class="common" verify="经办人签名|len<=20">
          </TD>        
          <TD  class= title8>
            投保单填写日期
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker"  dateFormat="short" elementtype=nacessary verify="投保单填写日期|notnull&&date" name=HandlerDate   onfocus="fm.PolApplyDate.value=this.value" onkeyup="fm.PolApplyDate.value=this.value" onkeydown="fm.PolApplyDate.value=this.value">
          </TD>
          <TD  class= title8>
            投保单位章
          </TD>
          <TD  class= input8>
            <Input name=HandlerPrint class="common" verify="投保单位章|len<=120">
          </TD> 
        </TR>        
        <TR  class= common8>
        	
          <TD  class= title8>
            中介公司代码
          </TD>
          <TD  class= input8>
            <Input class="code" name=AgentCom verify="中介公司代码|code:AgentCom" ondblclick="return showCodeList('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');">
            <Input class="code" name=AgentCom1 ondblclick="return showCodeList('agentcominput',[this,AgentComName1,AgentCom,AgentComName],[0,1,0,1],null, fm.all('AgentVar').value,'branchtype',null);" onkeyup="return showCodeListKey('agentcominput',[this,AgentComName1,AgentCom,AgentComName],[0,1,0,1],null, fm.all('AgentVar').value,'branchtype',null);" style="display: none" >
          </TD>
          <TD  class= title8>
            中介公司名称
          </TD>
          <TD  class= input8>
            <Input class="common" name=AgentComName readonly >
            <Input class="common" name=AgentComName1 readonly style="display: none" >
            <input name="AgentVar" class="common" type="hidden" readonly>
          </TD>      
          <td class="title8">&nbsp;</td>
          <td class="input8">&nbsp;</td>
        </TR>

        <!--tr class=common id="GrpAgentTitleID" style="display: ''" >
        	<td CLASS="title" >对方机构代码</td>
        	<td CLASS="input" >
        		<input NAME="GrpAgentCom" CLASS="common" >
        	</td>
        	<td CLASS="title">对方业务员代码</td>
        	<td CLASS="input" >
        		<input NAME="GrpAgentCode" CLASS="common" >
        	</td>
        	<td  class="title" >对方业务员姓名</td>
        	<td  class="input" >
        		<Input  name=GrpAgentName CLASS="common" >
        	</td>
        </tr-->	
        
	      <TR class=common>
	      	<TD  class= title8>
            业务员代码
          </TD>
          <TD  class= input8>
      			<Input NAME=AgentCode VALUE="" MAXLENGTH=0 CLASS=code8 elementtype=nacessary ondblclick="return queryAgent();"onkeyup="return queryAgent2();" verify="业务员代码|notnull">
         </TD>
          <TD  class= title8>
            业务员名称
          </TD>
          <TD  class= input8>
      			<Input NAME=AgentName VALUE=""  readonly CLASS=common >
         </TD>         
          <TD  class= title8>
            业务员组别
          </TD>
          <TD  class= input8>
            <Input class="readonly"  readonly name=AgentGroup verify="业务员组别|notnull&len<=12" >
          </TD>       
        </TR>
        <tr class=common>
        	<td class=title>资格证书号码</td>
			<td class=input>
				<input class=common readonly=true name=Certificate>
			</td>
			<td class=title>展业证书号码</td>
			<td class=input>
				<input class=common readonly=true name=Exhibition>
			</td>
			<td class=title></td>
			<td class=input></td>
        </tr>
        <tr class=common>
        	<td class=title id=T1 style="display:''">代理销售业务员编码</td>
			<td class=input id=I1 style="display:''">
				<input class=common readonly=true name=SalesAgentCode>
			</td>
			<td class=title id=T2 style="display:''">代理销售业务员姓名</td>
			<td class=input id=I2 style="display:''">
				<input class=common readonly=true name=SalesAgentName>
			</td>
			<td class=title></td>
			<td class=input></td>
        </tr>
	      <TR class=common>     
	      	<!--TD  class= title8>
            投保申请日期 
          </TD-->
          <!--TD  class= input8-->
            <Input  type="hidden"  name=PolApplyDate  >
          <!--/TD-->               
          
    			<!--TD  class= title8 >
            联合代理人代码
          </TD-->
          <!--TD  class= input8 -->
            <Input class= common8 type=hidden name=AgentCode1 >
            <Input class= common8 type=hidden name=MissionID >
            <Input class= common8 type=hidden name=SubMissionID >      
            <Input class="common" type=hidden name=ProposalGrpContNo> 
          <!--/TD-->
         <TD CLASS=title>
      			溢交保费方式
    			</TD>
    			<TD CLASS=input COLSPAN=1>
      			<!--Input NAME=OutPayFlag VALUE="1" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('OutPayFlag', [this]);" onkeyup="return showCodeListKey('OutPayFlag', [this]);" verify="溢交保费处理方式|code:OutPayFlag&notnull" -->
      			<Input NAME=OutPayFlag VALUE="1" MAXLENGTH=1 class=codeNo  verify="溢交保费方式|code:OutPayFlag" ondblclick="return showCodeList('OutPayFlag',[this,OutPayFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('OutPayFlag',[this,OutPayFlagName],[0,1],null,null,null,1);"><input class=codename name=OutPayFlagName readonly=true >
    			</TD>
			<td CLASS="title">初审人员 
    		</td>
			<td CLASS="input" COLSPAN="1">
			<Input class=codeno name=FirstTrialNo readonly=ture  MAXLENGTH="10" CLASS="common" ondblclick="return showCodeList('FirstTrialOperator',[this,FirstTrialOperator],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('FirstTrialOperator',[this,FirstTrialOperator],[0,1],null,null,null,1);"><input class=codename name=FirstTrialOperator elementtype=nacessary verify="初审人员|notnull">		
    		</td>
			 		<TD  class= title8>
            市场类型 
          </TD>
          <TD  class= input>
          <Input class=codeno name=MarketType  CodeData="0|^1|商业型^2|结合型^3|其它" ondblclick="return showCodeList('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);"><input class=codename name=MarketTypeName readonly=true elementtype=nacessary> 		     		
        	</td>
        </TR>
		<tr CLASS="common">    		
					<TD  class= title8>
            业务员填写日期 
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker"  dateFormat="short" name=AgentDate   verify="业务员填写日期|date" >
          </TD> 
        	<TD  class= title8>
            接收日期 
          </TD>
          <TD  class= input>
          <Input class=coolDatePicker dateFormat="short" name=ReceiveDate  elementtype=nacessary verify="接收日期|notnull&date"> 		     		
        	</td>
        	<TD  class= title8>
            暂收收据号码
          </TD>
          <TD  class= input>
          <Input class=common name=TempFeeNo  verify="暂收收据号码|int"> 		     		
        	</td>
        </TR>
        <tr class=common>
        	<TD  class= title8>
            询价号码
          </TD>
          <TD  class= input>
          <Input class=common name=AskGrpContNo  verify="询价号码|int"> 			     		
        	</td>
        	<TD class= title8>
        	  大项目标识
        	</TD>
        	<TD class=input>
        	<Input class=codeno name=BigProjectFlag VALUE="0" CodeData="0|^0|非大项目^1|大项目" ondblclick="return showCodeListEx('BigProjectFlag',[this,BigProjectFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('BigProjectFlag',[this,BigProjectFlagName],[0,1],null,null,null,1);"><input class=codename name=BigProjectFlagName readonly=true > 		     		
        	</TD>
        	<TD class= title8>
        	  团体保单属性
        	</TD>
        	<TD class=input>
        	<Input class=codeno name=ContPrintType  CodeData="0|^0|常规普通型^1|健管简易型^2|委托管理型" ondblclick="return showCodeListEx('ContPrintType',[this,ContPrintTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContPrintType',[this,ContPrintTypeName],[0,1],null,null,null,1);" readonly="readonly" /><input class=codename name=ContPrintTypeName readonly=true > 		     		
        	</TD>        	
        </tr>
        <tr class="common">
            <td class="title8">是否为共保保单</td>
            <td class="input8">
                <input class="codeNo" name="CoInsuranceFlag" value="0" verify="共保标志|notnull" CodeData="0|^0|非共保保单^1|共保保单" ondblclick="return showCodeListEx('CoInsuranceFlag',[this,CoInsuranceFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CoInsuranceFlag',[this,CoInsuranceFlagName],[0,1],null,null,null,1);"><input class="codename" name="CoInsuranceFlagName" readonly="readonly" elementtype="nacessary" />
            </td>
            <td CLASS="title">承保日期 </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="SignDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
				<td CLASS="title">保单生效日  </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="CValiDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
            <!--<td class="title8">开办市县数</td>
            <td class="input8"><input class=common name=CTCount verify="开办市县数|int"></td>
            <td class="title8">开办市县内容</td>
            <td class="input8"><input class=common name=CityInfo ></td>-->
        </tr>
        <tr CLASS="common">
		
    <td CLASS="title">满期日期 </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="ContInValidate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
			<td CLASS="title">是否正在理赔</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="PayFlag" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
            <td CLASS="title">是否正在保全 </td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="EdorFlag" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1"></td>
     
    </tr>
    <tr CLASS="common">
		<TD class= title8>
            	授权使用客户信息
        	</TD>
        	<TD class=input>
        		<input class="codeNo" id="auth" name="auth" readonly=true verify="授权使用客户信息|notnull" ondblclick="return false;return showCodeList('auth',[this,authName],[0,1],null,null,null,1);" onkeyup="return false;return showCodeList('auth',[this,authName],[0,1],null,null,null,1);"><input class="codename" id="authName" name="authName" readonly="readonly" elementtype="nacessary" />
        	</TD>
        	<TD class= title8>
        	</TD>
        	<TD class=input>
        	</TD>
        	<TD class= title8>
        	</TD>
        	<TD class=input>
        	</TD>
    </tr>
      </table>
      <table class=common>
      	<tr class = common>
					<td>
						材料交接&nbsp;&nbsp;&nbsp;被保险人清单
					  <input type="checkbox" name="ImpartCheck3" class="box">&nbsp;&nbsp;&nbsp;&nbsp;电子文档&nbsp;&nbsp;&nbsp;&nbsp;
					  <input type="checkbox" name="ImpartCheck3" class="box">&nbsp;&nbsp;&nbsp;&nbsp;纸质表格
					  <input class="common6" name="ImpartCheck3">页&nbsp;&nbsp;&nbsp;&nbsp;其它资料
					  <input class="common6" name="ImpartCheck3"> , 
					  <input class="common6" name="ImpartCheck3">&nbsp份/页
				</td>			
				</tr>
      </table>
    </Div>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol2);">
    		</td>
    		<td class= titleImg>
    			 团体基本资料（团体客户号 <Input class= common8 name=GrpNo  verify="查询|int"><INPUT id="butGrpNoQuery" class=cssButton VALUE="查  询"  name="Query" TYPE=button onclick="dateDiff(fm.PolApplyDate.value,fm.CValiDate.value,'D');showAppnt();"> ）(首次投保单位无需填写客户号)
    		</td>
    	</tr>
    </table>
    
    <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
       <TR>
          <TD  class= title8>
            投保团体名称
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpName elementtype=nacessary onkeydown="QueryOnKeyDown()" onblur="QueryOnBlur()" verify="单位名称|notnull&len<=60">
          </TD>   
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8  name=Phone elementtype=nacessary verify="联系电话|notnull&NUM&len<=30">
          </TD>     
          <TD  class= title8>
            组织机构代码
          </TD>
          <TD  class= input8>
             <Input class= common8 name=OrgancomCode verify="组织机构代码|len=10">
          </TD>               
        </TR>
          <TR>
          <!--
          <TD  class= title8>
            单位地址编码
          </TD>
          <TD  class= input8>
             <Input class="code" name="GrpAddressNo"   ondblclick="getaddresscodedata();return showCodeListEx('GetGrpAddressNo',[this],[0],'', '', '', true);" onkeyup="getaddresscodedata();return showCodeListKeyEx('GetGrpAddressNo',[this],[0],'', '', '', true);">
          </TD>								
           -->
              <Input class=common8 name=GrpAddressNo  type="hidden" >
          <TD  class= title8>
            单位地址
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpAddress elementtype=nacessary  verify="单位地址|notnull&len<=60">
          </TD>
          <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpZipCode  elementtype=nacessary  verify="邮政编码|notnull&zipcode">
          </TD>  
          <TD class="title8">
           统一社会信用代码
          </TD>
          <TD class="input8">
              <input class="common" name="UnifiedSocialCreditNo" verify="统一社会信用代码|len=18" />
          </TD>       
        </TR>   
        <TR  class= common>
          <TD  class= title8>
            联系人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan1 elementtype=nacessary verify="联系人姓名|notnull&len<=10">
          </TD>
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone1 elementtype=nacessary  verify="联系电话|notnull&NUM&len<=30">
          </TD>
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax1 verify="保险联系人一传真|NUM&len<=30">
          </TD>                      
        </TR>    
        
        <TR  class= common>
		<TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <input class=codeNo name="AppntNativePlace" verify="投保人国籍|code:GrpNativePlace" ondblclick="return showCodeList('GrpNativePlace',[this,AppntNativePlaceName],[0,1]);" onkeyup="return showCodeListKey('GrpNativePlace',[this,AppntNativePlaceName],[0,1]);"><input class=codename name=AppntNativePlaceName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title8 id="NativeCityInfo" style="display: none">
          <div id="NativeCityTitle">国家</div>
          </TD>
          <TD  class= input8 id="NativeCity" style="display: none">
          <input class=codeNo name="AppntNativeCity" ondblclick="return showCodeList('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);"><input class=codename name=AppntNativeCityName readonly=true elementtype=nacessary>    
          </TD>
		<TD  class= title id="Sex1" style="display: none">
            性别
          </TD>
          <TD  class= input id="Sex2" style="display: none">
            <Input class=codeNo name=AppntSex  verify="投保人性别|code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true>    
          </TD>
          </TR>
          <TR id="OccupationInfo" style="display: none">
          <TD  class= title>
	职业代码
          </TD>
          <TD  class= input>
            <Input class="code" name="AppntOccupationCode" verify="投保人职业代码" ondblclick="return showCodeList('OccupationCode',[this],null,null,null,null,null,300);" onkeyup="return showCodeListKey('OccupationCode',[this],null,null,null,null,null,300);" onblur="getdetailwork();">
          </TD>
           <TD  class= title>
	职业类别
          </TD>
          <TD  class= input>
            <Input class="code" name="AppntOccupationType" verify="投保人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>
          </tr>
                 
        <TR  class= common>
          <TD  class= title8>
            电子邮箱
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail1 verify="保险联系人一E-MAIL|len<=60&Email">
          </TD>        
          <TD  class= title8>
            行业编码
          </TD>
          <TD  class= input8>
            <Input class="code8" name=BusinessType  elementtype=nacessary verify="行业编码|notnull&code:BusinessType&len<=20" ondblclick="return showCodeList('BusinessType',[this,BusinessTypeName,BusinessBigType],[0,1,2],null,null,null,1,300);" onkeyup="return showCodeListKey('BusinessType',[this,BusinessTypeName,BusinessBigType],[0,1,2],null,null,null,1,300);">
          </TD> 
          <TD  class= title8>
            行业性质
          </TD>                   
          <TD  class= input8>   
            <Input class="common8" name=BusinessTypeName elementtype=nacessary readonly >
            <Input class="common8" type="hidden" name=BusinessBigType  >
          </TD>                                     
        <TR  class= common>
          <TD  class= title8>
            企业类型编码
          </TD>
          <TD  class= input8>
            <Input class=code8 name=GrpNature elementtype=nacessary  verify="企业类型编码|notnull&code:grpNature&len<=10" ondblclick="showCodeList('GrpNature',[this,GrpNatureName],[0,1]);" onkeyup="showCodeListKey('GrpNature',[this,GrpNatureName],[0,1]);">
          </TD> 
          <TD  class= title8>
            企业类型
          </TD>                  
          <TD  class= input8>            
            <Input class=common8 name=GrpNatureName elementtype=nacessary readonly >          
          </TD>        
          <TD  class= title8>
            员工总人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Peoples  elementtype=nacessary verify="员工总人数|notnull&int">
          </TD>
        </TR>          
        <TR  class= common>          
          <TD  class= title8>
            在职人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AppntOnWorkPeoples  elementtype=nacessary  verify="在职人数|int&notnull elementtype=nacessary">
          </TD>
          <TD  class= title8>
            退休人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AppntOffWorkPeoples   verify="退休人数|int">
          </TD>
          <TD  class= title8>
            其他人员人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AppntOtherPeoples   verify="其他人员人数|int">
          </TD>                                               
          <!--TD  class= title8>
            投保次数
          </TD>
          <TD  class= input8-->
            <Input class= common8 name=GrpAppNum  type="hidden" verify="投保次数|int">
          <!--/TD--> 
          </TR>
          
          <TR id="ShareHolder1" style="display: none">
        	<TD  class= title8>
            控股股东/实际控制人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8  name=ShareholderName>
          </TD>
          <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class=codeno name=ShareholderIDType  CodeData="0|1^0|身份证^4|其他" ondblclick="return showCodeListEx('ShareholderIDType',[this,IDTypeName2],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ShareholderIDType',[this,IDTypeName2],[0,1],null,null,null,1);"><input class=codename name=IDTypeName2 readonly=true >
          </TD>
          <TD  class= title8>
            证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=ShareholderIDNo verify="证件号码|NUM&len<=20" >
          </TD>
        </TR>
          <TR id="ShareHolder2" style="display: none">
		  <TD  class= title8>
            证件生效日期
          </TD>                  
          <TD  class= input8>            
            <Input class=coolDatePicker dateFormat="short" name=ShareholderIDStart  >        
          </TD>        
          <TD  class= title8>
            证件失效日期
          </TD>
          <TD  class= input8>
            <Input class=coolDatePicker dateFormat="short" name=ShareholderIDEnd  > 
            <input type="checkbox" name="IdNoValidity2" class="box"  onclick="setIDLongEffFlag2();">&nbsp;&nbsp;&nbsp;&nbsp;长期有效
            <Input class= common8  name=ShareholderIDLongFlag type=hidden>
          </TD>
        </TR>
        
        <TR id="LegalPerson1" style="display: none">
        	<TD  class= title8>
            法人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8  name=LegalPersonName1>
          </TD>
          <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class=codeno name=LegalPersonIDType1  CodeData="0|1^0|身份证^4|其他" ondblclick="return showCodeListEx('LegalPersonIDType1',[this,IDTypeName1],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('LegalPersonIDType1',[this,IDTypeName1],[0,1],null,null,null,1);"><input class=codename name=IDTypeName1 readonly=true >
          </TD>
          <TD  class= title8>
            证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LegalPersonIDNo1 verify="证件号码|NUM&len<=20" >
          </TD>
        </TR>
          <TR id="LegalPerson2" style="display: none">
		  <TD  class= title8>
            证件生效日期
          </TD>                  
          <TD  class= input8>            
            <Input class=coolDatePicker dateFormat="short" name=LegalPersonIDStart1  >        
          </TD>        
          <TD  class= title8>
            证件失效日期
          </TD>
          <TD  class= input8>
            <Input class=coolDatePicker dateFormat="short" name=LegalPersonIDEnd1  > 
            <input type="checkbox" name="IdNoValidity1" class="box"  onclick="setIDLongEffFlag1();">&nbsp;&nbsp;&nbsp;&nbsp;长期有效
            <Input class= common8  name=LegalPersonIDLongFlag1 type=hidden>
          </TD>
        </TR>
        
        <TR id="Responsible1" style="display: none">
        	<TD  class= title8>
            负责人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8  name=ResponsibleName>
          </TD>
          <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class=codeno name=ResponsibleIDType  CodeData="0|1^0|身份证^4|其他" ondblclick="return showCodeListEx('ResponsibleIDType',[this,IDTypeName3],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ResponsibleIDType',[this,IDTypeName3],[0,1],null,null,null,1);"><input class=codename name=IDTypeName3 readonly=true >
          </TD>
          <TD  class= title8>
            证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=ResponsibleIDNo verify="联系人证件号码|NUM&len<=20" >
          </TD>
        </TR>
          <TR id="Responsible2" style="display: none">
		  <TD  class= title8>
            证件生效日期
          </TD>                  
          <TD  class= input8>            
            <Input class=coolDatePicker dateFormat="short" name=ResponsibleIDStart  >        
          </TD>        
          <TD  class= title8>
            证件失效日期
          </TD>
          <TD  class= input8>
            <Input class=coolDatePicker dateFormat="short" name=ResponsibleIDEnd  > 
            <input type="checkbox" name="IdNoValidity3" class="box"  onclick="setIDLongEffFlag3();">&nbsp;&nbsp;&nbsp;&nbsp;长期有效
            <Input class= common8  name=ResponsibleIDLongFlag type=hidden>
          </TD>
        </TR>
          
      </table>
    </Div>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divbenef);">
    		</td>
    		<td class= titleImg>
    			 受益所有人信息
    		</td>
    	</tr>
    </table>
    <table  class= common id="divbenef">
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanBeneficiaryDetailGrid">
						</span>
					</td>
				</tr>
	</table>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divpeople);">
    		</td>
    		<td class= titleImg>
    			 参保人员资料
    		</td>
    	</tr>
    </table>
    
    <Div  id= "divpeople" style= "display: ''">
      <table class=common>
         <TR  class= common>       
          <TD  class= title8>
            被保险人数（成员）
          </TD>
          <TD  class= input8>
          <Input name=Peoples3 class= common8 elementtype=nacessary verify="被保险人数（成员）|int&notnull">
          </TD>           
          <TD  class= title8>
            在职人数
          </TD>
          <TD  class= input8>
          <Input name=OnWorkPeoples class= common8 elementtype=nacessary verify="在职人数|notnull&int">
          </TD>
          <TD  class= title8>
            退休人数
          </TD>
          <TD  class= input8>
          <Input name=OffWorkPeoples class= common8 verify="退休人数|int">
          </TD>           
          </TR>
         <TR  class= common>                   
          <TD  class= title8>
            其他人员人数
          </TD>
          <TD  class= input8>
          <Input name=OtherPeoples class= common8 verify="其他人员人数|int">
          </TD>                                          
         <TD  class= title8>
            连带被保险人数（家属）
          </TD>
          <TD  class= input8>
          <Input name=RelaPeoples class= common8 verify="连带被保险人数（家属）|int">
          </TD>  
          <TD  class= title8>
            配偶人数
          </TD>
          <TD  class= input8>
          <Input name=RelaMatePeoples class= common8 verify="配偶人数|int">
          </TD>                  
          </TR>  
          <TR  class= common>             
          <TD  class= title8>
            子女人数
          </TD>
          <TD  class= input8>
          <Input name=RelaYoungPeoples class= common8 verify="子女人数|int">
          </TD>                                                    
          <TD  class= title8>
            其他人员人数
          </TD>
          <TD  class= input8>
          <Input name=RelaOtherPeoples class= common8 verify="其他人员人数|int">
          </TD>
          <TD>
          </TD>
      <TD  class= input type=hidden>
      <Input class=common8 name=InputOperator type=hidden >
      </TD>   
          </TR>     
      </table>
      <!-- by gzh 20101211 -->
 	   <Div  id= "divpeopletype" style= "display: ''">
 	   <table class=common>
 	 	   <TR  class= common>             
	          <TD  class= title8>参保人员职业类别:
	          </TD>
	          <TD  class= input8>
	          </TD>                                                    
	          <TD  class= title8>
	          </TD>
	          <TD  class= input8>
	          </TD>
	          <TD  class= title8>
	          </TD>
	          <TD  class= input8>
	          </TD>
          </TR>
 	     
 	   	
	 	   <TR  class= common>             
	          <TD  class= title8>
	            一类
	          </TD>
	          <TD  class= input8>
	          <Input name=OneType class=readonly verify="一类|int" readonly>人
	          </TD>                                                    
	          <TD  class= title8>
	           二类
	          </TD>
	          <TD  class= input8>
	          <Input name=TwoType class=readonly verify="二类|int" readonly>人
	          </TD>
	          <TD  class= title8>
	           三类
	          </TD>
	          <TD  class= input8>
	          <Input name=ThreeType class=readonly verify="三类|int" readonly>人
	          </TD>
          </TR>
          <TR  class= common>             
	          <TD  class= title8>
	            四类
	          </TD>
	          <TD  class= input8>
	          <Input name=FourType class=readonly verify="四类|int" readonly>人
	          </TD>                                                    
	          <TD  class= title8>
	           五类
	          </TD>
	          <TD  class= input8>
	          <Input name=FiveType class=readonly verify="五类|int" readonly>人
	          </TD>
	          <TD  class= title8>
	           六类
	          </TD>
	          <TD  class= input8>
	          <Input name=SixType class=readonly verify="六类|int" readonly>人
	          </TD>
          </TR>
        </table>
 	   </DIV>
      <!-- by gzh  end -->    
      <table class = common>
		<tr class = common>
			<td>
			  <input type="checkbox" name="ImpartCheck1" class="box">&nbsp;&nbsp;&nbsp;&nbsp;社会基本医疗保险&nbsp;&nbsp;&nbsp;&nbsp;参加年份
			  <input class="common6" name="ImpartCheck1">&nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="checkbox" name="ImpartCheck1" class="box">&nbsp;&nbsp;&nbsp;&nbsp;商业医疗保险&nbsp;&nbsp;&nbsp;&nbsp;
			  <input type="checkbox" name="ImpartCheck1" class="box">&nbsp;&nbsp;&nbsp;&nbsp;单位报销&nbsp;&nbsp;&nbsp;&nbsp;其它
			  <input class="common6" name="ImpartCheck1">
		</td>			
		</tr>
	  </table>
    </Div>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFee);">
    		</td>
    		<td class= titleImg>
    			 缴费信息
    		</td>
    	</tr>
    </table>

    <Div  id= "divFee" style= "display: ''">  
    
    	<table class=common style = "display: none">
      	<tr class = common>
      		<td class= common>
    			 缴费主体
    		</td>
					<td>
						  <input type="checkbox" name="ImpartCheck2" class="box" onclick="ImpartCheck2Radio1();">&nbsp;&nbsp;&nbsp;&nbsp;投保人全额承担&nbsp;&nbsp;&nbsp;&nbsp;
						  <input type="checkbox" name="ImpartCheck2" class="box" onclick="ImpartCheck2Radio2();">&nbsp;&nbsp;&nbsp;&nbsp;被保险人全额承担&nbsp;&nbsp;&nbsp;&nbsp;
						  <input type="checkbox" name="ImpartCheck2" class="box" onclick="ImpartCheck2Radio3();">&nbsp;&nbsp;&nbsp;&nbsp;双方共同承担，其中投保人承担
						  <input class="common6" name="ImpartCheck2">%,被保险人承担
						  <input class="common6" name="ImpartCheck2">%
					</td>			
				</tr>  
      </table>
      <!-- by gzh -->
    	<table class=common>
      	<tr class = common>
      		<td class= common>
    			 首期保险费信息
    		</td>
			<td>
		    公共账户缴费金额<input class="common6" name="ImpartCheck4" verify="公共账户缴费金额|num">,
		    个人账户个人缴费金额<input class="common6" name="ImpartCheck4" verify="个人账户个人缴费金额|num">,
		    个人账户单位缴费金额<input class="common6" name="ImpartCheck4" verify="个人账户单位缴费金额|num">,
		    首次保险费合计<input class="common6" name="ImpartCheck4" verify="首次保险费合计|num">
			</td>			
		</tr>  
      </table>  
    	<!-- by gzh end -->
      <!-- by gzh end -->
    	<table>
    	<tr>
    	<td>
    	<font size=2 color="#ff0000">
	     <b>新、旧缴费方式对照说明</b> 
		   &nbsp;&nbsp;1现金--->1现金、3转账支票--->3支票 </font>
    	</td>
    	</tr>
    	</table>
    	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDWFee);">
    		</td>
    		<td class= titleImg>
    			 公共账户、个人账户单位交费部分
    		</td>
    	</tr>
    	</table>
    	<Div  id= "divDWFee" style= "display: ''">
	      <table class=common> 
	        <TR  class= common>
	          <TD  class= title8>
	            缴费方式
	          </TD>
	          <TD  class= input8>
	          	<!--<Input name=PayMode CLASS="code" type="hidden">-->
	          <Input class=codeNo name=PayMode  VALUE="1" MAXLENGTH=1 verify="缴费方式|notnull&code:PayMode" ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);" onkeyup="return showCodeListKey('GrpPayMode',[this,PayModeName],[0,1],null,tsql,'1',1);"><input class=codename name=PayModeName readonly=true elementtype=nacessary>
	          </TD>
	          <TD  class= title8>
	            开户银行
	          </TD>
	          <TD  class= input8>
	            <Input class=codeNo  name=BankCode style="display:'none'"  MAXLENGTH=1 verify="开户银行|code:bank&len<=24" ondblclick="return showCodeList('Bank',[this,BankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName],[0,1],null,null,null,1);"><input class=codename name=BankCodeName style="display:'none'" readonly=true >
	          </TD>
	          <TD  class= title8>
	            账&nbsp;&nbsp;&nbsp;号
	          </TD>
	          <TD  class= input8>
	            <Input class= common8 name=BankAccNo style="display:'none'" verify="帐号|len<=40">
	          </TD>       
	        </TR> 
	        <TR class= common>    
	          <TD  class= title8>
	            户&nbsp;&nbsp;&nbsp;名
	          </TD>
	          <TD  class= input8>
	            <Input class= common8 name=AccName style="display:'none'" verify="户名|len<=40">
	          </TD>  
	          <TD  class= title8>
	           缴费频次
	          </TD>
	          <TD  class= input8>
	      	   <!--Input name=GrpContPayIntv CLASS="code" type= "hidden"
	           <Input class=codeNo name=GrpContPayIntv verify="缴费频次|notnull&code:grppayintv" ondblclick="return showCodeList('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);" onkeyup="showCodeListKey('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);"><input class=codename name=GrpContPayIntvName readonly=true elementtype=nacessary>  
	         	-->
	         	<Input class=codeno name=GrpContPayIntv verify="缴费频次|notnull"  CodeData="0|^0|趸交" ondblclick="return showCodeListEx('GrpContPayIntv',[this,GrpContPayIntvName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('GrpContPayIntv',[this,GrpContPayIntvName],[0,1],null,null,null,1);"><input class=codename name=GrpContPayIntvName readonly=true elementtype=nacessary> 		     		
	         </TD>       
	         <TD  class= title8>
	           保费合计
	          </TD>
	          <TD  class= input8>
	      	   <!--Input name=GrpContPayIntv CLASS="code" type= "hidden"-->
	           <Input class=common  name=GrpContSumPrem verify="保费合计|NUM">  
	          </td>
	        </TR>  
	        </table>   
 		</div>
 		<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGRFee);">
    		</td>
    		<td class= titleImg>
    			 个人账户个人交费部分
    		</td>
    	</tr>
    	</table>
    	<Div  id= "divGRFee" style= "display: ''">
	      <table class=common> 
	        <TR  class= common>
	          <TD  class= title8>
	            <INPUT TYPE="checkbox" NAME="GRMixComFlag" checked   disabled>单位代扣代交
	          </TD>
	        </TR>  
	        </table>   
 		</div>
    </Div>  
    <br> 
    <div id="divBookingPayInty" style="display: 'none'">
    	<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanBookingPayIntyGrid" >
						</span>
					</td>
				</tr>
			</table>
    </div> 
    <Div  id= "divDuty" style= "display: none"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDuty);">
    		</td>
    		<td class= titleImg>
    			 保险责任
    		</td>
    	</tr>
    </table>
      <table class=common>    
        <TR  class= common>
          <TD  class= title8>
            保障计划区分标准
          </TD>
          <TD  class= input8>
          <Input name=EnterKind class=codeNo verify="保障计划区分标准|code:enterkind" ondblclick="return showCodeList('enterkind',[this,EnterKindName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('enterkind',[this,EnterKindName],[0,1],null,null,null,1);"><input class=codename name=EnterKindName readonly=true >  
          </TD> 
                       
        </TR>             
        </table>               
    </Div>             
<DIV id=DivLCImpart STYLE="display:''">
<!-- 告知信息部分（列表） -->
	<div  id= "divLCImpart0" style= "display: none">
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart0);">
				</td>
				<td class= titleImg>
					客户服务需求
				</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanServInfoGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>	
	<div  id= "divLCImpart2" style= "display: none">    
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart2);">
					</td>
					<td class= titleImg>
					保障情况告知（<font color="red">新团险投保书中已没有此项告知，此处仍保留仅用作显示历史数据</font>）
				</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanHistoryImpartGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>
	<div  id= "divLCImpart33" style= "display: none">
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart33);">
					</td>
					<td class= titleImg>
					历史保障情况告知（金额单位：万元）
				</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanNewHistoryImpartGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart4);">
					</td>
					<td class= titleImg>
					基本告知
				</td>
			</tr>
		</table>
		<div  id= "divLCImpart4" style= "display: 'none';float:left;">
			<table width="942" border="0" align="center" cellpadding="0" cellspacing="0" class="muline">
  				<tr>
					<td colspan="7" class="mulinetitle2" height="20">&nbsp;</td>
				</tr>
  				<tr> 
    				<td colspan="7" class="mulinetitle3">1.被保险人平均年龄<input name="Detail101" class = "common66" style="text-align:center;">&nbsp&nbsp&nbsp&nbsp 最高年龄 <input name="Detail101" class = "common66" style="text-align:center;">&nbsp&nbsp&nbsp&nbsp 最低年龄 <input name="Detail101" class = "common66" style="text-align:center;"></td>
  				</tr>
  				<tr> 
    				<td colspan="7"  class="mulinetitle3">2.被保险人退休人数占比<input name="Detail102" class = "common66" style="text-align:center;">&nbsp&nbsp&nbsp&nbsp连带被保险人人数<input name="Detail102" class = "common66" style="text-align:center;">&nbsp&nbsp 外籍人员人数 <input name="Detail102" class = "common66" style="text-align:center;"></td>
  				</tr>
  				<tr> 
    				<td colspan="7" class="mulinetitle3" height=23px>3.被保险人税前人均年薪&nbsp&nbsp&nbsp&nbsp<input type="checkbox" name="Detail103" class="box">&nbsp&nbsp 万元以下（不含） <input type="checkbox" name="Detail103" class="box">&nbsp&nbsp 万元～四万元（不含）  <input type="checkbox" name="Detail103" class="box">&nbsp&nbsp 四万元～十二万元（不含） <input type="checkbox" name="Detail103" class="box">&nbsp&nbsp 十二万元以上（不含）  </td>
  				</tr>  
  				<tr> 
    				<td colspan="6" class="mulinetitle3">4.被保险人（包括连带被保险人）一年内是否有前往中国大陆以外地区的安排？(若“是”请详述拟前往的国家或地区及时间人员安排)&nbsp&nbsp&nbsp&nbsp</td>
    				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail105" class="box" onclick="InputDetailToMuline105();"> &nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail105" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  				</tr>
  				<tr> 
    				<td colspan="6" class="mulinetitle3">5.是否有残疾人员参加本次投保？（若“是”请详述残疾人数及残疾情况）&nbsp&nbsp&nbsp&nbsp</td>
    				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail106" class="box" onclick="InputDetailToMuline106();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail106" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  				</tr>
			</table>
		</div>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanBaseImpartDetailGrid">
					</span>
				</td>
			</tr>
		</table>
	
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart5);">
					</td>
					<td class= titleImg>
					健康告知
				</td>
			</tr>
		</table>
		<div  id= "divLCImpart5" style= "display: 'none';float:left;">
		<table width="942" border="0" align="center" cellpadding="0" cellspacing="0" class="muline">
 				<tr>
				<td colspan="7" class="mulinetitle2" height="20">&nbsp;</td>
			</tr>
 				<tr> 
   				<td colspan="7" class="mulinetitle3">1.是否定期组织被保险人或员工体检?&nbsp&nbsp&nbsp&nbsp<input type="checkbox" name="Detail201" class="box">&nbsp&nbsp 是 <input type="checkbox" name="Detail201" class="box">&nbsp&nbsp 否&nbsp&nbsp频次：<input name="Detail201" class = "common66" style="text-align:center;">次/年&nbsp&nbsp最近一次体检医院为： <input name="Detail201" class = "common66" style="width:200px;"></td>
 				</tr>  
 				<tr> 
   				<td colspan="6" class="mulinetitle3">2.被保险人在体检中是否有因异常情形而被建议接受其它检查或治疗的情况?&nbsp&nbsp&nbsp&nbsp</td>
   				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail202" class="box" onclick="InputDetailToMuline202();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail202" class="box">&nbsp&nbsp 否 <input class="common7"></td>
 				</tr>
 				<tr> 
   				<td colspan="6" class="mulinetitle3">3.目前被保险人中是否有正在住院治疗的情况?&nbsp&nbsp&nbsp&nbsp</td>
   				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail203" class="box" onclick="InputDetailToMuline203();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail203" class="box">&nbsp&nbsp 否 <input class="common7"></td>
 				</tr>
 				<tr> 
   				<td colspan="6" class="mulinetitle3">4.近一年内，被保险人中是否有因病不在工作岗位累计超过10天者？&nbsp&nbsp&nbsp&nbsp</td>
   				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail204" class="box" onclick="InputDetailToMuline204();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail204" class="box">&nbsp&nbsp 否 <input class="common7"></td>
 				</tr>
 				<tr> 
   				<td colspan="6" class="mulinetitle3">5.被保险人中是否有遭受意外伤害后，至今未愈或留有残疾后遗症者?&nbsp&nbsp&nbsp&nbsp</td>
   				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail205" class="box" onclick="InputDetailToMuline205();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail205" class="box">&nbsp&nbsp 否 <input class="common7"></td>
 				</tr>
 				<tr> 
   				<td colspan="6" class="mulinetitle3">6.过去五年内，被保险人中是否有曾患下列疾病而住院治疗或接受医师诊治者？
   													<br/>(1)高血压、冠心病、心律失常、心肌梗塞、主动脉瘤、心脏瓣膜疾病；
   													<br/>(2)脑中风(脑出血、脑栓塞、脑梗塞)、脑瘤、癫痫、重症肌无力、精神病；
   													<br/>(4)肝炎、肝硬化、胆石症、胰腺炎、消化性溃疡；
   													<br/>(5)肾炎、肾病综合症、肾功能不全、尿毒症、泌尿系统结石、肾囊肿；
   													<br/>(6)癌症(恶性肿瘤)或未经证实为良性或恶性之肿瘤、肿物、息肉或硬块；
   													<br/>(7)糖尿病、甲状腺功能亢进或低下；
   													<br/>(8)系统性红斑狼疮、类风湿性关节炎、强直性脊柱炎；
   													<br/>(9)艾滋病或艾滋病病毒携带者；
   													<br/>(10)慢性支气管炎、肺气肿、呼吸衰竭；
   													<br/>(11)白血病、再生障碍性贫血、淋巴瘤；
   													<br/>(12)先天性疾病、职业病、酒精或药物滥用成瘾；
   													<br/>(13)乳腺疾病、子宫肌瘤、卵巢囊肿等妇科疾病；
   													<br/>(14)以上未提及的其它疾病。
   													</td>
   				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail206" class="box" onclick="InputDetailToMuline206();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail206" class="box">&nbsp&nbsp 否 <input class="common7"></td>
 				</tr>
 				<tr> 
   				<td colspan="6" class="mulinetitle3">7.是否会组织被保险人参加潜水、跳伞、探险、攀岩、武术比赛、特技表演、赛马、赛车等高风险运动?&nbsp&nbsp&nbsp&nbsp</td>
   				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail207" class="box" onclick="InputDetailToMuline207();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" name="Detail207" class="box">&nbsp&nbsp 否 <input class="common7"></td>
 				</tr>
		</table>
		</div>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanHealthImpartDetailGrid" >
					</span>
				</td>
			</tr>
		</table>
	
	<div  id= "divLCImpart3" style= "display: none">
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart3);">
				</td>
				<td class= titleImg>
				严重疾病情况告知（<font color="red">新团险投保书中已没有此项告知，此处仍保留仅用作显示历史数据</font>）
				</td>
			</tr>
		</table>
		
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanDiseaseGrid" >
						</span>
					</td>
				</tr>
			</table>
		</div>
 		 
</DIV>

		<!--table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
				</td>
				<td class= titleImg>
					团体告知信息
				</td>
			</tr>
		</table-->	
		<div id="divLCImpart11" style="display:''">					
			
		</div>		
		<div  id= "divLCImpart1" style= "display: 'none'">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanImpartGrid" >
						</span>
					</td>
				</tr>
			</table>
		</div>
      <table class=common>
         <TR  class= common> 
           <TD  class= title8> 特别约定 </TD>
         </TR>
         <TR  class= common>
           <TD  class= title8>
             <textarea name="Remark" cols="110" rows="3" class="common" verify="特别约定|len<=1000" >
             </textarea>
           </TD>
         </TR>
 
            <Input type=hidden name=EmployeeRate verify="雇员自付比例|int&len<=5">
            <Input type=hidden name=FamilyRate verify="家属自付比例|int&len<=80">
    
    </table>
    <!-- by gzh -->
           <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divgetinfo);">
    		</td>
    		<td class= titleImg>
    			投保基本信息
    		</td>
    	</tr>
 	   </table>
 	   <Div  id= "divgetinfo" style= "display: ''">
   	<table class=common>
      	<tr class = common>
      		<td class= common>
    			 老年护理保险金领取起始日
    		</td>
			<td>
		    <input type="checkbox" name="ImpartCheck5" class="box" onclick="ImpartCheck5Radio1();">&nbsp;&nbsp;&nbsp;&nbsp;
		    全员统一（男性<input class="common6" name="ImpartCheck5">周岁，女性<input class="common6" name="ImpartCheck5">周岁）;
		    <input type="checkbox" name="ImpartCheck5" class="box" onclick="ImpartCheck5Radio2();">&nbsp;&nbsp;&nbsp;&nbsp;详见被保险人清单
			</td>			
		</tr>  
      </table>
      <table class=common>
      	<tr class = common>
      		<td class= common>
    			 老年护理金领取方式
    		</td>
			<td>
		    <input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio1();">&nbsp;&nbsp;&nbsp;&nbsp;A一次性领取
		    <input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio2();">&nbsp;&nbsp;&nbsp;&nbsp;B按年或按月分期领取
		    <input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio3();">&nbsp;&nbsp;&nbsp;&nbsp;C部分分期领取
		    <input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio4();">&nbsp;&nbsp;&nbsp;&nbsp;D按年或按月分次领取
		    <input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio5();">&nbsp;&nbsp;&nbsp;&nbsp;详见被保险人清单
			</td>			
		</tr>  
      </table>  
      </Div>  
   	<!-- by gzh end -->         
   <Div  id= "divButton" style= "display: ''" align= left >
 	<!--INPUT class=cssButton VALUE="查 询"  TYPE=button onclick="queryClick()"> 
 	<INPUT class=cssButton VALUE="修 改"  TYPE=button onclick="updateClick()"> 
 	<INPUT class=cssButton VALUE="删 除"  TYPE=button onclick="deleteClick()"> 
 	<INPUT class=cssButton VALUE="保 存"  TYPE=button onclick="submitForm();"--> 
 	<%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>   
   </DIV>    
   
    <div>
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" />
                </td>
                <td class= titleImg>共保信息</td>
            </tr>
        </table>
        
        <table class="common">
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnCoInsuranceParam" name="btnCoInsuranceParam" value="录入共保要素信息" onclick="showCoInsuranceParamWin();" disabled="disabled" />
                </td>
            </tr>
        </table>
    </div>
    
    <br />
   
<Div  id= "divhiddeninfo" style= "display: 'none'"> 
<table class= common>
    	<TR>

          <TD  class= title8>
            销售渠道
          </TD>
          <TD  class= input8>
            <Input class="code8" name=AgentType elementtype=nacessary  ondblclick="return showCodeList('AgentType',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('AgentType',[this],null,null,null,null,1);">
          </TD>
          <TD  class= title8>
            保额等级
          </TD>
          <TD  class= input8>
          <Input name=AmntGrade class="code8" ondblclick="return showCodeList('amntgrade',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('amntgrade',[this],null,null,null,null,1);">
          </TD>
          
          </TR>            
	
	  

<TR  class= common>
          <TD  class= title8>
            资产总额(元)
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Asset verify="资产总额|int&len<=17">
          </TD>  
          <TD  class= title8>
            注册资本金
          </TD>
          <TD  class= input8>
            <Input class= common8 name=RgtMoney verify="注册资本金|int&len<=17">
          </TD>
          <TD  class= title8>
            净资产收益率
          </TD>
          <TD  class= input8>
            <Input class= common8 name=NetProfitRate verify="净资产收益率|int&len<=17">
          </TD>       
        </TR>
        <TR>
          <TD  class= title8>
            单位传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax >
          </TD>
          <TD  class= title8>
            成立时间
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker" dateFormat="short" name=FoundDate >
          </TD>       
        </TR>
        <TR  class= common>        
           <TD  class= title8>
            部&nbsp;&nbsp;&nbsp;门
          </TD>
          <TD  class= input8 colspan=3>
            <Input class= common3 name=Department1 verify="保险联系人一部门|len<=30">
          </TD> 
        </TR>       
        <TR  class= common>
          <TD  class= title8>
            保险联系人二
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            姓&nbsp;&nbsp;&nbsp;名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan2 verify="保险联系人二姓名|len<=10">
          </TD>
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone2 verify="保险联系人二联系电话|NUM&len<=30">
          </TD>
          <TD  class= title8>
            E-MAIL
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail2 verify="保险联系人二E-MAIL|len<=60&Email">
          </TD>
        </TR>  
        <TR  class= common>        
           <TD  class= title8>
            部&nbsp;&nbsp;&nbsp;门
          </TD>
          <TD  class= input8 colspan=3>
            <Input class= common3 name=Department2 verify="保险联系人二部门|len<=30">
          </TD> 
        </TR>                          
        <TR  class= common>
          <TD  class= title8>
            主营业务
          </TD>
          <TD  class= input8>
            <Input class= common8 name=MainBussiness verify="主营业务|len<=60">
          </TD>
          <TD  class= title8>
            单位法人代表
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Corporation verify="单位法人代表|len<=20">
          </TD>
          <TD  class= title8>
            机构分布区域
          </TD>
          <TD  class= input8>
            <Input class= common8 name=ComAera verify="机构分布区域|len<=30">
          </TD>       
        </TR>
                <TR  class= common>
          <TD  class= title8>
            职务
          </TD>
          <TD  class= input8>
            <Input class= common8 name=HeadShip1 verify="保险联系人一职务|len<=30">
          </TD>                      
        </TR>
                <TR  class= common>
          <TD  class= title8>
            职务
          </TD>
          <TD  class= input8>
            <Input class= common8 name=HeadShip2 verify="保险联系人二职务|len<=30">
          </TD>                
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax2 verify="保险联系人二传真|len<=30">
          </TD>       
        </TR>
         <TR  class= common>
          <TD  class= title8>
            币种
          </TD>
          <TD  class= input8>
            <Input class=code8 name=Currency verify="币种|len<=2" ondblclick="showCodeList('currency',[this]);" onkeyup="showCodeListKey('currency',[this]);">
          </TD>
             <Input class= common8 name=Prem >
             <Input class= common8 name=Amnt >    
        </TR>     
        <tr class=common>
        <td class= titleImg>
    			
    		</td>
        </TR> 
      </table>
</DIV>   

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol3);">
    		</td>
    		<td class= titleImg>
    			 团体保单险种信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divGroupPol3" style= "display: ''">
    <Div  id= "divGroupPol5" style= "display: 'none'">
    <table  class= common>
        <TR  class= common>
          <TD  class= title>
            团体合同号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GrpContNo  >
          </TD>
    
        </TR>
  </table>   
  </Div>     
  <!--录入的暂交费表部分 -->
    <Table  class= common>
      	<tr>
    	 <td text-align: left colSpan=1>
	 <span id="spanRiskGrid" >
	 </span> 
	</td>
       </tr>
    </Table>
<Div  id= "divGroupPol4" style= "display: ''">
          
     <Div  id= "divRiskDeal" style= "display: ''">
        <table>
        <TR >     
     <TD >
      <input type =button class=cssButton value="添加险种" onclick="addRecord();">      
     </TD>
     <TD >
      <input type =button class=cssButton value="删除险种" onclick="deleteRecord();">      
     </TD>
    </TR> 
    </table> 
</Div>
</Div>
</DIV>
    
<Div  id= "divHidden" style= "display: 'none'">
  <table class=common> 
      <TR  class= common>               
          <TD  class= title>
            封顶线
          </TD>
          <TD  class= input>
            <Input class= common name=PeakLine >
          </TD>
        </TR>
        <TR  class= common>          
          <TD  class= title>
            医疗费用限额
          </TD>
          <TD  class= input>
            <Input class= common name=MaxMedFee >
          </TD>
        </TR>
      </table>  
  </Div>    
    <hr/>
    <Div  id= "divnormalbtn" style= "display: ''" > 
      <!--INPUT class=cssButton VALUE="险种信息"  TYPE=button onclick="grpFeeInput()"        
      <INPUT class=cssButton VALUE="保障计划定制"  TYPE=button onclick="grpRiskPlanInfo()">-->
      <Input VALUE="管理费定义" class=cssButton  TYPE=button onclick="GrpRiskFeeInfo();">
      <Input VALUE="归属规则定义" class=cssButton  TYPE=button onclick="AscriptionRuleInfo();">
	  <INPUT VALUE="退保费率查询 "   class=cssButton TYPE=button onclick="ChangExtrateRate();">
	  <INPUT class=cssButton VALUE="被保人清单"  TYPE=button onclick="grpInsuList();">
	  <INPUT class=CssButton VALUE="万能账户查询" TYPE=button onclick="GPublicAccQuery();"> 
       <INPUT class=CssButton VALUE="保单缴费明细" TYPE=button onclick="payQueryClick();">
	   <INPUT class=CssButton VALUE="投保书查询" TYPE=button onclick="ScanQuery();"> 
		<INPUT class=CssButton VALUE="保全信息" TYPE=button onclick="taskQueryClick();"> 
		<INPUT class=CssButton VALUE="理赔明细" TYPE=button onclick="claimQueryClick();"> 
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      <!--在处理公共账户处理错误，不应将公共账户挂在人下，应处理一个单独一个人承担一个公共账户-->

 <!-- 
      <INPUT class=cssButton VALUE="定义建工意外要素"  TYPE=button onclick="initContractor()"> -->
	  <Input VALUE="缴费定义" class=cssButton  TYPE=hidden onclick="PayRuleInfo();">
      <!--INPUT class=cssButton VALUE="分单定制"  TYPE=button onclick="grpSubContInfo()"-->
      <!--INPUT class=cssButton VALUE="年龄分布"  TYPE=button onclick="grpPersonAge()"-->
 </Div>
    <Div  id= "divapprovenormalbtn" style= "display: 'none'" > 
      <!--INPUT class=cssButton VALUE="险种信息"  TYPE=button onclick="grpFeeInput()"--> 
      <INPUT class=cssButton VALUE="保障计划定制"  TYPE=button onclick="grpRiskPlanInfo()">   
      <INPUT class=cssButton VALUE="定义公共账户"  TYPE=button onclick="grpPubAccInput()">
      <INPUT class=cssButton VALUE="被保人清单"  TYPE=button onclick="grpInsuList()">
      <!--INPUT class=cssButton VALUE="分单定制"  TYPE=button onclick="grpSubContInfo()"-->
    </Div>
     <Div  id= "divnormalquesbtn" style= "display: 'none'" align= right> 
      <hr/>            
      <INPUT VALUE="录入完毕"    class=cssButton TYPE=button onclick="GrpInputConfirm(1);">
      <INPUT VALUE="团体问题件处理"    class=cssButton TYPE=hidden onclick="GrpQuestQuery();">
      <INPUT VALUE="团体问题件处理"    class=cssButton TYPE=button onclick="GrpQuestInput();">
      </Div>
      <Div id="divfilllistbtn" style="display: 'none'" align=left>
			<INPUT VALUE="补名单" class=cssButton TYPE=button onclick="grpfilllist();">
			<INPUT class=cssButton VALUE="被保人清单"  TYPE=button onclick="grpInsuList()">
			<INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">		
		  </Div>
      <Div  id= "divapprovebtn" style= "display: 'none'" align= right> 
      <hr/>            
      <INPUT VALUE="团体复核通过" class=cssButton TYPE=button onclick="gmanuchk();">      
      <INPUT VALUE="团体问题件处理"    class=cssButton TYPE=hidden onclick="GrpQuestQuery();">
      <INPUT VALUE="团体问题件处理"    class=cssButton TYPE=button onclick="GrpQuestInput();">
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      </Div>
      <Div  id= "divapproveupdatebtn" style= "display: 'none'" align= right> 
      <hr/>
      <INPUT VALUE="修  改" class=cssButton TYPE=button onclick="updateClick();">            
      <INPUT VALUE="复核修改确认" class=cssButton TYPE=button onclick="GrpInputConfirm(2);">    
      <INPUT VALUE="团体问题件处理"    class=cssButton TYPE=hidden onclick="GrpQuestQuery();">
      <INPUT VALUE="团体问题件处理"    class=cssButton TYPE=button onclick="GrpQuestInput();">
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      </Div>
      <Div  id= "divchangplanbtn" style= "display: 'none'" align= right> 
      <INPUT VALUE="修  改" class=cssButton TYPE=button onclick="updateClick();">            
      
      <INPUT VALUE="团体问题件处理"    class=cssButton TYPE=hidden onclick="GrpQuestQuery();">
      <INPUT VALUE="团体问题件处理"    class=cssButton TYPE=button onclick="GrpQuestInput();">
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      </Div>
      
      <Div  id= "divuwupdatebtn" style= "display: 'none'" align= left> 
      <hr/>          
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      </Div>
      <Div  id= "divquerybtn" style= "display: 'none'" align= right> 
      <hr/>  
      <INPUT VALUE="团体问题件处理"    class=cssButton TYPE=button onclick="GrpQuestQuery();">        
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">
      </Div> 
      <div id="autoMoveButton" style="display: none">
	 <Input VALUE="管理费定义" class=cssButton  TYPE=button onclick="GrpRiskFeeInfo();">
      <Input VALUE="归属规则定义" class=cssButton  TYPE=button onclick="AscriptionRuleInfo();">
	  <INPUT VALUE="退保费率修改 "   class=cssButton TYPE=button onclick="ChangExtrateRate();">
	<%--<input type="button" name="Next" value="下一步" onclick="location.href='ContInsuredInput.jsp?LoadFlag='+tLoadFlag+'&prtNo='+prtNo+'&checktype=2&scantype='+scantype" class=cssButton>	
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="goback();">       
        --%>
        <input type=hidden id="" name="autoMoveFlag">
        <input type=hidden id="" name="autoMoveValue">   
        <input type=hidden id="" name="pagename" value="21">                         
      </div>         
      <input type=hidden id="fmAction" name="fmAction">  
      <input type=hidden id="" name="WorkFlowFlag">      
      <!--INPUT class=cssButton VALUE="导入被保人清单"  TYPE=button onclick=""-->   
      <!--<INPUT class=cssButton VALUE="进入保单险种信息"  TYPE=button onclick="grpRiskInfo()">-->   
  		
  		<input type=hidden name=LoadFlag>
  		<input type=hidden name=RiskType><!--yangming：用于区分普通险种和套餐类险种-->
  		<input type=hidden name=Resource>
  		<input type=hidden id="" name=MissionProp20> 
  <div id= "divBigProjectApprove" style= "display: 'none'" align= right>
  	<INPUT VALUE="团体复核通过" class=cssButton TYPE=button onclick="BigProjectApprove();">
  </div>
  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 