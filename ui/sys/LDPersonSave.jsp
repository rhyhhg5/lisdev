<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LDPersonSave.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LDPersonSchema tLDPersonSchema   = new LDPersonSchema();
  LCAddressSchema tLCAddressSchema   = new LCAddressSchema();
  LCAccountSchema tLCAccountSchema   = new LCAccountSchema();  
  LDPersonUI tLDPersonUI   = new LDPersonUI();
  //输出参数
  String FlagStr = "";
  String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
   String Operator  = tGI.Operator ;  //保存登陆管理员账号
   String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
  
  CErrors tError = null;
  String tBmCert = "";
  
  //后面要执行的动作：添加，修改，删除
  String transact=request.getParameter("Transact");
System.out.println("transact:"+transact); 

/*        
  String tLimit="";
  String CustomerNo="";
*/ 
    if(transact.equals("INSERT"))
    {
    //生成客户号
    //tLimit=PubFun.getNoLimit(ManageCom);
/*    
    tLimit = "SN";
    CustomerNo=PubFun1.CreateMaxNo("CustomerNo",tLimit);   
    tLDPersonSchema.setCustomerNo(CustomerNo);
*/
    }
    else{
    tLDPersonSchema.setCustomerNo(request.getParameter("CustomerNo"));    
    }
    tLDPersonSchema.setName(request.getParameter("Name"));
    tLDPersonSchema.setSex(request.getParameter("Sex"));
    tLDPersonSchema.setBirthday(request.getParameter("Birthday"));
    tLDPersonSchema.setIDType(request.getParameter("IDType"));
    tLDPersonSchema.setIDNo(request.getParameter("IDNo"));
    tLDPersonSchema.setPassword(request.getParameter("Password"));
    tLDPersonSchema.setNativePlace(request.getParameter("NativePlace"));
    tLDPersonSchema.setNationality(request.getParameter("Nationality"));
    tLDPersonSchema.setRgtAddress(request.getParameter("RgtAddress"));
    tLDPersonSchema.setMarriage(request.getParameter("Marriage"));
    tLDPersonSchema.setMarriageDate(request.getParameter("MarriageDate"));
    tLDPersonSchema.setHealth(request.getParameter("Health"));
    tLDPersonSchema.setStature(request.getParameter("Stature"));
    tLDPersonSchema.setAvoirdupois(request.getParameter("Avoirdupois"));
    tLDPersonSchema.setDegree(request.getParameter("Degree"));
    tLDPersonSchema.setCreditGrade(request.getParameter("CreditGrade"));
    tLDPersonSchema.setOthIDType(request.getParameter("OthIDType"));
    tLDPersonSchema.setOthIDNo(request.getParameter("OthIDNo"));
    tLDPersonSchema.setICNo(request.getParameter("ICNo"));
    tLDPersonSchema.setGrpNo(request.getParameter("GrpNo"));
    tLDPersonSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
    tLDPersonSchema.setStartWorkDate(request.getParameter("StartWorkDate"));
    tLDPersonSchema.setPosition(request.getParameter("Position"));
    tLDPersonSchema.setSalary(request.getParameter("Salary"));
    tLDPersonSchema.setOccupationType(request.getParameter("OccupationType"));
    tLDPersonSchema.setOccupationCode(request.getParameter("OccupationCode"));
    tLDPersonSchema.setWorkType(request.getParameter("WorkType"));
    tLDPersonSchema.setPluralityType(request.getParameter("PluralityType"));
    tLDPersonSchema.setDeathDate(request.getParameter("DeathDate"));
    tLDPersonSchema.setSmokeFlag(request.getParameter("SmokeFlag"));
    tLDPersonSchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));
    tLDPersonSchema.setProterty(request.getParameter("Proterty"));
    tLDPersonSchema.setRemark(request.getParameter("Remark"));
    tLDPersonSchema.setState(request.getParameter("State"));
    tLDPersonSchema.setVIPValue(request.getParameter("VIPValue"));
    tLDPersonSchema.setOperator(Operator);
     
    
    tLCAddressSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLCAddressSchema.setAddressNo(request.getParameter("AddressNo"));
    tLCAddressSchema.setPostalAddress(request.getParameter("PostalAddress"));
    tLCAddressSchema.setZipCode(request.getParameter("ZipCode"));
    tLCAddressSchema.setPhone(request.getParameter("Phone"));
    tLCAddressSchema.setFax(request.getParameter("Fax"));
    tLCAddressSchema.setHomeAddress(request.getParameter("HomeAddress"));
    tLCAddressSchema.setHomeZipCode(request.getParameter("HomeZipCode"));
    tLCAddressSchema.setHomePhone(request.getParameter("HomePhone"));
    tLCAddressSchema.setHomeFax(request.getParameter("HomeFax"));
    tLCAddressSchema.setCompanyAddress(request.getParameter("CompanyAddress"));
    tLCAddressSchema.setCompanyZipCode(request.getParameter("CompanyZipCode"));
    tLCAddressSchema.setCompanyPhone(request.getParameter("CompanyPhone"));
    tLCAddressSchema.setCompanyFax(request.getParameter("CompanyFax"));
    tLCAddressSchema.setMobile(request.getParameter("Mobile"));
    tLCAddressSchema.setMobileChs(request.getParameter("MobileChs"));
    tLCAddressSchema.setEMail(request.getParameter("EMail"));
    tLCAddressSchema.setBP(request.getParameter("BP"));
    tLCAddressSchema.setMobile2(request.getParameter("Mobile2"));
    tLCAddressSchema.setMobileChs2(request.getParameter("MobileChs2"));
    tLCAddressSchema.setEMail2(request.getParameter("EMail2"));
    tLCAddressSchema.setBP2(request.getParameter("BP2"));
    tLCAddressSchema.setOperator(Operator);
    
    tLCAccountSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLCAccountSchema.setAccKind(request.getParameter("AccKind"));
    tLCAccountSchema.setBankCode(request.getParameter("BankCode"));
    tLCAccountSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLCAccountSchema.setAccName(request.getParameter("AccName"));
    tLCAccountSchema.setOperator(Operator);    
    
 try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tLDPersonSchema);
   tVData.addElement(tLCAddressSchema);
   tVData.addElement(tLCAccountSchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  if ( tLDPersonUI.submitData(tVData,transact))
  {
    		if (transact.equals("INSERT"))
		{
		    	System.out.println("------return");
			    	
		    	tVData.clear();
		    	tVData = tLDPersonUI.getResult();
		    	System.out.println("-----size:"+tVData.size());
		    	
		    	LDPersonSchema mLDPersonSchema = new LDPersonSchema(); 
		    	LCAddressSchema mLCAddressSchema=new LCAddressSchema();
			mLDPersonSchema=(LDPersonSchema)tVData.getObjectByObjectName("LDPersonSchema",0);
			mLCAddressSchema=(LCAddressSchema)tVData.getObjectByObjectName("LCAddressSchema",0);
			System.out.println("test");
			
			if( mLDPersonSchema.getCustomerNo() == null ) {
				System.out.println("null");
			}
			if( mLCAddressSchema.getAddressNo() == null ) {
				System.out.println("null");
			}			
			
			String strCustomerNo = mLDPersonSchema.getCustomerNo();
			String strAddressNo=mLCAddressSchema.getAddressNo();
			System.out.println("jsp"+strCustomerNo);
			if( strCustomerNo == null ) {
				strCustomerNo = "123";
				System.out.println("null");
			}
			
			// System.out.println("-----:"+mLAAgentSchema.getAgentCode());
		%>
		<SCRIPT language="javascript">
			parent.fraInterface.fm.all("CustomerNo").value ="<%=strCustomerNo%>";
			parent.fraInterface.fm.all("AddressNo").value ="<%=strAddressNo%>";
		</SCRIPT>
		<%
		}
    		if (transact.equals("UPDATE"))
    		{
		    	System.out.println("------return");
			    	
		    	tVData.clear();
		    	tVData = tLDPersonUI.getResult();
		    	System.out.println("-----size:"+tVData.size());
		    	LCAddressSchema mLCAddressSchema=new LCAddressSchema();
			mLCAddressSchema=(LCAddressSchema)tVData.getObjectByObjectName("LCAddressSchema",0);
			if( mLCAddressSchema.getAddressNo() == null ) {
				System.out.println("null");
			}						
			String strAddressNo=mLCAddressSchema.getAddressNo();
			
		%>
		<SCRIPT language="javascript">
			parent.fraInterface.fm.all("AddressNo").value ="<%=strAddressNo%>";
		</SCRIPT>   
		<%		 		       
    		}
	}
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDPersonUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
   }
}//页面有效区
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

