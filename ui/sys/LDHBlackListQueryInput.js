
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();



//***************************************************
//* 单击“查询”进行的操作
//***************************************************

function easyQueryClick()
{  
  if( verifyInput() == false )   
  	return false;  
	var tSQL = "";

	tSQL = "select BlacklistNo," 
	+ " (case when Type = '0' then '个人' when Type = '1' then '组织' end ) Type," 
	+ " Name," 
	+ " (case when SEX = '0' then '男' when SEX = '1' then '女' end ) SEX," 
	+ " Birthday,"
	+ " (select codename from ldcode where codetype = 'idtype' and code = IDType),"
	+ " IDNo, "
	+ " OrganComCode,"  //组织机构代码
	+ " UnifiedSocialCreditNo,"  //统一社会信用代码
	+ " PublishCom,"  //发布机构
	+ " (case when Cause = '01' then '同业重疾或伤残理赔史' when Cause = '02' then '密集投保' when Cause = '03' then '不如实告知史' when Cause = '04' then '其他' end ) Cause,"  //原因
	+ " Remark"
    + " FROM "
    + " LDHBlackList" 
    + " where  1=1  "
	+ getWherePart('BlacklistNo', 'BlacklistNo')
	+ getWherePart('Name', 'Name')
    + " ORDER BY BlacklistNo" ;
	
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  
		alert("没有符合条件的信息！"); 
		turnPage.queryModal(tSQL, EvaluateGrid); 
		return false;
	}
    else 
    	turnPage.queryModal(tSQL, EvaluateGrid);
}

//***************************************************
//* 单击“新增”进行的操作
//***************************************************
function gotoAdd()
{
  //下面增加相应的代码
  showInfo=window.open("./LDHBlackListMain.jsp?transact=insert");
}

//***************************************************
//* 单击“修改”进行的操作
//***************************************************
function gotoUpdate()
{
  var checkFlag = 0;
  for (i=0; i<EvaluateGrid.mulLineCount; i++)
  {
    if (EvaluateGrid.getSelNo(i))
    {
      checkFlag = EvaluateGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	var	tBlacklistNo = EvaluateGrid.getRowColData(checkFlag - 1, 1);
  	showInfo=window.open("./LDHBlackListMain.jsp?transact=update&BlacklistNo="+tBlacklistNo);
  }else{
  	alert("请选择需要修改的黑名单信息！");
  	return false;
  }
  return true;
}
//***************************************************
//* 点击“删除”进行的操作
//***************************************************
function deleteClick()
{
  var checkFlag = 0;
  for (i=0; i<EvaluateGrid.mulLineCount; i++)
  {
    if (EvaluateGrid.getSelNo(i))
    {
      checkFlag = EvaluateGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	 var tBlacklistNo = EvaluateGrid.getRowColData(checkFlag - 1, 1);
  	 if(tBlacklistNo== null || tBlacklistNo == ""){
  	 	alert("删除时，获取黑名单人员编码失败！");
  		return false;
  	 }
  	 if (confirm("您确实想删除该记录吗?"))
	  {
	  var i = 0;
	  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.fmtransact.value = "DELETE||MAIN";
	  fm.action = "./LDHBlackListQuerySave.jsp?BlacklistNo="+tBlacklistNo;
	  fm.submit(); //提交
	  fm.action="";
	  initForm();
	  }
	  else
	  {
	    alert("您取消了删除操作！");
	  }
  }else{
  	alert("请选择需要删除的黑名单信息！");
  	return false;
  }
}
//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content,aBlacklistNo)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  easyQueryClick();
} 
