//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

// 数据返回父窗口
function returnMain()
{
	top.close();
}

/*********************************************************************
 *  显示保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showPolDetail()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	var cPolNo = "";
	if( tSel != null && tSel != 0 )
		cPolNo = GrpPolGrid.getRowColData( tSel - 1, 1 );

	if( cPolNo == null || cPolNo == "" )
		alert("请选择一张投保单后，再进行查询个人投保单明细操作");
	else
	{
//		mSwitch.deleteVar( "PolNo" );
//		mSwitch.addVar( "PolNo", "", cPolNo );
//		
//		window.open("./ProposalMain.jsp?LoadFlag=4");
			window.open("../sys/PolDetailQueryMain.jsp?PolNo=" + cPolNo + "&IsCancelPolFlag=0");
	}
}
       
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select PolNo,InsuredName,InsuredSex,InsuredBirthday,Prem,Amnt,SequenceNo from LCPol where 1=1 "
				 + "and AppFlag='1' "
				 + "and GrpPolNo='" + groupPNo + "'"
				 + getWherePart( 'PolNo' )
				 + getWherePart( 'InsuredName' )
				 + getWherePart( 'InsuredSex' )
				 + getWherePart( 'InsuredBirthday' )
				 + getWherePart( 'InsuredAppAge' )
				 + getWherePart( 'OccupationType' )
				 + getWherePart('SequenceNo');
//alert(strSQL);
	//execEasyQuery( strSQL );
	turnPage.queryModal(strSQL, GrpPolGrid);
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpPolGrid();
		GrpPolGrid.recordNo = (currPageIndex - 1) * MAXSCREENLINES;
		GrpPolGrid.loadMulLine(GrpPolGrid.arraySave);
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpPolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

function deleteClick() {
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
    if (GrpPolGrid.getSelNo(i)) { 
      checkFlag = GrpPolGrid.getSelNo(i);
      break;
    }
  }
  
  if (checkFlag) { 
  	var cProposalNo = GrpPolGrid.getRowColData(checkFlag - 1, 1); 	

    mAction = "DELETE";
		fm.all('fmAction').value = mAction;
		fm.all('ProposalNo').value = cProposalNo;
		fm.submit(); //提交
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content ) {
	if (FlagStr == "Fail" )	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else { 
		content = "撤单成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	
	fm.all('ProposalNo').value = "";
	
	easyQueryClick();
}
