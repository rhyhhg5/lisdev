   //               该文件中包含客户端需要处理的函数和事件
//程序名称：DoubleContListInput.js
//程序功能：Input.js
//创建日期：2017-11-1
//创建人  ：yangjian
var showInfo;
var mDebug="0";

try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
 
 
//执行查询
function DoubleContListQuery()
{
  fm.target="fraSubmit";
  //首先检验录入框
  if(!verifyForm("fm")) return false;
  // 拼SQL语句，从页面采集信息
  var strSQL = "select a.ManageCom ," +
  		"(select name from ldcom where comcode=a.managecom) ," +
  		"a.salechnl||'-'||db2inst1.codename('salechnl',a.salechnl) Salechnl," +
  		"a.agentcode," +
  		"(select name from laagent where agentcode=a.agentcode) Name," +
  		"(select branchattr from labranchgroup where agentgroup=a.agentgroup)  ," +
  		"(select name from labranchgroup where agentgroup=a.agentgroup) ," +
  		"a.prtno," +
  		"a.appntname," +
  		"a.appntbirthday," +
  		"db2inst1.codename('sex', a.appntsex)," +
  		"db2inst1.codename('idtype',a.appntidtype)," +
  		"a.appntidno," +
  		"a.polapplydate," +
  		"a.cvalidate," +
  		"db2inst1.codename('appflag',a.appflag),'','', " +
  		"(select name from lacom where agentcom=a.agentcom) agentcomName, " +
  		"a.agentcom, " +
  		"(select name from laagenttemp where agentcode=a.agentsalecode) agentsalecodeName, " +
  		"a.agentsalecode " +
  		"from lccont a " +
  		"where 1=1 and conttype='1'	" +
  		"and uwflag<>'a'" +
  		"and exists (select 1 from lccontsub where prtno=a.prtno and RecordFlag='1') "    
		+ getWherePart('a.ManageCom','ManageCom','like')
		+ getWherePart('Salechnl','Salechnl')
		+ getWherePart('a.AgentCode','AgentCode')
		+ getWherePart('Name','Name')
		+ getWherePart("a.POLAPPLYDATE","SignStartDate",">=")					 
		+ getWherePart("a.POLAPPLYDATE","SignEndDate","<=");	
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    DoubleContListGrid.clearData('DoubleContListGrid');  
    alert("查询失败！");
    return false;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = DoubleContListGrid;    
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}	
		
		
//下载	
function DoubleContListDownload()
{
	
	if(!verifyForm("fm")) return false;
 // 书写SQL语句
	var strSQLD = "select a.ManageCom ," +
  		"(select name from ldcom where comcode=a.managecom) ," +
  		"a.salechnl||'-'||db2inst1.codename('salechnl',a.salechnl) Salechnl," +
  		"a.agentcode," +
  		"(select name from laagent where agentcode=a.agentcode) Name," +
  		"(select branchattr from labranchgroup where agentgroup=a.agentgroup)  ," +
  		"(select name from labranchgroup where agentgroup=a.agentgroup) ," +
  		"a.prtno," +
  		"a.appntname," +
  		"a.appntbirthday," +
  		"db2inst1.codename('sex', a.appntsex)," +
  		"db2inst1.codename('idtype',a.appntidtype)," +
  		"a.appntidno," +
  		"a.polapplydate," +
  		"a.cvalidate," +
  		"db2inst1.codename('appflag',a.appflag),'','', " +
  		"(select name from lacom where agentcom=a.agentcom) agentcomName, " +
  		"a.agentcom, " +
  		"(select name from laagenttemp where agentcode=a.agentsalecode) agentsalecodeName, " +
  		"a.agentsalecode " +
  		"from lccont a " +
  		"where 1=1 and conttype='1'	" +
  		"and uwflag<>'a'" +
  		"and exists (select 1 from lccontsub where prtno=a.prtno and RecordFlag='1') "    
	+ "and a.managecom like '"+fm.all('ManageCom').value+"%'"
	+ getWherePart('Salechnl','Salechnl')
	+ getWherePart('a.AgentCode','AgentCode')
	+ getWherePart('Name','Name')
	+ getWherePart("a.POLAPPLYDATE","SignStartDate",">=")					 
	+ getWherePart("a.POLAPPLYDATE","SignEndDate","<=");
    fm.querySql.value = strSQLD;
    fm.action = "DoubleContListReport.jsp";
    fm.submit();
}				
		
		
		
		
		
		
		
		
		
		
  

