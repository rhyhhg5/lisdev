//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var arrAllDataSet;

/*********************************************************************
 *  查询缴费信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPayGrid()
{
	// 初始化表格
	initPayGrid();	
	// 书写SQL语句RiskCode
	var strSQL = "";

	//tContLoadFlag = 2 查询集体实收表
    //tContLoadFlag = 1 查询个人实收表

	//查询集体实收表
	if (tContLoadFlag == "2")
	{
		strSQL = "select a.PayNo,a.LastPayToDate, "
		  + "   (select sum(SumDuePayMoney) from LJAPayGrp where payNo=a.payNo and payType='ZC'), "
		  + "   a.PayDate,sum(a.SumActuPayMoney),"
		  + "   (select nvl(sum(SumDuePayMoney), 0) from LJAPayGrp where payNo=a.payNo and payType='YEL'),"
		  + "   (select MakeDate from LJSPayB where GetNoticeNo = a.GetNoticeNo),a.EnterAccDate,"
		  + "   CodeName('dealstate', (select DealState from LJSPayB where GetNoticeNo = a.GetNoticeNo)), "
		  + "   a.GetNoticeNo "
		  + " from LJAPayGrp a "  
			+ " where GrpContNo='" + ContNo + "' and a.paytype <> 'YET' "
			+ "   and exists(select 1 from LJAPay where PayNo = a.PayNo and IncomeType  in('1', '2','3')) "
			+ " group by  a.PayNo, a.getNoticeNo, a.LastPayToDate,a.PayDate ,a.EnterAccDate "
			+ "order by a.PayNo "; 
	}else
	{
    strSQL = "select a.PayNo, "
      + "   (select nvl(sum(SumDuePayMoney), 0) from LJAPayPerson where payNo=a.payNo and payType='YEL'), "
      + "   a.LastPayToDate,"
      + "   (select sum(SumDuePayMoney) from LJAPayPerson where payNo=a.payNo and payType='ZC'),"
      + "     a.EnterAccDate,sum(a.SumActuPayMoney), "
      + "   (select sum(SumActuPayMoney)from LJAPayPerson where ContNo = a.ContNo and LastPayToDate <= a.LastPayToDate and PayType = 'ZC'), "
      + "   (select MakeDate from LJSPayB where GetNoticeNo = a.GetNoticeNo),a.MakeDate, "
      + "   CodeName('dealstate', (select DealState from LJSPayB where GetNoticeNo = a.GetNoticeNo)),"
      + "   a.Operator, a.GetNoticeNo "
      + " from LJAPayPerson a" 
			+ "  where ContNo='" + ContNo + "' and a.paytype <> 'YET' "
			+ "   and exists(select 1 from LJAPay where PayNo = a.PayNo and IncomeType in('1', '2','10')) "
			+ "group by a.PayNo, a.getNoticeNo, a.ContNO, a.Operator, a.MakeDate, a.LastPayToDate,a.EnterAccDate  "
			+ "order by a.PayNo ";
	}
	//查询SQL，返回结果字符串
		fm.printSql.value = strSQL;
   turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
  //判断是否查询成功
  if (!turnPage2.strQueryResult) {
  	PayGrid.clearData();
 
    return false;
  }
 
  //查询成功则拆分字符串，返回二维数组
  turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult);
  arrAllDataSet = turnPage2.arrDataCacheSet;

  //设置初始化过的MULTILINE对象
  turnPage2.pageDisplayGrid = PayGrid;   
         
  //保存SQL语句
  turnPage2.strQuerySql   = strSQL;
 // fm.LCIndipaySql.value = strSQL; //fx
 
  //设置查询起始位置
  turnPage2.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //setInvalidate(); 
  var arrDataSet = turnPage2.getData(arrAllDataSet, turnPage2.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage2.pageDisplayGrid);
  
  
  //查询余额
  if (tContLoadFlag == "2")
  {
	  queryDifGrp();
  }else
  {
	   queryDif();
  }
}  

/*********************************************************************
 *  查询保单余额
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryDif()
{
  for (i=0;i<PayGrid.mulLineCount;i++ )
	{
	  var payNo = PayGrid.getRowColDataByName(i, "PayNo");
	  if(payNo == null || payNo == "" || payNo == "null")
	  {
	    continue;
	  }
  	var strSql = "select sum(SumDuePayMoney) from LJAPayPerson "
  	            + "where payNo = '" + payNo + "' "
  	            + "   and contNo = '" + ContNo + "' "
  	            + "   and payType = 'YEL'";
  	
  	var arrReturn = easyExecSql(strSql);
  	if (arrReturn != null && arrReturn[0][0] != "" && arrReturn[0][0] != "null")
  	{
  		PayGrid.setRowColDataByName(i, "AppAccMoney", arrReturn[0][0]);
  	}
		
		//处理状态
		strSql = "select codeName from LDCode a, LJSPayB b "
		         + "where a.codeType = 'dealstate' and a.code=b.dealstate "
		         + "    and b.getNoticeNo = '" + PayGrid.getRowColDataByName(i, "GetNoticeNo") + "' ";
	  var arrReturn = easyExecSql(strSql);
  	if (arrReturn == null || arrReturn[0][0] == "" || arrReturn[0][0] == "null")
  	{
  		continue;
  	}
		PayGrid.setRowColDataByName(i, "DealState", arrReturn[0][0]);
	}
  
	//var strTableName = "";
	//if (tContType=='1')
	//{
	//	strTableName = "LCCont";
	//}else if (tContType=='2')
	//{  
	//	strTableName = "LBCont";
	//}
  //
	//var strSql = "select Dif from " + strTableName　+　" where ContNo='" + ContNo +　"'";
	//var arrReturn = easyExecSql(strSql);
	//if (arrReturn == null)
	//{
	//	return false;
	//}else{
	//	for (i=0;i<arrAllDataSet.length ;i++ )
	//	{
	//		arrAllDataSet[i][5] = arrReturn[0][0];
	//	}
	//}
  
} 
/*********************************************************************
 *  查询集体保单余额
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryDifGrp()
{
	for (i=0;i<PayGrid.mulLineCount;i++ )
	{
	  var payNo = PayGrid.getRowColDataByName(i, "PayNo");
	  if(payNo == null || payNo == "" || payNo == "null")
	  {
	    continue;
	  }
  	var strSql = "select sum(SumDuePayMoney) from LJAPayGrp "
  	            + "where payNo = '" + payNo + "' "
  	            + "   and grpContNo = '" + ContNo + "' "
  	            + "   and payType = 'YEL'";
  	
  	var arrReturn = easyExecSql(strSql);
  	if (arrReturn != null && arrReturn[0][0] != "" && arrReturn[0][0] != "null")
  	{
  		PayGrid.setRowColDataByName(i, "AppAccMoney", arrReturn[0][0]);
  	}
		
		//处理状态
		strSql = "select codeName from LDCode a, LJSPayB b "
		         + "where a.codeType = 'dealstate' and a.code=b.dealstate "
		         + "    and b.getNoticeNo = '" + PayGrid.getRowColDataByName(i, "GetNoticeNo") + "' ";
	  var arrReturn = easyExecSql(strSql);

  	if (arrReturn == null || arrReturn[0][0] == "" || arrReturn[0][0] == "null")
  	{
  		continue;
  	}
		PayGrid.setRowColDataByName(i, "DealState", arrReturn[0][0]);
	}
}

/*********************************************************************
 *  特别缴费约定查询
 *  参数  ： 无
 *  返回值：  无
 *********************************************************************
 */
function specQuery()
{
	var arrReturn = new Array();
	  initSpecGrid();
	 // 书写SQL语句

	 var strTableName = "";
	 if (tContType=='1')
	 {
		 strTableName = "LCPol";
	 }else if (tContType=='2')
  	 {  
		strTableName = "LBPol";
	 }

	  var mSQL = "";
	  mSQL = "select PolNo,PrtNo from "+strTableName +" where 1>2";			 
		
		//查询SQL，返回结果字符串
	 turnPage1.strQueryResult  = easyQueryVer3(mSQL, 1, 0, 1);  
	  
	  //判断是否查询成功
	  if (!turnPage1.strQueryResult) {
		SpecGrid.clearData();
		return false;
	  }
	  
	  //查询成功则拆分字符串，返回二维数组
	  turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
	  
	  //设置初始化过的MULTILINE对象
	  turnPage1.pageDisplayGrid = SpecGrid;    
			  
	  //保存SQL语句
	  turnPage1.strQuerySql     = mSQL; 
	  
	  //设置查询起始位置
	  turnPage1.pageIndex = 0;  
	  
	  //在查询结果数组中取出符合页面显示大小设置的数组
	  var arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex, MAXSCREENLINES);
	  
	  //调用MULTILINE对象显示查询结果
	  displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);
}

/*********************************************************************
 *  设置文本框值
 *  参数  ： 无
 *  返回值：  无
 *********************************************************************
 */
function setBoxValue()
{
	if (ContNo !=null)
	try { fm.all('ContNo').value = ContNo; } catch(ex) { };

	var strTableName = "";


	//设置投保人
	if (tContLoadFlag == "2")
	{ 
	    if (tContType=='1')
	    {
		   strTableName = "LCGrpCont";
	    }else if (tContType=='2')
  	    {  
		    strTableName = "LBGrpCont";
	    }

		var mSQL =  "select GrpName from "+ strTableName +" where GrpContNo='" + ContNo + "'";	
	}else
	{
		if (tContType=='1')
	    {
		   strTableName = "LCCont";
	    }else if (tContType=='2')
  	    {  
		    strTableName = "LBCont";
	    }
        var mSQL =  "select AppntName from " + strTableName + " where ContNo='" + ContNo + "'";	
	}
	var arrReturn = easyExecSql(mSQL);
	if (arrReturn == null)
	{}else
	{
        try { fm.all('Name').value = arrReturn[0][0]; } catch(ex) { };
	}
}

//------------------打印------------------
function printContList(contType)
{
	
  	fm.action = "PrtLCIndiPayQuerySave.jsp?ContType=" + contType;
	fm.target = "_blank";
	fm.submit();
}

