<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="AllProposalQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AllProposalQueryInit.jsp"%>
  <title>保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            投保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=ProposalNo >
          </TD>
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);">
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskVersion ondblclick="return showCodeList('RiskVersion',[this]);" onkeyup="return showCodeListKey('RiskVersion',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode verify="代理人编码|notnull&code:AgentCode" ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [1, 0]);" onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [1, 0]);">
          </TD>
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AgentGroup verify="代理人组别|notnull&len<=12" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="保单明细" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
     <!-- <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 	
     --> 
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
