var turnPage = new turnPageClass();
var k = 0;

//点击“查询”执行的函数
function selectSalesMan()
{	
	var GrpAgentCode = fm.GrpAgentCode.value;
	
	if(GrpAgentCode==""){
		alert("请输入要查询的业务员代码");
		return;
	}
	fm.submit();
}
function afterSubmit(rspSaleInfo,errInfo){
	
	if(rspSaleInfo=="01"){
		alert(errInfo);
	}else{
		if("null#null#null#null#null#"==rspSaleInfo){
			alert("系统异常，请稍后重试");
		}else{
			easyQueryClick(rspSaleInfo);
		}
		
	}
	
}
//点击“返回”执行的函数
function ReturnData()
{
	
	var tRow=GrpGrid.getSelNo();
  	if (tRow==0)
   	{
   		alert("请您先进行选择!");
  		return;
  	}
  	  
	try{
		var arrReturn = getQueryResult();
		var arrA = arrReturn.split("#");
		if("null"==arrA[0]||""==arrA[0]||"null"==arrA[2]||""==arrA[2]||"null"==arrA[3]||""==arrA[3]||"null"==arrA[4]||""==arrA[3]){
			alert("您返回的业务员信息不完整，请查询后在返回");
			return;
		}
		top.opener.afterQueryMIX(arrReturn);
		}catch(ex){
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	top.close();
	
}
//点击“取消”执行的函数
function resetSalesMan(){
	fm.GrpAgentCode.value="";
}

function easyQueryClick(rspSaleInfo)
{
  // 初始化表格 
  if(!verifyInput2())
    return false;
  var saleInfo =  rspSaleInfo.split("#");
  
  if(null==saleInfo[0]||""==saleInfo[0]||"null"==saleInfo[0]){
  	alert("没有此业务员信息，请重新输入查询条件");
  	return;
  }
  
  GrpGrid.clearData();
  GrpGrid.addOne();   
  GrpGrid.setRowColData(0,1,saleInfo[0]);
  GrpGrid.setRowColData(0,2,saleInfo[1]);
  GrpGrid.setRowColData(0,3,saleInfo[2]);
  GrpGrid.setRowColData(0,4,saleInfo[3]);
  GrpGrid.setRowColData(0,5,saleInfo[4]);
  
}

function getQueryResult()
{
	//设置需要返回的数组
	var tRow=GrpGrid.getSelNo();
	var tGrpAgentCode = GrpGrid.getRowColData(tRow-1,1);//对方业务员代码
    var tGrpAgentCom = GrpGrid.getRowColData(tRow-1,2);//对方业务员姓名
    var tGrpAgentComName = GrpGrid.getRowColData(tRow-1,3);//对方机构代码
    var tGrpAgentName = GrpGrid.getRowColData(tRow-1,4);//对方机构名称
    var tGrpAgentIDNo = GrpGrid.getRowColData(tRow-1,5);//对方业务员证件号码
	
	var saleInfo = "";
	saleInfo = saleInfo + (tGrpAgentComName+"#");
	saleInfo = saleInfo + (tGrpAgentName+"#");
	saleInfo = saleInfo + (tGrpAgentCode+"#");
	saleInfo = saleInfo + (tGrpAgentCom+"#");
	saleInfo = saleInfo + tGrpAgentIDNo;
	return saleInfo;
}
