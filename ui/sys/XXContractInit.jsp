<%
//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-12
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput m_gInput=(GlobalInput)session.getValue("GI");
	String strManageCom="";
	String strCurrentDate="";
	if(m_gInput==null)
	{
		strManageCom="Unknown";
		strCurrentDate="";
	}
	else
	{
		strManageCom=m_gInput.ManageCom;
		strCurrentDate=PubFun.getCurrentDate();
	}
%>
<script language="javascript" type="text/javascript">
	function initForm()
	{
		try
		{
			initTextbox();
			//initDataGrid();
			//initDataTable();
		}
		catch(ex)
		{
			document.write("文本框初始化失败!");
		}
	}
	
	//初始化文本框
	function initTextbox()
	{	
		//险种代码
		document.all["RiskCode"].value="";
		//超始日期
		document.all["ValidateDate"].value="";
		//终止日期
		document.all["InValidateDate"].value="<%=strCurrentDate%>";
		//保单号
		document.all["ContNo"].value="";
	}
	
	
	  //初始化DataGrid表格组件
	  function initDataGrid()
	  {                               
	      var iArray = new Array(); 
	      try
	      {
	        iArray[0]=new Array();
	        iArray[0][0]="序号"
	        iArray[0][1]="30px";         //宽度
	        iArray[0][2]=100;         //最大长度
	        iArray[0][3]=0;  
	        
	        iArray[1]=new Array();
	        iArray[1][0]="管理机构"
	        iArray[1][1]="60px";         //宽度
	        iArray[1][2]=100;         //最大长度
	        iArray[1][3]=0;  
	        
	        iArray[2]=new Array();
	        iArray[2][0]="产品编码";         //列名
	        iArray[2][1]="60px";         //宽度
	        iArray[2][2]=100;         //最大长度
	        iArray[2][3]=0;         //是否允许录入,0--不能，1--允许
	   
	        iArray[3]=new Array();
	        iArray[3][0]="保单号";         //列名
	        iArray[3][1]="120px";         //宽度
	        iArray[3][2]=100;         //最大长度
	        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[4]=new Array();
	        iArray[4][0]="生效日期";         //列名
	        iArray[4][1]="90px";         //宽度
	        iArray[4][2]=100;         //最大长度
	        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
	        

	        iArray[5]=new Array();
	        iArray[5][0]="失效日期";         //列名
	        iArray[5][1]="90px";         //宽度
	        iArray[5][2]=100;         //最大长度
	        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[6]=new Array();
	        iArray[6][0]="增减人日期";         //列名
	        iArray[6][1]="90px";         //宽度
	        iArray[6][2]=100;         //最大长度
	        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

	        iArray[7]=new Array();
	        iArray[7][0]="签单日期";         //列名
	        iArray[7][1]="90px";         //宽度
	        iArray[7][2]=100;         //最大长度
	        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[8]=new Array();
	        iArray[8][0]="投保人";         //列名
	        iArray[8][1]="80px";         //宽度
	        iArray[8][2]=100;         //最大长度
	        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

	        iArray[9]=new Array();
	        iArray[9][0]="投保人客户号";         //列名
	        iArray[9][1]="80px";         //宽度
	        iArray[9][2]=100;         //最大长度
	        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[10]=new Array();
	        iArray[10][0]="被保人";         //列名
	        iArray[10][1]="80px";         //宽度
	        iArray[10][2]=100;         //最大长度
	        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[11]=new Array();
	        iArray[11][0]="限额/保额";         //列名
	        iArray[11][1]="60px";         //宽度
	        iArray[11][2]=100;         //最大长度
	        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[12]=new Array();
	        iArray[12][0]="标准保费";         //列名
	        iArray[12][1]="60px";         //宽度
	        iArray[12][2]=100;         //最大长度
	        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[13]=new Array();
	        iArray[13][0]="实收保费";         //列名
	        iArray[13][1]="60px";         //宽度
	        iArray[13][2]=100;         //最大长度
	        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[14]=new Array();
	        iArray[14][0]="折扣率";         //列名
	        iArray[14][1]="40px";         //宽度
	        iArray[14][2]=100;         //最大长度
	        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[15]=new Array();
	        iArray[15][0]="年度化保费";         //列名
	        iArray[15][1]="80px";         //宽度
	        iArray[15][2]=100;         //最大长度
	        iArray[15][3]=0;         //是否允许录入，0--不能，1--允许
	     
	        DataGrid = new MulLineEnter( "fm" , "DataGrid" ); 

	        //这些属性必须在loadMulLine前
	        DataGrid.mulLineCount = 0;//行属性，设置需要显示的行数   
	        DataGrid.displayTitle = 1;
	        DataGrid.hiddenPlus = 1;
	        DataGrid.hiddenSubtraction = 1;
	        DataGrid.locked=1;
	        //DataGrid.canSel=1;
	        DataGrid.canChk=0;
	        DataGrid.loadMulLine(iArray);  
	      }
	      catch(ex)
	      {
	        window.alert("初始化DataGrid时出错："+ ex);
	      }
	    }
	
	  
//	初始化DataGrid表格组件
	  function initDataTable()
	  {                               
	      var iArray = new Array(); 
	      
	      try
	      {

	        iArray[0]=new Array();
	        iArray[0][0]="序号"
	        iArray[0][1]="30px";         //宽度
	        iArray[0][2]=100;         //最大长度
	        iArray[0][3]=0;  
	        
	        iArray[1]=new Array();
	        iArray[1][0]="管理机构"
	        iArray[1][1]="60px";         //宽度
	        iArray[1][2]=100;         //最大长度
	        iArray[1][3]=0;  
	        
	        iArray[2]=new Array();
	        iArray[2][0]="产品编码";         //列名
	        iArray[2][1]="50px";         //宽度
	        iArray[2][2]=100;         //最大长度
	        iArray[2][3]=0;         //是否允许录入,0--不能，1--允许
	   
	        iArray[3]=new Array();
	        iArray[3][0]="保单号";         //列名
	        iArray[3][1]="110px";         //宽度
	        iArray[3][2]=100;         //最大长度
	        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[4]=new Array();
	        iArray[4][0]="生效日期";         //列名
	        iArray[4][1]="80px";         //宽度
	        iArray[4][2]=100;         //最大长度
	        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
	        

	        iArray[5]=new Array();
	        iArray[5][0]="失效日期";         //列名
	        iArray[5][1]="80px";         //宽度
	        iArray[5][2]=100;         //最大长度
	        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[6]=new Array();
	        iArray[6][0]="签单日期";         //列名
	        iArray[6][1]="80px";         //宽度
	        iArray[6][2]=100;         //最大长度
	        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

	        iArray[7]=new Array();
	        iArray[7][0]="投保人";         //列名
	        iArray[7][1]="120px";         //宽度
	        iArray[7][2]=100;         //最大长度
	        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[8]=new Array();
	        iArray[8][0]="投保人客户号";         //列名
	        iArray[8][1]="80px";         //宽度
	        iArray[8][2]=100;         //最大长度
	        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

	        iArray[9]=new Array();
	        iArray[9][0]="被保人";         //列名
	        iArray[9][1]="50px";         //宽度
	        iArray[9][2]=100;         //最大长度
	        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[10]=new Array();
	        iArray[10][0]="保额";         //列名
	        iArray[10][1]="80px";         //宽度
	        iArray[10][2]=100;         //最大长度
	        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[11]=new Array();
	        iArray[11][0]="索赔金额";         //列名
	        iArray[11][1]="80px";         //宽度
	        iArray[11][2]=100;         //最大长度
	        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[12]=new Array();
	        iArray[12][0]="赔付金额";         //列名
	        iArray[12][1]="80px";         //宽度
	        iArray[12][2]=100;         //最大长度
	        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[13]=new Array();
	        iArray[13][0]="诊断";         //列名
	        iArray[13][1]="200px";         //宽度
	        iArray[13][2]=100;         //最大长度
	        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        
	        iArray[14]=new Array();
	        iArray[14][0]="出险原因";         //列名
	        iArray[14][1]="80px";         //宽度
	        iArray[14][2]=100;         //最大长度
	        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[15]=new Array();
	        iArray[15][0]="出险日期";         //列名
	        iArray[15][1]="80px";         //宽度
	        iArray[15][2]=100;         //最大长度
	        iArray[15][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        
	        iArray[16]=new Array();
	        iArray[16][0]="出院日期";         //列名
	        iArray[16][1]="80px";         //宽度
	        iArray[16][2]=100;         //最大长度
	        iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        
	        iArray[17]=new Array();
	        iArray[17][0]="索赔日期";         //列名
	        iArray[17][1]="80px";         //宽度
	        iArray[17][2]=100;         //最大长度
	        iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[18]=new Array();
	        iArray[18][0]="结案日期";         //列名
	        iArray[18][1]="80px";         //宽度
	        iArray[18][2]=100;         //最大长度
	        iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[19]=new Array();
	        iArray[19][0]="理赔结论";         //列名
	        iArray[19][1]="80px";         //宽度
	        iArray[19][2]=100;         //最大长度
	        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[20]=new Array();
	        iArray[20][0]="赔款";         //列名
	        iArray[20][1]="50px";         //宽度
	        iArray[20][2]=100;         //最大长度
	        iArray[20][3]=0;         //是否允许录入，0--不能，1--允许
	        
	    
	        DataTable = new MulLineEnter( "fm" , "DataTable" ); 

	        //这些属性必须在loadMulLine前
	        DataTable.mulLineCount = 0;//行属性，设置需要显示的行数   
	        DataTable.displayTitle = 1;
	        DataTable.hiddenPlus = 1;
	        DataTable.hiddenSubtraction = 1;
	        DataTable.locked=1;
	        //DataTable.canSel=1;
	        DataTable.canChk=0;
	        DataTable.loadMulLine(iArray);  
	      }
	      catch(ex)
	      {
	        window.alert("初始化DataGrid时出错："+ ex);
	      }
	    }
</script>