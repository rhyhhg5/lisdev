//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var arrResult;
var mDebug = "0";
var mOperate = "";
var mAction = "";
//top.fraPic.window.service.focus();
var mSwitch = parent.VD.gVSwitch;
var mShowCustomerDetail = "GROUPPOL";
var turnPage = new turnPageClass();
var cflag = "5";
var mWFlag = 0 ;
var mGrpContNo;
var LoadFlag;
var Resource;

parent.fraMain.rows = "0,0,0,0,*";
/*********************************************************************
 *  保存集体投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm() {    

	BookingPayIntyGrid.delBlankLine();
	DiseaseGrid.delBlankLine();
	HistoryImpartGrid.delBlankLine();
	if(!chkDayAndMonth()) return false;
	
	// by gzh 20101118
	//选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
	
	if( verifyInput2() == false )
    return false;
    
    // 生效日期校验
//    if(!chkValidate())
//    {
//        if(!confirm("生效日期早于录入日期，是否继续录入？"))
//            return false;
//    }

    
  if(fm.all('PayMode').value=="4" && fm.all('BankCode').value=="") {
    alert("请输入银行转帐的银行和该银行的帐号");
    fm.all('BankCode').focus();
    return false;
  }

  if(fm.all('MarketType').value == ""){
  	alert("市场类型不能为空,请录入市场类型");
  	return false;
  }

    if(fm.all('SaleChnl').value=="03" && fm.all('AgentCom').value=="") {
    alert("销售渠道为中介，请录入中介公司代码！");
    fm.all('AgentCom').focus();
    return false;
  }
  //开办市县数和开办市县内容校验
//  if(!checkCity(1))
//  {
//    return false;
//  }

    // 集团交叉业务要素校验
    if(!checkCrsBussParams())
    {
        return false;
    }
    // --------------------

//2006-03-20 闫少杰 约定缴费方式时进行的校验 --- START
    //  1   缴费期次的缴费时间必须是递增的；
    //  2   约定缴费金额的合计应该等于总保费；
    if(fm.all('GrpContPayIntv').value=="-1")
    {
        if(BookingPayIntyGrid.mulLineCount==0)
        {
            alert("缴费频次为约定缴费，必须录入约定的缴费时间和缴费内容！");
            return false;
        }
        else
        {
            var sumSpecFee=0;       //约定缴费金额的合计
            var earlyDate = "";     //前一个缴费期次的日期
            var laterDate = "";     //后一个缴费期次的日期
            for(var indexOfSpecFee=0;indexOfSpecFee<BookingPayIntyGrid.mulLineCount;indexOfSpecFee++)
            {
                //检验录入的缴费日期是否为约定日期格式
                if( isDate( BookingPayIntyGrid.getRowColData(indexOfSpecFee,1) ) )
                {
                    if(indexOfSpecFee==0)
                    {
                        earlyDate = BookingPayIntyGrid.getRowColData(indexOfSpecFee,1);
                    }
                    else
                    {
                        laterDate = BookingPayIntyGrid.getRowColData(indexOfSpecFee,1);
                        if(compareDate(laterDate,earlyDate)!=1) //laterDate>earlyDate时，函数返回值为1
                        {
                            alert("缴费期次的缴费时间必须是按照期次进行递增的，即第二次的缴费日期在第一次的缴费日期之后，以此类推！");
                            return false;
                        }
                        earlyDate = laterDate;  //将当前行日期置为earlyDate，为下次比较做准备
                    }
                }
                else
                {
                    alert("录入的约定缴费时间存在错误，请确认日期格式为'yyyy-mm-dd'并且日期填写正确！");
                    return false;
                }
                //校验录入的缴费金额是否为合法数字
                if( isNumeric( BookingPayIntyGrid.getRowColData(indexOfSpecFee,2) ) )
                {
                    sumSpecFee = sumSpecFee + Number(BookingPayIntyGrid.getRowColData(indexOfSpecFee,2));
                }
                else
                {
                    alert("录入的约定缴费金额有误，请确认您录入的缴费金额！");
                    return false;
                }
            }
            //alert("约定缴费金额的合计="+sumSpecFee);
            //alert("保费合计="+fm.GrpContSumPrem.value);
            if(sumSpecFee!=fm.GrpContSumPrem.value)
            {
                alert("约定缴费金额的合计和保费合计不相等，请确认后进行修改！");
                return false;
            }
        }
    }
//2006-03-20 闫少杰 约定缴费方式时进行的校验 --- END

  if(dateDiff(fm.all('PolApplyDate').value, fm.all('CValiDate').value,"D") > 365) {
    alert("保单生效日期最多为投保日期前一年");
    fm.all('PolApplyDate').focus();
    return false;
  }

    if(dateDiff( fm.all('CValiDate').value,fm.all('CInValiDate').value,"D")< 0) {
    alert("保单生效日期不应在保单失效日期之前!");
    fm.all('CInValiDate').focus();
    return false;
  }
  if(dateDiff(fm.all('CValiDate').value, fm.all('HandlerDate').value,"D") > 365) {
    alert("保单生效日期最多为投保填写日期前一年");
    fm.all('HandlerDate').focus();
    return false;
  }
	if(dateDiff(fm.all('CValiDate').value, fm.all('HandlerDate').value,"D")>1)
	{
		if(!confirm("填单日期大于保单生效日期！是否继续录入？"))
		{
			return false;
		}
	}
	if(!checkInsurePeriod())
	{
	  return false;
	}
	if ( fm.all('ContPrintType').value==null || fm.all('ContPrintType').value=="") {
   alert("保单打印类型不能为空!");
   fm.all('ContPrintType').focus();
   return false;
	}
  //校验被保人人数
  /*
  	if(fm.all('Peoples3').value="0"){
  		alert("被保人数不能为0");
  		fm.all('Peoples3').focus();
  		return false;

  		}
  */

  //员工总人数=在职人数+退休人数+其它人员人数
  if ( fm.all('Peoples').value==null || fm.all('Peoples').value=="") {
    fm.all('Peoples').value = 0
	}
	if ( fm.all('AppntOnWorkPeoples').value==null || fm.all('AppntOnWorkPeoples').value=="") {
		fm.all('AppntOnWorkPeoples').value = 0
	}
	if ( fm.all('AppntOffWorkPeoples').value==null || fm.all('AppntOffWorkPeoples').value=="") {
		fm.all('AppntOffWorkPeoples').value = 0
	}
	if ( fm.all('AppntOtherPeoples').value==null || fm.all('AppntOtherPeoples').value=="") {
		fm.all('AppntOtherPeoples').value = 0
	}
	  if (CheckDateDollar()==false)
  {
  	return false;
  }
  var intPeoples=parseInt(fm.all('Peoples').value);
  var intAppntOnWorkPeoples=parseInt(fm.all('AppntOnWorkPeoples').value);
  var intAppntOffWorkPeoples=parseInt(fm.all('AppntOffWorkPeoples').value);
  var intAppntOtherPeoples=parseInt(fm.all('AppntOtherPeoples').value );

  if (intPeoples!=intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples) {
    alert("员工总人数应该为"+(intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples));
    fm.all('Peoples').focus();
    return false;
  }

  if (GrpNameDif()==false) {
    alert("你修改了投保团体名称,请删除团体客户号,再保存");
    return false;
  }
   //by gzh date 20101210
  if(checkPrem()==false){
  	return false;
  }
  if(!checkOldPeople()){
  	return false;
  }
  if(!checkYearType()){
  	return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  LCImpartInput();  //团体告知
  if (InputMuline1()==false)
  {
     return false;
  }
  ImpartGrid.delBlankLine();
  BaseImpartDetailGrid.delBlankLine();
  HealthImpartDetailGrid.delBlankLine();
  NewHistoryImpartGrid.delBlankLine();
  if(CheckImpart()==false)
  {
  	return false;
  }
 
  if( mAction == "" ) {
    //showSubmitFrame(mDebug);
    mAction = "INSERT";
    fm.all( 'fmAction' ).value = mAction;
    fm.all( 'LoadFlag' ).value = LoadFlag;
	
    if (fm.all('ProposalGrpContNo').value != "") {
      alert("保存结果只能进行修改操作！");
      initArray();
      mAction = "";
    } else {
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      tAction = fm.action;
      fm.action="./ContPolSave.jsp"
      ChangeDecodeStr();
      fm.submit(); //提交
    }
  }

}

/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content ,RiskCode ,GrpPolNo) {
	UnChangeDecodeStr();
  showInfo.close();
  window.focus();
  LCImpartStr1 = "";
	LCImpartStr2 = "";
	LCImpartStr3 = "";
	ImpartCheck1 = new Array();
	ImpartCheck2 = new Array();
	ImpartCheck3 = new Array();
	//by gzh
	LCImpartStr4 = "";
	ImpartCheck4 = new Array();
	LCImpartStr5 = "";
	ImpartCheck5 = new Array();
	LCImpartStr6 = "";
	ImpartCheck6 = new Array();
  if( FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
    content = "处理成功！";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
     fillriskgrid();
    showDiv(operateButton, "true");
    showDiv(inputButton, "false");
  }

  mAction = "";
  if(this.ScanFlag == "1") {
  	//注掉下面一行，因为在点击修改后会把印刷号置空，
    //fm.PrtNo.value=prtNo;
    initRiskGrid();
    fillriskgrid();
  }
  //alert(mWFlag+"   "+FlagStr);

  if(mWFlag == 1 && FlagStr != "Fail") {
  	top.window.close();
    window.location.href("./ContPolInput.jsp");
  }
	/*
	if(RiskCode != null && RiskCode != "")
	{
		var strSql = "select 1 from lmrisktoacc where RiskCode='"+RiskCode+"'";
		var arr = easyExecSql(strSql);
		if(arr)
		{
			if(confirm("录入险种为账户险需要定义账户信息"))
			{
				grpPubAccInput(RiskCode,GrpPolNo);
			}
			else
				{
					alert("账户型险种必须定义账户信息");
					grpPubAccInput(RiskCode,GrpPolNo);
				}
		}
	}
	*/
  //location.reload();
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm() {
  try {
    initForm();
    fm.all('PrtNo').value = prtNo;
  } catch( re ) {
    alert("在GroupPolInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm() {
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
  if( cDebug == "1" )
    parent.fraMain.rows = "0,0,50,82,*";
  else
    parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick() {

  //下面增加相应的代码
  showDiv( operateButton, "false" );
  showDiv( inputButton, "true" );

  fm.all('RiskCode').value = "";

  //保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
  if (BQFlag=="2") {
    var strSql = "select grppolno, grpno from lcgrppol where prtno='" + prtNo + "' and riskcode in (select riskcode from lmriskapp where subriskflag='M')";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);

    mOperate = 1;
    afterQuery(arrResult);

    //strSql = "select GrpNo,GrpName,GrpAddress,Satrap from LDGrp where GrpNo='" + arrResult[0][1] + "'";
    //arrResult = easyExecSql(strSql);
    //mOperate = 2;
    //afterQuery(arrResult);

    fm.all('RiskCode').value = BQRiskCode;
    fm.all('RiskCode').className = "readonly";
    fm.all('RiskCode').readOnly = true;
    fm.all('RiskCode').ondblclick = "";
  }

  fm.all('ContNo').value = "";
  fm.all('ProposalGrpContNo').value = "";
}

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick() {
  if(this.ScanFlag == "1") {
    alert( "有扫描件录入不允许查询!" );
    return false;
  }
  if( mOperate == 0 ) {
    mOperate = 1;
    //cContNo = fm.all( 'ContNo' ).value;
    showInfo = window.open("./GroupPolQueryMain.jsp");
  }
}


/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick() {
    //销售渠道和业务员的校验
	if(!checkSaleChnl())
	{
		return false;
	}
    
    // 生效日期校验
//    if(!chkValidate())
//   {
//        if(!confirm("生效日期早于录入日期，是否继续录入？"))
//            return false;
//    }
    // --------------------
    
    
    // by gzh 20101118
	//选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
    // 集团交叉业务要素校验
    //if(!checkCrsBussParams())
    //{
        //return false;
    //}
    // --------------------
    
	//ChangeDecodeStr();
	BookingPayIntyGrid.delBlankLine();
	DiseaseGrid.delBlankLine();
	HistoryImpartGrid.delBlankLine();
	if( verifyInput2() == false )
    return false;
	if(!chkDayAndMonth()) return false;
  if(fm.all('PayMode').value=="4" && fm.all('BankCode').value=="") {
    alert("请输入银行转帐的银行和该银行的帐号");
    fm.all('BankCode').focus();
    return false;
  }
  
  if(fm.all('MarketType').value == ""){
  	alert("市场类型不能为空,请录入市场类型");
  	return false;
  }
  
  //alert(fm.all('SaleChnl').value);
 // alert(fm.all('AgentCom').value);
   if(fm.all('SaleChnl').value=="03" && fm.all('AgentCom').value=="") {
    alert("销售渠道为中介，请录入中介公司代码！");
    //fm.all('AgentCom').focus();
    return false;
  }
  //开办市县数和开办市县内容校验
//  if(!checkCity(1))
//  {
//    return false;
//  }
//2006-03-20 闫少杰 约定缴费方式时进行的校验 --- START
    //  1   缴费期次的缴费时间必须是递增的；
    //  2   约定缴费金额的合计应该等于总保费；
    if(fm.all('GrpContPayIntv').value=="-1")
    {
        if(BookingPayIntyGrid.mulLineCount==0)
        {
            alert("缴费频次为约定缴费，必须录入约定的缴费时间和缴费内容！");
            return false;
        }
        else
        {
            var sumSpecFee=0;       //约定缴费金额的合计
            var earlyDate = "";     //前一个缴费期次的日期
            var laterDate = "";     //后一个缴费期次的日期
            for(var indexOfSpecFee=0;indexOfSpecFee<BookingPayIntyGrid.mulLineCount;indexOfSpecFee++)
            {
                //检验录入的缴费日期是否为约定日期格式
                if( isDate( BookingPayIntyGrid.getRowColData(indexOfSpecFee,1) ) )
                {
                    if(indexOfSpecFee==0)
                    {
                        earlyDate = BookingPayIntyGrid.getRowColData(indexOfSpecFee,1);
                    }
                    else
                    {
                        laterDate = BookingPayIntyGrid.getRowColData(indexOfSpecFee,1);
                        if(compareDate(laterDate,earlyDate)!=1) //laterDate>earlyDate时，函数返回值为1
                        {
                            alert("缴费期次的缴费时间必须是按照期次进行递增的，即第二次的缴费日期在第一次的缴费日期之后，以此类推！");
                            return false;
                        }
                        earlyDate = laterDate;  //将当前行日期置为earlyDate，为下次比较做准备
                    }
                }
                else
                {
                    alert("录入的约定缴费时间存在格式错误，请确认日期格式为'yyyy-mm-dd'！");
                    return false;
                }
                //校验录入的缴费金额是否为合法数字
                if( isNumeric( BookingPayIntyGrid.getRowColData(indexOfSpecFee,2) ) )
                {
                    sumSpecFee = sumSpecFee + Number(BookingPayIntyGrid.getRowColData(indexOfSpecFee,2));
                }
                else
                {
                    alert("录入的约定缴费金额有误，请确认您录入的缴费金额！");
                    return false;
                }
            }
            //alert("约定缴费金额的合计="+sumSpecFee);
            //alert("保费合计="+fm.GrpContSumPrem.value);
            if(sumSpecFee!=fm.GrpContSumPrem.value)
            {
                alert("约定缴费金额的合计和保费合计不相等，请确认后进行修改！");
                return false;
            }
        }
    }
//2006-03-20 闫少杰 约定缴费方式时进行的校验 --- END

    if(dateDiff( fm.all('CValiDate').value,fm.all('CInValiDate').value,"D")< 0) {
    alert("保单生效日期不应在保单失效日期之前!");
    fm.all('CInValiDate').focus();
    return false;
  }
  if(dateDiff(fm.all('PolApplyDate').value, fm.all('CValiDate').value,"D") > 365) {
    alert("保单生效日期最多为投保填写日期前一年");
    fm.all('PolApplyDate').focus();
    return false;
  }
  if(dateDiff(fm.all('CValiDate').value, fm.all('HandlerDate').value,"D") > 365) {
    alert("保单生效日期最多为投保填写日期前一年");
    fm.all('HandlerDate').focus();
    return false;
  }
  //alert(dateDiff(fm.all('CValiDate').value, fm.all('HandlerDate').value,"D"));
	if(dateDiff(fm.all('CValiDate').value, fm.all('HandlerDate').value,"D")>=1)
	{
		if(!confirm("填单日期大于保单生效日期！是否继续录入？"))
		{
			return false;
		}
	}
	if(!checkInsurePeriod())
	{
	  return false;
	}
		if ( fm.all('ContPrintType').value==null || fm.all('ContPrintType').value=="") {
   alert("保单打印类型不能为空!");
   fm.all('ContPrintType').focus();
   return false;
	}
  //员工总人数=在职人数+退休人数+其它人员人数
  if ( fm.all('Peoples').value==null || fm.all('Peoples').value=="") {
    fm.all('Peoples').value = 0
	}
	if ( fm.all('AppntOnWorkPeoples').value==null || fm.all('AppntOnWorkPeoples').value=="") {
		fm.all('AppntOnWorkPeoples').value = 0
	}
	if ( fm.all('AppntOffWorkPeoples').value==null || fm.all('AppntOffWorkPeoples').value=="") {
		fm.all('AppntOffWorkPeoples').value = 0
	}
	if ( fm.all('AppntOtherPeoples').value==null || fm.all('AppntOtherPeoples').value=="") {
		fm.all('AppntOtherPeoples').value = 0
	}

	var intPeoples=parseInt(fm.all('Peoples').value);
  var intAppntOnWorkPeoples=parseInt(fm.all('AppntOnWorkPeoples').value);
  var intAppntOffWorkPeoples=parseInt(fm.all('AppntOffWorkPeoples').value);
  var intAppntOtherPeoples=parseInt(fm.all('AppntOtherPeoples').value );

  if (intPeoples!=intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples) {
    alert("员工总人数应该为"+(intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples));
    fm.all('Peoples').focus();
    return false;
  }

  if (GrpNameDif()==false) {
    alert("你修改了投保团体名称,请删除团体客户号,再保存");
    return false;
  }

  var tProposalGrpContNo = "";
  tProposalGrpContNo = fm.all( 'ProposalGrpContNo' ).value;

//  if (fm.GrpNo.value==''&&fm.GrpAddressNo.value!='') {
//    alert("客户号为空，不能有地址编码");
//    return false;
//  }
  if(fm.GrpNo.value!='')
  {
    arrResult = easyExecSql("select * from LCGrpCont where ProposalGrpContNo = '" + tProposalGrpContNo + "'", 1, 0);
    if(arrResult==null)
    {
      alert("没有找到团体合同信息,请检查");
      return false;
    }
    if(fm.GrpNo.value!=arrResult[0][12])
    {
       alert("团体客户号与数据库中团体合同下的客户号不符,请检查");
       return false;
    }
  }
  LCImpartInput();  //团体告知
if (InputMuline1()==false)
  {
     return false;
  }
  ImpartGrid.delBlankLine();
  BaseImpartDetailGrid.delBlankLine();
  HealthImpartDetailGrid.delBlankLine();
  NewHistoryImpartGrid.delBlankLine();
  if (CheckImpart()==false)
  {
  	return false;
  }
  //gzh date 20101210
  if(checkPrem()==false){
  	return false;
  }
  if(!checkOldPeople()){
  	return false;
  }
  if(!checkYearType()){
  	return false;
  }
  if(!checkGetYearAfter()){
  	return false;
  }
  if (CheckDateDollar()==false)
  {
  	return false;
  }
  if( tProposalGrpContNo == null || tProposalGrpContNo == "" )
    if(this.ScanFlag == "1") {
      alert( "还未录入数据,请先增加合同信息,再进行修改!" );
    } else {
      alert( "请先做投保单保存操作，再进行修改!" );
    }
  else {
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    if( mAction == "" ) {
      //showSubmitFrame(mDebug);
      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      mAction = "UPDATE";
      fm.all( 'fmAction' ).value = mAction;
      fm.action="./ContPolSave.jsp"
      ChangeDecodeStr();
      fm.submit(); //提交
    }
  }
}

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick() {
  var tProposalGrpContNo = "";
  tProposalGrpContNo = fm.all( 'ProposalGrpContNo' ).value;
  if( tProposalGrpContNo == null || tProposalGrpContNo == "" )
    alert( "请先做投保单保存操作，然后再进行删除操作！" );
  else {
    if (confirm("您确定要删除该团单吗？")) {
      var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

      if( mAction == "" ) {
        //showSubmitFrame(mDebug);
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        mAction = "DELETE";
        fm.all( 'fmAction' ).value = mAction;
        fm.action="./ContPolSave.jsp"
                  fm.submit(); //提交
        fm.all('Remark').value="";
        ImpartClear();
      }
    }
  }
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow) {
  if( cShow == "true" )
    cDiv.style.display = "";
  else
    cDiv.style.display = "none";
}

/*********************************************************************
 *  当点击“进入个人信息”按钮时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoPol() {
  //下面增加相应的代码
  tProposalGrpContNo = fm.ProposalGrpContNo.value;
  if( tProposalGrpContNo == "" ) {
    alert("您必须先录入集体信息才能进入个人信息部分。");
    return false
         }

         //把集体信息放入内存
         mSwitch = parent.VD.gVSwitch;  //桢容错
  putGrpPol();

  try {
    goToPic(2)
  } catch(e) {}

  try {
    parent.fraInterface.window.location = "./ProposalGrpInput.jsp?LoadFlag=" + LoadFlag + "&type=" + type;
  } catch (e) {
    parent.fraInterface.window.location = "./ProposalGrpInput.jsp?LoadFlag=2&type=" + type;
  }
}

/*********************************************************************
 *  把集体信息放入内存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function putGrpPol() {
  delGrpPolVar();
  addIntoGrpPol();
}

/*********************************************************************
 *  把集体信息放入加到变量中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addIntoGrpPol() {
  try {
    mSwitch.addVar( "intoPolFlag", "", "GROUPPOL" );
  } catch(ex) { }
  ;
  // body信息
  try {
    mSwitch.addVar( "BODY", "", window.document.body.innerHTML );
  } catch(ex) { }
  ;
  // 集体信息
  //由"./AutoCreatLDGrpInit.jsp"自动生成
  try {
    mSwitch.addVar('GrpNo', '', fm.all('GrpNo').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PrtNo', '', fm.all('PrtNo').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Password', '', fm.all('Password').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpName', '', fm.all('GrpName').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpAddressCode', '', fm.all('GrpAddressCode').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpAddress', '', fm.all('GrpAddress').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpZipCode', '', fm.all('GrpZipCode').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BusinessType', '', fm.all('BusinessType').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpNature', '', fm.all('GrpNature').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Peoples', '', fm.all('Peoples').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('RgtMoney', '', fm.all('RgtMoney').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Asset', '', fm.all('Asset').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('NetProfitRate', '', fm.all('NetProfitRate').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('MainBussiness', '', fm.all('MainBussiness').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Corporation', '', fm.all('Corporation').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ComAera', '', fm.all('ComAera').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LinkMan1', '', fm.all('LinkMan1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Department1', '', fm.all('Department1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('HeadShip1', '', fm.all('HeadShip1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone1', '', fm.all('Phone1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('E_Mail1', '', fm.all('E_Mail1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Fax1', '', fm.all('Fax1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LinkMan2', '', fm.all('LinkMan2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Department2', '', fm.all('Department2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('HeadShip2', '', fm.all('HeadShip2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone2', '', fm.all('Phone2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('E_Mail2', '', fm.all('E_Mail2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Fax2', '', fm.all('Fax2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Fax', '', fm.all('Fax').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone', '', fm.all('Phone').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetFlag', '', fm.all('GetFlag').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Satrap', '', fm.all('Satrap').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('EMail', '', fm.all('EMail').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('FoundDate', '', fm.all('FoundDate').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntOnWorkPeoples', '', fm.all('AppntOnWorkPeoples').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntOffWorkPeoples', '', fm.all('AppntOffWorkPeoples').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntOtherPeoples', '', fm.all('AppntOtherPeoples').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BankAccNo', '', fm.all('BankAccNo').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BankCode', '', fm.all('BankCode').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpGroupNo', '', fm.all('GrpGroupNo').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('State', '', fm.all('State').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BlacklistFlag', '', fm.all('BlacklistFlag').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Currency', '', fm.all('Currency').value);
  } catch(ex) { }
  ;

  try {
    mSwitch.addVar( "ContNo", "", fm.all( 'ContNo' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "ProposalGrpContNo", "", fm.all( 'ProposalGrpContNo' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "ManageCom", "", fm.all( 'ManageCom' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "SaleChnl", "", fm.all( 'SaleChnl' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "AgentCom", "", fm.all( 'AgentCom' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "AgentCode", "", fm.all( 'AgentCode' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "AgentGroup", "", fm.all( 'AgentGroup' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "AgentCode1", "", fm.all( 'AgentCode1' ).value );
  } catch(ex) { }
  ;

  try {
    mSwitch.addVar( "RiskCode", "", fm.all( 'RiskCode' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "RiskVersion", "", fm.all( 'RiskVersion' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "CValiDate", "", fm.all( 'CValiDate' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar( "PolApplyDate", "", fm.all( 'PolApplyDate' ).value );
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag1', '', fm.all('StandbyFlag1').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag2', '', fm.all('StandbyFlag2').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SaleChnl', '', fm.all('SaleChnl').value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SaleChnlDetail', '', fm.all('SaleChnlDetail').value);
  } catch(ex) { }
}

/*********************************************************************
 *  把集体信息从变量中删除
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delGrpPolVar() {
  try {
    mSwitch.deleteVar( "intoPolFlag" );
  } catch(ex) { }
  ;
  // body信息
  try {
    mSwitch.deleteVar( "BODY" );
  } catch(ex) { }
  ;
  // 集体信息
  //由"./AutoCreatLDGrpInit.jsp"自动生成
  try {
    mSwitch.deleteVar('GrpNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PrtNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Password');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpName');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpAddressCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpAddress');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpZipCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BusinessType');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpNature');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Peoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('RgtMoney');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Asset');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('NetProfitRate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('MainBussiness');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Corporation');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ComAera');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LinkMan1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Department1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('HeadShip1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('E_Mail1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Fax1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LinkMan2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Department2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('HeadShip2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('E_Mail2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Fax2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Fax');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Satrap');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('EMail');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('FoundDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOnWorkPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOffWorkPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOtherPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BankAccNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BankCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpGroupNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('State');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BlacklistFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Currency');
  } catch(ex) { }
  ;

  mSwitch.deleteVar( "ContNo" );
  mSwitch.deleteVar( "ProposalGrpContNo" );
  mSwitch.deleteVar( "ManageCom" );
  mSwitch.deleteVar( "SaleChnl" );
  mSwitch.deleteVar( "AgentCom" );
  mSwitch.deleteVar( "AgentCode" );
  mSwitch.deleteVar( "AgentCode1" );

  mSwitch.deleteVar( "RiskCode" );
  mSwitch.deleteVar( "RiskVersion" );
  mSwitch.deleteVar( "CValiDate" );
  mSwitch.deleteVar( "SaleChnl" );
  mSwitch.deleteVar( "SaleChnlDetail" );

}

/*********************************************************************
 *  Click事件，当双击“投保单位客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt1() {
  if( mOperate == 0 ) {
    mOperate = 2;
    showInfo = window.open( "../sys/GroupMain.html" );
  }
}
function showAppnt() {
  if (fm.all("GrpNo").value == "" ) {
    showAppnt1();
  } else {
    arrResult = easyExecSql("select b.CustomerNo,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum from LDGrp b where  b.CustomerNo='" + fm.all("GrpNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到投保单位信息");
    } else {
      displayAddress(arrResult[0]);
      getaddresscodedata();
    }
  }
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {
  if( arrQueryResult != null ) {
    arrResult = arrQueryResult;
    if( mOperate == 1 )	{		// 查询集体投保单
    
      Resource= fm.all( 'Resource' ).value ;
      if(Resource=="1"){
      cContType = "2";
      }else if(Resource=="2"){
      cContType = "1";
      }
      mGrpContNo=polNo;
      fm.all( 'GrpContNo' ).value = polNo;
      LoadFlag=fm.LoadFlag.value;
      var authtable="";
     if(cContType == "2")
  	{
  	  tTableType = "LbGrpCont";
  	  Resource="1";
  	  authtable = "Lbgrpappnt";
  	}
  	else
  	{
  	 tTableType = "LcGrpCont";
  	  Resource="2";
  	  authtable = "Lcgrpappnt";
  	}
     authresult = easyExecSql("select authorization from "+authtable+" where grpcontno='"+mGrpContNo+"'", 1, 0);
     if(authresult == null){
     }
     else{
    	 try {
    		    fm.all('auth').value= authresult[0][0];
    		  } catch(ex) { }
    		  ;
     }
      arrResult = easyExecSql("select * from "+tTableType+" where GrpContNo = '" + mGrpContNo + "'", 1, 0);
      if (arrResult == null) {
        //alert("未查到团单信息");
      }
      else
      { 
	       if(cContType == "2")
	  	{
	  	  displayLBGrpCont(arrResult[0]);
	  	}
	  	else
	  	{
	  	 displayLCGrpCont(arrResult[0]);
	  	}
       
        showOneCodeName("PayMode",fm.PayModeName.name);
        showOneCodeName("GrpContPayIntv",fm.GrpContPayIntvName.name);
        fm.all( 'ContInValidate' ).value='每个被保险人老年护理保险金领取起始日止';
        var tgrpcontno=arrResult[0][1];
         fillriskgrid();
         //是否保全
   var sql = "select 1 from LPEdorApp a, LPGrpEdorItem b "
            + "where a.EdorAcceptNo = b.EdorNo "
            + "   and b.GrpContNo = '" + mGrpContNo + "' "
            + "   and a.EdorState != '0' ";
   var rs = easyExecSql(sql);
   if(rs)
   {
    try {fm.all('EdorFlag').value= "是"; } catch(ex) { }; 
   }
   else
   {
    try {fm.all('EdorFlag').value= "否"; } catch(ex) { }; 
   }
   
  
   //是否理赔
  var sql = "  select 1 "
       + "from LLClaimDetail a, LCPol b "
       + "where a.polNo = b.polNo "
       + "   and B.GrpContNo = '" + mGrpContNo + "' "
       + "   and not exists "
       + "      (select 1 from LJAGetClaim "
       + "      where PolNo = b.PolNo "
       + "         and OtherNo = a.caseNo)";
        rs = easyExecSql(sql);
   if(rs)
   {
    try {fm.all('PayFlag').value= "是"; } catch(ex) { }; 
   }
   else
   {
    try {fm.all('PayFlag').value= "否"; } catch(ex) { }; 
   }
        var tSql1=" select a.CustomerNo,a.AddressNo,a.GrpAddress,a.GrpZipCode,a.LinkMan1,a.Department1,a.HeadShip1,a.Phone1,a.E_Mail1,a.Fax1,a.LinkMan2,a.Department2,a.HeadShip2,a.Phone2,a.E_Mail2,a.Fax2 from LCGrpAddress a where a.AddressNo=(select AddressNo from LCGrpAppnt  where GrpContNo = '" + mGrpContNo + "') and a.CustomerNo=(select CustomerNo from LCGrpAppnt  where GrpContNo = '" + mGrpContNo + "') "
								 +" union "
								 +" select a.CustomerNo,a.AddressNo,a.GrpAddress,a.GrpZipCode,a.LinkMan1,a.Department1,a.HeadShip1,a.Phone1,a.E_Mail1,a.Fax1,a.LinkMan2,a.Department2,a.HeadShip2,a.Phone2,a.E_Mail2,a.Fax2 from LcGrpAddress a where a.AddressNo=(select AddressNo from LbGrpAppnt  where GrpContNo = '" + mGrpContNo + "') and a.CustomerNo=(select CustomerNo from LbGrpAppnt  where GrpContNo = '" + mGrpContNo + "') "
								 +" union "
								 +" select a.CustomerNo,a.AddressNo,a.GrpAddress,a.GrpZipCode,a.LinkMan1,a.Department1,a.HeadShip1,a.Phone1,a.E_Mail1,a.Fax1,a.LinkMan2,a.Department2,a.HeadShip2,a.Phone2,a.E_Mail2,a.Fax2 from LcGrpAddress a where a.AddressNo=(select AddressNo from LobGrpAppnt  where GrpContNo = '" + mGrpContNo + "') and a.CustomerNo=(select CustomerNo from LobGrpAppnt  where GrpContNo = '" + mGrpContNo + "') "
								 ;
        arrResult = easyExecSql(tSql1, 1, 0);
        if (arrResult == null) {
          alert("未查到投保单位地址信息");

        } else {

          displayAddress1(arrResult[0]);
        }

        //var tSql2=" select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum,b.organcomcode from LDGrp b where b.CustomerNo=(select CustomerNo from LCGrpAppnt  where GrpContNo = '" + tgrpcontno + "') "
        //				 +" union "
        //				 +" select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum,b.organcomcode from LDGrp b where b.CustomerNo=(select CustomerNo from LbGrpAppnt  where GrpContNo = '" + tgrpcontno + "') "
        //				 +" union "
        //				 +" select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum,b.organcomcode from LDGrp b where b.CustomerNo=(select CustomerNo from LobGrpAppnt  where GrpContNo = '" + tgrpcontno + "') "
				//							;
        var tSql2 = "" 
            + " select b.GrpName,b.BusinessType,b.GrpNature,(case when lga.Peoples is null then b.Peoples else lga.Peoples end) Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,(case when lga.OnWorkPeoples is null then b.OnWorkPeoples else lga.OnWorkPeoples end) OnWorkPeoples,(case when lga.OffWorkPeoples is null then b.OffWorkPeoples else lga.OffWorkPeoples end) OffWorkPeoples,(case when lga.OtherPeoples is null then b.OtherPeoples else lga.OtherPeoples end) OtherPeoples,b.GrpAppntNum,b.organcomcode from LDGrp b inner join LCGrpAppnt lga on lga.CustomerNo = b.CustomerNo where lga.GrpContNo = '" + mGrpContNo + "' "
            + " union "
            + " select b.GrpName,b.BusinessType,b.GrpNature,(case when lga.Peoples is null then b.Peoples else lga.Peoples end) Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,(case when lga.OnWorkPeoples is null then b.OnWorkPeoples else lga.OnWorkPeoples end) OnWorkPeoples,(case when lga.OffWorkPeoples is null then b.OffWorkPeoples else lga.OffWorkPeoples end) OffWorkPeoples,(case when lga.OtherPeoples is null then b.OtherPeoples else lga.OtherPeoples end) OtherPeoples,b.GrpAppntNum,b.organcomcode from LDGrp b inner join LBGrpAppnt lga on lga.CustomerNo = b.CustomerNo where lga.GrpContNo = '" + mGrpContNo + "' "
            + " union "
            + " select b.GrpName,b.BusinessType,b.GrpNature,(case when lga.Peoples is null then b.Peoples else lga.Peoples end) Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,(case when lga.OnWorkPeoples is null then b.OnWorkPeoples else lga.OnWorkPeoples end) OnWorkPeoples,(case when lga.OffWorkPeoples is null then b.OffWorkPeoples else lga.OffWorkPeoples end) OffWorkPeoples,(case when lga.OtherPeoples is null then b.OtherPeoples else lga.OtherPeoples end) OtherPeoples,b.GrpAppntNum,b.organcomcode from LDGrp b inner join LOBGrpAppnt lga on lga.CustomerNo = b.CustomerNo where lga.GrpContNo = '" + mGrpContNo + "' "
            ;

				arrResult = easyExecSql(tSql2,1,0);
        if (arrResult == null) {
          alert("未查到投保单位信息");

        } else {
          displayAddress2(arrResult[0]);
        }

        var tSql3=" select c.Name,c.PostalAddress,c.ZipCode,c.Phone,c.UnifiedSocialCreditNo from  LCGrpAppnt c where c.GrpContNo = '" + mGrpContNo + "' "
        					+" union "
        					+" select c.Name,c.PostalAddress,c.ZipCode,c.Phone,c.UnifiedSocialCreditNo from  LbGrpAppnt c where c.GrpContNo = '" + mGrpContNo + "' "
        					+" union "
        					+" select c.Name,c.PostalAddress,c.ZipCode,c.Phone,c.UnifiedSocialCreditNo from  LobGrpAppnt c where c.GrpContNo = '" + mGrpContNo + "' "
									;
					        arrResult = easyExecSql(tSql3,1,0);
        if (arrResult != null) {
          displayAddress3(arrResult[0]);
        }
      }
    }
    if( mOperate == 2 )	{		// 投保单位信息
      arrResult = easyExecSql("select b.CustomerNo,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpAppntNum from LDGrp b where  b.CustomerNo='" + mGrpContNo + "'", 1, 0);
      if (arrResult == null) {
        alert("未查到投保单位信息");
      } else {
        displayAddress(arrResult[0]);
      }
    }
  }
  //alert("hehe");
  getLCImpart();   //团体告知
  getCountsOfPeopleByType();//人员类别人数
  var tStrqSq1="select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LCCustomerImpart where GrpContNo='" + mGrpContNo + "'"
  					//	+" union "
  					//	+" select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LbCustomerImpart where GrpContNo='" + arrQueryResult[0][0] + "'"
  						+" union "
  						+" select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LobCustomerImpart where GrpContNo='" + mGrpContNo + "'";
    turnPage.queryModal(tStrqSq1,ImpartGrid);
    turnPage.queryModal("select InsuStartYear,InsuEndYear,InsuContent,Rate,EnsureContent,Peoples,RecompensePeoples,OccurMoney,RecompenseMoney,PendingMoney,SerialNo from LCHistoryImpart where GrpContNo='"+ mGrpContNo + "' and (insuyear<=0 or insuyear is null)",HistoryImpartGrid);
    turnPage.queryModal("select InsuYear,InsuContent,EnsureContent,Rate,Peoples,RecompensePeoples,OccurMoney,RecompenseMoney,PendingMoney,SerialNo from LCHistoryImpart where GrpContNo='"+ mGrpContNo + "' and insuyear>0",NewHistoryImpartGrid);
    turnPage.queryModal("select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where GrpContNo='"+ mGrpContNo + "' and impartver='031' order by impartcode",BaseImpartDetailGrid);
    turnPage.queryModal("select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where GrpContNo='"+ mGrpContNo + "' and impartver='032' order by impartcode",HealthImpartDetailGrid);
    turnPage.queryModal("select OcurTime,DiseaseName,DiseasePepoles,CureMoney,Remark,SerialNo from LCDiseaseImpart where GrpContNo='"+ arrQueryResult[0][0] + "'",DiseaseGrid);
    //客户需求服务
    if(ImpartGrid.mulLineCount<=0)
        ImpartGrid.addOne();
    if(HistoryImpartGrid.mulLineCount<=0)
        HistoryImpartGrid.addOne();
    if(DiseaseGrid.mulLineCount<=0)
        DiseaseGrid.addOne();
    //修改客户服务需求查询，按 ProposalGrpContNo 查询客户服务需求
    arrResult = easyExecSql("select ProposalGrpContNo from LCGrpCont where GrpContNo = '" + mGrpContNo + "'");
    var ProposalGrpContNo = "";
    if(arrResult)
        ProposalGrpContNo = arrResult[0][0];
    else
        ProposalGrpContNo = arrQueryResult[0][0];
    var servInfoSql = "select distinct a.ServKind,b.ServKindRemark,a.ServDetail,c.ServDetailRemark,trim(a.servkind)||'-'||trim(a.servdetail),a.servchoose,'',d.ServChooseRemark "
        + "from LCGrpServInfo a,LDServKindInfo b,LDServDetailInfo c,LDServChooseInfo d "
        + "where a.ServKind=b.ServKind and a.ServKind=c.ServKind and a.ServDetail=c.ServDetail and a.ServKind=d.ServKind "
        + "and a.ServDetail=d.ServDetail and b.KindCode=d.KindCode and a.ServChoose=d.ServChoose and GrpContNo='" 
        + ProposalGrpContNo + "' order by a.ServKind,a.ServDetail";
    turnPage.queryModal(servInfoSql, ServInfoGrid);
    if(ServInfoGrid.mulLineCount <= 0)
        initServInfoGrid();

    mOperate = 0;		// 恢复初态
    displayBookingPay();
    getaddresscodedata();
}

/*********************************************************************
 *  把查询返回的客户地址数据返回
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAddress() {
  try {
    fm.all('GrpNo').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('CustomerNo').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressNo').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value="";
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value= "";
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value= "";
  } catch(ex) { }
  ;
  //以下是ldgrp表
  try {
    fm.all('GrpNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][2]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][3]);
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOnWorkPeoples').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOffWorkPeoples').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOtherPeoples').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAppNum').value= arrResult[0][17];
  } catch(ex) { }
  ;
}
function displayAddress1() {
  try {
    fm.all('GrpNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('CustomerNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressNo').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value= arrResult[0][15];
  } catch(ex) { }
  ;



}

function displayAddress2() {
  //以下是ldgrp表
  try {
    fm.all('GrpName').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][1]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][2]);
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOnWorkPeoples').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOffWorkPeoples').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOtherPeoples').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAppNum').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('OrgancomCode').value= arrResult[0][17];
  } catch(ex) { }
  ;
}
function displayAddress3() {
  try {
    fm.all('GrpName').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
	    fm.all('UnifiedSocialCreditNo').value= arrResult[0][4];
	  } catch(ex) { }
	  ;
}
/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt() {
  //由"./AutoCreatLDGrpInit.jsp"自动生成
  try {
    fm.all('GrpNo').value = arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('Password').value = arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value = arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressCode').value = arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value = arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value = arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value = arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value = arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value = arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value = arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value = arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value = arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value = arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value = arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value = arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value = arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value = arrResult[0][17];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value = arrResult[0][18];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value = arrResult[0][19];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value = arrResult[0][20];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value = arrResult[0][21];
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value = arrResult[0][22];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value = arrResult[0][23];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value = arrResult[0][24];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value = arrResult[0][25];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value = arrResult[0][26];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value = arrResult[0][27];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value = arrResult[0][28];
  } catch(ex) { }
  ;
  try {
    fm.all('GetFlag').value = arrResult[0][29];
  } catch(ex) { }
  ;
  try {
    fm.all('Satrap').value = arrResult[0][30];
  } catch(ex) { }
  ;
  try {
    fm.all('EMail').value = arrResult[0][31];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value = arrResult[0][32];
  } catch(ex) { }
  ;
  try {
    fm.all('BankAccNo').value = arrResult[0][33];
  } catch(ex) { }
  ;
  try {
    fm.all('BankCode').value = arrResult[0][34];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpGroupNo').value = arrResult[0][35];
  } catch(ex) { }
  ;
  try {
    fm.all('State').value = arrResult[0][36];
  } catch(ex) { }
  ;
  try {
    fm.all('Remark').value = arrResult[0][37];
  } catch(ex) { }
  ;
  try {
    fm.all('BlacklistFlag').value = arrResult[0][38];
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value = arrResult[0][39];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value = arrResult[0][40];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value = arrResult[0][41];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value = arrResult[0][42];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value = arrResult[0][43];
  } catch(ex) { }
  ;
  try {
    fm.all('FIELDNUM').value = arrResult[0][44];
  } catch(ex) { }
  ;
  try {
    fm.all('PK').value = arrResult[0][45];
  } catch(ex) { }
  ;
  try {
    fm.all('fDate').value = arrResult[0][46];
  } catch(ex) { }
  ;
  try {
    fm.all('mErrors').value = arrResult[0][47];
  } catch(ex) { }
  ;
}
function displayLCGrpCont() {
  try {
    fm.all('GrpContNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('ProposalGrpContNo').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('PrtNo').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('SaleChnl').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageCom').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCom').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCom1').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentComName').value= getNameByCode("Name","LACom","AgentCom",arrResult[0][5]);
  } catch(ex) { }
  ;
  try {
    fm.all('AgentComName1').value= getNameByCode("Name","LACom","AgentCom",arrResult[0][5]);
  } catch(ex) { }
  ;
  try {
    fm.all('AgentType').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentName').value= getNameByCode("Name","LAAgent","AgentCode",arrResult[0][7]);
  } catch(ex) { }
  ;
  try {
    fm.all('AgentGroup').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode1').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Password').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Password2').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntNo').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressNo').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples2').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][16]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][17];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][17]);
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][18];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][19];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][20];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][21];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][22];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][23];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][24];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][25];
  } catch(ex) { }
  ;
  try {
    fm.all('GetFlag').value= arrResult[0][26];
  } catch(ex) { }
  ;
  try {
    fm.all('Satrap').value= arrResult[0][27];
  } catch(ex) { }
  ;
  try {
    fm.all('EMail').value= arrResult[0][28];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][29];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpGroupNo').value= arrResult[0][30];
  } catch(ex) { }
  ;
  try {
    fm.all('BankCode').value= arrResult[0][31];
  } catch(ex) { }
  ;
  try {
    fm.all('BankAccNo').value= arrResult[0][32];
  } catch(ex) { }
  ;
  try {
    fm.all('AccName').value= arrResult[0][33];
  } catch(ex) { }
  ;
  try {
    fm.all('DisputedFlag ').value= arrResult[0][34];
  } catch(ex) { }
  ;
  try {
    fm.all('OutPayFlag').value= arrResult[0][35];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolMode').value= arrResult[0][36];
  } catch(ex) { }
  ;
  try {
    fm.all('Lang').value= arrResult[0][37];
  } catch(ex) { }
  ;
  try {
    fm.all('Currency').value= arrResult[0][38];
  } catch(ex) { }
  ;
  try {
    fm.all('LostTimes').value= arrResult[0][39];
  } catch(ex) { }
  ;
  try {
    fm.all('PrintCount').value= arrResult[0][40];
  } catch(ex) { }
  ;
  try {
    fm.all('RegetDate').value= arrResult[0][41];
  } catch(ex) { }
  ;
  try {
    fm.all('LastEdorDate').value= arrResult[0][42];
  } catch(ex) { }
  ;
  try {
    fm.all('LastGetDate').value= arrResult[0][43];
  } catch(ex) { }
  ;
  try {
    fm.all('LastLoanDate').value= arrResult[0][44];
  } catch(ex) { }
  ;
  try {
    fm.all('SpecFlag').value= arrResult[0][45];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpSpec').value= arrResult[0][46];
  } catch(ex) { }
  ;
  try {
    fm.all('PayMode').value= arrResult[0][47];
  } catch(ex) { }
  ;
  try {
    fm.all('PayModeName').value= arrResult[0][47];
  } catch(ex) { }
  ;
  try {
    fm.all('SignCom').value= arrResult[0][48];
  } catch(ex) { }
  ;
  try {
    fm.all('SignDate').value= arrResult[0][49];
  } catch(ex) { }
  ;
  try {
    fm.all('SignTime').value= arrResult[0][50];
  } catch(ex) { }
  ;
  try {

    fm.all('CValiDate').value= arrResult[0][51];

  } catch(ex) { }
  ;
  try {
    fm.all('GrpContPayIntv').value= arrResult[0][52];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpContPayIntvName').value= arrResult[0][52];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageFeeRate').value= arrResult[0][53];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPeoples').value= arrResult[0][54];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPremium').value= arrResult[0][55];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpAmnt').value= arrResult[0][56];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][57];
  } catch(ex) { }
  ;
  try {
    fm.all('Mult').value= arrResult[0][58];
  } catch(ex) { }
  ;
  try {
    fm.all('Amnt').value= arrResult[0][60];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPrem').value= arrResult[0][61];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPay').value= arrResult[0][62];
  } catch(ex) { }
  ;
  try {
    fm.all('Dif').value= arrResult[0][63];
  } catch(ex) { }
  ;
  try {
    fm.all('Remark').value= arrResult[0][64];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag1').value= arrResult[0][65];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag2').value= arrResult[0][66];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag3').value= arrResult[0][67];
  } catch(ex) { }
  ;
  try {
    fm.all('InputOperator').value= arrResult[0][68];
  } catch(ex) { }
  ;
 // alert(arrResult[0][68]);
  try {
    fm.all('InputDate').value= arrResult[0][69];
  } catch(ex) { }
  ;
  try {
    fm.all('InputTime').value= arrResult[0][70];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveFlag').value= arrResult[0][71];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveCode').value= arrResult[0][72];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveDate').value= arrResult[0][73];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveTime').value= arrResult[0][74];
  } catch(ex) { }
  ;
  try {
    fm.all('UWOperator').value= arrResult[0][75];
  } catch(ex) { }
  ;
  try {
    fm.all('UWFlag').value= arrResult[0][76];
  } catch(ex) { }
  ;
  try {
    fm.all('UWDate').value= arrResult[0][77];
  } catch(ex) { }
  ;
  try {
    fm.all('UWTime').value= arrResult[0][78];
  } catch(ex) { }
  ;
  try {
    fm.all('AppFlag').value= getNameFromLDCode("stateflag",arrResult[0][79]);
  } catch(ex) { }
  ;
  try {
    fm.all('PolApplyDate').value= arrResult[0][80];
  } catch(ex) { }
  ;
  try {
    fm.all('CustomGetPolDate').value= arrResult[0][81];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolDate').value= arrResult[0][82];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolTime').value= arrResult[0][83];
  } catch(ex) { }
  ;
  try {
    fm.all('State').value= arrResult[0][84];
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value= arrResult[0][85];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value= arrResult[0][86];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value= arrResult[0][87];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value= arrResult[0][88];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value= arrResult[0][89];
  } catch(ex) { }
  ;
  try {
    fm.all('EnterKind').value= arrResult[0][90];
  } catch(ex) { }
  ;
  try {
    fm.all('AmntGrade').value= arrResult[0][91];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples3').value= arrResult[0][92];
  } catch(ex) { }
  ;
  try {
    fm.all('OnWorkPeoples').value= arrResult[0][93];
  } catch(ex) { }
  ;
  try {
    fm.all('OffWorkPeoples').value= arrResult[0][94];
  } catch(ex) { }
  ;
  try {
    fm.all('OtherPeoples').value= arrResult[0][95];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaPeoples').value= arrResult[0][96];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaMatePeoples').value= arrResult[0][97];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaYoungPeoples').value= arrResult[0][98];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaOtherPeoples').value= arrResult[0][99];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstTrialOperator').value= arrResult[0][100];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstTrialDate').value= arrResult[0][101];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstTrialTime').value= arrResult[0][102];
  } catch(ex) { }
  ;
  try {
    fm.all('ReceiveOperator').value= arrResult[0][103];
  } catch(ex) { }
  ;
  try {
    fm.all('ReceiveDate').value= arrResult[0][104];
  } catch(ex) { }
  ;
  try {
    fm.all('ReceiveTime').value= arrResult[0][105];
  } catch(ex) { }
  ;
  try {
    fm.all('TempFeeNo').value= arrResult[0][106];
  } catch(ex) { }
  ;
  try {
    fm.all('HandlerName').value= arrResult[0][107];
  } catch(ex) { }
  ;
  try {
    fm.all('HandlerDate').value= arrResult[0][108];
  } catch(ex) { }
  ;
  try {
    fm.all('HandlerPrint').value= arrResult[0][109];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentDate').value= arrResult[0][110];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessBigType').value= arrResult[0][111];
  } catch(ex) { }
  ;
  try {
    fm.all('MarketType').value= arrResult[0][112];
  } catch(ex) { }
  ;
  try {
    fm.all('SaleChnlDetail').value= arrResult[0][114];
  } catch(ex) { }
  ;
  try {
    fm.all('CInValiDate').value= "每个被保险人老年护理保险金领取起始日止";
  } catch(ex) { }
  ;
  try {
    fm.all('AskGrpContNo').value= arrResult[0][121];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpContSumPrem').value= arrResult[0][125];
  } catch(ex) { }
  ;
  try {
    fm.all('BigProjectFlag').value= arrResult[0][128];
  } catch(ex) { }
  ;
    try {
    fm.all('ContPrintType').value= arrResult[0][129];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAgentCom').value= arrResult[0][131];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAgentCode').value= arrResult[0][132];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAgentName').value= arrResult[0][133];
  } catch(ex) { }
  ;
//  try
//  {
//    fm.all('CTCount').value= arrResult[0][134];
//  } catch(ex) {}
  try
  {
    fm.all('CoInsuranceFlag').value= arrResult[0][135];
  } catch(ex) {}
//  try
//  {
//    fm.all('CityInfo').value= arrResult[0][136];
//  } catch(ex) {}
  try
  {
    fm.all('Crs_SaleChnl').value= arrResult[0][137];
  } catch(ex) {}
  try
  {
    fm.all('Crs_BussType').value= arrResult[0][138];
  } catch(ex) {}
  try
  {
    fm.all('GrpAgentIDNo').value= arrResult[0][139];
  } catch(ex) {}
  if(arrResult[0][131] != "" && arrResult[0][132]!=""&&arrResult[0][133]!=""&&arrResult[0][137]!=""&&arrResult[0][138]!=""&&arrResult[0][139]!="")
  {
        fm.MixComFlag.checked = true;
        if(fm.MixComFlag.checked == true)
        {
            fm.all('GrpAgentComID').style.display = "";
	        fm.all('GrpAgentTitleID').style.display = "";
	        fm.all('GrpAgentTitleIDNo').style.display = "";
        }
        var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
	    var arrResult1 = easyExecSql(strSql);
		if (arrResult1 != null) {
		     fm.GrpAgentComName.value = arrResult1[0][0];
		}
		else{  
		     fm.GrpAgentComName.value = "";
		}
  }
  
  //新添字段（资格证书号码、展业证书号码、代理销售业务员编码、代理销售业务员姓名）的显示
  var theGrpContNo = fm.all('GrpContNo').value;
  var theSQL = "select lgc.AgentSaleCode "
	  		 + ",(select name from laagenttemp where agentcode=lgc.Agentsalecode) "
	  		 + ",(select QualifNo from LAQualification where agentcode=lgc.agentcode order by validend desc fetch first 1 rows only) "
	  		 + ",(select CertifNo from LACertification where agentcode=lgc.agentcode order by validend desc fetch first 1 rows only) "
	  		 + "from lcgrpcont lgc "
	  		 + "where grpcontno='"+theGrpContNo+"'";
  var thrArr = easyExecSql(theSQL);
  var theSalechnl = fm.all('SaleChnl').value;
  if(thrArr){
	  try { fm.all('Certificate').value = thrArr[0][2]; } catch(ex) { };
	  try { fm.all('Exhibition').value = thrArr[0][3]; } catch(ex) { };
		if(theSalechnl!="03" && theSalechnl!="10" && theSalechnl!="04" && theSalechnl!="15"){
			fm.all("T1").style.display = "none";
			fm.all("I1").style.display = "none";
			fm.all("T2").style.display = "none";
			fm.all("I2").style.display = "none";
		}else{
			try { fm.all('SalesAgentCode').value = thrArr[0][0]; } catch(ex) { };
			try { fm.all('SalesAgentName').value = thrArr[0][1]; } catch(ex) { };
		}
  }
}

function displayLBGrpCont(){
 try {
    fm.all('GrpContNo').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('ProposalGrpContNo').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('PrtNo').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('SaleChnl').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageCom').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCom').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCom1').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentComName').value= getNameByCode("Name","LACom","AgentCom",arrResult[0][6]);
  } catch(ex) { }
  ;
  try {
    fm.all('AgentComName1').value= getNameByCode("Name","LACom","AgentCom",arrResult[0][6]);
  } catch(ex) { }
  ;
  try {
    fm.all('AgentType').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentName').value= getNameByCode("Name","LAAgent","AgentCode",arrResult[0][8]);
  } catch(ex) { }
  ;
  try {
    fm.all('AgentGroup').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode1').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Password').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('Password2').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntNo').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressNo').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples2').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][17];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][17]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][18];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][18]);
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][19];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][20];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][21];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][22];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][23];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][24];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][25];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][26];
  } catch(ex) { }
  ;
  try {
    fm.all('GetFlag').value= arrResult[0][27];
  } catch(ex) { }
  ;
  try {
    fm.all('Satrap').value= arrResult[0][28];
  } catch(ex) { }
  ;
  try {
    fm.all('EMail').value= arrResult[0][29];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][30];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpGroupNo').value= arrResult[0][31];
  } catch(ex) { }
  ;
  try {
    fm.all('BankCode').value= arrResult[0][32];
  } catch(ex) { }
  ;
  try {
    fm.all('BankAccNo').value= arrResult[0][33];
  } catch(ex) { }
  ;
  try {
    fm.all('AccName').value= arrResult[0][34];
  } catch(ex) { }
  ;
  try {
    fm.all('DisputedFlag ').value= arrResult[0][35];
  } catch(ex) { }
  ;
  try {
    fm.all('OutPayFlag').value= arrResult[0][36];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolMode').value= arrResult[0][37];
  } catch(ex) { }
  ;
  try {
    fm.all('Lang').value= arrResult[0][38];
  } catch(ex) { }
  ;
  try {
    fm.all('Currency').value= arrResult[0][39];
  } catch(ex) { }
  ;
  try {
    fm.all('LostTimes').value= arrResult[0][40];
  } catch(ex) { }
  ;
  try {
    fm.all('PrintCount').value= arrResult[0][41];
  } catch(ex) { }
  ;
  try {
    fm.all('RegetDate').value= arrResult[0][42];
  } catch(ex) { }
  ;
  try {
    fm.all('LastEdorDate').value= arrResult[0][43];
  } catch(ex) { }
  ;
  try {
    fm.all('LastGetDate').value= arrResult[0][44];
  } catch(ex) { }
  ;
  try {
    fm.all('LastLoanDate').value= arrResult[0][45];
  } catch(ex) { }
  ;
  try {
    fm.all('SpecFlag').value= arrResult[0][46];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpSpec').value= arrResult[0][47];
  } catch(ex) { }
  ;
  try {
    fm.all('PayMode').value= arrResult[0][48];
  } catch(ex) { }
  ;
  try {
    fm.all('PayModeName').value= arrResult[0][48];
  } catch(ex) { }
  ;
  try {
    fm.all('SignCom').value= arrResult[0][49];
  } catch(ex) { }
  ;
  try {
    fm.all('SignDate').value= arrResult[0][50];
  } catch(ex) { }
  ;
  try {
    fm.all('SignTime').value= arrResult[0][51];
  } catch(ex) { }
  ;
  try {

    fm.all('CValiDate').value= arrResult[0][52];

  } catch(ex) { }
  ;
  try {
    fm.all('GrpContPayIntv').value= arrResult[0][53];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpContPayIntvName').value= arrResult[0][53];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageFeeRate').value= arrResult[0][54];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPeoples').value= arrResult[0][55];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPremium').value= arrResult[0][56];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpAmnt').value= arrResult[0][57];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][58];
  } catch(ex) { }
  ;
  try {
    fm.all('Mult').value= arrResult[0][59];
  } catch(ex) { }
  ;
  try {
    fm.all('Amnt').value= arrResult[0][61];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPrem').value= arrResult[0][62];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPay').value= arrResult[0][63];
  } catch(ex) { }
  ;
  try {
    fm.all('Dif').value= arrResult[0][64];
  } catch(ex) { }
  ;
  try {
    fm.all('Remark').value= arrResult[0][65];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag1').value= arrResult[0][66];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag2').value= arrResult[0][67];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag3').value= arrResult[0][68];
  } catch(ex) { }
  ;
  try {
    fm.all('InputOperator').value= arrResult[0][69];
  } catch(ex) { }
  ;
 // alert(arrResult[0][68]);
  try {
    fm.all('InputDate').value= arrResult[0][70];
  } catch(ex) { }
  ;
  try {
    fm.all('InputTime').value= arrResult[0][71];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveFlag').value= arrResult[0][72];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveCode').value= arrResult[0][73];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveDate').value= arrResult[0][74];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveTime').value= arrResult[0][75];
  } catch(ex) { }
  ;
  try {
    fm.all('UWOperator').value= arrResult[0][76];
  } catch(ex) { }
  ;
  try {
    fm.all('UWFlag').value= arrResult[0][77];
  } catch(ex) { }
  ;
  try {
    fm.all('UWDate').value= arrResult[0][78];
  } catch(ex) { }
  ;
  try {
    fm.all('UWTime').value= arrResult[0][79];
  } catch(ex) { }
  ;
  try {
    fm.all('AppFlag').value= getNameFromLDCode("stateflag",arrResult[0][80]);
  } catch(ex) { }
  ;
  try {
    fm.all('PolApplyDate').value= arrResult[0][81];
  } catch(ex) { }
  ;
  try {
    fm.all('CustomGetPolDate').value= arrResult[0][82];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolDate').value= arrResult[0][83];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolTime').value= arrResult[0][84];
  } catch(ex) { }
  ;
  try {
    fm.all('State').value= arrResult[0][85];
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value= arrResult[0][86];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value= arrResult[0][87];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value= arrResult[0][88];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value= arrResult[0][89];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value= arrResult[0][90];
  } catch(ex) { }
  ;
  try {
    fm.all('EnterKind').value= arrResult[0][91];
  } catch(ex) { }
  ;
  try {
    fm.all('AmntGrade').value= arrResult[0][92];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples3').value= arrResult[0][93];
  } catch(ex) { }
  ;
  try {
    fm.all('OnWorkPeoples').value= arrResult[0][94];
  } catch(ex) { }
  ;
  try {
    fm.all('OffWorkPeoples').value= arrResult[0][95];
  } catch(ex) { }
  ;
  try {
    fm.all('OtherPeoples').value= arrResult[0][96];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaPeoples').value= arrResult[0][97];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaMatePeoples').value= arrResult[0][98];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaYoungPeoples').value= arrResult[0][99];
  } catch(ex) { }
  ;
  try {
    fm.all('RelaOtherPeoples').value= arrResult[0][100];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstTrialOperator').value= arrResult[0][101];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstTrialDate').value= arrResult[0][102];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstTrialTime').value= arrResult[0][103];
  } catch(ex) { }
  ;
  try {
    fm.all('ReceiveOperator').value= arrResult[0][104];
  } catch(ex) { }
  ;
  try {
    fm.all('ReceiveDate').value= arrResult[0][105];
  } catch(ex) { }
  ;
  try {
    fm.all('ReceiveTime').value= arrResult[0][106];
  } catch(ex) { }
  ;
  try {
    fm.all('TempFeeNo').value= arrResult[0][107];
  } catch(ex) { }
  ;
  try {
    fm.all('HandlerName').value= arrResult[0][108];
  } catch(ex) { }
  ;
  try {
    fm.all('HandlerDate').value= arrResult[0][109];
  } catch(ex) { }
  ;
  try {
    fm.all('HandlerPrint').value= arrResult[0][110];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentDate').value= arrResult[0][111];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessBigType').value= arrResult[0][112];
  } catch(ex) { }
  ;
  try {
    fm.all('MarketType').value= arrResult[0][113];
  } catch(ex) { }
  ;
  try {
    fm.all('SaleChnlDetail').value= arrResult[0][115];
  } catch(ex) { }
  ;
  try {
    fm.all('CInValiDate').value= "每个被保险人老年护理保险金领取起始日止";
  } catch(ex) { }
  ;
  try {
    fm.all('AskGrpContNo').value= arrResult[0][122];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpContSumPrem').value= arrResult[0][126];
  } catch(ex) { }
  ;
  try {
    fm.all('BigProjectFlag').value= arrResult[0][129];
  } catch(ex) { }
  ;
    try {
    fm.all('ContPrintType').value= arrResult[0][130];
  } catch(ex) { }
  ;

//新添字段（资格证书号码、展业证书号码、代理销售业务员编码、代理销售业务员姓名）的显示
  var theGrpContNo = fm.all('GrpContNo').value;
  var theSQL = "select lgc.AgentSaleCode "
	  		 + ",(select name from laagenttemp where agentcode=lgc.Agentsalecode) "
	  		 + ",(select QualifNo from LAQualification where agentcode=lgc.agentcode order by validend desc fetch first 1 rows only) "
	  		 + ",(select CertifNo from LACertification where agentcode=lgc.agentcode order by validend desc fetch first 1 rows only) "
	  		 + "from lbgrpcont lgc "
	  		 + "where grpcontno='"+theGrpContNo+"'";
  var thrArr = easyExecSql(theSQL);
  var theSalechnl = fm.all('SaleChnl').value;
  if(thrArr){
	  try { fm.all('Certificate').value = thrArr[0][2]; } catch(ex) { };
	  try { fm.all('Exhibition').value = thrArr[0][3]; } catch(ex) { };
		if(theSalechnl!="03" && theSalechnl!="10" && theSalechnl!="04" && theSalechnl!="15"){
			fm.all("T1").style.display = "none";
			fm.all("I1").style.display = "none";
			fm.all("T2").style.display = "none";
			fm.all("I2").style.display = "none";
		}else{
			try { fm.all('SalesAgentCode').value = thrArr[0][0]; } catch(ex) { };
			try { fm.all('SalesAgentName').value = thrArr[0][1]; } catch(ex) { };
		}
  }

}
function displayLCGrpPol() {
  //由"./AutoCreatLCGrpPolInit.jsp"自动生成
  try {
    fm.all('ContNo').value = arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('ProposalGrpContNo').value = arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('PrtNo').value = arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('KindCode').value = arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('RiskCode').value = arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('RiskVersion').value = arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('SignCom').value = arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageCom').value = arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCom').value = arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentType').value = arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('SaleChnl').value = arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('Password').value = arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNo').value = arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('Password2').value = arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpName').value = arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddressCode').value = arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value = arrResult[0][17];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value = arrResult[0][18];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value = arrResult[0][19];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][19]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][20];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][20]);
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples2').value = arrResult[0][21];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value = arrResult[0][22];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value = arrResult[0][23];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value = arrResult[0][24];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value = arrResult[0][25];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value = arrResult[0][26];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value = arrResult[0][27];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value = arrResult[0][28];
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value = arrResult[0][29];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value = arrResult[0][30];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value = arrResult[0][31];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value = arrResult[0][32];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value = arrResult[0][33];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value = arrResult[0][34];
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value = arrResult[0][35];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value = arrResult[0][36];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value = arrResult[0][37];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value = arrResult[0][38];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value = arrResult[0][39];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value = arrResult[0][40];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value = arrResult[0][41];
  } catch(ex) { }
  ;
  try {
    fm.all('GetFlag').value = arrResult[0][42];
  } catch(ex) { }
  ;
  try {
    fm.all('Satrap').value = arrResult[0][43];
  } catch(ex) { }
  ;
  try {
    fm.all('EMail').value = arrResult[0][44];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value = arrResult[0][45];
  } catch(ex) { }
  ;
  try {
    fm.all('BankAccNo').value = arrResult[0][46];
  } catch(ex) { }
  ;
  try {
    fm.all('BankCode').value = arrResult[0][47];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpGroupNo').value = arrResult[0][48];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpContPayIntv').value = arrResult[0][49];
  } catch(ex) { }
  ;
  try {
    fm.all('PayMode').value = arrResult[0][50];
  } catch(ex) { }
  ;
  try {
    fm.all('CValiDate').value = arrResult[0][51];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolDate').value = arrResult[0][52];
  } catch(ex) { }
  ;
  try {
    fm.all('SignDate').value = arrResult[0][53];
  } catch(ex) { }
  ;
  try {
    fm.all('FirstPayDate').value = arrResult[0][54];
  } catch(ex) { }
  ;
  try {
    fm.all('PayEndDate').value = arrResult[0][55];
  } catch(ex) { }
  ;
  try {
    fm.all('PaytoDate').value = arrResult[0][56];
  } catch(ex) { }
  ;
  try {
    fm.all('RegetDate').value = arrResult[0][57];
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value = arrResult[0][58];
  } catch(ex) { }
  ;
  try {
    fm.all('Mult').value = arrResult[0][59];
  } catch(ex) { }
  ;
  try {
    fm.all('Prem').value = arrResult[0][60];
  } catch(ex) { }
  ;
  try {
    fm.all('Amnt').value = arrResult[0][61];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPrem').value = arrResult[0][62];
  } catch(ex) { }
  ;
  try {
    fm.all('SumPay').value = arrResult[0][63];
  } catch(ex) { }
  ;
  try {
    fm.all('Dif').value = arrResult[0][64];
  } catch(ex) { }
  ;
  try {
    fm.all('SSFlag').value = arrResult[0][65];
  } catch(ex) { }
  ;
  try {
    fm.all('PeakLine').value = arrResult[0][66];
  } catch(ex) { }
  ;
  try {
    fm.all('GetLimit').value = arrResult[0][67];
  } catch(ex) { }
  ;
  try {
    fm.all('GetRate').value = arrResult[0][68];
  } catch(ex) { }
  ;
  try {
    fm.all('MaxMedFee').value = arrResult[0][69];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPeoples').value = arrResult[0][70];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpPremium').value = arrResult[0][71];
  } catch(ex) { }
  ;
  try {
    fm.all('ExpAmnt').value = arrResult[0][72];
  } catch(ex) { }
  ;
  try {
    fm.all('DisputedFlag').value = arrResult[0][73];
  } catch(ex) { }
  ;
  try {
    fm.all('BonusRate').value = arrResult[0][74];
  } catch(ex) { }
  ;
  try {
    fm.all('Lang').value = arrResult[0][75];
  } catch(ex) { }
  ;
  try {
    fm.all('Currency').value = arrResult[0][76];
  } catch(ex) { }
  ;
  try {
    fm.all('State').value = arrResult[0][77];
  } catch(ex) { }
  ;
  try {
    fm.all('LostTimes').value = arrResult[0][78];
  } catch(ex) { }
  ;
  try {
    fm.all('AppFlag').value = arrResult[0][79];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveCode').value = arrResult[0][80];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveDate').value = arrResult[0][81];
  } catch(ex) { }
  ;
  try {
    fm.all('UWOperator').value = arrResult[0][82];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode').value = arrResult[0][83];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentGroup').value = arrResult[0][84];
  } catch(ex) { }
  ;
  try {
    fm.all('AgentCode1').value = arrResult[0][85];
  } catch(ex) { }
  ;
  try {
    fm.all('Remark').value = arrResult[0][86];
  } catch(ex) { }
  ;
  try {
    fm.all('UWFlag').value = arrResult[0][87];
  } catch(ex) { }
  ;
  try {
    fm.all('OutPayFlag').value = arrResult[0][88];
  } catch(ex) { }
  ;
  try {
    fm.all('ApproveFlag').value = arrResult[0][89];
  } catch(ex) { }
  ;
  try {
    fm.all('EmployeeRate').value = arrResult[0][90];
  } catch(ex) { }
  ;
  try {
    fm.all('FamilyRate').value = arrResult[0][91];
  } catch(ex) { }
  ;
  try {
    fm.all('Operator').value = arrResult[0][92];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeDate').value = arrResult[0][93];
  } catch(ex) { }
  ;
  try {
    fm.all('MakeTime').value = arrResult[0][94];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyDate').value = arrResult[0][95];
  } catch(ex) { }
  ;
  try {
    fm.all('ModifyTime').value = arrResult[0][96];
  } catch(ex) { }
  ;
  try {
    fm.all('FIELDNUM').value = arrResult[0][97];
  } catch(ex) { }
  ;
  try {
    fm.all('PK').value = arrResult[0][98];
  } catch(ex) { }
  ;
  try {
    fm.all('fDate').value = arrResult[0][99];
  } catch(ex) { }
  ;
  try {
    fm.all('ManageFeeRate').value = arrResult[0][100];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpSpec').value = arrResult[0][101];
  } catch(ex) { }
  ;
  try {
    fm.all('GetPolMode').value = arrResult[0][102];
  } catch(ex) { }
  ;
  try {
    fm.all('PolApplyDate').value = arrResult[0][103];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag1').value = arrResult[0][105];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag2').value = arrResult[0][106];
  } catch(ex) { }
  ;
  try {
    fm.all('StandbyFlag3').value = arrResult[0][107];
  } catch(ex) { }
  ;

}




/*********************************************************************
 *  Click事件，当点击“关联暂交费信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showFee() {
  cPolNo = fm.ProposalGrpContNo.value;
  if( cPolNo == "" ) {
    alert( "您必须先查询投保单才能进入暂交费信息部分。" );
    return false
         }

         showInfo = window.open( "./ProposalFee.jsp?PolNo=" + cPolNo + "&polType=GROUP" );
}

function queryAgent()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        return ;
    }
    
    if(fm.all('AgentCode').value == "")
    {
        var tSaleChnl = (fm.SaleChnl != null && fm.SaleChnl != "undefined") ? fm.SaleChnl.value : "";
        var tAgentCom = (fm.AgentCom != null && fm.AgentCom != "undefined") ? fm.AgentCom.value : "";
        
        // 交叉渠道，个险直销人员可以销售团险产品。
        var tBranchType = 2;
        if(tSaleChnl == '06')
        {
            tBranchType = 1;
        }
        // ----------------------------------
        
        //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&SaleChnl=" + tSaleChnl
            + "&AgentCom=" + tAgentCom
            + "&branchtype=" + tBranchType;
        //alert("tSaleChnl : " + strURL);
        var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('AgentCode').value != "")
    {
        var cAgentCode = fm.AgentCode.value;  //保单号码
        var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode + "' and ManageCom = '" + fm.all('ManageCom').value + "'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentName.value = arrResult[0][1];
            fm.AgentGroup.value = arrResult[0][2];
            alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value = "";
            alert("代码为:["+fm.all('AgentCode').value+"]的业务员不存在，请确认!");
        }
    }
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult) {
  if(arrResult!=null) {
    fm.AgentCode.value = arrResult[0][0];
    fm.AgentName.value = arrResult[0][5];
    fm.AgentGroup.value = arrResult[0][1];
  }
}

function queryAgent2() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
  }
  if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==8 )	 {
    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    } else {
      fm.AgentGroup.value="";
      alert("代码为:["+fm.all('AgentCode').value+"]的业务员不存在，请确认!");
    }
  }
}

function afterCodeSelect( cCodeName, Field ) {
  if(cCodeName=="GetGrpAddressNo") {
    var strSQL="select b.AddressNo,b.GrpAddress,b.GrpZipCode,b.LinkMan1,b.Department1,b.HeadShip1,b.Phone1,b.E_Mail1,b.Fax1,b.LinkMan2,b.Department2,b.HeadShip2,b.Phone2,b.E_Mail2,b.Fax2 from LCGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
    arrResult=easyExecSql(strSQL);
    try {
      fm.all('GrpAddressNo').value= arrResult[0][0];
    } catch(ex) { }
    ;
    try {
      fm.all('GrpAddress').value= arrResult[0][1];
    } catch(ex) { }
    ;
    try {
      fm.all('GrpZipCode').value= arrResult[0][2];
    } catch(ex) { }
    ;
    try {
      fm.all('LinkMan1').value= arrResult[0][3];
    } catch(ex) { }
    ;
    try {
      fm.all('Department1').value= arrResult[0][4];
    } catch(ex) { }
    ;
    try {
      fm.all('HeadShip1').value= arrResult[0][5];
    } catch(ex) { }
    ;
    try {
      fm.all('Phone1').value= arrResult[0][6];
    } catch(ex) { }
    ;
    try {
      fm.all('E_Mail1').value= arrResult[0][7];
    } catch(ex) { }
    ;
    try {
      fm.all('Fax1').value= arrResult[0][8];
    } catch(ex) { }
    ;
    try {
      fm.all('LinkMan2').value= arrResult[0][9];
    } catch(ex) { }
    ;
    try {
      fm.all('Department2').value= arrResult[0][10];
    } catch(ex) { }
    ;
    try {
      fm.all('HeadShip2').value= arrResult[0][11];
    } catch(ex) { }
    ;
    try {
      fm.all('Phone2').value= arrResult[0][12];
    } catch(ex) { }
    ;
    try {
      fm.all('E_Mail2').value= arrResult[0][13];
    } catch(ex) { }
    ;
    try {
      fm.all('Fax2').value= arrResult[0][14];
    } catch(ex) { }
    ;
  }


  if(cCodeName=="PayMode") {
    if(Field.value!="4") {
      fm.all("BankCode").className = "readonly";
      fm.all("BankCode").readOnly = true;
      fm.all("BankCode").tabIndex = -1;
      //fm.all("BankCode").ondblclick = "";

      fm.all("BankAccNo").className = "readonly";
      fm.all("BankAccNo").readOnly = true;
      fm.all("BankAccNo").tabIndex = -1;
      //fm.all("BankAccNo").ondblclick = "";

      fm.all("AccName").className = "readonly";
      fm.all("AccName").readOnly = true;
      fm.all("AccName").tabIndex = -1;



    } else {
      fm.all("BankCode").focus();
      fm.all("BankCode").className = "code8";
      fm.all("BankCode").readOnly = false;
      fm.all("BankCode").tabIndex = 0;
      //fm.all("BankCode").ondblclick = ;

      fm.all("BankAccNo").className = "common";
      fm.all("BankAccNo").readOnly = false;
      fm.all("BankAccNo").tabIndex = 0;

      fm.all("AccName").className = "common";
      fm.all("AccName").readOnly = false;
      fm.all("AccName").tabIndex = 0;
    }
  }

  if(cCodeName=="SaleChnl")
  {
  	if(Field.value=="03")
  	{
  		fm.all('AgentCom').style.display="";
  		fm.all('AgentComName').style.display="";

  		fm.all('AgentCom').className = "code";
	    //fm.all('AgentCom').readOnly = false;
	    ////fm.all('AgentCom').ondblclick = " showCodeList('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');";
	    fm.all('AgentComName').className = "common";
	    //fm.all('AgentComName').readOnly = false;
	    //fm.all('AgentComName').ondblclick = "";
	    fm.all("AgentCom1").style.display = "none";
	    fm.all("AgentComName1").style.display = "none";
	    //fm.all("GrpAgentTitleID").style.display = "none";
  	}
    else if(Field.value == "11" || Field.value == "12"){
	  	fm.all("AgentCom").style.display = "none";
	    fm.all("AgentComName").style.display = "none";
	    fm.all("AgentCom1").style.display = "";
	    fm.all("AgentComName1").style.display = "";
	    //fm.all("GrpAgentTitleID").style.display = "";
	    var tBranchType="2";
	    var tBranchType2="";
	    if (Field.value == "11"){
	    	tBranchType2="04";	    	
	    }else if (Field.value == "12"){
	    	tBranchType2="04";
	    }
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
	  }
	  else {
  		fm.all('AgentCom').style.display="none";
  		fm.all('AgentComName').style.display="none";
  		fm.all("AgentCom1").style.display = "none";
	    fm.all("AgentComName1").style.display = "none";
	    //fm.all("GrpAgentTitleID").style.display = "none";
	  	//fm.all('AgentCom').className = "readonly";
	    //fm.all('AgentCom').readOnly = true;
	    //fm.all('AgentCom').disable = true;
	    //fm.all('AgentCom').ondblclick = "";
	    //fm.all('AgentComName').className = "readonly";
	    //fm.all('AgentComName').readOnly = true;
	    //fm.all('AgentComName').ondblclick = "";
  	} 

 
  }
  if(cCodeName=="grppayintv")
  {
  	if(Field.value == -1)
  	{
  		//为约定缴费方式
  	divBookingPayInty.style.display='';}
  else
  	{
  		divBookingPayInty.style.display='none';
  		}
  }
  if(cCodeName=="GrpPayMode")
  {

  	if(Field.value == 2||3||4)
  	{
  		//alert();
  		fm.BankCode.style.display='';

  		fm.BankCodeName.style.display='';
  		fm.BankAccNo.style.display='';
      fm.AccName.style.display='';

    	//fm.all('BankAccNo').readOnly=true;
    	//fm.AccName.className="readonly";
    	//fm.all('AccName').readOnly=false;
  	}
  if(Field.value==1)
  	{
  		fm.BankCode.style.display='none';

  		fm.BankCodeName.style.display='none';
  		fm.BankAccNo.style.display='none';
      fm.AccName.style.display='none';
  	}
  }
  
    // 改变“是否为共保保单”选择时，控制“录入共保要素信息”按钮。
    if(cCodeName == "CoInsuranceFlag")
    {
        initCoInsuranceParamInput();
    }
    // ------------------------------
}

function checkMainAppender(cRiskCode) {
  if( isSubRisk( cRiskCode ) == true ) {   // 附险
    var tPolNo = getMainRiskNo(cRiskCode);   //弹出录入附险的窗口,得到主险保单号码
    if (!checkRiskRelation(tPolNo, cRiskCode)) {
      alert("主附险包含关系错误，输入的主险号不能带这个附加险！");
      return false;
    }
  }
  return true;
}


function isSubRisk(cRiskCode) {
  var arrQueryResult = easyExecSql("select SubRiskFlag from LMRiskApp where RiskCode='" + cRiskCode + "'", 1, 0);

  if(arrQueryResult[0] == "S")    //需要转成大写
    return true;
  if(arrQueryResult[0] == "M")
    return false;

  if (arrQueryResult[0].toUpperCase() == "A")
    if (confirm("该险种既可以是主险,又可以是附险!选择确定进入主险录入,选择取消进入附险录入"))
      return false;
    else
      return true;

  return false;
}



function checkRiskRelation(tPolNo, cRiskCode) {
  // 集体下个人投保单
  var strSql = "select RiskCode from LCGrpPol where GrpPolNo = '" + tPolNo
               + "' and RiskCode in (select Code1 from LDCode1 where Code = '" + cRiskCode + "' and codetype='grpchkappendrisk')";
  return easyQueryVer3(strSql);
}

function getMainRiskNo(cRiskCode) {
  var urlStr = "./MainRiskNoInput.jsp";
  var tPolNo="";

  tPolNo = window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:310px;dialogHeight:100px;center:yes;");
  return tPolNo;
}

function grpRiskInfo() {
  //alert('a');
  //var newWindow = window.open("./GroupRisk.jsp");
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能进入〔险种信息〕！");
  }
  delGrpVar();
  addGrpVar();
  showInfo = window.open("./GroupRisk.jsp");
}

function grpInsuInfo() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能〔添加被保人〕信息！");
    return false;
  }
  //alert("1111"+fm.GrpContNo.value);
  fm.GrpContNo.value=fm.ProposalGrpContNo.value;
  delGrpVar();
  addGrpVar();
  parent.fraInterface.window.location = "./ContInsuredInput.jsp?LoadFlag="+LoadFlag+"&ContType=2&scantype="+ scantype+"&checktype=2"+"&ScanFlag="+ScanFlag;
}

function grpInsuList() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能进入〔被保人清单〕信息界面！");
    return false;
  }

  //delGrpVar();
  //addGrpVar();
  //parent.fraInterface.window.location = "./ContGrpInsuredInput.jsp?LoadFlag="+LoadFlag+"&scantype="+scantype+"&ScanFlag="+ScanFlag+"&oldContNo="+oldContNo+"&Resource="+Resource+"&GrpContNo="+fm.GrpContNo.value;

	var tContNo = fm.GrpContNo.value;
  	var strSql = "select count(GrpContNo) from LCGrpCont where GrpContNo='" + tContNo + "'";
	var arrReturn = new Array();
	arrReturn = easyExecSql(strSql);

	//查询团单记录(撤保或退保)
  	var strSql1 = "select count(GrpContNo) from LBGrpCont where GrpContNo='" + tContNo + "'";
	var arrReturn1 = new Array();
	arrReturn1 = easyExecSql(strSql1);	

	var openWindow;
	var strSql = "SELECT 1 from lcgrppol l where grpcontno='"+tContNo+"' and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode) union SELECT 1 from lbgrppol l where grpcontno='"+tContNo+"' and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
	var arrResult = easyExecSql(strSql);
	if (arrResult == null) {    	 
		openWindow = "../sys/GrpInsuredInput.jsp";
	}else{
	   	openWindow = "../sys/GrpUliInsuredInput.jsp"; 
	}

  	if (arrReturn == 0 && arrReturn1 ==0 ) {
	   alert("找不到合同信息！");
	} else if(arrReturn != 0 ){	
		window.open(openWindow + "?ContNo=" + tContNo + "&ModeFlag=0&ContLoadFlag=2" +"&ContType=1");
	}else if(arrReturn1 != 0 ){	
		 window.open(openWindow + "?ContNo=" + tContNo + "&ModeFlag=0&ContLoadFlag=2" +"&ContType=2");
	}	
}
function grpRiskPlanInfo() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能进行〔保险计划制定〕！");
    return false;
  }
  //alert(Resource);
  var newWindow = window.open("./ContPlan.jsp?prtNo="+fm.PrtNo.value+"&GrpContNo="+fm.GrpContNo.value+"&Resource="+Resource+"&LoadFlag="+LoadFlag);
}

function focuswrap() {
  myonfocus(showInfo);
}

function delGrpVar() {
  //删除可能留在缓存中的个人合同信息
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ProposalContNo');
  } catch(ex) { }
  ;

  //团体合同信息
  try {
    mSwitch.deleteVar('GrpContNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ProposalGrpContNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PrtNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SaleChnl');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ManageCom');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentCom');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentType');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentGroup');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AgentCode1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Password');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Password2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AddressNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Peoples2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpName');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BusinessType');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpNature');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('RgtMoney');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Asset');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('NetProfitRate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('MainBussiness');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Corporation');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ComAera');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Fax');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Satrap');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('EMail');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('FoundDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOnWorkPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOffWorkPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntOtherPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpGroupNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BankCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('BankAccNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AccName');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('DisputedFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('OutPayFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetPolMode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Lang');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Currency');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LostTimes');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PrintCount');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('RegetDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LastEdorDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LastGetDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('LastLoanDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SpecFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpSpec');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PayMode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SignCom');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SignDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SignTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('CValiDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PayIntv');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ManageFeeRate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ExpPeoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ExpPremium');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ExpAmnt');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Peoples');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Mult');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Prem');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Amnt');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SumPrem');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('SumPay');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Dif');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Remark');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('StandbyFlag1');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('StandbyFlag2');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('StandbyFlag3');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('InputOperator');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('InputDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('InputTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ApproveFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ApproveCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ApproveDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('ApproveTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('UWOperator');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('UWFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('UWDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('UWTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppFlag');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PolApplyDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('CustomGetPolDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetPolDate');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GetPolTime');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('State');
  } catch(ex) { }
  ;
  //集体投保人信息
  try {
    mSwitch.deleteVar('GrpNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AddressNo');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntGrade');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpName');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('PostalAddress');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('GrpZipCode');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Phone');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('Password');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('State');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('AppntType');
  } catch(ex) { }
  ;
  try {
    mSwitch.deleteVar('RelationToInsured');
  } catch(ex) { }
  ;


}

function addGrpVar() {
  try {
    mSwitch.addVar('ContNo','','');
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ProposalContNo','','');
  } catch(ex) { }
  ;
  //集体合同信息
  try {
    mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ProposalGrpContNo','',fm.ProposalGrpContNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PrtNo','',fm.PrtNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SaleChnl','',fm.SaleChnl.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ManageCom','',fm.ManageCom.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentCom','',fm.AgentCom.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentType','',fm.AgentType.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentCode','',fm.AgentCode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentGroup','',fm.AgentGroup.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AgentCode1','',fm.AgentCode1.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Password','',fm.Password.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Password2','',fm.Password2.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntNo','',fm.AppntNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Addressno','',fm.AddressNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Peoples2','',fm.Peoples2.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpName','',fm.GrpName.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BusinessType','',fm.BusinessType.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpNature','',fm.GrpNature.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('RgtMoney','',fm.RgtMoney.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Asset','',fm.Asset.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('NetProfitRate','',fm.NetProfitRate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('MainBussiness','',fm.MainBussiness.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Corporation','',fm.Corporation.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ComAera','',fm.ComAera.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Fax','',fm.Fax.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone','',fm.Phone.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetFlag','',fm.GetFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Satrap','',fm.Satrap.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('EMail','',fm.EMail.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('FoundDate','',fm.FoundDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpGroupNo','',fm.GrpGroupNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BankCode','',fm.BankCode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AccName','',fm.AccName.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('DisputedFlag','',fm.DisputedFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('OutPayFlag','',fm.OutPayFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetPolMode','',fm.GetPolMode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Lang','',fm.Lang.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Currency','',fm.Currency.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LostTimes','',fm.LostTimes.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PrintCount','',fm.PrintCount.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('RegetDate','',fm.RegetDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LastEdorDate','',fm.LastEdorDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LastGetDate','',fm.LastGetDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('LastLoanDate','',fm.LastLoanDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SpecFlag','',fm.SpecFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpSpec','',fm.GrpSpec.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PayMode','',fm.PayMode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SignCom','',fm.SignCom.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SignDate','',fm.SignDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SignTime','',fm.SignTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('CValiDate','',fm.CValiDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PayIntv','',fm.GrpContPayIntv.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ManageFeeRate','',fm.ManageFeeRate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ExpPeoples','',fm.ExpPeoples.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ExpPremium','',fm.ExpPremium.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ExpAmnt','',fm.ExpAmnt.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Peoples','',fm.Peoples.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Mult','',fm.Mult.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Prem','',fm.Prem.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Amnt','',fm.Amnt.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SumPrem','',fm.SumPrem.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('SumPay','',fm.SumPay.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Dif','',fm.Dif.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Remark','',fm.Remark.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag1','',fm.StandbyFlag1.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag2','',fm.StandbyFlag2.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('StandbyFlag3','',fm.StandbyFlag3.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('InputOperator','',fm.InputOperator.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('InputDate','',fm.InputDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('InputTime','',fm.InputTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ApproveFlag','',fm.ApproveFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ApproveCode','',fm.ApproveCode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ApproveDate','',fm.ApproveDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ApproveTime','',fm.ApproveTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('UWOperator','',fm.UWOperator.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('UWFlag','',fm.UWFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('UWDate','',fm.UWDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('UWTime','',fm.UWTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppFlag','',fm.AppFlag.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PolApplyDate','',fm.PolApplyDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('CustomGetPolDate','',fm.CustomGetPolDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetPolDate','',fm.GetPolDate.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GetPolTime','',fm.GetPolTime.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('State','',fm.State.value);
  } catch(ex) { }
  ;
  //集体投保人信息

  try {
    mSwitch.addVar('GrpNo','',fm.GrpNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PrtNo','',fm.PrtNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AddressNo','',fm.AddressNo.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntGrade','',fm.AppntGrade.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('GrpName','',fm.Name.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('PostalAddress','',fm.PostalAddress.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('ZipCode','',fm.ZipCode.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Phone','',fm.Phone.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('Password','',fm.Password.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('State','',fm.State.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('AppntType','',fm.AppntType.value);
  } catch(ex) { }
  ;
  try {
    mSwitch.addVar('RelationToInsured','',fm.RelationToInsured.value);
  } catch(ex) { }
  ;

}
//投保公司的查询
function QueryOnKeyDown() {
  var keycode = event.keyCode;
  if(keycode=="13")
  {
  	QueryOnBlur();
   }
}
function QueryOnBlur(){
	 arrResult = easyExecSql("select * from LDGrp  where GrpName like '%%" + fm.all('GrpName').value + "%%'", 1, 0);
    if (arrResult == null) {
      alert("对不起,找不到该投保单位!");
      return false;
    } else {
      arrResult = easyExecSql("select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,"
                              + " b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.CustomerNo,"
                              + " b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples from LDGrp b where"
                              + " b.GrpName like '%%" + fm.all('GrpName').value + "%%'", 1, 0);
      if (arrResult == null) {
        alert("未查到投保单位信息");
      }
      if (arrResult.length == 1) {
      	if(!confirm("系统找到一个团体客户信息,客户名称："+arrResult[0][0]+";是否录入此客户信息")){
      		return false;
      	}
        try {
          fm.all('GrpName').value= arrResult[0][0];
        } catch(ex) { }
        ;
        try {
          fm.all('BusinessType').value= arrResult[0][1];
        } catch(ex) { }
        ;
        try {
          fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][1]);
        } catch(ex) { }
        ;
        try {
          fm.all('GrpNature').value= arrResult[0][2];
        } catch(ex) { }
        ;
        try {
          fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][2]);
        } catch(ex) { }
        ;
        try {
          fm.all('Peoples').value= arrResult[0][3];
        } catch(ex) { }
        ;
        try {
          fm.all('RgtMoney').value= arrResult[0][4];
        } catch(ex) { }
        ;
        try {
          fm.all('Asset').value= arrResult[0][5];
        } catch(ex) { }
        ;
        try {
          fm.all('NetProfitRate').value= arrResult[0][6];
        } catch(ex) { }
        ;
        try {
          fm.all('MainBussiness').value= arrResult[0][7];
        } catch(ex) { }
        ;
        try {
          fm.all('Corporation').value= arrResult[0][8];
        } catch(ex) { }
        ;
        try {
          fm.all('ComAera').value= arrResult[0][9];
        } catch(ex) { }
        ;
        try {
          fm.all('Fax').value= arrResult[0][10];
        } catch(ex) { }
        ;
        try {
          fm.all('Phone').value= arrResult[0][11];
        } catch(ex) { }
        ;
        try {
          fm.all('FoundDate').value= arrResult[0][12];
        } catch(ex) { }
        ;
        try {
          fm.all('GrpNo').value= arrResult[0][13];
        } catch(ex) { }
        ;
        try {
          fm.all('AppntOnWorkPeoples').value= arrResult[0][14];
        } catch(ex) { }
        ;
        try {
          fm.all('AppntOffWorkPeoples').value= arrResult[0][15];
        } catch(ex) { }
        ;
        try {
          fm.all('AppntOtherPeoples').value= arrResult[0][16];
        } catch(ex) { }
        ;
      	var AddressarrResult=easyExecSql("select GrpAddress,GrpZipCode,LinkMan1,Phone1,Fax1,E_Mail1 from lcgrpaddress where customerno='"+fm.all('GrpNo').value+"' and addressno in (select max(int(AddressNo)) from lcgrpaddress where customerno='"+fm.all('GrpNo').value+"') ",1,0);
				//alert(AddressarrResult);
        try {
          fm.all('GrpAddress').value= AddressarrResult[0][0];
        } catch(ex) { }
        ;				
        try {
          fm.all('GrpZipCode').value= AddressarrResult[0][1];
        } catch(ex) { }
        ;
        try {
          fm.all('LinkMan1').value= AddressarrResult[0][2];
        } catch(ex) { }
        ;
        try {
          fm.all('Phone1').value= AddressarrResult[0][3];
        } catch(ex) { }
        ;
        try {
          fm.all('Fax1').value= AddressarrResult[0][4];
        } catch(ex) { }
        ;
        try {
          fm.all('E_Mail1').value= AddressarrResult[0][5];
        } catch(ex) { }
        ;						        
      }
      if (arrResult.length > 1) {
        var varSrc = "&Grpname=" + fm.all('Grpname').value;
        showinfo = window.open("../app/FrameMainGrpAppntQuery.jsp?Interface= GrpAppntQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
      }
    }
  }

function afterAppntQuery( arrReturn ) {

  arrResult = easyExecSql("select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,"
                          + " b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.CustomerNo,"
                          + " b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples from LDGrp b where"
                          + " b.Customerno='" + arrReturn[0][0] + "'", 1, 0);

  try {
    fm.all('GrpName').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][1]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][2]);
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNo').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOnWorkPeoples').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOffWorkPeoples').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOtherPeoples').value= arrResult[0][16];
  } catch(ex) { }
  ;
}

//来自险种页面的函数，未必用到
function InputPolicy() {
  var newWindow = window.open("./NewProposal.jsp?RiskCode=111302");
}
function InputPolicyNoList() {
  var newWindow = window.open("./NewProposal.jsp?NoListFlag=1&RiskCode=111302");
}

//添加一笔险种纪录
function addRecord() {
	//首先区分是套餐险种还是普通险种
	var strSql = "select 1 from LMRiskApp where RiskCode='"+fm.RiskCode.value+"'"
							+" union "
							+"select 2 from LDRiskWrap where RiskWrapCode='"+fm.RiskCode.value+"'";
	//alert(strSql);
	var arr = easyExecSql(strSql);
	if(arr){
		if(arr[0][0] == "1"){
			fm.RiskType.value = "Risk";
		}else if(arr[0][0] == "2"){
			fm.RiskType.value = "Wrap";
		}
	}else{
		alert("当前险种代码或套餐代码系统中不存在，请确认是否录入正确！");
		return false;
	}
	if(!verifyInput2())
	return false;
  if(!ChkRiskVer()) return false;
  var tRiskCode = fm.all('RiskCode').value ;
  //var tCalRule = fm.all('CalRule').value ;
  var ExpPeoples = fm.all('ExpPeoples').value ;
  var tGrpContNo = fm.all('GrpContNo').value ;
  fm.all( 'LoadFlag' ).value = LoadFlag ;

  if(tGrpContNo==null ||tGrpContNo=="") {
    alert("团单合同信息未保存，不容许〔添加险种〕！");
    return ;
  }

  //if(tRiskCode==null ||tRiskCode==""|| tCalRule==null|| tCalRule=="") {
  //  alert("险种编码或保费计算方式录入不完整！");
  //  return ;
  //}

  RiskGrid.delBlankLine();
  
  fm.all('fmAction').value="INSERT||GROUPRISK";

  var showStr="正在添加团单险种信息，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//alert(fm.RiskType.value);
	if(fm.RiskType.value == "Risk"){
		fm.action="./GroupRiskSave.jsp";
	}else if(fm.RiskType.value == "Wrap"){
		fm.action="./GroupWrapSave.jsp";
	}
  fm.submit();
}

//删除一笔险种纪录
function deleteRecord() {
  //此方法得到的行数需要-1处理
	  var checkAcount = 0;
	  var WrapFlag = false;
	  var RiskFlag = false;
	  for (i=0; i<RiskGrid.mulLineCount; i++) {
	    if (RiskGrid.getChkNo(i)) {
	    	var RiskCode = RiskGrid.getRowColData(i,1);

	    	var strSql = "select 1 from LMRiskApp where RiskCode='"+RiskCode+"'"
										+" union "
										+"select 2 from LDRiskWrap where RiskWrapCode='"+RiskCode+"'";
				var arr = easyExecSql(strSql);
				if(arr){
					if(arr[0][0] == "1"){
						fm.RiskType.value = "Risk";
						RiskFlag = true;
					}else if(arr[0][0] == "2"){
						fm.RiskType.value = "Wrap";
						WrapFlag = true;
					}
				}
	    	checkAcount++;
	    }
	  }
	  if (checkAcount < 1) {
	  	alert("请至少选择一条险种信息！");
	  	return;
	  }
	  if(RiskFlag && WrapFlag){
	  	alert("险种和套餐不能同时删除，请先删除险种");
	  	return;
	  }
  if( confirm("是否要删除此险种信息？删除请点击“确定”，否则点“取消”。"))
  {
  RiskGrid.delBlankLine();
  var showStr="正在删除险种信息，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.all('fmAction').value="DELETE||GROUPRISK";
  if(fm.RiskType.value == "Risk"){
		fm.action="./GroupRiskSave.jsp";
	}else if(fm.RiskType.value == "Wrap"){
		fm.action="./GroupWrapSave.jsp";
	}
   fm.submit();
    }
 else
  	{
	return false;
	  }
}


/*********************************************************************
 *  团单险种信息的的录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function grpFeeInput() {
  var tGrpContNo = fm.all('GrpContNo').value ;
  if(tGrpContNo==null ||tGrpContNo=="") {
    alert("团单合同信息未保存，不容许〔添加险种〕！");
    return ;
  }
  parent.fraInterface.window.location = "./GrpFeeInput.jsp?ProposalGrpContNo="+tGrpContNo+"&LoadFlag="+LoadFlag;
}

/*********************************************************************
 *  初始化险种显示，包括 人数和保费的统计
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function fillriskgrid() {
  if(fm.ProposalGrpContNo.value!="") {
//  	  var Resource= fm.all( 'Resource' ).value ;
      mGrpContNo=fm.all( 'GrpContNo' ).value;

//      var LoadFlag=fm.LoadFlag.value;
      if(LoadFlag==16 && Resource==1){
    var strSql = " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
								+" code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+fm.all('GrpContNo').value+"' "
								+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
								+" (select sum(Peoples2) from lbcontplan where grpcontno='"+fm.GrpContNo.value+"' "
								+" and ContPlancode in (select distinct contplancode from lbcontplanrisk where "
								+" grpcontno='"+fm.GrpContNo.value+"' and riskcode=a.riskcode)), "
								+" a.peoples2, "
								+" to_zero((select sum(to_zero(prem)) From lbpol Where grppolno=a.Grppolno )) "
								+" From lbgrppol a,LMRiskApp b "
								+" Where  a.riskcode=b.riskcode  and  a.grpcontno='" + fm.all('GrpContNo').value +"' and a.riskCode not in (select riskcode from LCRiskWrap where grpcontno=a.grpcontno) "
								+" union "
                +" select a,b,c,d,e, "                                                                                                         
                +" to_zero(f) from (select a.RiskWrapCode a,b.RiskWrapName b,(select codename from ldcode where codetype='calrule' and "       
                +"  code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+fm.GrpContNo.value+"' "                                      
                +"  and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')) c, "                                               
                +" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"'  and ContPlancode "                                      
                +" in (select distinct contplancode from lccontplanrisk where  grpcontno='"+fm.GrpContNo.value+"' "                                        
                +" and riskcode=a.riskcode))  d,a.peoples2 e, "                                                                                
                +" (select sum(to_zero(prem)) From lbpol Where grppolno=a.Grppolno ) f from LCRiskWrap a,LDRiskWrap "                          
                +"  b  where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode "                                                       
                +" and a.Grpcontno='"+fm.GrpContNo.value+"' ) as x group by a,b,c,d,e,f  fetch first 3000 rows only  "  
                
}else if(LoadFlag==16&Resource==2){

  var strSql =  " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
+" code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+fm.all('GrpContNo').value+"' "
+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
+" a.peoples2, "
+" a.peoples2, "
+" to_zero((select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno )) "
+" From lcgrppol a,LMRiskApp b "
+" Where  a.riskcode=b.riskcode   and  a.grpcontno='" + fm.all('GrpContNo').value +"' and a.riskCode not in (select riskcode from LCRiskWrap where grpcontno=a.grpcontno)"
+" union "
+" select a,b,c,d,e,to_zero(sum(f)) from ("
+" select a.RiskWrapCode a,b.RiskWrapName b,'' c,0 d,a.peoples2 e,"
+" (select to_zero(prem) From lcpol Where grppolno=a.Grppolno ) f"
+" from LCRiskWrap a,LDRiskWrap b "
+" where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode and a.Grpcontno='"+mGrpContNo+"'"
+" ) as x group by a,b,c,d,e "
+" union "
+" Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
+" code in(select CalFactorValue from LobContPlanDutyParam where grpcontno='"+fm.all('GrpContNo').value+"' "
+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
+" a.peoples2, "
+" a.peoples2, "
+" to_zero((select sum(to_zero(prem)) From lobpol Where grppolno=a.Grppolno )) "
+" From lobgrppol a,LMRiskApp b "
+" Where  a.riskcode=b.riskcode   and a.GrpContNo='" + fm.all('GrpContNo').value +"' and a.riskCode not in (select riskcode from LCRiskWrap where grpcontno=a.grpcontno)"
+" union "
+" select a,b,c,d,e,f from ("
+" select a.RiskWrapCode a,b.RiskWrapName b,'' c,0 d,a.peoples2 e,"
+" (select sum(to_zero(prem)) From lobpol Where grppolno=a.Grppolno ) f"
+" from LCRiskWrap a,LDRiskWrap b "
+" where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode and a.Grpcontno='"+mGrpContNo+"'"
+" ) as x group by a,b,c,d,e,f ";

}else if(LoadFlag==16&Resource==3){
  var strSql =  " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
+" code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+fm.all('GrpContNo').value+"' "
+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
+" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"' "
+" and ContPlancode in (select distinct contplancode from lccontplanrisk where "
+" grpcontno='"+fm.GrpContNo.value+"' and riskcode=a.riskcode)), "
+" a.peoples2, "
+" (select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno ) "
+" From lcgrppol a,LMRiskApp b "
+" Where  a.riskcode=b.riskcode   and   a.grpcontno='" + fm.all('GrpContNo').value +"' and a.riskCode not in (select riskcode from LCRiskWrap where grpcontno=a.grpcontno)"
+" union "
+" select a,b,c,d,e, "                                                                                                         
+" to_zero(f) from (select a.RiskWrapCode a,b.RiskWrapName b,(select codename from ldcode where codetype='calrule' and "       
+"  code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+fm.GrpContNo.value+"' "                                      
+"  and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')) c, "                                               
+" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"'  and ContPlancode "                                      
+" in (select distinct contplancode from lccontplanrisk where  grpcontno='"+fm.GrpContNo.value+"' "                                        
+" and riskcode=a.riskcode))  d,a.peoples2 e, "                                                                                
+" (select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno ) f from LCRiskWrap a,LDRiskWrap "                          
+"  b  where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode "                                                       
+" and a.Grpcontno='"+fm.GrpContNo.value+"' ) as x group by a,b,c,d,e,f  fetch first 3000 rows only  "  

}else{
  var strSql =  " Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
+" code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+fm.all('GrpContNo').value+"' "
+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
+" a.peoples2, "
+" a.peoples2, "
+" to_zero((select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno )) "
+" From lcgrppol a,LMRiskApp b "
+" Where b.riskcode not in (select Riskcode from LCRiskWrap where Grpcontno=a.GrpContNo) and  a.riskcode=b.riskcode   and  a.grpcontno='" + fm.all('GrpContNo').value +"'  "
+" union "
+" Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and "
+" code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+fm.all('GrpContNo').value+"' "
+" and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')), "
+" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"' "
+" and ContPlancode in (select distinct contplancode from lccontplanrisk where "
+" grpcontno='"+fm.GrpContNo.value+"' and riskcode=a.riskcode)), "
+" a.peoples2, "
+" to_zero((select sum(to_zero(prem)) From lbpol Where grppolno=a.Grppolno )) "
+" From lbgrppol a,LMRiskApp b "
+" Where b.riskcode not in (select Riskcode from LCRiskWrap where Grpcontno=a.GrpContNo) and  a.riskcode=b.riskcode   and  a.grpcontno='" + fm.all('GrpContNo').value +"'  "
+" union "
+" select a,b,c,d,e, "                                                                                                         
+" to_zero(f) from (select a.RiskWrapCode a,b.RiskWrapName b,(select codename from ldcode where codetype='calrule' and "       
+"  code in(select CalFactorValue from LcContPlanDutyParam where grpcontno='"+fm.GrpContNo.value+"' "                                      
+"  and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')) c, "                                               
+" (select sum(Peoples2) from lccontplan where GrpContNo='"+fm.GrpContNo.value+"'  and ContPlancode "                                      
+" in (select distinct contplancode from lccontplanrisk where  grpcontno='"+fm.GrpContNo.value+"' "                                        
+" and riskcode=a.riskcode))  d,a.peoples2 e, "                                                                                
+" (select sum(to_zero(prem)) From lcpol Where grppolno=a.Grppolno ) f from LCRiskWrap a,LDRiskWrap "                          
+"  b  where a.RiskWrapCode = b.RiskWrapCode and a.RiskCode=b.RiskCode "                                                       
+" and a.Grpcontno='"+fm.GrpContNo.value+"' ) as x group by a,b,c,d,e,f  fetch first 3000 rows only  "  

}
                 
    turnPage.queryModal(strSql,RiskGrid);
  } else {
    return false;
  }
}

function getapproveinfo() {
//alert("hehe2");
  mOperate=1;
  var approveinfo=new Array();
  approveinfo[0]=new Array();
  approveinfo[0][0]=polNo;
  afterQuery(approveinfo);
}
/*********************************************************************
 *  选中团单问题件的录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpQuestInput() {
  var cGrpProposalContNo = fm.GrpContNo.value;  //团体保单号码
  if(cGrpProposalContNo==""||cGrpProposalContNo==null) {
    alert("请先选择一个团体投保单!");
    return ;
  }
  if(LoadFlag==2)
  var loadflag=4;
  else
  var	loadflag=3;
  window.open("../uw/GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+LoadFlag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&loadflag="+loadflag);


}
/*********************************************************************
 *  选中团单问题件的查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpQuestQuery() {
  var cGrpContNo = fm.GrpContNo.value;  //团单投保单号码
  if(cGrpContNo==""||cGrpContNo==null) {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  window.open("../uw/GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpContNo+"&ProposalNo="+cGrpContNo+"&Flag="+LoadFlag);
}
/*********************************************************************
 *  复核通过该团单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function gmanuchk() {
    //销售渠道和业务员的校验
	if(!checkSaleChnl())
	{
		return false;
	}
    //开办市县数和开办市县内容校验
//	if(!checkCity(2))
//	{
//		return false;
//	}
    if(!checkCoInsuranceParams())
    {
        return false;
    }
    if(!ChkPayIntv())
    {
        return false;
    }
    //校验被保人职业类别与职业代码
    if(!checkOccTypeOccCode())
    {
    	return false;
    }
    //校验被保人性别
    if(!CheckSex())
    {
    	return false;
    }
    //gzh date 20101210
    //判断是否有公共账户
    if(!checkGrpAcc())
    {
        return false;
    }
    //校验“公共账户缴费金额”、“个人账户个人缴费金额”、“个人账户单位缴费金额”和实际人员导入后后校验相关金额合计是否一致
    if(!checkGrpPrem()){
    	return false;
    }
    //校验管理费是否录入
    if(!checkManageFee()){
    	return false;
    }
    //校验退保费率是否录入
	if(!checkRiskZTFee()){
		return false;
	}
	//校验每个被保人是否都录入了险种信息
	if(!checkInsuredAndRisk()){
		return false;
	}
    //二期剩余功能，暂时注掉
	var qurSql="select bussno from es_issuedoc where bussno like '"+fm.PrtNo.value+"%' and status='1' and stateflag='1'";
  	var arrqurSql=easyExecSql(qurSql);
  	if(arrqurSql){
  		if (!confirm("该单还有扫描修改申请待审批，确认要继续吗？"))
        {
            return;
        }
    }
    
  cProposalGrpContNo = fm.ProposalGrpContNo.value;  //团单投保单号码
  cflag="5";

  if(MissionID == "null" || SubMissionID == "null") {
    fm.MissionID.value = mSwitch.getVar('MissionID');
    fm.SubMissionID.value = mSwitch.getVar('SubMissionID');
  } else {
    mSwitch.deleteVar("MissionID");
    mSwitch.deleteVar("SubMissionID");
    mSwitch.addVar("MissionID", "", MissionID);
    mSwitch.addVar("SubMissionID", "", SubMissionID);
    mSwitch.updateVar("MissionID", "", MissionID);
    mSwitch.updateVar("SubMissionID", "", SubMissionID);
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  }
  if( cProposalGrpContNo == null || cProposalGrpContNo == "" )
    alert("请选择团体主险投保单后，再进行复核操作");
  else {
    if (confirm("该操作将复核通过该保单号下的所有投保信息,确定吗？")) {
      var i = 0;
      //var showStr="正在复核团体投保单,系统将会在后台运行,此页面可以关闭,复合完毕后会自动进入人工核保,团体投保单号是:"+cProposalGrpContNo;
      var showStr="正在复核团体投保单,系统将会在后台运行,此页面可以关闭,复核完毕后会自动进入人工核保！";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.action="./GroupPolApproveSave.jsp?ProposalGrpContNo1="+cProposalGrpContNo+"&Flag1=5";
      fm.submit();
    } else
      return false;
  }

}
/*********************************************************************
 *  点击返回按钮,关闭当前页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
/**function goback()
{
    if (LoadFlag=='14'||LoadFlag=='23')
    {
          top.opener.querygrp();
    }
    else
    {
    	  top.opener.easyQueryClick();
    }
     top.close();
}  */

function goback() {
  /**if (LoadFlag=='14'||LoadFlag=='23')
  {
        top.opener.querygrp();
  }
  else
  {
  	  top.opener.easyQueryClick();
  }*/
  top.close();
}

/*********************************************************************
 *  复核修改该团单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function approveupdate() {

  cProposalGrpContNo = fm.ProposalGrpContNo.value;  //团单投保单号码
  cflag="5";
  if( cProposalGrpContNo == null || cProposalGrpContNo == "" )
    alert("请选择集体主险投保单后，再进行复核修改确认操作");
  else {
    if (confirm("该操作表示所有的修改已完成,确定吗？")) {
      var i = 0;
      var showStr="正在复核修改集体投保单，请您稍候并且不要修改屏幕上的值或链接其他页面,团体投保单号是:"+cProposalGrpContNo;
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      //window.open("./GroupPolApproveSave.jsp?ProposalGrpContNo="+cProposalGrpContNo+"&Flag1=5","windows1");
      fm.action="./GrpApproveModifyMakeSure.jsp?ProposalGrpContNo="+cProposalGrpContNo+"&Flag1=5";
      fm.submit();
    } else
      return false;
    //window.close();
    //fm.submit(); //提交
  }

}


/*********************************************************************
 *  团单分单定制
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function grpSubContInfo() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能进行〔分单定制〕！");
    return;
  }
  delGrpVar();
  addGrpVar();
  cGrpContNo = fm.all("GrpContNo").value;
  cPrtNo = fm.all("PrtNo").value;
  var newWindow = window.open("./SubContPolMain.jsp?GrpContNo=" + cGrpContNo + "&PrtNo=" + cPrtNo+"&LoadFlag="+LoadFlag);
}



function getdetailaddress() {
  var strSQL="select b.AddressNo,b.GrpAddress,b.GrpZipCode,b.LinkMan1,b.Department1,b.HeadShip1,b.Phone1,b.E_Mail1,b.Fax1,b.LinkMan2,b.Department2,b.HeadShip2,b.Phone2,b.E_Mail2,b.Fax2 from LCGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
  arrResult=easyExecSql(strSQL);
  try {
    fm.all('GrpAddressNo').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpAddress').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan1').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('Department1').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip1').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone1').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail1').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax1').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('LinkMan2').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Department2').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('HeadShip2').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone2').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('E_Mail2').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax2').value= arrResult[0][14];
  } catch(ex) { }
  ;
}

/*********************************************************************
 *  团体合同信息录入完毕确认
 *  参数  ：  wFlag--各状态时调用此函数所走的分支
 *  返回值：  无
 *********************************************************************
 */
function GrpInputConfirm(wFlag) {
    //销售渠道和业务员的校验
	if(!checkSaleChnl())
	{
		return false;
	}
    
    if(!checkCoInsuranceParams())
    {
        return false;
    }
    //gzh date 20101210
    //判断是否有公共账户
    if(!checkGrpAcc())
    {
        return false;
    }
    //校验“公共账户缴费金额”、“个人账户个人缴费金额”、“个人账户单位缴费金额”和实际人员导入后后校验相关金额合计是否一致
    if(!checkGrpPrem()){
    	return false;
    }
    //校验管理费是否录入
    if(!checkManageFee()){
    	return false;
    }
    //校验退保费率是否录入
	if(!checkRiskZTFee()){
		return false;
	}
	//校验每个被保人是否都录入了险种信息
	if(!checkInsuredAndRisk()){
		return false;
	}
  mWFlag = 1;
  if (wFlag ==1 ) //录入完毕确认
  {


    var tStr= "	select * from lwmission where 1=1 "
              +" and lwmission.processid = '0000000004'"
              +" and lwmission.activityid = '0000002001'"
              +" and lwmission.missionprop1 = '"+fm.ProposalGrpContNo.value+"'";
    turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
    if (turnPage.strQueryResult) {
      alert("该团单合同已经做过保存！");
      return;
    }

    var strsql = "";
    strsql="select InsuredNo from LCInsured where PrtNo ='"+fm.PrtNo.value+"'"	;
    turnPage.strQueryResult = easyQueryVer3(strsql, 1, 0, 1);
    if (!turnPage.strQueryResult) {
      alert("请添加被保人");
      return;
    }

    var strsql2 = "";
    strsql2="select PolNo from LcPol where PrtNo ='"+fm.PrtNo.value+"'"	;
    turnPage.strQueryResult = easyQueryVer3(strsql2, 1, 0, 1);
    if (!turnPage.strQueryResult) {
      alert("请添加险种信息");
      return;
    }

    if(fm.all('ProposalGrpContNo').value == "") {
      alert("团单合同信息未保存,不容许您进行 [录入完毕] 确认！");
      return;
    }
    
    // 如果是特需险种，进行公共帐户是否录入的判断。
    var tTmpGrpPrtNo = fm.PrtNo == null ? "" : fm.PrtNo.value;
    if(!checkPubAccData(tTmpGrpPrtNo))
    {
        return false;
    }
    
    var strsql3="select count(1) from LCGrpIssuePol where grpcontno in (select grpcontno from lcgrpcont where PrtNo ='"+fm.PrtNo.value+"') and state<>'5' ";
    var arr20=easyExecSql(strsql3);
 //   alert(arr20);
    if(arr20)
    {
    	if(arr20[0][0]!=0){
    	fm.all('MissionProp20').value='N';
//    	alert(fm.all('MissionProp20').value);
    	}else{
    	fm.all('MissionProp20').value='';
//    	alert(fm.all('MissionProp20').value);
			}
    }
//        return;

    if(ScanFlag=="0") {
      fm.WorkFlowFlag.value = "0000002098";
    }
    if(ScanFlag=="1") {
      fm.WorkFlowFlag.value = "0000002099";
    }
  }
  else if (wFlag ==2)//复核完毕确认
  {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出团单合同信息,不容许您进行 [复核完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000002002";					//复核完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else if (wFlag ==3) {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001002";					//复核修改完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else if(wFlag == 4) {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出合同信息,不容许您进行 [修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001021";					//问题修改
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else
    return;

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./GrpInputConfirm.jsp";
  ChangeDecodeStr();
  fm.submit(); //提交
}

/*********************************************************************
 *  初始化工作流MissionID
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initMissionID() {
  if(MissionID == "null" || SubMissionID == "null") {
    MissionID = mSwitch.getVar('MissionID');
    SubMissionID = mSwitch.getVar('SubMissionID');
  } else {
    mSwitch.deleteVar("MissionID");
    mSwitch.deleteVar("SubMissionID");
    mSwitch.addVar("MissionID", "", MissionID);
    mSwitch.addVar("SubMissionID", "", SubMissionID);
    mSwitch.updateVar("MissionID", "", MissionID);
    mSwitch.updateVar("SubMissionID", "", SubMissionID);
  }
}
function getaddresscodedata() {
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  //strsql = "select AddressNo,GrpAddress from LCGrpAddress where CustomerNo ='"+fm.GrpNo.value+"'";
  strsql = "select max(int(AddressNo)) from LCGrpAddress where CustomerNo ='"+fm.GrpNo.value+"'";
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData =turnPage.arrDataCacheSet[i][0];
    }
  }
  //alert ("tcodedata : " + tCodeData);
  //return tCodeData;
  fm.all("GrpAddressNo").value=tCodeData;
  afterCodeSelect( "GetGrpAddressNo","");
}

function grpPersonAge() {
  //showInfo = window.open("./GrpPersonAgeInput.jsp?GrpContNo="+fm.GrpContNo.value+");
  var tProposalGrpContNo = "";
  tProposalGrpContNo = fm.all( 'ProposalGrpContNo' ).value;
  if( tProposalGrpContNo == null || tProposalGrpContNo == "" ) {
    alert( "必须保存合同信息才能进入〔险种信息〕！" );
    return false
         }
         delGrpVar();
  addGrpVar();

  var cGrpContNo = fm.all("GrpContNo").value;
  var	cPrtNo = fm.all("PrtNo").value;
  showInfo = window.open("GrpPersonAgeMain.jsp?GrpContNo=" + cGrpContNo + "&PrtNo=" + cPrtNo);
}

//用于改变日期格式
function ChangDateFormate(obj,cDate) {
  if(cDate.length==8) {
    var year = cDate.substring(0,4);
    var month = cDate.substring(4,6);
    var day = cDate.substring(6,8);

    var rDate = year+"-"+month+"-"+day;
    if(isDate(rDate)) {
      return obj.value=rDate;
    }
  }
}

function GrpNameDif() {
  if(fm.all('Grpno').value!="") {
    var strSql="select * from LDGrp where Grpname='"+fm.all('Grpname').value+"' and Customerno = '"+fm.all('Grpno').value+"'";
    var arrResult = easyExecSql(strSql);
    if(!arrResult) {
      return false;
    }
  }
}
//此函数用于无名单补名单
function grpfilllist() {
  if (fm.ProposalGrpContNo.value=="") {
    alert("必须保存合同信息才能〔添加被保人〕信息！");
    return false;
  }
  //alert("1111"+fm.GrpContNo.value);
  //fm.GrpContNo.value=fm.ProposalGrpContNo.value;
  delGrpVar();
  addGrpVar();
  parent.fraInterface.window.location = "./ContInsuredInput.jsp?LoadFlag=9&ContType=2&scantype="+ scantype+"&checktype=2"+"&ScanFlag="+ScanFlag+"&oldContNo="+oldContNo;
}

//对团体告知信息进行保存
var LCImpartStr1 = "";
var LCImpartStr2 = "";
var LCImpartStr3 = "";
var ImpartCheck1 = new Array();
var ImpartCheck2 = new Array();
var ImpartCheck3 = new Array();
//by gzh
var LCImpartStr4 = "";
var ImpartCheck4 = new Array();
var LCImpartStr5 = "";
var ImpartCheck5 = new Array();
var LCImpartStr6 = "";
var ImpartCheck6 = new Array();
function LCImpartInput() {
  //告知
  ImpartGrid.clearData();
  for(var i=0;i < fm.ImpartCheck1.length;i++) {
    //判断输入框
    if(fm.ImpartCheck1[i].type=="text") {
      if(fm.ImpartCheck1[i].value=="") {
        ImpartCheck1[i] = "N,";
      } else {
        ImpartCheck1[i] = fm.ImpartCheck1[i].value + ",";
      }
    }
    //判断复选框
    if(fm.ImpartCheck1[i].type=="checkbox") {
      if(fm.ImpartCheck1[i].checked==true) {
        ImpartCheck1[i] = "Y,";
      } else {
        ImpartCheck1[i] = "N,";
      }
    }
    //各字符串相加
    LCImpartStr1 += ImpartCheck1[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr1.indexOf("Y,") != -1) {
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,1,"011");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,2,"010");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,3,"投保前参保人员医疗保障形式   □社会基本医疗保险  参加年份______　　□商业医疗保险　□单位报销  □其它______");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,4,deleteLastCharWhenComma(LCImpartStr1));
  }

  //缴费主体
  for(var i=0;i < fm.ImpartCheck2.length;i++) {
    //判断输入框
    if(fm.ImpartCheck2[i].type=="text") {
      if(fm.ImpartCheck2[i].value=="") {
        ImpartCheck2[i] = "N,";
      } else {
        ImpartCheck2[i] = fm.ImpartCheck2[i].value + ",";
      }
    }
    //判断复选框
    if(fm.ImpartCheck2[i].type=="checkbox") {
      if(fm.ImpartCheck2[i].checked==true) {
        ImpartCheck2[i] = "Y,";
      } else {
        ImpartCheck2[i] = "N,";
      }
    }
    //各字符串相加
    LCImpartStr2 += ImpartCheck2[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr2.indexOf("Y,") != -1) {
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,1,"021");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,2,"010");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,3,"□ 投保人全额承担  □被保人全额承担  □双方共同承担,其中投保人承担______％，被保人承担______％");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,4,deleteLastCharWhenComma(LCImpartStr2));
  }
  //gzh by 20101210
 //首期保险费信息
  for(var i=0;i < fm.ImpartCheck4.length;i++) {
    //判断输入框
    if(fm.ImpartCheck4[i].value=="") {
      ImpartCheck4[i] = "N,";
    } else {
      ImpartCheck4[i] = fm.ImpartCheck4[i].value + ",";
    }
    
    //各字符串相加
    LCImpartStr4 += ImpartCheck4[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr4 != "") {
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,1,"007");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,2,"010");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,3,"公共账户缴费金额______,个人账户个人缴费金额______,个人账户单位缴费金额______,首次保险费合计______");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,4,deleteLastCharWhenComma(LCImpartStr4));
  }
  //老年护理保险金领取起始日
  for(var i=0;i < fm.ImpartCheck5.length;i++) {
    //判断输入框
    if(fm.ImpartCheck5[i].type=="text") {
      if(fm.ImpartCheck5[i].value=="") {
        ImpartCheck5[i] = "N,";
      } else {
        ImpartCheck5[i] = fm.ImpartCheck5[i].value + ",";
      }
    }
    //判断复选框
    if(fm.ImpartCheck5[i].type=="checkbox") {
      if(fm.ImpartCheck5[i].checked==true) {
        ImpartCheck5[i] = "Y,";
      } else {
        ImpartCheck5[i] = "N,";
      }
    }
    //各字符串相加
    LCImpartStr5 += ImpartCheck5[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr5.indexOf("Y,") != -1) {
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,1,"008");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,2,"010");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,3,"□ 全员统一（男性______周岁，女性______周岁），□ 详见被保险人清单");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,4,deleteLastCharWhenComma(LCImpartStr5));
  }
  //年金领取方式
  for(var i=0;i < fm.ImpartCheck6.length;i++) {
     if(fm.ImpartCheck6[i].checked==true) {
       ImpartCheck6[i] = "Y,";
     } else {
       ImpartCheck6[i] = "N,";
     }
    //各字符串相加
    LCImpartStr6 += ImpartCheck6[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr6.indexOf("Y,") != -1) {
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,1,"009");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,2,"010");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,3,"□ A一次性领取，□ B按年或按月分期领取，□ C部分分期领取，□ D按年或按月分次领取，□ 详见被保险人清单");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,4,deleteLastCharWhenComma(LCImpartStr6));
  }
  //----------
  //材料渠道
  for(var i=0;i < fm.ImpartCheck3.length;i++) {
    //判断输入框
    if(fm.ImpartCheck3[i].type=="text") {
      if(fm.ImpartCheck3[i].value=="") {
        ImpartCheck3[i] = "N,";
      } else {
        ImpartCheck3[i] = fm.ImpartCheck3[i].value + ",";
      }
    }
    //判断复选框
    if(fm.ImpartCheck3[i].type=="checkbox") {
      if(fm.ImpartCheck3[i].checked==true) {
        ImpartCheck3[i] = "Y,";
      } else {
        ImpartCheck3[i] = "N,";
      }
    }
    //各字符串相加
    LCImpartStr3 += ImpartCheck3[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr3.indexOf("Y,") != -1) {
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,1,"022");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,2,"001");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,3,"材料交接：被保险人清单 □电子文档 □纸质表格______页 其它资料______,______份/页");
    ImpartGrid.setRowColData(ImpartGrid.mulLineCount-1,4,deleteLastCharWhenComma(LCImpartStr3));
  }
  ImpartGrid.delBlankLine();
}

//若最后一个字符是逗号，删除它
function deleteLastCharWhenComma(str)
{
  if(str == null || str.length == 0)
  {
    return str;
  }
  
	if(str.charAt(str.length - 1) == ",")
	{
	  str = str.substring(0, str.length - 1);  //去掉最后一个逗号
	}
	return str;
}

function getLCImpart() {
  var strSQL =  " select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '011' and ImpartCode = '010'"
  						 +" union "
  						 +" select ImpartParamModle from LbCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '011' and ImpartCode = '010'"
							 +" union "
  						 +" select ImpartParamModle from LobCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '011' and ImpartCode = '010'"
  							;
  var arrResult = easyExecSql(strSQL);
  if (arrResult != null) {
    var ImpartInfo1 = new Array();
    ImpartInfo1 = arrResult[0][0].split(",");

    if(ImpartInfo1[0]=="N") {
      fm.ImpartCheck1[0].checked = false;
    } else {
      fm.ImpartCheck1[0].checked = true;
    }

    if(ImpartInfo1[1]=="N") {
      fm.ImpartCheck1[1].value = "";
    } else {
      fm.ImpartCheck1[1].value = ImpartInfo1[1];
    }

    if(ImpartInfo1[2]=="N") {
      fm.ImpartCheck1[2].checked = false;
    } else {
      fm.ImpartCheck1[2].checked = true;
    }

    if(ImpartInfo1[3]=="N") {
      fm.ImpartCheck1[3].checked = false;
    } else {
      fm.ImpartCheck1[3].checked = true;
    }

    if(ImpartInfo1[4]=="N") {
      fm.ImpartCheck1[4].value = "";
    } else {
      fm.ImpartCheck1[4].value = ImpartInfo1[4];
    }
  }

  //缴费主体

  var strSQL2 = " select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '021' and ImpartCode = '010'"
  						 +" union "
  						 +" select ImpartParamModle from LbCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '021' and ImpartCode = '010'"
							 +" union "
  						 +" select ImpartParamModle from LobCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '021' and ImpartCode = '010'"
								;
  var arrResult2 = easyExecSql(strSQL2);
  if (arrResult2 != null) {
    var ImpartInfo2 = new Array();
    ImpartInfo2 = arrResult2[0][0].split(",");

    if(ImpartInfo2[0]=="N") {
      fm.ImpartCheck2[0].checked = false;
    } else {
      fm.ImpartCheck2[0].checked = true;
    }

    if(ImpartInfo2[1]=="N") {
      fm.ImpartCheck2[1].checked = false;
    } else {
      fm.ImpartCheck2[1].checked = true;
    }

    if(ImpartInfo2[2]=="N") {
      fm.ImpartCheck2[2].checked = false;
    } else {
      fm.ImpartCheck2[2].checked = true;
    }

    if(ImpartInfo2[3]=="N") {
      fm.ImpartCheck2[3].value = "";
    } else {
      fm.ImpartCheck2[3].value = ImpartInfo2[3];
    }

    if(ImpartInfo2[4]=="N") {
      fm.ImpartCheck2[4].value = "";
    } else {
      fm.ImpartCheck2[4].value = ImpartInfo2[4];
    }
  }
  //材料
  var strSQL3 = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '022' and ImpartCode = '001'"
							 +" union "
							 +"select ImpartParamModle from LbCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '022' and ImpartCode = '001'"
							 +" union "
							 +"select ImpartParamModle from LobCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '022' and ImpartCode = '001'"
							 ;
  var arrResult3 = easyExecSql(strSQL3);
  if (arrResult3 != null) {
    var ImpartInfo3 = new Array();
    ImpartInfo3 = arrResult3[0][0].split(",");

    if(ImpartInfo3[0]=="N") {
      fm.ImpartCheck3[0].checked = false;
    } else {
      fm.ImpartCheck3[0].checked = true;
    }

    if(ImpartInfo3[1]=="N") {
      fm.ImpartCheck3[1].checked = false;
    } else {
      fm.ImpartCheck3[1].checked = true;
    }
    if(ImpartInfo3[2]=="N") {
      fm.ImpartCheck3[2].checked = false;
    } else {
      fm.ImpartCheck3[2].value = ImpartInfo3[2];
    }
    if(ImpartInfo3[3]=="N") {
      fm.ImpartCheck3[3].value = "";
    } else {
      fm.ImpartCheck3[3].value = ImpartInfo3[3];
    }

    if(ImpartInfo3[4]=="N") {
      fm.ImpartCheck3[4].value = "";
    } else {
      fm.ImpartCheck3[4].value = ImpartInfo3[4];
    }
  }
  //date 20101210 by gzh
  //首期保险费信息
  var strSQL4 = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '007' and ImpartCode = '010'"
							 +" union "
							 +"select ImpartParamModle from LbCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '007' and ImpartCode = '010'"
							 +" union "
							 +"select ImpartParamModle from LobCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '007' and ImpartCode = '010'"
							 ;
  var arrResult4 = easyExecSql(strSQL4);
  if (arrResult4 != null) {
    var ImpartInfo4 = new Array();
    ImpartInfo4 = arrResult4[0][0].split(",");

    if(ImpartInfo4[0]=="N") {
      fm.ImpartCheck4[0].value = "";
    } else {
      fm.ImpartCheck4[0].value = ImpartInfo4[0];
    }

    if(ImpartInfo4[1]=="N") {
      fm.ImpartCheck4[1].value = "";
    } else {
      fm.ImpartCheck4[1].value = ImpartInfo4[1];
    }
    if(ImpartInfo4[2]=="N") {
      fm.ImpartCheck4[2].checked = "";
    } else {
      fm.ImpartCheck4[2].value = ImpartInfo4[2];
    }
    if(ImpartInfo4[3]=="N") {
      fm.ImpartCheck4[3].value = "";
    } else {
      fm.ImpartCheck4[3].value = ImpartInfo4[3];
    }
  }
  //老年护理保险金领取起始日
  var strSQL5 = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '008' and ImpartCode = '010'"
							 +" union "
							 +"select ImpartParamModle from LbCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '008' and ImpartCode = '010'"
							 +" union "
							 +"select ImpartParamModle from LobCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '008' and ImpartCode = '010'"
							 ;
  var arrResult5 = easyExecSql(strSQL5);
  if (arrResult5 != null) {
    var ImpartInfo5 = new Array();
    ImpartInfo5 = arrResult5[0][0].split(",");

    if(ImpartInfo5[0]=="N") {
      fm.ImpartCheck5[0].checked = false;
    } else {
      fm.ImpartCheck5[0].checked = true;
    }

    if(ImpartInfo5[1]=="N") {
      fm.ImpartCheck5[1].value = "";
    } else {
      fm.ImpartCheck5[1].value = ImpartInfo5[1];
    }
    if(ImpartInfo5[2]=="N") {
      fm.ImpartCheck5[2].checked = "";
    } else {
      fm.ImpartCheck5[2].value = ImpartInfo5[2];
    }
    if(ImpartInfo5[3]=="N") {
      fm.ImpartCheck5[3].checked = false;
    } else {
      fm.ImpartCheck5[3].checked = true;
    }
  }
  //年金领取方式
  var strSQL6 = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '009' and ImpartCode = '010'"
							 +" union "
							 +"select ImpartParamModle from LbCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '009' and ImpartCode = '010'"
							 +" union "
							 +"select ImpartParamModle from LobCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '009' and ImpartCode = '010'"
							 ;
  var arrResult6 = easyExecSql(strSQL6);
  if (arrResult6 != null) {
    var ImpartInfo6 = new Array();
    ImpartInfo6 = arrResult6[0][0].split(",");

    if(ImpartInfo6[0]=="N") {
      fm.ImpartCheck6[0].checked = false;
    } else {
      fm.ImpartCheck6[0].checked = true;
    }
    if(ImpartInfo6[1]=="N") {
      fm.ImpartCheck6[1].checked = false;
    } else {
      fm.ImpartCheck6[1].checked = true;
    }
    if(ImpartInfo6[2]=="N") {
      fm.ImpartCheck6[2].checked = false;
    } else {
      fm.ImpartCheck6[2].checked = true;
    }
    if(ImpartInfo6[3]=="N") {
      fm.ImpartCheck6[3].checked = false;
    } else {
      fm.ImpartCheck6[3].checked = true;
    }
    if(ImpartInfo6[4]=="N") {
      fm.ImpartCheck6[4].checked = false;
    } else {
      fm.ImpartCheck6[4].checked = true;
    }
  }
}
//获取各类人员数量
function getCountsOfPeopleByType(){
	var notPubSql = " and not exists (select 1 from lccont where contno=lcinsured.contno and poltype='2')"
	var Countsql = "select count(1) from lcinsured where grpcontno='"+fm.all( 'GrpContNo' ).value+"' and occupationtype = '1'";
	Countsql = Countsql + notPubSql;
	var arrCount = easyExecSql(Countsql);
	fm.OneType.value = arrCount[0][0];
	if(fm.OneType.value == "" || fm.OneType.value == null)
	{
		fm.OneType.value = "0";
	}
	Countsql = "select count(1) from lcinsured where grpcontno='"+fm.all( 'GrpContNo' ).value+"' and occupationtype = '2'";
	Countsql = Countsql + notPubSql;
	arrCount = easyExecSql(Countsql);
	fm.TwoType.value = arrCount[0][0];
	if(fm.TwoType.value == "" || fm.TwoType.value == null)
	{
		fm.TwoType.value = "0";
	}
	Countsql = "select count(1) from lcinsured where grpcontno='"+fm.all( 'GrpContNo' ).value+"' and occupationtype = '3'";
	Countsql = Countsql + notPubSql;
	arrCount = easyExecSql(Countsql);
	fm.ThreeType.value = arrCount[0][0];
	if(fm.ThreeType.value == "" || fm.ThreeType.value == null)
	{
		fm.ThreeType.value = "0";
	}
	Countsql = "select count(1) from lcinsured where grpcontno='"+fm.all( 'GrpContNo' ).value+"' and occupationtype = '4'";
	Countsql = Countsql + notPubSql;
	arrCount = easyExecSql(Countsql);
	fm.FourType.value = arrCount[0][0];
	if(fm.FourType.value == "" || fm.FourType.value == null)
	{
		fm.FourType.value = "0";
	}
	Countsql = "select count(1) from lcinsured where grpcontno='"+fm.all( 'GrpContNo' ).value+"' and occupationtype = '5'";
	Countsql = Countsql + notPubSql;
	arrCount = easyExecSql(Countsql);
	fm.FiveType.value = arrCount[0][0];
	if(fm.FiveType.value == "" || fm.FiveType.value == null)
	{
		fm.FiveType.value = "0";
	}
	Countsql = "select count(1) from lcinsured where grpcontno='"+fm.all( 'GrpContNo' ).value+"' and occupationtype = '6'";
	Countsql = Countsql + notPubSql;
	arrCount = easyExecSql(Countsql);
	fm.SixType.value = arrCount[0][0];
	if(fm.SixType.value == "" || fm.SixType.value == null)
	{
		fm.SixType.value = "0";
	}
}

//缴费主体的单选效果
function ImpartCheck2Radio1() {
  if(fm.ImpartCheck2[0].checked == true) {
    fm.ImpartCheck2[1].checked = false;
    fm.ImpartCheck2[2].checked = false;
    fm.ImpartCheck2[3].value = "";
    fm.ImpartCheck2[4].value = "";
  }
}

function ImpartCheck2Radio2() {
  if(fm.ImpartCheck2[1].checked == true) {
    fm.ImpartCheck2[0].checked = false;
    fm.ImpartCheck2[2].checked = false;
    fm.ImpartCheck2[3].value = "";
    fm.ImpartCheck2[4].value = "";
  }
}

function ImpartCheck2Radio3() {
  if(fm.ImpartCheck2[2].checked == true) {
    fm.ImpartCheck2[0].checked = false;
    fm.ImpartCheck2[1].checked = false;
    fm.ImpartCheck2[3].value = "";
    fm.ImpartCheck2[4].value = "";
  }
}

  //gzh date 20101210
  //老年护理保险金领取起始日
function ImpartCheck5Radio1() {
  if(fm.ImpartCheck5[0].checked == true) {
    fm.ImpartCheck5[1].value = "";
    fm.ImpartCheck5[2].value = "";
    fm.ImpartCheck5[3].checked = false;
  }
}

function ImpartCheck5Radio2() {
  if(fm.ImpartCheck5[3].checked == true) {
    fm.ImpartCheck5[0].checked = false;
    fm.ImpartCheck5[1].value = "";
    fm.ImpartCheck5[2].value = "";
  }
}
//年金领取方式
function ImpartCheck6Radio1() {
  if(fm.ImpartCheck6[0].checked == true) {
  	fm.ImpartCheck6[1].checked = false;
  	fm.ImpartCheck6[2].checked = false;
  	fm.ImpartCheck6[3].checked = false;
    fm.ImpartCheck6[4].checked = false;
  }
}
function ImpartCheck6Radio2() {
  if(fm.ImpartCheck6[1].checked == true) {
  	fm.ImpartCheck6[0].checked = false;
  	fm.ImpartCheck6[2].checked = false;
  	fm.ImpartCheck6[3].checked = false;
    fm.ImpartCheck6[4].checked = false;
  }
}
function ImpartCheck6Radio3() {
  if(fm.ImpartCheck6[2].checked == true) {
  	fm.ImpartCheck6[0].checked = false;
  	fm.ImpartCheck6[1].checked = false;
  	fm.ImpartCheck6[3].checked = false;
    fm.ImpartCheck6[4].checked = false;
  }
}
function ImpartCheck6Radio4() {
  if(fm.ImpartCheck6[3].checked == true) {
  	fm.ImpartCheck6[0].checked = false;
  	fm.ImpartCheck6[1].checked = false;
  	fm.ImpartCheck6[2].checked = false;
    fm.ImpartCheck6[4].checked = false;
  }
}
function ImpartCheck6Radio5() {
  if(fm.ImpartCheck6[4].checked == true) {
  	fm.ImpartCheck6[0].checked = false;
  	fm.ImpartCheck6[1].checked = false;
  	fm.ImpartCheck6[2].checked = false;
    fm.ImpartCheck6[3].checked = false;
  }
}
//告知的初始化
function ImpartClear() {
  try {
    fm.ImpartCheck1[0].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck1[1].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck1[2].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck1[3].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck1[4].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[0].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[1].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[2].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[3].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck2[4].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[0].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[1].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[2].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[3].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck3[4].value="";
  } catch(ex) { }
  ;
  // by gzh date 20101210
  try {
    fm.ImpartCheck4[0].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck4[1].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck4[2].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck4[3].value="";
  } catch(ex) { }
  ;
   try {
    fm.ImpartCheck5[0].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck5[1].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck5[2].value="";
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck5[3].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck6[0].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck6[1].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck6[2].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck6[3].checked=false;
  } catch(ex) { }
  ;
  try {
    fm.ImpartCheck6[4].checked=false;
  } catch(ex) { }
  ;
}
function ChkMulLineCount()
{
	if(BookingPayIntyGrid.mulLineCount==7)
	{
		alert("约定缴费方式只能录入六次");
		BookingPayIntyGrid.delBlankLine();
		return ;
	}
}
function displayBookingPay()
{
	if(fm.GrpContPayIntv.value==-1)
	{
		divBookingPayInty.style.display='';
		var strSql = "select PayToDate, PayToAmount from LCGrpSpecFee where GrpContNo='" + fm.all('GrpContNo').value + "' order by PeriodNo";
		turnPage.queryModal(strSql,BookingPayIntyGrid);
	}
}
//添加缴费频次的校验
function ChkPayIntv()
{
	var tPayInv = fm.GrpContPayIntv.value;
	
	for (m=0; m<RiskGrid.mulLineCount; m++)
	{
	    var tRiskCode = RiskGrid.getRowColData(m,1);
	    var strSQL = "select 1 from LMRiskPayIntv where riskcode = '"+tRiskCode+"'";
	    var arrResult = easyExecSql(strSQL);
	    if(arrResult != null)
	    {
	        var strSql = "select 1 from LMRiskPayIntv where "
							+" riskcode ='"+tRiskCode+"'"
							+" and PayIntv='"+tPayInv+"'";
			  
			var arr = easyExecSql(strSql);
			if(!arr)
			{
				alert("险种"+tRiskCode+"的缴费频次不支持"+fm.GrpContPayIntvName.value+",请修改页面中的缴费频次并且保存至数据库.");
				return false;
			}
	    }
	}
	return true;
}
//定义公共账户
function grpPubAccInput(RiskCode,GrpPolNo)
{
	var ChkCount = 0;
	var tRow;
	if(RiskCode==null)
	{
		for(var row=0;row<RiskGrid.mulLineCount;row++)
		{
			var ChkNo = RiskGrid.getChkNo(row);
			if(ChkNo)
			{
				ChkCount++;
				tRow=row;
			}
		}
		if(ChkCount<=0||ChkCount>1)
		{
			alert("请选择一条险种信息！");
			return;
		}
		RiskCode = RiskGrid.getRowColData(tRow,1);
		var strSql = "select grppolno from lcgrppol where GrpContNo='"
		+fm.all('GrpContNo').value+"' and RiskCode ='"+RiskCode+"'";
		var arr = easyExecSql(strSql);
		if(arr)
		{
			GrpPolNo=arr[0][0];
		}
	}
	var showInfo = window.open("./PublicAccMain.jsp?GrpContNo="+fm.all('GrpContNo').value+"&RiskCode="+RiskCode+"&GrpPolNo="+GrpPolNo,"PublicAccMain");
}

//缴费主体的共担校验
function CheckImpart()
{
  if(fm.ImpartCheck2[2].checked==true)
  {
    if(fm.ImpartCheck2[3].value=="" || fm.ImpartCheck2[4].value=="")
    {
    	alert("交费主体,双方共同承担,投被保人承担比例不能为空");
    		LCImpartStr1 = "";
			LCImpartStr2 = "";
			LCImpartStr3 = "";
			ImpartCheck1 = new Array();
			ImpartCheck2 = new Array();
			ImpartCheck3 = new Array();
			//by gzh
			LCImpartStr4 = "";
			ImpartCheck4 = new Array();
			LCImpartStr5 = "";
			ImpartCheck5 = new Array();
			LCImpartStr6 = "";
			ImpartCheck6 = new Array();
      return false;
    }
   if(parseInt(fm.ImpartCheck2[3].value)==100 || parseInt(fm.ImpartCheck2[4].value)==100)
   {
   	alert("交费主体,双方共同承担,投被保人承担百分比不能一方为100%,请检查!");
   		LCImpartStr1 = "";
		LCImpartStr2 = "";
		LCImpartStr3 = "";
		ImpartCheck1 = new Array();
		ImpartCheck2 = new Array();
		ImpartCheck3 = new Array();
		//by gzh
		LCImpartStr4 = "";
		ImpartCheck4 = new Array();
		LCImpartStr5 = "";
		ImpartCheck5 = new Array();
		LCImpartStr6 = "";
		ImpartCheck6 = new Array();
     return false;
   }

   if((parseInt(fm.ImpartCheck2[3].value)+parseInt(fm.ImpartCheck2[4].value))!=100)
   {
   	alert("交费主体,双方共同承担,投被保人承担百分比合计不为100%,请检查!");
   	LCImpartStr1 = "";
		LCImpartStr2 = "";
		LCImpartStr3 = "";
		ImpartCheck1 = new Array();
		ImpartCheck2 = new Array();
		ImpartCheck3 = new Array();
		//by gzh
		LCImpartStr4 = "";
		ImpartCheck4 = new Array();
		LCImpartStr5 = "";
		ImpartCheck5 = new Array();
		LCImpartStr6 = "";
		ImpartCheck6 = new Array();
     return false;
   }
  }
}


function ChkRiskVer()
{
	var tRiskCode = fm.all('RiskCode').value;
	var strSql = "select enddate from LMRiskApp where "
							+" riskcode ='"+tRiskCode+"'";
	var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
      if((fm.all('CValiDate').value)>arrResult[0][0] &&  arrResult[0][0]!='')
      {
      	alert("该产品已经停售!");
      	return false;
      }
    else
    {
      return true;
    }
  }
	return true;
}
function CheckDateDollar()
{
	var linecount = DiseaseGrid.mulLineCount;
	for( var i=0; i<linecount;i++)
	{
		var iarray=DiseaseGrid.getRowColData(i,1);
		//if(!isDate(iarray))
		//{
		//	alert("日期格式有误!正确格式为'yyyy-mm-dd'");
		//	return false;
		//}
		iarray=DiseaseGrid.getRowColData(i,4);
		if(!isNumeric(iarray))
		{
			alert("医疗费用金额只能为数字");
			return false;
		}
		iarray=DiseaseGrid.getRowColData(i,3);
		if(!isInteger(iarray))
		{
			alert("患病人数必须为整数!");
		 	return false;
		}
	}
	var historylinecount =HistoryImpartGrid.mulLineCount;
	for( var i=0;i<historylinecount;i++)
	{
		var iarray =HistoryImpartGrid.getRowColData(i,4);
		if(!isNumeric(iarray))
		{
			alert("保险费只能为数字");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,6);
		if(!isInteger(iarray))
		{
			alert("参加人数必须为整数");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,7);
		if(!isInteger(iarray))
		{
			alert("参加人数必须为整数");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,8);
		if(!isNumeric(iarray))
		{
			alert("发生金额必须为数字!");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,9);
		if(!isNumeric(iarray))
		{
			alert("报销金额只能为数字!");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,10);
		if(!isNumeric(iarray))
		{
			alert("未决金额必须为数字!");
			return false;
		}
	}
}
function BigProjectApprove(){
    //销售渠道和业务员的校验
	if(!checkSaleChnl())
	{
		return false;
	}
    
    if(!checkCoInsuranceParams())
    {
        return false;
    }
    
	if(fm.GrpContNo.value=="" || fm.GrpContNo.value==null){
		alert("团体合同号码为null");
		return;
	}
	 //gzh date 20101210
    //判断是否有公共账户
    if(!checkGrpAcc())
    {
        return false;
    }
    //校验“公共账户缴费金额”、“个人账户个人缴费金额”、“个人账户单位缴费金额”和实际人员导入后后校验相关金额合计是否一致
    if(!checkGrpPrem()){
    	return false;
    }
    //校验管理费是否录入
    if(!checkManageFee()){
    	return false;
    }
    //校验退保费率是否录入
	if(!checkRiskZTFee()){
		return false;
	}
	//校验每个被保人是否都录入了险种信息
	if(!checkInsuredAndRisk()){
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./BigProjectApproveSave.jsp";
	fm.fmAction.value = "INSERT||BIGPROJCET";
	fm.submit();
}


//定义建工意外要素信息
function initContractor(){

	var strSql = "select count(*) from lcgrppol where GrpContNo='"+GrpContNo+"' and riskcode in (select riskcode from LMRiskApp where risktype8='4') ";
	var arr = easyExecSql(strSql);

	if(arr){
		if(arr[0][0]<0){
			alert("保单没有建工险产品不需要定义要素信息！");
			return;
		}
	}else{
		alert("保单没有建工险产品不需要定义要素信息！");
		return;
	}
	//alert(LoadFlag);
	var showInfo = window.open("./TbGrpContractorMain.jsp?GrpContNo="+fm.all('GrpContNo').value+"&LoadFlag="+LoadFlag,"PublicAccMain");

}

/**
 * 判断特需险种是否已经录入了帐户信息。
 */
function checkPubAccData(tGrpPrtNo)
{
    var tBResult = true;
    var tStrSql = " select distinct lgp.grppolno, lgp.riskcode "
        + " from lcgrppol lgp "
        + " where prtno = '" + tGrpPrtNo + "' "
        + " and exists (select 1 from LMRisktoAcc lmrta where lmrta.riskcode = lgp.riskcode) "
        + " and not exists (select 1 from LCGrpFee lgf where lgf.riskcode = lgp.riskcode and lgf.grppolno = lgp.grppolno) ";
    var arr = easyExecSql(tStrSql);
    if(arr)
    {
        var tComment = "";
        for(index in arr)
        {
            if(arr[index] != "")
            {
                tComment += "[" + arr[index][1] + "]";
            }
        }
        tComment += "并未添加相应帐户信息。";
        alert(tComment);
        tBResult = false; 
    }
    return tBResult;
}

//若保障期不为整年，则保单必须是趸交，整年判断标准如下
//1、若起始日期和终止日期的下一天 的月、日分别相等则为整年
//2、闰月需要特殊处理，闰月判断标准如下:
//起始日期  终止日期  终止日期+1天  是否整年
//（闰月开始）：
//2008-2-28	2009-2-27	2009-2-28	    是    同规则1
//2008-2-29	2009-2-28	2009-3-1	    是
//2008-3-1	2009-2-28	2009-3-1	    是    同规则1
//（闰月结束）：
//2007-2-28	2008-2-27	2008-2-28	    是    同规则1
//2007-3-1	2008-2-29	2008-3-1	    是    同规则1
//2007-3-1	2008-2-28	2008-2-29	    不是  同规则1
//所以闰月只需要处理以2月29日起始的情况
function checkInsurePeriod()
{
  //趸交不做判断
  if(fm.GrpContPayIntv.value == "0")
  {
    return true;
  }
  
  var tCValiDate = toDate(fm.CValiDate.value);
  var tCInValiDate = toDate(fm.CInValiDate.value);
  if(tCValiDate == null || tCInValiDate == null)
  {
    alert("保险责任终起止日期格式不正确");
  }
  
  //求终止日期的下一天
  var tCInvaliDateAddOne = easyExecSql("select date('" + fm.CInValiDate.value + "') + 1 days from dual ");
  if(tCInvaliDateAddOne == null || tCInvaliDateAddOne == "" || tCInvaliDateAddOne == "null")
  {
    alert("保险责任终止日期格式有误");
    return false;
  }
  tCInvaliDateAddOne = toDate(tCInvaliDateAddOne);
  
  //规则1
  if(tCValiDate.getMonth() == tCInvaliDateAddOne.getMonth() && tCValiDate.getDate() == tCInvaliDateAddOne.getDate())
  {
    return true;
  }
  
  //规则2
  if(tCValiDate.getMonth() == tCInValiDate.getMonth() && tCValiDate.getMonth() == 1
    && tCValiDate.getDate() == 29 && tCInValiDate.getDate() == 28)
  {
    return true;
  }
  
//  alert("若保单保障期不为整年，则保单应该是趸交。");
  
  return true;
}

//由字符串生成日期
function toDate(str)
{
    var pattern = /^(\d{4})(-)(0?[1-9]|1[0-2])(-)(0?[1-9]|[12][0-9]|3[01])$/g; 
    var arr = pattern.exec(str);
    if (arr == null)
    {
      return null;
    }
    var date = new Date(arr[1], arr[3]-1, arr[5]);
    return date;
}

//销售渠道和业务员的校验
function checkSaleChnl()
{
	var saleChnl = fm.SaleChnl.value;
	var agentCode = fm.AgentCode.value;

	var check = easyExecSql("select 1 from LAAGENT a, LDCode1 b where CodeType = 'salechnl' and Code = '" 
	    + saleChnl + "' and AGENTCODE = '" + agentCode + "' AND BranchType = Code1 and BranchType2 = CodeName");
	if(!check)
	{
        //alert("销售渠道和业务员不相符!");
        //return false;
        //去除销售渠道和业务员不相符的阻断校验，改为提示    2008-9-28
        return confirm("销售渠道和业务员不相符！确认保存？");
	}
	return true;
}

/**
 * 校验共保保单的相关要素是否齐全。
 */
function checkCoInsuranceParams()
{
    var tStrSql = ""
        + " select 1 "
        + " from LCGrpCont lgc "
        + " left join LCIGrpCont lcigc on lgc.GrpContNo = lcigc.GrpContNo "
        + " left join LCCoInsuranceParam lcip on lcip.GrpContNo = lcigc.GrpContNo "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" + mGrpContNo + "' "
        + " and lgc.CoInsuranceFlag = '1' "
        + " and lcigc.GrpContNo is null "
        + " and lcip.GrpContNo is null "
        ;
    var arr = easyExecSql(tStrSql);
    if(arr)
    {
        alert("该单为共保保单，但共保要素信息不全，请进行确认。");
        return false;
    }
    
    return true;
}

/**
 * 显示共保险种录入要素界面。
 */
function showCoInsuranceParamWin()
{
    //此处仍调用app文件夹中的文件
    var tUrl = ""
        + " ../app/CoInsuranceParamMain.jsp"
        + "?GrpContNo=" + fm.all('GrpContNo').value 
        + "&LoadFlag=" + LoadFlag
        ;
    var showInfo = window.open(tUrl, "PublicAccMain");
}

/**
 * 启用/禁用“共保要素录入”按钮。
 */
function initCoInsuranceParamInput()
{
    var flag = fm.CoInsuranceFlag.value;
    
    if(flag == "1")
    {
        fm.btnCoInsuranceParam.disabled = false;
    }
    else
    {
        fm.btnCoInsuranceParam.disabled = true;
    }
}

//开办市县数和开办市县内容校验
function checkCity(flag)
{
//    return true;//临时取消校验。
//    if(fm.MarketType.value >= 2 && fm.MarketType.value <= 99)
//    {
//        if(fm.CTCount.value == null || fm.CTCount.value == "" || fm.CityInfo.value == null || fm.CityInfo.value == "")
//        {
//            alert("市场类型选择2-99时，开办市县数和开办市县内容必须录入！");
//            return false;
//        }
//    }
//    
//    //保存不校验数据库值
//    if(flag == 1) return true;
//    
//    var citySQL = "select 1 from LCGrpCont where PrtNo = '"
//    + fm.PrtNo.value + "' and MarketType between '2' and '99' "
//    + "and (CTCount is null or CityInfo is null or CityInfo = '')";
//    var check = easyExecSql(citySQL);
//    if(check)
//    {
//        alert("市场类型选择2-99时，开办市县数及开办市县内容必须录入！");
//        return false;
//    }
    return true;
}


/**
 * 集团交叉要素校验
 */
function checkCrsBussParams()
{
    var tCrs_SaleChnl = trim(fm.Crs_SaleChnl.value);
    var tCrs_BussType = trim(fm.Crs_BussType.value);
    
    var tGrpAgentCom = trim(fm.GrpAgentCom.value);
    var tGrpAgentCode = trim(fm.GrpAgentCode.value);
    var tGrpAgentName = trim(fm.GrpAgentName.value);
    var tGrpAgentIDNo = trim(fm.GrpAgentIDNo.value);
    
    if(tCrs_SaleChnl != null && tCrs_SaleChnl != "")
    {
        if(tCrs_BussType == null || tCrs_BussType == "")
        {
            alert("选择集团交叉渠道时，集团交叉业务类型不能为空。");
            return false;
        }
        if(tGrpAgentCom == null || tGrpAgentCom == "")
        {
            alert("选择集团交叉渠道时，对方业务员机构不能为空。");
            return false;
        }
        if(tGrpAgentCode == null || tGrpAgentCode == "")
        {
            alert("选择集团交叉渠道时，对方业务员代码不能为空。");
            return false;
        }
        if(tGrpAgentName == null || tGrpAgentName == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名不能为空。");
            return false;
        }
        if(tGrpAgentIDNo == null || tGrpAgentIDNo == "")
        {
            alert("选择集团交叉渠道时，对方业务员身份证不能为空。");
            return false;
        }
    }
    else
    {
        if(tCrs_BussType != null && tCrs_BussType != "")
        {
            alert("未选择集团交叉渠道时，集团交叉业务类型不能填写。");
            return false;
        }
        if(tGrpAgentCom != null && tGrpAgentCom != "")
        {
            alert("未选择集团交叉渠道时，对方业务员机构不能为填写。");
            return false;
        }
        if(tGrpAgentCode != null && tGrpAgentCode != "")
        {
            alert("未选择集团交叉渠道时，对方业务员代码不能为填写。");
            return false;
        }
        if(tGrpAgentName != null && tGrpAgentName != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名不能为填写。");
            return false;
        }
        if(tGrpAgentIDNo != null && tGrpAgentIDNo != "")
        {
            alert("未选择集团交叉渠道时，对方业务员身份证不能为填写。");
            return false;
        }
    }
    
    return true;
}

//显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value = "";
        fm.all('Crs_SaleChnlName').value = "";
        fm.all('Crs_BussType').value = "";
        fm.all('Crs_BussTypeName').value = "";
        fm.all('GrpAgentCom').value = "";
        fm.all('GrpAgentComName').value = "";
        fm.all('GrpAgentCode').value = "";
        fm.all('GrpAgentName').value = "";
        fm.all('GrpAgentIDNo').value = "";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}

//根据对方机构代码带出对方机构名称
function GetGrpAgentName()
{
	var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     fm.GrpAgentComName.value = "";
	}
}  
//根据机构代码显示机构名称
function getAgentName()
{
    var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     alert("对方机构代码有错误,请修改");
	     fm.GrpAgentCom.value = "";
	     return false;
	}
}

/**
 * 校验生效日期填写规范。
 * 规则：生效日期 在 录入日期（判断录入当天） 之前时，返回false；否则，返回true；
 */
function chkValidate()
{
    var tValidate = fm.CValiDate.value;
    var tCurDate = getCurrentDate();
    
    var tMarketType = fm.MarketType.value;
    
    if(tMarketType == null || tMarketType == "" || tMarketType == "1" || tMarketType == "99")
    {
        if(dateDiff(tCurDate, tValidate, "D") < 0)
        {
            return false;
        }
    }
    return true;
}

//校验被保人职业类别与职业代码
function checkOccTypeOccCode()
{
	var sql = "select 1 from lcpol where poltypeflag = '0' and prtno = '" + fm.PrtNo.value + "'";
	var result = easyExecSql(sql);
	if(result != null)
	{
		var strSql = "select distinct OccupationType,OccupationCode from lcinsured "
					+ " where prtno = '" + fm.PrtNo.value + "'";
		var arrResult = easyExecSql(strSql);
		var i = 0;
		
		if(arrResult != null) 
		{
			for(i = 0; i < arrResult.length; i++)
			{
				var OccupationType = arrResult[i][0];
				var OccupationCode = arrResult[i][1];
				
				if(OccupationType != null && OccupationType != "")
				{
					if(!CheckOccupationType(OccupationType))
					{
						return false;
					}
				}
				
				if(OccupationCode != null && OccupationCode != "")
				{
					
					if(!CheckOccupationCode(OccupationType,OccupationCode))
					{
						return false;
					}
					
				}
			}
		}
	}
	return true;
}

//校验职业代码
function CheckOccupationCode(Type,Code)
{
	var strSql = "select 1 from ldoccupation where OccupationCode = '" + Code +"' and OccupationType = '" + Type + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别和职业代码与系统描述不一致，请查看！");
		return false
	} 
	return true;
}

//校验职业类别
function CheckOccupationType(Type)
{
	var strSql = "select 1 from dual where '" + Type + "' in (select distinct occupationtype from ldoccupation)";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6");
		return false
	} 
	return true;
}

//校验被保人性别
function CheckSex()
{
	var strSql = "select distinct Sex from lcinsured where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSql);
	var i = 0;
	if(arrResult != null)
	{
		for(i = 0; i < arrResult.length; i++)
		{
			var sex = arrResult[i][0];
			if(sex != "0" && sex != "1" && sex != "2")
			{
				alert("被保人的性别填写有误，请查看。\n性别必须是半角数字的0、1、2 ，规则是0-男，1-女，2-其他");
				return false;
			}
		}
	}
	return true;
}

//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryGrpAgent()
{
    if(fm.all('GrpAgentCode').value == "" && fm.all('GrpAgentIDNo').value == "" )
    {  
        var tGrpAgentCom = (fm.GrpAgentCom != null && fm.GrpAgentCom != "undefined") ? fm.GrpAgentCom.value : "";
        var tGrpAgentName = (fm.GrpAgentName != null && fm.GrpAgentName != "undefined") ? fm.GrpAgentName.value : "";
        var tGrpAgentIDNo = (fm.GrpAgentIDNo != null && fm.GrpAgentIDNo != "undefined") ? fm.GrpAgentIDNo.value : "";
        var strURL = "../sys/GrpAgentCommonQueryMain.jsp?GrpAgentCom=" + tGrpAgentCom +
                     "&GrpAgentName=" + tGrpAgentName + "&GrpAgentIDNo=" + tGrpAgentIDNo;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCode').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Sales_Cod='" + fm.all('GrpAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.GrpAgentCode.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }else if(fm.all('GrpAgentIDNo').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Id_No='" + fm.all('GrpAgentIDNo').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("身份证号码为:[" +  fm.GrpAgentIDNo.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery3(arrResult)
{  
  if(arrResult!=null)
    {  	
  	fm.GrpAgentCode.value = arrResult[0][0];
  	fm.GrpAgentName.value = arrResult[0][1];
    fm.GrpAgentIDNo.value = arrResult[0][2];
  }
}


//执行查询交叉销售对方机构代码 date 20101117 by gzh
function queryGrpAgentCom()
{
	if(fm.all('Crs_SaleChnl').value == "" || fm.all('Crs_SaleChnl').value == null)
	{
		alert("请先选择交叉销售渠道！！");
		return false;
	}
    if(fm.all('GrpAgentCom').value == "")
    {  
        var tCrs_SaleChnl =  fm.Crs_SaleChnl.value;
        var tCrs_SaleChnlName = fm.Crs_SaleChnlName.value;
        var strURL = "../sys/GrpAgentComQueryMain.jsp?Crs_SaleChnl="+tCrs_SaleChnl+"&Crs_SaleChnlName="+tCrs_SaleChnlName;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentComQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCom').value != "")
    {	
        var strGrpSql = "select grpagentcom,Under_Orgname  from lomixcom  where  grpagentcom ='"+fm.GrpAgentCom.value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery4(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("机构代码为:[" +  fm.GrpAgentCom.value + "]的机构不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery4(arrResult)
{  
  if(arrResult!=null)
    { 	
  	fm.GrpAgentCom.value = arrResult[0][0];
  	fm.GrpAgentComName.value = arrResult[0][1];
  }
}

//选择交叉销售后对交叉销售各不为空项的校验
    // by gzh 20101118
function MixComCheck()
{    
    if(fm.MixComFlag.checked == true)
    {
    	if(fm.Crs_SaleChnl.value == "" || fm.Crs_SaleChnl.value == null)
    	{
    		alert("选择交叉销售时，交叉销售渠道不能为空，请核查！");
    		fm.all('Crs_SaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.Crs_BussType.value == "" || fm.Crs_BussType.value == null)
    	{
    		alert("选择交叉销售时，交叉销售业务类型不能为空，请核查！");
    		fm.all('Crs_BussType').focus();
    		return false;
    	}
    	if(fm.GrpAgentCom.value == "" || fm.GrpAgentCom.value == null)
    	{
    		alert("选择交叉销售时，对方机构代码不能为空，请核查！");
    		fm.all('GrpAgentCom').focus();
    		return false;
    	}
    	/**
    	if(fm.GrpAgentComName.value == "" || fm.GrpAgentComName.value == null)
    	{
    		alert("选择交叉销售时，对方机构名称不能为空，请核查！");
    		fm.all('GrpAgentComName').focus();
    		return false;
    	}
    	*/
    	if(fm.GrpAgentCode.value == "" || fm.GrpAgentCode.value == null)
    	{
    		alert("选择交叉销售时，对方业务员代码不能为空，请核查！");
    		fm.all('GrpAgentCode').focus();
    		return false;
    	}
    	if(fm.GrpAgentName.value == "" || fm.GrpAgentName.value == null)
    	{
    		alert("选择交叉销售时，对方业务员姓名不能为空，请核查！");
    		fm.all('GrpAgentName').focus();
    		return false;
    	}
    	if(fm.GrpAgentIDNo.value == "" || fm.GrpAgentIDNo.value == null)
    	{
    		alert("选择交叉销售时，对方业务员身份证号码不能为空，请核查！");
    		fm.all('GrpAgentIDNo').focus();
    		return false;
    	}
      	return true;
    }else{
    	return true;
    }
}
//date 20101202 by gzh
//管理费定义
function GrpRiskFeeInfo(){
    //alert("合同号："+fm.all('GrpContNo').value);
    delGrpVar();
	addGrpVar(); 
	 if (LoadFlag=="99")
	{
	    parent.fraInterface.window.location ="./RiskFeeInput.jsp?GrpContNo=" + fm.all('GrpContNo').value+"&ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype;
	    return;
	}
	parent.fraInterface.window.location = "./RiskFeeInput.jsp?GrpContNo=" + fm.all('GrpContNo').value+"&ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype+"&cContType="+cContType;
}
// 退保费利率
function ChangExtrateRate()
{
	   delGrpVar();
	   addGrpVar(); 
	//alert("aaa");
 	//var cgrpContNo = fm.GrpContNo.value;
	var cGrpContNo = fm.all("GrpContNo").value;
		 if (LoadFlag=="99")
	{
	    parent.fraInterface.window.location="./ChangeExtractRate.jsp?GrpContNo=" + cGrpContNo+ "&ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype;
	    return;
	}
	//var newWindow = window.open("../app/ChangeExtractRate.jsp?GrpContNo=" + cGrpContNo);
	parent.fraInterface.window.location="./ChangeExtractRate.jsp?GrpContNo=" + fm.all('GrpContNo').value+ "&ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype+"&cContType="+cContType;
	// var newWindow = window.open("./ChangeExtractRateMain.jsp?GrpContNo=" + cGrpContNo+ "&LoadFlag="+LoadFlag);	
}
//进入公共账户界面
function GrpAddAcc()
{
		delGrpVar();
		addGrpVar();
		parent.fraInterface.window.location = "./ContInsuredInput.jsp?LoadFlag="+LoadFlag+"&acctype=2&ContType=2&scantype="+ scantype+"&checktype=2"+"&ScanFlag="+ScanFlag;
}
//缴费规则定义
function PayRuleInfo(){
	delGrpVar();
	addGrpVar();
	parent.fraInterface.window.location = "./PayRuleInput.jsp?GrpContNo=" + fm.all('GrpContNo').value+"&LoadFlag="+LoadFlag;
}
//归属规则定义
function AscriptionRuleInfo(){
    delGrpVar();
	addGrpVar();
	
   if (LoadFlag=="99")
	{
	parent.fraInterface.window.location = "./AscriptionRuleInput.jsp?GrpContNo=" + fm.all('GrpContNo').value+"&ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype;
	    return;
	}
	parent.fraInterface.window.location = "./AscriptionRuleInput.jsp?GrpContNo=" + fm.all('GrpContNo').value+"&ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag+"&scantype="+scantype+"&cContType="+cContType;
}
//首期保险费信息校验
function checkPrem(){
var sums;
	if(fm.ImpartCheck4[0].value =="" || fm.ImpartCheck4[0].value == null)
	{
		alert("公共账户缴费金额不能为空！");
		fm.ImpartCheck4[0].focus();
		initArray();
		return false;
	}
	if(fm.ImpartCheck4[1].value =="" || fm.ImpartCheck4[1].value == null)
	{
		alert("个人账户个人缴费金额不能为空！");
		fm.ImpartCheck4[1].focus();
		initArray();
		return false;
	}
	if(fm.ImpartCheck4[2].value =="" || fm.ImpartCheck4[2].value == null)
	{
		alert("个人账户单位缴费金额不能为空！");
		fm.ImpartCheck4[2].focus();
		initArray();
		return false;
	}
	if(fm.ImpartCheck4[3].value =="" || fm.ImpartCheck4[3].value == null)
	{
		alert("首次保险费合计不能为空！");
		fm.ImpartCheck4[3].focus();
		initArray();
		return false;
	}
	sums = Number(fm.ImpartCheck4[0].value)+Number(fm.ImpartCheck4[1].value)+Number(fm.ImpartCheck4[2].value);
	if(fm.ImpartCheck4[3].value != sums){
		alert("公共账户缴费金额+个人账户个人缴费金额+个人账户单位缴费金额之和与首次保险费合计不符！！");
		initArray();
		return false;
	}
	return true;
	
}
//校验是否有公共用户
function checkGrpAcc(){
	var GrpAccsql = "select 1 from lccont where grpcontno='"+fm.all( 'GrpContNo' ).value+"' and poltype = '2'";
	var arr = easyExecSql(GrpAccsql);
	if(arr == null){
		alert("团单号为"+fm.all( 'GrpContNo' ).value+"的保单无公共账户");
		return false;
	}
	return true;
}
//校验“公共账户缴费金额”、“个人账户个人缴费金额”、“个人账户单位缴费金额”和实际人员导入后后校验相关金额合计是否一致
function checkGrpPrem(){
	var GrpPremsql = "select nvl(sum(prem),0) from lcprem lcp left join lmdutypay lmd on lcp.payplancode = lmd.payplancode where lcp.grpcontno = '"+fm.all( 'GrpContNo' ).value+"' and lmd.accpayclass ='3'";
	var GrpPremArr = easyExecSql(GrpPremsql);
	if(GrpPremArr[0][0] != fm.ImpartCheck4[0].value){
		alert("保单信息中的公共账户缴费金额与实际录入的公共账户金额不一致，请修改！");
		fm.ImpartCheck4[0].focus();
		return false;
	}
	GrpPremsql = "select nvl(sum(prem),0) from lcprem lcp left join lmdutypay lmd on lcp.payplancode = lmd.payplancode where lcp.grpcontno = '"+fm.all( 'GrpContNo' ).value+"' and lmd.accpayclass ='4'";
	GrpPremArr = easyExecSql(GrpPremsql);
	if(GrpPremArr[0][0] != fm.ImpartCheck4[2].value){
		alert("保单信息中的个人账户单位缴费金额与实际录入的个人账户单位缴费金额不一致，请修改！");
		fm.ImpartCheck4[2].focus();
		return false;
	}
	GrpPremsql = "select nvl(sum(prem),0) from lcprem lcp left join lmdutypay lmd on lcp.payplancode = lmd.payplancode where lcp.grpcontno = '"+fm.all( 'GrpContNo' ).value+"' and lmd.accpayclass in ('5','6')";
	GrpPremArr = easyExecSql(GrpPremsql);
	if(GrpPremArr[0][0] != fm.ImpartCheck4[1].value){
		alert("保单信息中的个人账户个人缴费金额与实际录入的个人账户个人缴费金额不一致，请修改！");
		fm.ImpartCheck4[1].focus();
		return false;
	}
	return true;
}
//老年护理保险金领取起始日为空校验
function checkOldPeople(){
	if((fm.ImpartCheck5[0].checked == false) && (fm.ImpartCheck5[3].checked == false)){
		alert("请选择老年护理保险金领取起始日信息！");
		fm.ImpartCheck5[0].focus();
		initArray();
		return false;
	}
	if(fm.ImpartCheck5[0].checked == true){
		if((fm.ImpartCheck5[1].value =="") || (fm.ImpartCheck5[1].value ==null)){
			alert("请设置老年护理保险金领取起始日男性年龄！");
			fm.ImpartCheck5[1].focus();
			initArray();
			return false;
		}
		if((fm.ImpartCheck5[2].value =="") || (fm.ImpartCheck5[1].value ==null)){
			alert("请设置老年护理保险金领取起始日女性年龄！");
			fm.ImpartCheck5[2].focus();
			initArray();
			return false;
		}
	}
	return true;
}
//年金领取方式为空校验
function checkYearType(){
	if((fm.ImpartCheck6[0].checked == false) && (fm.ImpartCheck6[1].checked == false) && (fm.ImpartCheck6[2].checked == false) && (fm.ImpartCheck6[3].checked == false) && (fm.ImpartCheck6[4].checked == false)){
		alert("请选择护理保险金领取方式！");
		fm.ImpartCheck6[0].focus();
		initArray();
		return false;
	}
	return true;
}
//初始化每个数组
function initArray(){
	LCImpartStr1 = "";
	LCImpartStr2 = "";
	LCImpartStr3 = "";
	ImpartCheck1 = new Array();
	ImpartCheck2 = new Array();
	ImpartCheck3 = new Array();
	LCImpartStr4 = "";
	ImpartCheck4 = new Array();
	LCImpartStr5 = "";
	ImpartCheck5 = new Array();
	LCImpartStr6 = "";
	ImpartCheck6 = new Array();
	
	ImpartboxStr1 = "";       
	ImpartboxStr2 = "";       
	ImpartboxStr3 = "";       
	ImpartboxStr4 = "";       
	ImpartboxStr5 = "";       
	ImpartboxStr6 = "";       
	ImpartboxStr7 = "";       
	ImpartboxStr17 = "";      
	ImpartboxStr18 = "";      
	ImpartboxStr8 = "";       
	ImpartGridDate1 = "";     
	ImpartGridDate2 = "";     
	ImpartGridDate3 = "";     
	ImpartGridDate4 = "";     
	ImpartGridDate5 = "";     
	ImpartGridDate6 = "";     
	ImpartGridDate7 = "";     
	ImpartGridDate8 = "";     
	ImpartGridDate9 = "";     
	ImpartGridDate16 = "";    
	ImpartGridDate17 = "";    
	ImpartGridDate18 = "";    
	ImpartGridDate19 = "";    
	Impartbox1 = new Array(); 
	Impartbox2 = new Array(); 
	Impartbox3 = new Array(); 
	Impartbox4 = new Array(); 
	Impartbox5 = new Array(); 
	Impartbox6 = new Array(); 
	Impartbox7 = new Array(); 
	Impartbox8 = new Array(); 
	Impartbox17 = new Array();
	Impartbox18 = new Array();
	                          
Detail101 = new Array();
Detail102 = new Array();
Detail103 = new Array();
Detail104 = new Array();
Detail105 = new Array();
Detail106 = new Array();
Detail107 = new Array();
Detail201 = new Array();
Detail202 = new Array();
Detail203 = new Array();
Detail204 = new Array();
Detail205 = new Array();
Detail206 = new Array();
Detail207 = new Array();

 DetailStr101 = "";
 DetailStr102 = "";
 DetailStr103 = "";
 DetailStr104 = "";
 DetailStr105 = "";
 DetailStr106 = "";
 DetailStr107 = "";
 DetailStr201 = "";
 DetailStr202 = "";
 DetailStr203 = "";
 DetailStr204 = "";
 DetailStr205 = "";
 DetailStr206 = "";
 DetailStr207 = "";
}
//管理费必须录入
function checkManageFee(){
	var ManageFeeSql = "select * from lcgrpfee where grpcontno = '"+fm.all('GrpContNo').value+"'";
	var ManageFeeResult = easyExecSql(ManageFeeSql);
	if(ManageFeeResult==null){
		alert("管理费用必须录入！");
		return false;
	}
	return true;
}
//退保费率必须录入
function checkRiskZTFee(){
	var RiskZTFeeSql = "select * from lcriskztfee where grpcontno = '"+fm.all('GrpContNo').value+"'";
	var RiskZTFeeResult = easyExecSql(RiskZTFeeSql);
	if(RiskZTFeeResult==null){
		alert("退保费率必须录入！");
		return false;
	}
	return true;
}
//保费计算后，不可再修改护理保险金领取起始日信息
function checkGetYearAfter(){
	var insuredSql = "select prtno from lccont where grpcontno = '"+fm.all('GrpContNo').value+"' and poltype <>'2'" ;
	var insruedResult = easyExecSql(insuredSql);
	var getYearSql = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '008' and ImpartCode = '010'";
	var getYearResult = easyExecSql(getYearSql);
	var YesOrNo0 = "";
	var value1 = ""
	var value2 = ""
	var YesOrNo3 = "";
	
	if(getYearResult==null){
		alert("没有获取到保单中的老年护理保险金领取起始日信息！");
		initArray();
		return false;
	}
	var getYears = new Array()	
	getYears = getYearResult[0][0].split(",");
	if(fm.ImpartCheck5[0].checked == true){
		YesOrNo0 = "Y";
	}else{
		YesOrNo0 = "N";
	}
	if(fm.ImpartCheck5[1].value == "" || fm.ImpartCheck5[1].value == null){
		value1 = "N";
	}else{
		value1 = fm.ImpartCheck5[1].value;
	}
	if(fm.ImpartCheck5[2].value == "" || fm.ImpartCheck5[2].value == null){
		value2 = "N";
	}else{
		value2 = fm.ImpartCheck5[2].value;
	}
	if(fm.ImpartCheck5[3].checked == true){
		YesOrNo3 = "Y";
	}else{
		YesOrNo3 = "N";
	}
	if(insruedResult != null){
		//fm.ImpartCheck5[0].disabled = true;
		//fm.ImpartCheck5[1].disabled = true;
		//fm.ImpartCheck5[2].disabled = true;
		//fm.ImpartCheck5[3].disabled = true;
		if(YesOrNo0 != getYears[0] || getYears[1]!=value1 ||getYears[2]!=value2 ||YesOrNo3 != getYears[3]){
			alert("保费已计算完毕，不可修改老年护理保险金信息！");
			initArray();
			return false;
		}
	}
	return true;
}
//录入完毕和复核时，校验每个被保人是否都有险种信息
function checkInsuredAndRisk(){
	var InsureAndRiskSql="select lci.insuredno, lci.name,lcp.riskcode "+
	                     " from lcinsured lci"+
                         " left join lcpol lcp"+
                         " on lci.grpcontno=lcp.grpcontno"+
                         " and lci.insuredno = lcp.insuredno"+
                         " where lci.grpcontno='"+fm.all( 'GrpContNo' ).value+"'"+
                         " group by lci.insuredno,lci.name,lcp.riskcode";
//alert(InsureAndRiskSql);                         
    var InsureAndRiskResult = easyExecSql(InsureAndRiskSql);
    if(InsureAndRiskResult==null){
    	alert("该保单下无被保人信息，请核查！");
    	return false;
    }
    for(var i=0;i<InsureAndRiskResult.length;i++){
    	if(InsureAndRiskResult[i][2]=="" || InsureAndRiskResult[i][2]==null){
    		alert("被保人"+InsureAndRiskResult[i][1]+"没有增加险种信息，请核查！");
    		return false;
    	}
    }
    return true;
}   
function GPublicAccQuery()
{
  
  window.open("./GPublicAccQueryInput.jsp?GrpContNo="+fm.all( 'GrpContNo' ).value+"&PrtNo="+fm.all('PrtNo').value);  
  
} 
function payQueryClick()
{
    window.open("../sys/LCPayQuery.jsp?ContNo=" + polNo + "&ContLoadFlag=2"+ "&ContType="+cContType  );	
}
/*********************************************************************
 *  投保书查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ScanQuery()
{

	var arrReturn = new Array();
	var arrReturn1 = new Array();
	var tBussNoType = "";
	var tBussType = "";
    var tSubType = "";
	//获的个人保单印刷号
	var strTableName = "";
	if (cContType=='1')
	{
		strTableName = "LCGrpCont";
	}else if (cContType=='2')
	{  
		strTableName = "LBGrpCont";
	}

    var strSql = "select PrtNo from " + strTableName + " where GrpContNo='" + polNo + "'";
	arrReturn = easyExecSql(strSql);
	if (arrReturn == null)
	{
		alert("没有查询到该合同号，请重新输入");
		return false;
	}
	else
	{
	var strPrtNo = arrReturn[0][0];
	var strSQL1 = "select a.docid,a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
					+ " a.maketime,a.modifydate,a.modifytime,a.docid "
					+ " from es_doc_main a ,es_doc_def b where 1=1 "
					+ " and a.subtype = b.subtype "
				  + " and a.doccode like '"+strPrtNo+"%%'"
				  + " and a.busstype='TB'"
				  + " and b.busstype='TB'";	
	var arrReturn5 = new Array();
  	arrReturn5 = easyExecSql(strSQL1);
  	if(arrReturn5 != null)
  	{   
  	    var cDocID = arrReturn5[0][0];
  	    var cDocCode = arrReturn5[0][1];
  	    var	cBussTpye = "TB" ;
  	    var cSubTpye = arrReturn5[0][2];
  	    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
  	}else{
  	   alert("该印刷号没有投保书影像件！");
  	}
	}
}
/*********************************************************************
 *  理赔明细信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function claimQueryClick()
{
		window.open("ClaimListMain.jsp?ContNo=" + polNo + "&AppObj=G"); //团单
}
/*********************************************************************
 *  客户服务信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function taskQueryClick()
{

	var tCustomer = trim(fm.all('GrpNo').value); 
	if (tCustomer=="" || tCustomer==null)
	{}
	else
	{
		window.open("EdorListMain.jsp?ContNo=" + polNo + "&AppObj=G"); //团单
	}
}
// --------------------

function initHolder(){
	var arrResult = easyExecSql("select InsuredProperty from LCGrpAppnt where grpcontno = '" + mGrpContNo + "'", 1, 0);
	if (arrResult != null) {
        insuredProp=arrResult[0][0];                     
        if(insuredProp == "1"){
        	fm.all('ShareHolder1').style.display='';
    		fm.all('ShareHolder2').style.display='';
    		fm.all('LegalPerson1').style.display='';
    		fm.all('LegalPerson2').style.display='';
    		fm.all('Responsible1').style.display='';
    		fm.all('Responsible2').style.display='';
    		displayFXQ();
        }else if(insuredProp == "2"){
        	fm.all('Sex1').style.display='';
        	fm.all('Sex2').style.display='';
        	fm.all('OccupationInfo').style.display='';
        }
    }
	var tSql1=" select a.sex,a.nativeplace,a.occupationcode,a.occupationtype,a.nativecity from LCGrpAddress a where a.AddressNo=(select AddressNo from LCGrpAppnt  where GrpContNo = '" + mGrpContNo + "') and a.CustomerNo=(select CustomerNo from LCGrpAppnt  where GrpContNo = '" + mGrpContNo + "') ";
	var rs = easyExecSql(tSql1);
	if(rs){
		try {fm.AppntSex.value= rs[0][0];   } catch(ex) { }; 
		try {fm.AppntNativePlace.value= rs[0][1];   } catch(ex) { }; 
		try {fm.AppntOccupationCode.value= rs[0][2];   } catch(ex) { }; 
		try {fm.AppntOccupationType.value= rs[0][3];   } catch(ex) { }; 
		try {fm.all('AppntNativeCity').value= rs[0][4]; } catch (ex) { };
		setCodeName("AppntNativeCityName", "nativecity", rs[0][4]);
		controlNativeCity("");
	}
}

function displayFXQ(){
	var sql = "select c.ShareholderName,c.ShareholderIDType,c.ShareholderIDNo,c.ShareholderIDStart,c.ShareholderIDEnd,c.ShareholderIDLongFlag," +
			"c.LegalPersonName1,c.LegalPersonIDType1,c.LegalPersonIDNo1,c.LegalPersonIDStart1,c.LegalPersonIDEnd1,c.LegalPersonIDLongFlag1," +
			"c.ResponsibleName,c.ResponsibleIDType,c.ResponsibleIDNo,c.ResponsibleIDStart,c.ResponsibleIDEnd,c.ResponsibleIDLongFlag "
        + "from LCGrpAppnt c "
        + "where c.GrpContNo = '" + mGrpContNo + "' ";
	var rs = easyExecSql(sql);
	if(rs)
	{
		try {fm.ShareholderName.value= rs[0][0];   } catch(ex) { }; 
		try {fm.ShareholderIDType.value= rs[0][1];   } catch(ex) { }; 
		try {fm.ShareholderIDNo.value= rs[0][2];   } catch(ex) { }; 
		try {fm.ShareholderIDStart.value= rs[0][3];   } catch(ex) { };
		try {fm.ShareholderIDEnd.value= rs[0][4];   } catch(ex) { };
		try { fm.ShareholderIDLongFlag.value= rs[0][5];if(fm.ShareholderIDLongFlag.value == "Y"){fm.IdNoValidity2.checked = true}} catch(ex) { };
		try {fm.LegalPersonName1.value= rs[0][6];   } catch(ex) { };
		try {fm.LegalPersonIDType1.value= rs[0][7];   } catch(ex) { };
		try {fm.LegalPersonIDNo1.value= rs[0][8];   } catch(ex) { };
		try {fm.LegalPersonIDStart1.value= rs[0][9];   } catch(ex) { };
		try {fm.LegalPersonIDEnd1.value= rs[0][10];   } catch(ex) { };
		try { fm.LegalPersonIDLongFlag1.value= rs[0][11];if(fm.LegalPersonIDLongFlag1.value == "Y"){fm.IdNoValidity1.checked = true}} catch(ex) { };
		try {fm.ResponsibleName.value= rs[0][12];   } catch(ex) { };
		try {fm.ResponsibleIDType.value= rs[0][13];   } catch(ex) { };
		try {fm.ResponsibleIDNo.value= rs[0][14];   } catch(ex) { };
		try {fm.ResponsibleIDStart.value= rs[0][15];   } catch(ex) { };
		try {fm.ResponsibleIDEnd.value= rs[0][16];   } catch(ex) { };
		try { fm.ResponsibleIDLongFlag.value= rs[0][17];if(fm.ResponsibleIDLongFlag.value == "Y"){fm.IdNoValidity3.checked = true}} catch(ex) { };
	}
}
function displayBnf()
{
		var strSql1 = "select bnfname,bnftype,bnfaddress,bnfidtype,(select codename from ldcode where codetype='identitytype' and code=bnfidtype),bnfidno,bnfidstartdate,bnfidenddate from lcgrpcontbnf where prtno='"+fm.PrtNo.value+"'";
		turnPage.queryModal(strSql1,BeneficiaryDetailGrid);
}
function setIDLongEffFlag1(){

	   if(fm.IdNoValidity1.checked == true)
	    {
	        fm.LegalPersonIDLongFlag1.value = "Y";
	    }else{
	        fm.LegalPersonIDLongFlag1.value = "";
	    }
	    
	}
function setIDLongEffFlag2(){

	   if(fm.IdNoValidity2.checked == true)
	    {
	        fm.ShareholderIDLongFlag.value = "Y";
	    }else{
	        fm.ShareholderIDLongFlag.value = "";
	    }
	    
	}
function setIDLongEffFlag3(){

	   if(fm.IdNoValidity3.checked == true)
	    {
	        fm.ResponsibleIDLongFlag.value = "Y";
	    }else{
	        fm.ResponsibleIDLongFlag.value = "";
	    }
	    
	}

function controlNativeCity(displayFlag) {
	if (fm.AppntNativePlace.value == "OS") {
		fm.all("NativeCityInfo").style.display = displayFlag;
		fm.all("NativeCity").style.display = displayFlag;
		fm.all("NativeCityTitle").innerHTML = "国家";
	} else if (fm.AppntNativePlace.value == "HK") {
		fm.all("NativeCityInfo").style.display = displayFlag;
		fm.all("NativeCity").style.display = displayFlag;
		fm.all("NativeCityTitle").innerHTML = "地区";
	} else {
		fm.all("NativeCity").style.display = "none";
	}
}
//为代码赋汉字
function setCodeName(fieldName, codeType, code) {
	var sql = "select CodeName('" + codeType + "', '" + code + "') from dual";
	var rs = easyExecSql(sql)
	if (rs) {
		fm.all(fieldName).value = rs[0][0];
	}
}