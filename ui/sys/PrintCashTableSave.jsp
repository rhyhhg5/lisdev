<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PrintEdorListSave.jsp
//程序功能：
//创建日期：2007-3-19 11:07
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  boolean operFlag = true;
  String flagStr = "";
  String content = "";

  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String polNo = request.getParameter("PolNo");
  String riskCode = request.getParameter("RiskCode");
  
  String sql = "select RiskName from LMRisk where RiskCode = '" + riskCode + "' ";
  String riskName = new ExeSQL().getOneValue(sql);
  
  Reflections ref = new Reflections();
  
  LPPolSchema tLPPolSchema = new LPPolSchema();
  
  LCPolDB tLCPolDB = new LCPolDB();
  tLCPolDB.setPolNo(polNo);
  if(tLCPolDB.getInfo())
  {
    ref.transFields(tLPPolSchema, tLCPolDB.getSchema());
    tLPPolSchema.setEdorNo("userless");
    tLPPolSchema.setEdorType("userless");
  }
  else
  {
    LBPolDB tLBPolDB = new LBPolDB();
    tLBPolDB.setPolNo(polNo);
    if(tLBPolDB.getInfo())
    {
      ref.transFields(tLPPolSchema, tLBPolDB.getSchema());
      tLPPolSchema.setEdorType("userless");
    }
  }
  
  VData data = new VData();
  data.add(tGI);
  data.add(tLPPolSchema);

  PrtCashValueTableBL bl = new PrtCashValueTableBL(1);
  bl.setRowCount(43);   //每栏显示的行数
  bl.setTitle(riskName + "(" + riskCode + ")现金价值表");
  XmlExport tXmlExport = bl.getXmlExport(data, "");
  if(tXmlExport == null)
  {
    operFlag = false;
    content = bl.mErrors.getFirstError().toString();                 
  }
  
	if (operFlag==true)
	{
	  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();  
    CombineVts tcombineVts = new CombineVts(tXmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);  
    session.putValue("PrintVts", dataStream);
	  session.putValue("PrintStream", tXmlExport.getInputStream());
		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
	}
	else
	{
    	flagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=content%>");
	top.close();
</script>
</html>
<%
  	}
%>