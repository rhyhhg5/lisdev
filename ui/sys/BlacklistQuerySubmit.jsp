<%
//程序名称：BlacklistQuery.js
//程序功能：
//创建日期：2002-08-16 17:19:56
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  LDBlacklistSchema tLDBlacklistSchema   = new LDBlacklistSchema();

  BlacklistUI tBlacklistQueryUI   = new BlacklistUI();
  //读取Session中的全局类
	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

    tLDBlacklistSchema.setBlacklistNo(request.getParameter("BlacklistNo"));
    tLDBlacklistSchema.setBlacklistType(request.getParameter("BlacklistType"));
    tLDBlacklistSchema.setBlackName(request.getParameter("BlackName"));
    tLDBlacklistSchema.setBlacklistOperator(request.getParameter("BlacklistOperator"));
    tLDBlacklistSchema.setBlacklistMakeDate(request.getParameter("BlacklistMakeDate"));
    tLDBlacklistSchema.setBlacklistMakeTime(request.getParameter("BlacklistMakeTime"));
    tLDBlacklistSchema.setBlacklistReason(request.getParameter("BlacklistReason"));



  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLDBlacklistSchema);
	tVData.add(tG);

  FlagStr="";
  // 数据传输
	if (!tBlacklistQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " +  tError.getFirstError();
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tBlacklistQueryUI.getResult();
		
		// 显示
		LDBlacklistSet mLDBlacklistSet = new LDBlacklistSet();
		mLDBlacklistSet.set((LDBlacklistSet)tVData.getObjectByObjectName("LDBlacklistSet",0));
		int n = mLDBlacklistSet.size();
		LDBlacklistSchema mLDBlacklistSchema;
		for (int i = 1; i <= n; i++)
		{
		  	mLDBlacklistSchema = mLDBlacklistSet.get(i);
		   	%>
		   	<script language="javascript">
        parent.fraInterface.BlacklistGrid.addOne("BlacklistGrid")
parent.fraInterface.fm.BlacklistGrid1[<%=i-1%>].value="<%=mLDBlacklistSchema.getBlacklistNo()%>";
parent.fraInterface.fm.BlacklistGrid2[<%=i-1%>].value="<%=mLDBlacklistSchema.getBlacklistType()%>";
parent.fraInterface.fm.BlacklistGrid3[<%=i-1%>].value="<%=mLDBlacklistSchema.getBlackName()%>";
parent.fraInterface.fm.BlacklistGrid4[<%=i-1%>].value="<%=mLDBlacklistSchema.getBlacklistMakeDate()%>";

			</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (!FlagStr.equals("Fail"))
  {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

