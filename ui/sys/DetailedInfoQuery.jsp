<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//修改：刘岩松
//修改日期：2004-2-17
%>
<%
	String tContNo = "";
	String tIsCancelPolFlag = "";
	String tContType = "";
	String tInsuredNo = "";
	try
	{
		tContNo = request.getParameter("ContNo");
		tIsCancelPolFlag = request.getParameter("IsCancelPolFlag");
		tContType = request.getParameter("ContType");
		tInsuredNo=request.getParameter("InsuredNo");
		if(tContType == null)
		tContType = "1";//用于测试；
		System.out.println("tContNodfdf="+tContNo);
	}
	catch( Exception e )
	{
		tContNo = "";
		tIsCancelPolFlag = "0";
	}
%>
<head >
<script>
	var tContNo = "<%=tContNo%>"; 
	var tIsCancelPolFlag = "<%=tIsCancelPolFlag%>";
	var tContType = "<%=tContType%>";
	var tInsuredNo=	"<%=tInsuredNo%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="DetailedInfoQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="DetailedInfoQueryInit.jsp"%>
  <title>保单详细信息</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>保单险种信息</td>
		</tr>
	</table>
  	<Div  id= "divLCClent" style= "display: ''" align = center>
      	<table  class= common align = center>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanRiskPolGrid">
  					</span> 
  			  	</td>
     </div>
  			</tr>
		</table>
	<table align = center>
		<tr>
			<td>
  			<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage.previousPage();"> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage.lastPage();"> 
	</td>
</tr>
</table>
	<br>
	<table class="common" id="table2">
		<tr CLASS="common">
			<td CLASS="title">险种状态</td>    		       
				<td CLASS="input" COLSPAN="1">
					<input NAME="Riskstate" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20" READONLY="true">
				</td>	
			<td CLASS="title">保额</td>    		         
				<td CLASS="input" COLSPAN="1">
					<input NAME="Amnt" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20"READONLY="true">
				</td>		
			<td CLASS="title">应缴保费</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="StandPrem" VALUE CLASS="common" READONLY="true"TABINDEX="-1" >
				</td>
</tr>
<tr CLASS="common">
			<td CLASS="title">
				档次 </td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="Mult" VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
    	</td>	
		<TD CLASS=title  >
	     缴次 
	   </TD>
	   <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PayCount VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
	   </TD>	

			<TD  class= title>
      被保人名称
			</TD>
    <TD  class= input>
      <Input name=InsuredName VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>
</tr>
<tr CLASS="common">
		<TD  class= title>
      生效时的年龄
			</TD>
    <TD  class= input>
      <Input name=InsuredAppAge VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>	
    <TD  class= title>
        被保人性别
    </TD>
    <TD  class= input>
       <Input  name=InsuredSex VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD> 
  <TD  class= title>
        被保人职业
    </TD>
    <TD  class= input>
       <Input  name=Occupationcode type=hidden VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
       <Input  name=OccupationName VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    </TD>
</tr>
<tr CLASS="common">             			
<td CLASS="title">投保年度</td>
		<td CLASS="input" COLSPAN="1">
			<input NAME="InsuYear" VALUE CLASS="common" READONLY="true"TABINDEX="-1" MAXLENGTH="20" >
		</td>
</tr>
</table>
<p>
      <td><INPUT VALUE="应缴保费信息" class = cssbutton TYPE=button onclick="PayQueryClick();"> </td> 
			 <td><INPUT VALUE="保费历史资料" class = cssbutton TYPE=button onclick="PayhistoryQueryClick();"> </td> 
			  <td><INPUT VALUE="保单杂项信息" class = cssbutton TYPE=button onclick="MiscellaneouspolQueryClick();"> </td> 
			   <td><INPUT VALUE="受益人资料" class = cssbutton TYPE=button onclick="BnfQueryClick();"> </td>
			    <td><INPUT VALUE="业务员资料" class = cssbutton TYPE=button onclick="AggentQueryClick();"> </td>
			     <td><INPUT VALUE="保全历史资料" class = cssbutton TYPE=button onclick="EndorhistoryQueryClick();"> </td> 
			      <td><INPUT VALUE="保单理赔历史" class = cssbutton TYPE=button onclick="ClaimhistoryQueryClick();"> </td> 
</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

