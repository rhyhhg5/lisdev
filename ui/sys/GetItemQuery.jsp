<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-12-24 11:10:36
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String tPolNo = "";
	try
	{
		tPolNo = request.getParameter("PolNo");
	}
	catch( Exception e )
	{
		tPolNo = "";
	}
	String tIsCancelPolFlag = "";
	try
	{
		tIsCancelPolFlag = request.getParameter("IsCancelPolFlag");
	}
	catch( Exception e )
	{
		tIsCancelPolFlag = "";
	}
%>
<head >
<script> 
	var tPolNo = "<%=tPolNo%>";
	var tIsCancelPolFlag = "<%=tIsCancelPolFlag%>";
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="GetItemQuery.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="GetItemQueryInit.jsp"%>
	
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>给付项查询 </title>
</head>

<body  onload="initForm();easyQueryClick();" >
  <form  name=fm >
  <table >
    	<tr>
    	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    	</td>
			<td class= titleImg>
				保单信息
			</td>
		</tr>
	</table>
	<Div  id= "divLCPol1" style= "display: ''">
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            保单号码
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo >
          </TD>
				</TR>
     </table>
  </Div>
  
  
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGet1);">
    		</td>
    		<td class= titleImg>
    			 给付项信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCGet1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class=common TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=common TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=common TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页"  class=common TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script>
var turnPage = new turnPageClass(); 
function easyQueryClick()
{
	
	//alert("here");
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	if(tIsCancelPolFlag == "0"){
	strSQL = "select LCGet.PolNo,LMDutyGet.GetDutyName,LCGet.InsuredNo,LCGet.AppntNo,LCGet.GetIntv,LCGet.GetStartDate,LCGet.GetEndDate,LCGet.GettoDate,LCGet.StandMoney,LCGet.ActuGet,LCGet.GetMode,LCGet.GetLimit,LCGet.GetRate from LCGet,LMDutyGet where LCGet.PolNo='" + tPolNo + "' and LMDutyGet.GetDutyCode = LCGet.GetDutyCode";			 
	}
	else
	 if(tIsCancelPolFlag == "1") {//销户保单
	    strSQL = "select LBGet.PolNo,LMDutyGet.GetDutyName,LBGet.InsuredNo,LBGet.AppntNo,LBGet.GetIntv,LBGet.GetStartDate,LBGet.GetEndDate,LBGet.GettoDate,LBGet.StandMoney,LBGet.ActuGet,LBGet.GetMode,LBGet.GetLimit,LBGet.GetRate from LBGet,LMDutyGet where LBGet.PolNo='" + tPolNo + "' and LMDutyGet.GetDutyCode = LBGet.GetDutyCode";			 
	}
	else {
	  alert("保单类型传输错误!");
	  return;
	  }
	//alert(strSQL);
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}
</script>
</html>


