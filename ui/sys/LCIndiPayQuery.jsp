<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-12-19 11:10:36
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<script> 
	var ContNo = "<%=request.getParameter("ContNo")%>";	
    var tContLoadFlag = "<%=request.getParameter("ContLoadFlag")%>";
　　 var tContType = "<%=request.getParameter("ContType")%>";
</script>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="LCIndiPayQuery.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LCIndiPayQueryInit.jsp"%>

<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<title>缴费明细查询 </title>
</head>

<body  onload="initForm();" >
  <form  name=fm >
      <br>
      <table class="common" id="table2">
		<tr CLASS="three">
			<td CLASS="title">保单号</td>    		       
			<td CLASS="input" COLSPAN="1">
			<input NAME="ContNo" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20"READONLY="true">
    		</td>
			<td CLASS="title">投保人</td>    		         
			<td CLASS="input" COLSPAN="5">
			<input NAME="Name" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="14" READONLY="true"></td>
    	 </tr>
	  </table>
	  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSpec);">
    		</td>
    		<td class= titleImg> 特别缴费约定</td>  			
			<td class=common><Input class= common1 name=specExsit  readonly></td>
			<td><INPUT class=cssButton VALUE="查询" TYPE=button onclick="specQuery();"> </td>
			<td><INPUT class=cssButton VALUE="打  印" onclick="printContList('Vali');" id="PrintContListInValiID" class = cssbutton TYPE=button></td> 
    	</tr>
    </table>
    <Div  id= "divSpec" style= "display: "> 
      	<table  class= two>
       		<tr class=common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanSpecGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    </DIV>   
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJAPPerson1);">
    		</td>
    		<td class= titleImg> 
    			 缴费明细：<font color="#ff0000">页面上显示首期收费和各期实收保费记录的明细</font> 
    		</td>
    	</tr>
    </table>
    <table>
    <tr>
    		<td class= titleImg> 
    			 <font color="#ff0000">1.本次使用保费余额:续期收费时从账户中抵扣的金额。该余额只有负值和零。取负值时，表示使用了投保人账户中多少钱。取零时表示没有使用投保人账户中的钱。</font> 
    		</td>
    	</tr>
    	<tr>
    		<td class= titleImg> 
    			 <font color="#ff0000">2.应缴保费:该字段记录首期收费、续期应收保费和实收保费转出的保费。其中实收保费转出的值为负值，其他都为正值</font> 
    		</td>
    	</tr>
    	<tr>
    		<td class= titleImg> 
    			 <font color="#ff0000">3.实缴保费:“应缴保费”加“本次使用保费余额”的和</font> 
    		</td>
    	</tr>
    	<tr>
    		<td class= titleImg> 
    			 <font color="#ff0000">4.累计保费:应收保费项之和。</font> 
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLJAPPerson1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPayGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<br>
		</div>
    <Div id= "divPage" align=center style= "display: '' ">	
      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="turnPage2.lastPage();">				
  	</div>
  	<input name= "printSql" type=hidden>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


