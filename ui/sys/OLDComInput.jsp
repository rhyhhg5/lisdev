<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 17:44:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="OLDComInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="OLDComInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./OLDComSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOLDCom1);">
    </IMG>
    <Div  id= "divOLDCom1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            机构编码
          </TD>
          <TD  class= input>
            <Input class= common name=ComCode >
          </TD>
          <TD  class= title>
            对外公布的机构代码
          </TD>
          <TD  class= input>
            <Input class= common name=OutComCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            机构名称
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
          <TD  class= title>
            短名称
          </TD>
          <TD  class= input>
            <Input class= common name=ShortName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            机构地址
          </TD>
          <TD  class= input>
            <Input class= common name=Address >
          </TD>
          <TD  class= title>
            机构邮编
          </TD>
          <TD  class= input>
            <Input class= common name=ZipCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            机构电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone >
          </TD>
          <TD  class= title>
            机构传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            EMail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
          <TD  class= title>
            网址
          </TD>
          <TD  class= input>
            <Input class= common name=WebAddress >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            主管人姓名
          </TD>
          <TD  class= input>
            <Input class= common name=SatrapName >
          </TD>
          <TD  class= title>
            标志
          </TD>
          <TD  class= input>
            <Input class= common name=Sign >
          </TD>
        </TR>
      </table>
    </Div>
    <input type=hidden name=hideOperate value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
