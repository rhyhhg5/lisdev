<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String cContNo = "";
	String cContLoadFlag = "";
	String cContType = "";
	try
	{
		cContNo = request.getParameter("GrpContNo");
		cContLoadFlag = request.getParameter("ContLoadFlag");
		cContType = request.getParameter("ContType");
	}
	catch( Exception e )
	{
		cContNo = "";
	}	
%>
<head >
<script> 
	var tContNo = "<%=cContNo%>"; 
	var tContLoadFlag = "<%=cContLoadFlag%>";
	var tContType =  "<%=cContType%>";
	var loadFlag = "<%= request.getParameter("LoadFlag")%>";
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="GrpPolDetailQuery.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="GrpPolDetailQueryInit.jsp"%>
	
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>保单查询 </title>
</head>
<body  onload="initForm();easyQueryClick();" >
  <form  name=fm > 
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCGrpCont);">
    		</td>
    		<td class= titleImg>
    			 内控信息
    		</td>
    	</tr>
    </table>
  <div id="DivLCGrpCont" STYLE="display:''">
	<table class="common" id="table2">
		<tr CLASS="common">
			<td CLASS="title">保单状态2 </td>    		       
			<td CLASS="input" COLSPAN="1">
			  <input NAME="AppFlag" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="20"READONLY="true">
      </td>
			<td CLASS="title">最后处理人</td>    		         
			<td CLASS="input" COLSPAN="1">
			<input NAME="Operator" VALUE CLASS="common" TABINDEX="-1"MAXLENGTH="14" READONLY="true"></td>
    	<td CLASS="title">最后处理时间</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="ModifyDate" VALUE MAXLENGTH="10" CLASS="common" TABINDEX="-1" READONLY="true">
    	</td>
			 <td CLASS="title">管理机构 </td>    		        
			<td CLASS="input" COLSPAN="1">
			<Input class="code" name=ManageCom ondblclick="return showCodeList('comcode',[this]);" onkeyup="return showCodeListKey('comcode',[this]);"> </td>    		         
		</tr>
		<tr CLASS="common">
			<td CLASS="title">销售渠道</td>    		           
			<td CLASS="input" COLSPAN="1">
			  <input NAME="SaleChnl" VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
    	</td>
			<td CLASS="title">服务业务员代码</td>    		           
			<td CLASS="input" COLSPAN="1">
			  <input NAME="AgentCode" VALUE MAXLENGTH="10" CLASS="common" READONLY="true" TABINDEX="-1">
    	</td>
			<td CLASS="title">服务业务员名称</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="AgentName" VALUE CLASS="common" READONLY="true"TABINDEX="-1" MAXLENGTH="20" >
    	</td>
			<td CLASS="title">所属部门</td>    		         
			<td CLASS="input" COLSPAN="1">
			<input NAME="AgentCom" VALUE CLASS="common" READONLY="true"TABINDEX="-1" >
			</td>
		</tr>
		<tr class=common>
			<td class=title>资格证书号码</td>
			<td class=input>
				<input class=common readonly=true name=Certificate>
			</td>
			<td class=title>展业证书号码</td>
			<td class=input>
				<input class=common readonly=true name=Exhibition>
			</td>
			<td class=title id=T1 style="display:''">代理销售业务员编码</td>
			<td class=input id=I1 style="display:''">
				<input class=common readonly=true name=SalesAgentCode>
			</td>
			<td class=title id=T2 style="display:''">代理销售业务员姓名</td>
			<td class=input id=I2 style="display:''">
				<input class=common readonly=true name=SalesAgentName>
			</td>
		</tr>
		<tr CLASS="common">
			<td CLASS="title">销售机构代码</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="SaleChnlCode" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    		</td>
    		<td CLASS="title">销售机构地址</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="SaleChnlAddress" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    		</td>
      		<td CLASS="title">联系电话</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="AgentPhone" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    		</td>
			<td CLASS="title">上次抽档时间 </td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="PostalDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1"></td>
		</tr>
			 <tr CLASS="common">
      <td CLASS="title">中介销售</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="agency" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
			
		</tr>
		<tr CLASS="common">
		  <td CLASS="title">保单回执客户签收日期</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="CustomGetPolDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
    	</td>
			<td CLASS="title">业务员递交客户回执时间跨度（天） </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="CustomPolInteDay" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
			<td CLASS="title">大项目标识</td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="BigProjectFlag" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
			<td CLASS="title">承保日期 </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="SignDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
    </tr>
		<tr CLASS="common">
			<td CLASS="title">保单生效日  </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="CValiDate" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
      <td CLASS="title">保单满期日 </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="CInValiDate" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
      <td CLASS="title">保费交至日期 </td>
			<td CLASS="input" COLSPAN="1">
			  <input NAME="PayToDate2" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
    </tr>
		<tr CLASS="common">
			<td CLASS="title">是否正在理赔</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="PayFlag" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
			</td>
            <td CLASS="title">是否正在保全 </td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="EdorFlag" VALUE MAXLENGTH="10" CLASS="common" READONLY="true"TABINDEX="-1"></td>
			<td CLASS="title">是否正在续期</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="DueFeeFlag" VALUE CLASS="common" READONLY="true"TABINDEX="-1">
    		</td>
    		<td class="title">是否为共保保单</td>
            <td class="input">
                <input class="common" name="CoInsuranceFlag"  READONLY="true" TABINDEX="-1" >
            </td>
     </tr>
   </table>
	   <table class=common>	   
         <TR  class= common> 
           <TD  class= common>  特别约定 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Remark" cols="110" rows="2" class="common" READONLY="TRUE"></textarea>
           </TD>
         </TR>		
      </table>
      <table>
        <tr> 
          <td class= common> 
          	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divInnerCueGrid);"> 
          </td><br>
          <td class= titleImg>内部操作提示</td>
          <td class= common> 
            &nbsp;&nbsp;
          </td>
        </tr>
      </table>
      <Div  id= "divInnerCueGrid" style= "display: ''" align="center">  
    	  <table  class= common>
     		  <tr  class= common>
    	  		<td text-align: left colSpan=1>
    				<span id="spanInnerCueGrid" >
    				</span> 
    	  		</td>
    		  </tr>
    	  </table>
      	<Div id= "divPage1" align="center" style= "display: 'none' ">
      		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();	showCodeName();"> 
      		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage(); showCodeName();"> 					
      		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage(); showCodeName();"> 
      		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage(); showCodeName();"> 
      	</Div>
      </Div>
    <table class=common style="display:none">
		  <TR  class= common> 
           <TD  class= common>  核保结论 </TD>
       </TR>
         <TR  class= common>
          <TD  class= common>
             <textarea name="PassFalg" cols="110" rows="2" class="common" READONLY="TRUE"></textarea>
          </TD>
        </TR>
		  <TR  class= common> 
        <TD  class= common>  核保决定 </TD>
      </TR>
      <TR  class= common>
        <TD  class= common>
          <textarea name="PassDecize" cols="110" rows="2" class="common" READONLY="TRUE"></textarea>
        </TD>
      </TR>
    </table>
   </div>  
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupCont2);">
    		</td>
    		<td class= titleImg>
    			 投保时团体基本信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divGroupCont2" style= "display: ''">
      <table  class= common>
       <TR>
          <TD  class= title8>
            单位名称
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpName elementtype=nacessary onchange=checkuseronly(this.value) verify="单位名称|notnull&len<=60" readonly>
          </TD>  
		   <TD  class= title8>
            单位地址
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpAddress elementtype=nacessary  verify="单位地址|notnull&len<=60" readonly>
          </TD>
		  <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpZipCode  elementtype=nacessary  verify="邮政编码|notnull&zipcode" readonly>
          </TD>       
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8  name=Phone elementtype=nacessary verify="单位总机|notnull&NUM&len<=30" readonly>
          </TD>                    
        </TR>
        <TR>
		  <TD  class= title8>
            保单性质
          </TD>                   
          <TD  class= input8>   
            <Input class="common8" name=ContKind elementtype=nacessary readonly >
          </TD>
		  <TD  class= title8>
            投保时间
          </TD>                  
          <TD  class= input8>            
            <Input class=common8 name=PolApplyDate elementtype=nacessary readonly >          
          </TD>
		 <TD  class= title8>
            行业性质
          </TD>                   
          <TD  class= input8>   
            <Input class="common8" name=BusinessTypeName elementtype=nacessary readonly >
          </TD>
		  <TD  class= title8>
            企业类型
          </TD>                  
          <TD  class= input8>            
            <Input class=common8 name=GrpNatureName elementtype=nacessary readonly >          
          </TD>
        </TR>   
        <TR  class= common>
		      <TD  class= title8>
            员工总人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Peoples  elementtype=nacessary verify="单位总人数|notnull&int" readonly>
          </TD>
		      <TD  class= title8>
            在职人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AppntOnWorkPeoples   verify="在职人数|int" readonly>
          </TD>
          <TD  class= title8>
            退休人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AppntOffWorkPeoples   verify="退休人数|int" readonly>
          </TD>
          <TD  class= title8>
            其他人员人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AppntOtherPeoples   verify="其他人员人数|int" readonly>
          </TD>                     
        </TR>    
        <TR  class= common>
		      <TD  class= title8>
            被保险人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Peoples2  elementtype=nacessary verify="单位总人数|notnull&int" readonly>
          </TD>
		      <TD  class= title8>
            参保在职人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=OnWorkPeoples2   verify="在职人数|int" readonly>
          </TD>
          <TD  class= title8>
            参保退休人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=OffWorkPeoples2   verify="退休人数|int" readonly>
          </TD>
          <TD  class= title8>
            其他参保人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=OtherPeoples2   verify="其他人员人数|int" readonly>
          </TD>                     
        </TR>     
        <TR  class= common>
		      <TD  class= title8>
            连带被保人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=RelaPeoples  elementtype=nacessary verify="单位总人数|notnull&int" readonly>
          </TD>
		      <TD  class= title8>
            配偶人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=RelaMatePeoples   verify="在职人数|int" readonly>
          </TD>
          <TD  class= title8>
            子女人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=RelaYoungPeoples   verify="退休人数|int" readonly>
          </TD>
          <TD  class= title8>
            其他连带人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=RelaOtherPeoples   verify="其他人员人数|int" readonly>
          </TD>                     
        </TR>       
        <TR  class= common>
		      <TD  class= title8>
            联系人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan1 elementtype=nacessary verify="保险联系人一姓名|notnull&len<=10" readonly>
          </TD>
          <TD  class= title8>
            联系人电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone1 elementtype=nacessary  verify="保险联系人一联系电话|notnull&len<=30" readonly>
          </TD>
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax1 verify="保险联系人一传真|len<=30" readonly>
          </TD>             
          <TD  class= title8>
            电子邮箱
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail1 verify="保险联系人一E-MAIL|len<=60&Email" readonly>
          </TD>
	
	<tr >
     	  <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <input class=codeNo name="AppntNativePlace" verify="投保人国籍|code:GrpNativePlace" ondblclick="return showCodeList('GrpNativePlace',[this,AppntNativePlaceName],[0,1]);" onkeyup="return showCodeListKey('GrpNativePlace',[this,AppntNativePlaceName],[0,1]);"><input class=codename name=AppntNativePlaceName readonly=true>    
          </TD>
          <TD  class= title8 id="NativeCityInfo" style="display: none">
          <div id="NativeCityTitle">国家</div>
          </TD>
          <TD  class= input8 id="NativeCity" style="display: none">
          <input class=codeNo name="AppntNativeCity" ondblclick="return showCodeList('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);"><input class=codename name=AppntNativeCityName readonly=true>    
          </TD>
        <TD  class= title id="Sex1" style="display: none" >
            性别
          </TD>
          <TD  class= input id="Sex2" style="display: none">
            <Input class=codeNo name=AppntSex  verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true>    
          </TD>
          </TR>
          <TR id="OccupationInfo" style="display: none">
          <TD  class= title >
            职业代码
          </TD>
          <TD  class= input >
            <Input class="code" name="AppntOccupationCode" verify="投保人职业代码|notnull" ondblclick="return showCodeList('OccupationCode',[this,AppntOccupationName],[0,1],null,null,null,null,300);" onkeyup="return showCodeListKey('OccupationCode',[this,AppntOccupationName],[0,1],null,null,null,null,300);"onfocus="getdetailwork();">
          </TD>
          
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input >
            <Input class="code" name="AppntOccupationType" verify="投保人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>
          </tr>
  
          <TR  class= common>  
          <TD class="title8">
           统一社会信用代码
          </TD>
          <TD class="input8">
              <input class="common" name="UnifiedSocialCreditNo" verify="统一社会信用代码|len=18" />
          </TD>
          <TD class="title8">
           纳税人类型
          </TD>
          <TD class="input8">
			  <Input class=codeno name=TaxpayerType verify="纳税人类型|code:GrpTaxpayerType" ondblclick="return showCodeList('GrpTaxpayerType',[this,TaxpayerTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('GrpTaxpayerType',[this,TaxpayerTypeName],[0,1],null,null,null,1);"><input class=codename name=TaxpayerTypeName readonly=true > 		     		
          </TD>
		  <TD class="title8">
           税务登记证号码
          </TD>
          <TD class="input8">
		      <input class="common" name="TaxNo"/>
          </TD>
		  <TD class="title8">
           客户开户银行
          </TD>
          <TD class="input8">
			   <Input name=CustomerBankCode class=codeNo ondblclick="return showCodeList('bank',[this,CustomerBankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bank',[this,CustomerBankCodeName],[0,1],null,null,null,1);"><input class=codename name=CustomerBankCodeName readonly=true >  
          </TD>
         </TR> 
         <TR>
     	  <TD class="title8">
           客户银行账户
          </TD>
          <TD class="input8">
			 	<Input class="common8" name=CustomerBankAccNo verify="客户银行账户|NUM">
          </TD>
          <TD class="title8">
           组织机构代码
          </TD>
          <TD class="input8">
		      <input class="common" name="OrgancomCode"/>
          </TD>
		  <TD  class="title8">
   授权使用客户信息
    </TD>
    <TD  class= "input8">
    	<input class="codeNo" name="auth" verify="授权使用客户信息|notnull" ondblclick="return showCodeList('auth',[this,authName],[0,1],null,null,null,1);" onkeyup="return showCodeList('auth',[this,authName],[0,1],null,null,null,1);"><input class="codename" name="authName" readonly="readonly" elementtype="nacessary" />
    	
    </TD>
         </TR>           
				  <TR  class= common style="display:none">
					  <TD  class= title>
						帐户余额
					  </TD>
					  <TD  class= input >
					  	<input class= common name="AccBala" readonly >
					  </TD>
					  <TD  class= input  colspan="4">
					  	<input class=cssButton type="button" value="余额记录" onclick="printAppAccTraceList();">
					  </TD>
				  </TR>
				  
		<tr id="ShareHolder1" style="display: none">
        	<TD  class= title8>
            控股股东/实际控制人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8  name=ShareholderName readonly="true"> 
          </TD>
          <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class=codeno name=ShareholderIDType  CodeData="0|1^0|身份证^4|其他" ondblclick="return showCodeListEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);"><input class=codename name=IDTypeName readonly=true >
          </TD>
          <TD  class= title8>
            证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=ShareholderIDNo readonly="true" verify="联系人证件号码|NUM&len<=20" >
          </TD>
		  <TD  class= title8>
            证件生效日期
          </TD>                  
          <TD  class= input8>            
            <Input class=common8 dateFormat="short" name=ShareholderIDStart readonly="true"  verify="证件生效日期|date">        
          </TD>        
        </TR>
          <TR id="ShareHolder2" style="display: none">
          <TD  class= title8>
            证件失效日期
          </TD>
          <TD  class= input8>
            <Input class=common8 dateFormat="short" readonly name=ShareholderIDEnd  verify="证件失效日期|date"> 
            <input type="checkbox" name="IdNoValidity2" class="box"  >&nbsp;&nbsp;&nbsp;长期有效
            <Input class= common8  name=ShareholderIDLongFlag type=hidden>
          </TD>
        </TR>
        
        <tr id="LegalPerson1" style="display: none">
        	<TD  class= title8>
            法人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 readonly name=LegalPersonName1>
          </TD>
          <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class=codeno name=LegalPersonIDType1  CodeData="0|1^0|身份证^4|其他" ondblclick="return showCodeListEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);"><input class=codename name=IDTypeName readonly=true >
          </TD>
          <TD  class= title8>
            证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 readonly name=LegalPersonIDNo1 verify="联系人证件号码|NUM&len<=20" >
          </TD>
		  <TD  class= title8>
            证件生效日期
          </TD>                  
          <TD  class= input8>            
            <Input class=common8 dateFormat="short" readonly name=LegalPersonIDStart1  verify="证件生效日期|date">        
          </TD>        
        </TR>
          <TR id="LegalPerson2" style="display: none">
          <TD  class= title8>
            证件失效日期
          </TD>
          <TD  class= input8>
            <Input class=common8 dateFormat="short" readonly name=LegalPersonIDEnd1  verify="证件失效日期|date"> 
            <input type="checkbox" name="IdNoValidity1" class="box"  onclick="setIDLongEffFlag1();">&nbsp;&nbsp;&nbsp;长期有效
            <Input class= common8  name=LegalPersonIDLongFlag1 type=hidden>
          </TD>
        </TR>
        
        <TR id="Responsible1" style="display: none">
        	<TD  class= title8>
            负责人姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 readonly name=ResponsibleName>
          </TD>
          <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class=codeno name=ResponsibleIDType  CodeData="0|1^0|身份证^4|其他" ondblclick="return showCodeListEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);"><input class=codename name=IDTypeName readonly=true >
          </TD>
          <TD  class= title8>
            证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 readonly name=ResponsibleIDNo verify="联系人证件号码|NUM&len<=20" >
          </TD>
		  <TD  class= title8>
            证件生效日期
          </TD>                  
          <TD  class= input8>            
            <Input class=common8 dateFormat="short" readonly name=ResponsibleIDStart  verify="证件生效日期|date">        
          </TD>        
        </TR>
          <TR id="Responsible2" style="display: none">
          <TD  class= title8>
            证件失效日期
          </TD>
          <TD  class= input8>
            <Input class=common8 dateFormat="short" readonly name=ResponsibleIDEnd  verify="证件失效日期|date"> 
            <input type="checkbox" name="IdNoValidity3" class="box"  onclick="setIDLongEffFlag3();">&nbsp;&nbsp;&nbsp;长期有效
            <Input class= common8  name=ResponsibleIDLongFlag type=hidden>
          </TD>
        </TR>
				    
      </table>
    </Div>
    <input type=hidden id="GrpAppntNo" name="GrpAppntNo">
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divbenef);">
    		</td>
    		<td class= titleImg>
    			 受益所有人信息
    		</td>
    	</tr>
    </table>
    <table  class= common id="divbenef">
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanBeneficiaryDetailGrid">
						</span>
					</td>
				</tr>
	</table>
    
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divpeople);">
    		</td>
    		<td class= titleImg>
    			 现有参保人员资料
    		</td>
    	</tr>
    </table>
    
    <Div  id= "divpeople" style= "display: ''">
      <table class=common>
        <TR  class= common>       
          <TD  class= title8>
            被保人数
          </TD>
          <TD  class= input8>
          <Input name=Peoples3 class= common8 readonly>
          </TD>           
          <TD  class= title8>
            在职人数
          </TD>
          <TD  class= input8>
          <Input name=OnWorkPeoples class= common8 readonly>
          </TD>
          <TD  class= title8>
            退休人数
          </TD>
          <TD  class= input8>
          <Input name=OffWorkPeoples class= common8 readonly>
          </TD> 
		  <TD  class= title8>
            其他人数
          </TD>
          <TD  class= input8>
          <Input name=OtherPeoples class= common8 readonly>
          </TD>  
       </TR>
       <TR  class= common>                   
         <TD  class= title8>
            实名人数
          </TD>
          <TD  class= input8>
            <Input name=RealPeoPles2 class= common8 readonly>
          </TD>  
          <TD  class= title8>
            连带被保人数
          </TD>
          <TD  class= input8>
            <Input name=RelaPeoples2 class= common8 readonly>
          </TD> 
          <TD  class= title8>
            配偶人数
          </TD>
          <TD  class= input8>
          <Input name=RelaMatePeoples2 class= common8 readonly>
          </TD>                  
          <TD  class= title8>
            子女人数
          </TD>
          <TD  class= input8>
          <Input name=RelaYoungPeoples2 class= common8 readonly>
          </TD>   
        </TR>
        <TR  class= common>                                                   
          <TD  class= title8>
            其他连带人数
          </TD>
          <TD  class= input8>
          <Input name=RelaOtherPeoples2 class= common8 readonly>
          </TD> 
        </TR>       
      </table>    
    </Div>
       <table>
    	<tr>
        <td class=common>
			
    		</td>
    		<td class= titleImg> 
    			交叉销售信息 
    		</td>
    	</tr>
    </table>
    
      <table class="common" id="table2">
     <tr class="common8" id="GrpAgentComID">
            <td class="title8">交叉销售渠道</td>
            <td class="input8">
                <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl"  readonly/><input class="codename" name="Crs_SaleChnlName" readonly/>
            </td>
            <td class="title8">交叉销售业务类型</td>
            <td class="input8">
                <input class="codeNo" name="Crs_BussType" id="Crs_BussType"  readonly /><input class="codename" name="Crs_BussTypeName" readonly />
            </td>
            <td class="title8">&nbsp;</td>
            <td class="input8">&nbsp;</td>
        </tr>	
		<tr class=common id="GrpAgentTitleID" >
    		<td CLASS="title" >对方机构代码</td>    		
			<td CLASS="input" COLSPAN="1" >
    	    <Input class="code" name="GrpAgentCom"  readonly>
    	    </td>   	    
    	    <td  class= title>对方机构名称</td>
	        <td  class= input>
	          <Input class="common" name="GrpAgentComName"  TABINDEX="-1"  readonly>
	        </td>  
			<td CLASS="title">对方业务员代码</td>
    		<td CLASS="input" COLSPAN="1">
			<input NAME="GrpAgentCode" VALUE MAXLENGTH="20" CLASS="code"  readonly >
    		</td>
        </tr>
   	 	<tr class=common id="GrpAgentTitleIDNo" >
            <td  class="title" >对方业务员姓名</td>
	        <td  class="input" COLSPAN="1">
	            <Input  name=GrpAgentName CLASS="common"  readonly >
	        </td>
	        <td CLASS="title">对方业务员身份证</td>
	    	<td CLASS="input" COLSPAN="1">
			<input NAME="GrpAgentIDNo" VALUE MAXLENGTH="18" CLASS="code"  readonly >
    	    </td>
    	</tr>
      </table>
      
	 <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFee);">
    		</td>
    		<td class= titleImg>
    			 缴费资料
    		</td>
    	</tr>
    </table>
    <Div  id= "divFee" style= "display: ''">     
      <table class=common>    
        <TR  class= common>
          <TD  class= title8>
            缴费方式
          </TD>
          <TD  class= input8>
            <Input class="code8" name=PayMode elementtype=nacessary verify="付款方式|notnull&code:PayMode" ondblclick="return showCodeList('PayMode',[this]);" onkeyup="return showCodeListKey('PayMode',[this]);">
          </TD>
          <TD  class= title8>
            缴费银行
          </TD>
          <TD  class= input8>
            <Input class=code8 name=BankCode verify="开户银行|code:bank&len<=24" ondblclick="showCodeList('bank',[this]);" onkeyup="showCodeListKey('bank',[this]);" >
          </TD>
          <TD  class= title8>
            缴费账号
          </TD>
          <TD  class= input8>
            <Input class= common8 name=BankAccNo verify="帐号|len<=40" readonly>
          </TD> 
		  <TD  class= title8>
            缴费户名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AccName verify="户名|len<=40" readonly>
          </TD>       
        </TR> 
        <TR  class= common>    
          <TD  class= title8>
           交费频次
          </TD>
          <TD  class= input8>
          <Input class=code8 name=PayIntv  codeData="0|^1|月缴 ^3|季缴 ^6|半年缴 ^12|年缴"   verify="缴费频次|notnull&code:PayIntv" ondblclick="return showCodeList('PayIntv',[this]);" onkeyup="return showCodeListKey('PayIntv');" ></TD>
		      <TD  class= title8>
            期缴保费
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpPrem readonly>
          </TD>
          <TD  class= title8>
            缴至日期
          </TD>
          <TD  class= input8>
            <Input class= common8 name=PaytoDate readonly>
          </TD>
          <TD  class= title8>
            缴费主体
          </TD>
          <TD  class= input8>
            <Input class= common8 name=PayPerson readonly>
          </TD>
        </TR>   
        <TR  class= common>    
          <TD  class= title8>
           首期收费合计
          </TD>
          <TD  class= input8>
          <Input class=code8 name=theFirstPremRyan readonly></TD>
		      <TD  class= title8>
            续期收费合计
          </TD>
          <TD  class= input8>
            <Input class= common8 name=SumGetMoneyRyan readonly>
          </TD>
          <TD  class= title8>
           保全收费合计
          </TD>
          <TD  class= input8>
          <Input class=code8 name=SumActuPayMoneyRyan readonly></TD>
           <TD  class= title8>
            保全付费合计
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GetMoneyRyan readonly>
          </TD>
        </TR>   
        <TR>
        <TD  class= title8>
            最近一次续期费用
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LastSumGetMoneyRyan readonly>
          </TD>
           <TD  class= title8>
           满期金额
          </TD>
          <TD  class= input8>
          <Input class=code8 name=checkmoneyRyan readonly></TD>
          <TD  class= title8>
           续期收费共保分出合计
          </TD>
          <TD  class= input8>
          <Input class=common8 name=SumGetMoneyCoInsuRyan readonly></TD>
          <TD  class= title8>
           保全收费共保分出合计
          </TD>
          <TD  class= input8>
          <Input class=common8 name=ActuPayMoneyCoInsuRyan readonly></TD>
        </TR>
        <TR>
        <TD  class= title8>
            保全付费共保分出合计
          </TD>
          <TD  class= input8>
            <Input class=common8 name=GetMoneyCoInsuRyan readonly>
          </TD>
          <TD  class= title8>
            满期金额共保分出合计
          </TD>
          <TD  class= input8>
            <Input class=common8 name=CheckMoneyCoInsuRyan readonly>
          </TD>
           <TD  class= title8>
            首期收费共保分出合计
          </TD>
          <TD  class= input8>
            <Input class=common8 name=theFirstPremCoInsuRyan readonly>
          </TD>
       </table>   
        <p><font color="#ff0000"> 
       1.期缴保费：如果是“约定缴费”的保单，显示首期保费。<br>
       2.首期收费合计：首期实收保费合计,不包括共保分出保费。<br> 
       3.续期收费合计：续期实收保费合计,不包括共保分出保费。<br> 
       4.保全收费合计：保全的实收保费合计,不包括共保分出保费。<br> 
       5.保全付费合计：保全的付费合计，不包括满期给付金,不包括共保分出保费。<br> 
       6.最近一次续期费用：显示最近一期的续期实收保费，如果没有续期，此处显示为0。<br> 
       7.满期金额：团单的满期给付金，包括特需险、安康专家日常看护（A款）团体护理保险等险种的满期给付金,不包括共保分出保费。<br> 
       8.首期收费共保分出合计: 首期实收保费共保分出合计。 <br> 
	   9.续期收费共保分出合计: 续期实收保费共保分出合计。<br> 
	   10.保全收费共保分出合计: 保全的实收保费共保分出合计。<br> 
	   11.保全付费共保分出合计: 保全的付费共保分出合计。<br> 
	   12.满期金额共保分出合计: 团单的满期给付金共保分出合计。<br> 
       </font><br>
       </p> 
    </Div>
    <!--  
	 <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSpec);">
    		</td>
    		<td class= titleImg> 特别缴费约定</td>  			
			<td class=common><Input class= common1 name=specExsit  readonly></td>
			<td><INPUT class=cssButton VALUE="查询" TYPE=button onclick="specQuery();"> </td>
    	</tr>
    </table>
     -->
    <Div  id= "divSpec" style= "display:none "> 
      	<table  class= two>
       		<tr class=common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanSpecGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    </DIV>   
    <table>
      <tr>
        <td class=common>
		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divServ);">
    	</td>
    	<td class= titleImg>
    		保单服务约定
        </td>
      </tr>
    </table>
	<Div  id= "divServ" style= "display: "> 
     <table class= three>
       <TR  class= common>
          <TD  class= title8>
            变更提交方式
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpSubmitType readonly>
          </TD>
          <TD  class= title8>
            提交频次
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpSubmitPayv readonly>
          </TD>
          <TD  class= title8>
            结算频次
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpComPayv readonly>
          </TD>       
        </TR> 
        <TR  class= common>
          <TD  class= title8>
            理赔提交方式
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpSubmitTypeLL readonly>
          </TD>
          <TD  class= title8>
            理赔金领取人
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpSubmitPersonLL readonly>
          </TD>
          <TD  class= title8>
            理赔金支付方式
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpComMoneyPayWayLL readonly>
          </TD>       
        </TR> 
        <TR  class= common>
          <TD  class= title8 colspan="8">
            团体理赔金账户信息
          </TD>
        </TR> 
        <TR  class= common>
          <TD  class= title8>
            开户银行
          </TD>
          <TD  class= input8 style="display:none">
            <Input class= common8 name=ClaimBankCode readonly>
          </TD>
          <TD  class= input8>
            <Input class= common8 name=ClaimBankName readonly>
          </TD>
          <TD  class= title8>
            理赔金帐号
          </TD>
          <TD  class= input8>
            <Input class= common8 name=ClaimBankAccNo readonly>
          </TD>
          <TD  class= title8>
            理赔金户名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=ClaimAccName readonly>
          </TD>       
        </TR> 
      </table>
    </DIV> 
    
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAppAccTrace);">
    		</td>
    		<td class= titleImg>
    			 保费余额账户信息
    		</td>
    		<td class=common>
    			 <INPUT VALUE="历史查询" class = cssbutton TYPE=button onclick="quryAccHistroy();">
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAppAcc" style= "display: ''" align = center>
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  					<span id="spanAppAccGrid" >
  					</span> 
  			  </td>
  			</tr>
    	</table>
    	<Div id= "divPage5" align="center" style= "display: 'none' ">
        <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage5.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage5.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage5.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage5.lastPage();">
    	</Div>
  	</div>
    
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAppAcc);">
    		</td>
    		<td class= titleImg>
    			 账户轨迹信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAppAccTrace" style= "display: ''" align = center>
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  					<span id="spanAppAccTraceGrid" >
  					</span> 
  			  </td>
  			</tr>
    	</table>
    	<Div id= "divPage6" align="center" style= "display: 'none' ">
        <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage6.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage6.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage6.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage6.lastPage();">
    	</Div>
  	</div>
    
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPol1);">
    		</td>
    		<td class= titleImg>
    			 保障信息：
    		</td>
			<td class= titleImg>
    			 保费合计：
    		</td>
			<TD  class= input>
            <Input class= common name=SumPrem readonly>
          </TD>    
    	</tr>
    </table>
  	<Div  id= "divPol1" style= "display: ''" align = center>
      	<table  class= common>
      	<tr class= common>
      	<td class= titleImg><font color="#ff0000"> 
    			保费合计是首期收费、续期收费、保全收费、保全付费的合计，不包括满期给付金 </font> 
    		</td>
      	</tr>
      	<tr class= common>
      	<td class= titleImg><font color="#ff0000"> 
    			 若保单为&lsquo;约定缴费&rsquo;的保单，&lsquo;期交保费&rsquo;为首期保费</font> 
    		</td>
      	</tr>
      	<tr class= common>
      	<td class= titleImg><font color="#ff0000"> 
    			 录入的约定保费计划可以在“保障计划”处，进行查询。</font> 
    		</td>
      	</tr>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div id= "divPage4" align="center" style= "display: 'none' ">
		    <INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage4.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage4.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage4.nextPage();"> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage4.lastPage();"> 			
      </Div>
  	</Div>
	<br>
	<%--  因为所有按钮显示在一行会很长，所以将table注释掉，改用div
	<table class= common border=0 width=100%>
	  <tr>
		 <td><INPUT class=CssButton VALUE="被保人清单" TYPE=button onclick="insuredQueryClick();"></td> 
		 <td><INPUT class=CssButton VALUE="保单首/续期缴费明细" TYPE=button onclick="payQueryClick();"></td>
		 <td><INPUT class=CssButton VALUE="投保书查询" TYPE=button onclick="ScanQuery();"> </td>
		 <td><INPUT class=CssButton VALUE="保全信息" TYPE=button onclick="taskQueryClick();"> </td>
		 <td><INPUT class=CssButton VALUE="理赔明细" TYPE=button onclick="claimQueryClick();"> </td>
		 <td><INPUT class=CssButton VALUE="保障计划" TYPE=button onclick="grpRiskPlanInfo();"> </td>	
		 <td><INPUT class=CssButton VALUE="特需医疗事项查询" TYPE=button onclick="grpSpecQuery();"> </td>		 	 
		 <td><input class="cssButton" type="button" id="btnCoInsuranceParam" name="btnCoInsuranceParam" value="共保要素信息" onclick="showCoInsuranceParamWin();" disabled="disabled" /></td>
		 <td><input class="cssButton" type="button" id="btnBalance" name="btnBalance" value="社保项目要素信息" onclick="initBalance();"/></td>
		 <td><input class=cssButton type=button name=btnPubAccInfo value="账户信息查询" onclick="pubAccInfoQuery();"></td>
	  	 <td><input class=cssButton type=button name=btnDefGradeRate value="查询伤残等级给付比例" onclick="defGradeRate();"></td>
		 <td><input class=cssButton type=button name=BPQuery value="商团险大项目查询" onclick="bigProjectQuery();"></td>
		 <td><INPUT class=cssButton VALUE="返  回" TYPE=button onclick="GoBack();"> </td>
	  </tr>
  	</table>
  	--%>
  	<hr/>
  	<div id="buttonGroups1">
  		<INPUT class=CssButton VALUE="被保人清单" TYPE=button onclick="insuredQueryClick();"> 
		<INPUT class=CssButton VALUE="保单首/续期缴费明细" TYPE=button onclick="payQueryClick();">
		<INPUT class=CssButton VALUE="投保书查询" TYPE=button onclick="ScanQuery();"> 
		<INPUT class=CssButton VALUE="保全信息" TYPE=button onclick="taskQueryClick();"> 
		<INPUT class=CssButton VALUE="理赔明细" TYPE=button onclick="claimQueryClick();"> 
		<INPUT class=CssButton VALUE="保障计划" TYPE=button onclick="grpRiskPlanInfo();"> 	
		<INPUT class=CssButton VALUE="特需医疗事项查询" TYPE=button onclick="grpSpecQuery();"> 		 	 
		<input class="cssButton" type="button" id="btnCoInsuranceParam" name="btnCoInsuranceParam" value="共保要素信息" onclick="showCoInsuranceParamWin();" disabled="disabled" />
		<input class="cssButton" type="button" id="btnBalance" name="btnBalance" value="社保项目要素信息" onclick="initBalance();"/>
		<input class=cssButton type=button name=btnPubAccInfo value="账户信息查询" onclick="pubAccInfoQuery();">
  	</div><br/>
  	<div id="buttonGroups2">
  		<input class=cssButton type=button name=btnDefGradeRate value="查询伤残等级给付比例" onclick="defGradeRate();">
		<input class=cssButton type=button name=BPQuery value="商团险大项目查询" onclick="bigProjectQuery();">
		<input class=cssButton type=button name=RoadQuery value="一带一路产品要素查询" onclick="LCRoadQuery();">
		<INPUT class=cssButton VALUE="返  回" TYPE=button onclick="GoBack();">
  	</div>
  	<hr/>
  	<input name= "AppntNo" type=hidden>
  	<input name= "PrtNo" type=hidden>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>


