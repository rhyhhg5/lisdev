<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//修改：刘岩松
//修改日期：2004-2-17
%>
<%
	String tPolNo = "";
	String tIsCancelPolFlag = "";
	try
	{
		tPolNo = request.getParameter("PolNo");
		tIsCancelPolFlag = request.getParameter("IsCancelPolFlag");
	}
	catch( Exception e )
	{
		tPolNo = "";
		tIsCancelPolFlag = "0";
	}
%>
<head >
<script> 
	var tPolNo = "<%=tPolNo%>";  
	var tIsCancelPolFlag = "<%=tIsCancelPolFlag%>";
	var codeSql = "";
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="ChangePolStateDetail.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="ChangePolStateDetailInit.jsp"%>
	
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>保单查询 </title>
</head>

<body  onload="initForm();easyQueryClick();" >
 <form method=post name=fm target="fraSubmit" action="./ChangePolStateChk.jsp"> 
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" onclick="getPolGridCho();" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
              <!--tr-->		
		<!--td><INPUT class=common VALUE="交费查询" TYPE=button onclick="FeeQueryClick();"> </td!-->
		<!--td><INPUT class=common VALUE="给付查询" TYPE=button onclick="GetQueryClick();"> </td!-->
		<!--td><!--INPUT class=common VALUE="批改补退费查询" TYPE=button onclick="EdorQueryClick();"-->
		<!--INPUT class=common VALUE="保全查询" TYPE=button onclick="PerEdorQueryClick();"> </td!-->
		<!--td><INPUT class=common VALUE="保单明细查询" TYPE=button onclick="getQueryDetail();"> </td-->
		<!--INPUT class=common VALUE="主险查询" TYPE=button onclick="MainRiskQuery();"--> 
		<!--INPUT class=common VALUE="附加险查询" TYPE=button onclick="OddRiskQuery();"--> 
		<!--td><INPUT class=common VALUE="暂交费查询" TYPE=button onclick="TempFeeQuery();"> </td!-->
		<!--td><INPUT class=common VALUE="保费项查询" TYPE=button onclick="PremQuery();"> </td!-->
		<!--/tr-->	
		<!--tr-->	
		<!--td><INPUT class=common VALUE="垫交/借款查询" TYPE=button onclick="LoLoanQuery();"> </td-->
		<!--<td><INPUT class=common VALUE="借款查询" TYPE=button onclick="LoanQuery();"> </td>-->
		<!--td><INPUT class=common VALUE="给付项查询" TYPE=button onclick="GetItemQuery();"> </td-->
		<!--td><INPUT class=common VALUE="账户查询" TYPE=button onclick="InsuredAccQuery();"> </td-->
		<!--td><INPUT class=common VALUE="理赔查询" TYPE=button onclick="ClaimGetQuery();"> </td-->
		<!--td><INPUT class=common VALUE="扫描件查询" TYPE=button onclick="ScanQuery();"> </td-->
		<!--/tr-->
 </Div>
 <Div  id= "divPol2" style= "display: none">
   <table  class= common> 	
 <tr><td class= titleImg align= center>保单历史状态信息：</td></tr>
  <tr>
  <TD  class= title> 历史状态编码 </TD>
  <TD  class= input><Input class="code" name=OldPolState verify="保单状态编码|notnull&code:PolState" ondblclick="return showCodeList('PolState',[this], [0]);" onkeyup="return showCodeListKey('PolState', [this], [1]);"></TD> 
  <!--TD  class= input><Input class="code" name=OldPolState verify="保单状态编码|notnull&code:PolState" ondblclick="return showCodeList('PolState',[this, OldPolStateDesc], [0, 1]);" onkeyup="return showCodeListKey('PolState', [this, OldPolStateDesc], [1, 0]);"></TD--> 
  <!--TD  class= title> 历史状态描述 </TD>
  <TD  class= input> <Input class= readonly name=OldPolStateDesc > </TD-->  
  <TD  class= title> 历史状态导致原因编码 </TD>
  <TD  class= input><Input class="code" name=OldPolStateReason verify="代理人编码|notnull&code:PolStateReason" ondblclick="return showCodeList('PolStateReason',[this], [0]);" onkeyup="return showCodeListKey('PolStateReason', [this], [1]);"></TD> 
  <!--TD  class= input><Input class="code" name=OldPolStateReason verify="代理人编码|notnull&code:PolStateReason" ondblclick="return showCodeList('PolStateReason',[this, OldPolStateReasonDesc], [0, 1]);" onkeyup="return showCodeListKey('PolStateReason', [this, OldPolStateReasonDesc], [1, 0]);"></TD> 
  <TD  class= title> 历史状态导致原因描述 </TD>
  <TD  class= input> <Input class= readonly name=OldPolStateReasonDesc > </TD-->  
 </tr>
 </table>
 <table  class= common> 	
 <tr><td class= titleImg align= center>保单当前状态信息：</td></tr>
  <tr>
  <TD  class= title> 当前状态编码 </TD>
  <TD  class= input><Input class="code" name=CurrPolState verify="保单状态编码|notnull&code:PolState" ondblclick="return showCodeList('PolState',[this], [0]);" onkeyup="return showCodeListKey('PolState', [this], [1]);"></TD> 
  <!--TD  class= input><Input class="code" name=CurrPolState verify="保单状态编码|notnull&code:PolState" ondblclick="return showCodeList('PolState',[this, CurrPolStateDesc], [0, 1]);" onkeyup="return showCodeListKey('PolState', [this, CurrPolStateDesc], [1, 0]);"></TD> 
  <TD  class= title> 当前状态描述 </TD>
  <TD  class= input> <Input class= readonly name=CurrPolStateDesc > </TD-->  
  <TD  class= title> 当前状态导致原因编码 </TD>
  <TD  class= input><Input class="code" name=CurrPolStateReason verify="代理人编码|notnull&code:PolStateReason" ondblclick="return showCodeList('PolStateReason',[this], [0]);" onkeyup="return showCodeListKey('PolStateReason', [this], [1]);"></TD> 
  <!--TD  class= input><Input class="code" name=CurrPolStateReason verify="代理人编码|notnull&code:PolStateReason" ondblclick="return showCodeList('PolStateReason',[this, CurrPolStateReasonDesc], [0, 1]);" onkeyup="return showCodeListKey('PolStateReason', [this, CurrPolStateReasonDesc], [1, 0]);"></TD> 
  <TD  class= title> 当前状态导致原因描述 </TD>
  <TD  class= input> <Input class= readonly name=CurrPolStateReasonDesc > </TD-->  
 </tr>
 </table>
 
 <table  class= common> 	
 <tr><td class= titleImg align= center>强制转变当前状态信息：</td></tr>
  <tr>
  <TD  class= title> 转变后状态编码 </TD>
  <TD  class= input><Input class="code" name=NewPolState verify="转变后状态编码|notnull&code:NewPolState" ondblclick="return showCodeList('PolState2',[this, NewPolStateDesc], [0, 1]);" onkeyup="return showCodeListKey('PolState2', [this, NewPolStateDesc], [1, 0]);"></TD> 
  <TD  class= title> 转变后状态描述 </TD>
  <TD  class= input><Input class="readonly" readonly name=NewPolStateDesc verify="转变后状态描述|notnull&len<=12" ></TD> 
  </tr>
 <tr>
  <TD  class= title> 转变状态原因编码 </TD>
  <TD  class= input><Input class="code" name=NewPolStateReason verify="转变状态原因编码|notnull&code:NewPolStateReason" ondblclick="return showCodeList('PolStateReason',[this, NewPolStateReasonDesc],[0, 1],null,codeSql,'1',1);" onkeyup="return showCodeListKey('PolStateReason', [this, NewPolStateReasonDesc],[0, 1],null,codeSql,'1',1);"></TD> 
  <TD  class= title> 转变状态原因描述 </TD>
  <TD  class= input><Input class="readonly" readonly name=NewPolStateReasonDesc verify="转变状态原因描述|notnull&len<=12" ></TD> 
 </tr>
 </table>
 <table> 
 <td><INPUT class=common VALUE="提交修改信息" TYPE=button onclick="ModifyInfo();"> </td>  
</table>  
 </Div>
 <!--td><INPUT class=common VALUE="返回" TYPE=button onclick="GoBack();"> </td-->
<INPUT  type= "hidden" class= Common name= PolNo value= "">

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script>
var turnPage = new turnPageClass(); 
function easyQueryClick()
{
	
	//alert("here");
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	if(tIsCancelPolFlag=="0"){
	 strSQL = "select LCPol.grppolno,LCPol.PolNo,LCPol.PrtNo,LCPol.AppntName,LCPol.InsuredName,LCPol.RiskCode,LCPol.ManageCom from LCPol where MainPolNo='" + tPolNo + "' order by LCPol.proposalno";			 
	}
	else
	 if(tIsCancelPolFlag=="1"){//销户保单查询
	 strSQL = "select LCPol.grppolno,LCPol.PolNo,LCPol.PrtNo,LCPol.AppntName,LCPol.InsuredName,LCPol.RiskCode,LCPol.ManageCom from LCPol where MainPolNo='" + tPolNo + "' order by LCPol.proposalno";			 
	}
	else
	{
	  alert("保单类型传输错误!");
	  return ;
	}
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}
</script>
</html>


