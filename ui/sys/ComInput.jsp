<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 17:44:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="ComInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="ComInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ComSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>


    <table>
    	<tr>
    		<td>
    		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCom1);">
    		</td>
    		 <td class= titleImg>
        		机构管理
       		 </td>
    	</tr>
    </table>
    <Div  id= "divCom1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            机构编码
          </TD>
          <TD  class= input>
            <Input class= common name=ComCode >
          </TD>
          <TD  class= title>
            对外公布的机构代码
          </TD>
          <TD  class= input>
            <Input class= common name=OutComCode >
          </TD>
        </TR>
        <TR>
        <TD class = title>
        保单登记平台对应代码
        </TD>
        <TD class = input >
          <Input class = common name =PolicyRegistCode onchange="return checkpolicyregistcode();">
        </TD>
        <TD  class=input colSpan= 4>
			<font color='red'>注：机构编码为8位时，“保单登记平台对应代码”字段为必录项</font>
		</TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            机构名称
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
          <TD  class= title>
            简称
          </TD>
          <TD  class= input>
            <Input class= common name=ShortName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            机构地址
          </TD>
          <TD  class= input>
            <Input class= common name=Address >
          </TD>
          <TD  class= title>
            机构邮编
          </TD>
          <TD  class= input>
            <Input class= common name=ZipCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            机构电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone >
          </TD>
          <TD  class= title>
            机构传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            EMail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
          <TD  class= title>
            网址
          </TD>
          <TD  class= input>
            <Input class= common name=WebAddress >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            主管人姓名
          </TD>
          <TD  class= input>
            <Input class= common name=SatrapName >
          </TD>
          <TD  class= title>
            开办标志
          </TD>
	       <TD CLASS=input>
	          <Input name=Sign CLASS=codeNo CodeData="0|^0|未开业 ^1|开业" ondblclick="return showCodeListEx('Sign',[this,SignName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Sign',[this,SignName],[0,1],null,null,null,1);"><input class=codename name=SignName readonly=true>
	       </TD>	          
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            客户服务中心邮编
          </TD>
          <TD  class= input>
            <Input class= common name=ServicePostZipcode >
          </TD>            
          <TD  class= title>
            全国客户服务热线
          </TD>
          <TD  class= input>
            <Input class= common name=ServicePhone >
          </TD>        
        </TR>    
        <TR  class= common>
          <TD  class= title >
            客户服务中心地址
          </TD>
          <TD  class= input COLSPAN=3>
            <Input class= common3 name=ServicePostAddress >
          </TD>
        </TR> 
        <TR  class= common>
          <TD  class= title>
            信函服务邮编
          </TD>
          <TD  class= input>
            <Input class= common name=LetterServicePostZipcode >
          </TD>        	
          <TD  class= title>
            信函服务机构名称
          </TD>
          <TD  class= input>
            <Input class= common name=LetterServiceName >
          </TD>          
         </TR>

        <TR  class= common>
          <TD  class= title>
            信函服务地址
          </TD>
          <TD  class= input COLSPAN=3>
            <Input class= common3 name=LetterServicePostAddress >
          </TD>
        </TR>  

        <TR  class= common>
          <TD  class= title>
            分公司客户服务电话
          </TD>
          <TD  class= input>
            <Input class= common name=ServicePhone1 >
          </TD>        	
          <TD  class= title>
            分公司客户服务电话2
          </TD>
          <TD  class= input>
            <Input class= common name=ServicePhone2 >
          </TD>          
         </TR>
        <TR  class= common>
          <TD  class= title>
            理赔报案电话
          </TD>
          <TD  class= input>
            <Input class= common name=ClaimReportPhone >
          </TD>        	
          <TD  class= title>
            体检预约电话
          </TD>
          <TD  class= input>
            <Input class= common name=PEOrderPhone >
          </TD>          
         </TR>
        <TR  class= common>
          <TD  class= title>
            备用电话1
          </TD>
          <TD  class= input>
            <Input class= common name=BackupPhone1 >
          </TD>        	
          <TD  class= title>
            备用电话2
          </TD>
          <TD  class= input>
            <Input class= common name=BackupPhone2 >
          </TD>          
         </TR> 
        <TR  class= common>
          <TD  class= title>
            备用地址1
          </TD>
          <TD  class= input COLSPAN=3>
            <Input class= common3 name=BackupAddress1 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            备用地址2
          </TD>
          <TD  class= input COLSPAN=3>
            <Input class= common3 name=BackupAddress2 >
          </TD>
        </TR> 
        <TR  class= common>
          <TD  class= title>
            机构英文名称
          </TD>
          <TD  class= input>
            <Input class= common name=EName >
          </TD>        	
          <TD  class= title>
            英文简称
          </TD>
          <TD  class= input>
            <Input class= common name=EShortName >
          </TD>          
         </TR> 
        <TR  class= common>
          <TD  class= title>
            机构英文地址
          </TD>
          <TD  class= input>
            <Input class= common name=EAddress >
          </TD>        	
          <TD  class= title>
            客户服务中心英文地址
          </TD>
          <TD  class= input>
            <Input class= common name=EServicePostAddress >
          </TD>          
         </TR> 
        <TR  class= common>
          <TD  class= title>
            信函服务机构英文名称
          </TD>
          <TD  class= input>
            <Input class= common name=ELetterServiceName >
          </TD>        	
          <TD  class= title>
            信函服务英文地址
          </TD>
          <TD  class= input>
            <Input class= common name=ELetterServicePostAddress >
          </TD>          
         </TR> 
        <TR  class= common>
          <TD  class= title>
            备用英文地址1
          </TD>
          <TD  class= input>
            <Input class= common name=EBackupAddress1 >
          </TD>        	
          <TD  class= title>
            备用英文地址2
          </TD>
          <TD  class= input>
            <Input class= common name=EBackupAddress2 >
          </TD>   
          <TD  class= title style="display:'none'" >
            机构简称
          </TD>
          <TD  class= input style="display:'none'" >
            <Input class= common name=ShowName >
          </TD>        
         </TR>  
         <TR  class= common>
          <TD  class= title>
           税务登记号
          </TD>
          <TD  class= input>
            <Input class= common name=TaxRegistryNo >
          </TD>  
          <TD  class= title>
            县级机构标志
          </TD>
	       <TD CLASS=input>
	          <Input name=ContyFlag CLASS=codeNo CodeData="0|^0|非县级机构 ^1|县级机构" ondblclick="return showCodeListEx('ContyFlag',[this,ContyFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContyFlag',[this,ContyFlagName],[0,1],null,null,null,1);"><input class=codename name=ContyFlagName readonly=true>
	       </TD>
          </TR>   
          <tr>
          <TD  class= title>
           地区代码
          </TD>
          <TD  class= input>
            <Input class= common name=AreaCode >
          </TD> 
          <TD  class=input colSpan= 4>
			<font color='red'>注：机构编码为8位时，“地区代码”字段为必录项</font>
		</TD> 
          </tr>                                                                                                          	                   
      </table>
    </Div>
    <table>
        <tr>
            <td><img src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPrintComInfo);" /></td>
            <td class="titleImg">打印专用信息</td>
        </tr>
    </table>
    <div id= "divPrintComInfo" style= "display:'';">
        <table class="common">
            <tr class="common">
                <td class="title">机构打印时显示名称</td>
                <td class="input"><input class="common" name="PrintComName" /></td>
                <td class="title">所属上级机构</td>
                <td class="input"><input class="common" name="SuperComCode" /></td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="fmtransact" name="fmtransact" />
  </form>
  <span id="spanCode" style="display:none; position:absolute; slategray;"></span>
</body>
</html>
