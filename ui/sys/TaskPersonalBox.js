//程序名称：TaskPersonalBox.js
//程序功能：工单管理个人信箱页面
//创建日期：2005-01-17 11:03:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();

var mType = 0;
var mType2 = 0;

/*********************************************************************
 *  查询保全申请在列表中显示结果
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{	
	
	//初始化表格
	initGrid();
	
	//查询SQL语句
	var strSQL;
	strSQL =  "select w.WorkNo, w.AcceptNo, w.PriorityNo, w.TypeNo, w.CustomerNo, " +
				 "		w.CustomerName, '记录', t.SendPersonNo, t.InDate, w.AcceptCom, " +
				 "		Case When w.DateLimit Is Null then '' Else trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
				 "		w.StatusNo, e.EdorState, (Select count(NodeNo) from LGWorkRemark where WorkNo = w.WorkNo), " +
				 "		case ScanFlag " +
				 "			when '0' then '新扫描件' " +
				 "			when '1' then '旧扫描件' " +
				 "			else '无扫描件' " +
				 "		end, " +
				 "		w.detailWorkNo, w.StatusNo, e.EdorState " +
				 "from LGWork w, LGWorkTrace t, lpedorapp e " +
		         "where  w.WorkNo = t.WorkNo " +
		      	 "and    w.NodeNo = t.NodeNo " +
		      	 "and	 e.EdorAcceptNo=w.workNo " +
				 "and    t.WorkBoxNo = " +
				 "      (select WorkBoxNo from LGWorkBox " + 
				 "       where OwnerNo = '" + operator + "' " +
				 "       and   OwnerTypeNo = '2') " +
	        " and w.CustomerNo='" + tCustomer + "'"
		   ;

	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	  
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		return false;
	}
   
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = TaskGrid;    
	      
	//保存SQL语句
	turnPage.strQuerySql = strSQL; 
	
	//设置查询起始位置
	turnPage.pageIndex = 0;
	
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

	return true;
}
/*********************************************************************
 *  查询咨询/投诉申请在列表中显示结果
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick1()
{	
	
	//初始化表格
	initGrid1();
	
	//查询SQL语句
	var strSQL;
	strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, w.TypeNo, w.CustomerNo, " +
			 "       (Select Name from LDPerson where CustomerNo = w.CustomerNo), '记录', " +
			 "       t.SendPersonNo, t.InDate, w.AcceptCom, " +
			 "       Case When w.DateLimit Is Null then '' Else  trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
			 "       w.StatusNo, (Select count(NodeNo) from LGWorkRemark where WorkNo = w.WorkNo), '' " +
			 "from   LGWork w, LGWorkTrace t " +
	         "where  w.WorkNo = t.WorkNo " +
	      	 "and    w.NodeNo = t.NodeNo " +
			 "and    t.WorkBoxNo = " +
			 "      (select WorkBoxNo from LGWorkBox " + 
			 "       where OwnerNo = '" + operator + "' " +
			 "       and   OwnerTypeNo = '2') and substr(w.TypeNo,1,2) ='01'"
	        + " and w.CustomerNo='" + tCustomer + "'"
	        ;
	
	turnPage1.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	  
	//判断是否查询成功
	if (!turnPage1.strQueryResult) {
		return false;
	}
   
	//查询成功则拆分字符串，返回二维数组
	turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
	
	//设置初始化过的MULTILINE对象
	turnPage1.pageDisplayGrid = Task1Grid;    
	      
	//保存SQL语句
	turnPage1.strQuerySql = strSQL; 
	
	//设置查询起始位置
	turnPage1.pageIndex = 0;
	
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex, MAXSCREENLINES);
	
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);

	return true;
}

/*********************************************************************
 *  查询理赔申请在列表中显示结果
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick2()
{	
	
	//初始化表格
	initGrid2();
	
	//查询SQL语句
	var strSQL;
	strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, w.TypeNo, w.CustomerNo, " +
			 "       (Select Name from LDPerson where CustomerNo = w.CustomerNo), '记录', " +
			 "       t.SendPersonNo, t.InDate, w.AcceptCom, " +
			 "       Case When w.DateLimit Is Null then '' Else  trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
			 "       w.StatusNo, (Select count(NodeNo) from LGWorkRemark where WorkNo = w.WorkNo), '' " +
			 "from   LGWork w, LGWorkTrace t " +
	         "where  w.WorkNo = t.WorkNo " +
	      	 "and    w.NodeNo = t.NodeNo " +
			 "and    t.WorkBoxNo = " +
			 "      (select WorkBoxNo from LGWorkBox " + 
			 "       where OwnerNo = '" + operator + "' " +
			 "       and   OwnerTypeNo = '2') and substr(w.TypeNo,1,2) ='02'" 
		     + " and w.CustomerNo='" + tCustomer + "'"
	        ;
	

	turnPage2.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	  
	//判断是否查询成功
	if (!turnPage2.strQueryResult) {
		return false;
	}
   
	//查询成功则拆分字符串，返回二维数组
	turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult);
	
	//设置初始化过的MULTILINE对象
	turnPage2.pageDisplayGrid = Task2Grid;    
	      
	//保存SQL语句
	turnPage2.strQuerySql = strSQL; 
	
	//设置查询起始位置
	turnPage2.pageIndex = 0;
	
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex, MAXSCREENLINES);
	
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage2.pageDisplayGrid);

	return true;
}

/*********************************************************************
 *  查询投保申请在列表中显示结果
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick3()
{	
	
	//初始化表格
	initGrid3();
	
	//查询SQL语句
	var strSQL ="select * from LGWork where 1>2";
	/*strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, w.TypeNo, w.CustomerNo, " +
			 "       (Select Name from LDPerson where CustomerNo = w.CustomerNo), '记录', " +
			 "       t.SendPersonNo, t.InDate, w.AcceptCom, " +
			 "       Case When w.DateLimit Is Null then '' Else  trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
			 "       w.StatusNo, (Select count(NodeNo) from LGWorkRemark where WorkNo = w.WorkNo), '' " +
			 "from   LGWork w, LGWorkTrace t " +
	         "where  w.WorkNo = t.WorkNo " +
	      	 "and    w.NodeNo = t.NodeNo " +
			 "and    t.WorkBoxNo = " +
			 "      (select WorkBoxNo from LGWorkBox " + 
			 "       where OwnerNo = '" + operator + "' " +
			 "       and   OwnerTypeNo = '2') " 
			  + " and w.CustomerNo='" + tCustomer + "'"
	        ;*/
	

	turnPage3.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	  
	//判断是否查询成功
	if (!turnPage3.strQueryResult) {
		return false;
	}
   
	//查询成功则拆分字符串，返回二维数组
	turnPage3.arrDataCacheSet = decodeEasyQueryResult(turnPage3.strQueryResult);
	
	//设置初始化过的MULTILINE对象
	turnPage3.pageDisplayGrid = Task3Grid;    
	      
	//保存SQL语句
	turnPage3.strQuerySql = strSQL; 
	
	//设置查询起始位置
	turnPage3.pageIndex = 0;
	
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage3.getData(turnPage3.arrDataCacheSet, turnPage3.pageIndex, MAXSCREENLINES);
	
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage3.pageDisplayGrid);

	return true;
}

//选中一条工单后跳转到相应的处理页面
function linkToPage(pageUrl)
{
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = TaskGrid.getRowColData(i, 1);
				CustomerNo = TaskGrid.getRowColData(i, 5);
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}
	var width = screen.availWidth - 10;
	var height = screen.availHeight - 28;
	
	if (WorkNo)
	{
		 window.open(pageUrl + "WorkNo="+WorkNo +"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + WorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
		//location.replace(pageUrl + "WorkNo=" + WorkNo + "&CustomerNo=" + CustomerNo);
	}
}

//选中一条工单后跳转到相应的处理页面
function linkToPage1(pageUrl)
{
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	for (i = 0; i < Task1Grid.mulLineCount; i++)
	{
		if (Task1Grid.getChkNo(i)) 
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = Task1Grid.getRowColData(i, 1);
				CustomerNo = Task1Grid.getRowColData(i, 5);
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}
	
	var width = screen.availWidth - 10;
	var height = screen.availHeight - 28;
	if (WorkNo)
	{
		 window.open(pageUrl + "WorkNo="+WorkNo +"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + WorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
		//location.replace(pageUrl + "WorkNo=" + WorkNo + "&CustomerNo=" + CustomerNo);
	}
}

//选中一条工单后跳转到相应的处理页面
function linkToPage2(pageUrl)
{
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	for (i = 0; i < Task2Grid.mulLineCount; i++)
	{
		if (Task2Grid.getChkNo(i)) 
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = Task2Grid.getRowColData(i, 1);
				CustomerNo = Task2Grid.getRowColData(i, 5);
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}
	
	var width = screen.availWidth - 10;
	var height = screen.availHeight - 28;
	if (WorkNo)
	{
		 window.open(pageUrl + "WorkNo="+WorkNo +"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + WorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
		//location.replace(pageUrl + "WorkNo=" + WorkNo + "&CustomerNo=" + CustomerNo);
	}
}

//选中一条工单后跳转到相应的处理页面
function linkToPage3(pageUrl)
{
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	for (i = 0; i < Task3Grid.mulLineCount; i++)
	{
		if (Task3Grid.getChkNo(i)) 
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = Task3Grid.getRowColData(i, 1);
				CustomerNo = Task3Grid.getRowColData(i, 5);
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}

	var width = screen.availWidth - 10;
	var height = screen.availHeight - 28;
	
	if (WorkNo)
	{
		window.open(pageUrl + "WorkNo="+WorkNo +"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + WorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
	}
}


/* 查看选中的工单 */
function viewTask(strFlag)
{
	var strPage = "../task/TaskViewMain.jsp?"
	if (strFlag == 0)
	{
		linkToPage(strPage);
	}
	if (strFlag == 1)
	{
		linkToPage1(strPage);
	}
	if (strFlag == 2)
	{
		linkToPage2(strPage);
	}
	if (strFlag == 3)
	{
		linkToPage3(strPage);
	}
}


/* 提交表单 */
function submitForm()
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit(); //提交
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit1(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		location.reload();
	}
}

/* 保存完成后的操作，由弹出窗口调用 */
function afterSubmit2(FlagStr, content, tUrl)
{
	if ((tUrl != null) && (tUrl != ""))
	{
		location.replace(tUrl);
	}
}    

/* 保存完成后的操作，由弹出窗口调用 */
function afterSubmit3(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		location.reload();
	}
}

