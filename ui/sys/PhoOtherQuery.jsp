<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PhoAgentQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);

    String Branch =tG.ComCode;

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

  <SCRIPT src="./PhoOther.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./PhoOtherInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>保单杂项信息 </title>
</head>
<body  onload="initForm();">
  <form action="./PhoOtherQuery.jsp" method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            <td class= titleImg>
                保单杂项信息
            </td>
            </td>
    	</tr>
    </table>
    <table  class= common>
      <TR>
    	 <TD  class= input> 
        </TD>
         <TD class= title> 
          开户银行
        </TD>
        <TD CLASS=input >
	     <Input NAME=BankCode VALUE="" CLASS="code" MAXLENGTH=20 verify="开户行|code:bank" ondblclick="return showCodeList('bank',[this]);" onkeyup="return showCodeListKey('bank',[this]);" >
	    </TD>	
        <TD  class= input READONLY> 
        </TD>
      </TR>
      <TR> 
        <TD  class= input> 
        </TD>
        <TD  class= title>
          银行账号
        </TD>
        <TD  class= input>
          <Input name=BankAccNo class= common READONLY>
        </TD> 
       
        <TD  class= input>
        </TD>
     </TR>
     <TR>
        <TD  class= input> 
        </TD>
         <TD  class= title>
          联系地址
        </TD>
        <TD  class= input>
          <Input name=PostalAddress class=common READONLY> 
        </TD>
        <TD  class= input>
        </TD>
      </TR>
      <TR>
        <TD  class= input> 
        </TD>
         <TD  class= title>
         邮编
        </TD>
        <TD  class= input>
          <Input name=ZipCode class=common READONLY > 
        </TD>
        <TD  class= input>
        </TD>
      </TR>
      <TR>
        <TD  class= input> 
        </TD>
        <TD  class= title>
          补发次数
        </TD>
        <TD  class= input> 
          <Input name=PrintCount class= common READONLY> 
        </TD>
        <TD  class= input>
        </TD>
    	</TR>
      <TR>
        <TD  class= input> 
        </TD>
        <TD  class= title>
          机构名称
        </TD>
        <TD  class= input> 
          <Input  name=ManageCom class= common  READONLY> 
        </TD>
        <TD  class= input>
        </TD>
      </TR>      
     </table> 
	 <table class= common>  
	   <TR>
        <TD  class= input> 
        </TD>
        <TD  class= title>
          特别约定
        </TD>
        <TD  class= input> 
        </TD>
      </TR>
      <TR>	
	    <TD  class= input> </TD>
		<TD  class= common>
             <textarea name="Remark" cols="55" rows="4" class="common" READONLY="TRUE"></textarea>
         </TD>
		 <TD  class= input> </TD>
	  </TR>
   </table> 
  </form>
</body>
</html>
