<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：LaratecommisionSetInput.jsp
		//程序功能：个团保单明细查询
		//创建时间：2009-3-16
		//创建人  ：miaoxiangzheng
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<script language="JavaScript">
//var StrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode)";
</script>
	<script language="JavaScript">
   var msql=" 1 and   char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000#  ";
   var StrSql="1 and code not in (select riskcode from lmriskapp where risktype4=#4#)";
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="GrpCDQListRiskInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="GrpCDQListRiskInit.jsp"%>
		<%@include file="../common/jsp/ManageComLimit.jsp"%>

	</head>

	<body onload="initForm();initElementtype();">
		<form action="./GrpCDQListReport.jsp" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divQryModify);">
					</td>
					<td class=titleImg>
						查询条件
					</td>
				</tr>
			</table>
			<div id="divQryModify" style="display:''">
				<table class=common>
					<tr class=common>
						<td class=title>
							管理机构
						</td>
						<TD class=input>
							<Input class='codeno' name=ManageCom
								verify="管理机构|notnull&code:ComCode "
								ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,1,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,1,1);"><Input class=codename name=ManageComName readOnly elementtype=nacessary>
						</TD>

						<td class=title>
							代理人编码
						</td>
						<TD class=input>
							<Input class="common" name=AgentCode>
						</TD>
					</tr>
					<tr>
						<TD class=title>
							保单录入起期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=StartDate>
						</TD>
						<TD class=title>
							保单录入止期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=EndDate>
						</TD>
					</tr>
					<tr>
						<TD class=title>
							保单生效起期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=StartCvaliDate>
						</TD>
						<TD class=title>
							保单生效止期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=EndCvaliDate>
						</TD>
					</tr>
					<tr>
						<TD class=title>
							保单签单起期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=StartSignDate>
						</TD>
						<TD class=title>
							保单签单止期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=EndSignDate>
						</TD>
					</tr>
					<tr>
						<TD class=title>
							市场类型
						</TD>
						<TD class=input colspan="3">
							<input type="checkbox" name="MarketType" value="1"/><span>商业&nbsp&nbsp</span>
							<input type="checkbox" name="MarketType" value="10"/><span>基本工伤</span>
							<input type="checkbox" name="MarketType" value="11"/><span>补充工伤</span>
							<input type="checkbox" name="MarketType" value="12"/><span>大病保险A</span>
							<input type="checkbox" name="MarketType" value="13"/><span>大病保险B</span>
							<input type="checkbox" name="MarketType" value="14"/><span>大病保险C</span>
							<input type="checkbox" name="MarketType" value="15"/><span>大病保险D</span>
							<input type="checkbox" name="MarketType" value="2"/><span>新农合&nbsp</span>
							<input type="checkbox" name="MarketType" value="3"/><span>新农合补充</span>
							<br>
							<input type="checkbox" name="MarketType" value="4"/><span>城镇职工基本医疗</span>
							<input type="checkbox" name="MarketType" value="5"/><span>城镇职工补充医疗</span>
							<input type="checkbox" name="MarketType" value="6"/><span>城镇居民基本医疗</span>
							<input type="checkbox" name="MarketType" value="7"/><span>城镇居民补充医疗</span>
							<input type="checkbox" name="MarketType" value="8"/><span>医疗救助</span>
							<input type="checkbox" name="MarketType" value="9"/><span>企事业团体补充医疗</span>
							<input type="checkbox" name="MarketType" value="99"/><span>其它</span>
						</TD>
					</tr>
					<tr>
						<TD class=title>
							销售渠道
						</TD>
						<TD class=input colspan="3">
							<Input type="checkbox" name="SaleChnl" value="02"/><span>团险直销</span>
							<input type="checkbox" name="SaleChnl" value="03"/><span>团险中介</span>
							<input type="checkbox" name="SaleChnl" value="04"/><span>银行代理</span>
							<input type="checkbox" name="SaleChnl" value="07"/><span>职团开拓</span>
							<input type="checkbox" name="SaleChnl" value="10"/><span>个险中介</span>
							<input type="checkbox" name="SaleChnl" value="14"/><span>互动直销</span>
							<input type="checkbox" name="SaleChnl" value="15"/><span>互动中介</span>
							<input type="checkbox" name="SaleChnl" value="20"/><span>社保综拓中介</span>
							<input type="checkbox" name="SaleChnl" value="06"/><span>个销团</span>
							<br>
							<input type="checkbox" name="SaleChnl" value="01"/><span>个人直销</span>
							<input type="checkbox" name="SaleChnl" value="13"/><span>银代直销</span>
							<input type="checkbox" name="SaleChnl" value="17"/><span>健管直销</span>
							<input type="checkbox" name="SaleChnl" value="21"/><span>电商直销</span>
							<input type="checkbox" name="SaleChnl" value="22"/><span>电商中介</span>
							<input type="checkbox" name="SaleChnl" value="19"/><span>个险续收</span>
							<input type="checkbox" name="SaleChnl" value="16"/><span>社保直销</span>
							<input type="checkbox" name="SaleChnl" value="18"/><span>社保综拓直销</span>
							
						</TD>
					</tr>
					<tr>
						<TD class=title>
							出单平台
						</TD>
						<TD class=input colspan="3">
							<Input type="checkbox" name="CardFlag" value="standardlgc"/><span>标准团单</span>
							<input type="checkbox" name="CardFlag" value="0"/><span>简易团单</span>
							<input type="checkbox" name="CardFlag" value="2"/><span>卡折出单</span>
							<input type="checkbox" name="CardFlag" value="3"/><span>网销业务</span>
							<input type="checkbox" name="CardFlag" value="b"/><span>电商业务</span>
							<input type="checkbox" name="CardFlag" value="ej"/><span>E家业务</span>
							<input type="checkbox" name="CardFlag" value="cd"/><span>一卡通出单</span>
							<input type="checkbox" name="CardFlag" value="4"/><span>补充工伤险业务</span>
						</TD>
					</tr>
					<tr>
						<td class=title>
							险种
						</td>
           				<TD  class= title8 colspan="3">
             				<textarea name="RiskCode" cols="110" rows="3" class="common" ></textarea>
           				</TD>
					</tr>
					<table>
						<tr class=common>
							<td>
								<font color=red size=2> 
								说明：<br>
								1、险种输入时，各险种之间以英文半角逗号隔开。<br>
								2、险种不输入，则默认查询全部险种。<br>
								3、若市场类型不勾选，则默认查询全部市场类型、若销售渠道不勾选，则默认查询全部销售渠道、若出单平台不勾选，则默认查询全部出单平台。<br>
									 
							</td>
						</tr>
					</table>
				</table>
			</div>
			<table>
				<tr>
					<td>
						<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
						<input type=button value="下  载" class=cssButton onclick="DoNewDownload();">
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSetGrid);">
					</td>
					<td class=titleImg>
						团体保单明细查询结果显示
					</td>
				</tr>
			</table>

			<div id="divSetGrid" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanSetGrid"></span>
						</td>
					</tr>
				</table>
				<INPUT VALUE=" 首页 " TYPE="button" class=cssButton onclick="turnPage.firstPage();">
				<INPUT VALUE="上一页" TYPE="button" class=cssButton onclick="turnPage.previousPage();">
				<INPUT VALUE="下一页" TYPE="button" class=cssButton onclick="turnPage.nextPage();">
				<INPUT VALUE=" 尾页 " TYPE="button" class=cssButton onclick="turnPage.lastPage();">
				<br>
				<hr>
			</div>
			<input type=hidden id="sql_where" name="sql_where">
			<input type=hidden id="fmAction" name="fmAction">
			<input type=hidden class=Common name=querySql>
			<Input class="readonly" type=hidden name=diskimporttype>
		</form>
		<table>
			<br />
			<tr class=common>
				<td>
					<font color=red size=2> 说明：<br />
						1、缴费方式为趸缴及约定缴费的，保费列显示为保单签单总保费，缴费方式为月缴、季缴、半年缴及年缴的，保费列显示为首期录入保费。<br>
						2、保单签单日期，保单生效日期和保单录入日期中，需要至少录入一项。<br>
						3、实收总保费为契约、续期及保全收付费的实收保费之和。不包括满期给付。<br>
				</td>
			</tr>
		</table>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>