<%
//程序名称：AppStatisticSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
ReportUI
</title>
<head>
</head>
<body>
<%

//String FileName = "GroupProduct_Income";
//String StartDate = "2005-01-01";
//String EndDate = "2005-01-11";
//String ManageCom = "86";
String flag = "0";
String FlagStr = "";
String Content = "";
String QYType_ch = StrTool.unicodeToGBK(request.getParameter("QYType_ch"));
String QYType = StrTool.unicodeToGBK(request.getParameter("QYType"));
String SQl1="";
String ManageCom = request.getParameter("ManageCom");
String AppntSex = request.getParameter("AppntSex");
String AppntOccupationCode = request.getParameter("AppntOccupationCode");
String Birthday = request.getParameter("Birthday");
String tBirthmonth="";
String tBirthday="";
String CValiDateStart=request.getParameter("CValiDateStart");
String CValiDateEnd=request.getParameter("CValiDateEnd");
System.out.println("QYType"+QYType);
System.out.println("QYType_ch"+QYType_ch);


//设置模板名称
String FileName = "";

//设置模板名称
if(QYType.equals("1"))
{
 FileName ="tbCustomerQueryStatistic";

if(!ManageCom.equals("")){
SQl1=SQl1+" and b.ManageCom like '"+ManageCom+"%' ";
}
if(!AppntSex.equals("")){
SQl1=SQl1+" and a.appntsex='"+AppntSex+"' ";
}
if(!AppntOccupationCode.equals("")){
SQl1=SQl1+" and a.OccupationCode='"+AppntOccupationCode+"' ";
}
if(!Birthday.equals("")){
 if ( Birthday.indexOf("-")>0){
    tBirthmonth = Birthday.substring( 0,Birthday.indexOf("-"));
		tBirthday =Birthday.substring(Birthday.lastIndexOf("-")+1);
	}
SQl1=SQl1+" and (select month(date(a.appntBirthday)) from dual)="+tBirthmonth+" and (select day(date(a.appntBirthday)) from dual)="+tBirthday+" ";
}
if(!CValiDateStart.equals("")||!CValiDateEnd.equals("")){

SQl1=SQl1+" and b.CValiDate >='"+CValiDateStart+"' and b.CValiDate <='"+CValiDateEnd +"'";
}
System.out.println("SQl1"+SQl1);

}else
{
	 FileName ="bbCustomerQueryStatistic";
	 
if(!ManageCom.equals("")){
SQl1=SQl1+" and a.ManageCom like '"+ManageCom+"%' ";
}
if(!AppntSex.equals("")){
SQl1=SQl1+" and a.InsuredSex='"+AppntSex+"' ";
}
if(!AppntOccupationCode.equals("")){
SQl1=SQl1+" and (select OccupationCode from lcinsured where insuredno=a.insuredno and prtno=a.prtno)='"+AppntOccupationCode+"' ";
}
if(!Birthday.equals("")){
 if ( Birthday.indexOf("-")>0){
    tBirthmonth = Birthday.substring( 0,Birthday.indexOf("-"));
		tBirthday =Birthday.substring(Birthday.lastIndexOf("-")+1);
	}
SQl1=SQl1+" and (select month(date(a.InsuredBirthday)) from dual)="+tBirthmonth+" and (select day(date(a.InsuredBirthday)) from dual)="+tBirthday+" ";
}
if(!CValiDateStart.equals("")||!CValiDateEnd.equals("")){

SQl1=SQl1+" and a.CValiDate >='"+CValiDateStart+"' and a.CValiDate <='"+CValiDateEnd +"'";
}
System.out.println("SQl1"+SQl1);
}

JRptList t_Rpt = new JRptList();
String tOutFileName = "";
if(flag.equals("0"))
{
t_Rpt.m_NeedProcess = false;
t_Rpt.m_Need_Preview = false;
t_Rpt.mNeedExcel = true;

String CurrentDate = PubFun.getCurrentDate();
CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");

t_Rpt.AddVar("ManageCom",ManageCom);
t_Rpt.AddVar("SQl1",SQl1);

t_Rpt.AddVar("ManageComName", ReportPubFun.getMngName(ManageCom));
 String YYMMDD = "";
YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
       + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
       + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("MakeDate", YYMMDD);


t_Rpt.Prt_RptList(pageContext,FileName);
tOutFileName = t_Rpt.mOutWebReportURL;
String strVFFileName = 
FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
String strRealPath = 
application.getRealPath("/web/Generated").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName; 
System.out.println("strVFPathName : "+ strVFPathName); 
System.out.println("=======Finshed in JSP==========="); 
System.out.println(tOutFileName);
response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+
"&RealPath="+strVFPathName);
}
%>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >