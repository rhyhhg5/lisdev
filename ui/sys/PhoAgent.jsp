<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PhoAgentQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./PhoAgent.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./PhoAgentInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>营销员查询 </title>
</head>
<body  onload="initForm();">
  <form action="./LAAgentQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            <td class= titleImg>
                业务员资料
            </td>
            </td>
    	</tr>
    </table>
    <table  class= common>
    	<TR>
    	 <TD  class= input> 
        </TD>
         <TD class= title> 
          业务员代码
        </TD>
        <TD  class= input> 
          <Input class=common  name=AgentCode >
        </TD> 
        <TD  class= input> 
        </TD>
    	</TR>
    	<TR> 
        <TD  class= input> 
        </TD>
        <TD  class= title>
          业务员姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD> 
       
        <TD  class= input>
        </TD>
    	</TR>
    	<TR>
        <TD  class= input> 
        </TD>
         <TD  class= title>
          业务员性别 
        </TD>
        <TD  class= input>
          <Input name=Sex class=common > 
        </TD>
        <TD  class= input>
        </TD>
    	</TR>
    	<TR>
        <TD  class= input> 
        </TD>
         <TD  class= title>
         证件类型
        </TD>
        <TD  class= input>
          <Input name=IDNoStyle class=common > 
        </TD>
        <TD  class= input>
        </TD>
    	</TR>
    	<TR>
        <TD  class= input> 
        </TD>
        <TD  class= title>
          身份证号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common > 
        </TD>
        <TD  class= input>
        </TD>
    	</TR>
    	<TR>
        <TD  class= input> 
        </TD>
        <TD  class= title>
          业务员电话
        </TD>
        <TD  class= input> 
          <Input name=PhoneNo class= common > 
        </TD>
        <TD  class= input>
        </TD>
    	</TR>
    	<TR>
        <TD  class= input> 
        </TD>
        <TD  class= title>
          业务员入司时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployeeDate class= common > 
        </TD>
        <TD  class= input>
        </TD>
    	</TR>
      <TR>
        <TD  class= input> 
        </TD>
        <TD  class= title>
          业务员离职时间 
        </TD>
        <TD  class= input> 
          <Input name=DimissionDate class= common > 
        </TD>
        <TD  class= input>
        </TD>
    	</TR>
    	 <TR>
        <TD  class= input> 
        </TD>
        <TD  class= title>
          业务员职级 
        </TD>
        <TD  class= input> 
          <Input name=AgentGrade class= common > 
        </TD>
        <TD  class= input>
        </TD>
    	</TR>
    	 <TR>
        <TD  class= input> 
        </TD>
        <TD  class= title>
          机构名称 
        </TD>
        <TD  class= input> 
          <Input name=AgentGroup class= common > 
        </TD>
        <TD  class= input>
        </TD>
    	</TR>
     </table>              				
   
  </form>
</body>
</html>
