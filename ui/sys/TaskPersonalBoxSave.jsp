<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskPersonalBoxSava.jsp
//程序功能：工单管理经办页面
//创建日期：2005-02-24 15:54:20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	String tUrl = "";
	
	VData tVData = new VData();
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	//得到经办路径
	String tWorkNo = request.getParameter("WorkNo");
	tVData.add(tWorkNo);
	tVData.add(tGI);
	TaskDoBusinessBL tTaskDoBusinessBL = new TaskDoBusinessBL();
	if (tTaskDoBusinessBL.submitData(tVData, "") == false)
	{
		FlagStr = "Fail";
	}
	VData tRet = tTaskDoBusinessBL.getResult();
	tUrl = (String) tRet.getObjectByObjectName("String", 0);
	System.out.println("URL:"+tUrl);
	
	//改变工单状态为经办
	tLGWorkSchema.setWorkNo(tWorkNo);
	tLGWorkSchema.setStatusNo("3");
	tVData.clear();
	tVData.add(tLGWorkSchema);
	tVData.add(tGI);
	TaskChangeStatusBL tTaskChangeStatusBL = new TaskChangeStatusBL();
	if (tTaskChangeStatusBL.submitData(tVData, "") == false)
	{
		FlagStr = "Fail";
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>", "<%=tUrl%>");
</script>
</html>

