<%
//程序名称：LDUserInput.jsp
//程序功能：
//创建日期：2002-03-17 14:27:10
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bl.sys.*"%>

<!--用户校验类-->

<%
  //接收信息，并作校验处理。
  //输入参数
  LDUserSchema tLDUserSchema   = new LDUserSchema();
  
  JdbcUrl  tDBC      = new JdbcUrl();
  DBLDUser tLDUser   = new DBLDUser(tDBC);
  //输出参数
//  DBTerror dbTerror = null;
  String tBmCert = "";
  System.out.println("Start save");
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

    tLDUserSchema.setUserCode(request.getParameter("UserCode"));
    tLDUserSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLDUserSchema.setUserName(request.getParameter("UserName"));
    tLDUserSchema.setOtherPopedom(request.getParameter("OtherPopedom"));
    tLDUserSchema.setComCode(request.getParameter("ComCode"));
    tLDUserSchema.setPopUWFlag(request.getParameter("PopUWFlag"));
    tLDUserSchema.setPassword(request.getParameter("Password"));
    tLDUserSchema.setSuperPopedomFlag(request.getParameter("SuperPopedomFlag"));
    tLDUserSchema.setUserDescription(request.getParameter("UserDescription"));
    tLDUserSchema.setOperator(request.getParameter("Operator"));
    tLDUserSchema.setUserState(request.getParameter("UserState"));
    tLDUserSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDUserSchema.setUWPopedom(request.getParameter("UWPopedom"));
    tLDUserSchema.setMakeTime(request.getParameter("MakeTime"));
    tLDUserSchema.setValidEndDate(request.getParameter("ValidEndDate"));
    tLDUserSchema.setValidStartDate(request.getParameter("ValidStartDate"));


  try
  {
    //tLDUser.submitData(tLDUserSchema,"控制数据");
    tLDUser.insert(tLDUserSchema);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//  dbTerror = tLDUser.getDBTerror();
//  if (dbTerror.getErrno()==0)
//  {                          
    Content = " 保存成功";
  	FlagStr = "Succ";
//  }
//  else                                                                           
//  {
//  	Content = " 保存失败，原因是:" + dbTerror.getErrmessage() ;
//  	FlagStr = "Fail";
//  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

