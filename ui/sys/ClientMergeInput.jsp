<html>
<%
//程序名称：ClientMergeInput.jsp
//程序功能：
//创建日期：2002-08-19
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="ClientMergeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ClientMergeInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ClientMergeSave.jsp" method=post name=fm target="fraSubmit">

	<table>
      <tr>
        <td width="80%">&nbsp;&nbsp;</td>
        <td><input  class=cssButton TYPE=button  value="&nbsp;合&nbsp;&nbsp;并&nbsp;" onclick="mergeClick( )"></td>
        <td><input  class=cssButton TYPE=button  value="&nbsp;查&nbsp;&nbsp;询&nbsp;" onclick="queryClick( )"></td></tr>
    </table>

    <table>
      <tr>
        <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson);"></td>
        <td class= titleImg>客户信息</td></tr>
    </table>

    <div  id= "divLDPerson" style= "display: ''">
      <table class="common">
        <tr class="common">
          <td class="title">客户姓名</td>
          <td class="input"><Input class="common" name=Name ></td>
          
         	<TD  class= title8>客户性别</TD>
					<TD  class= input8> <input class=codeno CodeData="0|8^0|男^1|女^2|其他 "  name=Sex ondblclick="return showCodeListEx('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Sex',[this,SexName],[0,1],null,null,null,1);"><input class=codename name=SexName></TD></tr>

        <tr class="common">
          <td class="title">客户出生日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="Birthday"></td>
          
					<TD  class= title> 婚姻状况</TD>
					<TD  class= input>
    		    <Input class= "codeno"  name=Marriage  ondblclick="return showCodeList('marriage',[this,marriagename],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('marriage',[this,marriagename],[0,1],null,null,null,1);" ><Input class=codename  name=marriagename></td>
</tr>
        <tr class="common">
					<TD  class= title> 职业类别</TD>
					<TD  class= input>
    		    <Input class= "codeno"  name=OccupationType  ondblclick="return showCodeList('OccupationType',[this,OccupationTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('OccupationType',[this,OccupationTypeName],[0,1],null,null,null,1);" ><Input class=codename  name=OccupationTypeName></td>
   
					<TD  class= title> 健康状况</TD>
					<TD  class= input>
    		    <Input class= "codeno"  name=Health  ondblclick="return showCodeList('Health',[this,HealthName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Health',[this,HealthName],[0,1],null,null,null,1);" ><Input class=codename  name=HealthName></td>
    </tr>

        <tr class="common"><td cols=4><br></br></td></tr>
        
        <tr class="common">
      		 <TD  class= title> 证件类型</TD>
					<TD  class= input>
    		  <Input class= "codeno"  name=IDType  ondblclick="return showCodeList('IDType',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1],null,null,null,1);" ><Input class=codename  name=IDTypeName></td>
    
          <td class="title">属性</td>
          <td class="input"><Input class="common" name=Proterty ></td></tr>

        <tr class="common">
          <td class="title">证件号码</td>
          <td class="input"><Input class="common" name=IDNo ></td>
          
          <td class="title">ic卡号</td>
          <td class="input"><Input class="common" name=ICNo ></td></tr>

        <tr class="common">
          <td class="title">其它证件号码</td>
          <td class="input"><Input class="common" name=OthIDNo ></td>
          
          <td class="title">其它证件类型</td>
          <td class="input"><Input class="common" name=OthIDType ></td></tr>
          
        <tr class="common"><td cols=4><br></br></td></tr>
          
        <tr class="common">
          <td class="title">单位名称</td>
          <td class="input"><Input class="common" name=GrpName ></td>
        
          <td class="title">单位地址</td>
          <td class="input"><Input class="common" name=GrpAddress ></td></tr>
          
        <tr class="common">
          <td class="title">家庭地址编码</td>
          <td class="input"><Input class="common" name=HomeAddressCode ></td>
          
          <td class="title">家庭地址</td>
          <td class="input"><Input class="common" name=HomeAddress ></td></tr>
          
        <tr class="common">
          <td class="title">电话</td>
          <td class="input"><Input class="common" name=Phone ></td>
          
          <td class="title">传呼</td>
          <td class="input"><Input class="common" name=BP ></td></tr>
          
        <tr class="common">
          <td class="title">手机</td>
          <td class="input"><Input class="common" name=Mobile ></td>
          
          <td class="title">e_mail</td>
          <td class="input"><Input class="common" name=EMail ></td></tr>

      </table>
    </div>
    
    <!-- 客户列表 -->
    <table>
   	  <tr>
        <td class="common"><IMG  src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divClientList);"></td>
    	<td class= titleImg>客户列表</td></tr>
    </table>
    
	<div id="divClientList" style="display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanClientList" ></span></td></tr>
	  </table>
	</div>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>
