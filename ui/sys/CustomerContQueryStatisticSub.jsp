<%
//程序名称：AppStatisticSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
ReportUI
</title>
<head>
</head>
<body>
<%

//String FileName = "GroupProduct_Income";
//String StartDate = "2005-01-01";
//String EndDate = "2005-01-11";
//String ManageCom = "86";
String CurrentDate = PubFun.getCurrentDate();
String flag = "0";
String FlagStr = "";
String Content = "";
String QYType_ch = StrTool.unicodeToGBK(request.getParameter("QYType_ch"));
String QYType = StrTool.unicodeToGBK(request.getParameter("QYType"));
String SQl1="";
String SQl2="";
String SQl3="";
String ManageCom = request.getParameter("ManageCom");
String AgeBegin = request.getParameter("AgeBegin");
String AgeEnd = request.getParameter("AgeEnd");
String RiskCode = request.getParameter("RiskCode");
String SignDateBegin = request.getParameter("SignDateBegin");
String SignDateEnd = request.getParameter("SignDateEnd");
String AgentCode = request.getParameter("AgentCode");
String PremBegin = request.getParameter("PremBegin");
String PremEnd = request.getParameter("PremEnd");
System.out.println("QYType"+QYType);
System.out.println("QYType_ch"+QYType_ch);


//设置模板名称
String FileName = "";

//设置模板名称
if(QYType.equals("1"))
{
 FileName ="tbCustomerContQueryStatistic";

if(!ManageCom.equals("")){
SQl1=SQl1+" and b.ManageCom like '"+ManageCom+"%' ";
SQl2=" ";
SQl3=" ";
}
if(!AgeBegin.equals("")&&!AgeEnd.equals("")){
	
SQl1=SQl1;
SQl2=SQl2+",YearDiff(varchar(Current date) ,varchar(a.appntbirthday) ) m ";
SQl3=SQl3+" where m between "+AgeBegin+" and "+AgeEnd+" ";
}
if(!SignDateBegin.equals("")&&!SignDateEnd.equals("")){
	
SQl1=SQl1+" and b.signdate between '"+SignDateBegin+"' and '"+SignDateEnd+"'";

}

if(!PremBegin.equals("")&&!PremEnd.equals("")){
	
SQl1=SQl1+" and (select sum(prem) from lcpol where prtno=b.prtno and  uwflag in('4','9') and conttype='1' and appflag='1') between "+PremBegin+" and "+PremEnd+" ";

}

if(!AgentCode.equals("")){
	
SQl1=SQl1+" and b.agentcode= '"+AgentCode+"' ";

}

if(!RiskCode.equals("")){
	
SQl1=SQl1+" and b.prtno in (select prtno from lcpol where riskcode in (select riskcode from lmriskapp where riskprop='I' and riskcode='"+RiskCode+"') and conttype='1' and appflag='1' and uwflag in('4','9') ) ";

}
System.out.println("SQl1"+SQl1);
System.out.println("SQl2"+SQl2);
System.out.println("SQl3"+SQl3);
}else if(QYType.equals("2"))
{
	 FileName ="bbCustomerContQueryStatistic";
	 
if(!ManageCom.equals("")){
SQl1=SQl1+" and a.ManageCom like '"+ManageCom+"%' ";
SQl2=" ";
SQl3=" ";
}
if(!AgeBegin.equals("")&&!AgeEnd.equals("")){
	
SQl1=SQl1;
SQl2=SQl2+",YearDiff(varchar(Current date) ,varchar(a.insuredbirthday) ) m ";
SQl3=SQl3+" where m between "+AgeBegin+" and "+AgeEnd+" ";

}
if(!SignDateBegin.equals("")&&!SignDateEnd.equals("")){
	
SQl1=SQl1+" and a.signdate between '"+SignDateBegin+"' and '"+SignDateEnd+"'";
}

if(!PremBegin.equals("")&&!PremEnd.equals("")){
	
SQl1=SQl1+" and (select sum(prem) from lcpol where prtno=a.prtno and  uwflag in('4','9') and conttype='1' and appflag='1') between "+PremBegin+" and "+PremEnd+" ";
}

if(!AgentCode.equals("")){
	
SQl1=SQl1+" and a.agentcode= '"+AgentCode+"' ";
}

if(!RiskCode.equals("")){
	
SQl1=SQl1+" and a.riskcode in (select riskcode from lmriskapp where riskprop='I' and riskcode='"+RiskCode+"') ";
}
System.out.println("SQl1"+SQl1);
}else{
	 FileName ="bbCustomerContPremQueryStatistic";
	 
if(!ManageCom.equals("")){
SQl1=SQl1+" and a.ManageCom like '"+ManageCom+"%' ";
SQl2=" ";
SQl3=" ";
}
if(!AgeBegin.equals("")&&!AgeEnd.equals("")){
	
SQl1=SQl1;
SQl2=SQl2+",YearDiff(varchar(Current date) ,varchar(a.insuredbirthday) ) m ";
SQl3=SQl3+" where m between "+AgeBegin+" and "+AgeEnd+" ";
}
if(!SignDateBegin.equals("")&&!SignDateEnd.equals("")){
	
SQl1=SQl1+" and a.signdate between '"+SignDateBegin+"' and '"+SignDateEnd+"'";
}

if(!PremBegin.equals("")&&!PremEnd.equals("")){
	
SQl1=SQl1+" and (select sum(prem) from lcpol where prtno=a.prtno and a.insuredno=insuredno and appflag='1' and conttype='1' and uwflag in('4','9')) between "+PremBegin+" and "+PremEnd+" ";
}

if(!AgentCode.equals("")){
	
SQl1=SQl1+" and a.agentcode= '"+AgentCode+"' ";
}

if(!RiskCode.equals("")){
	
SQl1=SQl1+" and a.riskcode in (select riskcode from lmriskapp where riskprop='I' and riskcode='"+RiskCode+"') ";
}
System.out.println("SQl1"+SQl1);	

}	

JRptList t_Rpt = new JRptList();
String tOutFileName = "";
if(flag.equals("0"))
{
t_Rpt.m_NeedProcess = false;
t_Rpt.m_Need_Preview = false;
t_Rpt.mNeedExcel = true;


CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");

t_Rpt.AddVar("ManageCom",ManageCom);
t_Rpt.AddVar("SQl1",SQl1);
t_Rpt.AddVar("SQl2",SQl2);
t_Rpt.AddVar("SQl3",SQl3);

t_Rpt.AddVar("ManageComName", ReportPubFun.getMngName(ManageCom));
 String YYMMDD = "";
YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
       + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
       + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("MakeDate", YYMMDD);

t_Rpt.Prt_RptList(pageContext,FileName);
tOutFileName = t_Rpt.mOutWebReportURL;
String strVFFileName = 
FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
String strRealPath = 
application.getRealPath("/web/Generated").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName; 
System.out.println("strVFPathName : "+ strVFPathName); 
System.out.println("=======Finshed in JSP==========="); 
System.out.println(tOutFileName);

response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+
"&RealPath="+strVFPathName);
}
%>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >