<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
</head>
<body  onload="" >
  <form action="./ProposalSave.jsp" method=post name=fm target="fraSubmit">
    <table class= common>
    <tr class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskCode ondblclick=" showCodeList('RiskInd',[this]);" onkeyup="return showCodeListKey('RiskInd',[this]);">
          </TD>
       </tr>
       </table>
    <!-- 隐藏信息 -->
			<table>
    		<tr>
        	<td class=common>
			    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskInfo);">
    			</td>
    			<td class= titleImg>
    			  险种信息
    			</td>
    		</tr>
    	</table>
    	
    	<Div  id= "divRiskInfo" style= "display: ''">
			<table class= common>
				<TR class=common>	
					<TD  class= title>
        		保单号码
        	</TD>
        	<TD  class= input>
        		<Input class="readonly" readonly name=PolNo >
        	</TD>
        	<TD  class= title>
            集体保单号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GrpPolNo >
          </TD>
          <TD  class= title>
            总单/合同号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ContNo  >
          </TD>
        </TR>
        
        <TR class= common>
        	<TD  class= title>
            保单生效日期
          </TD>
          <TD  class= input>
	          <input class="readonly" dateFormat="short" name="CValiDate" verify="保单生效日期|NOTNULL" >
          </TD>	
          <TD  class= title>
            保单状态
          </TD>
          <TD  class= input>
            <Input class="code" name=PolState verify="保单状态|NOTNULL" CodeData="0|^0|有效^1|失效^2|解约" ondblClick="showCodeListEx('PolState_1',[this],[0,1,2]);" onkeyup="showCodeListKeyEx('PolState_1',[this],[0,1,2]);">
          </TD>
          <TD  class= title>
            销售渠道
          </TD>
          <TD  class= input>
            <Input class="code" name=SaleChnl ondblclick="return showCodeList('SaleChnl',[this]);"  onkeyup="return showCodeListKey('SaleChnl',[this]);">
          </TD>
        </TR>
          
        <TR class= common>
        	<TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input name=AgentCode class="readonly" readonly >
          </TD>
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AgentGroup  >
          </TD>                   
          <TD  class= title>
            联合代理人编码
          </TD>
          <TD  class= input>
            <Input  name=AgentCode1 class="readonly" readonly>
          </TD>
        </TR>
        
        <TR class= common>
        	<TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code"  name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
          </TD>        
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input name=AgentCom class="readonly" readonly >
          </TD>
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input name=PrtNo class="readonly" readonly >
          </TD>
        </TR>
        
        <TR class= common>
        	<TD  class= title>
 					   签单日期
           </TD>
          <TD  class= input>
            <Input class= readonly name=SignDate >
          </TD>
          <TD  class= title>
            首期交费日期
          </TD>
          <TD  class= input>
            <Input class= readonly name=FirstPayDate >
          </TD>
          <TD  class= title>
            终交日期
          </TD>
          <TD  class= input>
            <Input class= readonly name=PayEndDate >
          </TD>
        </TR>
        
        <TR class= common>
        	<TD  class= title>
            交至日期
          </TD>
          <TD  class= input>
            <Input class= readonly name=PaytoDate >
          </TD>
          <TD  class= title>
            终交期间
          </TD>
          <TD  class= input>
	          <input class="readonly" name="PayEndYear">
          </TD>
          <TD  class= title>
            保单送达日期
          </TD>
          <TD  class= input>
            <Input class= readonly name=GetPolDate >
          </TD>
        </TR>
        
        <TR class=common>
        	<TD  class= title>
              起领期间
          </TD>
          <TD  class= input>
            <Input class= "readonly" name=GetYear verify="起领期间|NOTNULL" CodeData="0|^1|55岁起|55|A|^2|50岁起|50|A" ondblClick="showCodeListEx('GetYear212401',[this,GetYearFlag],[2,3]);" onkeyup="showCodeListKeyEx('GetYear212401',[this,GetYearFlag],[2,3]);">
          </TD>
				  <TD  class= title>
            起领日期参照
          </TD>
          <TD  class= input>
            <Input class="readonly" name=GetStartType CodeData="0|^S|起保日期对应日|^B|出生日期对应日" ondblClick="showCodeListEx('StartDateCalRef212401',[this],[0]);" onkeyup="showCodeListKeyEx('StartDateCalRef212401',[this],[0]);" >
          </TD>
          <TD  class= title>
            保险责任终止日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=EndDate >
          </TD>
        </TR>
        
        <TR class= common>
        	<TD  class= title>
            保费
          </TD>
          <TD  class= input>
            <Input class= readonly name=Prem >
          </TD>
          <TD  class= title>
            保额
          </TD>
          <TD  class= input>
            <Input class= readonly name=Amnt >
          </TD>
          <TD  class= title>
            份数
          </TD>
          <TD  class= input>
            <Input class= readonly name=Mult >
          </TD>
        </TR>
        
        <TR class= common>
        	<TD class= title>
            累计保费
          </TD>
          <TD  class= input>
            <Input class="readonly" name=SumPrem >
          </TD>
        	<TD  class= title>
            余额
          </TD>
          <TD  class= input>
            <Input class= readonly name=LeavingMoney >
          </TD>
          <TD  class= title>
            保险期间
          </TD>
          <TD  class= input>
            <Input class= readonly name=InsuYear >
          </TD> 
        </TR>
        
        <TR class= common>
        	<TD  class= title>
            给付方法
          </TD>
          <TD  class= input>
            <Input class= "readonly" name=GetDutyKind verify="给付方法|NOTNULL" CodeData="0|^1|一次领取型|^2|年领定额型|^3|月领定额型|^4|年领十年固定定额型|^5|月领十年固定定额型|^6|年领算术增额型|^7|月领算术增额型|^8|年领几何增额型|^9|月领几何增额型" ondblClick="showCodeListEx('GetIntv212401',[this],[0]);" onkeyup="showCodeListKeyEx('GetIntv212401',[this],[0]);">
          </TD>
          <TD  class= title>
            缴费位置
          </TD>
          <TD  class= input>
            <Input class="readonly" name=PayLocation ondblclick="return showCodeList('PayLocation',[this]);" onkeyup="return showCodeListKey('PayLocation',[this]);">
          </TD>
          <TD  class= title>
            最近复效日期
          </TD>
          <TD  class= input>
            <Input class= readonly name=LastRevDate >
          </TD>
        </TR>
        
        <TR class= common>
        	<TD  class= title>
          	银行委托书号码
          </TD>
          <TD  class= input>
            <Input class="readonly" name=ConsignNo>
          </TD>	
          <TD  class= title>
            利差返还方式
          </TD>
          <TD  class= input>
            <Input class=code name=InterestDifFlag ondblclick="return showCodeList('InterestDifFlag',[this]);" onkeyup="return showCodeListKey('InterestDifFlag',[this]);">
          </TD>
          <TD  class= title>
            减额交清标志
          </TD>
          <TD  class= input>
            <Input class="code" name=SubFlag verify="减额|NOTNULL" CodeData="0|^0|正常^1|减额" ondblClick="showCodeListEx('JE_1',[this],[0,1]);" onkeyup="showCodeListKeyEx('JE_1',[this],[0,1]);">
          </TD>
        </TR>
        
        <TR class= common>
        	<TD  class= title>
            自动垫交标志
          </TD>
          <TD  class= input>
            <Input class="code" name=AutoPayFlag verify="垫交标志|NOTNULL" CodeData="0|^0|正常^1|垫交" ondblClick="showCodeListEx('AutoPayFlag_1',[this],[0,1]);" onkeyup="showCodeListKeyEx('AutoPayFlag_1',[this],[0,1]);">
          </TD>
          <TD  class= title>
            红利领取方式
          </TD>
          <TD  class= input>
            <Input class="code" name=BonusGetMode ondblclick="return showCodeList('BonusMode',[this]);" onkeyup="return showCodeListKey('BonusMode',[this]);">
          </TD>
          <TD  class= title>
            体检标志
          </TD>
          <TD  class= input>
            <Input class=code name=HealthCheckFlag verify="体检|NOTNULL" CodeData="0|^0|不体检^1|体检" ondblClick="showCodeListEx('TJ_1',[this],[0,1]);" onkeyup="showCodeListKeyEx('TJ_1',[this],[0,1]);">
          </TD>
        </TR>
        
        <TR class= common>
        	<TD  class= title>
            生存保险金领取方式
          </TD>
          <TD  class= input>
            <Input class="code" name=LiveGetMode verify="生存|NOTNULL" CodeData="0|^1|现金领取^2|抵缴保费^3|购买缴清增额保险^4|累积生息^9|其他" ondblClick="showCodeListEx('SC_1',[this],[1,2,3,4,9]);" onkeyup="showCodeListKeyEx('SC_1',[this],[1,2,3,4,9]);">
          </TD>
          <TD  class= title>
            年金开始领取年龄
          </TD>
          <TD  class= input>
            <Input class= readonly name=GetYear >
          </TD>
        	<TD  class= title>
            保单打印次数
          </TD>
          <TD  class= input>
            <Input class="readonly" name=PrintCount >
          </TD>
        </TR>   
      </table> 
      <table class= common>
      <TR class= common>
				<TD class= title>	保单备注 	</TD>
			</TR>

			<TR  class= common>
  			<TD  class= input><textarea name="Remark" cols="120%" rows="3" witdh=25% class="common"> </textarea> </TD>
  		</TR>

      </table>   
      </Div>    

    <!-- 投保人信息部分 -->
    <Div  id= "divLCAppntInd0" style= "display: none ">
    <table>
    	<tr>
        	<td>
    	  		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCAppntInd1);">
    		</td>
    		<td class= titleImg>
    			 投保人信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCAppntInd1" style= "display: ''">
      <table  class= common>
        <TR class= common>
          <TD  class= title>
            投保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntCustomerNo ondblclick="showAppnt();">
          </TD>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntName >
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <input class="code"  name=AppntSex verify="被保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
            <input class="readonly" readonly name="AppntBirthday" >
          </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class="code"  name=AppntIDType verify="被保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntIDNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            与被保人关系
          </TD>
          <TD  class= input>
            <Input class="readonly" name=RelationToInsured ondblclick="return showCodeList('Relation',[this]);" onkeyup="return showCodeListKey('Relation',[this]);">
          </TD>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= readonly name=AppntPhone >
          </TD>
          <TD  class= title>
            手机
          </TD>
          <TD  class= input>
            <Input class= readonly name=AppntMobile >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class= readonly name=AppntEMail >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= readonly name=AppntZipCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            通讯地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= readonly name=AppntPostalAddress >
          </TD>
        </TR>
      </table>
    </Div>
    </Div>
    <!-- 集体投保人信息部分 -->
    <table>
    	<tr>
        	<td>
    	  		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCAppntGrp1);">
    		</td>
    		<td class= titleImg>
    			 投保单位资料
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCAppntGrp1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            单位客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppGrpNo  >
          </TD>        
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppGrpName >
          </TD>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppGrpAddress >
          </TD>          
        </TR>        
        <TR  class= common>          
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppGrpZipCode >
          </TD>       
          <TD  class= title>
            单位性质
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GrpNature >
          </TD>
          <TD  class= title>
            行业类别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=BusinessType >
          </TD>
        </TR>        
        <TR  class= common>          
          <TD  class= title>
            单位总人数
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Peoples >
          </TD>       
          <TD  class= title>
            注册资本金
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RgtMoney >
          </TD>
          <TD  class= title>
            资产总额
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Asset >
          </TD>
        </TR>        
        <TR  class= common>          
          <TD  class= title>
            净资产收益率
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=NetProfitRate >
          </TD>       
          <TD  class= title>
            主营业务
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=MainBussiness >
          </TD>
          <TD  class= title>
            单位法人代表
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Corporation >
          </TD>
        </TR>        
        <TR  class= common>
          <TD  class= title>
            机构分布区域
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ComAera >
          </TD>
        </TR>        
        <TR  class= common>                 
          <TD  class= title>
            保险联系人一
          </TD>
        </TR>        
        <TR  class= common>                 
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=LinkMan1 >
          </TD>
          <TD  class= title>
            部门
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Department1 >
          </TD>
          <TD  class= title>
            职务
          </TD>
          <TD  class= input>
            <Input name=HeadShip1 class="readonly" readonly>
          </TD>
        </TR>        
        <TR  class= common>                 
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Phone1 >
          </TD>
          <TD  class= title>
            E-MAIL
          </TD>
          <TD  class= input>
            <Input name=E_Mail1 class="readonly" readonly>
          </TD>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input  name=Fax1 class="readonly" readonly>
          </TD>       
        </TR>        
        <TR  class= common>
          <TD  class= title>
            保险联系人二
          </TD>       
        </TR>        
        <TR  class= common>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=LinkMan2 >
          </TD>
          <TD  class= title>
            部门
          </TD>
          <TD  class= input>
            <Input name=Department2 class="readonly" readonly>
          </TD>
          <TD  class= title>
            职务
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=HeadShip2 >
          </TD>       
        </TR>        
        <TR  class= common>
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
            <Input name=Phone2 class="readonly" readonly>
          </TD>
          <TD  class= title>
            E-MAIL
          </TD>
          <TD  class= input>
            <Input name=E_Mail2 class="readonly" readonly>
          </TD>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input name=Fax2 class="readonly" readonly>
          </TD>       
        </TR>        
        <TR  class= common>
          <TD  class= title>
            付款方式
          </TD>
          <TD  class= input>
            <Input name=GetFlag class="readonly" readonly>
          </TD>
          <TD  class= title>
            开户银行
          </TD>
          <TD  class= input>
            <Input name=GrpBankCode class="readonly" readonly>
          </TD>
          <TD  class= title>
            帐号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=GrpBankAccNo >
          </TD>       
        </TR>        
      </table>
    </Div>
    <!-- 被保人信息部分 -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured1);">
    		</td>
    		<td class= titleImg>
    			 被保人信息（客户号：<Input  class="readonly" readonly name=CustomerNo > 首次投保客户无需填写客户号）
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCInsured1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= readonly name=Name verify="被保人姓名|notnull&len<=20" >
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class="code" name=Sex verify="被保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
          </TD>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
          <input class="readonly" dateFormat="short" name="Birthday" verify="被保人出生日期|notnull&date" >
          </TD>
        </TR>
        <TR  class= common>  
          <TD  class= title>
            年龄
          </TD>
          <TD  class= input>
          <input class="readonly" readonly name="Age" >
          </TD>               
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class="code" name="IDType" verify="被保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= readonly name="IDNo" verify="被保人证件号码|len<=20" >
          </TD>          
        </TR>
        <TR  class= common>
          <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <input class="code" name="NativePlace" verify="被保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">
          </TD>
          <TD  class= title>
            户口所在地
          </TD>
          <TD  class= input>
            <Input class= readonly name="RgtAddress" verify="被保人户口所在地|len<=80" >
          </TD>
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class="code" name="Marriage" verify="被保人婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            民族
          </TD>
          <TD  class= input>
          <input class="code" name="Nationality" verify="被保人民族|code:Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">
          </TD>
          <TD  class= title>
            学历
          </TD>
          <TD  class= input>
            <Input class="code" name="Degree" verify="被保人学历|code:Degree" ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">
          </TD>
          <TD  class= title>
            是否吸烟
          </TD>
          <TD  class= input>
            <Input class="code" name="SmokeFlag" verify="被保人是否吸烟|code:YesNo" ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= readonly name="PostalAddress" verify="被保人联系地址|len<=80" >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= readonly name="ZipCode" verify="被保人邮政编码|zipcode" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            家庭电话
          </TD>
          <TD  class= input>
          <input class= readonly name="Phone" verify="被保人家庭电话|len<=18" >
          </TD>
          <TD  class= title>
            移动电话
          </TD>
          <TD  class= input>
            <Input class= readonly name="Mobile" verify="被保人移动电话|len<=15" >
          </TD>
          <TD  class= title>
            电子邮箱
          </TD>
          <TD  class= input>
            <Input class= readonly name="EMail" verify="被保人电子邮箱|len<=20" >
          </TD>
        </TR>               
         <TR  class= common>
          <TD  class= title>
            职业（工种）
          </TD>
          <TD  class= input>
            <Input class= readonly name="WorkType" verify="被保人职业（工种）|len<=10" >
          </TD>
          <TD  class= title>
            兼职（工种）
          </TD>
          <TD  class= input>
            <Input class= readonly name="PluralityType" verify="被保人兼职（工种）|len<=10" >
          </TD>
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input>
            <Input class="code" name="OccupationType" verify="被保人职业类别|notnull&code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>
        </TR>        
        <TR  class= common>
          <TD  class= title>
            职业代码
          </TD>
          <TD  class= input>
            <Input class="code" name="OccupationCode" verify="被保人职业代码|code:OccupationCode" ondblclick="return showCodeList('OccupationCode',[this]);" onkeyup="return showCodeListKey('OccupationCode',[this]);">
          </TD>
        </TR>                                
      </table>
    </Div>
    <!-- 险种信息部分 -->
      </Div>
    <!-- 连带被保人信息部分（列表） -->
	<Div  id= "divLCInsured0" style= "display: ''">
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured2);">
    		</td>
    		<td class= titleImg>
    			 连带被保人信息
    		</td>
    	</tr>
      </table>
	  <Div  id= "divLCInsured2" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanSubInsuredGrid" >
					</span> 
				</td>
			</tr>
		</table>
	  </div>
	</div>
	
    <!-- 受益人信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCBnf1);">
    		</td>
    		<td class= titleImg>
    			 受益人信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCBnf1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanBnfGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
    <!-- 告知信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
    		</td>
    		<td class= titleImg>
    			 告知信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCImpart1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanImpartGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
    <!-- 特约信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCSpec1);">
    		</td>
    		<td class= titleImg>
    			 特约信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCSpec1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanSpecGrid">
					</span> 
				</td>
			</tr>
		</table>
	</div>

    <!--可以选择的责任部分，该部分始终隐藏-->
	<Div  id= "divChooseDuty0" style= "display: ''">    
    <table>
	<tr>
    	<td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDutyGrid);">
		</td>
		<td class= titleImg>
			 可选责任信息
		</td>
	</tr>
    </table>
	<Div  id= "divDutyGrid" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanDutyGrid" >
					</span> 
				</td>
			</tr>
		</table>
		<!--确定是否需要责任信息-->
	</div>
	</div>
		  <table  class= common>
        <TR  class= common>
          <TD  class= title>
            录入人员
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=Operator >
          </TD>
          <TD  class= title>
            复核人员
          </TD>
          <TD  class= input>
            <Input class= readonly readonly name=ApproveCode >
          </TD>
          <TD  class= title>
            核保人员
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=UWCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            录入日期
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=MakeDate >
          </TD>
          <TD  class= title>
            复核日期
          </TD>
          <TD  class= input>
            <Input class= readonly readonly name=ApproveDate >
          </TD>
          <TD  class= title>
            核保日期
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=UWDate >
          </TD>
        </TR>
    	</table>
		<input type=hidden id="inpNeedDutyGrid" name="inpNeedDutyGrid" value="0">
		<input type=hidden id="fmAction" name="fmAction">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
<script>
function returnParent()
{
	var isDia=0;
	var haveMenu=0;
	var callerWindowObj;
	try
	{
		callerWindowObj = dialogArguments; 
		isDia=1;
	}
	catch(ex1)
	{ isDia=0;}
	try
	{
		if(isDia==0)//如果是打开一个新的窗口，则执行下面的代码
		{
			top.opener.parent.document.body.innerHTML=window.document.body.innerHTML;
		}
		else//如果打开一个模态对话框，则调用下面的代码
		{
			callerWindowObj.document.body.innerHTML=window.document.body.innerHTML;
			haveMenu = 1;
			callerWindowObj.parent.frames("fraMenu").Ldd = 0;
			callerWindowObj.parent.frames("fraMenu").Go();
		}
	}
	catch(ex)
	{
		if( haveMenu != 1 ) 
		{
			alert("Riskxxx.jsp:发生错误："+ex.name);
		}
	}
	top.close();
}  
returnParent();
</script>
