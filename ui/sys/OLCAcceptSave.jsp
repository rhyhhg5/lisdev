<%
//程序名称：OLCAcceptInput.jsp
//程序功能：
//创建日期：2002-08-16 14:47:23
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LCAcceptSchema tLCAcceptSchema   = new LCAcceptSchema();

  OLCAcceptUI tOLCAccept   = new OLCAcceptUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLCAcceptSchema.setPrtNo(request.getParameter("PrtNo"));
    tLCAcceptSchema.setManageCom(request.getParameter("ManageCom"));
    tLCAcceptSchema.setRiskCode(request.getParameter("RiskCode"));
    tLCAcceptSchema.setRiskVersion(request.getParameter("RiskVersion"));
    tLCAcceptSchema.setGrpFlag(request.getParameter("GrpFlag"));
    tLCAcceptSchema.setMEflag(request.getParameter("MEflag"));
    tLCAcceptSchema.setPayMoney(request.getParameter("PayMoney"));
    tLCAcceptSchema.setAppntName(request.getParameter("AppntName"));
    tLCAcceptSchema.setInsuredPeoples(request.getParameter("InsuredPeoples"));
    tLCAcceptSchema.setAgentCode(request.getParameter("AgentCode"));
    tLCAcceptSchema.setAffixNum(request.getParameter("AffixNum"));
    tLCAcceptSchema.setAffixPageNum(request.getParameter("AffixPageNum"));


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLCAcceptSchema);
	tVData.add(tG);
  try
  {
    tOLCAccept.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tOLCAccept.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

