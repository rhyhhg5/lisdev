<%
//程序名称：ClaimListInit.jsp
//程序功能：
//创建日期：2007-3-18 21:54
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

function initForm()
{
  try
  {
    initClaimList();
    //queryClaimList();
  }
  catch(re)
  {
    alert("在GrpInsuredInit.jsp-->InitForm函数中发生异常:初始化界面错误!" + re.message);
  }
}

//初始化无名单操作轨迹
function initClaimList()
{                               
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=3;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="立案号";         		//列名
    iArray[1][1]="100px";            		//列宽
    iArray[1][2]=120;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="CaseNo";
    
    iArray[2]=new Array();
    iArray[2][0]="立案日期";         		//列名
    iArray[2][1]="60";            		//列宽
    iArray[2][2]=120;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="RgtDate";
    
    iArray[3]=new Array();
    iArray[3][0]="受理人";         		//列名
    iArray[3][1]="60px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="rigister";

    iArray[4]=new Array();
    iArray[4][0]="被保人";         		//列名
    iArray[4][1]="60px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="InsuredName";

    iArray[5]=new Array();
    iArray[5][0]="理赔结论";         		//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="GiveTypeDesc";
    
    iArray[6]=new Array();
    iArray[6][0]="赔付金额";         		//列名
    iArray[6][1]="60px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="RealPay";
    
    iArray[7]=new Array();
    iArray[7][0]="结案日期";         		//列名
    iArray[7][1]="60px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="EndCaseDate"; 
    
    iArray[8]=new Array();
    iArray[8][0]="受理机构";         		//列名
    iArray[8][1]="100px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="ManageCom";
    
    iArray[9]=new Array();
    iArray[9][0]="处理状态";         		//列名
    iArray[9][1]="60px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[9][21]="RgtState";
    
    iArray[10]=new Array();
    iArray[10][0]="批注";         		//列名
    iArray[10][1]="70px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[10][21]="Remark";

    ClaimListGrid = new MulLineEnter( "fm" , "ClaimListGrid" ); 
    //这些属性必须在loadMulLine前
    ClaimListGrid.mulLineCount = 0;   
    ClaimListGrid.displayTitle = 1;
    ClaimListGrid.locked = 1;
    ClaimListGrid.canSel = 1;
    ClaimListGrid.hiddenPlus = 1;
    ClaimListGrid.hiddenSubtraction = 1;
    ClaimListGrid.loadMulLine(iArray);       
  }
  catch(ex)
  {
    alert("在GrpInsuredInit.jsp-->initGrpInseGrid函数中发生异常:初始化界面错误!");
  }
}

</script>