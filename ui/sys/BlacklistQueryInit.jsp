<%
//程序名称：BlacklistQuery.js
//程序功能：
//创建日期：2003-01-10
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{

  try
  {
  	fm.all('BlacklistNo').value='';
  	fm.all('BlacklistType').value='';
  }
  catch(ex)
  {
    alert("在BlacklistQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
//    setOption("t_sex","0=男&1=女&2=不详");
//    setOption("sex","0=男&1=女&2=不详");
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");
  }
  catch(ex)
  {
    alert("在BlacklistQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
	  initBlacklistGrid();
  }
  catch(re)
  {
    alert("BlacklistQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *
 *输入：          没有
 *输出：          没有
 *功能：          初始化BlacklistGrid
 ************************************************************
 */
var BlacklistGrid;          //定义为全局变量，提供给displayMultiline使用
function initBlacklistGrid()
  {
    var iArray = new Array();

      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
		    iArray[1][0]="客户号码";         //列名
		    iArray[1][1]="150px";         //宽度
		    iArray[1][2]=100;         //最大长度
		    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

		    iArray[2]=new Array();
		    iArray[2][0]="客户类型";         //列名
		    iArray[2][1]="100px";         //宽度
		    iArray[2][2]=100;         //最大长度
		    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

		    iArray[3]=new Array();
		    iArray[3][0]="名称";         //列名
		    iArray[3][1]="150px";         //宽度
		    iArray[3][2]=100;         //最大长度
		    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

		    iArray[4]=new Array();
		    iArray[4][0]="操作员";         //列名
		    iArray[4][1]="100px";         //宽度
		    iArray[4][2]=100;         //最大长度
		    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

		    iArray[5]=new Array();
		    iArray[5][0]="日期";         //列名
		    iArray[5][1]="100px";         //宽度
		    iArray[5][2]=100;         //最大长度
		    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

		    iArray[6]=new Array();
		    iArray[6][0]="原因描述";         //列名
		    iArray[6][1]="300px";         //宽度
		    iArray[6][2]=100;         //最大长度
		    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        BlacklistGrid = new MulLineEnter( "fm" , "BlacklistGrid" );

        //这些属性必须在loadMulLine前
        BlacklistGrid.mulLineCount = 10;
        BlacklistGrid.displayTitle = 1;
        BlacklistGrid.canSel=1;
      BlacklistGrid.hiddenPlus = 1;
      BlacklistGrid.hiddenSubtraction = 1;
        BlacklistGrid.loadMulLine(iArray);

      }
      catch(ex)
      {
        alert("初始化BlacklistGrid时出错："+ ex);
      }
    }


</script>