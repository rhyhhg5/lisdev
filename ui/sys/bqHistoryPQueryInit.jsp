<SCRIPT>
var turnPage = new turnPageClass();
var strSQL;
var mulLineC;



function initForm()
{
	initbqViewGrid();
}

//查询作业历史(个单)
function queryPEndorItemGrid()
{
	strSQL =  "	select a.EdorAcceptNo,a.EdorNo,a.EdorAppDate,a.DisplayType,a.GrpContNo,a.ContNo,a.InsuredNo, "
				+ "		a.PolNo,a.EdorValiDate,a.GetMoney,a.EdorType,a.MakeTime,a.Operator, "
				+ "		case a.EdorState "
				+ "			when '1' then '录入完成' "
				+ "			when '3' then '待录入' "
				+ "			when '0' then '保全确认' "
				+ "			when '4' then '试算完成' "
				+ "			when '2' then '理算完成'  "
				+ "		end, a.EdorType,b.applyname,a.ModifyDate"
				+ " from LPEdorItem a,LGWork b"
				+ " where a.EdorAcceptNo=b.workno  "
   		  + " and a.contno='"+tContNo+" ' "
				+ " order by a.MakeDate desc";
		
	turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSQL, bqViewGrid);
	
	mulLineC=bqViewGrid.mulLineCount;
	
	if(mulLineC==0)
	{
	fm.Query.disabled=true;
	}
	return true;
}

/* 查看批单 */
function viewpidan()
{	
	var chkNum = 0;
	var WorkNo;


	
	for (i = 0; i < bqViewGrid.mulLineCount; i++)
	{
		if (bqViewGrid.getChkNo(i)) 
		{
			chkNum = chkNum + 1;
			WorkNo = bqViewGrid.getRowColData(i, 1);

		}
	}
	if(chkNum > 1)
	{
		alert("您只能选择一条工单");
	}
	else if(chkNum == 0)
	{
		alert("请先选择一条工单！"); 
	}
 else	if(WorkNo )
	{
		window.open("../bq/ShowEdorPrint.jsp?EdorAcceptNo="+WorkNo )
		win.focus();
	}

	
}

/* 查看扫描 */
function ScanQuery() {
	var chkNum = 0;
	var WorkNo;

	
	for (i = 0; i < bqViewGrid.mulLineCount; i++)
	{
		if (bqViewGrid.getChkNo(i)) 
		{
			chkNum = chkNum + 1;
			WorkNo = bqViewGrid.getRowColData(i, 1);
			
			
		}
	}
	if(chkNum > 1)
	{
		alert("您只能选择一条工单");
	}
	else if(chkNum == 0)
	{
		alert("请先选择一条工单！"); 
	}
	else {
	   			
		
		if (WorkNo == "") return;
		    
	
		window.open("../sys/ProposalEasyScan.jsp?prtNo="+WorkNo+"&SubType=TB1001&BussType=TB&BussNoType=11", "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");								
	}	     
}

//初始化项目列表
function initbqViewGrid()
{                               
	var iArray = new Array();
	try
	{
	  	iArray[0]=new Array();
	  	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  	iArray[0][1]="0px";            		//列宽
	  	iArray[0][2]=10;            			//列最大值
	  	iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
	  	iArray[1]=new Array();
	  	iArray[1][0]="保全受理号";
	  	iArray[1][1]="110px";
	  	iArray[1][2]=100;
	  	iArray[1][3]=0;
	  	
	  	iArray[2]=new Array();
	  	iArray[2][0]="批单号";
	  	iArray[2][1]="0px";
	  	iArray[2][2]=100;
	  	iArray[2][3]=3;
	  	
	  	iArray[3]=new Array();
	  	iArray[3][0]="申请日期";
	  	iArray[3][1]="100px";
	  	iArray[3][2]=100;
	  	iArray[3][3]=1;

	  	
	  	iArray[4]=new Array();
	  	iArray[4][0]="批改类型显示级别";
	  	iArray[4][1]="0px";
	  	iArray[4][2]=100;
	  	iArray[4][3]=3;        
	  	
	  	iArray[5]=new Array();
	  	iArray[5][0]="集体合同号码";
	  	iArray[5][1]="0px";
	  	iArray[5][2]=100;
	  	iArray[5][3]=3;
	  	
	  	iArray[6]=new Array();
	  	iArray[6][0]="保单号码";
	  	iArray[6][1]="0px";
	  	iArray[6][2]=100;
	  	iArray[6][3]=3	;
	  	
	  	iArray[7]=new Array();
	  	iArray[7][0]="客户号码";
	  	iArray[7][1]="0px";
	  	iArray[7][2]=100;
	  	iArray[7][3]=3;
	  	
	  	iArray[8]=new Array();
	  	iArray[8][0]="保单险种号码";
	  	iArray[8][1]="100px";
	  	iArray[8][2]=100;
	  	iArray[8][3]=3;
	  	
	  	iArray[9]=new Array();
	  	iArray[9][0]="批改生效日期";
	  	iArray[9][1]="100px";
	  	iArray[9][2]=100;
	  	iArray[9][3]=3;
	  	
	  	iArray[10]=new Array();
	  	iArray[10][0]="补退费金额";
	  	iArray[10][1]="100px";
	  	iArray[10][2]=100;
	  	iArray[10][3]=3;
	  	
	  	iArray[11]=new Array();
	  	iArray[11][0]="保全类型";
	  	iArray[11][1]="80px";
	  	iArray[11][2]=100;
	  	iArray[11][3]=0;
	  	
	  	iArray[12]=new Array();
	  	iArray[12][0]="生成具体时间";
	  	iArray[12][1]="0px";
	  	iArray[12][2]=100;
	  	iArray[12][3]=3;
	  	

	  	
	  	iArray[13]=new Array();
	  	iArray[13][0]="最后处理人";
	  	iArray[13][1]="0px";
	  	iArray[13][2]=100;
	  	iArray[13][3]=3;
	
	  	
	  	iArray[14]=new Array();
	  	iArray[14][0]="处理状态";
	  	iArray[14][1]="80px";
	  	iArray[14][2]=100;
	  	iArray[14][3]=0;
	  	
	  	iArray[15]=new Array();
	  	iArray[15][0]="批改类型";
	  	iArray[15][1]="80px";
	  	iArray[15][2]=100;
	  	iArray[15][3]=3;
	
	    iArray[16]=new Array();
	  	iArray[16][0]="申请人";
	  	iArray[16][1]="80px";
	  	iArray[16][2]=100;
	  	iArray[16][3]=0;
	  	
	  	iArray[17]=new Array();
	  	iArray[17][0]="批改日期";
	  	iArray[17][1]="80px";
	  	iArray[17][2]=100;
	  	iArray[17][3]=0;
		bqViewGrid = new MulLineEnter( "fm" , "bqViewGrid" ); 
		//这些属性必须在loadMulLine前
		bqViewGrid.mulLineCount = 0;   
		bqViewGrid.displayTitle = 1;
		bqViewGrid.canSel =0;
	  bqViewGrid.canChk =1;
		bqViewGrid.selBoxEventFuncName ="getRowData" ;     //点击RadioBox时响应的JS函数
		bqViewGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		bqViewGrid.hiddenSubtraction=1;
		bqViewGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
	queryPEndorItemGrid();
	
}




</SCRIPT>