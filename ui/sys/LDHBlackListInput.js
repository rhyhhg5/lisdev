
var showInfo;

var turnPage = new turnPageClass();


//***************************************************
//* 验证必填项
//***************************************************
function checkValid()
{

	if (document.fm.Type.value== "")
	{
		alert("请录入客户类型!");
		document.fm.Type.value="";
		fm.all('Type').focus();
		return false;
	}
	if (document.fm.Name.value== "")
	{
		alert("请输入姓名!");
		document.fm.Name.value="";
		fm.all('Name').focus();
		return false;
	}

	if (document.fm.Type.value== "0" && (document.fm.IDType.value== "" || document.fm.Sex.value== "" || document.fm.Birthday.value== "" || document.fm.IDType.value== ""))
	{
		alert("黑名单用户类型为个人，证件类型,证件号码,出生日期,性别为必录项。");
		return false;
	}
	
//	if (document.fm.Type.value== "1" && document.fm.OrganComCode.value == "" && document.fm.UnifiedSocialCreditNo.value == "")
//	{
//		alert("黑名单用户类型为组织，请录入组织机构代码或统一社会信用代码");
//		return false;
//	}
	
//	if (document.fm.Cause.value== "")
//	{
//		alert("请录入进入黑名单原因!");
//		document.fm.Cause.value="";
//		fm.all('Cause').focus();
//		return false;
//	}
	return true;
}


//***************************************************
//* 点击‘保存’进行的操作
//***************************************************
function submitForm()
{

    if (checkValid() == false)
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN";
  fm.submit(); //提交
  
  return true;
}


//***************************************************
//* 单击“修改”进行的操作
//***************************************************

function updateClick()
{
  //下面增加相应的代码

  if (fm.all('BlacklistNo').value==null || fm.all('BlacklistNo').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('BlacklistNo').value="";
 	return;
}
  
  if (fm.all('Name').value==null || fm.all('Name').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('Name').value="";
 	return;
}
  
  if (checkValid() == false)
 {
	   return false;
}
  
  if (confirm("您确实想修改该条记录?"))
  {
 
  //showSubmitFrame(mDebug);
  
   //alert("xiu1");
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}           
//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content,BlacklistNo)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  fm.all('BlacklistNo').value  = BlacklistNo;
}

function initUpdate()
{
	if(transact == "update"){
		if(tBlacklistNo == null || tBlacklistNo == ""){
			alert("修改操作时，获取黑名单人员编码失败！");
			return false;
		}
		var tSQL = "select BlacklistNo," 
			+ " Type," 
			+ " (case when Type = '0' then '个人' when Type = '1' then '组织' end ) TypeName," 
			+ " Name," 
			+ " Sex," 
			+ " (case when SEX = '0' then '男' when SEX = '1' then '女' end ) SexName," 
			+ " IDType,"
			+ " (select codename from ldcode where codetype = 'idtype' and code = IDType) IDTypeName,"
			+ " IDNo, "
			+ " Birthday,"
			+ " OrganComCode,"  //组织机构代码
			+ " UnifiedSocialCreditNo,"  //统一社会信用代码
			+ " Nationality,"  
			+ " (select codename from ldcode where codetype = 'nativeplace' and code = Nationality) NationalityName,"  
			+ " PublishCom,"  //发布机构
			+ " Cause,"
			+ " (case when Cause = '01' then '同业重疾或伤残理赔史' when Cause = '02' then '密集投保' when Cause = '03' then '不如实告知史' when Cause = '04' then '其他' end ) CauseName,"  //原因
			+ " Remark"
		    + " FROM "
		    + " LDHBlackList" 
		    + " where  1=1 and BlacklistNo = '"+tBlacklistNo+"' ";
		
		//执行查询并返回结果
		var strQueryResult = easyExecSql(tSQL);
		if(!strQueryResult){
			alert("修改操作时，根据黑名单人员编码获取人员信息失败！");
			return false;
		}
		fm.all('BlacklistNo').value = strQueryResult[0][0];
	    fm.all('Type').value = strQueryResult[0][1];
	    fm.all('TypeName').value = strQueryResult[0][2];
	    fm.all('Name').value = strQueryResult[0][3];
	    fm.all('Sex').value = strQueryResult[0][4];
	    fm.all('SexName').value = strQueryResult[0][5];
	    fm.all('IDType').value = strQueryResult[0][6];
	    fm.all('IDTypeName').value = strQueryResult[0][7];
	    fm.all('IDNo').value = strQueryResult[0][8];
	    fm.all('Birthday').value = strQueryResult[0][9];
	    fm.all('OrganComCode').value = strQueryResult[0][10];
	    fm.all('UnifiedSocialCreditNo').value = strQueryResult[0][11];
	    fm.all('Nationality').value = strQueryResult[0][12];
	    fm.all('NationalityName').value = strQueryResult[0][13];
	    fm.all('PublishCom').value = strQueryResult[0][14];
	    fm.all('Cause').value = strQueryResult[0][15];
	    fm.all('CauseName').value = strQueryResult[0][16];
	    fm.all('Remark').value = strQueryResult[0][17];
	}
	return true;
}