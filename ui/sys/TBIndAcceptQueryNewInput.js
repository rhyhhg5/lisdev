var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>3)
	{
		alert("统计期最多为三个月！");
		return false;
	}
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.target = "f1print";
	fm.submit();
	showInfo.close();
}