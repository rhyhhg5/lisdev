<%
//程序名称：ClientMergeInit.jsp
//程序功能：
//创建日期：2002-08-19
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('Name').value = '1';
    fm.all('Sex').value = 'm';
    fm.all('Birthday').value = '1971-1-1';
    fm.all('Marriage').value = '';
    fm.all('OccupationType').value = '';
    fm.all('Health').value = '';

    fm.all('IDType').value = '';
    fm.all('Proterty').value = '';
    fm.all('IDNo').value = '';
    fm.all('ICNo').value = '';
    fm.all('OthIDType').value = '';
    fm.all('OthIDNo').value = '';

    fm.all('GrpName').value = '';
    fm.all('GrpAddress').value = '';
    fm.all('HomeAddressCode').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('Phone').value = '';
    fm.all('BP').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';

  }
  catch(ex)
  {
    alert("在ClientMergeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initClientList( );
  }
  catch(re)
  {
    alert("ClientMergeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initClientList()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		    //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="客户号";    	        //列名
      iArray[1][1]="180";            		//列宽
      iArray[1][2]=180;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="婚姻状况";    	        //列名
      iArray[2][1]="180";            		//列宽
      iArray[2][2]=180;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="职业类别"; 	            //列名
      iArray[3][1]="100";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="健康状况"; 	            //列名
      iArray[4][1]="100";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="证件类别"; 	            //列名
      iArray[5][1]="100";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="证件属性"; 	            //列名
      iArray[6][1]="100";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="证件号码"; 	            //列名
      iArray[7][1]="100";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      ClientList = new MulLineEnter( "fm" , "ClientList" ); 
      //这些属性必须在loadMulLine前
      ClientList.mulLineCount = 3;   
      ClientList.displayTitle = 1;
      ClientList.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
    } catch(ex) {
      alert(ex);
    }
}

</script>