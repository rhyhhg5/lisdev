 //               该文件中包含客户端需要处理的函数和事件

//var tTableType: C:C表数据；B：B表数据

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 
var turnPage4 = new turnPageClass(); 
var arrAllDateSet;
var cPerCustomerNo = "";
var tContType="";  //1：个单；2：团单
var mContNo = "";

var tSel = -1;	//所选的有效保单行
var tSel1 = -1; //所选的无效保单行

// 王珑制作

/*********************************************************************
 *  查询按钮实现
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
    if (trim(fm.all('CustomerNo').value) ==''&& trim(fm.all('Name').value) == ''
      && trim(fm.all('Sex').value) == ''
      && trim(fm.all('Birthday').value) == ''&& trim(fm.all('IDNo').value) == ''
      && trim(fm.all('HomeAddress').value) == '' &&  trim(fm.all('HomeZipCode').value) == '' 
      )
    {
      alert("请输入客户相关查询条件");
      try{
      	showInfo.close();
    	}catch(ex)
    	{
    	}
      return false;
    }
    else
    {
      initPersonGrid();
      initGrpPersonGrid();
      if(fm.CustomerKind.value != '2')
      {
        setPersonValue();
      }
      if(fm.CustomerKind.value != '1')
      {
        setGrpValue();
      }
      
      if(PersonGrid.mulLineCount == 0 && GrpPersonGrid.mulLineCount == 0)
      {
        alert("没有查询到客户信息");
      }
    }
    
    clearGlobalVar();
    initGrpPolGrid();
    initBContGrid();
    
    showCodeName();   
}

/*********************************************************************
 *  查询个单客户信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setPersonValue()
{
	 initPersonGrid();
		var strSQL = "";
	  if (fm.all('HomeAddress').value == '' &&  fm.all('HomeZipCode').value == '')
    {
   
		   strSQL = "select a.CustomerNo as CustomerNo, a.Name, CodeName('sex', a.Sex), a.Birthday, CodeName('idtype', a.IDType),a.IDNo,"
		        		+ "  case when (select count(1) from LCAppnt where AppntNo = a.CustomerNo) > 0 then '投保人' else '被保人' end, "
		        		+" case when (select count(1) from LCAppnt where AppntNo = a.CustomerNo) > 0 then "
		        		+" (select case max(HighestEvaLevel) "
		        		+" when 1 then "
		        		+"  '低风险' "
		        		+" when 2 then "
		        		+"  '中风险' "
		        		+" when 3 then "
		        		+"  '高风险' "
		        		+" else"
		        		+"  '' "
		        		+" end "
		        		+"  from fx_FXQVIEW"
		        		+" where AOGTYPE = '1' "
		        		+" and AOGNAME = a.Name"
		        		+" and APPNTIDTYPE = a.IDtype"
		        		+" and APPNTIDNO = a.IDNo"
		        		+" and APPNTBIRTHDAY = a.Birthday"
		        		+" and APPNTSEX = a.Sex) "
		        		+" else '' end ,case when (a.Authorization='1') then '是' when (a.Authorization='0') then '否' end "
		   					+ " from LDPerson a where 1=1 "	
		   					+ getWherePart( 'a.CustomerNo','CustomerNo') 
		   					+ getWherePart( 'a.Name','Name') 
		   					+ getWherePart( 'a.Birthday','Birthday') 
		   					+ getWherePart( 'a.Sex','Sex') 
		   					+ getWherePart( 'a.IDNo','IDNo')
		   					;
	  }
	  else
	  {
	  	  strSQL = "select a.CustomerNo as CustomerNo, a.Name, CodeName('sex', a.Sex), a.Birthday, CodeName('idtype', a.IDType), a.IDNo, "
     					+ "  case when (select count(1) from LCAppnt where AppntNo = a.CustomerNo) > 0 then '投保人' else '被保人' end, "
     					+" case when (select count(1) from LCAppnt where AppntNo = a.CustomerNo) > 0 then "
     					+" (select case max(HighestEvaLevel) "
		        		+" when 1 then "
		        		+"  '低风险' "
		        		+" when 2 then "
		        		+"  '中风险' "
		        		+" when 3 then "
		        		+"  '高风险' "
		        		+" else"
		        		+"  '' "
		        		+" end "
		        		+"  from fx_FXQVIEW"
		        		+" where AOGTYPE = '1' "
		        		+" and AOGNAME = a.Name"
		        		+" and APPNTIDTYPE = a.IDtype"
		        		+" and APPNTIDNO = a.IDNo"
		        		+" and APPNTBIRTHDAY = a.Birthday"
		        		+" and APPNTSEX = a.Sex) "
		        		+" else '' end ,case when (a.Authorization='1') then '是' when (a.Authorization='0') then '否' end "
	   					+ " from LDPerson a "
	   					+ " where 1=1 and exists ("	
	   					+ " select 1 from LCAddress b where b.CustomerNo = a.CustomerNo "
	   					+ getWherePart( 'b.HomeAddress','HomeAddress','like' ) 
	   					+ getWherePart( 'b.HomeZipCode','HomeZipCode' ,'like')			   		
				   		+ " )"
	   					+ getWherePart( 'a.CustomerNo','CustomerNo') 
		   				+ getWherePart( 'a.Name','Name') 
	   					+ getWherePart( 'a.Birthday','Birthday' ) 
	   					+ getWherePart( 'a.Sex','Sex') 
	   					+ getWherePart( 'a.IDNo','IDNo')		   
	   					;
	  }
	  strSQL = strSQL + " order by CustomerNo with ur"; 
	turnPage1.queryModal(strSQL, PersonGrid);  
}


/*********************************************************************
 *  查询团单客户信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setGrpValue()
{
	initGrpPersonGrid();
  var  strSQL ="";
  if (fm.all('Birthday').value == '' && fm.all('Sex').value == ''&&  fm.all('IDNo').value == '')
  {
  
    strSQL = "select distinct LDGrp.CustomerNo,LDGrp.GrpName,LDGrp.Satrap,LDGrp.FoundDate, "
    + "   CodeName('grpnature', LDGrp.GrpNature),CodeName('businesstype', LDGrp.BusinessType), "
    + " (select case max(HighestEvaLevel)"
    + " when 1 then"
    + " '低风险' "
    + " when 2 then"
    + " '中风险' "
    + " when 3 then"
    + " '高风险' "
    + " else "
    + " '' "
    + " end"
    + " from fx_FXQVIEW"
    + " where AOGNAME = LDGrp.GrpName"
    + " and AOGTYPE = '2') "
    + " from LDGrp, LCGrpAddress "
    + " where LDGrp.CustomerNo=LCGrpAddress.CustomerNo "	
    + getWherePart( 'LDGrp.CustomerNo','CustomerNo' ) 
    + getWherePart( 'LDGrp.GrpName','Name','like' ) 	   	   
    + getWherePart( 'LCGrpAddress.GrpAddress','HomeAddress','like' ) 
    + getWherePart( 'LCGrpAddress.GrpZipCode','HomeZipCode' ) 
    + " order by LDGrp.CustomerNo";
    turnPage2.queryModal(strSQL,GrpPersonGrid);
  }
  else
  {
  }
}



/*********************************************************************
 *  通过个单客户查询有效保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setPolValue()
{
	initGrpPolGrid();
	clearContQueryBox();
	
  var customerNo = PersonGrid.getRowColData(PersonGrid.getSelNo() - 1, 1);
 
	//保单性质为1：已签单； 2：撤保或退保
	//查询保单信息
	var strSQL = 
	    //个人做为投保人
	    "select a.ContNo,a.PrtNo,'个单', "
	    + "   case when hasLongRisk(a.ContNo) = 'L' then '个险长期' else '个险一年' end, "
	    + "   AppntName,a.InsuredName,a.CValiDate, a.CInvaliDate, a.PayToDate, a.Prem, "
	    + "   codeName('stateflag', StateFlag), "
      + "   ShowManageName(a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only), "
      + "   (select Name from LAAgent where AgentCode = a.AgentCode), '1', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom ,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.Phone from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.PostalAddress from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ) "
      + "from LCCont a "
      + "where AppFlag='1' "
      + "   and ContType = '1' "
      + "   and (StateFlag is null or StateFlag in('1'))"
      + "   and AppntNo='" + customerNo + "' "
      
      //个人作为个单被保人
      + "union "
      + "select a.ContNo,a.PrtNo,'个单', "
      + "   case when hasLongRisk(a.ContNo) = 'L' then '个险长期' else '个险一年' end, "
      + "   a.AppntName,a.InsuredName,a.CValiDate, a.CInvaliDate, a.PayToDate, a.Prem, "
      + "   codeName('stateflag', StateFlag), ShowManageName(a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only), "
      + "   (select Name from LAAgent where AgentCode = a.AgentCode), '1', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.Phone from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.PostalAddress from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno )  "
      + "from LCCont a, LCInsured b "
      + "where a.AppFlag = '1'  "
      + "   and ContType = '1' "
      + "   and a.ContNo = b.ContNo "
      + "   and (StateFlag is null or StateFlag in('1'))"
      + "   and b.InsuredNo = '" + customerNo + "' "
      
      //个人作为个单被保人(多被保险人)
      + "union all"
      + " select a.ContNo,a.PrtNo,'个单', "
      + " case when hasLongRisk(a.ContNo) = 'L' then '个险长期' else '个险一年' end, "
      + " a.AppntName,a.InsuredName,a.CValiDate, a.CInvaliDate, a.PayToDate, a.Prem, "
      + " codeName('stateflag', StateFlag), ShowManageName(a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only), "
      + " (select Name from LAAgent where AgentCode = a.AgentCode), '1', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.Phone from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.PostalAddress from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno )  "
      + " from LCCont a, LBInsured b "
      + " where a.AppFlag = '1'  "
      + " and ContType = '1' "
      + " and a.ContNo = b.ContNo "
      + " and (StateFlag is null or StateFlag in('1'))"
      + " and b.InsuredNo = '" + customerNo + "' "
      
      //个人作为作为团单被保人
      + " union all "  //这里用union all比用union效率高
      + "select a.GrpContNo, a.PrtNo, '团单', "
      + "   case when isNoNameCont(a.GrpContNo) = 'W' then '团险无名单' else '团险普通保单' end, "
      + "   a.GrpName, '', a.CValiDate, a.CInvaliDate, "
      + "   (select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo and (StateFlag is null or StateFlag != '3')), "
      + "   a.Prem, codeName('stateflag', a.Stateflag), ShowManageName(a.ManageCom), (select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only),"
      + "   (select Name from LAAgent where AgentCode = a.AgentCode), '2', a.ManageCom , 'C'"
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,(select codename from ldcode where 1 = 1 and codetype = 'markettype' and code=a.MarketType),(select c.Phone1 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ),(select c.Phone2 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )  "
      + "from LCGrpCont a, LCInsured b "
      + "where a.GrpContNo = b.GrpContNo "
      + "   and (a.StateFlag is null or a.StateFlag in('1'))"
      + "   and a.AppFlag = '1' "
      + "   and b.InsuredNo = '" + customerNo + "' " 
      
      //个人作为作为团单被保人（多被保险人）
      + " union all "  //这里用union all比用union效率高
      + " select a.GrpContNo, a.PrtNo, '团单', "
      + " case when isNoNameCont(a.GrpContNo) = 'W' then '团险无名单' else '团险普通保单' end, "
      + " a.GrpName, '', a.CValiDate, a.CInvaliDate, "
      + " (select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo and (StateFlag is null or StateFlag != '3')), "
      + " a.Prem, codeName('stateflag', a.Stateflag), ShowManageName(a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only), "
      + " (select Name from LAAgent where AgentCode = a.AgentCode), '2', a.ManageCom , 'C'"
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,(select codename from ldcode where 1 = 1 and codetype = 'markettype' and code=a.MarketType),(select c.Phone1 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ),(select c.Phone2 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )  "
      + " from LCGrpCont a, LBInsured b "
      + " where a.GrpContNo = b.GrpContNo "
      + " and (a.StateFlag is null or a.StateFlag in('1'))"
      + " and a.AppFlag = '1' "
      + " and b.InsuredNo = '" + customerNo + "' "  
      ;    
    fm.ContSql.value = strSQL;
    turnPage3.pageDivName = "divPage3";
    turnPage3.queryModal(strSQL, GrpPolGrid);
    
    document.all("divPage4").style.display="";
    document.all("divPage3").style.display="";
    
    //个单判断保单是否续保  
    
  
//    var Count = GrpPolGrid.mulLineCount; //
//    alert("AA"+ Count+ "fdf"+ turnPage3.queryAllRecordCount);
//    for(var i = 0; i < Count; i++)
//    {
//  	  var isrnew = false;
//  	  var sSQL = "  select cvalidate from lbcont where contno =(select distinct newcontno from lcrnewstatelog where  contno = '"
//  		  + GrpPolGrid.getRowColData(i, 1) +"') ";
//  	  var rs = easyExecSql(sSQL);
//  	
//  	  if(rs){ //是续保 加续保标识
//  		alert("CC"+ rs[0][0]);
//  		  GrpPolGrid.setRowColData(i,28,rs[0][0]); 
//  		  isrnew = true;
//  	  }
//  	  else {//不是续保 可能是续期
//  		
//  		  var sxqSQL = "  select cvalidate from lccont where contno ='"+   GrpPolGrid.getRowColData(i, 1) +"'"; 
//  		  var xqrs = easyExecSql(sxqSQL); 
// 
//  		  if(xqrs){
//  			  GrpPolGrid.setRowColData(i,28,xqrs[0][0]); 
//  		  }
//  	  }
//   
//  	  
//    }
    next();
    
    setPolValue1();
}	
function next(){
	
	 var Count = GrpPolGrid.mulLineCount; //
	    //套餐ZZJ228为无社保，ZZJ229为有社保
	    for(var i = 0; i < Count; i++)
	    {
	    	
	    	if(GrpPolGrid.getRowColData(i, 3).indexOf("个") >= 0){
	    		
	    		 var sdsSQL = "  select cardflag ,mult from lccont where contno ='"+   GrpPolGrid.getRowColData(i, 1) +"'"; 
	    		 if(easyExecSql(sdsSQL)[0][0] =="b"){ //电商电子
	  	  var isrnew = false;
	  	  
	  		var staikangSQL = "select 1 from lcriskdutywrap where riskwrapcode in ( select code from ldcode where codetype='zhqueryzb' ) and contno ='"+   GrpPolGrid.getRowColData(i, 1)+"'" ;
	  		var taikangrs = easyExecSql(staikangSQL);
	  	  if(taikangrs){  //泰康转过来的保单
	  		  var sxbSQL = "  select cvalidate ,mult from lccont where contno ='"+   GrpPolGrid.getRowColData(i, 1) +"'"; 
	  		  var xbrs = easyExecSql(sxbSQL); 
	  		try{GrpPolGrid.setRowColData(i,28,xbrs[0][0]); }catch(ex){} 	  		  
	  		  GrpPolGrid.setRowColData(i,29,"是"); 
//	  		  if (xbrs[0][1]=="1"){
//	  			 GrpPolGrid.setRowColData(i,30,"无"); 
//	  		  }
//	  		  else if (xbrs[0][1]=="2"){
//		  			 GrpPolGrid.setRowColData(i,30,"有"); 
//		  		  }
	  		  
	  		
  			  var sxqsbSQL ="  select codealias ,OTHERSIGN from ldcode where codetype ='zhqueryyb' and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and contno ='"+   GrpPolGrid.getRowColData(i, 1) +"')";
  		    	var sebaosql = easyExecSql(sxqsbSQL);
  	  	       if(sebaosql){
  	  	    	   if(sebaosql[0][0]=="N"){
  	  	    		GrpPolGrid.setRowColData(i,30,"无"); 
  	  	    	   }
  	  	    	   if(sebaosql[0][0]=="Y"){
  	  	    		GrpPolGrid.setRowColData(i,30,"有");    
  	  	    	   }
	  	    	       if(sebaosql[0][1]=="Nowait"){
		  	    		 GrpPolGrid.setRowColData(i,29,"无等待期");  
//		  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ GrpPolGrid.getRowColData(i, 1)+"' ";
//			  			 try{ GrpPolGrid.setRowColData(i,28,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
			  	    	   }
		           }	
		  		  var lcmult =" select mult from lcpol where contno ='"+   GrpPolGrid.getRowColData(i, 1) +"'  and riskcode =(select CodeName from ldcode where codetype='zhquerymult' and exists(select riskwrapcode from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   GrpPolGrid.getRowColData(i, 1) +"' ))";
		  		  var lcpolmult = easyExecSql(lcmult);
  		  	   if(lcpolmult){
	  		  		var sxqmultSQL = "  select ldcode.codealias ,ldcode.OTHERSIGN from ldcode where codetype in ('zhqueryyby','zhqueryybn') and  ldcode.comcode ='"+lcpolmult[0][0]+"' and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   GrpPolGrid.getRowColData(i, 1) +"')";
		  		    var sxqyibaosql = easyExecSql(sxqmultSQL);
		  		    if (sxqyibaosql){
			 	      if(sxqyibaosql[0][0]=="NO"){
		  	    		GrpPolGrid.setRowColData(i,30,"无"); 
		  	    	   }
		  	    	   if(sxqyibaosql[0][0]=="YES"){
		  	    		GrpPolGrid.setRowColData(i,30,"有");    
		  	    	   }
		  	         if(sxqyibaosql[0][1]=="Nowait"){
		  	    		 GrpPolGrid.setRowColData(i,29,"无等待期");  
//		  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ GrpPolGrid.getRowColData(i, 1)+"' ";
//			  			 try{ GrpPolGrid.setRowColData(i,28,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
			  	    	   }
		  		    }
  	  	    
	  	        }
	  	  }
	  	  else {
//	  	  var sSQL = "  select cvalidate from lbcont where contno =(select distinct newcontno from lcrnewstatelog where 1=1 and  (contno = '"
//	  		  + GrpPolGrid.getRowColData(i, 1) +"' or newcontno =' "+ GrpPolGrid.getRowColData(i, 1) +"' )) ";
	  	  
	  	  
	  	  var olsql=" select  newcontno, contno from lcrnewstatelog where 1=1  and state='6' and  (contno = '"
	  		  + GrpPolGrid.getRowColData(i, 1) +"' or newcontno ='"+ GrpPolGrid.getRowColData(i, 1) +"' ) order by makedate ";
	  	  var rs = easyExecSql(olsql);
	 
	  	  if(rs){ //是续保 加续保标识
	  	
//	  		  if(rs[0][0].indexOf("E") >= 0) //电商单子的续保
//	  		  {
//	  			  
//	  			  if(rs[0][1].indexOf("E") >= 0){
//		  				 var soldSQL = "  select cvalidate from lbcont where contno = '"+ rs[0][0]+"' union select cvalidate from lccont where contno = '"+ rs[0][0]+"' ";
//		  				 try{ GrpPolGrid.setRowColData(i,28,easyExecSql(soldSQL)[0][0]); }catch(ex){}
//		  			  }else {//老电商单子新电商续保
//		  				 var sSQL = "  select cvalidate from lbcont where contno = '"+ rs[0][1]+"' union select cvalidate from lccont where contno = '"+ rs[0][1]+"' ";
//		  				 try{ GrpPolGrid.setRowColData(i,28,easyExecSql(sSQL)[0][0]); }catch(ex){}
//		  			  }
//	  		  }
//	  		  else {
//	  			 var sSQL2 = "  select cvalidate from lbcont where contno = '"+ rs[0][0]+"' union select cvalidate from lccont where contno = '"+ rs[0][0]+"' ";
//	  			 try{ GrpPolGrid.setRowColData(i,28,easyExecSql(sSQL2)[0][0]);  }catch(ex){}
//	  		  }
	  		 
	  		 var soldSQL = "  select cvalidate from lbcont where (contno = '"+ rs[0][0]+"' or contno= '"+ rs[0][1]+"') union select cvalidate from lccont where (contno = '"+ rs[0][0]+"' or contno= '"+ rs[0][1]+"') order by cvalidate  ";
				try{GrpPolGrid.setRowColData(i,28,easyExecSql(soldSQL)[0][0]); }catch(ex){}	  		  
	  		  
	  		  GrpPolGrid.setRowColData(i,29,"是"); 
  
	  	   
	  		  var sxqsbSQL = "  select ldcode.codealias ,ldcode.OTHERSIGN from ldcode where codetype ='zhqueryyb'  and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   GrpPolGrid.getRowColData(i, 1) +"')";
		    	var sebaosql = easyExecSql(sxqsbSQL);
	  	       if(sebaosql){
	  	    	   if(sebaosql[0][0]=="N"){
	  	    		GrpPolGrid.setRowColData(i,30,"无"); 
	  	    	   }
	  	    	   if(sebaosql[0][0]=="Y"){
	  	    		GrpPolGrid.setRowColData(i,30,"有");    
	  	    	   }
	  	    	   if(sebaosql[0][1]=="Nowait"){
		  	    		 GrpPolGrid.setRowColData(i,29,"无等待期");  
		  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ GrpPolGrid.getRowColData(i, 1)+"' ";
			  			 try{ GrpPolGrid.setRowColData(i,28,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
			  	     }

	  	     }
		  		  var lcmult =" select mult from lcpol where contno ='"+  GrpPolGrid.getRowColData(i, 1) +"'  and riskcode =(select CodeName from ldcode where codetype='zhquerymult' and exists(select riskwrapcode from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   GrpPolGrid.getRowColData(i, 1) +"' ))";
		  		  var lcpolmult = easyExecSql(lcmult);	
	  	    	 if(lcpolmult){  
	  		  		  var sxqmultSQL = "  select ldcode.codealias ,ldcode.OTHERSIGN from ldcode where codetype in ('zhqueryyby','zhqueryybn') and  ldcode.comcode ='"+lcpolmult[0][0]+"' and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   GrpPolGrid.getRowColData(i, 1) +"')";
	  		    	var sxqyibaosql = easyExecSql(sxqmultSQL);
	  		    	if(sxqyibaosql){
			 	   if(sxqyibaosql[0][0]=="NO"){
		  	    		GrpPolGrid.setRowColData(i,30,"无"); 
		  	    	   }
		  	    	   if(sxqyibaosql[0][0]=="YES"){
		  	    		GrpPolGrid.setRowColData(i,30,"有");    
		  	    	   }
		  	    	   
		  	    	   if(sxqyibaosql[0][1]=="Nowait"){
			  	    		 GrpPolGrid.setRowColData(i,29,"无等待期");  
			  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ GrpPolGrid.getRowColData(i, 1)+"' ";
				  			 try{ GrpPolGrid.setRowColData(i,28,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
				  	     }
	  		    	}
	  	      }
	  		  isrnew = true;
	  	  }
	  	  else {//不是续保 可能是续期

	  		  var sxqSQL = "  select cvalidate ,mult from lccont where contno ='"+   GrpPolGrid.getRowColData(i, 1) +"'"; 
	  		  var xqrs = easyExecSql(sxqSQL); 
	 
	  		  if(xqrs){
	  			   isrnew = false;
	  			 try{  GrpPolGrid.setRowColData(i,28,xqrs[0][0]); }catch(ex){}
	  			  GrpPolGrid.setRowColData(i,29,"否"); 
		  		  
	
	  			  var sxqsbSQL ="  select codealias ,OTHERSIGN from ldcode where codetype ='zhqueryyb' and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and contno ='"+   GrpPolGrid.getRowColData(i, 1) +"')";
	  		    	var sebaosql = easyExecSql(sxqsbSQL);
	  	  	       if(sebaosql){
	  	  	    	   if(sebaosql[0][0]=="N"){
	  	  	    		GrpPolGrid.setRowColData(i,30,"无"); 
	  	  	    	   }
	  	  	    	   if(sebaosql[0][0]=="Y"){
	  	  	    		GrpPolGrid.setRowColData(i,30,"有");    
	  	  	    	   }
		  	    	       if(sebaosql[0][1]=="Nowait"){
			  	    		 GrpPolGrid.setRowColData(i,29,"无等待期");  
			  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ GrpPolGrid.getRowColData(i, 1)+"' ";
				  			 try{ GrpPolGrid.setRowColData(i,28,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
				  	    	   }
  		           }	
			  		  var lcmult =" select mult from lcpol where contno ='"+   GrpPolGrid.getRowColData(i, 1) +"'  and riskcode =(select CodeName from ldcode where codetype='zhquerymult' and exists(select riskwrapcode from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   GrpPolGrid.getRowColData(i, 1) +"' ))";
			  		  var lcpolmult = easyExecSql(lcmult);
	  		  	   if(lcpolmult){
		  		  		var sxqmultSQL = "  select ldcode.codealias ,ldcode.OTHERSIGN from ldcode where codetype in ('zhqueryyby','zhqueryybn') and  ldcode.comcode ='"+lcpolmult[0][0]+"' and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   GrpPolGrid.getRowColData(i, 1) +"')";
			  		    var sxqyibaosql = easyExecSql(sxqmultSQL);
			  		    if (sxqyibaosql){
				 	      if(sxqyibaosql[0][0]=="NO"){
			  	    		GrpPolGrid.setRowColData(i,30,"无"); 
			  	    	   }
			  	    	   if(sxqyibaosql[0][0]=="YES"){
			  	    		GrpPolGrid.setRowColData(i,30,"有");    
			  	    	   }
			  	         if(sxqyibaosql[0][1]=="Nowait"){
			  	    		 GrpPolGrid.setRowColData(i,29,"无等待期");  
			  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ GrpPolGrid.getRowColData(i, 1)+"' ";
				  			 try{ GrpPolGrid.setRowColData(i,28,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
				  	    	   }
			  		    }
	  	  	    
		  	        }
	  	        }
	  		  }  	       
	  	  }
	   }
	  	  
	}
   }
}

function gnext(){
	
	 var Count = BContGrid.mulLineCount; //
	  
	    //套餐ZZJ228为无社保，ZZJ229为有社保
	    for(var i = 0; i < Count; i++)
	    {
	    	if(BContGrid.getRowColData(i, 3).indexOf("个") >= 0){
	    		 var sdsSQL = "  select cardflag ,mult from lccont where contno ='"+   BContGrid.getRowColData(i, 1) +"'"; 
	    		 if(easyExecSql(sdsSQL)[0][0] =="b"){ //电商电子
	    		
	  	  var isrnew = false;
	  	  //ZZJ165 泰康转过来的保单
	  		var staikangSQL = "select 1 from lcriskdutywrap where riskwrapcode in( select code from ldcode where codetype='zhqueryzb' ) and contno ='"+   BContGrid.getRowColData(i, 1)+"'" ;
	  		var taikangrs = easyExecSql(staikangSQL);
	  	  if(taikangrs){
	  		  var sxbSQL = "  select cvalidate ,mult from lccont where contno ='"+   BContGrid.getRowColData(i, 1) +"'"; 
	  		  var xbrs = easyExecSql(sxbSQL); 
	  		try{BContGrid.setRowColData(i,27,xbrs[0][0]); }catch(ex){}		  
	  		BContGrid.setRowColData(i,28,"是"); 
//	  		  if (xbrs[0][1]=="1"){
//	  			BContGrid.setRowColData(i,29,"无"); 
//	  		  }
//	  		  else if (xbrs[0][1]=="2"){
//	  			BContGrid.setRowColData(i,29,"有"); 
//		  		  }
	  		  var lcmult =" select mult from lcpol where contno ='"+   BContGrid.getRowColData(i, 1) +"'  and riskcode =(select CodeName from ldcode where codetype='zhquerymult' and exists(select riskwrapcode from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   BContGrid.getRowColData(i, 1) +"' ))";
	  		  var lcpolmult = easyExecSql(lcmult);
	  		  
			  var sxqsbSQL = "   select codealias, OTHERSIGN from ldcode where codetype = 'zhqueryyb'  and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code   and  contno ='"+   BContGrid.getRowColData(i, 1) +"')";
		    	var sebaosql = easyExecSql(sxqsbSQL);
	  	       if(sebaosql){
	  	    	   if(sebaosql[0][0]=="N"){
	  	    		BContGrid.setRowColData(i,29,"无"); 
	  	    	   }
	  	    	   if(sebaosql[0][0]=="Y"){
	  	    		BContGrid.setRowColData(i,29,"有");    
	  	    	   }
	  	    	   
	  	    	   if(sebaosql[0][1]=="Nowait"){
	  	    		 BContGrid.setRowColData(i,28,"无等待期");  
//		  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ BContGrid.getRowColData(i, 1)+"' ";
//			  			 try{ BContGrid.setRowColData(i,27,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
			  	    }
		  	  
	  	       }
  		  	   if(lcpolmult){
	  		  		var sxqmultSQL = "  select ldcode.codealias ,ldcode.OTHERSIGN from ldcode where codetype in ('zhqueryyby','zhqueryybn') and  ldcode.comcode ='"+lcpolmult[0][0]+"' and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   BContGrid.getRowColData(i, 1) +"')";
		  		    var sxqyibaosql = easyExecSql(sxqmultSQL);
		  		    if (sxqyibaosql){
				 if(sxqyibaosql[0][0]=="NO"){
						BContGrid.setRowColData(i,29,"无"); 
			  	      }
			  	     if(sxqyibaosql[0][0]=="YES"){
			  	  		BContGrid.setRowColData(i,29,"有");     
			  	     }
			  	   if(sxqyibaosql[0][1]=="Nowait"){
		  	    		 BContGrid.setRowColData(i,28,"无等待期");  
//			  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ BContGrid.getRowColData(i, 1)+"' ";
//				  			 try{ BContGrid.setRowColData(i,27,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
				  	    }
		  		    }
		    }
	  		
	  		
	  	  }
	  	  else {
//	  	  var sSQL = "  select cvalidate from lbcont where contno =(select distinct newcontno from lcrnewstatelog where 1=1 and  (contno = '"
//	  		  + BContGrid.getRowColData(i, 1) +"' or newcontno =' "+ BContGrid.getRowColData(i, 1) +"' )) ";
	 
	  		  var olsql=" select  newcontno, contno from lcrnewstatelog where 1=1 and state='6' and  (contno = '"
		  		  + BContGrid.getRowColData(i, 1) +"' or newcontno ='"+ BContGrid.getRowColData(i, 1) +"' ) order by makedate";
		  	  var rs = easyExecSql(olsql);
		  	
		  	  if(rs){ //是续保 加续保标识
		  	
//		  		  if(rs[0][0].indexOf("E") >= 0) //电商单子的续保
//		  		  {
//		  			  if(rs[0][1].indexOf("E") >= 0){
//		  				 var soldSQL = "  select cvalidate from lbcont where contno = '"+ rs[0][0]+"' union select cvalidate from lccont where contno = '"+ rs[0][0]+"'";
//		  				try{BContGrid.setRowColData(i,27,easyExecSql(soldSQL)[0][0]); }catch(ex){}		
//		  			  }else {//老电商单子新电商续保
//		  				 var sSQL = "  select cvalidate from lbcont where contno = '"+ rs[0][1]+"' union select cvalidate from lccont where contno = '"+ rs[0][1]+"' ";
//		  				try{BContGrid.setRowColData(i,27,easyExecSql(sSQL)[0][0]); }catch(ex){}		
//		  			  }
//		  			
//		  		  }
//		  		  else {
//		  			 var sSQL2 = "  select cvalidate from lbcont where contno = '"+ rs[0][0]+"' union select cvalidate from lccont where contno = '"+ rs[0][0]+"' ";
//		  			try{BContGrid.setRowColData(i,27,easyExecSql(sSQL2)[0][0]); }catch(ex){}		
//		  		  }	  
		  		 var soldSQL = "  select cvalidate from lbcont where (contno = '"+ rs[0][0]+"' or contno= '"+ rs[0][1]+"') union select cvalidate from lccont where (contno = '"+ rs[0][0]+"' or contno= '"+ rs[0][1]+"') order by cvalidate  ";
	  				try{BContGrid.setRowColData(i,27,easyExecSql(soldSQL)[0][0]); }catch(ex){}	
	  	
	  		BContGrid.setRowColData(i,28,"是"); 
	  		
	  		  var lcmult =" select mult from lcpol where contno ='"+   GrpPolGrid.getRowColData(i, 1) +"'  and riskcode =(select CodeName from ldcode where codetype='zhquerymult' and exists(select riskwrapcode from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   BContGrid.getRowColData(i, 1) +"' ))";
	  		  var lcpolmult = easyExecSql(lcmult);
	  		  
			  var sxqsbSQL = "   select codealias, OTHERSIGN from ldcode where codetype = 'zhqueryyb'  and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code   and  contno ='"+   BContGrid.getRowColData(i, 1) +"')";
		    	var sebaosql = easyExecSql(sxqsbSQL);
	  	       if(sebaosql){
	  	    	   if(sebaosql[0][0]=="N"){
	  	    		BContGrid.setRowColData(i,29,"无"); 
	  	    	   }
	  	    	   if(sebaosql[0][0]=="Y"){
	  	    		BContGrid.setRowColData(i,29,"有");    
	  	    	   }
	  	    	   
	  	    	   if(sebaosql[0][1]=="Nowait"){
	  	    		 BContGrid.setRowColData(i,28,"无等待期");  
		  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ BContGrid.getRowColData(i, 1)+"' ";
			  			 try{ BContGrid.setRowColData(i,27,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
			  	    }
		  	  
	  	       }
  		  	   if(lcpolmult){
	  		  		var sxqmultSQL = "  select ldcode.codealias ,ldcode.OTHERSIGN from ldcode where codetype in ('zhqueryyby','zhqueryybn') and  ldcode.comcode ='"+lcpolmult[0][0]+"' and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   BContGrid.getRowColData(i, 1) +"')";
		  		    var sxqyibaosql = easyExecSql(sxqmultSQL);
		  		    if (sxqyibaosql){
				 if(sxqyibaosql[0][0]=="NO"){
						BContGrid.setRowColData(i,29,"无"); 
			  	      }
			  	     if(sxqyibaosql[0][0]=="YES"){
			  	  		BContGrid.setRowColData(i,29,"有");     
			  	     }
			  	   if(sxqyibaosql[0][1]=="Nowait"){
		  	    		 BContGrid.setRowColData(i,28,"无等待期");  
			  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ BContGrid.getRowColData(i, 1)+"' ";
				  			 try{ BContGrid.setRowColData(i,27,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
				  	    }
		  		    }
		    }

	  		  isrnew = true;
	  	  }
	  	  else {//不是续保 可能是续期

	  		  var sxqSQL = "  select cvalidate ,mult from lccont where contno ='"+   BContGrid.getRowColData(i, 1) +"'"; 
	  		  var xqrs = easyExecSql(sxqSQL); 
	 
	  		  if(xqrs){
	  			   isrnew = false;
	  			 try{ BContGrid.setRowColData(i,27,xqrs[0][0]); }catch(ex){}
	  			BContGrid.setRowColData(i,28,"否"); 
		  		  var lcmult =" select mult from lcpol where contno ='"+   GrpPolGrid.getRowColData(i, 1) +"'  and riskcode =(select CodeName from ldcode where codetype='zhquerymult' and exists(select riskwrapcode from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   BContGrid.getRowColData(i, 1) +"' ))";
		  		  var lcpolmult = easyExecSql(lcmult);
		  		  
	  			  var sxqsbSQL = "  select codealias ,OTHERSIGN from ldcode where codetype in( 'zhqueryyb','zhqueryyby','zhqueryybn')  and ldcode.comcode ='"+easyExecSql(sdsSQL)[0][1]+"' and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   BContGrid.getRowColData(i, 1) +"')";
	  		    	var sebaosql = easyExecSql(sxqsbSQL);
	  	  	       if(sebaosql){
	  	  	    	   if(sebaosql[0][0]=="N"){
	  	  	    		BContGrid.setRowColData(i,29,"无"); 
	  	  	    	   }
	  	  	    	   if(sebaosql[0][0]=="Y"){
	  	  	    		BContGrid.setRowColData(i,29,"有");    
	  	  	    	   }
	  	  		         if(sebaosql[0][1]=="Nowait"){
		  	    		 BContGrid.setRowColData(i,28,"无等待期");  
			  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ BContGrid.getRowColData(i, 1)+"' ";
				  			 try{ BContGrid.setRowColData(i,27,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
				  	    	   }
			  	  
	  	  	       }
	  	  	       if (lcpolmult){
		  		  		var sxqmultSQL = "  select ldcode.codealias ,ldcode.OTHERSIGN from ldcode where codetype in ('zhqueryyby','zhqueryybn') and  ldcode.comcode ='"+lcpolmult[0][0]+"' and  exists (select 1 from lcriskdutywrap where riskwrapcode = ldcode.code and  contno ='"+   BContGrid.getRowColData(i, 1) +"')";
			  		    var sxqyibaosql = easyExecSql(sxqmultSQL);
			  		    if (sxqyibaosql){
					 if(sxqyibaosql[0][0]=="NO"){
							BContGrid.setRowColData(i,29,"无"); 
				  	      }
				  	     if(sxqyibaosql[0][0]=="YES"){
				  	  		BContGrid.setRowColData(i,29,"有");     
				  	     }
				  	     
				         if(sxqyibaosql[0][1]=="Nowait"){
			  	    		 BContGrid.setRowColData(i,28,"无等待期");  
				  	    		 var sSQLt1 = "  select cvalidate from lccont where contno = '"+ BContGrid.getRowColData(i, 1)+"' ";
					  			 try{ BContGrid.setRowColData(i,27,easyExecSql(sSQLt1)[0][0]);  }catch(ex){}
					  	    }
			  		    }
			    }

	  	        }
	  		  }  	       
	  	  }
	    		 }
	    	}
	    }
}
/*********************************************************************
 *  通过个单客户查询无效保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setPolValue1()
{
	initBContGrid();
  var customerNo = PersonGrid.getRowColData(PersonGrid.getSelNo() - 1, 1);        
  
  //个人做为投保人
	var strSQL= "select a.ContNo,a.PrtNo,'个单', "
	    + "   a.AppntName,a.InsuredName,a.CValiDate, "
	    + "   (select sum(SumActuPayMoney) from LJAPayPerson where ContNo = a.ContNo and PayType = 'ZC'), "
	    + "   codeName('stateflag', StateFlag), "
      + "   case when a.StateFlag = '3' then a.CInvaliDate else a.PayToDate end,"
      + "   case when a.StateFlag = '3' then codeName('stateflag', a.Stateflag) else codeName('stateflag', a.Stateflag) end, '', "
      + "   ShowManageName(a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only), '1', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.Phone from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.PostalAddress from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno )  "
      + "from LCCont a "
      + "where AppFlag='1' "
      + "   and ContType = '1' "
      + "   and a.StateFlag in('2', '3') "
      + "   and AppntNo='" + customerNo + "' "
      
      //个人作为个单被保人
      + "union "      
      + "select a.ContNo,a.PrtNo,'个单', "
	    + "   a.AppntName,a.InsuredName,a.CValiDate, "
	    + "   (select sum(SumActuPayMoney) from LJAPayPerson where ContNo = a.ContNo and PayType = 'ZC'), "
	    + "   codeName('stateflag', StateFlag), "
      + "   case when a.StateFlag = '3' then a.CInvaliDate else a.PayToDate end,"
      + "   case when a.StateFlag = '3' then codeName('stateflag', a.Stateflag) else codeName('stateflag', a.Stateflag) end, '', "
      + "   ShowManageName(a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only), '1', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.Phone from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.PostalAddress from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno )  "
      + "from LCCont a, LCInsured b "
      + "where a.AppFlag = '1'  "
      + "   and ContType = '1' "
      + "   and a.ContNo = b.ContNo "
      + "   and a.StateFlag in('2', '3') "
      + "   and b.InsuredNo = '" + customerNo + "' "
      
      //个人作为个单被保人(多被保险人)
      + " union all "      
      + " select a.ContNo,a.PrtNo,'个单', "
	  + " a.AppntName,a.InsuredName,a.CValiDate, "
	  + " (select sum(SumActuPayMoney) from LJAPayPerson where ContNo = a.ContNo and PayType = 'ZC'), "
	  + " codeName('stateflag', StateFlag), "
      + " case when a.StateFlag = '3' then a.CInvaliDate else a.PayToDate end,"
      + " case when a.StateFlag = '3' then codeName('stateflag', a.Stateflag) else codeName('stateflag', a.Stateflag) end, '', "
      + " ShowManageName(a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only), '1', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.Phone from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.PostalAddress from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno )  "
      + " from LCCont a, LBInsured b "
      + " where a.AppFlag = '1'  "
      + " and ContType = '1' "
      + " and a.ContNo = b.ContNo "
      + " and a.StateFlag in('2', '3') "
      + " and b.InsuredNo = '" + customerNo + "' "
      
      //个人作为作为团单被保人
      + " union all "  //这里用union all比用union效率高      
      + " select a.GrpContNo, a.PrtNo, '团单', "
      + "   a.GrpName, '', a.CValiDate, "
      + "   (select sum(SumActuPayMoney) from LJAPayGrp where GrpContNo = a.GrpContNo and PayType = 'ZC'), "
      + "   codeName('stateflag', a.Stateflag), "
      + "   case when a.StateFlag = '3' then a.CInvaliDate else (select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo) end,"
      + "   case when a.StateFlag = '3' then codeName('stateflag', a.Stateflag) else codeName('stateflag', a.Stateflag) end, '', "
      + "   (select Name from LDCom where ComCode = a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only), '2', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,(select codename from ldcode where 1 = 1 and codetype = 'markettype' and code=a.MarketType),(select c.Phone1 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ),(select c.Phone2 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )  "
      + "from LCGrpCont a, LCInsured b "
      + "where a.GrpContNo = b.GrpContNo "
      + "   and a.StateFlag in('2', '3') "
      + "   and a.AppFlag = '1' "
      + "   and b.InsuredNo = '" + customerNo + "' " 
      
        //个人作为作为团单被保人（多被保险人）
      + " union all "  //这里用union all比用union效率高      
      + " select a.GrpContNo, a.PrtNo, '团单', "
      + " a.GrpName, '', a.CValiDate, "
      + " (select sum(SumActuPayMoney) from LJAPayGrp where GrpContNo = a.GrpContNo and PayType = 'ZC'), "
      + " codeName('stateflag', a.Stateflag), "
      + " case when a.StateFlag = '3' then a.CInvaliDate else (select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo) end,"
      + " case when a.StateFlag = '3' then codeName('stateflag', a.Stateflag) else codeName('stateflag', a.Stateflag) end, '', "
      + " (select Name from LDCom where ComCode = a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only), '2', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,(select codename from ldcode where 1 = 1 and codetype = 'markettype' and code=a.MarketType),(select c.Phone1 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ),(select c.Phone2 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )  "
      + " from LCGrpCont a, LBInsured b "
      + " where a.GrpContNo = b.GrpContNo "
      + " and a.StateFlag in('2', '3') "
      + " and a.AppFlag = '1' "
      + " and b.InsuredNo = '" + customerNo + "' "  
  
	//查询保单信息
	    + "union "
	    + "select a.ContNo,a.PrtNo,'个单', "
	    + "   a.AppntName,a.InsuredName,a.CValiDate, "
	    + "   (select sum(SumActuPayMoney) from LJAPayPerson where ContNo = a.ContNo and PayType = 'ZC'), "
	    + "   codeName('stateflag', StateFlag),b.EdorValiDate,"
	    //#2051 合同终止退费--允许修改保费的功能
      + " (case (select 1 from llcontdeal where edorno=a.edorno) when 1 then (select case edortype when 'CT' then '解约' when 'HZ' then '合同终止' when 'CD' then '合同终止退费' when 'RB' then '解约回退' end from llcontdeal where edorno=a.edorno) "
      + " else (select min(EdorName) from LMEdorItem where EdorCode = b.EdorType) end) , "
      + "   CodeName('reason_tb', b.ReasonCode), "
      + "   (select Name from LDCom where ComCode = a.ManageCom), (select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only),'1', a.ManageCom , 'B'"
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lbappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ) ,(select c.Phone from lcaddress c,lbappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ) ,(select c.PostalAddress from lcaddress c,lbappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno )  "
      + "from LBCont a, LPEdorItem b "
      + "where a.EdorNo = b.EdorNo "
      + "and a.ContType = '1' "
      + "and a.AppntNo = '" + customerNo + "' "
   //   + "       or exists (select ContNo from LBInsured where ContNo = a.ContNo and ContType = '1' "
   //   + "                     and  InsuredNo = '" +  customerNo +"')) " 
   // modify by fuxin 2008-6-11 原有sql用or效率比较低下，现在将条件差分开然后用 union 
   	  + "union "
	    + "select a.ContNo,a.PrtNo,'个单', "
	    + "   a.AppntName,a.InsuredName,a.CValiDate, "
	    + "   (select sum(SumActuPayMoney) from LJAPayPerson where ContNo = a.ContNo and PayType = 'ZC'), "
	  	    + "   codeName('stateflag', StateFlag),b.EdorValiDate,"
	  //#2051 合同终止退费--允许修改保费的功能
      + " (case (select 1 from llcontdeal where edorno=a.edorno) when 1 then (select case edortype when 'CT' then '解约' when 'HZ' then '合同终止' when 'CD' then '合同终止退费' when 'RB' then '解约回退' end from llcontdeal where edorno=a.edorno) "
      + " else (select min(EdorName) from LMEdorItem where EdorCode = b.EdorType) end) , "
      + "   CodeName('reason_tb', b.ReasonCode), "
      + "   (select Name from LDCom where ComCode = a.ManageCom), (select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only),'1', a.ManageCom , 'B'"
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lbappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ) ,(select c.Phone from lcaddress c,lbappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ) ,(select c.PostalAddress from lcaddress c,lbappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno )  "
      + "from LBCont a, LPEdorItem b "
      + "where a.EdorNo = b.EdorNo "
      + "and a.ContType = '1' " 
      +" and exists(select ContNo  from LBInsured where ContNo = a.ContNo and ContType = '1' and InsuredNo = '" + customerNo + "') "
      
      //个人作为团单被保人且已退保
      + " union all "                                                                                                                                         
      + " select a.GrpContNo, a.PrtNo, '团单', "
      + "   a.GrpName, '', a.CValiDate, "
      + "   (select sum(SumActuPayMoney) from LJAPayGrp where GrpContNo = a.GrpContNo and PayType = 'ZC'), "
      + "   codeName('stateflag', a.Stateflag), "
      + "   b.EdorValiDate, (select min(EdorName) from LMEdorItem where EdorCode = b.EdorType), "
      + "   CodeName('reason_tb', b.ReasonCode), "
      + "   (select Name from LDCom where ComCode = a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only), '2', a.ManageCom, 'B' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Phone1  from LCGrpAddress c,LBGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.Phone2  from LCGrpAddress c,LBGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LBGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )  "
      + "from LBGrpCont a, LPGrpEdorItem b, LBInsured c "                                                                                                                       
      + "where a.EdorNo = b.EdorNo "
      + "and a.GrpContNo = c.GrpContNo "
      + "and c.InsuredNo = '" + customerNo + "'";  
      
    turnPage4.pageDivName = "divPage4";
    turnPage4.queryModal(strSQL, BContGrid);
    fm.InvaliContSql.value = strSQL;
    document.all("divPage4").style.display="";
    document.all("divPage3").style.display="";
    
    gnext();
}	
  
/*********************************************************************
 *  通过团单客户查询有效保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setGrpPolValue()
{ 
	initGrpPolGrid();
	initBContGrid();
	
	clearContQueryBox();
	
  var customerNo = GrpPersonGrid.getRowColData(GrpPersonGrid.getSelNo() - 1, 1);
  //查询保单信息
  var strSQL = " select a.GrpContNo, a.PrtNo, '团单', "
      + "   case when isNoNameCont(a.GrpContNo) = 'W' then '团险无名单' else '团险普通保单' end, "
      + "   a.GrpName, '', a.CValiDate, "
      + "   case when (select count(1) from lcgrppol l where l.grpcontno=a.grpcontno and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')) > 0 then '每个被保险人老年护理保险金领取起始日止' else varchar(a.CInvaliDate) end,  case when (select count(1) from lcgrppol l where l.grpcontno=a.grpcontno and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')) > 0 then '每个被保险人老年护理保险金领取起始日止' else varchar((select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo and (StateFlag is null or StateFlag != '3'))) end, "
      + "   a.Prem, codeName('stateflag', a.Stateflag), "
      + "   (select Name from LDCom where ComCode = a.ManageCom), (select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only),"
      + "   (select Name from LAAgent where AgentCode = a.AgentCode), '2', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,(select codename from ldcode where 1 = 1 and codetype = 'markettype' and code=a.MarketType),(select c.Phone1 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ),(select c.Phone2 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )  "
  		+ " from LCGrpCont a"
  		+ " where a.AppFlag = '1' "
  		+ "   and (StateFlag is null or StateFlag in('1')) "
      + "   and a.AppntNo = '" + customerNo + "' ";	  
  fm.ContSql.value = strSQL;
  turnPage3.pageDivName = "divPage3";
  turnPage3.queryModal(strSQL, GrpPolGrid);
  
    document.all("divPage4").style.display="";
    document.all("divPage3").style.display="";
    
  setGrpPolValue1();
  
}
/*********************************************************************
 *  通过团单客户查询其他保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setGrpPolValue1()
{	
	initBContGrid();
	
  var customerNo = GrpPersonGrid.getRowColData(GrpPersonGrid.getSelNo() - 1, 1);
  //查询保单信息
  var strSQL = " select a.GrpContNo, a.PrtNo, '团单', "
      + "   a.GrpName, '', a.CValiDate, "
      + "   (select sum(SumActuPayMoney) from LJAPayGrp where GrpContNo = a.GrpContNo and PayType = 'ZC'), "
      + "   codeName('stateflag', a.Stateflag), "
      + "   case when a.StateFlag = '3' then a.CInvaliDate else (select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo) end,"
      + "   case when a.StateFlag = '3' then codeName('stateflag', a.Stateflag) else codeName('stateflag', a.Stateflag) end, '', "
      + "   (select Name from LDCom where ComCode = a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only), '2', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,(select codename from ldcode where 1 = 1 and codetype = 'markettype' and code=a.MarketType),(select c.Phone1 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ),(select c.Phone2 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )  "
  		+ " from LCGrpCont a"
  		+ " where a.AppFlag = '1' "
  		+ "   and StateFlag in('2', '3') "
      + "   and a.AppntNo = '" + customerNo + "' "
      
      + "union "      
      + " select a.GrpContNo, a.PrtNo, '团单', "
      + "   a.GrpName, '', a.CValiDate, "
      + "   (select sum(SumActuPayMoney) from LJAPayGrp where GrpContNo = a.GrpContNo and PayType = 'ZC'), "
      + "   codeName('stateflag', a.Stateflag), "
      + "   b.EdorValiDate, (select min(EdorName) from LMEdorItem where EdorCode = b.EdorType), "
      + "   CodeName('reason_tb', b.ReasonCode), "
      + "   (select Name from LDCom where ComCode = a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only), '2', a.ManageCom, 'B' "
      + " ,CodeName('lcsalechnl', a.SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,'',(select c.Phone1  from LCGrpAddress c,LBGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.Phone2  from LCGrpAddress c,LBGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LBGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )    "
  		+ " from LBGrpCont a, LPGrpEdorItem b "
  		+ " where a.EdorNo = b.EdorNo "
  		+ "   and a.GrpContNo = b.GrpContNo "
  		+ "   and (a.AppFlag = '1' or a.AppFlag = '3') "  //AppFlag = '3'犹豫期退保
      + "   and a.AppntNo = '" + customerNo + "' ";
  fm.InvaliContSql.value = strSQL;
  turnPage4.pageDivName = "divPage4";
  turnPage4.queryModal(strSQL, BContGrid);
  
  document.all("divPage4").style.display="";
  document.all("divPage3").style.display="";

}
/*********************************************************************
 *  通过选择有效保单信息设置保单号
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setContValue()
{
  mContNo = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo() - 1, 1);
 	tContType = GrpPolGrid.getRowColDataByName(GrpPolGrid.getSelNo() - 1, "ContType");
  fm.ContNoSelected.value = mContNo;
   
  tSel = GrpPolGrid.getSelNo();	//所选的有效保单行
  tSel1 = -1; //所选的无效保单行
}
/*********************************************************************
 *  通过选择其他保单信息设置保单号
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setContValue1()
{
  mContNo = BContGrid.getRowColData(BContGrid.getSelNo() - 1, 1);  
  tContType = BContGrid.getRowColDataByName(BContGrid.getSelNo() - 1, "ContType");
  fm.ContNoSelected.value = mContNo;
   
  tSel = -1;	//所选的有效保单行
  tSel1 = BContGrid.getSelNo(); //所选的无效保单行
}

/*********************************************************************
 *  查询有效保单客户属性信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setCosProrperty(customerNo)
{
	for (i = 0; i < GrpPolGrid.mulLineCount; i++)
	{
	  var prorperty = "";
	  var contNo = GrpPolGrid.getRowColData(i, 1);
	  var flag = GrpPolGrid.getRowColData(i, 4);
	  var contType = GrpPolGrid.getRowColData(i, 14);
	  var appntNo = GrpPolGrid.getRowColData(i, 2);
	  var insuredNo =  GrpPolGrid.getRowColData(i, 3);
	  if (flag == '团单')
	  {
	    if (customerNo == appntNo)
	    {
	      prorperty = "投保人";
	    }
	    else
	    {
	      prorperty = "被保人";
	    }
	  }
	  else if (flag == '个单')
	  {
	    if ((customerNo == appntNo) && (customerNo == insuredNo))
	    {
	      prorperty = "投保人/被保人";
	    }
	    else if (customerNo == appntNo)
	    {
	      prorperty = "投保人";
	    }
	    else
	    {
	      prorperty = "被保人";
	    }
	  }
	  GrpPolGrid.setRowColData(i, 5, prorperty);
	}
}

/*********************************************************************
 *  查询保单明细
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function contDetail()
{
	//判断是否输入保单号，以两种方式查询投保信息
	//if (fm.all('ContNo').value == ''||fm.all('ContNo').value == null)
	//{
	  fromListReturn();
	//}
	//else
	//{
	//  fromBoxReturn();
	//}
}

/*********************************************************************
 *  投保信息列表中选择一条，查看投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function fromListReturn()
{
	//从客户投保信息列表中选择一条，查看投保信息
	//var tSel = GrpPolGrid.getSelNo();	
	//var tSel1 = BContGrid.getSelNo();
	if(fm.ContNoSelected.value == "")
	{
	  alert("请选择保单");
	  return false;
	}
	//失效的数据
  if(tSel1 > 0)
  {
    var cContNo = BContGrid.getRowColDataByName(tSel1 - 1, "ContNo");
    var tContType = BContGrid.getRowColDataByName(tSel1 - 1, "ContType");
  	var cPrtNo = BContGrid.getRowColDataByName(tSel1 - 1, "PrtNo");
  	var tTableType = BContGrid.getRowColDataByName(tSel1 - 1, "TableType");
  	var cManageCom = BContGrid.getRowColDataByName(tSel1 - 1, "ManageCom");
  	
  	if(tTableType == "C")
  	{
  	  tTableType = '1';
  	}
  	else
  	{
  	  tTableType = '2';
  	}
  	
    if (IsValiCom(cManageCom))
    {
			if (tContType == '2')
			{
			  var strSql = "SELECT 1 from lcgrppol l where prtno='"+cPrtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode) union SELECT 1 from lbgrppol l where prtno='"+cPrtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
	          var arrResult = easyExecSql(strSql);
	          var strSql1 = "SELECT 1 from lcgrppol l where prtno='"+cPrtNo+"'and riskcode='370301' union SELECT 1 from lbgrppol l where prtno='"+cPrtNo+"'and riskcode='370301'";
	          var arrResult1 = easyExecSql(strSql1);
	          if (arrResult == null||arrResult1=="1") {
    	var str = "SELECT 1 from lwmission where missionprop1='"+cPrtNo+"' and activityid  in ('0000002098','0000002099') and  missionprop5='1'";
	   	var arr = easyExecSql(str);
	   	if (arr == null) {  
        	 window.open("./GrpPolDetailQueryMain.jsp?ContNo="+ cContNo+"&ContType="+tTableType);
       	}else{
   			easyScanWin = window.open("./GrpPolULIDetailQueryMain.jsp?LoadFlag=16&polNo="+cContNo+"&prtNo="+cPrtNo+"&ContType="+tTableType, "", "status=no,resizable=yes,scrollbars=yes ");    
        }
	}else{
    	easyScanWin = window.open("./GrpPolULIDetailQueryMain.jsp?LoadFlag=16&polNo="+cContNo+"&prtNo="+cPrtNo+"&ContType="+tTableType, "", "status=no,resizable=yes,scrollbars=yes ");    
   	}
		}
  		
  		
  		else if (tContType == '1')
  		{
  			window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType="+tTableType);	
  		}
  	} 
    else
    {
      alert("所处机构权限不能察看该保单！");
    }
  }

	else
	{ 
	  var cContNo = GrpPolGrid.getRowColDataByName(tSel - 1, "ContNo");
	  var tContType = GrpPolGrid.getRowColDataByName(tSel - 1, "ContType");
		var cPrtNo = GrpPolGrid.getRowColDataByName(tSel - 1, "PrtNo");
		var tTableType = GrpPolGrid.getRowColDataByName(tSel - 1, "TableType");
		var cManageCom = GrpPolGrid.getRowColDataByName(tSel - 1, "ManageCom");

		
  	if(tTableType == "C")
  	{
  	  tTableType = '1';
  	}
  	else
  	{
  	  tTableType = '2';
  	}
  	
	  if (IsValiCom(cManageCom))
	  {

			if (tContType == '2')
			{
			  var strSql = "SELECT 1 from lcgrppol l where prtno='"+cPrtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode) union SELECT 1 from lbgrppol l where prtno='"+cPrtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
	          var arrResult = easyExecSql(strSql);
	          var strSql1 = "SELECT 1 from lcgrppol l where prtno='"+cPrtNo+"'and riskcode='370301' union SELECT 1 from lbgrppol l where prtno='"+cPrtNo+"'and riskcode='370301'";
	          var arrResult1 = easyExecSql(strSql1);
	          if (arrResult == null||arrResult1=="1") {
    	var str = "SELECT 1 from lwmission where missionprop1='"+cPrtNo+"' and activityid  in ('0000002098','0000002099') and  missionprop5='1'";
	   	var arr = easyExecSql(str);
	   	if (arr == null) {  
        	 window.open("./GrpPolDetailQueryMain.jsp?ContNo="+ cContNo+"&ContType="+tTableType);
       	}else{
   			easyScanWin = window.open("./GrpPolULIDetailQueryMain.jsp?LoadFlag=16&polNo="+cContNo+"&prtNo="+cPrtNo+"&ContType="+tTableType, "", "status=no,resizable=yes,scrollbars=yes ");    
        }
	}else{
    	easyScanWin = window.open("./GrpPolULIDetailQueryMain.jsp?LoadFlag=16&polNo="+cContNo+"&prtNo="+cPrtNo+"&ContType="+tTableType, "", "status=no,resizable=yes,scrollbars=yes ");    
   	}
		}
			else if (tContType == '1')
			{
				window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType="+tTableType);	
			}
		} 
	  else
	  {
	    alert("所处机构权限不能察看该保单！");
	  }
	}
}

/*********************************************************************
 *  读取输入的保单号码，查看投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
//function fromBoxReturn()
//{
//	var tContNo = trim(fm.all('ContNo').value);
//  //查询个单记录
//  var arrReturn = new Array();
//  var strSql = "select count(ContNo),managecom from LCCont where ContNo='" + tContNo + "' group by managecom";
//  arrReturn = easyExecSql(strSql);
//  //查询团单记录
//  var strSql1 = "select count(GrpContNo),managecom from LCGrpCont where GrpContNo='" + tContNo + "' group by managecom";
//  var arrReturn1 = new Array();
//	arrReturn1 = easyExecSql(strSql1);
//	if (arrReturn == null && arrReturn1 ==null ) 
//	{
//	    showBInfo();
//	} 
//	else if (arrReturn1 != null)
//	{	
//		if (IsValiCom(arrReturn1[0][1]))
//		{
//		    window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ tContNo +"&ContType=1");
//		}
//	   else
//	   {
//	        alert("所处机构权限不够，不能察看该保单！");
//	   }
//	}
//	else if(arrReturn != null )
//    {
//		if (IsValiCom(arrReturn[0][1]))
//		{
//		    window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + tContNo +"&ContType=1");	
//		}
//		else
//	    {
//	        alert("所处机构权限不够，不能察看该保单！");
//	    }
//	}
//}


/*********************************************************************
 *  读取输入的保单号码，查看退保/撤保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showBInfo()
{
	var tContNo = trim(fm.all('ContNo').value);
    //查询个单记录(撤保或退保)
	var arrReturn2 = new Array();
    var strSql = "select count(ContNo),managecom from LBCont where ContNo='" + tContNo + "' group by managecom";
    arrReturn2 = easyExecSql(strSql);

	//查询团单记录(撤保或退保)
    var strSql1 = "select count(GrpContNo),managecom from LBGrpCont where GrpContNo='" + tContNo + "' group by managecom";
    var arrReturn3 = new Array();
	arrReturn3 = easyExecSql(strSql1);
	if (arrReturn2 == null && arrReturn3 ==null) {
	    alert("没有该合同号，请重新输入");
	} else if(arrReturn3 != null ){	
	  if (IsValiCom(arrReturn3[0][1]))
		{
		  window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ tContNo +"&ContType=2");
		}
	  else
	  {
	    alert("所处机构权限不够，不能察看该保单！");
	  }
	}else if(arrReturn2 != null ){	
	  if (IsValiCom(arrReturn2[0][1]))
		{
		  window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + tContNo +"&ContType=2");
	  }
		else
	  {
			alert("所处机构权限不够，不能察看该保单！");
	  }	
	}
}

/*********************************************************************
 *  保单被保人清单查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function insuredQueryClick()
{
  var tContNo = mContNo;
  if (tContNo == "")
  {
    tContNo= trim(fm.all('ContNo').value);
	}
	if (tContNo == "")
	{
	  alert("请选择一个保单！");
	  return false;
	}
	//个单不能查看被保人清单
	
	if (tContType == 1)
	{
	  alert("个单没有被保人清单！");
	  return false;
	}

  var strSql = "select count(GrpContNo) from LCGrpCont where GrpContNo='" + tContNo + "'";
	var arrReturn = new Array();
	arrReturn = easyExecSql(strSql);

	//查询团单记录(撤保或退保)
  var strSql1 = "select count(GrpContNo) from LBGrpCont where GrpContNo='" + tContNo + "'";
	var arrReturn1 = new Array();
	arrReturn1 = easyExecSql(strSql1);	

	var openWindow;
	var strSql = "SELECT 1 from lcgrppol l where grpcontno='"+tContNo+"' and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode) union SELECT 1 from lbgrppol l where grpcontno='"+tContNo+"' and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
	var arrResult = easyExecSql(strSql);
	if (arrResult == null) {  
		//wdx注释了原来代码
//		原本的需要跳转的jsp
//		openWindow = "../sys/GrpInsuredInput.jsp";
		//wdx新增了代码
//		现在需要跳转到XXXmain.jsp然后在跳转到XXXinput.jsp
		openWindow = "../sys/GrpInsuredInputMain.jsp";
	}else{
	   	openWindow = "../sys/GrpUliInsuredInput.jsp"; 
	}

  if (arrReturn == 0 && arrReturn1 ==0 ) {
	   alert("找不到合同信息！");
	} else if(arrReturn != 0 ){	
		window.open(openWindow + "?ContNo=" + tContNo + "&ModeFlag=0&ContLoadFlag=2" +"&ContType=1");
	}else if(arrReturn1 != 0 ){	
		 window.open(openWindow + "?ContNo=" + tContNo + "&ModeFlag=0&ContLoadFlag=2" +"&ContType=2");
	}							
}


/*********************************************************************
 *  保单明细缴费查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function payQueryClick()
{
  var tContNo = mContNo;
  if (tContNo == "")
  {
    tContNo= trim(fm.all('ContNo').value);
	}
	if (tContNo == "")
	{
	  alert("请选择一个保单！");
	  return false;
	}
	
  //查询团单记录(撤保或退保)
  var strSql = "select count(GrpContNo) from LCGrpCont where GrpContNo='" + tContNo + "'";
  var arrReturn = new Array();
  arrReturn = easyExecSql(strSql);
  
  //查询团单记录(撤保或退保)
  var strSql1 = "select count(GrpContNo) from LBGrpCont where GrpContNo='" + tContNo + "'";
  var arrReturn1 = new Array();
  arrReturn1 = easyExecSql(strSql1);
  
  if (arrReturn == 0 && arrReturn1 ==0 ) {
  
      var arrReturn2 = new Array();
  var strSql2 = "select count(ContNo) from LCCont where ContNo='" + tContNo + "'";
  arrReturn2 = easyExecSql(strSql2);
  
        //查询团单记录(撤保或退保)
  var strSql3 = "select count(GrpContNo) from LBCont where ContNo='" + tContNo + "'";
  var arrReturn3 = new Array();
  arrReturn3 = easyExecSql(strSql3);
        
  //ContLoadFlag: 1 个单; 2 团单
  //ContType: 1 签单; 2 撤保/退保
  if (arrReturn2 == 0 && arrReturn3 ==0 ) {
  	alert("没有该合同号，请重新输入");
  } else if(arrReturn3 != 0 ){	
  	
  	window.open("../sys/LCIndiPayQuery.jsp?ContNo=" +  tContNo + "&ContLoadFlag=1"+"&ContType=2");
  }else if(arrReturn2 != 0 ){	
  	window.open("../sys/LCIndiPayQuery.jsp?ContNo=" +  tContNo + "&ContLoadFlag=1"+"&ContType=1");	
  }
  
  } else if(arrReturn != 0 ){	
  window.open("../sys/LCPayQuery.jsp?ContNo=" +  tContNo + "&ContLoadFlag=2" +"&ContType=1");
  }else if(arrReturn1 != 0 ){	
  window.open("../sys/LCPayQuery.jsp?ContNo=" +  tContNo + "&ContLoadFlag=2"+"&ContType=2" );
  }
}


/*********************************************************************
 *  投保书查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ScanQuery()
{
	var arrReturn = new Array();
	var arrReturn1 = new Array();
	var tBussNoType = "";
	var tBussType = "";
    var tSubType = "";
	//获的个人保单印刷号
    
  var tContNo = mContNo;
  if (tContNo == "")
  {
    tContNo= trim(fm.all('ContNo').value);
	}
	if (tContNo == "")
	{
	  alert("请选择一个保单！");
	  return false;
	}
		 
  //查询团单记录(撤保或退保)
    var strSql = "select PrtNo from LCGrpCont where GrpContNo='" + tContNo + "'";
  var arrReturn = new Array();
  arrReturn = easyExecSql(strSql);
  
  //查询团单记录(撤保或退保)
    var strSql1 = "select PrtNo from LBGrpCont where GrpContNo='" + tContNo + "'";
    var arrReturn1 = new Array();
    arrReturn1 = easyExecSql(strSql1);
  
  if (arrReturn == null && arrReturn1 ==null ) {
  
    var arrReturn2 = new Array();
  	var strSql2 = "select PrtNo from LCCont where ContNo='" + tContNo + "'";
  	arrReturn2 = easyExecSql(strSql2);
  
          //查询团单记录(撤保或退保)
  	var strSql3 = "select PrtNo from LBCont where ContNo='" + tContNo + "'";
  	var arrReturn3 = new Array();
  	arrReturn3 = easyExecSql(strSql3);
          
  	//ContLoadFlag: 1 个单; 2 团单
  	//ContType: 1 签单; 2 撤保/退保
  	if (arrReturn2 == null && arrReturn3 ==null ) {
  		alert("没有该合同号，请重新输入");
  	} 
  	var strPrtNo = "";
  	if(arrReturn2== null && arrReturn3 !=null){
  	    strPrtNo = arrReturn3[0][0];
  	}
  	if(arrReturn2 != null&& arrReturn3 ==null){
  	    strPrtNo = arrReturn2[0][0];
  	}
  	var strSQL = "select a.docid,a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
					+ " a.maketime,a.modifydate,a.modifytime,a.docid "
					+ " from es_doc_main a ,es_doc_def b where 1=1 "
					+ " and a.subtype = b.subtype "
				  + " and a.doccode like '"+strPrtNo+"%%'"
				  + " and a.busstype='TB'"
				  + " and b.busstype='TB'";	
	var arrReturn4 = new Array();
  	arrReturn4 = easyExecSql(strSQL);
  	if(arrReturn4 != null)
  	{   
  	    var cDocID = arrReturn4[0][0];
  	    var cDocCode = arrReturn4[0][1];
  	    var	cBussTpye = "TB" ;
  	    var cSubTpye = arrReturn4[0][2];
  	    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
  	}else{
  	   alert("该印刷号没有投保书影像件！");
  	}	
  }else{
    var strPrtNo1 = "";
  	if(arrReturn== null && arrReturn1 !=null){
  	    strPrtNo1 = arrReturn1[0][0];
  	}
  	if(arrReturn != null&& arrReturn1 ==null){
  	    strPrtNo1 = arrReturn[0][0];
  	}
  	var strSQL1 = "select a.docid,a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
					+ " a.maketime,a.modifydate,a.modifytime,a.docid "
					+ " from es_doc_main a ,es_doc_def b where 1=1 "
					+ " and a.subtype = b.subtype "
				  + " and a.doccode like '"+strPrtNo1+"%%'"
				  + " and a.busstype='TB'"
				  + " and b.busstype='TB'";	
	var arrReturn5 = new Array();
  	arrReturn5 = easyExecSql(strSQL1);
  	if(arrReturn5 != null)
  	{   
  	    var cDocID = arrReturn5[0][0];
  	    var cDocCode = arrReturn5[0][1];
  	    var	cBussTpye = "TB" ;
  	    var cSubTpye = arrReturn5[0][2];
  	    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
  	}else{
  	   alert("该印刷号没有投保书影像件！");
  	}
  }			  
}

/*********************************************************************
 *  客户服务信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function edorInfo()
{
	if (fm.ContNoSelected.value == "")
	{
	  alert("请选择一个保单！");
	  return false;
	}
  var strSql = "select AppntNo from LCGrpCont where GrpContNo='" + fm.ContNoSelected.value + "'"
            + " union all "
        	  + " select AppntNo from LBGrpCont where GrpContNo='" + fm.ContNoSelected.value + "'"
        	  + " UNION ALL select AppntNo from LCCont where ContNo='" + fm.ContNoSelected.value + "'"
        	  + " union all select AppntNo from LBCont where ContNo='" + fm.ContNoSelected.value + "' ";
  var arrReturn = new Array();
  arrReturn = easyExecSql(strSql);
  var tCustomer = arrReturn[0][0];
  
  win = window.open("../task/TaskCustomer.jsp?CustomerNo="+tCustomer, "CustomerSys");
  win.focus();
  
  //window.open("../sys/TaskPersonalBox.jsp?ContNo=" + tContNo+ "&Customer=" + tCustomer);	
}

//察看理赔信息
function claimInfo()
{
  alert("尚未实现");
}

/*********************************************************************
 *  清除按保单查询录入条件内容
 *  参数  ：
 *  返回值： 
 *********************************************************************
 */
function clearContQueryBox()
{
  fm.ContNo.value = "";
  fm.PrtNo.value = "";
  fm.StateFlag.value = "";
  fm.ContKind.value = "";
  fm.ManageCom.value = comCode;
  fm.AgentCode.value = "";
  fm.Group03.value = "";
  fm.RiskCode1.value = "";
  fm.RiskCode2.value = "";
  fm.ApplyDateStart.value = "";
  fm.ApplyDateEnd.value = "";
  fm.PayToDateStart.value = "";
  fm.PayToDateEnd.value = "";
}

/*********************************************************************
 *  按保单信息查询保单
 *  参数  ：
 *  返回值：
 *********************************************************************
 */
function queryCont()
{
  if(!checkQueryCont())
  {
    return false;
  }
  clearGlobalVar();
  initGrpPolGrid();
  initBContGrid();

  queryContVali();
  queryContInVali();
  
  if(GrpPolGrid.mulLineCount == 0 && BContGrid.mulLineCount == 0)
  {
    alert("没有查询到保单信息");
    return false;
  }
  next();
  gnext();
  getqspkFlag();
  return true;
}

/*********************************************************************
 *  按保单条件查询有效保单
 *********************************************************************
 */
function queryContVali()
{   
  var contKindP = "";
  var contKindG = "";
  
  if(fm.ContKind.value != "")
  {
    if(fm.ContKind.value == "11")
    {
      contKindP = " and hasLongRisk(a.ContNo) = 'O' ";
    }
    else if(fm.ContKind.value == "12")
    {
      contKindP = " and hasLongRisk(a.ContNo) = 'L' ";
    }
    else if(fm.ContKind.value == "21")
    {
      contKindG = " and isNoNameCont(a.GrpContNo) = 'W' ";
    }
    else if(fm.ContKind.value == "22")
    {
      contKindG = " and isNoNameCont(a.GrpContNo) = 'N' ";
    }
  }

  //如果录入保单号或者是投保单号就不受登陆机构限制，可以查询跨机构保单；否则，只能查询登陆机构或者是所辖机构的保单信息--sgh-2009-6-1
  if((fm.ContNo.value ==""||fm.ContNo.value ==null)&&(fm.PrtNo.value =="" || fm.PrtNo.value ==null))
  {
  	if(!IsValiCom(fm.ManageCom.value))
	{
	 alert("登陆机构不能查询界面录入机构的保单信息！");
	 return false;
	 }
  }

  //查询有效保单
  var sql = ""
  var Sql11=""
  if(fm.AgentCode.value!=null&&fm.AgentCode.value!=""){
  	Sql11=" and a.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
  }
    if(fm.ContKind.value != "21" && fm.ContKind.value != "22")
    {
      //个单未失效
      sql = sql + "select a.ContNo,a.PrtNo,'个单', "
	    + "   case when hasLongRisk(a.ContNo) = 'L' then '个险长期' else '个险一年' end, "
	    + "   AppntName,a.InsuredName,a.CValiDate, varchar(a.CInvaliDate), varchar(a.PayToDate), a.Prem, "
	    + "   codeName('stateflag', StateFlag), "
      + "   ShowManageName(a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only), "
      + "   (select Name from LAAgent where AgentCode = a.AgentCode), '1', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', SaleChnl), AgentCom, (select Name from lacom where  AgentCom=a.agentcom)," +
      		"(select contno from lppol where polno in" +
      		"(select polno from lcpol where contno=a.contno) and  edortype = 'CF' order by edorno desc " +
      		" fetch first 1 rows only ),a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.Phone from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.PostalAddress from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ) "
      + "from LCCont a "
      + "where AppFlag='1' "
      + "   and ContType = '1' "
      + "   and (StateFlag is null or StateFlag in('1'))"
	    + getWherePart( 'a.ContNo','ContNo') 
	    + getWherePart( 'a.PrtNo','PrtNo') 
	    + getWherePart( 'a.StateFlag','StateFlag')       
			// 加入渠道、网点条件
    	+ getWherePart("SaleChnl","SaleChnlCode")
      + getWherePart("AgentCom","AgentComBank")
   	  // ----------------------------------------
	    + contKindP
	    + "   and a.ManageCom like '" + fm.ManageCom.value + "%' "
		
	    + "   and exists(select 1 from LDCom where Name like '%" + fm.ManageComName.value + "%') "
	    + getWherePart( 'a.AgentGroup','Group03') 
	    + "   and exists(select 1 from LABranchGroup where Name like '%" + fm.Group03Name.value + "%') "
	    //+ getWherePart( 'getUniteCode(a.AgentCode)','AgentCode') 
	    + Sql11
	    + "   and exists(select 1 from LAAgent where Name like '%" + fm.AgentName.value + "%') "
	    + (fm.RiskCode1.value == "" ? "" : "  and exists (select 1 from LCPol where ContNo = a.ContNo and RiskCode = '" + fm.RiskCode1.value + "') ")
	    + (fm.RiskCode2.value == "" ? "" : "  and exists (select 1 from LCPol where ContNo = a.ContNo and RiskCode = '" + fm.RiskCode2.value + "') ")
	    + (fm.ApplyDateStart.value == "" ? "" : "  and a.PolApplyDate between '" + fm.ApplyDateStart.value + "' and '" + fm.ApplyDateEnd.value + "' ")
	    + (fm.PayToDateStart.value == "" ? "" : "  and a.PayToDate between '" + fm.PayToDateStart.value + "' and '" + fm.PayToDateEnd.value + "' ")
	    ;
	  }

    if(fm.ContKind.value != "11" && fm.ContKind.value != "12")
    {
      if(sql != "")
      {
        sql = sql + " union all ";  //这里用union all比用union效率高
      }
      //团单未失效
      sql = sql + " select a.GrpContNo, a.PrtNo, '团单', "
      + "   case when isNoNameCont(a.GrpContNo) = 'W' then '团险无名单' else '团险普通保单' end, "
      + "   a.GrpName, '', a.CValiDate, "
      +"  case when (select count(1) from lcgrppol l where  l.grpcontno=a.grpcontno and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')) > 0 then '每个被保险人老年护理保险金领取起始日止' else varchar(a.CInvaliDate) end,  case when (select count(1) from lcgrppol l where l.grpcontno=a.grpcontno and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')) > 0 then '每个被保险人老年护理保险金领取起始日止' else varchar((select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo and (StateFlag is null or StateFlag != '3'))) end, "
      + "   a.Prem, codeName('stateflag', a.Stateflag), "
      + "   (select Name from LDCom where ComCode = a.ManageCom), (select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only),"
      + "   (select Name from LAAgent where AgentCode = a.AgentCode), '2', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', SaleChnl), AgentCom ,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,(select codename from ldcode where 1 = 1 and codetype = 'markettype' and code=a.MarketType),(select c.Phone1 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ),(select c.Phone2 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )  "
  		+ " from LCGrpCont a"
  		+ " where a.AppFlag = '1' "
      + "   and (StateFlag is null or StateFlag in('1'))"
  		+ getWherePart( 'a.GrpContNo','ContNo') 
	    + getWherePart( 'a.PrtNo','PrtNo') 
	    + getWherePart( 'a.StateFlag','StateFlag') 
			// 加入渠道、网点条件
    	+ getWherePart("SaleChnl","SaleChnlCode")
      + getWherePart("AgentCom","AgentComBank")
      
      //add by Houyd 加入团体保单市场类型条件
      + getWherePart("MarketType","MarketType")
   	  // ----------------------------------------
	    + contKindG
	    + "   and a.ManageCom like '" + fm.ManageCom.value + "%' "
	    + "   and exists(select 1 from LDCom where Name like '%" + fm.ManageComName.value + "%') "
	    + getWherePart( 'a.AgentGroup','Group03') 
	    + "   and exists(select 1 from LABranchGroup where Name like '%" + fm.Group03Name.value + "%') "
	    //+ getWherePart( 'getUniteCode(a.AgentCode)','AgentCode') 
	    + Sql11
	    + "   and exists(select 1 from LAAgent where Name like '%" + fm.AgentName.value + "%') "
	    + (fm.RiskCode1.value == "" ? "" : "  and exists (select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode = '" + fm.RiskCode1.value + "') ")
	    + (fm.RiskCode2.value == "" ? "" : "  and exists (select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode = '" + fm.RiskCode2.value + "') ")
	    + (fm.ApplyDateStart.value == "" ? "" : "  and a.PolApplyDate between '" + fm.ApplyDateStart.value + "' and '" + fm.ApplyDateEnd.value + "' ")
	    + (fm.PayToDateStart.value == "" ? "" : "  and (select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo) between '" + fm.PayToDateStart.value + "' and '" + fm.PayToDateEnd.value + "' ")
      ;    
  }
  
  fm.ContSql.value = sql;
  turnPage3.pageLineNum = 50;
  turnPage3.pageDivName = "divPage3";
  turnPage3.queryModal(sql, GrpPolGrid)
  
    document.all("divPage4").style.display="";
    document.all("divPage3").style.display="";
}

/*********************************************************************
 *  按保单条件查询无效保单
 *********************************************************************
 */
function queryContInVali()
{
  var contKindPC = "";
  var contKindPB = "";
  var contKindGC = "";
  var contKindGB = "";
  
  if(fm.ContKind.value != "")
  {
    var tempShortC = " exists(select 1 from LCPol where ContNo = a.ContNo and RiskCode in(select RiskCode from LMRiskApp where RiskPeriod = 'L')) ";
    var tempShortB = " exists(select 1 from LBPol where ContNo = a.ContNo and RiskCode in(select RiskCode from LMRiskApp where RiskPeriod = 'L')) ";
    var tempLongC = " exists(select 1 from LCCont where GrpContNo = a.GrpContNo and PolType = '1') ";
    var tempLongB = " exists(select 1 from LBCont where GrpContNo = a.GrpContNo and PolType = '1') ";
    if(fm.ContKind.value == "11")
    {
      contKindPC = " and not " + tempShortC;
      contKindPB = " and not " + tempShortB;
    }
    else if(fm.ContKind.value == "12")
    {
      contKindPC = " and " + tempShortC;
      contKindPB = " and " + tempShortB;
    }
    else if(fm.ContKind.value == "21")
    {
      contKindGC = " and " + tempLongC;
      contKindGB = " and " + tempLongB;
    }
    else if(fm.ContKind.value == "22")
    {
      contKindGC = " and not " + tempLongC;
      contKindGB = " and not " + tempLongB;
    }
  }
  //如果录入保单号或者是投保单号就不受登陆机构限制，可以查询跨机构保单；否则，只能查询登陆机构或者是所辖机构的保单信息--sgh-2009-6-1
  if((fm.ContNo.value ==""||fm.ContNo.value ==null)&&(fm.PrtNo.value =="" || fm.PrtNo.value ==null))
  {
  	if(!IsValiCom(fm.ManageCom.value))
	{
	  alert("登陆机构不能查询界面录入机构的保单信息！");
	  return false;
	}
  }
  //查询无效保单
  var sql = ""
  var Sql11=""
  if(fm.AgentCode.value!=null&&fm.AgentCode.value!=""){
  	Sql11=" and a.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
  }
  var strSQL = ""
  if(fm.ContKind.value != "21" && fm.ContKind.value != "22")
  {
  //个单失效C表
	  strSQL = strSQL + "select a.ContNo,a.PrtNo,'个单', "
	    + "   a.AppntName,a.InsuredName,a.CValiDate, "
	    + "   a.prem, "
	    + "   codeName('stateflag', StateFlag), "
      + "   case when a.StateFlag = '3' then a.CInvaliDate else a.PayToDate end,"
      + "   case when a.StateFlag = '3' then codeName('stateflag', a.Stateflag) else codeName('stateflag', a.Stateflag) end, '', "
      + "   ShowManageName(a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only), '1', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', SaleChnl), AgentCom,(select Name from lacom where  AgentCom=a.agentcom)," +
      		" (select contno from lppol where polno in" +
      		"(select polno from lcpol where contno=a.contno) and  edortype = 'CF' order by edorno desc " +
      		" fetch first 1 rows only ),a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.Phone from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ),(select c.PostalAddress from lcaddress c,lcappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno )  "
      + "from LCCont a "
      + "where AppFlag='1' "
      + "   and ContType = '1' "
      + "   and a.StateFlag in('2', '3') "
      + getWherePart( 'a.ContNo','ContNo') 
	    + getWherePart( 'a.PrtNo','PrtNo') 
	    + getWherePart( 'a.StateFlag','StateFlag') 
      // 加入渠道、网点条件
      + getWherePart("SaleChnl","SaleChnlCode")
      + getWherePart("AgentCom","AgentComBank")
      // ----------------------------------------
	    + contKindPC
	    + "   and a.ManageCom like '" + fm.ManageCom.value + "%' "
	    + (fm.ManageComName.value == "" ? "" : "   and exists(select 1 from LDCom where Name like '%" + fm.ManageComName.value + "%') ")
	    + getWherePart( 'a.AgentGroup','Group03') 
	    + (fm.Group03Name.value == "" ? "" : "   and exists(select 1 from LABranchGroup where Name like '%" + fm.Group03Name.value + "%') ")
	   // + getWherePart( 'getUniteCode(a.AgentCode)','AgentCode') 
	    + Sql11
	    + (fm.AgentName.value == "" ? "" : "   and exists(select 1 from LAAgent where Name like '%" + fm.AgentName.value + "%') ")
	    + (fm.RiskCode1.value == "" ? "" : "  and exists (select 1 from LCPol where ContNo = a.ContNo and RiskCode = '" + fm.RiskCode1.value + "') ")
	    + (fm.RiskCode2.value == "" ? "" : "  and exists (select 1 from LCPol where ContNo = a.ContNo and RiskCode = '" + fm.RiskCode2.value + "') ")
	    + (fm.ApplyDateStart.value == "" ? "" : "  and a.PolApplyDate between '" + fm.ApplyDateStart.value + "' and '" + fm.ApplyDateEnd.value + "' ")
	    + (fm.PayToDateStart.value == "" ? "" : "  and a.PayToDate between '" + fm.PayToDateStart.value + "' and '" + fm.PayToDateEnd.value + "' ")
  
	    + "union "
	    
	    //个单失效B表
	    + "select a.ContNo,a.PrtNo,'个单', "
	    + "   a.AppntName,a.InsuredName,a.CValiDate, "
	    + "   a.prem, "
	    + "   codeName('stateflag', StateFlag),b.EdorValiDate,"
	    //#2051 合同终止退费--允许修改保费的功能
		+ " (case (select 1 from llcontdeal where edorno=a.edorno) when 1 then (select case edortype when 'CT' then '解约' when 'HZ' then '合同终止' when 'CD' then '合同终止退费' when 'RB' then '解约回退' end from llcontdeal where edorno=a.edorno) "
      + " else (select min(EdorName) from LMEdorItem where EdorCode = b.EdorType) end) , "
      + "   CodeName('reason_tb', b.ReasonCode), "
      + "   (select Name from LDCom where ComCode = a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.contno fetch first 1 rows only), '1', a.ManageCom,'B' "
      + " ,CodeName('lcsalechnl', SaleChnl), a.AgentCom,(select Name from lacom where  AgentCom=a.agentcom)," +
      		" (select contno from lppol where polno in" +
      		"(select polno from lcpol where contno=a.contno" +
      		" union "+ 
            "select polno from lppol where contno = a.contno" +
      		") and  edortype = 'CF' order by edorno desc " +
      		" fetch first 1 rows only ),a.signdate,a.signtime,'',(select c.Mobile from lcaddress c,lbappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ) ,(select c.Phone from lcaddress c,lbappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ) ,(select c.PostalAddress from lcaddress c,lbappnt p where c.CustomerNo=p.appntno and c.AddressNo=p.addressno and p.contno=a.contno ) "
      + "from LBCont a, LPEdorItem b "
      + "where a.EdorNo = b.EdorNo "
      + "and a.ContType = '1' "
      + getWherePart( 'a.ContNo','ContNo') 
	    + getWherePart( 'a.PrtNo','PrtNo') 
	    + getWherePart( 'a.StateFlag','StateFlag') 
	    // 加入渠道、网点条件
      + getWherePart("SaleChnl","SaleChnlCode")
      + getWherePart("AgentCom","AgentComBank")
      // ----------------------------------------
	    + contKindPB
	    + "   and a.ManageCom like '" + fm.ManageCom.value + "%' "
	    + (fm.ManageComName.value == "" ? "" : "   and exists(select 1 from LDCom where Name like '%" + fm.ManageComName.value + "%') ")
	    + getWherePart( 'a.AgentGroup','Group03') 
	    + (fm.Group03Name.value == "" ? "" : "   and exists(select 1 from LABranchGroup where Name like '%" + fm.Group03Name.value + "%') ")
	    //+ getWherePart( 'getUniteCode(a.AgentCode)','AgentCode') 
	    + Sql11
	    + (fm.AgentName.value == "" ? "" : "   and exists(select 1 from LAAgent where Name like '%" + fm.AgentName.value + "%') ")
	    + (fm.RiskCode1.value == "" ? "" : "  and exists (select 1 from LBPol where ContNo = a.ContNo and RiskCode = '" + fm.RiskCode1.value + "') ")
	    + (fm.RiskCode2.value == "" ? "" : "  and exists (select 1 from LBPol where ContNo = a.ContNo and RiskCode = '" + fm.RiskCode2.value + "') ")
	    + (fm.ApplyDateStart.value == "" ? "" : "  and a.PolApplyDate between '" + fm.ApplyDateStart.value + "' and '" + fm.ApplyDateEnd.value + "' ")
	    + (fm.PayToDateStart.value == "" ? "" : "  and a.PayToDate between '" + fm.PayToDateStart.value + "' and '" + fm.PayToDateEnd.value + "' ")
  ;
  }

  
  if(fm.ContKind.value != "11" && fm.ContKind.value != "12")
  {
      if(strSQL != "")
      {
        strSQL = strSQL + " union all ";  //这里用union all比用union效率高
      }
      
      //团单失效C表
      strSQL = strSQL + " select a.GrpContNo, a.PrtNo, '团单', "
      + "   a.GrpName, '', a.CValiDate, "
      + "   a.prem, "
      + "   codeName('stateflag', a.Stateflag), "
      + "   case when a.StateFlag = '3' then a.CInvaliDate else (select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo) end,"
      + "   case when a.StateFlag = '3' then codeName('stateflag', a.Stateflag) else codeName('stateflag', a.Stateflag) end, '', "
      + "   (select Name from LDCom where ComCode = a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only), '2', a.ManageCom, 'C' "
      + " ,CodeName('lcsalechnl', SaleChnl), AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,(select codename from ldcode where 1 = 1 and codetype = 'markettype' and code=a.MarketType),(select c.Phone1 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ),(select c.Phone2 from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LCGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) "
  		+ " from LCGrpCont a"
  		+ " where a.AppFlag = '1' "
  		+ "   and StateFlag in('2', '3') "
  		+ getWherePart( 'a.GrpContNo','ContNo') 
	    + getWherePart( 'a.PrtNo','PrtNo') 
	    + getWherePart( 'a.StateFlag','StateFlag') 
	   	+ getWherePart( 'a.StateFlag','StateFlag') 
	    // 加入渠道、网点条件
      + getWherePart("SaleChnl","SaleChnlCode")
      + getWherePart("AgentCom","AgentComBank")
      
      //add by Houyd 加入团体保单市场类型条件
      + getWherePart("MarketType","MarketType")
      //----------------
	    + contKindGC
	    + "   and a.ManageCom like '" + fm.ManageCom.value + "%' "
	    + (fm.ManageComName.value == "" ? "" : "   and exists(select 1 from LDCom where Name like '%" + fm.ManageComName.value + "%') ")
	    + getWherePart( 'a.AgentGroup','Group03') 
	    + (fm.Group03Name.value == "" ? "" : "   and exists(select 1 from LABranchGroup where Name like '%" + fm.Group03Name.value + "%') ")
	 //   + getWherePart( 'getUniteCode(a.AgentCode)','AgentCode') 
	    + Sql11
	    + (fm.AgentName.value == "" ? "" : "   and exists(select 1 from LAAgent where Name like '%" + fm.AgentName.value + "%') ")
	    + (fm.RiskCode1.value == "" ? "" : "  and exists (select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode = '" + fm.RiskCode1.value + "') ")
	    + (fm.RiskCode2.value == "" ? "" : "  and exists (select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode = '" + fm.RiskCode2.value + "') ")
	    + (fm.ApplyDateStart.value == "" ? "" : "  and a.PolApplyDate between '" + fm.ApplyDateStart.value + "' and '" + fm.ApplyDateEnd.value + "' ")
	    + (fm.PayToDateStart.value == "" ? "" : "  and (select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo) between '" + fm.PayToDateStart.value + "' and '" + fm.PayToDateEnd.value + "' ")
      
      + "union "
      
      //团单失效B表
      + " select a.GrpContNo, a.PrtNo, '团单', "
      + "   a.GrpName, '', a.CValiDate, "
      + "   a.prem, "
      + "   codeName('stateflag', a.Stateflag), "
      + "   b.EdorValiDate, (select min(EdorName) from LMEdorItem where EdorCode = b.EdorType), "
      + "   CodeName('reason_tb', b.ReasonCode), "
      + "   (select Name from LDCom where ComCode = a.ManageCom),(select (case returnvisitflag when '1' then '健康件' when '2' then '问题件' when '3' then '拒访件' when '4' then '书面回访件' when '5' then '核实件' when '6' then '方言件' end) from ReturnVisitTable where policyno=a.grpcontno fetch first 1 rows only), '2', a.ManageCom, 'B' "
      + " ,CodeName('lcsalechnl', SaleChnl), AgentCom,(select Name from lacom where  AgentCom=a.agentcom),'',a.signdate,a.signtime,(select codename from ldcode where 1 = 1 and codetype = 'markettype' and code=a.MarketType),(select c.Phone1  from LCGrpAddress c,LBGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.Phone2  from LCGrpAddress c,LBGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo ) ,(select c.GrpAddress from LCGrpAddress c,LBGrpAppnt p where c.CustomerNo=p.CustomerNo and c.AddressNo=p.addressno and p.GrpContNo=a.GrpContNo )   "
  		+ " from LBGrpCont a, LPGrpEdorItem b "
  		+ " where a.EdorNo = b.EdorNo "
  		+ "   and a.GrpContNo = b.GrpContNo "
  		+ "   and (a.AppFlag = '1' or a.AppFlag = '3') "  //AppFlag = '3'犹豫期退保
  		+ getWherePart( 'a.GrpContNo','ContNo') 
	    + getWherePart( 'a.PrtNo','PrtNo') 
	    + getWherePart( 'a.StateFlag','StateFlag') 
	    // 加入渠道、网点条件
      + getWherePart("SaleChnl","SaleChnlCode")
      + getWherePart("AgentCom","AgentComBank")
      
      //add by Houyd 加入团体保单市场类型条件
      + getWherePart("MarketType","MarketType")
      //----------------
	    + contKindGB
	    + "   and a.ManageCom like '" + fm.ManageCom.value + "%' "
	    + (fm.ManageComName.value == "" ? "" : "   and exists(select 1 from LDCom where Name like '%" + fm.ManageComName.value + "%') ")
	    + getWherePart( 'a.AgentGroup','Group03') 
	    + (fm.Group03Name.value == "" ? "" : "   and exists(select 1 from LABranchGroup where Name like '%" + fm.Group03Name.value + "%') ")
	  //  + getWherePart( 'getUniteCode(a.AgentCode)','AgentCode') 
	    + Sql11
	    + (fm.Group03Name.value == "" ? "" : "   and exists(select 1 from LAAgent where Name like '%" + fm.AgentName.value + "%') ")
	    + (fm.RiskCode1.value == "" ? "" : "  and exists (select 1 from LBGrpPol where GrpContNo = a.GrpContNo and RiskCode = '" + fm.RiskCode1.value + "') ")
	    + (fm.RiskCode2.value == "" ? "" : "  and exists (select 1 from LBGrpPol where GrpContNo = a.GrpContNo and RiskCode = '" + fm.RiskCode2.value + "') ")
	    + (fm.ApplyDateStart.value == "" ? "" : "  and a.PolApplyDate between '" + fm.ApplyDateStart.value + "' and '" + fm.ApplyDateEnd.value + "' ")
	    + (fm.PayToDateStart.value == "" ? "" : "  and (select min(PayToDate) from LBGrpPol where GrpContNo = a.GrpContNo) between '" + fm.PayToDateStart.value + "' and '" + fm.PayToDateEnd.value + "' ")
	    ;
  }
 
  turnPage4.pageDivName = "divPage4";
  turnPage4.pageLineNum = 50;
  turnPage4.queryModal(strSQL, BContGrid);
  fm.InvaliContSql.value = strSQL;
  document.all("divPage4").style.display="";
  document.all("divPage3").style.display="";
}

//------------------------------校验区----------------

/*********************************************************************
 *  上下级机构判断
 *  参数  ：  pManageCom String : 管理机构
 *  返回值：  true 可查看该机构保单；false 不可查看
 *********************************************************************
 */
function IsValiCom(pManageCom)
{
	if (comCode =='86') return true;
	if (pManageCom =='86'  && comCode =='86') return true;
	if (pManageCom =='86000000'  && comCode =='86000000') return true;	
	if (pManageCom.length >4 && comCode.substring(0,4) == pManageCom.substring(0,4) 
		&& comCode <= pManageCom  ) return true;
	if (pManageCom.length == 4 && comCode == pManageCom) return true;
	
	return false;
	
}

/*********************************************************************
 *  打印保单清单
 *  参数：contType: "Vali" 有效保单
 *  返回值：  
 *********************************************************************
 */
function printContList(contType)
{
  if(contType == "Vali" && GrpPolGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }
  
  if(contType == "InVali" && BContGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }
  
  fm.action = "PrintContListSave.jsp?ContType=" + contType;
	fm.target = "_blank";
	fm.submit();
}


/*********************************************************************
 *  校验能否执行保单查询
 *  参数  ：  
 *  返回值：  
 *********************************************************************
 */
function checkQueryCont()
{
  if(fm.ManageCom.value == "")
  {
    alert("请选择机构");
    return false;
  }
  
  if(fm.ContNo.value == "" && fm.PrtNo.value == "" && fm.ApplyDateStart.value == "" && fm.ApplyDateEnd.value == "")
  {
    alert("保单号和印刷号都为空时，必须录入投保日期范围！");
    return false;
  }
  
  if(fm.ContNo.value == ""
      && fm.PrtNo.value == ""
      && fm.StateFlag.value == ""
      && fm.ContKind.value == ""
      && fm.ManageCom.value == ""
      && fm.AgentCode.value == ""
      && fm.Group03.value == ""
      && fm.RiskCode1.value == ""
      && fm.RiskCode2.value == ""
      && fm.ApplyDateStart.value == ""
      && fm.ApplyDateEnd.value == ""
      && fm.PayToDateStart.value == ""
      && fm.PayToDateEnd.value == "")
  {
    alert("按保单查询时，各录入项不能全为空");
    return false;
  }
  
  if(fm.ApplyDateStart.value != "" && fm.ApplyDateEnd.value == ""
    || fm.ApplyDateStart.value == "" && fm.ApplyDateEnd.value != "")
  {
    alert("申请日期应同时录入或均不录入");
    return false;
  }
  
  if(fm.PayToDateStart.value != "" && fm.PayToDateEnd.value == ""
    || fm.PayToDateStart.value == "" && fm.PayToDateEnd.value != "")
  {
    alert("交至日期范围应同时录入或均不录入");
    return false;
  }
  
  if(!checkApplyDate())
  {
     return false;
  }
  
  if(fm.ApplyDateStart.value == "" && fm.ApplyDateEnd.value == ""
    && fm.PayToDateStart.value == "" && fm.PayToDateEnd.value == ""
    && fm.ContNo.value == "" && fm.PrtNo.value == "")
  {
    if(!confirm("没有选择时间段，可能查询数据较多，速度较慢，仍然继续？"))
    {
      return false;
    }
  }
  
  return true;
}

//请除全局变量
function clearGlobalVar()
{
  tSel = -1;	//所选的有效保单行
  tSel1 = -1; //所选的无效保单行
  fm.ContNoSelected.value = "";
}

//扫描件查询
function allScanInfo()
{
  if(fm.ContNoSelected.value == "")
  {
    alert("请选择保单");
    return false;
  }
  
  var tContType
  var tPrtNo = "";
  if(tSel1 > 0)
  {
    tContType = BContGrid.getRowColDataByName(tSel1 - 1, "ContType");
    tPrtNo = BContGrid.getRowColDataByName(tSel1 - 1, "PrtNo");
  }
  else
  {
    tContType = GrpPolGrid.getRowColDataByName(tSel - 1, "ContType");
    tPrtNo = GrpPolGrid.getRowColDataByName(tSel - 1, "PrtNo");
  }
  
  var tAppObj = "";
  if(tContType == "1")
  {
    tAppObj = "I";
  }
  else
  {
    tAppObj = "G";
  }
  
  window.open("../easyscan/ScanListMain.jsp?ContNo=" + fm.ContNoSelected.value + "&AppObj=" + tAppObj + "&PrtNo=" + tPrtNo);
}

/**
 * 下拉框，选定后触发事件。
 */
function afterCodeSelect(tCodeName, tObj)
{
    if (tCodeName == "salechnlall")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    var tTmpUrl = "../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01";
    showInfo=window.open(tTmpUrl);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
        fm.AgentComName.value = arrQueryResult[0][1];
    }
}

//20080731 zhanggm 选择投保日期范围不能大于6个月
function checkApplyDate()
{
  var sql = "select 1 from Dual where date('" + fm.ApplyDateEnd.value + "') > date('" + fm.ApplyDateStart.value + "') + 6 month ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    alert("选择投保日期范围不能大于6个月！")
    return false;
  }
  return true;
}

function HJAppnt(){
	 try{
		 var tContNo = mContNo;
		  if (tContNo == "")
		  {
		    tContNo= trim(fm.all('ContNo').value);
			}
			if (tContNo == "")
			{
			  alert("请选择一个保单！");
			  return false;
			}
			//个单不能查看被保人清单
			
			if (tContType == 1)
			{
			  alert("个单没有被保人清单！");
			  return false;
			}

			var tContPrintType = "";
			var tContPrintTypeSQL = "select ContPrintType,ManageCom from lcgrpcont where grpcontno = '"+tContNo+"' ";
			var tContPrintTypeArr = easyExecSql(tContPrintTypeSQL);
			if(!tContPrintTypeArr){
				alert("获取保单数据失败！");
				return false;
			}else{
				tContPrintType = tContPrintTypeArr[0][0];
			}
			if(tContPrintType!="" && tContPrintType!="5"){
				alert("非汇交件不能查看投保人信息！");
				return;
			}else{
				showInfo = window.open("../app/HJAppntMain.jsp?GrpContNo="+tContNo+"&flag=view");
			}
	 }catch(e){ 
		 alert("错误信息"+e.message);
	 }
}
function getqspkFlag(){
	var Count = BContGrid.mulLineCount;
	for (var i = 0; i < Count; i++) {
		if (BContGrid.getRowColData(i, 14) == '1') {
			continue;
		}
		var sql = " select standbyflag3,mult from lcgrpcont where grpcontno ='"
				+ BContGrid.getRowColData(i, 1)
				+ "' and stateflag in('2','3')"
				+ "union select standbyflag3,mult from lbgrpcont where grpcontno ='"
				+ BContGrid.getRowColData(i, 1)
				+ "' and stateflag in('2','3') ";
		var result = easyExecSql(sql);
		if (result == null) {
			BContGrid.setRowColData(i, 30, "无");
		}
		if ("01" == result[0][0]) {
			BContGrid.setRowColData(i, 30, "有");
		} else {
			BContGrid.setRowColData(i, 30, "无");
		}
	}
	var Count2 = GrpPolGrid.mulLineCount;
	for (var i = 0; i < Count2; i++) {
		if (GrpPolGrid.getRowColData(i, 15) == '1') {
			continue;
		}
		var sql = " select standbyflag3,mult from lcgrpcont where grpcontno ='"
				+ GrpPolGrid.getRowColData(i, 1)
				+ "' and stateflag = '1' ";
		var result = easyExecSql(sql);
		if (result == null) {
			GrpPolGrid.setRowColData(i, 31, "无");
		}
		if ("01" == result[0][0]) {
			GrpPolGrid.setRowColData(i, 31, "有");
		} else {
			GrpPolGrid.setRowColData(i, 31, "无");
		}
	}
	    		 
}