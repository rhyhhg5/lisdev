 //               该文件中包含客户端需要处理的函数和事件
var mOperate = "";
var showInfo1;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 

var arrResult;

//存放添加动作执行的次数
var addAction = 0;
//暂交费总金额
var sumTempFee = 0.0;
//暂交费信息中交费金额累计
var tempFee = 0.0;
//暂交费分类信息中交费金额累计
var tempClassFee = 0.0;
//单击确定后，该变量置为真，单击添加一笔时，检验该值是否为真，为真继续，然后再将该变量置假
var confirmFlag=false;
//
var arrCardRisk;
//window.onfocus=focuswrap;
var mSwitch = parent.VD.gVSwitch;
//工作流flag
var mWFlag = 0;


/*********************************************************************
 *  返回上一页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */  
function returnparent()
{    	
	parent.fraInterface.window.location="../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"
	+ "&ContType=" + tContType;
}

/*********************************************************************
 *  代码选择后触发时间
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
    try	
    {
   	    if(cCodeName == "Relation")
   	    {
	          if(fm.all('RelationToAppnt').value=="00")
	          {		
	          	fm.all('SamePersonFlag').checked=true;
	          	displayissameperson();
	          	isSamePerson();
		        }	
   	    }
        //如果是无名单
        if( cCodeName == "PolTypeFlag")
        {
            if (Field.value!='0')
            {
                fm.all('InsuredPeoples').readOnly=false;
                fm.all('InsuredAppAge').readOnly=false;
            }
            else
            {
                fm.all('InsuredPeoples').readOnly=true;
                fm.all('InsuredAppAge').readOnly=true;
            }
        }     
         if( cCodeName == "ImpartCode")
         {
	   
         }  
         if( cCodeName == "SequenceNo")
         {
	   if(Field.value=="1"&&fm.SamePersonFlag.checked==false)
	   {
	   	     emptyInsured();
		     param="121";
		     fm.pagename.value="121"; 
                     fm.InsuredSequencename.value="第一被保险人资料";	
                     fm.RelationToMainInsured.value="00";	   	
	   }
	   if(Field.value=="2"&&fm.SamePersonFlag.checked==false)
	   {
	   	if(InsuredGrid.mulLineCount==0)
	   	{	   		
	   		alert("请先添加第一被保人");
	   		fm.SequenceNo.value="1";	   		
	   		return false;
	   	}	   	
	   	     emptyInsured();
                     noneedhome();   	     
		     param="122";
		     fm.pagename.value="122"; 
                     fm.InsuredSequencename.value="第二被保险人资料";		   	
	   }
	   if(Field.value=="3"&&fm.SamePersonFlag.checked==false)
	   {
	   	if(InsuredGrid.mulLineCount==0)
	   	{	   		
	   		alert("请先添加第一被保人");
	   		Field.value="1";
	   		return false;
	   	}
	   	if(InsuredGrid.mulLineCount==1)
	   	{	   		
	   		alert("请先添加第二被保人");
	   		Field.value="1";
	   		return false;
	   	}	   		   	
	   	     emptyInsured();
                     noneedhome();	   	     
		     param="123";
		     fm.pagename.value="123"; 
                     fm.InsuredSequencename.value="第三被保险人资料";	   	
	   }	
           if (scantype== "scan") 
           {  
           setFocus(); 
           }	      	   	
         } 
         
    if(cCodeName=="OccupationCode")
    {
    	//alert();
    	var t = Field.value;
    	var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+t+"'";
    	//alert(strSql);
    	var arr = easyExecSql(strSql);
    	if( arr )
    	{
    		fm.OccupationType.value = arr[0][0];
    	}
    }       
    }
    catch(ex) {}
      
}

/*********************************************************************
 *  把查询返回的客户地址数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayAddress()
{	
	try{fm.all('Phone').value= arrResult[0][4]; }catch(ex){}; 
	try{fm.all('Mobile').value= arrResult[0][14]; }catch(ex){}; 
	try{fm.all('EMail').value= arrResult[0][16]; }catch(ex){}; 
  //try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){}; 
	try{fm.all('GrpPhone').value= arrResult[0][12]; }catch(ex){}; 
	try{fm.all('CompanyAddress').value= arrResult[0][10]; }catch(ex){}; 
	try{fm.all('GrpZipCode').value= arrResult[0][11]; }catch(ex){}; 
}



/*********************************************************************
 *  获得被保人险种信息，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function getInsuredPolInfo()
{
  initPolGrid();	
  var InsuredNo=cInsuredNo;
  var tContNo=cContNo;
  //险种信息初始化
  if(InsuredNo!=null&&InsuredNo!="")
  {
	
    var strSQL = "select a.RiskSeqNo,b.RiskName, a.RiskCode, a.Amnt, a.Mult, a.Prem, a.PolNo "
                + "from LCPol a, LMRisk b "
                + "where a.RiskCode = b.RiskCode "
                + "   and ContNo = '" + tContNo + "' "
                + "   and InsuredNo = '" + InsuredNo + "' "
                + "union "
                + "select a.RiskSeqNo,b.RiskName, a.RiskCode, a.Amnt, a.Mult, a.Prem, a.PolNo "
                + "from LBPol a, LMRisk b "
                + "where a.RiskCode = b.RiskCode "
                + "   and ContNo = '" + tContNo + "' "
                + "   and InsuredNo = '" + InsuredNo + "' "
                
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
}


/*********************************************************************
 *  被保人客户号查询事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */                                                      
function queryInsuredNo() 
{     
    if (fm.all("InsuredNo").value == "") 
    {                    
      showAppnt1();                                                                                      
    } 
    else 
    {                                                                   
//        arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + fm.all("InsuredNo").value + "'", 1, 0);
//        if (arrResult == null) 
//        {                                                 
//          displayAppnt(new Array());   
//		  return false;
//        } 
//        else 
//        {                                                                 
//          displayAppnt(); 
//		  showInsuredOther();
//		  getdetailaddress();
//		  queryPay();
//        }
    	var sql = "";
    	if(tContType == 1) {//有效的保单
    		sql = "select Name,Sex,Birthday,IDType,IDNo,OccupationCode,Marriage,Authorization from lcinsured where contno = '"+cContNo+"' and insuredno = '"+fm.all("InsuredNo").value+"' ";
    	}else if(tContType == 2) {//失效的保单
    		sql = "select Name,Sex,Birthday,IDType,IDNo,OccupationCode,Marriage,Authorization from lbinsured where contno = '"+cContNo+"' and insuredno = '"+fm.all("InsuredNo").value+"' ";
    	}
    	arrResult = easyExecSql(sql,1,0);
    	if(arrResult == null) {
    		displayInsured(new Array());//未查到相关数据，传入空值代替
    		return false;
    	}else {
    		displayInsured();
    		showInsuredOther();
  		    getdetailaddress();
  		    queryPay();
    	}
    }                                                                          
}
/*********************************************************************
 * 在页面增加如下显示
 * 被保险人资料:证件生效日期、证件失效日期、岗位职务
 *********************************************************************
 */
function showMoreInsured() {
	try{
		var sql = "";
		if(tContType == 1) {//有效保单
			sql = "select IDStartDate,IDEndDate,Position from lcinsured where contno = '"+cContNo+"' and insuredno = '"+cInsuredNo+"' ";
		}else if(tContType == 2) {//失效的保单
			sql = "select IDStartDate,IDEndDate,Position from lbinsured where contno = '"+cContNo+"' and insuredno = '"+cInsuredNo+"' ";
		}
		var rs = easyExecSql(sql);
		if(rs != null) {
			fm.all('InsuredIDStartDate').value = rs[0][0];
			fm.all('InsuredIDEndDate').value = rs[0][1];
			fm.all('InsuredPosition').value = rs[0][2];
		}
	}catch(ex) {
		alert("证件生效日期、证件失效日期、岗位职务取值失败！");
	}
	
}


/*********************************************************************
 *  投保人查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */                                                                                 
function showAppnt1()                                                        
{                                                                            
	if( mOperate == 0 )                                                          
	{                                                                            
		mOperate = 2;                                                        
		showInfo = window.open( "../sys/LDPersonQueryNew.html" );               
	}                                                                            
}

/*********************************************************************
 *  显示被保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */        
function displayInsured() {
	try{
		fm.all('Name').value = arrResult[0][0];
		fm.all('Sex').value = arrResult[0][1];
		var sql = "select CodeName('sex', '" + fm.Sex.value + "') from dual ";
	    var rs = easyExecSql(sql);
	    if(rs) {
	      fm.all('Sex').value= rs[0][0];
	    }
		fm.all('Birthday').value = arrResult[0][2];
		fm.all('IDType').value = arrResult[0][3];
		var sql = "select CodeName('idtype', '" + fm.IDType.value + "') from dual ";
	    var rs = easyExecSql(sql);
	    if(rs) {
	      fm.all('IDType').value= rs[0][0];
	    }
		fm.all('IDNo').value = arrResult[0][4];
		fm.all('OccupationCode').value = arrResult[0][5];
		fm.all('OccupationName').value = getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][5]);
		fm.all('Marriage').value = arrResult[0][6];
		//modify by zxs 
			fm.all('InsuredAuth').value= arrResult[0][7];
			if(arrResult[0][7]=='1'){
				fm.all('InsuredAuthName').value = '是';
			}
			if(arrResult[0][7]=='0'){
				fm.all('InsuredAuthName').value = '否';

			}
		
		var sql = "select CodeName('marriage', '" + fm.Marriage.value + "') from dual ";
	    var rs = easyExecSql(sql);
	    if(rs) {
	      fm.all('Marriage').value= rs[0][0];
	    }
	}catch(ex){
		alert("ContInsuredInput.js--displayInsured被保人信息赋值出错！");
	};
//    try{fm.all('InsuredNo').value= arrResult[0][0]; }catch(ex){};
//    try{fm.all('Name').value= arrResult[0][1]; }catch(ex){}; 
//    try{fm.all('Sex').value= arrResult[0][2]; }catch(ex){}; 
//    var sql = "select CodeName('sex', '" + fm.Sex.value + "') from dual ";
//    var rs = easyExecSql(sql);
//    if(rs)
//    {
//      try{fm.all('Sex').value= rs[0][0]; }catch(ex){}; 
//    }
//    
//    try{fm.all('Birthday').value= arrResult[0][3]; }catch(ex){}; 
//    try{fm.all('IDType').value= arrResult[0][4]; }catch(ex){}; 
//    var sql = "select CodeName('idtype', '" + fm.IDType.value + "') from dual ";
//    var rs = easyExecSql(sql);
//    if(rs)
//    {
//      try{fm.all('IDType').value= rs[0][0]; }catch(ex){}; 
//    }
//    
//    try{fm.all('IDNo').value= arrResult[0][5]; }catch(ex){}; 
//    try{fm.all('Password').value= arrResult[0][6]; }catch(ex){}; 
//    try{fm.all('NativePlace').value= arrResult[0][7]; }catch(ex){}; 
//    try{fm.all('Nationality').value= arrResult[0][8]; }catch(ex){}; 
//    try{fm.all('RgtAddress').value= arrResult[0][9]; }catch(ex){}; 
//    try{fm.all('Marriage').value= arrResult[0][10];}catch(ex){}; 
//    var sql = "select CodeName('marriage', '" + fm.Marriage.value + "') from dual ";
//    var rs = easyExecSql(sql);
//    if(rs)
//    {
//      try{fm.all('Marriage').value= rs[0][0]; }catch(ex){}; 
//    }
//    
//    try{fm.all('MarriageDate').value= arrResult[0][11];}catch(ex){}; 
//    try{fm.all('Health').value= arrResult[0][12];}catch(ex){}; 
//    try{fm.all('Stature').value= arrResult[0][13];}catch(ex){}; 
//    try{fm.all('Avoirdupois').value= arrResult[0][14];}catch(ex){}; 
//    try{fm.all('Degree').value= arrResult[0][15];}catch(ex){}; 
//    try{fm.all('CreditGrade').value= arrResult[0][16];}catch(ex){}; 
//    try{fm.all('OthIDType').value= arrResult[0][17];}catch(ex){}; 
//    try{fm.all('OthIDNo').value= arrResult[0][18];}catch(ex){}; 
//    try{fm.all('ICNo').value= arrResult[0][19];}catch(ex){}; 
//    try{fm.all('GrpNo').value= arrResult[0][20];}catch(ex){}; 
//    try{fm.all('JoinCompanyDate').value= arrResult[0][21];}catch(ex){}; 
//    try{fm.all('StartWorkDate').value= arrResult[0][22];}catch(ex){}; 
//    try{fm.all('Position').value= arrResult[0][23];}catch(ex){}; 
//    try{fm.all('Salary').value= arrResult[0][24];}catch(ex){}; 
//    try{fm.all('OccupationType').value= arrResult[0][25];}catch(ex){};
//    //从lcinsured表中重新获取OccupationCode，因为ldperson表中出现了该字段数据为空的情况
//    var occupationCodeSql = "select OccupationCode from lcinsured where contno = '"+cContNo+"' and insuredno = '"+arrResult[0][0]+"'";
//    var occupationCode = easyExecSql(occupationCodeSql);
//    try{fm.all('OccupationCode').value= occupationCode;}catch(ex){}; 
//    try{fm.all('OccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",occupationCode);} catch(ex) { };      
//    try{fm.all('WorkType').value= arrResult[0][27];}catch(ex){}; 
//    try{fm.all('PluralityType').value= arrResult[0][28];}catch(ex){}; 
//    try{fm.all('DeathDate').value= arrResult[0][29];}catch(ex){}; 
//    try{fm.all('SmokeFlag').value= arrResult[0][30];}catch(ex){}; 
//    try{fm.all('BlacklistFlag').value= arrResult[0][31];}catch(ex){}; 
//    try{fm.all('Proterty').value= arrResult[0][32];}catch(ex){}; 
//    try{fm.all('Remark').value= arrResult[0][33];}catch(ex){}; 
//    try{fm.all('State').value= arrResult[0][34];}catch(ex){}; 
//    try{fm.all('Operator').value= arrResult[0][35];}catch(ex){}; 
//    try{fm.all('MakeDate').value= arrResult[0][36];}catch(ex){}; 
//    try{fm.all('MakeTime').value= arrResult[0][37];}catch(ex){}; 
//    try{fm.all('ModifyDate').value= arrResult[0][38];}catch(ex){}; 
//    try{fm.all('ModifyTime').value= arrResult[0][39];}catch(ex){}; 
//    try{fm.all('GrpName').value= arrResult[0][41];}catch(ex){}; 
//	  //try{fm.all('AddressNo').value= "";}catch(ex){}; 
//    //地址显示部分的变动
//       
//    try{fm.all('Phone').value=  "";}catch(ex){}; 
//    try{fm.all('Mobile').value=  "";}catch(ex){}; 
//    try{fm.all('EMail').value=  "";}catch(ex){}; 
//    //try{fm.all('GrpName').value= arrResult[0][46];}catch(ex){}; 
//    try{fm.all('GrpPhone').value=  "";}catch(ex){}; 
//    try{fm.all('GrpAddress').value=  "";}catch(ex){}; 
//    try{fm.all('GrpZipCode').value=  "";}catch(ex){}; 
//    try{fm.all('HomeAddress').value=  "";}catch(ex){}; 
//    try{fm.all('HomeZipCode').value=  "";}catch(ex){}; 
//    try{fm.all('HomePhone').value=  "";}catch(ex){};     
}

/*********************************************************************
 *  显示地址信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 
function getdetailaddress()
{
    var strSQL="select b.AddressNo,b.PostalAddress,b.ZipCode,b.Phone,b.Mobile,b.EMail,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode,b.HomeAddress,b.HomeZipCode,b.HomePhone,b.GrpName from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.InsuredNo.value+"'";
    arrResult=easyExecSql(strSQL);

	try{fm.all('PostalAddress').value= arrResult[0][1];}catch(ex){}; 
    try{fm.all('ZipCode').value= arrResult[0][2];}catch(ex){}; 
    try{fm.all('Phone').value= arrResult[0][3];}catch(ex){}; 
    try{fm.all('Mobile').value= arrResult[0][4];}catch(ex){}; 
    try{fm.all('EMail').value= arrResult[0][5];}catch(ex){};     
    try{fm.all('GrpPhone').value= arrResult[0][6];}catch(ex){}; 
    try{fm.all('GrpAddress').value= arrResult[0][7];}catch(ex){}; 
    try{fm.all('GrpZipCode').value= arrResult[0][8];}catch(ex){}; 
    try{fm.all('HomeAddress').value= arrResult[0][9];}catch(ex){}; 
    try{fm.all('HomeZipCode').value= arrResult[0][10];}catch(ex){}; 
    try{fm.all('HomePhone').value= arrResult[0][11];}catch(ex){}; 
    try{fm.all('GrpName').value= arrResult[0][12];}catch(ex){};       
}

/*********************************************************************
 *  被保人其他信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */           
function showInsuredOther()                                                        
{   
	var strTableName = "";
	if (tContType=='1')
	{
		strTableName = "LCInsured";
	}else if (tContType=='2')
	{  
		strTableName = "LBInsured";
	}
	var strSql = "select a.SequenceNo,CodeName('relation', a.RelationToAppnt),CodeName('relation', a.RelationToMainInsured),CodeName('nativeplace', a.NativePlace),a.AddressNo " 
		+ " from " +  strTableName + " a where 1=1  and a.InsuredNo = '" + fm.all("InsuredNo").value 
		+ "' and a.ContNo = '" + cContNo + "'"
        ;
	var arrReturn  = easyExecSql(strSql, 1, 0);

	if (arrReturn == null) {
		alert("查询结果为空！");
	}else {
         try{fm.all('SequenceNo').value= arrReturn[0][0];}catch(ex){}; 
		 try{fm.all('RelationToAppnt').value= arrReturn[0][1];}catch(ex){}; 
		 try{fm.all('RelationToMainInsured').value= arrReturn[0][2];}catch(ex){}; 
		 try{fm.all('NativePlace').value= arrReturn[0][3];}catch(ex){}; 
		 if(arrReturn[0][3] == "外籍人士" || arrReturn[0][3] == "港澳台人士") {
		  		fm.all('NativeCity').style.display = "";
		  		var Sql = "select codename from ldcode where codetype = 'nativecity' and code = (select nativecity from "+strTableName+" where contno = '"+cContNo+"' )";
		  		var rs = easyExecSql(Sql);
		  		if(rs != null) {
		  			fm.all('NativeCity').value = rs[0][0];
		  		}else {
		  			alert(arrReturn[0][3]+"：获取到国家或地区信息失败！");
		  		}
		 }
		 try{fm.all('AddressNo').value= arrReturn[0][4];}catch(ex){}; 
	}
}

 /*********************************************************************
 *  设置理赔信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */                                                      
function queryPay() 
{   

	   //区分签单和撤保/退保
	var strTableName = "";
	if (tContType=='1')
	{
		strTableName = "LCInsured";
	}else if (tContType=='2')
	{  
		strTableName = "LBInsured";
	}
	var arrReturn2 = new Array();

	tInsuredNo = trim(fm.all("InsuredNo").value);
  if ((tInsuredNo == "" || tInsuredNo == null) && tLoadFlag=="1")  {                    
  } else 
  {      
     
    var strSql2 = "select b.BankName, a.BankAccNo, a.AccName "
                  + "from LCInsured a, LDBank b "
                  + "where a.BankCode = b.BankCode "
                  + "   and a.ContNo = '" + cContNo + "' "
                  + "   and a.InsuredNo = '" + fm.InsuredNo.value + "' ";
		arrReturn2 =easyExecSql(strSql2);
			
		if (arrReturn2 == null) {

	    } else {
			displyPay(arrReturn2[0]);
		}
    }                                                                      
}

 /*********************************************************************
 *  理赔信息查询
 *  参数  ： cArr:一维数组
 *  返回值：  无
 *********************************************************************
 */                                                      
function displyPay(cArr) 
{   
	try { fm.all('BankCode').value = cArr[0]; } catch(ex) { };
  	try { fm.all('BankAccNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('AccName').value = cArr[2]; } catch(ex) { };
}

/*********************************************************************
 *  险种明细查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function riskQueryClick()  
 {  
	 var tSel = PolGrid.getSelNo();	
	 if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	 else
	{
		 var cPolNo = PolGrid.getRowColData( tSel - 1, 7 );
		 var cRiskSeqNo = PolGrid.getRowColData( tSel - 1, 1);
		  window.open("./LCProposalInput.jsp?InsuredNo=" 
			  + cInsuredNo + "&ContNo=" + cContNo+"&PolNo=" + cPolNo + "&RiskSeqNo=" 
			  + cRiskSeqNo + "&LogFlag=2" + "&ContType=" + tContType);
	}
 }

/*********************************************************************
 *  体检信息查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function queryPE()  
 {  
     initPEGrid();	
    var tContNo=cContNo;
    //体检信息初始化
    if(tContNo!=null&&tContNo!="")
    {
		//var strSql = "select PEDate,'' from LCPENotice where ContNo='" + tContNo + "'" ;
		
		var strSql = "select a.PEDate, a.pEAddress, sum(decimal(c.MedicaItemPrice, 20, 0)) "
            + "from LCPENotice a, LCPENoticeItem b, LDTestPriceMgt c "
            + "where a.ProposalContNo = b.ProposalContNo "
	        + "     and a.PrtSeq = b.PrtSeq "
	        + "     and b.peItemCode = c.MedicaItemCode "
	        + "     and a.pEAddress = c.hospitCode "
	        + "     and a.contNo = '" + cContNo + "' "
            + "group by a.PEDate, a.pEAddress ";
		
        turnPage3.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage3.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage3.arrDataCacheSet = decodeEasyQueryResult(turnPage3.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage3.pageDisplayGrid = PEGrid;    
        //保存SQL语句
        turnPage3.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage3.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage3.getData(turnPage3.arrDataCacheSet, turnPage3.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage3.pageDisplayGrid);
    }
 }

/*********************************************************************
 *  设置被保人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function setSequencename()  
 {  
    if(fm.SequenceNo.value=="1")
	{
		fm.InsuredSequencename.value="第一被保险人资料";
	}else	if(fm.SequenceNo.value=="2")
	{
		fm.InsuredSequencename.value="第二被保险人资料";
	}else	if(fm.SequenceNo.value=="3")
	{
		fm.InsuredSequencename.value="第三被保险人资料";
	}else
	{
         fm.InsuredSequencename.value="被保险人资料";
	}
 }