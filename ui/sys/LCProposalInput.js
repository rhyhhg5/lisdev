//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 
var turnPage4 = new turnPageClass(); 
var arrAllDataSet；
var arrPayData;
window.onfocus = myonfocus;

/*********************************************************************
 *  查询被保险人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function getRiskQuery()  
 {  
  
  try { fm.all('RiskSeqNo').value = tRiskSeqNo; } catch(ex) { };
	var arrReturn = new Array();

	var strTableName = "";
	if (tContType=='1')
	{
		strTableName = "LCInsured";
	}else if (tContType=='2')
	{  
		strTableName = "LBInsured";
	}
	var  strSQL = "select Name,InsuredNo,Birthday,OccupationCode,CodeName('relation', RelationToAppnt) from LCInsured"  
	+  " where ContNo='" + tContNo + "' and InsuredNo='" + tInsuredNo + "'"
	+ " union all "	
	+ " select Name,InsuredNo,Birthday,OccupationCode,CodeName('relation', RelationToAppnt) from LBInsured"  
	+  " where ContNo='" + tContNo + "' and InsuredNo='" + tInsuredNo + "'"
    ;
	arrReturn = easyExecSql(strSQL);
	if ( arrReturn == null)
	{
         return false;
	}
   displayInsured(arrReturn[0]);
 }
  
/*********************************************************************
 *  设置险种信息
 *  参数  ：  cArr
 *  返回值：  无
 *********************************************************************
 */
  function displayInsured(cArr)  
 { 
	try { fm.all('Name').value = cArr[0]; } catch(ex) { };
	try { fm.all('InsuredNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('Birthday').value = cArr[2]; } catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[3]; } catch(ex) { };
	try { fm.all('RelationToAppnt').value = cArr[4]; } catch(ex) { };
 }

 /*********************************************************************
 *  查询险种信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryRisk()
{
	
	// 初始化表格
	initRiskGrid();	
	// 书写SQL语句RiskCode
	var strSQL = "";

	//区分签单和撤保/退保
	var strTableName = "";
	var strInValidate = "";
	strSQL = "select a.RiskSeqNo, a.CValiDate,a.Years,a.PayYears, b.riskname,a.RiskCode, "
	+ "   (select a.Amnt FROM LDRiskParamPrint c  where c.factorName='AmntFlag' and  c.RiskCode=a.RiskCode and c.FactorValue='1'), "
	+ "   (select a.Mult FROM LDRiskParamPrint c  where c.factorName='AmntFlag' and  c.RiskCode=a.RiskCode and c.FactorValue='3'),"
	+ "   a.Prem ,(select nvl(sum(Prem), 0) from LCPrem where PolNo = a.PolNo and PayPlanCode like '000000%'),a.StandPrem,'', a.PolNo "
	+ " from  LCPol a,lmrisk b where PolNo='" + tPolNo + "' and  a.RiskCode=b.riskcode "
	+ " union all "
	+ " select a.RiskSeqNo, a.CValiDate,a.Years,a.PayYears, b.riskname, a.RiskCode, "
	+ " (select a.Amnt FROM LDRiskParamPrint c  where c.factorName='AmntFlag' and  c.RiskCode=a.RiskCode and c.FactorValue='1'), "
	+ " (select a.Mult FROM LDRiskParamPrint c  where c.factorName='AmntFlag' and  c.RiskCode=a.RiskCode and c.FactorValue='3'),"
	+ " a.Prem ,(select nvl(sum(Prem), 0) from LCPrem where PolNo = a.PolNo and PayPlanCode like '000000%'),a.StandPrem, "
	+ " (select min(EdorName) from LMEdorItem where EdorCode = (select EdorType from LPEdorItem where EdorNo = a.EdorNo)), a.PolNo "
	+ " from  LBPol a,lmrisk b where PolNo='" + tPolNo + "' and  a.RiskCode=b.riskcode "
	+ " union all "
	//续保
	+ " select a.RiskSeqNo, a.CValiDate,a.Years,a.PayYears, b.riskname,a.RiskCode, "
	+ "   (select a.Amnt FROM LDRiskParamPrint c  where c.factorName='AmntFlag' and  c.RiskCode=a.RiskCode and c.FactorValue='1'),"
	+ "   (select a.Mult FROM LDRiskParamPrint c  where c.factorName='AmntFlag' and  c.RiskCode=a.RiskCode and c.FactorValue='3'),"
	+ "   a.Prem ,(select nvl(sum(Prem), 0) from LCPrem where PolNo = a.PolNo and PayPlanCode like '000000%'),a.StandPrem,'已续保', a.PolNo "
	+ " from  LBPol a,lmrisk b "
	+ " where PolNo in(select newPolNo from LCRnewStateLog where state = '6' and polNo = '" + tPolNo + "' and  a.RiskCode=b.riskcode) "
	
	
	
	//查询SQL，返回结果字符串
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	RiskGrid.clearData();
 
    return false;
  }
 
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  arrAllDataSet = turnPage.arrDataCacheSet;  
  setAmntMult();
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = RiskGrid;   
         
  //保存SQL语句
  turnPage.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 // setInvalidate(); 
  var arrDataSet = turnPage.getData(arrAllDataSet, turnPage.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  nullToEmpty();
} 

//null转换为""
function nullToEmpty()
{
  for(var i = 0; i < RiskGrid.mulLineCount; i++)
  {
    if(RiskGrid.getRowColDataByName(i, "Amnt") == "null")
    {
      RiskGrid.setRowColDataByName(i, "Amnt", "");
    }
    
    if(RiskGrid.getRowColDataByName(i, "Mult") == "null")
    {
      RiskGrid.setRowColDataByName(i, "Mult", "");
    }
  }
}

/*********************************************************************
 *  查询加费信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryAddPrem()
{
  var sql = "select Prem, case when Rate > 0 then varchar(Rate) else '' end, "
            + "   PayStartDate, PayEndDate "
            + "from LCPrem "
            + "where PolNo = '" + tPolNo + "'and payplancode like '000000%'"
            + "union all "
            + "select Prem, case when Rate > 0 then varchar(Rate) else '' end, "
            + "   PayStartDate, PayEndDate "
            + "from LBPrem "
            + "where PolNo = '" + tPolNo + "'and payplancode like '000000%'";
  turnPage3.queryModal(sql, AddPremGrid);
}


/*********************************************************************
 *  查询免责信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function querySpecGrid()
{
  var sql = "select SpecCode, SpecContent, SpecStartDate, SpecEndDate "
            + "from LCSpec "
            + "where PolNo = '" + tPolNo + "' "
            + "union all "
            + "select SpecCode, SpecContent, SpecStartDate, SpecEndDate "
            + "from LBSpec "
            + "where PolNo = '" + tPolNo + "' ";
  turnPage4.queryModal(sql, SpecGrid);
}

 /*********************************************************************
 *  查询保额、档次
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function setAmntMult()
 {
	if (arrAllDataSet.length != 0  )
	 {		
		for (i=0;i<arrAllDataSet.length ;i++ )
		{
		    
			if (arrAllDataSet[i][3]  =='null')
			{
				arrAllDataSet[i][3] = '' ;
			}
			if (arrAllDataSet[i][4]  =='null')
			{
				arrAllDataSet[i][4] = '' ;
			}
		}
	 }
	
 }

/*********************************************************************
 *  查询受益人信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryBnf()
{
	
	// 初始化表格
	initBnfGrid();	
	//区分签单和撤保/退保
	var strTableName = "";
	if (tContType=='1')
	{
		strTableName = "LCBnf";
	}else if (tContType=='2')
	{  
		strTableName = "LBBnf";
	}

	//查询被保人
	var strSQL  = "select CodeName('bnftype', a.BnfType),a.Name,CodeName('idtype', a.IDType),a.IDNo,CodeName('relation', a.RelationToInsured),a.BnfLot,a.Bnfgrade "
		+ " from " + strTableName +  " a where a.ContNo='" + tContNo + "' and InsuredNo ='"+ tInsuredNo + "'"
		+ " and polno='" + tPolNo+ "'";
	//查询SQL，返回结果字符串
   turnPage1.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
 
  //判断是否查询成功
  if (!turnPage1.strQueryResult) {
  	BnfGrid.clearData();
    return false;
  }
 
  //查询成功则拆分字符串，返回二维数组
  turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
 
  //设置初始化过的MULTILINE对象
  turnPage1.pageDisplayGrid = BnfGrid;   
         
  //保存SQL语句
  turnPage1.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage1.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //setInvalidate(); 
  var arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);
  showCodeName();
}  

/*********************************************************************
 *  查询理赔信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPayGrid()
{
	// 初始化表格
	initPayGrid();	
	// 书写SQL语句RiskCode
	var strSQL = "";
	
	strSQL = "select a.RgtDate,a.EndCaseDate,c.AccDate,e.Pay"
	+ " from LLCase a,LLClaimDetail b,LLSubReport c,LLCaseRela d,LJAGetClaim e"
    + " where b.PolNo='" +tPolNo + "'"
    + " and a.CaseNo=b.CaseNo"
    + " and d.CaseNo = a.CaseNo"
	+ " and d.SubRptNo = c.SubRptNo"
	+ " and e.OtherNo = a.CaseNo"
	+ " and e.PolNo = b.PolNo";

  //查询SQL，返回结果字符串
   turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
  //判断是否查询成功
  if (!turnPage2.strQueryResult) {
  	PayGrid.clearData();
    return false;
  }
 //queryCase();
  //查询成功则拆分字符串，返回二维数组
  turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult);
  arrPayData = turnPage2.arrDataCacheSet


  //设置初始化过的MULTILINE对象
  turnPage2.pageDisplayGrid = PayGrid;   
         
  //保存SQL语句
  turnPage2.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage2.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //setInvalidate(); 
  var arrDataSet = turnPage2.getData(arrPayData, turnPage2.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage2.pageDisplayGrid);
} 

/*********************************************************************
 *  查询赔付金额、赔付率
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */  
function queryPay()
{
	var strSQL = "";
	var arrReturn =  new Array();
	var arrReturn1 = new Array();

	//赔付金额
	strSQL = "select sum(Pay) from LJAGetClaim where PolNo='" + tPolNo +"'";
	arrReturn = easyExecSql(strSQL);
	
	if (arrReturn == "null")
	{
	  try {fm.all('SumPay').value = "0"; } catch(ex) { }
		try {fm.all('PayLevel').value = 0; } catch(ex) { }
	}
	else
	{
		try { fm.all('SumPay').value = arrReturn[0][0]; } catch(ex) { };
        
		//保费

		//区分签单和撤保/退保
		var strTableName = "";
		if (tContType=='1')
		{
			strTableName = "LCPol";
		}else if (tContType=='2')
		{  
			strTableName = "LBPol";
		}

		strSQL = "select SumPrem from " + strTableName + " where PolNo='" + tPolNo +"'";
		arrReturn1 = easyExecSql(strSQL);
		if (arrReturn1 == "null")
		{
			alert("没有满足条件的保费");
		}else if(arrReturn1[0][0] != "null" && arrReturn[0][0] != 0)
		{
			try { fm.all('PayLevel').value = arrReturn1[0][0]/arrReturn[0][0]; } catch(ex) { };
			if (fm.all('PayLevel').value.length>6)
			{
				var payLev = fm.all('PayLevel').value;
				try { fm.all('PayLevel').value = payLev.substr(0,6); } catch(ex) { };
			}
		}
	}
	
}

/*********************************************************************
 *  返回上一页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */  
function returnparent()
{  
	  top.opener.focus();
		top.close();
}


/*********************************************************************
 *  设置险种风险值
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */  
function setPolValue()
{  
   var strSql = "select sum(prem) from lcprem where PolNo='"+tPolNo+"' and payplancode like '000000%%'";
	var arr = easyExecSql(strSql);
	var strSql_1 = "select sum(prem) from lcpol where PolNo='"+tPolNo+"'";
	var arr_1 = easyExecSql(strSql_1);
	var strSql_2 = "select SpecContent from LCSpec where PolNo='"+tPolNo+"'";
	var arr_2 = easyExecSql(strSql_2);
	if(arr!="null" && arr_1 !="null"){
		try { fm.all('AddPrem').value = arr[0][0]; } catch(ex) { };
	}
	if(arr_2 !="null"){
		try { fm.all('DutyDell').value = arr_2[0][0]; } catch(ex) { };
	}
}

//打印长期险种现金价值表
function printCashValue()
{
  if(RiskGrid.mulLineCount == 0)
  {
    alert("没有需要打印的险种信息");
    return false;
  }
  var riskCode = RiskGrid.getRowColData(0, 6);
  
  var sql = "select 1 from LMRiskApp "
            + "where RiskPeriod = 'L' "
            + "   and RiskCode = '" + riskCode + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.action = "PrintCashTableSave.jsp?PolNo=" + RiskGrid.getRowColData(0, 13)
        + "&RiskCode=" + riskCode;
  	fm.target = "_blank";
  	fm.submit();
  }
  else
  {
    alert("险种不是长期险，不能打印现金价值");
    return false;
  }
}

/*********************************************************************
 *  查询 初始保费 当前帐户余额
 *  参数  ：  无
 *  返回值：  
 *  zhanggm 071207
 *********************************************************************
 */  
 
function queryMoney()
{
  //查询初始保费
  var strSql = "  select sum(SumDuePayMoney) from LJAPayPerson "
             + "  where PolNo = '" + tPolNo + "'"
             + "      and PayCount = 1"
             + "      and (GetNoticeNo is null"
             + "      or GetNoticeNo = '')";
         
  var arrReturn = easyExecSql(strSql);
  if(arrReturn == "null" || arrReturn == "" || arrReturn == null)
  {
    fm.all('SumDuePayMoney').value = "0" ;
  }
  else
  {
    fm.all('SumDuePayMoney').value = arrReturn[0][0];
  }
  //查询当前帐户余额
  strSql = "  select sum(InsuAccBala) from LCInsureAcc"
         + "  where Polno = '" + tPolNo + "'";
  var arrReturn1 = easyExecSql(strSql);
  if(arrReturn1 == "null" || arrReturn1 == "" || arrReturn1 == null)
  {
    fm.all('InsuAccBala').value = "0" ;
  }
  else
  {
    fm.all('InsuAccBala').value = arrReturn1[0][0];
  }
}



