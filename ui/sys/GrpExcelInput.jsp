<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="java.util.*"%>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>

<script>
  var managecom = <%=tGI.ManageCom%>;
</script> 
	<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<%@include file="./GrpExcelInit.jsp"%>
		<SCRIPT src="./GrpExcelInput.js"></SCRIPT>

		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

	</head>
	<body onload="initForm();initElementtype();">
		<form action="" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class="titleImg">
						资料清单
					</td>
				</tr>
			</table>
			<br>
			<TABLE class="common">
				<tr class=common>
					<td class=title>
						管理机构
					</td>
					<td class=input colspan='3'>
						<Input class=codeNo name=ManageCom verify="管理机构|notnull"
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true elementtype="nacessary">
					</td>
				</tr>
				<tr class="common">
					<td class="title">
						签单日期起期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="SignStartDate" verify="签单日期起期|date" elementtype="nacessary">
					<td class="title">
						签单日期止期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="SignEndDate" verify="签单日期止期|date" elementtype="nacessary">
					</td>
				</tr>
				
			</TABLE><br>
			
			<input value="下   载" type=button onclick="Print()" class="cssButton"
				type="button">
			
			<input type="hidden" value="" name="downloadtype">
			<br><br>
			
			<font color="red">1.是否已分配保额仅适用于170501险种</font><br>
			<font color="red">2.档次仅适用于170501、162501和162401险种</font><br>
			<font color="red">3.管理费比例和利率仅适用于170301和170401险种</font><br>
			<input type="hidden" name="querySql1" value="">
			<input type="hidden" name="querySql2" value="">
			<input type="hidden" name="querySql3" value="">
		</form>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</body>
</html>
