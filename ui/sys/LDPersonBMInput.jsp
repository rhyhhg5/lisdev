<html> 
<%
//程序名称：LDPersonBMInput.jsp
//程序功能：
//创建日期：2002-08-18
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LDPersonBMInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LDPersonBMInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LDPersonBMSave.jsp" method=post name=fm target="fraSubmit">

	<table>
      <tr>
        <td width="80%">&nbsp;&nbsp;</td>
        <td><input  class=cssButton TYPE=button  value="&nbsp;批&nbsp;&nbsp;改&nbsp;" onclick="updateClick( )"></td>
        <td><input  class=cssButton TYPE=button  value="&nbsp;查&nbsp;&nbsp;询&nbsp;" onclick="queryClick( )"></td></tr>
    </table>

    <input type="radio" value="important" name="ra_important" onclick="trigger(this)">重要信息
    <input type="radio" checked value="noimportant" name="ra_important" onclick="trigger(this)">普通信息

    <table>
      <tr>
        <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson);"></td>
        <td class= titleImg>客户信息</td></tr>
    </table>

    <div  id= "divLDPerson" style= "display: ''">
      <table class="common">
        <tr class="common">
          <td class= title>客户号码</td>
          <td class="input"><Input class="common" name=CustomerNo ></td>
          
          <td class="title">密码</td>
          <td class="input"><Input class="common" name=Password ></td></tr>

        <tr class="common">
          <td class="title">客户姓名</td>
          <td class="input"><Input class="common" name=Name ></td>
          

         	<TD  class= title8>客户性别</TD>
					<TD  class= input8> <input class=codeno CodeData="0|8^0|男^1|女^2|其他 "  name=Sex ondblclick="return showCodeListEx('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Sex',[this,SexName],[0,1],null,null,null,1);"><input class=codename name=SexName></TD></tr>

        <tr class="common">
          <td class="title">客户出生日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="Birthday"></td>
          

           <TD  class= title> 籍贯</TD><TD  class= input>
           <Input class= "codeno"  name=NativePlace  ondblclick="return showCodeList('NativePlace',[this,NativePlacename],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('NativePlace',[this,NativePlacename],[0,1],null,null,null,1);" ><Input class=codename  name=NativePlacename></td>
 <tr class="common">
 	

      		<TD  class= title> 民族</TD><TD  class= input>
      		<Input class= "codeno"  name=Nationality  ondblclick="return showCodeList('Nationality',[this,Nationalityname],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Nationality',[this,Nationalityname],[0,1],null,null,null,1);" ><Input class=codename  name=Nationalityname></td>
					<TD  class= title> 婚姻状况</TD>
					<TD  class= input>
    		    <Input class= "codeno"  name=Marriage  ondblclick="return showCodeList('marriage',[this,marriagename],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('marriage',[this,marriagename],[0,1],null,null,null,1);" ><Input class=codename  name=marriagename></td>
</tr>

	

        <tr class="common">
          <td class="title">结婚日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="MarriageDate" ></td>

					<TD  class= title> 职业类别</TD>
					<TD  class= input>
    		    <Input class= "codeno"  name=OccupationType  ondblclick="return showCodeList('OccupationType',[this,OccupationTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('OccupationType',[this,OccupationTypeName],[0,1],null,null,null,1);" ><Input class=codename  name=OccupationTypeName></td>
</tr>


        <tr class="common">
          <td class="title">开始工作日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="StartWorkDate" ></td>
          
          <td class="title">工资</td>
          <td class="input"><Input class="common" name=Salary ></td></tr>
          
        <tr class="common">
					<TD  class= title> 健康状况</TD>
					<TD  class= input>
    		    <Input class= "codeno"  name=Health  ondblclick="return showCodeList('Health',[this,HealthName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Health',[this,HealthName],[0,1],null,null,null,1);" ><Input class=codename  name=HealthName></td>
    
          <td class="title">身高</td>
          <td class="input"><Input class="common" name=Stature ></td></tr>

        <tr class="common">
          <td class="title">体重</td>
          <td class="input"><Input class="common" name=Avoirdupois ></td>
          
       		<TD  class= title> 信用等级</TD>
					<TD  class= input>
    		  <Input class= "codeno"  name=CreditGrade  ondblclick="return showCodeList('creditlevel',[this,CreditGradeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('creditlevel',[this,CreditGradeName],[0,1],null,null,null,1);" ><Input class=codename  name=CreditGradeName></td>
</tr>  
        <tr class="common">
      		 <TD  class= title> 证件类型</TD>
					<TD  class= input>
    		  <Input class= "codeno"  name=IDType  ondblclick="return showCodeList('IDType',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1],null,null,null,1);" ><Input class=codename  name=IDTypeName></td>

          <td class="title">属性</td>
          <td class="input"><Input class="common" name=Proterty ></td></tr>

        <tr class="common">
          <td class="title">证件号码</td>
          <td class="input"><Input class="common" name=IDNo ></td>
          
          <td class="title">其它证件类型</td>
          <td class="input"><Input class="common" name=OthIDType ></td></tr>
          
        <tr class="common">
          <td class="title">其它证件号码</td>
          <td class="input"><Input class="common" name=OthIDNo ></td>
          
          <td class="title">ic卡号</td>
          <td class="input"><Input class="common" name=ICNo ></td></tr>
          
        <tr class="common">
          <td class="title">家庭地址编码</td>
          <td class="input"><Input class="common" name=HomeAddressCode ></td>
          
          <td class="title">家庭地址</td>
          <td class="input"><Input class="common" name=HomeAddress ></td></tr>
          
        <tr class="common">
          <td class="title">通讯地址</td>
          <td class="input"><Input class="common" name=PostalAddress ></td>
          
          <td class="title">邮政编码</td>
          <td class="input"><Input class="common" name=ZipCode ></td></tr>
          
        <tr class="common">
          <td class="title">电话</td>
          <td class="input"><Input class="common" name=Phone ></td>
          
          <td class="title">传呼</td>
          <td class="input"><Input class="common" name=BP ></td></tr>
          
        <tr class="common">
          <td class="title">手机</td>
          <td class="input"><Input class="common" name=Mobile ></td>
          
          <td class="title">e_mail</td>
          <td class="input"><Input class="common" name=EMail ></td></tr>
          
        <tr class="common">
          <td class="title">银行编码</td>
          <td class="input"><Input class="common" name=BankCode ></td>
          
          <td class="title">银行账号</td>
          <td class="input"><Input class="common" name=BankAccNo ></td></tr>
          
        <tr class="common">
          <td class="title">入司日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="JoinCompanyDate" ></td>
          
          <td class="title">职位</td>
          <td class="input"><Input class="common" name=Position ></td></tr>
          
        <tr class="common">
          <td class="title">单位号码</td>
          <td class="input"><Input class="common" name=GrpNo ></td>
          
          <td class="title">单位名称</td>
          <td class="input"><Input class="common" name=GrpName ></td></tr>
          
        <tr class="common">
          <td class="title">单位电话</td>
          <td class="input"><Input class="common" name=GrpPhone ></td>
          
          <td class="title">单位地址编码</td>
          <td class="input"><Input class="common" name=GrpAddressCode ></td></tr>

        <tr class="common">
          <td class="title">单位地址</td>
          <td class="input"><Input class="common" name=GrpAddress ></td>
          
          <td class="title">死亡日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="DeathDate" ></td></tr>
          
        <tr class="common">
          <td class="title">备注</td>
          <td class="input"><Input class="common" name=Remark ></td>
          
         	<TD  class= title8>状态</TD>
				<TD  class= input8> <input class=codeno CodeData="0|8^0|在职^1|退休" name=State ondblclick="return showCodeListEx('State',[this,StateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('State',[this,StateName],[0,1],null,null,null,1);"><input class=codename name=StateName></TD>
</tr>
        <tr class="common">
          <td class="title">黑名单标记</td>
          <td class="input"><Input class="common" name=BlacklistFlag ></td>
          
          <td class="title">操作员代码</td>
          <td class="input"><Input class="common" name=Operator ></td></tr>
          
        <tr class="common">
          <td><input type=hidden name=Transact ></td></tr>
          
      </table>
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>
