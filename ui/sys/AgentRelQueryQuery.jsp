<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/AgentCheck.jsp"%>
<%
     //添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}	
	String strOperator = globalInput.Operator;
%> 
<html>
<head>
<title>客户信息查询</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<script src="./RelQueryQuery.js"></script> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LDPersonQueryInit.jsp"%>


</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm target=fraSubmit method=post>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>  客户号码： </TD>
          <TD  class= input> <Input class= common name=CustomerNo >  </TD>
          <TD  class= title>    姓名： </TD>
          <TD  class= input> <Input class= common name=Name >  </TD>
         <TD  class= title> 出生日期：</TD>
          <TD  class= input>  <Input class="coolDatePicker" dateFormat="short" name=Birthday >             </TD>
          </TR>             
      	<TR  class= common>
      	  <TD  class= title> 性别  </TD>
          <TD  class= input> <Input name=Sex class="code" MAXLENGTH=1 ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);" > </TD>       
          <TD  class= title> 证件类型： </TD>
          <TD  class= input><Input class="code" name=IDType ondblclick="return showCodeList('IDType',[this]);">             </TD>
          <TD  class= title> 证件号码： </TD>
          <TD  class= input> <Input class= common name=IDNo > </TD>
       </TR>   
   </Table>  
   <table> 
    <tr>		
    <td> <INPUT class=common VALUE="查  询" TYPE=button onclick="easyQueryClick();">  </td>
    <td>  <INPUT class=common VALUE="客户信息" TYPE=button onclick="returnParent();">  </td>
    <td><INPUT class=common VALUE="投保单信息" TYPE=button onclick="ProposalClick();"> </td>
    <td>  <INPUT class=common VALUE="保单信息" TYPE=button onclick="PolClick();"> </td>
    <td>  <INPUT class=common VALUE="销户保单信息" TYPE=button onclick="DesPolClick();"> </td>
    </tr> 
   </table> 
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    		</TD>
    		<TD class= titleImg>
    			 客户信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divLDPerson1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanPersonGrid" ></span> 
  	</TD>
      </TR>
    </Table>
   <table> 
    <tr>		
    <td>			
      <INPUT class=common VALUE="首页" TYPE=button onclick="getFirstPage();"> 
    </td>
    <td>
      <INPUT class=common VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
    </td>
    <td>  
      <INPUT class=common VALUE="下一页" TYPE=button onclick="getNextPage();"> 
    </td>
    <td>  
      <INPUT class=common VALUE="尾页" TYPE=button onclick="getLastPage();"> 
    </td>
    </tr> 
   </table> 
 
 </Div>				
   <input type=hidden name=aAgentCode value=<%=strOperator%>>	
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
</html>
