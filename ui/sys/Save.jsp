<%@ page language="java" contentType="text/html; charset=GBK" %>
<%@page import="com.sinosoft.lis.llcase.LLCaseCommon"%>
<%@page import="com.sinosoft.utility.*"%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>

<html>

<%
ExeSQL exeSQL = new ExeSQL();
//String polno = request.getParameter("PolGrid13"); //polno

//String riskcode = request.getParameter("PolGrid4"); //riskcode
String startDate = request.getParameter("seStart"); //当日日期
//String grpcontno = request.getParameter("h_grpcontno"); //
String insuredno = request.getParameter("h_insuredno"); // 客户编码
String grpcontno = request.getParameter("h_grpcontno"); //单号
//System.out.println("polno:"+polno);
System.out.println("startDate:"+startDate);
System.out.println("insuredno:"+insuredno);
System.out.println("grpcontno:"+grpcontno);
String message=" select a.grpcontno,a.contno,a.polno,a.riskcode "
	 		+ " from LCPol a, lmrisk  "
	 		+ " where a.InsuredNo = '"+insuredno+"' "
	 		+ " and a.grpcontno='"+grpcontno+"'  "
	 		+ " and a.riskcode=lmrisk.riskcode " ;

SSRS ssrs= new SSRS();
ssrs=exeSQL.execSQL(message) ;
if(ssrs!=null && ssrs.getMaxRow()>0){ //说明不为空
	
	String risk=" select distinct a.riskcode "
 		+ " from LCPol a, lmrisk  "
 		+ " where a.InsuredNo = '"+insuredno+"' "
 		+ " and a.grpcontno='"+grpcontno+"'  "
 		+ " and a.riskcode=lmrisk.riskcode " ;
		SSRS ssrs1= new SSRS();
		ssrs1=exeSQL.execSQL(risk) ;
		if("".equals(ssrs1) || ssrs1==null){
			%>
			<script type="text/javascript">
			 alert("此客户,没有查询到险种信息");
	</script>
	<% 
			return;
		}else{
			//不为空空，判断险种编码是否是A
			if(ssrs1.getMaxRow()>1){
				%>
				<script type="text/javascript">
					 alert("仅支持查询管A、管B、补A三种产品中的一种");
			</script>
				<%
				 return ;
			}else{
				String riskcode=ssrs1.GetText(1,1);
				if("170501".equals(riskcode) || "162401".equals(riskcode) ||"162501".equals(riskcode)){
					//险种正确
					//新增加，如果责任为理赔疾病身故保险金后，查询到的保额we
					String se1=" select distinct 1  from llclaimdetail a , LcPol b ,llcase c  where 1 =1 "
						+ " and a.contno=b.contno "
						+ " and a.grpcontno=b.grpcontno "
						+ " and a.polno=b.polno "
						+ " and a.riskcode=b.riskcode "
						+ " and a.caseno=c.caseno "
						+ " and c.rgtstate in ('11','12') "
						+ " and a.getdutycode in ('741204','740204','701204') "
						+ " and b.insuredno='"+insuredno+"'  " 
				 		+ " and b.grpcontno='"+grpcontno+"'  "
						+ " union  "
						+ " select distinct 1  from llclaimdetail a , LbPol b ,llcase c  where 1 =1 "
						+ " and a.contno=b.contno "
						+ " and a.grpcontno=b.grpcontno "
						+ " and a.polno=b.polno "
						+ " and a.riskcode=b.riskcode "
						+ " and a.caseno=c.caseno "
						+ " and c.rgtstate in ('11','12') "
						+ " and a.getdutycode in ('741204','740204','701204') "
						+ " and b.insuredno='"+insuredno+"'  " 
				 		+ " and b.grpcontno='"+grpcontno+"'  "
						
						;
					String se1val=exeSQL.getOneValue(se1) ;
					
					if("1".equals(se1val) || se1val=="1"){
						
						%>
						<script type="text/javascript">
						alert("该客户的保额为( 0 )");
					</script>
						<%
						 return ;
					}
				}else {
					%>
					<script type="text/javascript">
					 alert("仅支持查询管A、管B、补A三种产品中的一种");
				</script>
					<%
					 return ;
				}
				System.out.println("险种编码："+ssrs1.GetText(1,1));
				//得到险种符合A 并且保障信息不为空，开始循环
				LLCaseCommon llcc = new LLCaseCommon();
				double money=0;
				Boolean bool=true;
					for (int i = 1; i <= ssrs.getMaxRow(); i++) {
				String Tgrpcontno=ssrs.GetText(i,1);
				String Tcontno=ssrs.GetText(i,2);
				String Tpolno=ssrs.GetText(i,3);
				String Triskcode=ssrs.GetText(i,4);
				//111111111111111
				  Double Tmoney=llcc.getAmntFromAcc(insuredno,Tpolno,startDate,"CB") ;
				  money=money+Tmoney;
				  System.out.println("客户:"+insuredno+",polno:"+Tpolno+",最早日期:"+startDate+"的保额为："+Tmoney+",累加后："+money+"。");
			}
					double amnt=0;
					if(bool){
						amnt=money;
				        amnt = Arith.round(amnt, 2);
				        
						%>
						<script type="text/javascript">
						 alert("该客户的保额为( "+<%=amnt%>+" )");
						</script>
					<%
					}
					
			}
		}
}else{
	%>
	<script type="text/javascript">
		 alert("未查询到任何保障信息");
</script>
	<%
	 return ;
	
}
	%>
	 
  
</html>