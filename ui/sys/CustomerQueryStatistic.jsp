<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：CustomerQueryStatistic
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>

<%
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>	        //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<%@include file="CustomerQueryStatisticInit.jsp"%>
<SCRIPT src="CustomerQueryStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>

<body  onload="initForm();initElementtype();" >
  <form  action="./CustomerQueryStatisticSub.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input><Input class="codeno" name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class = codename name =ManageComName elementtype=nacessary ></TD>  
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class=codeNo name=AppntSex   ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true >    
          </TD>
          <TD  class= title>
            职业代码
          </TD>
          <TD  class= input>
            <Input class="codeno" name="AppntOccupationCode"   ondblclick="return showCodeList('OccupationCode',[this,AppntOccupationCodeName],[0,1],null,null,null,null,300);" onkeyup="return showCodeListKey('OccupationCode',[this,AppntOccupationCodeName],[0,1],null,null,null,null,300);"onfocus="getdetailwork();"><input class = codename name =AppntOccupationCodeName >
          </TD> 
        </TR>
        <TR  class= common>
     <TD  class= title>
     客户身份
    </TD>
    <TD  class= input>
      <Input  class=codeno name=QYType  verify="|len<=20" CodeData= "0|^投保人|1^被保人|2" ondblClick= "showCodeListEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);" onkeyup= "showCodeListKeyEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);"><Input class= 'codename' name=QYType_ch elementtype=nacessary >
    </TD>
     <TD  class= title>
          客户生日(MM-DD)
     </TD>
    <TD  class= input  >
   <Input class= common name="Birthday" verify="客户生日|len<=5" >
   </TD>
 <TD  class= title>
         销售渠道
     </TD>
    <TD  class= input  >
   <Input class= common name="SaleChnl"  >
   </TD>
   </TR>
  <TR  class= common> 
          <TD  class= title>
            保单生效日期起始
          </TD>          
          <TD  class= input>
          <Input class="coolDatePicker"  name=CValiDateStart >
          </TD>        
          <TD  class= title>
            保单生效日期终止
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" name=CValiDateEnd>
          </TD>          
      </TR>
    </table>

    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>