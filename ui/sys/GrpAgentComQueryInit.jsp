<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>

<%
 		GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Crs_SaleChnl =  request.getParameter("Crs_SaleChnl");
  	String Crs_SaleChnlName =  request.getParameter("Crs_SaleChnlName");
  	
 %>

<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  { 
    var Crs_SaleChnl='<%=Crs_SaleChnl%>';
    if(Crs_SaleChnl != null && Crs_SaleChnl !="")
    {
    fm.all('Crs_SaleChnl').value = Crs_SaleChnl;
    }
   
    //fm.all('EntryNo').value = ''; 
  }
  catch(ex)
  {
    alert("在AgentQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AgentQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initGrpAgentComGrid();
    showAllCodeName();
  }
  catch(re)
  {
    alert("AgentQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化GrpAgentGrid
 ************************************************************
 */
function initGrpAgentComGrid()
  {                             
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="对方机构代码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="对方机构名称";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
/*
        iArray[3]=new Array();
        iArray[3][0]="销售渠道名称";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="姓名";         //列名
        iArray[4][1]="60px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="性别";         //列名
        iArray[5][1]="30px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="身份证号";         //列名
        iArray[6][1]="140px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="状态";         //列名
        iArray[7][1]="40px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
      */  
       

  
        GrpAgentComGrid = new MulLineEnter( "fm" , "GrpAgentComGrid" ); 

        //这些属性必须在loadMulLine前
        GrpAgentComGrid.mulLineCount = 10;   
        GrpAgentComGrid.displayTitle = 1;
        GrpAgentComGrid.canSel=1;
//        AgentGrid.canChk=0;
        GrpAgentComGrid.locked=1;
	    GrpAgentComGrid.hiddenPlus=1;
	    GrpAgentComGrid.hiddenSubtraction=1;
        GrpAgentComGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化GrpAgentComGrid时出错："+ ex);
      }
    }


</script>