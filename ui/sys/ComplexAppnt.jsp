<DIV id=DivLCAppntIndButton STYLE="display:''">
<!-- 投保人信息部分 -->
  <table>
	<tr>
	  <td>
	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
	  </td>
	  <td class= titleImg>
	  投保人基本资料:
	  </td>
	</tr>
  </table>
</DIV>
<DIV id=DivLCAppntInd STYLE="display:''">
 <table  class= common>
	<TR  class= common>
	  <TD  class= title>
		客户号
	  </TD>
	  <TD  class= input>
		<Input class= common name=AppntNo readonly >
	  </TD>
	  <TD  class= title>
		姓名
	  </TD>
	  <TD  class= input>
		<Input class= common name=ClietName elementtype=nacessary verify="投保人姓名|notnull&len<=20" readonly=true>
	  </TD>
	  <TD  class= title>
		性别
	  </TD>
	  <TD  class= input>
		<Input class="code" name=AppntSex elementtype=nacessary verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
	  </TD>
	  <TD  class= title>
		婚姻
	  </TD>
	  <TD  class= input>
		<Input class="code" name="AppntMarriage" elementtype=nacessary verify="投保人婚姻状况|notnull&code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);">
	  </TD> 
	</TR>
	<TR  class= common>  
	  <TD  class= title>
		出生日期
	  </TD>
	  <TD  class= input>
	  <input class="common" dateFormat="short" elementtype=nacessary name="AppntBirthday" readonly >
	  </TD>     
	  <TD  class= title>
		职业
	  </TD>
	  <TD  class= input>
		  <Input class="common" name="AppntOccupationName"  TABINDEX="-1" readonly >
	  </TD>           
	  <TD  class= title>
		职业代码
	  </TD>
	  <TD  class= input>
		  <Input class="common" name="AppntOccupationCode" readonly>
	  </TD> 
	  <TD  class= title>
		国籍
	  </TD>
	  <TD  class= input>
	  <input class="code" name="AppntNativePlace" verify="投保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">
	  </TD>
	  <TD  class= input>
	  <input class="common" style="display: none"  name="NativePlace" >
	  </TD>  
	</TR>                                       
	<TR  class= common>
	  <TD  class= title>
		证件类型
	  </TD>
	  <TD  class= input>
		<Input class="code" name="AppntIDType" value="0" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
	  </TD>
	  <TD  class= title>
		证件号码
	  </TD>
	  <TD  class= input colspan=1>
		<Input class= common name="AppntIDNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" TABINDEX="-1" readonly>
	  </TD>
	  <TD  class= title>
		证件生效日期
	  </TD>
	  <TD  class= input>
		<input class="common" dateFormat="short" elementtype=nacessary name="AppIDStartDate" readonly >
	  </TD>
	  <TD  class= title>
		证件失效日期
	  </TD>
	  <TD  class= input>
		<input class="common" dateFormat="short" elementtype=nacessary name="AppIDEndDate" readonly >
	  </TD>        
	</TR>       
	<TR  class= common>
	  <TD  class= title>
		工作单位
	  </TD>
	  <TD  class= input  >
		<Input class= common name="AppntGrpName" verify="投保人工作单位|len<=60"TABINDEX="-1" readonly >
	  </TD>
	  <TD  class= title>
		办公电话
	  </TD>
	  <TD  class= input>
		<Input class= common name="AppntGrpPhone" verify="投保人单位电话|NUM&len<=18"TABINDEX="-1" readonly >
	  </TD>
	  <TD  class= title>
      	岗位职务
      </TD>
      <TD  class= input>
      	<Input class= 'common' name=AppntPosition >
      </TD>  
		 <TD  class= title>
            		授权使用客户信息
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntAuth" readonly=true  verify="授权使用客户信息|notnull&code:Auth" ondblclick=" return false ; return showCodeList('AUTH',[this,AppntAuthName],[0,1]);" onkeyup="return showCodeListKey('Auth',[this,AppntAuthName],[0,1]);"><input class=codename name=AppntAuthName readonly=true elementtype=nacessary>    
          </TD>   
	</TR>
	<TR>
  	      <TD  class= title8>
           个税征收方式
          </TD>
          <TD  class= input8>
      			<input NAME="TaxPayerType" CLASS=codeNo ondblclick="return showCodeList('taxpayertype',[this,TaxPayerTypeName],[0,1],null,'1','1',1);" onkeyup="return showCodeListKey('taxpayertype',[this,TaxPayerTypeName],[0,1],null,'1','1',1);" verify="个税征收方式|code:taxpayertype"><input class=codename name=TaxPayerTypeName readonly=true >
         </TD>
  	      <TD  class= title8>
            个人税务登记证代码
          </TD>
          <TD  class= input8>
      			<Input NAME=TaxNo VALUE="" CLASS=common >
         </TD>
         <TD  class= title8>
            个人社会信用代码
          </TD>
          <TD  class= input8>
      			<Input NAME=CreditCode VALUE="" CLASS=common >
         </TD> 
  	      </TR>   
  	      <TR > 
  	      <TD  class= title8>
            团体编号
          </TD>
          <TD  class= input8>
      		 <input NAME="GrpNo" VALUE MAXLENGTH="0" CLASS="code" ondblclick="return queryGrpTax();" onkeyup="return queryGrpTax1();">
         </TD>         
          <TD  class= title8>
            单位税务登记证代码
          </TD>
          <TD  class= input8>
      			<Input NAME=GTaxNo VALUE=""  readonly CLASS=common >
      			<Input NAME=BatchNo VALUE=""  readonly CLASS=common type=hidden>
      			<Input NAME=InsuredId VALUE=""  readonly CLASS=common type=hidden>
         </TD>
         <TD  class= title8>
            单位社会信用代码
          </TD>
          <TD  class= input8>
      			<Input NAME=GOrgancomCode VALUE=""  readonly CLASS=common >
         </TD>
         </TR>                          
	<TR  class= common>
	  <TD  class= title>
		单位地址
	  </TD>
	  <TD  class= input colspan=3 >
		<Input class= common3 name="CompanyAddress" verify="投保人单位地址|len<=80" TABINDEX="-1" readonly>
	  </TD> 
	  <TD  class= title>
		邮编
	  </TD>
	  <TD  class= input>
		<Input class= common name="AppntGrpZipCode" verify="投保人单位邮政编码|zipcode"TABINDEX="-1" readonly >
	  </TD>                   
	</TR> 
	<TR  class= common>
	  <TD  class= title>
		家庭地址
	  </TD>
	  <TD  class= input colspan=3 >
		<Input class= common3 name="AppntHomeAddress" verify="投保人家庭地址|len<=80" TABINDEX="-1" readonly>
	  </TD>
	  <TD  class= title>
		邮编
	  </TD>
	  <TD  class= input>
		<Input class= common name="AppntHomeZipCode" verify="投保人家庭邮政编码|zipcode" TABINDEX="-1" readonly >
	  </TD>
	</TR>     
	<TR  class= common>
	  <TD  class= title>
		联系地址
	  </TD>
	  <TD  class= input colspan=3 >
		<Input class= common3 name="AppntPostalAddress" verify="投保人联系地址|len<=80" TABINDEX="-1" readonly>
	  </TD>
	  <TD  class= title>
		邮编
	  </TD>
	  <TD  class= input>
		<Input class= common name="AppntPostalZipCode" verify="投保人联系邮编|zipcode" TABINDEX="-1" readonly >
	  </TD>
	</TR>       
	
	<TR  class= common>
	  <TD  class= title>
		住宅电话
	  </TD>
	  <TD  class= input>
	  <input class= common name="AppntHomePhone" verify="投保人家庭电话|NUM&len<=18" TABINDEX="-1" readonly >
	  </TD>                
	  <TD  class= title>
		移动电话
	  </TD>
	  <TD  class= input>
		<Input class= common name="AppntMobile" verify="投保人移动电话|len<=15" TABINDEX="-1" readonly >
	  </TD> 
	  <TD  class= title>
		联系电话
	  </TD>
	  <TD  class= input>
		<Input class= common name="AppntPhone" verify="投保人联系电话|len<=15" TABINDEX="-1" readonly >
	  </TD> 
	  <TD  class= title>
		E-mail
	  </TD>
	  <TD  class= input>
		<Input class= common name="AppntEMail" verify="投保人电子邮箱|Email" TABINDEX="-1" readonly >
	  </TD>                    
	</TR>  
	<TR  class= common style="display:none">
	  <TD  class= title>
		帐户余额
	  </TD>
	  <TD  class= input >
	  	<input class= common name="AccBala" readonly >
	  </TD>
	  <TD  class= input  colspan="4">
	  	<input class=cssButton type="button" value="余额记录" onclick="printAppAccTraceList();">
	  </TD>
  </TR>
  <TR  class= common>                   
          <TD  class= title>
             折扣方式
          </TD>
          <TD  class= input>
			<Input class="codeno" name=DiscountMode  ondblclick="return showCodeList('DiscountMode',[this,DiscountMode_Name],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('DiscountMode',[this,DiscountMode_Name],[0,1],null,'0','Code1',1);"  readonly=true ><input class=codename name=DiscountMode_Name  readonly=true  >
          </TD>                    
          <TD  class= title id=AppointDiscountID style= "display: 'none'">
            约定折扣
          </TD>
          <TD  class= input id=AppointDiscountID2 style= "display: 'none'">
      			<Input NAME=AppointDiscount VALUE=""  type  CLASS=common onblur="dealDiscount();">
          </TD> 
        
        </TR>   
            <TR  class= common>
              <TD  class= title8>
            年龄折扣因子
          </TD>
          <TD  class= input>
      			<Input NAME=Agediscountfactor VALUE=""  readonly CLASS=common >
         </TD>
                   <TD  class= title8>
            补充医疗保险折扣因子
          </TD>
          <TD  class= input>
      			<Input NAME=Supdiscountfactor VALUE=""   CLASS=common >
         </TD>
                            <TD  class= title8>
           团体投保人数折扣因子
          </TD>
          <TD  class= input>
      			<Input NAME=Grpdiscountfactor VALUE=""   CLASS=common >
         </TD>
                            <TD  class= title8>
            税优总折扣因子
          </TD>
          <TD  class= input>
      			<Input NAME=Totaldiscountfactor VALUE=""  readonly CLASS=common >
         </TD>
                 </TR>  
                  <tr>
            <TD  class= title>
            被保险人风险保险费档次
          	</TD>
          	<TD  class= input>
      		 <input NAME="PremMult" CLASS=codeNo CodeData="0|^1|未参加补充医疗保险^2|已参加补充医疗保险" ondblclick="return showCodeListEx('PremMult',[this,PremMultName],[0,1]);" onkeyup="return showCodeListKeyEx('PremMult',[this,PremMultName],[0,1]);"><input class=codename name=PremMultName readonly=true >
          	</TD>
           </tr>
  </table>   
</DIV>
<DIV id=divFeeButton STYLE="display:''">
<table>
<tr>
<td class= titleImg>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFee);">
缴费资料
</td>
</tr>
</table>
</DIV>        
<Div  id= "divFee" style= "display: ''">  
  <table  class= common>
    <TR CLASS=common>
	    <TD CLASS=title  >
	      缴费方式 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PayMode VALUE="" CLASS="code" CodeData="0|^4|银行转账 ^1|自缴" MAXLENGTH=20  ondblclick="return showCodeListEx('PayMode',[this]);" onkeyup="return showCodeListKeyEx('PayMode',[this]);" >
	    </TD>	
	    <TD CLASS=title  >
	      开户银行 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=BankCode VALUE="" CLASS="code" MAXLENGTH=20 verify="开户行|code:bank" ondblclick="return showCodeList('bank',[this]);" onkeyup="return showCodeListKey('bank',[this]);" >
	    </TD>	   
		 <TD CLASS=title >
	      户名
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20 TABINDEX="-1" readonly  >
	    </TD>
      <TD CLASS=title width="109" >帐号</TD>	
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=BankAccNo VALUE="" CLASS=common TABINDEX="-1" readonly >
	    </TD>
	  </TR>              
    <TR CLASS=common>
	    <TD CLASS=title id ='SysQuery1'  style="display:''">
	      缴费频次 
	    </TD>
	    <TD CLASS=input COLSPAN=1 id ='SysQuery2' style="display:''" >
	      <Input NAME=PayIntv1 VALUE="" class=code8 name=PayIntv  codeData="0|^1|月缴 ^3|季缴 ^6|半年缴 ^12|年缴"   verify="缴费频次|notnull&code:PayIntv" ondblclick="return showCodeList('PayIntv',[this]);" onkeyup="return showCodeListKey('PayIntv',[this]);" >
	    </TD>	
		  <TD CLASS=title  >
	      期交保费 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=Prem VALUE="" CLASS=common>
	    </TD>		
		  <TD CLASS=title  >
	      缴至日期 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PaytoDate VALUE="" CLASS=common>
	    </TD>		
	  </TR>
	   <TR CLASS=common style="display: none;">
	<font color="#ff0000">保单&ldquo;未整单&rdquo;退保时，&ldquo;期交保费&rdquo;为所有被保人的当期保费合计，不含已经退保部分险种的保费;保单&ldquo;整单&rdquo;退保时，&ldquo;期交保费&rdquo;为保单所有已退保险种的当期保费合计。</font>
	    </TD>		
	  </TR>
   </table>   
</DIV>    	  
