<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：TianJinGrpInput.jsp
//程序功能：天津社保补充业务数据采集统计分析
//创建时间：2017-12-4
//创建人  ：yangjian
%>


<head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="TianJinGrpInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="TianJinGrpInit.jsp"%> 
</head>

<body  onload="initForm();initElementtype();TianJinGrpQuery();" >    
  <form action=""  method=post name=fm target="fraSubmit">
	 <Div  id= "divAgent1" style= "display: ''">
	 	<h3>天津社保补充业务数据采集统计分析</h3>
	 </Div>
	
     <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentQuery);">
    		</td>
    		<td class= titleImg>数据采集统计结果</td>   		 
    	</tr>
   	 </table>
   	 <Div  id= "divAgentQuery" style= "display: ''">
     <Table  class= common>
       	<TR  class= common>
        	<TD text-align: left colSpan=1>
            <span id="spanTianJinGrpGrid" ></span> 
  			</TD>
     	</TR>
     </Table>	
     <Table>
		<TR  class= common>  
		  <TD class=common>   					
	      	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	          </TD>
		  <TD class=common>
	      		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
	          </TD>
		  <TD class=common>					
	      		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	          </TD>
		  <TD class=common>
	      		<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	          </TD>
		</TR>
     </Table>
     
     
	 <table>
  	  <tr>      
     	<td>
     		<!-- <input class="cssfile" name="FileName" type="file" /> -->
     		<!-- <input type=button value="文 件 查 询" class=cssButton onload="TianJinGrpQuery();"> -->
     		<br>
     		<input type=button value="文 件 下 载" class=cssButton onclick="TianJinGrpDownload();">

    	</td>
      </tr>      
   	 </table> 
  	 </Div>
   
   
  	 <input type=hidden class=Common name=querySql > 
   	
  </form>   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
<script>
  var mcodeSql = "#1# and (code = #2# or code = #3#)";
</script>

