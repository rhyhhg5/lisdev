<html> 
<%
  //程序名称：BlackListBatchInput.jsp
  //程序功能：黑名单批量导入
  //创建日期：2014-01-07 00:00:00
  //创建人  ：Houyd
  //更新记录：  更新人    更新日期     更新原因/内容
  %>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<%
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="BlackListBatchInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BlackListBatchInit.jsp"%>
  
  <%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
 
  <title>黑名单批量导入</title>   
</head>
<body  onload="initForm();initElementtype();" >
	<!--initDate();-->
  
 <form action="./BlackListBatchSave.jsp" method=post name=fm target="fraSubmit"> 
    <table class=common border=0 width=100%>
     <tr>
      <td class=titleImg align=center>请输入查询信息：</td>
     </tr>
    </table>    
    
    <table class=common align=center>
     <TR  class=common>
      <TD  class=title>
          黑名单组织姓名
      </TD>
      <TD  class=input>
       <Input class=common name=BlackGroupName onkeydown="QueryOnKeyDown()">
      </TD>       
      <TD  class=title>
          黑名单人员姓名
      </TD>
      <TD  class=input>
       <Input class=common  name=BlackPersonName  onkeydown="QueryOnKeyDown()">
      </TD>
     </TR>  
    </table>
    <iframe name="UploadiFrame" src="UpLoadInput.jsp" frameborder="0" scrolling="no" height="40px" width="100%"></iframe>
    <table>
     <tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssess1);">
      </td>
      <td class=titleImg>
          查询信息
      </td>
     </tr>
    </table>
    <Div  id="divLAAssess1" style= "display:''">
    <table  class=common>
     <tr  class=common>
      <td text-align: left colSpan=1>
      <span id="spanEvaluateGrid" >
      </span> 
      </td>
     </tr>
    </table>
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton >                  
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
    </div>
    <input type=hidden id="fmtransact" name="fmtransact"> 
    
  </form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
