<html>
<%
//程序名称：ClientConjoinQueryInput.jsp
//程序功能：
//创建日期：2002-08-19
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="ClientConjoinQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ClientConjoinQueryInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="CollectivityClientSave.jsp" method=post name=fm target="fraSubmit">

	<table>
      <tr>
        <td width="100%">&nbsp;&nbsp;</td>
        <td><input type="button" value="&nbsp;查询客户&nbsp;" onclick="queryClientClick( )"></td></tr>
    </table>

    <table>
      <tr>
        <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson);"></td>
        <td class= titleImg>客户信息</td></tr>
    </table>

    <div  id= "divLDPerson" style= "display: ''">
      <table class="common">
        <tr class="common">
          <td class="title">客户姓名</td>
          <td class="input"><Input class="common" name=Name ></td>
          
          <td class="title">客户性别</td>
          <td class="input"><Input class="code" name=Sex ondblclick="showCodeList('Sex',this)"></td></tr>

        <tr class="common">
          <td class="title">客户出生日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="Birthday"></td>
          
          <td class="title">婚姻状况</td>
          <td class="input"><Input class="code" name=Marriage ondblclick="return showCodeList('Marriage',[this]);"></td></tr>

        <tr class="common">
          <td class="title">职业类别</td>
          <td class="input"><Input class="code" name=OccupationType ondblclick="return showCodeList('OccupationType',[this]);"></td>
          
          <td class="title">健康状况</td>
          <td class="input"><Input class="code" name=Health ondblclick="return showCodeList('Health',[this]);"></td></tr>

        <tr class="common"><td cols=4><br></br></td></tr>
        
        <tr class="common">
          <td class="title">证件类型</td>
          <td class="input"><Input class="code" name=IDType ondblclick="return showCodeList('IDType',[this]);"></td>
          
          <td class="title">属性</td>
          <td class="input"><Input class="code" name=Proterty ondblclick="return showCodeList('Proterty',[this]);"></td></tr>

        <tr class="common">
          <td class="title">证件号码</td>
          <td class="input"><Input class="common" name=IDNo ></td>
          
          <td class="title">ic卡号</td>
          <td class="input"><Input class="common" name=ICNo ></td></tr>

        <tr class="common">
          <td class="title">其它证件号码</td>
          <td class="input"><Input class="common" name=OthIDNo ></td>
          
          <td class="title">其它证件类型</td>
          <td class="input"><Input class="code" name=OthIDType ondblclick="return showCodeList('OthIDType',[this]);"></td></tr>
          
        <tr class="common"><td cols=4><br></br></td></tr>
          
        <tr class="common">
          <td class="title">单位名称</td>
          <td class="input"><Input class="common" name=GrpName ></td>
        
          <td class="title">单位地址</td>
          <td class="input"><Input class="common" name=GrpAddress ></td></tr>
          
        <tr class="common">
          <td class="title">家庭地址编码</td>
          <td class="input"><Input class="common" name=HomeAddressCode ></td>
          
          <td class="title">家庭地址</td>
          <td class="input"><Input class="common" name=HomeAddress ></td></tr>
          
        <tr class="common">
          <td class="title">电话</td>
          <td class="input"><Input class="common" name=Phone ></td>
          
          <td class="title">传呼</td>
          <td class="input"><Input class="common" name=BP ></td></tr>
          
        <tr class="common">
          <td class="title">手机</td>
          <td class="input"><Input class="common" name=Mobile ></td>
          
          <td class="title">e_mail</td>
          <td class="input"><Input class="common" name=EMail ></td></tr>

      </table>
      
      <table align="center">
        <tr>
          <td><input type="button" value="关联有效保单" onclick="queryValidCertify( )"></td>
          <td><input type="button" value="关联解约保单" onclick="queryInvalidCertify( )"></td></tr>
      </table>

    </div>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>
