<%
//程序名称：TaskCommonCustomerInit.jsp
//程序功能：工单管理客服信息初始化页面
//创建日期：2005-03-20 15:20:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<SCRIPT>
var turnPage2 = new turnPageClass();

//查询作业历史
function queryHistoryGrid()
{
	var strSQL;
	strSQL = "select t.WorkNo, t.WorkNo, (select OwnerNo from LGWorkBox where WorkBoxNo = t.WorkBoxNo), " +
			 "       (select GroupNo from LGGroupMember where MemberNo = (select OwnerNo from LGWorkBox where WorkBoxNo = t.WorkBoxNo)), " +
			 "       char(t.InDate) || ' ' || t.InTime,  t.InMethodNo " +
			 "from   LGWorkTrace t " +
	         "where  t.WorkNo = '" + WorkNo + "'" +
			 "order by t.NodeNo ";
			 
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, HistoryGrid);
	
	return true;
}

// 保单信息列表的初始化
function initHistoryGrid()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="作业号";         	  //列名
		iArray[1][1]="120px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="受理号";         	  //列名
		iArray[2][1]="150px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="经办人";         	//列名
		iArray[3][1]="100px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="经办机构";              //列名
		iArray[4][1]="160px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="经办时间";              //列名
		iArray[5][1]="200px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[6]=new Array();
		iArray[6][0]="操作类型";              //列名
		iArray[6][1]="100px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许       
		
		HistoryGrid = new MulLineEnter("fm", "HistoryGrid"); 
		//这些属性必须在loadMulLine前
		HistoryGrid.mulLineCount = 0;
		HistoryGrid.displayTitle = 1;
		HistoryGrid.locked = 1;
		HistoryGrid.canSel = 0;
		HistoryGrid.canChk = 0;
		HistoryGrid.hiddenSubtraction = 1;
		HistoryGrid.hiddenPlus = 1;
		HistoryGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
	
	queryHistoryGrid();
}
</SCRIPT>