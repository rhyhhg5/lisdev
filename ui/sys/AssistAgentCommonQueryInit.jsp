<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>

<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");	
	System.out.println(tGI.ManageCom);
%>

<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  { 
    fm.all('AssistAgentCode').value = '';
    fm.all('AssistAgentName').value = '';
  }
  catch(ex)
  {
    alert("在AssistAgentGrid.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm()
{
  try
  {
    initInpBox();
    initAssistSalechnl();
    initAssistAgentGrid();
    showAllCodeName();
  }
  catch(re)
  {
    alert("AssistAgentGrid.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentGrid
 ************************************************************
 */
function initAssistAgentGrid()
  {                             
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="业务员代码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="姓名";         //列名
        iArray[2][1]="60px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="性别";         //列名
        iArray[3][1]="60px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[4]=new Array();
        iArray[4][0]="身份证号";         //列名
        iArray[4][1]="120px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="状态";         //列名
        iArray[5][1]="40px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
  
        AgentGrid = new MulLineEnter( "fm" , "AgentGrid" ); 

        //这些属性必须在loadMulLine前
        AgentGrid.mulLineCount = 10;   
        AgentGrid.displayTitle = 1;
        AgentGrid.canSel=1;
        AgentGrid.locked=1;
	    AgentGrid.hiddenPlus=1;
	    AgentGrid.hiddenSubtraction=1;
        AgentGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化AssistAgentGrid时出错："+ ex);
      }
    }
</script>
