<%
//程序名称：LCFamilyInfoQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-26 15:55:46
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('FamilyNo').value = "";
    fm.all('Remark').value = "";
    fm.all('HomeAddress').value = "";
    fm.all('HomeZipCode').value = "";
    fm.all('HomePhone').value = "";
    fm.all('HomeFax').value = "";
  }
  catch(ex) {
    alert("在LCFamilyInfoQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLCFamilyInfoGrid();  
  }
  catch(re) {
    alert("LCFamilyInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LCFamilyInfoGrid;
function initLCFamilyInfoGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="家庭号";         		//列名
    iArray[1][1]="30px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="家庭地址";         		//列名
    iArray[2][1]="30px";         		//列名
    iArray[2][3]=0;         		//列名   
    
    iArray[3]=new Array();
    iArray[3][0]="家庭邮编";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="家庭电话";         		//列名
    iArray[4][1]="30px";         		//列名
    iArray[4][3]=0;         		//列名    

    iArray[5]=new Array();
    iArray[5][0]="家庭传真";         		//列名
    iArray[5][1]="30px";         		//列名
    iArray[5][3]=0;         		//列名   
        
    iArray[6]=new Array();                          
    iArray[6][0]="备注";         		
    iArray[6][1]="30px";         		
    iArray[6][3]=0;         		
  LCFamilyInfoGrid = new MulLineEnter( "fm" , "LCFamilyInfoGrid" ); 
    //这些属性必须在loadMulLine前

    LCFamilyInfoGrid.mulLineCount = 0;   
    LCFamilyInfoGrid.displayTitle = 1;
    LCFamilyInfoGrid.hiddenPlus = 1;
    LCFamilyInfoGrid.hiddenSubtraction = 1;
    LCFamilyInfoGrid.canSel = 1;
    LCFamilyInfoGrid.canChk = 0;
    LCFamilyInfoGrid.selBoxEventFuncName = "showOne";

    LCFamilyInfoGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LCFamilyInfoGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
