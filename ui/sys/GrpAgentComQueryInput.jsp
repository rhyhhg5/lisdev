<%
//程序名称：GrpAgentComQueryInput.jsp
//程序功能：
//创建日期：2010-11-29
//创建人  ：gzh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<script>
	var Crs_SaleChnl = "<%= request.getParameter("Crs_SaleChnl")%>";
	var Crs_SaleChnlName = "<%= request.getParameter("Crs_SaleChnlName")%>";		
</script>	
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./GrpAgentComQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <%@include file="./GrpAgentComQueryInit.jsp"%>
  <title>交叉销售对方业务员查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./GrpAgentCommonQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--业务员查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAGrpAgent);">
            </td>
            <td class= titleImg>
                业务员查询条件
            </td>            
    	</tr>
    </table>
  <Div  id= "divLAGrpAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <td class="title8">交叉销售渠道</td>
        <td class="input8">
            <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onblur=" return GrpAgentComReset();" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" elementtype=nacessary/>
        </td>
        <TD class= title>   对方机构编码  </TD>
        <TD class= input> 
        <Input class=common name="GrpAgentCom">
        </TD>
        <TD  class= title> 对方机构名称 </TD>
        <TD  class= input> <Input  class="common" name="GrpAgentComName" elementtype=nacessary TABINDEX="-1"  >
      </TR>   
    </table>
          
          <!--INPUT class=common VALUE="查询" TYPE=button onclick="easyQueryClick();"--> 
	   <table> 
		    <tr>
		    <td><INPUT class=cssbutton VALUE="查  询" TYPE=button onclick="easyQueryClick();"> </td>
		    <td><INPUT class=cssbutton VALUE="返  回" TYPE=button onclick="returnParent();"> 	</td>
		    <td><INPUT class=cssbutton VALUE="取  消" TYPE=button onclick="closePage();"> 	</td>

			<td>  
			 <!--INPUT class=common VALUE="业务员信息" TYPE=button onclick="returnParent();"-->  </td>
		   <td><!--INPUT class=common VALUE="投保单信息" TYPE=button onclick="ProposalClick();"-->  </td>
			<td> <!--INPUT class=common VALUE="保单信息" TYPE=button onclick="PolClick();"-->  </td>
			<td>  
			<!--INPUT class=common VALUE="销户保单信息" TYPE=button onclick="DesPolClick();"-->  </td>
		    </tr> 
	   </table> 
    </Div>      
          				
    <table>
    	<tr>
        <td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpAgentComGrid);">
    		</td>
    		<td class= titleImg>
    			 对方机构查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divGrpAgentComGrid" style= "display: ''" align =center>
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpAgentComGrid" align=center>
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table>
    		<tr>
    			<td>
			      <INPUT class=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
			    </td>
			    <td>  
			      <INPUT class=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    </td>
			    <td> 			      
			      <INPUT class=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    </td>
			    <td> 			      
			      <INPUT class=cssbutton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 						
			    </td>  			
  			</tr>
  		</table>
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
