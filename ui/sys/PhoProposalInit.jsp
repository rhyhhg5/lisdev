<%
//程序名称：LCProposalInput.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox() { 
  try {  
                                   
    
  } catch(ex) {
    //alert("在LCProposalInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  } 
  
  //附加字段
  try{
  
  }     
  catch(ex){}
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
   if(checktype=="1")
   {
     param="13";
     fm.pagename.value="13";     
   }
   if(checktype=="2")
   {
     param="23";
     fm.pagename.value="23";     
   } 
  }
  catch(ex)
  {
    alert("在LCProposalInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                      

//初始化界面
function initForm() {
	try	{ 	
		initBnfGrid();
		initRiskGrid();
		queryRisk();
		
	} catch(ex) {
	}
}


// 受益人信息列表的初始化
function initBnfGrid() {                               
  var iArray = new Array();

  try {
    iArray[0]=new Array();
    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";		//列宽
    iArray[0][2]=10;			//列最大值
    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许
  
    iArray[1]=new Array();
    iArray[1][0]="姓名"; 	//列名
    iArray[1][1]="120px";		//列宽
    iArray[1][2]=30;			//列最大值
    iArray[1][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[1][9]="姓名|len<=20";//校验

	iArray[2]=new Array();
    iArray[2][0]="生日"; 		//列名
    iArray[2][1]="70px";		//列宽
    iArray[2][2]=40;			//列最大值
    iArray[2][3]=1;			//是否允许输入,1表示允许，0表示不允许

	iArray[3]=new Array();
    iArray[3][0]="性别";         		//列名
    iArray[3][1]="40px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

	iArray[4]=new Array();
    iArray[4][0]="受益性质"; 		//列名
    iArray[4][1]="50px";		//列宽
    iArray[4][2]=40;			//列最大值
    iArray[4][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[4][4]="BnfType";
    iArray[4][9]="受益人类别|notnull&code:BnfType";

    iArray[5]=new Array();
    iArray[5][0]="比例"; 		//列名
    iArray[5][1]="50px";		//列宽
    iArray[5][2]=40;			//列最大值
    iArray[5][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[5][9]="受益比例|num&len<=10";

	iArray[6]=new Array();
    iArray[6][0]="状态"; 		//列名
    iArray[6][1]="90px";		//列宽
    iArray[6][2]=40;			//列最大值
    iArray[6][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[6][9]="受益比例|num&len<=10";

	iArray[7]=new Array();
    iArray[7][0]="受益顺序"; 		//列名
    iArray[7][1]="50px";		//列宽
    iArray[7][2]=40;			//列最大值
    iArray[7][3]=1;			//是否允许输入,1表示允许，0表示不允许

	iArray[8]=new Array();
    iArray[8][0]="与被保人关系"; 	//列名
    iArray[8][1]="70px";		//列宽
    iArray[8][2]=60;			//列最大值
    iArray[8][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[8][4]="Relation";
    iArray[8][9]="与被保人关系|code:Relation";
  
    iArray[9]=new Array();
    iArray[9][0]="证件类型"; 		//列名
    iArray[9][1]="50px";		//列宽
    iArray[9][2]=40;			//列最大值
    iArray[9][3]=1;			//是否允许输入,1表示允许，0表示不允许
  
    iArray[10]=new Array();
    iArray[10][0]="证件号码"; 		//列名
    iArray[10][1]="130px";		//列宽
    iArray[10][2]=80;			//列最大值
    iArray[10][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[10][9]="证件号码|len<=20"; 
 
    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" ); 
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 0; 
    BnfGrid.displayTitle = 1;
    BnfGrid.locked = 1;
    BnfGrid.loadMulLine(iArray); 
  
    //这些操作必须在loadMulLine后面
    //BnfGrid.setRowColData(0,8,"1");
    //BnfGrid.setRowColData(0,9,"1");
  } catch(ex) {
    alert("在PhoProposalInit.jsp-->initBnfGrid函数中发生异常:初始化界面错误!");
  }
}

// 告知信息列表的初始化
function initRiskGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种代码";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="险种名称";         		//列名
      iArray[2][1]="180px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[3]=new Array();
      iArray[3][0]="被保人客户号";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="被保人姓名";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=150;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[5]=new Array();
      iArray[5][0]="险种保单号";         		//列名
      iArray[5][1]="0px";            		//列宽
      iArray[5][2]=150;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许


        			//是否允许输入,1表示允许，0表示不允许


      RiskGrid = new MulLineEnter( "fm" , "RiskGrid" ); 
      //这些属性必须在loadMulLine前
      RiskGrid.mulLineCount = 0;   
      RiskGrid.displayTitle = 1;
	  RiskGrid.locked = 1;
	  RiskGrid.canSel = 1;

      //RiskGrid.tableWidth   ="500px";
      RiskGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      RiskGrid.selBoxEventFuncName = "queryBnf";
    }
    catch(ex) {
      alert("在PhoProposalInit.jsp-->initRiskGrid函数中发生异常:初始化界面错误!");
    }
}


</script>