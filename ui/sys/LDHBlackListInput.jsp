<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
	//程序名称：LAAgentInput.jsp
	//程序功能：个人代理增员管理
	//创建日期：2002-08-16 15:39:06
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String transact = request.getParameter("transact");
    String tBlacklistNo = request.getParameter("BlacklistNo");
	%>
	<script type="text/javascript">
	var transact = "<%=transact%>";
	var tBlacklistNo= "<%=tBlacklistNo%>";
	</script>
	<%@page contentType="text/html;charset=GBK" %>
	<head >
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="LDHBlackListInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LDHBlackListInit.jsp"%>
	</head>
	<body  onload="initForm();initElementtype();" >
	<form action="./LDHBlackListSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
						<td class=titleImg>
							黑名单人员信息
						</td>
					</td>
				</tr>
			</table>
			<Div  id= "divLAAgent1" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD class= title>
							黑名单人员编号
						</TD>
						<TD  class= input>
							<Input class= 'readonly' readonly name=BlacklistNo >
						</TD>
						<TD  class= title>
	    					  类型 
	   					 </TD>
	   					 <TD CLASS=input COLSPAN=1  >
	     					 <Input class=codeNo  NAME="Type" verify="类型|code:Type" CodeData="0|^0|个人^1|组织 " ondblclick="showCodeListEx('Type',[this,TypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Type',[this,TypeName],[0,1],null,null,null,1);" ><input class=codename name=TypeName readonly=true elementtype=nacessary>
						</TR>
						<TR  class= common>
						<TD  class= title>
							名称
						</TD>
						<TD  class= input>
						<Input name=Name class= common elementtype=nacessary>
						</TD>
						<TD  class= title>
							性别
						</TD>
          				<TD  class= input>
            			<Input class=codeNo name=Sex  verify="性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input class=codename name=SexName readonly=true >    
						</TR>
						<TR  class= common>
						<TD  class= title>
							证件类型
						</TD>
						<TD  class= input>
							<Input class=codeNo name="IDType" ondblclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1]);"><input class=codename name=IDTypeName readonly=true >    
						</TD>
						<TD  class= title>
							证件号码
						</TD>
						<TD  class= input>
							<Input name=IDNo class= common verify="证件号码|len<=20" >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
						  	 出生日期
  	       			    </TD> 
  	        			<TD  class= input COLSPAN="1" >
  	        				<Input class= coolDatePicker name=Birthday  verify="出生日期|date">
  	       			    </TD>
						
						<TD  class= title>
							组织机构代码
						</TD>
						<TD  class= input>
							<Input name=OrganComCode class= common >
						</TD>
						</TR>
						<TR  class= common>
						<TD  class= title>
							统一社会信用代码
						</TD>
						<TD  class= input>
							<Input name=UnifiedSocialCreditNo class=common >
						</TD>
						<TD  class= title>
							国籍
        			    </TD>
          				<TD  class= input>
         					 <input class=codeNo name="Nationality" verify="投保人国籍|code:Nationality" ondblclick="return showCodeList('NativePlace',[this,NationalityName],[0,1]);" onkeyup="return showCodeListKey('NativePlace',[this,NationalityName],[0,1]);"><input class=codename name=NationalityName readonly=true >    
         				 </TD>
						</TR>
						<TR  class= common>
						<TD  class= title>
							发布机构
						</TD>
						<TD  class= input>
							<Input name=PublishCom class=common >
						</TD>
						<TD  class= title>
							原因	    					  
	   					 </TD>
	   					 <TD CLASS=input COLSPAN=1  >
	     					 <Input class=codeNo  NAME="Cause" verify="类型|code:Cause" CodeData="0|^01|同业重疾或伤残理赔史^02|密集投保^03|不如实告知史^04|其他" ondblclick="showCodeListEx('Cause',[this,CauseName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Cause',[this,CauseName],[0,1],null,null,null,1);" ><input class=codename name=CauseName readonly=true >
						</TR>
						<TR  class= common>
						<TD  class= input>
						</TD>
						</TR>
				</table>
				 <table class= common>
        <TR class= common>
			<TD class= title>备注</TD>
		</TR>					
		<TR  class= common>
		  	<TD  class= input>
	    		<textarea name=Remark cols="100%" rows="5"  class="common"></textarea>
		    </TD>
  		</TR>
      </table>
   <INPUT VALUE="保  存" TYPE=button onclick="submitForm();" class=cssbutton >
   <INPUT VALUE="修  改" TYPE=button onclick="updateClick();" class=cssbutton >
      
      <input type=hidden id="fmtransact" name="fmtransact">
			</Div>
				 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>