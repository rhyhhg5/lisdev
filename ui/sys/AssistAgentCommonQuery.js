//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在AgentQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();
	//alert(tSel);
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery6(arrReturn);
			top.close();
		}
		catch(ex)
		{
			alert(ex.message);
			alert( "要返回的页面函数出错！");
		}
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = AgentGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	  return arrSelected;
	
	arrSelected = new Array();
	var strSQL = "";
	strSQL = strSQL = "select agentcode,name from LAAgent where 1=1 "
	         + "and AgentCode ='"+AgentGrid.getRowColData(tRow-1,1)+"'"; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	return arrSelected;
}
// 查询按钮
function easyQueryClick()
{
	initAssistAgentGrid();
	if(!verifyInput2())
	return false;
	var tAssistSaleChnl = fm.AssistSaleChnl.value;
	var tSQL = "select code1,codename from ldcode1 where codetype = 'salechnl' and code = '"+tAssistSaleChnl+"' ";
	var arrResult = easyExecSql(tSQL);
	if(!arrResult){
		alert("未查询到满足条件的数据！");
		return false;
	}
	// 书写SQL语句
	var strSQL = "";
	var consql = " and a.BranchType = '"+arrResult[0][0]+"' and a.BranchType2 = '"+arrResult[0][1]+"' ";
    
	strSQL = "select a.agentcode,a.name,"
		+" (select codename from ldcode where codetype = 'sex' and code = a.sex),a.idno,"
		+" (select codename from ldcode where codetype = 'agentstate' and code = a.agentstate) "
        +" from LAAgent a where 1=1 "
        + " and a.ManageCom like '" +ManageCom+"%'"
        + " and (a.AgentState is null or a.AgentState < '06')  "
        + consql
        + getWherePart('a.AgentCode','AssistAgentCode','like')
        + getWherePart('a.Name','AssistAgentName','like')
        + " order by a.AgentCode";
    var arrResult1 = easyExecSql(strSQL);
	if(!arrResult1){
		alert("未查询到满足条件的数据！");
		return false;
	}
	turnPage1.queryModal(strSQL, AgentGrid);
}

function closePage()
{        
	top.close();     
}
function initAssistSalechnl(){
	fm.all('AssistSaleChnl').value = tAssistSaleChnl;
	var tSQL = "select codename from ldcode where codetype = 'assistsalechnl' and code = '"+tAssistSaleChnl+"' ";
	var arrResult = easyExecSql(tSQL);
    if (arrResult != null)
    {
        fm.all('AssistSaleChnlName').value = arrResult[0][0];
    }
}