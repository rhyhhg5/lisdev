//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mAction = "";
var turnPage = new turnPageClass(); 
/*function showPersonInfo()
{	
	fm.action = "./CQPersonQueryDetail.jsp";
	//fm.target="_blank";
	fm.submit();
}
*/

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    initForm();
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    if( mAction == "INSERT" ) mAction = "INSERT||OK";
    
  }
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  if(!verifyInput()) 
    return false;
  if(fm.all('CustomerNo').value!='')
    {
      alert("不能保存");
      return false;
      }
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var loadFlag = "0";
	
	try
	{
		if( top.opener.mShowCustomerDetail == "PROPOSAL" ) loadFlag = "1";
	}
	catch(ex){}
	
	if( loadFlag == "1" )
	{
		if( mAction != "INSERT||OK" )
			alert( "请先保存，再点击返回按钮。" );
		else
		{
			try
			{
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		}
	}
}

function getQueryResult()
{
	var arrSelected = new Array();

	arrSelected[0] = new Array();
	arrSelected[0][0] = fm.all( 'CustomerNo' ).value;
	arrSelected[0][1] = fm.all( 'Name' ).value;
	arrSelected[0][2] = fm.all( 'Sex' ).value;
	arrSelected[0][3] = fm.all( 'Birthday' ).value;
	arrSelected[0][4] = fm.all( 'IDType' ).value;
	arrSelected[0][5] = fm.all( 'IDNo' ).value;

	return arrSelected;
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	if( arrQueryResult != null )
	{
		fm.all( 'CustomerNo' ).value = arrQueryResult[0][0];
		
		easyQueryClick();
	}
}


// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL = "";
	//var arrResult=new Array();
	var tCustomerNo = fm.all( 'CustomerNo' ).value;
	strSQL = "select a.* from LDPerson a where  a.CustomerNo='" + tCustomerNo + "'";
        arrResult=easyExecSql(strSQL,1,0);
        if(arrResult!=null)
        {
        displaycustomer(arrResult[0]);
        }
        strSQL="select b.AccKind,b.BankCode,b.BankAccNo,b.AccName  from LCAccount b where b.CustomerNo='" + tCustomerNo + "'";
        arrResult=easyExecSql(strSQL,1,0);
        if(arrResult!=null)
        {
        displayaccount(arrResult[0]);
        }
}

function displaycustomer()
{
	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		 
		// 显示查询结果
   fm.all('CustomerNo').value= arrResult[0][0];                     
   fm.all('Name').value= arrResult[0][1];                            
   fm.all('Sex').value= arrResult[0][2];                             
   fm.all('Birthday').value= arrResult[0][3];                        
   fm.all('IDType').value= arrResult[0][4];                          
   fm.all('IDNo').value= arrResult[0][5];                            
   fm.all('Password').value= arrResult[0][6];                        
   fm.all('NativePlace').value= arrResult[0][7];                     
   fm.all('Nationality').value= arrResult[0][8];                     
   fm.all('RgtAddress').value= arrResult[0][9];                      
   fm.all('Marriage').value= arrResult[0][10];                       
   fm.all('MarriageDate').value= arrResult[0][11];                   
   fm.all('Health').value= arrResult[0][12];                         
   fm.all('Stature').value= arrResult[0][13];                        
   fm.all('Avoirdupois').value= arrResult[0][14];                    
   fm.all('Degree').value= arrResult[0][15];                      
   fm.all('CreditGrade').value= arrResult[0][16];                    
   fm.all('OthIDType').value= arrResult[0][17];                      
   fm.all('OthIDNo').value= arrResult[0][18];                        
   fm.all('ICNo').value= arrResult[0][19];                           
   fm.all('GrpNo').value= arrResult[0][20];                          
   fm.all('JoinCompanyDate').value= arrResult[0][21];                
   fm.all('StartWorkDate').value= arrResult[0][22];                  
   fm.all('Position').value= arrResult[0][23];                       
   fm.all('Salary').value= arrResult[0][24];                         
   fm.all('OccupationType').value= arrResult[0][25];                 
   fm.all('OccupationCode').value= arrResult[0][26];                 
   fm.all('WorkType').value= arrResult[0][27];                       
   fm.all('PluralityType').value= arrResult[0][28];                  
   fm.all('DeathDate').value= arrResult[0][29];                      
   fm.all('SmokeFlag').value= arrResult[0][30];                      
   fm.all('BlacklistFlag').value= arrResult[0][31];                  
   fm.all('Proterty').value= arrResult[0][32];                       
   fm.all('Remark').value= arrResult[0][33];                         
   fm.all('State').value= arrResult[0][34]; 
   fm.all('VIPValue').value = arrResult[0][35];                       
   //fm.all('Operator').value= arrResult[0][35];                       
   //fm.all('MakeDate').value= arrResult[0][36];                       
   //fm.all('MakeTime').value= arrResult[0][37];                       
   //fm.all('ModifyDate').value= arrResult[0][38];                     
   //fm.all('ModifyTime').value= arrResult[0][39];   

	} // end of if
}
function displayaccount()
{
		fm.all('AccKind').value = arrResult[0][0];
		fm.all('BankCode').value = arrResult[0][1];
		fm.all('BankAccNo').value = arrResult[0][2];
		fm.all('AccName').value = arrResult[0][3];
}

function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
function getaddresscodedata()
{
    var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select AddressNo,PostalAddress from LCAddress where CustomerNo ='"+fm.CustomerNo.value+"'";
    //alert("strsql :" + strsql);
    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
    if (turnPage.strQueryResult != "")
    {
    	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	m = turnPage.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    	}
    }
    //alert ("tcodedata : " + tCodeData);
    //return tCodeData;
    fm.all("AddressNo").CodeData=tCodeData;
}  
function getdetailaddress()
{
    var strSQL="select b.* from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.CustomerNo.value+"'";
    arrResult=easyExecSql(strSQL);
try{fm.all('CustomerNo').value= arrResult[0][0];}catch(ex){}; 
try{fm.all('AddressNo').value= arrResult[0][1];}catch(ex){}; 
try{fm.all('PostalAddress').value= arrResult[0][2];}catch(ex){}; 
try{fm.all('ZipCode').value= arrResult[0][3];}catch(ex){}; 
try{fm.all('Phone').value= arrResult[0][4];}catch(ex){}; 
try{fm.all('Fax').value= arrResult[0][5];}catch(ex){}; 
try{fm.all('HomeAddress').value= arrResult[0][6];}catch(ex){}; 
try{fm.all('HomeZipCode').value= arrResult[0][7];}catch(ex){}; 
try{fm.all('HomePhone').value= arrResult[0][8];}catch(ex){}; 
try{fm.all('HomeFax').value= arrResult[0][9];}catch(ex){}; 
try{fm.all('CompanyAddress').value= arrResult[0][10];}catch(ex){}; 
try{fm.all('CompanyZipCode').value= arrResult[0][11];  }catch(ex){}; 
try{fm.all('CompanyPhone').value= arrResult[0][12];}catch(ex){}; 
try{fm.all('CompanyFax').value= arrResult[0][13];}catch(ex){}; 
try{fm.all('Mobile').value= arrResult[0][14];}catch(ex){}; 
try{fm.all('MobileChs').value= arrResult[0][15];}catch(ex){}; 
try{fm.all('EMail').value= arrResult[0][16];}catch(ex){}; 
try{fm.all('BP').value= arrResult[0][17];}catch(ex){}; 
try{fm.all('Mobile2').value= arrResult[0][18];}catch(ex){}; 
try{fm.all('MobileChs2').value= arrResult[0][19];}catch(ex){}; 
try{fm.all('EMail2').value= arrResult[0][20];}catch(ex){}; 
try{fm.all('BP2').value= arrResult[0][21];}catch(ex){}; 
try{fm.all('Operator').value= arrResult[0][22];}catch(ex){}; 
try{fm.all('MakeDate').value= arrResult[0][23];}catch(ex){}; 
try{fm.all('MakeTime').value= arrResult[0][24];}catch(ex){}; 
try{fm.all('ModifyDate').value= arrResult[0][25];}catch(ex){}; 
try{fm.all('ModifyTime').value= arrResult[0][26];}catch(ex){}; 
} 
