//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var arrAllDataSet;

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//王珑修改

/*********************************************************************
 *  查询险种信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	
	// 初始化表格
	initPolGrid();	
	// 书写SQL语句RiskCode
	var strSQL = "";	
	
	    strSQL = 
	    //有效险种
	   "select a.RiskSeqNo,a.InsuredName,a.InsuredNo,b.riskname,a.RiskCode,"
		 +"case when  c.FactorValue='1' then a.Amnt end," 
		 +"case when  c.FactorValue='3' then a.Mult end,"
		 +"a.Prem ,a.CValiDate,date(a.EndDate) - 1 day ,a.PayToDate, a.EndDate,"
		 +"case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end "
		 +" ,case when CodeName('polstate',a.PolState) is not null then CodeName('polstate',a.PolState) else CodeName('stateflag',a.stateflag) end "
		 +" ,a.PolNo"
		 +" ,case when (select 1 from lcpol where lcpol.paytodate<=current date "
		 +" and (lcpol.paytodate + (select case riskperiod when 'L' then 60 else 30 end "
		 +" from lmriskapp where riskcode=lcpol.riskcode) day)>=current date and polno=a.polno and current date < payenddate)"
		+" is null then '否' else '是' end,"
		+" coalesce((select sum(sumduepaymoney) from ljspayperson where polno=a.polno ),0) "
	   +"from LCPol a inner join lmrisk b on b.RiskCode=a.RiskCode "
	   +"inner join LDRiskParamPrint c on c.RiskCode=a.RiskCode "
	   +"where a.ContNo='" + tContNo + "' and c.factorName='AmntFlag' "
	   + "  and (a.StateFlag is null or a.StateFlag in('1')) "
	   
		 + "union "
		 
		 //C表的失效险种
		 +"select a.RiskSeqNo,a.InsuredName,a.InsuredNo,b.riskname,a.RiskCode,"
		 +"case when  c.FactorValue='1' then a.Amnt end," 
		 +"case when  c.FactorValue='3' then a.Mult end,"
		 +"a.Prem ,a.CValiDate,date(a.EndDate) - 1 day ,a.PayToDate, a.PayToDate,"
		 +"case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end "
		 //+",CodeName('stateflag', a.StateFlag)
		 //+", case PolState when '03050001' then '满期终止' else CodeName('stateflag', a.StateFlag) end "
		 + " ,case when CodeName('polstate',a.PolState) is not null then CodeName('polstate',a.PolState) else CodeName('stateflag',a.stateflag) end "
		 +",a.PolNo "
		 +" ,case when (select 1 from lcpol where lcpol.paytodate<=current date "
		 +" and (lcpol.paytodate + (select case riskperiod when 'L' then 60 else 30 end "
		 +" from lmriskapp where riskcode=lcpol.riskcode) day)>=current date and polno=a.polno and current date < payenddate)"
		 +" is null then '否' else '是' end,"
		 +" coalesce((select sum(sumduepaymoney) from ljspayperson where polno=a.polno ),0) "
	     +"from LCPol a inner join lmrisk b on b.RiskCode=a.RiskCode "
	     +"inner join LDRiskParamPrint c on c.RiskCode=a.RiskCode "
	     +"where a.ContNo='" + tContNo + "' and c.factorName='AmntFlag' "
	     + "  and a.StateFlag in('2', '3') "
	   
		 + "union "
		 
		 //B表的失效险种
		 + "select a.RiskSeqNo,a.InsuredName,a.InsuredNo,b.riskname,a.RiskCode,"
		 +"case when  c.FactorValue='1' then a.Amnt end," 
		 +"case when  c.FactorValue='3' then a.Mult end,"
		 +"a.Prem ,a.CValiDate,date(a.EndDate) - 1 day,a.PayToDate, char(d.Edorvalidate),"
		 +"case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end, "
		 +"(case (select 1 from llcontdeal where edorno=a.edorno) when 1 then (select case edortype when 'CT' then '解约' when 'HZ' then '合同终止' when 'CD' then '合同终止退费' when 'RB' then '解约回退' end from llcontdeal where edorno=a.edorno) else(case when d.edortype = 'CT' then '终止合同' when d.edortype = 'XT' then '协议退保' when d.edortype = 'WT' then '犹豫期退保' when d.edortype = 'ZT' then '团单减人' end) end),"
		 +" a.PolNo "
		 +" ,case when (select 1 from lcpol where lcpol.paytodate<=current date "
		 +" and (lcpol.paytodate + (select case riskperiod when 'L' then 60 else 30 end "
		 +" from lmriskapp where riskcode=lcpol.riskcode) day)>=current date and polno=a.polno and current date < payenddate)"
		 +" is null then '否' else '是' end,"
		 +" coalesce((select sum(sumduepaymoney) from ljspayperson where polno=a.polno ),0) "
		 //e.CodeName,a.PolNo "
	     +"from LBPol a inner join lmrisk b on b.RiskCode=a.RiskCode "
	     +"inner join LDRiskParamPrint c on c.RiskCode=a.RiskCode "
	     +"inner join LPEdoritem d on a.edorno=d.edorno and d.edortype in ('CT','XT','WT','ZT') "
	   //+"left join LDCode e on d.Reasoncode=e.Code "
	     +"where a.ContNo='" + tContNo + "' and c.factorName='AmntFlag'";
	   // and e.codetype='reason_tb'";
	   
	   
	   //turnPage1.queryModal(strSQL, PolGrid);       
	  
	//查询SQL，返回结果字符串
   turnPage1.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
 
  //判断是否查询成功
  if (!turnPage1.strQueryResult) {
  	PolGrid.clearData(); 	 
    return false;
  }
 
  //查询成功则拆分字符串，返回二维数组
  turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
  arrAllDataSet = turnPage1.arrDataCacheSet;  
 
  //设置初始化过的MULTILINE对象
  turnPage1.pageDisplayGrid = PolGrid;   
         
  //保存SQL语句
  turnPage1.strQuerySql   = strSQL;
 
  //设置查询起始位置
  turnPage1.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  setAmntMult();
  var arrDataSet = turnPage1.getData(arrAllDataSet, turnPage1.pageIndex,MAXSCREENLINES);
 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);
}  


 /*********************************************************************
 *  查询保额、档次
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function setAmntMult()
 {
	if (arrAllDataSet.length != 0  )
	 {		
		for (i=0;i<arrAllDataSet.length ;i++ )
		{
		    
			if (arrAllDataSet[i][6]  =='null')
			{
				arrAllDataSet[i][6] = '' ;
			}
			if (arrAllDataSet[i][5]  =='null')
			{
				arrAllDataSet[i][5] = '' ;
			}
		}
	 }
	
 }
 
/*********************************************************************
 *  查询合同信息及投保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function returnParent()
 {
 	               
	var arrReturn1 = new Array();
	var arrReturn2 = new Array();
	var arrReturn3 = new Array();
	
		try{
			if(tContType=="1"){
				var strSQL = "select codeName('stateflag', Stateflag), Operator,ModifyDate,(select Name from LDCom where ComCode = a.ManageCom),getUniteCode(AgentCode),PayIntv,Prem,Remark" 
				 + ",CodeName('payintv', char(PayIntv)),CodeName('paymode', PayMode),PaytoDate,(select BankName from LDBank where BankCode = a.BankCode),BankAccNo,AccName,SignDate,CValiDate,date(CInValiDate)-1 day,CustomGetPolDate, "
				 + "  CodeName('lcsalechnl', SaleChnl) "
				 + " ,(select Name from LACom where a.Agentcom = Agentcom and cardflag='6') "
				 + " ,(select Name from LACom where a.Agentcom = Agentcom and  salechnl='03') "
				 + ", (select Name from LDCom where ComCode = a.ManageCom) "
		 		 + ", (select Address from LDCom where ComCode = a.ManageCom) "
		 		 + ", LostTimes "
		 		 + ", (select QualifNo from LAQualification where agentcode=a.agentcode order by validend desc fetch first 1 rows only) 资格证书号码 "
		 		 + ", (select CertifNo from LACertification where agentcode=a.agentcode order by validend desc fetch first 1 rows only) 展业证书号码 "
		 		 + ", Agentsalecode 代理销售业务员编码 "
		 		 + ", (select name from laagenttemp where agentcode=a.Agentsalecode) 代理销售业务员姓名 "
				 + " from LCCont a where ContNo= '"+tContNo+"'";
			}else if(tContType=="2"){
                 var strSQL = "select (select max(EdorName) from LMEdorItem where EdorCode = (select max(EdorType) from LPEdorItem where EdorNo = a.EdorNo)),Operator,ModifyDate,(select Name from LDCom where ComCode = a.ManageCom),getUniteCode(AgentCode),PayIntv,Prem,Remark" 
				 + ",CodeName('payintv', char(PayIntv)),CodeName('paymode', PayMode),PaytoDate,(select BankName from LDBank where BankCode = a.BankCode),BankAccNo,AccName,SignDate,CValiDate,date(CInValiDate) -1 day,CustomGetPolDate, "
				 + "    CodeName('salechnl', SaleChnl) "
				 + ",'','','','','', (select QualifNo from LAQualification where agentcode=a.agentcode order by validend desc fetch first 1 rows only) 资格证书号码 "
		 		 + ", (select CertifNo from LACertification where agentcode=a.agentcode order by validend desc fetch first 1 rows only) 展业证书号码 "
		 		 + ", Agentsalecode 代理销售业务员编码 "
		 		 + ", (select name from laagenttemp where agentcode=a.Agentsalecode) 代理销售业务员姓名 "
				 + " from LBCont a where ContNo= '"+tContNo+"'";
			}			
			arrReturn1 =easyExecSqlReMark(strSQL);
			if (arrReturn1 == null) {
                return false;
            } else {
			   displayCont(arrReturn1[0]);
			   displayPay(arrReturn1[0]);
			   setBankInfo(arrReturn1[0]);
			}
            
			//投保人信息
            if(tContType=="1"){
				var strSql2 = "select LDPerson.CustomerNo,LCAppnt.Appntname,LDPerson.Birthday,LCAppnt.OccupationCode,'',LCAddress.AddressNo" 
				 + ",CodeName('sex', LDPerson.Sex),CodeName('marriage', LCAppnt.Marriage),CodeName('nativeplace', LCAppnt.Nativeplace),CodeName('idtype', LDPerson.IDType),LDPerson.IDNo,'',LDPerson.GrpName,LCAddress.CompanyPhone" 
				 + ",LCAddress.CompanyAddress,LCAddress.CompanyZipCode,LCAddress.HomeAddress,LCAddress.HomeZipCode,LCAddress.HomePhone" 
				 + ",LCAddress.Mobile,LCAddress.EMail"
				 + ",LCAddress.PostalAddress,LCAddress.ZipCode,LCAddress.Phone,LCAppnt.Authorization" 
				 + " from LCAppnt,LDPerson,LCAddress"
				 + " where LCAppnt.ContNo= '"+tContNo+"'"
				 + " and LDPerson.CustomerNo=LCAppnt.AppntNo and LDPerson.CustomerNo = LCAddress.CustomerNo and LCAppnt.AddressNo=LCAddress.AddressNo";

			}else if(tContType=="2")
			{
                 var strSql2 = "select LDPerson.CustomerNo,LBAppnt.Appntname,LDPerson.Birthday,LBAppnt.OccupationCode,'',LCAddress.AddressNo" 
				 + ",CodeName('sex', LDPerson.Sex),CodeName('marriage', LBAppnt.Marriage),LBAppnt.NativePlace,CodeName('idtype', LDPerson.IDType),LDPerson.IDNo,'',LDPerson.GrpName,LCAddress.CompanyPhone" 
				 + ",LCAddress.CompanyAddress,LCAddress.CompanyZipCode,LCAddress.HomeAddress,LCAddress.HomeZipCode,LCAddress.HomePhone" 
				 + ",LCAddress.Mobile,LCAddress.EMail"
				 + ",LCAddress.PostalAddress,LCAddress.ZipCode,'',LBAppnt.Authorization" 
				 + " from LBAppnt,LDPerson,LCAddress"
				 + " where LBAppnt.ContNo= '"+tContNo+"'"
				 + " and LDPerson.CustomerNo=LBAppnt.AppntNo and LDPerson.CustomerNo = LCAddress.CustomerNo and LBAppnt.AddressNo=LCAddress.AddressNo";

			}
			arrReturn2 =easyExecSql(strSql2);
			
			if (arrReturn2 == null) {
			} else {
              
			   var strSql3="select  trim(OccupationName)||'-'||trim(workname) from LDOccupation" 
                   + " where OccupationCode='" + arrReturn2[0][3]+ "'"                   
                   + " order by OccupationCode"
			     ;
			    arrReturn3 =easyExecSql(strSql3);
			    if (arrReturn3 == null) {
                    arrReturn2[0][4]='';
			    } else {
			         arrReturn2[0][4]=arrReturn3[0][0];
			    }
			   displayAppnt(arrReturn2[0]);
			}
			
			//上次抽档时间
			var sql = "select MakeDate from LJSPayB "
			          + "where OtherNo = '" + tContNo + "' "
			          + "   and OtherNoType = '2' ";
			var rs = easyExecSql(sql);
			if(rs)
			{
			  fm.PostalDate.value = rs[0][0];
			}
			
			//得到帐户余额信息
			var sql = "select AccBala from LCAppAcc where " +
			          "CustomerNo = (select AppntNo from LCCont where ContNo = '" + tContNo + "' ) ";
			var result = easyExecSql(sql);
			if (result)
			{
				fm.AccBala.value = result[0][0];
			}
		}
		catch(ex)
		{
			alert( "请先选择一条非空记录。");			
		}		
	showCodeName();
  } 
  
/*********************************************************************
 *  设置合同信息
 *  参数  ：  cArr 数组
 *  返回值：  无
 *********************************************************************
 */
  function displayCont(cArr)
  {    
  	try { fm.all('AppFlag').value = cArr[0]; } catch(ex) { };
  	try { fm.all('Operator').value = cArr[1]; } catch(ex) { };
  	try { fm.all('ModifyDate').value = cArr[2]; } catch(ex) { };
  	try { fm.all('ManageCom').value = cArr[3]; } catch(ex) { };
	  try { fm.all('AgentCode').value = cArr[4]; } catch(ex) { };
  	try { fm.all('PayIntv').value = cArr[5]; } catch(ex) { };
  	try { fm.all('Prem').value = cArr[6]; } catch(ex) { };
	try { fm.all('Remark').value = cArr[7]; } catch(ex) { };
	try { fm.all('SignDate').value = cArr[14]; } catch(ex) { };
  	try { fm.all('CValiDate').value = cArr[15]; } catch(ex) { };
	try { fm.all('CInValiDate').value = cArr[16]; } catch(ex) { };
	try { fm.all('SaleChnlCode').value = cArr[21]; } catch(ex) { };
	try { fm.all('SaleChnlAddress').value = cArr[22]; } catch(ex) { };
	try { fm.all('LostTimes').value = cArr[23]; } catch(ex) { };
	
	//若是续保保单，则投保日期，生效日期都取原始保单的日期
	var sql = "  select a.signDate, a.CValiDate "
	          + "from LBCont a, LCRnewStateLog b "
	          + "where a.contNo = b.newContNo "
	          + "   and b.contNo = '" + tContNo + "' "
	          + "order by b.newPolNo ";
	var result = easyExecSql(sql);
	if(result)
	{
	  try { fm.all('SignDate').value = result[0][0]; } catch(ex) { };
  	try { fm.all('CValiDate').value = result[0][1]; } catch(ex) { };
	}

	//保单回执客户签收时间
	try { fm.all('CustomGetPolDate').value = cArr[17]; } catch(ex) { };
	try { fm.all('SaleChnl').value = cArr[18]; } catch(ex) { };
	
	
	//业务员递交客户回执时间跨度（天）
	var strSqlDate ="select case  when  (select makedate  from lzsyscertify   where certifyno = '"+tContNo+"') is null " +
    "then GetPolDate else   (select makedate  from lzsyscertify   where certifyno = '"+tContNo+"')    " +
	"end  from lccont where contno = '"+tContNo+"'";

	var arrDate = easyExecSql(strSqlDate);
	var datMake='';
	if (arrDate !=null && arrDate != "") {
		datMake = arrDate[0][0];
		try { 
			fm.all('CustomPolInteDay').value = dateDiff(fm.all('CustomGetPolDate').value,datMake,'D'); 
		} catch(ex) { 
			
		};
	}else {
		fm.all('CustomPolInteDay').value ="";
	}
	
	//核保结论
	var strSql = "select  CodeName from ldcode where codetype = 'uwflag' and Code=(select PassFlag from LCCUWMaster where ContNo='" +tContNo +"')";
	var arrReturn = easyExecSql(strSql);
	if (!(arrReturn ==null))
	{
		try { fm.all('PassFalg').value = arrReturn[0][0]; } catch(ex) { };
	}
   

	//显示其他合同信息
	if (cArr[4] !='' ||cArr[4] !=null) displayAgent(cArr[4]);  	
	
	//新添字段（资格证书号码、展业证书号码、代理销售业务员编码、代理销售业务员姓名）的显示
	var tSalechnl = fm.all('SaleChnl').value;
	try { fm.all('Certificate').value = cArr[24]; } catch(ex) { };
	try { fm.all('Exhibition').value = cArr[25]; } catch(ex) { };
	if(tSalechnl!="银行代理" && tSalechnl!="个险中介" && tSalechnl!="团险中介" && tSalechnl!="互动中介"){
		fm.all("T1").style.display = "none";
		fm.all("I1").style.display = "none";
		fm.all("T2").style.display = "none";
		fm.all("I2").style.display = "none";
	}else{
		try { fm.all('SalesAgentCode').value = cArr[26]; } catch(ex) { };
		try { fm.all('SalesAgentName').value = cArr[27]; } catch(ex) { };
	}
  }

/*********************************************************************
 *  设置投保人信息
 *  参数  ：  cArr 数组
 *  返回值：  无
 *********************************************************************
 */
  function displayAppnt(cArr)
  {
   var strTableName = "";
   if (tContType=='1'){
	   strTableName = "lcappnt";
   }else if (tContType=='2') {  
	   strTableName = "lbappnt";
   }
   	try { fm.all('AppntNo').value = cArr[0]; } catch(ex) { };
  	try { fm.all('ClietName').value = cArr[1]; } catch(ex) { };
  	try { fm.all('AppntBirthday').value = cArr[2]; } catch(ex) { };
  	try { fm.all('AppntOccupationCode').value = cArr[3]; } catch(ex) { };
  	try { fm.all('AppntOccupationName').value = cArr[4]; } catch(ex) { };
  	try { fm.all('AddressNo').value = cArr[5]; } catch(ex) { };
  	try { fm.all('AppntSex').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntMarriage').value = cArr[7]; } catch(ex) { };
  	try { fm.all('AppntNativePlace').value = cArr[8]; } catch(ex) { };
  	if(cArr[8] == "OS" || cArr[8] == "HK" || cArr[8] == "外籍人士" || cArr[8] == "港澳台人士") {
  		fm.all('NativePlace').style.display = "";
  		var Sql = "select codename from ldcode where codetype = 'nativecity' and code = (select nativecity from "+strTableName+" where contno = '"+tContNo+"' )";
  		var rs = easyExecSql(Sql);
  		if(rs != null) {
  			fm.all('NativePlace').value = rs[0][0];
  		}else {
  			alert(cArr[8]+"：获取到国家或地区信息失败！");
  		}
  	}
  	try { fm.all('AppntIDType').value = cArr[9]; } catch(ex) { };
  	try { fm.all('AppntIDNo').value = cArr[10]; } catch(ex) { };
  	try { fm.all('AppntGrpNo').value = cArr[11]; } catch(ex) { };
  	try { fm.all('AppntGrpName').value = cArr[12]; } catch(ex) { };
    try { fm.all('AppntGrpPhone').value = cArr[13]; } catch(ex) { };
	try { fm.all('CompanyAddress').value = cArr[14]; } catch(ex) { };
  	try { fm.all('AppntGrpZipCode').value = cArr[15]; } catch(ex) { };
  	try { fm.all('AppntHomeAddress').value = cArr[16]; } catch(ex) { };
  	try { fm.all('AppntHomeZipCode').value = cArr[17]; } catch(ex) { };
	try { fm.all('AppntPostalAddress').value = cArr[21]; } catch(ex) { };
  	try { fm.all('AppntPostalZipCode').value = cArr[22]; } catch(ex) { };
   	try { fm.all('AppntHomePhone').value = cArr[18]; } catch(ex) { };
  	try { fm.all('AppntMobile').value = cArr[19]; } catch(ex) { };
  	try { fm.all('AppntEMail').value = cArr[20]; } catch(ex) { };
  	try { fm.all('AppntPhone').value = cArr[23]; } catch(ex) { };
	try { fm.all('AppntAuth').value = cArr[24]; } catch(ex) { };
	if(cArr[24]=='1'){
		try { fm.all('AppntAuthName').value = '是'; } catch(ex) { };
	}
	if(cArr[24]=='0'){
		try { fm.all('AppntAuthName').value = '否'; } catch(ex) { };
	}

 }
  
/*********************************************************************
 * 显示用来显示证件生效日期、证件失效日期、岗位职务、以及税优投保单的特别信息
 * （个税征收方式、个人税务登记证代码、个人社会信用代码、团体编号、单位税务登记证代码、单位社会信用代码）
 *********************************************************************
 */
function showMoreAppnt() {
	try {
		if(tContType == 1) {//有效保单
			//1、IDStartDate,IDEndDate,Position 证件生效日期、证件失效日期、岗位职务
			var sql = "select IDStartDate,IDEndDate,Position,Appntno,Addressno,prtno from lcappnt where contno = '"+tContNo+"'";
			var rs = easyExecSql(sql);
		    if(rs != null) {
		    	fm.all('AppIDStartDate').value = rs[0][0];
				fm.all('AppIDEndDate').value = rs[0][1];
				fm.all('AppntPosition').value = rs[0][2];
				sql = "select grpname from lcaddress where customerno = '"+rs[0][3]+"' and addressno = '"+rs[0][4]+"' ";
				var rss = easyExecSql(sql);
				if(rss != null) {
					fm.all('AppntGrpName').value = rss[0][0];
				}
				showSYInfo(rs[0][5]);
				showdiscountfactorInfo(rs[0][5]);
		    } 
		}else if(tContType == 2) {//失效保单
			var sql = "select IDStartDate,IDEndDate,Position,Appntno,Addressno,prtno from lbappnt where contno = '"+tContNo+"'";
			var rs = easyExecSql(sql);
			if(rs != null) {
				fm.all('AppIDStartDate').value = rs[0][0];
				fm.all('AppIDEndDate').value = rs[0][1];
				fm.all('AppntPosition').value = rs[0][2];
				sql = "select grpname from lcaddress where customerno = '"+rs[0][3]+"' and addressno = '"+rs[0][4]+"' ";
				var rss = easyExecSql(sql);
				if(rss != null) {
					fm.all('AppntGrpName').value = rss[0][0];
				}
				showSYInfo(rs[0][5]);
				showdiscountfactorInfo(rs[0][5]);
			}
		}
	}catch(ex) {
		alert("PolDetailQuery.js--showMoreAppnt()方法中,赋值错误！"+ex.message);
	}
}

/**********************************************************
 * 显示税优具体信息的方法
 * @param Prtno
 * @returns
 * ********************************************************
 */
function showSYInfo(Prtno) {
	    var sql1 = "select taxflag,TaxPayerType from lccontsub where prtno='"+Prtno+"' ";
		var TaxFlag = easyExecSql(sql1);
		if (TaxFlag) {
			if (TaxFlag[0][0] == '1' && TaxFlag[0][1] == '01') {
				fm.all('GTaxNo').readOnly = false;
				fm.all('GOrgancomCode').readOnly = false;
			}
			if (TaxFlag[0][0] == '1') {
				var sql = "select lc.taxflag,codename('taxflag',lc.taxflag),lc.taxno,lc.taxpayertype,codename('taxpayertype',lc.taxpayertype),lc.batchno,lc.insuredid,"
						+ " ls.grpno,lc.GTaxNo,lc.transflag,codename('transflag',lc.transflag)  "
						+ " ,lc.CreditCode,lc.GOrgancomCode "
						+ " from lccontsub lc  left join lsbatchinfo ls on ls.batchno=lc.batchno "
						+ " left join lsgrp lg on lg.grpno=ls.grpno "
						+ " where lc.prtno='"+Prtno+"' ";
				var rs = easyExecSql(sql);
				if (rs) {
//					fm.TaxFlag.value = rs[0][0];
//					fm.TaxFlagName.value = rs[0][1];
					fm.TaxNo.value = rs[0][2];
					fm.TaxPayerType.value = rs[0][3];
					fm.TaxPayerTypeName.value = rs[0][4];
					fm.BatchNo.value = rs[0][5];
					fm.InsuredId.value = rs[0][6];
					// fm.GrpNo.value = rs[0][7];
					fm.GTaxNo.value = rs[0][8];
//					fm.TransFlag.value = rs[0][9];
//					fm.TransFlagName.value = rs[0][10];
					fm.CreditCode.value = rs[0][11];
					fm.GOrgancomCode.value = rs[0][12];
				}
			} else {
				var sql = "select lc.taxflag,codename('taxflag',lc.taxflag),lc.taxno,lc.taxpayertype,codename('taxpayertype',lc.taxpayertype),lc.batchno,lc.insuredid,"
						+ " ls.grpno,lg.TaxRegistration,lc.transflag,codename('transflag',lc.transflag)  "
						+ " ,lc.CreditCode,lg.OrgancomCode "
						+ " from lccontsub lc  left join lsbatchinfo ls on ls.batchno=lc.batchno "
						+ " left join lsgrp lg on lg.grpno=ls.grpno "
						+ " where lc.prtno='"+Prtno+"' ";
				var rs = easyExecSql(sql);
				if (rs) {
//					fm.TaxFlag.value = rs[0][0];
//					fm.TaxFlagName.value = rs[0][1];
					fm.TaxNo.value = rs[0][2];
					fm.TaxPayerType.value = rs[0][3];
					fm.TaxPayerTypeName.value = rs[0][4];
					fm.BatchNo.value = rs[0][5];
					fm.InsuredId.value = rs[0][6];
					fm.GrpNo.value = rs[0][7];
					fm.GTaxNo.value = rs[0][8];
//					fm.TransFlag.value = rs[0][9];
//					fm.TransFlagName.value = rs[0][10];
					fm.CreditCode.value = rs[0][11];
					fm.GOrgancomCode.value = rs[0][12];
				}
			}
		}
}
//显示折扣因子信息
function showdiscountfactorInfo(PrtNo){
	var sql ="select Agediscountfactor,Supdiscountfactor,Grpdiscountfactor,Totaldiscountfactor,premmult,codename('premmult',premmult),DiscountMode from lccontsub "
		+ " where prtno='" + PrtNo + "' ";
		var rs = easyExecSql(sql);
	if (rs) {
		fm.Agediscountfactor.value = rs[0][0];
		fm.Supdiscountfactor.value = rs[0][1];
		fm.Grpdiscountfactor.value = rs[0][2];
		fm.Totaldiscountfactor.value=rs[0][3];
		fm.PremMult.value=rs[0][4];
		fm.PremMultName.value=rs[0][5];
		fm.DiscountMode.value=rs[0][6];
		if(rs[0][6] == "2"){
			fm.all('AppointDiscountID').style.display = "";
			fm.all('AppointDiscountID2').style.display = "";
			fm.AppointDiscount.value=rs[0][3];
			fm.all('Supdiscountfactor').readOnly = true;
			fm.all('Grpdiscountfactor').readOnly = true;
		}
		sql = "select codename from ldcode1 where codetype='discountmode' and code='"+rs[0][6]+"'";
		rs = easyExecSql(sql);
		if(rs){
			fm.DiscountMode_Name.value=rs[0][0];
		}
	}

}
/*********************************************************************
 *  设置缴费信息
 *  参数  ：  cArr 数组
 *  返回值：  无
 *********************************************************************
 */
   function displayPay(cArr)
   {
   	try { fm.all('PayIntv1').value = cArr[8]; } catch(ex) { };
  	try { fm.all('PayMode').value = cArr[9]; } catch(ex) { };
  	try { fm.all('PaytoDate').value = cArr[10];} catch(ex) { };
  	try { fm.all('PaytoDate2').value = cArr[10];} catch(ex) { };
  	try { fm.all('BankCode').value = cArr[11]; } catch(ex) { };
  	try { fm.all('BankAccNo').value = cArr[12]; } catch(ex) { };
  	try { fm.all('AccName').value = cArr[13]; } catch(ex) { };  
   }
/*********************************************************************
 *  通过业务员代码，设置业务员信息
 *  参数  ：  cAgentCode : 业务员代码
 *  返回值：  无
 *********************************************************************
 */
  function displayAgent(cAgentCode)  
 {  	  
	var cArr = new Array();
	var arrReturn = new Array();	
	cArr = easyExecSql("select Name,BranchCode,Phone,PostalAddress,ZipCode from LAAgent where AgentCode=getAgentCode('" +cAgentCode + "')");
	if (cArr == null) {
	    alert("未查到业务员信息");
	} else {	
		try { fm.all('AgentName').value = cArr[0][0]; } catch(ex) { };
		try { fm.all('BranchCode').value = cArr[0][1]; } catch(ex) { };
		try { fm.all('Phone').value = cArr[0][2]; } catch(ex) { };
		try { fm.all('PostalAddress').value = cArr[0][3]; } catch(ex) { };
		try { fm.all('ZipCode').value = cArr[0][4]; } catch(ex) { }; 	
	}  		
} 
/*********************************************************************
 *  被保人信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function InsuredQueryClick()  
 {  
	 var tSel = PolGrid.getSelNo();	
	 if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	 else
	{
		 var cInsuredNo = PolGrid.getRowColData( tSel - 1, 3 );
		  parent.fraInterface.window.location = "./ContInsuredInput.jsp?cInsuredNo=" + cInsuredNo + "&ContNo=" + tContNo + "&ContType=" + tContType;
	}
 }
 /*********************************************************************
 *  险种明细查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
  function riskQueryClick()  
 {  
	 var tSel = PolGrid.getSelNo();	
	 if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	 else
	{
		 var cInsuredNo = PolGrid.getRowColDataByName(tSel - 1,"InsuredNo");		
		 var cPolNo = PolGrid.getRowColDataByName( tSel - 1, "PolNo");
		 var cRiskSeqNo = PolGrid.getRowColDataByName( tSel - 1, "RiskSeq");
		 
		  window.open("./LCProposalInput.jsp?InsuredNo=" 
			  + cInsuredNo + "&ContNo=" + tContNo+"&PolNo=" + cPolNo + "&RiskSeqNo="
		      + cRiskSeqNo + "&LogFlag=1"  + "&ContType=" + tContType);
	}
 }

/*********************************************************************
 *  保单明细缴费查看
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function payQueryClick()
{
    window.open("../sys/LCIndiPayQuery.jsp?ContNo=" + tContNo + "&ContLoadFlag=1"+ "&ContType=" + tContType);										
}

/*********************************************************************
 *  客户服务信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function taskQueryClick()
{
	//var tCustomer = trim(fm.all('AppntNo').value); 
	//if (tCustomer=="" || tCustomer==null)
	//{}else
	//{
	//	window.open("../sys/TaskPersonalBox.jsp?ContNo=" + tContNo + "&IsCancelPolFlag=" + tIsCancelPolFlag + "&Customer=" + tCustomer);
	//}
	
		window.open("EdorListMain.jsp?ContNo=" + tContNo + "&AppObj=I"); //团单
}

/*********************************************************************
 *  理赔明细信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function claimQueryClick()
{
		window.open("ClaimListMain.jsp?ContNo=" + tContNo + "&AppObj=I"); //团单
}

/*********************************************************************
 *  客户服务信息查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function serviceClick()
{
  var strSql = "select AppntNo from LCCont where ContNo='" + tContNo + "'"
        	  + " union all select AppntNo from LBCont where ContNo='" + tContNo + "' ";
  var arrReturn = new Array();
  arrReturn = easyExecSql(strSql);
  var tCustomer = arrReturn[0][0];
  
  win = window.open("../task/TaskCustomer.jsp?CustomerNo="+tCustomer, "CustomerSys");
  win.focus();
  
  //window.open("../sys/TaskPersonalBox.jsp?ContNo=" + tContNo+ "&Customer=" + tCustomer);	
}

/*********************************************************************
 *  返回
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GoBack(){
	//此方法工单复用，修改请慎重,yangyalin
	if(top.loadFlag != "TASK")
	{
		//top.opener.easyQueryClick();
		top.opener.focus();
		top.close();
	}
	else
	{
		//工单用，修改请慎重
		top.close();
		top.opener.focus();
	}
}

/*********************************************************************
 *  扫描件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ScanQuery()
{
	var arrReturn = new Array();
	var arrReturn1 = new Array();
    var con = tContNo;
	var tBussNoType = "";
	var tBussType = "";
    var tSubType = "";

	//获的个人保单印刷号
    if (tContType=='1')
	{
		 strTableName = "LCCont";
	}else if (tContType=='2')
  	{  
		 strTableName = "LBCont";
	}
    var strSql = "select PrtNo from " + strTableName+ " where ContNo='" + tContNo + "'";
	arrReturn = easyExecSql(strSql);
	if (arrReturn == null)
	{
		alert("没有查询到该合同号，请重新输入");
		return false;
	}
	else
	{
	var strPrtNo = arrReturn[0][0];
	var strSQL = "select a.docid,a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
					+ " a.maketime,a.modifydate,a.modifytime,a.docid "
					+ " from es_doc_main a ,es_doc_def b where 1=1 "
					+ " and a.subtype = b.subtype "
				  + " and a.doccode like '"+strPrtNo+"%%'"
				  + " and a.busstype='TB'"
				  + " and b.busstype='TB'";	
	var arrReturn4 = new Array();
  	arrReturn4 = easyExecSql(strSQL);
  	if(arrReturn4 != null)
  	{   
  	    var cDocID = arrReturn4[0][0];
  	    var cDocCode = arrReturn4[0][1];
  	    var	cBussTpye = "TB" ;
  	    var cSubTpye = arrReturn4[0][2];
  	    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
  	}else{
  	   alert("该印刷号没有投保书影像件！");
  	  }
	}		     
}
//王珑修改  

//打印投保人帐户历史轨迹清单
function printAppAccTraceList()
{
	var sql = "select appntNo from LCAppnt where ContNo = '" + tContNo + "' ";
	var result = easyExecSql(sql);
	if (!result)
	{
		alert("投保人信息查询错误！");
		return;
	}
	var customerNo = result[0][0];
  window.open("../bq/BqAppAccListMain.jsp?ContType=0&CustomerNo=" + customerNo);
}
//Modify byfuxin  显示银代和中介信息

function setBankInfo(cArr)
{
	try { fm.all('BankName').value = cArr[19]; } catch(ex) { };  
	try { fm.all('agency').value = cArr[20]; } catch(ex) { };  
}
/*********************************************************************
 *  万能帐户信息
 *  参数  ：  无
 *  返回值：  无
 *  添加日期：2007-12-20
 *********************************************************************
 */
function omnipotenceAcc()
{
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择一条记录！");
  }
  else
  {
    var cPolNo = PolGrid.getRowColDataByName( tSel - 1, "PolNo");
    var cRiskCode = PolGrid.getRowColData( tSel - 1, 5);
    var sql = "select 1 from LMRiskApp where RiskCode = '" + cRiskCode + "' and RiskType4 = '4' ";
    var rs = easyExecSql(sql);
    if(rs == null || rs[0][0] == null || rs[0][0] == "")
    {
      alert( "此险种不是万能险！");
    }
    else
    {
  	  var sql = "select min(InsuAccNo) from LMRiskToAcc where RiskCode = '" + cRiskCode + "'";
  	  var arrReturn = easyExecSql(sql);
  	  var cInsuAccNo = arrReturn[0][0];
      window.open("../bq/OmnipotenceAcc.jsp?PolNo=" + cPolNo + "&InsuAccNo=" + cInsuAccNo);
  	}
  }
}
function bonusShareQuery()
{
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择一条记录！");
    return;
  }
  else
  {
    var cPolNo = PolGrid.getRowColDataByName( tSel - 1, "PolNo");
    var cRiskCode = PolGrid.getRowColData( tSel - 1, 5);
    var sql = "select 1 from LMRiskApp where RiskCode = '" + cRiskCode + "' and RiskType4 = '2' ";
    var rs = easyExecSql(sql);
    if(rs == null || rs[0][0] == null || rs[0][0] == "")
    {
      alert( "此险种不是分红险！");
      return;
    }
    else
    {
      window.open("../bq/BonusShareQuery.jsp?PolNo=" + cPolNo);
  	}
  }
}

function setHuan()
{
  var tHuan = "";
  var sql = "select standbyflag1 from lcpol a where contno = '" +tContNo + "' and exists (select 1 from "
          + "lmriskapp where riskcode = a.riskcode and risktype4 = '4') " ;
  var rs = easyExecSql(sql);
  if(rs == null || rs[0][0] == null || rs[0][0] == "")
  {
    tHuan = "否";
  }
  else
  {
    tHuan = "是";
  }
  fm.Huan.value = tHuan;
}

//设置交叉销售的查询信息
function initCrosalexp()
{
	var sqlinitCrosale = "select Crs_SaleChnl, "+ //1.	交叉销售渠道：
                    " Crs_BussType, "+ //2.	交叉销售业务类型：
                    " GrpAgentCom, "+  //3.	对方机构代码：
                    " GrpAgentCom, "+ //4.	对方机构名称：
                    " GrpAgentCode, "+ //5.	对方业务员代码：
                    " GrpAgentName, "+ //6.	对方业务员姓名：
                    " GrpAgentIDNo "+  //7.	对方业务员身份证：
	          		" from lbcont a " +
	          		" where contno ='" + tContNo + "'" +
	          		" union   " +
	          		"select Crs_SaleChnl, "+ //1.	交叉销售渠道：
                    " Crs_BussType, "+ //2.	交叉销售业务类型：
                    " GrpAgentCom, "+  //3.	对方机构代码：
                    " GrpAgentCom, "+ //4.	对方机构名称：
                    " GrpAgentCode, "+ //5.	对方业务员代码：
                    " GrpAgentName, "+ //6.	对方业务员姓名：
                    " GrpAgentIDNo "+  //7.	对方业务员身份证：
	          		" from lccont a " +
	          		" where contno ='" + tContNo + "' with ur" ;
	          		
      var arrResultCrosale = easyExecSql(sqlinitCrosale);
      
      if(arrResultCrosale[0][0]=='')
      {
      	fm.all('Crs_SaleChnl').value = ''; //1.	交叉销售渠道：
      fm.all('Crs_SaleChnlName').value = '';//1.1	交叉销售渠道名称：      
      fm.all('Crs_BussType').value = '';//2.	交叉销售业务类型：
      fm.all('Crs_BussTypeName').value = ''; //2.1	交叉销售业务类型名称     
      fm.all('GrpAgentCom').value = '';//3.	对方机构代码：
      fm.all('GrpAgentComName').value = '';//3.1	对方机构名称：      
      fm.all('GrpAgentCode').value = '';//5.	对方业务员代码
      fm.all('GrpAgentName').value = '';//6.	对方业务员姓名：      
      fm.all('GrpAgentIDNo').value = '';  //7.	对方业务员身份证：
      
      return true;
      
      }
      else
      {
      var sqlcrs_salechnl="select  CodeName  from ldcode where  codetype = 'crs_salechnl' and code='"+arrResultCrosale[0][0]+"' ";
      
      var arrResultCrosale2 = easyExecSql(sqlcrs_salechnl);
      
       var sqlcrs_busstype="select  CodeName  from ldcode where  codetype = 'crs_busstype' and code='"+arrResultCrosale[0][1]+"' ";
      
      var arrResultCrosale3 = easyExecSql(sqlcrs_busstype);
      
      var sqlGrpAgentCom=" select  Under_Orgname  from lomixcom  where grpagentcom ='"+arrResultCrosale[0][2]+"'";
      
       var arrResultCrosale4 = easyExecSql(sqlGrpAgentCom);

      fm.all('Crs_SaleChnl').value = arrResultCrosale[0][0]; //1.	交叉销售渠道：
      fm.all('Crs_SaleChnlName').value = arrResultCrosale2[0][0];//1.1	交叉销售渠道名称：      
      fm.all('Crs_BussType').value = arrResultCrosale[0][1];//2.	交叉销售业务类型：
      fm.all('Crs_BussTypeName').value = arrResultCrosale3[0][0]; //2.1	交叉销售业务类型名称     
      fm.all('GrpAgentCom').value = arrResultCrosale[0][2];//3.	对方机构代码：
      fm.all('GrpAgentComName').value = arrResultCrosale4[0][0];//3.1	对方机构名称：      
      fm.all('GrpAgentCode').value = arrResultCrosale[0][4];//5.	对方业务员代码
      fm.all('GrpAgentName').value = arrResultCrosale[0][5];//6.	对方业务员姓名：      
      fm.all('GrpAgentIDNo').value = arrResultCrosale[0][6];  //7.	对方业务员身份证：
      }
}