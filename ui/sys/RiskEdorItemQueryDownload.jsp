<%@page import="com.f1j.swing.tools.tt"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProjectReportQueryDownLoadDynamic.jsp
//程序功能：项目制管理-->项目综合-->项目信息查询报表
//创建日期：2012-06-27
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "险种保全项目信息查询报表_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String querySql = request.getParameter("querySql");
    querySql = querySql.replaceAll("%25","%");
    //设置表头
    ExeSQL exe=new ExeSQL();
	SSRS outTitle=exe.execSQL("select distinct edorcode,edorname from lmedoritem where 1=1 order by edorcode with ur");
	int totalcol=outTitle.MaxRow+2;
    String[][] tTitle = new String[1][totalcol];
	tTitle[0][0]="团单项目";
	tTitle[0][1]="项目名称";
    for(int i=0;i<outTitle.MaxRow;i++)
    	tTitle[0][i+2]=outTitle.GetText(i+1, 2)+"("+outTitle.GetText(i+1, 1)+")";
    //表头的显示属性
    int []displayTitle = new int[totalcol];
    for(int i=0;i<totalcol;i++)
    	displayTitle[i]=i+1;
    
    //数据的显示属性
    int []displayData  = new int[totalcol];
    for(int i=0;i<totalcol;i++)
    	displayData[i]=i+1;
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
        createexcellist.setRowColOffset(row,0);//设置偏移
    if(createexcellist.setData(querySql,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>
