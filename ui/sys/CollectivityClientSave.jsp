<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
 <%
//程序名称：LDGrpSave.jsp
//程序功能：
//创建日期：2002-1-7
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%> 
  <%@page import="java.lang.*"%>    
<%

  //接收信息，并作校验处理。
  //输入参数
  System.out.println("start Save");
  LDGrpSchema tLDGrpSchema   = new LDGrpSchema();
  LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema();
  LDGrpUI tLDGrpUI   = new LDGrpUI();

  //输出参数
  CErrors tError = null;
  String tBmCert = "";
  //后面要执行的动作：添加，修改，删除
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
   String Operator  = tGI.Operator ;  //保存登陆管理员账号
   //String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码

  String transact=request.getParameter("Transact");
System.out.println("transact:"+transact);
   
    if(transact.equals("INSERT"))
    {
    //生成客户号
    /*
    String tLimit=PubFun.getNoLimit(ManageCom);
    String GrpNo=PubFun1.CreateMaxNo("GrpNo",tLimit);           
    tLDGrpSchema.setGrpNo(GrpNo);
    */
    }
    else
    {
    tLDGrpSchema.setCustomerNo(request.getParameter("GrpNo"));    
    }
    tLDGrpSchema.setPassword(request.getParameter("Password"));
    System.out.println("password"+tLDGrpSchema.getPassword());
    tLDGrpSchema.setGrpName(request.getParameter("GrpName"));
    tLDGrpSchema.setBusinessType(request.getParameter("BusinessType"));
    tLDGrpSchema.setGrpNature(request.getParameter("GrpNature"));
    tLDGrpSchema.setPeoples(request.getParameter("Peoples"));
    tLDGrpSchema.setRgtMoney(request.getParameter("RgtMoney"));
    tLDGrpSchema.setAsset(request.getParameter("Asset"));
    tLDGrpSchema.setNetProfitRate(request.getParameter("NetProfitRate"));       
    tLDGrpSchema.setMainBussiness(request.getParameter("MainBussiness"));
    tLDGrpSchema.setCorporation(request.getParameter("Corporation"));
    tLDGrpSchema.setComAera(request.getParameter("ComAera"));
    tLDGrpSchema.setFax(request.getParameter("Fax"));
    tLDGrpSchema.setPhone(request.getParameter("Phone"));
    tLDGrpSchema.setGetFlag(request.getParameter("GetFlag"));
    tLDGrpSchema.setSatrap(request.getParameter("Satrap"));
    tLDGrpSchema.setEMail(request.getParameter("EMail"));    
    tLDGrpSchema.setFoundDate(request.getParameter("FoundDate"));  
    tLDGrpSchema.setBankCode(request.getParameter("BankCode"));
    tLDGrpSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLDGrpSchema.setGrpGroupNo(request.getParameter("GrpGroupNo"));
    tLDGrpSchema.setState(request.getParameter("State"));
    tLDGrpSchema.setRemark(request.getParameter("Remark"));
    tLDGrpSchema.setVIPValue(request.getParameter("VIPValue"));
    tLDGrpSchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));
    tLDGrpSchema.setSubCompanyFlag(request.getParameter("SubCompanyFlag"));
    tLDGrpSchema.setSupCustoemrNo(request.getParameter("SupCustomerNo"));
    tLDGrpSchema.setOperator(Operator);
    
    tLCGrpAddressSchema.setAddressNo(request.getParameter("GrpAddressCode"));
    tLCGrpAddressSchema.setGrpAddress(request.getParameter("GrpAddress"));
    tLCGrpAddressSchema.setGrpZipCode(request.getParameter("GrpZipCode"));
    tLCGrpAddressSchema.setLinkMan1(request.getParameter("LinkMan1"));
    tLCGrpAddressSchema.setDepartment1(request.getParameter("Department1"));
    tLCGrpAddressSchema.setHeadShip1(request.getParameter("HeadShip1"));
    tLCGrpAddressSchema.setPhone1(request.getParameter("Phone1"));
    tLCGrpAddressSchema.setE_Mail1(request.getParameter("E_Mail1"));
    tLCGrpAddressSchema.setFax1(request.getParameter("Fax1"));
    tLCGrpAddressSchema.setLinkMan2(request.getParameter("LinkMan2")); 
    tLCGrpAddressSchema.setDepartment2(request.getParameter("Department2"));
    tLCGrpAddressSchema.setHeadShip2(request.getParameter("HeadShip2"));
    tLCGrpAddressSchema.setPhone2(request.getParameter("Phone2"));
    tLCGrpAddressSchema.setE_Mail2(request.getParameter("E_Mail2"));
    tLCGrpAddressSchema.setFax2(request.getParameter("Fax2"));  
    //tLCGrpAddressSchema.setOperator(Operator);  
    System.out.println("address:"+tLCGrpAddressSchema.getGrpAddress());
    
try
	{
  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tLDGrpSchema);
   tVData.addElement(tLCGrpAddressSchema);
   tVData.addElement(tGI);
   System.out.println("size:"+tVData.size());
   
    
   //执行动作：INSERT 添加纪录，UPDATE 修改纪录，DELETE 删除纪录
   if(tLDGrpUI.submitData(tVData,transact))
  {
    		if (transact.equals("INSERT")||transact.equals("UPDATE"))
		{
		    	System.out.println("------return");
			    	
		    	tVData.clear();
		    	tVData = tLDGrpUI.getResult();
		    	System.out.println("-----size:"+tVData.size());
		    	
		    	LDGrpSchema mLDGrpSchema = new LDGrpSchema(); 
			mLDGrpSchema=(LDGrpSchema)tVData.getObjectByObjectName("LDGrpSchema",0);
			LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();
			mLCGrpAddressSchema=(LCGrpAddressSchema)tVData.getObjectByObjectName("LCGrpAddressSchema",0);
			System.out.println("test");
			
			String strGrpNo = mLDGrpSchema.getCustomerNo();
			System.out.println("jsp"+strGrpNo);
			
			System.out.println("-------addressno:"+mLCGrpAddressSchema.getAddressNo());
		%>
		<script>
		parent.fraInterface.fm.all('GrpNo').value="<%=strGrpNo%>";
		</script>
		<%
		if(mLCGrpAddressSchema.getAddressNo()!=null){
		%>
		<script>
		parent.fraInterface.fm.all('GrpAddressCode').value="<%=mLCGrpAddressSchema.getAddressNo()%>";
		</script>
		<%
		}
		}
		
	}
}
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDGrpUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
    }
}//页面有效区
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

