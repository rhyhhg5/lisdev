<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskRemarkInit.jsp
//程序功能：工单管理工单批注页面初始化
//创建日期：2005-01-19
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	//如果有复选框用复选框传参数,否则用隐藏域传参数
	//用CheckBox来判断是否是从checkbox中接收参数
    String CheckBox = request.getParameter("CheckBox"); 
    String[] WorkNo;
    if ((CheckBox != null) && (CheckBox.equals("YES")))
    {
    	WorkNo = request.getParameterValues("TaskGridChk");
    }
    else
	{
		WorkNo = new String[1];
		WorkNo[0] = request.getParameter("WorkNo");
	}
	if (WorkNo != null)
	{
		session.setAttribute("WORKNO", WorkNo);
	}
%>
<script language="JavaScript">
var WorkNo = "<%=WorkNo[0]%>";

//初始化表单
function initForm()
{
<%
  //只有一条记录时显示记录
  if (WorkNo.length == 1) 
  {
%>
	 initTaskInfo();
<%
  }
%>
}
</script>