<%
//程序名称：LDPersonBMInit.jsp
//程序功能：
//创建日期：2002-08-18
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('CustomerNo').value = '2';
    fm.all('Password').value = '1';
    fm.all('Name').value = '1';
    fm.all('Sex').value = 'm';
    fm.all('Birthday').value = '1971-1-1';
    fm.all('NativePlace').value = '';
    fm.all('Nationality').value = '';
    fm.all('Marriage').value = '';
    fm.all('MarriageDate').value = '';
    fm.all('OccupationType').value = '';
    fm.all('StartWorkDate').value = '';
    fm.all('Salary').value = 0.0;       //float 
    fm.all('Health').value = '';
    fm.all('Stature').value = 0.0;      //float 
    fm.all('Avoirdupois').value = 0.0;   //float
    fm.all('CreditGrade').value = '';
    fm.all('IDType').value = '';
    fm.all('Proterty').value = '';
    fm.all('IDNo').value = '';
    fm.all('OthIDType').value = '';
    fm.all('OthIDNo').value = '';
    fm.all('ICNo').value = '';
    fm.all('HomeAddressCode').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('PostalAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('BP').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('BankCode').value = '';
    fm.all('BankAccNo').value = '';
    fm.all('JoinCompanyDate').value = '';
    fm.all('Position').value = '';
    fm.all('GrpNo').value = '';
    fm.all('GrpName').value = '';
    fm.all('GrpPhone').value = '';
    fm.all('GrpAddressCode').value = '';
    fm.all('GrpAddress').value = '';
    fm.all('DeathDate').value = '';
    fm.all('Remark').value = '';
    fm.all('State').value = '';
    fm.all('BlacklistFlag').value = '';
    fm.all('Operator').value = 'pp';
    //执行动作 insert,update,delete,不包括query动作,因为有返回值！ 
    fm.all('Transact').value='';

  }
  catch(ex)
  {
    alert("在LDPersonBMInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LDPersonBMInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    trigger(fm.ra_important[1]);
  }
  catch(re)
  {
    alert("LDPersonBMInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>