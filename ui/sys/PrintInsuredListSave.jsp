<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PrintInsuredListSave.jsp
//程序功能：
//创建日期：2007-3-18 19:36
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%@page import="com.sinosoft.lis.pubfun.*" %>

<%
  boolean operFlag = true;
  String flagStr = "";
  String content = "";
  String outname = "";
  String outpathname = "";
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  String contType = request.getParameter("ContType");
  
  String sql = null;
  String templateName = null;
  
  TransferData tTransferData = new TransferData();
  
  if("Vali".equals(contType))
  {
    templateName = "PrtContListVali.vts";
    sql = request.getParameter("ContSql");
  }
  else
  {
    templateName = "PrintInsuredList.vts";
    sql = request.getParameter("InsuredSql");
  }
  
  tTransferData.setNameAndValue(PrtCoreBL.TemplateName, templateName);
  tTransferData.setNameAndValue(PrtCoreBL.SQL, sql);
  tTransferData.setNameAndValue("GrpContNo", request.getParameter("GrpContNo"));
  
  TextTag tTextTag = new TextTag();
  tTextTag.add("Name", request.getParameter("Name"));
  tTextTag.add("InsuredNo", request.getParameter("InsuredNo"));
  tTextTag.add("IDNo", request.getParameter("IDNo"));
  tTextTag.add("InsuredStat", request.getParameter("InsuredStat"));
  tTextTag.add("StatName", StrTool.unicodeToGBK(request.getParameter("StatName")));
  tTextTag.add("ContPlanCode", request.getParameter("ContPlanCode"));
  tTextTag.add("ContPlanName", StrTool.unicodeToGBK(request.getParameter("ContPlanName")));
  
  VData d = new VData();
  d.add(tG);
  d.add(tTransferData);
  d.add(tTextTag);
 	System.out.println("\n\nthis is ");        
  PrtInsuredListUI ui = new PrtInsuredListUI();
  XmlExport txmlExport = ui.getXmlExport(d, "");
  System.out.println("\n\n\n\n\nsssssssss" + sql);
  if(txmlExport == null)
  {
    operFlag = false;
    content = ui.mErrors.getFirstError().toString();                 
  }
  
	if (operFlag==true)
	{
//	  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
//    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();  
//    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
//    tcombineVts.output(dataStream);  
//    session.putValue("PrintVts", dataStream);
//	  session.putValue("PrintStream", txmlExport.getInputStream());
//		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");

	          Readhtml rh=new Readhtml();
	          System.out.println(txmlExport.getInputStream());
			  rh.XmlParse(txmlExport.getInputStream()); //相当于XmlExport.getInputStream();
			  
			  String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
			  String temp=realpath.substring(realpath.length()-1,realpath.length());
			  if(!temp.equals("/"))
			  {
				  realpath=realpath+"/"; 
			  }
			  String templatename=rh.getTempLateName();//模板名字
			  String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
			  System.out.println("*********************templatepathname= " + templatepathname);
			  System.out.println("************************realpath="+realpath);
			  String date=PubFun.getCurrentDate().replaceAll("-","");
			  String time=PubFun.getCurrentTime().replaceAll(":","");
			  outname="被保险人清单打印"+tG.Operator+date+time+".xls";
			  outpathname=realpath+"vtsfile/"+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
				System.out.println("outpathname"+outpathname);
				System.out.println("templatepathname为空？"+templatepathname);
				System.out.println("**++**"+realpath+"f1print/picctemplate/");
				System.out.println("templatename："+templatename);
			  rh.setReadFileAddress(templatepathname);
			  rh.setWriteFileAddress(outpathname);
			  rh.start("vtsmuch");
			  try {
					outname = java.net.URLEncoder.encode(outname, "UTF-8");
					outname = java.net.URLEncoder.encode(outname, "UTF-8");
					outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
					outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	else
	{
    	flagStr = "Fail";
  	}
%>
<a href="../f1print/download.jsp?filename=<%=outname%>&filenamepath=<%=outpathname%>">点击下载</a>