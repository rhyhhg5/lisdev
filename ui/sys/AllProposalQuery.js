//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		var cPrtNo = GrpPolGrid.getRowColData( tSel - 1, 7 );
		var cContNo = GrpPolGrid.getRowColData( tSel - 1, 1 );
		var cFlag = GrpPolGrid.getRowColData( tSel - 1, 8 );
		try
		{
			if (cFlag == '团单')
			{
				window.open("./GrpPolDetailQueryMain.jsp?PrtNo="+ cPrtNo);
			}else
			{
				window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0");	
			}
			
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}
//
function getQueryResult()
{
	var arrSelected = null;
	tRow = GrpPolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}
// 数据返回父窗口
function returnParentBQ()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResultBQ();
			//alert(arrReturn);
			
			top.opener.afterQuery( arrReturn );
			//alert("333");
			top.close();
		}
		catch(ex)
		{
			alert( "请先选择一条非空记录，再点击返回按钮。");
			//alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		
	}
}

function getQueryResultBQ()
{
	var arrSelected = null;
	var tRow = GrpPolGrid.getSelNo();
	//alert(tRow);
	if (tRow==0 || tRow==null) return arrSelected;
  
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = GrpPolGrid.getRowData(tRow-1);
	//alert(arrSelected[0][0]);
	//tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	//arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
	//设置需要返回的数组
	//arrSelected[0] = arrDataSet[tRow-1];
	//alert(arrDataSet[tRow-1]);
	return arrSelected;
}


// 王珑制作

/*********************************************************************
 *  查询按钮实现
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{

   setPersonValue();
   setGrpValue();

}

/*********************************************************************
 *  查询个单客户信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setPersonValue()
{
	
   initPersonGrid();
   var  strSQL ="";
   
   if (trim(fm.all('Flag').value) == '投保人')
  {
	   strSQL = "select  distinct LDPerson.CustomerNo,LDPerson.Name,LDPerson.Sex,LDPerson.Birthday,LDPerson.IDType,LDPerson.IDNo,LCAddress.PostalAddress"
	   + " from LDPerson,LCAddress,LCAppnt ,LCCont where 1=1 "	
	   + getWherePart( 'LCCont.ContNo','ContNo' ) 	  
	   + getWherePart( 'LCCont.PrtNo','PrtNo' )	
	   + getWherePart( 'LCCont.ManageCom','ManageCom' ) 
	   + getWherePart( 'LCCont.PrtNo','PrtNo' )	
	   + getWherePart( 'LCCont.SaleChnl','SaleChnl' ) 
	   + getWherePart( 'LDPerson.CustomerNo','CustomerNo' ) 
	   + getWherePart( 'LDPerson.Name','Name','like' ) 
	   + getWherePart( 'LDPerson.IDNo','IDNo' )		   
	   + getWherePart( 'LCAddress.HomeAddress','HomeAddress' ) 
	   + getWherePart( 'LCAddress.HomeZipCode','HomeZipCode' ) 
	   + " and LCAppnt.AppntNo=LDPerson.CustomerNo"
	   + " and  LDPerson.CustomerNo=LCAddress.CustomerNo"
	   + " and  LCAppnt.AddressNo=LCAddress.AddressNo"
	   + " and LCCont.ContNo=LCAppnt.ContNo"
	   + " and LCCont.AppntNo=LCAppnt.AppntNo"
	   + " order by  LDPerson.CustomerNo"
   }else  if (trim(fm.all('Flag').value) == '被保人')
   {
	  strSQL = "select  distinct LDPerson.CustomerNo,LDPerson.Name,LDPerson.Sex,LDPerson.Birthday,LDPerson.IDNo,LDPerson.IDType,LCAddress.PostalAddress"
	   + " from LDPerson,LCAddress,LCAppnt ,LCCont where 1=1 "	
	   + getWherePart( 'LCCont.ContNo','ContNo' ) 	  
	   + getWherePart( 'LCCont.PrtNo','PrtNo' )	
	   + getWherePart( 'LCCont.ManageCom','ManageCom' ) 
	   + getWherePart( 'LCCont.PrtNo','PrtNo' )	
	   + getWherePart( 'LCCont.SaleChnl','SaleChnl' ) 
	   + getWherePart( 'LDPerson.CustomerNo','CustomerNo' ) 
	   + getWherePart( 'LDPerson.Name','Name','like') 
	   + getWherePart( 'LDPerson.IDNo','IDNo' )	  
	   + getWherePart( 'LCAddress.HomeAddress','HomeAddress' ) 
	   + getWherePart( 'LCAddress.HomeZipCode','HomeZipCode' ) 
	   + " and LCAppnt.AppntNo=LDPerson.CustomerNo"
	   + " and  LDPerson.CustomerNo=LCAddress.CustomerNo"
	   + " and  LCAppnt.AddressNo=LCAddress.AddressNo"
	   + " and LCCont.ContNo=LCAppnt.ContNo"
	   + " and LCCont.AppntNo=LCAppnt.AppntNo"
	   + " order by  LDPerson.CustomerNo"
       
   }else
   {
	   strSQL = "select  distinct LDPerson.CustomerNo,LDPerson.Name,LDPerson.Sex,LDPerson.Birthday,LDPerson.IDType,LDPerson.IDNo,LCAddress.PostalAddress"
	   + " from LDPerson,LCAddress,LCAppnt ,LCCont where 1=1 "	   
	   + " and LCAppnt.AppntNo=LDPerson.CustomerNo"
	   + " and  LDPerson.CustomerNo=LCAddress.CustomerNo"
	   + " and  LCAppnt.AddressNo=LCAddress.AddressNo"
	   + " and LCCont.ContNo=LCAppnt.ContNo"
	   + " and LCCont.AppntNo=LCAppnt.AppntNo"	  
	   + getWherePart( 'LCCont.ContNo','ContNo' ) 	  
	   + getWherePart( 'LCCont.PrtNo','PrtNo' )	
	   + getWherePart( 'LCCont.ManageCom','ManageCom' ) 
	   + getWherePart( 'LCCont.PrtNo','PrtNo' )	
	   + getWherePart( 'LCCont.SaleChnl','SaleChnl' ) 
	   + getWherePart( 'LDPerson.CustomerNo','CustomerNo' ) 
	   + getWherePart( 'LDPerson.Name','Name','like' ) 
	   + getWherePart( 'LDPerson.IDNo','IDNo' )	  
	   + getWherePart( 'LCAddress.HomeAddress','HomeAddress' ) 
	   + getWherePart( 'LCAddress.HomeZipCode','HomeZipCode' ) 
	   + " union " 
	   + " select  distinct LDPerson.CustomerNo,LDPerson.Name,LDPerson.Sex,LDPerson.Birthday,LDPerson.IDType,LDPerson.IDNo,LCAddress.PostalAddress"
	   + " from LDPerson,LCAddress,LCAppnt ,LCCont where 1=1 "	
	   + getWherePart( 'LCCont.ContNo','ContNo' ) 	  
	   + getWherePart( 'LCCont.PrtNo','PrtNo' )	
	   + getWherePart( 'LCCont.ManageCom','ManageCom' ) 
	   + getWherePart( 'LCCont.PrtNo','PrtNo' )	
	   + getWherePart( 'LCCont.SaleChnl','SaleChnl' ) 
	   + getWherePart( 'LDPerson.CustomerNo','CustomerNo' ) 
	   + getWherePart( 'LDPerson.Name','Name','like') 
	   + getWherePart( 'LDPerson.IDNo','IDNo' )	  
	   + getWherePart( 'LCAddress.HomeAddress','HomeAddress' ) 
	   + getWherePart( 'LCAddress.HomeZipCode','HomeZipCode' ) 
	   + " and LCAppnt.AppntNo=LDPerson.CustomerNo"
	   + " and  LDPerson.CustomerNo=LCAddress.CustomerNo"
	   + " and  LCAppnt.AddressNo=LCAddress.AddressNo"
	   + " and LCCont.ContNo=LCAppnt.ContNo"
	   + " and LCCont.AppntNo=LCAppnt.AppntNo"
	 
   }
	turnPage1.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
	//判断是否查询成功
	if (!turnPage1.strQueryResult) {
		alert("数据库中没有个单客户数据！");
		return false;
	}
  
	//查询成功则拆分字符串，返回二维数组
	turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
  
	//设置初始化过的MULTILINE对象
	turnPage1.pageDisplayGrid = PersonGrid;    
		  
	//保存SQL语句
	turnPage1.strQuerySql     = strSQL; 
  
	//设置查询起始位置
	turnPage1.pageIndex = 0;  
  
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex, MAXSCREENLINES);
  
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);	  
}
/*********************************************************************
 *  查询团单客户信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setGrpValue()
{
   initGrpPersonGrid();
   var  strSQL ="";
   
   if ((trim(fm.all('Flag').value) == '投保人'||trim(fm.all('Flag').value) == '') && fm.all('IDNo').value =='' )
  {
	   strSQL = "select distinct LDGrp.CustomerNo,LDGrp.GrpName,LDGrp.Satrap,LDGrp.FoundDate"
	   + " from LDGrp,LCGrpAddress,LCGrpAppnt ,LCGrpCont where 1=1 "	
	   + getWherePart( 'LCGrpCont.GrpContNo','ContNo' ) 	  
	   + getWherePart( 'LCGrpCont.PrtNo','PrtNo' )	
	   + getWherePart( 'LCGrpCont.ManageCom','ManageCom' ) 	   
	   + getWherePart( 'LCGrpCont.SaleChnl','SaleChnl' ) 
		+ getWherePart( 'LCGrpCont.AgentCom','AgentCom' )	
	   + getWherePart( 'LDGrp.CustomerNo','CustomerNo' ) 
	   + getWherePart( 'LDGrp.Name','Name','like' ) 	   	   
	   + getWherePart( 'LCGrpAddress.GrpAddress','HomeAddress' ) 
	   + getWherePart( 'LCGrpAddress.GrpZipCode','HomeZipCode' ) 
	   + " and LCGrpAppnt.CustomerNo=LDGrp.CustomerNo"
	   + " and LDGrp.CustomerNo=LCGrpAddress.CustomerNo"
	   + " and LCGrpAppnt.AddressNo=LCGrpAddress.AddressNo"
	   + " and LCGrpCont.GrpContNo=LCGrpAppnt.GrpContNo"
	   + " and LCGrpCont.AppntNo=LCGrpAppnt.CustomerNo"
	   + " order by LDGrp.CustomerNo"
   }else  if (trim(fm.all('Flag').value) == '被保人'||fm.all('IDNo').value !='' )
   {
	   strSQL = "select * from LDGrp where 1>2"
   }
 
	turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
	//判断是否查询成功
	if (!turnPage2.strQueryResult) {
		alert("数据库中没有团单客户数据！");
		return false;
	}
  
	//查询成功则拆分字符串，返回二维数组
	turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult);
  
	//设置初始化过的MULTILINE对象
	turnPage2.pageDisplayGrid = GrpPersonGrid;    
		  
	//保存SQL语句
	turnPage2.strQuerySql     = strSQL; 
  
	//设置查询起始位置
	turnPage2.pageIndex = 0;  
  
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex, MAXSCREENLINES);
  
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage2.pageDisplayGrid);	  
}


/*********************************************************************
 *  查询保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setPolValue()
{
	// 初始化表格
	initGrpPolGrid();

   //个单客户选中客户号码
	var tSel = PersonGrid.getSelNo();

	if( tSel == 0 || tSel == null )
	{		
    }else
    {		
	    var cPerCustomerNo = PersonGrid.getRowColData( tSel - 1, 1 );
	}
	
	//团单客户选中客户号码
	var tSel2 = GrpPersonGrid.getSelNo();
	
	if( tSel2 == 0 || tSel2 == null )
	{		
    }else
	{
	    var cPerCustomerNo = GrpPersonGrid.getRowColData( tSel2 - 1, 1 );
	}

	var strSQL = "select distinct ContNo as contno,AppntName as appname,CValiDate ,'' ,AppFlag,Prem,PrtNo,'个单' as flag from LCCont where grpcontno='00000000000000000000'"

		+ getWherePart( 'ContNo' )
		+ getWherePart( 'PrtNo' )
		+ getWherePart( 'ManageCom','ManageCom','like' )
		+ getWherePart( 'SaleChnl' )
		+ getWherePart( 'AgentCom' )		
		
	if (trim(fm.all('Flag').value) == '投保人')
	{
		strSQL = strSQL + " and AppntNo='" + cPerCustomerNo + "'"
	}else if (trim(fm.all('Flag').value) == '被保人')
	{
		strSQL = strSQL + " and InsuredNo='" + cPerCustomerNo + "'"
	}else
	{
		strSQL = strSQL + " and (AppntNo='" + cPerCustomerNo + "' or InsuredNo='" + cPerCustomerNo + "')"
	}	
	

	strSQL = strSQL + " union "
		+ "select distinct GrpContNo as contno,GrpName as appname,CValiDate ,'' ,AppFlag,Prem,PrtNo,'团单' as flag from LCGrpCont LCGrpCont where 1=1 "
		+ getWherePart( 'GrpContNo','ContNo' )
		+ getWherePart( 'PrtNo')
		+ getWherePart( 'ManageCom','ManageCom','like' )
		+ getWherePart( 'SaleChnl' )
		+ getWherePart( 'AgentCom' )			
		if (trim(fm.all('Flag').value) == '投保人' ||fm.all('Flag').value == '')
		{
			strSQL = strSQL + " and AppntNo='" + cPerCustomerNo + "'"
		}else
	    {
			strSQL = strSQL + " and  1 > 2"
		}
	;	
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("数据库中没有保单数据！！");
		return false;
	}
  
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = GrpPolGrid;    
		  
	//保存SQL语句
	turnPage.strQuerySql     = strSQL; 
  
	//设置查询起始位置
	turnPage.pageIndex = 0;  
  
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}	
	
//王珑制作

