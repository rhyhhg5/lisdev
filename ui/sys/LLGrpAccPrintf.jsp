<%
//程序名称：LLCaseTraceQuerySave.jsp
//程序功能：理赔案件轨迹查询
//创建日期：2014-03-04
//创建人  ：houyd
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="java.io.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<html>
<title>
账户交易信息查询
</title>
<head>
</head>
<body>
<%
System.out.println("<-Go Into LLGrpAccPrintf.jsp->");
 
boolean operFlag = true;
String flag = "0";
String FlagStr = "";
String Content = "";
String status="";
CError cError = new CError( );
CErrors tError = null;
XmlExport txmlExport = new XmlExport();
CombineVts tcombineVts = null;
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String tOperator = tG.Operator;
String tManageCom=tG.ManageCom;

String tLLAccSql = request.getParameter("LLAccSql");
System.out.println("*********************lyc-fighting = " + tLLAccSql);


//生成文件的文件名
String fileNameB = tG.Operator + "_" + FileQueue.getFileName()+".csv";
//参数
System.out.println("ManageCom："+tManageCom);

TransferData tTransferData= new TransferData();
//传参
tTransferData.setNameAndValue("tManageCom",tManageCom);
tTransferData.setNameAndValue("tOperator",tOperator);
tTransferData.setNameAndValue("tFileNameB",fileNameB);
tTransferData.setNameAndValue("LLAccSql",tLLAccSql);


VData tVData = new VData();
tVData.addElement(tG);
tVData.addElement(tTransferData);
String CSVFileName="";

LLGrpAccPrintfBL tLLGrpAccPrintfBL = new LLGrpAccPrintfBL();
     if(!tLLGrpAccPrintfBL.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	//Content = tLLCasePolicyBLCSV.mErrors.getFirstError();  
   		//System.out.println(Content);
    }
    if (FlagStr == "") {
    	tError = tLLGrpAccPrintfBL.mErrors;
    		if (!tError.needDealError()) {
    			Content = " 保存成功! ";
    			FlagStr = "Success";
    		    System.out.println("--------成功----------");   
	            CSVFileName = tLLGrpAccPrintfBL.getMFileName() + ".csv";

	  			if ("".equals(CSVFileName)) {
					operFlag = false;
					Content = "没有得到要显示的数据文件";  
					return;
 				 	}
    			
    			
    		} else {
    			Content = " 保存失败，原因是:" + tError.getFirstError();
    			FlagStr = "Fail";
    		
    			%>
    			<%=Content%>
    			<%
    			return;
    		}
    }
    
    Readhtml rh = new Readhtml();

	String realpath = application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
	String temp = realpath.substring(realpath.length() - 1, realpath
			.length());
	if (!temp.equals("/")) {
		realpath = realpath + "/";
	}
	realpath = tLLGrpAccPrintfBL.getMFilePath();
	System.out.println("realpath:"+realpath);
	String outpathname=realpath+CSVFileName;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	//本地测试使用
	//String outpathname="F:\\"+CSVFileName;
	try {
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	String[] InputEntry = new String[1];
	InputEntry[0] = outpathname;
	System.out.println("压缩包下载文件名："+InputEntry[0]);
	String tZipFile = StrTool.replace(outpathname, ".csv", ".zip");
	System.out.println("tZipFile == " + tZipFile);
	rh.CreateZipFile(InputEntry, tZipFile);
%>

<html>
<%@page contentType="text/html;charset=GBK"%>
<a
	href="../f1print/download.jsp?filename=<%=StrTool.replace(CSVFileName,".csv",".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>

