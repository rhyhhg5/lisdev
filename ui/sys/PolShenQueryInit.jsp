<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件

    fm.all('CustomerNo').value =customerNo;	
    fm.all('Name').value = '';
    fm.all('Birthday').value = '';    
    fm.all('Sex').value = '';
    fm.all('IDNo').value = '';
	  fm.all('ContNo').value = '';
	  fm.all('HomeAddress').value = '';
    fm.all('HomeZipCode').value = '';

  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm()
{
  try
  {
    initInpBox();
    initGrpPolGrid();	
	  initBContGrid();
	if (customerNo =='' || customerNo ==null)
	{}
	else
	{
		easyQueryClick();
	}
  }
  catch(re)
  {
    alert("GroupPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpPolGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[1]=new Array();
    iArray[1][0]="保单号码";         		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="投保人客户号";         		//列名
    iArray[2][1]="0px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=3; 
  
    iArray[3]=new Array();
    iArray[3][0]="被保人客户号";         		//列名
    iArray[3][1]="0px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=3; 
    
    iArray[4]=new Array();
    iArray[4][0]="标志";         		//列名
    iArray[4][1]="30px";            		//列宽                                              
    iArray[4][2]=100;            			//列最大值                                              
    iArray[4][3]=0;                   
    
    iArray[5]=new Array();
    iArray[5][0]="客户属性";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[6]=new Array();
    iArray[6][0]="投保人";         		//列名
    iArray[6][1]="140px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="投保时间";         		//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[8]=new Array();
    iArray[8][0]="总保费";         		//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[9]=new Array();
    iArray[9][0]="保单状态";         		//列名
    iArray[9][1]="50px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[10]=new Array();
    iArray[10][0]="缴至日期";         		//列名
    iArray[10][1]="80px";            		//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="终止原因";         		//列名
    iArray[11][1]="0px";            		//列宽
    iArray[11][2]=100;            			//列最大值
    iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="退保原因";         		//列名
    iArray[12][1]="0px";            		//列宽
    iArray[12][2]=100;            			//列最大值
    iArray[12][3]=3;      
           
                                              
    iArray[13]=new Array();
    iArray[13][0]="印刷号";         		//列名
    iArray[13][1]="0px";            		//列宽
    iArray[13][2]=100;            			//列最大值
    iArray[13][3]=3; 
  
    iArray[14]=new Array();
    iArray[14][0]="保单性质";         		//列名
    iArray[14][1]="0px";            		//列宽
    iArray[14][2]=100;            			//列最大值
    iArray[14][3]=3; 
  
    iArray[15]=new Array();
    iArray[15][0]="承保机构";         		//列名
    iArray[15][1]="90px";            		//列宽
    iArray[15][2]=100;            			//列最大值
    iArray[15][3]=0; 
    
  
    GrpPolGrid = new MulLineEnter( "fm" , "GrpPolGrid" ); 
    //这些属性必须在loadMulLine前
    GrpPolGrid.mulLineCount = 0;   
    GrpPolGrid.displayTitle = 1;
    GrpPolGrid.locked = 1;
    GrpPolGrid.canSel = 0;
    GrpPolGrid.hiddenPlus = 1;
    GrpPolGrid.hiddenSubtraction = 1;
    GrpPolGrid.loadMulLine(iArray); 
    GrpPolGrid.selBoxEventFuncName = "setContValue";
  }
  catch(ex)
  {
    alert("GroupPolQueryInit.jsp-->initGrpPolGrid函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initBContGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[1]=new Array();
    iArray[1][0]="保单号码";         		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="投保人客户号";         		//列名
    iArray[2][1]="0px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=3; 
  
    iArray[3]=new Array();
    iArray[3][0]="被保人客户号";         		//列名
    iArray[3][1]="0px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=3; 
    
    iArray[4]=new Array();
    iArray[4][0]="标志";         		//列名
    iArray[4][1]="30px";            		//列宽                                              
    iArray[4][2]=100;            			//列最大值                                              
    iArray[4][3]=0;                   
    
    iArray[5]=new Array();
    iArray[5][0]="客户属性";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[6]=new Array();
    iArray[6][0]="投保人";         		//列名
    iArray[6][1]="120px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="投保时间";         		//列名
    iArray[7][1]="70px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[8]=new Array();
    iArray[8][0]="总保费";         		//列名
    iArray[8][1]="60px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[9]=new Array();
    iArray[9][0]="保单状态";         		//列名
    iArray[9][1]="50px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[10]=new Array();
    iArray[10][0]="终止时间";         		//列名
    iArray[10][1]="70px";            		//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="终止原因";         		//列名
    iArray[11][1]="60px";            		//列宽
    iArray[11][2]=100;            			//列最大值
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="退保原因";         		//列名
    iArray[12][1]="80px";            		//列宽
    iArray[12][2]=100;            			//列最大值
    iArray[12][3]=0;       
    iArray[12][4]="reason_tb";           //是否引用代码:null||""为不引用
    iArray[12][5]="11";              	        //引用代码对应第几列，'|'为分割符
    iArray[12][9]="保单状态原因|code:reason_tb&NOTNULL";
    iArray[12][18]=150;
    iArray[12][19]= 0 ;          
                                              
    iArray[13]=new Array();
    iArray[13][0]="印刷号";         		//列名
    iArray[13][1]="0px";            		//列宽
    iArray[13][2]=100;            			//列最大值
    iArray[13][3]=3; 
  
    iArray[14]=new Array();
    iArray[14][0]="保单性质";         		//列名
    iArray[14][1]="0px";            		//列宽
    iArray[14][2]=100;            			//列最大值
    iArray[14][3]=3; 
  
    iArray[15]=new Array();
    iArray[15][0]="承保机构";         		//列名
    iArray[15][1]="50px";            		//列宽
    iArray[15][2]=100;            			//列最大值
    iArray[15][3]=0; 

  
    BContGrid = new MulLineEnter( "fm" , "BContGrid" ); 
    //这些属性必须在loadMulLine前
    BContGrid.mulLineCount = 0;   
    BContGrid.displayTitle = 1;
    BContGrid.locked = 1;
    BContGrid.canSel = 0;
    BContGrid.hiddenPlus = 1;
    BContGrid.hiddenSubtraction = 1;
    BContGrid.loadMulLine(iArray); 
    BContGrid.selBoxEventFuncName = "setContValue1";
  }
  catch(ex)
  {
    alert("GroupPolQueryInit.jsp-->initBContGrid函数中发生异常:初始化界面错误!");
  }
}



</script>