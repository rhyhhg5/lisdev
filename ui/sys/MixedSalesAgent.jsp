<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head> 
    <title>对方业务员信息</title>
    
  </head>
  <body>
    <tr class="common8" id="GrpAgentComID" style="display: none">
            <td class="title8">交叉销售渠道</td>
            <td class="input8">
                <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" elementtype=nacessary />
            </td>
            <td class="title8">交叉销售业务类型</td>
            <td class="input8">
                <input class="codeNo" name="Crs_BussType" id="Crs_BussType"  ondblclick="return showCodeList('crs_busstype',[this,Crs_BussTypeName],[0,1],null,fm.all('SaleChnl').value,'salechnl',1);" onkeyup="return showCodeListKey('crs_busstype',[this,Crs_BussTypeName],[0,1],null,fm.all('SaleChnl').value,'salechnl',1);" readonly="readonly" /><input class="codename" name="Crs_BussTypeName" readonly="readonly" elementtype=nacessary/><input type =button class=cssButton value="业务说明" onclick="Crs_BussTypeHelp();">
            </td>
            <td class="title8">&nbsp;</td>
            <td class="input8">&nbsp;</td>
        </tr>
        <!-- 复核二次录入 -->	
        <tr class="common8" id="TRGrpAgentComIDR" style="display: none">
            <td class="title8">交叉销售渠道</td>
            <td class="input8">
                <input class="readonly" name="Crs_SaleChnlR" id="Crs_SaleChnlR" />
            </td>
            <td class="title8">交叉销售业务类型</td>
            <td class="input8">
                <input class="readonly" name="Crs_BussTypeR" id="Crs_BussTypeR"  />
            </td>
            <td class="title8">&nbsp;</td>
            <td class="input8">&nbsp;</td>
        </tr>
        
		<tr class=common id="GrpAgentTitleID" style="display: none ">
    		<td CLASS="title">对方业务员代码</td>
			<td CLASS="input" COLSPAN="1">
				<input NAME="GrpAgentCode" CLASS=code8 elementtype=nacessary readonly="readonly" ondblclick="otherSalesInfo();">
    		</td>
    		<td CLASS="title" >对方机构代码</td>
			<td CLASS="input" COLSPAN="1" >
    	      <Input class="common" name="GrpAgentCom" elementtype=nacessary readonly="readonly" onchange="getAgentName();">
    	    </td>
    	    
    	    <td  class= title style="display: none">对方机构名称</td>
	        <td  class= input style="display: none">
	          <Input class="common" name="GrpAgentComName" readonly="readonly" TABINDEX="-1" readonly >
	        </td> 
	        
    </tr>
    <!-- 复核二次录入 -->	
    <tr class=common id="TRGrpAgentTitleIDR" style="display: none ">
    		<td CLASS="title">对方业务员代码</td>
			<td CLASS="input" COLSPAN="1">
				<input NAME="GrpAgentCodeR" CLASS=readonly  readonly="readonly" >
    		</td>
    		<td CLASS="title" >对方机构代码</td>
			<td CLASS="input" COLSPAN="1" >
    	      <Input class="readonly" name="GrpAgentComR"  readonly="readonly" >
    	    </td>
    	    
    	    <td  class= title style="display: none">对方机构名称</td>
	        <td  class= input style="display: none">
	          <Input class="readonly" name="GrpAgentComNameR" readonly="readonly" TABINDEX="-1" readonly >
	        </td> 
	        
    </tr>
    <tr class=common id="GrpAgentTitleIDNo" style="display: none">
        <td  class="title" >对方业务员姓名</td>
	    <td  class="input" COLSPAN="1">
	        <Input  name=GrpAgentName CLASS="common" readonly="readonly" elementtype=nacessary>
	    </td>
        <td CLASS="title">对方业务员证件号码</td>
     	<td CLASS="input" COLSPAN="1">
			<input NAME="GrpAgentIDNo" CLASS="common" readonly="readonly" elementtype=nacessary>
    	</td>
    </tr>
    
    <!-- 复核二次录入 -->		
    <tr class=common id="TRGrpAgentTitleIDNoR" style="display: none">
        <td  class="title" >对方业务员姓名</td>
	    <td  class="input" COLSPAN="1">
	        <Input  name=GrpAgentNameR CLASS="readonly" readonly="readonly" >
	    </td>
        <td CLASS="title">对方业务员证件号码</td>
     	<td CLASS="input" COLSPAN="1">
			<input NAME="GrpAgentIDNoR" CLASS="readonly" readonly="readonly" >
    	</td>
    </tr>
  </body>
</html>
