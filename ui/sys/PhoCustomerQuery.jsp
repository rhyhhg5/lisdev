<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-1-22
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//修改：刘岩松
//修改日期：2004-2-17
%>
<%
	String tContNo = "";
	String tIsCancelPolFlag = "";
	String tContType = "";
	try
	{
		tContNo = request.getParameter("ContNo");
		tIsCancelPolFlag = request.getParameter("IsCancelPolFlag");
		tContType = request.getParameter("ContType");
		System.out.println("tContNodfdf="+tContNo);
	}
	catch( Exception e )
	{
		tContNo = "";
		tIsCancelPolFlag = "0";
	}
%>
<head >
<script>
	var tContNo = "<%=tContNo%>"; 
	var tIsCancelPolFlag = "<%=tIsCancelPolFlag%>";
	var tContType = "<%=tContType%>";	
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="PhoCustomer.js"></SCRIPT>
	<!--<SCRIPT src="AllProposalQuery.js"></SCRIPT>-->
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="PhoCustomerInit.jsp"%>
	
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>保单查询 </title>
</head>

<body  onload="initForm();" >
  <form  name=fm >     
    <DIV id=DivLCAppntInd STYLE="display:''">
	<table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
    		</td>
    		<td class= titleImg>
    			 投保人资料：
    		</td>
    	</tr>
    </table>
	 <table  class= common>
		<TR  class= common>
		   <TD  class= title>
			客户号
		  </TD>
		  <TD  class= input>
			<Input class= common name=AppntNo readonly >
		  </TD>
		  <TD  class= title>
			姓名
		  </TD>
		  <TD  class= input>
			<Input class= common name=ClietName elementtype=nacessary verify="投保人姓名|notnull&len<=20" readonly=true>
		  </TD>
		  <TD  class= title>
			性别
		  </TD>
		  <TD  class= input>
			<Input class="code" name=AppntSex elementtype=nacessary verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
		  </TD>
		  <TD  class= title>
			生日
		  </TD>
		  <TD  class= input>
		  <input class="common" dateFormat="short" elementtype=nacessary name="AppntBirthday" readonly >
		  </TD>          
		</TR>
		<TR  class= common> 
		  <TD  class= title>
			年龄
		  </TD>
		  <TD  class= input>
		  <input class="common" dateFormat="short" elementtype=nacessary name="AppntYear" readonly >
		  </TD> 
		  <TD  class= title>
			证件类型
		  </TD>
		  <TD  class= input>
			<Input class="code" name="AppntIDType" value="0" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
		  </TD>
		  <TD  class= title>
			证件号码
		  </TD>
		  <TD  class= input colspan=1>
			<Input class= common3 name="AppntIDNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" TABINDEX="-1" readonly>
		  </TD> 
		  <TD  class= title>
			工作单位
		  </TD>
		  <TD  class= input  >
			<Input class= common name="AppntGrpName" verify="投保人工作单位|len<=60"TABINDEX="-1" readonly >
		  </TD>		  
		</TR> 		                
		
		<TR  class= common>
		    <TD  class= title>
                与被保险人关系</TD>             
            <TD  class= input>
                <Input class="common" name="RelationToMainInsured" readonly ></TD>   
		  <TD  class= title>
			家庭地址
		  </TD>
		  <TD  class= input >
			<Input class= common name="AppntHomeAddress" verify="投保人家庭地址|len<=80" TABINDEX="-1" readonly>
		  </TD>
		  <TD  class= title>
			邮编
		  </TD>
		  <TD  class= input>
			<Input class= common name="AppntHomeZipCode" verify="投保人家庭邮政编码|zipcode" TABINDEX="-1" readonly >
		  </TD>
		   <TD  class= title>
			联系电话
		  </TD>
		  <TD  class= input>
			<Input class= common name="AppntMobile" verify="投保人移动电话|len<=15" TABINDEX="-1" readonly >
		  </TD>
		</TR>
		<TR  class= common>          
		  <TD  class= title>
			职业代码
		  </TD>
		  <TD  class= input>
			<Input class="common" name="AppntOccupationCode"readonly>
		  </TD> 
		  <TD  class= title>
			职业
		  </TD>
		  <TD  class= input>
			<Input class="common" name="AppntOccupationName"  TABINDEX="-1" readonly >
		  </TD>
		   <TD  class= title>
			地址代码
		  </TD>
		  <TD  class= input>
			<Input class=common  name="AddressNo" TABINDEX="-1" readonly>
		  </TD> 
		                  
		</TR>  
	  </table>   
	</DIV>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick= "showPage(this,divPol1);">
    		</td>
    		<td class= titleImg>
    			 被保人资料：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanInsuredGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>    			
  	</Div>
	<DIV id=DivLCInsured STYLE="display:''">  
		<table  class= common>  
		  <TR  class= common>
			<TD  class= title>
			  客户号
		    </TD>
		    <TD  class= input>
			  <Input class= common name=InsuredNo readonly >
		    </TD>
			  <TD  class= title>
				姓名
			  </TD>
			  <TD  class= input>
				<Input class= common name=Name elementtype=nacessary verify="被保险人姓名|notnull&len<=20" onblur="getallinfo();"readonly>
			  </TD>
			  <TD  class= title>
				性别
			  </TD>
			  <TD  class= input>
				<Input class="code" name=Sex elementtype=nacessary verify="被保险人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
			  </TD>
			  <TD  class= title>
				出生日期
			  </TD>
			  <TD  class= input>
			  <input class="common"  name="Birthday" readonly >
			  </TD> 			                 
			</TR>       
			<TR  class= common>
			  <TD  class= title>
			    年龄
			  </TD>
			  <TD  class= input>
			  <input class="common" dateFormat="short" elementtype=nacessary name="INsuredYear" readonly >
			  </TD>
			   <TD  class= title>
				证件类型
			  </TD>
			  <TD  class= input>
				<Input class="code" name="IDType" value="0" verify="被保险人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);" onblur="getallinfo();" >
			  </TD>
			  <TD  class= title>
				证件号码
			  </TD>
			  <TD  class= input COLSPAN=1>
				<Input class= common3 name="IDNo" readonly onblur="checkidtype();getBirthdaySexByIDNo(this.value);getallinfo();" verify="被保险人证件号码|len<=20"  >
			  </TD> 
			  <TD  class= title>
				工作单位
			  </TD>
			  <TD  class= input>
				<Input class= common3 name="GrpName" readonly >
			  </TD>			 
			</TR> 
			<TR class=common>
			    <TD class=title>
				  与投保人关系
				</TD>
				<TD class=input >
				  <Input name="RelationToAppnt" class=common readonly>
				</TD>
				<TD class=title>
				  家庭地址
				</TD>
				<TD class=input >
				  <Input name="HomeAddress" class=common readonly>
				</TD>
				<TD class=title>
				  家庭邮编
				</TD>
				<TD class=input>
				  <Input name="HomeZipCode" class=common readonly>
				</TD>
				<TD  class= title>
					移动电话
				</TD>
				<TD  class= input>
					<Input class= common name="Mobile" verify="被保险人移动电话|len<=15" readonly >
				</TD>
			</TR>
			<TR  class= common> 
			    <TD  class= title>
					职业
				</TD>
				<TD  class= input >
					<Input class="common3" name="OccupationName"  readonly >
				</TD> 
				<TD  class= title>
					职业代码
				</TD>
				<TD  class= input>
					<Input class="common3" name="OccupationCode" readonly >
			    </TD>
				
			    <TD  class= title>
					地址代码
			    </TD>                  
			  <TD  class= input>
					<Input class="common3" name="InsuredAddressNo"  readonly >
			  </TD>
			</TR>                          
		</Table> 
	</DIV>  
	<br>
	<table>
         <INPUT class=CssButton VALUE="险种明细" TYPE=button onclick="riskQueryClick();"> 		    
	     <INPUT class=CssButton VALUE="返  回" TYPE=button onclick="GoBack();"> 
  	</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script>

</script>
</html>


