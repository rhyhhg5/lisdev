<%
//程序名称：OLDUserQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 17:44:47
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./OLDUserQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./OLDUserQueryInit.jsp"%>
  <title>用户表 </title>
</head>
<body  onload="initForm();" >
  <form action="./OLDUserQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <table  class= common>
  <TR  class= common>
    <TD  class= title>
      用户编码
    </TD>
    <TD  class= input>
      <Input class= common name=UserCode >
    </TD>
    <TD  class= title>
      用户姓名
    </TD>
    <TD  class= input>
      <Input class= common name=UserName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      机构编码
    </TD>
    <TD  class= input>
      <Input class= common name=ComCode >
    </TD>
    <TD  class= title>
      口令
    </TD>
    <TD  class= input>
      <Input class= common name=Password >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      用户描述
    </TD>
    <TD  class= input>
      <Input class= common name=UserDescription >
    </TD>
    <TD  class= title>
      用户状态
    </TD>
    <TD  class= input>
      <Input class= common name=UserState >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      核保权限
    </TD>
    <TD  class= input>
      <Input class= common name=UWPopedom >
    </TD>
    <TD  class= title>
      核赔权限
    </TD>
    <TD  class= input>
      <Input class= common name=ClaimPopedom >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      其它权限
    </TD>
    <TD  class= input>
      <Input class= common name=OtherPopedom >
    </TD>
    <TD  class= title>
      首席核保标志
    </TD>
    <TD  class= input>
      <Input class= common name=PopUWFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      超级权限标志
    </TD>
    <TD  class= input>
      <Input class= common name=SuperPopedomFlag >
    </TD>
    <TD  class= title>
      操作员
    </TD>
    <TD  class= input>
      <Input class= common name=Operator >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= common name=MakeDate >
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= common name=MakeTime >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      有效开始日期
    </TD>
    <TD  class= input>
      <Input class= common name=ValidStartDate >
    </TD>
    <TD  class= title>
      有效结束日期
    </TD>
    <TD  class= input>
      <Input class= common name=ValidEndDate >
    </TD>
  </TR>
</table>

    </table>
          <INPUT VALUE="查询" TYPE=button onclick="submitForm();return false;"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUserGrid);">
    		</td>
    		<td class= titleImg>
    			 用户表结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divUserGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanUserGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button > 
      <INPUT VALUE="上一页" TYPE=button > 					
      <INPUT VALUE="下一页" TYPE=button > 
      <INPUT VALUE="尾页" TYPE=button > 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
