<%
//程序名称：OLDBlacklistQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 17:19:56
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./OLDBlacklistQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./OLDBlacklistQueryInit.jsp"%>
  <title>黑名单表 </title>
</head>
<body  onload="initForm();" >
  <form action="./OLDBlacklistQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <table  class= common>
  <TR  class= common>
    <TD  class= title>
      黑名单客户号码
    </TD>
    <TD  class= input>
      <Input class= common name=BlacklistNo >
    </TD>
    <TD  class= title>
      黑名单客户类型
    </TD>
    <TD  class= input>
      <Input class= common name=BlacklistType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      黑名单名称
    </TD>
    <TD  class= input>
      <Input class= common name=BlackName >
    </TD>
    <TD  class= title>
      黑名单操作员
    </TD>
    <TD  class= input>
      <Input class= common name=BlacklistOperator >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      黑名单日期
    </TD>
    <TD  class= input>
      <Input class= common name=BlacklistMakeDate >
    </TD>
    <TD  class= title>
      黑名单时间
    </TD>
    <TD  class= input>
      <Input class= common name=BlacklistMakeTime >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      黑名单原因描述
    </TD>
    <TD  class= input>
      <Input class= common name=BlacklistReason >
    </TD>
  </TR>
</table>

    </table>
          <INPUT VALUE="查询" TYPE=button onclick="submitForm();return false;"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBlacklistGrid);">
    		</td>
    		<td class= titleImg>
    			 黑名单表结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBlacklistGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBlacklistGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button > 
      <INPUT VALUE="上一页" TYPE=button > 					
      <INPUT VALUE="下一页" TYPE=button > 
      <INPUT VALUE="尾页" TYPE=button > 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
