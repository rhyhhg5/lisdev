<%
//程序名称：LCCustomerdRelaInfoQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-26 14:32:38
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CustomerNo').value = "";
    fm.all('RelaCustomerNo').value = "";
    fm.all('Relation').value = "";
    fm.all('Remark').value = "";
    fm.all('MakeDate').value = "";
  }
  catch(ex) {
    alert("在LCCustomerdRelaInfoQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLCCustomerdRelaInfoGrid();  
  }
  catch(re) {
    alert("LCCustomerdRelaInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LCCustomerdRelaInfoGrid;
function initLCCustomerdRelaInfoGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="客户号";         	
    iArray[1][1]="30px";         	
    iArray[1][3]=0;   
    
    iArray[2]=new Array();
    iArray[2][0]="客户姓名";         		
    iArray[2][1]="30px";         		
    iArray[2][3]=0;   
    
    iArray[3]=new Array();
    iArray[3][0]="关联客户号";         		
    iArray[3][1]="30px";         		
    iArray[3][3]=0; 
    
    iArray[4]=new Array();
    iArray[4][0]="关联客户名称";         		
    iArray[4][1]="30px";         		
    iArray[4][3]=0; 
    
    iArray[5]=new Array();
    iArray[5][0]="关联关系";         		
    iArray[5][1]="30px";         		
    iArray[5][3]=2;  
    iArray[5][4]="Relation";
     
    iArray[6]=new Array();
    iArray[6][0]="备注";         		
    iArray[6][1]="30px";         		
    iArray[6][3]=0;  

      
    iArray[7]=new Array();
    iArray[7][0]="入机日期";         		
    iArray[7][1]="30px";         		
    iArray[7][3]=0;           		     		

    
    LCCustomerdRelaInfoGrid = new MulLineEnter( "fm" , "LCCustomerdRelaInfoGrid" ); 
    //这些属性必须在loadMulLine前
    LCCustomerdRelaInfoGrid.mulLineCount = 0;   
    LCCustomerdRelaInfoGrid.displayTitle = 1;
    LCCustomerdRelaInfoGrid.hiddenPlus = 1;
    LCCustomerdRelaInfoGrid.hiddenSubtraction = 1;
    LCCustomerdRelaInfoGrid.canSel = 1;
    LCCustomerdRelaInfoGrid.canChk = 0;
    LCCustomerdRelaInfoGrid.selBoxEventFuncName = "showOne";
    
    LCCustomerdRelaInfoGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LCCustomerdRelaInfoGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
