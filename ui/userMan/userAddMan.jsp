
<%@page contentType="text/html;charset=GBK" %>


<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.menumang.*"%>
<%@page import="com.sinosoft.lis.userMan.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
           
<SCRIPT src="UserAdd.js"></SCRIPT>
<html>
<head> 
 <%@include file="../common/jsp/UsrCheck.jsp"%>
</head>
  
<body>
<% 
   String FlagStr = "false";
   String action = request.getParameter("Action");
   String underStr = request.getParameter("UnderStr");
   System.out.println("action:"+action);
   String Result = "";
   if (action.compareTo("query") == 0) {
       System.out.println("<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>");
       LDUserSchema userSchema = new LDUserSchema();
       String tManageCom=request.getParameter("MngCom");
       System.out.println(tManageCom);
       userSchema.setUserCode(request.getParameter("UserCode"));
       if(tManageCom!=null&&!"".equals(tManageCom)){
    	   userSchema.setComCode(tManageCom);
       }
       else{
    	   userSchema.setComCode(request.getParameter("OperatorComCode"));
       }
       
       userSchema.setUserName(StrTool.unicodeToGBK(request.getParameter("UserName")));
       userSchema.setCrs_Check_Status("00");
       String operator = request.getParameter("OperatorCode"); //得到正在操作的操作员
       
       VData tData = new VData();
       tData.add(userSchema);
       tData.add(operator);
    
       LDUserManUI tLDUserManUI = new LDUserManUI(); 
       if (tLDUserManUI.submitData(tData,action)) {
           FlagStr = "true"; 
           Result = (String)tLDUserManUI.getResult().get(0);
           System.out.println("-------Result : " + Result);
           
       } else {
           FlagStr = "false";
       }
   }
   
   if (action.compareTo("update") == 0 || action.compareTo("insert") == 0) {
       LDUserSchema userSchema = new LDUserSchema();
       userSchema.setUserCode(request.getParameter("UserCode"));      
System.out.println("request.getParameter('UserCode')   "+request.getParameter("UserCode"));       
       
       userSchema.setUserName(request.getParameter("UserName"));
       // this must be encrypted
       userSchema.setPassword(request.getParameter("Password"));
       
       userSchema.setComCode(request.getParameter("ComCode"));      
       userSchema.setMakeDate(request.getParameter("MakeDate"));
       userSchema.setMakeTime(request.getParameter("MakeTime"));
       userSchema.setUserDescription(request.getParameter("UserDescription"));
       userSchema.setUserState(request.getParameter("UserState"));
       userSchema.setUWPopedom(request.getParameter("UWPopedom"));
       userSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
       userSchema.setOtherPopedom(request.getParameter("OtherPopedom"));
       userSchema.setPopUWFlag(request.getParameter("PopUWFlag"));
       userSchema.setSuperPopedomFlag(request.getParameter("SuperPopedomFlag"));
       userSchema.setOperator(request.getParameter("Operator"));
       userSchema.setValidStartDate(request.getParameter("ValidStartDate"));
       userSchema.setValidEndDate(request.getParameter("ValidEndDate"));
       userSchema.setAgentCom(request.getParameter("AgentCom"));
       userSchema.setEdorPopedom("Z");
       userSchema.setCrs_Check_Status("00");
       
       String sqlStr ="";
       ExeSQL tExeSQL = new ExeSQL();
       SSRS tSSRS = new SSRS();
       if(!request.getParameter("Password").equals(request.getParameter("oldPassword"))) {
             userSchema.setPModifydate(PubFun.getCurrentDate());
       } else {
            sqlStr = "select pmodifydate from lduser where usercode='"+ request.getParameter("UserCode") +"'";
            tSSRS = tExeSQL.execSQL(sqlStr);
            userSchema.setPModifydate(tSSRS.GetText(1, 1));
       }
              
       System.out.println("userCode:" + userSchema.getUserCode());
             
       LDUserTOMenuGrpSchema userToMenuGrpSchema = new LDUserTOMenuGrpSchema(); 
 
       String tGrid1[] = request.getParameterValues("HideMenuGrpGrid11");
       String tGrid2[] = request.getParameterValues("HideMenuGrpGrid12");
       String tGridNo[] = request.getParameterValues("HideMenuGrpGrid1No");
       
       LDUserTOMenuGrpSet tSet = new LDUserTOMenuGrpSet();
       
       //新增的用户有菜单组
       if (tGridNo != null) {
           int Count = tGridNo.length; //得到接受到的记录数
       	   System.out.println("Count:" + Count);
    
           LDUserTOMenuGrpSchema tSchema;

           String UserCode = request.getParameter("UserCode");
  
           System.out.println("UserCode="+UserCode);   

           for(int index=0;index< Count;index++){
               tSchema = new LDUserTOMenuGrpSchema();
               tSchema.setUserCode(tGrid1[index]);
               tSchema.setMenuGrpCode(tGrid2[index]);
               
               System.out.println("UserCode****************************************" + tGrid1[index]);
               System.out.println("MenuGrpCode*************************************" + tGrid2[index]);
               
               tSet.add(tSchema);
           }
       
       }       
                                                         
       String operator = request.getParameter("OperatorCode"); //得到正在操作的操作员
       
       userSchema.setModifyOperator(operator);
       userSchema.setModifydate(PubFun.getCurrentDate());
       userSchema.setModifytime(PubFun.getCurrentTime());
       
       VData tData = new VData();
       tData.add(tSet);
       tData.add(userSchema); 
       tData.add(operator);   
       tData.add(underStr); 
       
       GlobalInput tG = new GlobalInput();
       tG=(GlobalInput)session.getValue("GI");
       
       tData.add(tG);//在调用理赔权限配置时需要此对象
       
System.out.println("In save UnderStr............."+underStr);       
       
       LDUserManUI tLDUserManUI = new LDUserManUI(); 
       if (tLDUserManUI.submitData(tData,action))
           FlagStr = "true"; 
       else
       {
    	   FlagStr = "false";
    	   Result=tLDUserManUI.mErrors.getFirstError();
       }
    } // end of update or insert action
    
    if (action.compareTo("delete") == 0) {
        LDUserSchema tUserSchema = new LDUserSchema();
        
        //取得指定的的待删除用户
        String tRadio[] = request.getParameterValues("InpUserGridSel"); 
        if (tRadio == null)
            System.out.println("tRadio = null"); 
        String tUserCode[] = request.getParameterValues("UserGrid2");  
        int index = 0;             
        for (; index< tRadio.length;index++) {
            if(tRadio[index].equals("1"))
               break;
        }
        System.out.println("index = " + index);
        //没有选中对象
        if (index == tRadio.length){
            System.out.println("未找到选中对象");
        } else {
            String userCode = tUserCode[index];
            //删除者编码
            String deletor = request.getParameter("OperatorCode");
            System.out.println("deletor is :" + deletor);
            System.out.println("UserCode = " + userCode);
	    tUserSchema.setUserCode(userCode);
	    VData tData = new VData();
	    tData.add(tUserSchema);
	    tData.add(deletor); 

	    LDUserManUI tLDUserManUI = new LDUserManUI();
	    if (tLDUserManUI.submitData(tData,action))
	        FlagStr = "true";
	    else
	    {
	    	   FlagStr = "false";
	    	   Result=tLDUserManUI.mErrors.getFirstError();
	    }
	}
    }
    
    if (action.compareTo("unLock") == 0) {
        System.out.println("===>>>>>>>>>>>>>===");
         LDUserSchema userSchema = new LDUserSchema();	
         userSchema.setUserCode(request.getParameter("UserCodeReadOnly"));      
         userSchema.setUserName(request.getParameter("UserNameReadOnly"));
         userSchema.setPassword(request.getParameter("resetPassword")); //重置后的密码
         String operator = request.getParameter("OperatorCode"); //得到正在操作的操作员
         
   //      System.out.println("dddddliulilililililiilliililililililil"+request.getParameter("UserCodeReadOnly")+"---"+userSchema.getUserCode());
         VData tData = new VData();
         tData.add(userSchema);
         tData.add(operator);
      
         LDUserManUI tLDUserManUI = new LDUserManUI(); 
         if (tLDUserManUI.submitData(tData,action)) {
             FlagStr = "true";          
         }   
     }
    
    
 
%>  

<script>
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Result%>");
</script>
	
</body>

</html>