
<%@page contentType="text/html;charset=GBK" %>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.agentlogon.*"%>
<%@page import="com.sinosoft.lis.encrypt.*"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>           
<SCRIPT src="AgentAdd.js"></SCRIPT>
<html>
<body>
<% 
    String FlagStr = "false";
  	GlobalInput tG1 = new GlobalInput();
	tG1=(GlobalInput)session.getValue("GI");
	String Agentcode = tG1.Operator;
	System.out.println("Agentcode" + Agentcode);
	
	LisIDEA tLisIdea = new LisIDEA();
	String oldpwd = tLisIdea.encryptString(request.getParameter("oldPwd"));
	String newpwd = tLisIdea.encryptString(request.getParameter("newPwd"));
	LAAgentSchema oLAAgentSchema = new LAAgentSchema();
	LAAgentSchema newAgentSchema = new LAAgentSchema();
	
	oLAAgentSchema.setAgentCode(Agentcode);
	oLAAgentSchema.setPassword(oldpwd);
	System.out.println("oldpwd:" + oldpwd);
	
	newAgentSchema.setAgentCode(Agentcode);
	newAgentSchema.setPassword(newpwd);
    System.out.println("newpwd:" + newpwd);
    
    
    VData tData = new VData();
    tData.add(oLAAgentSchema);
    tData.add(newAgentSchema);
    LAAgentChangePwdUI tLAAgentChangePwdUI = new LAAgentChangePwdUI();
    if (tLAAgentChangePwdUI.submitData(tData,"changePwd")) {
        FlagStr = "true";
    } else {
        FlagStr = "false";
    }    
%>  

<script>
	parent.fraInterface.afterSubmit("<%=FlagStr%>");
</script>
	
</body>

</html>