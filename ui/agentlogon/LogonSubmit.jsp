<%@ page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
</head>
<%
 //******************************************************
 // 程序名称：LogonSubmit.jsp
 // 程序功能:：处理用户登录提交
 // 最近更新人：DingZhong
 // 最近更新日期：2002-10-15
 //******************************************************
 %>
 
<%@page import="java.util.Vector"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.menumang.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.encrypt.*"%>
<%@page import="com.sinosoft.lis.agentlogon.*"%>
<%                
	boolean bSuccess = false;
	//代理人编码和密码
	String AgentCode = request.getParameter("AgentCode");
	System.out.println("************+" + AgentCode + "+*********");
	
	String Password = request.getParameter("PWD");
        
        LisIDEA tIdea = new LisIDEA();
        Password = tIdea.encryptString(Password);
	
	String StationCode = request.getParameter("StationCode");
	
//	System.out.println("Password:" + Password + "1");

	
	//用户IP
	//String Ip = request.getRemoteAddr();
	String Ip = request.getHeader("X-Forwarded-For");
		if(Ip == null || Ip.length() == 0) { 
		   Ip = request.getRemoteAddr(); 
		}
		
	String ls;	//返回的字符串
		
	if (Password.length() == 0 || AgentCode.length() == 0) {
	    bSuccess = false;
	} else {
	    VData tVData = new VData();
	    LAAgentSchema tLAAgentSchema = new LAAgentSchema();
	
	    tLAAgentSchema.setAgentCode(AgentCode);
      tLAAgentSchema.setPassword(Password);
      tLAAgentSchema.setManageCom(StationCode);
	    tVData.add(tLAAgentSchema);
	    LAAgentLoginUI tLAAgentLoginUI = new LAAgentLoginUI();
            bSuccess=tLAAgentLoginUI.submitData(tVData,"query");
    }
      
	String title=AgentCode+"您好，欢迎登录本系统！"; 
	GlobalInput tG = new GlobalInput();
	tG.Operator = AgentCode;
	tG.ComCode  = StationCode;
	tG.ManageCom = StationCode;
 	session.putValue("GI",tG);
  	GlobalInput tG1 = new GlobalInput();
	tG1=(GlobalInput)session.getValue("GI");
	System.out.println("Current Operate is "+tG1.Operator);
    System.out.println("Current ComCode is "+tG1.ComCode);  
    if(bSuccess == true) {
	   out.print(title);
%>
    <script language=javascript>
    	if(parent.fraMain.rows == "0,0,0,0,*")
    		parent.document.frames('fraTitle').showTitle();
    	if(parent.fraSet.cols==	"0%,*,0%")
      		parent.document.frames('fraTitle').showHideFrame();
      		parent.fraMenu.window.location="./FrameMenu.jsp?AgentCode=<%=AgentCode%>";		

      		
    </script>
<%
  
  }
  else
  {
    session.putValue("GI",null);  
%>
    <script language=javascript>
        alert("代理人编码/密码/管理机构输入不正确!");
    	parent.window.location ="../indexagent.jsp";
    </script>
<%

  }
%> 
</html>