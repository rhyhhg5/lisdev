<!--*****************************************************
* 程序名称：index.html
* 程序功能：显示上海人保业务处理系统画面
* 最近更新人：郭妍
* 最近更新日期：2000-08-21
*******************************************************-->

<%@ page contentType="text/html;charset=GBK" %>

<%!
	//好像把这些代码仍到javabean里面会不错的，不过感觉上应该没有多大的差异吧

	//递归父节点不为0的数据节点

	//参数，数组，父节点值，数组个数

	public String node_Item(String[][] array_node,String fa_string,int intCount)

	{

		String zzz = "";

		for (int m=0;m<intCount;m++){

			if (array_node[m][0].compareTo(fa_string) == 0){

				zzz = zzz + array_node[m][0] + "|" + array_node[m][1] + "|" + array_node[m][2] + "|" + array_node[m][3] + "|" + array_node[m][4] + "|";

				zzz = zzz + node_Item(array_node,array_node[m][1],intCount);

			}

		}

		return zzz;

	}

	//递归函数入口，父节点为0

	//参数：数组strNode，数组的个数intCount，确定循环
	public String getNodeItem(String[][] strNode, int intCount)
	{
	  	String strReturn = "";	//初始化返回字串

	  	for (int j=0;j<intCount;j++)

	  		if (strNode[j][0].compareTo("0") == 0){

	  			strReturn = strReturn + strNode[j][0] + "|" + strNode[j][1] + "|" + strNode[j][2] + "|" + strNode[j][3] + "|" + strNode[j][4] + "|";

	  			strReturn = strReturn + node_Item(strNode,strNode[j][1],intCount);

	  		}

	  	return strReturn;
	}


	//页节点处理；
	//strHref:超连接文件名; strChildDisplay:菜单项显示名称; intvalue:node_valuel;
	//目前intvalue还没有用到；
	public String getMenuItem(String strHref,String strChildDisplay, int intLevel){
		String tdItemLink = "<table class=menu><tr><td class=itemLink>";
		String strChildReturn = "";
		strChildReturn = tdItemLink + "<A class=menu href='" + strHref + "' target='fraInterface'>" + strChildDisplay + "</A></td></tr></table>";

		//返回页节点字符串
		return strChildReturn;
	}
	//前提是该数组是一个排列好的数组，否则该递归可能遗漏或死锁

	//递归函数入口，凡是父节点都自行递归，好像还存在不小的问题，不过按照表结构来说应该不用担心

	//strParent字串数组；intParent数组的递归位置；strParentSize数组的大小，即个数；
	public String getMenuParent(String[][] strParent,int intParent,int strParentSize){

		String strParentReturn = "";

		//判定是否是父节点
		if (strParent[intParent][4].compareTo(" ") == 0){

			String strParentValue = strParent[intParent][0];

			//取当前节点的父节点值
			for (int j=intParent;j<strParentSize;j++){
				//循环全部数组，查询父节点为strParentValue的节点
				if (strParent[j][0].compareTo(strParentValue) == 0){

					String clickParent = "";

					clickParent = "T" + strParent[j][1];
					strParentReturn = strParentReturn + "<table class=menu><tr><td class=titleLink onclick='showMenu(" + clickParent + ")'><hr>" + strParent[j][2] + "</td></tr></table>";
					strParentReturn = strParentReturn + "<div id='" + clickParent + "' style='display:none'>";

					//递归下一个节点是否为父节点

					strParentReturn = strParentReturn + getMenuParent(strParent,j + 1,strParentSize);

					strParentReturn = strParentReturn + "</div>";

				}

			}

		}

		else{

			String strParentValue = strParent[intParent][0];

			//循环所有属于该父节点的子节点

			for (int i=intParent;i<strParentSize;i++){

				if (strParent[i][0].compareTo(strParentValue) == 0){

					strParentReturn = strParentReturn + getMenuItem(strParent[i][4],strParent[i][2],1);

				}

			}

		}

		//返回树型菜单字符串

		return strParentReturn;
	}
%>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=GBK">
<link rel='stylesheet' type='text/css' href='../css/PiccShStandard.css'>
<!-- JavaScript脚本-->
<script language=JavaScript>
//显示菜单事件
function showMenu(divID){
	if (divID.style.display == "")
		divID.style.display="none";
	else
	divID.style.display="";
}
function loadForm(){
	var i;
	for(i=0;i<document.getElementsByTagName("A").length;i++){
		document.getElementsByTagName("A")[i].onclick = clickme;
	}
}

function clickme(){
	parent.fraSet.cols = "134,*,0%";
}

function relogon(){
	parent.fraSet.cols = "0%,*,0%";
	parent.fraInterface.window.location="LogonInput.jsp";
}
</script>
</head>

<body bgColor=#4c4091>

	<table class=menu cellspacing=0 cellpadding=0>
		<tr>
			<td class=itemLink text-align=center>重新登录</td>
		<tr>
		<tr>
			<td class=itemLink>用户密码修改</td>
		<tr>
	</table>
<%
//Java调用，传递参数为bm_usr

//String ls = "9|0|1|社保|1| |1|2|01|1| |2|32|012|444| |32|444|01232|321232|test.jsp|32|7777|sdsafd|1234|test.jsp|1|5|01|5| |5|88|015|111|tests.jsp|0|3|人保|2| |3|99|test|2|test.jsp";
String ls = "9|1|2|01|1| |0|1|社保|1| |2|32|012|444| |32|444|01232|321232|test.jsp|32|7777|sdsafd|1234|test.jsp|1|5|01|5| |5|88|015|111|tests.jsp|0|3|人保|2| |3|99|test|2|test.jsp";
int i = 0;

java.util.StringTokenizer st = new java.util.StringTokenizer(ls,"|");
String ls_count = st.nextToken();	//得到该字符串的个数

int node_count = Integer.parseInt(ls_count);	//将数值整形化

String[][] node = new String[node_count][5];	//建立一个二维数组
//给数组赋值
while(st.hasMoreTokens()) {

	node[i][0] = st.nextToken();

	node[i][1] = st.nextToken();

	node[i][2] = st.nextToken();

	node[i][3] = st.nextToken();

	node[i][4] = st.nextToken();

	i++;
}
//调用数组排序
String sl = getNodeItem(node,node_count);
//去掉返回字符串的末尾|
sl = sl.substring(0,sl.length()-1);

//根据排序正确的字符串给数组重新赋值
java.util.StringTokenizer sts = new java.util.StringTokenizer(sl,"|");
i = 0;
while(sts.hasMoreTokens()) {

	node[i][0] = sts.nextToken();

	node[i][1] = sts.nextToken();

	node[i][2] = sts.nextToken();

	node[i][3] = sts.nextToken();

	node[i][4] = sts.nextToken();

	i++;

}
//调用生成菜单函数

%>

<%=getMenuParent(node,0,node_count)%>

</body>
</html>