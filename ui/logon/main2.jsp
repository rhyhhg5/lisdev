<!-- 登陆页面 -->
<%@page contentType='text/html;charset=GBK' %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
session.putValue("GI",null);
%>
<html>
<head>
<title>Sinosoft ------ 测试系统登陆</title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<script src="../common/javascript/Common.js"></script>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<!-- 页面样式  -->
<link rel='stylesheet' type='text/css' href='../common/css/other.css'>
<script language=javascript>

function submitForm(){
	if (!achieveInit()) return false;

	if(fm.UserCode.value.length == 0){
		alert("请输入用户编码.");
		return false;
	}

	if (fm.StationCode.value.length == 0){
		alert("请选择管理机构.");
		return false;
	}
	fm.ClientURL.value = document.location;
	return true;
}

function achieveInit() {
	try {
		var tVD = top.achieveVD;
		var tEX = top.achieveEX;

		if (!(tVD && tEX) || typeof(mCs) == "undefined") {
			//alert("tVD:" + tVD + "\ntEX:" + tEX + "\nmCs:" + typeof(mCs));
			top.window.location = "../indexdd.jsp";
			alert("页面初始化未完成，请等待！");
			return false;
		}
	}
	catch(ex) {
		alert("页面初始化未错误!\ntop.window.location = '../indexdd.jsp'");
		return false;
	}
	return true;
}

function initCom(){
	var strSQL = "select comcode from lduser where usercode='"+fm.UserCode.value+"'";
	arr=easyExecSql(strSQL);
	if(arr){
		fm.StationCode.value = arr[0][0];
	}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}

MM_reloadPage(true);
</script>
<style type="text/css">
	body {FONT-FAMILY: 宋体;font-size:9pt}
	td {FONT-FAMILY: 宋体;font-size:9pt}
	input {FONT-FAMILY: 宋体;font-size:9pt}
</style>
</head>
<body leftmargin="0" topmargin="0" onload="document.fm.UserCode.focus();">
<table width="1150" height="6" border="0" align="center" cellspacing="0">
  <tr><td></td></tr>
</table>
<table width="1150" height="750" border="0" align="center" cellspacing="0" class="table">
  <tr> 
    <td align="right" valign="top" background="../common/images/Login_I_3.jpg"> 
      <table width="400" height="380" border="0" cellspacing="0">
        <tr> 
          <td>&nbsp;&nbsp;</td>
        </tr>
      </table>
		<form name="fm" action="./LogonSubmit.jsp?IllFlag=dd" method="post">
        <table width="360" border="0" cellspacing="0">
          <tr> 
            <td><table width="301" border="0" cellpadding="3" cellspacing="0" height="165" align="right">
                <tr> 
                  <td width="100" align="center" valign="middle"><font size="2">用户编码</font></td>
                  <td><table width="210" border="0" cellspacing="0" height="26">
                      <tr> 
                        <td><input name=UserCode value="" class="common2" type="text" id="UserCode" size="20" maxlength="8"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td align="center" valign="middle"><font size="2">密　码</font></td>
                  <td><table width="209" border="0" cellspacing="0" height="26">
                      <tr> 
                        <td><input name=PWD value="" class="common2" type="Password" id="PWD" size="20" maxlength="20" onfocus="initCom();"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr>  
                  <td align="center" valign="middle"><font size="2">管理机构</font></td>
                  <td><table width="209" border="0" cellspacing="0" height="26">
                      <tr> 
                        <td><input name=StationCode value="" type="text" class="code2" id="StationCode" onDblClick="if (achieveInit()) showCodeList('comcode',[this]);"  onKeyUp="return showCodeListKey('comcode',[this]);" size="20"maxlength="8"></td>
                      </tr>
                    </table></td>
                </tr>
                <tr align="left" valign="top"> 
                  <td colspan="2">&nbsp;&nbsp;<input type="image" src="../common/images/login_Yes.gif" name="submit2" width="51" height="20" onClick="return submitForm();"> 
                    <input type="image" src="../common/images/login_No.gif" name="reset2" onClick="fm.reset();return false;" width="51" height="20"></td>
                </tr>
				<tr><td colspan="2"><small><font face="Verdana"><input TYPE="hidden" name="ClientURL" value=""><br></font></small></td></tr>
              </table></td>
          </tr>
        </table>
<!--添加层-->		
		<span id="spanCode"  style="display: none; position:absolute; slategray; left: 736px; top: 264px; width: 229px; height: 44px;"> 
        </span> 
      </form> </td>
  </tr>
</table>
</body>
</html>