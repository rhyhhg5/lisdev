<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String today=com.sinosoft.lis.pubfun.PubFun.getCurrentDate();
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>考评分数录入</title>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>      
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="BKBranchAssessMarkInput.js"></SCRIPT>
  <%@include file="BKBranchAssessMarkInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
    <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>

<body onload="initForm();">
<form action="BKAgentAssessMarkSave.jsp" method="post" name="fm" target="fraSubmit" id="fm">
	<%@include file="../common/jsp/OperateAgentButton.jsp"%>
	<%@include file="../common/jsp/InputButton.jsp"%>

<table >

	<tr class=common>
		<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssess);">
		 <td class=titleImg>
      查询考核分录入
		</td>
</table>
  <Div  id= "divAssess" style= "display: ''"> 
  <table class=common align="center">
    <tr class=common> 
      <TD  class=title> 考评年月</TD>
      <TD  class=input> <Input  name=AssessDate class='common' verify="考评年月|NOTNULL&NUM&len=6" verify="考评年月|NOTNULL" > 
      </TD>    
      <TD  class=title> 执行日期</TD>
      <TD  class= input width="25%">
          <Input class= 'readonly'  readonly  name=DoneDate  verify="执行日期|NOTNULL&DATE">
      </TD> 

    </tr class=common>
    <tr class=common> 
      <TD   class=title> 营业部经理编码</TD>
      <TD   class=input> <Input  name=AgentCode  class="code"  verify="营业部经理编码|NOTNULL" id="AgentCode" verify="代理人编码|NOTNULL"
               ondblclick="QueryAgent();" onkeyup="QueryAgent();" type=hidden> 
               <Input  name=GroupAgentCode  class="code"  verify="营业部经理编码|NOTNULL" id="GroupAgentCode" verify="代理人编码|NOTNULL"
               ondblclick="QueryAgent();" onkeyup="QueryAgent();" readonly> 
      </TD>
      <TD   class=title> 营业部经理姓名</TD>
      <TD   class=input> <Input  name=AgentCodeName  class="readonly"   readonly > 
      </TD>
      <!--TD  class=title> 考评类型</TD>
       <TD  class=Input > <Input name=AssessType class='codeno'  id="AssessType" ondblclick="return showCodeList('GradeType',[this,AssessTypeName],[0,1],null,null,null,null,'160');" onkeyup="return showCodeListKey('AssessType',[this,AssessTypeName],[0,1]);" debug='100'
          ><Input name=AssessTypeName class="codename" elementtype=nacessary >
       </TD-->      
      <!--TD  class=title> 展业类型</TD>
      <TD  class=Input > <Input name=BranchType class='readonly' readonly id="BranchType" > 
      </TD-->
    </tr class=common>
    <tr class=common> 
      <!--TD  class=title>考评序号</TD>
      <TD  class=Input > <Input name=Idx class='readonly' readonly id="Idx"  ></TD-->

     
    </tr class=common>

    <tr class=common>
      <TD  class=title>活动工具管理能力</TD>
      <TD  class=input> <Input  name=ToolMark class='common'  verify="活动工具管理能力|NOTNULL&NUM"> 
      </TD>
      <TD  class=title>培训、辅导能力</TD>
      <TD  class=input> <Input  name=ChargeMark class='common'  verify="培训、辅导能力|NOTNULL&NUM"> 
      </TD>
    </tr class=common>
    <tr class=common>
      <TD  class=title>会议经营能力</TD>
      <TD  class=input> <Input  name=MeetMark class='common' verify="会议经营能力|NOTNULL&NUM"> 
      </TD>
      <TD  class=title>日常管理能力</TD>
      <TD  class=input> <Input  name=RuleMark class='common'  verify="日常管理能力|NOTNULL&NUM"> 
      </TD>
    </tr class=common>
        
  </table>
  
    <table class=common align="center">
    <tr class=common>   
      <TD  class=title>总分</TD>
      <TD  class=input> <Input  name=AssessMark class='readonly' readonly > 
      </TD>
      <TD class= common>
         <Input type=button class= cssButton name=queryb value="计算" onclick="queryMark('2');">         
      <TD>           
    </tr >
  </table>  
  </DIV>
  <table>
  <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessYear);">
     <td class=titleImg>
      年终考核分录入
     </td>
    </table>
  <Div  id= "divAssessYear" style= "display: ''"> 
  <table class=common align="center">
   
	<tr class=commom>
		<td class=title>销售人员打分总和</td>
		<td class=input><Input name=AssessMarkCount class='common' verify="分数|NUM"></td>
		<td class=title>销售人员人数</td>
		<td class=input><Input name=AssessPersonCount class='common' verify="人数|NUM"></td>
	</tr>
  </table>  
  </div>
<input type=hidden name=hideOperate value=''>
<input type=hidden name=BranchType2 value=''>
<input type=hidden name=BranchType value=''>
<input type=hidden name=AssessType value=''>
<input type=hidden name=AgentGroup value=''>
<input type=hidden name=ManageCom value=''>
<input type=hidden name=hideAssessData value=''>
<input type=hidden name=hideAgentcode value=''>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
