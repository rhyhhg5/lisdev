<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LATreeRecoverSave.jsp
//程序功能：
//创建日期：2003-7-27
//创建人  ：   zsj
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>

<%
  //输出参数
  String FlagStr = "";
  String Content = "";

  VData tVData = new VData();

  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  System.out.println("ManagCom : " + tGI.ManageCom);
  System.out.println("Operator : " + tGI.Operator);
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tIndexCalNo = request.getParameter("IndexCalNo");
      String tDealType   = request.getParameter("fmAction");
      LAAssessSchema tSelSch = new LAAssessSchema();
      System.out.println("---fmAction: " + tDealType);

      String tRadio[] = request.getParameterValues("InpAssessGridSel");
      int tCount = tRadio.length;
      int tSel = 0;
      for ( int Index=0; Index<tCount; Index++ )
      {
        if ( tRadio[Index].equals("1") )
          tSel = Index;
      }
      String tAgentCode[] = request.getParameterValues("AssessGrid1");
      String tSelAgentCode = tAgentCode[tSel];

      LAAssessDB tDB = new LAAssessDB();
      tDB.setAgentCode(tSelAgentCode);
      tDB.setIndexCalNo(tIndexCalNo);
      if ( !tDB.getInfo() )  {
        FlagStr = "Fail";
        Content = FlagStr + "操作失败，原因是：" + tDB.mErrors.getFirstError();
      }
      else
      {
        tSelSch.setSchema(tDB.getSchema());
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.add(tGI);
        tInputData.add(0,tIndexCalNo);
        tInputData.add(tSelSch);
        AssessAgentRecover tRecover = new AssessAgentRecover();
        

        //同系列职级恢复
        if ( tDealType.equals("1") )   {
          if ( !tRecover.simpleRecover(tInputData) )
          {
            FlagStr = "Fail";
            Content = FlagStr + "操作失败，原因是：" + tRecover.mErrors.getFirstError();
          }
          else
          {
            FlagStr = "Succ";
            Content = FlagStr + "操作成功！";
          }
        }


        //离职恢复
        if ( tDealType.equals("2") )   {
        String tAimGroup = request.getParameter("AimGroup");
          tInputData.add(1,tAimGroup);
          if ( !tRecover.departRecover(tInputData) )
          {
            FlagStr = "Fail";
            Content = FlagStr + "操作失败，原因是：" + tRecover.mErrors.getFirstError();
          }
          else
          {
            FlagStr = "Succ";
            Content = FlagStr + "操作成功！";
          }
        }
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
  }// tGI!=null
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
