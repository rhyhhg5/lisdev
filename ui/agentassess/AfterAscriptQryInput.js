//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function easyQueryClick()
{		
	if (verifyInput() == false)
    return false;
  // 初始化表格
  initAfterGrid();
		
  // 书写SQL语句
  var strSQL = "";
  var tBranchAttr = fm.AgentGroup.value;
  
  if ( tBranchAttr == "" || tBranchAttr == null)
  {  
  	//alert("abc");
    strSQL = "select latree.AgentCode,getbranchattr(latree.AgentGroup),latree.AgentGrade from LATree,laagent where 1=1"
           + " and laagent.agentcode = latree.agentcode "           
           + " and latree.AgentCode in ( select agentcode from laassess where 1=1"
           + " and laassess.state = '2' and branchtype = '1' "	       
	       + getWherePart( 'LAAssess.ManageCom','ManageCom' )			 
	       + getWherePart( 'LAAssess.AgentCode','AgentCode' )		   
	       + getWherePart( 'LAAssess.AgentGrade','AgentGrade' )
	       + getWherePart( 'LAAssess.IndexCalNo','IndexCalNo')
	       + ")";		
  }		   
  else  
  {
  	//alert(tBranchAttr);
    strSQL = "select AgentCode,getbranchattr(AgentGroup),AgentGrade from LATree where 1=1"
           + " and AgentCode in ( select agentcode from laassess where 1=1"
           + " and laassess.state = '2'  and branchtype = '1' "	
           + " and trim(getbranchattr(agentgroup))='" + trim(tBranchAttr) + "'"			 
	       		+ getWherePart( 'LAAssess.ManageCom','ManageCom' )			 
		   			+ getWherePart( 'LAAssess.AgentCode','AgentCode' )	
           	+ getWherePart( 'LAAssess.IndexCalNo','IndexCalNo')		   	   
		   			+ getWherePart( 'LAAssess.AgentGrade','AgentGrade')
		   			+ ")";		
  }		   
  
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组  
  turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = AfterGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}

function returnParent()
{
  top.close();
}	