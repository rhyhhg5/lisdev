<html>
<%
//程序名称：AfterAscriptQryInput.jsp
//程序功能：代理人归属录入界面
//创建时间：2003-01-21
//更新记录：更新人   更新时间   更新原因
%>
<%@page contentType="text/html;charset=GBK" %>
<head>

<!--<meta http-equiv="Content-Type" content="text/html; charset=GBK">-->
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <!--<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>-->

   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

   <SCRIPT src="./AfterAscriptQryInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="./AfterAscriptQryInit.jsp"%>
   <%@include file="../common/jsp/UsrCheck.jsp"%>

</head>

<body  onload="initForm();" >
 <form action="" method=post name=fm target="fraSubmit">  
  <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,qryCondition);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    
   <Div  id= "qryCondition" style= "display: ''">
   <table class=common> 
   <tr class=common>
    <td class=title>管理机构</td>
    <td class=input > <input class=code name=ManageCom verify="管理机构|code:comcode&NOTNULL"  
ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" 
 onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">    
    </td>
    <td class=title>代理人组别</td>
    <td class=input > <input class=common type=text name=AgentGroup></td>
   </tr>
   <tr class=common>
    <td class=title>代理人编码</td>
    <td class=input > <input class=common type=text name=AgentCode></td>
    <td class=title>代理人职级</td>
    <td class=input > <input class=common type=text name=AgentGrade></td>
   </tr>
   <tr>
    <td class=title>考核年月代码</td>
    <td class=input colspan=3> <input class=common name=IndexCalNo></td>
   </tr> 
   </table>
          <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返  回" class="cssbutton" TYPE=button onclick="returnParent();"> 		
   </div>
   
   <table>   
   <tr class=common>
    <td >
     <IMG src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this,divAfterGrid);">
    </td>
    <td class=titleImg>
    归属后代理人信息
    </td>
   </tr>
  </table>

  <div id="divAfterGrid" style="display:''">
   <table class=common>
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanAfterGrid"></span>
     </td>
    </tr>
   </table>
   	  <INPUT VALUE="首  页"  class="cssbutton" TYPE="button" onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  class="cssbutton" TYPE="button" onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  class="cssbutton" TYPE="button" onclick="turnPage.nextPage();">
      <INPUT VALUE="尾  页"  class="cssbutton" TYPE="button" onclick="turnPage.lastPage();">
  </div>
      
 </form>
 <span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>