<%
//程序名称：AgentAscriptionSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人	更新日期     更新原因/内容
%>
<%@page	contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="java.lang.reflect.*"%>
  <%@page import="java.sql.*"%>
  <%@page import="com.sinosoft.lis.bl.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
	<%@page	import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>

<%
  System.out.println("to AgentAscription Save...");
  //输出参数
  CErrors tError   = null;
  String  FlagStr  = "Fail";
  String  Content  = "";
  String  tAction  = "";
  String  tOperate = "";
  int     tCount   = 0;

  //读取Session中的全局类
  GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");

  if( tG == null )
  {
    Content="网页超时或者是没有操作员信息";
  }
	else
	{
    try{
      HDAscriptControlBLF tHDAscriptControlBLF= new HDAscriptControlBLF();
      VData tVData = new VData();
      tVData.add(tG);
      tVData.add(request.getParameter("ManageCom"));
      tVData.add(request.getParameter("IndexCalNo"));
      tVData.add(request.getParameter("BranchType"));
      tVData.add(request.getParameter("BranchType2"));
      tVData.add(request.getParameter("AgentSeries"));

      System.out.println("begin HDAscriptControlBLF.SubmitData...");
      
      if( !tHDAscriptControlBLF.submitData(tVData) )
    	{
				Content="查询失败，原因是："	+ tHDAscriptControlBLF.mErrors.getFirstError();
    	}
    	else
    	{
				Content="  归属成功！ ";
    		FlagStr="Succ";
    	}
    }
    catch (Exception ex)
    {
      	Content=" 失败，原因:" + ex.toString();
    }  
}
%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>