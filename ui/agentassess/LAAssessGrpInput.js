//               该文件中包含客户端需要处理的函数和事件
var strRT=""; //存储代理人代码记录
var arrGrade = "";
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//代码限制长度
function initEdorType(cObj)
{
	mEdorType = " 1 and #1# = #1# and length(trim(ComCode))=8 ";
	showCodeList('comcode',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " 1 and #1# = #1# and length(trim(ComCode))=8 ";
	showCodeListKey('comcode',[cObj], null, null, mEdorType, "1");
} 


//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit()) return false;
  if(!chkMulLine()) return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
    fm.all('AgentGrade').value = "";
    strRT=null;
    initLAAssessGrid();
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAAssessInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  if( verifyInput() == false ) return false;
  var tStartDate=fm.all("StartDate").value;
  if(tStartDate.substring(8,10)!='01')
  {
    alert("生效日期必须是每月1日!");
    return false ;
  }
  if (fm.all('AgentGrade').value == null || fm.all('AgentGrade').value == '')
  {
    alert("员工职级不能为空！");
    return false;
  }  
    
  //检查考评信息是否录入  
  var lineCount = 0;  
  var tempObj = fm.all('LAAssessGridNo'); //假设在表单fm中
  if (tempObj == null)
  {
    alert("请录入员工考评信息！");
    return false;
  }
  LAAssessGrid.delBlankLine("LAAssessGrid");
  lineCount = LAAssessGrid.mulLineCount;
  if (lineCount == 0)
  {
    alert("请录入员工考评信息！");
    return false;
  }else
  {
    var sValue;
    var strChkIdNo;
    var sSql;
    var tResult;
    for(var i=0;i<lineCount;i++)
    {
      sValue = LAAssessGrid.getRowColData(i,1);
      if ((trim(sValue)=='')||(sValue==null))
      {
        alert('请确定员工代码！');
        return false;
      }   
      //暂定----------------校验代理人代码-----------------------//
      sSql = "select b.AgentCode,b.AgentGroup,b.AgentSeries, " 
            +"(select trim(BranchAttr) from laBranchGroup "
            +"where agentgroup = (select branchcode from laagent where agentcode = b.agentcode)) BA " 
            +"from latree b where 1=1 "
            +"and b.AgentCode in (select AgentCode from LAAgent Where AgentState in ('01','02')"
            +getWherePart('BranchType')
            +getWherePart('BranchType2')
            +") "
            +"and b.AgentCode = getAgentCode('"+sValue+"') "
            +getWherePart('b.AgentGrade','AgentGrade')
            +" and b.managecom like '"+fm.all('ManageCom').value+"%' ";        
      tResult = easyQueryVer3(sSql,1,1,1);
      if (!tResult)
      {
         alert('代理人'+sValue+"录入错误！");
         return false;	
      }else
      {
      	 var arr = decodeEasyQueryResult(tResult);
      } 
      //----------------------------------------------------------//
      //一页的代理人代码不能重复
      for(var j=1+i;j<lineCount;j++)
      {
        if (sValue==LAAssessGrid.getRowColData(j,1))
        {
       	  alert('代理人不能重复！');
       	  return false;	
        }
      } 
      sValue = LAAssessGrid.getRowColData(i,3);
      if ((trim(sValue)=='')||(sValue==null))
      {
        alert('请确定代理人的建议职级！');
        return false;
      }
    }	
  }
  //if (!LAAssessGrid.checkValue())
    //return false;
  return true;
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


// 查询按钮
function easyQuery()
{	
  initLAAssessGrid();
 
  if (verifyInput() == false)
    return false;
    
  //校验：该年月的佣金计算过而且下月的佣金每算过，才可录入考核信息
  var strSQL = "select distinct IndexCalNo from LAWage where 1=1 "
              +"and not exists(select * From LAWage Where 1=1 "
              +getWherePart('IndexCalNo','IndexCalNo','>')              
              +getWherePart('BranchType')
              +" and managecom like '"+fm.all('ManageCom').value+"%')"
              +" and managecom like '"+fm.all('ManageCom').value+"%' "
              +getWherePart('IndexCalNo')
              +getWherePart('BranchType')
              +getWherePart('BranchType2');
  //alert(strSQL);
    
  var tResult = easyQueryVer3(strSQL,1,0,1); 
  /* 
  if (!tResult)
  {
      alert("现在无法录入该年月的考核信息！");
      return false;
  }*/
  tResult = null;
  /*---若没有做基本法考核就在此手工录入考核结果，则给出提示信息--*/
  
  //判断本管理机构下的职级本考核年月是否已正常考核
  strSQL = "select AgentGrade from LAAssessMain where 1=1 "
              +getWherePart('AgentGrade')
              +" and managecom like '"+fm.all('ManageCom').value+"%' "
              +getWherePart('IndexCalNo')
              +getWherePart('BranchType')
              +getWherePart('BranchType2');
  tResult = easyQueryVer3(strSQL,1,0,1);  

   
  var tSql = "select b.AgentCode,"
            +"(select Name from laagent where agentcode = b.agentcode),"
            +"b.AgentGroup,"
            +"(select trim(BranchAttr) from laBranchGroup "
            +"where agentgroup = (select branchcode from laagent where agentcode = b.agentcode)),"             
            +"b.AgentSeries,"
            +"b.AgentGrade "
            +"from latree b where 1=1 "
            +"and b.AgentCode in (select AgentCode from LAAgent Where AgentState in ('01','02')"
            +getWherePart('BranchType')
            +getWherePart('BranchType2')
            +") "

            +getWherePart('b.AgentGrade','AgentGrade')
            +" and b.managecom like '"+fm.all('ManageCom').value+"%' "
            +" ORDER BY b.AgentCode";
  strRT = "and branchType = #"+fm.all('BranchType').value+"#  and branchType2 = #"+fm.all('BranchType2').value+"#) and b.AgentGrade = #"+fm.all('AgentGrade').value+"# "
         +"And b.ManageCom like #"+fm.all('ManageCom').value+"%#";

  initLAAssessGrid();
  return true;  
  
}

function afterCodeSelect(cCodeName,Field)
{
   var tValue = Field.value;
   try	
   {
     if( cCodeName == "AgentGrade" )	
     {     	
        strRT=null;
     	if (verifyInput() == false) 
     	{     		
          LAAssessGrid.hiddenPlus=1;
          LAAssessGrid.hiddenSubtraction=1;
          LAAssessGrid.muLineCount = 0;
          fm.all('AgentGrade').value = "";
     	  return false;
     	}
     	
     	if (tValue!=null && trim(tValue) != "")
        {
           //为下拉框查出结果集
                 
           if (!easyQuery())
           {
           	
             return false;
           }
            
        }else
        {        	
           strRT=null;
           initLAAssessGrid();
        }
     }else if (cCodeName == 'station')
     {
     	fm.all('AgentGrade').value = "";
     	strRT=null;
        initLAAssessGrid();
     }
     else if (cCodeName == 'AgentCode2')
     {
     	//查询机构代码和代理人系列
     	
     }
   }
   catch( ex ) 
   {
     alert('员工职级选择出错!'+ex);
   }
   return true;
}
function getAgentGroup()
{
   var tAgentGrade=fm.all("AgentGrade").value;
   if(tAgentGrade==null || tAgentGrade=='')
   {
   	alert("请先录入职级！");
   	return false;   	
   }
   var tSql = "select agentgroup from labranchgroup where 1=1  and branchattr='"+fm.all("BranchAttr").value+"' "
            +getWherePart("ManageCom",'like')
            +getWherePart("BranchType")
            +getWherePart("BranchType2");
   var strQueryResult = easyQueryVer3(tSql, 1, 1, 1);
  
  //判断是否查询成功
   if (!strQueryResult) 
   {
    alert("无此展业机构！");
    fm.all('BranchAttr').value='';
    fm.all('AgentGroup').value = ''; 
    strRT = "and branchType = #"+fm.all('BranchType').value+"#  and branchType2 = #"+fm.all('BranchType2').value+"#) and b.AgentGrade = #"+fm.all('AgentGrade').value+"# "
         +"And b.ManageCom like #"+fm.all('ManageCom').value+"%#";  
    initLAAssessGrid();
    return;
   }
   //查询成功则拆分字符串，返回二维数组  
   var tArr = new Array();
   tArr = decodeEasyQueryResult(strQueryResult);  
   fm.all('AgentGroup').value =tArr[0][0];
   strRT = "and branchType = #"+fm.all('BranchType').value+"#  and branchType2 = #"+fm.all('BranchType2').value+"#) and b.AgentGrade = #"+fm.all('AgentGrade').value+"# "
         +"And b.ManageCom like #"+fm.all('ManageCom').value+"%#  and b.agentgroup=#"+fm.all('AgentGroup').value+"# ";
   
   initLAAssessGrid();
    
   return;
}
function changeYearMonth()
{
 
  fm.all('AgentGrade').value = "";
  strRT=null;
  initLAAssessGrid();
  return true; 
}

function ascriptionClick()
{
	if( verifyInput() == false ) return false;
    var newWin=window.open("./AgentAscriptionInput.html");
}

function initAgentGrade()
{  

}

function getAgentGradeStr()
{
      var tReturn = "";
      var tAgentGrade = fm.all('AgentGrade').value;
      var tBranchType = fm.all('BranchType').value;
      var tBranchType2 = fm.all('BranchType2').value;
      if (tAgentGrade != null && trim(tAgentGrade) != "")
      {
        var sSql = "select gradecode,gradename from laagentgrade where 1=1 "
                   +getWherePart("BranchType")
                   +getWherePart("BranchType2");
 
            sSql = sSql +" and gradecode <> '"+tAgentGrade+"'" ;
			        
	    sSql = sSql + " and gradeproperty2=(select gradeproperty2 from laagentgrade where 1=1 "
				        +getWherePart("BranchType")
				        +getWherePart("BranchType2")
				        +" and gradecode='"+tAgentGrade+"'  ) order by gradecode";
        var tqueryRe = easyQueryVer3(sSql,1,0,1);
        if (tqueryRe)
            tReturn = tqueryRe;
       }
 	 	 	
      return tReturn;
}

// add  new 
function chkMulLine()
{
	var i;
	var rowNum = LAAssessGrid.mulLineCount;
	var tAgent="( ";
	var checkSql ;
	
    if(rowNum == 0)
	{
		alert("至少选择一条需变更职级人员!");
		return false
	}
	for(i=0;i<rowNum;i++)
	{	
		
		 if(i ==rowNum-1)
		 {
		   tAgent += " '"+LAAssessGrid.getRowColData(i,1)+"')";
		 }else{
		   tAgent += " '"+LAAssessGrid.getRowColData(i,1)+"',";
		 }				 
	}
	checkSql ="select agentcode from latree where 1=1 and indueformflag = 'N' and agentcode in " +tAgent;
    var strQueryResult  = easyQueryVer3(checkSql, 1, 1, 1);
	if (strQueryResult)
	 {
       var arr = decodeEasyQueryResult(strQueryResult);
       if (!confirm('业务员'+arr+'尚未转正，是否确认进行职级异动！'))
	   {
		return false;	
	   }
       return true;
     }
		
	return true;
}