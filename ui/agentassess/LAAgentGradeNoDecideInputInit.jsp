<%
//程序名称：LAAgentGradeDecideInit.jsp
//程序功能：
//创建日期：2005-8-26 9:30
//创建人  ：LiuH
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

if(globalInput == null) {
	out.println("session has expired");
	return;
}

String strOperator = globalInput.Operator;
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">
var tBranchType = "<%=BranchType%>";
var tAgentTitleGrid = "营销员";
if("2"==tBranchType)
{
  tAgentTitleGrid = "业务员";
}
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';
    fm.all('IndexCalNo').value = '';
    fm.all('BranchAttr').value = '';
    fm.all('BranchName').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';   

    fm.all('BranchType').value = '<%=BranchType%>';    
    fm.all('BranchType2').value = '<%=BranchType2%>';    
    
    if (fm.all('BranchType').value == '11')
    {
      fm.AscBtn.disabled = false;
    }
    else
    {
      fm.AscBtn.disabled = true;
      divQQQQ.style.display='none';
     }
  }
  catch(ex)
  {
    alert("在LAAgentGradeDecideInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    //initInpBox();
    initEvaluateGrid();
  }
  catch(re)
  {
    alert("LAAgentGradeDecideInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 var LAAssessGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initEvaluateGrid()
{
    var iArray = new Array();
    
    try
    {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]=tAgentTitleGrid+"代码";         		//列名
    iArray[1][1]="100px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]=tAgentTitleGrid+"姓名";         		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="销售单位代码";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="销售单位名称";         		//列名
    iArray[4][1]="120px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="当前职级";      		//列名
    iArray[5][1]="40px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="确认职级";         		//列名
    iArray[6][1]="40px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许    
   
    GradeDecideGrid = new MulLineEnter( "fm" , "GradeDecideGrid" );
    //这些属性必须在loadMulLine前
    GradeDecideGrid.displayTitle = 1;
    GradeDecideGrid.canChk = 1;
    GradeDecideGrid.mulLineCount = 0;
    GradeDecideGrid.hiddenPlus=1;
    GradeDecideGrid.hiddenSubtraction=1;
    GradeDecideGrid.loadMulLine(iArray);

    //这些操作必须在loadMulLine后面
    //GradeDecideGrid.setRowColData(1,1,"asdf");
    }
    catch(ex)
    {
      alert(ex);
    }
}

</script>
