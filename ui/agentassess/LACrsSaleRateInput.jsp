<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LACrsSaleRateInput.jsp
//程序功能：个人代理增员管理
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String BranchType=request.getParameter("BranchType");
String BranchType2=request.getParameter("BranchType2");
%>
<script>
var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
var manageCom = <%=tG.ManageCom%>;

</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="LACrsSaleRateInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="LACrsSaleRateInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="./LACrsSaleRateSave.jsp" method=post name=fm target="fraSubmit">
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
				<td class=titleImg>
					  查询条件录入
				</td>
			</tr>
		</table>
	<Div  id= "divLAAgent1" style= "display: ''">
		<table class="common" align=center>
			<TR  class= common> 
				<TD  class= title>
					管理机构
				</TD>
				<TD  class= input>
					<Input class="codeno" name=ManageCom verify="管理机构|notnull"
					ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
					onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
					><Input class=codename name=ManageComName readOnly elementtype=nacessary  > 
				</TD> 
				<TD class=title>考核方式</TD>
				<TD class=input><Input name=AssessType class='codeno'  CodeData="0|^1|月均互动开拓提奖|^2|季度规模保费"
					ondblclick="return showCodeListEx('AssessType',[this,AssessTypeName],[0,1]);"
					onkeyup="return showCodeListKeyEx('AssessType',[this,AssessTypeName],[0,1]);"
					><Input
					class=codename name=AssessTypeName   readOnly></TD>
		</TR>
	</table>
</Div>
<br/>
   &nbsp;&nbsp;<INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class=cssbutton />
			   <INPUT VALUE="保  存" TYPE=button onclick="submitForm();" class=cssbutton />
			   <INPUT VALUE="重  置" TYPE=button onclick="initForm();" class=cssbutton />
			   <INPUT VALUE="修  改" TYPE=button onclick="updateClick();" class=cssbutton />
	 <table>
	 	<tr>
	 		<td class=common>
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);"></td>
 			<td class= titleImg>
	 				交叉比例录入
 			</td>
	 	</tr>
	 </table>
 <Div  id= "divLAAgent2" style= "display: ''">
 	<table  class= common>
 		<tr  class= common>
 			<td text-align: left colSpan=1>
 				<span id="spanCrsSaleRateGrid" >
 				</span>
 			</td>
 		</tr>
 	</table>
 </div>
  <font color="red">注：1.录入的系数只能为小数,如：0.02<br/>
  	&nbsp;&nbsp;&nbsp;&nbsp;2.只能修改比例
  </font>
 <Input type=hidden name=BranchType  value =<%=BranchType %>>   
 <Input type=hidden name=BranchType2 value =<%=BranchType2 %>>    
 <Input type=hidden name=hideOperate >   
 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>


