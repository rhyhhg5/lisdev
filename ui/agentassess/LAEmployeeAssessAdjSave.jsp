<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LAEmployeeAssessAdjSave.jsp
//程序功能：
//创建日期：2003-3-26
//创建人  ：zsj
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
	<SCRIPT src="LAEmployeeAssessAdjInput.js"></SCRIPT>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>

<%
  //输出参数
  String FlagStr = "";
  String Content = "";
  VData tVData = new VData();
  LAEmployeeAssessAdjUI tLAEmployeeAssessAdjUI   = new LAEmployeeAssessAdjUI();
  CErrors tError=new CErrors();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  System.out.println("ManagCom : " + tGI.ManageCom);
  System.out.println("Operator : " + tGI.Operator);
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {    
      //将界面上的录入框值取出来
      String tManageCom2  = request.getParameter("ManageCom");
      String tAgentCode2 = request.getParameter("AgentCode");
      String tAgentGrade2 = request.getParameter("AgentGrade");
      String tQueryType2 = request.getParameter("QueryType");
      String tBranchAttr2 = request.getParameter("BranchAttr");
      String tIndexCalNo2 = request.getParameter("IndexCalNo");
      String tOperate=request.getParameter("Operate"); 
     
	      if (tOperate.equals("QUERY||MAIN"))//执行查询操作
	      {
	     
	          tVData.clear();
	          tVData.add(tGI);
	          tVData.add(0,tManageCom2);
	          tVData.add(1,tAgentCode2);
	          tVData.add(2,tAgentGrade2);
	          tVData.add(3,tQueryType2);
	          tVData.add(4,tBranchAttr2);
	          tVData.add(5,tIndexCalNo2);
	          
		   try
		    {
		       if (!tLAEmployeeAssessAdjUI.submitData(tVData,tOperate))
		       {
		          Content = " 查询失败，原因是: " + tLAEmployeeAssessAdjUI.mErrors.getError(0).errorMessage;
	                  FlagStr = "Fail";  
		       }
		       else
		       {
			       tVData = tLAEmployeeAssessAdjUI.getResult();
			       SSRS tSSRS = new SSRS();	
				
		               tSSRS= ((SSRS)tVData.getObjectByObjectName("SSRS",0));
		               String StrResult = tSSRS.encode();	//将结果按位置排序
		              // if (StrResult.equals(""))
		              // StrResult="0|0^";
		              // System.out.println(StrResult);
		               
		               if (tSSRS.getMaxRow()>0)
		               {
		               Content="查询成功";
		                %>
				   	<script language="javascript">
				   	try {
				   	  parent.fraInterface.displayQueryResult('<%=StrResult%>');
				   	 // parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
				   	} catch(ex) {}
					</script>
					<%
		               }
		               else
				{
				  Content="没有符合条件的记录";
				}
		              
					
				
	               }
	            }
		     catch (Exception ex)
		    {
		      ex.printStackTrace();
		      FlagStr = "Fail";
		      Content = FlagStr + "查询失败，原因是：" + ex.toString();
		    }
	                                 
	      
	   //如果在Catch中发现异常，则不从错误类中提取错误信息
		      if (FlagStr == "Fail")
		      {
		        tError = tLAEmployeeAssessAdjUI.mErrors;
		        if (!tError.needDealError())
		        {                          
			    	//Content = " 查询成功! ";
			    	FlagStr = "Succ";
		        }
		        else                                                                           
		        {
			    	Content = " 查询失败，原因是:" + tError.getFirstError();
			    	FlagStr = "Fail";
		        }
		      }
		      else 
		      {
		  	//Content = " 查询成功! ";
		        FlagStr = "Succ";
		      }
	     }


     else if (tOperate.equals("INSERT||MAIN"))
      {
      System.out.println(tOperate); 
           LAAssessAccessorySet tSet = new LAAssessAccessorySet();
           LAAssessAccessorySchema tSchema = new LAAssessAccessorySchema(); 
        
        //将mulline字段值取出来    
        String tChk[] = request.getParameterValues("InpAgentAdjGridChk");
        String tAgentCode[] = request.getParameterValues("AgentAdjGrid1");
        String tAgentGroup[] = request.getParameterValues("AgentAdjGrid2");
        String tAgentGrade[] = request.getParameterValues("AgentAdjGrid5");
        String tCalAgentGrade[] = request.getParameterValues("AgentAdjGrid6");
        String tAgentGrade1[] = request.getParameterValues("AgentAdjGrid7");
        
        int tCount = tAgentCode.length;
        int tR = 0;
        int tIndex = 0;

        for ( tIndex=0;tIndex<tCount;tIndex++ )
        {
          if (tChk[tIndex].equals("1") )//选中一条纪录
          {
            tAgentCode[tR] = tAgentCode[tIndex];
            tAgentGroup[tR] = tAgentGroup[tIndex];
            tAgentGrade[tR] = tAgentGrade[tIndex];
            tCalAgentGrade[tR] = tCalAgentGrade[tIndex];
            tAgentGrade1[tR] = tAgentGrade1[tIndex];
            tR +=1;
          }
        }
        if ( tR == 0 )
        {
          FlagStr = "Fail";
          Content = FlagStr + "没有选中被确认的代理人！";
        }
        else  
        {	      
	      for (tIndex=0;tIndex<tR;tIndex++)
	      {
	        System.out.println(tAgentCode[tIndex]);
	        tSchema=new LAAssessAccessorySchema();
	        tSchema.setAgentCode(tAgentCode[tIndex]);
	        tSchema.setAssessType("01");
	        tSchema.setIndexCalNo(tIndexCalNo2);
	        tSchema.setAgentGroup(tBranchAttr2);
	        tSchema.setAgentGrade(tAgentGrade[tIndex]);
	        tSchema.setManageCom(tManageCom2);
	        if( tAgentGrade1[tIndex].equals(""))
	           tSchema.setAgentGrade1(tCalAgentGrade[tIndex]);
	        else
	           tSchema.setAgentGrade1(tAgentGrade1[tIndex]);
	        if(tCalAgentGrade[tIndex].equals(""))
	        {
	          
	           tSchema.setCalAgentGrade(tAgentGrade1[tIndex]);
	        }
	        else
	        {
	         tSchema.setCalAgentGrade(tCalAgentGrade[tIndex]);
	        }
	         tSet.add(tSchema);
	         
	         
              }  
              System.out.println(tSet.size());
              tVData = new VData();
              tVData.add(tSet);
               tVData.add(tGI);
              tLAEmployeeAssessAdjUI   = new LAEmployeeAssessAdjUI();
              if (!tLAEmployeeAssessAdjUI.submitData(tVData,tOperate))
	      {
	      	
		  Content = " 查询失败，原因是: " + tLAEmployeeAssessAdjUI.mErrors.getFirstError()+tLAEmployeeAssessAdjUI.mErrors.getError(0).functionName;
	          FlagStr = "Fail";  
	      }
	      if (!FlagStr.equals("Fail"))
	        Content="保存成功！";
                                                         
         }
         
     }
  }
  catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "保存失败，原因是：" + ex.toString();
    
      
    }
   
   }
   %>
    
    <html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  </script>
  </html>
   
   





