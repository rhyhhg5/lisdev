<html>
<%
//程序名称：LATreeRecoverInput.jsp
//程序功能：代理人归属后考核调整界面
//创建时间：2003-07-16
//更新记录：更新人   更新时间   更新原因
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=GBK">-->
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
 	 <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>

   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

   <SCRIPT src="LATreeRecoverInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="LATreeRecoverInit.jsp"%>
   <%@include file="../common/jsp/UsrCheck.jsp"%>

</head>

<body  onload="initForm();" >
 <form action="./LATreeRecoverSave.jsp" method=post name=fm target="fraSubmit">
  <table class=common>
   <tr class=common>
    <td class=title>管理机构</td>
    <td class=input > <input class=code name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"></td>
    <td class=title nowrap>考核年月代码</td>
    <td class=input > <input class=common type=text name=IndexCalNo></td>
   </tr>
   <tr>
    <td class=title>代理人职级</td>
    <td class=input > <input class=Code type=text name=AgentGrade 
    ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'1 and codealias = #1#','1');" 
         onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'1 and codealias = #1#','1');" >       
    </td>    
    <td class=title>代理人编码</td>
    <td class=input > <input class=common type=text name=AgentCode ></td>    
   </tr>
   <tr> 
    <td class=input width="26%" colspan=4 align=center><input type=button value="查    询" class=common onclick="easyQueryClick();"></td>
   </tr>
  </table>

  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessGrid);">
    		</td>
    		<td class= titleImg>
    			 代理人考核信息
    		</td>
    	</tr>
  </table>

  <div id="divAssessGrid" style="display:''">
   <table class=common>
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanAssessGrid"></span>
     </td>
    </tr>
   </table>
      <INPUT VALUE=" 首页 "  TYPE="button" onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" onclick="turnPage.lastPage();">
  </div>

  <table class=common>
   <tr></tr>
   <tr></tr>
   <tr class=common> 
    <!--<td class=input width="26%"> <input type=button value="机构恢复" class=common onclick="orgRecover();"></td>-->   
    <!--<td class=input width="26%"> <input type=button value="行政信息恢复及业绩回归" class=common onclick="LATreeRecover();"></td>-->
    <td class=input></td>
    <td class=input > <input type=button value="同系列职级恢复" class=common onclick="simpleRecover();"></td>    
    <td class=input > <input type=button value="降级到维持恢复" class=common onclick="orgRecover();"></td>           
    <td class=input></td>    
   </tr>
   
   <tr class=common>
    <td class=title>目标组</td>
    <td class=input><input type=text class=common  name=AimGroup></td>
    <td class=input> <input type=button value="离  职  恢  复" class=common onclick="departRecover();"></td>       
    <td class=input></td>    
   </tr>  
  </table>

   <input type=hidden id="sql_where" name="sql_where">
   <input type=hidden id="fmAction" name="fmAction">   
   <input type=hidden id="CurBranchAttr" name="CurBranchAttr">
   <input type=hidden id="UpdAgentCode" name="UpdAgentCode">
   <input type=hidden id="CurBranchAttr" name="NewBranchAttr">
   <input type=hidden id="UpdAgentCode" name="NewAgentGroup">

 </form>
 <span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>




