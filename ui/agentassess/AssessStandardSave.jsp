<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String managecom = request.getParameter("ManageCom");
  String assessType = request.getParameter("AssessType");
  String branchtype = request.getParameter("BranchType");
  String branchtype2 = request.getParameter("BranchType2");
  String FlagStr = "Fail";
  String Content = "";

  //取得录入的职级晋升信息
  int lineCount = 10;
//String arrCount[] = request.getParameterValues("AssessGrid");
  String tAgentGrade[] = request.getParameterValues("AssessGrid1");
  String tDestAgentGrade[] = request.getParameterValues("AssessGrid3");
  String tUpStandPrem[] = request.getParameterValues("AssessGrid4");
  //String tKeepAgentGrade[] = request.getParameterValues("AssessGrid5");
  String tKeepStandPrem[] = request.getParameterValues("AssessGrid6");
  //lineCount = arrCount.length; //行数
  
  LAActiveAssessStandBL tLAActiveAssessStandBL = new LAActiveAssessStandBL();
  LAAgentPromRadix5Schema tLAAgentPromRadix5Schema;
  LAAgentPromRadix5Set tLAAgentPromRadix5Set = new LAAgentPromRadix5Set();
  for(int i=0;i<lineCount;i++)
  {
  	tLAAgentPromRadix5Schema = new LAAgentPromRadix5Schema();
    tLAAgentPromRadix5Schema.setManageCom(managecom);
    tLAAgentPromRadix5Schema.setBranchType(branchtype);
    tLAAgentPromRadix5Schema.setBranchType2(branchtype2);
    tLAAgentPromRadix5Schema.setAgentGrade(tAgentGrade[i]);
    tLAAgentPromRadix5Schema.setDestAgentGrade(tDestAgentGrade[i]);
    tLAAgentPromRadix5Schema.setAreaType("11");
    if(tAgentGrade[i].equals("U10"))
    {
    	tUpStandPrem[i] = "9999999999";
    }
    tLAAgentPromRadix5Schema.setUpStandPrem(tUpStandPrem[i]);
    tLAAgentPromRadix5Schema.setKeepStandPrem(tKeepStandPrem[i]);
    tLAAgentPromRadix5Schema.setAssessType(assessType);
    tLAAgentPromRadix5Schema.setStandFlag("1");//1  是考核标准 
    tLAAgentPromRadix5Schema.setAssessState("00");//00   有效   01  无效
    tLAAgentPromRadix5Set.add(tLAAgentPromRadix5Schema);
  }

System.out.println("tOperate11:"+tOperate);
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.add(tLAAgentPromRadix5Set);

System.out.println("tOperate:"+tOperate);
  try
  {
    System.out.println("this will save the data!!!");	
    tLAActiveAssessStandBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAActiveAssessStandBL.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
