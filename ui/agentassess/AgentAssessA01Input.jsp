<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String AgentSeries=request.getParameter("AgentSeries");
  String CalFlag=request.getParameter("CalFlag");
%>
<html>  
<%
//程序名称：AgentAssessInput.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="AgentAssessA01Input.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AgentAssessA01Init.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<script>
   var msql=" 1 and branchtype=#"+'<%=tBranchType%>'+"# and branchtype2=#"+'<%=tBranchType2%>'+"# and gradecode < #B21# ";
</script>
</head>
<body  onload="initForm();initElementtype();" >    
  <form action="./AgentAssessA01Save.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		代理人每月考核计算
       		 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR>
          <TD class='title'>
           管理机构
          </TD>
          <TD class='input'>
             <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len=8"
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
             ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>                       
          </TD>
          <!--
          <TD class= title>
            考核职级
          </TD>
	        <TD class= input>
	          <Input name=AgentGrade class="codeno"  verify="考核职级|notnull&code:AgentGrade" 
	           ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
	          onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
	          ><Input name=AgentGradeName class="codename" elementtype=nacessary > 
	        </TD>
	        -->
	        <TD class= title>
            职级 
          </TD>
	        <!-- 
	        <TD class= input>
	          <Input name=AgentSeries class="codeno"  verify="职级系列|notnull" 
	           ondblclick="return showCodeList('AgentSeries',[this,AgentSeriesName],[0,1]);" 
	          onkeyup="return showCodeListKey('AgentSeries',[this,AgentSeriesName],[0,1]);" 
	          ><Input name=AgentSeriesName class="codename" elementtype=nacessary > 
	        </TD>
	        -->
	        <TD class= input>
	          <Input name=AgentGrade class="codeno"  verify="职级系列|notnull"
	           readonly><Input name=AgentGradeName class="codename" elementtype=nacessary > 
	        </TD>
	        
        </TR>
        <TR  class= common>
	        <TD class='title'>
	          考核所属年
	        </TD>
	        <TD  class= input>
	          <Input class=common  name=AssessYear verify="考核所属年|NOTNULL&INT&len=4" elementtype=nacessary >
	        </TD>
	        <TD class='title'>
	          考核所属月
	        </TD>
	        <TD  class= input>
	          <Input class=common  name=AssessMonth verify="考核所属月|NOTNULL&INT&len=2" elementtype=nacessary >
	        </TD>
        </TR>

        <TR class=input>     
         <TD class=common>
          <input type=button class=cssbutton value="计算" onclick="AgentAssessSave();">   
          <!--
          <input type=button class=cssbutton value="删除" onclick="clearHistoryData();">  
          -->
        </TD>   
          
       </TR>          
      </table>
    </Div>  
    <Div  id= "divAgentQuery" style= "display: 'none'">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAssessQueryGrid" ></span> 
  	</TD>
      </TR>
     </Table>					
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
   </Div>	     
    <Input type=hidden name=mOperate value="">
    <input type=hidden name=BranchType>
    <input type=hidden name=BranchType2>
    <input type=hidden name=CalFlag>
    <input type=hidden name=AgentSeries>
    <input type=hidden name=AgentSeriesName>
    
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>