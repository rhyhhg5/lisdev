
//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var showInfo;
var mDebug="0";
var arrStrReturn = new Array();
var arrGrid;


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

// 查询按钮
function easyQueryClick()
{				
  // 书写SQL语句
  var strSQL = "";
  if(!verifyInput()) 
  {
  	return;
  	}
  var tAgentCode = fm.AgentCode.value;
  if ( tAgentCode == null || tAgentCode == "" )  {
    alert("请录入代理人编码！");
    return;
  }
  var tIndexCalNo = fm.IndexCalNo.value;
  if ( tIndexCalNo == null || tIndexCalNo == "" )  {
    alert("请录入考核年月代码！");
    return;
  }  
  
  var tReturn = getManageComLimitlike("LAAssess.ManageCom");
  strSQL = "select laassess.AgentCode,laagent.name,getbranchattr(latree.AgentGroup),laassess.agentgrade,latree.AgentGrade,laassess.AgentGrade1,laassess.confirmer from LAAssess,laagent,latree where laassess.State = '2' "
         + " and laassess.agentcode = laagent.agentcode "		
         + " and laassess.agentcode = latree.agentcode "
         + getWherePart( 'LAAssess.ManageCom','ManageCom' )+tReturn
		 + getWherePart( 'LAAssess.IndexCalNo','IndexCalNo')
		 + getWherePart( 'LAAssess.AgentCode','AgentCode' );
		 
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未找到符合查询条件的数据！");
    return false;
  }
  
  // 初始化表格
	var tCount = AssessGrid.mulLineCount;
	
	if ( tCount == 0 )
	{
	  initAssessGrid();
	
      //查询成功则拆分字符串，返回二维数组  
      turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
      //设置初始化过的MULTILINE对象
      turnPage.pageDisplayGrid = AssessGrid;    
          
      //保存SQL语句
      turnPage.strQuerySql     = strSQL; 
  
      //设置查询起始位置
      turnPage.pageIndex = 0;  
  
      //在查询结果数组中取出符合页面显示大小设置的数组
      var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
      arrGrid = arrDataSet;
      //调用MULTILINE对象显示查询结果
      displayMultiline(arrDataSet, turnPage.pageDisplayGrid); 
    }
    else
    {      	  
	  //查询成功则拆分字符串，返回二维数组  
      turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
      arrGrid = arrDataSet;
	  decodeToGrid(arrGrid,tCount);
  }	 
}


function decodeToGrid(tArr,tCount) 
{  
  var index=0;	
  var tAgentCode = "";
  for ( index=0;index<tCount;index++)
  {
  	tAgentCode = AssessGrid.getRowColData(index,1);
  	if (tAgentCode == tArr[0][0] )  {
  	  alert("列表框中已存在该代理人记录！");
  	  return;
  	}  	
  }	
  AssessGrid.addOne();
  AssessGrid.setRowColData(tCount,1,tArr[0][0]);
  AssessGrid.setRowColData(tCount,2,tArr[0][1]);
  AssessGrid.setRowColData(tCount,3,tArr[0][2]);  
  AssessGrid.setRowColData(tCount,4,tArr[0][3]);
  AssessGrid.setRowColData(tCount,5,tArr[0][4]);
  AssessGrid.setRowColData(tCount,6,tArr[0][5]);
  AssessGrid.setRowColData(tCount,7,tArr[0][6]);
}	
	


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
               
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
}


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//提交，保存按钮对应操作
function submitForm()
{
	 if(!verifyInput()) 
  { return; }	
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
 
  fm.submit();
 
}	



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LABranchGroup.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 