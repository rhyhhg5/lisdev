
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrStrReturn = new Array();
var arrGrid;
try{
  var turnPage = new turnPageClass();
 }
 catch(ex)
 {
   //alert("error");
 }

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function easyQueryClick()
{
  //首先检验录入框
  if(!verifyInput()) 
    return false;
  var tManageCom = fm.all('ManageCom').value;
  var tIndexCalNo = fm.all('IndexCalNo').value;
  var tAgentGrade1 = fm.all('AgentGrade1').value;
  
  //查询信息	 
  StrSQL = "select AgentCode , BranchAttr , AgentGrade ,CalAgentGrade , AgentGrade1 from LAAssessAccessory where 1=1 and state = '1' "
           + " and managecom like '" +tManageCom+"%%' " 
           + getWherePart('IndexCalNo')
           + getWherePart('AgentGrade1')
           + " order by agentgrade1 asc ";
           	

  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(StrSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组  
  turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = BeforeGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = StrSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);  	
	
}	


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function qryAfterAscript()
{
	if(!verifyInput()) 
    return false;
  //下面增加相应的代码
  showInfo=window.open("./AfterEmpAscriptQry.html");
}

   	
function AgentAscript()
{
  //首先检验录入框
  if(!verifyInput()) return false;
  
  var showStr="正在执行归属操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
  
  fm.mOperate.value = 'INSERT||MAIN';
  //fm.fmAction.value = "Ascript";
  fm.submit();
}	
	

