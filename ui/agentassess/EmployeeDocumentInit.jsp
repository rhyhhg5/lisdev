<%
//程序名称：EmployeeAssessConfirmInit.jsp
//程序功能：
//创建日期：2004-02-16 09:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('QualiPassFlag').value = '';
  }
  catch(ex)
  {
    alert("在EmployeeDocumentInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}


function initForm()
{
  try
  {
    initInpBox();
	initAgentInfoGrid();
  }
  catch(re)
  {
    alert("EmployeeDocumentInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentInfoGrid
 ************************************************************
 */
function initAgentInfoGrid()
  {                               
    var iArray = new Array();
      
      try
      {
		iArray[0]=new Array();
		iArray[0][0]="序号";         //列名
		iArray[0][1]="30px";         //列名
		iArray[0][2]=100;         //列名
		iArray[0][3]=0;         //列名		
		
		iArray[1]=new Array();
		iArray[1][0]="代理人编码";         //列名
		iArray[1][1]="100px";         //宽度
		iArray[1][2]=100;         //最大长度
		iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[2]=new Array();
		iArray[2][0]="代理人组别";         //列名
		iArray[2][1]="120px";         //宽度
		iArray[2][2]=100;         //最大长度
		iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[3]=new Array();
		iArray[3][0]="代理人职级";         //列名
		iArray[3][1]="100px";         //宽度
		iArray[3][2]=100;         //最大长度
		iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[4]=new Array();
		iArray[4][0]="入司日期";         //列名
		iArray[4][1]="100px";         //宽度
		iArray[4][2]=100;         //最大长度
		iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[5]=new Array();
		iArray[5][0]="档案调入标记";         //列名
		iArray[5][1]="80px";         //宽度
		iArray[5][2]=100;         //最大长度
		iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[6]=new Array();
		iArray[6][0]="档案调动日期";         //列名
		iArray[6][1]="80px";         //宽度
		iArray[6][2]=100;         //最大长度
		iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
	
		
        AgentInfoGrid = new MulLineEnter( "fm" , "AgentInfoGrid" ); 

        //这些属性必须在loadMulLine前
        AgentInfoGrid.canChk = 1;
        AgentInfoGrid.mulLineCount = 6;   
        AgentInfoGrid.displayTitle = 1;
        
        AgentInfoGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化AgentInfoGrid时出错："+ ex);
      }
    }

</script>