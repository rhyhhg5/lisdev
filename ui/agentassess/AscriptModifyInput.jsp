<html>
<%
//程序名称：AscriptModifyInput.jsp
//程序功能：代理人归属后调整录入界面
//创建时间：2003-07-10
//更新记录：更新人   更新时间   更新原因

%>
<%@page contentType="text/html;charset=GBK" %>
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=GBK">-->
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>

   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

   <SCRIPT src="AscriptModifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="./AscriptModifyInit.jsp"%>
   <%@include file="../common/jsp/UsrCheck.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
  
</head>

<body  onload="initForm();" >
 <form action="./AscriptModifySave.jsp" method=post name=fm target="fraSubmit">
  <%@include file = "../common/jsp/OperateTopButton.jsp" %>
  <table class=common>
    <tr class=common>
      <td class=title>管理机构 </td>
      <td class=input> <input class=code name="ManageCom" verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
      </td>    
      <td class=title>代理人编码 </td>
      <td class=input> <input class=common name="AgentCode"> </td>      
    </tr>
    <tr>  
      <td class=title>考核年月代码 </td>
      <td class=input> <input class=common name="IndexCalNo"></td> 
      <td class=input colspan=2> <input type=button class=common name="Qry" value="查    询" onclick="easyQueryClick();"> 
      </td>
    </tr>
  </table>
  
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessGrid);">
    		</td>
    		<td class= titleImg>
    			 代理人信息
    		</td>
    	</tr>
  </table>

  <div id="divAssessGrid" style="display:''">
   <table class=common>
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanAssessGrid"></span>
     </td>
    </tr>
   </table>      
  </div>


  <input type=hidden id="sql_where" name="sql_where">
  <input type=hidden id="fmAction" name="fmAction">   

 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"> </span> 
</body>
</html>




