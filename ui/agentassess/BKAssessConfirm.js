//               该文件中包含客户端需要处理的函数和事件
var strRT=""; //存储代理人代码记录
//var strAgentGrade=""; //存储代理人职级
//var arrGrade = new Array();
var arrGrade = "";
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//***************************************************
//* 查询职级信息
//***************************************************
function getAgentGradeStr()
{
  var tReturn = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;

  var sSql = "select gradecode,gradename from laagentgrade where 1=1 ";
     sSql += getWherePart("BranchType");
     sSql += getWherePart("BranchType2");  
	   sSql += "order by gradecode";
	   

  var tqueryRe = easyQueryVer3(sSql,1,0,1);
  if (tqueryRe)
      tReturn = tqueryRe;

  return tReturn;
}

//***************************************************
//* 查询考核信息
//***************************************************
function easyQueryClick()
{
	//验证必填项
	if(!checkValid())
	{
		return false;
	}
	
	var tSQL = "";
	
	tSQL  = "SELECT"; 
	tSQL += " b.groupAgentCode,";
	tSQL += " b.Name,";
	tSQL += " c.BranchAttr,";
	tSQL += " c.Name,";
	tSQL += " a.AgentGrade,";
	tSQL += " '',";
	tSQL += " a.CalAgentGrade,";
	tSQL += " (SELECT GradeName FROM LAAgentGrade WHERE GradeCode= a.CalAgentGrade)";
	tSQL += " ,a.AgentCode ";
	tSQL += " ,(case when a.state='00' then '调整未确认'  when a.state = '01' then '调整已确认' else '' end) ";
	tSQL += " FROM";
	tSQL += " LAAssess a,";
	tSQL += " LAAgent b,";
	tSQL += " LABranchGroup c,";
	tSQL += " LAAgentGrade d";
	tSQL += " WHERE";
	tSQL += " a.AgentCode=b.AgentCode AND";
	tSQL += " a.AgentGroup=c.AgentGroup AND";
	tSQL += " a.AgentGrade=d.GradeCode and a.state in('00','01') ";
	tSQL += " and d.GradeProperty2= '"+document.fm.GradeProperty2.value+ "' "
	tSQL += getWherePart("a.indexcalno","IndexCalNo");	
	tSQL += getWherePart("b.groupagentcode","AgentCode");	
	tSQL += getWherePart("a.ManageCom","ManageCom","like");
	tSQL += getWherePart("c.BranchAttr","BranchAttr");
	tSQL += getWherePart("b.Name","AgentName");
	tSQL += getWherePart("a.state","AdjustState");
	//tSQL += " and b.agentstate<='02'";
	tSQL += " and a.BranchType='3' and a.BranchType2='01'";
	tSQL += " and not exists ";
	tSQL += "(select 'X' from laassess where agentcode = a.agentcode and indexcalno='"+fm.IndexCalNo.value+"' and state ='02')";
	tSQL += " ORDER BY c.BranchAttr,a.AgentGrade";
	
	var tResult = easyQueryVer3(tSQL,1,0,1);
	if(tResult==null||tResult==""){
		alert("查询失败，没有相关信息");
		initEvaluateGrid();
		return false;
	}
	//执行查询并返回结果
	turnPage.queryModal(tSQL, EvaluateGrid);
}

//***************************************************
//* 验证必填项
//***************************************************
function checkValid()
{
	if (trim(document.fm.ManageCom.value)=="")
	{
		alert("请填写管理机构!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		return false;
	}
	
	if (document.fm.ManageCom.value.length != 8)
	{
		alert("管理机构输入错误!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		fm.all('ManageCom').focus();
		return false;
	}
	
	if(trim(document.fm.IndexCalNo.value)=="")
	{
		alert("请填写考核年月!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	if (document.fm.IndexCalNo.value.length != 6)
	{
		alert("考核年月输入错误!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	
	return true;
}

//***************************************************
//* 点击‘保存’进行的操作
//***************************************************
function submitForm()
{
	var tRowCount = EvaluateGrid.mulLineCount;  //取得MulLine的行数
	
  //判断是否有数据需要处理
  if(tRowCount==0)
  {
  	alert("没有数据需要处理！请先查询！");
  	return false;
  }
  
	// 提交画面前的验证
	if (checkValid() == false)
    return false;
    
  var i = 0;
  var querySql = "";
	querySql  = "SELECT"; 
	querySql += " a.*"
	querySql += " FROM";
	querySql += " LAAssess a,";
	querySql += " LAAgent b,";
	querySql += " LABranchGroup c,";
	querySql += " LAAgentGrade d";
	querySql += " WHERE";
	querySql += " a.AgentCode=b.AgentCode AND";
	querySql += " a.AgentGroup=c.AgentGroup AND";
	querySql += " a.AgentGrade=d.GradeCode and a.state='00' ";
	querySql += " and d.GradeProperty2= '"+document.fm.GradeProperty2.value+ "' "
	querySql += getWherePart("a.indexcalno","IndexCalNo");	
	querySql += getWherePart("b.groupagentcode","AgentCode");	
	querySql += getWherePart("a.ManageCom","ManageCom","like");
	querySql += getWherePart("c.BranchAttr","BranchAttr");
	querySql += getWherePart("b.Name","AgentName");
	querySql += getWherePart("a.state","AdjustState");
	//querySql += " and b.agentstate<='02'";
	querySql += " and a.BranchType='3' and a.BranchType2='01'";
	querySql += " and not exists ";
	querySql += "(select 'X' from laassess where agentcode = a.agentcode and indexcalno='"+fm.IndexCalNo.value+"' and state in ('02','01'))";
	querySql += " ORDER BY c.BranchAttr,a.AgentGrade";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.all('querySql').value=querySql;
  fm.action="./BKAssessConfirmSave.jsp";
  fm.submit(); //提交
  return true;
}

//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    initEvaluateGrid();
  }
}

function downLoad(){
	var tRowCount = EvaluateGrid.mulLineCount;  //取得MulLine的行数
	  //判断是否有数据需要处理
	  if(tRowCount==0)
	  {
	  	alert("没有数据！请先查询！");
	  	return false;
	  }	
		var downSql = "";
		
		downSql  = "SELECT"; 
		downSql += " b.groupAgentCode,";
		downSql += " b.Name,";
		downSql += " c.BranchAttr,";
		downSql += " c.Name,";
		downSql += " a.AgentGrade,";
		downSql += " a.CalAgentGrade,";
		downSql += " (SELECT GradeName FROM LAAgentGrade WHERE GradeCode= a.CalAgentGrade)";
		downSql += " ,(case when a.state='00' then '调整未确认'  when a.state = '01' then '调整已确认' else '' end) ";
		downSql += " FROM";
		downSql += " LAAssess a,";
		downSql += " LAAgent b,";
		downSql += " LABranchGroup c,";
		downSql += " LAAgentGrade d";
		downSql += " WHERE";
		downSql += " a.AgentCode=b.AgentCode AND";
		downSql += " a.AgentGroup=c.AgentGroup AND";
		downSql += " a.AgentGrade=d.GradeCode and a.state in('00','01') ";
		downSql += " and d.GradeProperty2= '"+document.fm.GradeProperty2.value+ "' "
		downSql += getWherePart("a.indexcalno","IndexCalNo");	
		downSql += getWherePart("b.groupagentcode","AgentCode");	
		downSql += getWherePart("a.ManageCom","ManageCom","like");
		downSql += getWherePart("c.BranchAttr","BranchAttr");
		downSql += getWherePart("b.Name","AgentName");
		downSql += getWherePart("a.state","AdjustState");
		//downSql += " and b.agentstate<='02'";
		downSql += " and a.BranchType='3' and a.BranchType2='01'";
		downSql += " and not exists ";
		downSql += "(select 'X' from laassess where agentcode = a.agentcode and indexcalno='"+fm.IndexCalNo.value+"' and state ='02')";
		downSql += " ORDER BY c.BranchAttr,a.AgentGrade";	  
		var tResult = easyQueryVer3(downSql,1,0,1);
		if(tResult==null||tResult==""){
			alert("下载失败，没有相关信息");
			return false;
		}
		
		var i = 0;
		fm.all("downSql").value = downSql;
		fm.action = "./BKAssessConfirmToDownLoad.jsp"
		fm.submit();
	  
}