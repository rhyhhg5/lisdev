var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initAssessMarkGrid();
	
	//alert(fm.all('AgentCode').value);
	// 书写SQL语句
	var strSQL = "";
	var strsql="select startdate,enddate from  lastatsegment  where  stattype ='5' and char(yearmonth)='"+fm.AssessDate.value+"' ";
	var Result  = easyQueryVer3(strsql, 1, 1, 1);   
	var tArr = new Array();
	tArr = decodeEasyQueryResult(Result);
	var  tstartdate=tArr[0][0];
	var  tEndDate=tArr[0][1];	
    if (fm.AssessType.value=='03')
     {	
    strSQL=" select GroupAgentCode, Name,'在职',agentcode from laagent where   agentstate<'06' "
       +" and agentcode in (select agentcode from latree a where a.agentgrade<='D11')  "
			 +getWherePart('ManageCom','ManageCom','like')
       +getWherePart('BranchType','BranchType')
       +getWherePart('BranchType2','BranchType2')
			 +getWherePart('GroupAgentCode','GroupAgentCode')       
       +" union "
       +" select GroupAgentCode, Name,'离职',agentcode from laagent where    agentstate='06' "
       +" and agentcode in (select agentcode from latree a where a.agentgrade<='D11')  "
       +" and managecom in(select managecom from lawagehistory where wageno='"+fm.AssessDate.value+"' and branchtype2='01' and branchtype='2' ) "
       +" and '"+fm.AssessDate.value+"' in (select wageno from lawagehistory where wageno='"+fm.AssessDate.value+"' and branchtype2='01' and branchtype='2' and state='11') "
			 +getWherePart('ManageCom','ManageCom','like')
       +getWherePart('BranchType','BranchType')
       +getWherePart('BranchType2','BranchType2')
			 +getWherePart('GroupAgentCode','GroupAgentCode')
			 +" union "
 			 +" select GroupAgentCode, Name,'在职',agentcode from laagent where agentcode in "
       +" (select agentcode from latree where oldenddate<='"+tEndDate+"' and oldenddate>='"+tstartdate+"' "
			 +" and agentlastgrade like 'D%' and agentgrade like 'E%') and agentstate<'06'	"		 
			 +getWherePart('ManageCom','ManageCom','like')
       +getWherePart('BranchType','BranchType')
       +getWherePart('BranchType2','BranchType2')
			 +getWherePart('GroupAgentCode','GroupAgentCode')       
			 +" union "
 			 +" select GroupAgentCode, Name,'离职',agentcode from laagent where agentcode in "
       +" (select agentcode from latree where oldenddate<='"+tEndDate+"' and oldenddate>='"+tstartdate+"' "
			 +" and agentlastgrade like 'D%' and agentgrade like 'E%') and agentstate='06'	"		 
			 +getWherePart('ManageCom','ManageCom','like')
       +getWherePart('BranchType','BranchType')
       +getWherePart('BranchType2','BranchType2')
			 +getWherePart('GroupAgentCode','GroupAgentCode')       			 
			 +" order by GroupAgentCode with ur"
			 ;
      }
          if (fm.AssessType.value=='02')
     {	
    strSQL=" select GroupAgentCode, Name,'在职',agentcode from laagent where   agentstate<'06' "
       +" and agentcode in (select agentcode from latree a where a.agentgrade>='E01')  "
			 +getWherePart('ManageCom','ManageCom','like')
       +getWherePart('BranchType','BranchType')
       +getWherePart('BranchType2','BranchType2')
			 +getWherePart('GroupAgentCode','GroupAgentCode')       
       +" union "
       +" select GroupAgentCode, Name,'离职',agentcode from laagent where    agentstate='06' "
       +" and agentcode in (select agentcode from latree a where a.agentgrade>='E01')  "
       +" and outworkdate>='"+tstartdate+"' and  outworkdate<='"+tEndDate+"' "
       +" and managecom in(select managecom from lawagehistory where wageno='200701' and branchtype2='01' and branchtype='2' ) "
       +" and '"+fm.AssessDate.value+"' in (select wageno from lawagehistory where wageno='200701' and branchtype2='01' and branchtype='2' and state='11') "
			 +getWherePart('ManageCom','ManageCom','like')
       +getWherePart('BranchType','BranchType')
       +getWherePart('BranchType2','BranchType2')
			 +getWherePart('GroupAgentCode','GroupAgentCode')
			 +" union "
 			 +" select GroupAgentCode, Name,'在职',agentcode from laagent where agentcode in "
       +" (select agentcode from latree where oldenddate<='"+tEndDate+"' and oldenddate>='"+tstartdate+"' "
			 +" and agentlastgrade like 'E%' and agentgrade like 'D%') and agentstate<'06'	"		 
			 +getWherePart('ManageCom','ManageCom','like')
       +getWherePart('BranchType','BranchType')
       +getWherePart('BranchType2','BranchType2')
			 +getWherePart('GroupAgentCode','GroupAgentCode')       
			 +" union "
 			 +" select GroupAgentCode, Name,'离职',agentcode from laagent where agentcode in "
       +" (select agentcode from latree where oldenddate<='"+tEndDate+"' and oldenddate>='"+tstartdate+"' "
			 +" and agentlastgrade like 'E%' and agentgrade like 'D%') and agentstate='06'	"		 
			 +getWherePart('ManageCom','ManageCom','like')
       +getWherePart('BranchType','BranchType')
       +getWherePart('BranchType2','BranchType2')
			 +getWherePart('GroupAgentCode','GroupAgentCode')       			 
			 +" order by GroupAgentCode with ur"
			 ;        
    }
	//alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有找到满足条件的数据!");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AssessMarkGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}


function returnParent()
{
  var arrReturn = new Array();
	var tSel = AssessMarkGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{
				arrReturn = AssessMarkGrid.getRowData(tSel-1);
				top.opener.afterAgentQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();		
	}
}