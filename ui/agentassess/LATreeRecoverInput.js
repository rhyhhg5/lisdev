//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function easyQueryClick()
{		
	 if(!verifyInput()) 
  { return; }	
  // 初始化表格
  initAssessGrid();
  var tManageCom  = fm.ManageCom.value;
  var tIndexCalNo = fm.IndexCalNo.value;
  if ( tManageCom == null || tManageCom == "" ) 
  {
  	alert("请选择管理机构编码！");
  	return;
  }	
  if ( tIndexCalNo == null || tIndexCalNo == "" ) 
  {
  	alert("请录入考核年月编码！");
  	return;
  }
  		
  // 书写SQL语句
  var strSQL = "";  
  strSQL = "select laassess.agentcode,laagent.name,getbranchattr(latree.agentgroup),latree.agentgrade,laassess.agentgrade1,laassess.confirmer from laassess,latree,laagent "
         + "where laassess.agentcode = latree.agentcode and latree.agentcode = laagent.agentcode "
         + "and laassess.state = '1' "
         + getWherePart( 'LAAssess.ManageCom','ManageCom' )		 
		 + getWherePart( 'LAAssess.AgentCode','AgentCode' )		   
		 + getWherePart( 'LAAssess.AgentGrade','AgentGrade' )
		 + getWherePart( 'LAAssess.IndexCalNo','IndexCalNo') 
  
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据，请重新录入查询条件！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组  
  turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = AssessGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}

function orgRecover()
{
	 if(!verifyInput()) 
  { return; }	
  var arrSelected = null;
  var tRow = AssessGrid.getSelNo();
  
  if ( tRow == 0 || tRow == null )
  {
  	alert("请选中一条记录！");
  	return;
  }
  
  fm.UpdAgentCode.value = AssessGrid.getRowColData(tRow-1,1);
  fm.CurBranchAttr.value = AssessGrid.getRowColData(tRow-1,3);
  
  
  showInfo=window.open("./OrgRecoverInput.html");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}	


function simpleRecover()
{
	 if(!verifyInput()) 
  { return; }	
  var tSelNo = AssessGrid.getSelNo();
  if ( tSelNo == 0 )
  {
  	alert("请选中一条记录!");
  	return;
  }  
  	
  var showStr="正在执行操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
  fm.fmAction.value="1";  
  fm.submit();		
}



function departRecover()
{
	 if(!verifyInput()) 
  { return; }	
  var tSelNo = AssessGrid.getSelNo();
  if ( tSelNo == 0 )
  {
  	alert("请选中一条记录!");
  	return;
  }  
  	
  var showStr="正在执行操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
  fm.fmAction.value="2";
  fm.submit();
}


function afterQuery(tNewGroup,tNewAttr)
{
  fm.NewBranchAttr.value = tNewAttr;
  fm.NewAgentGroup.value = tNewGroup;
  
}	