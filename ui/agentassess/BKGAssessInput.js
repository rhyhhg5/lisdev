   //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false; 
  if (!chkValid()) return false;
  if(beforeSubmit() == false) return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在BKAssessInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
     parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
     parent.fraMain.rows = "0,0,0,82,*";
  }
}
 //****************************
 //*
 //****************************
 function beforeSubmit()
 {
   //如果上次考核确认未做归属,则不能进行本次的考核计算
   var tMonth = fm.all('AssessMonth').value;
   if (tMonth.length ==1) 
   {
   	    tMonth = "0" + tMonth;
   }
   fm.all('AssessMonth').value = tMonth;
   var yearmonth = fm.all('AssessYear').value+tMonth;
   var SQL = "select '1' from laassess where managecom like '"+fm.all('ManageCom').value+"%' and branchtype='3' and branchtype2='01' and state in ('01','00')  and indexcalno<'"+yearmonth+"'";
   var result = easyQueryVer3(SQL,1,0,1);
   if(result!=null&&result!='')
   {
     alert("上次考核未确认,不能进行本次计算!");
     return false;
   }
   var tSQL="select '1' from lawagehistory where managecom like '"+fm.all('ManageCom').value+"%' and branchtype = '3' and branchtype2 = '01' and state = '14' and wageno = '"+yearmonth+"' ";
   var tResult = easyQueryVer3(tSQL,1,0,1);
   if(tResult==null||tResult==''){
	 alert("考核月薪资未确认，不能进行考核计算");
	 return false;
   }
   var stateSql = "select '1' from laassesshistory where state = '01' and indexcalno = '"+yearmonth+"'"
	            +getWherePart('BranchType')
	            +getWherePart('BranchType2')
	            +getWherePart('ManageCom')
    var stateResult = easyQueryVer3(stateSql,1,0,1);
   if(stateResult!=null&&stateResult!=''){
	 alert("该管理机构当前考核月，考核计算正在进行");
	 return false;
   }  
   
   var stateAssessSql = "select '1' from laassess where (state ='02' or (state='01' and agentgrade like 'G%')) and indexcalno = '"+yearmonth+"'"
   +getWherePart('BranchType')
   +getWherePart('BranchType2')
   +getWherePart('ManageCom')
   var stateAssessSqlResult = easyQueryVer3(stateAssessSql,1,0,1);
   if(stateAssessSqlResult!=null&&stateAssessSqlResult!=''){
	   alert("考核结果已确认，不能计算");
	   return false;
   }  

   return true;
 }
  //****************************
 //* chkValid() 校验录入是否符合规则
 //****************************
 function chkValid()
 {
   //如果上次考核确认未做归属,则不能进行本次的考核计算
   var tMonth = fm.all('AssessMonth').value;
   if(tMonth != '03'&&tMonth != '06'&&tMonth != '09'&&tMonth != '12')
   {
     alert("考核所属月录入错误，月份应该是 03、06、09、12 月!");
     return false;
   }
   return true;
 }
//执行计算         
function BKAssessSave()
{  
  submitForm();	
}  
                  
