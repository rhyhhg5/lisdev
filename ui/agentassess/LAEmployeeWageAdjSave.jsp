<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAEmployeeWageAdjSave.jsp
//程序功能：
//创建日期：2003-07-15 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAWelfareInfoSchema mLAWelfareInfoSchema = new LAWelfareInfoSchema();
  LAEmployeeWageAdjUI mLAEmployeeWageAdjUI  = new LAEmployeeWageAdjUI();
  LAEmployeeGenWelfareUI mLAEmployeeGenWelfareUI = new LAEmployeeGenWelfareUI();

  //输出参数
  CErrors tError = null;
  GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin prepare LAWelfareInfoSet...");
  String tAgentCode = request.getParameter("AgentCode");
  System.out.println(tAgentCode);
  
  String tBranchAttr = request.getParameter("BranchAttr");
  System.out.println(tBranchAttr);
  
 
  String tAClass = request.getParameter("AClass");
  System.out.println(tAClass);
  String tSumMoney = request.getParameter("SumMoney");
  System.out.println(tSumMoney);
  String tIndexCalNo = request.getParameter("IndexCalNo");
  System.out.println(tIndexCalNo);
  String tServeYear = request.getParameter("ServeYear");
   System.out.println(tServeYear);
  String tOperate =  request.getParameter("mOperate");
  System.out.println(tOperate);
  String sql="select a.managecom,b.branchattr from latree a,labranchgroup b where a.agentgroup=b.agentgroup and agentcode='"+tAgentCode+"'";
  SSRS aSSRS = new SSRS();
  ExeSQL aExeSQL=new ExeSQL();
  aSSRS=aExeSQL.execSQL(sql);
  String tManageCom=aSSRS.GetText(1,1);
  String ttBranchAttr=aSSRS.GetText(1,2);
  
  
     mLAWelfareInfoSchema = new LAWelfareInfoSchema();
     mLAWelfareInfoSchema.setManageCom(tManageCom);
     mLAWelfareInfoSchema.setAgentCode(tAgentCode);  
     mLAWelfareInfoSchema.setBranchAttr(ttBranchAttr);
     mLAWelfareInfoSchema.setAClass(tAClass);
     mLAWelfareInfoSchema.setIndexCalNo(tIndexCalNo);
     mLAWelfareInfoSchema.setServeYear(tServeYear);
     mLAWelfareInfoSchema.setSumMoney(tSumMoney);
   
  System.out.println("end 信息...");
	String Content="";
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.addElement(mLAWelfareInfoSchema);
  System.out.println("add over");
  String FlagStr="";
  if(tOperate.equals("INSERT||MAIN"))
  {
   try
   {
     mLAEmployeeGenWelfareUI.submitData(tVData , tOperate);
   }
   catch(Exception ex)
   {
     Content = "保存失败，原因是:" + ex.toString();
     FlagStr = "Fail";
   }
   if (!FlagStr.equals("Fail"))
   {
     tError = mLAEmployeeGenWelfareUI.mErrors;
     if (!tError.needDealError())
     {
     	Content = " 保存成功! ";
     	FlagStr = "Succ";
     }
     else
     {
     	Content = " 保存失败，原因是:" + tError.getFirstError();
     	FlagStr = "Fail";
     }
   }
  }
  else if(tOperate.equals("UPDATE||MAIN"))
  {
    try
    {
      mLAEmployeeWageAdjUI.submitData(tVData , tOperate);
    }
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
     if (!FlagStr.equals("Fail"))
    {
      tError = mLAEmployeeWageAdjUI.mErrors;
      if (!tError.needDealError())
      {
     	Content = " 保存成功! ";
      	FlagStr = "Succ";
      }
      else
      {
    	 Content = " 保存失败，原因是:" + tError.getFirstError();
     	 FlagStr = "Fail";
      }
    }
  }


  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>