<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "代理人N月FYC为0报表_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String querySql = request.getParameter("querySql");
    String lenh = request.getParameter("lenh");
    querySql = querySql.replaceAll("%25","%");
    
    	String[][] tTitle = {{"省公司代码","省公司","中支公司代码","中支公司","营业部代码","营业部","营业区代码","营业区","营业处代码","营业处","营业组代码","营业组","营销员代码","营销员","N月FYC为0","最近考核时点前职级代码","最近考核时点前职级","最近考核时点后职级代码","最近考核时点后职级"}};
    //表头的显示属性
  		int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
    
    //数据的显示属性
    	int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
    	CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
	    createexcellist.createExcelFile();
	    String[] sheetName ={"list"};
	    createexcellist.addSheet(sheetName);
	    int row = createexcellist.setData(tTitle,displayTitle);
	    if(row ==-1) errorFlag = true;
	        createexcellist.setRowColOffset(row,0);//设置偏移
	    if(createexcellist.setData(querySql,displayData)==-1)
	        errorFlag = true;
	    if(!errorFlag)
	        //写文件到磁盘
	        try{
	            createexcellist.write(tOutXmlPath);
	        }catch(Exception e)
	        {
	            errorFlag = true;
	            System.out.println(e);
	        }
	    //返回客户端
	    if(!errorFlag)
	        downLoadFile(response,filePath,downLoadFileName);
	    out.clear();
	    out = pageContext.pushBody();    
%>