//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function submitForm()
{
	if(!verifyInput()) 
  { return; }	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  //showSubmitFrame(mDebug);
  fm.submit(); //提交

}

// 查询按钮
function easyQueryClick()
{		
  if(!verifyInput()) 
  { return; }	
  // 初始化表格
  initAssessGrid();
  var tManageCom  = fm.ManageCom.value;
  var tIndexCalNo = fm.IndexCalNo.value;
  var tAgentCode  = fm.all('AgentCode').value; 
  if ( tManageCom == null || tManageCom == "" ) 
  {
  	alert("请选择管理机构编码！");
  	return;
  }	
  if ( tIndexCalNo == null || tIndexCalNo == "" ) 
  {
  	alert("请录入考核年月编码！");
  	return;
  }
    		
  // 书写SQL语句
  var strSQL = ""; 
  strSQL = "select a.AgentCode,b.name,a.IndexCalNo,a.BranchAttr,a.AgentGrade,"
           +" decode(trim(a.AgentGrade1),'00','清退',a.agentGrade1)"
           +" from LAAssess a,laagent b where a.agentcode=b.agentcode and a.state = '0'"
	   + getWherePart('a.ManageCom','ManageCom','like')
	   + getWherePart('a.IndexCalNo','IndexCalNo')
	   + getWherePart('a.BranchType','BranchType')
	   + getWherePart('a.AgentCode','AgentCode')
	   + getWherePart('a.AgentGrade','AgentGrade')
	   +" order by a.AgentCode,a.branchattr"; 
  
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据，请重新录入查询条件！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组  
  turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = AssessGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


//执行确认         
function WageConfirm()
{   
	 
	submitForm();	   
}  

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}