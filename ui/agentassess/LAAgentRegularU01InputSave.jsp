
<%
// 程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.encrypt.*"%>
     <%@page import="com.sinosoft.lis.agentassess.*"%>
    

<%@page contentType="text/html;charset=GBK" %>
<%

  System.out.println("保存转证信息------------------starte");
  LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
  LATreeSchema tLATreeSchema = new LATreeSchema();
  LAAgentRegularU01InputUI tLAAgentRegularU01InputUI = new LAAgentRegularU01InputUI();
  //获取页面的参数
  String Agentcode = request.getParameter("Agentcode"); //业务员的劳工号
  String GroupAgentCode = request.getParameter("groupAgentcode"); //业务员的统一工号
    
  String ManageCom = request.getParameter("ManageCom");
  String employdate = request.getParameter("employdate");
  String agentName = request.getParameter("agentName");
  String InDueFormDate = request.getParameter("InDueFormDate"); //转正日期
  String indueformflag = "1"; //转正状态
  String AgentGrade = request.getParameter("AgentGrade"); //代理人职级
  System.out.println("agentgrade:"+AgentGrade);
  String assessType = request.getParameter("assessType");//考核指标
  String operator = request.getParameter("operator");  //操作人员
  String makedate = request.getParameter("makedate"); //操作日期
  System.out.println("转正日期------------------"+InDueFormDate);
  
  //给laagent赋值
  tLAAgentSchema.setAgentCode(Agentcode);
  tLAAgentSchema.setInDueFormDate(InDueFormDate); 
  tLAAgentSchema.setGroupAgentCode(GroupAgentCode);
  
  //给latree赋值
  tLATreeSchema.setInDueFormDate(InDueFormDate);
  tLATreeSchema.setAgentCode(Agentcode);
  tLATreeSchema.setInDueFormFlag(indueformflag);//转正状态
  tLATreeSchema.setAssessType(assessType); //考核指标
  tLATreeSchema.setAgentGrade(AgentGrade);
	GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;

  String tRearStr = "";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  
  
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLATreeSchema);
  
  try
  { 
	 //LAAgentRegularU01InputSave.jsp
	
    tLAAgentRegularU01InputUI.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAgentRegularU01InputUI.mErrors;
    if (!tError.needDealError())
    {
    	if(Agentcode == null || "".equals(Agentcode)){
    		// Agentcode = tLAAgentRegularU01InputUI.getAgentCode();
    	}
    	String tGroupAgentCodeSQL = "select GroupAgentCode from laagent where agentcode = '"+Agentcode+"' ";
    	GroupAgentCode = new ExeSQL().getOneValue(tGroupAgentCodeSQL);
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript" type="">
        parent.fraInterface.fm.all('AgentCode').value = '<%=Agentcode%>';
		parent.fraInterface.fm.all('GroupAgentCode').value = '<%=GroupAgentCode%>';
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>
