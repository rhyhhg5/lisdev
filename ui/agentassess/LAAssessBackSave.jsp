<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAGroupSplitSave.jsp
//程序功能：
//创建日期：2005-7-30 17:06
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentassess.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //考核处理类
  LAAgentAssessBackUI tLAAgentAssessBackUI = new LAAgentAssessBackUI();
  //输出参数
  CErrors tError = null;
  String tOperate="";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  //接收画面的数据
  String tManageCom = request.getParameter("ManageCom");
  String tIndexCalNo = request.getParameter("IndexCalNo");
  String tBranchType = request.getParameter("BranchType");
  String tBranchType2 = request.getParameter("BranchType2");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.add(tManageCom);
  tVData.add(tIndexCalNo);
  tVData.add(tBranchType);
  tVData.add(tBranchType2);
	
	
  try
  {
    tLAAgentAssessBackUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAgentAssessBackUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>