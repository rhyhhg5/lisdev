//               该文件中包含客户端需要处理的函数和事件
var arrDataSet; 
var tArr;
var showInfo;
var mDebug="1";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 
var queryCount;


function initEdorType(cObj)
{
	mEdorType = " #1# and length(trim(ComCode))=8 ";
	showCodeList('comcode',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and length(trim(ComCode))=8 ";
	showCodeListKey('comcode',[cObj], null, null, mEdorType, "1");
}




//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function easyQueryClick()
{
	
	if (verifyInput() == false)
    return false;
	
  // 初始化表格
 initAgentAdjGrid();
// alert("111");
 var tQueryType = fm.all('QueryType').value;
// var tCurrentTime = easyQueryVer3("select to_char(SYSDATE,'YYYYMM') from dual ", 1, 1, 1);
//alert(tCurrentTime);
// if (!verifyInput())
//   {
//     queryCount = false;
//     return false;   
//   }
   AgentAdjGrid.clearData("AgentAdjGrid");
   
   var strSQL = "";
//   alert(tQueryType);
   if (tQueryType =='YT0')
   {
//   	alert(tQueryType);
   	strSQL ="select laagent.agentcode,laagent.name,"
   		+" latree.agentgrade,labranchgroup.branchattr,nvl(latree.branchcode,'Y00'),'YT0',"
   		+" decode(laagent.qualipassflag,'1','Y','N'),"
   		+" getEmployeeSendDate(laagent.agentcode),"
   		+" getEmployeeSendEndDate(laagent.agentcode) "
   		+" from latree,labranchgroup,laagent where "
   		+" laagent.branchcode not in  (select distinct agentgroup from labranchgroup"
   		+" where labranchgroup.branchtype='1' and labranchgroup.branchlevel='01'"
   		+" and labranchgroup.state='1' ) and  laagent.agentcode=latree.agentcode "
   		+" and (laagent.agentstate < '03' OR laagent.agentstate is null ) "
   		+" and labranchgroup.agentgroup=latree.agentgroup " 
    		+getWherePart('laagent.agentcode','AgentCode')
    		+getWherePart('labranchgroup.branchAttr','BranchAttr')
    		+getWherePart('latree.agentgrade','AgentGrade')
    		+getWherePart('nvl(trim(latree.branchcode),\'Y00\')','EmployeeGrade')
    		+getWherePart('laagent.manageCom','ManageCom','like')
    		+" and laagent.branchtype='1' "
      		+" and laagent.agentcode not in (select distinct agentcode from "
      		+" lawelfareinfo where 1=1 "
      		+getWherePart('lawelfareInfo.ManageCom','ManageCom','like')
      		+" and to_char(WSendDate,'yyyyMM')<=(select to_char(SYSDATE,'YYYYMM') from dual)"
      		+" and to_char(WSendEndDate,'yyyyMM')>=(select to_char(SYSDATE,'YYYYMM') from dual)) " 
    		+ "order by latree.agentgrade,laagent.agentcode ";
//    		alert(strSQL);
   }
   else
   if (tQueryType=='YT1')
   {
   	strSQL ="select lawelfareinfo.agentcode,laagent.name,latree.agentgrade,"
   	        +"lawelfareinfo.branchattr,nvl(latree.branchcode,'Y00'),'YT1',"
   	        +"decode(laagent.qualipassflag,'1','Y','N'),lawelfareinfo.wsenddate,"
   	        +"lawelfareinfo.wsendEnddate from "
   	        +" laagent,latree,lawelfareinfo "
   	        +" where  lawelfareinfo.aclass='12'   "
   	        
   	        +" and lawelfareinfo.agentcode=latree.agentcode  "
   	        +" and laagent.agentcode=lawelfareinfo.agentcode "
   	        +" and (laagent.agentstate < '03' OR laagent.agentstate is null ) "
   	        +"and to_char(LAWelfareInfo.WSendDate,'YYYYMM')<=(select to_char(SYSDATE,'YYYYMM') from dual) "
   	        +"and to_char(LAWelfareInfo.WSendEndDate,'YYYYMM')>=(select to_char(SYSDATE,'YYYYMM') from dual )" 
   	        +" and laagent.branchtype='1' "
   	        +getWherePart('lawelfareinfo.ManageCom','ManageCom','like')
		+getWherePart('nvl(trim(latree.branchcode),\'Y00\')','EmployeeGrade')
		+getWherePart('LATree.AgentGrade','AgentGrade')
		+getWherePart('LAWelfareInfo.branchAttr','BranchAttr')
		+getWherePart('lawelfareinfo.AgentCode','AgentCode')
		
    		+"order by latree.agentgrade,laagent.agentcode " ;
//    		alert(strSQL);
   }
   else
   if (tQueryType=='YT2')
   {
   	strSQL =" select lawelfareinfo.agentcode,laagent.name,latree.agentgrade,"
   		+" lawelfareinfo.branchattr,nvl(latree.branchcode,'Y00'),'YT2',"
   		+" decode(laagent.qualipassflag,'1','Y','N'),lawelfareinfo.wsenddate,"
   		+" lawelfareinfo.wsendEnddate from laagent,latree,lawelfareinfo "
	   	+" where  lawelfareinfo.aclass='11'  "
	   	+" and  laagent.agentcode=latree.agentcode and "
	   	+" laagent.agentcode=lawelfareinfo.agentcode and "
	   	+" (laagent.QualiPassFlag<>'1' or laagent.QualiPassFlag is null) "
	   	+" and (laagent.agentstate < '03' OR laagent.agentstate is null ) "
	   	+" and to_char(LAWelfareInfo.WSendDate,'YYYYMM')<=(select to_char(SYSDATE,'YYYYMM') from dual) "
	   	+" and to_char(LAWelfareInfo.WSendEndDate,'YYYYMM')>=(select to_char(SYSDATE,'YYYYMM') from dual )" 
	   	+" and laagent.branchtype='1' "
		+getWherePart('lawelfareinfo.ManageCom','ManageCom','like')
		+getWherePart('nvl(trim(latree.branchcode),\'Y00\')','EmployeeGrade')
		+getWherePart('LATree.AgentGrade','AgentGrade')
		+getWherePart('LAWelfareInfo.branchAttr','BranchAttr')
		+getWherePart('lawelfareinfo.AgentCode','AgentCode')
		
    		+" order by latree.agentgrade,laagent.agentcode " ;
    
//    		alert(strSQL);
   }
   else
   if (tQueryType=='YT3')
   {
   	strSQL =" select lawelfareinfo.agentcode,laagent.name,latree.agentgrade,"
   		+" lawelfareinfo.branchattr,nvl(latree.branchcode,'Y00'),'YT3',"
   		+" decode(laagent.qualipassflag,'1','Y','N'),lawelfareinfo.wsenddate,"
   		+" lawelfareinfo.wsendEnddate from laagent,latree,lawelfareinfo "
	   	+" where  lawelfareinfo.aclass='11'  "
	   	+" and  laagent.agentcode=latree.agentcode and "
	   	+" laagent.agentcode=lawelfareinfo.agentcode and "
	   	+" laagent.QualiPassFlag= '1' "
	   	+" and (laagent.agentstate < '03' OR laagent.agentstate is null ) "
	   	+" and to_char(LAWelfareInfo.WSendDate,'YYYYMM')<=(select to_char(SYSDATE,'YYYYMM') from dual) "
	   	+" and to_char(LAWelfareInfo.WSendEndDate,'YYYYMM')>=(select to_char(SYSDATE,'YYYYMM') from dual )" 
	   	+" and laagent.branchtype='1' "
		+getWherePart('lawelfareinfo.ManageCom','ManageCom','like')
		+getWherePart('nvl(trim(latree.branchcode),\'Y00\')','EmployeeGrade')
		+getWherePart('LATree.AgentGrade','AgentGrade')
		+getWherePart('LAWelfareInfo.branchAttr','BranchAttr')
		+getWherePart('lawelfareinfo.AgentCode','AgentCode')
		
    		+" order by latree.agentgrade,laagent.agentcode " ;
//    		alert(strSQL);
   }
   else
   {
   	strSQL =" select laagent.agentcode,laagent.name,latree.agentgrade,"
	   	+" labranchgroup.branchattr,nvl(latree.branchcode,'Y00'),"
	   	+" judgeEmployeeType(laagent.agentcode,(select to_char(SYSDATE,'YYYYMM') from dual )),"
	   	+" decode(laagent.qualipassflag,'1','Y','N'),"
	   	+" getEmployeeSendDate(laagent.agentcode),getEmployeeSendEndDate(laagent.agentcode)"
	   	+"  from laagent,latree,labranchgroup where "   
	   	+" laagent.branchcode not  in (select agentgroup from labranchgroup where"
	   	+" labranchgroup.branchtype='1'"
	   	+"  and labranchgroup.branchlevel='01' and labranchgroup.state='1' ) "
	   	+" and labranchgroup.agentgroup=latree.agentgroup "
	   	+" and laagent.branchtype='1' "
	   	+" and laagent.agentcode=latree.agentcode " 
	   	+" and (laagent.agentstate < '03' OR laagent.agentstate is null ) "
	   	+getWherePart('laagent.ManageCom','ManageCom','like')
		+getWherePart('nvl(trim(latree.branchcode),\'Y00\')','EmployeeGrade')
		+getWherePart('LATree.AgentGrade','AgentGrade')
		+getWherePart('labranchattr.branchAttr','BranchAttr')
		+getWherePart('laagent.AgentCode','AgentCode')
		+" order by latree.agentgrade,laagent.agentcode ";
//    		alert(strSQL);
   }
   
 
   turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
   //alert(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
//    fm.all('ManageCom').value="";
//    fm.all('IndexCalNo').value="";
    queryCount = false;
    return false;
  }  
  //查询成功则拆分字符串，返回二维数组
  //turnPage.arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //alert(arrDataSet.length);
  //turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentAdjGrid;              
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;   
  //设置查询起始位置
  turnPage.pageIndex       = 0;    
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果  
  displayMultiline(tArr, turnPage.pageDisplayGrid);
//  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
  queryCount = true;
 
   
}


	
function displayQueryResult(strResult) 
{
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
	
	//if (strResult=='0|0^')
	
	//{
		//AgentAdjGrid.clear();
		//alert('查询失败');
		//return ;
       // }	
	
	
  strResult = Conversion(strResult);
  var filterArray  = new Array(0,1,2,3,4,5,6,7,8);

  //保存查询结果字符串
  turnPage.strQueryResult  = strResult;

  //使用模拟数据源
  turnPage.useSimulation   = 1;

  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  //alert(turnPage.arrDataCacheSet);


  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentAdjGrid;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //控制是否显示翻页按钮
  /*if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }*
  */
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
}
  


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//alert('嘿嘿，我来了');
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}





//提交，保存按钮对应操作
function submitForm()
{
	//alert('pc ');
	var tManageCom = fm.all('ManageCom').value;
	
	
	
	var tQueryType=fm.all('QueryType').value;
	if ( tManageCom == null || tManageCom == '' )   
	{
	  alert("请录入管理机构！");
	  return;
	}  
	
    
	
	
	
	
  	 if (  tQueryType == null||tQueryType == '' )   
	{
		var tQueryType = '0';
	}  
		
	
        {
        	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        }
	
	fm.submit(); //提交
	
}




