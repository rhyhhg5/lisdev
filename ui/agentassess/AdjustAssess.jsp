<html>
<%
//程序名称：AdjustAssess.jsp
//程序功能：代理人归属后考核调整界面
//创建时间：2003-07-16
//更新记录：更新人   更新时间   更新原因
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=GBK">-->
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>

   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>

   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

   <SCRIPT src="AdjustAssess.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="AdjustAssessInit.jsp"%>
   <%@include file="../common/jsp/UsrCheck.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>

</head>

<body  onload="initForm();" >
 <form action="./AdjustAssessSave.jsp" method=post name=fm target="fraSubmit">
 <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
  <table class=common>
   <tr class=common>
    <td class=title>管理机构</td>
    <td class=input > <input class=code name=ManageCom  verify = "管理机构|notnull&code:ComCode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"></td>
    <td class=title nowrap>考核年月代码</td>
    <td class=input > <input class=common type=text name=IndexCalNo></td>
   </tr>
   <tr>
    <td class=title>代理人原职级</td>
    <td class=input > <input class=Code type=text name=AgentGrade 
       ondblclick="return showCodeList('AgentGrade', [this]);" 
       onkeyup="return showCodeList('AgentGrade', [this]);">
    </td>    
    <td class=title>代理人编码</td>
    <td class=input > <input class=common type=text name=AgentCode ></td>    
   </tr>
   
  </table>     
    <input type=button value="查询" class=common onclick="easyQueryClick();">

  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessGrid);">
    		</td>
    		<td class= titleImg>
    			 代理人考核信息
    		</td>
    	</tr>
  </table>
  <div id="divAssessGrid" style="display:''">
   <table class=common>
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanAssessGrid"></span>
     </td>
    </tr>
   </table>      
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">     
  </div>
   
  <table class=common>   
   <tr class=common>    
    <input type=button value="确认" class=common onclick="WageConfirm();"></td>    
   </tr>    
  </table>   
    <Input name= BranchType type= hidden value= ''>
 </form>
 <span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>




