<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.db.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
	String tManageCom = request.getParameter("ManageCom");
	String tIndexCalNo = request.getParameter("IndexCalNo");
	String tBranchType = request.getParameter("BranchType");
	String tBranchType2 = request.getParameter("BranchType2");
	String tAgentSeries = request.getParameter("AgentSeries");
	String tAgentCode[] = request.getParameterValues("EvaluateGrid1");
	String tBranchAttr[] = request.getParameterValues("EvaluateGrid3");
	String tAgentGrade[] = request.getParameterValues("EvaluateGrid5");
	String tCalAgentGrade[] = request.getParameterValues("EvaluateGrid6");
	String tTurnAgentGrade[] = request.getParameterValues("EvaluateGrid7");
	String tTurnFlag[] = request.getParameterValues("EvaluateGrid8");
	String tAgentGrade1[] = request.getParameterValues("EvaluateGrid9");
	int tDateCount = 0;


  //输入参数
  LAAssessSet mLAAssessSet = new LAAssessSet();
  LAAssessSchema mLAAssessSchema = new LAAssessSchema();
  LAAgentAssessAdjUI mLAAgentAssessAdjUI = new LAAgentAssessAdjUI();

  //输出参数
  CErrors tError = null;
  String tOperate="";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");
	// 准备数据

  System.out.println("开始准备后台处理");

  // 准备个人考核信息
	tDateCount = tBranchAttr.length;
	System.out.println("本次处理人数:["+tDateCount+"]");
	for(int i=0;i<tDateCount;i++)
	{
		//根据集团统一工号查询业务员编码
		String agentcode = "";
		agentcode = new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+tAgentCode[i]+"'");
		
		mLAAssessSchema = new LAAssessSchema();
		mLAAssessSchema.setAgentCode(agentcode);
		mLAAssessSchema.setBranchAttr(tBranchAttr[i]);
		mLAAssessSchema.setAgentGrade(tAgentGrade[i]);
		mLAAssessSchema.setCalAgentGrade(tCalAgentGrade[i]);
		mLAAssessSchema.setTurnAgentGrade(tTurnAgentGrade[i]);
		mLAAssessSchema.setTurnFlag(tTurnFlag[i]);
		mLAAssessSchema.setAgentGrade1(tAgentGrade1[i]);
		mLAAssessSet.add(mLAAssessSchema);
	}


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(mLAAssessSet);
  tVData.addElement(tManageCom);
  tVData.addElement(tIndexCalNo);
	tVData.addElement(tBranchType);
	tVData.addElement(tBranchType2);
  tVData.addElement(tAgentSeries);
  System.out.println("add over");
  try
  {
    mLAAgentAssessAdjUI.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLAAgentAssessAdjUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

