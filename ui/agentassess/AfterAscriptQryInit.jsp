<%
//程序名称：AgentAscriptionInit.jsp
//程序功能：
//创建日期：2002-08-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = top.opener.fm.all('ManageCom').value;
    fm.all('IndexCalNo').value = top.opener.fm.all('IndexCalNo').value;    
    fm.all('AgentGroup').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGrade').value = '';    
  }
  catch(ex)
  {
    alert("在AgentAscriptionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initAfterGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="代理人编码"; //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许


    iArray[2]=new Array();
    iArray[2][0]="代理人新组别"; //列名
    iArray[2][1]="100px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许    

    iArray[3]=new Array();
    iArray[3][0]="考核后级别"; //列名
    iArray[3][1]="100px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="考核后代理人状态"; //列名
    iArray[4][1]="0px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=3;              //是否允许输入,1表示允许,0表示不允许
    
   

    AfterGrid = new MulLineEnter( "fm" , "AfterGrid" );
    AfterGrid.canChk = 0;
	AfterGrid.mulLineCount = 0;
    AfterGrid.displayTitle = 1;
    AfterGrid.hiddenPlus = 1;
      AfterGrid.hiddenSubtraction = 1;
    AfterGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在AfterAscriptQryInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
    initAfterGrid();   
  }
  catch(re)
  {
    alert("AfterAscriptQryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>