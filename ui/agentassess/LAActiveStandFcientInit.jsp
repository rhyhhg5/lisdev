<%
//程序名称：LAActiveStandFcientInit.jsp
//程序功能：
//创建时间：2017-12-12
//创建人  ：wangqingmin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 ////var  tmanagecom=" 1 and  char(length(trim(comcode))) in (#4#,#2#)";
  var  tmanagecom=" 1 and  char(length(trim(comcode))) in (#4#,#2#,#8#)";
 var StrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode and riskprop = #I#)";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('RiskCode').value='';
    fm.all('RiskCodename').value=''; 
    fm.all('PayIntv').value='';
    fm.all('PayIntvName').value='';
    fm.all('Rate').value='';
    fm.all('ManageCom').value='';
    fm.all('ManageComName').value='';
//    fm.all('BranchType').value=getBranchType();
    
    fm2.diskimporttype.value='LADiscount';  
//    fm.BranchType.value=getBranchType();
    fm2.BranchType.value='5'
    fm2.BranchType2.value='01'
    //alert(fm2.BranchType.value);
    //alert(getBranchType());
  }
  catch(ex)
  {
    alert("在LADiscountInit.jsp.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initInpBox1()
{
	try
	  {
		  fm2.all('queryType').value ="";
		  fm2.all('queryName').value="";
		  fm2.all('FileImportNo').value="";
	    //alert(fm2.BranchType.value);
	    //alert(getBranchType());
	  
	    //alert(fm.all('branchtype2').value);                        
	  }
	  catch(ex)
	  {
		  alert("在LADiscountInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	  }
 	
	}

function initLADiscountGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    iArray[1]=new Array();
    iArray[1][0]="idx";            //列名 idx
    iArray[1][1]="0px";          //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[2]=new Array();
    iArray[2][0]="管理机构";          		        //列名
    iArray[2][1]="80px";      	      		//列宽
    iArray[2][2]=20;            			//列最大值
    iArray[2][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[2][4]="comcode";              	        //是否引用代码:null||""为不引用
	iArray[2][5]="2|3";
	iArray[2][6]="0|1";
	iArray[2][9]="管理机构|NotNull&NUM";  
	iArray[2][15]="1";
	iArray[2][16]=tmanagecom;
	iArray[2][18]="undefined";

	iArray[3]=new Array();
	iArray[3][0]="管理机构名称"; //列名
	iArray[3][1]="80px";        //列宽
	iArray[3][2]=100;            //列最大值
	iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许        
	
    
    iArray[4]=new Array();
	iArray[4][0]="险种编码";          		//列名
	iArray[4][1]="80px";      	      		//列宽
	iArray[4][2]=20;            			//列最大值
	iArray[4][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[4][4]="riskcode";              	        //是否引用代码:null||""为不引用
	iArray[4][5]="4|5";              	                //引用代码对应第几列，'|'为分割符
	iArray[4][6]="0|1"; 
    
    iArray[5]=new Array();
    iArray[5][0]="险种名称"; //列名
    iArray[5][1]="120px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[6]=new Array();
    iArray[6][0]="缴费年限"; //列名
    iArray[6][1]="50px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[6][10]="Payintv";
    iArray[6][11]="0|^0|趸交|^1|1年含以内|^3|3年期|^5|5年期|^10|10年期|^99|10年期以上|";
  
    iArray[7]=new Array();
    iArray[7][0]="折标系数"; //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][9]="折标系数|NotNull&NUM";
  
        
    LADiscountGrid = new MulLineEnter( "fm" , "LADiscountGrid" );
    LADiscountGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    LADiscountGrid.displayTitle = 1;
    //SetGrid.hiddenSubtraction =0;
    //SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    LADiscountGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LADiscountInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initImportResultGrid()
{
	try
	  {
	    var iArray = new Array();

	    iArray[0]=new Array();
	    iArray[0][0]="序号";         //列名
	    iArray[0][1]="30px";        //列宽
	    iArray[0][2]=100;            //列最大值
	    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

	    iArray[1]=new Array();
		  iArray[1][0]="导入文件批次";          		//列名
		  iArray[1][1]="120px";      	      		//列宽
		  iArray[1][2]=20;            			//列最大值
		  iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

	    iArray[2]=new Array();
	    iArray[2][0]="所在行"; //列名
	    iArray[2][1]="60px";        //列宽
	    iArray[2][2]=100;            //列最大值
	    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
	   
	                                       
	    iArray[3]=new Array();
	    iArray[3][0]="日志信息"; //列名
	    iArray[3][1]="160px";        //列宽
	    iArray[3][2]=100;            //列最大值
	    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
	    
	      
	    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
	    ImportResultGrid.canChk = 0;
			//SetGrid.mulLineCount = 10;
	    //ImportResultGrid.displayTitle = 1;
	    ImportResultGrid.hiddenSubtraction =1;
	    ImportResultGrid.hiddenPlus=1;
	    //SetGrid.locked=1;
	    //SetGrid.canSel=0;
	    ImportResultGrid.loadMulLine(iArray);    
	  }
	  catch(ex)
	  {
	    alert("在LADiscountInit.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
	  }
	}

function initForm()
{
  try
  {
    
    //debugger;
    initImportResultGrid();
    initInpBox();    
    initLADiscountGrid();
    
  }
  catch(re)
  {
    alert("在LADiscountInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>