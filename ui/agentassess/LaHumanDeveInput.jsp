<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String AgentSeries=request.getParameter("AgentSeries");
  String CalFlag=request.getParameter("CalFlag");
%>
<html>  
<%
//程序名称：AgentAssessInput.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="LaHumanDeveInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LaHumanDeveInit.jsp"%>

</head>
<body  onload="initForm();initElementtype();" >    
  <form action="./LaHumanDeveSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		上月人力发展指标计算
       		 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR>                      
          <TD  class= title>管理机构</TD>
            <TD  class= input><Input class="codeno" name=MngCom  ondblclick="return showCodeList('humandeveimngcom',[this,ComName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('humandeveimngcom',[this,ComName],[0,1],null,null,null,1,180);"><input class=codename name=ComName elementtype=nacessary></TD>
	        	<td class=title>考核年月</td>
					<td class=input>
						<input maxLength=6 name=AssessYearMonth verify="考核年月|YYYYMM&notnull" elementtype=nacessary>
						<label style="font-size:11px;color:red;font:italic arial,sans-serif">考核年月录入格式如:201603</label>
					</td>
        </TR>

        <TR class=input>     
         <TD class=common>
          <input type=button class=cssbutton value="查询状态" onclick="QueryState();">   
          <input type=button class=cssbutton value="计算" onclick="submitForm();">  
        </TD>   
          
       </TR>          
      </table>
    </Div>  
    <Div  id= "divAgentQuery" style= "display: 'none'">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAssessQueryGrid" ></span> 
  	</TD>
      </TR>
     </Table>					

   </Div>	     

    	<Input type=hidden name=operator value='<%=tG.Operator %>' />
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>