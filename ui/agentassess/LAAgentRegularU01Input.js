//               该文件中包含客户端需要处理的函数和事件

/**
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}

*/
var empolydate ="";
// 查询按钮
function queryClick()
{		
  showInfo=window.open("./LAAgentRegularU01InputQueryHtml.jsp?BranchType="+fm.all('BranchType').value+"&BranchType2="+fm.all('BranchType2').value); 
}

function afterQuery(arrQueryResult){
	var arrResult = new Array();
	if( arrQueryResult != null ){
		arrResult = arrQueryResult;
		fm.ManageCom.value = arrResult[0][0];
		fm.ManageComName.value = arrResult[0][1];
		fm.employdate.value = arrResult[0][2];
		fm.Agentcode.value = arrResult[0][3];
		fm.groupAgentcode.value = arrResult[0][4];
		fm.agentName.value = arrResult[0][5];
		fm.OldAgentGrade.value = arrResult[0][6];
		fm.assessType.value = arrResult[0][8];
		fm.assessTypeName.value = arrResult[0][9];
		fm.indueformflag.value = arrResult[0][10];
		fm.indueformflagName.value = arrResult[0][11];
		empolydate=arrResult[0][2];
		isnotused();
		//fm.operator.value =;  //操作人员
		//fm.makedate.value =; //操作时间
	}
	
}
function agentConfirm(){
	if(!verifyInput()){
		return false;
	}
	isused();
	fm.submit();//提交
}

function verifyInput(){
	if(fm.Agentcode.value == ""||fm.Agentcode.value ==null){
		alert("请先查询再进行保存");
		return false;
	}
 	var InDueFormDate = fm.InDueFormDate.value ;//转正日期
 	var makedate = fm.makedate.value ;//当前日期	
	if( InDueFormDate =="")
	{
		alert("转正日期不能为空");
		return false;
	}
	if(fm.all('AgentGrade').value==""||fm.all('AgentGrade').value==null){
		alert("转正职级不能为空！")
		return false;
	}
	var arrDueForm = InDueFormDate.split("-") ;
	var arrEmpoly = empolydate.split("-") ;
	if (arrDueForm[2]!='01'){
		alert("转正日期必须为每月的1号");
		return false;
	}	
	var InDueFormDate1 = DateAdd( "M",-1,getDate(InDueFormDate));
	var months=dateDiff(empolydate,InDueFormDate1,'D');
	if(months<0)
	{
		alert("转正日期必须为入职一个月后！");
		return false;
	}
	
	return true ;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    initForm();
    //showDiv(operateButton,"true"); 
   // showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
function isnotused()
{
	fm.all('ManageCom').disabled = true;
	fm.all('ManageComName').disabled = true;
	fm.all('employdate').disabled = true;          
	fm.all('Agentcode').disabled = true;           
	fm.all('groupAgentcode').disabled = true;      
	fm.all('agentName').disabled = true;           
	fm.all('AgentGrade').disabled = false;          
	fm.all('AgentGradeName').disabled = false;      
	fm.all('assessType').disabled = true;          
	fm.all('assessTypeName').disabled = true;
	fm.all('indueformflag').disabled = true;          
	fm.all('indueformflagName').disabled = true; 
}
function isused()
{
	fm.all('ManageCom').disabled = false;
	fm.all('ManageComName').disabled = false;
	fm.all('employdate').disabled = false;          
	fm.all('Agentcode').disabled = false;           
	fm.all('groupAgentcode').disabled = false;      
	fm.all('agentName').disabled = false;           
	fm.all('AgentGrade').disabled = false;          
	fm.all('AgentGradeName').disabled = false;      
	fm.all('assessType').disabled = false;          
	fm.all('assessTypeName').disabled = false; 
	fm.all('indueformflag').disabled = false;          
	fm.all('indueformflagName').disabled = false; 
}

