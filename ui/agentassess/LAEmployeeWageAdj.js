 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var queryCount=false;
var arrDataSet;
var turnPage = new turnPageClass();


window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function initEdorType(cObj)
{
	mEdorType = " #1# and (code=#11# or code=#12#)";
	showCodeList('employeeaclass',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and (code=#11# or code=#12#)";
	showCodeListKey('employeeaclass',[cObj], null, null, mEdorType, "1");
}

//提交，保存按钮对应操作
function submitForm()
{
  //修改：修改校验功能
  //修改时间：2004-03-31
  //修改人： LL
  //if (!beforeSubmit())
  //  return false;
   
  //首先检验录入框
  if(!verifyInput()) 
    return false;
  //校验输入类型
  if( fm.all('AClass').value != '11' && fm.all('AClass').value != '12' )
  {
     //alert(fm.all('AClass').value);
     alert("福利类型只能选择基薪或社保扶持金！！");
     fm.all('AClass').focus();
     return false;
  }
  
  var i = 0;
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //修改：注释掉 LL 2004-03-31 
  /*
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  */
  fm.mOperate.value = 'UPDATE||MAIN';
  
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LAEmployeeWageAdj.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
     //执行下一步操作
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
 fm.all('AClass').className='code';
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           


//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LAEmployeeWageAdjQuery.html");
}           



//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	 
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
  {
  	alert("请输入代理人编码！");
  	fm.all('AgentCode').focus();
  	return false;
  }
  
  return true;
}           



//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
   parent.fraMain.rows = "0,0,50,82,*";
  }
 else {
  	parent.fraMain.rows = "0,0,0,82,*";
 }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



//员工待遇级别信息字段
function queryWageAdj()
{
   //getManageCom2();
	
   var tAgentCode = fm.all('AgentCode').value;
   var strSql = "";
   var tResult;
   if (tAgentCode!='')
   {   	
   strSql = " select a.Name,b.branchattr from LAAgent a,LABranchGroup b where a.branchcode=b.agentgroup and a.agentcode='"+tAgentCode+"'";
   tResult = easyQueryVer3(strSql,1,1,1);
   }
   else  
  {  	
    fm.all('AgentCode').value = "";
    fm.all('Name').value = "";
    fm.all('BranchAttr').value = "";
    initInpBox();
    return false;    
  }
   //判断是否查询成功
  if (!tResult) 
  {
    alert("无此代理人！");
    fm.all('AgentCode').value  = "";
    fm.all('Name').value = "";
    fm.all('BranchAttr').value = "";
    initInpBox();
    return false;    
  }
   else
   {
   var arr = decodeEasyQueryResult(tResult);
   fm.all('Name').value= trim(arr[0][0]);
   fm.all('BranchAttr').value= trim(arr[0][1]);    
   }
 }  
 
function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		
		arrResult = arrQueryResult;
		fm.all('AgentCode').value = arrResult[0][0];  
		fm.all('Name').value = arrResult[0][1];                                               
		fm.all('BranchAttr').value = arrResult[0][2];                                              
		fm.all('AClass').value = arrResult[0][3];                         
                fm.all('BranchCode').value = arrResult[0][4];   
		fm.all('SumMoney').value = arrResult[0][5];                                             
		fm.all('IndexCalNo').value = arrResult[0][6];                                                                                      
		fm.all('ServeYear').value = arrResult[0][7];                                           
		fm.all('WValidDate').value = arrResult[0][8];
		fm.all('WValidEndDate').value = arrResult[0][9]; 
	                                                                                                                                                                                                                              	
 	}
 	
}

/*
 * 功能：生成四险一金 
 * 新增功能：LL
 * 新增时间：2004-03-31
 */
function  genNewData()
{	
  //首先检验录入框
  if(!verifyInput()) return false; 
  
  if( fm.all('AClass').value != '11' && fm.all('AClass').value != '12' )
  {
     //alert(fm.all('AClass').value);
     alert("福利类型只能选择基薪或社保扶持金！！");
     fm.all('AClass').focus();
     return false;
  }
  
  var i = 0;
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  fm.mOperate.value = 'INSERT||MAIN';
  
  fm.submit(); //提交 
}