//               该文件中包含客户端需要处理的函数和事件
var strRT=""; //存储代理人代码记录
//var strAgentGrade=""; //存储代理人职级
//var arrGrade = new Array();
var arrGrade = "";
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();



//***************************************************
//* 查询考核信息
//***************************************************
function easyQueryClick()
{
	
	if(!verifyInput()) return false;
	//验证必填项
	if(!checkValid())
	{
		return false;
	}
	
	var tSQL = "";
	
	tSQL  = "SELECT"; 
	tSQL += " b.groupAgentCode,";
	tSQL += " b.Name,";
	tSQL += " c.BranchAttr,";
	tSQL += " c.Name,";
	tSQL += " a.AgentGrade,";
	tSQL += " '',";
	tSQL += " a.CalAgentGrade,";
	tSQL += " (SELECT GradeName FROM LAAgentGrade WHERE GradeCode= a.CalAgentGrade)";
	tSQL += " ,a.AgentCode,(case when a.state='01' then '考核未确认' when a.state='02' then '考核已确认' else '' end)";
	tSQL += " FROM";
	tSQL += " LAAssess a,";
	tSQL += " LAAgent b,";
	tSQL += " LABranchGroup c,";
	tSQL += " LAAgentGrade d";
	tSQL += " WHERE";
	tSQL += " a.AgentCode=b.AgentCode AND";
	tSQL += " a.AgentGroup=c.AgentGroup AND";
	tSQL += " a.AgentGrade=d.GradeCode and a.state in ('01','02')";
	tSQL += getWherePart("a.indexcalno","IndexCalNo");	
	tSQL += getWherePart("a.ManageCom","ManageCom","like");
	tSQL += getWherePart("a.state","State");

	//tSQL += " and b.agentstate<='02'";
	tSQL += " and a.BranchType='3' and a.BranchType2='01'";
//	tSQL += " and not exists ";
//	tSQL += "(select 'X' from laassess where agentcode = a.agentcode and indexcalno='"+fm.IndexCalNo.value+"' and state in ('02'))";
	tSQL += " ORDER BY c.BranchAttr,a.AgentGrade";
	
	var tResult = easyQueryVer3(tSQL,1,0,1);
	if(tResult==null||tResult==""){
		alert("查询失败，没有相关信息");
		return false;
	}
	//执行查询并返回结果
	turnPage.queryModal(tSQL, EvaluateGrid);
}

//***************************************************
//* 验证必填项
//***************************************************
function checkValid()
{
	var month = fm.IndexCalNo.value.substring(4,6);

	if (month!='03'&&month!='06'&&month!='09'&&month!='12')
	{
		alert("考核年月输入错误!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
    var strSql = "select count(1) from laassess where state in ('01','02') "
	    +getWherePart('BranchType')
        +getWherePart('BranchType2')
        +getWherePart('IndexCalNo')
        +getWherePart('managecom','ManageCom','like');
	var arr = new Array();
    var arr = easyExecSql(strSql);
    if(arr[0][0]<=0){
    	alert("没有相关数据");
    	return false;
    }

    
	return true;
}

//***************************************************
//* 点击‘保存’进行的操作
//***************************************************
function submitForm()
{
	// 提交前的验证
	if(!verifyInput()) return false;
	if (checkValid() == false)
    return false;
	var tRowCount = EvaluateGrid.mulLineCount;  //取得MulLine的行数	
	  //判断是否有数据需要处理
	  if(tRowCount==0)
	  {
	  	alert("无考核结果数据！请先查询！");
	  	return false;
	  }
	 var adjustStateSql ="select '1' from laassess where state = '00'"
	                    +getWherePart('IndexCalNo')	
	                    +getWherePart('BranchType')
	                    +getWherePart('BranchType2')
	                    +getWherePart('managecom','ManageCom','like')
	 var adjustStateSqlResult = easyQueryVer3(adjustStateSql,1,0,1);
	 if(adjustStateSqlResult!=null&&adjustStateSqlResult!=''){
	     	   alert("有未调整确认的人员，请先调整确认");
	     	   return false;
	 }
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.submit();   
    return true;
}

//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    initEvaluateGrid();
  }
  
}

function downLoad()
{
	var tSql = "";
	tSql  = "SELECT"; 
	tSql += " b.groupAgentCode,";
	tSql += " b.Name,";
	tSql += " c.BranchAttr,";
	tSql += " c.Name,";
	tSql += " a.AgentGrade,";
	tSql += " a.CalAgentGrade,";
	tSql += " (SELECT GradeName FROM LAAgentGrade WHERE GradeCode= a.CalAgentGrade)";
	tSql += " ,(case when a.state='01' then '考核未确认' when a.state='02' then '考核已确认' else '' end)";
	tSql += " FROM";
	tSql += " LAAssess a,";
	tSql += " LAAgent b,";
	tSql += " LABranchGroup c,";
	tSql += " LAAgentGrade d";
	tSql += " WHERE";
	tSql += " a.AgentCode=b.AgentCode AND";
	tSql += " a.AgentGroup=c.AgentGroup AND";
	tSql += " a.AgentGrade=d.GradeCode and a.state in ('01','02')";
	tSql += getWherePart("a.indexcalno","IndexCalNo");	
	tSql += getWherePart("a.ManageCom","ManageCom","like");
	tSql += getWherePart("a.state","State");
	tSql += " and a.BranchType='3' and a.BranchType2='01'";
	tSql += " ORDER BY c.BranchAttr,a.AgentGrade";
	
	var tResult = easyQueryVer3(tSql,1,0,1);
	if(tResult==null||tResult==""){
		alert("下载失败，没有相关信息");
		return false;
	}
	
	var i = 0;
	fm.all("querySql").value = tSql;
	fm.action = "./BKAssessToDownLoad.jsp"
	fm.submit();
}