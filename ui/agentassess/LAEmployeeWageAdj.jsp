<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LAEmployeeWageAdj.jsp
//程序功能：
//创建日期：2003-07-09 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LAEmployeeWageAdj.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAEmployeeWageAdjInit.jsp"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>  
<title>员工待遇级别调整</title>
</head>
<body  onload="initForm();" >
  <form action="./LAEmployeeWageAdjSave.jsp" method=post name=fm target="fraSubmit">  
  <%@include file="../common/jsp/OperateButton1.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%> 
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            员工编码
          </TD>
          <TD  class= input>
            <Input class=common  name=AgentCode verify="员工编码|NOTNULL" onchange="return queryWageAdj();" > 
          </TD>
          <TD  class= title>
            员工姓名
          </TD>
          <TD  class= input>
            <Input class=readonly  name=Name readonly verify="员工姓名|NOTNULL" > 
          </TD>
        </TR>
          <TR  class= common>
          <TD  class= title>
            展业机构
          </TD>
          <TD  class= input>
            <Input class=readonly name=BranchAttr readonly verify="展业机构|NOTNULL">
          </TD> 
         <TD
         class=title>福利类别
         </td>
        <td class=input> 
            <Input class=code readonly name=AClass  verify="福利类别|notnull&code:employeeaclass"
                                 ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);" >
        </td>              
        </TR>    
       <TR class=common>
        <TD
         class=titles>员工待遇级别
         </td>
        <td class=input> 
            <Input class= 'readonly' readonly name=BranchCode >
        </td>               
        <TD  class= title>
            福利金额
          </TD>
          <TD  class= input>
            <Input class=common name=SumMoney  verify="福利金额|NOTNULL">
          </TD> 
        </tr>
       <TR  class= common>
          <TD  class= title>
            最近一次薪资计算月份 
          </TD>
          <TD  class= input>
            <Input class=common  name=IndexCalNo verify="考核年月|NOTNULL&INT&len=6" > (例:200401)
          </TD>
          <TD  class= title>
            享受员工制月份(例:3)
          </TD>
          <TD  class= input>
            <Input class=common  name=ServeYear  verify="服务期限|NOTNULL" > 
          </TD>
       </TR>
       <TR  class= common>
          <TD  class= title>
            生效日期
          </TD>
          <TD  class= input>
            <Input class='readonly'  readonly name=WValidDate  > 
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class='readonly'  readonly  name=WValidEndDate  > 
          </TD>
        </TR>
        <TR  class= common>
           <TD class=common>
             <input type =button class=common value="生成福利待遇" onclick="genNewData();">   
           </TD>
        </TR>
     
    
     <input type=hidden name=hideOperate value=''>
     <Input type=hidden name=mOperate value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>










 