<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：XQConYearReport.jsp
//程序功能：F1报表生成
//创建日期：2006-02-20
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.agentassess.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="java.io.*"%>

<%
		System.out.println("start");
		CError cError = new CError( );
		boolean operFlag=true;
		String tRela  = "";
		CombineVts tcombineVts = null;
		String FlagStr = "";
		String Content = "";
		String strOperation = "";		

		String ManageCom = request.getParameter("ManageCom");
		String AssessYear = request.getParameter("AssessYear");
		String BranchType = request.getParameter("BranchType");
		String BranchType2 = request.getParameter("BranchType2");

		System.out.println(ManageCom);
		System.out.println(AssessYear);
		
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput)session.getValue("GI");
		TransferData tTransferData = new TransferData();
		
		tTransferData.setNameAndValue("ManageCom",ManageCom);
		tTransferData.setNameAndValue("AssessYear",AssessYear);
		tTransferData.setNameAndValue("BranchType",BranchType);
		tTransferData.setNameAndValue("BranchType2",BranchType2);

		VData mResult = new VData();
		CErrors mErrors = new CErrors();
		
		VData tVData = new VData();
		tVData.addElement(tTransferData);
		tVData.addElement(tG);
				
		QuarterCheckTeamBL tQuarterCheckTeamBL = new QuarterCheckTeamBL();
		XmlExport txmlExport = new XmlExport();

    if(!tQuarterCheckTeamBL.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tQuarterCheckTeamBL.mErrors.getFirstError().toString();
    }
    else
    {
			mResult = tQuarterCheckTeamBL.getResult();
			txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
			if(txmlExport==null)
			{
				operFlag=false;
				Content="没有得到要显示的数据文件";
			}
			
		}

if (operFlag==true)
		{
				  ExeSQL tExeSQL = new ExeSQL();
		//获取临时文件名
		String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
		String strFilePath = tExeSQL.getOneValue(strSql);
		String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
		//获取存放临时文件的路径
		String strRealPath = application.getRealPath("/").replace('\\','/');
		String strVFPathName = strRealPath +"//"+ strVFFileName;
	  
	  //session.putValue("PrintStream", txmlExport.getInputStream());
	  //System.out.println("put session value");
	  ////response.sendRedirect("../f1print/GetF1Print.jsp");
	  //response.sendRedirect("../uw/GetF1PrintJ1.jsp");
	  
	  //合并VTS文件
		String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
	
		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);
	
		//把dataStream存储到磁盘文件
		//System.out.println("存储文件到"+strVFPathName);
		AccessVtsFile.saveToFile(dataStream,strVFPathName);
		System.out.println("==> Write VTS file to disk ");
	
		System.out.println("===strVFFileName : "+strVFFileName);
		//本来打算采用get方式来传递文件路径
		response.sendRedirect("../uw/GetF1PrintJ1.jsp?Code=07&RealPath="+strVFPathName);
	}
		else
		{
    	FlagStr = "Fail";
%><html><script language="javascript">
		alert("<%=Content%>");
		top.close();
	</script></html><%
  }
%>