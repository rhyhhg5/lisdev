<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAGrpRearQueryInput.jsp
//程序功能：
//创建日期：2009-1-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="./LAGrpRearQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAGrpRearQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>育成关系查询</title>
</head>  
<body  onload="initForm();initElementtype();" >
  <form action="./LAGrpRearQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLARewardPunish1);">
    
    <td class=titleImg>
      育成关系查询条件
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLARewardPunish1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
         <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom  verify="管理机构|NOTNULL"
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            ><Input class="codename" name=ManageComName readOnly  elementtype=nacessary> 
          </TD> 
       
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  被育成人代码
		</td>
        <td  class= input> 
		  <input class= common name=AgentCode > 
		</td>
        <td  class= title> 
		  被育成人姓名
		</td>
        <td  class= input> 
		  <input class=common name=AgentName > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  销售团队代码
		</td>
        <td  class= input> 
		  <input class=common name=BranchAttr > 
		</td>
        <td  class= title> 
		  销售团队名称 
		</td>
        <td  class= input> 
		  <input class=common name=BranchName > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  育成人代码
		</td>
        <td  class= input> 
		  <input name=RearAgentCode class= common  > 
		</td>
        <td  class= title> 
		  育成人名称
		</td>
        <td  class= input> 
		  <input name=RearAgentName class= common  > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  被育成人销售团队代码
		</td>
        <td  class= input> 
		  <input name=RearBranchAttr class= common  > 
		</td>
        <td  class= title> 
		  被育成人销售团队名称
		</td>
        <td  class= input> 
		  <input class= common name=RearBranchName > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  育成起期
		</td>
        <td  class= input> 
		  <Input class= "coolDatePicker" dateFormat="short" name=StartDate  > 
		</td>
        <td  class= title> 
		 有效状态
		</td>
    <td  class= input> 
		  <input class='codeno' name=RearFlag verify="有效状态|code:RearFlag"
		         CodeData="0|^0|无效 ^1|有效" 
		         ondblclick="showCodeListEx('RearFlag',[this,RearFlagName],[0,1]);" 
		         onkeyup="showCodeListKeyEx('RearFlag',[this,RearFlagName],[0,1]);"
		         ><input class='codename' name=RearFlagName   >
  
		</td>  
      </tr>
      
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返  回" class=cssbutton TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRewardPunishGrid);">
    		</td>
    		<td class= titleImg>
    			 育成关系信息结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divRewardPunishGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="SpanRewardPunishGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 					
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
