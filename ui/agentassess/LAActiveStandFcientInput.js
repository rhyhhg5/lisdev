//               该文件中包含客户端需要处理的函数和事件
//程序名称：LAActiveStandFcientInput.js
//程序功能：
//创建时间：2017-12-12
//创建人  ：wangqingmin
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var mOperate="";




//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//xiugai按钮对应操作
function updateClick()
{
	mOperate="UPDATE||MAIN";
	if(!beforeSubmit()) {
		return false;
	}
	if (confirm("您确实想修改该记录吗?"))
     {
        mOperate="UPDATE||MAIN";
        submitForm();
     }
     else
    {
        mOperate="";
        alert("您取消了修改操作！");
    }

}


function addClick() {

	mOperate="INSERT||MAIN";
	if(!beforeSubmit()) {
		return false;
	}
	submitForm();
}

//提交，保存按钮对应操作

function submitForm()
{ 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  } 
 
  fm.submit(); //提交
  
  
  
}

function deleteClick()
{
	mOperate="DELETE||MAIN"; 
	if(!beforeSubmit()) {
		return false;
	}
	if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}

// 查询按钮
function easyQueryClick()
{
	//fm.all('saveButton').disabled = true;
	//alert ("start easyQueryClick");
	if(fm.all('ManageCom').value ==null || fm.all('ManageCom').value =='')
	{
		alert("管理机构不能为空,请录入管理机构!");
		return false;
	}
	if(fm.all('Rate').value!=null&&fm.all('Rate').value!=""){
	    if(!checkNumber(fm.all('Rate'))){
	    	return false;
	    }

	}
	// 初始化表格
	initLADiscountGrid();
	// 书写SQL语句
	var strSQL = "";
	var conStr = "";
	if (!(fm.all('Rate').value == null || fm.all('Rate').value ==""))
	{
		  conStr = " and a.rate = " + parseFloat(fm.all('Rate').value );
	}				
	strSQL = "select a.idx,a.managecom,(select name from ldcom where comcode=a.managecom),a.riskcode,b.riskname,a.payintv,a.rate "
				 + "from ladiscount a,lmriskapp b "
				 + "where a.riskcode = b.riskcode and discounttype='01' and a.branchtype ='5' and a.branchtype2='01'  "
				 + getWherePart( 'a.riskcode','RiskCode')
				 + getWherePart( 'a.payintv','PayIntv')
				 + getWherePart('a.managecom','ManageCom','like')
				 + conStr
				 ;

	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
			alert("没有符合条件的数据，请重新录入查询条件！");
			return false;
	}

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;

					//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
					//设置初始化过的MULTILINE对象
					turnPage.pageDisplayGrid = LADiscountGrid;
					//保存SQL语句
					turnPage.strQuerySql = strSQL;

					//设置查询起始位置
					turnPage.pageIndex = 0;

					//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
					tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

					//调用MULTILINE对象显示查询结果
					displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
				}


				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}

	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
	function resetForm()
		{
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
		}




	//显示frmSubmit框架，用来调试
	function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}


		//Click事件，当点击“修改”图片时触发该函数
		function DoUpdate()
		{
			if (confirm("您确实想修改该记录吗?"))
			{
				mOperate = "UPDATE||MAIN";
				if(!beforeSubmit()) return false;
				var i = 0;
				var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.submit(); //提交
			}
			else
				{
					fm.OperateType.value = "";
					alert("您取消了修改操作！");
				}
			}

							//判断是否选择了要增加、修改或删除的行
							function chkMulLine()
							{
								//alert("enter chkmulline");
								var i;
								var selFlag = true;
								var iCount = 0;
								var rowNum = LADiscountGrid.mulLineCount;
								for(i=0;i<rowNum;i++)
								{
									
									if(LADiscountGrid.getChkNo(i))
									{
										iCount++;
										if((LADiscountGrid.getRowColData(i,2) == null)||(LADiscountGrid.getRowColData(i,2) == ""))
		   	   							 {
		   	    								alert("管理机构代码不能为空");
		   	    								LADiscountGrid.setFocus(i,2,LADiscountGrid);
		   	    								selFlag = false;
		   	    								break;
		   	   							}		
										if((LADiscountGrid.getRowColData(i,4) == null)||(LADiscountGrid.getRowColData(i,4) == ""))
   	   							 {
   	    								alert("险种代码不能为空");
   	    								LADiscountGrid.setFocus(i,4,LADiscountGrid);
   	    								selFlag = false;
   	    								break;
   	   							}
										if((LADiscountGrid.getRowColData(i,6) == null)||(LADiscountGrid.getRowColData(i,6) == ""))
										{
											alert("缴费年期不能为空");
											LADiscountGrid.setFocus(i,6,LADiscountGrid);
											selFlag = false;
											break;
									  	}
										if((LADiscountGrid.getRowColData(i,7) == null)||(LADiscountGrid.getRowColData(i,7) == ""))
										{
											alert("折标系数不能为空");
											LADiscountGrid.setFocus(i,7,LADiscountGrid);
											selFlag = false;
											break;
										}
										
										
										if(!isNumeric(LADiscountGrid.getRowColData(i,6)))
   	   							 {
   	       							alert("缴费年期不是有效的数字！");
   	       							LADiscountGrid.checkBoxAllNot();
   	       							LADiscountGrid.setFocus(i,6,LADiscountGrid);
   	       							selFlag = false;
   	        						break;
   	     						}
										 if(!isNumeric(LADiscountGrid.getRowColData(i,7)))
   	   							 {
   	       							alert("折标系数不是有效的数字！");
   	       							LADiscountGrid.checkBoxAllNot();
   	       							LADiscountGrid.setFocus(i,7,LADiscountGrid);
   	       							selFlag = false;
   	        						break;
   	     						}


										//如果ｒａｔｅ为空不能删除和更新
										if((LADiscountGrid.getRowColData(i,1) == null)||(LADiscountGrid.getRowColData(i,1)==""))
	    							{
	    								if((mOperate =="DELETE||MAIN") || (mOperate =="UPDATE||MAIN"))
	    								{
	    	   							 alert("必须先查询出记录，再修改和删除！");
	    	   							 LADiscountGrid.checkBoxAllNot();
	    	    						 LADiscountGrid.setFocus(i,2,LADiscountGrid);
	    	    						 selFlag = false;
	    	   							 break;
	    								}
	    							}
	    							else if ((LADiscountGrid.getRowColData(i,1) != null)||(LADiscountGrid.getRowColData(i,1)!=""))
   	                {	//如果Serialno不为空不能插入
   	    	            if(mOperate =="INSERT||MAIN")
   	    	            {
   	    	               alert("此纪录已存在，不可插入！");
   	    	               LADiscountGrid.checkBoxAllNot();
   	    	               LADiscountGrid.setFocus(i,2,LADiscountGrid);
   	    	               selFlag = false;
   	    	                break;
   	    	             }
   	                }
								}		
									else
									{}
								}
										if(!selFlag) return selFlag;
										if(iCount == 0)
										{
											alert("请选择要保存或删除的记录!");
											return false
										}
										return true;
									
                  }
//--									//提交前的校验、计算
									function beforeSubmit()
									{
										
										
										if(!chkMulLine()) return false;
										if(fm.all('PayIntv').value<0) {
											alert("保险期间必须为正值！");
											return false;
										}	
										
										if(fm.all('Rate').value<0) {
											alert("折标系数必须为正值！");
											return false;
										}	
										
										return true;
									}

									function showDiv(cDiv,cShow)
									{
										if (cShow=="true")
										{
											cDiv.style.display="";
										}
										else
											{
												cDiv.style.display="none";
											}
									}

//Click事件，当点击“下载”图片时触发该函数								
function downLoad(){

	if(fm.all('ManageCom').value==null||fm.all('ManageCom').value==""){
		alert("管理机构代码不能为空");
		return false;
	}

	// 书写SQL语句
	var strSQL = "";
	var conStr = "";
	if (!(fm.all('Rate').value == null || fm.all('Rate').value ==""))
	{
		  conStr = " and a.rate = " + parseFloat(fm.all('Rate').value );
	}				
	strSQL = "select a.managecom,(select name from ldcom where comcode = a.managecom),a.riskcode,b.riskname,(case a.payintv when '0' then '趸交' when '1' then '1年含以内' when '3' then '3年期' when '5' then '5年期' when '10' then '10年期'  else '10年期以上' end),a.rate "
				 + "from ladiscount a,lmriskapp b "
				 + "where a.riskcode = b.riskcode and discounttype='01' and a.branchtype ='5' and a.branchtype2='01'  "
				 + getWherePart( 'a.riskcode','RiskCode')
				 + getWherePart( 'a.payintv','PayIntv')
				 + getWherePart('a.managecom','ManageCom','like')
				 + conStr
				 ;
	var oldAction = fm.action;
    fm.querySql.value = strSQL;
    fm.action="	LAActiveStandFcientDownLoad.jsp";
    fm.submit();
    fm.action=oldAction;

}
						
function moduleDownload(){
	var tSQL="";
	//fm.querySql.value = tSQL;
    var oldAction = fm2.action;
    fm.action = "./LAActiveRateDownload.jsp";
    fm.submit();
    fm.action = oldAction;
}

function diskImport()
{ 
    //alert("111111111111111");
		//alert("====="+fm.all("diskimporttype").value);
   if(fm2.FileName.value==null||fm2.FileName.value==""){
   	 alert("请选择要导入的文件！");
   	 return false;
   }
   var showStr="正在导入数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   fm2.submit();
}

function clearImport(){
	
	initInpBox1();
	document.getElementById('FileName').outerHTML=document.getElementById('FileName').outerHTML;
	initImportResultGrid();
}

function queryImportResult(){
	var tbatchno=fm2.FileImportNo.value;
	var tqueryType=fm2.queryType.value;
	
	str_SQL = "select batchno,contid,errorinfo"
	+ " from lcgrpimportlog "
	+ " where 1=1 and contno='ActivetandRate'  and grpcontno='00000000000000000000' " ;
	if(tbatchno!=""&&tbatchno!=null)
	{
		str_SQL+=" and batchno='"+tbatchno+"' ";
	}
	if(tqueryType==1){
		str_SQL+=" and errorstate='1'";
	}else if(tqueryType==2){
		str_SQL+=" and errorstate='0'";
	}
	str_SQL+=" order by contid";
  //执行查询并返回结果
	turnPage2.queryModal(str_SQL, ImportResultGrid);
}




