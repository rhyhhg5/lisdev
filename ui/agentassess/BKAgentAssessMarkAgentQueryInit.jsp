<script language="JavaScript">
function initInpBox()
{
  try
  {
   	fm.all('GroupAgentCode').value="";
  	fm.all('BranchType').value='<%=BranchType%>';
  	fm.all('BranchType2').value='<%=BranchType2%>';
  	fm.all('AssessDate').value='<%=AssessDate%>';
  	fm.all('AssessType').value='<%=AssessType%>';
    fm.all('ManageCom').value = '<%=tG.ManageCom%>';
  }catch(ex){alert("在BKAgentAssessInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");}
}

function initAssessMarkGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="员工编号"; //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许


    iArray[2]=new Array();
    iArray[2][0]="员工姓名"; //列名
    iArray[2][1]="50px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="状态"; //列名
    iArray[3][1]="50px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="原业务员编码"; //列名
    iArray[4][1]="50px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=3;              //是否允许输入,1表示允许,0表示不允许
   
    
    AssessMarkGrid = new MulLineEnter( "fm" , "AssessMarkGrid");
    
    AssessMarkGrid.mulLineCount = 0;   
    AssessMarkGrid.displayTitle = 1;
    AssessMarkGrid.locked=1;
    AssessMarkGrid.canSel=1;
    AssessMarkGrid.canChk=0;
    AssessMarkGrid.hiddenPlus=1;
    AssessMarkGrid.hiddenSubtraction=1;
    AssessMarkGrid.loadMulLine(iArray); 
  }
  catch(ex)
  {
    alert("在AfterAscriptQryInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox(); 
    initAssessMarkGrid();
    easyQueryClick();  
  }
  catch(re)
  {
    alert("InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>