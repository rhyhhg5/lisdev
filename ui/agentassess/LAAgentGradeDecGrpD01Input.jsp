 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String tDivShow = "none";
%>
<script>
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAAgentGradeDecGrpD01Input.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAAgentGradeDecGrpD01Init.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <title>考核评估计算 </title>   
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
    tDivShow = "";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAgentGradeDecGrpD01Save.jsp" method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
	  <td class= titleImg align= center>请输入考核信息的查询条件：</td>
	</tr>
	</table>
    <table class= common align=center>
      <TR class= common>
          <TD class= title>
            管理机构
          </TD>
          <TD class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len=4"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,4,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,4,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>
          
          <TD class= title>
            考核年月
          </TD>
          <TD class= input>
            <Input class=common name=IndexCalNo verify="考核年月|notnull&len<=6&int" elementtype=nacessary >
            <font color="red">  'YYYYMM'</font>
          </TD>
      </TR>
      <TR class=common>
        <TD class=title>
          销售单位代码
        </TD>
        <TD class=title>
          <Input class=common name=BranchAttr>
        </TD>
        <TD class=title>
          销售单位名称
        </TD>
        <TD class=title>
          <Input class='readonly' name=BranchName readOnly >
        </TD>
      </TR>
      <TR class=common>
        <TD class=title>
          <%=tTitleAgent%>编码
        </TD>
        <TD class=title>
          <Input class=common name=GroupAgentCode onchange = "return checkAgentCode()">
        </TD>
        <TD class=title>
          <%=tTitleAgent%>名称
        </TD>
        <TD class=title>
          <Input class='common' name=AgentName >
        </TD>
      </TR>
      <!--TR class=common>
        <TD class=title>
          归属日期
        </TD>
        <TD class=input>
          <Input class='coolDatePicker' name=VestDate verify="成立标志日期|notnull&DATE" format='short' elementtype=nacessary>
        </TD>
      </TR-->
    </table>
    <!--
    <Div id= "divLAAssessTeam" style= "display: '<%=tDivShow%>'">
    	<table  class= common align=center>
    		<TR class=common>
    			<TD class=title>
    				参考序列
    			</TD>
	        <TD class=input>
	          <Input name=GroupSeries class="codeno" id="GroupSeries" 
	           ondblclick="return showCodeList('groupseries',[this,GroupSeriesName],[0,1]);" 
	           onkeyup="return showCodeListKey('groupseries',[this,GroupSeriesName],[0,1]);" 
	           ><Input class=codename name=GroupSeriesName readOnly elementtype=nacessary >
	        </TD>
	        <TD class=title>
	        	
	        </TD>
	        <TD class=input>
	          <Input class='readonly' readonly name=AAA value='<%=tDivShow%>'>
	        </TD>
    		</TR>
    	</table>
    </Div>
    -->
    <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class=cssbutton >
    <INPUT VALUE="保  存" TYPE=button onclick="return submitForm();" class=cssbutton >
    <input type=hidden name=BranchType value='<%=BranchType%>'>
    <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
    <input type=hidden name=AgentCode value=''>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAssess1);">
    		</td>
    		<td class= titleImg>
    			 考核信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLAAssess1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGradeDecideGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton > 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
