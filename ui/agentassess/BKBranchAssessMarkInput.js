//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
function  getAgentGroup()
{
   var tAssessType=fm.all('AssessType').value;
 
   strSQL = "select agentgroup from laagent where 1=1 "
           + getWherePart('AgentCode','AgentCode')
           + getWherePart('BranchType','BranchType')
           + getWherePart('BranchType2','BranchType2');
   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该业务员!');
   	fm.all('AgentGroup').value = '';
   	fm.all('AgentCode').value = '';
   	fm.all('AgentCodeName').value = '';
   	
   	return false;
   }  
   var arr = decodeEasyQueryResult(strQueryResult);
   fm.all('AgentGroup').value=arr[0][0];
  
 // else if(tAssessType=='02')
//{
//  strSQL = "select agentgroup from labranchgroup where 1=1 "
//          + getWherePart('BranchAttr','AgentCode')
//          + getWherePart('BranchType','BranchType')
//          + getWherePart('BranchType2','BranchType2');
//  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
//  if (!strQueryResult)
//  {
//  	alert('不存在该业务员!');
//  	fm.all('AgentGroup').value = '';
//  	fm.all('AgentCode').value = '';
//  	fm.all('AgentCodeName').value = '';
//  	
//  	return false;
//  }  
// var arr = decodeEasyQueryResult(strQueryResult);
// fm.all('AgentGroup').value=arr[0][0];
//}

   //alert(fm.all('AgentGroup').value);
   return true;
}
function getname()
{
   	var tReturn = getManageComLimitlike("ManageCom");   
     	var Sql="select name from laagent where 1=1 "
     	+ getWherePart('AgentCode','AgentCode')
	+ getWherePart('BranchType','BranchType')
        + getWherePart('BranchType2','BranchType2')     	
     	+tReturn;
	var strQueryResult  = easyQueryVer3(Sql, 1, 0, 1);  

    	var arr = decodeEasyQueryResult(strQueryResult);
    	
    	fm.all("AgentCodeName").value = arr[0][0];  
}
function queryMark(type)
{
 var tAssessType=fm.all('AssessType').value;
 
  var tToolMark=fm.all('ToolMark').value;
  var tChargeMark=fm.all('ChargeMark').value;
  var tMeetMark=fm.all('MeetMark').value;
  var tRuleMark=fm.all('RuleMark').value; 

if(type=='1'){
  if(tToolMark >100){
	alert("活动工具使用分数的数值应在0-100之间");
	return false;
	}

  if(tChargeMark >100){
	alert("培训管理分数的数值应在0-100之间");
	return false;
    }

  if(tMeetMark > 100){
	alert("会议纪律分数的数值应在0-100之间");
	return false;
    }

  if(tRuleMark > 100){
	alert("考勤分数的数值应在0-100之间");
	return false;
    }
}else{
  if(tToolMark >100){
	alert("活动工具管理能力分数的数值应在0-100之间");
	return false;
	}

  if(tChargeMark >100){
	alert("费用使用分数的数值应在0-100之间");
	return false;
    }

  if(tMeetMark > 100){
	alert("会议经营能力分数的数值应在0-100之间");
	return false;
    }

  if(tRuleMark > 100){
	alert("遵守规章制度分数的数值应在0-100之间");
	return false;
  }
}
  
  if (tAssessType=='03')
  {
   var t=tToolMark*0.35+tChargeMark*0.30+tMeetMark*0.1+tRuleMark*0.25;
   //fm.all('AssessMark').value=t.toFixed(2);

  }
  else if (tAssessType=='02')
  {
   var t=tToolMark*0.30+tChargeMark*0.30+tMeetMark*0.25+tRuleMark*0.15; 
   //fm.all('AssessMark').value=t.toFixed(2);
  }

  if(t.toFixed(2)>100){
	fm.all('AssessMark').value=t.toFixed(2);
	alert("总分的数值应在0-100之间");
	return false;
	
  }else{
	fm.all('AssessMark').value=t.toFixed(2);
  }


}

//提交，保存按钮对应操作
function submitForm()
{
	 
	if (verifyInput() == false)
    return false;
  var i = 0;
 
  if (!beforeSubmit()) return false;
  if (!getAgentGroup()) return false;
 // if (!check()) return false;
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  

  
  fm.all('hideOperate').value=mOperate;

	//alert(fm.all('hideOperate').value);
  if (fm.all('hideOperate').value=="")
  {
    fm.all('hideOperate').value="INSERT||MAIN";
  }
  //调试信息窗口
  //showSubmitFrame(mDebug);

  //alert(fm.all('hideOperate').value);
  fm.submit(); 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
//  fm.all('hideIsManager').value = false;
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LABankAgentInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

//提交前的校验、计算  
function beforeSubmit()
{
	var tManageCom=fm.all('ManageCom').value;	
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
	var tAssessType=fm.all('AssessType').value;
  
     	var tReturn = getManageComLimitlike("ManageCom");
    
     	var Sql="select AgentGrade from latree where 1=1 "
     	+ getWherePart('AgentCode','AgentCode')
	+ getWherePart('BranchType','BranchType')
        + getWherePart('BranchType2','BranchType2')     	
     	+tReturn;
 
	var strQueryResult  = easyQueryVer3(Sql, 1, 0, 1);  

    	var arr = decodeEasyQueryResult(strQueryResult);
    	
    	var tAgentGrade = arr[0][0];  
    	
      if (tAssessType=='03')
     {	
     	
     	if(tAgentGrade>'D10')
     	{     		
     		alert("必须录入业务系列的人员!");
     		return false ;
     	}
     }
      else if (tAssessType=='02')
     {	
     	if(tAgentGrade<'E01')
     	{
     		alert("必须录入管理系列的人员!");
     		return false ;
     	}
     }
	 if(!check()) return false;
     queryMark();
  //   alert(fm.all('AssessMark').value);
    return true;
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 else 
 {
  	parent.fraMain.rows = "0,0,0,82,*";
 }
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{

  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的代理人记录！');    
  }else
  {
	if(!check()) return false;
    if (confirm("您确实想修改该记录吗?"))
    {  	
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}  

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  var tAssessType=fm.all('AssessType').value;
  //alert(fm.all('AssessType').value);
  if(tAssessType=='03')
  {
    showInfo=window.open("./BKAssessMarkQuery.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
   }
  else if(tAssessType=='02')
  {
    showInfo=window.open("./BKBranchAssessMarkHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
  }
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要删除的代理人记录！');
  }else
  {	
  	//下面增加相应的删除代码
  	if (confirm("您确实想删除该记录吗?"))
  	{
    		mOperate="DELETE||MAIN";  
    		submitForm();
  	}
  	else
  	{
    		mOperate="";
    		alert("您取消了删除操作！");
  	}
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function afterQuery(arrQueryResult)
{
	initForm();
	if( arrQueryResult != null )
	{
		fm.all('GroupAgentCode').value = arrQueryResult[0];
		var tYAgentCodeSQL = "select agentcode,name from laagent where groupagentcode = '"+arrQueryResult[0]+"' ";
		var strQueryResult = easyExecSql(tYAgentCodeSQL);
		if(!strQueryResult){
			fm.all('GroupAgentCode').value = "";
			alert("获取业务员编码失败！");
			return false;
		}
		fm.all('AgentCode').value = strQueryResult[0][0];
		fm.all('AgentCodeName').value=strQueryResult[0][1];
		//showOneCodeNametoAfter('Agentcodet','AgentCode');
		//fm.all('Idx').value=arrQueryResult[1];
		fm.all('AssessType').value=arrQueryResult[2];
		fm.all('DoneDate').value=arrQueryResult[3];
		fm.all('AssessDate').value=arrQueryResult[4];
		fm.all('ToolMark').value=arrQueryResult[5];
		fm.all('ChargeMark').value=arrQueryResult[6];
		fm.all('MeetMark').value=arrQueryResult[7];
		fm.all('RuleMark').value=arrQueryResult[8];
		fm.all('AssessMarkCount').value=arrQueryResult[10];
		fm.all('AssessPersonCount').value=arrQueryResult[11];
		mOperate="";
		fm.all('AssessDate').disabled=true;
		fm.all('AgentCode').disabled=true;
		fm.all('hideAssessData').value=arrQueryResult[4];
		fm.all('hideAgentcode').value= strQueryResult[0][0];
	}
}  

function check(){

	var tToolMark = fm.all('ToolMark').value;
	var tChargeMark = fm.all('ChargeMark').value;
	var tMeetMark = fm.all('MeetMark').value;
	var tRuleMark = fm.all('RuleMark').value;
	var tAssessMark = fm.all('AssessMark').value
	var tAssessDate = fm.all('AssessDate').value;

	if(tToolMark >100||tToolMark<0){
		alert("活动工具管理能力分数的数值应在0-100之间");
		return false;
	}

	if(tChargeMark >100||tChargeMark<0){
		alert("费用使用分数的数值应在0-100之间");
		return false;
    }

	if(tMeetMark > 100||tMeetMark<0){
		alert("会议经营能力分数的数值应在0-100之间");
		return false;
    }

	if(tRuleMark > 100||tRuleMark<0){
		alert("遵守规章制度分数的数值应在0-100之间");
		return false;
	}

	if(tAssessDate.substring(4,6)>"12"){
		alert("日期输入错误");
		return false;
	}
	var tSQL="select 1 from lawage where agentcode='"+fm.AgentCode.value+"' and indexcalno='"+tAssessDate+"'";
	var tstrQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
	if(tstrQueryResult){
		alert("该业务员已经进行过薪资计算,不能进行录入考核分数!");
		return false;
	}
	return true;
}
function QueryAgent(){
  //下面增加相应的代码
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  var tAssessType=fm.all('AssessType').value;
	var tManageCom=fm.all('ManageCom').value;	
  var tAssessDate = fm.all('AssessDate').value;
	if(tAssessDate==""){
		alert("请先录入考评年月!");
		return false;
	}
  showInfo=window.open("./BKAssessMarkAgentQuery.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2 +"&ManageCom=" + tManageCom +"&AssessDate=" + tAssessDate+"&AssessType=" + tAssessType);
}
function afterAgentQuery(arrQueryResult)
{
	if( arrQueryResult != null )
	{
		fm.all('AgentCode').value = arrQueryResult[3];
		fm.all('GroupAgentCode').value = arrQueryResult[0];
		fm.all('AgentCodeName').value=arrQueryResult[1];
	}
} 