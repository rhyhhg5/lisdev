<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAssessSave.jsp
//程序功能：
//创建日期：2003-07-15 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentassess.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LATreeTempSchema mLATreeTempSchema = new LATreeTempSchema();
  LATreeTempSet mLATreeTempSet = new LATreeTempSet();
  LAAssessGrpInputUI mLAAssessGrpInputUI  = new LAAssessGrpInputUI();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT||MAIN";
  String tRela  = "";
  String FlagStr = "";
  String Content = "";

  GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin prepare LAAssessSet...");
  String tManageCom = request.getParameter("ManageCom");
  String tBranchType = request.getParameter("BranchType");
  String tBranchType2 = request.getParameter("BranchType2");
  String tStartDate = request.getParameter("StartDate");
  tStartDate=AgentPubFun.formatDate(tStartDate,"yyyy-MM-dd"); 
  String tAgentGrade = request.getParameter("AgentGrade");  
  
  //取得Muline信息
  int lineCount = 0;
  String tAgentCode[] = request.getParameterValues("LAAssessGrid1");
  String tAgentGrade1[] = request.getParameterValues("LAAssessGrid3");
  String tAgentGroup[] = request.getParameterValues("LAAssessGrid4");
  String tAgentSeries[] = request.getParameterValues("LAAssessGrid5");
  lineCount = tAgentCode.length; //行数
  
  String tCvaliMonth=AgentPubFun.formatDate(tStartDate,"yyyyMM");  
  for(int i=0;i<lineCount;i++)
  {
     String tBranchAttr = tAgentGroup[i];
     String tSql = "select agentgroup from labranchgroup where branchattr='"+tBranchAttr+"' and branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"'";
     ExeSQL tExeSQL= new ExeSQL();
     String tnowAgentGroup = tExeSQL.getOneValue(tSql);
     
	String agentcode ="";
     if(!"".equals(tAgentCode[i]))
     {
	     String sql ="select getAgentcode('"+tAgentCode[i]+"') from dual";
     	 agentcode = new ExeSQL().getOneValue(sql);
     }
     mLATreeTempSchema = new LATreeTempSchema();
     mLATreeTempSchema.setAgentCode(agentcode);  
     mLATreeTempSchema.setAgentGrade(tAgentGrade1[i]);
     mLATreeTempSchema.setCValiMonth(tCvaliMonth);
     mLATreeTempSchema.setAgentGroup(tnowAgentGroup);
     mLATreeTempSchema.setStartDate(tStartDate);
     mLATreeTempSchema.setAssessType(""); //默认为职级考核维度  
     mLATreeTempSchema.setManageCom(tManageCom);
     mLATreeTempSchema.setBranchType(tBranchType);
     mLATreeTempSchema.setBranchType2(tBranchType2);
     mLATreeTempSchema.setCValiFlag("1");     
     mLATreeTempSet.add(mLATreeTempSchema); 
  }
 
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.addElement(mLATreeTempSet);
  tVData.addElement(tBranchType);
  tVData.addElement(tBranchType2);
  try
  {
    mLAAssessGrpInputUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLAAssessGrpInputUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
