var turnPage = new turnPageClass();

/***************************************************
 * 执行计算操作
 ***************************************************
 */
 function AgentCalExamineSave()
 {
 	 submitForm();
 }

/***************************************************
 * 执行提交操作
 ***************************************************
 */
 function submitForm()
 {
 	 //对非空进行验证
   if( verifyInput() == false ) return false;
   if(checkValid1() == false) return false;
   if(beforeSubmit() == false) return false;
   var i = 0;
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
   fm.submit(); //提交
 }
 //****************************
 //*
 //****************************
 function beforeSubmit()
 {
   //如果上次考核确认未做归属,则不能进行本次的考核计算
   var yearmonth = fm.all('AssessYear').value+fm.all('AssessMonth').value;
   var SQL = "select '1' from laassess where managecom like '"+fm.all('ManageCom').value+"%' and branchtype='2' and branchtype2='01' and state='1'  and agentgrade<>'D01' and indexcalno<'"+yearmonth+"'";
   var result = easyQueryVer3(SQL,1,0,1);
   if(result!=null&&result!='')
   {
     alert("上次考核确认未归属,不能进行本次计算!");
     return false;
   }
   return true;
 }
/***************************************************
 * 提交后操作,服务器数据返回后执行的操作
 ***************************************************
 */
 function afterSubmit( FlagStr, content )
 {
 	 showInfo.close();
 	
   if (FlagStr == "Fail" )
   {
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
     showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else
   {
     var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
     showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
     //执行下一步操作
   }
 }
 
 function checkValid1()
{
	if (trim(document.fm.AssessMonth.value)!='01')
	{
		alert("年度考核所属月只能为一月!");
		document.fm.AssessMonth.value="";
		document.fm.AssessMonth.value="";
		return false;
	}
	return true;
}