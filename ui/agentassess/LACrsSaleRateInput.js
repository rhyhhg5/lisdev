//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var tOrphanCode="";
var queryFlag= false;
window.onfocus=myonfocus;

 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,500,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}


//提交，保存按钮对应操作
function submitForm()
{
	mOperate = "INSERT";
	fm.hideOperate.value=mOperate;
	if( verifyInput() == false ) return false;
	if(!chkMulLine()) return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
function updateClick()
{
	mOperate = "UPDATE";
	fm.hideOperate.value=mOperate;
	if( verifyInput() == false ) return false;
	if(!chkMulLine()) return false;
	if(!chkPrimaryKey()) return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
		queryFlag = false;
	}
	
}
//标准查询
function easyQueryClick()
{
	if(fm.all('ManageCom').value==""||fm.all('ManageCom').value==null) 
	{
		alert("管理机构不能为空！");
		return false;
	}
	var sql ="select ManageCom,(select name from ldcom where comcode =ManageCom),assessflag," +
			"case when assessflag ='1' then '月均互动开拓提奖' else '季度规模保费' end ,rate,operator," +
			" ManageCom,assessflag " +
			" from lacrssalerate where 1=1" 
			+getWherePart("managecom","ManageCom","like")
			+getWherePart("assessflag","AssessType");
	turnPage1.queryModal(sql, CrsSaleRateGrid);
	  if (!turnPage1.strQueryResult) {
	   alert("未查询到代理手续费的信息！");
	   return false;
	  }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}
function chkMulLine()
{
	//alert("enter chkmulline");
	var i;
	var selFlag = true;
	var iCount = 0;
	var reg = new RegExp("^[0-9][\.][0-9][0-9]*?$");  
	var rowNum = CrsSaleRateGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(CrsSaleRateGrid.getChkNo(i))
		{
			iCount++;
			if((CrsSaleRateGrid.getRowColData(i,1) == null)||(CrsSaleRateGrid.getRowColData(i,1) == ""))
			{
				alert("管理机构不能为空");
				CrsSaleRateGrid.setFocus(i,1,CrsSaleRateGrid);
				selFlag = false;
				break;
			}

			if((CrsSaleRateGrid.getRowColData(i,3) == null)||(CrsSaleRateGrid.getRowColData(i,3) == ""))
			{
				alert("考核方式不能为空");
				CrsSaleRateGrid.setFocus(i,2,CrsSaleRateGrid);
				selFlag = false;
				break;
			}
			var rate = CrsSaleRateGrid.getRowColData(i,5);
			if((rate == null)||(rate == ""))
			{
				alert("比例不能为空");
				CrsSaleRateGrid.setFocus(i,5,CrsSaleRateGrid);
				selFlag = false;
				break;
			}
			if(rate >1||rate <0)
			{
				alert("比例不在0到1之间");
				selFlag = false;
				break;
			}
			if(rate!=0&&!reg.test(rate))
			{
				alert("比例格式不正确,请重新录入");
				selFlag = false;
				break;
			}
			
  			//如果Serialno为空不能删除和更新
			if((CrsSaleRateGrid.getRowColData(i,6) == null)||(CrsSaleRateGrid.getRowColData(i,6)==""))
			{
				if((fm.hideOperate.value == "DELETE") || (fm.hideOperate.value == "UPDATE"))
				{
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					CrsSaleRateGrid.checkBoxAllNot();
					CrsSaleRateGrid.setFocus(i,1,CrsSaleRateGrid);
					selFlag = false;
					break;
				}
			}
			else if ((CrsSaleRateGrid.getRowColData(i,6) !== null)||(CrsSaleRateGrid.getRowColData(i,6)!==""))
			{	//如果Serialno不为空不能插入
				if(fm.hideOperate.value == "INSERT")
				{
					alert("此纪录已存在，不可插入！");
					CrsSaleRateGrid.checkBoxAllNot();
					CrsSaleRateGrid.setFocus(i,1,CrsSaleRateGrid);
					selFlag = false;
					break;
				}
			}
		}
		else
		{//不是选中的行
			if((CrsSaleRateGrid.getRowColData(i,6) == null)||(CrsSaleRateGrid.getRowColData(i,6)==""))
				{
					alert("有未保存的新增纪录！请先保存记录。");
					CrsSaleRateGrid.checkBoxAllNot();
					CrsSaleRateGrid.setFocus(i,1,CrsSaleRateGrid);
					selFlag = false;
					break;
				}
		}
	}
	if(!selFlag) return selFlag;
    if(iCount == 0)
	{
    	var title="";
    	if(fm.hideOperate.value=="UPDATE")
    	{
    		title="修改";
    	}
    	if(fm.hideOperate.value=="DELETE")
    	{
    		title="删除";
    	}
    	if(fm.hideOperate.value=="INSERT")
    	{
    		title="新增";
    	}
		alert("请选择要"+title+"的记录!");
		return false
	}
	return true;
}
function chkPrimaryKey()
{
	var rowNum = CrsSaleRateGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(CrsSaleRateGrid.getChkNo(i))
		{
			if(CrsSaleRateGrid.getRowColData(i,1)!=CrsSaleRateGrid.getRowColData(i,7))
			{
				alert("不能修改管理机构！");
				CrsSaleRateGrid.setRowColData(i,1,CrsSaleRateGrid.getRowColData(i,7));
				var sql="select name from ldcom where comcode ='"+CrsSaleRateGrid.getRowColData(i,7)+"'";
				var array=easyExecSql(sql);
				CrsSaleRateGrid.setRowColData(i,2,array[0][0]);
				return false;
			}
			if(CrsSaleRateGrid.getRowColData(i,3)!=CrsSaleRateGrid.getRowColData(i,8))
			{
				alert("不能修改考核类型！");
				CrsSaleRateGrid.setRowColData(i,3,CrsSaleRateGrid.getRowColData(i,8));
				var assessflag ="";
				if(CrsSaleRateGrid.getRowColData(i,8)="1")
				{
					assessflag="月均互动开拓提奖";
				}
				else if(CrsSaleRateGrid.getRowColData(i,8)="1")
				{
					assessflag="季度规模保费";
				}
				CrsSaleRateGrid.setRowColData(i,4,assessflag);
				return false;
			}
		}
	}
	return true;
}