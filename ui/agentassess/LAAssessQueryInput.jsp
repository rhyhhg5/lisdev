<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAssessQueryInput.jsp
//程序功能：
//创建日期：2003-07-08 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String BranchType = "";
	String BranchType2 = "";
	try
	{
		BranchType = request.getParameter("BranchType");
		BranchType2=request.getParameter("BranchType2");

	}
	catch( Exception e )
	{
		BranchType = "";
		BranchType2="";
	}
                String tTitleAgent="";
                if("1".equals(BranchType))
                {
                  tTitleAgent = "营销员";
               }
               else if("2".equals(BranchType))
               {
                 tTitleAgent = "业务员";
               }			
	
%>
<%@page contentType="text/html;charset=GBK" %>
<script>
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAAssessQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAssessQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
	<%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>人员职级异动查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./LAAssessQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssess1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAAssess1" style= "display: ''">
    <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly >
          </TD> 
          
          <TD  class= title>
            考核年月
          </TD>
          <TD  class= input>
            <Input class=common name=IndexCalNo verify="考核年月|len=6&int" >
          </TD>          
        </TR>
      	<TR  class= common>
      	  <TD  class= title>
            <%=tTitleAgent%>职级 
          </TD>
          <TD  class= input>
            <Input class="codeno" name=AgentGrade  
            ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
            onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
            ><Input class=codename name=AgentGradeName readOnly >    
          </TD>
          <TD  class= title>
                <%=tTitleAgent%>代码
         </TD> 
         <TD  class= input>
            <Input class=common name=AgentCode >
          </TD>        
        </TR>
      	<TR  class= common>         
         <TD  class= title>
                <%=tTitleAgent%>姓名
         </TD> 
         <TD  class= input>
            <Input class=common name=AgentName  >
          </TD>        
        </TR>
      </table>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();"  class="cssButton"> 
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();"  class="cssButton"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAssessGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAssessGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"  class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"  class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"  class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"  class="cssButton"> 				
  	</div>
  	  <input name=BranchType type=hidden value = ''>
  	  <input name=BranchType2 type=hidden value = ''>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
