<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAGrpRearSave.jsp
//程序功能：
//创建日期：2009-01-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentassess.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LARearRelationSchema tLARearRelationSchema   = new LARearRelationSchema();
  LAGrpRearUI tLAGrpRearUI = new LAGrpRearUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();	

  tG=(GlobalInput)session.getValue("GI");

  tLARearRelationSchema.setAgentCode(request.getParameter("AgentCode"));
  tLARearRelationSchema.setRearAgentCode(request.getParameter("RearAgentCode"));
  tLARearRelationSchema.setAgentGroup(request.getParameter("AgentGroup"));
  tLARearRelationSchema.setstartDate(request.getParameter("StartDate"));
  tLARearRelationSchema.setRearFlag(request.getParameter("RearFlag"));
  tLARearRelationSchema.setRearLevel(request.getParameter("RearLevel"));    
  tLARearRelationSchema.setRearedGens(request.getParameter("RearedGens"));
  tLARearRelationSchema.setRearCommFlag("1");
  tLARearRelationSchema.setOperator(tG.Operator);    



  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tG);
	tVData.addElement(tLARearRelationSchema);
	
 try
 {
   tLAGrpRearUI.submitData(tVData,tOperate);
 }
 catch(Exception ex)
 {
   Content = "保存失败，原因是:" + ex.toString();
   FlagStr = "Fail";
 }
 
 if (!FlagStr.equals("Fail"))
 {
   tError = tLAGrpRearUI.mErrors;
   if (!tError.needDealError())
   {                          
   	Content = " 保存成功! ";
   	FlagStr = "Succ";
   }
   else                                                                           
   {
   	Content = " 保存失败，原因是:" + tError.getFirstError();
   	FlagStr = "Fail";
   }
 }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
