//               该文件中包含客户端需要处理的函数和事件
var strRT=""; // 存储代理人代码记录
// var strAgentGrade=""; //存储代理人职级
// var arrGrade = new Array();
var arrGrade = "";
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

// ***************************************************
// * 查询职级信息
// ***************************************************
function getAgentGradeStr()
{
  var tReturn = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;

  var sSql = "select gradecode,gradename from laagentgrade where 1=1 ";
     sSql += getWherePart("BranchType");
     sSql += getWherePart("BranchType2");
/*
 * sSql += " and gradeproperty6 = '0' "; sSql = sSql + " and gradeproperty2 in
 * (select gradeproperty2 from laagentgrade where 1=1 "
 * +getWherePart("BranchType") +getWherePart("BranchType2") +" and
 * gradeproperty6='0') ";
 */
	   sSql += "order by gradecode";

  var tqueryRe = easyQueryVer3(sSql,1,0,1);
  if (tqueryRe)
      tReturn = tqueryRe;

  return tReturn;
}

// ***************************************************
// * 查询考核信息
// ***************************************************
function easyQueryClick()
{
	// 验证必填项
	if(!checkValid())
	{
		return false;
	}
	
	var tSQL = "";
	
	tSQL  = "SELECT";
	tSQL += "   getUniteCode(a.AgentCode),";
	tSQL += "    b.Name,";
	tSQL += "    a.BranchAttr,";
	tSQL += "    c.Name,";
	tSQL += "    a.AgentGrade,";
	tSQL += "    a.agentgrade1,";
	tSQL += " (SELECT GradeName FROM LAAgentGrade WHERE GradeCode= a.agentgrade1) ";
	tSQL += " FROM";
	tSQL += "    LAAssess a,";
	tSQL += "    LAAgent b,";
	tSQL += "    LABranchGroup c,";
	tSQL += "    LAAgentGrade d";
	tSQL += " WHERE";
	tSQL += "    a.AgentCode=b.AgentCode AND";
	tSQL += "    a.AgentGroup=c.AgentGroup AND";
	tSQL += "    a.CalAgentGrade=d.GradeCode AND";
	tSQL += "    a.AgentGrade = 'A01'  ";
// tSQL += " and a.turnflag in ('00','05') "; //去掉了延迟转正期
	tSQL += " and not exists ";
	tSQL += "(select 'X' from laassess where agentcode = a.agentcode and indexcalno='"+fm.IndexCalNo.value+"' and state in ('0','2'))";
	tSQL += getWherePart("a.indexcalno","IndexCalNo");
	tSQL += getWherePart("getGroupAgentcode(a.agentcode)","GroupAgentCode");
	tSQL += getWherePart("a.ManageCom","ManageCom","like");
	tSQL += getWherePart("a.BranchAttr","BranchAttr");
	tSQL += getWherePart("b.Name","AgentName");
	tSQL += " ORDER BY a.BranchAttr,a.AgentGrade";	
	// 执行查询并返回结果
	turnPage.queryModal(tSQL, GradeDecideGrid);
}

// ***************************************************
// * 验证必填项
// ***************************************************
function checkValid()
{
	if (trim(document.fm.ManageCom.value)=="")
	{
		alert("请填写管理机构!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		return false;
	}
	
	if (document.fm.ManageCom.value.length != 8)
	{
		alert("管理机构输入错误!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		fm.all('ManageCom').focus();
		return false;
	}
	
	if(trim(document.fm.IndexCalNo.value)=="")
	{
		alert("请填写考核年月!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	if (document.fm.IndexCalNo.value.length != 6)
	{
		alert("考核年月输入错误!");
		document.fm.IndexCalNo.value="";
		fm.all('IndexCalNo').focus();
		return false;
	}
	
	return true;
}

// ***************************************************
// * 点击‘保存’进行的操作
// ***************************************************
function submitForm()
{
	// 提交画面前的验证
	if (checkValid() == false)
    return false;
    
  if( verifyInput2() == false ) return false;
    
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.submit(); // 提交
  
  return true;
}

// ***************************************************
// * 提交操作完成后的处理
// ***************************************************
function afterSubmit(FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    // 执行下一步操作
    // GradeDecideGrid.clearData("GradeDecideGrid");
  }
}