//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
//</addcode>############################################################//
window.onfocus=myonfocus;
var nowDate = new Date().getFullYear()+"-"+(new Date().getMonth()+1)+"-"+new Date().getDay();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	mOperate = "INSERT";
	 fm.all('Month').value=fm.all('Month1').value;
	if(!verifyInput()) return false;
	if(checkWage()) return false;
	if(LAAgentCompreScoreGrid.getRowColData(1,6)!=null&&LAAgentCompreScoreGrid.getRowColData(1,6)!='')
	{
		alert("此条记录已经存在，请修改！");
		return false;
	}
	if (!beforeSubmit())
    return false;
	  //alert(mOperate);
    //if (mOperate == "QUERY||MAIN"){
   	//initForm();
   	//alert("请重新输入信息！");
   	//return false;
  //}
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	 mOperate="";
  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
		//fm.all('ModifyDate').value=fm.all('DoneDate').value;
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    //showDiv(operateButton,"true"); 
    initForm();
  }
  catch(re)
  {
    alert("在LARewardPunish.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码  
	if((LAAgentCompreScoreGrid.getRowColData(1,6)==null)||(LAAgentCompreScoreGrid.getRowColData(1,6)==''))
	{
		alert('请先查询出要修改的纪录！');
		return false;
	}
	if (!beforeSubmit())
	    return false;
  else if (confirm("您确实想修改该记录吗?"))
  {
	  var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
    mOperate="UPDATE";
    fm.hideOperate.value=mOperate;
    fm.submit(); //提交
    initForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LAActiveManageQueryInput.html?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
  
}   

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码  
  if((LAAgentCompreScoreGrid.getRowColData(1,6)==null)||(LAAgentCompreScoreGrid.getRowColData(1,6)==''))
    alert('请先查询出要删除的业务员记录！');
  else if (confirm("您确实想删除该记录吗?"))
  {
	  var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
    mOperate="DELETE";  
    fm.hideOperate.value=mOperate;
    fm.submit(); //提交
    initForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function checkvalid()
{
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
   var tstrSql = "select agentcode from laagent  where groupagentcode='" + fm.GroupAgentcode.value + "' ";
   var arrResult = easyExecSql(tstrSql);
   if(arrResult){
	   fm.AgentCode.value=arrResult[0][0];
   }
  
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select * from LAAgent where 1=1   "
	    + getWherePart('AgentCode','AgentCode')
	  + getWherePart('BranchType','BranchType')
	   + getWherePart('BranchType2','BranchType2');
	  
	     
    var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
     //alert(strQueryResult);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此代理人！");
    fm.all('AgentCode').value="";
    fm.all('AgentGroup').value = "";
    fm.all('ManageCom').value  = "";
    fm.all('Name').value  = "";
    return;
  }
 
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  if(tArr[0][61]>'02')
  {
	  alert("此代理人已经离职！");
	  return;
  }
  //<rem>######//
  fm.all('Name').value = tArr[0][5];
  //</rem>######//
  
  fm.all('ManageCom').value  = tArr[0][2];
  
  old_AgentGroup=tArr[0][1];
  
  fm.all('HiddenAgentGroup').value=tArr[0][1];
  
  strSQL_AgentGroup = "select BranchAttr from labranchgroup where 1=1 "
                      +"and AgentGroup='"+old_AgentGroup+"' and (state<>'1' or state is null)"
     var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
  //var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  var tArr_AgentGroup = new Array();
  tArr_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
  //以备显示时使用
  fm.all('AgentGroup').value = tArr_AgentGroup[0][0];
  new_AgentGroup=tArr_AgentGroup[0][0];
  
}

function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
  if( arrQueryResult != null )
  {
	  initForm();
  //	alert(1);
    arrResult = arrQueryResult;
    fm.all('GroupAgentcode').value = arrResult[0][0];
    fm.all('Name').value = arrResult[0][1];  
    fm.all('ManageCom').value = arrResult[0][2];
    fm.all('ManageComName').value = arrResult[0][3]; 
    fm.all('AgentGroup').value = arrResult[0][4];  
    fm.all('AgentGroupName').value = arrResult[0][5];                                              
    fm.all('AgentCode').value = arrResult[0][6];  
    var sql ="select projectCode,ProjectName,PropertyScore,LifeScore,SumScore,operator,makedate,wageno from LAAgentCompreScore where branchtype='5' and branchtype2='01' " +
    		" and agentcode ='"+fm.all('AgentCode').value+"'  order by projectcode  with ur" ;
    var returnResult = easyQueryVer3(sql, 1, 1, 1);
    if(!returnResult)
    {
    	fm.all('Operator').value=fm.all('hiddenOperator').value;
		fm.all('MakeDate').value=nowDate;
		initLAAgentCompreScoreGrid();
		initLAAgentCompreScoreGridValue();
		var inputMonth=document.getElementById("Month1");
	    inputMonth.disabled=false;
	    fm.all('Month1').value="";
	    fm.all('MonthName').value="";
    }
    else
    {
    	turnPage.strQueryResult  = easyQueryVer3(sql);
    	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    	    //查询成功则拆分字符串，返回二维数组
	    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	    turnPage.pageDisplayGrid = LAAgentCompreScoreGrid;
	    //保存SQL语句
	    turnPage.strQuerySql = sql;
	    //设置查询起始位置
	    turnPage.pageIndex = 0;
	    //在查询结果数组中取出符合页面显示大小设置的数组
	    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 11);
	    //调用MULTILINE对象显示查询结果
	    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	    fm.all('Score').value=LAAgentCompreScoreGrid.getRowColData(10,5);
	    fm.all('Operator').value=LAAgentCompreScoreGrid.getRowColData(10,6);
	    fm.all('MakeDate').value=LAAgentCompreScoreGrid.getRowColData(10,7);
	    var wageno = LAAgentCompreScoreGrid.getRowColData(10,8);
	    var month = wageno.substr(4);
	    fm.all('Month1').value=month;
	    fm.all('Month').value=month;
	    if(month ='06')
	    {
	    	fm.all('MonthName').value="上半年";
	    }
	    else  if(month ='12')
	    {
	    	fm.all('MonthName').value="下半年";
	    }
	    var inputMonth=document.getElementById("Month1");
	    inputMonth.disabled=true;
    }
  }
  
}

function initLAAgentCompreScoreGridValue()
{
	LAAgentCompreScoreGrid.setRowColData(0,1,"AC0001");
	LAAgentCompreScoreGrid.setRowColData(0,2,"辅导能力");

	LAAgentCompreScoreGrid.setRowColData(1,1,"AC0002");
	LAAgentCompreScoreGrid.setRowColData(1,2,"培训内容");

	LAAgentCompreScoreGrid.setRowColData(2,1,"AC0003");
	LAAgentCompreScoreGrid.setRowColData(2,2,"培训形式");

	LAAgentCompreScoreGrid.setRowColData(3,1,"AC0004");
	LAAgentCompreScoreGrid.setRowColData(3,2,"培训效果");

	LAAgentCompreScoreGrid.setRowColData(4,1,"AC0005");
	LAAgentCompreScoreGrid.setRowColData(4,2,"客户拓展能力");

	LAAgentCompreScoreGrid.setRowColData(5,1,"AC0006");
	LAAgentCompreScoreGrid.setRowColData(5,2,"与人保财险的沟通协调能力");

	LAAgentCompreScoreGrid.setRowColData(6,1,"AC0007");
	LAAgentCompreScoreGrid.setRowColData(6,2,"团队激励能力");

	LAAgentCompreScoreGrid.setRowColData(7,1,"AC0008");
	LAAgentCompreScoreGrid.setRowColData(7,2,"展业工具管理及服务");

	LAAgentCompreScoreGrid.setRowColData(8,1,"AC0009");
	LAAgentCompreScoreGrid.setRowColData(8,2,"保全等客户服务衔接");

	LAAgentCompreScoreGrid.setRowColData(9,1,"AC0010");
	LAAgentCompreScoreGrid.setRowColData(9,2,"投保出单等业务服务衔接");

	LAAgentCompreScoreGrid.setRowColData(10,1,"AC0011");
	LAAgentCompreScoreGrid.setRowColData(10,2,"合计");
}
function beforeSubmit()
{
	var rowNum = LAAgentCompreScoreGrid.mulLineCount;
	var propertyscoreSum=0;
	var lifescoreSum=0;
	var r = /^\+?[0-9][0-9]*$/;　　//整数 
	for(var i=0;i<rowNum-1;i++)
	{
		
		if(LAAgentCompreScoreGrid.getRowColData(i,3) == null||LAAgentCompreScoreGrid.getRowColData(i,3) == "")
		{
			LAAgentCompreScoreGrid.setRowColData(i,3,"0");
		}
		if(LAAgentCompreScoreGrid.getRowColData(i,4) == null||LAAgentCompreScoreGrid.getRowColData(i,4) == "")
		{
			LAAgentCompreScoreGrid.setRowColData(i,4,"0");
		}
		var num1=LAAgentCompreScoreGrid.getRowColData(i,3);
		if(num1<0)
		{
			alert(LAAgentCompreScoreGrid.getRowColData(i,2)+"财险分数不能为负数！");
			return false;
		}
		if(!r.test(num1))
		{
			alert(LAAgentCompreScoreGrid.getRowColData(i,2)+"财险分数只能录入整数！");
			return false;
		}
		var num2=LAAgentCompreScoreGrid.getRowColData(i,4);
		if(num2<0)
		{
			alert(LAAgentCompreScoreGrid.getRowColData(i,2)+"寿险分数不能为负数！");
			return false;
		}
		if(!r.test(num1))
		{
			alert(LAAgentCompreScoreGrid.getRowColData(i,2)+"寿险分数只能录入整数！");
			return false;
		}
		var num=parseInt(num1)+parseInt(num2);
		propertyscoreSum+=parseInt(num1);
		lifescoreSum+=parseInt(num2);
		LAAgentCompreScoreGrid.setRowColData(i,5,num+"");
	}
	LAAgentCompreScoreGrid.setRowColData(10,3,parseFloat(propertyscoreSum)/10+"");
	LAAgentCompreScoreGrid.setRowColData(10,4,parseFloat(lifescoreSum)/10+"");
	LAAgentCompreScoreGrid.setRowColData(10,5,(propertyscoreSum+lifescoreSum)+"");
	fm.all("Score").value=propertyscoreSum+lifescoreSum;
	return true;
}
function queryStandClick()
{
	document.getElementById("divLAActiveManageStand").style.display="";
}
function checkWage()
{
	var wageno = fm.all("Year").value+fm.all("Month").value;
	var sql ="select 1 from lawage where branchtype ='5' and branchtype2='01'  " +
	"  and agentcode ='"+fm.all("AgentCode").value+"'" +
	"  and indexcalno ='"+wageno+"' with ur";
	turnPage1.strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
	if (turnPage1.strQueryResult) {
		var message="";
		if(month ='06')
	    {
			message="上半年";
	    }
	    else  if(month ='12')
	    {
	    	message="下半年";
	    }
		alert("业务员"+fm.all("GroupAgentCode").value+"在"+fm.all("Year").value+"的"+message+"薪资已经确认");
		return true;
	}
	return false;
}
