//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 

//提交，保存按钮对应操作
function DoSave()
{
	//alert('dosave');
  if(fm.ManageCom1.value=="")
  {
    alert("请录入地区编码！");
    return ;
  }	
  var tAreaType =fm.AreaType2.value; 
  if ( tAreaType == null || tAreaType == "" ) 
  {
  	alert("请选择要删除的版本类型");
  	return;
  }  
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmAction.value = 'save';
  //alert('save');
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

function DoDel()
{
  if(fm.ManageCom.value=="")
  {
    alert("请录入要删除的地区编码！");
    return ;
  }	
  if(fm.WageCode.value=="")
  {
    alert("请录入要删除的资金类型编码！");
    return ;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
  }
  var tAreaType =fm.AreaType2.value; 
  if ( tAreaType == null || tAreaType == "" ) 
  {
  	alert("请选择要删除的版本类型");
  	return;
  }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
    
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmAction.value = 'del';
  fm.submit(); //提交
}

function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

// 查询按钮
function easyQueryClick()
{		
  // 初始化表格
  initSetGrid();
  fm.ManageCom1.value = "";
  var tManageCom  = fm.ManageCom.value;
  var tAreaType =fm.AreaType1.value;
  
  if ( tManageCom == null || tManageCom == "" ) 
  {
  	alert("请选择管理机构编码！");
  	return;
  }
  if ( tAreaType == null || tAreaType == "" ) 
  {
  	alert("请选择版本类型");
  	return;
  }	
  fm.AreaType2.value=tAreaType;
  		
  // 书写SQL语句
  var strSQL = "";  
  strSQL = "select agentgrade,wagecode,payperiodtype,paymonth,paymode,monthlimit,payinterval,baserate,baserate1,standmoney,standrate from lalinkwage where 1=1"
         + getWherePart( 'ManageCom','ManageCom' )			 
		 + getWherePart( 'AgentGrade','AgentGrade' )		   
		 + getWherePart( 'WageCode','WageCode' )
		 + getWherePart('AreaType','AreaType1');
		 
 
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据，请重新录入查询条件！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组  
  turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = SetGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    initForm();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("初始化页面错误，重置出错");
  }
}




//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
      parent.fraMain.rows = "0,0,0,0,*";
  }
   else
   {
      parent.fraMain.rows = "0,0,0,0,*";
   }
}


//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
{
  if (confirm("您确实想修改该记录吗?"))
   {
    fm.fmAction.value = "update";
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   
    fm.submit(); //提交
  }
  else
  {
    fm.OperateType.value = "";
    alert("您取消了修改操作！");
  }
}


function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
