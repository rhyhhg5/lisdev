<%
//程序名称：LaratecommisionOnmiSetInit.jsp
//程序功能：
//创建日期：2009-1-16
//创建人  ：   miaoxz
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  tmanagecom=" 1 and char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000# ";
 var StrSql=" 1 and riskcode not in ( #330801#,#331801#,#332701#,#333901#) ";
 var tagentcom = "1"
 //var StrSql=" 1 and risktype4=#4# and riskcode in (select code from ldcode where codetype=#bankriskcode#)";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  //alert("fdsa"); 
    fm.all('RiskCode').value='';
    fm.all('RiskCodename').value=''; 
    fm.all('CurYear').value=''; 
   // fm.all('PayintvCode').value='';
    //fm.all('PayintvName').value='';
   // fm.all('StartDay').value='';
   // fm.all('EndDay').value='';
    fm.all('Rate').value=''; 
    //fm.all('F01Code').value='';
   //   fm.all('F01Name').value='';
    // fm.all('diskimporttype').value='LARateCommision';    
    fm.all('BranchType').value=getBranchType();
    //alert(getBranchType());
   // fm.all('branchtype2').value="0"+<%= BranchType2%>;
    // alert(<%= BranchType2%>);
    //alert(fm.all('branchtype2').value); 
    //alert("fdsa");                    
  }
  catch(ex)
  {
    alert("在LaratecommisionOnmiSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initTraditionalChargeGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

      iArray[1]=new Array();
	  iArray[1][0]="险种编码";          		//列名
	  iArray[1][1]="80px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  iArray[1][4]="RiskName";              	        //是否引用代码:null||""为不引用
	  iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	  iArray[1][6]="0|1";
	  iArray[1][9]="险种编码|code:RiskName&notnull";  
	  iArray[1][15]= "1";              	        //校验输入是否正确
	  iArray[1][16]= StrSql;  

   	  iArray[2]=new Array();
      iArray[2][0]="险种名称"; //列名
      iArray[2][1]="120px";        //列宽
      iArray[2][2]=100;            //列最大值
      iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
   
    
    iArray[3]=new Array();
    iArray[3][0]="管理机构"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[3][4]="ComCode";
    iArray[3][5]="3|4";
    iArray[3][6]="0|1";
    iArray[3][9]="管理机构|NotNull&NUM";    
    iArray[3][15]="1";
    iArray[3][16]=tmanagecom;
                                      
    iArray[4]=new Array();
    iArray[4][0]="管理机构名称"; //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许    

    
    
    iArray[5]=new Array();
    iArray[5][0]="中介机构"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[5][4]="AgentCom";
    iArray[5][5]="5|6";
    iArray[5][6]="0|1";
    iArray[5][9]="中介机构|NotNull&NUM";    
    iArray[5][15]="1";
    iArray[5][16]=tagentcom;

    iArray[6]=new Array();
    iArray[6][0]="中介机构名称"; //列名
    iArray[6][1]="100px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=3;              //是否允许输入,1表示允许,0表示不允许

    
    iArray[7]=new Array();
    iArray[7][0]="保险期间"; //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=2;              //是否允许输入,1表示允许,0表示不允许 
    iArray[7][4]="payyear"; 
    iArray[7][5]="7";  
    iArray[7][6]="0|1"; 

    
    iArray[8]=new Array();
    iArray[8][0]="缴费方式"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[8][9]="缴费方式|NotNull";
    iArray[8][10]="PayintvCode";
    iArray[8][11]="0|^12|年缴|^6|半年缴|^3|季缴|^1|月缴|^0|趸缴";  

    iArray[9]=new Array();
    iArray[9][0]="缴费年期"; //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;  
    
          
    iArray[10]=new Array();
    iArray[10][0]="保单年度"; //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许
    iArray[10][9]="保单年度|NotNull&NUM";    
                                       
    
    iArray[11]=new Array();
    iArray[11][0]="佣金比率"; //列名
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[11][9]="佣金比率|NotNull&NUM";
            
           //是否允许输入,1表示允许,0表示不允许
    
    
         
    TraditionalChargeGrid = new MulLineEnter( "fm" , "TraditionalChargeGrid");
    TraditionalChargeGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    TraditionalChargeGrid.displayTitle = 1;
    //SetGrid.hiddenSubtraction =0;
    //SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    TraditionalChargeGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionOnmiSetInit.jsp-->initTraditionalChargeGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initTraditionalChargeGrid();
    
  }
  catch(re)
  {
    alert("在LaratecommisionOnmiSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>