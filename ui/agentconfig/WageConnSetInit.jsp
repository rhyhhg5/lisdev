<%
//程序名称：AscriptModifyInit.jsp
//程序功能：
//创建日期：2003-09-25 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').vlaue='';  
    fm.all('WageCode').vlaue='';  
    fm.all('AgentGrade').vlaue='';  
      
            
  }
  catch(ex)
  {
    alert("在WageConnSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}





function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       
      
    iArray[1]=new Array();
    iArray[1][0]="代理人职级"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[1][10]="AgentGradeList";
    iArray[1][11]="0|^A04|业务经理一级|^A05|业务经理二级|^A06|高级经理一级|^A07|高级经理二级|^A08|督导长|^A09|区域督导长"
      
      
    iArray[2]=new Array();
    iArray[2][0]="资金类型"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[2][10]="WageCodeList";
    iArray[2][11]="0|^W00058|支助薪资|^W00059|同业招募基金|^W00060|创业奖金"          
    
    
    iArray[3]=new Array();
    iArray[3][0]="发放期间类型"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[3][10]="PayPeriodTypeList"
    iArray[3][11]="0|^01|筹备期|^02|考核期"
    
      
    iArray[4]=new Array();
    iArray[4][0]="发放月度"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[4][10]="AssPayMonth"
    iArray[4][11]="0|^01|第1个月|^02|第2个月|^03|第3个月|^04|第4个月|^05|第5个月|^06|第6个月|^07|第7个月|^08|第8个月|^09|第9个月|^10|第10个月|^11|第11个月|^12|第12个月|"    
                
       
    iArray[5]=new Array();
    iArray[5][0]="发放方式"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[5][10]="PayModList"
    iArray[5][11]="0|^0|逐月发放|^1|分批发放"
           
       
    iArray[6]=new Array();
    iArray[6][0]="发放时间限制月数"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
       
    iArray[7]=new Array();
    iArray[7][0]="发放间隔"; //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
   
    iArray[8]=new Array();
    iArray[8][0]="发放标准"; //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[9]=new Array();
    iArray[9][0]="达标比例"; //列名
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    
    
    iArray[10]=new Array();
    iArray[10][0]="发放金额"; //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[11]=new Array();
    iArray[11][0]="发放比例"; //列名
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许    

        
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
	SetGrid.mulLineCount = 1;
    SetGrid.displayTitle = 1;
    SetGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在WageConnSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();    
    initSetGrid();
    
  }
  catch(re)
  {
    alert("在WageConnSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>