<%
//程序名称：LABComRateCommSetInit.jsp
//程序功能：
//创建时间：2008-01-24
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  mSQL=" 1 and  char(length(trim(comcode))) = #8# and sign=#1#" ;
 var StrSql=" 1 and risktype4=#4# and riskcode in (select code from ldcode where codetype=#bankriskcode#) and riskcode not in (#331701#)";
</SCRIPT> 
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('RiskCode').value='';
    fm.all('RiskCodename').value=''; 
    fm.all('CurYear').value=''; 
    fm.all('payyears').value='';
    fm.all('payyearsName').value ='';	
    fm.all('AgentCom').value='';
    fm.all('Name').value='';
    fm.all('Rate').value='';    
    fm.all('BranchType').value=getBranchType();
    fm.all('F01Code').value='';      
    fm.all('F01Name').value='';    
    fm.all('F06Code').value='';  
    fm.all('F06Name').value='';               
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
        
    iArray[1]=new Array();
	iArray[1][0]="险种编码";          		//列名
	iArray[1][1]="80px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
	iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[1][4]="RiskName";              	        //是否引用代码:null||""为不引用
	iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	iArray[1][6]="0|1";
    iArray[1][9]="险种编码|code:RiskName&notnull";
    iArray[1][15]= "1";              	        //校验输入是否正确
	iArray[1][16]= StrSql;
    
    iArray[2]=new Array();
    iArray[2][0]="险种代码"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
                                          
    iArray[3]=new Array();
    iArray[3][0]="序号"; //列名
    iArray[3][1]="0px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    iArray[3][9]="序号";           
    //iArray[5][10]="level";
    //iArray[5][11]="0|^02|支行|^03|分理处|^04|网点";
    
    
       iArray[4]=new Array();
    iArray[4][0]="管理机构"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[4][4]="ComCode";
    iArray[4][5]="4|5";
    iArray[4][6]="0|1";
    iArray[4][9]="管理机构|NotNull&NUM&len=8";  
    iArray[4][15]="1";
    iArray[4][16]=mSQL; 
    
    iArray[5]=new Array();
    iArray[5][0]="管理机构名称"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许     
       
    iArray[6]=new Array();
    iArray[6][0]="代理机构"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[6][4]="agentcom1";
    iArray[6][5]="6|7";
    iArray[6][6]="0|1"
    iArray[6][9]="网点代码|NotNull";
    iArray[6][15]="managecom";
    //iArray[6][16]=tSQL; 
    iArray[6][17]= "4"; 
    
       
    iArray[7]=new Array();
    iArray[7][0]="代理机构名称"; //列名
    iArray[7][1]="120px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="保费类型"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[8][9]="保费类型|NotNull";
    iArray[8][10]="F01Code";
    iArray[8][11]="0|^00|基本保费|^03|追加保费|";
        
    iArray[9]=new Array();
    iArray[9][0]="保险期间"; //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=2;              //是否允许输入,1表示允许,0表示不允许 
    iArray[9][4]="payyear"; 
    iArray[9][5]="9|10";  
    iArray[9][6]="0|1";  
    
    iArray[10]=new Array();
    iArray[10][0]="保险期间"; //列名
    iArray[10][1]="0px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许    
          
    iArray[11]=new Array();
    iArray[11][0]="保单年度"; //列名
    iArray[11][1]="60px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[11][9]="保单年度|NotNull&NUM";
    
    iArray[12]=new Array();
    iArray[12][0]="手续费比率"; //列名
    iArray[12][1]="80px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	  iArray[12][9]="佣金比率|NotNull&NUM";

	iArray[13]=new Array();
	iArray[13][0]="出单方式"; //列名
	iArray[13][1]="80px";        //列宽
	iArray[13][2]=100;            //列最大值
	iArray[13][3]=2;              //是否允许输入,1表示允许,0表示不允许    
	iArray[13][9]="出单方式|NotNull";
	iArray[13][10]="F06Code";
	iArray[13][11]="0|^0|不区分|^1|柜台和银保通出单|^2|自助和网银出单";
		  
      
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
    //SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
    //SetGrid.hiddenSubtraction =0;
    //SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();    
    initSetGrid(); 
  }
  catch(re)
  {
    alert("在LaratecommisionSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>