<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LARateStandPremSetSave.jsp
//程序功能：
//创建日期：2009-07-24
//创建人  ：   miaoxz
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
	String tfmAction=request.getParameter("fmAction");

  LAGrpWPRSetUI  tLAGrpWPRSetUI = new LAGrpWPRSetUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LDCodeSet tSetU = new LDCodeSet();	//用于更新
            
      String tChk[] = request.getParameterValues("InpSetGridChk"); 
      //String tAreaType[] = request.getParameterValues("SetGrid1");
      //String tBranchType[] = request.getParameterValues("SetGrid2");
      //String tAgentGrade[] = request.getParameterValues("SetGrid3");
      String tRiskCode[] = request.getParameterValues("SetGrid1");
      String tRiskName[] = request.getParameterValues("SetGrid2");
      //String tYear[] = request.getParameterValues("SetGrid3");
      //String tCurYear[] = request.getParameterValues("SetGrid4");
      String tCode1[] = request.getParameterValues("SetGrid5");
      String tCode2[] = request.getParameterValues("SetGrid9");
      String tWageSort[] = request.getParameterValues("SetGrid3");
      String tStandPremSort[] = request.getParameterValues("SetGrid4");
      //String tEnddate[] = request.getParameterValues("SetGrid8");
      //String tSerialno[] = request.getParameterValues("SetGrid9");
      String tManageCom[] = request.getParameterValues("SetGrid6");
      //String tCalType[] = request.getParameterValues("SetGrid11");
	System.out.println("~~~~~~~~~~~~~~~~~~~~");
      //创建数据集
      for(int i=0;i<tChk.length;i++)
      {
      	if(tChk[i].equals("1"))
      	{
      		//创建一个新的Schema
      		LDCodeSchema tSch1 = new LDCodeSchema();
      		LDCodeSchema tSch2 = new LDCodeSchema();
      		
      		tSch1.setCodeType("GSPRISKCODE");
          	tSch1.setCode(tCode1[i].trim());
          	tSch1.setCodeName(tRiskCode[i].trim());
          	tSch1.setComCode(tManageCom[i].trim());
          	tSch1.setOtherSign(tStandPremSort[i].trim());
          	
          	tSch2.setCodeType("GWRISKCODE");
          	tSch2.setCode(tCode2[i].trim());
          	tSch2.setCodeName(tRiskCode[i].trim());
          	tSch2.setComCode(tManageCom[i].trim());
          	tSch2.setOtherSign(tWageSort[i].trim());
          	
          	tSetU.add(tSch1);
          	tSetU.add(tSch2);
      	}      	
      }
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      
      tVData.add(tGI);
      //没有更新或删除或插入的数据
      if (tSetU.size() == 0)
      {
        	FlagStr = "Fail";
        	Content = FlagStr + "未选中要处理的数据！";      	
      }else {
        //
        tVData.add(tSetU);
        //tVData.add(tAction);
        System.out.println("Start LAGrpWPRSetUI Submit...UPDATE");
        tLAGrpWPRSetUI.submitData(tVData,tAction);        	    	
      }
    	

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError =  tLAGrpWPRSetUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
