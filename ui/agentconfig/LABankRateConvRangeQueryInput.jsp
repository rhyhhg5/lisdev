<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABankRateConvRangeQueryInput.jsp
//程序功能：
//创建日期：2008-03-3 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LABankRateConvRangeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LABankRateConvRangeQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>奖惩查询</title>
</head>
<body  onload="initForm();" >
  <form action="./LABankRateConvRangeQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABankRateConvRange1);">
    
    <td class=titleImg>
      提奖折算比例信息
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLABankRateConvRange1" style= "display: ''"> 
    <table  class= common>
      <TR  class= common>
   <TD  class= title>
            管理机构
   </TD>
   <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
    </TD>
    <TD  class= title>
       月度折算比例
    </TD>
    <TD  class= input>
       <Input class= common name=MonthRate verify="月度折算比例|code:comcode&NOTNULL&NUM">
    </TD>
  </TR>
  
  <TR  class= common>
   <TD  class= title>
       季度折算比例
    </TD>
    <TD  class= input>
       <Input class= common name=SeasonRate verify="季度折算比例|code:comcode&NOTNULL&NUM">
    </TD>
    <TD  class= title>
       年度折算比例
    </TD>
    <TD  class= input>
       <Input class= common name=YearRate verify="年度折算比例|code:comcode&NOTNULL&NUM">
    </TD>
  </TR>
      
    </table>
          <input type=hidden name=add value='1'>
          <input type=hidden name=BranchType value=''>
           <input type=hidden name=BranchType2 value=''>
          <INPUT VALUE="查  询" class=cssbutton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返  回" class=cssbutton TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divActiRateManageGrid);">
    		</td>
    		<td class= titleImg>
    			 品质管理查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLABankRateConvRangeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="SpanLABankRateConvRangeGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 					
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
