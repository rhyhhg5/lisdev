//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var tOrphanCode="";
var queryFlag= false;
window.onfocus=myonfocus;

 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,500,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}


//提交，保存按钮对应操作
function submitForm()
{
	mOperate = "INSERT";
	fm.hideOperate.value=mOperate;
	if( verifyInput() == false ) return false;
	if(!chkMulLine()) return false;
//	chkMulLineData();
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
function updateClick()
{
	mOperate = "UPDATE";
	fm.hideOperate.value=mOperate;
	if( verifyInput() == false ) return false;
	if(!chkMulLine()) return false;
//	chkMulLineData();
	if(!chkPrimaryKey()) return false;
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
		queryFlag = false;
	}
	
}
//标准查询
function easyQueryClick()
{
	if(fm.all('ManageCom').value==""||fm.all('ManageCom').value==null) 
	{
		alert("管理机构不能为空！");
		return false;
	}
	var sql ="select ManageCom,getUniteCode(AgentCode),WageNo,PropertyPrem,LifePrem,operator" +
			", ManageCom,getUniteCode(AgentCode),WageNo " +
			" from laactivecharge where 1=1 and branchtype ='2' and branchtype2 = '02' " 
			+getWherePart("managecom","ManageCom","like")
			+getWherePart("getUniteCode(AgentCode)","GroupAgentCode")
			+getWherePart("WageNo","WageNo");
	turnPage1.queryModal(sql, ActiveChargeGrid);
	  if (!turnPage1.strQueryResult) {
	   alert("未查询到交叉销售保费相关信息！");
	   return false;
	  }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}
function chkMulLine()
{
	//alert("enter chkmulline");
	if(!ActiveChargeGrid.checkValue("ActiveChargeGrid")) return false;
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = ActiveChargeGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(ActiveChargeGrid.getChkNo(i))
		{
			iCount++;
			if((ActiveChargeGrid.getRowColData(i,1) == null)||(ActiveChargeGrid.getRowColData(i,1) == ""))
			{
				alert("管理机构不能为空");
				ActiveChargeGrid.setFocus(i,1,ActiveChargeGrid);
				selFlag = false;
				break;
			}

			if((ActiveChargeGrid.getRowColData(i,2) == null)||(ActiveChargeGrid.getRowColData(i,2) == ""))
			{
				alert("业务员不能为空");
				ActiveChargeGrid.setFocus(i,2,ActiveChargeGrid);
				selFlag = false;
				break;
			}
			var wageno = ActiveChargeGrid.getRowColData(i,3);
			var managecom = ActiveChargeGrid.getRowColData(i,1);
			if((wageno == null)||(wageno == ""))
			{
				alert("月份不能为空");
				ActiveChargeGrid.setFocus(i,3,ActiveChargeGrid);
				selFlag = false;
				break;
			}
			if(wageno.length!=6)
			{
				alert("输入的月份非法");
				ActiveChargeGrid.setFocus(i,3,ActiveChargeGrid);
				selFlag = false;
				break;
			}
//			if(parseInt(wageno.substr(0,4))<=2014)
//			{
//				if (!confirm('输入的所属年月中年份为2015年之前，请确认您的操作'))
//				{
//					return false;
//				}
////				ActiveChargeGrid.setFocus(i,3,ActiveChargeGrid);
////				selFlag = false;
////				break;
//			}
			if(parseInt(wageno.substr(4,6),10)<1 ||parseInt(wageno.substr(4,6),10)>12 )
			{
				alert("月份中的月份为非法月份");
				ActiveChargeGrid.setFocus(i,3,ActiveChargeGrid);
				selFlag = false;
				break;
			}

			if((ActiveChargeGrid.getRowColData(i,4) == null)||(ActiveChargeGrid.getRowColData(i,4) == ""))
			{
				alert("财险交叉销售保费不能为空");
				ActiveChargeGrid.setFocus(i,4,ActiveChargeGrid);
				selFlag = false;
				break;
			}
			if((ActiveChargeGrid.getRowColData(i,5) == null)||(ActiveChargeGrid.getRowColData(i,5) == ""))
			{
				alert("寿险交叉销售保费不能为空");
				ActiveChargeGrid.setFocus(i,5,ActiveChargeGrid);
				selFlag = false;
				break;
			}
			if((ActiveChargeGrid.getRowColData(i,4) == null)||(ActiveChargeGrid.getRowColData(i,4) == ""))
			{
				ActiveChargeGrid.setRowColData(i,4,"0");
			}
			if((ActiveChargeGrid.getRowColData(i,5) == null)||(ActiveChargeGrid.getRowColData(i,5) == ""))
			{
				ActiveChargeGrid.setRowColData(i,5,"0");
			}
			if((ActiveChargeGrid.getRowColData(i,4) <0)||(ActiveChargeGrid.getRowColData(i,4) <0))
			{
				alert("财险交叉销售保费不能小于0");
				ActiveChargeGrid.setFocus(i,4,ActiveChargeGrid);
				selFlag = false;
				break;
			}
			if((ActiveChargeGrid.getRowColData(i,5) < 0)||(ActiveChargeGrid.getRowColData(i,5) < 0))
			{
				alert("寿险交叉销售保费不能小于0");
				ActiveChargeGrid.setFocus(i,5,ActiveChargeGrid);
				selFlag = false;
				break;
			}
			//校验考核信息
			if(parseInt(wageno.substr(4,6),10)>=1 &&parseInt(wageno.substr(4,6),10)<=3 )
			{
				var twageno = wageno.substr(0,4)+'03';
			}
			if(parseInt(wageno.substr(4,6),10)>=4 &&parseInt(wageno.substr(4,6),10)<=6 )
			{
				var twageno = wageno.substr(0,4)+'06';
			}
			if(parseInt(wageno.substr(4,6),10)>=7 &&parseInt(wageno.substr(4,6),10)<=9 )
			{
				var twageno = wageno.substr(0,4)+'09';
			}
			if(parseInt(wageno.substr(4,6),10)>=10 &&parseInt(wageno.substr(4,6),10)<=12 )
			{
				var twageno = wageno.substr(0,4)+'12';
			}
			   var strSql = "select agentcode from laassess where IndexCalNo = '"+twageno+"' "
			              +"and state>='1' and branchtype = '2' and branchtype2 = '02' and managecom = '"
			              +managecom +"'";
//			              +getWherePart('AgentSeries')
//			              +getWherePart('ManageCom')
//			              +getWherePart('BranchType')
//			              +getWherePart('BranchType2');
			              

			    
			  //查询SQL，返回结果字符串
			  var strResult  = easyQueryVer3(strSql, 1, 1, 1);  

			  //判断是否查询成功

			  if (strResult) { 
			  	 
			    alert("该机构已审核确认或组织归属，不能进行录入！");
			    return false;
			  }	
  			//如果Serialno为空不能删除和更新
			if((ActiveChargeGrid.getRowColData(i,6) == null)||(ActiveChargeGrid.getRowColData(i,6)==""))
			{
				if((fm.hideOperate.value == "DELETE") || (fm.hideOperate.value == "UPDATE"))
				{
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					ActiveChargeGrid.checkBoxAllNot();
					ActiveChargeGrid.setFocus(i,1,ActiveChargeGrid);
					selFlag = false;
					break;
				}
			}
			else if ((ActiveChargeGrid.getRowColData(i,6) !== null)||(ActiveChargeGrid.getRowColData(i,6)!==""))
			{	//如果Serialno不为空不能插入
				if(fm.hideOperate.value == "INSERT")
				{
					alert("此纪录已存在，不可插入！");
					ActiveChargeGrid.checkBoxAllNot();
					ActiveChargeGrid.setFocus(i,1,ActiveChargeGrid);
					selFlag = false;
					break;
				}
			}
		}
		else
		{//不是选中的行
			if((ActiveChargeGrid.getRowColData(i,6) == null)||(ActiveChargeGrid.getRowColData(i,6)==""))
				{
					alert("有未保存的新增纪录！请先保存记录。");
					ActiveChargeGrid.checkBoxAllNot();
					ActiveChargeGrid.setFocus(i,1,ActiveChargeGrid);
					selFlag = false;
					break;
				}
			}
		} 
		if(!selFlag) return selFlag;
	    if(iCount == 0)
		{
	    	var title="";
	    	if(fm.hideOperate.value=="UPDATE")
	    	{
	    		title="修改";
	    	}
	    	if(fm.hideOperate.value=="DELETE")
	    	{
	    		title="删除";
	    	}
	    	if(fm.hideOperate.value=="INSERT")
	    	{
	    		title="新增";
	    	}
			alert("请选择要"+title+"的记录!");
			return false
		}
		return true;
	
		
}
//function chkMulLineData()
//{
//	var rowNum = ActiveChargeGrid.mulLineCount;
//	for(i=0;i<rowNum;i++)
//	{
//		if(ActiveChargeGrid.getChkNo(i))
//		{
//			var lifeCharge = ActiveChargeGrid.getRowColData(i,6);
//			var PropertyCharge = ActiveChargeGrid.getRowColData(i,7);
//			var sumCharge = parseFloat(lifeCharge)+parseFloat(PropertyCharge);
//			ActiveChargeGrid.setRowColData(i,8,sumCharge+"");
//		}
//	}
//}
function chkPrimaryKey()
{
	var rowNum = ActiveChargeGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(ActiveChargeGrid.getChkNo(i))
		{
			if(ActiveChargeGrid.getRowColData(i,1)!=ActiveChargeGrid.getRowColData(i,7))
			{
				alert("不能修改管理机构！");
				ActiveChargeGrid.setRowColData(i,1,ActiveChargeGrid.getRowColData(i,7));
				return false;
			}
			if(ActiveChargeGrid.getRowColData(i,2)!=ActiveChargeGrid.getRowColData(i,8))
			{
				alert("不能修改业务员！");
				ActiveChargeGrid.setRowColData(i,2,ActiveChargeGrid.getRowColData(i,8));
				return false;
			}
			if(ActiveChargeGrid.getRowColData(i,3)!=ActiveChargeGrid.getRowColData(i,9))
			{
				alert("不能修改月份！");
				ActiveChargeGrid.setRowColData(i,3,ActiveChargeGrid.getRowColData(i,9));
				return false;
			}
		}
	}
	return true;
}
function getAgentName(a)
{
  var tManageCom = fm.all(a).all('ActiveChargeGrid1').value;
  if(tManageCom==null||tManageCom ==""){
	  alert("请先选择管理机构");
	  return false;
  }
  var strsql1 =" 1 and BranchType=#2# and BranchType2=#02#  and managecom like #" + tManageCom + "%# " ;
  showCodeList('agentcode',[fm.all(a).all('ActiveChargeGrid2')],[0,1],null,strsql1,1,1);
}

