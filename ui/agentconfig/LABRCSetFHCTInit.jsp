<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  tmanagecom=" 1 and  char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000# ";
 var triskcode="1 and code not in (select riskcode from lmriskapp where risktype4=#4#) and code not in (select CODE from LDCODE where codetype=#bankzxqj#) ";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {  
	fm.all('ManageCom').value='';
	fm.all('ManageComName').value='';
    fm.all('RiskCode').value='';
    fm.all('RiskCodename').value=''; 
    fm.all('CurYear').value=''; 
    fm.all('PayintvCode').value='';
    fm.all('PayintvName').value='';
    fm.all('StartDay').value='';
    fm.all('EndDay').value='';
    fm.all('Rate').value=''; 
    fm.all('payyears').value='';
    fm.all('payyearsName').value=''; 
    fm.diskimporttype.value='LARateCommision';
    fm2.diskimporttype.value='LARateCommision';  
    fm.BranchType.value=getBranchType();
    fm2.BranchType.value=getBranchType();
    //alert(fm2.BranchType.value);
    //alert(getBranchType());
    fm.branchtype2.value="0"+<%= BranchType2%>;
    fm2.branchtype2.value="0"+<%= BranchType2%>;
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	iArray[1][0]="险种编码";          		//列名
	iArray[1][1]="80px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
    iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[1][4]="BankRiskCode";              	        //是否引用代码:null||""为不引用
	iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	iArray[1][6]="0|1";
	iArray[1][9]="险种编码|code:RiskCode&notnull";  
	iArray[1][15]="1";
    iArray[1][16]=triskcode;

    iArray[2]=new Array();
    iArray[2][0]="险种名称"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[3]=new Array();
    iArray[3][0]="管理机构"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[3][4]="ComCode";
    iArray[3][5]="3|4";
    iArray[3][6]="0|1";
    iArray[3][9]="管理机构|NotNull&NUM";    
    iArray[3][15]="1";
    iArray[3][16]=tmanagecom;
                                      
    iArray[4]=new Array();
    iArray[4][0]="管理机构名称"; //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许    
          
      
    iArray[5]=new Array();
    iArray[5][0]="缴费方式"; //列名
    iArray[5][1]="60px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[5][9]="缴费方式|NotNull";
    iArray[5][10]="PayintvCode";
    iArray[5][11]="0|^12|年缴|^6|半年缴|^3|季缴|^1|月缴|^0|趸缴";
     
     
     
    iArray[6]=new Array();
    iArray[6][0]="保险期间"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=2;              //是否允许输入,1表示允许,0表示不允许 
    iArray[6][4]="payyear"; 
    iArray[6][5]="6|7";  
    iArray[6][6]="0|1";  

    
    iArray[7]=new Array();
    iArray[7][0]="隐藏"; //列名
    iArray[7][1]="0px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=1;              //是否允许输入,1表示允许,0表示不允许
     
     
    iArray[8]=new Array();
    iArray[8][0]="最低缴费年期"; //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[8][9]="最低缴费年期|NotNull&NUM";
    
    
    iArray[9]=new Array();
    iArray[9][0]="缴费年期小于"; //列名
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=1;              //是否允许输入,1表示允许,0表示不允许   
	iArray[9][9]="缴费年期小于|NotNull&NUM";
		 
       
    iArray[10]=new Array();
    iArray[10][0]="保单年度"; //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许
    iArray[10][9]="保单年度|NotNull&NUM";
       
   
   
    iArray[11]=new Array();
    iArray[11][0]="佣金比率"; //列名
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[11][9]="佣金比率|NotNull&NUM";
        
    iArray[12]=new Array();
    iArray[12][0]="执行起期"; //列名
    iArray[12][1]="80px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=1;              //是否允许输入,1表示允许,0表示不允许
    iArray[12][9]="执行起期|NotNull&Date&len>8";
    
	iArray[13]=new Array();
    iArray[13][0]="序号"; //列名
    iArray[13][1]="0px";        //列宽
    iArray[13][2]=100;            //列最大值
    iArray[13][3]=3;              //是否允许输入,1表示允许,0表示不允许
    
   
    
    
   

        
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
    //SetGrid.hiddenSubtraction =0;
    //SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initImportResultGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	  iArray[1][0]="导入文件批次";          		//列名
	  iArray[1][1]="120px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="所在行"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="日志信息"; //列名
    iArray[3][1]="160px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
      
    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
    ImportResultGrid.canChk = 0;
		//SetGrid.mulLineCount = 10;
    //ImportResultGrid.displayTitle = 1;
    ImportResultGrid.hiddenSubtraction =1;
    ImportResultGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    ImportResultGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    initImportResultGrid();
  }
  catch(re)
  {
    alert("在LaratecommisionSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>