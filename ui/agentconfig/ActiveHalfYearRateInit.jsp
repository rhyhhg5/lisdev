
<%
//程序名称：ActiveHalfYearRateInit.jsp
//程序功能：互动渠道半年奖系数录入初始化
//创建日期：2014-12-9   14:42:50
//创建人  ：yangyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     String currdate = PubFun.getCurrentDate();
%>

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                
	fm.all('ManageCom').value = <%=tG.ManageCom%>;
	var sql="select name from ldcom where comcode ='"+<%=tG.ManageCom%>+"'";
	var array=easyExecSql(sql);
	fm.all('ManageComName').value =array[0][0];
	fm.all('Year').value = '';
	fm.all('YearName').value = '';
	fm.all('Month').value = '';
	fm.all('MonthName').value = '';
	fm.all('hideYear').value = '';
	fm.all('hideYearName').value = '';
	fm.all('hideMonth').value = '';
	fm.all('hideMonthName').value = '';
	fm.all('hideManageCom').value = '';
	fm.all('hideManageComName').value = '';
  }
  catch(ex)
  {
    alert("在ActiveHalfYearRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
   initActiveHalfYearRateGrid(); 
  }
  catch(ex)
  {
    alert("在ActiveHalfYearRateInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    setActiveHalfYearRateValue(); 
  }
  catch(re)
  {
    alert("ActiveHalfYearRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
} 
// 考核职级的标准的初始化
function initActiveHalfYearRateGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="职级编码";          		        //列名
      iArray[1][1]="30px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="职级名称";         		        //列名
      iArray[2][1]="100px";            			//列宽
      iArray[2][2]=10;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="半年奖系数";      	   		//列名
      iArray[3][1]="100px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=1; 
      
      iArray[4]=new Array();
      iArray[4][0]="顺序号";      	   		//列名
      iArray[4][1]="0px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;  
      
      ActiveHalfYearRateGrid = new MulLineEnter( "fm" , "ActiveHalfYearRateGrid" ); 
      //这些属性必须在loadMulLine前
      ActiveHalfYearRateGrid.mulLineCount = 10;   
      ActiveHalfYearRateGrid.displayTitle = 1;  
      ActiveHalfYearRateGrid.hiddenPlus =1;  
      ActiveHalfYearRateGrid.hiddenSubtraction = 1;
      ActiveHalfYearRateGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
