<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
   <%@page import="com.sinosoft.lis.agentwages.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String managecom = request.getParameter("ManageCom");
  String branchtype = request.getParameter("BranchType");
  String branchtype2 = request.getParameter("BranchType2");
  String FlagStr = "Fail";
  String Content = "";
  String message="";
  LAActiveBaseWagesBL tLAActiveBaseWagesBL = new LAActiveBaseWagesBL();
  //取得录入的职级晋升信息
  int lineCount = 10;
  
  String tAgentGrade[] = request.getParameterValues("ActiveBaseWagesGrid1");
  String tAgentGradeName[] = request.getParameterValues("ActiveBaseWagesGrid2");
  String tActiveBaseWages[] = request.getParameterValues("ActiveBaseWagesGrid3");
  String tIdx[] = request.getParameterValues("ActiveBaseWagesGrid4");
  ExeSQL tExe = new ExeSQL();
  String tSql = "select int(max(idx)) from LAWageRadix5 order by 1 desc ";
  String strIdx = "";
  int tMaxIdx = 0;
  strIdx = tExe.getOneValue(tSql);
  if (strIdx == null || strIdx.trim().equals(""))
  {
      tMaxIdx = 0;
  }
  else
  {
      tMaxIdx = Integer.parseInt(strIdx);
      //System.out.println(tMaxIdx);
  }
  LAWageRadix5Set tLAWageRadix5Set = new LAWageRadix5Set();
  LAWageRadix5Schema tLAWageRadix5Schema;
  for(int i=0;i<lineCount;i++)
 {
    tLAWageRadix5Schema = new LAWageRadix5Schema();
    tLAWageRadix5Schema.setManageCom(managecom);//用来记录管理机构
    tLAWageRadix5Schema.setWageType("01");//底薪
    tLAWageRadix5Schema.setWageNo("999999");
    tLAWageRadix5Schema.setAgentGrade(tAgentGrade[i]);
    tLAWageRadix5Schema.setWageCode("WP0001");
    tLAWageRadix5Schema.setWageName("底薪");
    tLAWageRadix5Schema.setBranchType(branchtype);
    tLAWageRadix5Schema.setBranchType2(branchtype2);
    tLAWageRadix5Schema.setRewardMoney(tActiveBaseWages[i]);
   
    tLAWageRadix5Schema.setState("01");//01-- 有效      00--无效
    if((tIdx[i] == null)||(tIdx[i].equals(""))&&tOperate.trim().equals("INSERT"))
	{
  		message = "新增";
  		//需要插入记录
		tMaxIdx++;
		tLAWageRadix5Schema.setIdx(String.valueOf(tMaxIdx));
   	}
     //需要更新
    else if(tOperate.trim().equals("UPDATE"))      			      	
	{
		message = "修改";
		tLAWageRadix5Schema.setIdx(tIdx[i]);      			
	}
	tLAWageRadix5Set.add(tLAWageRadix5Schema);
  }
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.addElement(tLAWageRadix5Set);
  try
  {
    System.out.println("this will save the data!!!");	
    if(tLAActiveBaseWagesBL.submitData(tVData,tOperate))
    {
    	FlagStr = "Succ";
    }
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAActiveBaseWagesBL.mErrors;
    if (!tError.needDealError())
    {
    	Content = message+" 成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = message+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  else
  {
    tError = tLAActiveBaseWagesBL.mErrors;
  	Content = message+" 失败，原因是:" + tError.getFirstError();
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
