<%
//程序名称：IndexSetInit.jsp
//程序功能：
//创建日期：2002-08-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('BranchType').value = '';
    fm.all('IndexType').value = '';
    fm.all('IColName').value = '';
    fm.all('CalCode').value = '';
    fm.all('IndexCode').value = '';
    fm.all('IndexName').value = '';
    fm.all('CalType').value = '';
    fm.all('IndexSet').value = '';
    //  fm.all('calsql').value = '';
  }
  catch(ex)
  {
    alert("在IndexSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initRelaIndexGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许
//
//    iArray[1]=new Array();
//    iArray[1][0]="展业类型"; //列名
//    iArray[1][1]="100px";        //列宽
//    iArray[1][2]=100;            //列最大值
//    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许
//
//
//    iArray[2]=new Array();
//    iArray[2][0]="指标类型"; //列名
//    iArray[2][1]="120px";        //列宽
//    iArray[2][2]=100;            //列最大值
//    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="指标代码"; //列名
    iArray[1][1]="90px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="指标名称"; //列名
    iArray[2][1]="180px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="指标对应表名"; //列名
    iArray[3][1]="100px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="指标对应字段名"; //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="指标集"; //列名
    iArray[5][1]="150px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许

    RelaIndexGrid = new MulLineEnter( "fm" , "RelaIndexGrid" );
    RelaIndexGrid.canChk = 1;
    RelaIndexGrid.mulLineCount = 0;
    RelaIndexGrid.displayTitle = 1;
    RelaIndexGrid.locked = 1;
    RelaIndexGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在IndexSetInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initRelaIndexGrid();
  }
  catch(re)
  {
    alert("IndexSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>