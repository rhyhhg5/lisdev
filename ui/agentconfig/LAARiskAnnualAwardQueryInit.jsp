<%
//程序名称：LABranchGroupQuery.js
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
    fm.all('AgentCode').value = '';
    fm.all('ManageCom').value = '';    
    fm.all('WageNo').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';

  }
  catch(ex)
  {
    alert("在LAARiskAnnualAwardQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

                                

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox();   
    initRewardPunishGrid();
  }
  catch(re)
  {
    alert("在LAARiskAnnualAwardQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化RewardPunishGrid
 ************************************************************
 */
function initRewardPunishGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="管理机构";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="机构代码";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        
        iArray[3]=new Array();
        iArray[3][0]="机构名称";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[4]=new Array();
        iArray[4][0]="顺序号";         //列名
        iArray[4][1]="0px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        
        iArray[5]=new Array();
        iArray[5][0]="金额";         //列名
        iArray[5][1]="0px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="所属年月";         //列名
        iArray[6][1]="100px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        RewardPunishGrid = new MulLineEnter( "fm" , "RewardPunishGrid" ); 

        //这些属性必须在loadMulLine前
        RewardPunishGrid.mulLineCount = 0;   
        RewardPunishGrid.displayTitle = 1;
        RewardPunishGrid.hiddenPlus = 1;
        RewardPunishGrid.hiddenSubtraction = 1;
        RewardPunishGrid.locked=1;
        RewardPunishGrid.canSel=1;
        RewardPunishGrid.canChk=0;
        RewardPunishGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化RewardPunishGrid时出错："+ ex);
      }
    }


</script>