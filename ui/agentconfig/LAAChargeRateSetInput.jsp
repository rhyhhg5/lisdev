<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAAChargeWrapRateInput.jsp
//程序功能：银代手续费比例录入界面
//创建时间：2008-01-24
//创建人  ：Huxl

%>
<%@page contentType="text/html;charset=GBK" %>

<head>
<script>
 var  msql2=" 1 ";
 </script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LAAChargeRateSetInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <SCRIPT src="LAAChargeRateSetInput.js"></SCRIPT>
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./LAAChargeRateSetSave.jsp" method=post name=fm target="fraSubmit">
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 
    查询条件 
    </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
  <table class=common >
   	<TD  class= title>
   	管理机构
   	</TD>
    <TD  class= input>
      <Input class="codeno" name=ManageCom verify="管理机构|notnull"
         ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
         onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
           ><input class=codename name=ManageComName readonly=true elementtype=nacessary>
      </TD>
   	<td  class= title> 
   	代理机构
   	 </td>
    <td  class= input> 
    <input class=codeno name=AgentCom 	
         ondblclick="return getAgentCom(this,Name);"
         onkeyup="return getAgentCom(this,Name);" 
         ><input class="codename" name = "Name"  readOnly > 
         </td>
      </TR>       
   	
    <tr class=common>
    <TD  class= title>
       险种编码
    </TD>
    <TD  class= input>
       <Input class= 'codeno' name= RiskCode 
       ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);"
       onkeyup="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);"
      ><Input class=codename name=RiskCodeName readOnly  >

    </TD>	
    <td  class= title>
    手续费比例
    </td>
    <TD  class= input> 
    <Input class= "common" name=Rate>
    </TD>   
  </tr>
  <tr  class= common>
      <td  class= title> 
      有效起期 
      </td>
         <td  class= input> 
           <input class=coolDatePicker name=StartDate verify="有效起期|Date" format='short'>
         </td>
      <td  class= title> 
      有效止期 
      </td>
         <td  class= input> 
           <input class=coolDatePicker name=EndDate  verify="有效止期|Date" format='short'>
         </td>
      </tr>
  </table>
  </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
     		<input type=button value="修  改" class=cssButton onclick="return DoSave();"> 
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 
    		<input type=button value="下  载" class=cssButton onclick="doDownLoad();"> 
    	</td>
   </tr>      
  </table>
					<br/><font color="#ff0000">(注：手续费比例必须为0-1的数，如23%，记为0.23! 起期止期格式为YYYY-MM-DD，如2011-06-01！手续费比例录入最多可以支持到小数点儿后4位) 
					</font>
  <table>
  	<tr>
    	<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
			</td>
    	<td class= titleImg>比例设置
    	
    	</td>
		</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>      
   <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
   <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
   <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
   <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>  
  <Input type=hidden id="BranchType" name=BranchType value=''>
  <input type=hidden id="sql_where" name="sql_where" >
  <input type=hidden id="fmAction" name="fmAction">   
  <input type="hidden" name=querySQL value="">
 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




