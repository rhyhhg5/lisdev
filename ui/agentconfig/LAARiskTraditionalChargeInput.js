var showInfo;
var mDebug = "0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();

function easyQueryClick(){
	initTraditionalChargeGrid();
	var tManageCom = fm.all('ManageCom').value;
	var tRiskCode = fm.all('RiskCode').value;
	var tCurYear = fm.all('CurYear').value;
	var tPayYear = fm.all('payyear').value;
	var tRate = fm.all('Rate').value;
	if(tManageCom == null || tManageCom == ""){
		alert('请输入管理机构');
		return;
	}
	if(tRiskCode == null || tManageCom == ""){
		alert('请输入险种编码');
		return;
	}
	var strSQL = "";
	 	strSQL = "select riskcode,(select riskname from lmrisk b where a.riskcode = b.riskcode),managecom," +
	 			"(select name from ldcom c where c.comcode = a.managecom )," +
	 			"agentcom," +
	 			"(select name from lacom d where d.agentcom = a.agentcom)," +
	 			"year,payintv,f03,curyear,rate " +
	 			"from laratecommision a"
	 			"where managecom like '"+tManageCom+"%' and riskcode = '"+tRiskCode+"'";	
	 if(tCurYear!=null&&tCurYear!=""){
		 strSQL +="and curyear = '"+tCurYear+"'" ;
	 }
	 if(tPayYear!=null&&tPayYear!=""){
		 strSQL +="and f03 = '"+tPayYear+"'" ;
	 }
	 if(tRate!=null&&tRate!=""){
		 strSQL +="and rate = '"+tRate+"'" ;
	 }
	// 查询SQL，返回结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
	turnPage.queryModal(strSQL,TraditionalChargeGrid);	 	
}
function DoReset() {
	try {
		initForm();
	} catch (re) {
		alert("初始化页面错误，重置出错");
	}
}
function chkMulLine() {
	// alert("enter chkmulline");
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = TraditionalChargeGrid.mulLineCount;
	for (i = 0; i < rowNum; i++) {
		if (TraditionalChargeGrid.getChkNo(i)) {
			iCount++;
			if ((TraditionalChargeGrid.getRowColData(i, 1) == null)
					|| (SetGrid.getRowColData(i, 1) == "")) {
				alert("险种代码不能为空");
				TraditionalChargeGrid.setFocus(i, 1, TraditionalChargeGrid);
				selFlag = false;
				break;
			}
			// if((SetGrid.getRowColData(i,3) ==
			// null)||(SetGrid.getRowColData(i,3) == ""))
			// {
			// alert("缴费年期不能为空");
			// SetGrid.setFocus(i,1,SetGrid);
			// selFlag = false;
			// break;
			// }
			if ((TraditionalChargeGrid.getRowColData(i, 3) == null)
					|| (TraditionalChargeGrid.getRowColData(i, 3) == "")) {
				alert("管理机构不能为空");
				TraditionalChargeGrid.setFocus(i, 3, TraditionalChargeGrid);
				selFlag = false;
				break;
			}
			if ((TraditionalChargeGrid.getRowColData(i, 5) == null)
					|| (SetGrid.getRowColData(i, 5) == "")) {
				// alert('here1');
				alert("中介机构不能为空");
				TraditionalChargeGrid.setFocus(i, 5, TraditionalChargeGrid);
				selFlag = false;
				break;
			}
			if ((TraditionalChargeGrid.getRowColData(i, 7) == null)
					|| (TraditionalChargeGrid.getRowColData(i, 5) == "")) {
				alert("保险期间不能为空");
				TraditionalChargeGrid.setFocus(i, 5, TraditionalChargeGrid);
				selFlag = false;
				break;
			}
			if ((TraditionalChargeGrid.getRowColData(i, 8) == null)
					|| (TraditionalChargeGrid.getRowColData(i, 8) == "")) {
				// alert('here1');
				alert("缴费方式不能为空");
				TraditionalChargeGrid.setFocus(i, 8, TraditionalChargeGrid);
				selFlag = false;
				break;
			}
			if ((TraditionalChargeGrid.getRowColData(i, 9) == null)
					|| (TraditionalChargeGrid.getRowColData(i, 9) == "")) {
				// alert('here2');
				alert("缴费年期不能为空");
				TraditionalChargeGrid.setFocus(i, 9, TraditionalChargeGrid);
				selFlag = false;
				break;
			}
			if ((TraditionalChargeGrid.getRowColData(i, 10) == null)
					|| (TraditionalChargeGrid.getRowColData(i, 10) == "")) {

				alert("保单年度不能为空");
				TraditionalChargeGrid.setFocus(i, 10, TraditionalChargeGrid);
				selFlag = false;
				break;
			}


			if ((TraditionalChargeGrid.getRowColData(i, 11) - TraditionalChargeGrid.getRowColData(i, 11)) > 0) {
				alert("佣金比率不能为空");
				TraditionalChargeGrid.setFocus(i, 1, TraditionalChargeGrid);
				selFlag = false;
				break;
			}
			// 如果Serialno为空不能删除和更新
			if ((TraditionalChargeGrid.getRowColData(i, 9) == null)
					|| (TraditionalChargeGrid.getRowColData(i, 9) == "")) {
				if ((fm.fmAction.value == "DELETE")
						|| (fm.fmAction.value == "UPDATE")) {
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					TraditionalChargeGrid.checkBoxAllNot();
					TraditionalChargeGrid.setFocus(i, 1, TraditionalChargeGrid);
					selFlag = false;
					break;
				}
			} else if ((TraditionalChargeGrid.getRowColData(i, 9) !== null)
					|| (TraditionalChargeGrid.getRowColData(i, 9) !== "")) { // 如果Serialno不为空不能插入
				if (fm.fmAction.value == "INSERT") {
					alert("此纪录已存在，不可插入！");
					TraditionalChargeGrid.checkBoxAllNot();
					TraditionalChargeGrid.setFocus(i, 1, SetGrid);
					selFlag = false;
					break;
				}
			}

		} else {// 不是选中的行
			if ((TraditionalChargeGrid.getRowColData(i, 9) == null)
					|| (TraditionalChargeGrid.getRowColData(i, 9) == "")) {
				alert("有未保存的新增纪录！请先保存记录。");
				TraditionalChargeGrid.checkBoxAllNot();
				TraditionalChargeGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
		}
	}
	if (!selFlag)
		return selFlag;
	if (iCount == 0) {
		alert("请选择要保存或删除的记录!");
		return false
	}
	return true;
}
// 提交前的校验、计算
function beforeSubmit() {
//	if (!SetGrid.checkValue("SetGrid"))
//		return false;
	if (!chkMulLine())
		return false;
	return true;
}

function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}
}