//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mAction="";
var ttIndexCode = "";

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


/*********************************************************************
 *  保存指标设置信息的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
*/
function submitForm()
{
   var showStr = " 	正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

	 if (!verifyInput())
	   return false;
         if (!checkIndexCode())
           return false;
	 if( mAction == "" )
	 {
	    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	    mAction = "INSERT";
	    fm.all( 'fmAction' ).value = mAction;
            getIndexSet();
		  
		//  alert ('IndexSet = ' + tIndexSet);
		  mAction = "";
		  fm.submit(); //提交
	}
}

/*********************************************************************
 *  保存提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
*/
function afterSubmit( FlagStr, content)
{
   showInfo.close();
   if (FlagStr == "Fail")
   {
  	   var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
       showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else
   {
//   	  content = "保存成功！";
          var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

          if (fm.qryFlag.value != "A")
          {
	    showDiv(operateButton,"true"); 
	    showDiv(inputButton,"false"); 
	  }
   }
   if (fm.all('qryFlag').value == "A" && fm.IndexSet.value != "")
      RelaIndexGrid.checkBoxAll();
   mAction = "";
   fm.qryFlag.value = "";
   emptyUndefined();
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
   try
   {
   	   initForm();
   }
   catch(re)
   {
   	   alert("在IndexSetInput.js-->resetForm函数中发生异常:初始化界面错误!");
   }
}


/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}


/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	showDiv( operateButton, "false" );
	showDiv( inputButton, "true" );
	
        fm.all('ITableName').value = "LAIndexInfo";
        fm.all('IndexSet').value = "";
        RelaIndexGrid.clearData("RelaIndexGrid");
}


/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick()
{
		showInfo = window.open("./IndexQuery.html");
}


/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
   var tIndexCode = "";
   tIndexCode = fm.all('IndexCode').value;
   if (tIndexCode != ttIndexCode)
      {
           alert("您不能修改指标编码!");
           fm.all('IndexCode').value = ttIndexCode;
       }
   else
   {
   	if (tIndexCode == null || tIndexCode =="")
        {      
          alert("请先做指标查询，再做修改!");
         }
         else
         {
               getIndexSet();
               
               var showStr = "正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

   	        if (mAction == "")
   	       {
   	        //showSubmitFrame(mDebug);
		    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	            mAction = "UPDATE";
		    fm.all( 'fmAction' ).value = mAction;
		    mAction = "";
		    fm.submit(); //提交
                 }
          }
   }
}



/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick()
{
	var tIndexCode = "";
	tIndexCode = fm.all( 'IndexCode' ).value;
	if( tIndexCode == null || tIndexCode == "" )
		alert( "请先做指标查询操作，再进行删除!" );
	else
	{
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}


/*********************************************************************
 *  Click事件，当点击”查询“按钮时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function qry_rela_index()
{      
      var tBranchType = fm.BranchType.value;
      if (tBranchType == null || tBranchType == '')
      {
      	alert("请选择展业类型!");
      	return false;
      }
      var tIndexType = fm.IndexType.value;
      if (tIndexType == null || tIndexType == '')
      {
      	alert("请选择指标类型!");
      	return false;
      }
      
      var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      mAction = "QUERY";
      fm.all('fmAction').value = mAction;
      fm.all('qryFlag').value = "A";
      mAction = "";
      if(RelaIndexGrid.mulLineCount != 0)
	RelaIndexGrid.clearData("RelaIndexGrid");
      fm.submit();
      
}


/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv( cDiv, cShow )
{
	if( cShow == "true" )
	  cDiv.style.display = "";
        else
	  cDiv.style.display = "none";
}


function afterQuery(arrResult)
{
	initForm();
	fm.all('BranchType').value = arrResult[0];
	fm.all('IndexType').value = arrResult[1];
	fm.all('IndexCode').value = arrResult[2];
	fm.all('IndexName').value = arrResult[3];
        fm.all('ITableName').value = arrResult[4];
	fm.all('IColName').value = arrResult[5];
	fm.all('CalType').value = arrResult[6];
	fm.all('CalCode').value = arrResult[7];
	fm.all('IndexSet').value = arrResult[8];
	ttIndexCode = arrResult[2];
	emptyUndefined();
}
/* liujw */
function changeIndexType()
{
   var tBranchType = fm.BranchType.value;
   if (tBranchType == null || tBranchType == '')
     fm.sql.value = "1 and 1 <> 1";
   else
     fm.sql.value = "1 and (othersign = #0# or ("+tBranchType+">= 2 and othersign = #2#) or ("+tBranchType+"=1 and othersign = #"+tBranchType+"#))";

   return true;
}

function afterCodeSelect( cCodeName, Field )
{
	var value = Field.value;
	
	try	
	{
		if( cCodeName == 'BranchType' )	
		{
		  var tBranchType = fm.BranchType.value;
                  if (tBranchType == null || tBranchType == '')
                    fm.sql.value = "1 and 1 <> 1";
                  else
                    fm.sql.value = "1 and (othersign = #0# or ("+tBranchType+">= 2 and othersign = #2#) or ("+tBranchType+"=1 and othersign = #"+tBranchType+"#))";

		}
	}
	catch( ex ) 
	{
		alert('展业类型设置出错!');
	}
}

function checkIndexCode()
{
   var tIndexCode = fm.IndexCode.value;
   if (tIndexCode == null || tIndexCode == '')
     return false;
     
   var tSql = "Select * From LAAssessIndex Where 1=1"
              +getWherePart('IndexCode'); 
   var strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);  
   //判断是否查询成功
   if (strQueryResult) {
     alert("所录指标代码已存在！");
     fm.IndexCode.value = '';
     return false;
   }
   return true;
}
function getIndexSet()
{
 //为IndexSet拼串
  var tIndexSet = "";
  var i = RelaIndexGrid.mulLineCount;
  var tIndexSetNext;

  for (i=0;i<RelaIndexGrid.mulLineCount;i++)
  {
      if( RelaIndexGrid.getChkNo( i ))
      {
      	tIndexSetNext = RelaIndexGrid.getRowColData(i,1);
      	if( tIndexSet == null || tIndexSet == "")
      	  tIndexSet = tIndexSetNext;
      	else
          tIndexSet = tIndexSet + "," + tIndexSetNext;
      }
  }
  fm.all('IndexSet').value = tIndexSet;	
}
function clearIndexSet()
{
  fm.all('IndexSet').value = "";	
  RelaIndexGrid.clearData();
  RelaIndexGrid.checkBoxAllNot();
}

