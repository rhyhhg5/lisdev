<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LABankWageStandInput.jsp
//程序功能：薪资标准考核标准录入
//创建日期：2016-02-18 10:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String BranchType=request.getParameter("BranchType");
String BranchType2=request.getParameter("BranchType2");
%>
<script>
var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
var manageCom = <%=tG.ManageCom%>;
var tmsql= " 1 and char(length(trim(comcode))) in (#2#,#4#) ";

</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="LABankWageStandInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="LABankWageStandInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="./LABankWageStandSave.jsp" method=post name=fm target="fraSubmit">
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
				<td class=titleImg>
					  查询条件录入
				</td>
			</tr>
		</table>
	<Div  id= "divLAAgent1" style= "display: ''">
		<table class="common" align=center>
			<TR  class= common> 
				<TD  class= title>
					管理机构
				</TD>
				<TD  class= input>
					<Input class="codeno" name=ManageCom verify="管理机构|notnull"
					ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tmsql,1,1);" 
					onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tmsql,1,1);"
					><Input class=codename name=ManageComName readOnly elementtype=nacessary  > 
				</TD> 
                <td  class= title>薪资标准类型</td>
                <TD  class= input> <Input class= "codeno" name=WageCode CodeData="0|^A|A|^B|B" ondblclick="return showCodeListEx('Wagelist',[this,WageName],[0,1]);" onkeyup="return showCodeListKeyEx('Wagelist',[this,WageName],[0,1]);" onchange="" readonly=true><input class=codename name=WageName  readonly=true ></TD> 	
				</TR>
				<TR>	
                  <td  class= title>考核标准类型</td>
                 <TD  class= input> <Input class= "codeno" name=AssessCode CodeData="0|^A|A|^B|B" ondblclick="return showCodeListEx('Assesslist',[this,AssessName],[0,1]);" onkeyup="return showCodeListKeyEx('Assesslist',[this,AssessName],[0,1]);" onchange="" readonly=true><input class=codename name=AssessName  readonly=true ></TD> 	
		        </TD>
                <td  class= title>基本工资发放规则</td>
                   <TD  class= input> <Input class= "codeno" name=RuleCode CodeData="0|^0|全额|^1|业绩达成率" ondblclick="return showCodeListEx('Rulelist',[this,RuleName],[0,1]);" onkeyup="return showCodeListKeyEx('Rulelist',[this,RuleName],[0,1]);" onchange="" readonly=true><input class=codename name=RuleName  readonly=true ></TD> 	
				</TR>		
	
	</table>
</Div>
<br/>
   &nbsp;&nbsp;<INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class=cssbutton >
			   <INPUT VALUE="保  存" TYPE=button onclick="return submitForm();" class=cssbutton >
			   <INPUT VALUE="重  置" TYPE=button onclick="return initForm();" class=cssbutton >
			   <INPUT VALUE="修  改" TYPE=button onclick="return updateClick();" class=cssbutton >
	 <table>
	 	<tr>
	 		<td class=common>
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
 			<td class= titleImg>
	 				薪资考核标准录入
 			</td>
	 	</tr>
	 </table>
 <Div  id= "divLAAgent2" style= "display: ''">
 	<table  class= common>
 		<tr  class= common>
 			<td text-align: left colSpan=1>
 				<span id="spanActiveChargeGrid" >
 				</span>
 			</td>
 		</tr>
 	</table>
 </div>
<font color="red">注：发放规则只对员工制基本工资发放规则有效</font><br/><br/>
<font color="red">注：修改时只能修改薪资标准、考核标准、基本工资发放规则</font>
 <Input type=hidden name=BranchType  value =<%=BranchType %>>   
 <Input type=hidden name=BranchType2 value =<%=BranchType2 %>>    
 <Input type=hidden name=hideOperate >   
 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>


