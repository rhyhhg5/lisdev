<%
//程序名称：LABankRateConvRangeQueryInit.jsp
//程序功能：
//创建日期：2008-03-03 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
    fm.all('MonthRate').value = '';
    fm.all('SeasonRate').value = '';
    fm.all('YearRate').value = '';
    
    fm.all('ManageCom').value = '';
   
    
    
    fm.all('BranchType').value = top.opener.fm.all('BranchType').value;
  }
  catch(ex)
  {
    alert("在LABankRateConvRangeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

                                

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox();   
    initLABankRateConvRangeGrid();
  }
  catch(re)
  {
    alert("LABankRateConvRangeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化ActiRateManageGrid
 ************************************************************
 */
function initLABankRateConvRangeGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="管理机构";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="月度折算比例";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        
        iArray[3]=new Array();
        iArray[3][0]="季度折算比例";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="年度折算比例";         //列名
        iArray[4][1]="80px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="记录顺序号";         //列名
        iArray[5][1]="0px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        

        
  
        LABankRateConvRangeGrid = new MulLineEnter( "fm" , "LABankRateConvRangeGrid" ); 

        //这些属性必须在loadMulLine前
        LABankRateConvRangeGrid.mulLineCount = 0;   
        LABankRateConvRangeGrid.displayTitle = 1;
        LABankRateConvRangeGrid.hiddenPlus = 1;
        LABankRateConvRangeGrid.hiddenSubtraction = 1;
        LABankRateConvRangeGrid.locked=1;
        LABankRateConvRangeGrid.canSel=1;
        LABankRateConvRangeGrid.canChk=0;
        LABankRateConvRangeGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化LABankRateConvRangeGrid时出错："+ ex);
      }
    }


</script>