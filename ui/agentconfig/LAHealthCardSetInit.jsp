<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
// var  tmanagecom=" 1 and  char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000# ";
var tmanagecom =" 1 and   char(length(trim(comcode)))=#8# ";
var tBranchType ="  1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
// var triskcode="1 and code  in (select CODE from LDCODE where codetype=#healthriskcode#) ";
 
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value='';
    fm.all('ManageComName').value='';
    fm.BranchType.value=getBranchType();
    //alert(fm2.BranchType.value);
    //alert(getBranchType());
    fm.branchtype2.value="0"+<%= BranchType2%>;
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
    alert("在LAHealthCardSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[1][4]="ComCode";
    iArray[1][9]="管理机构|NotNull&NUM&len=8";    
    iArray[1][15]="1";
    iArray[1][16]=tmanagecom;
     
    iArray[2]=new Array();
	iArray[2][0]="套餐编码";          		//列名
	iArray[2][1]="100px";      	      		//列宽
	iArray[2][2]=20;            			//列最大值
    iArray[2][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[2][4]="WrapCode";              	        //是否引用代码:null||""为不引用
	iArray[2][5]="2|3";              	                //引用代码对应第几列，'|'为分割符
	iArray[2][6]="0|1";

    iArray[3]=new Array();
    iArray[3][0]="套餐名称"; //列名
    iArray[3][1]="120px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=1;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[4]=new Array();
    iArray[4][0]="佣金比率"; //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[4][9]="佣金比率|NotNull&NUM";
    
       
    iArray[5]=new Array();
    iArray[5][0]="序号"; //列名
    iArray[5][1]="0px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=3;              //是否允许输入,1表示允许,0表示不允许
    
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenSubtraction =1;
    SetGrid.hiddenPlus=0;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAHealthCardSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
  }
  catch(re)
  {
    alert("在LAHealthCardSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>