<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2006-08-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
 var  mSQL=" 1 and  char(length(trim(comcode)))  >= #8# and sign=#1#" ;
 var triskcode="1 and code not in (select riskcode from lmriskapp where risktype4=#4#) ";
  </SCRIPT>       


<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.all('ManageCom').value='';
  	fm.all('ManageComName').value='';
    fm.all('AgentGrade').value='';
    fm.all('CodeName').value='';     
          
  }
  catch(ex)
  {
    alert("在LAEntrySalaryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
        
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[1][4]="ComCode";
    iArray[1][5]="1|2";
    iArray[1][6]="0|1";
//    iArray[1][7]="ComCode";
    iArray[1][9]="管理机构|code:ComCode&NotNull&NUM&len=8";  
    iArray[1][15]="1";
    iArray[1][16]=mSQL; 
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许        

    iArray[3]=new Array();
    iArray[3][0]="职级代码"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[3][7]="getGradeName";
    iArray[3][9]="职级代码|NotNull";      
          
    iArray[4]=new Array();
    iArray[4][0]="职级名称"; //列名
    iArray[4][1]="120px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许
        
    iArray[5]=new Array();
    iArray[5][0]="基本底薪"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;          //列最大值
    iArray[5][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[5][9]="基本底薪|NotNull&NUM&value>";
    
//    iArray[6]=new Array();
//    iArray[6][0]="适用年份"; //列名
//    iArray[6][1]="80px";        //列宽
//    iArray[6][2]=100;          //列最大值
//    iArray[6][3]=1; 
//    iArray[6][9]="适用年份|NotNull&NUM&len=4"; 

    iArray[6]=new Array();
    iArray[6][0]="idx";            //列名 idx
    iArray[6][1]="0px";          //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0; 
   
  
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
   // SetGrid.mulLineCount = 3;   
    SetGrid.canChk = 1;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenSubtraction =1;
    SetGrid.hiddenPlus=0;
    SetGrid.loadMulLine(iArray);   
  }
  catch(ex)
  {
    alert("在LAEntrySalaryInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    //debugger;
    initInpBox();    
    initSetGrid();
  }
  catch(re)
  {
    alert("在LAEntrySalaryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>