//               该文件中包含客户端需要处理的函数和事件
var tAgentCom = "";
var tWageNo ="";
var tManageCom ="";
var mDebug="0";
var mOperate="";
var showInfo;
//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	
	  //alert(mOperate);
    //if (mOperate == "QUERY||MAIN"){
   	//initForm();
    //alert("请重新输入信息！");
    //return false;
  //}
  if (!beforeSubmit())
    return false; 
  if (mOperate=="")
	{
		if((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
		{
		addClick();
    fm.all("SendGrp").value="00"
			}
		else
			{
				alert("此条数据只能进行“修改”或“删除”，不能再次“保存”");
				return false;
				}
    
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  

  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  fm.HManagecom.value=tManageCom;
  fm.HAgentCom.value=tAgentCom;
  fm.HWageNo.value=tWageNo;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  
   
    fm.submit(); //提交
  //}
 //}
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  mOperate="";
  fm.all('Money').disabled=false;
  showInfo.close();
  fm.reset();
  initForm();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    initForm();
  }
  catch(re)
  {
    alert("LAARiskAward.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true"); 
  showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作
  if(!verifyInput()) 
  return false;
 
//  var tAClass=document.fm.AClass.value;
  var tMoney=document.fm.Money.value;  

  
  // 备注
  if(!(strLen(document.fm.Noti.value,100,"[备注]请输入33个汉字或100个字母或数字！")))
  return false;
   
   
  return true	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
 // showDiv(operateButton,"false"); 
 // showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{   
	var state=fm.all("SendGrp").value;
	
   if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要修改的纪录！');
  
 
  else if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    fm.all("SendGrp").value="00";
    fm.all('Money').disabled=false;
	 fm.all('ManageCom').disabled = true;
	 fm.all('AgentCom').disabled = true;
	 fm.all('WageNo').disabled = true;
	 fm.all('Noti').disabled = false;
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./LAARiskAwardManageQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}   
function queryClicksure()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  fm.submit();
  
}              

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{

		var state=fm.all("SendGrp").value;
 
   if ((fm.all("Idx").value==null)||(fm.all("Idx").value==''))
    alert('请先查询出要删除的纪录！');
  
  	
  else if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



function afterQuery(arrQueryResult)
{ 
  var arrResult = new Array();

  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
  
    fm.all('ManageCom').value = arrResult[0][0];
    tManageCom =  fm.all('ManageCom').value;
    fm.all('ManageCom').disabled = true;
    fm.all('ManageComName').value = arrResult[0][1];
    fm.all('AgentCom').value = arrResult[0][2];
    tAgentCom =  fm.all('AgentCom').value;
    //alert(tAgentCom);
    fm.all('AgentCom').disabled = true;
    fm.all('AgentComName').value = arrResult[0][3];                                              
    fm.all('WageNo').value = arrResult[0][4];
    fm.all('WageNo').disabled=true;
    fm.all('Money').value = arrResult[0][5];
    fm.all('Noti').value = arrResult[0][6];
    fm.all('Noti').disabled=false;
    fm.all('Idx').value = arrResult[0][7];
    fm.all('Operator').value = arrResult[0][8];
    fm.all('ModifyDate').value = arrResult[0][9];
    //fm.all('Aclass').value = arrResult[0][10];
    tWageNo = fm.all('WageNo').value;
  
  }
     
}

/***************************************
 * 验证字符的字节长度是否超标
 * 参数：pmStr      准备验证的字符串
 *       pmStrLen   长度标准
 *       pmMissage  报错信息
 * 返回：返回字符串字节长度
 ***************************************/
function strLen(pmStr,pmStrLen,pmMissage)
{
	var i;
  var len;
  len = 0;
  for (i=0;i<pmStr.length;i++)
  {
    if (pmStr.charCodeAt(i)>255) len+=3; else len++;
  }
  //alert("[" + pmStr + "] 长度为： " + len + " 规定长度为："+pmStrLen);
  if(len > pmStrLen)
  {
  	alert(pmMissage);
  	return false;
  }
  
  return true;
}

function getAClass(cObj,cName) 
{
	
 var strsql =" 1 ";
 showCodeList('addproject',[cObj,cName],[0,1]);
  fm.all('DoneFlag').value = '';
  fm.all('DoneFlagName').value = '';  
}



function getAgentCom(cObj,cName)
{
if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
	alert("请输入管理机构！");
	return false;
showCodeList('agentcom',[cObj,cName],[0,1],null,'1 and BranchType=#1# and BranchType2=#02# ',1,1);
}
else if (fm.all('ManageCom').value != null || trim(fm.all('ManageCom').value) != '') 
{
var strsql =" 1 and BranchType=#1# and BranchType2=#02#  and managecom like #" + fm.all('ManageCom').value + "%# " ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,1,1);
}

}