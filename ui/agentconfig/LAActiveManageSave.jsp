<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAActiveManage Save.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentdaily.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentCompreScoreSchema tLAAgentCompreScoreSchema   ;
  LAAgentCompreScoreSet tLAAgentCompreScoreSet = new LAAgentCompreScoreSet();
  LAActiveManageBL tLAActiveManageBL   = new LAActiveManageBL();
  
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";
  int lineCount =11;
	GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");
	String operator=request.getParameter("Operator");
    String agentcode = request.getParameter("AgentCode");
    String branchtype = request.getParameter("BranchType");
    String branchtype2 = request.getParameter("BranchType2");
    String wageno = request.getParameter("Year")+request.getParameter("Month");
    System.out.println("----"+request.getParameter("Month"));
    String ProjectCode[] = request.getParameterValues("LAAgentCompreScoreGrid1");
    String ProjectName[] = request.getParameterValues("LAAgentCompreScoreGrid2");
    String PropertyScore[] = request.getParameterValues("LAAgentCompreScoreGrid3");
    String LifeScore[] = request.getParameterValues("LAAgentCompreScoreGrid4");
    String SumScore[] = request.getParameterValues("LAAgentCompreScoreGrid5");
  
    for(int i=0;i<lineCount;i++)
 	{  
	    tLAAgentCompreScoreSchema = new LAAgentCompreScoreSchema();
	    tLAAgentCompreScoreSchema.setAgentCode(agentcode);
	    tLAAgentCompreScoreSchema.setWageNo(wageno);
	    tLAAgentCompreScoreSchema.setProjectCode(ProjectCode[i]);
	    tLAAgentCompreScoreSchema.setProjectName(ProjectName[i]);
	    tLAAgentCompreScoreSchema.setPropertyScore(PropertyScore[i]);
	    tLAAgentCompreScoreSchema.setLifeScore(LifeScore[i]);
	    tLAAgentCompreScoreSchema.setSumScore(SumScore[i]);
	    tLAAgentCompreScoreSchema.setBranchType(branchtype);
	    tLAAgentCompreScoreSchema.setBranchType2(branchtype2);
	    tLAAgentCompreScoreSchema.setState("01");//01  有效    00  无效
	    tLAAgentCompreScoreSet.add(tLAAgentCompreScoreSchema);
 	}


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	
	tVData.add(tLAAgentCompreScoreSet);
	tVData.add(tG);
	
  try
  {
    tLAActiveManageBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAActiveManageBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
