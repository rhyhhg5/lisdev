//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var End = fm.EndDate.value;
	var cur = fm.CurrentDate.value;	
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick1()
{	 
	fm.all('divSpanSetGrid1').style.display='';
	fm.all('divSpanSetGrid2').style.display='none';
	fm.all('divSpanSetGrid3').style.display='none';
	fm.all('divSpanSetGrid4').style.display='none';
    if(!verifyInput())
	{
	 return false;
	}
	initSetGrid1();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.riskcode  aa ,b.riskname bb ,a.managecom cc,(select Name from ldcom where comcode=a.managecom),"
	+"a.agentcom,(select name from lacom where agentcom=a.agentcom and branchtype='3'),"
	+ "a.f01,'-',value(a.year,0),value(a.f03,0),value(a.f04,0),value(a.curyear,0),"
	+ " value(a.rate,0),a.modifydate,a.f06"
	+ " from laratecommision a,lmriskapp b "
	+ " where a.branchtype='3' and  a.riskcode = b.riskcode and versiontype='11'"
	+" and b.risktype4='4' and a.f05='Y2' "
	+" and a.modifydate between '"
	+fm.all('StartDate').value+"' and '"+fm.all('EndDate').value+"' "
	+ " and a.managecom like '"+fm.all('ManageCom').value+"%' "
	+ getWherePart( 'a.riskcode','RiskCode')
	+" union "
	+" select a.riskcode  aa,b.riskname bb,a.managecom  cc,(select Name from ldcom where comcode=a.managecom),"
	+"a.agentcom,(select name from lacom where agentcom=a.agentcom and branchtype='3'),"
	+ "'-',a.payintv,value(a.year,0),value(a.f03,0),value(a.f04,0),value(a.curyear,0),"
	+ " value(a.rate,0),a.modifydate,a.f06"
	+ " from laratecommision a,lmriskapp b "
	+ " where a.branchtype='3' and  a.riskcode = b.riskcode and versiontype='11' and a.f05='Y2'  "
	+" and b.risktype4<>'4'  "
	+" and a.modifydate between '"
	+fm.all('StartDate').value+"' and '"+fm.all('EndDate').value+"' "
	+ " and a.managecom like '"+fm.all('ManageCom').value+"%' "
	+ getWherePart( 'a.riskcode','RiskCode')
	+" order by aa,bb,cc";
	
	fm.all('querySQL').value=strSQL;
	fm.all('downloadType').value='bankrate';
	turnPage.queryModal(strSQL, SetGrid1);
	fm.all('divPage').style.display='';   	
	
}
function easyQueryClick2()
{
	initSetGrid2();
	fm.all('divSpanSetGrid1').style.display='none';
	fm.all('divSpanSetGrid2').style.display='';
	fm.all('divSpanSetGrid3').style.display='none';
	fm.all('divSpanSetGrid4').style.display='none';
    if(!verifyInput())
	{
	 return false;
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL =  "select a.riskcode bb ,b.riskname cc ,a.ManageCom aa,(select name from ldcom where comcode=a.managecom)"
	+",'-',trim(a.payintv),char('-'),a.f03,a.f04,value(a.curyear,0),"
	+ " value(a.rate,0),a.modifydate,a.f06 "
	+ " from laratecommision a,lmriskapp b "
	+ " where a.riskcode = b.riskcode  and a.agentcom is null and b.risktype4<>'4' and a.f05='Z2' and a.riskcode not in ('332401','730101') "
	+" and a.modifydate between '"
	+fm.all('StartDate').value+"' and '"+fm.all('EndDate').value+"' "
	+ " and a.managecom like '"+fm.all('ManageCom').value+"%' "
	+ getWherePart( 'a.riskcode','RiskCode')
	+" union "
	+"select a.riskcode bb ,b.riskname cc ,a.ManageCom aa,(select name from ldcom where comcode=a.managecom),"
	+"a.f01,trim(a.payintv),char(value(a.year,0)),a.f03,a.f04,value(a.curyear,0),"
	+ " value(a.rate,0),a.modifydate,a.f06"
	+ " from laratecommision a,lmriskapp b "
	+ " where a.riskcode = b.riskcode and b.risktype4='4' and a.agentcom is null and a.f05='Z2'  "
	+" and a.modifydate between '"
	+fm.all('StartDate').value+"' and '"+fm.all('EndDate').value+"' "
	+ " and a.managecom like '"+fm.all('ManageCom').value+"%' "
	+ getWherePart( 'a.riskcode','RiskCode')
	+" union "
	+"select a.riskcode bb ,b.riskname cc ,a.ManageCom aa,(select name from ldcom where comcode=a.managecom)"
	+",'-',trim(a.payintv),char(value(a.year,0)),a.f03,a.f04,value(a.curyear,0),"
	+ " value(a.rate,0),a.modifydate,a.f06 "
	+ " from laratecommision a,lmriskapp b "
	+ " where a.riskcode = b.riskcode  and a.agentcom is null and b.risktype4<>'4' and a.f05='Z2' and a.riskcode  in ('332401','730101') "
	+"  and a.modifydate between '"
	+fm.all('StartDate').value+"' and '"+fm.all('EndDate').value+"' "
	+ " and a.managecom like '"+fm.all('ManageCom').value+"%' "
	+ getWherePart( 'a.riskcode','RiskCode')
	+" order by aa,bb,cc";
	fm.all('querySQL').value=strSQL;
	fm.all('downloadType').value='nonbankrate';
	turnPage.queryModal(strSQL, SetGrid2);
	fm.all('divPage').style.display=''; 
}
function easyQueryClick3()
{
	initSetGrid3();
	fm.all('divSpanSetGrid1').style.display='none';
	fm.all('divSpanSetGrid2').style.display='none';
	fm.all('divSpanSetGrid3').style.display='';
	fm.all('divSpanSetGrid4').style.display='none';
    if(!verifyInput())
	{
	 return false;
	}
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.managecom aa ,code6,"
	+" riskcode bb,(select riskname from lmrisk where riskcode = a.riskcode),"
	+" insureyear,payintv,code4,code1,code2,rate,a.modifydate cc"
	+" from LADiscount a "
	+" where a.discounttype='21' and code5= '1'  and a.modifydate between '"
	+fm.all('StartDate').value+"' and '"+fm.all('EndDate').value+"' "
	+" and a.managecom like '"+fm.all('ManageCom').value+"%' "
	+" "
	+ getWherePart( 'a.riskcode','RiskCode')
//	+ getWherePart( 'a.agentgrade','GradeSort')
	//+" union select agentgrade,(select codename from ldcode where code = a.agentgrade and codetype='gradelevel' ),"
	//+ "riskcode bb,(select riskname from lmrisk where riskcode = a.riskcode), insureyear,banktype,rate,a.modifydate cc,a.managecom aa "
	//+ "from LADiscountb a where a.discounttype='06'  and a.modifydate between '"
	//+fm.all('StartDate').value+"' and '"+fm.all('EndDate').value+"' "
	//+ " and a.managecom like '"+fm.all('ManageCom').value+"%' "
	//+ getWherePart( 'a.riskcode','RiskCode')
	//+ getWherePart( 'a.agentgrade','GradeSort')
	+" order by aa,bb,cc";
	fm.all('querySQL').value=strSQL;
	fm.all('downloadType').value='nondirectrate';
	turnPage.queryModal(strSQL, SetGrid3); 
	fm.all('divPage').style.display=''; 
}

function easyQueryClick4()
{
	initSetGrid4();
	fm.all('divSpanSetGrid1').style.display='none';
	fm.all('divSpanSetGrid2').style.display='none';
	fm.all('divSpanSetGrid3').style.display='none';
	fm.all('divSpanSetGrid4').style.display='';
    if(!verifyInput())
	{
	 return false;
	}
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.managecom aa ,"
	+" riskcode bb,(select riskname from lmrisk where riskcode = a.riskcode),"
	+" insureyear,payintv,code4,code1,code2,rate,a.modifydate cc"
	+" from LADiscount a "
	+" where a.discounttype='22' and code5= '1' and (a.code6 is null or a.code6='')  and a.modifydate between '"
	+fm.all('StartDate').value+"' and '"+fm.all('EndDate').value+"' "
	+" and a.managecom like '"+fm.all('ManageCom').value+"%' "
	+" "
	+ getWherePart( 'a.riskcode','RiskCode')
//	+ getWherePart( 'a.agentgrade','GradeSort')
	//+" union select agentgrade,(select codename from ldcode where code = a.agentgrade and codetype='gradelevel' ),"
	//+ "riskcode bb,(select riskname from lmrisk where riskcode = a.riskcode), insureyear,banktype,rate,a.modifydate cc,a.managecom aa "
	//+ "from LADiscountb a where a.discounttype='06'  and a.modifydate between '"
	//+fm.all('StartDate').value+"' and '"+fm.all('EndDate').value+"' "
	//+ " and a.managecom like '"+fm.all('ManageCom').value+"%' "
	//+ getWherePart( 'a.riskcode','RiskCode')
	//+ getWherePart( 'a.agentgrade','GradeSort')
	+" order by aa,bb,cc";
	fm.all('querySQL').value=strSQL;
	fm.all('downloadType').value='directrate';
	turnPage.queryModal(strSQL, SetGrid4); 
	fm.all('divPage').style.display=''; 
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == "BranchType2")
	{
          var  sql=" select enddate+1 day  from  lawagelog  where 1=1 and  wageno=(select max(wageno) from lawagelog where branchtype='"+fm.BranchType.value+"' "
                     +"  and  branchtype2='"+fm.BranchType2.value+"' and managecom='"+fm.ManageCom.value+"') "
                     + getWherePart('ManageCom', 'ManageCom')
                     + getWherePart('BranchType', 'BranchType')
          	     + getWherePart('BranchType2', 'BranchType2');
          var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
       
   //查询成功则拆分字符串，返回二维数组
   
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);
	
          if( tArr != null )
	  {
           fm.all('StartDate').value= tArr[0][0];                                   
             
          }
	}
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("计算起期必须小于等于计算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}

function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#2# and BranchType2=#02# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}
function doDownload(){
	var tQuerySQL=fm.all('querySQL').value;
	var tQueryType=fm.all('downloadType').value;
	  if(tQuerySQL==null || tQuerySQL=='' || tQueryType==null || tQueryType=='')
  {
    alert("没有进行查询！请先查询再进行下载。");
  }
    fm.action = "LABankWRateReport.jsp";
    fm.submit();
}