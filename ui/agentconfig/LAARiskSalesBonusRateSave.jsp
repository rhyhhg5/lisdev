<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  String tRiskCode=request.getParameter("RiskCode");
  String message ="";
  LAARiskSalesBonusRateUI tLAARiskSalesBonusRateUI = new LAARiskSalesBonusRateUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LARateCommisionSet tSetU = new LARateCommisionSet();	//用于更新
      LARateCommisionSet tSetD = new LARateCommisionSet();	//用于删除
      LARateCommisionSet tSetI = new LARateCommisionSet();//用于插入
            
      String tChk[] = request.getParameterValues("InpSetGridChk"); 
      String mRiskCode[] = request.getParameterValues("SetGrid1");
      String tRiskName[] = request.getParameterValues("SetGrid2");
      String tManageCom[] = request.getParameterValues("SetGrid3");
      String tManageName[] = request.getParameterValues("SetGrid4");
      String tAgentCom[] = request.getParameterValues("SetGrid5");
      String tName[] = request.getParameterValues("SetGrid6");
      String tF3[] = request.getParameterValues("SetGrid7");
      String tYear[] = request.getParameterValues("SetGrid8");
      String tRate[] = request.getParameterValues("SetGrid9");
      String tCuryear[] = request.getParameterValues("SetGrid10");
      String tPayIntv[] = request.getParameterValues("SetGrid11");
      String tSerialno[] = request.getParameterValues("SetGrid12");
			//获取最大的ID号
      ExeSQL tExe = new ExeSQL();
      String tSql = "select int(max(idx)) from laratecommision order by 1 desc ";
      String strIdx = "";
      int tMaxIdx = 0;

      strIdx = tExe.getOneValue(tSql);
      if (strIdx == null || strIdx.trim().equals(""))
      {
          tMaxIdx = 0;
      }
      else
      {
          tMaxIdx = Integer.parseInt(strIdx);
          //System.out.println(tMaxIdx);
      }
      //创建数据集
      for(int i=0;i<tChk.length;i++)
      {
      	if(tChk[i].equals("1"))
      	{
      		//创建一个新的Schema
      		LARateCommisionSchema tSch = new LARateCommisionSchema();
      		
      	  tSch.setRiskCode(mRiskCode[i].trim());
          tSch.setBranchType("1");
          tSch.setsex("A");
          tSch.setF01("10");
          tSch.setAppAge(0);
          tSch.setF03(tF3[i].trim());
          tSch.setCurYear(tCuryear[i].trim());
          tSch.setF04(100);
          tSch.setPayIntv(tPayIntv[i].trim());
          tSch.setF02("C10202");
          tSch.setF05("00");//        
          tSch.setF06("0");       
          tSch.setRate(Double.parseDouble(tRate[i].trim()));
          tSch.setManageCom(tManageCom[i].trim());
          tSch.setAgentCom(tAgentCom[i].trim());
          tSch.setBranchType2("02");
          

      		
      		if((tSerialno[i] == null)||(tSerialno[i].equals("")))
      		{
      			message = "新增";
      			//需要插入记录
				tMaxIdx++;
				tSch.setIdx(String.valueOf(tMaxIdx));
      			tSetI.add(tSch);
       		}
      		else
      		{
            //需要删除
            if(tAction.trim().equals("DELETE"))      			      	
      			{
      				message = "删除";
	      			tSch.setIdx(tSerialno[i]);      			
	      			tSetD.add(tSch);
      			}
            //需要更新
            else if(tAction.trim().equals("UPDATE"))      			      	
      			{
      				message = "修改";
	      			tSch.setIdx(tSerialno[i]);      			
	      			tSetU.add(tSch);
      			}
      		 
       		}
      	}      	
      }
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      
      tVData.add(tGI);
      //没有更新或删除或插入的数据
      if ((tSetD.size() == 0)&&(tSetU.size() == 0)&&(tSetI.size() == 0))
      {
        	FlagStr = "Fail";
        	Content = FlagStr + "未选中要处理的数据！";      	
      }
    else if (tSetI.size() != 0)
    	{
        	//只有插入数据
        	tVData.add(tSetI);
        	System.out.println("Start tLAARiskSalesBonusRateUI Submit...INSERT");
        	tLAARiskSalesBonusRateUI.submitData(tVData,"INSERT");        	    	
    	}
    else if (tSetD.size() != 0)
    	{
        	//只有删除数据
        	tVData.add(tSetD);
        	System.out.println("Start tLAARiskSalesBonusRateUI Submit...DELETE");
        	tLAARiskSalesBonusRateUI.submitData(tVData,"DELETE");        	    	
    	}
    else if (tSetU.size() != 0)
    	{
        	//只有删除数据
        	tVData.add(tSetU);
        	System.out.println("Start tLAARiskSalesBonusRateUI Submit...UPDATE");
        	tLAARiskSalesBonusRateUI.submitData(tVData,"UPDATE");        	    	
    	}
    	

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAARiskSalesBonusRateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = message+" 成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
