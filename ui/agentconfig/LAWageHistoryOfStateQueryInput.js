var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

// 页面查询功能
function rqueryClick()
{
   if (verifyInput()==false) return false; // 页面简单校验
   initBranchGroupGrid();
   var strSQL = "";
	//strSQL = "select BranchAttr,name,BranchManager,BranchManagerName,ManageCom,ApplyGbFlag,ApplyGbStartDate,GbuildFlag,GbuildStartDate,GbuildEndDate,AgentGroup from LaBranchgroup where 1=1 and Branchlevel = '03' "
	strSQL = "select ManageCom,(select Name from LDCom where ComCode=lawagehistory.ManageCom),WageNo,state,BranchType,(select CodeName from ldcode where codetype = 'branchtype' and Code=lawagehistory.BranchType),BranchType2,(select CodeName from ldcode where codetype = 'branchtype2' and Code=lawagehistory.BranchType2) from lawagehistory where 1=1 "    
			+ getWherePart('ManageCom','ManageCom')
			+ getWherePart('BranchType','BranchType')
			+ getWherePart('BranchType2','BranchType2')
			+ getWherePart('WageNo','WageNo');
	
	var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	if(!strQueryResult)
	{  
		alert("没有符合条件的信息！"); 
		turnPage.queryModal(strSQL, BranchGroupGrid);
		return false;
	}
	else turnPage.queryModal(strSQL, BranchGroupGrid);   	
}

//返回操作
function returnClick()
{
    var arrReturn = new Array();
	var tSel = BranchGroupGrid.getSelNo();
		
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
//			var tState = BranchGroupGrid.getRowColData(tSel-1,4);
//			if(tState=='14'){
//				alert("审核发放状态（14）不允许修改");
//				return false;
//			}
			
			try
			{		
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
// 查询返回数据信息
function getQueryResult()
{
   	var arrSelected = null;
	tRow = BranchGroupGrid.getSelNo();
	if( tRow == 0 || tRow == null) return arrSelected;
	arrSelected = new Array();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select ManageCom,(select Name from LDCom where ComCode=lawagehistory.ManageCom),WageNo,state,BranchType,(select CodeName from ldcode where codetype = 'branchtype' and Code=lawagehistory.BranchType),BranchType2,(select CodeName from ldcode where codetype = 'branchtype2' and Code=lawagehistory.BranchType2)";
	strSQL +="  from lawagehistory where ManageCom='"+BranchGroupGrid.getRowColData(tRow-1,1)+"' and WageNo='"+BranchGroupGrid.getRowColData(tRow-1,3)+"' and BranchType='"+BranchGroupGrid.getRowColData(tRow-1,5)+"' and BranchType2='"+BranchGroupGrid.getRowColData(tRow-1,7)+"' ";     
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
	
	
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
	
}




