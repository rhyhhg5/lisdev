//               该文件中包含客户端需要处理的函数和事件
//程序名称：LASIRiskRateInput.js
//创建时间：2018-02-05
//创建人  ：王清民

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var queryFlag =false;

//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!chkPrimaryKey()) return false;
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}

//提交，保存按钮对应操作
function DoInsert()
{   
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}

function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	if(!verifyInput())
	{
	 return false;
	}
	initSetGrid();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.riskcode,b.riskname,"
		     +"a.managecom,(select name from ldcom where comcode = a.managecom),"
		     +"a.agentcom,(select name from lacom where agentcom = a.agentcom),"
		     +"a.f01,a.year,a.PayIntv,a.f03,a.curyear,a.rate,a.idx,a.riskcode,a.managecom,a.agentcom,a.f01,a.year,a.PayIntv,a.f03,a.curyear from  laratecommision a ,lmriskapp b"
		     +" where a.riskcode = b.riskcode  and  a.branchtype='6' and  a.branchtype2='02' and a.f06='602' and  a.agentcom is not null and (b.taxoptimal <>'Y' or b.taxoptimal is null)  "
		     + " and  a.managecom like '"+fm.ManageCom.value+"%' "
		     + getWherePart( 'a.riskcode','RiskCode')
		 	 + getWherePart( 'a.agentcom','AgentCom')
		 	 + getWherePart( 'a.curYear','CurYear')
		 	 + getWherePart('a.rate','Rate')
		 	 + getWherePart('a.f01','F05Code')

    strSQL = strSQL +"  order by a.managecom, a.riskcode";
   //查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}

	//查询成功则拆分字符串，返回二维数组
	  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  turnPage.arrDataCacheSet = arrDataSet;

	//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = SetGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;

	//设置查询起始位置
	turnPage.pageIndex = 0;

	//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

	//调用MULTILINE对象显示查询结果
	displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			initForm();
		}
}

function chkPrimaryKey()
{
	var rowNum =  SetGrid.mulLineCount;
		for(i=0;i<rowNum;i++)
	{
		if( SetGrid.getChkNo(i))
		{   
			if(SetGrid.getRowColData(i,1)!=SetGrid.getRowColData(i,14))
			{
				alert("不能修改险种编码！");
				SetGrid.setRowColData(i,1,SetGrid.getRowColData(i,14));

				return false;
			}
			if(SetGrid.getRowColData(i,3)!=SetGrid.getRowColData(i,15))
			{
				alert("不能修改管理机构编码！");
				SetGrid.setRowColData(i,3,SetGrid.getRowColData(i,15));

				return false;
			}
			if(SetGrid.getRowColData(i,5)!=SetGrid.getRowColData(i,16))
			{
				alert("不能修改代理机构编码！");
				SetGrid.setRowColData(i,5,SetGrid.getRowColData(i,16));
				return false;
			}
			if(SetGrid.getRowColData(i,7)!=SetGrid.getRowColData(i,17))
			{
				alert("不能修改保费类型！");
				SetGrid.setRowColData(i,7,SetGrid.getRowColData(i,17));

				return false;
			}
			if(SetGrid.getRowColData(i,8)!=SetGrid.getRowColData(i,18))
			{
				alert("不能修改保险期间！");
				SetGrid.setRowColData(i,8,SetGrid.getRowColData(i,18));

				return false;
			}
			if(SetGrid.getRowColData(i,9)!=SetGrid.getRowColData(i,19))
			{    
				alert("不能修改缴费方式！");
				SetGrid.setRowColData(i,9,SetGrid.getRowColData(i,19));

				return false;
			}
			if(SetGrid.getRowColData(i,10)!=SetGrid.getRowColData(i,20))
			{   
				alert("不能修改缴费年期！");
				SetGrid.setRowColData(i,10,SetGrid.getRowColData(i,20));

				return false;
			}
			if(SetGrid.getRowColData(i,11)!=SetGrid.getRowColData(i,21))
			{
				alert("不能修改保单年度！");
				SetGrid.setRowColData(i,11,SetGrid.getRowColData(i,21));

				return false;
			}
		
		}
	}
	return true;
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("初始化页面错误，重置出错");
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}
		//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
{
	if (confirm("您确实想修改该记录吗?"))
	{
		fm.fmAction.value = "UPDATE";
		if(!beforeSubmit()) return false;
		var i = 0;
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
	}
	Else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}

//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{   
	//alert("enter chkmulline");
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = SetGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{   
		if(SetGrid.getChkNo(i))
		{

			iCount++;
			if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
			{
				alert("险种编码不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,3) == null)||(SetGrid.getRowColData(i,3) == ""))
			{
				alert("管理机构不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,5) == null)||(SetGrid.getRowColData(i,5) == ""))
			{
				alert("代理机构不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,7) == null)||(SetGrid.getRowColData(i,7) == ""))
			{
				alert("保费类型不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9) == ""))
			{
				alert("缴费方式不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,10) == null)||(SetGrid.getRowColData(i,10) == ""))
			{
				alert("缴费年期不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,11) == null)||(SetGrid.getRowColData(i,11) == ""))
			{
				alert("保单年度不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,12) == null)||(SetGrid.getRowColData(i,12) == ""))
			{
				alert("手续费比例不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			 
			if(SetGrid.getRowColData(i,12)<0)
			{
				alert("手续费比例不能为负数!");
			    SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
								       }

			 if(SetGrid.getRowColData(i,12)<0||SetGrid.getRowColData(i,12)>1)
			 {
				alert("手续费比例不在0到1之间!");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			  }
			//如果Serialno为空不能删除和更新
			if((SetGrid.getRowColData(i,13) == null)||(SetGrid.getRowColData(i,13)==""))
			{
				if((fm.fmAction.value == "DELETE") || (fm.fmAction.value == "UPDATE"))
				{
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
			else if ((SetGrid.getRowColData(i,13) !== null)||(SetGrid.getRowColData(i,13)!==""))
			{	//如果Serialno不为空不能插入
				if(fm.fmAction.value == "INSERT")
				{
					alert("此纪录已存在，不可插入！");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			
			}
		   var tcode = SetGrid.getRowColData(i,1);
		   var tF03  = SetGrid.getRowColData(i,7);
		   var checkSQL1 ="select riskname from lmriskapp where 1=1 and risktype4='4' and riskcode ='"+tcode+"'";
		    turnPage1.strQueryResult =easyQueryVer3(checkSQL1, 1, 0, 1);
		    if (!turnPage1.strQueryResult && tF03!=00) {
				alert("普通险种保费类型应为00！");
				return false;
			}
		}
		else
		{//不是选中的行
			if((SetGrid.getRowColData(i,13) == null)||(SetGrid.getRowColData(i,13)==""))
			{
				alert("有未保存的新增纪录！请先保存记录。");
				SetGrid.checkBoxAllNot();
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
		}
	}
	if(!selFlag) return selFlag;
	if(iCount == 0)
	{
		alert("请选择要保存或删除的记录!");
		return false
	}
	return true;
}

//提交前的校验、计算
function beforeSubmit()
{										
	if(!SetGrid.checkValue("SetGrid")) return false;
	if(!chkMulLine()) return false;
	return true;
	
}
//磁盘导入
function diskImport()
{ 

   if(fm2.FileName.value==null||fm2.FileName.value==""){
   	 alert("请选择要导入的文件！");
   	 return false;
   }
   var showStr="正在导入数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   fm2.submit();
}
function clearImport(){
	document.getElementById('FileName').outerHTML=document.getElementById('FileName').outerHTML;
	initImportResultGrid();
	initInpBox1();
}
//模板下载
function moduleDownload(){
	var tSQL="";
	fm.querySql.value = tSQL;
    var oldAction = fm.action;
    fm.action = "LASIRiskRateModule.jsp?flag=0";
    fm.submit();
    fm.action = oldAction;
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";
	}
}

function queryImportResult(){
	var tbatchno=fm2.FileImportNo.value;
	var tqueryType=fm2.queryType.value;
	if(tbatchno==null||tbatchno==""){
		alert("请输入查询文件批次号！");
		return false;
	}
	if(tqueryType==null||tqueryType==""){
		alert("请选择查询类型！");
		return false;
	}
	strSQL = "select batchno,contid,errorinfo"
	+ " from lcgrpimportlog"
	+ " where batchno='"+tbatchno+"' ";
	if(tqueryType==1){
		strSQL+=" and errorstate='1'";
	}else if(tqueryType==2){
		strSQL+=" and errorstate='0'";
	}
	strSQL+=" order by contid";
  //执行查询并返回结果
	turnPage2.queryModal(strSQL, ImportResultGrid);
	}

function getAgentCom(cObj,cName)
{
if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
showCodeList('agentcom',[cObj,cName],[0,1],null,'1 and BranchType=#6# and BranchType2 =#02# ',1,1);
}
else if (fm.all('ManageCom').value != null || trim(fm.all('ManageCom').value) != '') 
{
var strsql =" 1 and BranchType=#6# and BranchType2 =#02#  and managecom like #" + fm.all('ManageCom').value + "%# " ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,1,1);
}

}		

							
function doDownLoad(){
	if(fm.ManageCom.value==null||fm.ManageCom.value=='')
	{
	 alert("管理机构不能为空！");
	 return false;
	}
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.riskcode,(select riskname from lmriskapp where riskcode = a.riskcode),"
	     +"a.managecom,(select name from ldcom where comcode = a.managecom),"
	     +"a.agentcom,(select name from lacom where agentcom = a.agentcom),"
	     +"a.f01,a.PayIntv,a.f03,a.curyear,a.rate from  laratecommision a "
	     +" where a.f06 = '602' and a.branchtype='6' and  a.branchtype2='02' and a.agentcom is not null  "
	     + " and  a.managecom like '"+fm.ManageCom.value+"%' "
	     + getWherePart( 'a.riskcode','RiskCode')
	 	 + getWherePart( 'a.agentcom','AgentCom')
	 	 + getWherePart( 'a.curYear','CurYear')
	 	 + getWherePart('a.rate','Rate')
	 	 + getWherePart('a.f01','F05Code')
     
	if(fm.Rate.value!=null&&fm.Rate.value!='')
	{
	 strSQL =strSQL+ " and a.rate="+fm.Rate.value+" ";
	}
	fm.all('querySql').value=strSQL;
	var oldaction = fm.action;
    fm.action = "./LASIRiskRateDown.jsp?";
    fm.submit();
    fm.action =oldaction;
}
		




