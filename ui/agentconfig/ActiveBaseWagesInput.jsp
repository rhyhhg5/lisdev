<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAAgentInput.jsp
//程序功能：个人代理增员管理
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String BranchType=request.getParameter("BranchType");
String BranchType2=request.getParameter("BranchType2");
String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<script>
var manageCom = <%=tG.ManageCom%>;
var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
msql=" 1";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="ActiveBaseWagesInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="ActiveBaseWagesInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="./ActiveBaseWagesSave.jsp" method=post name=fm target="fraSubmit">
		<table class="common" align=center>
			<tr align=right>
				<td class=button >
					&nbsp;&nbsp;
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="保  存" name=Save TYPE=button onclick="return submitForm();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="重  置" name=reset TYPE=button onclick="return initForm();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="修  改" name=Modify TYPE=button onclick="return updateClick();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="查  询 " name=reset TYPE=button onclick="return queryClick();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="标准查询"  name=Query TYPE=button onclick="return queryStandClick();">
				</td>
			</tr>
		</table>
		<table>
			<tr class=common>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
				<td class=titleImg>
					  考核标准录入
				</td>
			</tr>
		</table>
	<Div  id= "divLAAgent1" style= "display: ''">
		<table class="common" align=center>
			<TR  class= common> 
				<TD  class= title>
					管理机构
				</TD>
				<TD  class= input>
					<Input class="codeno" name=ManageCom verify="管理机构|notnull"
					ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,'1',1);" 
					onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,'1',1);"
					><Input class=codename name=ManageComName readOnly elementtype=nacessary  > 
				</TD> 
				<td></td>
				<td></td>
		</TR>
	</table>
</Div>
	 <table>
	 	<tr>
	 		<td class=common>
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
 			<td class= titleImg>
	 				职级考核标准录入
 			</td>
	 	</tr>
	 </table>
 <Div  id= "divLAAgent2" style= "display: ''">
 	<table  class= common>
 		<tr  class= common>
 			<td text-align: left colSpan=1>
 				<span id="spanActiveBaseWagesGrid" >
 				</span>
 			</td>
 		</tr>
 	</table>
 </div>
<Div  id= "divActiveBaseWagesStand" style= "display: 'none'">
<hr/>
 	 <table>
	 	<tr>
	 		<td >
	 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAssessone);">
	 			<td class= titleImg>
		 				各个职级的底薪标准
	 			</td>
	 	</tr>
	 </table>
	 
	 <Div  id= "divAssessone" style= "display: ''">
		 <table border="2" cellpadding="2" cellspacing="0" align="center">
			<tr class=common>
			  <th rowspan="2">职级</th>
			  <th colspan="3">底薪（元/月）</th>
			</tr>
			<tr class=common align="center">
			  <td>一类公司</td>
			  <td>二类公司</td>
			  <td>三类公司</td>
			</tr>
	 		<tr class= common align="center">
				<td>资深互动业务经理三级</td>
				<td>6500</td>
				<td>5500</td>
				<td>4200</td>
			</tr>
			<tr class= common align="center">
				<td>资深互动业务经理二级</td>
				<td>5600</td>
				<td>4500</td>
				<td>3360</td>
			</tr>
			<tr class= common align="center">
				<td>资深互动业务经理一级</td>
				<td>4500</td>
				<td>3600</td>
				<td>2700</td>
			</tr>
			<tr class= common align="center">
				<td>高级互动业务经理三级</td>
				<td>3500</td>
				<td>2800</td>
				<td>2100</td>
			</tr>
			<tr class= common align="center">
				<td>高级互动业务经理二级</td>
				<td>2800</td>
				<td>2250</td>
				<td>1680 </td>
			</tr>
			<tr class= common align="center">
				<td>高级互动业务经理一级</td>
				<td>2300</td>
				<td>1850</td>
				<td>1380</td>
			</tr>
			<tr class= common align="center">
				<td>互动业务经理三级</td>
				<td>1800</td>
				<td>1450</td>
				<td>1100</td>
			</tr>
			<tr class= common align="center">
				<td>互动业务经理二级</td>
				<td>1300</td>
				<td>1050</td>
				<td>800   </td>
			</tr>
			<tr class= common align="center">
				<td>互动业务经理一级</td>
				<td>1000</td>
				<td>800</td>
				<td>600</td>
			</tr>
	 	</table>
 	</div>
 </Div>
 <Input type=hidden name=BranchType  value =<%=BranchType %>>   
 <Input type=hidden name=BranchType2 value =<%=BranchType2 %>>    
 <Input type=hidden name=hideOperate >   
 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>


