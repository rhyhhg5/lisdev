<%
//程序名称：LAAgentPromRadixQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAAgentPromRadixQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAgentPromRadixQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <title>考核标准查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./LAAgentPromRadixQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgentPromRadix1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAAgentPromRadix1" style= "display: ''">
     <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  代理人职级 
		</td>
        <td  class= input> 
		  <input class="code" name=AgentGrade ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
		                                      onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');"> 
		</td>
        <td  class= title> 
		  地区类型 
		</td>
        <td  class= input> 
		  <input class='code' name=AreaType ondblclick="return showCodeList('areatype',[this]);" 
		                                     onkeyup="return showCodeListKey('areatype',[this]);"> 
		</td>
       <td class=title>
           考核类型
       </td>
       <td class=input> 
          <input class=code name="AssessType" verify="考核类型|notnull&code:AssessType1" CodeData="0|2^01|维持^02|晋升"
                                                ondblclick="return showCodeListEx('AssessType1',[this]);" 
                                                onkeyup="return showCodeListKeyEx('AssessType1',[this]);">
          </td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  目标职级 
		</td>
        <td  class= input> 
		  <input class="code" name=DestAgentGrade ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlDestAgentGrade%>','1');" 
		                                      onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlDestAgentGrade%>','1');"> 
		</td>
         <td  class= title> 
		  考核期限 
		</td>
        <td  class= input> 
		  <input class='code' name=LimitPeriod CodeData="0|3^01|三个月^02|六个月^03|九个月^04|十二个月
		                       ondblclick="return showCodeList('LimitPeriod',[this],[0,1],null,tSqlLimitPeriod,'1');" 
		                       onkeyup="return showCodeListKey('LimitPeriod',[this],[0,1],null,tSqlLimitPeriod,'1');"> 
		</td>      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		   个人累计客户数
		</td>
        <td  class= input> 
		  <input class=common name=IndCustSum > 
		</td>
        <td  class= title> 
		  个人累计FYC
		</td>
        <td  class= input> 
		  <input name=IndFYCSum class=common > 
		</td>      
        <td  class= title> 
		   月均客户数
		</td>
        <td  class= input> 
		  <input class=common name=MonAvgCust > 
		</td>
      </tr>      
      <tr  class= common> 
        <td  class= title> 
		  月均FYC
		</td>
        <td  class= input> 
		  <input name=MonAvgFYC class=common > 
		</td>     
        <td  class= title> 
		   个人继续率
		</td>
        <td  class= input> 
		  <input class=common name=IndRate > 
		</td>
        <td  class= title> 
		  直接增员数
		</td>
        <td  class= input> 
		  <input name=DirAddCount class=common > 
		</td>      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  增员人数
		</td>
        <td  class= input> 
		  <input name=AddCount class=common > 
		</td>    
        <td  class= title> 
		   增员人中含理财主任数
		</td>
        <td  class= input> 
		  <input class=common name=ReachGradeCount > 
		</td>  
        <td  class= title> 
		   本人及所增人员合计FYC
		</td>
        <td  class= input> 
		  <input class=common name=IndAddFYCSum > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  组平均继续率
		</td>
        <td  class= input> 
		  <input name=TeamAvgRate class=common > 
		</td>      
        <td  class= title> 
		 直辖人数
		</td>
        <td  class= input> 
		  <input class=common name=DirMngCount > 
		</td>   
		
        <td  class= title> 		   
                直辖人中理财主任数
		</td>  
        <td  class= input> 
		  <input name=DirMngFinaCount class=common > 
		</td> 
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  直辖组累计FYC
		</td>
        <td  class= input> 
		  <input class=common name=TeamFYCSum > 
		</td>  
        <td  class= title> 
		   直接育成营业组数
		</td>
        <td  class= input> 
		  <input class=common name=DRTeamCount > 
		</td>  
        <td  class= title> 
		   直辖及育成组平均继续率
		</td>
        <td  class= input> 
		  <input class=common name=DRTeamAvgRate > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  直辖及育成组累计FYC
		</td>
        <td  class= input> 
		  <input name=DRFYCSum class=common > 
		</td>      
        <td  class= title> 
		   合计育成组数
		</td>
        <td  class= input> 
		  <input class=common name=RearTeamSum > 
		</td>
        <td  class= title> 
		  直辖及育成组月均有效人力
		</td>
        <td  class= input> 
		  <input name=DRMonLabor class=common > 
		</td>      
      </tr>
      
      <tr  class= common> 
        <td  class= title> 
		 有效人力的FYC标准
		</td>
        <td  class= input> 
		  <input class=common name=LaborFYCStand > 
		</td>
        <td  class= title> 
		  直辖部累计FYC
		</td>
        <td  class= input> 
		  <input name=DepFYCSum class=common > 
		</td>      
        <td  class= title> 
		   部门平均继续率
		</td>
        <td  class= input> 
		  <input class=common name=DepAvgRate > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  直接育成营销服务部数
		</td>
        <td  class= input> 
		  <input name=RearDepCount class=common > 
		</td>      
        <td  class= title> 
		   直辖部及育成部有效人力
		</td>
        <td  class= input> 
		  <input class=common name=DRDepLabor > 
		</td>
        <td  class= title> 
		  直辖部及育成部平均继续率
		</td>
        <td  class= input> 
		  <input name=DRDepAvgRate class=common > 
		</td>      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		   育成营销服务部数
		</td>
        <td  class= input> 
		  <input class=common name=RearDepSum > 
		</td>
        <td  class= title> 
		  直辖部月均FYC
		</td>
        <td  class= input> 
		  <input name=DirDepMonAvgFYC class=common > 
		</td>      
        <td  class= title> 
		   所辖营销服务部理财主任数
		</td>
        <td  class= input> 
		  <input class=common name=DepFinaCount > 
		</td>
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  所辖营业单位平均继续率
		</td>
        <td  class= input> 
		  <input name=AreaRate class=common > 
		</td>      
        <td  class= title> 
		   直接育成督导长数
		</td>
        <td  class= input> 
		  <input class=common name=DirRearAdmCount > 
		</td>
        <td  class= title> 
		  所辖支公司理财主任数
		</td>
        <td  class= input> 
		  <input name=MngAreaFinaCount class=common > 
		</td>      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		   育成督导长数
		</td>
        <td  class= input> 
		  <input class=common name=RearAdmCount > 
		</td>
        <td  class= title> 
		  最近12个月直接育成部数
		</td>
        <td  class= input> 
		  <input name=ReqDepCount class=common > 
		</td>      
      </tr>
      
    </table>
          <input type=hidden name=BranchType value=''>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 						
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 展业机构结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLAAgentPromRadixGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLAAgentPromRadixGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 				
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
