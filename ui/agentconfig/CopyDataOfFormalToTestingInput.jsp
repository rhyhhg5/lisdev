<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：CopyDataOfFormalToTestingInput.jsp
//程序功能：把正式库数据导入到外测，用来测试外测一些大功能，像：薪资计算，考核等，需要大量的数据时
//创建日期：2017/4/28 15:26
//创建人  ：yangyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="CopyDataOfFormalToTestingInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="CopyDataOfFormalToTestingInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="./CopyDataOfFormalToTestingSave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<TR class=common>
				<TD class=title>
					主表名
				</TD>
				<TD class=input>
					<input  name=TableName verify="主表名|notnull" elementtype=nacessary>
				</TD>
				<td>
				</td>
				<td>
				</td>
<!-- 				<TD class=title> -->
<!-- 					对应的schema -->
<!-- 				</TD> -->
<!-- 				<TD class=input> -->
<!-- 					<input  name=Schema verify="对应的schema|notnull" elementtype=nacessary> -->
<!-- 				</TD> -->
			</TR>
			<TR class=common>
				<TD class=title>
					查询条件
				</TD>
				<TD class=input colspan='3'>
					<input  name=WhereSql style="width: 98%" verify="查询条件|notnull" elementtype=nacessary>
				</TD>
			</TR>
		</table>
		<table class=common>
			<TR class=common>	
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="导  数" name=Save TYPE=button onclick="return CopyData();">
				</td>
				<td class=button width="10%">
					<INPUT class=cssButton VALUE="重  置" name=reset TYPE=button onclick="return initForm();">
				</td>
				<td>
				</td>
				<td>
				</td>
			</tr>
		</table>
		<br>
		<font color="red">注：1、从正式查询数据然后插入到外测，如果有主键冲突的，请先删除！<br>
		&nbsp;&nbsp;&nbsp;&nbsp;2、表名一定要写schema中的，且大小写可以对得上。比如：LACommisionSchema需要写LACommision.<br>
		&nbsp;&nbsp;&nbsp;&nbsp;3、查询条件只写where后面的.<br>
		&nbsp;&nbsp;&nbsp;&nbsp;4、举例：我要把正式lacommision表中的commisionsn='0167234870'，则表名写：LACommision，查询条件写：commisionsn='0167234870'		
		</font>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>


