<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LAASSESSHistoryOfStateUpdateInput.jsp
	//程序功能：
	//创建日期：2016-05-20
	//创建人  ：zyy程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
%>

<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LAASSESSHistoryOfStateUpdateInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
</head>
<body onload="initElementtype();">
<form action="./LAASSESSHistoryOfStateSave.jsp" method=post name=fm target="fraSubmit">
<span id="operateButton">
	<table class="common" align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="查询"  name=Query TYPE=button onclick="return queryClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="修改" name=Modify TYPE=button onclick="return updateClick();">
			</td>
		</tr>
	</table>
</span>
<table>
	<tr class=common>
		<td class=titleImg>查询条件</td>
	</tr>
</table>
<Div id="divLABranchGroup1" style="display: ''">
<table class=common>
	<TR class=common>
		<TD class=title>管理机构</TD>
		<TD class=input><Input class='codeno' name=ManageCom
			verify="管理机构|notnull"  
			readonly ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
			</TD>			
		<TD class=title>考核月</TD>
		<TD class=input><Input class=common name=WageNo verify="考核月|notnull" readonly  elementtype=nacessary onchange="return getBranch();">
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>计算状态</TD>
		<TD class=input><Input class=common name=State verify="计算状态|notnull&len=2"  elementtype=nacessary onchange="return getBranch();">
		</TD>
		<TD class=title>展业类型</TD>
		<TD class=input><Input class='codeno' name=BranchType
		    verify="展业类型|notnull"
			readonly><Input class=codename name=BranchTypeName readonly  elementtype=nacessary>
	   </TD>
	</TR>
	<TR class=common>
		<TD class=title>销售渠道</TD>
		<TD class=input><Input class='codeno' name=BranchType2
		    verify="销售渠道|notnull"
			readonly><Input class=codename name=BranchType2Name readonly elementtype=nacessary>
	   </TD>
	</TR>
</table>
</Div>

<table class='common'>
	<p><font color="red">注:考核计算状态有两种，分别是00（考核计算结束）、01（正在考核计算中），此功能是针对考核状态是计算中的维护功能</font></p>
</table>
<input type=hidden name=hideOperate value=''>  
<input type=hidden name=AgentGroup value=''> 
<input type=hidden name=FoundDate value=''>
<input type=hidden name=ReturnFlag value=''>
<input type=hidden name=BranchManager value=''>
<span  id="spanCode" style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
