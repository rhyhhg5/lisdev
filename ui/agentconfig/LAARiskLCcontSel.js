//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

var querySQL;
var qyeryFlag = false;
var mStartDate;
var mEndDate;
var tContNo;
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
 

function easyQueryClick()
{	 
    //首先检验录入框
  if(!verifyInput()) return false;
  var yearWhere = " and 1=1 ";
  var yearAndMonth = fm.all('WageNo').value;
  if(""!=yearAndMonth&&null!=yearAndMonth)
  {   
	
	  if(yearAndMonth.length!=6)
	  {
		  alert("统计年份输入有误，请重新输入");
		  return false;
	  }
	  var year =yearAndMonth.substr(0,4);
	  var month = yearAndMonth.substr(4,6);
	  if(parseInt(year,10)<2000)
	  {
		  alert("统计年份输入有误，请重新输入");
		  return false;
	  }
	  if(parseInt(month,10)<1||parseInt(month,10)>12 )
	  {
		  alert("统计年份输入有误，请重新输入");
		  return false;
	  }
	  yearWhere+= "  and year(a.tmakedate)	='"+year+"' and month(a.tmakedate)	='"+month+"'";
  }
     mStartDate = fm.all("StartDate").value;
     mEndDate = fm.all("EndDate").value;
     tContNo = fm.all("ContNo").value;
    

     

  
     if(((mStartDate != null && trim(mStartDate) != "")&&(mEndDate != null && trim(mEndDate) != ""))&&("" != trim(yearAndMonth) && null != yearAndMonth)){
	  alert("统计年月与起止日期只能录入1个");
	  return false;
  }
  
  querySQL= "select (case when a.grpcontno='00000000000000000000' then a.contno else a.grpcontno end ),a.managecom,a.agentcom,(select name from lacom where agentcom = b.agentcom), " 
  			+"b.p11 ,a.riskcode,(select riskname from lmrisk where lmrisk.riskcode=a.riskcode),b.payyears,b.TransMoney, "
  			//首期手续费
  			+"coalesce((select FirConCharge from lachargefee where commisionsn = a.commisionsn and exists(select 'x' from lacommision where commisionsn = a.commisionsn and payyear = 0 and ReNewCount >= 0)),0)  ,"
  			//首期手续费率
  			+"coalesce((select FirConChargeRate from lachargefee where commisionsn = a.commisionsn and exists(select 'x' from lacommision where commisionsn = a.commisionsn and payyear = 0 and ReNewCount >= 0)),0) ,"
  			//保单年度

  			+" b.payyear+1 ,"
  			//续年手续费
  			+"coalesce((select FirConCharge from lachargefee where commisionsn = a.commisionsn and exists(select 'x' from lacommision where commisionsn = a.commisionsn and payyear >0 and ReNewCount = 0)),0),"
  			//续年手续费比率
  			+"coalesce((select FirConChargeRate from lachargefee where commisionsn = a.commisionsn and exists(select 'x' from lacommision where commisionsn = a.commisionsn and payyear >0 and ReNewCount = 0)),0),"
  			//继续率奖金
  			+"coalesce((select ContCharge from lachargefee where commisionsn = a.commisionsn ),0),"
  			//月度奖
  			+"coalesce((select MonCharge from lachargefee where commisionsn = a.commisionsn ),0),"
  			//年度奖
  			+"coalesce((select YearCharge from lachargefee where commisionsn = a.commisionsn ),0),"
  			//缴费次数    签单日期
  			+"a.PayCount, b.signdate ,"
  			//回执加销日期
  			+"(select getpoldate from lccont where contno=b.contno),"//+"b.getpoldate,"
  			//交单日期
  			+"b.makepoldate,"
  			//专员代码，专员姓名，人员状态
  			+"c.groupagentcode,c.name,c.agentstate"
  			//回访日期
  			+",(select completetime from db2inst1.returnvisittable where policyno=a.contno and returnvisitflag in ('1','4')  fetch first 1 rows only ) ff"
  			//结算状态
  			+",(case when a.chargestate='0' then '未结算' when a.chargestate = '1' then '已结算'  end) "
  			+",(case when a.chargestate = '0'  then '' when  a.chargestate = '1' then a.commisionsn else '' end)"
  			+" from LACharge a, lacommision b,laagent c where a.chargetype='15' and a.commisionsn = b.commisionsn and b.agentcode = c.agentcode  "
             + getWherePart('a.ManageCom', 'ManageCom','like')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('a.RiskCode','RiskCode')
             + getWherePart('a.AgentCom','AgentCom')
             + getWherePart('c.GroupAgentCode','GroupAgentCode')
     if(""!=tContNo)
  			{
    	 querySQL+=" and ( a.grpcontno ='"+tContNo+"' or a.contno = '"+tContNo+"')";
  			}        
        
  if((mStartDate != null && trim(mStartDate) !="")||(mEndDate != null && trim(mEndDate) !="")){
        	 querySQL += " and a.tmakedate>= '"+fm.all('StartDate').value+"' "
        		+" and a.tmakedate<='"+fm.all('EndDate').value+"' "
         }
  if(""!=yearAndMonth&&null!=yearAndMonth){
 	 querySQL += yearWhere;
  }
  
	turnPage.queryModal(querySQL, LACommisionGrid);   
	qyeryFlag  = true;
	
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("财务结算起期必须小于等于财务结算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#1# and BranchType2=#02# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}
//页面下载
function doDownLoad(){
	if(!verifyInput()) return false;   
	if(false==qyeryFlag)
	{
		alert("请先查询！");
		return false;
	}
    fm.action = "./LAARiskLCcontDown.jsp";
    fm.all("querySQL").value = querySQL;
    
    fm.submit();
}


