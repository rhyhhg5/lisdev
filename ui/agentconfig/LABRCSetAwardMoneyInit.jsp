<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2006-08-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
 var  mSQL=" 1 and  char(length(trim(comcode)))  >= #8# and sign=#1#" ;
 var triskcode="1 and code not in (select riskcode from lmriskapp where risktype4=#4#) ";
  </SCRIPT>       


<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.all('ManageCom').value='';
  	fm.all('ManageComName').value='';
  	fm.all('AgentCode').value='';
  	fm.all('CodeName').value = '';
  	fm.all('WageNo').value = '';
  //	fm.all('money').value='';
 //   fm.all('RiskCode').value='';
 //   fm.all('RiskCodename').value=''; 
//s    fm.all('CurYear').value=''; 
//    fm.all('payyears').value='';
//    fm.all('payyearsName').value ='';	
//    fm.all('AgentCom').value='';
//    fm.all('Name').value='';
//    fm.all('Rate').value='';
//    fm.diskimporttype.value='LARateCommision';
//    fm2.diskimporttype.value='LARateCommision';    
    fm.all('BranchType').value=getBranchType();     
          
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
        
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许    
//    iArray[1][4]="ComCode";
//    iArray[1][5]="1|2";
//    iArray[1][6]="0|1";
    iArray[1][7]="chkManagecom";
    iArray[1][9]="管理机构|NotNull&NUM&len=8";  
//    iArray[1][15]="1";
//    iArray[1][16]=mSQL; 
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许        

    iArray[3]=new Array();
    iArray[3][0]="业务员代码"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[3][7]="getAgentName";    
 
//    iArray[6][15]="1";
 //   iArray[6][16]=mSQL; 
    
       
    iArray[4]=new Array();
    iArray[4][0]="业务员名称"; //列名
    iArray[4][1]="120px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许
        
    iArray[5]=new Array();
    iArray[5][0]="薪资月"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;          //列最大值
    iArray[5][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[5][9]="薪资月|NotNull&NUM&len=6";
    
    iArray[6]=new Array();
    iArray[6][0]="奖励方案奖金"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;          //列最大值
    iArray[6][3]=1;  

    iArray[7]=new Array();
    iArray[7][0]="idx";            //列名 idx
    iArray[7][1]="0px";          //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0; 
   
  
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
    SetGrid.displayTitle = 1;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initImportResultGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	  iArray[1][0]="导入文件批次";          		//列名
	  iArray[1][1]="120px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="所在行"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="日志信息"; //列名
    iArray[3][1]="160px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
      
    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
    ImportResultGrid.canChk = 0;
		//SetGrid.mulLineCount = 10;
    //ImportResultGrid.displayTitle = 1;
    ImportResultGrid.hiddenSubtraction =1;
    ImportResultGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    ImportResultGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    initImportResultGrid();
  }
  catch(re)
  {
    alert("在LaratecommisionSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>