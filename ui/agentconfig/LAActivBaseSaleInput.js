//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

//提交，保存按钮对应操作
function DoInsert()
{
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

// 查询按钮
function easyQueryClick()
{
	var tBranchType= fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
	// 初始化表格
	if(!verifyInput())
	{
	 return false;
	}
	initSetGrid();

	var xSQL = " and branchtype='"+tBranchType+"'and branchtype2='"+tBranchType2+"' ";
	var strSQL = "";
	strSQL = "select a.managecom,(select Name from ldcom where comcode=a.managecom),a.agentgrade," 
			+"(select gradename from laagentgrade where gradecode=a.agentgrade),a.rewardmoney,a.idx from lawageradixall a " 
			+" where agenttype = '2' and wagecode = 'HD0001' " 
			+ getWherePart( 'a.managecom','ManageCom','like')
			+ getWherePart( 'a.agentgrade','AgentGrade')
			+ xSQL
			;
	

					//查询SQL，返回结果字符串
					turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

					//判断是否查询成功
					if (!turnPage.strQueryResult) {
						alert("没有符合条件的数据，请重新录入查询条件！");
						return false;
					}

					//查询成功则拆分字符串，返回二维数组
					  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
					  turnPage.arrDataCacheSet = arrDataSet;

					//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
					//设置初始化过的MULTILINE对象
					turnPage.pageDisplayGrid = SetGrid;
					//保存SQL语句
					turnPage.strQuerySql = strSQL;

					//设置查询起始位置
					turnPage.pageIndex = 0;

					//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
					tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

					//调用MULTILINE对象显示查询结果
					displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
				}


				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}

	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset(){
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
	}




	//显示frmSubmit框架，用来调试
	function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}


		//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
{
	if (confirm("您确实想修改该记录吗?"))
	{
		fm.fmAction.value = "UPDATE";
		if(!beforeSubmit()) return false;
		var i = 0;
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
	}
	else
		{
			fm.OperateType.value = "";
			alert("您取消了修改操作！");
		}
	}

							//判断是否选择了要增加、修改或删除的行
function chkMulLine()
   {
		var i;
		var selFlag = true;
		var iCount = 0;
		var rowNum = SetGrid.mulLineCount;
		for(i=0;i<rowNum;i++){
			if(SetGrid.getChkNo(i)){
				iCount++;
				if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
					{
						alert("管理机构不能为空");
						SetGrid.setFocus(i,1,SetGrid);
						selFlag = false;
							break;
					}
				if((SetGrid.getRowColData(i,3) == null)||(SetGrid.getRowColData(i,3) == ""))
					{
						alert("职级代码不能为空");
						SetGrid.setFocus(i,1,SetGrid);
						selFlag = false;
						break;
					}
				if((SetGrid.getRowColData(i,5) == null)||(SetGrid.getRowColData(i,5) == ""))
					{
						alert("固定工资不能为空");
						SetGrid.setFocus(i,1,SetGrid);
						selFlag = false;
						break;
					}								     
				//如果Serialno为空不能删除和更新
				if((SetGrid.getRowColData(i,6) == null)||(SetGrid.getRowColData(i,6)==""))
					{
						if((fm.fmAction.value == "DELETE") || (fm.fmAction.value == "UPDATE"))
						{
							alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
							SetGrid.checkBoxAllNot();
							SetGrid.setFocus(i,1,SetGrid);
							selFlag = false;
							break;
						}
						}
				else if ((SetGrid.getRowColData(i,6) != null)||(SetGrid.getRowColData(i,6)!=""))
					{	//如果Serialno不为空不能插入
						if(fm.fmAction.value == "INSERT")
						{
							alert("此纪录已存在，不可插入！");
							SetGrid.checkBoxAllNot();
							SetGrid.setFocus(i,1,SetGrid);
							selFlag = false;
							break;
						}
					}
			}
			else
				{//不是选中的行
					if((SetGrid.getRowColData(i,6) == null)||(SetGrid.getRowColData(i,6)==""))
					{
						alert("有未保存的新增纪录！请先保存记录。");
						SetGrid.checkBoxAllNot();
						SetGrid.setFocus(i,1,SetGrid);
						selFlag = false;
						break;
					}
				}
			}
			if(!selFlag) return selFlag;
			if(iCount == 0)
			{
				var title="";
		    	if(fm.fmAction.value=="UPDATE")
		    	{
		    		title="修改";
		    	}
		    	if(fm.fmAction.value=="DELETE")
		    	{
		    		title="删除";
		    	}
		    	if(fm.fmAction.value=="INSERT")
		    	{
		    		title="新增";
		    	}
				alert("请选择要"+title+"的记录!");
				return false;
			}
			return true;
		}

									//提交前的校验、计算
function beforeSubmit(){								
	if(!SetGrid.checkValue("SetGrid")) return false;
	if(!chkMulLine()) return false;
	return true;
}

function showDiv(cDiv,cShow){
	if (cShow=="true"){
			cDiv.style.display="";
		}
		else{
			cDiv.style.display="none";
		}
}

									
function getAgentGrade(cObj,cName)
{
	var strsql =" 1 and BranchType=#5# and BranchType2=#01#" ;
	showCodeList('agentgrade',[cObj,cName],[0,1],null,strsql,1,1);
}
// muttline 实现双击触发函数
function getGradeName(a)
{
  var strsql =" 1 and BranchType=#5# and BranchType2=#01#";
  showCodeList('agentgrade',[fm.all(a).all('SetGrid3'),fm.all(a).all('SetGrid4')],[0,1],null,strsql,1,1);
}			

function chkManagecom(b){
	var strsql =" 1 and  char(length(trim(comcode)))= #8# " ;
	showCodeList('ComCode',[fm.all(b).all('SetGrid1'),fm.all(b).all('SetGrid2')],[0,1],null,strsql,1,1);
//	fm.all(b).all('SetGrid3').value ="";
//	fm.all(b).all('SetGrid4').value ="";
}

