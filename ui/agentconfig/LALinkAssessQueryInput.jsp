<%
//程序名称：LALinkAssessQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LALinkAssessQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LALinkAssessQueryInit.jsp"%>
  <title>衔接考核信息查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./LALinkAssessQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLALinkAssess1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLALinkAssess1" style= "display: ''">
     <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class='code' name=ManageCom ondblclick="return showCodeList('comcode',[this]);" 
		                                     onkeyup="return showCodeListKey('comcode',[this]);"> 
		</td>
        <td  class= title> 
		  代理人职级 
		</td>
        <td  class= input> 
		  <input class="code" name=AgentGrade ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,tSql,'1');" 
		                                      onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,tSql,'1');"> 
		</td>
      </tr>
      <tr  class= common> 
         <td  class= title> 
		  筹备期 
		</td>
        <td  class= input> 
		  <input class=common name=ArrangePeriod > 
		</td>
        <td  class= title> 
		  考核比率 
		</td>
        <td  class= input> 
		  <input name=AssessRate class=common > 
		</td>
      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  衔接止期 
		</td>
        <td  class= input> 
		  <input class='coolDatePicker' name=LinkEndDate dateFormat='short' > 
		</td>
	<td class=title>版本类型</td>
        <td class=input><input type=text class=code name="AreaType"
            CodeData="0|^11|旧版衔接资金指标|^12|新版衔接资金指标"
            ondblClick="showCodeListEx('AreaTypeList',[this],[0,1]);"
            onkeyup="showCodeListKeyEx('AreaTypeList',[this],[0,1]);">
         </td>
       </tr>
       <tr  class= common>
        <td  class= title> 
		  操作员
		</td>
        <td  class= input> 
		  <input name=Operator class='readonly'readonly > 
		</td>
      </tr>
     
      
    </table>
          <input type=hidden name=BranchType value=''>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 						
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 展业机构结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLALinkAssessGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLALinkAssessGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 				
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
