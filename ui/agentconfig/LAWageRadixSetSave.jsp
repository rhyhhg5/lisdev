<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LAWageRadixSetSave.jsp
//程序功能：
//创建日期：2003-10-16
//创建人  ：   zsj
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  String FlagStr = "";
  String Content = "";

  WageRadixSet tWageRadixSet = new WageRadixSet();
  VData tVData = new VData();

  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  System.out.println("ManagCom : " + tGI.ManageCom);
  System.out.println("Operator : " + tGI.Operator);
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LAWageRadixSet tSet = new LAWageRadixSet();
      System.out.println("--tAction--"+tAction);

      if ( tAction.trim().equals("save") )  {
        String tAreaType[] = request.getParameterValues("SetGrid1");
        String tBranchType[] = request.getParameterValues("SetGrid2");
        String tAgentGrade[] = request.getParameterValues("SetGrid3");
        String tWageCode[] = request.getParameterValues("SetGrid4");
        String tWageName[] = request.getParameterValues("SetGrid5");
        String tFycMin[] = request.getParameterValues("SetGrid6");
        String tFycMax[] = request.getParameterValues("SetGrid7");
        String tDrawRate[] = request.getParameterValues("SetGrid8");
        String tLimitPeriod[] = request.getParameterValues("SetGrid9");
        String tRewardMoney[] = request.getParameterValues("SetGrid10");
        String tDrawStart[] = request.getParameterValues("SetGrid11");
        String tDrawEnd[] = request.getParameterValues("SetGrid12");
        String tDrawRateOth[] = request.getParameterValues("SetGrid13");
        String tPremStand[] = request.getParameterValues("SetGrid14");
        String tRearType[] = request.getParameterValues("SetGrid15");

        int tCount = tAreaType.length;

        for (int i=0;i<tCount;i++ )
        {
          LAWageRadixSchema tSch = new LAWageRadixSchema();
          tSch.setAreaType(tAreaType[i]);
          tSch.setBranchType(tBranchType[i]);
          tSch.setAgentGrade(tAgentGrade[i]);
          tSch.setWageCode(tWageCode[i]);
          tSch.setWageName(tWageName[i]);
          tSch.setFYCMin(tFycMin[i]);
          tSch.setFYCMax(tFycMax[i]);
          tSch.setDrawEnd(tDrawEnd[i]);
          tSch.setDrawRate(tDrawRate[i]);
          tSch.setDrawRateOth(tDrawRateOth[i]);
          tSch.setDrawStart(tDrawStart[i]);
          tSch.setLimitPeriod(tLimitPeriod[i]);
          tSch.setPremStand(tPremStand[i]);
          tSch.setRewardMoney(tRewardMoney[i]);
          tSch.setRearType(tRearType[i]);

          tSet.add(tSch);
        }

        VData tInputData = new VData();
        tInputData.clear();
        tInputData.add(tGI);
        tInputData.add(0,tSet);

        if (!tWageRadixSet.getInputData(tInputData) )  {
          FlagStr = "Fail";
          Content = FlagStr + "保存失败，原因是：" + tWageRadixSet.mErrors.getFirstError();
        }

        if ( !tWageRadixSet.saveData() ) {
          FlagStr = "Fail";
          Content = FlagStr + "保存失败，原因是：" + tWageRadixSet.mErrors.getFirstError();
        }
        else  {
          FlagStr = "Succ";
          Content = FlagStr + "保存成功！";
        }
      }
      else
      {
        int tCount = 0;
        int tIndex = 0;
        LAWageRadixSet tSet1 = new LAWageRadixSet();

        String tChk[] = request.getParameterValues("InpSetGridChk");
        String tAreaType1[] = request.getParameterValues("SetGrid1");
        String tBranchType1[] = request.getParameterValues("SetGrid2");
        String tAgentGrade1[] = request.getParameterValues("SetGrid3");
        String tWageCode1[] = request.getParameterValues("SetGrid4");
        String tWageName1[] = request.getParameterValues("SetGrid5");
        String tFycMin1[] = request.getParameterValues("SetGrid6");
        String tFycMax1[] = request.getParameterValues("SetGrid7");
        String tDrawRate1[] = request.getParameterValues("SetGrid8");
        String tLimitPeriod1[] = request.getParameterValues("SetGrid9");
        String tRewardMoney1[] = request.getParameterValues("SetGrid10");
        String tDrawStart1[] = request.getParameterValues("SetGrid11");
        String tDrawEnd1[] = request.getParameterValues("SetGrid12");
        String tDrawRateOth1[] = request.getParameterValues("SetGrid13");
        String tPremStand1[] = request.getParameterValues("SetGrid14");
        String tRearType1[] = request.getParameterValues("SetGrid15");
        String tIdx1[] = request.getParameterValues("SetGrid16");
        
        tCount = tAreaType1.length;
        tSet1.clear();
        int tR = 0;
        for ( tIndex=0;tIndex<tCount;tIndex++ )
        {
          if (tChk[tIndex].equals("1") )
          {
            LAWageRadixSchema tSch1 = new LAWageRadixSchema();
            tSch1.setAgentGrade(tAgentGrade1[tIndex]);
            tSch1.setAreaType(tAreaType1[tIndex]);
            tSch1.setBranchType(tBranchType1[tIndex]);
            tSch1.setDrawEnd(tDrawEnd1[tIndex]);
            tSch1.setDrawRate(tDrawRate1[tIndex]);
            tSch1.setDrawRateOth(tDrawRateOth1[tIndex]);
            tSch1.setDrawStart(tDrawStart1[tIndex]);
            tSch1.setFYCMax(tFycMax1[tIndex]);
            tSch1.setFYCMin(tFycMin1[tIndex]);
            tSch1.setLimitPeriod(tLimitPeriod1[tIndex]);
            tSch1.setPremStand(tPremStand1[tIndex]);
            tSch1.setRewardMoney(tRewardMoney1[tIndex]);
            tSch1.setWageCode(tWageCode1[tIndex]);
            tSch1.setWageName(tWageName1[tIndex]);
            tSch1.setRearType(tRearType1[tIndex]);
            tSch1.setIdx(tIdx1[tIndex]);
            tSet1.add(tSch1);
            tR += 1;
          }
        }

        if ( tR == 0 )
        {
          FlagStr = "Fail";
          Content = FlagStr + "未选中要删除或修改的数据！";
        }
        else  {
         tVData.clear();
         tVData.add(tGI);
         tVData.add(tSet1);
         if (!tWageRadixSet.getInputData(tVData) ){
           FlagStr = "Fail";
           Content = FlagStr + "操作失败，原因是：" + tWageRadixSet.mErrors.getFirstError();
         }
         else
         {
           if ( tAction.trim().equals("update"))  {
            if ( !tWageRadixSet.updateData() ){
              FlagStr = "Fail";
              Content = FlagStr + "修改失败，原因是：" + tWageRadixSet.mErrors.getFirstError();
            }
            else  {
              FlagStr = "Succ";
              Content = FlagStr + "修改成功！";
            }
          }
          else  {
            if ( !tWageRadixSet.deleteData() ){
              FlagStr = "Fail";
              Content = FlagStr + "删除数据失败，原因是：" + tWageRadixSet.mErrors.getFirstError();
            }
            else  {
              FlagStr = "Succ";
              Content = FlagStr + "删除数据成功！";
           }
         }
       }
      }
     }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
  }// tGI!=null
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
