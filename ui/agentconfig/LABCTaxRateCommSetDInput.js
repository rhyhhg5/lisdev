//               该文件中包含客户端需要处理的函数和事件
//程序名称：LABComRateCommSetInput.js
//程序功能：
//创建时间：2008-01-24
//创建人  ：Huxl
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!beforeSubmit()) return false;
	if(!chkPrimaryKey()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./LABCTaxRateCommSetDSave.jsp";
	fm.submit(); //提交

}

//提交，保存按钮对应操作
function DoInsert()
{
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./LABCTaxRateCommSetDSave.jsp";
	fm.submit(); //提交

}

function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./LABCTaxRateCommSetDSave.jsp";
	fm.submit(); //提交
}

// 查询按钮
function easyQueryClick()
{
	//alert ("start easyQueryClick");
	// 初始化表格
	if(!verifyInput())
	{
	 return false;
	}
	initSetGrid();
	var tAgentCom  = fm.AgentCom.value;
  var tRiskCode = fm.RiskCode.value;
	//if ( tRiskCode == null || tRiskCode == "" )
	//{
		//alert("请选择险种代码！");
		//return;
	//}

	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.riskcode,b.riskname,a.idx,a.managecom,(select Name from ldcom where comcode=a.managecom),a.agentcom,(select name from lacom where agentcom=a.agentcom and branchtype='3'),"
	+"a.f01,"
	+ "nvl(a.year,0),'保单年度',nvl(a.curyear,0),"
	+ " nvl(a.rate,0),a.f06,a.idx,a.riskcode,a.managecom,a.agentcom,a.f01,a.year,a.curyear,a.f06   "
	+ " from laratecommision a,lmriskapp b "
	+ " where a.branchtype='3' and a.VersionType = '99' "
	+" and a.riskcode = b.riskcode and b.risktype4='4' and b.taxoptimal='Y' "
	+ getWherePart( 'a.riskcode','RiskCode')
	+ getWherePart( 'a.agentcom','AgentCom')
	+ getWherePart( 'a.managecom','ManageCom','like')
	+ getWherePart( 'a.f01','F01Code')
	+ getWherePart('a.f06','F06Code');
	//+ getWherePart( 'a.year','Year')
	//+ getWherePart( 'a.curyear','CurYear')
	//+ getWherePart( 'a.rate','Rate');
	if(fm.Rate.value!=null&&fm.Rate.value!='')
	{
	 strSQL += " and a.rate="+fm.Rate.value+" ";
	}
  if(fm.CurYear.value!=null&&fm.CurYear.value!='')
  {
   strSQL += " and a.curyear="+fm.CurYear.value+" ";
  }
  if(fm.payyears.value!=null&&fm.payyears.value!='')
  {
   strSQL += " and a.year="+fm.payyears.value+" ";
  }

	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}

	//查询成功则拆分字符串，返回二维数组
	  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  turnPage.arrDataCacheSet = arrDataSet;

	//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = SetGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;

	//设置查询起始位置
	turnPage.pageIndex = 0;

	//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

	//调用MULTILINE对象显示查询结果
	displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("初始化页面错误，重置出错");
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}
		//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
{   
	if(!chkPrimaryKey()) return false;
	if (confirm("您确实想修改该记录吗?"))
	{
		fm.fmAction.value = "UPDATE";
		if(!beforeSubmit()) return false;
		var i = 0;
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
	}
	Else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}

function chkPrimaryKey()
{
	var rowNum =  SetGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if( SetGrid.getChkNo(i))
		{   
			if(SetGrid.getRowColData(i,1)!=SetGrid.getRowColData(i,15))
			{
				alert("不能修改险种编码！");
				SetGrid.setRowColData(i,1,SetGrid.getRowColData(i,15));
				return false;
			}
			if(SetGrid.getRowColData(i,4)!=SetGrid.getRowColData(i,16))
			{
				alert("不能修改管理机构编码！");
				SetGrid.setRowColData(i,4,SetGrid.getRowColData(i,16));
				return false;
			}
			if(SetGrid.getRowColData(i,6)!=SetGrid.getRowColData(i,17))
			{
				alert("不能修改代理机构编码！");
				SetGrid.setRowColData(i,6,SetGrid.getRowColData(i,17));
				return false;
			}
			if(SetGrid.getRowColData(i,8)!=SetGrid.getRowColData(i,18))
			{
				alert("不能修改保费类型！");
				SetGrid.setRowColData(i,8,SetGrid.getRowColData(i,18));
				return false;
			}
			if(SetGrid.getRowColData(i,9)!=SetGrid.getRowColData(i,19))
			{
				alert("不能修改保险期间！");
				SetGrid.setRowColData(i,9,SetGrid.getRowColData(i,19));
				return false;
			}
			if(SetGrid.getRowColData(i,11)!=SetGrid.getRowColData(i,20))
			{
				alert("不能修改保单年度！");
				SetGrid.setRowColData(i,11,SetGrid.getRowColData(i,20));
				return false;
			}
			if(SetGrid.getRowColData(i,13)!=SetGrid.getRowColData(i,21))
			{    
				alert("不能修改出单方式！");
				SetGrid.setRowColData(i,13,SetGrid.getRowColData(i,21));
				return false;
			}
		
		}
	}
	return true;
}

//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{
	//alert("enter chkmulline");
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = SetGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(SetGrid.getChkNo(i))
		{
			iCount++;
			if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
			{
				alert("险种代码不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,8) == null)||(SetGrid.getRowColData(i,8) == ""))
			{
				alert("保费类型不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
		  	}
			if((SetGrid.getRowColData(i,9) == null)||(SetGrid.getRowColData(i,9) == ""))
			{
				alert("保险期间不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
		  	}
			if((SetGrid.getRowColData(i,11) == null)||(SetGrid.getRowColData(i,11) == ""))
			{
				alert("保单年度不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
	
			if((SetGrid.getRowColData(i,12) == null)||(SetGrid.getRowColData(i,12) == ""))
			{
				alert("手续费比率不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			
			if((SetGrid.getRowColData(i,13) == null)||(SetGrid.getRowColData(i,13) == ""))
			{
				alert("出单方式不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			/**
	        if((SetGrid.getRowColData(i,12) == null)||(SetGrid.getRowColData(i,12) == ""))
			{
				alert("手续费比率不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			*/
			 else if(SetGrid.getRowColData(i,12)<'0')
			{
								          alert("手续费比率不能为负数!");
								          SetGrid.setFocus(i,1,SetGrid);
										  selFlag = false;
										  break;
								       }
			 else if(SetGrid.getRowColData(i,12)<'0'||SetGrid.getRowColData(i,12)>'1')
			 {
				alert("手续费比率不在0到1之间!");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			  }
			//如果Serialno为空不能删除和更新
			if((SetGrid.getRowColData(i,3) == null)||(SetGrid.getRowColData(i,3)==""))
			{
				if((fm.fmAction.value == "DELETE") || (fm.fmAction.value == "UPDATE"))
				{
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
			else if ((SetGrid.getRowColData(i,3) !== null)||(SetGrid.getRowColData(i,3)!==""))
			{	//如果Serialno不为空不能插入
				if(fm.fmAction.value == "INSERT")
				{
					alert("此纪录已存在，不可插入！");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
		}
		else
		{//不是选中的行
			if((SetGrid.getRowColData(i,3) == null)||(SetGrid.getRowColData(i,3)==""))
			{
				alert("有未保存的新增纪录！请先保存记录。");
				SetGrid.checkBoxAllNot();
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
		}
	}
	if(!selFlag) return selFlag;
	if(iCount == 0)
	{
		alert("请选择要保存或删除的记录!");
		return false
	}
	return true;
}

//提交前的校验、计算
function beforeSubmit()
{										
	if(!SetGrid.checkValue("SetGrid")) return false;
	if(!chkMulLine()) return false;
	return true;
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";
	}
}

function getAgentCom(cObj,cName)
{
	if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
	  {
	  	alert("请先输入管理机构！");
	  	return false;
	showCodeList('agentcom',[cObj,cName],[0,1],null,'1 and BranchType=#3# and BranchType2 =#01# ',1,1);
	}
	else if (fm.all('ManageCom').value != null || trim(fm.all('ManageCom').value) != '') 
	{
	var strsql =" 1 and BranchType=#3# and BranchType2 =#01#  and managecom like #" + fm.all('ManageCom').value + "%# " ;
	showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,1,1);
	}

}		
function doDownLoad(){
	if(!verifyInput())
	{
	 return false;
	}
	//var tAgentCom  = fm.AgentCom.value;
  //var tRiskCode = fm.RiskCode.value;
	//if ( tRiskCode == null || tRiskCode == "" )
	//{
		//alert("请选择险种代码！");
		//return;
	//}

	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.riskcode,b.riskname,a.managecom,(select Name from ldcom where comcode=a.managecom),a.agentcom,(select name from lacom where agentcom=a.agentcom and branchtype='3'),"
	+"case a.f01 when '03' then '追加保费' else '基本保费' end,"
	+ "(select codename from ldcode where code=char(a.year) and codetype='payyear'),nvl(a.curyear,0),"
	+ " nvl(a.rate,0),case when a.f06='0' then '不区分' when a.f06='1' then '柜台和银保通出单' else '自助和网银出单' end  "
	+ " from laratecommision a,lmriskapp b "
	+ " where a.branchtype='3' and a.VersionType = '99'  and a.riskcode = b.riskcode and b.risktype4='4' and  b.taxoptimal='Y' and a.managecom like '"
	+fm.all('ManageCom').value+"%' "
	+ getWherePart( 'a.f01','F01Code')
	+ getWherePart( 'a.riskcode','RiskCode')
	+ getWherePart( 'a.agentcom','AgentCom')
	+ getWherePart('a.f06','F06Code');
	//+ getWherePart( 'a.year','Year')
	//+ getWherePart( 'a.curyear','CurYear')
	//+ getWherePart( 'a.rate','Rate');
	if(fm.Rate.value!=null&&fm.Rate.value!='')
	{
	 strSQL += " and a.rate="+fm.Rate.value+" ";
	}
  if(fm.CurYear.value!=null&&fm.CurYear.value!='')
  {
   strSQL += " and a.curyear="+fm.CurYear.value+" ";
  }
  if(fm.payyears.value!=null&&fm.payyears.value!='')
  {
   strSQL += " and a.year="+fm.payyears.value+" ";
  }
  fm.all('querySQL').value=strSQL;
  fm.action = "./LABComWNCRReportTS.jsp";
  fm.submit();
}
