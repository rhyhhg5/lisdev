<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAWageRadixSetInput.jsp
//程序功能：佣金基数表设置录入界面
//创建时间：2003-10-16
//更新记录：更新人   更新时间   更新原因

%>
<%@page contentType="text/html;charset=GBK" %>
<head>
<!--<meta http-equiv="Content-Type" content="text/html; charset=GBK">-->
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>

   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

   <SCRIPT src="LAWageRadixSetInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="LAWageRadixSetInit.jsp"%>  
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>

<body  onload="initForm();" >
 <form action="./LAWageRadixSetSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
    <tr class=common>
     <td class=title>地区类型</td>
     <td class= input>
     <Input class='code' name=AreaType verify="地区类型|code:AreaType" 
            ondblclick="return showCodeList('AreaType',[this]);" 
            onkeyup="return showCodeListKey('AreaType',[this]);">
     </td>       
     <td class=title>代理人职级</td>
     <td class=input><input type=text class=code name="AgentGrade"
     CodeData="0|^A01|理财服务专员|^A02|理财服务主任|^A03|见习业务经理|^A04|业务经理一级|^A05|业务经理二级|^A06|高级经理一级|^A07|高级经理二级|^A08|督导长|^A09|区域督导长"
     ondblClick="showCodeListEx('AgentGradeList',[this],[0,1]);"
     onkeyup="showCodeListKeyEx('AgentGradeList',[this],[0,1]);"></td>     
   </tr>
   <tr class=common>  
    <td class=title>展业类型</td>
    <td class= input>
     <Input class='code' name=BranchType verify="展业机构类型|code:BranchType" 
            ondblclick="return showCodeList('BranchType',[this]);" 
            onkeyup="return showCodeListKey('BranchType',[this]);">
    </td>
    <td class=title>奖金代码</td>
    <td class=input><input type=text class=code name="WageCode" ondblclick="qryWageCode();">
    </td>
   </tr>
   <tr class=common>      
     <td class=input colspan=4 align=center><input type=button value="查     询" class=common onclick="easyQueryClick();"></td>    
    </tr>      
   </table>
  </div>
  
     
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>佣金计算基数设置</td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
   <table class=common>
    <tr class=common>
     <td class=input colspan=4 align=center><input type=button value="增     加" class=common onclick="addOneRow();">
     </td>
    </tr>
   </table>      
      <INPUT VALUE=" 首页 "  TYPE="button" onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" onclick="turnPage.lastPage();">
  </div>  
  
  
   
  <table class=common>     
   
   <tr class=common>    
    <td class=input ><input type=button value="修     改" class=common onclick="DoUpdate();"></td>    
    <td class=input ><input type=button value="保     存" class=common onclick="DoSave();"></td>   
    <td class=input ><input type=button value="重     置" class=common onclick="DoReset();"></td>    
    <td class=input ><input type=button value="删     除" class=common onclick="DoDel();"></td>    
    <td class=input ></td>
   </tr> 
   
  </table>
   
  
  <input type=hidden id="sql_where" name="sql_where">
  <input type=hidden id="fmAction" name="fmAction">   

 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"> </span> 
</body>
</html>




