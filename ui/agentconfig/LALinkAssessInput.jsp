<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LALinkAssessInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LALinkAssessInit.jsp"%>

<title></title>
</head>
<body  onload="initForm();" >
  <form action="./LALinkAssessSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLALinkAssess1);">
    
    <td class=titleImg>
      衔接考核基础信息
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLALinkAssess1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class='code' name=ManageCom ondblclick="return showCodeList('comcode',[this]);" 
		                                     onkeyup="return showCodeListKey('comcode',[this]);"
		                                     verify = "管理机构|notnull&code:comcode"> 
		</td>
        <td  class= title> 
		  代理人职级 
		</td>
        <td  class= input> 
		  <input class="code" name=AgentGrade ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,tSql,'1');" 
		                                      onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,tSql,'1');"
		                                      verify = "代理人职级|notnull&code:AgentGrade"> 
		</td>
      </tr>
      <tr  class= common> 
         <td  class= title> 
		  筹备期 
		</td>
        <td  class= input> 
		  <input class=common name=ArrangePeriod verify = "筹备期|notnull&INT&value>=0"> 
		</td>
        <td  class= title> 
		  考核比率 
		</td>
        <td  class= input> 
		  <input name=AssessRate class=common verify = "考核比率|notnull&NUM"> 
		</td>
      
      </tr>
      <tr  class= common> 
        <td  class= title> 
		  衔接止期 
		</td>
        <td  class= input> 
		  <input class='coolDatePicker' name=LinkEndDate dateFormat='short' verify="衔接止期|notnull&Date"> 
		</td>
	<td class=title>
	         版本类型</td>
        <td class=input><input type=text class=code name="AreaType"
            CodeData="0|^11|旧版衔接资金指标|^12|新版衔接资金指标"
            ondblClick="showCodeListEx('AreaTypeList',[this],[0,1]);"
            onkeyup="showCodeListKeyEx('AreaTypeList',[this],[0,1]);"></td>
      </tr>
      <tr  class= common> 
      
        <td  class= title> 
		  操作员
		</td>
        <td  class= input> 
		  <input name=Operator class='readonly'readonly > 
		</td>      
      </tr>
      
    </table>
  </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=BranchType value='1'>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
