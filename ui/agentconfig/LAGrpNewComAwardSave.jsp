<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2009-01-23
//创建人  ：   miaoxz
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  LAActiveAgentTaxUI tLAActiveAgentTaxUI = new LAActiveAgentTaxUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LATaxSet tSet = new LATaxSet();
      LATaxSet tSetU = new LATaxSet();	//用于更新 latax
      LATaxSet tSetD = new LATaxSet();	//用于删除
      LATaxSet tSetI = new LATaxSet();    //用于插入
            
      String tChk[] = request.getParameterValues("InpSetGridChk"); 
      
      String tManageCom[] = request.getParameterValues("SetGrid1");
      String tRate[] = request.getParameterValues("SetGrid4");
      String tRate2[] =request.getParameterValues("SetGrid5");
      String tRate3[] =request.getParameterValues("SetGrid6");
      String tRate4[] =request.getParameterValues("SetGrid7");
      String tTaxType[] =request.getParameterValues("SetGrid3");
      
    
			//获取最大的ID号
//      ExeSQL tExe = new ExeSQL();
//      String tSql = "select int(max(idx)) from LAWageRadix5 order by 1 desc ";
//      String strIdx = "";
//      int tMaxIdx = 0;
//      strIdx = tExe.getOneValue(tSql);
//      if (strIdx == null || strIdx.trim().equals(""))
//      {
//         tMaxIdx = 0;
//     }
//     else
//      {
//          tMaxIdx = Integer.parseInt(strIdx);
          //System.out.println(tMaxIdx);
//      }
      //创建数据集
      for(int i=0;i<tChk.length;i++)
      {
      	if(tChk[i].equals("1"))
      	{      			
      		//创建一个新的Schema lawageradixall
      		LATaxSchema tSch = new LATaxSchema();
			tSch.setManageCom(tManageCom[i]);
			tSch.setCalRate1(tRate[i]);
			tSch.setTaxType("H1");
			tSch.setBaseMin(0);
			tSch.setBaseMax(10000);
			tSch.setTaxCode(1);
			tSet.add(tSch);
			LATaxSchema tSch2 = new LATaxSchema();
			tSch2.setManageCom(tManageCom[i]);
			tSch2.setCalRate1(tRate2[i]);
			tSch2.setTaxType("H1");
			tSch2.setBaseMin(10000);
			tSch2.setBaseMax(30000);
			tSch2.setTaxCode(2);
			tSet.add(tSch2);
			LATaxSchema tSch3 = new LATaxSchema();
			tSch3.setManageCom(tManageCom[i]);
			tSch3.setCalRate1(tRate3[i]);
			tSch3.setTaxType("H1");
			tSch3.setBaseMin(30000);
			tSch3.setBaseMax(50000);
			tSch3.setTaxCode(3);
			tSet.add(tSch3);
			LATaxSchema tSch4 = new LATaxSchema();
			tSch4.setManageCom(tManageCom[i]);
			tSch4.setCalRate1(tRate4[i]);
			tSch4.setTaxType("H1");
			tSch4.setBaseMin(50000);
			tSch4.setBaseMax(999999);
			tSch4.setTaxCode(4);
			tSet.add(tSch4);
			
      		if((tTaxType[i] == null)||(tTaxType[i].equals("")))
      		{
      			//需要插入记录
      			tSetI.add(tSet);
      			tSet.clear();
       		}
      		else
      		{
            //需要删除
            if(tAction.trim().equals("DELETE"))      			      	
      			{
            	tSetD.add(tSet);
      			tSet.clear();
      			}
            //需要更新
            else if(tAction.trim().equals("UPDATE"))      			      	
      			{
            	tSetU.add(tSet);
      			tSet.clear();
      			}
      		 
       		}
      	}      	
      }
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      
      tVData.add(tGI);
      //没有更新或删除或插入的数据
      if ((tSetD.size() == 0)&&(tSetU.size() == 0)&&(tSetI.size() == 0))
      {
        	FlagStr = "Fail";
        	Content = FlagStr + "未选中要处理的数据！";      	
      }
    else if (tSetI.size() != 0)
    	{
        	//只有插入数据
        	tVData.add(tSetI);
        	System.out.println("Start tLAActiveAgentTaxUI Submit...INSERT");
        	tLAActiveAgentTaxUI.submitData(tVData,"INSERT");        	    	
    	}
    else if (tSetD.size() != 0)
    	{
        	//只有删除数据
        	tVData.add(tSetD);
        	System.out.println("Start tLAActiveAgentTaxUI Submit...DELETE");
        	tLAActiveAgentTaxUI.submitData(tVData,"DELETE");        	    	
    	}
    else if (tSetU.size() != 0)
    	{
        	//只有更新数据
        	tVData.add(tSetU);
        	System.out.println("Start tLAActiveAgentTaxUI Submit...UPDATE");
        	tLAActiveAgentTaxUI.submitData(tVData,"UPDATE");        	    	
    	}
    	

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAActiveAgentTaxUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
