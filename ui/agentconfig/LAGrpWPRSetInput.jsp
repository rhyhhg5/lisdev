<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LaratecommisionSetInput.jsp
//程序功能：佣金基数表设置录入界面
//创建时间：2009-07-22
//创建人  ：miaoxz

%>
<%@page contentType="text/html;charset=GBK" %>
<script language="JavaScript">
var StrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode and riskprop = #G#)";
var tsql=" 1 and char(length(trim(comcode))) in (#4#,#2#) ";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LAGrpWPRSetInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <SCRIPT src="LAGrpWPRSetInput.js"></SCRIPT>
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./LAGrpWPRSetSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
    <tr class=common>
    	
    	
    	 <TD  class= title>
            管理机构
   </TD>
   <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode|NOTNULL"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);"
          ><Input class=codename name=ManageComName readOnly elementtype=nacessary > 
   </TD> 
    	
     <TD  class= title>
       险种
    </TD>
    <TD  class= input>
       <Input class= 'codeno' name= RiskCode verify="险种|code:riskcode" 
       ondblclick="return showCodeList('RiskCode',[this,RiskCodename],[0,1],null,StrSql,1,1);"
       onkeyup="return showCodeList('RiskCode',[this,RiskCodename],[0,1],null,StrSql,1,1);"
      ><Input class=codename name=RiskCodename readOnly >
  <!--    <Input class=codename name=RiskCodename readOnly  elementtype=nacessary >  -->
    </TD>	
             
        
                			
   
   </tr>
   <tr class=common>
   		<TD  class= title>
           提奖方式
          </TD>
          <TD  class= input>
             <Input class="codeno" name=WageSort  CodeData="0|^0|无缴费年期|^1|分缴费年期" ondblclick="return showCodeListEx('wagesortlist',[this,WageSortName],[0,1]);" onkeyup="return showCodeListKeyEx('wagesortlist',[this,WageSortName],[0,1]);" onchange=""><input class=codename name=WageSortName  readonly=true ></TD>  
          </TD>
          <TD  class= title>
           折标方式
          </TD>
          <TD  class= input>
             <Input class="codeno" name=StandPremSort  CodeData="0|^0|无缴费年期|^1|分缴费年期" ondblclick="return showCodeListEx('standpremsortlist',[this,StandPremSortName],[0,1]);" onkeyup="return showCodeListKeyEx('standpremsortlist',[this,StandPremSortName],[0,1]);" onchange=""><input class=codename name=StandPremSortName  readonly=true ></TD>  
          </TD>
   </tr>
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
     		<input type=button value="修  改" class=cssButton onclick="return DoUpdate();">  
    		<input type=button value="删  除" class=cssButton onclick="return DoDelete();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 

    	</td>
   
    </tr>      
  </table>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>险种提奖折标方式配置信息</td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
   <!--table class=common>
    <tr class=common>
     <td class=input colspan=4 align=center><input type=button value="增     加" class=common onclick="addOneRow();">
     </td>
    </tr>
   </table-->      
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div> <br/> 
  <font color=red>注意：<br/>1．分公司设置的险种折标方式将无效，折标方式以总公司设置的为准．<br/>2．分公司可自行设置提奖方式．<br/>3．变化折标或提奖方式后，请及时更新相应的折标或提奖比例信息，系统会取最近的更新时间获取相应的比例参与计算。</font>
  <Input type=hidden id="BranchType" name=BranchType value=''>
  <input type=hidden id="sql_where" name="sql_where" >
  <input type=hidden id="fmAction" name="fmAction">   

 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




