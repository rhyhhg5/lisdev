//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：   luomin
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug = "0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//提交，保存按钮对应操作
function DoSave() {
	fm.fmAction.value = "UPDATE";
	if (!beforeSubmit())
		return false;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交

}

//提交，保存按钮对应操作
function DoInsert() {
	fm.fmAction.value = "INSERT";
	if (!beforeSubmit())
		return false;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交

}

function DoDel() {
	fm.fmAction.value = "DELETE";
	if (!beforeSubmit())
		return false;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

// 查询按钮
function easyQueryClick() {

	// 初始化表格
	if (!verifyInput()) {
		return false;
	}
	initSetGrid();
	//var tAgentCom  = fm.AgentCom.value;
	// var tRiskCode = fm.RiskCode.value;
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.riskcode,b.riskname,a.idx,a.managecom,(select Name from ldcom where comcode=a.managecom),"
			+ "a.agentcom,(select name from lacom where agentcom=a.agentcom and branchtype='3'),"
			+ "a.payintv,value(a.year,0),'保单年度',value(a.f03,0),value(a.f04,0),value(a.curyear,0),"
			+ " value(a.rate,0),a.f06"
			+ " from laratecommision a,lmriskapp b "
			+ " where a.branchtype='3' and  a.riskcode = b.riskcode and versiontype='11' and a.f05='L'  "
			+ " and b.risktype4<>'4'  "
			+ getWherePart('a.riskcode', 'RiskCode')
			+ getWherePart('a.rate', 'Rate', '', '1')
			+ getWherePart('a.year', 'payyears', '', '1')
			+ getWherePart('a.AgentCom', 'AgentCom')
			+ getWherePart('a.f03', 'F03Code')
			+ getWherePart('a.f04', 'F04Code')
			+ getWherePart('a.managecom', 'ManageCom', 'like')
			+ getWherePart('a.curyear', 'CurYear', '', '1')
			+ getWherePart('a.rate', 'Rate', '', '1');

	//查询SQL，返回结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}

	//查询成功则拆分字符串，返回二维数组
	arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
	turnPage.arrDataCacheSet = arrDataSet;

	//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = SetGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;

	//设置查询起始位置
	turnPage.pageIndex = 0;

	//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,
			MAXSCREENLINES);

	//调用MULTILINE对象显示查询结果
	displayMultiline(tArr, turnPage.pageDisplayGrid);
	//return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//showDiv(operateButton,"true");
		//showDiv(inputButton,"false");
		initForm();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset() {
	try {
		initForm();
	} catch (re) {
		alert("初始化页面错误，重置出错");
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,0,0,*";
	} else {
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

//Click事件，当点击“修改”图片时触发该函数
function DoUpdate() {
	if (confirm("您确实想修改该记录吗?")) {
		fm.fmAction.value = "UPDATE";
		if (!beforeSubmit())
			return false;
		var i = 0;
		var showStr = "正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ showStr;
		showInfo = window
				.showModelessDialog(urlStr, window,
						"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
	} else {
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}

//判断是否选择了要增加、修改或删除的行
function chkMulLine() {
	//alert("enter chkmulline");
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = SetGrid.mulLineCount;
	for (i = 0; i < rowNum; i++) {
		if (SetGrid.getChkNo(i)) {
			iCount++;
			if ((SetGrid.getRowColData(i, 1) == null)
					|| (SetGrid.getRowColData(i, 1) == "")) {
				alert("险种代码不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
			if ((SetGrid.getRowColData(i, 4) == null)
					|| (SetGrid.getRowColData(i, 4) == "")) {
				alert("管理机构不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
			if ((SetGrid.getRowColData(i, 6) == null)
					|| (SetGrid.getRowColData(i, 6) == "")) {
				alert("代理机构不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
			if ((SetGrid.getRowColData(i, 8) == null)
					|| (SetGrid.getRowColData(i, 8) == "")) {
				alert("缴费方式不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
			if ((SetGrid.getRowColData(i, 9) == null)
					|| (SetGrid.getRowColData(i, 9) == "")) {
				alert("保险期间不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
			if ((SetGrid.getRowColData(i, 13) == null)
					|| (SetGrid.getRowColData(i, 13) == "")) {
				alert("保单年度不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
			if (SetGrid.getRowColData(i, 13)-SetGrid.getRowColData(i, 9)>0)
			{
				alert("保单年度不能大于保险期间");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
			if ((SetGrid.getRowColData(i, 11) == null)
					|| (SetGrid.getRowColData(i, 11) == "")) {
				alert("最低缴费年期不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
			if ((SetGrid.getRowColData(i, 12) == null)
					|| (SetGrid.getRowColData(i, 12) == "")) {
				alert("缴费年期小于不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
			if ((SetGrid.getRowColData(i, 15) == null)
					|| (SetGrid.getRowColData(i, 15) == "")) {
				alert("执行起期不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}

			if ((SetGrid.getRowColData(i, 14) == null)
					|| (SetGrid.getRowColData(i, 14) == "")) {
				alert("佣金比率不能为空");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			} else if (SetGrid.getRowColData(i, 14) < '0') {
				alert("佣金比率不能为负数!");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			} else if (SetGrid.getRowColData(i, 14) < '0'
					|| SetGrid.getRowColData(i, 14) > '1') {
				alert("佣金比率不在0到1之间!");
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}

			//如果Serialno为空不能删除和更新
			if ((SetGrid.getRowColData(i, 3) == null)
					|| (SetGrid.getRowColData(i, 3) == "")) {
				if ((fm.fmAction.value == "DELETE")
						|| (fm.fmAction.value == "UPDATE")) {
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i, 1, SetGrid);
					selFlag = false;
					break;
				}
			} else if ((SetGrid.getRowColData(i, 3) !== null)
					|| (SetGrid.getRowColData(i, 3) !== "")) { //如果Serialno不为空不能插入
				if (fm.fmAction.value == "INSERT") {
					alert("此纪录已存在，不可插入！");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i, 1, SetGrid);
					selFlag = false;
					break;
				}
			}

		} else {//不是选中的行
			if ((SetGrid.getRowColData(i, 3) == null)
					|| (SetGrid.getRowColData(i, 3) == "")) {
				alert("有未保存的新增纪录！请先保存记录。");
				SetGrid.checkBoxAllNot();
				SetGrid.setFocus(i, 1, SetGrid);
				selFlag = false;
				break;
			}
		}
	}
	if (!selFlag)
		return selFlag;
	if (iCount == 0) {
		alert("请选择要保存或删除的记录!");
		return false
	}
	return true;
}

//--									//提交前的校验、计算
function beforeSubmit() {
	if (!SetGrid.checkValue("SetGrid"))
		return false;
	if (!chkMulLine())
		return false;
	return true;
}

function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}

function getAgentCom(cObj, cName) {
	if (fm.all('ManageCom').value == null
			|| trim(fm.all('ManageCom').value) == '') {
		showCodeList('agentcom', [ cObj, cName ], [ 0, 1 ], null,
				'1 and BranchType=#3# and BranchType2=#01# ', 1, 1);
	} else if (fm.all('ManageCom').value != null
			|| trim(fm.all('ManageCom').value) != '') {
		var strsql = " 1 and BranchType=#3# and BranchType2=#01#  and managecom like #"
				+ fm.all('ManageCom').value + "%# ";
		showCodeList('agentcom', [ cObj, cName ], [ 0, 1 ], null, strsql, 1, 1);
	}

}
// muttline 实现双击触发函数
function getComName(a) {
	var tManageCom = fm.all(a).all('SetGrid4').value;
	var strsql = " 1 and BranchType=#3# and BranchType2=#01#  and managecom like #"
			+ tManageCom + "%# ";
	showCodeList('agentcom', [ fm.all(a).all('SetGrid6'),
			fm.all(a).all('SetGrid7') ], [ 0, 1 ], null, strsql, 1, 1);
}

function diskImport() {
	//alert("111111111111111");
	//alert("====="+fm.all("diskimporttype").value);
	if (fm2.FileName.value == null || fm2.FileName.value == "") {
		alert("请选择要导入的文件！");
		return false;
	}
	var showStr = "正在导入数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm2.submit();
}
function moduleDownload() {
	var tSQL = "";
	fm.querySql.value = tSQL;
	var oldAction = fm.action;
	fm.action = "BKRateCommisionDownload1.jsp";
	fm.submit();
	fm.action = oldAction;
}
function queryImportResult() {
	var tbatchno = fm2.FileImportNo.value;
	var tqueryType = fm2.queryType.value;
	if (tbatchno == null || tbatchno == "") {
		alert("请输入查询文件批次号！");
		return false;
	}
	if (tqueryType == null || tqueryType == "") {
		alert("请选择查询类型！");
		return false;
	}
	strSQL = "select batchno,contid,errorinfo" + " from lcgrpimportlog"
			+ " where batchno='" + tbatchno + "' ";
	if (tqueryType == 1) {
		strSQL += " and errorstate='1'";
	} else if (tqueryType == 2) {
		strSQL += " and errorstate='0'";
	}
	strSQL += " order by contid";
	//执行查询并返回结果
	turnPage2.queryModal(strSQL, ImportResultGrid);
}
function clearImport(){
	document.getElementById('FileName').outerHTML=document.getElementById('FileName').outerHTML;
}
