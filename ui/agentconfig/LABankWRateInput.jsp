<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：DataIntoLACommision.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();    
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    String strSql= " 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'"; 
%>
<script>
	 var  mBranchType =  <%=BranchType%>;
	 //var  mBranchType2 =  <%=BranchType2%>;
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LABankWRateInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LABankWRateInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		提奖比例查询
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'#1# and length(trim(comcode))<=8 ','char(1)',1);" verify="管理机构|notnull&code:comcode"><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>          
           <TD  class= title>
       		险   种
    		</TD>
    		<TD  class= input>
       			<Input class= 'codeno' name= RiskCode verify="险种|code:RiskCode" 
       				ondblclick="return showCodeList('RiskCode',[this,RiskCodename],[0,1],null,null,null,null,300);"
       				onkeyup="return showCodeListKey('RiskCode',[this,RiskCodename],[0,1],null,null,null,null,300);"
      			><Input class=codename name=RiskCodename readOnly  >
    		</TD>  	   	
        </TR>
        
        <TR  class = common>
           <TD  class = title>
           录入起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="起始年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            录入止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="终止年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>       
        </TR> 
      </table>       
          
         <Table class="common" align=center>
  			<Tr>
    			<Td class=button>
      				<INPUT class=cssButton VALUE="银代直接提奖查询"  TYPE=button onclick="easyQueryClick1()">
      				<INPUT class=cssButton VALUE="直销直接提奖查询"  TYPE=button onclick="easyQueryClick2()">
      				<INPUT class=cssButton VALUE="银代主管间接绩效提奖查询"  TYPE=button onclick="easyQueryClick3()">
      				<INPUT class=cssButton VALUE="直销主管间接绩效提奖查询"  TYPE=button onclick="easyQueryClick4()">
    			</Td>
  			</Tr>
		</Table>
          <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">
          <input type="hidden" class=input name=BranchType2 value="<%=BranchType2%>">
          <input type="hidden" class=input name=querySQL value=""> 
          <input type="hidden" class=input name=downloadType value="">           
    </Div>
    
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <Div id= "divSpanSetGrid1" align=center style= "display: '' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanSetGrid1">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
	<Div id= "divSpanSetGrid2" align=center style= "display: 'none' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanSetGrid2">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
	<Div id= "divSpanSetGrid3" align=center style= "display: 'none' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanSetGrid3">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
    <Div id= "divSpanSetGrid4" align=center style= "display: 'none' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanSetGrid4">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
    <Div id= "divPage" align=left style= "display: '' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      <INPUT VALUE="下  载" class=cssbutton TYPE=button onclick="doDownload();"> 
    </div>
      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>