<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称： 
//程序功能：
//创建日期： 
//创建人  ：   
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
 


  LABRCSetCSCoeffUI tLABRCSetCSCoeffUI = new LABRCSetCSCoeffUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      //String tBranchType = request.getParameter("BranchType"); 
      //String tBranchType2 = request.getParameter("BranchType2"); 
      String tOperate=request.getParameter("hideOperate");
  		tOperate=tOperate.trim();
      LADiscountSet tSetU = new LADiscountSet();	//用于更新
      LADiscountSet tSetD = new LADiscountSet();	//用于删除
      LADiscountSet tSetI = new LADiscountSet();     //用于插入
            
      String tChk[] = request.getParameterValues("InpLADiscountGridChk"); 
      String tIdx[] = request.getParameterValues("LADiscountGrid1");
      String tManageCom[] = request.getParameterValues("LADiscountGrid2");
      String tRiskCode[] = request.getParameterValues("LADiscountGrid4");
      String tRiskName[] = request.getParameterValues("LADiscountGrid5");
      String tPayintv[] = request.getParameterValues("LADiscountGrid6");
      String tYear[] = request.getParameterValues("LADiscountGrid7");
      String tRate[] = request.getParameterValues("LADiscountGrid8");
      String RiskCodename = request.getParameter("RiskCodename");
      
      
      
      //获取最大的ID号
      ExeSQL tExe = new ExeSQL();
      String tSql = "select max(idx) from LADiscount order by 1 desc ";
      String strIdx = "";
      int tMaxIdx = 0;
  
      strIdx = tExe.getOneValue(tSql);
      if (strIdx == null || strIdx.trim().equals(""))
      {
          tMaxIdx = 0;
      }
      else
      {
          tMaxIdx = Integer.parseInt(strIdx);
          //System.out.println(tMaxIdx);
      }
      //创建数据集
     if (tChk != null  )
      for(int i=0;i<tChk.length;i++)
      {
      	if(tChk[i].equals("1"))
      	{
      		//创建一个新的Schema
      	  LADiscountSchema tLADiscountSchema = new LADiscountSchema();
      		
      	  			tLADiscountSchema.setRiskCode(tRiskCode[i]);
					tLADiscountSchema.setBranchType("3");
					tLADiscountSchema.setBranchType2("01");
					tLADiscountSchema.setDiscountType("10");//银代直销奖励标识
					tLADiscountSchema.setManageCom(tManageCom[i]);
					tLADiscountSchema.setPayIntv(tPayintv[i]);
					tLADiscountSchema.setCode5(tYear[i]);
					tLADiscountSchema.setRate(tRate[i]);		
					//tLADiscountSchema.setOperator(tG.tOperate);	  
      	  if((tIdx[i] == null)||(tIdx[i].equals("")))
      	  {
      	  	//需要插入记录
				  	tMaxIdx++;
				  	tLADiscountSchema.setIdx(tMaxIdx);
      	  	tSetI.add(tLADiscountSchema);
       	  }
      	  else
      	  {
            //需要删除
            if(tOperate.equals("DELETE||MAIN"))      			      	
      	    {
      	        tLADiscountSchema.setIdx(tIdx[i]);      			
      	        tSetD.add(tLADiscountSchema);
      	    }
            //需要更新
            else if(tOperate.equals("UPDATE||MAIN"))      			      	
      	    {
      	        tLADiscountSchema.setIdx(tIdx[i]);      			
      	        tSetU.add(tLADiscountSchema);
      	    }
      		 
          }
      	}      	
      }
          
      System.out.println("111111111111111111111111111222");
      // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      //tVData.add(RiskCodename);
      tVData.add(tGI);
      //没有更新或删除或插入的数据
      if ((tSetD.size() == 0)&&(tSetU.size() == 0)&&(tSetI.size() == 0))
      {
      System.out.println("11111111111111111111111111wweeee");
        	FlagStr = "Fail";
        	Content = "未选中要处理的数据！";      	
      }
      else if (tSetI.size() != 0)
     {
        	//只有插入数据
        	tVData.add(tSetI);
        	System.out.println("Start LADiscountUI Submit...INSERT");
        	tLABRCSetCSCoeffUI.submitData(tVData,"INSERT");        	    	
     }
     else if (tSetD.size() != 0)
     {
        	//只有删除数据
        	tVData.add(tSetD);
        	System.out.println("Start LADiscountUI Submit...DELETE");
        	tLABRCSetCSCoeffUI.submitData(tVData,"DELETE");        	    	
     }
     else if (tSetU.size() != 0)
     {
        	//只有删除数据
        	tVData.add(tSetU);
        	System.out.println("Start LADiscountUI Submit...UPDATE");
        	tLABRCSetCSCoeffUI.submitData(tVData,"UPDATE");        	    	
     }
    	

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      
      FlagStr = "Fail";
      Content = "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLABRCSetCSCoeffUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
        System.out.println("11111111111111111111111111wwwqqq");
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
