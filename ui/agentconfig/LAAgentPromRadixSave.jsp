<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentPromRadixSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.util.Date"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentPromRadixSchema tLAAgentPromRadixSchema   = new LAAgentPromRadixSchema();

  ALAAgentPromRadixUI tLAAgentPromRadix   = new ALAAgentPromRadixUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLAAgentPromRadixSchema.setBranchType(request.getParameter("BranchType"));
    tLAAgentPromRadixSchema.setAgentGrade(request.getParameter("AgentGrade"));
    tLAAgentPromRadixSchema.setAreaType(request.getParameter("AreaType"));
    tLAAgentPromRadixSchema.setAssessCode(request.getParameter("AssessType"));
    tLAAgentPromRadixSchema.setDestAgentGrade(request.getParameter("DestAgentGrade"));
    tLAAgentPromRadixSchema.setLimitPeriod(request.getParameter("LimitPeriod"));
    tLAAgentPromRadixSchema.setIndCustSum(request.getParameter("IndCustSum"));
    tLAAgentPromRadixSchema.setIndFYCSum(request.getParameter("IndFYCSum"));
    tLAAgentPromRadixSchema.setMonAvgCust(request.getParameter("MonAvgCust"));
    tLAAgentPromRadixSchema.setMonAvgFYC(request.getParameter("MonAvgFYC"));
    tLAAgentPromRadixSchema.setIndRate(request.getParameter("IndRate"));
    tLAAgentPromRadixSchema.setDirAddCount(request.getParameter("DirAddCount"));
    tLAAgentPromRadixSchema.setAddCount(request.getParameter("AddCount"));
    tLAAgentPromRadixSchema.setReachGradeCount(request.getParameter("ReachGradeCount"));
    tLAAgentPromRadixSchema.setIndAddFYCSum(request.getParameter("IndAddFYCSum"));
    tLAAgentPromRadixSchema.setTeamAvgRate(request.getParameter("TeamAvgRate"));
    tLAAgentPromRadixSchema.setDirMngCount(request.getParameter("DirMngCount"));
    tLAAgentPromRadixSchema.setDirMngFinaCount(request.getParameter("DirMngFinaCount"));
    tLAAgentPromRadixSchema.setTeamFYCSum(request.getParameter("TeamFYCSum"));
    tLAAgentPromRadixSchema.setDRTeamCount(request.getParameter("DRTeamCount"));
    tLAAgentPromRadixSchema.setDRTeamAvgRate(request.getParameter("DRTeamAvgRate"));
    tLAAgentPromRadixSchema.setDRFYCSum(request.getParameter("DRFYCSum"));
    tLAAgentPromRadixSchema.setRearTeamSum(request.getParameter("RearTeamSum"));
    tLAAgentPromRadixSchema.setDRMonLabor(request.getParameter("DRMonLabor"));
    tLAAgentPromRadixSchema.setLaborFYCStand(request.getParameter("LaborFYCStand"));
    tLAAgentPromRadixSchema.setDepFYCSum(request.getParameter("DepFYCSum"));
    tLAAgentPromRadixSchema.setDepAvgRate(request.getParameter("DepAvgRate"));
    tLAAgentPromRadixSchema.setRearDepCount(request.getParameter("RearDepCount"));
    tLAAgentPromRadixSchema.setDRDepLabor(request.getParameter("DRDepLabor"));
    tLAAgentPromRadixSchema.setDRDepAvgRate(request.getParameter("DRDepAvgRate"));
    tLAAgentPromRadixSchema.setRearDepSum(request.getParameter("RearDepSum"));
    tLAAgentPromRadixSchema.setDirDepMonAvgFYC(request.getParameter("DirDepMonAvgFYC"));
    tLAAgentPromRadixSchema.setDepFinaCount(request.getParameter("DepFinaCount"));
    tLAAgentPromRadixSchema.setAreaRate(request.getParameter("AreaRate"));
    tLAAgentPromRadixSchema.setDirRearAdmCount(request.getParameter("DirRearAdmCount"));
    tLAAgentPromRadixSchema.setMngAreaFinaCount(request.getParameter("MngAreaFinaCount"));
    tLAAgentPromRadixSchema.setReqDepCount(request.getParameter("ReqDepCount"));
                  
  System.out.println("Operator:"+tG.Operator);

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLAAgentPromRadixSchema);
	tVData.add(tG);
  try
  {
    tLAAgentPromRadix.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

        System.out.println("flagStr"+FlagStr);
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAgentPromRadix.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
        System.out.println("no reord");
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

