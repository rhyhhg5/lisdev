<html>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-15 11:48:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="IndexSetInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="IndexSetInit.jsp"%>
</head>

<body  onload="initForm();" >
  <form action="IndexSetSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
      <table  class= common>
        <TR  class= common>
          <TD  class= title> 展业类型 </TD>
          <td class="input">
           <input class=code name=BranchType verify = "展业类型|notnull&code:branchType"
                             ondblclick="return showCodeList('BranchType',[this]);" 
                             onkeyup="return showCodeListKey('BranchType',[this]);"
                             onchange="return changeIndexType();">
          </td>

          <TD  class= title>指标类型</TD>
          <TD  class= input>
              <input class=code name=IndexType verify = "指标类型|notnull&code:IndexType" 
                                ondblclick="return showCodeList('IndexType',[this],[0,1],null,sql,'1');" 
                                onkeyup="return showCodeListKey('IndexType',[this],[0,1],null,sql,'1');">
          </TD>
        </TR>

       <tr class=common>
        <td class=title nowrap>新指标编码</td>
        <td class=input> <input class=common name=IndexCode verify="新指标编码|notnull&len<=6" >
        </td>
        <td class=title nowrap>新指标名称</td>
        <td class=input> <input class=common name=IndexName verify="新指标名称|notnull" >
        </td>
      </tr>

       <tr>
        <td class=title nowrap>指标对应表名</td>
        <td class=input> <input class='readonly' readonly name=ITableName value="LAIndexInfo" >
        </td>
        <td class=title nowrap>指标对应字段名</td>
        <td class=input> <input class=common name=IColName >
        </td>
      </tr>

      <tr>
	   <td class=title>计算类型</td>
	   <td class=input> <input class=code name=CalType verify="计算类型|code:caltype" 
	                                      ondblclick="return showCodeList('CalType',[this]);" 
	                                      onkeyup="return showCodeListKey('CalType',[this]);">
	   </td>
       <td class=title>Sql语句编码</td>
       <td class=input> <input class=common name=CalCode verify="sql语句编码|notnull&len<=10">
       </td>
     </tr>
     <tr> &nbsp;&nbsp; </tr>
    </Table>
    
    <input class=common type=hidden name=sql value="1 and 1 <> 1">

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRelaIndexGrid);">
    		</td>
    		<td class= titleImg>
    			 相关指标信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divRelaIndexGrid" style= "display: ''">
     <table class=common>	
        <TR class=input>   
      <td class=title>相关指标设置</td>  
         <TD class=common>
         <input class=common type=Button name=QryRelaIndex value="查 询" OnClick = "qry_rela_index();">   
        </TD>   
         <TD class=common>
         <input class=common type=Button name=QryRelaIndex value="重置指标集" OnClick = "clearIndexSet();">
        </TD>
        <Input type=hidden name=mOperate value="">
       </TR>
     </table>
      
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanRelaIndexGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
   </div>

      <input type=hidden id="fmAction" name="fmAction">
      <input type=hidden id="IndexSet" name="IndexSet">
      <input type=hidden id="qryFlag" name="qryFlag">

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
