<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String branchtype = request.getParameter("BranchType");
  String branchtype2 = request.getParameter("BranchType2");
  String FlagStr = "Fail";
  String Content = "";
  ExeSQL tExeSQL = new ExeSQL();
  String arrCount[] = request.getParameterValues("InpActiveChargeGridChk");
  String tmanagecom[] = request.getParameterValues("ActiveChargeGrid1");
  //String tGroupAgentCode[] = request.getParameterValues("ActiveChargeGrid2");
  String tWageFlag[] = request.getParameterValues("ActiveChargeGrid3");
  String tAssessFlag[] = request.getParameterValues("ActiveChargeGrid4");
  String tRuleFlag[] = request.getParameterValues("ActiveChargeGrid5");
  String tIdx[] = request.getParameterValues("ActiveChargeGrid8");
  //lineCount = arrCount.length; //行数
  
  LABankWageStandUI tLABankWageStandUI = new LABankWageStandUI();
  LABankIndexRadixSchema tLABankIndexRadixSchema;
  LABankIndexRadixSet tLABankIndexRadixSet = new LABankIndexRadixSet();
  int lineCount = arrCount.length;
  for(int i=0;i<lineCount;i++)
  {
  	if(arrCount[i].equals("1"))
    {
  		tLABankIndexRadixSchema = new LABankIndexRadixSchema();
  		tLABankIndexRadixSchema.setManageCom(tmanagecom[i]);
  		tLABankIndexRadixSchema.setBranchType(branchtype);
  		tLABankIndexRadixSchema.setBranchType2(branchtype2);
  		tLABankIndexRadixSchema.setWageFlag(tWageFlag[i]);
  		tLABankIndexRadixSchema.setAssessFlag(tAssessFlag[i]);
  		tLABankIndexRadixSchema.setRuleFlag(tRuleFlag[i]);
	    tLABankIndexRadixSet.add(tLABankIndexRadixSchema); 
	    if (tOperate.equals("UPDATE")){
	    	tLABankIndexRadixSchema.setIdx(tIdx[i]);
	    }
    }
  }

System.out.println("tOperate11:"+tOperate);
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.add(tLABankIndexRadixSet);

System.out.println("tOperate:"+tOperate);
  try
  {
    System.out.println("this will save the data!!!");	
    tLABankWageStandUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABankWageStandUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
