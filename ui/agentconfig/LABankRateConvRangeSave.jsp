<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABankRateConvRangeSave.jsp
//程序功能：
//创建日期：2008-03-3 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentconfig.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABaseWageSchema tLABaseWageSchema   = new LABaseWageSchema();
  LABankRateConvRangeUI tLABankRateConvRangeUI   = new LABankRateConvRangeUI();
  
  

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
    String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLABaseWageSchema.setManageCom(request.getParameter("ManageCom"));
    
    tLABaseWageSchema.setMonthRate(request.getParameter("HiddenMonthRate"));
    
    tLABaseWageSchema.setSeasonRate(request.getParameter("HiddenSeasonRate"));
    tLABaseWageSchema.setYearRate(request.getParameter("HiddenYearRate"));
    tLABaseWageSchema.setIdx(request.getParameter("idx"));
    tLABaseWageSchema.setType("02");
   /* tLABaseWageSchema.setMarkType(request.getParameter("AscClass"));
    tLABaseWageSchema.setAssessMarkType("33");
    tLABaseWageSchema.setAssessMark(request.getParameter("Score"));
    tLABaseWageSchema.setDoneDate(request.getParameter("DoneDate"));
   
    
   
    tLABaseWageSchema.setAssessYM(request.getParameter("AssessYM"));
    
    tLABaseWageSchema.setNoti(request.getParameter("Noti"));*/
    tLABaseWageSchema.setOperator(tG.Operator);
    tLABaseWageSchema.setBranchType("3");
    tLABaseWageSchema.setBranchType2("01");


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	
	/*tVData.add(tName);
	tVData.add(tBranchAttr);
	tVData.add(tAgentGroup);
	tVData.add(tManageCom);*/
	
	tVData.addElement(tLABaseWageSchema);
	tVData.add(tG);
	
  try
  {
    tLABankRateConvRangeUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLABankRateConvRangeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
        parent.fraInterface.fm.all('hideOperate').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
