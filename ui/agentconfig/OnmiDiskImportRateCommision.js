
//磁盘导入
function importFile()
{
  if (fm.FileName.value == "")
  {
    alert("请选择要导入的文件！");
    return false;
  }
  
	document.all("Info").innerText = "上传文件导入中，请稍后...";
	submitForm();
}

//提交
function submitForm()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit(); //提交
}

//执行导入后的处理
function afterSubmit(FlagStr, content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    else
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		top.opener.focus();
		top.opener.location.reload();
		top.close();
    }
}