<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LaratecommisionSetKLRSInput.jsp
//程序功能：佣金基数表设置录入界面
//创建时间：2006-08-22
//创建人  ：luomin 
// below is miaoxiangzheng's new_add .
// date is 2008-4-29
String BranchType2=request.getParameter("BranchType2");
if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
// above is miaoxiangzheng's new_add
%>
<%@page contentType="text/html;charset=GBK" %>
<script language="JavaScript">
//var StrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode)";
</script>
<script>
   var msql=" 1 and   char(length(trim(comcode)))<=#4# ";
   var msql1=" 1 and   trim(code) in(#5#,#6#,#8#,#10#) ";
   var StrSql2=" 1 and riskcode  in (select code from ldcode where codetype=#klrscode# and comcode=#1#) ";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LaratecommisionSetKLRSInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LaratecommisionSetKLRSInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./LaratecommisionSetKLRSSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
    <tr class=common>
     <td class=title>险种</td>
     <td class= input>
     	<Input class=codeno name=RiskCode verify="险种|code:RiskCode" readonly
            ondblclick="return showCodeList('RiskCode',[this,RiskCodename],[0,1],null,StrSql2,1,1);" 
            onkeyup="return showCodeListKey('RiskCode',[this,RiskCodename],[0,1],null,StrSql2,1,1);"  ><input class=codename name=RiskCodename readonly=true  >
             
             <TD  class= title>
           缴费方式
          </TD>
          <TD  class= input>
             <Input class="codeno" name=PayintvCode readonly CodeData="0|^0|趸缴|^12|年缴|^6|半年缴|^3|季缴|^1|月缴" ondblclick="return showCodeListEx('payintvlist',[this,PayintvName],[0,1]);" onkeyup="return showCodeListKeyEx('payintvlist',[this,PayintvName],[0,1]);" onchange=""><input class=codename name=PayintvName  readonly=true ></TD>  
          </TD>    
                			
   
   </tr>
   <tr>			
   	
       
   				 <TD  class= title>
            最低缴费年期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=StartDay >
          </TD>
            <TD  class= title>
            最高缴费年期
          </TD>
          <TD  class= input>
            <Input class= "common"  name=EndDay >
          </TD>
   	
   </tr>
  
   
   <tr class=common> 
         

          <td  class= title> 保单年度 </td>
          <TD  class= input> <Input class= "common" name=CurYear></TD>
           <td  class= title>佣金比率</td>
          <TD  class= input> <Input class= "common" name=Rate></TD>      			
   </tr>
   
   <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='code' name=ManageCom elementtype=nacessary verify = "管理机构|notnull&code:ComCode& " 
            ondblclick="return showCodeList('comcode',[this],[0],null,msql,1,1);" 
            onkeyup="return showCodeListKey('comcode',[this],[0],null,msql,1,1);"> 
          </TD>
   <td class="title">
       保险期间
      </td>
      <TD  class= input>
            <Input class= "codeno"  name=year  readonly 
            ondblclick="return showCodeList('payyear',[this,yearName],[0,1],null,msql1,1,1);"
            onkeyup="return showCodeList('payyear',[this,yearName],[0,1],null,msql1,1,1);"
            ><input class="codename" name=yearName readOnly=true>
          </TD>
   </TR>
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     	
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="下  载" class=cssButton onclick="DoNewDownload();">
     		<input type=button value="修  改" class=cssButton onclick="return DoSave();">  
    		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 

    	</td>
   
    </tr>      
  </table>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>佣金计算基数设置</td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>
  <br><hr>
  <Input class="readonly" type=hidden name=diskimporttype>
  <Input type=hidden name=branchtype2> 
  <Input type=hidden id="BranchType" name=BranchType value=''>
  <input type=hidden id="sql_where" name="sql_where" >
  <input type=hidden id="fmAction" name="fmAction">  
  <input type=hidden class=Common name=querySql > 
  </form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




