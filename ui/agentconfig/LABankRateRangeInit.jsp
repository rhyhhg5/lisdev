<%
//程序名称：LAArchieveInput.jsp
//程序功能：
//创建日期： 
//创建人   :
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>  
<script language="JavaScript">
 var  tmanagecom=" 1 and  char(length(trim(comcode)))=#4# ";
  
</SCRIPT>                                         
<script language="JavaScript">

function initInpBox()
{ 
  try
  {
    fm.all('ManageCom').value = "";
    fm.all('ManageComName').value = "";
    fm.all('RiskCode').value = "";
    fm.all('RiskCodeName').value = "";
    fm.all('BranchType').value = "<%=BranchType%>";
    fm.all('BranchType2').value = "<%=BranchType2%>";
  
  }
  catch(ex)
  {
    alert("  InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
      
    initArchieveGrid1();    
  
    
  }
  catch(re)
  {
    alert(" InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ArchieveGrid1;
function initArchieveGrid1() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许    
    
    iArray[1]=new Array();
    iArray[1][0]="idx";            //列名 idx
    iArray[1][1]="0px";          //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许,0表示不允许   

    iArray[2]=new Array();
    iArray[2][0]="险种编码";          		//列名
    iArray[2][1]="80px";      	      		//列宽
    iArray[2][2]=80;            			//列最大值
    iArray[2][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[2][4]="bankriskcode";              	        //是否引用代码:null||""为不引用
    iArray[2][5]="2|3";              	                //引用代码对应第几列，'|'为分割符
    iArray[2][6]="0|1";   


    iArray[3]=new Array();
    iArray[3][0]="险种名称"; //列名
    iArray[3][1]="120px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="提奖比例(%)";          		        //列名
    iArray[4][1]="100px";      	      		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=1;             //是否允许输入,    
    iArray[4][9]="提奖比例|notnull&num";  
   
    iArray[5]=new Array();
    iArray[5][0]="管理机构"; //列名
    iArray[5][1]="40px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[5][4]="ComCode";
    //iArray[5][5]="1|1";
    //iArray[5][6]="0|1";
    iArray[5][9]="管理机构|NotNull&NUM&len=4";    
    iArray[5][15]="1";
    iArray[5][16]=tmanagecom; 
    

    ArchieveGrid1 = new MulLineEnter( "fm" , "ArchieveGrid1" ); 
    //这些属性必须在loadMulLine前
    ArchieveGrid1.canChk = 1;
    ArchieveGrid1.mulLineCount = 0;   
    ArchieveGrid1.displayTitle = 1;
    ArchieveGrid1.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ArchieveGrid2.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }

}
</script>
