//               该文件中包含客户端需要处理的函数和事件
//程序名称：LAAChargeWrapRateInput.js
//程序功能：
//创建时间：2008-01-24
//创建人  ：Huxl
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function DoSave()
{
	fm.fmAction.value = "UPDATE";
	if(!chkPrimaryKey()) return false;
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}

//提交，保存按钮对应操作
function DoInsert()
{
	fm.fmAction.value = "INSERT";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}

function DoDel()
{
	fm.fmAction.value = "DELETE";
	if(!beforeSubmit()) return false;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	if(!verifyInput())
	{
	 return false;
	}
	initSetGrid();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.riskcode,(select wrapname from LDWrap where RiskWrapCode = a.riskcode),"
		     +"a.managecom,(select name from ldcom where comcode = a.managecom),"
		     +"a.agentcom,(select name from lacom where agentcom = a.agentcom),"
		     +"nvl(a.rate,0),a.f05,a.f06,a.idx,a.riskcode,a.managecom,a.agentcom,a.f05,a.f06 from  laratecommision a "
		     +" where a.f01 = 10 and a.branchtype='1' and  a.branchtype2='02' "
		     + " and  a.managecom like '"+fm.ManageCom.value+"%' "
		     +" and   a.riskcode in(select RiskWrapCode from LDWrap where  1 = 1) "
		     + getWherePart( 'a.riskcode','wrapcode')
		 	 + getWherePart( 'a.agentcom','AgentCom')
		 	 + getWherePart( 'a.f05','F05')
		 	 + getWherePart( 'a.f06','F06')
	
	
	if(fm.Rate.value!=null&&fm.Rate.value!='')
	{
	 strSQL =strSQL+ " and a.rate="+fm.Rate.value+" ";
	}
	
	//strSQL = strSQL +"  order by a.managecom, a.riskcode";
   //查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}

	//查询成功则拆分字符串，返回二维数组
	  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  turnPage.arrDataCacheSet = arrDataSet;

	//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = SetGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;

	//设置查询起始位置
	turnPage.pageIndex = 0;

	//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

	//调用MULTILINE对象显示查询结果
	displayMultiline(tArr, turnPage.pageDisplayGrid);
					//return true;
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			initForm();
		}
}

function chkPrimaryKey()
{
	var rowNum =  SetGrid.mulLineCount;;
	for(i=0;i<rowNum;i++)
	{
		if( SetGrid.getChkNo(i))
		{
			if(SetGrid.getRowColData(i,1)!=SetGrid.getRowColData(i,11))
			{
				alert("不能修改套餐编码！");
				//SetGrid.setRowColData(i,1,SetGrid.getRowColData(i,11));
				return false;
			}
			if(SetGrid.getRowColData(i,3)!=SetGrid.getRowColData(i,12))
			{
				alert("不能修改管理机构编码！");
				//SetGrid.setRowColData(i,3,SetGrid.getRowColData(i,12));
				return false;
			}
			if(SetGrid.getRowColData(i,5)!=SetGrid.getRowColData(i,13))
			{
				alert("不能修改代理机构编码！");
				//SetGrid.setRowColData(i,5,SetGrid.getRowColData(i,13));
				return false;
			}
			if(SetGrid.getRowColData(i,8)!=SetGrid.getRowColData(i,14))
			{
				alert("不能修改有效起期！");
				//SetGrid.setRowColData(i,8,SetGrid.getRowColData(i,14));
				return false;
			}
			if(SetGrid.getRowColData(i,9)!=SetGrid.getRowColData(i,15))
			{
				alert("不能修改有效止期！");
				//SetGrid.setRowColData(i,9,SetGrid.getRowColData(i,15));
				return false;
			}
		}
	}
	return true;
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function DoReset()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("初始化页面错误，重置出错");
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}
		//Click事件，当点击“修改”图片时触发该函数
function DoUpdate()
{
	if (confirm("您确实想修改该记录吗?"))
	{
		fm.fmAction.value = "UPDATE";
		if(!beforeSubmit()) return false;
		var i = 0;
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
	}
	Else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}

//判断是否选择了要增加、修改或删除的行
function chkMulLine()
{   
	//alert("enter chkmulline");
	var i;
	var selFlag = true;
	var iCount = 0;
	var rowNum = SetGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{   
		if(SetGrid.getChkNo(i))
		{
			iCount++;
			if((SetGrid.getRowColData(i,1) == null)||(SetGrid.getRowColData(i,1) == ""))
			{
				alert("套餐代码不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			if((SetGrid.getRowColData(i,7) == null)||(SetGrid.getRowColData(i,7) == ""))
			{
				alert("手续费比率不能为空");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
			 
			if(SetGrid.getRowColData(i,7)<'0')
			{
								          alert("手续费比率不能为负数!");
								          SetGrid.setFocus(i,1,SetGrid);
										  selFlag = false;
										  break;
								       }
			 if(SetGrid.getRowColData(i,7)<'0'||SetGrid.getRowColData(i,7)>'1')
			 {
				alert("手续费比率不在0到1之间!");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			  }
			 if(SetGrid.getRowColData(i,8)>SetGrid.getRowColData(i,9))
			 {
				alert("止期必须大于起期!");
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			  }
			//如果Serialno为空不能删除和更新
			if((SetGrid.getRowColData(i,10) == null)||(SetGrid.getRowColData(i,10)==""))
			{
				if((fm.fmAction.value == "DELETE") || (fm.fmAction.value == "UPDATE"))
				{
					alert("有未保存的新增纪录！不可删除或更新，请先保存记录。");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
			else if ((SetGrid.getRowColData(i,10) !== null)||(SetGrid.getRowColData(i,10)!==""))
			{	//如果Serialno不为空不能插入
				if(fm.fmAction.value == "INSERT")
				{
					alert("此纪录已存在，不可插入！");
					SetGrid.checkBoxAllNot();
					SetGrid.setFocus(i,1,SetGrid);
					selFlag = false;
					break;
				}
			}
		}
		else
		{//不是选中的行
			if((SetGrid.getRowColData(i,10) == null)||(SetGrid.getRowColData(i,10)==""))
			{
				alert("有未保存的新增纪录！请先保存记录。");
				SetGrid.checkBoxAllNot();
				SetGrid.setFocus(i,1,SetGrid);
				selFlag = false;
				break;
			}
		}
	}
	if(!selFlag) return selFlag;
	if(iCount == 0)
	{
		alert("请选择要保存或删除的记录!");
		return false
	}
	return true;
}

//提交前的校验、计算
function beforeSubmit()
{										
	if(!SetGrid.checkValue("SetGrid")) return false;
	if(!chkMulLine()) return false;
	return true;
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";
	}
}

function getAgentCom(cObj,cName)
{
if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
showCodeList('agentcom',[cObj,cName],[0,1],null,'1 and BranchType=#1# and BranchType2 =#02# ',1,1);
}
else if (fm.all('ManageCom').value != null || trim(fm.all('ManageCom').value) != '') 
{
var strsql =" 1 and BranchType=#1# and BranchType2 =#02#  and managecom like #" + fm.all('ManageCom').value + "%# " ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,1,1);
}

}		

function getRiskCode(cObj,cName)
{

 if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
{
	alert("请先输入管理机构！");
  	return false;
} 	
//showCodeList('wrapcode1',[cObj,cName],[0,1],null,null,null,1);


}

							
function doDownLoad(){
	if(!verifyInput())
	{
	 return false;
	}

	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.riskcode,(select wrapname from ldwrap where riskwrapcode = a.riskcode),"
	     +"a.managecom,(select name from ldcom where comcode = a.managecom),"
	     +"a.agentcom,(select name from lacom where agentcom = a.agentcom),"
	     +"nvl(a.rate,0),a.f05,a.f06 from  laratecommision a "
	     +" where a.f01 = 10 and a.branchtype='1' and  a.branchtype2='02'  and   a.riskcode in(select RiskWrapCode from LDWrap where  1 = 1) "
	     + " and  a.managecom like '"+fm.ManageCom.value+"%' "
	     + getWherePart( 'a.riskcode','wrapcode')
	 	 + getWherePart( 'a.agentcom','AgentCom')
	 	 + getWherePart( 'a.f05','F05')
	 	 + getWherePart( 'a.f06','F06')
	if(fm.Rate.value!=null&&fm.Rate.value!='')
	{
	 strSQL =strSQL+ " and a.rate="+fm.Rate.value+" ";
	}
	var oldaction = fm.action;
	fm.all('querySQL').value=strSQL;
    fm.action = "./LAAChargePackageWRReport.jsp";
    fm.submit();
    fm.action = oldaction;
}
						





