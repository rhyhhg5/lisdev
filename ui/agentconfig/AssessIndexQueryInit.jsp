<%
//程序名称：AssessIndexQueryInit.jsp
//程序功能：
//创建日期：2002-08-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('BranchType').value = '';
    fm.all('AssessType').value = '';
    fm.all('AssessCode').value = '';
    fm.all('AssessName').value = '';
    fm.all('IndexCode').value = '';
    fm.all('CalCode').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('DestAgentGrade').value = '';
  }
  catch(ex)
  {
    alert("在AssessIndexQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initAssessIndexGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="25px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="展业类型"; //列名
    iArray[1][1]="60px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="考核类型"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="代理人职级"; //列名
    iArray[3][1]="70px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="目标职级"; //列名
    iArray[4][1]="70px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="考核代码"; //列名
    iArray[5][1]="70px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="考核名称"; //列名
    iArray[6][1]="150px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="指标编码"; //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="考核结果对应表名"; //列名
    iArray[8][1]="100px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="考核结果对应字段名"; //列名
    iArray[9][1]="100px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[10]=new Array();
    iArray[10][0]="SQL语句编码"; //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=0;              //是否允许输入,1表示允许,0表示不允许


    AssessIndexGrid = new MulLineEnter( "fm" , "AssessIndexGrid" );
    AssessIndexGrid.canSel = 1;
    AssessIndexGrid.mulLineCount =  10;
    AssessIndexGrid.displayTitle = 1;
    AssessIndexGrid.locked = 1;
    AssessIndexGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在AssessIndexQueryInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();
    initAssessIndexGrid();
  }
  catch(re)
  {
    alert("AssessIndexQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>