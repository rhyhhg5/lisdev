<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2006-08-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 var  tmanagecom=" 1 and char(length(trim(comcode))) in (#8#,#4#,#2#) ";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.all('ManageCom').value='';
  	fm.all('ManageComName').value='';
    fm.all('WrapCode').value='';
    fm.all('WrapCodeName').value=''; 
    fm.all('F01Code').value='';
    fm.all('F01CodeName').value=''; 
    fm.all('Rate').value='';    
    fm.all('BranchType').value='<%=BranchType%>';   
    fm.all('BranchType2').value='<%=BranchType2%>';                 
  }
  catch(ex)
  {
    alert("在LAWrapRateSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[1][4]="ComCode";
    iArray[1][9]="管理机构|NotNull&NUM";    
    iArray[1][15]="1";
    iArray[1][16]=tmanagecom;
    
  
    
    
    iArray[2]=new Array();
    iArray[2][0]="套餐编码";          		//列名
    iArray[2][1]="80px";      	      		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[2][4]="WrapCode";              	        //是否引用代码:null||""为不引用
//	  iArray[2][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
//	  iArray[2][6]="0|1";
//	  iArray[2][9]="套餐编码|code:wrapcode&notnull";  
//	  iArray[2][15]= "1";              	        //校验输入是否正确
	 // iArray[1][16]= StrSql;  
    
   iArray[3]=new Array();
    iArray[3][0]="销售渠道"; //列名
    iArray[3][1]="60px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许 
	iArray[3][9]="销售渠道|NotNull";
    iArray[3][10]="branchtype3";
    iArray[3][11]="0|^1|互动直销|^2|互动开拓";
//    
//    iArray[3]=new Array();
//    iArray[3][0]="套餐名称"; //列名
//    iArray[3][1]="120px";        //列宽
//    iArray[3][2]=100;            //列最大值
//    iArray[3][3]=1;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[4]=new Array();
    iArray[4][0]="提奖比率"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=1;              //是否允许输入,1表示允许,0表示不允许    
	iArray[4][9]="提奖比率|NotNull&NUM";
        
  
		
	iArray[5]=new Array();
    iArray[5][0]="序号"; //列名
    iArray[5][1]="0px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=3;              //是否允许输入,1表示允许,0表示不允许
    
        
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenSubtraction =1;
    SetGrid.hiddenPlus=0;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LAWrapRateSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    
  }
  catch(re)
  {
    alert("在LAWrapRateSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>