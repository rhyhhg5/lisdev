<%
//程序名称：codeQueryKernel.jsp
//程序功能：codeQuery查询功能的核心，暂时用于校验功能的代码查询部分
//创建日期：2002-10-18
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
//朱向峰	2005-4-6 14:17	修改codequerybl总是抛出异常的错误
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.LDCodeSchema"%>
<%@page import="com.sinosoft.utility.*"%>
<%!
public String codeQueryKernel(String codeType, Object session)
{
	String strResult = "";
	TransferData tTransferData = new TransferData();
	CodeQueryUI tCodeQueryUI=new CodeQueryUI();
	VData tData=new VData();

	LDCodeSchema tLDCodeSchema =new LDCodeSchema();
	tLDCodeSchema.setCodeType(codeType);

	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session;

	tTransferData.setNameAndValue("codeCondition","");
	tTransferData.setNameAndValue("conditionField","") ;

	//必须采用如下的顺序，不许更改
	tData.add(tLDCodeSchema);
	tData.add(tGI);
	tData.add(tTransferData);

	tCodeQueryUI.submitData(tData, "QUERY||MAIN");
	if (tCodeQueryUI.mErrors.needDealError())
	{
		System.out.println(tCodeQueryUI.mErrors.getFirstError()) ;
	}
	else
	{
		tData.clear();
		tData=tCodeQueryUI.getResult();
		strResult=(String)tData.get(0);
	}
	return strResult;
}

public String codeQueryKernel2(String codeType,String codeField,String codeCondition, Object session)
{
	String strResult = "";
	TransferData tTransferData = new TransferData();
	CodeQueryUI tCodeQueryUI=new CodeQueryUI();
	VData tData=new VData();

	LDCodeSchema tLDCodeSchema =new LDCodeSchema();
	tLDCodeSchema.setCodeType(codeType);
	
	tTransferData.setNameAndValue("codeCondition",codeCondition);
	tTransferData.setNameAndValue("conditionField",codeField) ;
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session;

	//必须采用如下的顺序，不许更改
	tData.add(tLDCodeSchema);
	tData.add(tGI);
	tData.add(tTransferData);
	
	tCodeQueryUI.submitData(tData, "QUERY||MAIN");
	if (tCodeQueryUI.mErrors.needDealError())
	{
		System.out.println(tCodeQueryUI.mErrors.getFirstError()) ;
	}
	else
	{
		tData.clear();
		tData=tCodeQueryUI.getResult();
		strResult=(String)tData.get(0);
	}
	return strResult;
}
%>