<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	GlobalInput tGI = (GlobalInput) session.getAttribute("GI");
	String ComCode1 = tGI.ComCode;
	String ComType_ComLimit = "1201198308";
    System.out.println("-----ComType---------------"+ComType_ComLimit);
%>
<script type="text/javascript">
  var mComType = "<%=ComType_ComLimit%>";
//当前操作员所在机构的所有下级机构
function getManageComLimitlike1(cond)
{
	if (typeof(cond) == "undefined")
	{
		cond = "ComCode";
	}
	var tReturn = " and "+ cond + " in(select ComCode from FDCom where InComCode like ( select InComCode"
	               + " from FDCom where ComCode = '" + "<%=ComCode1%>" + "') || '%%') ";
	return tReturn;
}
function getHManageComLimitlike1(cond)
{
	if (typeof(cond) == "undefined")
	{
		cond = "ComCode";
	}
	var tReturn = " and "+ cond + " in(select ComCode from FDHCom where InComCode like ( select InComCode"
	               + " from FDhCom where ComCode = '" + "<%=ComCode1%>" + "') || '%%') ";
	return tReturn;
}
//当前操作员所在机构的所有下级法人机构
function getManageComLimitlike2(cond)
{
	if (typeof(cond) == "undefined")
	{
		cond = "ComCode";
	}
	if(mComType=="00"||mComType=="01"||mComType=="02"||mComType=="03"){
		var tReturn = " and "+ cond + " in(select ComCode from FDCom where InComCode like ( select InComCode"
	                + " from FDCom where ComCode = '" + "<%=ComCode1%>" + "') || '%%' and ComNature='Y' and ComType = '03' )";
	
	}else{
	   var tReturn = " and "+ cond + " in (select ComCode from FDCom where " 
                    +" ComType='03' and ComNature='Y' and  EndFlag = 'N' connect by PRIOR UpComCode = ComCode start with ComCode = '" + "<%=ComCode1%>" + "' )";
	}
	return tReturn;
}
//查询条件中机构的下级机构
function getManageComLimitlike3(cond1,cond2,cond3)
{
	//默认为ComCode
	if (typeof(cond1) == "undefined" || cond1 == null || cond1 == "null" || cond1 =="")
	{
		cond1 = "ComCode";
	}
	
	//默认为当前操作员所在机构
	if (typeof(cond2) == "undefined" || cond2 == null || cond2 == "null" || cond2 =="")
	{
		cond2 = "<%=ComCode1%>";
	}
	
	//默认为空
	if (typeof(cond3) == "undefined" || cond3 == null || cond3 == "null" || cond3 =="")
	{
		cond3 = "";
	}
	
	var tReturn = " and "+ cond1 + " in(select ComCode from FDCom where InComCode like ( select InComCode"
	               + " from FDCom where ComCode = '" + cond2 + "') || '%%' " + cond3 + ")";
	return tReturn;
}

//查询条件中机构的下级机构   和getManageComLimitlike3 相比 tReturn 返回的值多了个左括弧！ 供财险保单查询机构权限控制使用。//add by fuqc
function getManageComLimitlike4(cond1,cond2,cond3)
{
	//默认为ComCode
	if (typeof(cond1) == "undefined" || cond1 == null || cond1 == "null" || cond1 =="")
	{
		cond1 = "ComCode";
	}
	
	//默认为当前操作员所在机构
	if (typeof(cond2) == "undefined" || cond2 == null || cond2 == "null" || cond2 =="")
	{
		cond2 = "<%=ComCode1%>";
	}
	
	//默认为空
	if (typeof(cond3) == "undefined" || cond3 == null || cond3 == "null" || cond3 =="")
	{
		cond3 = "";
	}
	
	var tReturn = " and ("+ cond1 + " in(select ComCode from FDCom where InComCode like ( select InComCode"
	               + " from FDCom where ComCode = '" + cond2 + "') || '%%' " + cond3 + ")";
	return tReturn;
}




function getManageComLimit(cond)
{
	if (typeof(cond) == "undefined")
	{
		cond = "";
	}
	var ComCode = cond;
	var tReturn = "('";
	var strSql = "select ComCode from FDCom where ComType = '01' and EndFlag = 'N' connect by PRIOR ComCode = UpComCode start with "
						+ "ComCode = '" + ComCode + "'";
	var strQueryResult = easyQueryVer3(strSql, 1, 0, 1);
    if (!strQueryResult)
    {
       return null;
    }
    else{
	    var arr = decodeEasyQueryResult(strQueryResult);
	    for(i = 0 ;i < arr.length ; i++)
	    {
           if(i == arr.length - 1)
           {
             tReturn += arr[i][0] + "')";
           }
           else{
              tReturn += arr[i][0] + "','";
           }
	    }
		return tReturn;
	}
}

//根据用户登陆机构级别控制是否能够进行查询，当没有参数传入时，默认“00”、“01”、“02”都无法查询到，传入参数用","分割
function getIllegalComType(cond)
{	
	if (typeof(cond) == "undefined")
	{
		cond = "00,01,02";
	}
	
	var tReturn = "";
	var aa = '<%=ComType_ComLimit%>';
	
	if (cond.indexOf(aa)<0)  
		tReturn = " and 1=1 ";
	else 
		tReturn = " and 1=0 ";
		
	return tReturn;	
}
</script>