<%
//程序名称：GetMutLine.jsp
//程序功能：配合杨亚林的前台MutLine的使用名字的改进
//创建日期：2007-12-21 18:07
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%!
// 类的私有成员，用来缓存各个mutline的列和列名的关系
Map colName = new HashMap();
/**
*	getColName 
* 参数 request 对应jsp页面的请求对象，aMutlineName 对应mutline对象的名字，aColName 对应列的名字
* 返回列名对应的列顺序值，没有该列名则返回空
*/
private String getColName(HttpServletRequest request,String aMutlineName,String aColName)
{
	//先在缓存中查一下
	String temp = (String)colName.get(aMutlineName+"+"+aColName);
	if(temp==null||temp.equals(""))
	{
		String tColName = request.getParameter("ColName"+aMutlineName);

		if(tColName!=null)
		{
			//|符号在正则表达式中为关键字，需要转义
			String arrColName[] = tColName.split("\\|");
			for(int i=0;i<arrColName.length;i++)
			{
				String t[] = arrColName[i].split("@");
				if(t.length<2)
				{
					return null;
				}
				colName.put(aMutlineName+"+"+t[0],aMutlineName+t[1]);
			}
			temp = (String)colName.get(aMutlineName+"+"+aColName);
		}else
		{
			temp =null;
		}
	}
	return temp;
}
/**
* getColDataByName
*	参数 request 对应jsp页面的请求对象，aMutlineName 对应mutline对象的名字，aColName 对应列的名字
*
*	返回列名对应的列的值数组，没有该列名则返回空
*/
private String [] getColDataByName(HttpServletRequest request,String aMutlineName,String aColName)
{
	String colName ;
	colName = getColName(request,aMutlineName,aColName);
	if(!(colName==null||colName.equals("")))
	{
	 return	request.getParameterValues(colName);
	}
	return null;
}
%>