<!--
*******************************************************
* 程序名称：UsrCheck.jsp
* 程序功能：用户信息校验页面
* 创建日期：2002-11-25
* 更新记录：  更新人    更新日期     更新原因/内容
*******************************************************
-->
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.utility.*"%>
<%
try
{
	//System.out.println("request.getContextPath();"+request.getContextPath());
	
	String Crs_IP = new ExeSQL().getOneValue("select Sysvarvalue from LDSYSVAR WHERE sysvar ='Crs_IP'");
	String IP =  request.getServerName();
	//System.out.println("UsrCheck++IP:"+IP); 
	//System.out.println("UsrCheck++session:"+session); 
	if (session == null) {
		//System.out.println("session is null");
		if(!Crs_IP.equals(IP)){
		//System.out.println("UsrCheck.jsp+非集团交叉用户登录时执行此方法");
        %>
        <script src="../common/javascript/Common.js"></script>
        <script language=javascript>
		try
		{   
			
			try{
		    	var win = searchMainWindow(top.window);
		    }catch(e){
		    	alert(e.message);
		    	alert(win);
		    }
		    
		    var url = "../indexlis.jsp";
		    
		    if(win != null ){
		    	try{
		    		url = win.location;
		    	}catch(e){
		    		alert(url);
		    		url = "../indexlis.jsp";
		    	}
		    }
		   
		    top.window.location = url;
		 	
		}
		catch (exception)
		{
			top.window.location ="../indexlis.jsp";
		}
		</script>
        <%
		return;
		}else{
		//System.out.println("UsrCheck.jsp+集团交叉用户登录时执行此方法");
		%>
		<script language=javascript>
			alert("网页超时，请您重新登录!!");
		</script>
		<%
		}
	}//session为空判断完毕	
	GlobalInput tG1 = (GlobalInput)session.getValue("GI");
	if (tG1 == null) {
		System.out.println("GlobalInput is null");
		session.putValue("GI",null);
		out.println("网页超时，请您重新登录");
		if(!Crs_IP.equals(IP)){
		%>
		<script src="../common/javascript/Common.js"></script>
		<script language=javascript>
		try
		{
			try{
		    	var win = searchMainWindow(top.window);
		    }catch(e){
		    	alert(e.message);
		    	alert(win);
		    }
		    
		    var url = "../indexlis.jsp";
		    
		    if(win != null ){
		    	try{
		    		url = win.location;
		    	}catch(e){
		    		alert(url);
		    		url = "../indexlis.jsp";
		    	}
		    }
		    top.window.location = url;
		}
		catch (exception)
		{
			top.window.location ="../indexlis.jsp";
		}
		</script>
		<%
		return;
		}else{
		%>
		<script language=javascript>
			alert("网页超时，请您重新登录!");
		</script>
		<%
		}
	}//GlobalInput为空判断完毕
	String  userCode = tG1.Operator;
	String comCode =tG1.ComCode;
	String manageCom = tG1.ManageCom;
	if ((userCode.length()==0) || (userCode.compareTo("")==0)||(comCode.length()==0) || (comCode.compareTo("")==0) ||(manageCom.length()==0) || (manageCom.compareTo("") == 0))
	{
		System.out.println("用户名机构代码等信息 is null");
		session.putValue("GI",null);
		String ContentErr = " 请您重新登录！";
		System.out.println(ContentErr);
		if(!Crs_IP.equals(IP)){
		%>
		<script src="../common/javascript/Common.js"></script>
		<script language=javascript>
		try
		{
			try{
		    	var win = searchMainWindow(top.window);
		    }catch(e){
		    	alert(e.message);
		    	alert(win);
		    }
		    
		    var url = "../indexlis.jsp";
		    
		    if(win != null ){
		    	try{
		    		url = win.location;
		    	}catch(e){
		    		alert(url);
		    		url = "../indexlis.jsp";
		    	}
		    }
		    top.window.location = url;
		}
		catch (exception)
		{
			top.window.location ="../indexlis.jsp";
		}
		</script>
		<%
		return;
		}else{
		%>
		<script language=javascript>
			alert("网页超时，请您重新登录!!");
		</script>
		<%
		}
	}
	
	
	%>
	<script language=javascript>
	try
	{
		var exUserCode = parent.EX.fm.txtUserCode.value;
		var exComCode = parent.EX.fm.txtComCode.value;
		var exManageCom = parent.EX.fm.txtManageCom.value;
		if(exUserCode!="" && exUserCode!=null && exComCode!="" && exComCode!=null && exManageCom!="" && exManageCom!=null)
		{
			if(exUserCode!="<%=tG1.Operator%>" || exComCode!="<%=tG1.ComCode%>" || exManageCom!="<%=tG1.ManageCom%>"){
				alert("对不起，请您不要同时打开两个系统，请重新登录！！");
				top.window.location ="../indexlis.jsp";
			}
		}
	}
	catch(error)
	{
	}	
	</script>
	<%
}
catch(Exception exception)
{
	String ContentErr = " exception:请您重新登录！";
	System.out.println(ContentErr);
	//out.println("网页超时，请您重新登录！！！");
%>
<script language=javascript>
url = win.location;
</script>
<%
	return;
}
%>