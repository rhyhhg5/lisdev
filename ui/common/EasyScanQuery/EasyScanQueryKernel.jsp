<%
//程序名称：EasyScanQueryKernel.jsp
//程序功能：EasyScanQuery查询功能的核心函数
//创建日期：2002-11-07
//创建人  ：胡博
//更新记录：  更新人    更新日期     更新原因/内容       
%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>

<%!
// 调用EasyScanQueryUI进行单证印刷号PrtNo提交和数据库查找，返回结果字符串strResult
public String[] easyScanQueryKernel(String ptrNo,String BussNoType,String BussType,String SubType,String clientUrl) { 
  String[] arrResult;
  
  if (ptrNo == null || ptrNo.equals("") || ptrNo.equals("undefined")) {
    System.out.println("EasyScanQuery don't accept ptrNo!");
    return null;
  }

  EasyScanQueryUI tEasyScanQueryUI = new EasyScanQueryUI();
  VData tVData = new VData();
  VData tVData1 = new VData();
  tVData.add(ptrNo);
  tVData.add(BussNoType);
  tVData.add(BussType);
  tVData.add(SubType);
  tVData.add(clientUrl);					//LQ 2004-04-20
  tEasyScanQueryUI.submitData(tVData, "QUERY||MAIN");

  if(tEasyScanQueryUI.mErrors.needDealError()) {
	  System.out.println("EasyScanQueryUI throw errors:" + tEasyScanQueryUI.mErrors.getFirstError());
	  return null;
  } else {
	  tVData.clear() ;
	  tVData = tEasyScanQueryUI.getResult() ;
	  tVData1 = (VData)tVData.get(0);	  
    arrResult = new String[tVData1.size()];
    for (int i=0; i<tVData1.size(); i++) {
      arrResult[i] = (String)tVData1.get(i);
      //System.out.println(arrResult[i]);
    }
	  
  }
  
  return arrResult;
}
//另外一种查寻方法
public String[] easyScanQueryKernel2(String ptrNo,String BussNoType,String BussType,String clientUrl) { 
  String[] arrResult;
  
  if (ptrNo == null || ptrNo.equals("") || ptrNo.equals("undefined")) {
    System.out.println("EasyScanQuery don't accept ptrNo!");
    return null;
  }

  EasyScanQueryUI tEasyScanQueryUI = new EasyScanQueryUI();
  VData tVData = new VData();
  tVData.add(ptrNo);
  tVData.add(BussNoType);
  tVData.add(BussType);
  tVData.add(clientUrl);					//LQ 2004-04-20
  tEasyScanQueryUI.submitData(tVData, "QUERY||MAIN");

  if(tEasyScanQueryUI.mErrors.needDealError()) {
	  System.out.println("EasyScanQueryUI throw errors:" + tEasyScanQueryUI.mErrors.getFirstError());
	  return null;
  } else {
	  tVData.clear() ;
	  tVData = tEasyScanQueryUI.getResult();
	  System.out.println("涂强"+tVData.size());
	  VData mData = new VData();
	  mData = (VData)tVData.get(0);
	  System.out.println("涂强"+mData.size());
    arrResult = new String[mData.size()];
    for (int i=0; i<mData.size(); i++) {
      arrResult[i] = (String)mData.get(i);
      //System.out.println(arrResult[i]);
    }
	  
  }
  
  return arrResult;
}

/**支持查询多种单证的方法 
  prtNo：单证号码
  BussNoType：单证号码类型
  BussType：单证类型
  SubType：单证子类型
  clientUrl：客户端地址
  Order：查询到的单证扫描页的排序方式 
      if(Order == null || Order.equals("") || Order.toLowerCase().equals("asc"))
        升序
      else
        降序
  
  其中BussNoType、BussType、SubType必须一一对应
  by Yang Yalin  2005-09-29
*/
public String[] easyScanQueryKernelMuli(String ptrNo, String[] BussNoType, 
  String[] BussType, String[] SubType, String clientUrl, String Order)
{
  String[] arrResult;
  
  TransferData data = new TransferData();
  data.setNameAndValue("BussNo", ptrNo);
  data.setNameAndValue("BussNoType", BussNoType);
  data.setNameAndValue("BussType", BussType);
  data.setNameAndValue("SubType", SubType);
  data.setNameAndValue("Order", Order);
  
  VData tVData = new VData();
  tVData.add(data);
  
  EasyScanQueryMultiUI ui = new EasyScanQueryMultiUI();
  if(!ui.submitData(tVData, "QUERY||MAIN"))
  {
    System.out.println(ui.mErrors.getErrContent());
    
    return null;
  }
  else 
  {
	  tVData.clear() ;
	  tVData = ui.getResult() ;
	  tVData = (VData)tVData.get(0);
	  System.out.println(tVData.size());
    arrResult = new String[tVData.size()];
    
    for (int i=0; i<tVData.size(); i++) 
    {
      arrResult[i] = (String)tVData.get(i);
    }
  }
  
  return arrResult;
}
/**
* 针对理赔账单录入检录根据理赔号查询调查扫描件和理赔申请书
*/
public String[] easyScanQueryKernelMulilp(String[] ptrNo, String BussNoType, 
		  String BussType, String[] SubType, String clientUrl, String Order)
{
  String[] arrResult;
  
  TransferData data = new TransferData();
  data.setNameAndValue("BussNo", ptrNo);
  data.setNameAndValue("BussNoType", BussNoType);
  data.setNameAndValue("BussType", BussType);
  data.setNameAndValue("SubType", SubType);
  data.setNameAndValue("Order", Order);
  
  VData tVData = new VData();
  tVData.add(data);
  
  EasyScanQueryMultilpUI ui = new EasyScanQueryMultilpUI();
  if(!ui.submitData(tVData, "QUERY||MAIN"))
  {
    System.out.println(ui.mErrors.getErrContent());
    
    return null;
  }
  else 
  {
	  tVData.clear() ;
	  tVData = ui.getResult() ;
	  tVData = (VData)tVData.get(0);	  
    arrResult = new String[tVData.size()];
    
    for (int i=0; i<tVData.size(); i++) 
    {
      arrResult[i] = (String)tVData.get(i);
    }
  }
  
  return arrResult;
}
public String[] easyScanQueryKernelEasyway(String DocID,String DocCode,String BussType,String SubType,String clientUrl) { 
  String[] arrResult;
  
  if (DocCode == null || DocCode.equals("") || DocCode.equals("undefined")) {
    System.out.println("EasyScanQuery don't accept ptrNo!dddddd");
    return null;
  }

  EasyScanQueryEasywayUI tEasyScanQueryEasywayUI = new EasyScanQueryEasywayUI();
  VData tVData = new VData();
  VData tVData1 = new VData();
  tVData.add(DocID);
  tVData.add(DocCode);
  tVData.add(BussType);
  tVData.add(SubType);
  tVData.add(clientUrl);					//DJB 2005-12-30
  tEasyScanQueryEasywayUI.submitData(tVData, "QUERY||MAIN");

  if(tEasyScanQueryEasywayUI.mErrors.needDealError()) {
	  System.out.println("tEasyScanQueryEasywayUI throw errors:" + tEasyScanQueryEasywayUI.mErrors.getFirstError());
	  return null;
  } else {
	  tVData.clear() ;
	  tVData = tEasyScanQueryEasywayUI.getResult() ;
	  System.out.println(tVData);
	  tVData1 = (VData)tVData.get(0);	  
    arrResult = new String[tVData1.size()];
    for (int i=0; i<tVData1.size(); i++) {
      arrResult[i] = (String)tVData1.get(i);
      //System.out.println(arrResult[i]);
    }
	  
  }
  
  return arrResult;
}

%>


