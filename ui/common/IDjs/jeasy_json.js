﻿/******************************************************************
 * 概述：jeasy_json 对象定义
 * 描述：
 * 版本：V1.0.0.0-alpha
 * 历史：
 *     1）初始化版本
******************************************************************/

///imagejson参数定义：
//-------------------------------------------------------------------
//初始化查询对象
var imagejson= {
	"Url": "http://ip:port/ImageCache/writeFile",///影像上传服务URI地址。
	//----------------
	"OperType":"00000",
	"FileType":"LP",
	"FileCode":"025",
	"FilePage": "00002014012200002",
    "MainFileType":"",
    "FileFormat": "",
    "AgentCode": "",
    "ChangeId": "",
    "ServiceId": "",
    "PolicyCode": "",
    "SubFileCode": "",
    "OrgCode": "",
    "OperUser": "",
    "Priority": ""

}

///indexjson 参数定义：
//-------------------------------------------
var indexjson = {
	"Url": "http://10.135.100.160:7002/life/axis_services/MultimediaFileService",//WebService地址
	"Cmd": 1,//WebService的调用接口的第一个参数
	//-----------------------------------------------------------
	"policyId": "1111",
	"mainFileType": "",
	"subFileCode": "",
	"seqNumber": "",
	"empId": "",
	"organId": "",
	"fileCode": "00002014012200002",
	"groupPolicyId": "",
	"caseId": "",
	"changeId": 0,
	"serviceId": 0,
	"policyCode": "",
	"applyCode": "",

	"agentCode": "",
	"agencyCode": "",
	"priority": "",
	"actionType": "",
	"fileFormat": "",
	"fileName": "",
	"fileKey": "xxxxx"
}
