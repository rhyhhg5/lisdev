<%@page import="java.math.BigDecimal"%>
<%@page import="java.math.BigInteger"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<%@page import="com.sinosoft.lis.pubfun.AgentPubFun"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.ExeSQL"%>
<%
String strResult = "";
//request.setCharacterEncoding("UTF8");//设置request的接受字符集
String strSql = request.getParameter("strSql");
strSql = strSql.replaceAll("%", "#xi#jia#hui#");
strSql = strSql.replaceAll("@xi@jia@hui@", "%u");
strSql = unescape(strSql);
strSql = strSql.replaceAll("#xi#jia#hui#", "%");
try
{
    strResult = easyQuery(strSql);
}
catch(Exception ex)
{
    ex.printStackTrace();
    System.out.println("easyQueryVer4 throw Errors!\n" + "easyQuerySql:" + strSql);
}
%>
<%!
public String unescape(String src)
{
    StringBuffer tmp = new StringBuffer();
    tmp.ensureCapacity(src.length());
    int lastPos = 0, pos = 0;
    char ch;
    while(lastPos < src.length())
    {
        pos = src.indexOf("%", lastPos);
        if(pos == lastPos)
        {
            if(src.charAt(pos + 1) == 'u')
            {
                ch = (char) Integer.parseInt(src.substring(pos + 2, pos + 6), 16);
                tmp.append(ch);
                lastPos = pos + 6;
            }
            else
            {
                ch = (char) Integer.parseInt(src.substring(pos + 1, pos + 3), 16);
                tmp.append(ch);
                lastPos = pos + 3;
            }
        }
        else
        {
            if(pos == -1)
            {
                tmp.append(src.substring(lastPos));
                lastPos = src.length();
            }
            else
            {
                tmp.append(src.substring(lastPos, pos));
                lastPos = pos;
            }
        }
    }
    return tmp.toString();
}

public String easyQuery(String strSql)
{
    if("getCurrentDate".equalsIgnoreCase(strSql))
   {
       return PubFun.getCurrentDate();
    }
   else if("getCurrentTime".equalsIgnoreCase(strSql))
    {
        return PubFun.getCurrentTime();
    }
    else if("getCurrentYM".equalsIgnoreCase(strSql))
    {
       return AgentPubFun.formatDate(PubFun.getCurrentDate(),"yyyyMM");
    }
//    else if("getTaskEngineState".equalsIgnoreCase(strSql))
//    {
//        return String.valueOf(TaskService.getEngineState());
//    }
    else if(strSql.startsWith("EasyQuery"))
    {
        String b = strSql.substring("EasyQuery".length()+1);
        String a = new ExeSQL().getEncodedResult(b);
        return a;
    }
   else if(strSql.startsWith("calLastMonth"))
    {
        System.out.println(strSql);
       String a = AgentPubFun.formatDate(PubFun.calDate(strSql.substring("calLastMonth".length()+1)+"01", -1, "M", ""),"yyyyMM");
       System.out.println(a);
        return null;
    }
    else if(strSql.startsWith("calNextMonth"))
    {
        String a = AgentPubFun.formatDate(PubFun.calDate(strSql.substring("calNextMonth".length()+1)+"01", 1, "M", ""),"yyyyMM");
        System.out.println(a);
        return a;
   }
    else if(strSql.startsWith("BigIntegerAdd"))
    {
        String strSqlPara[] = strSql.substring("BigIntegerAdd".length()+1).split("\\+");
        BigInteger a = new BigInteger(strSqlPara[0]);
        for (int i = 1 ; i < strSqlPara.length ; i++ )
        {
            a = a.add(new BigInteger(strSqlPara[i]));
        }
        return a.toString();
    }
    else if(strSql.startsWith("BigIntegerSubtract"))
    {
        String strSqlPara[] = strSql.substring("BigIntegerSubtract".length()+1).split("-");
        BigInteger a = new BigInteger(strSqlPara[0]);
        for (int i = 1 ; i < strSqlPara.length ; i++ )
        {
            a = a.subtract(new BigInteger(strSqlPara[i]));
        }
        return a.toString();
    }
    else if(strSql.startsWith("BigIntegerCompare"))
    {
        String strSqlPara[] = strSql.substring("BigIntegerCompare".length()+1).split(":");
        return String.valueOf(new BigInteger(strSqlPara[0]).compareTo(new BigInteger(strSqlPara[1])));
    }
    else if(strSql.startsWith("BigDecimalAdd"))
    {
        String strSqlPara[] = strSql.substring("BigDecimalAdd".length()+1).split("\\+");
        BigDecimal a = new BigDecimal(strSqlPara[0]);
        for (int i = 1 ; i < strSqlPara.length ; i++ )
        {
            a = a.add(new BigDecimal(strSqlPara[i]));
        }
        return a.toString();
    }
    else if(strSql.startsWith("BigDecimalSubtract"))
    {
        String strSqlPara[] = strSql.substring("BigDecimalSubtract".length()+1).split("-");
        BigDecimal a = new BigDecimal(strSqlPara[0]);
        for (int i = 1 ; i < strSqlPara.length ; i++ )
        {
            a = a.subtract(new BigDecimal(strSqlPara[i]));
        }
        return a.toString();
    }
    else if(strSql.startsWith("BigDecimalCompare"))
    {
        String strSqlPara[] = strSql.substring("BigDecimalCompare".length()+1).split(":");
        return String.valueOf(new BigDecimal(strSqlPara[0]).compareTo(new BigDecimal(strSqlPara[1])));
    }
//   else if(strSql.startsWith("getDownManageCom"))
//    {
//        String result = GetManageComLimit.parseManageComLimitlike(strSql.substring("getDownManageCom".length()+1));
//        result = result.replace('\'','#');
//        return result;
//    }
    else
    {
    	return new ExeSQL().getOneValue(strSql);
    }
}
%>
<%=strResult%>
