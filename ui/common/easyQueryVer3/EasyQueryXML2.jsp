-<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.io.*"%>
<%@page import="javax.servlet.*"%>
<%@page import="javax.servlet.http.*"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="./EasyQueryKernel.jsp"%>


<%

//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//程序名称：EasyQueryXML.jsp
//程序功能：查询等待页面，负责调用后台查询，并接收返回结果
//创建日期：2005-6-3 8:48
//创建人  ：涂强
//更新记录：  更新人    更新日期     更新原因/内容
response.setHeader("Pragma","No-cache");
response.setHeader("Cache-Control","no-cache");
response.setDateHeader("Expires", 0);
String strResult = "";

String tReferer = request.getHeader("Referer");//获得申请此页的url信息
System.out.println(request.getHeader("Referer"));
String tHost = "http://" + request.getHeader("host");//获取应用的url信息
System.out.println(tHost);

//校验请求此页面的url是否是应用框架内的页面
if(tReferer == null || !tReferer.startsWith(tHost))
{
	System.out.println("不是一个地址的请求服务");
%>
<script language=jscript.encode>
session = null;
try
{
	CollectGarbage();
}
catch(ex)
{
	alert(ex.message);
}
top.window.location ="../../indexlis.jsp";
</script>  
<%
}
else
{
	//设置request的接受字符集
	String strSql = request.getParameter("strSql");
	//System.out.println("strSql is :~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+strSql);
	String strStart = request.getParameter("strStart");
	
	

  	EasyQuerySql tEasyQuerySql=new EasyQuerySql();
  	if (tEasyQuerySql.parsePara(strSql))
  	{
  	//System.out.println("EASYQUERY RAW SQL==="+strSql);
  	String jspName=tEasyQuerySql.getJspName();
  	//System.out.println("EASYQUERY JSP'S Name IS===="+jspName);
  	//System.out.println("sql ID *********" +tEasyQuerySql.getSqlId());
  	request.setAttribute("EASYQUERYSQLID",tEasyQuerySql.getSqlId());
  	for (int i = 0; i < tEasyQuerySql.getParaCount(); i++) 
  	{  	
  	    //System.out.println(tEasyQuerySql.getParaName(i).toUpperCase());
  	    //System.out.println(tEasyQuerySql.getParaValue(i));
  		request.setAttribute(tEasyQuerySql.getParaName(i).toUpperCase(),tEasyQuerySql.getParaValue(i));
  	}
  %>
  <jsp:include page="<%= jspName%>" />

  <%
  	strSql=(String)request.getAttribute("EASYQUERYSQL"); 
  	//strSql = tEasyQuerySql.convertToValue() ;
  	//System.out.println(strSql);
  	strSql=tEasyQuerySql.convertToValue(strSql);
  	//System.out.println("===EASYQUERY CONVERT SQL==="+strSql);

  %>

  <%
  //add by mojiao end

	
	try
	{
	    //System.out.println("StrSql is " + strSql+"*******************");
		strResult = easyQueryKernel(strSql, strStart,"");
		//System.out.println(strResult+"+==++++++++++++++++++++++++==");
	}
	catch(Exception ex)
	{
		System.out.println("easyQueryKernel throw Errors!\n" + "easyQuerySql:" + strSql +"\nStartRecord:" +strStart);
	}
	
	try
	{
		//做了一步特殊字符替换，可否考虑先判定是否含有特殊字符，然后再作处理
		//对于有回车的数据取出的可能性太小了
		if(strResult.indexOf("\"")!= -1 || strResult.indexOf("'")!= -1 || strResult.indexOf("\n")!= -1)
		{
			//String strPath = getRealPath(request,"config//Conversion.config");
			//strResult = StrTool.Conversion(strResult, strPath);
			System.out.println("完蛋了  我被执行了 改吧");
		}
	}
	catch(Exception ex)
	{
		System.out.println("not found Conversion.config ");
	}
	}
}
%>
<%=strResult%>
