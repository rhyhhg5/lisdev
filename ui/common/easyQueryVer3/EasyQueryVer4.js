function easyQueryVer4(strSql)
{
	var strQueryResult;
	var strDate = Date.parse(new Date());
	var tStrSql = strSql.toLowerCase();
	if (tStrSql.indexOf("where") != -1)
	{
		strSql = strSql.replace("where", "where '" + strDate + "'='" + strDate + "' and ");
	}
	strSql = escape(strSql);
	strSql = strSql.replace(/%u/g,"@xi@jia@hui@");
   	//解决=、+、?号在JS和JSP之间的传递问题
	while(strSql.indexOf("=") != -1)
	{
		strSql = strSql.replace("=", "%3D");
	}
	while(strSql.indexOf("?") != -1)
	{
		strSql = strSql.replace("?", "%3F");
	}
	while(strSql.indexOf("+") != -1)
	{
		strSql = strSql.replace("+", "%2B");
	}
	var strURL = "../common/easyQueryVer3/EasyQueryXmlVer4.jsp";
	var strPara = "strSql=" + strSql;
	Request = new ActiveXObject("Microsoft.XMLHTTP");
	Request.open("POST",strURL, false);
	Request.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	Request.send(strPara);
	try
	{
		strQueryResult = Request.responseText.trim();
	}
	catch(e1)
	{
		alert("数据返回出错："+e1.message);
	}
	return strQueryResult;
}
/**此函数登陆时使用**/
function getComCodeByUserID(userID)
{
	return easyQueryVer4("select comcode from lduser where usercode = '"+userID+"'");
}

function getCurrentDate()
{
	return easyQueryVer4("getCurrentDate");
}

function getCurrentTime()
{
	return easyQueryVer4("getCurrentTime");
}

function getCurrentYM()
{
	return easyQueryVer4("getCurrentYM");
}

function BigIntegerAdd(x,y)
{
	return easyQueryVer4("BigIntegerAdd:"+x+"+"+y)
}

function BigIntegerSubtract(x,y)
{
	return easyQueryVer4("BigIntegerSubtract:"+x+"-"+y)
}

function BigIntegerCompare(x,y)
{
	return easyQueryVer4("BigIntegerCompare:"+x+":"+y)
}

function BigDecimalAdd(x,y)
{
	return easyQueryVer4("BigDecimalAdd:"+x+"+"+y)
}

function BigDecimalSubtract(x,y)
{
	return easyQueryVer4("BigDecimalSubtract:"+x+"-"+y)
}

function BigDecimalCompare(x,y)
{
	return easyQueryVer4("BigDecimalCompare:"+x+":"+y)
}

//function getDownManageCom(x)
//{
//	return easyQueryVer4("getDownManageCom:"+x)
//}

function calLastMonth(x)
{
	return easyQueryVer4("calLastMonth:"+x)
}

function calNextMonth(x)
{
	return easyQueryVer4("calNextMonth:"+x)
}

//function getTaskEngineState()
//{
//	return easyQueryVer4("getTaskEngineState")
//}

function getEasyQuery(x)
{
	var arrVer4 = easyQueryVer4("EasyQuery:"+x);
   if (arrVer4.substring(0, arrVer4.indexOf("|")) != "0") {
   return null;
   }else{
	var arr = decodeEasyQueryResultV4(arrVer4);
   return arr;
  }

}

function decodeEasyQueryResultV4(strResult, notUseEasyQuery, notUseTurnPage, otherTurnPage)
{
	var arrEasyQuery = new Array();
	var arrRecord = new Array();
	var arrField = new Array();
	var recordNum, fieldNum, i, j;
	var cTurnPage;
	if (typeof(otherTurnPage)=="object")
	{
		cTurnPage = otherTurnPage;
	}
	else
	{
		try
		{
			cTurnPage = turnpage;
		}
		catch(e)
		{}
	}
	if (typeof(strResult) == "undefined" || strResult == "" || strResult == false)
	{
		return null;
	}
	//公用常量处理，增强容错性
	if (typeof(RECORDDELIMITER) == "undefined") RECORDDELIMITER = "^";
	if (typeof(FIELDDELIMITER) == "undefined") FIELDDELIMITER = "|";
	try
	{
		arrRecord = strResult.split(RECORDDELIMITER);	//拆分查询结果，得到记录数组
		//特殊处理，查询结果字符串首位存有所有满足条件记录总数信息
		if ((typeof(notUseTurnPage)=="undefined" || notUseTurnPage==0) && typeof(cTurnPage)=="object")
		{
			cTurnPage.queryAllRecordCount = arrRecord[0].substring(arrRecord[0].indexOf(FIELDDELIMITER) + 1);
		}
		if(typeof(notUseEasyQuery)=="undefined" || notUseEasyQuery=="" || notUseEasyQuery==0)
		{
			if (arrRecord[0].substring(arrRecord[0].indexOf(FIELDDELIMITER) + 1) != "" && arrRecord[0].substring(arrRecord[0].indexOf(FIELDDELIMITER) + 1) == 0)
			{
				return null;
			}
			arrRecord.shift();
		}
		recordNum = arrRecord.length;
//		for(i=0; i<recordNum; i++)
		var i=0;
		while(i<recordNum)
		{
			arrField = arrRecord[i].split(FIELDDELIMITER);	//拆分记录，得到字段数组
			fieldNum = arrField.length;
			arrEasyQuery[i] = new Array();
//			for(j=0; j<fieldNum; j++)
			var j=0;
			while(j<fieldNum)
			{
				arrEasyQuery[i][j] = arrField[j];	//形成以行为记录，列为字段的二维数组
				j++;
			}
			i++;
		}
	}
	catch(ex)
	{
		alert("拆分数据失败！错误原因是：" + ex);
		return null;
	}
	return arrEasyQuery;
}
