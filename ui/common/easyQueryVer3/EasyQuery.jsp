<%@page contentType="text/html;charset=GBK" %>
<%
response.setHeader("Pragma","No-cache");
response.setHeader("Cache-Control","no-cache");
response.setDateHeader("Expires", 0);
%>
<%
//程序名称：EasyQuery.jsp
//程序功能：EasyQuery查询功能
//创建日期：2005-12-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容       
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.query.*"%>
<%
  String output = "";
  String sql = request.getParameter("strSql");
  String start = request.getParameter("strStart");
  String pageRowNum = request.getParameter("pageRowNum");
  String codeType = request.getParameter("CodeType");
  System.out.println(codeType);
  Map paramMap = ParseParams.parseCodeType(codeType);
  EasyQueryUI tEasyQueryUI = new EasyQueryUI(sql, start, pageRowNum, paramMap);
  if (tEasyQueryUI.submitData())
  {
    output = "#S#"+tEasyQueryUI.getResult()+"#E#";
  }
%>
<%=output%>