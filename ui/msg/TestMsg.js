/*********************************************************************
 *  选择短信类型后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect(cCodeName, Field) {
	if(cCodeName == "MsgType") {  
	  showTypeInput(Field.value);
	}
}
function showTypeInput(type) {
  for (i=0; i<2; i++) {
    if ((i+1) == type) {
      fm.all("CustmernoType" + (i+1)).style.display = '';
      fm.all("CustmerType" + (i+1)).style.display = '';
    } 
    else {
      fm.all("CustmernoType" + (i+1)).style.display = 'none';
      fm.all("CustmerType" + (i+1)).style.display = 'none';
    }
  
  } 
}

//提交，发送按钮对应操作
function submitForm() {

	  if (fm.all("mobile").value == ""&&fm.all("to").value == "")
	  {
	  	alert("请输入手机号码和邮件地址");
	  	return;
	  }
	  if (fm.all("content").value == "")
	  {
	  	alert("请输入要发送的信息内容")
	  	return;
	  }
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
    window.focus;
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
  }
  
}

//查询客户信息
function QueryCustomer(){
	var num = fm.all("Customerno").value;
	if (fm.all("MsgTypeName").value=='客户通知')
	{
	  if (fm.all("Customerno").value != "" && num.length==9)
	  {
	  	var sql = "select a.Customerno,a.name, b.mobile,b.email from ldperson a,LCAddress b where a.Customerno = b.Customerno "
	  	           + "and a.Customerno = '"+fm.all("Customerno").value+"'";
      arrResult=easyExecSql( sql,1,0 );
      afterQuery(arrResult);
	  }else if (fm.all("Customerno").value != "" && num.length==8) 
	  {
	  	var sql = "select a.Customerno,a.name, b.mobile,b.Email from ldperson a,LCGrpAddress b where a.Customerno = b.Customerno "
	  	           + "and a.Customerno = '"+fm.all("Customerno").value+"'";	  
      arrResult=easyExecSql( sql,1,0 );
      afterQuery(arrResult);
	  }else if (fm.all("Customerno").value == "")
	  {
       window.open("./LDPersonMsgQuery.html");
	  	
	  }
	}
	else if(fm.all("MsgTypeName").value=='业务员通知')
	{
		if (fm.all("Customerno").value != "")
		{
		  var sql = "select * from laagent where agentcode = '"+fm.all("Customerno").value+"'";
		  arrResult=easyExecSql( sql,1,0 );
		  afterQuery2(arrResult);
	  }else if (fm.all("Customerno").value == "")
	  {
       var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom=86","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
		}		
	}
	else if(fm.all("MsgTypeName").value=="")
	{
	  alert("请选择短信类型");
	  return;
	}
}

//客户信息赋值
function afterQuery( arrResult )
{
		// 初始化表格
		initInpBox();
		// 显示查询结果
	 if (arrResult == null){
	 	alert("没有查到该客户号对应的信息");	
	 	return;
	 	}	
   fm.all('CustomerNo').value= arrResult[0][0]; 
   fm.all('Customer').value = arrResult[0][1];
   fm.all('mobile').value = arrResult[0][2];
   fm.all('to').value = arrResult[0][3];                    
}	
//业务员信息赋值
function afterQuery2( arrResult )
{
		// 初始化表格
		initInpBox();
		// 显示查询结果
	 if (arrResult != null){
   fm.all('CustomerNo').value= arrResult[0][0]; 
   fm.all('Customer').value = arrResult[0][5];
   fm.all('mobile').value = arrResult[0][18];
   fm.all('to').value = arrResult[0][19];   
  }else{
  		 	alert("没有查到该客户号对应的信息");	
	 	return;
  	}                 
}
function MsgQuery()	 {
	alert("待完成...");
	}
    	

