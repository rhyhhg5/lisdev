<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AskGroupTrackManuChk.jsp
//程序功能：补充资料回收
//创建日期：2005-03-04 11:10:36
//创建人  ：tuq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.ask.*"%>

<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  try
 { 
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
  String wFlag = "";
  VData tVData = new VData();
	LCGrpContSet tLCGrpContSet = new LCGrpContSet();
	LCGCUWMasterSet tLCGCUWMasterSet = new LCGCUWMasterSet();  //询价合同号

	String MissionID = request.getParameter("MissionID");
	String SubMissionID = request.getParameter("SubMissionID");
	String GrpContNo = request.getParameter("ContNo");
	String CertifyCode = request.getParameter("CertifyCode");
	String CertifyNo = request.getParameter("CertifyNo");
	String TakeBackOperator = request.getParameter("TakeBackOperator");
	String TakeBackDate = request.getParameter("TakeBackDate");
	boolean flag = false;
	if (!GrpContNo.equals(""))
	{     
	      
	      TransferData mTransferData = new TransferData();
        LZSysCertifySchema tLZSysCertifySchema = new LZSysCertifySchema();
        tLZSysCertifySchema.setTakeBackOperator(TakeBackOperator);
        tLZSysCertifySchema.setTakeBackMakeDate(TakeBackDate);
        mTransferData.setNameAndValue("ProposalGrpContNo",GrpContNo);
        mTransferData.setNameAndValue("GrpContNo",GrpContNo);
        mTransferData.setNameAndValue("LZSysCertifySchema",tLZSysCertifySchema);
        mTransferData.setNameAndValue("MissionID", MissionID);
        mTransferData.setNameAndValue("SubMissionID", SubMissionID);
        mTransferData.setNameAndValue("CertifyCode", CertifyCode);
        mTransferData.setNameAndValue("CertifyNo", CertifyNo);
		    tVData.add(tG);
		    tVData.add(mTransferData);
			  flag = true;
			  		if(CertifyCode.equals("6007"))
				{ 
			   wFlag = "0000006010";
			}else if(CertifyCode.equals("6016"))
			{ 
			   wFlag = "0000006022";
			}
	}
	else
	{
	    FlagStr = "Fail";
	    Content = "号码传输失败!";
	}
	
  	if (flag == true&&!wFlag.equals(""))
  	{
		
		// 数据传输 == 
		AskWorkFlowUI tAskWorkFlowUI   = new AskWorkFlowUI();
		if (tAskWorkFlowUI.submitData(tVData,wFlag)==false)
		{
			//int n = tAskWorkFlowUI.mErrors.getErrorCount();
			//for (int i = 0; i < n; i++)
			//Content = " 自动核保失败，原因是: " + tAskWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tAskWorkFlowUI.mErrors;
		    //tErrors = tAskWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                     
		    	Content = " 人工核保成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                              
		    {
		    	Content = " 人工核保失败，原因是:";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
			}
		    	FlagStr = "Fail";
		    }
		}
	}
}
catch(Exception e)
{
  System.out.println(e.toString());
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.close();
	this.close();
</script>
</html>
