<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html> 
<%
//程序名称：UWApp.jsp
//程序功能：既往投保信息查询
//创建日期：2002-06-19 11:10:36
//创建人  ： WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="AskUWApp.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AskUWAppInit.jsp"%>
  <title>既往投保信息 </title>
</head>
<body  onload="initForm('<%=tGrpContNo%>','<%=tAppntNo%>');" >
  <form method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 既往投保信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCDuty1);">
    		</td>
    		<td class= titleImg>
    			 既往询价信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCPol1" style= "display: ''" align = center>
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					<span id="spanPolGrid" >
					</span> 
				</td>
			</tr>
		</table>
		
      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();">
      </div>
      <INPUT type= "hidden" name= "ProposalNoHide" value= "">
      <INPUT type= "hidden" name= "ProposalNoHide2" value= "">
      
      <INPUT type= "hidden" name= "AppntNoHide" value= "">
      <hr></hr>
	
	
	<INPUT VALUE = "询价投保单明细信息" Class="cssButton" TYPE=button onclick= "showAskGrpCont();">
	
	<INPUT VALUE = "既往理赔信息" Class="cssButton" TYPE=hidden onclick= "ClaimGetQuery2();">
	<INPUT VALUE = "既往保全信息" Class="cssButton" TYPE=hidden  onclick= "Prt();">

	
	<hr></hr>
	<!--INPUT VALUE = "补充材料录入" Class="cssButton" TYPE=button onclick= "QuestInput();">
	<INPUT VALUE = "发补充材料通知书" Class="cssButton" TYPE=button onclick= "SendNotice();">
	<INPUT VALUE = "产品定价需求表" Class="cssButton" TYPE=button onclick= "showGrpCont();"-->				
	</div>
	<p>
        <INPUT VALUE="返  回" class= CssButton TYPE=button onclick="parent.close();"> 					
    </P>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
