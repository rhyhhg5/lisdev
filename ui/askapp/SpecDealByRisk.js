function specDealByRisk(){
	var chkNo=0;
	for (var index=0;index<DutyGrid.mulLineCount;index++){
	  if(DutyGrid.getChkNo(index)==true){
	    chkNo=chkNo+1;	
	  }	
	}
	if(DutyGrid.mulLineCount>0 && chkNo==0){
	  alert("请选择责任！");
	  return false;	
	}
	if(DutyGrid.mulLineCount>0)
	{
		//可以对险种条件校验
		var strChooseDuty="";
		for(i=0;i<=DutyGrid.mulLineCount-1;i++)
		{
			if(DutyGrid.getChkNo(i)==true)
			{
				strChooseDuty=strChooseDuty+"1";
				DutyGrid.setRowColData(i, 3, fm.all('InsuYear').value);//保险年期
				DutyGrid.setRowColData(i, 4, fm.all('InsuYearFlag').value);//保险年期单位
				if(DutyGrid.getRowColData(i,18)==null||DutyGrid.getRowColData(i,18)==""){ //界面不录入缴费间隔则取默认值
				  DutyGrid.setRowColData(i, 18, fm.all('PayIntv').value);//缴费间隔
				}
			}
			else
			{
				strChooseDuty=strChooseDuty+"0";
			}
		}
		//alert(strChooseDuty);
		//fm.all('StandbyFlag1').value=strChooseDuty;
		return true;		
	}
	if(fm.all('RiskCode').value=="212403"){
		var prem=0;
		var tPrem;
		var tRate=0;
		for(i=0;i<=PremGrid.mulLineCount-1;i++)
		{
			if(PremGrid.getChkNo(i)==true)
			{
				tPrem=PremGrid.getRowColData(i,4);
				tRate=parseFloat(PremGrid.getRowColData(i,9));
				if((tPrem==null||tPrem=="")&&tRate>0){
					var cprem=""+parseFloat(fm.all('Salary').value)*tRate+"";
					PremGrid.setRowColData(i,4,cprem);
				}
			}
		}
		for(i=0;i<=PremGrid.mulLineCount-1;i++)
		{
			if(PremGrid.getChkNo(i)==true)
			{
				prem=prem+parseFloat(PremGrid.getRowColData(i,4));
			}
		}
		fm.all('Prem').value=prem;
		//alert("prem:"+prem);
				
	}
}

//责任列表
function initDutyGrid()
{
    var iArray = new Array();
          
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";         			//列宽
        iArray[0][2]=10;          			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
				iArray[1][0]="责任编码";
				iArray[1][1]="0px";
				iArray[1][2]=100;
				iArray[1][3]=3;
				iArray[1][4]="DutyCode";
				iArray[1][15]="RiskCode";
				iArray[1][16]=fm.RiskCode.value;
				
				iArray[2]=new Array();
				iArray[2][0]="责任名称";
				iArray[2][1]="0px";
				iArray[2][2]=100;
				iArray[2][3]=3;
				
				iArray[3]=new Array();
				iArray[3][0]="保险年龄年期";
				iArray[3][1]="0px";
				iArray[3][2]=100;
				iArray[3][3]=3;
				
				iArray[4]=new Array();
				iArray[4][0]="保险年龄年期标志";
				iArray[4][1]="0px";
				iArray[4][2]=100;
				iArray[4][3]=3;
				
				iArray[5]=new Array();
				iArray[5][0]="终交年龄年期";
				iArray[5][1]="0px";
				iArray[5][2]=100;
				iArray[5][3]=3;
				
				iArray[6]=new Array();
				iArray[6][0]="终交年龄年期标志";
				iArray[6][1]="0px";
				iArray[6][2]=100;
				iArray[6][3]=3;
				
				iArray[7]=new Array();
				iArray[7][0]="领取年龄年期标志";
				iArray[7][1]="0px";
				iArray[7][2]=100;
				iArray[7][3]=3;
				
				iArray[8]=new Array();
				iArray[8][0]="领取年龄年期";
				iArray[8][1]="0px";
				iArray[8][2]=100;
				iArray[8][3]=3;
				
				iArray[9]=new Array();
				iArray[9][0]="备用属性字段1";
				iArray[9][1]="0px";
				iArray[9][2]=100;
				iArray[9][3]=3;
				
				iArray[10]=new Array();
				iArray[10][0]="备用属性字段2";
				iArray[10][1]="0px";
				iArray[10][2]=100;
				iArray[10][3]=3;
				
				iArray[11]=new Array();
				iArray[11][0]="备用属性字段3";
				iArray[11][1]="0px";
				iArray[11][2]=100;
				iArray[11][3]=3;
				
				iArray[12]=new Array();
				iArray[12][0]="计算方向";
				iArray[12][1]="0px";
				iArray[12][2]=100;
				iArray[12][3]=3;
				iArray[12][4]="PremToAmnt";
				
				
				iArray[13]=new Array();
				iArray[13][0]="实际保费";
				iArray[13][1]="0px";
				iArray[13][2]=100;
				iArray[13][3]=3;
				
				iArray[14]=new Array();
				iArray[14][0]="基本保额";
				iArray[14][1]="0px";
				iArray[14][2]=100;
				iArray[14][3]=3;
				
				iArray[15]=new Array();
				iArray[15][0]="份数";
				iArray[15][1]="0px";
				iArray[15][2]=100;
				iArray[15][3]=3;
				
				iArray[16]=new Array();
				iArray[16][0]="计算规则";
				iArray[16][1]="0px";
				iArray[16][2]=100;
				iArray[16][3]=3;
				iArray[16][4]="PolCalRule";
				
				iArray[17]=new Array();
				iArray[17][0]="费率/折扣";
				iArray[17][1]="0px";
				iArray[17][2]=100;
				iArray[17][3]=3;
				
				iArray[18]=new Array();
				iArray[18][0]="交费间隔";
				iArray[18][1]="0px";
				iArray[18][2]=100;
				iArray[18][3]=3;
				
				iArray[19]=new Array();
				iArray[19][0]="免赔额";
				iArray[19][1]="0px";
				iArray[19][2]=100;
				iArray[19][3]=3;

 				iArray[20]=new Array();
				iArray[20][0]="赔付比例";
				iArray[20][1]="0px";
				iArray[20][2]=100;
				iArray[20][3]=3;
				       
				iArray[21]=new Array();
				iArray[21][0]="社保标记";
				iArray[21][1]="0px";
				iArray[21][2]=100;
				iArray[21][3]=3;
				       
				iArray[22]=new Array();
				iArray[22][0]="封顶线";
				iArray[22][1]="0px";
				iArray[22][2]=100;
				iArray[22][3]=3;
     



        //if(fm.all('RiskCode').value=="211672"){
        //  //alert("riskCode:"+fm.all('RiskCode').value);
        //  iArray[0][1]="30px";         			//列宽
        //  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	//  
	//  //责任代码
	//  iArray[1][1]="60px";            		//列宽
        //  iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	//  
	//  //责任名称
	//  iArray[2][1]="120px";            		//列宽
        //  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	//  //保费
        //  iArray[13][1]="80px";            		//列宽
        //  iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	//  //保额
        //  iArray[14][1]="80px";            		//列宽
        //  iArray[14][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	//  //保险年期
        //  //iArray[13][1]="40px";            		//列宽
        //  //iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	//  //年期单位
        //  //iArray[12][1]="40px";            		//列宽
        //  //iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许
        //  //计算规则
        //  iArray[16][1]="40px";            		//列宽
        //  iArray[16][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      	//  //费率
        //  iArray[17][1]="40px";            		//列宽
        //  iArray[17][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      	//  //计算方向
        //  iArray[12][1]="40px";            		//列宽
        //  iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许
        //
        //}
       if(fm.all('RiskCode').value=="211370"||fm.all('RiskCode').value=="211601"){
          //alert("riskCode:"+fm.all('RiskCode').value);
          iArray[0][1]="30px";         			//列宽
          iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
          
          //责任代码
	  iArray[1][1]="60px";            		//列宽
          iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	  
          //责任名称
	  iArray[2][1]="120px";            		//列宽
          iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  //保费
          iArray[13][1]="80px";            		//列宽
          iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //保额
          iArray[14][1]="80px";            		//列宽
          iArray[14][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //保险年期
          //iArray[13][1]="40px";            		//列宽
          //iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //年期单位
          //iArray[12][1]="40px";            		//列宽
          //iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许
          //计算规则
          iArray[16][1]="40px";            		//列宽
          iArray[16][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      	  //费率
          iArray[17][1]="40px";            		//列宽
          iArray[17][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      	  //计算方向
          iArray[12][1]="40px";            		//列宽
          iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	
	  
       }
       
        if(fm.all('RiskCode').value=="1601"){
          //alert("riskCode:"+fm.all('RiskCode').value);
          iArray[0][1]="30px";         			//列宽
          iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
          
          //责任代码
	  			iArray[1][1]="60px";            		//列宽
          iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	  
          //责任名称
	  			iArray[2][1]="120px";            		//列宽
          iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
          
          iArray[15][0]="档次";						
					iArray[15][1]="30";
					iArray[15][3]=2;
					iArray[15][10]="Mult";
					iArray[15][11]="0|^1|一档^2|二档|^3|三档";
	  //保费
          iArray[13][1]="80px";            		//列宽
          iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //保额
          //iArray[14][1]="80px";            		//列宽
          //iArray[14][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //保险年期
          //iArray[13][1]="40px";            		//列宽
          //iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //年期单位
          //iArray[12][1]="40px";            		//列宽
          //iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许
          //计算规则
          //iArray[16][1]="40px";            		//列宽
          //iArray[16][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      	  //费率
          //iArray[17][1]="40px";            		//列宽
          //iArray[17][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      	  //计算方向
          //iArray[12][1]="40px";            		//列宽
          //iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许
          
	        
	      }
        if(fm.all('RiskCode').value=="GC11001"){
          //alert("riskCode:"+fm.all('RiskCode').value);
          iArray[0][1]="30px";         			//列宽
          iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
          
          //责任代码
	  			iArray[1][1]="60px";            		//列宽
          iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	  
          //责任名称
	  			iArray[2][1]="120px";            		//列宽
          iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
          
          iArray[15][0]="档次";						
					iArray[15][1]="30";
					iArray[15][3]=2;
					iArray[15][10]="Mult";
					iArray[15][11]="0|^1|一档^2|二档|^3|三档";
	  //保费
          iArray[13][1]="80px";            		//列宽
          iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //保额
          //iArray[14][1]="80px";            		//列宽
          //iArray[14][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //保险年期
          //iArray[13][1]="40px";            		//列宽
          //iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //年期单位
          //iArray[12][1]="40px";            		//列宽
          //iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许
          //计算规则
          //iArray[16][1]="40px";            		//列宽
          //iArray[16][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      	  //费率
          //iArray[17][1]="40px";            		//列宽
          //iArray[17][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      	  //计算方向
          //iArray[12][1]="40px";            		//列宽
          //iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许
          
	        
	      }
	       if(fm.all('RiskCode').value=="211704"){
          //alert("riskCode:"+fm.all('RiskCode').value);
          iArray[0][1]="30px";         			//列宽
          iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
          
          //责任代码
	  			iArray[1][1]="60px";            		//列宽
          iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	  
          //责任名称
	  			iArray[2][1]="80px";            		//列宽
          iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
          
	  //保费
          iArray[13][1]="80px";            		//列宽
          iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //保额
          iArray[14][1]="80px";            		//列宽
          iArray[14][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //保险年期
          //iArray[13][1]="40px";            		//列宽
          //iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	  //年期单位
          //iArray[12][1]="40px";            		//列宽
          //iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许
          //计算规则
          iArray[16][1]="40px";            		//列宽
          iArray[16][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      	  //费率
          iArray[17][1]="40px";            		//列宽
          iArray[17][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      	  //计算方向
          iArray[12][1]="40px";            		//列宽
          iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许
          
	        iArray[9][0]="在职/退休";						
					iArray[9][1]="40";
					iArray[9][3]=2; 
					iArray[9][10]="StandbyFlag1";
					iArray[9][11]="0|^0|在职^1|退休/离休";
	      }
       
      DutyGrid = new MulLineEnter( "fm" , "DutyGrid" ); 
      //这些属性必须在loadMulLine前
      DutyGrid.mulLineCount = 0;   
      DutyGrid.displayTitle = 1;
      DutyGrid.canChk = 1;
      DutyGrid.loadMulLine(iArray);  
      //DutyGrid.checkBoxSel(1);
      
      //这些操作必须在loadMulLine后面
      //DutyGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        //alert(ex);
      }
}


//保费项列表
function initPremGrid()
{
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="责任编码";    	//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="交费编码";         			//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="交费名称";         			//列名
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保费";         			//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="分配比例";         			//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="关联帐户号";         			//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[7]=new Array();
      iArray[7][0]="帐户分配比率";         			//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[8]=new Array();
      iArray[8][0]="管理费比例";         			//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="交费占员工工资比例";         			//列名
      iArray[9][1]="120px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
     
        if(typeof(window.spanPremGrid) == "undefined" )
        {
              //alert("out");
              return false;
        }
        else
        {
      			
      			
      			
      			//alert("in");
      	    PremGrid = new MulLineEnter( "fm" , "PremGrid" );
	  	    //这些属性必须在loadMulLine前
	 	     PremGrid.mulLineCount = 0;   
	 	     PremGrid.displayTitle = 1;
	 	     PremGrid.canChk = 1;
	 	     PremGrid.loadMulLine(iArray);  
     	 }

      
     }
     catch(ex)
    {
		return false;
    }
    return true;
}

function insertClick()
{
    if(fm.all('StandbyDuty').value!=null&&fm.all('StandbyDuty').value!=""){
      var rowCount = DutyGrid.mulLineCount;
      DutyGrid.addOne();
      saveValue(rowCount);
    }
    else{
      alert("请选择备用责任！");
    } 
}
function saveValue(index){
    var mulLineObj = DutyGrid;
    var StandbyDutyCode = fm.all('StandbyDuty').value;
    var StandbyDutyCodeName = fm.all('StandbyDutyName').value;
    //alert("Code:"+fm.all('StandbyDuty').value+"  name:"+fm.all('StandbyDutyName').value);
    mulLineObj.setRowColData(index,1,StandbyDutyCode);
    mulLineObj.setRowColData(index,2,StandbyDutyCodeName);
    DutyGrid.checkBoxSel(index+1);
    
}

function setInsuYearToGrid(){
	var strChooseDuty="";
		for(i=0;i<=DutyGrid.mulLineCount-1;i++)
		{
			if(DutyGrid.getChkNo(i)==true)
			{
				strChooseDuty=strChooseDuty+"1";
				DutyGrid.setRowColData(i, 13, fm.all('InsuYear').value);//保险年期
				DutyGrid.setRowColData(i, 12, fm.all('InsuYearFlag').value);//保险年期单位
				//DutyGrid.setRowColData(i, 11, fm.all('PayIntv').value);//缴费方式
			}
			else
			{
				strChooseDuty=strChooseDuty+"0";
			}
		}
}

//份数算保费的险种份数不能为空
function chkMult(){
  var tSql="";
  tSql="select CalMode from lmduty a,lmriskduty b where b.riskcode='"+fm.all('RiskCode').value+"' and a.dutycode=b.dutycode";
  var arrResult=easyExecSql(tSql);
  if(arrResult[0]=="O"){
    //alert("Mult:"+fm.all('Mult').value);
    if(fm.all('Mult').value==null||parseFloat(fm.all('Mult').value)==0||fm.all('Mult').value==""){
      alert("份数不能为空！");
      return false;	
    }
  }
  return true;	
}


function showFloatRate(tContPlanCode){
  //alert("calRule:"+fm.all('CalRule').value+"plan:"+tContPlanCode+"grpcontno:"+fm.all('GrpContNo').value);
  var arrResult=null;
  var tSql="";
  if(tContPlanCode!=null&&tContPlanCode!=""){
      	
  }
  if(fm.all('CalRule').value=="1"){
    divFloatRate.style.display="none";
    divFloatRate2.style.display="";	
  }
  else if(fm.all('CalRule').value=="2"){
    divFloatRate.style.display="";
    divFloatRate2.style.display="none";
  }
  else {
    divFloatRate.style.display="none";
    divFloatRate2.style.display="none";	
  }	
}

function getOtherInfo(payEndYear){
  //alert("payIntv:"+fm.all('PayIntv').value);
  if(fm.all('RiskCode').value=="211401"){
    if(payEndYear==1000){
      fm.all('PayEndYearFlag').value='A';
      fm.all('GetYear').className='code';
      //fm.all('GetYearFlag').className='code';
      fm.all('GetYear').CodeData="0|^1|即时领取^55|55岁领取^60|60岁领取";
      fm.all('GetYear').value="";
      fm.all('GetYearFlag').value="";	
    }
    else {		
      if(payEndYear<45){
        fm.all('PayEndYearFlag').value='Y';	
      }	
      else {
        fm.all('PayEndYearFlag').value='A';	
      }
      fm.all('GetYear').className='readonly';
      fm.all('GetYearFlag').className='readonly';
      if(payEndYear<45){
        fm.all('GetYear').value=1;
        fm.all('GetYearFlag').value='D';	
      }
      else{
        fm.all('GetYear').value=payEndYear;
        fm.all('GetYearFlag').value='A';
      }
    }
  }
}

function getGetYearFlag(getYear){
  if(fm.all('RiskCode').value=='211401'){
    if(getYear=='1'){
      fm.all('GetYearFlag').value='D';	
    }	
    else {
      fm.all('GetYearFlag').value='A';	
    }
  }	
}

function getPayRuleInfo(){
  if(fm.all('RiskCode').value=="212403"){
    var tSql="select distinct payrulecode,payrulename from lcpayrulefactory where grpcontno='"+fm.all('GrpContNo').value+"' and riskcode='"+fm.all('RiskCode').value+"'";
    var arr=easyExecSql(tSql);
    if (arr.length>0){
      var tCodeData="0|";
      for (var i=0;i<arr.length;i++){
      	tCodeData=tCodeData+"^"+arr[i][0]+"|"+arr[i][1];
      }
      fm.all('PayRuleCode').CodeData=tCodeData;
      
    }
  }	
}
function test1(){
  alert("test");	
}