<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：NoScanContInputSave.jsp
//程序功能：个单新契约无扫描件保单录入
//创建日期：2002-06-19 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
System.out.println("Auto-begin:");
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.workflow.ask.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String tFlag = request.getParameter("tFlag");
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  if(tG == null) 
  {
	System.out.println("session has expired");
	return;
   }
  //prepare data for workflow 
  VData tVData = new VData();
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("PrtNo",request.getParameter("PrtNo"));
  tTransferData.setNameAndValue("ManageCom",request.getParameter("ManageCom"));
  tTransferData.setNameAndValue("InputDate",PubFun.getCurrentDate());
  tTransferData.setNameAndValue("Operator",tG.Operator);
	tVData.add( tTransferData );
	tVData.add( tG ); 
	try
	{
		 AskWorkFlowUI tAskWorkFlowUI = new AskWorkFlowUI();
		if (tAskWorkFlowUI.submitData(tVData,"0000006000") == false)
				{
					int n = tAskWorkFlowUI.mErrors.getErrorCount();
					System.out.println("n=="+n);
					for (int j = 0; j < n; j++)
					System.out.println("Error: "+tAskWorkFlowUI.mErrors.getError(j).errorMessage);
					Content = " 投保单申请失败，原因是: " + tAskWorkFlowUI.mErrors.getError(0).errorMessage;
					FlagStr = "Fail";
				}
		  				//如果在Catch中发现异常，则不从错误类中提取错误信息
				if (FlagStr != "Fail")
				{
				    tError = tAskWorkFlowUI.mErrors;
				    //tErrors = tAskWorkFlowUI.mErrors;
				    Content = " 投保单申请成功! ";
				    if (!tError.needDealError())
				    {                          
				    	int n = tError.getErrorCount();
		    			if (n > 0)
		    			{    			      
					      for(int j = 0;j < n;j++)
					      {
					        //tError = tErrors.getError(j);
					        Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
					      }
					    }
		
				    	FlagStr = "Succ";
				    }
				    else                                                                           
				    {
				    	int n = tError.getErrorCount();
		    			if (n > 0)
		    			{
					      for(int j = 0;j < n;j++)
					      {
					        //tError = tErrors.getError(j);
					        Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
					      }
							}
				    	FlagStr = "Fail";
				    }
				}		

	}
	catch(Exception ex)
	{
			ex.printStackTrace();
			Content = Content.trim() +" 提示:异常退出.";
	}
%>                      
<html>
<script language="javascript">

parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
