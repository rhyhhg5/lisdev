<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：GroupMainUW.jsp
//程序功能：集体人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	//String tGrpPolNo = "";
	String tContNo = "";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="AskGroupMainUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AskGroupMainUWInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单查询条件 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>
            集体投保单号
          </TD>
          <TD  class= input>
            <Input class=common name=QGrpProposalNo>
          </TD>
          <TD  class= title>
            印刷号
          </TD>
          <TD  class= input>
            <Input class=common name=QPrtNo>
          </TD>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class="code" name=QAgentCode ondblclick="return showCodeList('AgentCode',[this,QAgentGroup],[0,2]);" onkeyup="return showCodeListKey('AgentCode',[this,QAgentGroup],[0,2]);">
          </TD>
       </TR>
       <TR>
          <TD  class= title>
            核保人
          </TD>
          <TD  class= input>
            <Input class= common name=QOperator value= "">
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=QManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>          
          <TD  class= title>
            保单状态
          </TD>
          <TD  class= input>
            <Input class="code" readonly name=QState value= "" CodeData= "0|^1|未人工核保^2|核保已回复^3|核保未回复" ondblClick="showCodeListEx('State',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('State',[this,''],[0,1]);">
          </TD>

        </TR>
    </table>
       <INPUT VALUE="查  询" Class="cssButton" TYPE=button onclick="querygrp();">
       <INPUT type= "hidden" name= "Operator" value= "">
    <!-- 查询未过集体单（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 集体投保单查询结果
    		</td>
    	</tr>
    </table>
    
  	<Div  id= "divLCPol1" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanGrpGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      		<INPUT VALUE="首  页" Class="cssButton" TYPE=button onclick="getFirstPage();"> 
      		<INPUT VALUE="上一页" Class="cssButton" TYPE=button onclick="getPreviousPage();"> 					
      		<INPUT VALUE="下一页" Class="cssButton" TYPE=button onclick="getNextPage();"> 
      		<INPUT VALUE="尾  页" Class="cssButton" TYPE=button onclick="getLastPage();">    	
	</Div>
	<P>
	<INPUT VALUE="人工核保" Class="cssButton" TYPE=button onclick=" PepoleUW();"> 
	</P>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
