<%--
    保存集体保单信息 2004-11-16 wzw
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%	         
	
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  //团体告知信息
  String tImpartNum[] = request.getParameterValues("ImpartGridNo");
	String tImpartVer[] = request.getParameterValues("ImpartGrid1");            //告知版别
	String tImpartCode[] = request.getParameterValues("ImpartGrid2");           //告知编码
	String tImpartContent[] = request.getParameterValues("ImpartGrid3");        //告知内容
	String tImpartParamModle[] = request.getParameterValues("ImpartGrid4");        //填写内容
	//既往告知
	String tImpartNum2[] = request.getParameterValues("HistoryImpartGridNo");
	String tInsuStartYear[] = request.getParameterValues("HistoryImpartGrid1");
	String tInsuEndYear[] = request.getParameterValues("HistoryImpartGrid2");
	String tInsuContent[] = request.getParameterValues("HistoryImpartGrid3");           //告知编码
	String tRate[] = request.getParameterValues("HistoryImpartGrid4");        //告知内容
	String tEnsureContent[] = request.getParameterValues("HistoryImpartGrid5");
	String tPeoples[] = request.getParameterValues("HistoryImpartGrid6");	 
	String tRecompensePeoples[] = request.getParameterValues("HistoryImpartGrid7"); 
	String tOccurMoney[] = request.getParameterValues("HistoryImpartGrid8"); 
	String tRecompenseMoney[] = request.getParameterValues("HistoryImpartGrid9"); 
	String tPendingMoney[] = request.getParameterValues("HistoryImpartGrid10");
	String tSerialNo1[] = request.getParameterValues("HistoryImpartGrid11"); 
	//严重疾病情况告知
	String tImpartNum3[] = request.getParameterValues("DiseaseGridNo");
	String tOcurTime[] = request.getParameterValues("DiseaseGrid1");
	String tDiseaseName[] = request.getParameterValues("DiseaseGrid2");            //告知版别
	String tDiseasePepoles[] = request.getParameterValues("DiseaseGrid3");           //告知编码
	String tCureMoney[] = request.getParameterValues("DiseaseGrid4");        //告知内容
	String tRemark[] = request.getParameterValues("DiseaseGrid5");
	String tSerialNo2[] = request.getParameterValues("DiseaseGrid6"); 
	
		//约定缴费方式
		String cGrpContPayIntv=request.getParameter("GrpContPayIntv");
  	String tBookingPayIntyNum[] = request.getParameterValues("BookingPayIntyGridNo");
	  String tBookingDate[] = request.getParameterValues("BookingPayIntyGrid1");
	  String tBookingMoney[] = request.getParameterValues("BookingPayIntyGrid2");  


  VData tVData = new VData();
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();      //集体保单
  LCGrpAppntSchema tLCGrpAppntSchema = new LCGrpAppntSchema();   //团单投保人
  
  LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();				 //团体告知信息
  LCHistoryImpartSet tLCHistoryImpartSet = new LCHistoryImpartSet();           //既往告知
  LCDiseaseImpartSet tLCDiseaseImpartSet = new LCDiseaseImpartSet();					 //严重疾病告知
    
  LDGrpSchema tLDGrpSchema   = new LDGrpSchema();                //团体客户
  LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema(); //团体客户地址 
  GlobalInput tG = new GlobalInput();
  LCCGrpSpecSet tLCCGrpSpecSet = new LCCGrpSpecSet();
  tG=(GlobalInput)session.getValue("GI");
  
  tAction = request.getParameter( "fmAction" );
            //集体保单信息  LCGrpCont
	    tLCGrpContSchema.setProposalGrpContNo(request.getParameter("ProposalGrpContNo"));  //集体投保单号码
	    tLCGrpContSchema.setGrpContNo(request.getParameter("GrpContNo"));
	    tLCGrpContSchema.setPrtNo(request.getParameter("PrtNo"));                  //印刷号码
	    tLCGrpContSchema.setManageCom(request.getParameter("ManageCom"));          //管理机构
	    tLCGrpContSchema.setSaleChnl(request.getParameter("SaleChnl"));            //销售渠道
	    tLCGrpContSchema.setAgentCom(request.getParameter("AgentCom"));            //代理机构
	    tLCGrpContSchema.setAgentType(request.getParameter("AgentType"));           //代理机构分类
	    tLCGrpContSchema.setAgentCode(request.getParameter("AgentCode"));          //代理人编码
	    tLCGrpContSchema.setAgentGroup(request.getParameter("AgentGroup"));        //代理人组别
	    tLCGrpContSchema.setAgentCode1(request.getParameter("AgentCode1"));        //联合代理人代码
	    tLCGrpContSchema.setGrpSpec(request.getParameter("GrpSpec"));              //集体特约
	    tLCGrpContSchema.setAppntNo(request.getParameter("GrpNo"));                //客户号码
	    tLCGrpContSchema.setAddressNo(request.getParameter("GrpAddressNo"));       //地址号码
	    tLCGrpContSchema.setGrpName(request.getParameter("GrpName"));              //单位名称
	    tLCGrpContSchema.setGetFlag(request.getParameter("GetFlag"));              //付款方式
	    tLCGrpContSchema.setBankCode(request.getParameter("BankCode"));            //银行编码
	    tLCGrpContSchema.setBankAccNo(request.getParameter("BankAccNo"));          //银行帐号
	    tLCGrpContSchema.setCurrency(request.getParameter("Currency"));            //币别
	    tLCGrpContSchema.setCValiDate(request.getParameter("CValiDate"));          //保单生效日期
	    tLCGrpContSchema.setPolApplyDate(request.getParameter("PolApplyDate"));          //保单投保日期
	    tLCGrpContSchema.setOutPayFlag(request.getParameter("OutPayFlag"));          //溢交处理方式
	    
	    tLCGrpContSchema.setGrpNature(request.getParameter("GrpNature"));         //单位性质
	    tLCGrpContSchema.setBusinessType(request.getParameter("BusinessType"));   //行业类别
	    tLCGrpContSchema.setPeoples(request.getParameter("Peoples"));             //总人数
	    tLCGrpContSchema.setRgtMoney(request.getParameter("RgtMoney"));           //注册资本
	    tLCGrpContSchema.setAsset(request.getParameter("Asset"));                 //资产总额
	    tLCGrpContSchema.setNetProfitRate(request.getParameter("NetProfitRate")); //净资产收益率
	    tLCGrpContSchema.setMainBussiness(request.getParameter("MainBussiness")); //主营业务
	    tLCGrpContSchema.setCorporation(request.getParameter("Corporation"));     //法人
	    tLCGrpContSchema.setComAera(request.getParameter("ComAera"));             //机构分布区域
	    tLCGrpContSchema.setPhone(request.getParameter("Phone"));             		//总机
	    tLCGrpContSchema.setFax(request.getParameter("Fax"));             				//传真
	    tLCGrpContSchema.setFoundDate(request.getParameter("FoundDate"));         //成立时间
	    tLCGrpContSchema.setRemark(request.getParameter("Remark"));								//备注
	    
	    tLCGrpContSchema.setAppFlag(request.getParameter("AppFlag"));
	    tLCGrpContSchema.setMarketType(request.getParameter("MarketType"));
	    tLCGrpContSchema.setPeoples3(request.getParameter("Peoples3"));
	    System.out.println("测试 在职人数 ："+request.getParameter("OnWorkPeoples"));
	    tLCGrpContSchema.setOnWorkPeoples(request.getParameter("OnWorkPeoples"));
	    tLCGrpContSchema.setOffWorkPeoples(request.getParameter("OffWorkPeoples"));
	    tLCGrpContSchema.setOtherPeoples(request.getParameter("OtherPeoples"));
	    tLCGrpContSchema.setRelaPeoples(request.getParameter("RelaPeoples"));
	    tLCGrpContSchema.setRelaMatePeoples(request.getParameter("RelaMatePeoples"));
	    tLCGrpContSchema.setRelaYoungPeoples(request.getParameter("RelaYoungPeoples"));
	    tLCGrpContSchema.setRelaOtherPeoples(request.getParameter("RelaOtherPeoples"));
	    tLCGrpContSchema.setEnterKind(request.getParameter("EnterKind"));          //参保形式
	    tLCGrpContSchema.setPayIntv(request.getParameter("GrpContPayIntv"));   
	    tLCGrpContSchema.setCInValiDate(request.getParameter("CInValiDate"));
	    tLCGrpContSchema.setReceiveDate(request.getParameter("ReceiveDate"));//收单日期
	    tLCGrpContSchema.setFirstTrialOperator(request.getParameter("FirstTrialOperator"));   //初审人       
	    tLCGrpContSchema.setReceiveOperator(request.getParameter("ReceiveOperator"));   //收单人
	    System.out.println("## 收单人 ："+request.getParameter("ReceiveOperator"));
	    tLCGrpContSchema.setFirstTrialDate(request.getParameter("FirstTrialDate"));	   //初审日期
	    tLCGrpContSchema.setPolApplyDate(request.getParameter("FirstTrialDate"));	   
	    tLCGrpContSchema.setAskGrpContNo(request.getParameter("AskGrpContNo"));	 
	    tLCGrpContSchema.setHandlerName(request.getParameter("HandlerName"));  
	    tLCGrpContSchema.setInfoSource(request.getParameter("InfoSource"));  
	    tLCGrpContSchema.setOperationManager(request.getParameter("OperationManager"));
	    tLCGrpContSchema.setAgentDate(request.getParameter("AgentDate"));     
	    tLCGrpContSchema.setPremScope(request.getParameter("PremScope"));  
	       
	    //团单投保人信息  LCGrpAppnt
	    tLCGrpAppntSchema.setGrpContNo(request.getParameter("ProposalGrpContNo"));     //集体投保单号码
	    tLCGrpAppntSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	    tLCGrpAppntSchema.setPrtNo(request.getParameter("PrtNo"));                 //印刷号码
	    tLCGrpAppntSchema.setName(request.getParameter("GrpName"));
	    tLCGrpAppntSchema.setPostalAddress(request.getParameter("GrpAddress"));
	    tLCGrpAppntSchema.setZipCode(request.getParameter("GrpZipCode"));
	    tLCGrpAppntSchema.setAddressNo(request.getParameter("GrpAddressNo"));
	    tLCGrpAppntSchema.setPhone(request.getParameter("Phone"));
	    //团体客户信息  LDGrp
	    tLDGrpSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	    tLDGrpSchema.setGrpName(request.getParameter("GrpName"));             //单位名称
	    tLDGrpSchema.setGrpNature(request.getParameter("GrpNature"));         //单位性质
	    tLDGrpSchema.setBusinessType(request.getParameter("BusinessType"));   //行业类别
	    tLDGrpSchema.setPeoples(request.getParameter("Peoples"));             //总人数
	    tLDGrpSchema.setOnWorkPeoples(request.getParameter("AppntOnWorkPeoples"));
	    tLDGrpSchema.setOffWorkPeoples(request.getParameter("AppntOffWorkPeoples"));
	    tLDGrpSchema.setOtherPeoples(request.getParameter("AppntOtherPeoples"));
	    tLDGrpSchema.setRgtMoney(request.getParameter("RgtMoney"));           //注册资本
	    tLDGrpSchema.setAsset(request.getParameter("Asset"));                 //资产总额
	    tLDGrpSchema.setNetProfitRate(request.getParameter("NetProfitRate")); //净资产收益率
	    tLDGrpSchema.setMainBussiness(request.getParameter("MainBussiness")); //主营业务
	    tLDGrpSchema.setCorporation(request.getParameter("Corporation"));     //法人
	    tLDGrpSchema.setComAera(request.getParameter("ComAera"));             //机构分布区域
	    tLDGrpSchema.setPhone(request.getParameter("Phone"));             //总机
	    tLDGrpSchema.setFax(request.getParameter("Fax"));             //传真
	    tLDGrpSchema.setFoundDate(request.getParameter("FoundDate"));             //成立时间
	    //团体客户地址  LCGrpAddress	    
	    tLCGrpAddressSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	    //tLCGrpAddressSchema.setAddressNo(request.getParameter("GrpAddressNo"));      //地址号码
	    tLCGrpAddressSchema.setGrpAddress(request.getParameter("GrpAddress"));       //单位地址
	    System.out.println("*******************"+request.getParameter("GrpAddress"));
	    tLCGrpAddressSchema.setGrpZipCode(request.getParameter("GrpZipCode"));       //单位邮编
	    //保险联系人一
	    tLCGrpAddressSchema.setLinkMan1(request.getParameter("LinkMan1"));
	    tLCGrpAddressSchema.setDepartment1(request.getParameter("Department1"));
	    tLCGrpAddressSchema.setHeadShip1(request.getParameter("HeadShip1"));
	    tLCGrpAddressSchema.setPhone1(request.getParameter("Phone1"));
	    tLCGrpAddressSchema.setE_Mail1(request.getParameter("E_Mail1"));
	    tLCGrpAddressSchema.setFax1(request.getParameter("Fax1"));
	    //保险联系人二
	    tLCGrpAddressSchema.setLinkMan2(request.getParameter("LinkMan2"));
	    tLCGrpAddressSchema.setDepartment2(request.getParameter("Department2"));
	    tLCGrpAddressSchema.setHeadShip2(request.getParameter("HeadShip2"));
	    tLCGrpAddressSchema.setPhone2(request.getParameter("Phone2"));
	    tLCGrpAddressSchema.setE_Mail2(request.getParameter("E_Mail2"));
	    tLCGrpAddressSchema.setFax2(request.getParameter("Fax2"));
	    //团体告知信息
			int ImpartCount = 0;
			if (tImpartNum != null) ImpartCount = tImpartNum.length;
	        
			for (int i = 0; i < ImpartCount; i++)	
			{
			LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
			tLCCustomerImpartSchema.setProposalContNo(request.getParameter("ContNo"));
				
				tLCCustomerImpartSchema.setCustomerNo(tLDGrpSchema.getCustomerNo());
				tLCCustomerImpartSchema.setCustomerNoType("0");
				tLCCustomerImpartSchema.setImpartCode(tImpartCode[i]);
				tLCCustomerImpartSchema.setImpartContent(tImpartContent[i]);
				tLCCustomerImpartSchema.setImpartParamModle(tImpartParamModle[i]);
				tLCCustomerImpartSchema.setImpartVer(tImpartVer[i]) ;
				tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
			}
			//既往情况告知
	    int ImpartCont2 = 0;
	    if (tImpartNum2 != null) ImpartCont2 = tImpartNum2.length;
	    				//System.out.println("三大法阿飞诉讼"+ImpartCont2);    
	    			for (int i = 0; i < ImpartCont2; i++)	
			{	    				//System.out.println("三大法阿飞诉讼琐琐碎碎ssss");    
			LCHistoryImpartSchema tLCHistoryImpartSchema = new LCHistoryImpartSchema();
			  //tLCHistoryImpartSchema.setPrtNo(request.getParameter("PrtNo"));
				//tLCHistoryImpartSchema.setGrpContNo(request.getParameter("GrpContNo"));
				//tLCHistoryImpartSchema.setSerialNo(request.getParameter("SerialNo"));

			//tLCHistoryImpartSchema.setInsuYear(tInsuYear[i]);
			//System.out.println("gjgjhgjj"+tInsuYear[i]);                                                            
			//tLCHistoryImpartSchema.setInsuYearFlag(tInsuYearFlag[i]);                                                             
				tLCHistoryImpartSchema.setInsuContent(tInsuContent[i]);                                                             
				tLCHistoryImpartSchema.setRate(tRate[i]) ;                                                             
				tLCHistoryImpartSchema.setEnsureContent(tEnsureContent[i]) ;                                                             
				tLCHistoryImpartSchema.setPeoples(tPeoples[i]) ;                                                             
				tLCHistoryImpartSchema.setRecompensePeoples(tRecompensePeoples[i]) ;                                                     
				tLCHistoryImpartSchema.setOccurMoney(tOccurMoney[i]) ;                                                             
				tLCHistoryImpartSchema.setRecompenseMoney(tRecompenseMoney[i]) ;                                                         
				tLCHistoryImpartSchema.setPendingMoney(tPendingMoney[i]) ;
				tLCHistoryImpartSchema.setSerialNo(tSerialNo1[i]) ;				         
				tLCHistoryImpartSchema.setInsuStartYear(tInsuStartYear[i]) ;
				tLCHistoryImpartSchema.setInsuEndYear(tInsuEndYear[i]) ;		                                              
				
				tLCHistoryImpartSet.add(tLCHistoryImpartSchema);
			}
			//严重疾病情况告知
	    int ImpartCont3 = 0;
	    if (tImpartNum3 != null) ImpartCont3 = tImpartNum3.length;
	     
	    for (int i = 0; i < ImpartCont3; i++)	
			{  System.out.println("三大法阿飞诉讼"+tOcurTime[i]);  
			  LCDiseaseImpartSchema tLCDiseaseImpartSchema = new LCDiseaseImpartSchema();
 				tLCDiseaseImpartSchema.setOcurTime(tOcurTime[i]);                                                             
				tLCDiseaseImpartSchema.setDiseaseName(tDiseaseName[i]);                                                             
				tLCDiseaseImpartSchema.setDiseasePepoles(tDiseasePepoles[i]) ;                                                             
				tLCDiseaseImpartSchema.setCureMoney(tCureMoney[i]) ;                                                             
				tLCDiseaseImpartSchema.setRemark(tRemark[i]) ; 
				tLCDiseaseImpartSchema.setSerialNo(tSerialNo2[i]) ;	                                                            
				
				tLCDiseaseImpartSet.add(tLCDiseaseImpartSchema);
			}			
	    int Booking = 0;
	    if (tBookingPayIntyNum != null) Booking = tBookingPayIntyNum.length;

	    for (int i = 0; i < Booking ; i++)	
			{  
				 System.out.println("检测约定缴费方式是否存入，暂时存储在特约表中，以后处理到特定表中");
			   LCCGrpSpecSchema tLCCGrpSpecSchema = new LCCGrpSpecSchema();
			   tLCCGrpSpecSchema.setProposalGrpContNo(request.getParameter("ProposalGrpContNo"));
			   tLCCGrpSpecSchema.setGrpContNo(request.getParameter("GrpContNo"));
			   tLCCGrpSpecSchema.setPrtSeq("0000000");
			   System.out.println("tBookingDate[i]+tBookingMoney[i]"+tBookingDate[i]+tBookingMoney[i]);
			   tLCCGrpSpecSchema.setSpecContent(tBookingDate[i]+"|"+tBookingMoney[i]);
			   tLCCGrpSpecSchema.setSerialNo(String.valueOf(i));
				 tLCCGrpSpecSet.add(tLCCGrpSpecSchema);
			}

  System.out.println("end setSchema:");
  // 准备传输数据 VData
  tVData.add( tLCGrpContSchema );
  tVData.add( tLCGrpAppntSchema );
  tVData.add( tLDGrpSchema );
  tVData.add( tLCGrpAddressSchema );
  tVData.add( tG );
  tVData.add( tLCCustomerImpartSet );   //团体告知信息
  tVData.add( tLCHistoryImpartSet );		//既往告知消息
  tVData.add( tLCDiseaseImpartSet );		//严重疾病告知消息
  tVData.add( tLCCGrpSpecSet );
	
  //传递非SCHEMA信息
  TransferData tTransferData = new TransferData();
  String SavePolType="";
  String BQFlag=request.getParameter("BQFlag");
  if(BQFlag==null) SavePolType="0";
  else if(BQFlag.equals("")) SavePolType="0";
  else  SavePolType=BQFlag;
  
  //传递LoadFlag
  mLoadFlag=request.getParameter("LoadFlag"); 
  tTransferData.setNameAndValue("LoadFlag",mLoadFlag);
  tTransferData.setNameAndValue("GrpContNo",request.getParameter("GrpContNo"));
  tTransferData.setNameAndValue("Department1",request.getParameter("Department1"));
  System.out.println("LoadFlag is : " + request.getParameter("LoadFlag"));

  
  tTransferData.setNameAndValue("SavePolType",SavePolType); //保全保存标记，默认为0，标识非保全
  System.out.println("SavePolType，BQ is 2，other is 0 : " + request.getParameter("BQFlag"));
  tVData.addElement(tTransferData);
  
  
  
	if( tAction.equals( "INSERT" )) tOperate = "INSERT||GROUPPOL";
	if( tAction.equals( "UPDATE" )) tOperate = "UPDATE||GROUPPOL";
	if( tAction.equals( "DELETE" )) tOperate = "DELETE||GROUPPOL";
	GroupContUI tGroupContUI = new GroupContUI();
	
	if( tGroupContUI.submitData( tVData, tOperate ) == false )
	{
		Content = " 保存失败，原因是: " + tGroupContUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";

		tVData.clear();
		tVData = tGroupContUI.getResult();

		// 显示
		if(( tAction.equals( "INSERT" ))||( tAction.equals( "UPDATE" )))
		{
			// 保单信息
			LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); 
			mLCGrpContSchema.setSchema(( LCGrpContSchema )tVData.getObjectByObjectName( "LCGrpContSchema", 0 ));
			%>
			   <script language="javascript">
			    	parent.fraInterface.fm.all("ProposalGrpContNo").value = "<%=mLCGrpContSchema.getProposalGrpContNo()%>";
			    	parent.fraInterface.fm.all("GrpContNo").value = "<%=mLCGrpContSchema.getGrpContNo()%>";
			     parent.fraInterface.fm.all("GrpNo").value = "<%=mLCGrpContSchema.getAppntNo()%>";
			     parent.fraInterface.fm.all("GrpAddressNo").value = "<%=mLCGrpContSchema.getAddressNo()%>"; 
			         	
			   </script>
			<%		
		}
		else
		{
			%>
			   <script language="javascript">
			     parent.fraInterface.emptyFormElements();
			    </script>
			<%
		
		}
	}
        System.out.println("Content:"+Content);	

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>