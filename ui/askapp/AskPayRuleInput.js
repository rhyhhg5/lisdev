//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var QueryResult="";
var QueryCount = 0;
var mulLineCount = 0;
var QueryWhere="";
window.onfocus=myonfocus;

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
	if (cShow=="true"){
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
	parent.fraMain.rows = "0,0,0,0,*";
}

//数据提交（保存）
function submitForm(){
	PayRuleGrid.delBlankLine("PayRuleGrid");
	if(PayRuleGrid.mulLineCount==0){
		alert("没有数据信息！");
		return false;
	}

	if (!beforeSubmit()){
		return false;
	}
	fm.all('mOperate').value = "INSERT||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

//返回上一步
function returnparent(){
	window.location.href = "AskGrpFeeInput.jsp?ProposalGrpContNo="+GrpContNo+"&LoadFlag=2";
}

//数据提交（删除）
function DelContClick(){
	PayRuleGrid.delBlankLine("PayRuleGrid");
//	alert(PayRuleGrid.mulLineCount);
	if(PayRuleGrid.mulLineCount==0){
		alert("没有数据信息！");
		return false;
	}

	fm.all('mOperate').value = "DELETE||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

//数据校验
function beforeSubmit(){
	return true;
}

function GrpPerPolDefine(){
	// 初始化表格
	var str = "";
	var tGrpContNo = GrpContNo;

	var tImpPayRuleCode = initPayRule(tGrpContNo);
	//初始化数组
	initPayRuleGrid(tImpPayRuleCode);

	divPayRule.style.display= "";
	var strSQL = "";

	strSQL = "select RiskCode,FactoryType,OtherNo,FactoryCode||to_char(FactorySubCode),CalRemark,Params,FactoryName,trim(FactoryType)||trim(RiskCode),GrpPolNo " 
		+ "from LCPayRuleFactory where 1=1 "
		+ "and GrpContNo='" +tGrpContNo+ "' "
		+ "order by FactoryType, OtherNo,FactoryCode,FactorySubCode ";
	//fm.all('PayRuleCode').value = strSQL;
	turnPage = new turnPageClass();
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (turnPage.strQueryResult) {
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象
		turnPage.pageDisplayGrid = PayRuleGrid;
		//保存SQL语句
		turnPage.strQuerySql     = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
		//调用MULTILINE对象显示查询结果
		displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

		strSQL = "select PayRuleCode,PayRuleName from LCPayRuleFactory where GrpContNo = '" +GrpContNo+ "'";
		turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		fm.all('PayRuleCode').value = turnPage.arrDataCacheSet[0][0];
		fm.all('PayRuleName').value = turnPage.arrDataCacheSet[0][1];
	}

	strSQL = "select GrpContNo,ProposalGrpContNo,ManageCom,AppntNo,GrpName from LCGrpCont where GrpContNo = '" +GrpContNo+ "'";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.all('GrpContNo').value = turnPage.arrDataCacheSet[0][0];
	fm.all('ProposalGrpContNo').value = turnPage.arrDataCacheSet[0][1];
	fm.all('ManageCom').value = turnPage.arrDataCacheSet[0][2];
	fm.all('AppntNo').value = turnPage.arrDataCacheSet[0][3];
	fm.all('GrpName').value = turnPage.arrDataCacheSet[0][4];
	return ;
}

function initPayRule(tGrpContNo){
	// 书写SQL语句
	var k=0;
	var strSQL = "";

	/*strSQL = "select distinct a.FactoryType,b.FactoryTypeName,a.FactoryType||"+tGrpContNo+" from LMFactoryMode a ,LMFactoryType b  where 1=1 "
	+ " and a.FactoryType= b.FactoryType "
	+ " and (RiskCode =('"+tGrpContNo+"' ) or RiskCode ='000000' )";
	*/
	//alert(GrpContNo);
	strSQL = "select a.RiskCode,b.RiskName,a.GrpPolNo from LCGrpPol a,LMRisk b where a.GrpContNo='"+GrpContNo+"' and a.RiskCode = b.RiskCode";
	//fm.all('ff').value=strSQL;
	var str  = easyQueryVer3(strSQL, 1, 0, 1);
	//alert(str);
    return str;
}

function afterSubmit(FlagStr,content){
	showInfo.close();
	if( FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{
		content = "操作成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}