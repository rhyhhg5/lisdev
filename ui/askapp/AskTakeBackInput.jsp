<html>
<% 
//清空缓存
//response.setHeader("Pragma","No-cache"); 
//response.setHeader("Cache-Control","no-cache"); 
//response.setDateHeader("Expires", 0); 
%>

<%@page contentType="text/html;charset=GBK" %>
 
<%
//程序名称：AskCertTakeBackInput.jsp
//程序功能：
//创建日期：2002-08-07
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="AskTakeBackInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AskTakeBackInputInit.jsp"%>
</head>
<body  onload="initForm()" >
  <form action="./AskCertTakeBackSave.jsp" method=post name=fm target="fraSubmit">
  
    <table class="common">
    	<tr class="common">
    		<td class="input"><input class="cssButton" type="button" value="回  收" onclick="submitForm()" >
    		<input class="cssButton" type="button" value="查  询" onclick="queryClick()" ></td></tr>
    
    </table>
  
    <!-- 单证类型 -->
    <table class="common">
      <tr class="common">
        <td class="title">单证编码</td>
        <td><input class="code" name="CertifyCode" CodeData="0|^6007|补充资料通知书^6016|询价跟踪通知书" ondblclick="return showCodeListEx('SysCertCode', [this])"onkeyup="return showCodeListKeyEx('SysCertCode', [this])"></td>
        </tr>
    <!-- 回收的信息 -->
    <div style="width:120">
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divTakeBackInfo);"></td>
          <td class="titleImg">回收信息</td></tr></table></div>

    <div id="divTakeBackInfo">
      <!-- 单证号码和经办日期 -->
      <table class="common"> 
        <tr class="common">
          <td class="title" width="25%">单证号码</td>
          <td class="input" width="25%">
          	<input class="common" name="CertifyNo">
          	<input class="cssButton" type="button" value="查询单证" style="width:70" onclick="query()"></td>

          <td class="title" width="25%">有效日期</td>
          <td class="input" width="25%"><input class="readonly" readonly name="ValidDate"></td></tr>

        <tr class="common">
          <td class="title">发放者</td>
          <td class="input"><input class="common" name="SendOutCom" verify="发放者|NOTNULL"></td>

          <td class="title">接收者</td>
          <td class="input"><input class="common" name="ReceiveCom" verify="接收者|NOTNULL"></td></tr>

        <tr class="common">
          <td class="title">经办人</td>
          <td class="input"><input class="readonly" readonly name="Handler"></td>

          <td class="title">经办日期</td>
          <td class="input"><input class="readonly" readonly name="HandleDate"></td></tr>
        
        <tr class="common">
          <td class="title">操作员</td>
          <td class="input"><input class="readonly" readonly name="Operator"></td>
        
          <td class="title">入机日期</td>
          <td class="input"><input class="readonly" readonly name="MakeDate"></td></tr>
          
        <tr><td><br></td></tr>
    
        <tr class="common">
          <td class="title">回收操作员</td>
          <td class="input"><input class="readonly" readonly name="TakeBackOperator"></td>
        
          <td class="title">回收日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="TakeBackDate" verify="回收日期|DATE"></td></tr>

        <tr class="common">
          <td class="title">发放批次号</td>
          <td class="input"><input class="readonly" readonly name="SendNo"></td>
        
          <td class="title">回收批次号</td>
          <td class="input"><input class="readonly" readonly name="TakeBackNo"></td></tr>
        
        <tr class="common">
          <td class="title">回收操作日期</td>
          <td class="input"><input class="readonly" readonly name="TakeBackMakeDate"></td>
          
          <td class="title">回收操作时间</td>
          <td class="input"><input class="readonly" readonly name="TakeBackMakeTime"></td></tr>
        
       <input type=hidden id="ContNo" name="ContNo">
  	   <input type=hidden id="MissionID" name="MissionID">
  	   <input type=hidden id="SubMissionID" name="SubMissionID">
  	   <input type=hidden id="EdorNo" name="EdorNo">
      </table>
    </div>

	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    
    <input type="hidden" name="hideOperation">
    <input type="hidden" name="sql_where">
    
    
  	
  </form>
</body>
</html>
