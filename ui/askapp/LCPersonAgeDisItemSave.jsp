<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LCPersonAgeDisItemSave.jsp
//程序功能：
//创建日期：2005-07-19 15:10:09
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LCPersonAgeDisItemSet tLCPersonAgeDisItemSet   = new LCPersonAgeDisItemSet();
  OLCPersonAgeDisItemUI tOLCPersonAgeDisItemUI   = new OLCPersonAgeDisItemUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  	
  	
  	String tNum[] = request.getParameterValues("PersonAgeGridNo");
		String tStartAge[] = request.getParameterValues("PersonAgeGrid1");
		String tEndAge[] = request.getParameterValues("PersonAgeGrid2");           
		String tOnWorkMCount[] = request.getParameterValues("PersonAgeGrid3");        
		String tOnWorkFCount[] = request.getParameterValues("PersonAgeGrid4"); 
		String tOffWorkMCount[] = request.getParameterValues("PersonAgeGrid5"); 
		String tOffWorkFCount[] = request.getParameterValues("PersonAgeGrid6"); 
		String tMateMCount[] = request.getParameterValues("PersonAgeGrid7"); 
		String tMateFCount[] = request.getParameterValues("PersonAgeGrid8"); 
		String tYoungMCount[] = request.getParameterValues("PersonAgeGrid9"); 
		String tYoungFCount[] = request.getParameterValues("PersonAgeGrid10"); 
		String tOtherMCount[] = request.getParameterValues("PersonAgeGrid11"); 
		String tOtherFCount[] = request.getParameterValues("PersonAgeGrid12"); 
		
		int tCount = 0;
		if (tNum != null) tCount = tNum.length;
	      
		for (int i = 0; i < tCount; i++)	
		{
			LCPersonAgeDisItemSchema tLCPersonAgeDisItemSchema = new LCPersonAgeDisItemSchema();
			
			tLCPersonAgeDisItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
			tLCPersonAgeDisItemSchema.setPrtNo(request.getParameter("PrtNo"));
			tLCPersonAgeDisItemSchema.setStartAge(tStartAge[i]);
			tLCPersonAgeDisItemSchema.setEndAge(tEndAge[i]);
			tLCPersonAgeDisItemSchema.setOnWorkMCount(tOnWorkMCount[i]);
			tLCPersonAgeDisItemSchema.setOnWorkFCount(tOnWorkFCount[i]);
			tLCPersonAgeDisItemSchema.setOffWorkMCount(tOffWorkMCount[i]);
			tLCPersonAgeDisItemSchema.setOffWorkFCount(tOffWorkFCount[i]);
			tLCPersonAgeDisItemSchema.setMateMCount(tMateMCount[i]);
			tLCPersonAgeDisItemSchema.setMateFCount(tMateFCount[i]);
			tLCPersonAgeDisItemSchema.setYoungMCount(tYoungMCount[i]);
			tLCPersonAgeDisItemSchema.setYoungFCount(tYoungFCount[i]);
			tLCPersonAgeDisItemSchema.setOtherMCount(tOtherMCount[i]);
			tLCPersonAgeDisItemSchema.setOtherFCount(tOtherFCount[i]);

			tLCPersonAgeDisItemSet.add(tLCPersonAgeDisItemSchema);
		}

  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLCPersonAgeDisItemSet);
  	tVData.add(tG);
    tOLCPersonAgeDisItemUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLCPersonAgeDisItemUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
