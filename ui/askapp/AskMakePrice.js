//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var QueryResult="";
var QueryCount = 0;
var mulLineCount = 0;
var QueryWhere="";
var tSearch = 0;
var arrRisk = new Array();
var arrDuty = new Array();
var arrCode = new Array();
var PlanCode;
window.onfocus=myonfocus;
/*************************************
/**询价保单操作来源判断
**************************************
*/
function initButtons(){
	if(ActivityID=='0000006004'){//人工核保
		fm.MPRiskFactor.disabled = true;
		fm.MPDutyFactor.disabled =true;
		fm.MPSave.disabled = true;
		fm.PlanTypeNew.value='4';
		
	}else if(ActivityID='0000006011'){//产品定价
		fm.UWRiskFactor.disabled = true;
		fm.UWDutyFactor.disabled =true;
		fm.UWSave.disabled = true;
		fm.PlanTypeNew.value='5';
	}
}
/***************************************
**得到险种责任信息
****************************************
*/
function getQueryResultContPlanDutyGrid()
{
	
	var arrSelected = null;
	
	tRow = ContPlanDutyGrid.getSelNo();
	if( tRow == 0 || tRow == null )
		return arrSelected;
	arrSelected = new Array();
	
	arrSelected[0] = new Array();
	arrSelected[0][0] = ContPlanDutyGrid.getRowColData(tRow-1,1);
	arrSelected[0][1] = ContPlanDutyGrid.getRowColData(tRow-1,2);
	arrSelected[0][2] = ContPlanDutyGrid.getRowColData(tRow-1,3);
	arrSelected[0][3] = ContPlanDutyGrid.getRowColData(tRow-1,4);
//	arrSelected[0][4] = ContPlanDutyGrid.getRowColData(tRow-1,5);
//	arrSelected[0][5] = DemoGrid.getRowColData(tRow-1,6);
	
	return arrSelected;
}
/***************************************
**得到险种信息
****************************************
*/
function getQueryResultRiskInfoGrid()
{
	var arrSelected = null;
	tRow = RiskInfoGrid.getSelNo();

	if( tRow == 0 || tRow == null)
		return arrSelected;
	arrSelected = new Array();
	
	arrSelected[0] = new Array();
	arrSelected[0][0] = RiskInfoGrid.getRowColData(tRow-1,1);
	arrSelected[0][1] = RiskInfoGrid.getRowColData(tRow-1,2);
//	arrSelected[0][2] = RiskInfoGrid.getRowColData(tRow-1,3);
//	arrSelected[0][3] = DemoGrid.getRowColData(tRow-1,4);
//	arrSelected[0][4] = DemoGrid.getRowColData(tRow-1,5);
//	arrSelected[0][5] = DemoGrid.getRowColData(tRow-1,6);

	return arrSelected;
}


/***************************************
**得到保险计划信息
****************************************
*/
function getQueryResultContPlanCodeGrid()
{
	var arrSelected = null;
	tRow = ContPlanCodeGrid.getSelNo();

	if( tRow == 0 || tRow == null)
		return arrSelected;
	arrSelected = new Array();
	
	arrSelected[0] = new Array();
	arrSelected[0][0] = ContPlanCodeGrid.getRowColData(tRow-1,1);
	arrSelected[0][1] = ContPlanCodeGrid.getRowColData(tRow-1,2);
	arrSelected[0][2] = ContPlanCodeGrid.getRowColData(tRow-1,3);
//	arrSelected[0][3] = DemoGrid.getRowColData(tRow-1,4);
//	arrSelected[0][4] = DemoGrid.getRowColData(tRow-1,5);
//	arrSelected[0][5] = DemoGrid.getRowColData(tRow-1,6);

	return arrSelected;
}

function beforeSubmitNew(){
		var arrReturn = new Array();
		//three
		tSel = ContPlanCodeGrid.getSelNo();
		if( tSel == 0 || tSel == null ){
			alert("请选择  保险计划信息 ");
			return false;
		}else{
			try
			{
				arrCode = getQueryResultContPlanCodeGrid();
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
		}
		
		//two
		tSel = RiskInfoGrid.getSelNo();
		if( tSel == 0 || tSel == null ){
			alert("请选择  险种信息 ");
			return false;
		}else{
			try
			{
				arrRisk = getQueryResultRiskInfoGrid();
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
		}
		
		
		//one
		var tSel = ContPlanDutyGrid.getSelNo();
		if( tSel == 0 || tSel == null ){
			alert("请选择  险种责任信息 ");
			return false;
		}else{
			try
			{
				arrDuty = getQueryResultContPlanDutyGrid();
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
		}
		fm.PlanCodeNew.value = arrCode[0][0];
		fm.GrpContNoNew.value = GrpContNo;
		return true;
}

/***************************************
**核保师“保存”按钮事件
****************************************
*/
function UWSave1(){

		if (!beforeSubmitNew()){
			return false;
		}
		SaveRemark();
}

/***************************************
**产品定价“保存”按钮事件
****************************************
*/
function MPSave1(){
	if (!beforeSubmitNew()){
		return false;
	}
	SaveRemark2();
}

function afterCodeSelect( cCodeName, Field ){
	//判定双击操作执行的是什么查询
	if (cCodeName=="GrpRisk"){
		var tRiskFlag = fm.all('RiskFlag').value;
		//由于附加险不带出主险录入框，因此判定当主附险为S的时候隐藏
		if (tRiskFlag!="S"){
			divmainriskname.style.display = 'none';
			divmainrisk.style.display = 'none';
			fm.all('MainRiskCode').value = fm.all('RiskCode').value;
		}
		else{
			divmainriskname.style.display = '';
			divmainrisk.style.display = '';
			fm.all('MainRiskCode').value = "";
		}
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
	if (cShow=="true"){
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
	parent.fraMain.rows = "0,0,0,0,*";
}

//数据查询
function AddContClick(){
	if (fm.all('ContPlanCode').value == ""){
		alert("请输入保障级别！");
		fm.all('ContPlanCode').focus();
		return false;
	}
	if(fm.all('RiskCode').value ==""){
		alert("请选择险种！");
		fm.all('RiskCode').focus();
		return false;
	}
	if(fm.all('MainRiskCode').value ==""){
		alert("请输入主险信息！");
		fm.all('MainRiskCode').focus();
		return false;
	}
	var lineCount = 0;
	var MainRiskCode = fm.all('MainRiskCode').value
	var sRiskCode ="";
	var sMainRiskCode="";
	ContPlanDutyGrid.delBlankLine("ContPlanDutyGrid");
	lineCount = ContPlanDutyGrid.mulLineCount;
	//还需要添加一个校验，不过比较麻烦，暂时先作了
	for (i=0;i<ContPlanGrid.mulLineCount;i++){
		sRiskCode=ContPlanGrid.getRowColData(i,2);
		sMainRiskCode=ContPlanGrid.getRowColData(i,12);
		//主要是考虑附险会挂在不同的主险下，因此需要双重校验
		if (sRiskCode == fm.all('RiskCode').value && sMainRiskCode == MainRiskCode){
			alert("已添加过该险种保险计划要素！");
			return false;
		}
	}

	var getWhere = "(";
	for (i=0;i<lineCount;i++){
		if (ContPlanDutyGrid.getChkNo(i)){
			//alert(ContPlanDutyGrid.getRowColData(i,1));
			getWhere = getWhere + "'"+ContPlanDutyGrid.getRowColData(i,1)+"',"
		}
	}
	if (getWhere == "("){
		alert("请选则责任信息");
		return false;
	}
	getWhere = getWhere.substring(0,getWhere.length-1) + ")"
	//alert(getWhere);

	// 书写SQL语句
	var strSQL = "";

	if (QueryCount == 0){
		//查询该险种下的险种计算要素
		strSQL = "select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti,case a.CalFactorType when '1' then a.CalSql else '' end,'',b.RiskVer,d.GrpPolNo,'"+MainRiskCode+"',a.CalFactorType,c.CalMode "
			+ "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCGrpPol d "
			+ "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
			+ "and a.DutyCode in "+ getWhere + " and a.ChooseFlag in ('0','2') "
			+ "and GrpContNO = '"+GrpContNo+"' and a.RiskCode = '"+fm.all('RiskCode').value+"' order by a.RiskCode,a.DutyCode,a.FactorOrder";
		//fm.all('PlanSql').value = strSQL;
		//alert(strSQL);
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		if (!turnPage.strQueryResult) {
			alert("没有该险种责任下要素信息！");
			return false;
		}
		QueryCount = 1;
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = ContPlanGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
	}
	else{
		strSQL = "select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti,case a.CalFactorType when '1' then a.CalSql else '' end,'',b.RiskVer,d.GrpPolNo,'',a.CalFactorType,c.CalMode "
			+ "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCGrpPol d "
			+ "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
			+ "and a.DutyCode in "+ getWhere + " and a.ChooseFlag in ('0','2') "
			+ "and GrpContNO = '"+GrpContNo+"' and a.RiskCode = '"+fm.all('RiskCode').value+"' order by a.RiskCode,a.DutyCode,a.FactorOrder";
		//fm.all('ContPlanName').value = strSQL;
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		if (!turnPage.strQueryResult) {
			alert("没有该险种责任下要素信息！");
			return false;
		}
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		mulLineCount = ContPlanGrid.mulLineCount;
		//alert(mulLineCount);
		for(i=0; i<turnPage.arrDataCacheSet.length; i++){
			ContPlanGrid.addOne("ContPlanGrid");
			ContPlanGrid.setRowColData(mulLineCount+i,1,turnPage.arrDataCacheSet[i][0]);
			ContPlanGrid.setRowColData(mulLineCount+i,2,turnPage.arrDataCacheSet[i][1]);
			ContPlanGrid.setRowColData(mulLineCount+i,3,turnPage.arrDataCacheSet[i][2]);
			ContPlanGrid.setRowColData(mulLineCount+i,4,turnPage.arrDataCacheSet[i][3]);
			ContPlanGrid.setRowColData(mulLineCount+i,5,turnPage.arrDataCacheSet[i][4]);
			ContPlanGrid.setRowColData(mulLineCount+i,6,turnPage.arrDataCacheSet[i][5]);
			ContPlanGrid.setRowColData(mulLineCount+i,7,turnPage.arrDataCacheSet[i][6]);
			ContPlanGrid.setRowColData(mulLineCount+i,8,turnPage.arrDataCacheSet[i][7]);
			ContPlanGrid.setRowColData(mulLineCount+i,10,turnPage.arrDataCacheSet[i][9]);
			ContPlanGrid.setRowColData(mulLineCount+i,11,turnPage.arrDataCacheSet[i][10]);
			ContPlanGrid.setRowColData(mulLineCount+i,12,MainRiskCode);
			ContPlanGrid.setRowColData(mulLineCount+i,13,turnPage.arrDataCacheSet[i][12]);
		}
	}
	//initContPlanDutyGrid();
}

//数据提交（保存）
function submitForm(){
	if (!beforeSubmit()){
		return false;
	}
	fm.all('mOperate').value = "INSERT||MAIN";
	if (fm.all('mOperate').value == "INSERT||MAIN"){
		if (!confirm('计划 '+fm.all('ContPlanCode').value+' 下的全部险种要素信息是否已录入完毕，您是否要确认操作？')){
			return false;
		}
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	QueryCount = 0;	//重新初始化查询次数
	fm.submit(); //提交
}

//返回上一步
function returnparent(){
	parent.close();
}

//数据提交（删除）
function DelContClick(){
	if (fm.all('ContPlanCode').value == ""){
		alert("请选则要删除的计划！");
		fm.all('ContPlanCode').focus();
		return false;
	}
	fm.all('mOperate').value = "DELETE||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	QueryCount = 0;	//重新初始化查询次数
	fm.submit(); //提交
}

//数据提交（修改）
function UptContClick(){
	if (tSearch == 0){
		alert("请先查询要修改的保障计划！");
		return false;
	}
	
	fm.all('mOperate').value = "UPDATE||MAIN";
	if (!beforeSubmit()){
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	QueryCount = 0;	//重新初始化查询次数
	fm.submit(); //提交
}

//数据校验
function beforeSubmit(){
	if (fm.all('ContPlanCode').value == ""){
		alert("请输入保障级别！");
		fm.all('ContPlanCode').focus();
		return false;
	}
	if (fm.all('mOperate').value != "UPDATE||MAIN"){
		if (fm.all('MainRiskCode').value == ""){
			alert("请输入主险编码！");
			fm.all('MainRiskCode').focus();
			return false;
		}
	}
	if (ContPlanGrid.mulLineCount == 0){
		alert("请输入保险计划详细信息");
		return false;
	}
	var lineCount = ContPlanGrid.mulLineCount;
	var sValue;
	var sCalMode;
	//添加要素值信息校验
	for(var i=0;i<lineCount;i++){
      	sValue = ContPlanGrid.getRowColData(i,8);
      	sCalMode = ContPlanGrid.getRowColData(i,14);
      	//互算校验
      	if (sCalMode == "A"){
			if (sValue!=""){
				if (!isNumeric(sValue)){
					alert("请录入数字！");
					ContPlanGrid.setFocus(i,8);
					return false;
				}
			}
		}
		//保费算保额校验
      	if (sCalMode == "P"){
			if (sValue==""){
				alert("请录入保费！");
				ContPlanGrid.setFocus(i,8);
				return false;
			}
			if (!isNumeric(sValue)){
				alert("请录入数字！");
				ContPlanGrid.setFocus(i,8);
				return false;
			}
			if(sValue=0){
				alert("保费不能为0！");
				ContPlanGrid.setFocus(i,8);
				return false;
			}
		}
		//保额算保费校验
      	if (sCalMode == "G"){
			if (sValue==""){
				alert("请录入保额！");
				ContPlanGrid.setFocus(i,8);
				return false;
			}
			if (!isNumeric(sValue)){
				alert("请录入数字！");
				ContPlanGrid.setFocus(i,8);
				return false;
			}
			if(sValue==0){
				alert("保额不能为0！");
				ContPlanGrid.setFocus(i,8);
				return false;
			}
		}
		//其他因素算保费保额校验
      	if (sCalMode == "O"){
			if (sValue==""){
				alert("请录入要素值！");
				ContPlanGrid.setFocus(i,8);
				return false;
			}
			if (!isNumeric(sValue)){
				alert("请录入数字！");
				ContPlanGrid.setFocus(i,8);
				return false;
			}
			if(sValue==0){
				alert("要素值不能为0！");
				ContPlanGrid.setFocus(i,8);
				return false;
			}
		}
		//录入保费保额校验
      	if (sCalMode == "I"){
			if (sValue!=""){
				if (!isNumeric(sValue)){
					alert("请录入数字！");
					ContPlanGrid.setFocus(i,8);
					return false;
				}
			}
		}
	}
	return true;
}



function initFactoryType(tRiskCode)
{
	// 书写SQL语句
	var k=0;
	var strSQL = "";
	strSQL = "select distinct a.FactoryType,b.FactoryTypeName,a.FactoryType||"+tRiskCode+" from LMFactoryMode a ,LMFactoryType b  where 1=1 "
		   + " and a.FactoryType= b.FactoryType "
		   + " and (RiskCode = '"+tRiskCode+"' or RiskCode ='000000' )";
    var str  = easyQueryVer3(strSQL, 1, 0, 1);
    return str;
}

/*********************************************************************
 *  Click事件，当点击“保险计划要约录入”按钮时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function nextstep()
{
	var newWindow = window.open("../app/ContPlanNextInput.jsp?GrpContNo="+fm.all('GrpContNo').value+"&LoadFlag="+LoadFlag,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');

}

function afterSubmit(FlagStr,content){
	showInfo.close();
	if( FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{
		content = "操作成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
//	    initContPlanGrid();
	    tSearch = 0;
	    QueryCount = 0;
	}
	fm.all('mOperate').value = "";
}

/*********************************************************************
 *  查询险种下责任
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QueryDutyClick(){

	var tSel = RiskInfoGrid.getSelNo();
	var tRiskCode = RiskInfoGrid.getRowColData(tSel - 1,1);
/*
	strSQL = "select distinct t.dutycode,b.dutyname,a.choflag,case a.ChoFlag when 'M' then '必选' when 'B' then '备用' else '可选' end ChoFlagName"
					+ " from LCContPlanDutyParam t,LMRiskDuty a, LMDuty b "
					+ " where t.grpcontno = '"+GrpContNo+"'"
					+ " and t.plantype = '0'"
					+ " and a.DutyCode = b.DutyCode and a.RiskCode = t.RiskCode and a.DutyCode = t.DutyCode "
					+ " and t.RiskCode = '"+tRiskCode+"'"
					;
*/
	strSQL = "select t.dutycode,b.dutyname,a.choflag,case a.ChoFlag when 'M' then '必选' when 'B' then '备用' else '可选' end ChoFlagName,c.factorname,t.calfactorvalue"
					+ " from LCContPlanDutyParam t,LMRiskDuty a, LMDuty b,lmriskdutyfactor c "
					+ " where t.grpcontno = '"+GrpContNo+"'"
					+ " and t.plantype = '0'"
					+ " and c.chooseflag not in ('3','4','5')"
					+ " and t.calfactor = c.calfactor"
					+ " and a.DutyCode = b.DutyCode and a.RiskCode = t.RiskCode and a.DutyCode = t.DutyCode and t.dutycode=c.dutycode and t.riskcode = c.riskcode"
					+ " and t.RiskCode = '"+tRiskCode+"'"
					;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if (!turnPage.strQueryResult) {
		alert("没有该险种下的责任信息！");
		return false;
	}
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	
	turnPage.pageDisplayGrid = ContPlanDutyGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	//调用MULTILINE对象显示查询结果
	displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
/*
	var cDutyCode="";
	var tSql="";
	for(var i=0;i<=ContPlanDutyGrid.mulLineCount-1;i++){
	  cDutyCode=ContPlanDutyGrid.getRowColData(i,1);
	  tSql="select choflag from lmriskduty where riskcode='"+fm.all('RiskCode').value+"' and dutycode='"+cDutyCode+"'";
	  var arrResult=easyExecSql(tSql,1,0);
	  //alert("ChoFlag:"+arrResult[0]);
	  if(arrResult[0]=="M"){
	  	 ContPlanDutyGrid.checkBoxSel(i+1);
	  }	
	} 
	*/

}

function easyQueryClick(){
	initContPlanCodeGrid();

	//查询该险种下的险种计算要素
	strSQL = "select ContPlanCode,ContPlanName,PlanSql "
		+ "from LCContPlan "
		+ "where 1=1 "
		+ "and GrpContNo = '"+fm.all('GrpContNo').value+"' and plantype='4' order by ContPlanCode";
	//fm.all('ContPlanName').value = strSQL;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//如果没数据也无异常
	if (!turnPage.strQueryResult) {
		//return false;
	}
	else{
		//QueryCount = 1;
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = ContPlanCodeGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
	}
	//不管前面的查询什么结果，都执行下面的查询
	//如果发生参数错误，导致查询失败，这里程序会出错
	strSQL = "select GrpContNo,ProposalGrpContNo,ManageCom,AppntNo,GrpName from LCGrpCont where GrpContNo = '" +GrpContNo+ "'";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.all('GrpContNo').value = turnPage.arrDataCacheSet[0][0];
	fm.all('ProposalGrpContNo').value = turnPage.arrDataCacheSet[0][1];
	fm.all('ManageCom').value = turnPage.arrDataCacheSet[0][2];
	fm.all('AppntNo').value = turnPage.arrDataCacheSet[0][3];
	fm.all('GrpName').value = turnPage.arrDataCacheSet[0][4];
}

//单选框点击触发事件
function ShowContPlan(parm1,parm2){
	if(fm.all(parm1).all('InpContPlanCodeGridSel').value == '1'){
		//当前行第1列的值设为：选中
		var cContPlanCode = fm.all(parm1).all('ContPlanCodeGrid1').value;	//计划编码
		var cContPlanName = fm.all(parm1).all('ContPlanCodeGrid2').value;	//计划名称
		var cPlanSql = fm.all(parm1).all('ContPlanCodeGrid3').value;	//分类说明
		fm.all('ContPlanCode').value = cContPlanCode;
		fm.all('ContPlanName').value = cContPlanName;
		fm.all('PlanSql').value = cPlanSql;
		//alert(cContPlanCode);
		var cGrpContNo = fm.all('GrpContNo').value;

		//查询该险种下的险种计算要素
		strSQL = "select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti,d.CalFactorValue,d.Remark,b.RiskVer,d.GrpPolNo,d.MainRiskCode,d.CalFactorType,c.CalMode "
			+ "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCContPlanDutyParam d "
			+ "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
			+ "and a.DutyCode = d.DutyCode and a.CalFactor = d.CalFactor and b.RiskVer = d.RiskVersion "
			+ "and ContPlanCode = '"+fm.all('ContPlanCode').value+"'"
			+ "and GrpContNO = '"+GrpContNo+"' order by a.RiskCode,d.MainRiskCode,a.DutyCode";
		//fm.all('PlanSql').value = strSQL;
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		//原则上不会失败，嘿嘿
		//alert("===========================");
		if (!turnPage.strQueryResult) {
			alert("查询失败！");
			return false;
		}
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = ContPlanGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
		QueryCount = 1;
		tSearch = 1;
	}
}

//计划变更则修改查询状态变量
function ChangePlan(){
	QueryCount = 0;
    initContPlanDutyGrid();
    initContPlanGrid();
}

/*********************************************************************
 *  查询计划下险种信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ShowRiskInfo()
{
	var tSel = ContPlanCodeGrid.getSelNo();
	var tContPlanCode = ContPlanCodeGrid.getRowColData(tSel - 1,1);
	var strSQL = "select t.riskcode,b.riskname from LCContPlanRisk t,lmrisk b where 1=1"
							+ " and t.grpcontno = '"+GrpContNo+"'"
							+ " and t.ContPlanCode = '"+tContPlanCode+"'"
							+ " and t.plantype='4'"
							+ " and t.riskcode = b.riskcode "
							;
							
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		//原则上不会失败，嘿嘿
		if (!turnPage.strQueryResult) {
			alert("查询失败！");
			return false;
		}
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = RiskInfoGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);	
		
		//get remark
			var strSQL = "select t.remark2 from LCContPlan t where 1=1"
							+ " and t.grpcontno = '"+GrpContNo+"'"
							+ " and t.ContPlanCode = '"+tContPlanCode+"'"
							+ " and t.plantype='"+fm.PlanTypeNew.value+"'"
							;
		strQueryResultNew  = easyQueryVer3(strSQL, 1, 0, 1);
		if(strQueryResultNew != null){
			strIndx = strQueryResultNew.indexOf('^')+1;
			if(fm.PlanTypeNew.value == '4'){
				fm.UWIdea.value = strQueryResultNew.substring(strIndx);
			}else if(fm.PlanTypeNew.value == '5'){
				fm.MPIdea.value = strQueryResultNew.substring(strIndx);
			}
			strQueryResultNew = null;
		}else{
			fm.UWIdea.value = '';
			fm.MPIdea.value  = '';
		}
}

/*********************************************************************
 *  查询计划险种定价要素信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ShowRiskFactor()
{
	
	var tSel = RiskInfoGrid.getSelNo();
	var tRiskCode = RiskInfoGrid.getRowColData(tSel - 1,1);
	var tContSel = ContPlanCodeGrid.getSelNo();
	var tContPlanCode = ContPlanCodeGrid.getRowColData(tContSel - 1,1);	

	var strSQL = "select '"+tContPlanCode+"',t.riskcode,t.factorytype,t.factoryname,FactoryCode||to_char(FactorySubCode),t.factorysubname,(select params from lccontplanfactory a where a.grpcontno = '"+GrpContNo+"'  and a.plantype = '4' and a.factoryname = t.factoryname ),'','','"+tContPlanCode+"','',t.factoryname,t.riskcode,'' from lmfactorymode t where 1=1 "
							+ " and t.riskcode = '"+tRiskCode+"' or t.riskcode = '000000'"
							+ " and t.factorytype = '000010' order by t.riskcode,t.factoryname"
							;
							
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		//原则上不会失败，嘿嘿
		if (!turnPage.strQueryResult) {
			alert("查询失败！");
			return false;
		}
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = RiskFactorGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);	
		
		QueryMPRiskFactory();
		QueryDutyClick();
}

/*********************************************************************
 *  查询产品险种定价信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QueryMPRiskFactory()
{
	var tSel = RiskInfoGrid.getSelNo();
	var tRiskCode = RiskInfoGrid.getRowColData(tSel - 1,1);
	var tContSel = ContPlanCodeGrid.getSelNo();
	var tContPlanCode = ContPlanCodeGrid.getRowColData(tContSel - 1,1);	

	var strSQL = "select '"+tContPlanCode+"',t.riskcode,t.factorytype,t.factoryname,FactoryCode||to_char(FactorySubCode),t.factorysubname,(select params from lccontplanfactory a where a.grpcontno = '"+GrpContNo+"'  and a.plantype = '5' and a.factoryname = t.factoryname ),'','','"+tContPlanCode+"','',t.factoryname,t.riskcode,'' from lmfactorymode t where 1=1 "
							+ " and t.riskcode = '"+tRiskCode+"' or t.riskcode = '000000' "
							+ " and t.factorytype = '000010' order by t.riskcode,t.factoryname"
							;
							
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		//原则上不会失败，嘿嘿
		if (!turnPage.strQueryResult) {
			alert("查询失败！");
			return false;
		}
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = MPRiskFactorGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);	
}

/*********************************************************************
 *  查询责任要素信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ShowDutyFactor()
{
	initDutyFactorGrid();
	var tSel = RiskInfoGrid.getSelNo();
	var tRiskCode = RiskInfoGrid.getRowColData(tSel - 1,1);
	var tContSel = ContPlanCodeGrid.getSelNo();
	var tContPlanCode = ContPlanCodeGrid.getRowColData(tContSel - 1,1);	
	var tDutySel = ContPlanDutyGrid.getSelNo();
	var tDutyCode = ContPlanDutyGrid.getRowColData(tDutySel - 1,1);	

	var strSQL = "select  R,Y,U,I  from (select t.calfactor R,t.FactorName Y,(select CalFactorValue from LCContPlanDutyParam a where '1125312087000'='1125312087000' and  a.grpcontno = '1400000562' and a.riskcode = '1602' and a.plantype = '4' and a.CalFactor = t.CalFactor and t.dutycode = a.dutycode ) U,t.chooseflag I from LMRiskDutyFactor t where 1=1 "
							+ " and t.riskcode = '"+tRiskCode+"'"
							+ " and t.dutycode = '"+tDutyCode+"'"
							+ " and t.ChooseFlag = '5'"
							+ " union "
							+ " select t.calfactor R,t.FactorName Y,(select CalFactorValue from LCContPlanDutyParam a where a.grpcontno = '1400000562' and a.riskcode = '1602' and a.plantype= '4' and a.CalFactor = t.CalFactor  ) U,t.chooseflag I from LMRiskDutyFactor t where 1=1  "
							+ " and t.riskcode = '000000'"
							+ " and t.dutycode = '000000'"
							+ " and t.ChooseFlag = '5') picc order by R "
							;
							
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		//原则上不会失败，嘿嘿
		if (!turnPage.strQueryResult) {
			alert("查询失败！");
			return false;
		}
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = DutyFactorGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
		
		QueryMPDutyFactor();
				
}

/*********************************************************************
 *  查询定价责任要素信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QueryMPDutyFactor()
{
	var tSel = RiskInfoGrid.getSelNo();
	var tRiskCode = RiskInfoGrid.getRowColData(tSel - 1,1);
	var tContSel = ContPlanCodeGrid.getSelNo();
	var tContPlanCode = ContPlanCodeGrid.getRowColData(tContSel - 1,1);	
	var tDutySel = ContPlanDutyGrid.getSelNo();
	var tDutyCode = ContPlanDutyGrid.getRowColData(tDutySel - 1,1);	

	var strSQL = "select  R,Y,U,I  from (select t.calfactor R,t.FactorName Y,(select CalFactorValue from LCContPlanDutyParam a where '1125312087000'='1125312087000' and  a.grpcontno = '1400000562' and a.riskcode = '1602' and a.plantype = '4' and a.CalFactor = t.CalFactor and t.dutycode = a.dutycode ) U,t.chooseflag I from LMRiskDutyFactor t where 1=1 "
							+ " and t.riskcode = '"+tRiskCode+"' "
							+ " and t.dutycode = '"+tDutyCode+"'"
							+ " and t.ChooseFlag = '5'"
							+ " union "
							+ "select t.calfactor R,t.FactorName Y,(select CalFactorValue from LCContPlanDutyParam a where a.grpcontno = '1400000562' and a.riskcode = '1602' and a.plantype= '4' and a.CalFactor = t.CalFactor  ) U,t.chooseflag I from LMRiskDutyFactor t where 1=1  "
							+ " and t.riskcode = '000000' "
							+ " and t.dutycode = '000000'"
							+ " and t.ChooseFlag = '5') picc order by R "
							;
							
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
		//原则上不会失败，嘿嘿
		if (!turnPage.strQueryResult) {
			alert("查询失败！");
			return false;
		}
		//查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = MPDutyFactorGrid;
		//保存SQL语句
		turnPage.strQuerySql = strSQL;
		//设置查询起始位置
		turnPage.pageIndex = 0;
		//在查询结果数组中取出符合页面显示大小设置的数组
		//调用MULTILINE对象显示查询结果
		displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
				
}
/*
********************************************************
**核保师“保存”按钮事件业务逻辑处理
********************************************************
*/
function SaveRemark()
{
	fm.all('OperFlag').value = 'remark';
	fm.all('RiskFlag').value = 'UW';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}
/*
************************************************************
//产品定价“保存”按钮事件业务逻辑处理
************************************************************
*/
function SaveRemark2()
{
	fm.all('OperFlag').value = 'remark';
	fm.all('RiskFlag').value = 'MP';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}
/*********************************************************************
 *  险种定价信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function SaveRiskFactor()
{
	fm.all('OperFlag').value = 'risk';
	fm.all('RiskFlag').value = 'UW';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}

/*********************************************************************
 *  险种定价信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function SaveMPRiskFactor()
{
	fm.all('OperFlag').value = 'risk';
	fm.all('RiskFlag').value = 'MP';
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}

/*********************************************************************
 *  责任定价信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function SaveDutyFactor()
{
	var tSel = RiskInfoGrid.getSelNo();
	fm.RiskCode.value = RiskInfoGrid.getRowColData(tSel - 1,1);
	var tContSel = ContPlanCodeGrid.getSelNo();
	fm.ContPlanCode.value = ContPlanCodeGrid.getRowColData(tContSel - 1,1);	
	var tDutySel =ContPlanDutyGrid.getSelNo();
	fm.DutyCode.value = ContPlanDutyGrid.getRowColData(tContSel - 1,1);	

	fm.all('OperFlag').value = 'duty';
	fm.all('DutyFlag').value = 'UW';
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}

/*********************************************************************
 *  责任定价信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function SaveMPDutyFactor()
{
	var tSel = RiskInfoGrid.getSelNo();
	fm.RiskCode.value = RiskInfoGrid.getRowColData(tSel - 1,1);
	var tContSel = ContPlanCodeGrid.getSelNo();
	fm.ContPlanCode.value = ContPlanCodeGrid.getRowColData(tContSel - 1,1);	
	var tDutySel =ContPlanDutyGrid.getSelNo();
	fm.DutyCode.value = ContPlanDutyGrid.getRowColData(tContSel - 1,1);	

	fm.all('OperFlag').value = 'duty';
	fm.all('DutyFlag').value = 'MP';
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交	
}

/*********************************************************************
 *  将产品险种要素移动到核保险种要素
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function moveRiskFactor()
{
	var lineCount = MPRiskFactorGrid.mulLineCount;
	for(var i=0;i<lineCount;i++)
	{
		RiskFactorGrid.setRowColData(i,7,MPRiskFactorGrid.getRowColData(i,7));
	}
}

/*********************************************************************
 *  将产品责任要素移动到核保险种要素
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function moveDutyFactor()
{
	var lineCount = MPDutyFactorGrid.mulLineCount;
	for(var i=0;i<lineCount;i++)
	{
		DutyFactorGrid.setRowColData(i,3,MPDutyFactorGrid.getRowColData(i,3));
	}
}
