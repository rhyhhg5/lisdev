//程序名称：UWAuto.js
//程序功能：个人自动核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{	
	// 书写SQL语句
	k++;
	var strSQL = "";

	  //查询SQL，返回结果字符串
	strSQL = "select lwmission.MissionProp1,lwmission.MissionProp2,lwmission.MissionProp3,lwmission.MissionProp4,a.GrpName,LWMission.MissionID ,LWMission.SubMissionID,LWMission.activityid from lwmission ,ldgrp a where "+k+"="+k
	           + " and LWMission.ProcessID = '0000000006' " 
             + " and LWMission.ActivityID = '0000006011' " 
             + " and LWMission.MissionProp5 = a.CustomerNo "
             //+ getWherePart('lwmission.MissionProp1','GrpContNo')
 						 + getWherePart('lwmission.MissionProp2','PrtNo')
 						 + getWherePart('lwmission.MissionProp4','ManageCom','like')
 						 + getWherePart('a.GrpName','GrpNo')
 						 //fm.GrpContNo.value=strSQL
 						 ;
 	  turnPage.queryModal(strSQL, GrpGrid);
}

/*********************************************************************
 *  产品定价
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GotoMakePrice()
{
	var tSel = GrpGrid.getSelNo();
	var tGrpContNo = GrpGrid.getRowColData(tSel - 1,1);
	var tPrtNo = GrpGrid.getRowColData(tSel - 1,2);
	var MissionID = GrpGrid.getRowColData(tSel - 1,6);
	var SubMissionID = GrpGrid.getRowColData(tSel - 1,7);
	var ActivityID = GrpGrid.getRowColData(tSel - 1,8);
	
	window.location="../askapp/AskGroupUW.jsp?GrpContNo="+tGrpContNo+"&PrtNo="+tPrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ActivityID="+ActivityID; 
}