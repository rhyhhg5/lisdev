<!--Root="../../" -->
<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWManuHealthMain.jsp
//程序功能：续保人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<html>
<head>
<title>补充资料查询</title>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>   
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<SCRIPT language="javascript">
	var intPageWidth=screen.availWidth;
	var intPageHeight=screen.availHeight;
	window.resizeTo(intPageWidth,intPageHeight);
	window.moveTo(-1, -1);
	window.focus();	
		
	var initWidth = 0;
	//图片的队列数组
	var pic_name = new Array();
	var pic_place = 0;
	var b_img	= 0;  //放大图片的次数
	var s_img = 0;	//缩小图片的次数
	
	function queryScanType() 
	{
    	var strSql = "select code1, codename, codealias from ldcode1 where codetype='scaninput'";
    	var a = easyExecSql(strSql);

    	return a;
  	} 
</SCRIPT>
</head>
<!--<frameset rows="0,0,0,65,*" frameborder="no" border="1" framespacing="0" cols="*"> -->
<frameset name="fraMain" rows="0,0,0,0,*" frameborder="no" border="1" framespacing="0" cols="*">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="yes" noresize src="about:blank" >
	<frame name="fraTitle"  scrolling="no" noresize src="about:blank" >
		<frameset name="fraSet" rows="300,*" cols="*">
		<!--扫描图片显示-->
		<frame id="fraPic" name="fraPic" scrolling="auto" src="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo=<%=request.getParameter("ScanDocNo")%>&BussNoType=&BussType=TB&SubType=TB41">
		<!--交互区域-->
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="./AskInfoQ.jsp?GrpContNo=<%=request.getParameter("GrpContNo")%>&PrtNo=<%=request.getParameter("PrtNo")%>">
    	<!--下一步页面区域-->
    	<frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
