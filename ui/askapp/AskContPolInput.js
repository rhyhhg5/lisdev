//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var arrResult;
var mDebug = "0";
var mOperate = "";
var mAction = "";
//window.onfocus=focuswrap;
var mSwitch = parent.VD.gVSwitch;
var mShowCustomerDetail = "GROUPPOL";
var turnPage = new turnPageClass();
var cflag = "5";
var mWFlag = 0 ;

/*********************************************************************
 *  保存集体投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
  initMuline();
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  if(checkdatediff() == false){
      return false;
    }
  if( verifyInput2() == false )
    return false;
  if( !beforeSubmit() )
    return false;
  LCImpartInput();  //团体告知
  ImpartGrid.delBlankLine();
  	  if (CheckDateDollar()==false)
  {
  	return false;
  }
  if( mAction == "" )
  {
    //showSubmitFrame(mDebug);
    mAction = "INSERT";
    fm.all( 'fmAction' ).value = mAction;
    fm.all( 'LoadFlag' ).value = LoadFlag;


    if (fm.all('ProposalGrpContNo').value != "")
    {
      alert("查询结果只能进行修改操作！");
      mAction = "";
    }
    else
    {
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

      tAction = fm.action;
      fm.action="../askapp/AskContPolSave.jsp"
                fm.submit(); //提交
    }
  }
}

/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if( FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    content = "处理成功！";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //showDiv(operateButton, "true");
    //showDiv(inputButton, "false");
  }

  mAction = "";
  if(this.ScanFlag == "1")
  {
    fm.PrtNo.value=prtNo;
    initRiskGrid();
    fillriskgrid();
  }
  if(mWFlag == 1 && FlagStr != "Fail")
  {
    window.location.href("./AskContPolInput.jsp");
  }

  //location.reload();
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
  try
  {
    initForm();
    fm.all('PrtNo').value = prtNo;
  }
  catch( re )
  {
    alert("在GroupPolInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
  if( cDebug == "1" )
    parent.fraMain.rows = "0,0,50,82,*";
  else
    parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
  //下面增加相应的代码
  showDiv( operateButton, "false" );
  showDiv( inputButton, "true" );

  fm.all('RiskCode').value = "";

  //保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
  if (BQFlag=="2")
  {
    var strSql = "select grppolno, grpno from lcgrppol where prtno='" + prtNo + "' and riskcode in (select riskcode from lmriskapp where subriskflag='M')";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);

    mOperate = 1;
    afterQuery(arrResult);

    //strSql = "select GrpNo,GrpName,GrpAddress,Satrap from LDGrp where GrpNo='" + arrResult[0][1] + "'";
    //arrResult = easyExecSql(strSql);
    //mOperate = 2;
    //afterQuery(arrResult);

    fm.all('RiskCode').value = BQRiskCode;
    fm.all('RiskCode').className = "readonly";
    fm.all('RiskCode').readOnly = true;
    fm.all('RiskCode').ondblclick = "";
  }

  fm.all('ContNo').value = "";
  fm.all('ProposalGrpContNo').value = "";
}

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick()
{
  if(this.ScanFlag == "1")
  {
    alert( "有扫描件录入不允许查询!" );
    return false;
  }
  if( mOperate == 0 )
  {
    mOperate = 1;
    //cContNo = fm.all( 'ContNo' ).value;
    showInfo = window.open("./AskGroupPolQueryMain.jsp");
  }
}


/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
  initMuline();
  var tProposalGrpContNo = "";
  tProposalGrpContNo = fm.all( 'ProposalGrpContNo' ).value;
  
  if(checkdatediff() == false){
      return false;
    }
  if( verifyInput2() == false )
    return false;
  if( !beforeSubmit() )
    return false;

  //if(ImpartGrid.checkValue2(ImpartGrid.name,ImpartGrid)== false)return false;

  LCImpartInput();  //团体告知
  ImpartGrid.delBlankLine();
  	  if (CheckDateDollar()==false)
  {
  	return false;
  }
  if( tProposalGrpContNo == null || tProposalGrpContNo == "" )
    if(this.ScanFlag == "1")
    {
      alert( "还未录入数据,请先增加合同信息,再进行修改!" );
    }
    else
    {
      alert( "请先做投保单查询操作，再进行修改!" );
    }
  else
  {
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

    if( mAction == "" )
    {
      //showSubmitFrame(mDebug);
      showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      mAction = "UPDATE";
      fm.all( 'fmAction' ).value = mAction;
      fm.action="../askapp/AskContPolSave.jsp"
                fm.submit(); //提交
    }
  }
}

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick()
{
  var tProposalGrpContNo = "";
  tProposalGrpContNo = fm.all( 'ProposalGrpContNo' ).value;
  if( tProposalGrpContNo == null || tProposalGrpContNo == "" )
    alert( "请先做投保单查询操作，再进行删除!" );
  else
  {
    if (confirm("您确定要删除该团单吗？"))
    {
      var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

      if( mAction == "" )
      {
        //showSubmitFrame(mDebug);
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        mAction = "DELETE";
        fm.all( 'fmAction' ).value = mAction;
        fm.action="../askapp/AskContPolSave.jsp"
                  fm.submit(); //提交
      }
    }
  }
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
  if( cShow == "true" )
    cDiv.style.display = "";
  else
    cDiv.style.display = "none";
}

/*********************************************************************
 *  当点击“进入个人信息”按钮时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoPol()
{
  //下面增加相应的代码
  tProposalGrpContNo = fm.ProposalGrpContNo.value;
  if( tProposalGrpContNo == "" )
  {
    alert("您必须先录入集体信息才能进入个人信息部分。");
    return false
         }

         //把集体信息放入内存
         mSwitch = parent.VD.gVSwitch;  //桢容错
  putGrpPol();

  try
  {
    goToPic(2)
  }
  catch(e)
  {}

  try
  {
    parent.fraInterface.window.location = "./AskProposalGrpInput.jsp?LoadFlag=" + LoadFlag + "&type=" + type;
  }
  catch (e)
  {
    parent.fraInterface.window.location = "./AsProposalGrpInput.jsp?LoadFlag=2&type=" + type;
  }
}

/*********************************************************************
 *  把集体信息放入内存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function putGrpPol()
{
  delGrpPolVar();
  addIntoGrpPol();
}

/*********************************************************************
 *  把集体信息放入加到变量中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addIntoGrpPol()
{
  try
  {
    mSwitch.addVar( "intoPolFlag", "", "GROUPPOL" );
  }
  catch(ex)
  { }
  ;
  // body信息
  try
  {
    mSwitch.addVar( "BODY", "", window.document.body.innerHTML );
  }
  catch(ex)
  { }
  ;
  // 集体信息
  //由"./AutoCreatLDGrpInit.jsp"自动生成
  try
  {
    mSwitch.addVar('GrpNo', '', fm.all('GrpNo').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('PrtNo', '', fm.all('PrtNo').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Password', '', fm.all('Password').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpName', '', fm.all('GrpName').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpAddressCode', '', fm.all('GrpAddressCode').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpAddress', '', fm.all('GrpAddress').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpZipCode', '', fm.all('GrpZipCode').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('BusinessType', '', fm.all('BusinessType').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpNature', '', fm.all('GrpNature').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Peoples', '', fm.all('Peoples').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('RgtMoney', '', fm.all('RgtMoney').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Asset', '', fm.all('Asset').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('NetProfitRate', '', fm.all('NetProfitRate').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('MainBussiness', '', fm.all('MainBussiness').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Corporation', '', fm.all('Corporation').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ComAera', '', fm.all('ComAera').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('LinkMan1', '', fm.all('LinkMan1').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Department1', '', fm.all('Department1').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('HeadShip1', '', fm.all('HeadShip1').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Phone1', '', fm.all('Phone1').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('E_Mail1', '', fm.all('E_Mail1').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Fax1', '', fm.all('Fax1').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('LinkMan2', '', fm.all('LinkMan2').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Department2', '', fm.all('Department2').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('HeadShip2', '', fm.all('HeadShip2').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Phone2', '', fm.all('Phone2').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('E_Mail2', '', fm.all('E_Mail2').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Fax2', '', fm.all('Fax2').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Fax', '', fm.all('Fax').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Phone', '', fm.all('Phone').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GetFlag', '', fm.all('GetFlag').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Satrap', '', fm.all('Satrap').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('EMail', '', fm.all('EMail').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('FoundDate', '', fm.all('FoundDate').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('BankAccNo', '', fm.all('BankAccNo').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('BankCode', '', fm.all('BankCode').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpGroupNo', '', fm.all('GrpGroupNo').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('State', '', fm.all('State').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('BlacklistFlag', '', fm.all('BlacklistFlag').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Currency', '', fm.all('Currency').value);
  }
  catch(ex)
  { }
  ;

  try
  {
    mSwitch.addVar( "ContNo", "", fm.all( 'ContNo' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "ProposalGrpContNo", "", fm.all( 'ProposalGrpContNo' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "ManageCom", "", fm.all( 'ManageCom' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "SaleChnl", "", fm.all( 'SaleChnl' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "AgentCom", "", fm.all( 'AgentCom' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "AgentCode", "", fm.all( 'AgentCode' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "AgentGroup", "", fm.all( 'AgentGroup' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "AgentCode1", "", fm.all( 'AgentCode1' ).value );
  }
  catch(ex)
  { }
  ;

  try
  {
    mSwitch.addVar( "RiskCode", "", fm.all( 'RiskCode' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "RiskVersion", "", fm.all( 'RiskVersion' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "CValiDate", "", fm.all( 'CValiDate' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar( "PolApplyDate", "", fm.all( 'PolApplyDate' ).value );
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('StandbyFlag1', '', fm.all('StandbyFlag1').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('StandbyFlag2', '', fm.all('StandbyFlag2').value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('StandbyFlag3', '', fm.all('StandbyFlag3').value);
  }
  catch(ex)
  { }
  ;

}

/*********************************************************************
 *  把集体信息从变量中删除
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delGrpPolVar()
{
  try
  {
    mSwitch.deleteVar( "intoPolFlag" );
  }
  catch(ex)
  { }
  ;
  // body信息
  try
  {
    mSwitch.deleteVar( "BODY" );
  }
  catch(ex)
  { }
  ;
  // 集体信息
  //由"./AutoCreatLDGrpInit.jsp"自动生成
  try
  {
    mSwitch.deleteVar('GrpNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('PrtNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Password');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpName');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpAddressCode');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpAddress');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpZipCode');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('BusinessType');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpNature');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Peoples');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('RgtMoney');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Asset');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('NetProfitRate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('MainBussiness');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Corporation');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ComAera');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('LinkMan1');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Department1');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('HeadShip1');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Phone1');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('E_Mail1');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Fax1');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('LinkMan2');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Department2');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('HeadShip2');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Phone2');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('E_Mail2');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Fax2');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Fax');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Phone');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GetFlag');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Satrap');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('EMail');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('FoundDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('BankAccNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('BankCode');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpGroupNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('State');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('BlacklistFlag');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Currency');
  }
  catch(ex)
  { }
  ;

  mSwitch.deleteVar( "ContNo" );
  mSwitch.deleteVar( "ProposalGrpContNo" );
  mSwitch.deleteVar( "ManageCom" );
  mSwitch.deleteVar( "SaleChnl" );
  mSwitch.deleteVar( "AgentCom" );
  mSwitch.deleteVar( "AgentCode" );
  mSwitch.deleteVar( "AgentCode1" );

  mSwitch.deleteVar( "RiskCode" );
  mSwitch.deleteVar( "RiskVersion" );
  mSwitch.deleteVar( "CValiDate" );

}

/*********************************************************************
 *  Click事件，当双击“投保单位客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt1(){
  if( mOperate == 1||mOperate == 0 ){
    mOperate = 2;
    showInfo = window.open( "../sys/GroupMain.html" );
  }
}
function showAppnt()
{
  if (fm.all("GrpNo").value == "" )
  {
    showAppnt1();
  }
  else
  {
    arrResult = easyExecSql("select b.CustomerNo,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate from LDGrp b where  b.CustomerNo='" + fm.all("GrpNo").value + "'", 1, 0);
    if (arrResult == null)
    {
      alert("未查到投保单位信息");
    }
    else
    {
      displayAddress(arrResult[0]);
    }
  }
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    if( mOperate == 1 )
    {		// 查询集体投保单
      fm.all( 'ProposalGrpContNo' ).value = arrQueryResult[0][0];
      arrResult = easyExecSql("select * from LCGrpCont where grpcontno = '" + arrQueryResult[0][0] + "'", 1, 0);
      if (arrResult == null)
      {}
      else
      {
        displayLCGrpCont(arrResult[0]);
        fillriskgrid();
        arrResult = easyExecSql("select a.*,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples from LCGrpAddress a,LDGrp b where a.AddressNo=(select AddressNo from LCGrpAppnt  where GrpContNo = '" + arrResult[0][0] + "') and a.CustomerNo=(select CustomerNo from LCGrpAppnt  where GrpContNo = '" + arrResult[0][0] + "') and b.CustomerNo=(select CustomerNo from LCGrpAppnt  where GrpContNo = '" + arrResult[0][0] + "')", 1, 0);
        if (arrResult == null)
        {
          alert("未查到投保单位信息");
        }
        else
        {
          displayAddress1(arrResult[0]);
        }

      }
    }
    if( mOperate == 2 )
    {		// 投保单位信息
      //arrResult = easyExecSql("select b.CustomerNo,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples from LDGrp b where  b.CustomerNo='" + arrQueryResult[0][0] + "'", 1, 0);
      arrResult = easyExecSql("select b.CustomerNo,b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,a.grpaddress,a.GrpZipCode,a.LinkMan1,a.Phone1 from LDGrp b,LCGrpAddress a where  b.CustomerNo='" + arrQueryResult[0][0] + "' and a.CustomerNo=b.CustomerNo ", 1, 0);
      if (arrResult == null)
      {
        alert("未查到投保单位信息");
      }
      else
      {
        displayAddress(arrResult[0]);
      }
    }
  }
  getLCImpart();
  turnPage.queryModal("select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LCCustomerImpart where GrpContNo='" + arrQueryResult[0][0] + "'",ImpartGrid);
  turnPage.queryModal("select InsuStartYear,InsuEndYear,InsuContent,Rate,EnsureContent,Peoples,RecompensePeoples,OccurMoney,RecompenseMoney,PendingMoney,SerialNo from LCHistoryImpart where GrpContNo='"+ arrQueryResult[0][0] + "'",HistoryImpartGrid);
  turnPage.queryModal("select OcurTime,DiseaseName,DiseasePepoles,CureMoney,Remark,SerialNo from LCDiseaseImpart where GrpContNo='"+ arrQueryResult[0][0] + "'",DiseaseGrid);
  mOperate = 0;		// 恢复初态
  displayBookingPay();
}
/*********************************************************************
 *  把查询返回的客户地址数据返回
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAddress()
{
  try
  {
    fm.all('GrpNo').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('CustomerNo').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddressNo').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddress').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpZipCode').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan1').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department1').value="";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip1').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone1').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail1').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax1').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan2').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department2').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip2').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone2').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail2').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax2').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Operator').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeDate').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeTime').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyDate').value= "";
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyTime').value= "";
  }
  catch(ex)
  { }
  ;
  //以下是ldgrp表
  try
  {
    fm.all('GrpNo').value= arrResult[0][0];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpName').value= arrResult[0][1];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BusinessType').value= arrResult[0][2];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpNature').value= arrResult[0][3];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Peoples').value= arrResult[0][4];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RgtMoney').value= arrResult[0][5];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Asset').value= arrResult[0][6];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('NetProfitRate').value= arrResult[0][7];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MainBussiness').value= arrResult[0][8];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Corporation').value= arrResult[0][9];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ComAera').value= arrResult[0][10];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax').value= arrResult[0][11];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone').value= arrResult[0][12];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FoundDate').value= arrResult[0][13];
  }
  catch(ex)
  { }
  ;
  //ldgrpaddrss
  try
  {
    fm.all('GrpAddress').value= arrResult[0][17];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpZipCode').value= arrResult[0][18];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan1').value= arrResult[0][19];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone1').value= arrResult[0][20];
  }
  catch(ex)
  { }
  ;

  
}
function displayAddress1()
{
  try
  {
    fm.all('GrpNo').value= arrResult[0][0];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('CustomerNo').value= arrResult[0][0];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddressNo').value= arrResult[0][1];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddress').value= arrResult[0][2];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpZipCode').value= arrResult[0][3];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan1').value= arrResult[0][4];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department1').value= arrResult[0][5];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip1').value= arrResult[0][6];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone1').value= arrResult[0][7];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail1').value= arrResult[0][8];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax1').value= arrResult[0][9];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan2').value= arrResult[0][10];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department2').value= arrResult[0][11];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip2').value= arrResult[0][12];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone2').value= arrResult[0][13];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail2').value= arrResult[0][14];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax2').value= arrResult[0][15];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Operator').value= arrResult[0][16];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeDate').value= arrResult[0][17];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeTime').value= arrResult[0][18];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyDate').value= arrResult[0][19];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyTime').value= arrResult[0][20];
  }
  catch(ex)
  { }
  ;
  //以下是ldgrp表
  try
  {
    fm.all('BusinessType').value= arrResult[0][30];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpNature').value= arrResult[0][31];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Peoples').value= arrResult[0][32];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RgtMoney').value= arrResult[0][33];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Asset').value= arrResult[0][34];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('NetProfitRate').value= arrResult[0][35];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MainBussiness').value= arrResult[0][36];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Corporation').value= arrResult[0][37];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ComAera').value= arrResult[0][38];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax').value= arrResult[0][39];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone').value= arrResult[0][40];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FoundDate').value= arrResult[0][41];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AppntOnWorkPeoples').value = arrResult[0][42];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AppntOffWorkPeoples').value = arrResult[0][43];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AppntOtherPeoples').value = arrResult[0][44];
  }
  catch(ex)
  { }
  ;
}
/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt()
{
  //由"./AutoCreatLDGrpInit.jsp"自动生成
  try
  {
    fm.all('GrpNo').value = arrResult[0][0];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Password').value = arrResult[0][1];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpName').value = arrResult[0][2];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddressCode').value = arrResult[0][3];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddress').value = arrResult[0][4];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpZipCode').value = arrResult[0][5];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpNature').value = arrResult[0][7];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Peoples').value = arrResult[0][8];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RgtMoney').value = arrResult[0][9];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Asset').value = arrResult[0][10];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('NetProfitRate').value = arrResult[0][11];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MainBussiness').value = arrResult[0][12];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Corporation').value = arrResult[0][13];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ComAera').value = arrResult[0][14];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan1').value = arrResult[0][15];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department1').value = arrResult[0][16];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip1').value = arrResult[0][17];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone1').value = arrResult[0][18];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail1').value = arrResult[0][19];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax1').value = arrResult[0][20];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan2').value = arrResult[0][21];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department2').value = arrResult[0][22];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip2').value = arrResult[0][23];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone2').value = arrResult[0][24];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail2').value = arrResult[0][25];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax2').value = arrResult[0][26];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax').value = arrResult[0][27];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone').value = arrResult[0][28];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetFlag').value = arrResult[0][29];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Satrap').value = arrResult[0][30];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('EMail').value = arrResult[0][31];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FoundDate').value = arrResult[0][32];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BankAccNo').value = arrResult[0][33];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BankCode').value = arrResult[0][34];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpGroupNo').value = arrResult[0][35];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('State').value = arrResult[0][36];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Remark').value = arrResult[0][37];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BlacklistFlag').value = arrResult[0][38];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Operator').value = arrResult[0][39];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeDate').value = arrResult[0][40];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeTime').value = arrResult[0][41];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyDate').value = arrResult[0][42];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyTime').value = arrResult[0][43];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FIELDNUM').value = arrResult[0][44];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PK').value = arrResult[0][45];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('fDate').value = arrResult[0][46];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('mErrors').value = arrResult[0][47];
  }
  catch(ex)
  { }
  ;
}
function displayLCGrpCont()
{
  try
  {
    fm.all('GrpContNo').value= arrResult[0][0];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ProposalGrpContNo').value= arrResult[0][1];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PrtNo').value= arrResult[0][2];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SaleChnl').value= arrResult[0][3];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ManageCom').value= arrResult[0][4];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentCom').value= arrResult[0][5];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentComName').value= getNameByCode("Name","LACom","AgentCom",arrResult[0][5]);
  }
  catch(ex)
  {}
  ;
  try
  {
    fm.all('AgentType').value= arrResult[0][6];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentCode').value= arrResult[0][7];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentName').value= getNameByCode("Name","LAAgent","AgentCode",arrResult[0][7]);
  }
  catch(ex)
  {}
  ;
  //try {fm.all('AgentPhone').value= easyExecSql("select Phone from laagent where agentcode='"+arrResult[0][7]+"'");} catch(ex) {alert(ex)};
  try
  {
    fm.all('AgentPhone').value= getNameByCode("Phone","LAAgent","AgentCode",arrResult[0][7]);
  }
  catch(ex)
  {}
  ;
  try
  {
    fm.all('AgentGroup').value= arrResult[0][8];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentCode1').value= arrResult[0][9];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Password').value= arrResult[0][10];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Password2').value= arrResult[0][11];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AppntNo').value= arrResult[0][12];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddressNo').value= arrResult[0][13];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Peoples2').value= arrResult[0][14];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpName').value= arrResult[0][15];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BusinessType').value= arrResult[0][16];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpNature').value= arrResult[0][17];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RgtMoney').value= arrResult[0][18];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Asset').value= arrResult[0][19];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('NetProfitRate').value= arrResult[0][20];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MainBussiness').value= arrResult[0][21];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Corporation').value= arrResult[0][22];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ComAera').value= arrResult[0][23];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax').value= arrResult[0][24];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone').value= arrResult[0][25];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetFlag').value= arrResult[0][26];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Satrap').value= arrResult[0][27];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('EMail').value= arrResult[0][28];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FoundDate').value= arrResult[0][29];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpGroupNo').value= arrResult[0][30];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BankCode').value= arrResult[0][31];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BankAccNo').value= arrResult[0][32];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AccName').value= arrResult[0][33];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('DisputedFlag ').value= arrResult[0][34];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('OutPayFlag').value= arrResult[0][35];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetPolMode').value= arrResult[0][36];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Lang').value= arrResult[0][37];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Currency').value= arrResult[0][38];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LostTimes').value= arrResult[0][39];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PrintCount').value= arrResult[0][40];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RegetDate').value= arrResult[0][41];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LastEdorDate').value= arrResult[0][42];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LastGetDate').value= arrResult[0][43];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LastLoanDate').value= arrResult[0][44];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SpecFlag').value= arrResult[0][45];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpSpec').value= arrResult[0][46];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PayMode').value= arrResult[0][47];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SignCom').value= arrResult[0][48];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SignDate').value= arrResult[0][49];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SignTime').value= arrResult[0][50];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('CValiDate').value= arrResult[0][51];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpContPayIntv').value= arrResult[0][52];
    if(fm.all('GrpContPayIntv').value==-1)
    {
    	fm.all('divBookingPayInty').style.display='';
    	}
    else{
    	fm.all('divBookingPayInty').style.display='none';
    	}
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ManageFeeRate').value= arrResult[0][53];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ExpPeoples').value= arrResult[0][54];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ExpPremium').value= arrResult[0][55];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ExpAmnt').value= arrResult[0][56];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Peoples').value= arrResult[0][57];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Mult').value= arrResult[0][58];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Prem').value= arrResult[0][59];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Amnt').value= arrResult[0][60];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SumPrem').value= arrResult[0][61];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SumPay').value= arrResult[0][62];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Dif').value= arrResult[0][63];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Remark').value= arrResult[0][64];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('StandbyFlag1').value= arrResult[0][65];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('StandbyFlag2').value= arrResult[0][66];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('StandbyFlag3').value= arrResult[0][67];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('InputOperator').value= arrResult[0][68];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('InputDate').value= arrResult[0][69];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('InputTime').value= arrResult[0][70];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ApproveFlag').value= arrResult[0][71];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ApproveCode').value= arrResult[0][72];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ApproveDate').value= arrResult[0][73];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ApproveTime').value= arrResult[0][74];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('UWOperator').value= arrResult[0][75];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('UWFlag').value= arrResult[0][76];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('UWDate').value= arrResult[0][77];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('UWTime').value= arrResult[0][78];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AppFlag').value= arrResult[0][79];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PolApplyDate').value= arrResult[0][80];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('CustomGetPolDate').value= arrResult[0][81];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetPolDate').value= arrResult[0][82];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetPolTime').value= arrResult[0][83];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('State').value= arrResult[0][84];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Operator').value= arrResult[0][85];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeDate').value= arrResult[0][86];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeTime').value= arrResult[0][87];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyDate').value= arrResult[0][88];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyTime').value= arrResult[0][89];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('EnterKind').value= arrResult[0][90];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AmntGrade').value= arrResult[0][91];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Peoples3').value= arrResult[0][92];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('OnWorkPeoples').value= arrResult[0][93];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('OffWorkPeoples').value= arrResult[0][94];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('OtherPeoples').value= arrResult[0][95];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RelaPeoples').value= arrResult[0][96];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RelaMatePeoples').value= arrResult[0][97];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RelaYoungPeoples').value= arrResult[0][98];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RelaOtherPeoples').value= arrResult[0][99];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FirstTrialOperator').value= arrResult[0][100];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FirstTrialDate').value= arrResult[0][101];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FirstTrialTime').value= arrResult[0][102];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ReceiveOperator').value= arrResult[0][103];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ReceiveDate').value= arrResult[0][104];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ReceiveTime').value= arrResult[0][105];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('TempFeeNo').value= arrResult[0][106];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HandlerName').value= arrResult[0][107];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HandlerDate').value= arrResult[0][108];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HandlerPrint').value= arrResult[0][109];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentDate').value= arrResult[0][110];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BusinessBigType').value= arrResult[0][111];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MarketType').value= arrResult[0][112];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ProposalType').value= arrResult[0][113];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SaleChnlDetail').value= arrResult[0][114];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ContPrintLoFlag').value= arrResult[0][115];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PremApportFlag').value= arrResult[0][116];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ContPremFeeNo').value= arrResult[0][117];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('CustomerReceiptNo').value= arrResult[0][118];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('CInValiDate').value= arrResult[0][119];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RoleAgetCode').value= arrResult[0][120];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AskGrpContNo').value= arrResult[0][121];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AskGrpContNo').value= arrResult[0][121];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('OperationManager').value= arrResult[0][123];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('InfoSource').value= arrResult[0][124];
  }
  catch(ex)
  { }
  ;
}

function displayLCGrpPol()
{
  //由"./AutoCreatLCGrpPolInit.jsp"自动生成
  try
  {
    fm.all('ContNo').value = arrResult[0][0];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ProposalGrpContNo').value = arrResult[0][1];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PrtNo').value = arrResult[0][2];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('KindCode').value = arrResult[0][3];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RiskCode').value = arrResult[0][5];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RiskVersion').value = arrResult[0][6];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SignCom').value = arrResult[0][7];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ManageCom').value = arrResult[0][8];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentCom').value = arrResult[0][9];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentType').value = arrResult[0][10];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SaleChnl').value = arrResult[0][11];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Password').value = arrResult[0][12];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpNo').value = arrResult[0][13];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Password2').value = arrResult[0][14];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpName').value = arrResult[0][15];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddressCode').value = arrResult[0][16];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddress').value = arrResult[0][17];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpZipCode').value = arrResult[0][18];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BusinessType').value = arrResult[0][19];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpNature').value = arrResult[0][20];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Peoples2').value = arrResult[0][21];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RgtMoney').value = arrResult[0][22];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Asset').value = arrResult[0][23];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('NetProfitRate').value = arrResult[0][24];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MainBussiness').value = arrResult[0][25];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Corporation').value = arrResult[0][26];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ComAera').value = arrResult[0][27];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan1').value = arrResult[0][28];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department1').value = arrResult[0][29];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip1').value = arrResult[0][30];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone1').value = arrResult[0][31];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail1').value = arrResult[0][32];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax1').value = arrResult[0][33];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan2').value = arrResult[0][34];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department2').value = arrResult[0][35];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip2').value = arrResult[0][36];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone2').value = arrResult[0][37];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail2').value = arrResult[0][38];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax2').value = arrResult[0][39];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax').value = arrResult[0][40];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone').value = arrResult[0][41];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetFlag').value = arrResult[0][42];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Satrap').value = arrResult[0][43];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('EMail').value = arrResult[0][44];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FoundDate').value = arrResult[0][45];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BankAccNo').value = arrResult[0][46];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BankCode').value = arrResult[0][47];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpGroupNo').value = arrResult[0][48];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PayIntv').value = arrResult[0][49];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PayMode').value = arrResult[0][50];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('CValiDate').value = arrResult[0][51];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetPolDate').value = arrResult[0][52];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SignDate').value = arrResult[0][53];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FirstPayDate').value = arrResult[0][54];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PayEndDate').value = arrResult[0][55];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PaytoDate').value = arrResult[0][56];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('RegetDate').value = arrResult[0][57];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Peoples').value = arrResult[0][58];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Mult').value = arrResult[0][59];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Prem').value = arrResult[0][60];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Amnt').value = arrResult[0][61];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SumPrem').value = arrResult[0][62];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SumPay').value = arrResult[0][63];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Dif').value = arrResult[0][64];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('SSFlag').value = arrResult[0][65];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PeakLine').value = arrResult[0][66];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetLimit').value = arrResult[0][67];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetRate').value = arrResult[0][68];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MaxMedFee').value = arrResult[0][69];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ExpPeoples').value = arrResult[0][70];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ExpPremium').value = arrResult[0][71];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ExpAmnt').value = arrResult[0][72];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('DisputedFlag').value = arrResult[0][73];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('BonusRate').value = arrResult[0][74];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Lang').value = arrResult[0][75];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Currency').value = arrResult[0][76];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('State').value = arrResult[0][77];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LostTimes').value = arrResult[0][78];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AppFlag').value = arrResult[0][79];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ApproveCode').value = arrResult[0][80];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ApproveDate').value = arrResult[0][81];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('UWOperator').value = arrResult[0][82];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentCode').value = arrResult[0][83];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentGroup').value = arrResult[0][84];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('AgentCode1').value = arrResult[0][85];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Remark').value = arrResult[0][86];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('UWFlag').value = arrResult[0][87];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('OutPayFlag').value = arrResult[0][88];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ApproveFlag').value = arrResult[0][89];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('EmployeeRate').value = arrResult[0][90];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FamilyRate').value = arrResult[0][91];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Operator').value = arrResult[0][92];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeDate').value = arrResult[0][93];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('MakeTime').value = arrResult[0][94];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyDate').value = arrResult[0][95];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ModifyTime').value = arrResult[0][96];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('FIELDNUM').value = arrResult[0][97];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PK').value = arrResult[0][98];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('fDate').value = arrResult[0][99];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('ManageFeeRate').value = arrResult[0][100];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpSpec').value = arrResult[0][101];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GetPolMode').value = arrResult[0][102];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('PolApplyDate').value = arrResult[0][103];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('StandbyFlag1').value = arrResult[0][105];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('StandbyFlag2').value = arrResult[0][106];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('StandbyFlag3').value = arrResult[0][107];
  }
  catch(ex)
  { }
  ;

}




/*********************************************************************
 *  Click事件，当点击“关联暂交费信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showFee()
{
  cPolNo = fm.ProposalGrpContNo.value;
  if( cPolNo == "" )
  {
    alert( "您必须先查询投保单才能进入暂交费信息部分。" );
    return false
         }

         showInfo = window.open( "./ProposalFee.jsp?PolNo=" + cPolNo + "&polType=GROUP" );
}

function queryAgent()
{
  if(fm.all('ManageCom').value=="")
  {
    alert("请先录入管理机构信息！");
    return;
  }
  if(fm.all('AgentCode').value == "")
  {
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=2","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  }
  if(fm.all('AgentCode').value != "")
  {
    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null)
    {
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else
    {
      fm.AgentGroup.value="";
      alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
    }
  }
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{
  if(arrResult!=null)
  {
    fm.AgentCode.value = arrResult[0][0];
    fm.AgentGroup.value = arrResult[0][1];
    fm.AgentName.value = arrResult[0][5];
    fm.AgentPhone.value = arrResult[0][16];
  }
}

function queryAgent2()
{
  if(fm.all('ManageCom').value=="")
  {
    alert("请先录入管理机构信息！");
    return;
  }
  if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==8 )
  {
    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null)
    {
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else
    {
      fm.AgentGroup.value="";
      alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
    }
  }
}

function afterCodeSelect( cCodeName, Field )
{
  if(cCodeName=="GetGrpAddressNo")
  {
    var strSQL="select b.AddressNo,b.GrpAddress,b.GrpZipCode,b.LinkMan1,b.Department1,b.HeadShip1,b.Phone1,b.E_Mail1,b.Fax1,b.LinkMan2,b.Department2,b.HeadShip2,b.Phone2,b.E_Mail2,b.Fax2 from LCGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
    arrResult=easyExecSql(strSQL);
    try
    {
      fm.all('GrpAddressNo').value= arrResult[0][0];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('GrpAddress').value= arrResult[0][1];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('GrpZipCode').value= arrResult[0][2];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('LinkMan1').value= arrResult[0][3];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('Department1').value= arrResult[0][4];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('HeadShip1').value= arrResult[0][5];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('Phone1').value= arrResult[0][6];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('E_Mail1').value= arrResult[0][7];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('Fax1').value= arrResult[0][8];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('LinkMan2').value= arrResult[0][9];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('Department2').value= arrResult[0][10];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('HeadShip2').value= arrResult[0][11];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('Phone2').value= arrResult[0][12];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('E_Mail2').value= arrResult[0][13];
    }
    catch(ex)
    { }
    ;
    try
    {
      fm.all('Fax2').value= arrResult[0][14];
    }
    catch(ex)
    { }
    ;
  }
  
  if(cCodeName=="salechnl")
  {
  	if(Field.value!="03") {
      fm.all('AgentCom').style.display="none";
  		fm.all('AgentComName').style.display="none";
  		fm.all('AgentCom').value="";
  		fm.all('AgentComName').value="";
  		fm.all('AgentFee').value="";
  		fm.all('divAgentFee').style.display="none";

    } else {
      fm.all('AgentCom').style.display="";
  		fm.all('AgentComName').style.display="";
  		
  		fm.all('AgentCom').className = "code";
	    //fm.all('AgentCom').readOnly = false;
	    ////fm.all('AgentCom').ondblclick = " showCodeList('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');";
	    fm.all('AgentComName').className = "common";
	    fm.all('divAgentFee').style.display="";
    }
  	}
   if(cCodeName=="grpPayIntv")
  {
  	if(Field.value == -1)
  	{
  		//为约定缴费方式
  	fm.all('divBookingPayInty').style.display='';}
  else
  	{
  	fm.all('divBookingPayInty').style.display='none';
  	BookingPayIntyGrid.clearData("BookingPayIntyGrid");
  		}
  }
}

function checkMainAppender(cRiskCode)
{
  if( isSubRisk( cRiskCode ) == true )
  {   // 附险
    var tPolNo = getMainRiskNo(cRiskCode);   //弹出录入附险的窗口,得到主险保单号码
    if (!checkRiskRelation(tPolNo, cRiskCode))
    {
      alert("主附险包含关系错误，输入的主险号不能带这个附加险！");
      return false;
    }
  }
  return true;
}


function isSubRisk(cRiskCode)
{
  var arrQueryResult = easyExecSql("select SubRiskFlag from LMRiskApp where RiskCode='" + cRiskCode + "'", 1, 0);

  if(arrQueryResult[0] == "S")    //需要转成大写
    return true;
  if(arrQueryResult[0] == "M")
    return false;

  if (arrQueryResult[0].toUpperCase() == "A")
    if (confirm("该险种既可以是主险,又可以是附险!选择确定进入主险录入,选择取消进入附险录入"))
      return false;
    else
      return true;

  return false;
}



function checkRiskRelation(tPolNo, cRiskCode)
{
  // 集体下个人投保单
  var strSql = "select RiskCode from LCGrpPol where GrpPolNo = '" + tPolNo
               + "' and RiskCode in (select Code1 from LDCode1 where Code = '" + cRiskCode + "' and codetype='grpchkappendrisk')";
  return easyQueryVer3(strSql);
}

function getMainRiskNo(cRiskCode)
{
  var urlStr = "../app/MainRiskNoInput.jsp";
  var tPolNo="";

  tPolNo = window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:310px;dialogHeight:100px;center:yes;");
  return tPolNo;
}

function grpRiskInfo()
{
  //alert('a');
  //var newWindow = window.open("../app/GroupRisk.jsp");
  if (fm.ProposalGrpContNo.value=="")
  {
    alert("必须保存合同信息才能进入〔险种信息〕！");
  }
  delGrpVar();
  addGrpVar();
  showInfo = window.open("../app/GroupRisk.jsp");
}

function grpInsuInfo()
{
  if (fm.ProposalGrpContNo.value=="")
  {
    alert("必须保存合同信息才能〔添加被保人〕信息！");
    return false;
  }
  //alert("1111"+fm.GrpContNo.value);
  fm.GrpContNo.value=fm.ProposalGrpContNo.value;
  delGrpVar();
  addGrpVar();
  parent.fraInterface.window.location = "../askapp/AskContInsuredInput.jsp?LoadFlag=2&ContType=2&scantype="+ scantype;
}

function grpInsuList()
{
  if (fm.ProposalGrpContNo.value=="")
  {
    alert("必须保存合同信息才能进入〔被保人清单〕信息界面！");
    return false;
  }

  delGrpVar();
  alert();
  addGrpVar();
  alert();
  var LocalWin=parent.fraInterface.window.location = "../askapp/AskContGrpInsuredInput.jsp?LoadFlag="+LoadFlag+"&scantype="+scantype;

}
function grpRiskPlanInfo()
{
  if (fm.ProposalGrpContNo.value=="")
  {
    alert("必须保存合同信息才能进行〔保险计划制定〕！");
    return false;
  }
  var newWindow = window.open("../askapp/AskContPlan.jsp?GrpContNo="+fm.GrpContNo.value+"&LoadFlag="+LoadFlag+"&EnterKind="+fm.EnterKind.value);
}

function focuswrap()
{
  myonfocus(showInfo);
}

function delGrpVar()
{
  //删除可能留在缓存中的个人合同信息
  try
  {
    mSwitch.deleteVar('ContNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ProposalContNo');
  }
  catch(ex)
  { }
  ;

  //团体合同信息
  try
  {
    mSwitch.deleteVar('GrpContNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ProposalGrpContNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('PrtNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('SaleChnl');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ManageCom');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AgentCom');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AgentType');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AgentCode');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AgentGroup');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AgentCode1');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Password');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Password2');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AppntNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AddressNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Peoples2');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpName');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('BusinessType');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpNature');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('RgtMoney');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Asset');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('NetProfitRate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('MainBussiness');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Corporation');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ComAera');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Fax');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Phone');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GetFlag');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Satrap');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('EMail');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('FoundDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpGroupNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('BankCode');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('BankAccNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AccName');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('DisputedFlag');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('OutPayFlag');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GetPolMode');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Lang');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Currency');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('LostTimes');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('PrintCount');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('RegetDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('LastEdorDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('LastGetDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('LastLoanDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('SpecFlag');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpSpec');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('PayMode');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('SignCom');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('SignDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('SignTime');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('CValiDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('PayIntv');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ManageFeeRate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ExpPeoples');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ExpPremium');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ExpAmnt');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Peoples');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Mult');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Prem');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Amnt');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('SumPrem');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('SumPay');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Dif');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Remark');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('StandbyFlag1');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('StandbyFlag2');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('StandbyFlag3');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('InputOperator');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('InputDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('InputTime');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ApproveFlag');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ApproveCode');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ApproveDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('ApproveTime');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('UWOperator');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('UWFlag');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('UWDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('UWTime');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AppFlag');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('PolApplyDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('CustomGetPolDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GetPolDate');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GetPolTime');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('State');
  }
  catch(ex)
  { }
  ;
  //集体投保人信息
  try
  {
    mSwitch.deleteVar('GrpNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AddressNo');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AppntGrade');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpName');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('PostalAddress');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('GrpZipCode');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Phone');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('Password');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('State');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('AppntType');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.deleteVar('RelationToInsured');
  }
  catch(ex)
  { }
  ;


}

function addGrpVar()
{
  try
  {
    mSwitch.addVar('ContNo','','');
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ProposalContNo','','');
  }
  catch(ex)
  { }
  ;
  //集体合同信息
  try
  {
    mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ProposalGrpContNo','',fm.ProposalGrpContNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('PrtNo','',fm.PrtNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('SaleChnl','',fm.SaleChnl.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ManageCom','',fm.ManageCom.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AgentCom','',fm.AgentCom.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AgentType','',fm.AgentType.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AgentCode','',fm.AgentCode.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AgentGroup','',fm.AgentGroup.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AgentCode1','',fm.AgentCode1.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Password','',fm.Password.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Password2','',fm.Password2.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AppntNo','',fm.AppntNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Addressno','',fm.AddressNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Peoples2','',fm.Peoples2.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpName','',fm.GrpName.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('BusinessType','',fm.BusinessType.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpNature','',fm.GrpNature.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('RgtMoney','',fm.RgtMoney.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Asset','',fm.Asset.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('NetProfitRate','',fm.NetProfitRate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('MainBussiness','',fm.MainBussiness.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Corporation','',fm.Corporation.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ComAera','',fm.ComAera.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Fax','',fm.Fax.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Phone','',fm.Phone.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GetFlag','',fm.GetFlag.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Satrap','',fm.Satrap.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('EMail','',fm.EMail.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('FoundDate','',fm.FoundDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpGroupNo','',fm.GrpGroupNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('BankCode','',fm.BankCode.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AccName','',fm.AccName.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('DisputedFlag','',fm.DisputedFlag.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('OutPayFlag','',fm.OutPayFlag.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GetPolMode','',fm.GetPolMode.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Lang','',fm.Lang.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Currency','',fm.Currency.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('LostTimes','',fm.LostTimes.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('PrintCount','',fm.PrintCount.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('RegetDate','',fm.RegetDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('LastEdorDate','',fm.LastEdorDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('LastGetDate','',fm.LastGetDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('LastLoanDate','',fm.LastLoanDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('SpecFlag','',fm.SpecFlag.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpSpec','',fm.GrpSpec.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('PayMode','',fm.PayMode.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('SignCom','',fm.SignCom.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('SignDate','',fm.SignDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('SignTime','',fm.SignTime.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('CValiDate','',fm.CValiDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('PayIntv','',fm.PayIntv.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ManageFeeRate','',fm.ManageFeeRate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ExpPeoples','',fm.ExpPeoples.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ExpPremium','',fm.ExpPremium.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ExpAmnt','',fm.ExpAmnt.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Peoples','',fm.Peoples.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Mult','',fm.Mult.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Prem','',fm.Prem.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Amnt','',fm.Amnt.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('SumPrem','',fm.SumPrem.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('SumPay','',fm.SumPay.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Dif','',fm.Dif.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Remark','',fm.Remark.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('StandbyFlag1','',fm.StandbyFlag1.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('StandbyFlag2','',fm.StandbyFlag2.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('StandbyFlag3','',fm.StandbyFlag3.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('InputOperator','',fm.InputOperator.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('InputDate','',fm.InputDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('InputTime','',fm.InputTime.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ApproveFlag','',fm.ApproveFlag.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ApproveCode','',fm.ApproveCode.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ApproveDate','',fm.ApproveDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ApproveTime','',fm.ApproveTime.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('UWOperator','',fm.UWOperator.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('UWFlag','',fm.UWFlag.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('UWDate','',fm.UWDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('UWTime','',fm.UWTime.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AppFlag','',fm.AppFlag.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('PolApplyDate','',fm.PolApplyDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('CustomGetPolDate','',fm.CustomGetPolDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GetPolDate','',fm.GetPolDate.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GetPolTime','',fm.GetPolTime.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('State','',fm.State.value);
  }
  catch(ex)
  { }
  ;
  //集体投保人信息

  try
  {
    mSwitch.addVar('GrpNo','',fm.GrpNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('PrtNo','',fm.PrtNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AddressNo','',fm.AddressNo.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AppntGrade','',fm.AppntGrade.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('GrpName','',fm.Name.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('PostalAddress','',fm.PostalAddress.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('ZipCode','',fm.ZipCode.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Phone','',fm.Phone.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('Password','',fm.Password.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('State','',fm.State.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('AppntType','',fm.AppntType.value);
  }
  catch(ex)
  { }
  ;
  try
  {
    mSwitch.addVar('RelationToInsured','',fm.RelationToInsured.value);
  }
  catch(ex)
  { }
  ;

}
function checkuseronly(comname)
{
  arrResult = easyExecSql("select * from LDGrp  where GrpName='" + comname + "'", 1, 0);
  if (arrResult == null)
  {}
  else
  {
    arrResult = easyExecSql("select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.CustomerNo from LDGrp b where  b.CustomerNo='" + arrResult[0][0] + "'", 1, 0);
    if (arrResult == null)
    {
      alert("未查到投保单位信息");
    }
    else
    {
      try
      {
        fm.all('GrpName').value= arrResult[0][0];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('BusinessType').value= arrResult[0][1];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('GrpNature').value= arrResult[0][2];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('Peoples').value= arrResult[0][3];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('RgtMoney').value= arrResult[0][4];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('Asset').value= arrResult[0][5];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('NetProfitRate').value= arrResult[0][6];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('MainBussiness').value= arrResult[0][7];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('Corporation').value= arrResult[0][8];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('ComAera').value= arrResult[0][9];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('Fax').value= arrResult[0][10];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('Phone').value= arrResult[0][11];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('FoundDate').value= arrResult[0][12];
      }
      catch(ex)
      { }
      ;
      try
      {
        fm.all('GrpNo').value= arrResult[0][13];
      }
      catch(ex)
      { }
      ;
    }
  }
}
//来自险种页面的函数，未必用到
function InputPolicy()
{
  var newWindow = window.open("../app/NewProposal.jsp?RiskCode=111302");
}
function InputPolicyNoList()
{
  var newWindow = window.open("../app/NewProposal.jsp?NoListFlag=1&RiskCode=111302");
}

//添加一笔险种纪录
function addRecord()
{
  var tRiskCode = fm.all('RiskCode').value ;
//  var tPayIntv = fm.all('PayIntv').value ;
  var tGrpContNo = fm.all('GrpContNo').value ;
  fm.all( 'LoadFlag' ).value = LoadFlag ;

  if(tGrpContNo==null ||tGrpContNo=="")
  {
    alert("团单合同信息未保存，不容许〔添加险种〕！");
    return ;
  }

/*  if(tRiskCode==null ||tRiskCode==""|| tPayIntv==null|| tPayIntv=="")
  {
    alert("险种信息未完整录入！");
    return ;
  }
*/
  RiskGrid.delBlankLine();

  fm.all('fmAction').value="INSERT||GROUPRISK";

  var showStr="正在添加团单险种信息，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.action="../app/GroupRiskSave.jsp"

            fm.submit();
}

//删除一笔险种纪录
function deleteRecord()
{

  RiskGrid.delBlankLine();
  var showStr="正在删除险种信息，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.all('fmAction').value="DELETE||GROUPRISK";
  fm.action="../app/GroupRiskSave.jsp"
            fm.submit();

}


/*********************************************************************
 *  团单险种信息的的录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function grpFeeInput()
{
  var tGrpContNo = fm.all('GrpContNo').value ;
  if(tGrpContNo==null ||tGrpContNo=="")
  {
    alert("团单合同信息未保存，不容许〔添加险种〕！");
    return ;
  }
  parent.fraInterface.window.location = "../askapp/AskGrpFeeInput.jsp?ProposalGrpContNo="+tGrpContNo+"&LoadFlag="+LoadFlag;
}

/*********************************************************************
 *  初始化险种显示，包括 人数和保费的统计
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function fillriskgrid()
{
  if(fm.ProposalGrpContNo.value!="")
  {
    var strSql = "Select a.riskcode ,b.riskname ,(select codename from ldcode where codetype='calrule' and code in(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+fm.all('GrpContNo').value+"' and riskcode=a.riskcode and ContPlancode='11' and CalFactor='CalRule')),"
                 +"(Select Count(RiskCode) From lcpol Where grppolno=a.Grppolno),"
                 +"(select case when  Sum(prem) is null then 0 else Sum(prem)  end From lcpol Where grppolno=a.Grppolno )"
                 +"From lcgrppol a,LMRiskApp b "
                 +"Where  a.riskcode=b.riskcode and a.GrpContNo='" + fm.all('GrpContNo').value +"'";
    turnPage.queryModal(strSql, RiskGrid);
  }
  else
  {
    return false;
  }
}

function getapproveinfo()
{
  var strSQL = "select grpcontno from lcgrpcont where 1= 1 "
               + " and prtno = '" +prtNo+"'"
               ;

  turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  var arrSelected = new Array();
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  mOperate=1;
  var approveinfo=new Array();
  approveinfo[0]=new Array();
  approveinfo[0][0]=polNo;
  if (turnPage.strQueryResult)
  {
    afterQuery(arrSelected);
  }
}
/*********************************************************************
 *  选中团单问题件的录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpQuestInput()
{
  var cGrpProposalContNo = fm.ProposalGrpContNo.value;  //团体保单号码
  if(cGrpProposalContNo==""||cGrpProposalContNo==null)
  {
    alert("请先选择一个团体投保单!");
    return ;
  }
  window.open("./GrpQuestInputMain.jsp?GrpContNo="+cGrpProposalContNo+"&Flag="+LoadFlag);

}
/*********************************************************************
 *  选中团单问题件的查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpQuestQuery()
{
  var cGrpContNo = fm.GrpContNo.value;  //团单投保单号码
  if(cGrpContNo==""||cGrpContNo==null)
  {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  window.open("./GrpQuestQueryMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+LoadFlag);
}
/*********************************************************************
 *  复核通过该团单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function gmanuchk()
{

  cProposalGrpContNo = fm.ProposalGrpContNo.value;  //团单投保单号码
  cflag="5";

  if(MissionID == "null" || SubMissionID == "null")
  {
    fm.MissionID.value = mSwitch.getVar('MissionID');
    fm.SubMissionID.value = mSwitch.getVar('SubMissionID');
  }
  else
  {
    mSwitch.deleteVar("MissionID");
    mSwitch.deleteVar("SubMissionID");
    mSwitch.addVar("MissionID", "", MissionID);
    mSwitch.addVar("SubMissionID", "", SubMissionID);
    mSwitch.updateVar("MissionID", "", MissionID);
    mSwitch.updateVar("SubMissionID", "", SubMissionID);
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  }
  if( cProposalGrpContNo == null || cProposalGrpContNo == "" )
    alert("请选择集体主险投保单后，再进行复核操作");
  else
  {
    if (confirm("该操作将复核通过该保单号下的所有投保信息,确定吗？"))
    {
      var i = 0;
      var showStr="正在复核集体投保单，请您稍候并且不要修改屏幕上的值或链接其他页面,团体投保单号是:"+cProposalGrpContNo;
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.action="./GroupPolApproveSave.jsp?ProposalGrpContNo1="+cProposalGrpContNo+"&Flag1=5";
      fm.submit();
    }
    else
      return false;
  }

}
/*********************************************************************
 *  点击返回按钮,关闭当前页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function goback()
{
  if (LoadFlag=='14')
  {
    top.opener.querygrp();
  }
  else if (LoadFlag=='23')
  {
    top.opener.querygrp();
  }
  else
  {
    top.opener.easyQueryClick();
  }
  top.close();
}

/*********************************************************************
 *  复核修改该团单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function approveupdate()
{

  cProposalGrpContNo = fm.ProposalGrpContNo.value;  //团单投保单号码
  cflag="5";
  if( cProposalGrpContNo == null || cProposalGrpContNo == "" )
    alert("请选择集体主险投保单后，再进行复核修改确认操作");
  else
  {
    if (confirm("该操作表示所有的修改已完成,确定吗？"))
    {
      var i = 0;
      var showStr="正在复核修改集体投保单，请您稍候并且不要修改屏幕上的值或链接其他页面,团体投保单号是:"+cProposalGrpContNo;
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      //window.open("./GroupPolApproveSave.jsp?ProposalGrpContNo="+cProposalGrpContNo+"&Flag1=5","windows1");
      fm.action="./GrpApproveModifyMakeSure.jsp?ProposalGrpContNo="+cProposalGrpContNo+"&Flag1=5";
      fm.submit();
    }
    else
      return false;
    //window.close();
    //fm.submit(); //提交
  }

}


/*********************************************************************
 *  团单分单定制
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function grpSubContInfo()
{
  if (fm.ProposalGrpContNo.value=="")
  {
    alert("必须保存合同信息才能进行〔分单定制〕！");
    return;
  }
  delGrpVar();
  addGrpVar();
  cGrpContNo = fm.all("GrpContNo").value;
  cPrtNo = fm.all("PrtNo").value;
  var newWindow = window.open("../app/SubContPolMain.jsp?GrpContNo=" + cGrpContNo + "&PrtNo=" + cPrtNo+"&LoadFlag="+LoadFlag);
}



function getdetailaddress()
{
  var strSQL="select b.AddressNo,b.GrpAddress,b.GrpZipCode,b.LinkMan1,b.Department1,b.HeadShip1,b.Phone1,b.E_Mail1,b.Fax1,b.LinkMan2,b.Department2,b.HeadShip2,b.Phone2,b.E_Mail2,b.Fax2 from LCGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
  arrResult=easyExecSql(strSQL);
  try
  {
    fm.all('GrpAddressNo').value= arrResult[0][0];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpAddress').value= arrResult[0][1];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('GrpZipCode').value= arrResult[0][2];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan1').value= arrResult[0][3];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department1').value= arrResult[0][4];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip1').value= arrResult[0][5];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone1').value= arrResult[0][6];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail1').value= arrResult[0][7];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax1').value= arrResult[0][8];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('LinkMan2').value= arrResult[0][9];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Department2').value= arrResult[0][10];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('HeadShip2').value= arrResult[0][11];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Phone2').value= arrResult[0][12];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('E_Mail2').value= arrResult[0][13];
  }
  catch(ex)
  { }
  ;
  try
  {
    fm.all('Fax2').value= arrResult[0][14];
  }
  catch(ex)
  { }
  ;
}

/*********************************************************************
 *  团体合同信息录入完毕确认
 *  参数  ：  wFlag--各状态时调用此函数所走的分支
 *  返回值：  无
 *********************************************************************
 */
function GrpInputConfirm(wFlag)
{
  if (wFlag ==1 ) //录入完毕确认
  {
    var tStr= "	select * from lwmission where 1=1 "
              +" and lwmission.processid = '0000000006'"
              +" and lwmission.activityid = '0000006004'"
              +" and lwmission.missionprop1 = '"+fm.ProposalGrpContNo.value+"'";
    turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
    if (turnPage.strQueryResult)
    {
      alert("该团单合同已经做过保存！");
      return;
    }
    if(fm.all('ProposalGrpContNo').value == "")
    {
      alert("团单合同信息未保存,不容许您进行 [录入完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000006002";
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;			//录入完毕
  } else if(wFlag==100)
  {
  	//alert("sdfsfsdfsd");
    var tStr= "	select * from lwmission where 1=1 "
              +" and lwmission.processid = '0000000006'"
              +" and lwmission.activityid = '0000006001'"
              +" and lwmission.missionprop1 = '"+fm.ProposalGrpContNo.value+"'";
    turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
    if (turnPage.strQueryResult)
    {
      alert("该团单合同已经做过保存！");
      return;
    }
    if(fm.all('ProposalGrpContNo').value == "")
    {
      alert("团单合同信息未保存,不容许您进行 [录入完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000006001";			//录入完毕
  }
  else if (wFlag ==2)//复核完毕确认
  {
    if(fm.all('ProposalGrpContNo').value == "")
    {
      alert("未查询出团单合同信息,不容许您进行 [复核完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000002002";					//复核完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else if (wFlag ==3)
  {
    if(fm.all('ProposalGrpContNo').value == "")
    {
      alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001002";					//复核修改完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  }
  else if(wFlag == 4)
  {
    if(fm.all('ProposalGrpContNo').value == "")
    {
      alert("未查询出合同信息,不容许您进行 [修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001021";					//问题修改
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  }
  else
    return;

  if(!ChkAge())
    return false;

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./AskInputConfirm.jsp";
  fm.submit(); //提交
}

/*********************************************************************
 *  初始化工作流MissionID
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initMissionID()
{

  if(MissionID ==null || SubMissionID ==null)
  {
    MissionID = mSwitch.getVar('MissionID');
    SubMissionID = mSwitch.getVar('SubMissionID');
  }
  else
  {
    mSwitch.deleteVar("MissionID");
    mSwitch.deleteVar("SubMissionID");
    mSwitch.addVar("MissionID", "", MissionID);
    mSwitch.addVar("SubMissionID", "", SubMissionID);
    mSwitch.updateVar("MissionID", "", MissionID);
    mSwitch.updateVar("SubMissionID", "", SubMissionID);
  }
}
function getaddresscodedata()
{
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select AddressNo,GrpAddress from LCGrpAddress where CustomerNo ='"+fm.GrpNo.value+"'";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "")
  {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++)
    {
      j = i + 1;
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
  }
  //alert ("tcodedata : " + tCodeData);
  //return tCodeData;
  fm.all("GrpAddressNo").CodeData=tCodeData;
}

//返回数据
function GrpInputReConfirm()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  if(fm.all('ProposalGrpContNo').value == "")
  {
    alert("团单合同信息未保存,不容许您进行 [录入完毕] 确认！");
    return;
  }
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  tAction = fm.action;
  fm.action="./AskChangeNoSave.jsp"
            fm.submit(); //提交
}
function grpPersonAge()
{
  //showInfo = window.open("./AskGrpPersonAgeInput.jsp?GrpContNo="+fm.GrpContNo.value+");
  var tProposalGrpContNo = "";
  tProposalGrpContNo = fm.all( 'ProposalGrpContNo' ).value;
  if( tProposalGrpContNo == null || tProposalGrpContNo == "" )
  {
    alert( "必须保存合同信息才能进入〔险种信息〕！" );
    return false
         }
         delGrpVar();
  addGrpVar();

  var cGrpContNo = fm.all("GrpContNo").value;
  var	cPrtNo = fm.all("PrtNo").value;
  showInfo = window.open("AskGrpPersonAgeMain.jsp?GrpContNo=" + cGrpContNo + "&PrtNo=" + cPrtNo+"&LoadFlag="+LoadFlag);
}
//更改日期格式
//将YYYYMMDD --> YYYY-MM-DD
function ChangDateFormate(obj,cDate)
{
  if(cDate.length==8)
  {
    var year = cDate.substring(0,4);
    var month = cDate.substring(4,6);
    var day = cDate.substring(6,8);

    var rDate = year+"-"+month+"-"+day;
    if(isDate(rDate))
    {
      return obj.value=rDate;
    }
  }
}
function beforeSubmit()
{
  //员工总人数=在职人数+退休人数+其它人员人数
  if ( fm.all('Peoples').value==null || fm.all('Peoples').value=="")
  {
    fm.all('Peoples').value = 0
	}
	if ( fm.all('AppntOnWorkPeoples').value==null || fm.all('AppntOnWorkPeoples').value=="")
	{
		fm.all('AppntOnWorkPeoples').value = 0
	}
	if ( fm.all('AppntOffWorkPeoples').value==null || fm.all('AppntOffWorkPeoples').value=="")
	{
		fm.all('AppntOffWorkPeoples').value = 0
	}
	if ( fm.all('AppntOtherPeoples').value==null || fm.all('AppntOtherPeoples').value=="")
	{
		fm.all('AppntOtherPeoples').value = 0
	}

	var intPeoples=parseInt(fm.all('Peoples').value);
  var intAppntOnWorkPeoples=parseInt(fm.all('AppntOnWorkPeoples').value);
  var intAppntOffWorkPeoples=parseInt(fm.all('AppntOffWorkPeoples').value);
  var intAppntOtherPeoples=parseInt(fm.all('AppntOtherPeoples').value );
  if (intPeoples!=(intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples))
  {
    alert("员工总人数"+(intAppntOnWorkPeoples+intAppntOffWorkPeoples+intAppntOtherPeoples));
    fm.all('Peoples').focus();
    return false;
  }

  //被保险人数(成员)=在职人数+退休人数+其它人员人数
  var intPeoples3=parseInt(fm.all('Peoples3').value);
  var intOnWorkPeoples=parseInt(fm.all('OnWorkPeoples').value);
  var intOffWorkPeoples=parseInt(fm.all('OffWorkPeoples').value);
  var intOtherPeoples=parseInt(fm.all('OtherPeoples').value );

  if (intPeoples3!=intOnWorkPeoples+intOffWorkPeoples+intOtherPeoples)
  {
    alert("被保险人数(成员)"+(intOnWorkPeoples+intOffWorkPeoples+intOtherPeoples));
    fm.all('Peoples3').focus();
    return false;
  }

  //连带被保险人=配偶+子女+其它人员人数
  var intRelaPeoples=parseInt(fm.all('RelaPeoples').value);
  var intRelaMatePeoples=parseInt(fm.all('RelaMatePeoples').value);
  var intRelaYoungPeoples=parseInt(fm.all('RelaYoungPeoples').value);
  var intRelaOtherPeoples=parseInt(fm.all('RelaOtherPeoples').value );

  if (intRelaPeoples!=intRelaMatePeoples+intRelaYoungPeoples+intRelaOtherPeoples)
  {
    alert("连带被保险人"+(intRelaMatePeoples+intRelaYoungPeoples+intRelaOtherPeoples));
    fm.all('RelaPeoples').focus();
    return false;
  }
  //双方共同承担
  if(fm.ImpartCheck2[2].checked){
  	if(parseInt(fm.ImpartCheck2[3].value)+parseInt(fm.ImpartCheck2[4].value)!=100){
  		alert("双方共同承担比例录入错误！");
  		fm.ImpartCheck2[3].focus();
  		return false;
  	}
  }
  //投保前参保人员医疗保障形式 年份校验
  if(fm.ImpartCheck1[0].checked){
  	if(fm.ImpartCheck1[1].value==""){
  		alert("参加社保年份未填！");
  		fm.ImpartCheck1[1].focus();
  		return false;
  	}
  }
  //校验对职业分类人数；
  var OccupationType = 0;
  for(var i=0 ; i<fm.ImpartCheck3.length ; i++){
  	OccupationType += fm.ImpartCheck3[i].value=="" ? 0 : parseInt(fm.ImpartCheck3[i].value);
  }
  if(OccupationType!=intOnWorkPeoples){
  	alert("职业分类人数与在职人数不符！");
  	fm.ImpartCheck3[0].focus();
  	return false;
  }
  /**
    if(dateDiff(fm.all('CValiDate').value, fm.all('CInValiDate').value,"M") < 1) {
    alert("保险责任终止时间应与生效时间间隔一年整");
    fm.all('CInValiDate').focus();
    return false;
  }*/
    if((fm.all('AgentType').value=="03")&&(fm.all('AgentCom').value==""))
  {
  	alert("请选择中介公司代码");
  	fm.all('AgentCom').focus();
  	return false;
  }
  if(dateDiff(fm.all('CValiDate').value,currentDate,"D") > 365)
  {
    alert("保险责任生效时间最多为系统操作时间前一年");
    fm.all('CValiDate').focus();
    return false;
  }

  if(dateDiff(fm.all('CValiDate').value, currentDate,"D")>1)
  {
    if(!confirm("系统操作时间大于保险责任生效时间！是否继续录入？"))
    {
      return false;
    }
  }

  if(dateDiff(currentDate, fm.all('AgentDate').value,"D") > 0)
  {

    alert("提交时间不能晚于系统当前日期");
    fm.all('AgentDate').focus();
    return false;
  }

  if(dateDiff(currentDate, fm.all('FirstTrialDate').value,"D") > 0)
  {
    alert("审批时间不能晚于系统当前日期");
    fm.all('FirstTrialDate').focus();
    return false;
  }

  if(dateDiff(currentDate, fm.all('ReceiveDate').value,"D") > 0)
  {
    alert("接收时间不能晚于系统当前日期");
    fm.all('ReceiveDate').focus();
    return false;
  }
  return true;
}


function ChkAge()
{
  //添加操作
  var tGrpContNo = fm.GrpContNo.value;
  var	strsql= "SELECT 1  FROM LCPersonAgeDisItem where GrpContNo='"+tGrpContNo +"'";
  var arr = easyExecSql(strsql);
  if(!arr)
  {
    alert("年龄分布表未保存,不容许您进行 [录入完毕] 确认！");
    return false;
  }
  return true;
}
//缴费主体的单选效果
function ImpartCheck2Radio1()
{
  if(fm.ImpartCheck2[0].checked == true)
  {
    fm.ImpartCheck2[1].checked = false;
    fm.ImpartCheck2[2].checked = false;
    fm.ImpartCheck2[3].value = "";
    fm.ImpartCheck2[4].value = "";
  }
}

function ImpartCheck2Radio2()
{
  if(fm.ImpartCheck2[1].checked == true)
  {
    fm.ImpartCheck2[0].checked = false;
    fm.ImpartCheck2[2].checked = false;
    fm.ImpartCheck2[3].value = "";
    fm.ImpartCheck2[4].value = "";
  }
}

function ImpartCheck2Radio3()
{
  if(fm.ImpartCheck2[2].checked == true)
  {
    fm.ImpartCheck2[0].checked = false;
    fm.ImpartCheck2[1].checked = false;
    fm.ImpartCheck2[3].value = "";
    fm.ImpartCheck2[4].value = "";
  }
}

//对团体告知信息进行保存
var LCImpartStr1 = "";
var LCImpartStr2 = "";
var LCImpartStr3 = "";
var LCImpartStr4 = "";
var LCImpartStr5 = "";
var ImpartCheck1 = new Array();
var ImpartCheck2 = new Array();
var ImpartCheck3 = new Array();
var ImpartCheck4 = new Array();
function LCImpartInput()
{
  //告知
  ImpartGrid.clearData();
  for(var i=0;i < fm.ImpartCheck1.length;i++)
  {
    //判断输入框
    if(fm.ImpartCheck1[i].type=="text")
    {
      if(fm.ImpartCheck1[i].value=="")
      {
        ImpartCheck1[i] = "N,";
      }
      else
      {
        ImpartCheck1[i] = fm.ImpartCheck1[i].value + ",";
      }
    }
    //判断复选框
    if(fm.ImpartCheck1[i].type=="checkbox")
    {
      if(fm.ImpartCheck1[i].checked==true)
      {
        ImpartCheck1[i] = "Y,";
      }
      else
      {
        ImpartCheck1[i] = "N,";
      }
    }
    //各字符串相加
    LCImpartStr1 += ImpartCheck1[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr1.indexOf("Y,") != -1)
  {
    ImpartGrid.setRowColData(0,1,"011");
    ImpartGrid.setRowColData(0,2,"010");
    ImpartGrid.setRowColData(0,3,"投保前参保人员医疗保障形式   □社会基本医疗保险  参加年份______　　□商业医疗保险　□单位报销  □其它______");
    ImpartGrid.setRowColData(0,4,LCImpartStr1);
  }

  //缴费主体
  for(var i=0;i < fm.ImpartCheck2.length;i++)
  {
    //判断输入框
    if(fm.ImpartCheck2[i].type=="text")
    {
      if(fm.ImpartCheck2[i].value=="")
      {
        ImpartCheck2[i] = "N,";
      }
      else
      {
        ImpartCheck2[i] = fm.ImpartCheck2[i].value + ",";
      }
    }
    //判断复选框
    if(fm.ImpartCheck2[i].type=="checkbox")
    {
      if(fm.ImpartCheck2[i].checked==true)
      {
        ImpartCheck2[i] = "Y,";
      }
      else
      {
        ImpartCheck2[i] = "N,";
      }
    }
    //各字符串相加
    LCImpartStr2 += ImpartCheck2[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr2.indexOf("Y,") != -1)
  {
    ImpartGrid.setRowColData(1,1,"021");
    ImpartGrid.setRowColData(1,2,"010");
    ImpartGrid.setRowColData(1,3,"□ 投保人全额承担  □被保人全额承担  □双方共同承担,其中投保人承担______％，被保人承担______％");
    ImpartGrid.setRowColData(1,4,LCImpartStr2);
  }
  //人员类别
  for(var i=0;i < fm.ImpartCheck3.length;i++)
  {
    //判断输入框
    if(fm.ImpartCheck3[i].type=="text")
    {
      if(fm.ImpartCheck3[i].value=="")
      {
        ImpartCheck3[i] = "N,";
      }
      else
      {
        ImpartCheck3[i] = fm.ImpartCheck3[i].value + ",";
      }
    }
    //判断复选框
    if(fm.ImpartCheck3[i].type=="checkbox")
    {
      if(fm.ImpartCheck3[i].checked==true)
      {
        ImpartCheck3[i] = "Y,";
      }
      else
      {
        ImpartCheck3[i] = "N,";
      }
    }
    //各字符串相加
    LCImpartStr3 += ImpartCheck3[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr3 != "" )
  {
    ImpartGrid.setRowColData(2,1,"012");
    ImpartGrid.setRowColData(2,2,"011");
    ImpartGrid.setRowColData(2,3,"职业分类人数：   一类人________二类人________三类人________四类人________五类人________六类人________");
    ImpartGrid.setRowColData(2,4,LCImpartStr3);
  }
  //参保形式
    for(var i=0;i < fm.ImpartCheck4.length;i++)
  {
    if(fm.ImpartCheck4[i].type=="checkbox")
    {
      if(fm.ImpartCheck4[i].checked==true)
      {
        ImpartCheck4[i] = "Y,";
      }
      else
      {
        ImpartCheck4[i] = "N,";
      }
    }
    //各字符串相加
    LCImpartStr4 += ImpartCheck4[i]	;
  }

  ImpartGrid.addOne();
  if(LCImpartStr4.indexOf("Y,") != -1)
  {
    ImpartGrid.setRowColData(3,1,"015");
    ImpartGrid.setRowColData(3,2,"010");
    ImpartGrid.setRowColData(3,3,"□ 团体统一投保  □成员自愿投保");
    ImpartGrid.setRowColData(3,4,LCImpartStr4);
  }
  LCImpartStr5=fm.AgentFee.value;
  ImpartGrid.addOne();
  if(LCImpartStr5 != "")
  {
  	ImpartGrid.setRowColData(4,1,"015");
    ImpartGrid.setRowColData(4,2,"011");
    ImpartGrid.setRowColData(4,3,"手续费");
    ImpartGrid.setRowColData(4,4,LCImpartStr5);
  	}
 //删除空白行
  ImpartGrid.delBlankLine();
}

function getLCImpart()
{
  var strSQL = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '011' and ImpartCode = '010'";
  var arrResult = easyExecSql(strSQL);
  if (arrResult != null)
  {
    var ImpartInfo1 = new Array();
    ImpartInfo1 = arrResult[0][0].split(",");

    if(ImpartInfo1[0]=="N")
    {
      fm.ImpartCheck1[0].checked = false;
    }
    else
    {
      fm.ImpartCheck1[0].checked = true;
    }

    if(ImpartInfo1[1]=="N")
    {
      fm.ImpartCheck1[1].value = "";
    }
    else
    {
      fm.ImpartCheck1[1].value = ImpartInfo1[1];
    }

    if(ImpartInfo1[2]=="N")
    {
      fm.ImpartCheck1[2].checked = false;
    }
    else
    {
      fm.ImpartCheck1[2].checked = true;
    }

    if(ImpartInfo1[3]=="N")
    {
      fm.ImpartCheck1[3].checked = false;
    }
    else
    {
      fm.ImpartCheck1[3].checked = true;
    }

    if(ImpartInfo1[4]=="N")
    {
      fm.ImpartCheck1[4].value = "";
    }
    else
    {
      fm.ImpartCheck1[4].value = ImpartInfo1[4];
    }
  }

  //缴费主体

  var strSQL2 = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '021' and ImpartCode = '010'";
  var arrResult2 = easyExecSql(strSQL2);
  if (arrResult2 != null)
  {
    var ImpartInfo2 = new Array();
    ImpartInfo2 = arrResult2[0][0].split(",");

    if(ImpartInfo2[0]=="N")
    {
      fm.ImpartCheck2[0].checked = false;
    }
    else
    {
      fm.ImpartCheck2[0].checked = true;
    }

    if(ImpartInfo2[1]=="N")
    {
      fm.ImpartCheck2[1].checked = false;
    }
    else
    {
      fm.ImpartCheck2[1].checked = true;
    }

    if(ImpartInfo2[2]=="N")
    {
      fm.ImpartCheck2[2].checked = false;
    }
    else
    {
      fm.ImpartCheck2[2].checked = true;
    }

    if(ImpartInfo2[3]=="N")
    {
      fm.ImpartCheck2[3].value = "";
    }
    else
    {
      fm.ImpartCheck2[3].value = ImpartInfo2[3];
    }

    if(ImpartInfo2[4]=="N")
    {
      fm.ImpartCheck2[4].value = "";
    }
    else
    {
      fm.ImpartCheck2[4].value = ImpartInfo2[4];
    }
  }
  //人员类别
  var strSQL3 = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '012' and ImpartCode = '011'";
  var arrResult3 = easyExecSql(strSQL3);
  if (arrResult3 != null)
  {
    var ImpartInfo3 = new Array();
    ImpartInfo3 = arrResult3[0][0].split(",");

    if(ImpartInfo3[0]=="N")
    {
      fm.ImpartCheck3[0].value = "0";
    }
    else
    {
      fm.ImpartCheck3[0].value = ImpartInfo3[0];
    }
    if(ImpartInfo3[1]=="N")
    {
      fm.ImpartCheck3[1].value = "0";
    }
    else
    {
      fm.ImpartCheck3[1].value = ImpartInfo3[1];
    }
    if(ImpartInfo3[2]=="N")
    {
      fm.ImpartCheck3[2].value = "0";
    }
    else
    {
      fm.ImpartCheck3[2].value = ImpartInfo3[2];
    }
    if(ImpartInfo3[3]=="N")
    {
      fm.ImpartCheck3[3].value = "0";
    }
    else
    {
      fm.ImpartCheck3[3].value = ImpartInfo3[3];
    }

    if(ImpartInfo3[4]=="N")
    {
      fm.ImpartCheck3[4].value = "0";
    }
    else
    {
      fm.ImpartCheck3[4].value = ImpartInfo3[4];
    }
    if(ImpartInfo3[5]=="N")
    {
      fm.ImpartCheck3[5].value = "0";
    }
    else
    {
      fm.ImpartCheck3[5].value = ImpartInfo3[5];
    }
  }
  var strSQL4 = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '015' and ImpartCode = '010'";
  var arrResult4 = easyExecSql(strSQL4);
  if (arrResult4 != null)
  {
  	var ImpartInfo4 = new Array();
    ImpartInfo4 = arrResult4[0][0].split(",");

    if(ImpartInfo4[0]=="N")
    {
      fm.ImpartCheck4[0].checked = false;
    }
    else
    {
      fm.ImpartCheck4[0].checked = true;
    }

    if(ImpartInfo4[1]=="N")
    {
      fm.ImpartCheck4[1].checked = false;
    }
    else
    {
      fm.ImpartCheck4[1].checked = true;
    }
  	}
  var strSQL5 = "select ImpartParamModle from LCCustomerImpart where GrpContNo='" + fm.all( 'GrpContNo' ).value + "' and ImpartVer = '015' and ImpartCode = '011'";
  var arrResult5 = easyExecSql(strSQL5);
  if (arrResult5 != null)
  {
  	var ImpartInfo5 = new Array();
    ImpartInfo5 = arrResult5[0][0].split(",");
      fm.AgentFee.value = ImpartInfo5[0];
  }
}
function initMuline()
{
  LCImpartStr1 = "";
  LCImpartStr2 = "";
  LCImpartStr3 = "";
  LCImpartStr4 = "";
  LCImpartStr5 = "";
  ImpartCheck1 = new Array();
  ImpartCheck2 = new Array();
  ImpartCheck3 = new Array();
  ImpartCheck4 = new Array();
}
function ChkMulLineCount()
{
	if(BookingPayIntyGrid.mulLineCount==7)
	{
		alert("约定缴费方式只能录入六次");
		BookingPayIntyGrid.delBlankLine();
		return ;
	}
}
function displayBookingPay()
{
	if(fm.GrpContPayIntv.value==-1)
	{
		divBookingPayInty.style.display='';
		var strSql = "select SpecContent from LCCGrpSpec where GrpContNo='"+fm.all('GrpContNo').value+"'";
		turnPage.queryModal(strSql,BookingPayIntyGrid);
	}
}
function checkdatediff()
{
	  if(dateDiff(fm.all('AgentDate').value, fm.all('FirstTrialDate').value,"D") < 0) {
    alert("审批日期应该晚于提交日期");
    fm.all('FirstTrialDate').focus();
    return false;
  }
    if(dateDiff(fm.all('FirstTrialDate').value, fm.all('ReceiveDate').value,"D") < 0) {
    alert("接收日期应该晚于提交日期");
    fm.all('FirstTrialDate').focus();
    return false;
  }

}
function ImpartCheck4Radio1()
{
  if(fm.ImpartCheck4[0].checked == true)
  {
    fm.ImpartCheck4[1].checked = false;
  }
}

function ImpartCheck4Radio2()
{
  if(fm.ImpartCheck4[1].checked == true)
  {
    fm.ImpartCheck4[0].checked = false;
  }
}
function CheckDateDollar()
{
	var linecount = DiseaseGrid.mulLineCount;
	for( var i=0; i<linecount;i++)
	{
		var iarray=DiseaseGrid.getRowColData(i,1);
		if(!isDate(iarray))
		{
			alert("日期格式有误!正确格式为'yyyy-mm-dd'");
			return false;
		}
		iarray=DiseaseGrid.getRowColData(i,4);  
		if(!isNumeric(iarray))
		{
			alert("医疗费用金额只能为数字");
			return false;
		}
		iarray=DiseaseGrid.getRowColData(i,3);  
		if(!isInteger(iarray))
		{
			alert("患病人数必须为整数!");
		 	return false;
		}
	}
	var historylinecount =HistoryImpartGrid.mulLineCount;
	for( var i=0;i<historylinecount;i++)
	{
		var iarray =HistoryImpartGrid.getRowColData(i,4);
		if(!isNumeric(iarray))
		{
			alert("保险费只能为数字");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,6);
		if(!isInteger(iarray))
		{
			alert("参加人数必须为整数");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,7);
		if(!isInteger(iarray))
		{
			alert("参加人数必须为整数");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,8);
		if(!isNumeric(iarray))
		{
			alert("发生金额必须为数字!");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,9);
		if(!isNumeric(iarray))
		{
			alert("报销金额只能为数字!");
			return false;
		}
		iarray =HistoryImpartGrid.getRowColData(i,10);
		if(!isNumeric(iarray))
		{
			alert("未决金额必须为数字!");
			return false;
		}		
	}
}