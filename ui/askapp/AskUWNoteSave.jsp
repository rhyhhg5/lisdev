<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：GrpFeeSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人 ：CrtHtml程序创建
//更新记录： 更新人  更新日期   更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*,com.sinosoft.lis.db.*"%>
<%
//接收信息，并作校验处理。
//输入参数
LCAskUWNoteSchema tLCAskUWNoteSchema=new LCAskUWNoteSchema();

//输出参数
CErrors tError = null;
String tRearStr = "";
String tRela = "";
String FlagStr = "Fail";
String Content = "";

//全局变量
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
System.out.println("tG.ComCode:"+tG.ComCode);
System.out.println("tG.ManageCom:"+tG.ManageCom);
System.out.println("tG.Operator:"+tG.Operator);
System.out.println("begin ...");

String tOperate=request.getParameter("mOperate");
String tOperFlag = request.getParameter("OperFlag");	//操作编码
String tNoteID = request.getParameter("NoteID");	//顺序号
String GrpContNo = request.getParameter("GrpContNo");	//集体合同号码
String PrtNo = request.getParameter("PrtNo");	//集体印刷号码
String UWPlanNote=request.getParameter("UWNote");
String ProPlanNote=request.getParameter("MPNote");
System.out.println("GrpContNo=="+GrpContNo);
System.out.println("PrtNo=="+PrtNo);
if(tOperFlag.equals("SAVEUWPLAN"))
{
tLCAskUWNoteSchema.setPrtNo(PrtNo);
tLCAskUWNoteSchema.setGrpContNo(GrpContNo);
tLCAskUWNoteSchema.setUWPlan(UWPlanNote);
VData tVData = new VData();
tVData.addElement(tG);
tVData.addElement(tLCAskUWNoteSchema);
LCAskUWNoteUI tLCAskUWNoteUI=new LCAskUWNoteUI();
try{
	            if(tLCAskUWNoteUI.submitData(tVData,"INSERT||MAIN")){
	            	
	            }else{
	                System.out.println("+++++++++++++++++++++++ERR+++++++++++++++++");
	            }
	            FlagStr = "Succ";
	            
            }catch(Exception ex){
            	FlagStr = "Fail";
            }
						if (!FlagStr.equals("Fail")){
							tError = tLCAskUWNoteUI.mErrors;
							if (!tError.needDealError()){
								Content = " 保存成功! ";
								FlagStr = "Succ";
							}
							else{
								Content = " 保存失败，原因是:" + tError.getFirstError();
								FlagStr = "Fail";
							}
						}

}

else if(tOperFlag.equals("SAVEPROPLAN"))//“保存”按钮点击事件处理
{
tLCAskUWNoteSchema.setNoteID(tNoteID);
tLCAskUWNoteSchema.setPrtNo(PrtNo);
tLCAskUWNoteSchema.setGrpContNo(GrpContNo);
tLCAskUWNoteSchema.setProPlan(ProPlanNote);
System.out.println("ProPlanNote is "+ProPlanNote);
VData tVData = new VData();
tVData.addElement(tG);
tVData.addElement(tLCAskUWNoteSchema);
LCAskUWNoteUI tLCAskUWNoteUI=new LCAskUWNoteUI();
try{
	            if(tLCAskUWNoteUI.submitData(tVData,"UPDATE||MAIN")){
	            	
	            }else{
	                System.out.println("+++++++++++++++++++++++ERR+++++++++++++++++");
	            }
	            FlagStr = "Succ";
	            
            }catch(Exception ex){
            	FlagStr = "Fail";
            }
						if (!FlagStr.equals("Fail")){
							tError = tLCAskUWNoteUI.mErrors;
							if (!tError.needDealError()){
								Content = " 保存成功! ";
								FlagStr = "Succ";
							}
							else{
								Content = " 保存失败，原因是:" + tError.getFirstError();
								FlagStr = "Fail";
							}
						}
}
else if(tOperFlag.equals("UPDATEUWPLAN"))//“保存”按钮点击事件处理
{
tLCAskUWNoteSchema.setNoteID(tNoteID);
tLCAskUWNoteSchema.setPrtNo(PrtNo);
tLCAskUWNoteSchema.setGrpContNo(GrpContNo);
tLCAskUWNoteSchema.setUWPlan(UWPlanNote);
System.out.println("UWPlanNote is "+UWPlanNote);
VData tVData = new VData();
tVData.addElement(tG);
tVData.addElement(tLCAskUWNoteSchema);
LCAskUWNoteUI tLCAskUWNoteUI=new LCAskUWNoteUI();
try{
	            if(tLCAskUWNoteUI.submitData(tVData,"UPDATE||UWPLAN")){
	            	
	            }else{
	                System.out.println("+++++++++++++++++++++++ERR+++++++++++++++++");
	            }
	            FlagStr = "Succ";
	            
            }catch(Exception ex){
            	FlagStr = "Fail";
            }
						if (!FlagStr.equals("Fail")){
							tError = tLCAskUWNoteUI.mErrors;
							if (!tError.needDealError()){
								Content = " 保存成功! ";
								FlagStr = "Succ";
							}
							else{
								Content = " 保存失败，原因是:" + tError.getFirstError();
								FlagStr = "Fail";
							}
						}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>