<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AskUWGrpManuNormChk.jsp
//程序功能：询价集体人工核保最终结论录入保存
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.ask.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
  VData tVData = new VData();
	LCGrpContSet tLCGrpContSet = new LCGrpContSet();
	LCGCUWMasterSet tLCGCUWMasterSet = new LCGCUWMasterSet();  //询价合同号

	String tGrpContNo = request.getParameter("GrpContNo");
	String tUWFlag = request.getParameter("GUWState");  //核保结论
	String tUWIdea = request.getParameter("GUWIdea");   //核保意见
	
	boolean flag = false;
	
	if (!tGrpContNo.equals("")&& !tUWFlag.equals(""))
	{     
	      TransferData mTransferData = new TransferData();
        LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();
        tLCGCUWMasterSchema.setPassFlag(tUWFlag);
        tLCGCUWMasterSchema.setUWIdea(tUWIdea);
        mTransferData.setNameAndValue("LCGCUWMasterSchema",tLCGCUWMasterSchema);
        mTransferData.setNameAndValue("GrpContNo",request.getParameter("GrpContNo"));
        mTransferData.setNameAndValue("MissionID", request.getParameter("MissionID"));
        mTransferData.setNameAndValue("SubMissionID", request.getParameter("SubMissionID"));
        mTransferData.setNameAndValue("PrtNo", request.getParameter("PrtNo"));
	        mTransferData.setNameAndValue("AgentCode", request.getParameter("AgentCode"));
	       // mTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
        mTransferData.setNameAndValue("GrpNo", request.getParameter("GrpNo"));
       // mTransferData.setNameAndValue("GrpName","1234");
        mTransferData.setNameAndValue("UWFlag", tUWFlag);
    /**总变量*/
    tVData.add(tG);
    tVData.add(mTransferData);
	  flag = true;
	}
	else
	{
	    FlagStr = "Fail";
	    Content = "号码传输失败!";
	}
	
try
{
  	if (flag == true)
  	{
		
		// 数据传输 == 
		AskWorkFlowUI tAskWorkFlowUI   = new AskWorkFlowUI();
		if (tAskWorkFlowUI.submitData(tVData,"0000006005")==false)
		{
			//int n = tAskWorkFlowUI.mErrors.getErrorCount();
			//for (int i = 0; i < n; i++)
			//Content = " 自动核保失败，原因是: " + tAskWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tAskWorkFlowUI.mErrors;
		    //tErrors = tAskWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                     
		    	Content = " 人工核保成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                              
		    {
		    	Content = " 人工核保失败，原因是:";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
			}
		    	FlagStr = "Fail";
		    }
		}
	}
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
