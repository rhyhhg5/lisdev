<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AskContPlanSave2.jsp
//程序功能：
//创建日期：2006-02-27 10:12:33
//创建人 ：CrtHtml程序创建
//更新记录： 更新人  更新日期   更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//接收信息，并作校验处理。
//输入参数
LCContPlanRiskSet tLCContPlanRiskSet = new LCContPlanRiskSet();
LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
AskLCContPlanUI tAskLCContPlanUI = new AskLCContPlanUI();

//输出参数
CErrors tError = null;
String tRearStr = "";
String tRela = "";
String FlagStr = "Fail";
String Content = "";

//全局变量
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

System.out.println("begin in AskContPlanSave.jsp ...");

String tOperate=request.getParameter("mOperate");	//操作模式
String GrpContNo = request.getParameter("GrpContNo");	//集体合同号码
String ProposalGrpContNo = request.getParameter("ProposalGrpContNo");	//集体投保单号码
String PlanSql = null;	//分类说明 
String PlanType = "0";	//计划类型


int lineCount = 0;
String arrCount[] = request.getParameterValues("ContPlanRemarkGridNo");
if( arrCount == null){
	Content = "保存失败：首先应该增加险种信息然后再保存保障计划";
	FlagStr = "Fail";
%>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<%
	return ;
}
	String tRiskCode[] = request.getParameterValues("ContPlanRemarkGrid1");//险种代码
	String tContPlanCode[] = request.getParameterValues("ContPlanRemarkGrid2");//保障计划
	String tRiskCodeName[] = request.getParameterValues("ContPlanRemarkGrid3");//险种名称
	String tRiskPrem[] = request.getParameterValues("ContPlanRemarkGrid4");//保费
  String tRemark[] = request.getParameterValues("ContPlanRemarkGrid5");//备注
  String tContPlanName[] = request.getParameterValues("ContPlanRemarkGrid6");//人员类别           
  String tPeoples2[] = request.getParameterValues("ContPlanRemarkGrid7");//参保人数
lineCount = arrCount.length;
if(lineCount == 0 ){
System.out.println("请先设置保障计划下的险种信息");

}
if(!tOperate.equals("UPDATE||MAIN")){
	for( int i = 0; i<lineCount; i++)
	{
		LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
		tLCContPlanRiskSchema.setGrpContNo(GrpContNo);
		tLCContPlanRiskSchema.setProposalGrpContNo(ProposalGrpContNo);
		tLCContPlanRiskSchema.setPlanType(PlanType);
		tLCContPlanRiskSchema.setRiskCode(tRiskCode[i]);
		tLCContPlanRiskSchema.setMainRiskCode(tRiskCode[i]);
		tLCContPlanRiskSchema.setContPlanCode(tContPlanCode[i]);
		tLCContPlanRiskSchema.setContPlanName(tContPlanName[i]);
		tLCContPlanRiskSchema.setRiskPrem(tRiskPrem[i]);
		tLCContPlanRiskSchema.setRemark(tRemark[i]);
		tLCContPlanRiskSet.add(tLCContPlanRiskSchema);
	}
} else {
	LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
	tLCContPlanRiskSchema.setGrpContNo(GrpContNo);
	tLCContPlanRiskSchema.setProposalGrpContNo(ProposalGrpContNo);
	tLCContPlanRiskSchema.setPlanType(PlanType);
	tLCContPlanRiskSchema.setRiskCode(request.getParameter("RiskCode"));
	tLCContPlanRiskSchema.setMainRiskCode(request.getParameter("RiskCode"));
	tLCContPlanRiskSchema.setContPlanCode(request.getParameter("ContPlanCode"));
	tLCContPlanRiskSchema.setContPlanName(request.getParameter("ContPlanName"));
	tLCContPlanRiskSchema.setRiskPrem(request.getParameter("SumPrem"));
	tLCContPlanRiskSchema.setRemark(request.getParameter("Remark"));
	tLCContPlanRiskSet.add(tLCContPlanRiskSchema);
}
tLCContPlanSchema.setGrpContNo(GrpContNo);
tLCContPlanSchema.setProposalGrpContNo(ProposalGrpContNo);        
tLCContPlanSchema.setPlanSql(PlanSql);
tLCContPlanSchema.setPlanType(PlanType);
tLCContPlanSchema.setContPlanCode(tContPlanCode[0]);
tLCContPlanSchema.setContPlanName(tContPlanName[0]);
tLCContPlanSchema.setPeoples2(tPeoples2[0]);
System.out.println("end ...");

// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tG);
tVData.addElement(tLCContPlanSchema);
tVData.addElement(tLCContPlanRiskSet);

try{
	System.out.println("this will save the data!!!");
	tAskLCContPlanUI.submitData(tVData,tOperate);
}
catch(Exception ex){
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}

if (!FlagStr.equals("Fail")){
	tError = tAskLCContPlanUI.mErrors;
	if (!tError.needDealError()){
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	else{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>