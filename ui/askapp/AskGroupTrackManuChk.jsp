<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AskGroupTrackManuChk.jsp
//程序功能：询价跟踪结论
//创建日期：2005-03-04 11:10:36
//创建人  ：tuq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.ask.*"%>

<%

  //输出参数
  CErrors tError = null;
  //CErrors tErrors = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  try
 { 
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
  VData tVData = new VData();
	LCGrpContSet tLCGrpContSet = new LCGrpContSet();
	LCGCUWMasterSet tLCGCUWMasterSet = new LCGCUWMasterSet();  //询价合同号

	String tGrpContNo = request.getParameter("GrpContNo");
	String tUWFlag = request.getParameter("AskTrack");  //核保结论
	String tUWIdea = request.getParameter("GUWIdea");   //核保意见
	
	boolean flag = false;
	System.out.println(tUWFlag);
	System.out.println(tUWIdea);
	if (!tGrpContNo.equals("")&& !tUWFlag.equals(""))
	{     
	      
	      TransferData mTransferData = new TransferData();
        LCGCUWMasterSchema tLCGCUWMasterSchema = new LCGCUWMasterSchema();
        tLCGCUWMasterSchema.setPassFlag(tUWFlag);
        tLCGCUWMasterSchema.setUWIdea(tUWIdea);
        mTransferData.setNameAndValue("LCGCUWMasterSchema",tLCGCUWMasterSchema);
        mTransferData.setNameAndValue("ProposalGrpContNo",request.getParameter("GrpProposalContNo"));
        mTransferData.setNameAndValue("MissionID", request.getParameter("MissionID"));
        mTransferData.setNameAndValue("SubMissionID", request.getParameter("SubMissionID"));
        mTransferData.setNameAndValue("PrtNo", request.getParameter("PrtNoHide"));
        //mTransferData.setNameAndValue("AgentCode", request.getParameter("AgentCode"));
        //mTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
        mTransferData.setNameAndValue("GrpContNo", request.getParameter("GrpProposalContNo"));
        //mTransferData.setNameAndValue("GrpName","1234");
    /**总变量*/
    tVData.add(tG);
    tVData.add(mTransferData);
	  flag = true;
	}
	else
	{
	    FlagStr = "Fail";
	    Content = "号码传输失败!";
	}
	
  	if (flag == true)
  	{
		
		// 数据传输 == 
		AskWorkFlowUI tAskWorkFlowUI   = new AskWorkFlowUI();
		if (tAskWorkFlowUI.submitData(tVData,"0000006018")==false)
		{
			//int n = tAskWorkFlowUI.mErrors.getErrorCount();
			//for (int i = 0; i < n; i++)
			//Content = " 自动核保失败，原因是: " + tAskWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tAskWorkFlowUI.mErrors;
		    //tErrors = tAskWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                     
		    	Content = " 操作成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                              
		    {
		    	Content = " 操作失败，原因是:";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
			}
		    	FlagStr = "Fail";
		    }
		}
	}
}
catch(Exception e)
{
  System.out.println(e.toString());
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.close();
	this.close();
</script>
</html>
