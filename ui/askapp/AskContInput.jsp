<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-15 11:48:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%  
	String tPNo = "";
	try
	{
		tPNo = request.getParameter("PNo");

	}
	catch( Exception e )
	{ 
		tPNo = "";
	}
 
//得到界面的调用位置,默认为1,表示个人保单直接录入.
// 1 -- 个人投保单直接录入
// 2 -- 集体下个人投保单录入
// 3 -- 个人投保单明细查询
// 4 -- 集体下个人投保单明细查询
// 5 -- 复核
// 6 -- 查询
// 7 -- 保全新保加人
// 8 -- 保全新增附加险
// 9 -- 无名单补名单
// 10-- 浮动费率
// 99-- 随动定制

	String tLoadFlag = "";
	try
	{
		tLoadFlag = request.getParameter( "LoadFlag" );
		//默认情况下为个人保单直接录入
		if( tLoadFlag == null || tLoadFlag.equals( "" ))
			tLoadFlag = "1";
	}
	catch( Exception e1 )
	{
		tLoadFlag = "1";
	}
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
        System.out.println("LoadFlag:" + tLoadFlag);
%>
<script>
	var	tMissionID = "<%=request.getParameter("MissionID")%>";
	var	tSubMissionID = "<%=request.getParameter("SubMissionID")%>"; 
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var ManageCom = "<%=request.getParameter("ManageCom")%>";
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var type = "<%=request.getParameter("type")%>";
	//保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
	var BQFlag = "<%=request.getParameter("BQFlag")%>";
	if (BQFlag == "null") BQFlag = "0";
	var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
	if (ScanFlag == "null") ScanFlag = "0";
	//保全调用会传险种过来
	var BQRiskCode = "<%=request.getParameter("riskCode")%>";
	//添加其它模块调用处理
	var LoadFlag ="<%=tLoadFlag%>"; //判断从何处进入保单录入界面,该变量需要在界面出始化前设置
</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <%@include file="ContInit.jsp"%>
  <SCRIPT src="ContInput.js"></SCRIPT>
  <SCRIPT src="ProposalAutoMove.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

<% if (request.getParameter("type") == null) { %>
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
<!--<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>-->
<% } %>

 
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./ContSave.jsp" method=post name=fm target="fraSubmit">

   <Div  id= "divButton" style= "display: ''">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
  </DIV>
    <!-- 合同信息部分 ContPage.jsp-->
     
    <%@include file="ContPage.jsp"%> 
    <%@include file="ComplexAppnt.jsp"%> 
    <DIV id=DivLCImpart STYLE="display:''">
    <!-- 告知信息部分（列表） -->
    <table>
        <tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
            </td>
            <td class= titleImg>
                投保人告知信息
            </td>
        </tr>
    </table>
    
    <Div  id= "divLCImpart1" style= "display: ''">
        <table  class= common>
            <tr  class= common>
                <td text-align: left colSpan=1>
                    <span id="spanImpartGrid" >
                    </span>
                </td>
            </tr>
        </table>
    </div>
    
    </DIV>   
    <br>
    <Div  id= "divInputContButton" style= "display: ''" style="float: right">
    	<INPUT class=cssButton id="Donextbutton5" VALUE="问题件录入" TYPE=button onClick="QuestInput();">
      <INPUT class=cssButton id="Donextbutton1" VALUE="录入完毕" TYPE=button onclick="inputConfirm(1);">        
      <INPUT class=cssButton id="Donextbutton2" VALUE="保  存"  TYPE=button onclick="submitForm();">           
      <INPUT class=cssButton id="Donextbutton3" VALUE="下一步" TYPE=button onclick="intoInsured();">    
    </DIV>
    <DIV id = "divApproveContButton" style = "display:'none'" style="float: right">
      <INPUT class=cssButton id="Donextbutton5" VALUE="问题件录入" TYPE=button onClick="QuestInput();">
    	<INPUT class=cssButton id="Donextbutton4" VALUE="复核完毕" TYPE=button onclick="inputConfirm(2);">   	
    	<INPUT class=cssButton id="Donextbutton6" VALUE="下一步" TYPE=button onclick="intoInsured();"> 
    </DIV>
    <DIV id = "divApproveModifyContButton" style = "display:'none'" style="float: right">
    		<INPUT class=cssButton id="Donextbutton5" VALUE="问题件录入" TYPE=button onClick="QuestInput();">
    	  <INPUT class=cssButton id="Donextbutton7" VALUE="复核修改完毕" TYPE=button onclick="inputConfirm(3);"> 
    	  <INPUT class=cssButton id="Donextbutton8" VALUE="保  存"  TYPE=button onclick="submitForm();">    
    	  <INPUT class=cssButton id="Donextbutton9" VALUE="下一步" TYPE=button onclick="intoInsured();">   
    </DIV>
    <Div  id= "HiddenValue" style= "display:'none'" style="float: right"> 
    	<input type=hidden id="fmAction" name="fmAction">	
			<input type=hidden id="WorkFlowFlag" name="WorkFlowFlag">
			<INPUT  type= "hidden" class= Common name= MissionID value= ""><!-- 工作流任务编码 -->
      <INPUT  type= "hidden" class= Common name= SubMissionID value= "">
    </DIV>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
