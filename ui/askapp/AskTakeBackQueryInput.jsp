<%
//程序名称：SysCertTakeBackQueryInput.jsp
//程序功能：
//创建日期：2002-10-14 10:20:49
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./AskTakeBackQueryInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./AskTakeBackQueryInputInit.jsp"%>
<title>系统单证表 </title>
</head>
<body  onload="initForm();" >
  <form action="./SysCertTakeBackQuerySubmit.jsp" method=post name=fm target="fraSubmit">

    <!-- 单证号码和经办日期 -->
    <table class="common">
    	<tr class="common">
    		<td class="title" width="25%">单证编码</td>
           <td><input class="code" name="CertifyCode" CodeData="0|^6007|补充资料通知书^6016|询价跟踪通知书" ondblclick="return showCodeListEx('SysCertCode', [this])"onkeyup="return showCodeListKeyEx('SysCertCode', [this])"></td>
        </tr>
        <tr class="common">
        <td class="title" width="25%">单证号码</td>
        <td class="input" width="25%"><input class="common" name="CertifyNo"></td>

        <td class="title" width="25%">有效日期</td>
        <td class="input" width="25%"><input class="coolDatePicker" dateFormat="short" name="ValidDate"></td></tr>

      <tr class="common">
        <td class="title">发放机构</td>
        <td class="input"><input class="common" name="SendOutCom"></td>

        <td class="title">接收机构</td>
        <td class="input"><input class="common" name="ReceiveCom"></td></tr>

      <tr class="common">
        <td class="title">经办人</td>
        <td class="input"><input class="common" name="Handler"></td>

        <td class="title">经办日期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDate"></td></tr>
      
    </table>

    </table>
          <!--INPUT VALUE="查询" class="common" TYPE=button onclick="submitForm();return false;"--> 
          <INPUT VALUE="查询" class="common" TYPE=button onclick="QueryClick();"> 
          <INPUT VALUE="返回" class="common" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCardPrintGrid);">
    		</td>
    		<td class= titleImg>
    			 单证印刷表结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divSysCertifyGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanSysCertifyGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE=" 首页 " class="common" TYPE="button" onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="common" TYPE="button" onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="common" TYPE="button" onclick="turnPage.nextPage();"> 
      <INPUT VALUE=" 尾页 " class="common" TYPE="button" onclick="turnPage.lastPage();"> 					

      <input type="hidden" name="sql_where">
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
