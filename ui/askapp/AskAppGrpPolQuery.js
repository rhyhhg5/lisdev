//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
  if (GrpPolGrid.getSelNo() == 0) {
    alert("请先选择一条保单信息！");
    return false;
  } 
  var polNo = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo() - 1, 1);
  var prtNo = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo() - 1, 2);
        mSwitch.deleteVar("PolNo");
	mSwitch.addVar("PolNo", "", polNo);
	mSwitch.updateVar("PolNo", "", polNo);
	
	mSwitch.deleteVar("GrpContNo");
	mSwitch.addVar("GrpContNo", "", polNo);
	mSwitch.updateVar("GrpContNo", "", polNo);

  easyScanWin = window.open("./AskGroupPolApproveInfo.jsp?LoadFlag=16&polNo="+polNo+"&prtNo="+prtNo, "", "status=no,resizable=yes,scrollbars=yes ");    
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = GrpPolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
	
	// 初始化表格
	initGrpPolGrid();
		
	//alert(contNo);
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select ProposalGrpContNo,PrtNo,SaleChnl,GrpName,CValiDate,AppFlag from LCGrpCont where 1=1 "
				 +" and AppFlag='8'"
				 + getWherePart( 'PrtNo' )
				 + getWherePart( 'ProposalGrpContNo','GrpProposalNo' )
				 + getWherePart( 'ManageCom' )
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'AgentGroup' )
				 + getWherePart( 'SaleChnl' )
				+ "order by PrtNo"
				;

	//execEasyQuery( strSQL );
	turnPage.queryModal(strSQL,GrpPolGrid);
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpPolGrid();
		//HZM 到此修改
		GrpPolGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpPolGrid.loadMulLine(GrpPolGrid.arraySave);		
		//HZM 到此修改
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpPolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//GrpPolGrid.delBlankLine();
	} // end of if
}
/*********************************************************************
 *  点击扫描件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function ScanQuery()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var prtNo = GrpPolGrid.getRowColData(tSel - 1,2);				

		
		if (prtNo == "")
		    return;

		 window.open("../sys/ProposalEasyScan.jsp?prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");								
	}	     
}
