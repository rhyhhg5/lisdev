//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var QueryResult="";
var QueryCount = 0;
var mulLineCount = 0;
var QueryWhere="";
var tSearch = 0;
var divprem="";
var sumarray=new Array();
var sumarraycount=0;
window.onfocus=myonfocus;

function afterCodeSelect( cCodeName, Field )
{
  //判定双击操作执行的是什么查询
  if (cCodeName=="GrpRisk")
  {
    var tRiskFlag = fm.all('RiskFlag').value;
    //由于附加险不带出主险录入框，因此判定当主附险为S的时候隐藏
    if (tRiskFlag!="S")
    {
      divmainriskname.style.display = 'none';
      divmainrisk.style.display = 'none';
      fm.all('MainRiskCode').value = fm.all('RiskCode').value;
    }
    else
    {
      divmainriskname.style.display = '';
      divmainrisk.style.display = '';
      fm.all('MainRiskCode').value = "";
    }
  }
  //判断是否选择保险套餐
  if (cCodeName=="RiskPlan1")
  {
    var tRiskPlan1 = fm.all('RiskPlan1').value;
    if (tRiskPlan1!="0")
    {
      divriskcodename.style.display = 'none';
      divriskcode.style.display = 'none';
      divcontplanname.style.display = '';
      divcontplan.style.display = '';
      ContPlanGrid.lock();
    }
    else
    {
      divriskcodename.style.display = '';
      divriskcode.style.display = '';
      divcontplanname.style.display = 'none';
      divcontplan.style.display = 'none';
      ContPlanGrid.unLock();
    }
  }
  //将保险套餐数据带入界面
  if (cCodeName=="RiskPlan")
  {
    var tRiskPlan = fm.all('RiskPlan').value;
    if (tRiskPlan!=null&&tRiskPlan!="")
    {
      showPlan();
    }
  }
}

//数据查询
function AddContClick()
{
  if (fm.all('ContPlanCode').value == "")
  {
    alert("请输入保障计划！");
    fm.all('ContPlanCode').focus();
    return false;
  }
  if(fm.all('RiskCode').value =="")
  {
    alert("请选择险种！");
    fm.all('RiskCode').focus();
    return false;
  }
  if(fm.all('MainRiskCode').value =="")
  {
    alert("请输入主险信息！");
    fm.all('MainRiskCode').focus();
    return false;
  }
  var lineCount = 0;
  var MainRiskCode = fm.all('MainRiskCode').value
  var sRiskCode ="";
  var sMainRiskCode="";
  ContPlanDutyGrid.delBlankLine("ContPlanDutyGrid");
  lineCount = ContPlanDutyGrid.mulLineCount;
  //还需要添加一个校验，不过比较麻烦，暂时先作了
  for ( i=0;i<ContPlanGrid.mulLineCount;i++ )
  {
    sRiskCode=ContPlanGrid.getRowColData(i,2);
    sMainRiskCode=ContPlanGrid.getRowColData(i,12);
    //主要是考虑附险会挂在不同的主险下，因此需要双重校验
    if (sRiskCode == fm.all('RiskCode').value && sMainRiskCode == MainRiskCode)
    {
      alert("已添加过该险种保险计划要素！");
      return false;
    }
  }

  var getWhere = "(";
  for (i=0;i<lineCount;i++)
  {
    if (ContPlanDutyGrid.getChkNo(i))
    {
      //alert(ContPlanDutyGrid.getRowColData(i,1));
      getWhere = getWhere + "'"+ContPlanDutyGrid.getRowColData(i,1)+"',"
               }
             }
             if (getWhere == "(")
             {
               alert("请选则责任信息");
               return false;
             }
             getWhere = getWhere.substring(0,getWhere.length-1) + ")"
                        //alert(getWhere);

                        // 书写SQL语句
                        var strSQL = "";

  if (QueryCount == 0)
  {
    //查询该险种下的险种计算要素
    strSQL = "select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti,case a.CalFactorType when '1' then a.CalSql else '' end,'',b.RiskVer,d.GrpPolNo,'"+MainRiskCode+"',a.CalFactorType,c.CalMode,'',a.PayPlanCode,a.GetDutyCode,a.InsuAccNo "
             + "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCGrpPol d "
             + "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
             + "and a.DutyCode in "+ getWhere + " and a.ChooseFlag in ('0','2') "
             + "and GrpContNO = '"+GrpContNo+"' and a.RiskCode = '"+fm.all('RiskCode').value+"' order by a.RiskCode,a.DutyCode,a.FactorOrder";
    //fm.all('PlanSql').value = strSQL;
    //alert(strSQL);
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (!turnPage.strQueryResult)
    {
      alert("没有该险种责任下要素信息！");
      return false;
    }
    QueryCount = 1;
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = ContPlanGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    //调用MULTILINE对象显示查询结果
    displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
    if(fm.Peoples3.value=="")
    {
      alert("请选择可保人数!");
      initContPlanGrid();
      return false;
    }
    else if(fm.SumPrem.value=="")
    {
      //alert("请选择总保费!");
      initContPlanGrid();
      return false;
    }
    else
    {
      divprem=String(fm.SumPrem.value/fm.Peoples3.value);
      divprem=String(floatRound(divprem,3));
      for(var n=0;n<ContPlanGrid.mulLineCount;n++)
      {
        if(ContPlanGrid.getRowColData(n,6)=="保费")
        {
          sumarray[sumarraycount]=n;
          sumarraycount++;
        }
      }
      divprem=String(divprem/sumarray.length);
      divprem=String(floatRound(divprem,3));
      for(var m=0;m<sumarray.length;m++)
      {
        ContPlanGrid.setRowColData(sumarray[m],8,divprem);
      }
    }
    for(var n=0;n<ContPlanGrid.mulLineCount;n++)
    {
      ContPlanGrid.setRowColData(n,15,fm.SumPrem.value);
    }
  }
  else
  {
    strSQL = "select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,"
             +"a.CalFactor,a.FactorName,a.FactorNoti,case a.CalFactorType "
             +"when '1' then a.CalSql else '' end,'',b.RiskVer,d.GrpPolNo,''"
             +",a.CalFactorType,c.CalMode,a.PayPlanCode,a.GetDutyCode,a.InsuAccNo "
             + "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LCGrpPol d "
             + "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
             + "and a.DutyCode in "+ getWhere + " and a.ChooseFlag in ('0','2') "
             + "and GrpContNO = '"+GrpContNo+"' and a.RiskCode = '"+fm.all('RiskCode').value+"' order by a.RiskCode,a.DutyCode,a.FactorOrder";
    //fm.all('ContPlanName').value = strSQL;
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (!turnPage.strQueryResult)
    {
      alert("没有该险种责任下要素信息！");
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    mulLineCount = ContPlanGrid.mulLineCount;
    //alert(mulLineCount);
    for(i=0; i<turnPage.arrDataCacheSet.length; i++)
    {
      ContPlanGrid.addOne("ContPlanGrid");
      ContPlanGrid.setRowColData(mulLineCount+i,1,turnPage.arrDataCacheSet[i][0]);
      ContPlanGrid.setRowColData(mulLineCount+i,2,turnPage.arrDataCacheSet[i][1]);
      ContPlanGrid.setRowColData(mulLineCount+i,3,turnPage.arrDataCacheSet[i][2]);
      ContPlanGrid.setRowColData(mulLineCount+i,4,turnPage.arrDataCacheSet[i][3]);
      ContPlanGrid.setRowColData(mulLineCount+i,5,turnPage.arrDataCacheSet[i][4]);
      ContPlanGrid.setRowColData(mulLineCount+i,6,turnPage.arrDataCacheSet[i][5]);
      ContPlanGrid.setRowColData(mulLineCount+i,7,turnPage.arrDataCacheSet[i][6]);
      ContPlanGrid.setRowColData(mulLineCount+i,8,turnPage.arrDataCacheSet[i][7]);
      ContPlanGrid.setRowColData(mulLineCount+i,10,turnPage.arrDataCacheSet[i][9]);
      ContPlanGrid.setRowColData(mulLineCount+i,11,turnPage.arrDataCacheSet[i][10]);
      ContPlanGrid.setRowColData(mulLineCount+i,12,MainRiskCode);
      ContPlanGrid.setRowColData(mulLineCount+i,13,turnPage.arrDataCacheSet[i][12]);
      ContPlanGrid.setRowColData(mulLineCount+i,15,fm.SumPrem.value);
      ContPlanGrid.setRowColData(mulLineCount+i,16,turnPage.arrDataCacheSet[i][14]);
      ContPlanGrid.setRowColData(mulLineCount+i,17,turnPage.arrDataCacheSet[i][15]);
      ContPlanGrid.setRowColData(mulLineCount+i,18,turnPage.arrDataCacheSet[i][16]);
      if(turnPage.arrDataCacheSet[i][5]=='保费')
      {
        divprem=String(fm.SumPrem.value/fm.Peoples3.value);
        divprem=String(floatRound(divprem,3));
        ContPlanGrid.setRowColData(mulLineCount+i,8,divprem);
      }
    }
  }
  ShowManageFee();
  //initContPlanDutyGrid();
}

//数据提交（保存）
function submitForm()
{
  //if (!beforeSubmit())
  //{
  //	return false;
  //}
  fm.all('mOperate').value = "INSERT||MAIN";
  if (fm.all('mOperate').value == "INSERT||MAIN")
  {
    if (!confirm('计划 '+fm.all('ContPlanCode').value+' 下的全部险种要素信息是否已录入完毕，您是否要确认操作？'))
    {
      return false;
    }
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  QueryCount = 0;	//重新初始化查询次数
  fm.submit(); //提交
  initRiskCode();
}

//返回上一步
function returnparent()
{
  parent.close();
}

//数据提交（删除）
function DelContClick()
{
  //此方法得到的行数需要-1处理
  var line = ContPlanCodeGrid.getSelNo();
  if (line == 0)
  {
    alert("请选择要删除的计划！");
    fm.all('ContPlanCode').value = "";
    return false;
  }
  else
  {
    fm.all('ContPlanCode').value = ContPlanCodeGrid.getRowColData(line-1,1);
  }
  fm.all('mOperate').value = "DELETE||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  QueryCount = 0;	//重新初始化查询次数
  fm.submit(); //提交
  initRiskCode();    
}

//数据提交（修改）
function UptContClick()
{

  if (tSearch == 0)
  {
    alert("请先查询要修改的保障计划！");
    return false;
  }
  fm.all('mOperate').value = "UPDATE||MAIN";
  //if (!beforeSubmit())
  //{
  //  return false;
  //}
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  QueryCount = 0;	//重新初始化查询次数
  fm.submit(); //提交
  initRiskCode();
}

//数据校验
function beforeSubmit()
{
  var strSql = "select 1 from lcgrpcont where grpcontno='"+GrpContNo+"' and EnterKind<>'1'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    if(fm.ContPlanName.value=="")
    {
      alert("保障计划区分标准除“统一无区分”选项外，均需录入对应人员类别。");
      fm.all('ContPlanName').focus();
      return false;
    }
  }
  if (fm.all('ContPlanCode').value == "")
  {
    alert("请输入保障计划！");
    fm.all('ContPlanCode').focus();
    return false;
  }
  if (fm.all('mOperate').value != "UPDATE||MAIN")
  {
    if (fm.all('MainRiskCode').value == "")
    {
      alert("请输入主险编码！");
      fm.all('MainRiskCode').focus();
      return false;
    }
  }
  if (ContPlanGrid.mulLineCount == 0)
  {
    alert("请输入保险计划详细信息");
    return false;
  }
  var lineCount = ContPlanGrid.mulLineCount;
  var sValue;
  var sCalMode;
  //参保人数小于等于应保人数
  //if ( fm.all('Peoples3').value==null || fm.all('Peoples3').value==""){
  //  fm.all('Peoples3').value = 0;
  //  }
  //if ( fm.all('Peoples2').value==null || fm.all('Peoples2').value==""){
  //  fm.all('Peoples2').value = 0;
  //                          }
  //var intPeoples3=parseInt(fm.all('Peoples3').value);
  //var intPeoples2=parseInt(fm.all('Peoples2').value);
  //if (intPeoples3>intPeoples2) {
  //  alert("参保人数应小于等于应保人数");
  //  fm.all('Peoples3').focus();
  //  return false;
  //}

  //添加要素值信息校验
  //for(var i=0;i<lineCount;i++){
  //    	sValue = ContPlanGrid.getRowColData(i,8);
  //    	sCalMode = ContPlanGrid.getRowColData(i,14);
  //    	//互算校验
  //    	if (sCalMode == "A"){
  //		if (sValue!=""){
  //			if (!isNumeric(sValue)){
  //				alert("请录入数字！");
  //				ContPlanGrid.setFocus(i,8);
  //				return false;
  //			}
  //		}
  //	}
  //	//保费算保额校验
  //    	if (sCalMode == "P"){
  //		if (sValue==""){
  //			alert("请录入保费！");
  //			ContPlanGrid.setFocus(i,8);
  //			return false;
  //		}
  //		if (!isNumeric(sValue)){
  //			alert("请录入数字！");
  //			ContPlanGrid.setFocus(i,8);
  //			return false;
  //		}
  //		if(sValue=0){
  //			alert("保费不能为0！");
  //			ContPlanGrid.setFocus(i,8);
  //			return false;
  //		}
  //	}
  //	//保额算保费校验
  //    	if (sCalMode == "G"){
  //		if (sValue==""){
  //			alert("请录入保额！");
  //			ContPlanGrid.setFocus(i,8);
  //			return false;
  //		}
  //		if (!isNumeric(sValue)){
  //			alert("请录入数字！");
  //			ContPlanGrid.setFocus(i,8);
  //			return false;
  //		}
  //		if(sValue==0){
  //			alert("保额不能为0！");
  //			ContPlanGrid.setFocus(i,8);
  //			return false;
  //		}
  //	}
  //	//其他因素算保费保额校验
  //    	if (sCalMode == "O"){
  //		if (sValue==""){
  //			alert("请录入要素值！");
  //			ContPlanGrid.setFocus(i,8);
  //			return false;
  //		}
  //		if (!isNumeric(sValue)){
  //			alert("请录入数字！");
  //			ContPlanGrid.setFocus(i,8);
  //			return false;
  //		}
  //		//if(sValue==0){
  //		//	alert("要素值不能为0！");
  //		//	ContPlanGrid.setFocus(i,8);
  //		//	return false;
  //		//}
  //	}
  //	//录入保费保额校验
  //    	if (sCalMode == "I"){
  //		if (sValue!=""){
  //			if (!isNumeric(sValue)){
  //				alert("请录入数字！");
  //				ContPlanGrid.setFocus(i,8);
  //				return false;
  //			}
  //		}
  //	}
  //}
  return true;
}

function initFactoryType(tRiskCode)
{
  // 书写SQL语句
  var k=0;
  var strSQL = "";
  strSQL = "select distinct a.FactoryType,b.FactoryTypeName,a.FactoryType||"+tRiskCode+" from LMFactoryMode a ,LMFactoryType b  where 1=1 "
           + " and a.FactoryType= b.FactoryType "
           + " and (RiskCode = '"+tRiskCode+"' or RiskCode ='000000' )";
  var str  = easyQueryVer3(strSQL, 1, 0, 1);
  return str;
}

/*********************************************************************
 *  Click事件，当点击“保险计划要约录入”按钮时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function nextstep()
{
  var newWindow = window.open("../app/ContPlanNextInput.jsp?GrpContNo="+fm.all('GrpContNo').value+"&LoadFlag="+LoadFlag,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function afterSubmit(FlagStr,content)
{
  showInfo.close();
  window.focus();
  if( FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    content = "操作成功！";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //initContPlanCodeGrid();
    //initContPlanRemarkGrid();
    //easyQueryClick();
    tSearch = 0;
    QueryCount = 0;
  }
  fm.all('mOperate').value = "";
  sumarray=new Array();
  sumarraycount=0;
}

function QueryDutyClick()
{
  if (fm.all('ContPlanCode').value == "")
  {
    alert("请输入保障计划！");
    fm.all('ContPlanCode').focus();
    return false;
  }
  if(fm.all('RiskCode').value =="")
  {
    alert("请选择险种！");
    fm.all('RiskCode').focus();
    return false;
  }

  initContPlanDutyGrid();

  //查询该险种下的险种计算要素
  strSQL = "select distinct a.DutyCode,b.DutyName,a.ChoFlag,case a.ChoFlag when 'M' then '必选' when 'B' then '备用' else '可选' end ChoFlagName "
           + "from LMRiskDuty a, LMDuty b ,LMRiskDutyFactor c "
           + "where a.DutyCode = b.DutyCode and a.RiskCode = c.RiskCode and a.DutyCode = c.DutyCode "
           + "and a.RiskCode = '"+fm.all('RiskCode').value+"' order by a.DutyCode";
  //fm.all('ContPlanName').value = strSQL;
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
    alert("没有该险种下的责任信息！");
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ContPlanDutyGrid;
  //保存SQL语句
  turnPage.strQuerySql = strSQL;
  //设置查询起始位置
  turnPage.pageIndex = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //调用MULTILINE对象显示查询结果
  displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
  var cDutyCode="";
  var tSql="";
  for(var i=0;i<=ContPlanDutyGrid.mulLineCount-1;i++)
  {
    cDutyCode=ContPlanDutyGrid.getRowColData(i,1);
    tSql="select choflag from lmriskduty where riskcode='"+fm.all('RiskCode').value+"' and dutycode='"+cDutyCode+"'";
    var arrResult=easyExecSql(tSql,1,0);
    //alert("ChoFlag:"+arrResult[0]);
    if(arrResult[0]=="M")
    {
      ContPlanDutyGrid.checkBoxSel(i+1);
    }
  }
}

function easyQueryClick()
{
  strSQL = "select GrpContNo,ProposalGrpContNo,ManageCom,AppntNo,GrpName from LCGrpCont where GrpContNo = '" +GrpContNo+ "'";
  turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  fm.all('GrpContNo').value = turnPage.arrDataCacheSet[0][0];
  fm.all('ProposalGrpContNo').value = turnPage.arrDataCacheSet[0][1];
  fm.all('ManageCom').value = turnPage.arrDataCacheSet[0][2];
  fm.all('AppntNo').value = turnPage.arrDataCacheSet[0][3];
  fm.all('GrpName').value = turnPage.arrDataCacheSet[0][4];
  
  strSQL = "select ContPlanCode,ContPlanName,PlanSql,Peoples2 "
  	+ " from LCContPlan "
  	+ " where 1=1 "
  	+ " and GrpContNo = '"+fm.all('GrpContNo').value+"' and ContPlanCode <> '00' and ContPlanCode <> '11' and plantype='0' order by ContPlanCode";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //如果没数据也无异常
  if ( !turnPage.strQueryResult ) {	
  	return false;
  }
  else{
  	//QueryCount = 1;
  	//查询成功则拆分字符串，返回二维数组
  	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  	turnPage.pageDisplayGrid = ContPlanCodeGrid;
  	//保存SQL语句
  	turnPage.strQuerySql = strSQL;
  	//设置查询起始位置
  	turnPage.pageIndex = 0;
  	//在查询结果数组中取出符合页面显示大小设置的数组
  	//调用MULTILINE对象显示查询结果
  	displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
  }
}

//单选框点击触发事件
function ShowContPlan(parm1,parm2)
{
  if(fm.all(parm1).all('InpContPlanCodeGridSel').value == '1')
  {
    //当前行第1列的值设为：选中
    var cContPlanCode = fm.all(parm1).all('ContPlanCodeGrid1').value;	//计划编码
    var cContPlanName = fm.all(parm1).all('ContPlanCodeGrid2').value;	//计划名称
    var cPlanSql = fm.all(parm1).all('ContPlanCodeGrid3').value;	//分类说明      
   	var cPeoples2 = fm.all(parm1).all('ContPlanCodeGrid4').value;	//应保人数
    fm.all('ContPlanCode').value = cContPlanCode;
    fm.all('ContPlanName').value = cContPlanName;                        
    fm.all('PlanSql').value = cPlanSql;                                  
    fm.all('Peoples2').value = cPeoples2;
    var cGrpContNo = fm.all('GrpContNo').value;

    //查询该险种计划下的险种信息
    strSQL = "select  b.RiskCode ,b.ContPlanCode, c.RiskName,b.riskprem ,b.remark,a.contplanname,a.peoples2  from lccontplan a , lccontplanrisk b, lmrisk c where a.grpcontno = '"
           + cGrpContNo+"' and a.contplancode = b.contplancode "            
           + "and  b.grpcontno = '"
           + cGrpContNo + "' and c.riskcode= b.riskcode and b.contPlancode = '"
           + cContPlanCode+"'";
    fm.all('PlanSql').value = strSQL;
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    ////原则上不会失败，嘿嘿
    if (!turnPage.strQueryResult)
    {
      alert("查询失败！");
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = ContPlanRemarkGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    //调用MULTILINE对象显示查询结果
    displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
    QueryCount = 1;
    tSearch = 1;
  }
  //QueryManageFee(ContPlanCodeForManageFee);                            
}                           

//选择保险套餐
function showPlan()
{
  var arrResult  = new Array();
  strSQL = "select a.ContPlanCode,a.ContPlanName,a.PlanSql,a.Peoples3,b.RiskCode,b.MainRiskCode from LDPlan a,LDPlanRisk b where a.ContPlanCode = b.ContPlanCode and a.ContPlanCode='"+fm.all("RiskPlan").value+"'";

  var cGrpContNo = fm.all('GrpContNo').value;

  arrResult =  decodeEasyQueryResult(easyQueryVer3(strSQL, 1, 0, 1));
  if(arrResult==null)
  {
    alert("查询保险套餐数据失败！");
  }
  else
  {
    fm.all('ContPlanCode').value = arrResult[0][0];
    fm.all('ContPlanName').value = arrResult[0][1];
    fm.all('PlanSql').value = arrResult[0][2];
    fm.all('Peoples3').value = arrResult[0][3];
    fm.all('RiskCode').value = arrResult[0][4];
    fm.all('MainRiskCode').value = arrResult[0][5];
  }
  //查询该险种下的险种计算要素
  strSQL = "select b.RiskName,a.RiskCode,a.DutyCode,c.DutyName,a.CalFactor,a.FactorName,a.FactorNoti,d.CalFactorValue,d.Remark,b.RiskVer,e.GrpPolNo,d.MainRiskCode,d.CalFactorType,c.CalMode "
           + "from LMRiskDutyFactor a, LMRisk b, LMDuty c, LDPlanDutyParam d,LCGrpPol e "
           + "where a.RiskCode = b.RiskCode and a.DutyCode = c.DutyCode and a.RiskCode = d.RiskCode "
           + "and a.DutyCode = d.DutyCode and a.CalFactor = d.CalFactor and b.RiskVer = d.RiskVersion "
           + "and ContPlanCode = '"+fm.all('RiskPlan').value+"' "
           + "and e.GrpContNO = '"+cGrpContNo+"' and e.RiskCode = d.RiskCode  order by a.RiskCode,d.MainRiskCode,a.DutyCode";

  //fm.all('PlanSql').value = strSQL;
  //alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //原则上不会失败，嘿嘿
  if (!turnPage.strQueryResult)
  {
    alert("查询失败！");
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ContPlanGrid;
  //保存SQL语句
  turnPage.strQuerySql = strSQL;
  //设置查询起始位置
  turnPage.pageIndex = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //调用MULTILINE对象显示查询结果
  displayMultiline(turnPage.arrDataCacheSet, turnPage.pageDisplayGrid);
  QueryCount = 1;
  tSearch = 1;
}

//计划变更则修改查询状态变量
function ChangePlan()
{
  QueryCount = 0;
  initContPlanDutyGrid();
  initContPlanGrid();
}
function floatRound(myFloat,mfNumber)
{
  var cutNumber = Math.pow(10,mfNumber-1);
  return Math.round(myFloat * cutNumber)/cutNumber;
}

//初始化
function initRiskCode()
{
  initForm();

  fm.all('Peoples2').value="";
  fm.all('RiskCodeName').value="";
  fm.all('SumPrem').value="";
}
//录入管理费及管理费比率
function ShowManageFee()
{

  if(fm.RiskCode.value=="")
  {
    alert("请填入险种代码！");
    return;
  }
  var strSql = "select 1 from LMRiskAccPay where riskcode='"+fm.RiskCode.value+"'";
  var arr = easyExecSql(strSql);
  if(!arr)
  {
    //alert("此险种未描述管理费！");
    return;
  }
  var strSql = "select a.FeeCode,a.FeeName,'',b.InsuAccNo,b.riskcode,b.PayPlanCode from LMRiskFee a,LMRiskAccPay b where a.InsuAccNo=b.InsuAccNo "
               +" and a.PayPlanCode=b.PayPlanCode and b.riskcode='"+fm.RiskCode.value+"'";
 	turnPage.queryModal(strSql,ManageFeeGrid);
	divManageFee.style.display='';
}
//管理费查询
function QueryManageFee(ContPlanCode)
{
	var strSql = "select a.FeeCode,b.feename,a.FeeValue,a.InsuAccNo,a.RiskCode,a.PayPlanCode from LCGrpFee a,lmriskfee b where a.grpcontno='"+GrpContNo+"' and a.feecode=b.feecode"
								+" and a.riskcode in (select riskcode from lccontplanrisk where grpcontno='"+GrpContNo+"' and contplancode='"+ContPlanCode+"')";
	var arr=easyExecSql(strSql);
	if(arr)
	{
		displayMultiline(arr, ManageFeeGrid, turnPage);
		//divManageFee.style.display='';
	}
else
	{
		//divManageFee.style.display='';
		ManageFeeGrid.clearData(); 
	}
}
//添加公共账户，保存类型
//个人账户为每个人的保费，公共账户需要录入
//且不区分保险计划。
function AddPublicAcc()
{
	var ShowInfo = window.open("./PublicAccMain.jsp?GrpContNo="+fm.all('GrpContNo').value,"PublicAccMain",'width='+screen.availWidth*0.7+',height='+screen.availHeight*0.7+',top='+screen.availHeight*0.2+',left='+screen.availWidth*0.2+',toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}
//添加险种,只能是当前计划下
function addRiskType(){
	ContPlanRemarkGrid.delBlankLine();
  if(!beforeAddRiskType())
  {
  	return;
  }
	var mullinenumber=ContPlanRemarkGrid.mulLineCount;
	//alert(mullinenumber);
	if(parseInt(mullinenumber)!=0)
	{
		if(ContPlanRemarkGrid.getRowColData(0,2)!=fm.all('ContPlanCode').value)
		{
			alert("未保存计划"+ContPlanRemarkGrid.getRowColData(1,2)+"前,不能添加其他计划的险种!");
		  return;
		}
		
		if (ContPlanRemarkGrid.getRowColData(0,6) != fm.all('ContPlanName').value)
		{
			alert("同一保障计划下的人员类别相同,必须为"+ContPlanRemarkGrid.getRowColData(0,6));
		  return;
		}
		
			if (ContPlanRemarkGrid.getRowColData(0,7) != fm.all('Peoples2').value)
		{
			alert("同一保障计划下的参保人数应该相同,必须为"+ContPlanRemarkGrid.getRowColData(0,7));
		  return;
		}
		
		for( var i = 0;i < mullinenumber; i++)
		{
			if( ContPlanRemarkGrid.getRowColData(i,1) == fm.all('RiskCode').value)
			{
				alert("该计划下已经存在该险种,请选则其他险种!");
				fm.RiskCode.focus();
				return;
			}
		}
	}
	ContPlanRemarkGrid.addOne();
  //alert(ContPlanRemarkGrid.mulLineCount);
	ContPlanRemarkGrid.setRowColData(mullinenumber,1,fm.all('RiskCode').value);
	ContPlanRemarkGrid.setRowColData(mullinenumber,2,fm.all('ContPlanCode').value);
	ContPlanRemarkGrid.setRowColData(mullinenumber,3,fm.all('RiskCodeName').value);
	ContPlanRemarkGrid.setRowColData(mullinenumber,4,fm.all('SumPrem').value);
	ContPlanRemarkGrid.setRowColData(mullinenumber,5,fm.all('Remark').value);
	ContPlanRemarkGrid.setRowColData(mullinenumber,6,fm.all('ContPlanName').value);
	ContPlanRemarkGrid.setRowColData(mullinenumber,7,fm.all('Peoples2').value);
	afterAddRiskType();
}
//校验添加的险种信息是否完整
function beforeAddRiskType()
{
	//判断保障计划
	if(fm.all('ContPlanCode').value=='')
	{
		alert("保障计划不能为空");
		fm.ContPlanCode.focus();
		return false;	
	}
	
	//判断人员类别
	if(fm.all('ContPlanName').value=='')
	{
		alert("人员类别不能为空");
		fm.ContPlanName.focus();
		return false;	
	}
	
	//判断参保人数
	if(fm.all('Peoples2').value=='')
	{
		alert("参保人数不能为空");
		fm.Peoples2.focus();
		return false;	
	}
	
	//判断险种代码
	if(fm.all('RiskCode').value=='')
	{
		alert("险种代码不能为空");
		fm.RiskCode.focus();
		return false;	
	}
	
	//判断总保费
	if(fm.all('SumPrem').value=='')
	{
		alert("总保费不能为空");
		fm.SumPrem.focus();
		return false;	
	}	
	return true;
}

function afterAddRiskType()
{
	//fm.all('ContPlanName').value='';
	//fm.all('Peoples2').value='';
	fm.all('RiskCode').value='';
	fm.all('RiskCodeName').value='';
	fm.all('SumPrem').value='';	
	fm.all('Remark').value='';
}
function ShowContPlanRemark(parm1,parm2){
	if( fm.all(parm1).all('InpContPlanRemarkGridSel').value == '1'){
		//当前行第1列的值设为：选中
    var cContPlanCode = fm.all(parm1).all('ContPlanRemarkGrid2').value;	//计划编码
    var cRiskCode = fm.all(parm1).all('ContPlanRemarkGrid1').value;//保险代码
    var cRiskCodeName = fm.all(parm1).all('ContPlanRemarkGrid3').value;//保险名称
    var cSumPrem =fm.all(parm1).all('ContPlanRemarkGrid4').value;//保费
    var cRemark = fm.all(parm1).all('ContPlanRemarkGrid5').value;//备注
    var cContPlanName = fm.all(parm1).all('ContPlanRemarkGrid6').value;	//人员类别    
   	var cPeoples2 = fm.all(parm1).all('ContPlanRemarkGrid7').value;	//参保人数

    fm.all('ContPlanCode').value = cContPlanCode;
    fm.all('ContPlanName').value = cContPlanName;                                                        
    fm.all('Peoples2').value = cPeoples2;
    fm.all('SumPrem').value = cSumPrem;
    fm.all('Remark').value = cRemark;
    fm.all('RiskCode').value = cRiskCode;	
    fm.all('RiskCodeName').value = cRiskCodeName;
	}


}