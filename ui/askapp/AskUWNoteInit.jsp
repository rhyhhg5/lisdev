<%
//程序名称：AskMakePriceInit.jsp
//程序功能：
//创建日期：2005-02-30
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{

  try
  {

	// 保单查询条件
  }
  catch(ex)
  {
    alert("在ContPlanInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在ContPlanInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initAskUWNoteGrid();
    easyQueryClick();
  }
  catch(re)
  {
    alert("LCAskUWNoteInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initAskUWNoteGrid() {
    var iArray = new Array();

    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="核保操作员";    	        //列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;                       //是否允许输入,1表示允许，0表示不允许 2表示代码选择

      iArray[2]=new Array();
      iArray[2][0]="核保录入日期";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=150;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="定价操作员";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]= 60;            			//列最大值
      iArray[3][3]= 0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="定价录入日期";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]= 60;            			//列最大值
      iArray[4][3]= 0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="状态";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]= 60;            			//列最大值
      iArray[5][3]= 0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="核保意见";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]= 1000;            			//列最大值
      iArray[6][3]= 3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="产品意见";         		//列名
      iArray[7][1]="0px";            		//列宽
      iArray[7][2]= 1000;            			//列最大值
      iArray[7][3]= 3;              			//是否允许输入,1表示允许，0表示不允许
      
      AskUWNoteGrid = new MulLineEnter( "fm" , "AskUWNoteGrid" );
      //这些属性必须在loadMulLine前
      AskUWNoteGrid.mulLineCount = 0;
      AskUWNoteGrid.displayTitle = 1;
      AskUWNoteGrid.hiddenPlus = 1;
      AskUWNoteGrid.hiddenSubtraction = 1;
      AskUWNoteGrid.canSel=1;
      AskUWNoteGrid.selBoxEventFuncName = "ShowAskUWNoteInfo"; 
      AskUWNoteGrid.loadMulLine(iArray);
    }
    catch(ex) {
      alert(ex);
    }
}
</script> 