var turnPage = new turnPageClass();  
var showInfo;
var FlagStr = "";
var content = "";
var queryFlag="false";
var sumCessprem=0;
var sumReprocfee=0;
var sumClaimbackfee=0;
function queryData(){
	if( verifyInput2() == false )
		{	
  	 		return false; 
		}	
	if(checkValid() == false)
		{	
			return false;
		}
	var tWhereSQL = "";
	var tReComCode = fm.all('ReComCode').value;
	var tRecontType = fm.all('RecontType').value;
	if(""!=tReComCode&&null!=tReComCode)
	{
		tWhereSQL+=" and exists(select 1 from lrcontinfo where recomcode='"+tReComCode+"' and  recontcode = c.recontcode union select 1 from lrtempcesscont where comcode='"+tReComCode+"' and tempcontcode = c.recontcode)";
	}
	if("C"==tRecontType)
	{
		tWhereSQL+=" and exists(select 1 from lrcontinfo where  recontcode = c.recontcode)";
	}
	else if("T"==tRecontType)
	{
		tWhereSQL+=" and exists(select 1 from lrtempcesscont where tempcontcode  = c.recontcode)";
	}
	var tSQL = "select (select distinct recomname from lrrecominfo where recomcode in (select recomcode from lrcontinfo where recontcode = a.recontcode union select comcode from lrtempcesscont where tempcontcode = a.recontcode)) as recomname,belongyear,belongmonth,recontcode,riskcode," 
		  +"sum(cessprem),(case a.cesspremstate when '02' then '已流转' else '已结算' end),"
		  +	"sum(reprocfee),(case a.reprocfeestate when '02' then '已流转' else '已结算' end)," 
		  +	"sum(claimbackfee),(case a.claimbackfeestate when '02' then '已流转' else '已结算' end)," 
		  +	"(case when (select 1 from lrcontinfo where a.recontcode = recontcode) is null then '临时分保' else '合同分保' end  ),datatype,(case when datatype='00' then '系统数据' when datatype ='01' then '每月调整' when datatype ='02' then  '其它调整' else '' end),sum(cessprem)-sum(reprocfee)-sum(claimbackfee),accountdate,accountoprater "
		  +	"from (select c.belongyear,c.belongmonth,c.recontcode,c.riskcode," 
		  +	"c.cessprem,c.cesspremstate," 
		  +	"c.reprocfee,c.reprocfeestate," 
		  +	"c.claimbackfee,c.claimbackfeestate," 
		  +	"c.accountmoney,c.accountdate,c.datatype,c.accountoprater from LRAccount c"
		  +" where  c.cesspremstate = 02"
		  +" and c.reprocfeestate = 02"
		  +" and c.claimbackfeestate = 02 " 
		  +  getWherePart("c.belongyear","year")
		  +  getWherePart("c.belongquarter","quarter")
		  +  getWherePart("c.belongmonth","month")
		  +  getWherePart("c.RecontCode","RecontCode");
	tSQL+= tWhereSQL
		  +") a"
		  +" group by belongyear,belongmonth,cesspremstate,reprocfeestate,claimbackfeestate,recontcode,riskcode,accountdate,datatype,accountoprater";	
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		turnPage.queryModal(tSQL, LRAccountSettleGrid); 
		return false;
	}
    else{ 
    	turnPage.queryModal(tSQL, LRAccountSettleGrid);
    }
    queryFlag="true";
}
// 账单结算
function accountSettle(){
	if(queryFlag=="false")
	{
		alert("请先进行查询！");
		return false;
	}
	if(!cheMul())
	{
		return false;
	}
	var type =fm.all("Type").value;
	if("ALL"==type)
	{
		var tWhereSQL = "";
		var tReComCode = fm.all('ReComCode').value;
		var tRecontType = fm.all('RecontType').value;
		if(""!=tReComCode&&null!=tReComCode)
		{
			tWhereSQL+=" and exists(select 1 from lrcontinfo where recomcode='"+tReComCode+"' and  recontcode = c.recontcode union select 1 from lrtempcesscont where comcode='"+tReComCode+"' and tempcontcode = c.recontcode)";
		}
		if("C"==tRecontType)
		{
			tWhereSQL+=" and exists(select 1 from lrcontinfo where  recontcode = c.recontcode)";
		}
		else if("T"==tRecontType)
		{
			tWhereSQL+=" and exists(select 1 from lrtempcesscont where tempcontcode  = c.recontcode)";
		}
		var tSumSql ="select sum(cessprem),sum(reprocfee),sum(claimbackfee) from LRAccount c"
			  +" where  c.cesspremstate = 02"
			  +" and c.reprocfeestate = 02"
			  +" and c.claimbackfeestate = 02 " 
			  +tWhereSQL
			  +  getWherePart("c.belongyear","year")
			  +  getWherePart("c.belongquarter","quarter")
			  +  getWherePart("c.belongmonth","month")
			  +  getWherePart("c.RecontCode","RecontCode");
			var strQuerySumResult = easyQueryVer3(tSumSql, 1, 0, 1);
			turnPage.strQuerySumResult=  easyQueryVer3(tSumSql, 1, 0, 1); 
			var arrSelected = decodeEasyQueryResult(turnPage.strQuerySumResult,0,1);
			sumCessprem = arrSelected[0][0];
		    sumReprocfee = arrSelected[0][1];
		    sumClaimbackfee = arrSelected[0][2];
	}
	if(!confirm("本次结算数据中，总分出保费："+sumCessprem+";总的手续费："+sumReprocfee+";总的理赔摊回:"+sumClaimbackfee))
	{
		return false;
	}
	fm.submit();
}
//账单下载
function accountDownload(){
	if(checkValid() == false){
		return false;
	} 
	var tWhereSQL = "";
	var tReComCode = fm.all('ReComCode').value;
	var tRecontType = fm.all('RecontType').value;
	if(""!=tReComCode&&null!=tReComCode)
	{
		tWhereSQL+=" and exists(select 1 from lrcontinfo where recomcode='"+tReComCode+"' and  recontcode = c.recontcode union select 1 from lrtempcesscont where comcode='"+tReComCode+"' and tempcontcode = c.recontcode)";
	}
	if("C"==tRecontType)
	{
		tWhereSQL+=" and exists(select 1 from lrcontinfo where  recontcode = c.recontcode)";
	}
	else if("T"==tRecontType)
	{
		tWhereSQL+=" and exists(select 1 from lrtempcesscont where tempcontcode  = c.recontcode)";
	}
	fm.querySql.value = "select recontcode,riskcode,(case when grpcontno='00000000000000000000' then '' else grpcontno end ),(case when datatype='00' then '系统数据' when datatype ='01' then '每月调整' when datatype ='02' then  '其它调整' else '' end), " +
			"  ImportCount,belongyear,BelongQuarter,belongmonth,FinAccountDate," 
			+"cessprem,(case cesspremstate when '02' then '已流转' else '已结算' end),"
			+"reprocfee,(case reprocfeestate when '02' then '已流转' else '已结算' end)," 
			+"claimbackfee,(case claimbackfeestate when '02' then '已流转' else '已结算' end)," 
			+"managecom,CostCenter,markettype,(case when TempCessFlag='C' then '合同分保' when TempCessFlag='T' then '临时分保' end  ),"
			+"BranchNo,AccountMoney,AccountDate,AccountOprater,Oprater,MakeDate,MakeTime,modifydate,modifytime from LRAccount c where 1=1 "
//		    + " and cesspremstate ='03' and reprocfeestate = '03' and claimbackfeestate ='03'"
		    + tWhereSQL
		    +	getWherePart("c.belongyear","year")
		    +  getWherePart("c.belongquarter","quarter")
		    +  getWherePart("c.belongmonth","month")
		    +  getWherePart("c.RecontCode","RecontCode");
	fm.action="./LRAccountSettleDownload.jsp";
	fm.submit();
}
function checkValid(){
	if (fm.year.value == "")
	{
		alert("请输入年份!");
		fm.year.value="";
		fm.all('year').focus();
		return false;
	}
	if (fm.quarter.value != ""&&fm.month.value != "")
	{
		alert("季度和月份只能录入一个");
		fm.quarter.value="";
		fm.quarterName.value="";
		fm.all('quarter').focus();
		fm.month.value="";
		fm.monthName.value="";
		return false;
	}
//	if(fm.quarter.value == "")
//	{
//		alert("请输入季度!");
//		fm.quarter.value="";
//		fm.all('quarter').focus();
//		return false;
//	}
	return true;
}

function cheMul(){
	sumCessprem=0;
	sumReprocfee=0;
	sumClaimbackfee=0;
	var iCount = 0;
	var rowNum1 = LRAccountSettleGrid.mulLineCount;
	//判断作为查询条件的值不能为空
	for(var i=0;i<rowNum1;i++){
		if(LRAccountSettleGrid.getChkNo(i)){
			var	belongyear = LRAccountSettleGrid.getRowColData(i,2);
			var belongmonth  = LRAccountSettleGrid.getRowColData(i,3);
			var recontcode = LRAccountSettleGrid.getRowColData(i,4);
			var riskcode = LRAccountSettleGrid.getRowColData(i,5);
			var cessprem=LRAccountSettleGrid.getRowColData(i,6);
			var reprocfee=LRAccountSettleGrid.getRowColData(i,8);
			var claimbackfee=LRAccountSettleGrid.getRowColData(i,10);
			var datatype = LRAccountSettleGrid.getRowColData(i,13);
			iCount++;
			if(belongyear == "" || belongmonth == "" || recontcode == "" || riskcode == "" || datatype == "")
			{
				alert("所选数据中有不符合条件");
				return false;
			}	
			sumCessprem+=parseFloat(cessprem);
			sumReprocfee+=parseFloat(reprocfee);
			sumClaimbackfee+=parseFloat(claimbackfee);
		}
	}
	if(iCount==0){
		fm.all("Type").value="ALL";
		if(!confirm("没有勾选要结算的数据，则会按照页面上录入的条件进行全部数据结算！"))
		{
			return false;
		}
		return true;
	}
	fm.all("Type").value="SEL";
	return true;
}

function afterSubmit(FlagStr,content){
	if (FlagStr == "Fail" ) {             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	    
	  } else { 
		  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;		  
		  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }
	  initLRAccountSettleGrid();
}

