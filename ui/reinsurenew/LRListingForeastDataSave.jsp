<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LRListingForeatDataSave.jsp
//程序功能：
//创建日期：2005-02-22 17:32:49
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LRListingForeastDataUI tLRListingForeastDataUI   = new LRListingForeastDataUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String querySql = "";
  String year = "";
  String month = "";
  String quarter = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
   //传输sql语句等参数
   
  TransferData tTransferData = new TransferData();
  transact = request.getParameter("fmtransact");
  year = request.getParameter("year");
  month = request.getParameter("month");

	tTransferData.setNameAndValue("year",year);
	tTransferData.setNameAndValue("month",month);
			
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tTransferData);
  	tVData.add(tG);
  	tLRListingForeastDataUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLRListingForeastDataUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	if (transact.equals("INSERT||MAIN"))
    	{
    	 
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
