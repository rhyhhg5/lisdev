<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate(); 
%>
<!--<script language="javascript">
    
   function initDate(){
     fm.InputDate.value="<%=CurrentDate%>";
        }
</script> --> 
        
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html;charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LRListingForeastData.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRListingForeastDataInit.jsp"%>
 
  <title>清单预估数据生成</title>   
</head>
<body  onload="initForm();initElementtype();" >
	<!--initDate();-->
  
 <form action="./LRListingForeastDataSave.jsp" method=post name=fm target="fraSubmit"> 
    <table>
     <tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssess1);">
      </td>
      <td class=titleImg>
          请输入查询条件
      </td>
     </tr>
    </table>
    <Div  id="divLAAssess1" style= "display:''">
    <table class=common align=center>
     <TR  class=common>
      <TD  class=title>
          年份
      </TD>
      <TD  class=input>
       <Input class=common   name=year elementtype=nacessary verify="年份|num&len=4" 
       onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>       
      <TD  class=title>
          月份
      </TD>
      <TD  class=input>
       <Input class=common name=month
        elementtype=nacessary verify="月份|num&len=2&value>=1&value<=12" onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>
     </TR>  
    </table>
    <br/>
  <INPUT class=cssButton VALUE="查   询" TYPE=button onClick="queryData()">
  <INPUT class=cssButton VALUE="自动核算数据转入账单表" TYPE=button onClick="autoAccount()">
  <INPUT class=cssButton VALUE="账单表系统明细数据下载" TYPE=button onClick="download();">
   <hr />
   <table class=common align =center>
     <tr class=common>
      <td class=titleImg>
                 保费汇总信息
      </td>
     </tr>
     <tr class=common>
       <td class=common>
                     分出保费
       </td>
       <td class=common>
        <Input class=common name=CessPrem>
       </td>
       <td class=common>
                      手续费
       </td>
       <td>
        <Input class=common name=ReProcFee>
       </td>    
     </tr>   
     <tr class=common>
       <td class=common> 
                    理赔摊回
       </td>
       <td class=common>
        <Input class=common name=ClaimBackFee>
       </td>
       <td class=common>
                   分出金额(汇总) 
       </td>
       <td class=common>
         <Input class=common name=CRCsum>
       </td>
     </tr>
   </table>
   <br/>
   <font color="red" size="3px">注：清单下载时，需要1~2分钟，请耐心等待</font>
<div style="display: none">

<table  class=common>
    <tr>
      <td class=titleImg>
                   清单预估数据明细
      </td>
    </tr>
     <tr  class=common>
      <td text-align: left colSpan=1>
      <span id="spanListForeastDataGrid" >
      </span> 
      </td>
     </tr>
    </table>
    <div  align="center">
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton >                  
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
      </div>
    </div>
</Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden name=Type value='1'>
    <input type=hidden name=MakeContNo  value=''>
    <input type=hidden id="fmAction" name="fmAction">  
    <input type=hidden class=Common name=querySql > 
    
  </form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
