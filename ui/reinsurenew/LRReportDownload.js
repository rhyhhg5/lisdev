var turnPage = new turnPageClass();  
var tSQL="";
var year="";
var quarter="";
var showInfo;
var FlagStr = "";
var content = "";
var datype = ""
var branchno = "";
var selSql1 = "";
var selSql2 = "";
var selSql3 = "";
function queryData(){
	if( verifyInput2() == false )
		{	
  	 		return false; 
		}	
	if(checkValid() == false)
		{	
			return false;
		}
	year=fm.all("year").value;
	month=fm.all("month").value;
	datype = fm.all("Standby1").value;
	branchno = fm.all("branchno").value;
	recontno = fm.all("recontno").value;
	riskcode = fm.all("riskcode").value;
	if(branchno != "" && branchno != null){
		tSQL = "select recomname,belongyear,belongmonth,recontcode,riskcode," 
			  +"sum(cessprem),(case a.cesspremstate when '02' then '已流转' when '00' then '未流转' else '已结算' end),"
			  +	"sum(reprocfee),(case a.reprocfeestate when '02' then '已流转' when '00' then '未流转' else '已结算' end)," 
			  +	"sum(claimbackfee),(case a.claimbackfeestate when '02' then '已流转' when '00' then '未流转' else '已结算' end)," 
			  +	"sum(accountmoney),accountdate,grpcontno,contno,datatype,accountoprater ,branchno "
			  +	"from (select (select recomname from lrrecominfo where recomcode in ((select recomcode from lrcontinfo where recontcode = c.recontcode))) as recomname,"
			  +"c.belongyear,c.belongmonth,c.recontcode,c.riskcode," 
			  +	"c.cessprem,c.cesspremstate," 
			  +	"c.reprocfee,c.reprocfeestate," 
			  +	"c.claimbackfee,c.claimbackfeestate," 
			  +	"c.accountmoney,c.accountdate,c.grpcontno,c.contno,c.datatype,c.accountoprater,c.branchno from LRAccount c) a"
			//  +" where c.belongyear = '"+year+"'"
			//  +" and c.belongmonth = '"+month+"') a"
			  +" where a.branchno = '"+branchno+"'"
			  +" group by recomname,belongyear,belongmonth,cesspremstate,reprocfeestate,claimbackfeestate,recontcode,riskcode,accountdate,grpcontno,contno,datatype,accountoprater,branchno";
	}else{
		if(recontno != "" && recontno != null){
			selSql2 = " and a.recontcode = '" +recontno +"'";
		}
		if(riskcode != "" && riskcode != null){
			selSql3 = " and a.riskcode = '" +riskcode +"'";
		}
		tSQL = "select recomname,belongyear,belongmonth,recontcode,riskcode," 
			  +"sum(cessprem),(case a.cesspremstate when '02' then '已流转' when '00' then '未流转' else '已结算' end),"
			  +	"sum(reprocfee),(case a.reprocfeestate when '02' then '已流转' when '00' then '未流转' else '已结算' end)," 
			  +	"sum(claimbackfee),(case a.claimbackfeestate when '02' then '已流转' when '00' then '未流转' else '已结算' end)," 
			  +	"sum(accountmoney),accountdate,grpcontno,contno,datatype,accountoprater ,branchno "
			  +	"from (select (select recomname from lrrecominfo where recomcode in ((select recomcode from lrcontinfo where recontcode = c.recontcode))) as recomname,"
			  +"c.belongyear,c.belongmonth,c.recontcode,c.riskcode," 
			  +	"c.cessprem,c.cesspremstate," 
			  +	"c.reprocfee,c.reprocfeestate," 
			  +	"c.claimbackfee,c.claimbackfeestate," 
			  +	"c.accountmoney,c.accountdate,c.grpcontno,c.contno,c.datatype,c.accountoprater,c.branchno from LRAccount c"
			  +" where c.belongyear = '"+year+"'"
			  +" and c.belongmonth = '"+month+"') a"
			  +" where a.cesspremstate = "+datype
			  +" and a.reprocfeestate = "+datype
			  +" and a.claimbackfeestate = "+datype +selSql2+selSql3
			  +" group by recomname,belongyear,belongmonth,cesspremstate,reprocfeestate,claimbackfeestate,recontcode,riskcode,accountdate,grpcontno,contno,datatype,accountoprater,branchno";	
	}
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		turnPage.queryModal(tSQL, LRReportDownloadGrid); 
		return false;
	}
    else{ 
    	turnPage.queryModal(tSQL, LRReportDownloadGrid);
    }
    tSQL = "";
    year = "";
    quarter = "";
    selSql1 = "";
    selSql2 = "";
    selSql3 = "";
    
}

function reportDownload(){
	if(checkValid() == false){
		return false;
	}
	year = fm.all("year").value;
	month = fm.all("month").value;
	datype = fm.all("Standby1").value;
	branchno = fm.all("branchno").value;
	recontno = fm.all("recontno").value;
	riskcode = fm.all("riskcode").value;
	if(branchno != "" && branchno != null){
		fm.querySql.value = "select recomname,belongyear,belongmonth,recontcode,riskcode," 
			  +"sum(cessprem),(case a.cesspremstate when '02' then '已流转' when '00' then '未流转' else '已结算' end),"
			  +	"sum(reprocfee),(case a.reprocfeestate when '02' then '已流转' when '00' then '未流转' else '已结算' end)," 
			  +	"sum(claimbackfee),(case a.claimbackfeestate when '02' then '已流转' when '00' then '未流转' else '已结算' end)," 
			  +	"sum(accountmoney),accountdate,accountoprater,branchno "
			  +	"from (select (select recomname from lrrecominfo where recomcode in ((select recomcode from lrcontinfo where recontcode = c.recontcode))) as recomname,"
			  +"c.belongyear,c.belongmonth,c.recontcode,c.riskcode," 
			  +	"c.cessprem,c.cesspremstate," 
			  +	"c.reprocfee,c.reprocfeestate," 
			  +	"c.claimbackfee,c.claimbackfeestate," 
			  +	"c.accountmoney,c.accountdate,c.grpcontno,c.contno,c.datatype,c.accountoprater,c.branchno from LRAccount c) a"
			//  +" where c.belongyear = '"+year+"'"
			 // +" and c.belongmonth = '"+month+"') a"
			  +" where a.branchno = '"+branchno+"'"
			  +" group by recomname,belongyear,belongmonth,cesspremstate,reprocfeestate,claimbackfeestate,recontcode,riskcode,accountdate,grpcontno,contno,datatype,accountoprater,branchno";		
	}else{
		if(recontno != "" && recontno != null){
			selSql2 = " and a.recontcode = '" +recontno +"'";
		}
		if(riskcode != "" && riskcode != null){
			selSql3 = " and a.riskcode = '" +riskcode +"'";
		}
		fm.querySql.value = "select recomname,belongyear,belongmonth,recontcode,riskcode," 
			  +"sum(cessprem),(case a.cesspremstate when '02' then '已流转' when '00' then '未流转' else '已结算' end),"
			  +	"sum(reprocfee),(case a.reprocfeestate when '02' then '已流转' when '00' then '未流转' else '已结算' end)," 
			  +	"sum(claimbackfee),(case a.claimbackfeestate when '02' then '已流转' when '00' then '未流转' else '已结算' end)," 
			  +	"sum(accountmoney),accountdate,accountoprater,branchno "
			  +	"from (select (select recomname from lrrecominfo where recomcode in ((select recomcode from lrcontinfo where recontcode = c.recontcode))) as recomname,"
			  +"c.belongyear,c.belongmonth,c.recontcode,c.riskcode," 
			  +	"c.cessprem,c.cesspremstate," 
			  +	"c.reprocfee,c.reprocfeestate," 
			  +	"c.claimbackfee,c.claimbackfeestate," 
			  +	"c.accountmoney,c.accountdate,c.grpcontno,c.contno,c.datatype,c.accountoprater,c.branchno from LRAccount c"
			  +" where c.belongyear = '"+year+"'"
			  +" and c.belongmonth = '"+month+"') a"
			  +" where a.cesspremstate = "+datype
			  +" and a.reprocfeestate = "+datype
			  +" and a.claimbackfeestate = "+datype+selSql2+selSql3
			  +" group by recomname,belongyear,belongmonth,cesspremstate,reprocfeestate,claimbackfeestate,recontcode,riskcode,accountdate,grpcontno,contno,datatype,accountoprater,branchno";	
	}
	fm.submit();
}

function checkValid(){
	branchno = fm.all("branchno").value;
	if(branchno == ""){
	if (fm.year.value == "")
	{
		alert("请输入年份!");
		fm.year.value="";
		fm.all('year').focus();
		return false;
	}
	if(fm.month.value == "")
	{
		alert("请输入月份!");
		fm.month.value="";
		fm.all('month').focus();
		return false;
	}
	if(fm.Standby1.value == "")
	{
		alert("请输入数据类型!");
		fm.datype.value="";
		fm.all('Standby1').focus();
		return false;
	}
	}
	return true;
}