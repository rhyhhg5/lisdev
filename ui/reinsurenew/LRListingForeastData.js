var FlagStr = "";
var content = "";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var tSQL="";
var year="";
var month="";
var showInfo;

function queryData()
{
	if( verifyInput2() == false )
		{	
	  	 	return false; 
		}	
	if( checkValid() == false)
		{	
			return false;
		}
	
	year=fm.all("year").value;
	month=fm.all("month").value;
	var strWhere =" and getdatadate between '"+year+"-1-1' and Date('"+year+"-"+month+"-01')+1 months -1 days ";
	var CRCsum = 0;
	var cessprem_lraccount =0.00;
	var reprocfee_lraccount = 0.00;
	var claimbackfee_lraccount=0.00;
	if("01"!=month)
	{
		var tSQL_lraccount  = "select nvl(sum(cessprem),0),nvl(sum(reprocfee),0),nvl(sum(claimbackfee),0)  from db2inst1.lraccount where datatype ='00' and belongYear||belongmonth<trim('"+year+"')||trim('"+month+"') and  belongYear||belongmonth >='"+year+"'||'01'  with ur";
		//取出本年账单表中系统清单的数据
		var strQueryResult = easyQueryVer3(tSQL_lraccount, 1, 0, 1);
		//turnPage.strQueryResult = easyQueryVer3(tSQL1, 1, 0, 1); 
		if(!strQueryResult)
		{  	
			alert("查询失败"); 
			return false;
		}
		else{ 
			arrSelected = decodeEasyQueryResult(strQueryResult,0,1);
			cessprem_lraccount = arrSelected[0][0];
			reprocfee_lraccount = arrSelected[0][1];
			claimbackfee_lraccount = arrSelected[0][2];
		}
	}
	//获取分出保费、手续费、理赔摊回的和在控件中显示
	var tSQL_list="select sum(cessprem)-("+cessprem_lraccount+"),sum(reprocfee)-("+reprocfee_lraccount+"),sum(claimbackfee)-("+claimbackfee_lraccount+") from (select cessprem,reprocfee,0 as claimbackfee"
          +" from lrcesslist a"
          +" where 1=1 "+strWhere
          +" union all"
          +" select 0 as cessprem,0 as reprocfee,claimbackfee"
          +" from lrclaimlist a"
          +" where 1=1 "+strWhere
          +") c";

	var strQueryResult1 = easyQueryVer3(tSQL_list, 1, 0, 1);
//    turnPage.strQueryResult = easyQueryVer3(tSQL1, 1, 0, 1); 
	if(!strQueryResult1)
	{  
		alert("查询失败"); 
		return false;
	}
    else{ 
    	arrSelected = decodeEasyQueryResult(strQueryResult1,0,1);
    	fm.all("CessPrem").value = arrSelected[0][0];
    	fm.all("ReProcFee").value = arrSelected[0][1];
    	fm.all("ClaimBackFee").value = arrSelected[0][2];
    	fm.all("CRCsum").value = (parseFloat(fm.all("CessPrem").value)+parseFloat(fm.all("ReProcFee").value)-parseFloat(fm.all("ClaimBackFee").value)).toFixed(2)+'';
    }
}

function autoAccount(){
	if( verifyInput2() == false )
	{	
  	 	return false; 
	}	
	if( checkValid() == false)
	{	
		return false;
	}
	year=fm.all("year").value;
	month=fm.all("month").value;
	var tDate = year +"-"+month+"-"+"01";
	//验证该月是否能自动核算
	var tSQL3 = "select * from lrgetdatalog where enddate >= last_day('"+tDate+"')";
	var strQueryResult1 = easyQueryVer3(tSQL3, 1, 0, 1);
	if(!strQueryResult1){
		alert("该月数据未达到完整月，不能核算");
		return false;
	}
	
	var tSQL4 = "select * from lraccount where belongyear||belongmonth >= '"+year+month+"' and datatype ='00'";
	var strQueryResult2 = easyQueryVer3(tSQL4, 1, 0, 1);
	if(!strQueryResult2){
		var tSQL5 = "select max(belongyear||belongmonth) from lraccount where datatype ='00' with ur";
		var strQueryResult5 = easyQueryVer3(tSQL5, 1, 0, 1);
		if(strQueryResult5)
		{
			var tArr = new Array();
			tArr = decodeEasyQueryResult(strQueryResult5);
			if(tArr!=null&&tArr!=''&&tArr.length>0)
			{
				var calYear = tArr[0][0].substring(0,4);
				var calMonth = tArr[0][0].substring(4,6);
				if((parseInt(calMonth,"10")+1+parseInt(calYear,"10")*12)==(parseInt(month,"10")+parseInt(year,"10")*12))
				{
					if (confirm("您确实想执行该操作吗?"))
					{	
						var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
						var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
						showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
						fm.fmtransact.value="INSERT||MAIN";
						fm.submit(); //提交
						return true;
						
					}
					else
					{
						return false;
					}
				}
				else
				{
					alert("不能隔月数据流转账单表，请先流转"+calYear+"年"+(parseInt(calMonth,"10")+1)+"月的数据");
					return false;
				}
			}
			if (confirm("您确实想执行该操作吗?"))
			{	
				var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
				showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.fmtransact.value="INSERT||MAIN";
				fm.submit(); //提交
				return true;
			}
			else
			{
				return false;
			}
		}
	   
	}else{
		alert(year+"年"+month+"月清单预估数据已经上传到账单表，请不要重复上传");
		return false;
	}
	
}

function checkValid(){
	if (fm.year.value == "")
	{
		alert("请输入年份!");
		fm.month.value="";
		fm.all('year').focus();
		return false;
	}
	if(fm.month.value == "")
	{
		alert("请输入月份!");
		fm.month.value="";
		fm.all('year').focus();
		return false;
	}
	return true;
}

function afterSubmit(FlagStr,content){
	  showInfo.close();
//	  alert(FlagStr);
	  if (FlagStr == "Fail" ) {             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	    
	  } else { 
		  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		  
		  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }
		 fm.all('CessPrem').value = "";
		 fm.all('ReProcFee').value = "";
	 	 fm.all('ClaimBackFee').value = "";
		 fm.all('CRCsum').value = "";
}
function download()
{
	if( checkValid() == false)
	{	
		return false;
	}

	year=fm.all("year").value;
	month=fm.all("month").value;
	var strWhere =" and getdatadate between '"+year+"-1-1' and Date('"+year+"-"+month+"-01')+1 months -1 days ";
	var tSQL ="select managecom,riskcode,recontcode,costcenter,"
	+" grpcontno,"+
	" (select b.RecomName from lrrecominfo b,lrcontinfo c where b.Recomcode = c.Recomcode " +
	" and c.RecontCode = aa.recontCode union all select b.RecomName from lrrecominfo b,lrtempcesscont c " +
		" where b.Recomcode = c.comcode " +
		" and c.tempcontcode = aa.recontCode),sum(cessprem),sum(reprocfee),sum(claimbackfee) "+
	" ,tempcessflag ,markettype from  " +
	" ((select managecom,riskcode,recontcode,costcenter,(case grpcontno when '00000000000000000000' then contno else grpcontno end) as grpcontno,cessprem as cessprem,reprocfee as  " +
	" reprocfee,0 as claimbackfee,tempcessflag,markettype from lrcesslist  where  1=1" +strWhere+
		"  and SUBSTR(costcenter,5,2)='93' )"  +
	" union all (select  managecom,riskcode,recontcode,costcenter,(case grpcontno when '00000000000000000000' then contno else grpcontno end) as grpcontno,0 as cessprem," +
	" 0 as reprocfee,claimbackfee" +
	" as claimbackfee,tempcessflag,markettype from lrclaimlist where 1=1 "+strWhere+
	" and SUBSTR(costcenter,5,2)='93'  ))"+
	" as aa  group by   recontcode,riskcode,costcenter,grpcontno,managecom,tempcessflag,markettype " +
		" union all"+
		" select managecom,riskcode,recontcode,costcenter, '',"+
	" (select b.RecomName from lrrecominfo b,lrcontinfo c where b.Recomcode = c.Recomcode " +
	" and c.RecontCode = aa.recontCode union all select b.RecomName from lrrecominfo b,lrtempcesscont c " +
		" where b.Recomcode = c.comcode " +
		" and c.tempcontcode = aa.recontCode),sum(cessprem),sum(reprocfee),sum(claimbackfee) "+
	" ,tempcessflag,markettype from  " +
	" ((select managecom,riskcode,recontcode,costcenter,cessprem as cessprem,reprocfee as  " +
	" reprocfee,0 as claimbackfee,tempcessflag,markettype from lrcesslist  where 1=1 "+strWhere+
		" and SUBSTR(costcenter,5,2)<>'93'  ) "+
	" union all (select  managecom,riskcode,recontcode,costcenter,0 as cessprem," +
	" 0 as reprocfee,claimbackfee as claimbackfee,tempcessflag,markettype from lrclaimlist where 1=1 "+strWhere+
	" and SUBSTR(costcenter,5,2)<>'93' )) "+
	" as aa  group by   recontcode,riskcode,costcenter,managecom,tempcessflag,markettype with ur" ;  
	tSQL="select managecom,riskcode,recontcode,costcenter,grpcontno,"+
	" (select b.RecomName from lrrecominfo b,lrcontinfo c where b.Recomcode = c.Recomcode " +
	" and c.RecontCode = aa.recontCode union all select b.RecomName from lrrecominfo b,lrtempcesscont c " +
		" where b.Recomcode = c.comcode " +
		" and c.tempcontcode = aa.recontCode),sum(cessprem),sum(reprocfee),sum(claimbackfee) "+
	" ,case when tempcessflag ='C' then '合同分保' when tempcessflag ='C' then '临时分保' else '' end ,markettype from db2inst1.lraccount aa where belongyear='"+year+"' and belongmonth ='"+month+"' "+
	"group by managecom,riskcode,recontcode,costcenter,grpcontno,tempcessflag ,markettype";
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  
		alert("没有符合条件的信息！"); 
		return false;
	}
	fm.all('querySql').value=tSQL;
	var oldAction = fm.action;
	fm.action="./LRListingForeastDataDownload.jsp";
	fm.submit();
	fm.action = oldAction;
}