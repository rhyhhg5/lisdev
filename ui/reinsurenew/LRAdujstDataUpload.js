var showInfo;
var turnPage = new turnPageClass(); 
var year;
var month;
var datatype;

function downLoad(){
	
	
 	 	fm.OperateType.value = "DownLoad";
 	 	fm.action = "./LRAdujstDataUploadSave.jsp";
 	    fm.submit();
	
}


/**
 * 导入清单。
 */
//function importList1()
//{
//	fm1.btnImport1.disabled = true;   
//    var showStr = "正在导入清单数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
//    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//    fm1.action = "./LRAdujstDataUploadListImportSave1.jsp?DataType=02";
//    fm1.submit();
//   
//}
function importList2()
{
	fm.btnImport2.disabled = true;   
    var showStr = "正在导入清单数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.action = "./LRAdujstDataUploadListImportSave.jsp";
    fm.submit();
   
}


function afterImportCertifyList(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("导入批次失败，请尝试重新进行申请。");
       
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
       
    }
    
    fm.btnImport2.disabled = false;
}

function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

function queryDetailData(){
	year = fm.all("year").value;
	month = fm.all("month").value;
	datatype = fm.all("Standby1").value;
	var tSQL = "";
	if(checkValid() == false)
	{	
		return false;
	}
	tSQL = "select c.belongyear,to_char(c.belongmonth), c.managecom,c.riskcode,c.recontcode,c.costcenter,c.grpcontno,"
		   +"(select recomname from lrrecominfo  where recomcode in (select recomcode from lrcontinfo where recontcode = c.recontcode union select comcode from lrtempcesscont where tempcontcode = c.recontcode)),"
		   +"sum(c.cessprem),sum(c.reprocfee),sum(c.claimbackfee),"
		   +"(case when c.tempcessflag = 'C' then '合同分保' when c.tempcessflag = 'T' then '临时分保' else '其它' end),"
		   +"(case when c.datatype = '01' then '每月调整' when c.datatype = '02' then '其它调整' end)  from LRAccount c"
		   +" where c.belongyear = '"+year+"' and c.datatype in ('01','02')"
		   +getWherePart('belongmonth','month')
		   +getWherePart('datatype','Standby1');
	tSQL+=" group by c.belongyear,c.belongmonth,c.managecom,c.riskcode,c.recontcode,c.costcenter,c.grpcontno,c.tempcessflag,c.datatype ";
    var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		turnPage.queryModal(tSQL, AdujstDataUploadGrid); 
		return false;
	}
    else{ 
		turnPage.queryModal(tSQL, AdujstDataUploadGrid);
		
	}	   
	tSQL = "";
}


function checkValid(){
	if (fm.year.value == "")
	{
		alert("请输入年份!");
		fm.year.value="";
		fm.all('year').focus();
		return false;
	}
//	if(fm.month.value == "")
//	{
//		alert("请输入月份!");
//		fm.quarter.value="";
//		fm.all('month').focus();
//		return false;
//	}
//	if(fm.Standby1Name.value == "")
//	{
//		alert("请输入数据类型!");
//		fm.Standby1Name.value="";
//		fm.all('Standby1Name').focus();
//		return false;
//	}
	return true;
}
function downLoadData()
{
	if(checkValid() == false){
		return false;
	} 
	var year = fm.all("year").value;
	tSQL = "select c.belongyear,to_char(c.belongmonth), c.managecom,c.riskcode,c.recontcode,c.costcenter,c.grpcontno,"
		   +"(select recomname from lrrecominfo  where recomcode in ((select recomcode from lrcontinfo where recontcode = c.recontcode))),"
		   +"sum(c.cessprem),sum(c.reprocfee),sum(c.claimbackfee),"
		   +"(case when c.tempcessflag = 'C' then '合同分保' when c.tempcessflag = 'T' then '临时分保' else '其它' end),"
		   +"(case when c.datatype = '01' then '每月调整' when c.datatype = '02' then '其它调整' end)  from LRAccount c"
		   +" where c.belongyear = '"+year+"' and c.datatype in ('01','02')"
		   +getWherePart('belongmonth','month')
		   +getWherePart('datatype','Standby1');
	tSQL+=" group by c.belongyear,c.belongmonth,c.managecom,c.riskcode,c.recontcode,c.costcenter,c.grpcontno,c.tempcessflag,c.datatype ";
	fm.action="./LRAdujstDataDownload.jsp?querySql="+tSQL;
	fm.submit();
}
