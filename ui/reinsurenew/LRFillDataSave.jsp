<%
//程序名称：LRGetDataSave.jsp
//程序功能：
//创建日期：2006-10-24
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  System.out.println("开始执行Save页面");
  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
	
  
  CErrors tError = null;
  String mOperateType=request.getParameter("OperateType"); 
  System.out.println("操作的类型是"+mOperateType);
  String tRela  		= "";
  String FlagStr 		= "";
  String Content 		= "";
  String mDescType 	= ""; //将操作标志的英文转换成汉字的形式
  LRFillDataBL tLRFillDataBL = new LRFillDataBL();
  System.out.println("开始进行获取数据的操作！！！");
	String tStartDate=request.getParameter("StartDate"); 
	String tEndDate=request.getParameter("EndDate"); 
	String tReContCode=request.getParameter("ReContCode"); 
	String tRiskCode=request.getParameter("RiskCode"); 
	String tGrpContNo=request.getParameter("GrpContNo"); 
	String tContNo=request.getParameter("ContNo"); 
	String tPolNo=request.getParameter("PolNo"); 
	String tOperator=request.getParameter("Operator"); 
	
  VData tVData = new VData(); 
  //将团单的公共信息通过TransferData传到UI
  try
  {
  	tVData.addElement(globalInput);
  	TransferData tTransferData = new TransferData();
  	tTransferData.setNameAndValue("StartDate", tStartDate);
  	tTransferData.setNameAndValue("EndDate", tEndDate);
  	tTransferData.setNameAndValue("ReContCode", tReContCode);
  	tTransferData.setNameAndValue("RiskCode", tRiskCode);
  	tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
  	tTransferData.setNameAndValue("ContNo", tContNo);
  	tTransferData.setNameAndValue("PolNo", tPolNo);
  	tTransferData.setNameAndValue("Operator", tOperator);
  	tVData.addElement(globalInput);
  	tVData.addElement(tTransferData);
  	
  	tLRFillDataBL.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  tError = tLRFillDataBL.mErrors;
	if (FlagStr=="")
	{
	  if (!tError.needDealError())
	  {
	    Content = mDescType+"完成";
	  	FlagStr = "Succ";
	  }
	  else
	  {
	  	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
	  	FlagStr = "Fail";
	  }
	}
	
	 
%>

<html>
	<script language="javascript">
			parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>