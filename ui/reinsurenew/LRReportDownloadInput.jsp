<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate(); 
%>
<!--<script language="javascript">
    
   function initDate(){
     fm.InputDate.value="<%=CurrentDate%>";
        }
</script> --> 
        
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html;charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LRReportDownload.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRReportDownloadInit.jsp"%>
 
  <title>清单预估数据生成</title>   
</head>
<body  onload="initForm();initElementtype();" >
	<!--initDate();-->
  
 <form action="./LRReportDownloadSave.jsp" method=post name=fm target="fraSubmit"> 
    <table>
     <tr>
      <td class=common>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssess1);">
      </td>
      <td class=titleImg>
          请输入查询条件
      </td>
     </tr>
    </table>
    <Div  id="divLAAssess1" style= "display:''">
    <table class=common align=center>
     <TR  class=common>
      <TD  class=title>
          年份
      </TD>
      <TD  class=input>
       <Input class=common name=year elementtype=nacessary verify="年份|num&len=4" 
       onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>       
      <TD  class=title>
 <!--          季度
      </TD>
      <TD  class=input>
       <Input class=common name=quarter elementtype=nacessary verify="季度|num&len=2&value>=1&value<=4" 
       onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>
 -->   
 	     月份
      </TD>    
      <TD  class=input>
       <Input class=common name=month elementtype=nacessary verify="月份|num&len=2&value>=1&value<=12" 
       onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>   
     </TR> 
     <TR  class=common>
      <TD  class=title>
                数据类型       
      </TD>
      <TD  class=input>
     	<Input class="codeno"  readOnly name= "Standby1" CodeData="0|^00|未流转^02|已流转^03|已结算" 
          ondblClick="showCodeListEx('Standby1',[this,Standby1Name],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('Standby1',[this,Standby1Name],[0,1],null,null,null,1);"><Input 
          class= codename name= 'Standby1Name' elementtype=nacessary value=""> 
      </TD>       
      <TD  class=title> 
 	     结算账单号
      </TD>    
      <TD  class=input>
       <Input class=common name=branchno  
       onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>   
     </TR> 
         <TR  class=common>
      <TD  class=title>
                再保合同号     
      </TD>
      <TD  class=input>
       <Input class=common name=recontno  
       onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>       
      <TD  class=title> 
 	   险种编码
      </TD>    
      <TD  class=input>
       <Input class=common name=riskcode  
       onchange=verifyElementWrap(this.verify,this.value,this.form.name+"."+this.name)>
      </TD>   
     </TR> 
    </table>
  <INPUT class=cssButton VALUE="查   询" TYPE=button onClick="queryData();">
   <INPUT class=cssButton VALUE="报表下载" TYPE=button onClick="reportDownload();">  
   
   <table  class=common>
     <tr  class=common>
      <td text-align: left colSpan=1>
      <span id="spanLRReportDownloadGrid" >
      </span> 
      </td>
     </tr>
    </table>
    <div  align="center">
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton >                  
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
      </div>
    </div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden name=Type value='1'>
    <input type=hidden name=MakeContNo  value=''>
    <input type=hidden id="fmAction" name="fmAction">  
    <input type=hidden class=Common name=querySql > 
    
  </form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
