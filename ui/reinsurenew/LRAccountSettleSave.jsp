<% 
	//程序名称：LRGetDataSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%
  GlobalInput tGI = new GlobalInput( );
   tGI = (GlobalInput)session.getValue("GI") ;
  LRAccountSet tLRAccountSet = new LRAccountSet();
  LRAccountDB tLRAccountDB = new LRAccountDB();
  LRAccountSettleUI  tLRAccountSettleUI= new LRAccountSettleUI();
  TransferData tTransferData = new TransferData();
  VData tVData = new VData();
  CErrors tError = null;
  String Content = "";
  String FlagStr = "";
  String year = "";
  String quarter = "";
  String month="";
  String recomcode="";
  String reconttype="";
  String recontcode="";
  
  String aBelongYear = "";
  String aBelongMonth = "";
  String aReContCode = "";
  String aRiskCode = "";
  String aGrpContNo = "";
  String aContNo = "";
  String aDataType = "";
  String tType = request.getParameter("Type");
  String tLRAccountSettleGrid2[] = request.getParameterValues("LRAccountSettleGrid2"); 
  String tLRAccountSettleGrid3[] = request.getParameterValues("LRAccountSettleGrid3");
  String tLRAccountSettleGrid4[] = request.getParameterValues("LRAccountSettleGrid4");
  String tLRAccountSettleGrid5[] = request.getParameterValues("LRAccountSettleGrid5");
  String tLRAccountSettleGrid13[] = request.getParameterValues("LRAccountSettleGrid13");
  String tChk[] = request.getParameterValues("InpLRAccountSettleGridChk");
  System.out.println(tChk.length);
  for(int index=0;index<tChk.length;index++)
  {
    if(tChk[index].equals("1")){           
       aBelongYear = tLRAccountSettleGrid2[index];
       aBelongMonth = tLRAccountSettleGrid3[index];
       aReContCode = tLRAccountSettleGrid4[index];
       aRiskCode = tLRAccountSettleGrid5[index];
       aDataType = tLRAccountSettleGrid13[index];
       
 
       tLRAccountDB.setBelongYear(aBelongYear);
       tLRAccountDB.setBelongMonth(aBelongMonth);
       tLRAccountDB.setReContCode(aReContCode);
       tLRAccountDB.setRiskCode(aRiskCode);
       tLRAccountDB.setDataType(aDataType);
       tLRAccountDB.setCessPremState("02");
	   tLRAccountDB.setReProcFeeState("02");
	   tLRAccountDB.setClaimBackFeeState("02");
       LRAccountSet mLRAccountSet = tLRAccountDB.query();
       tLRAccountSet.add(mLRAccountSet);
    }
  }
  try
  { 
	 year = request.getParameter("year"); 
	 quarter = request.getParameter("quarter"); 
	 month = request.getParameter("month"); 
	 recomcode = request.getParameter("ReComCode"); 
	 reconttype = request.getParameter("RecontType"); 
	 recontcode = request.getParameter("RecontCode"); 
	 tTransferData.setNameAndValue("year",year);
	 tTransferData.setNameAndValue("quarter",quarter);
	 tTransferData.setNameAndValue("month",month);
	 tTransferData.setNameAndValue("ReComCode",recomcode);
	 tTransferData.setNameAndValue("RecontType",reconttype);
	 tTransferData.setNameAndValue("RecontCode",recontcode);
	 tTransferData.setNameAndValue("Type",tType);
	 tVData.add(tTransferData);
  // 准备传输数据 VData
  	tVData.add(tGI);
  	tVData.add(tLRAccountSet);
  	tLRAccountSettleUI.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
    System.out.println(FlagStr);
  }
  if (FlagStr=="")
  {
    tError = tLRAccountSettleUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
