<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();
    initTaskInfo();  
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 

  fm.TaskTitle.value = "";
  fm.MonitorContent.value = "";
  fm.QuerySQL.value = "";
  initMonitorGrid();
  initLogGrid();
}

var MonitorGrid;
function initMonitorGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="监控条件";         	  //列名
    iArray[1][1]="300px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    
    
    MonitorGrid = new MulLineEnter("fm", "MonitorGrid"); 
    //设置Grid属性
    MonitorGrid.mulLineCount = 1;
    MonitorGrid.displayTitle = 1;
    MonitorGrid.hiddenSubtraction = 0;
    MonitorGrid.hiddenPlus = 0;
    MonitorGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
var LogGrid;
function initLogGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="日志描述";         	  //列名
    iArray[1][1]="300px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    
    
    LogGrid = new MulLineEnter("fm", "LogGrid"); 
    //设置Grid属性
    LogGrid.mulLineCount = 1;
    LogGrid.displayTitle = 1;
    LogGrid.hiddenSubtraction = 0;
    LogGrid.hiddenPlus = 0;
    LogGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
