
<%
	//程序名称：TaskServiceInput.jsp
	//程序功能：
	//创建日期：2004-12-15 
	//创建人  ：ZhangRong
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{ 
}

function initSelBox()
{  
}                                        

function initForm()
{
  try
  {
  initKnowledgeBaseGuideGrid();
  query();
  }
  catch(re)
  {
    alert("TaskServiceInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var KnowledgeBaseGuideGrid;
function initKnowledgeBaseGuideGrid()
{                               
    var iArray = new Array();      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="任务编码";          	//列名
      iArray[1][1]="80px";      	      	//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;                       //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="任务名称";         		//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="文件类型";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      //iArray[10][11]="0|^0|等待^1|启动^2|暂停^3|正常终止^4|强行终止^5|异常终止"; 
		
      KnowledgeBaseGuideGrid = new MulLineEnter( "fm" , "KnowledgeBaseGuideGrid" ); 
      //这些属性必须在loadMulLine前
      KnowledgeBaseGuideGrid.mulLineCount = 1;   
      KnowledgeBaseGuideGrid.displayTitle = 1;
      KnowledgeBaseGuideGrid.canSel =1;
      KnowledgeBaseGuideGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      KnowledgeBaseGuideGrid.hiddenSubtraction=1;

	  KnowledgeBaseGuideGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert(ex);
    }
}
</script>
