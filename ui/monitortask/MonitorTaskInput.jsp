<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：
		//创建人  ：
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	String tTaskCode = request.getParameter("TaskCode");
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="MonitorTaskInput.js"></script>
		<%@include file="MonitorTaskInputInit.jsp"%>

		<script>
		var tTaskCode = "<%=tTaskCode%>";
  		</script>

	</head>
	<body onload="initForm();" style="background-color: #F7F7F7;">
		<form action="" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=titleImg>
						监控任务
					</td>
				</tr>
			</table>

			<table class=common>
				<tr>
					<td class=title>
						监控任务标题
					</td>
					<td colspan="5">
						<Input type="text" name="TaskTitle" elementtype="nacessary" style="width: 80%;">
					</td>
				<tr>
					<td class=title>
						监控任务内容
					</td>
					<td colspan="5">
						<textarea name="MonitorContent" cols="110%" rows="3"> 
        				</textarea>
					</td>
				</tr>
			</table>

			<table class=common>
				<tr>
					<td class=title>
						查询sql
					</td>
					<td>
						<textarea name="QuerySQL" cols="110%" rows="3"></textarea>
					</td>
					<td>
						<input value="sql校验" type="button" onclick="">
					</td>
				</tr>
			</table>

			<table class=common>
				<tr>
					<td colspan="6">
						监控条件
					</td>
				</tr>
			</table>

			<Div id="divMonitorGrid1" style="display:''">
				<table>
					<tr>
						<td text-align:left colSpan=1>
							<span id="spanMonitorGrid"> </span>
						</td>
					</tr>
				</table>
			</div>

			<table class=common>
				<tr>
					<td colspan="6">
						日志描述
					</td>
				</tr>
			</table>

			<Div id="divLogGrid1" style="display:''">
				<table>
					<tr>
						<td text-align:left colSpan=1>
							<span id="spanLogGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<hr>
			<table>
				<tr>
					<td class=titleImg>
						执行监控时刻设定
					</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>
						起始时间 (YYYY-MM-DD HH:MM:SS)
					</td>
					<td class=input8>
						<Input class=coolDatePicker style="width:90px" name=Startdate>
						&nbsp&nbsp
						<Input class=common style="width:20px" name=StartH
							verify="起始时间(时)|len=2&INT">
						：
						<Input class=common style="width:20px" name=StartM
							verify="起始时间(分)|len=2&INT">
						：
						<Input class=common style="width:20px" name=StartS
							verify="起始时间(秒)|len=2&INT">
						<Input type=hidden name=StartTime>
					</td>
					<td class=title>
						类型设定
					</td>
					<td class=input>
						<Input class=codeno name=MonitorType value="1" CodeData="0|^1|一次^2|每天^3|每月" ondblclick="return showCodeListEx('MonitorType',[this,MonitorTypeName],[0,1],null,null,null,1);"><input class=codename name=MonitorTypeName value="" readonly=true>
					</td>
				</tr>
			</table>
			<hr>
			<table>
				<tr>
					<td class=titleImg>
						数据库配置
					</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>
						数据库
					</td>
					<td class=input>
						<Input class=codeno name=DBType value="" CodeData="0|^1|DB2^2|Oracle^3|SQLServer^4|MySQL" ondblclick="return showCodeListEx('DBType',[this,DBTypeName],[0,1],null,null,null,1);"><input class=codename name=DBTypeName value="" readonly=true>
					</td>
					<td class=title>
						驱动
					</td>
					<td class=input>
						<Input class=codeno name=DriverType value="" CodeData="0|^1|com.ibm.db2.jcc.DB2Driver^2|Oracle^3|SQLServer^4|MySQL" ondblclick="return showCodeListEx('DriverType',[this,DriverTypeName],[0,1],null,null,null,1);"><input class=codename name=DriverTypeName value="" readonly=true>
					</td>
					<td class=title>
						IP地址
					</td>
					<td>
						<Input type="text" name="IPAddress">
					</td>
				</tr>
				<tr>
					<td class=title>
						端口号
					</td>
					<td>
						<Input type="text" name="PortNo">
					</td>
					<td class=title>
						用户名
					</td>
					<td>
						<Input type="text" name="UserName">
					</td>
					<td class=title>
						密码
					</td>
					<td>
						<Input type="text" name="PassWord">
					</td>
				<tr>
			</table>
			<hr>
			<table>
				<tr>
					<td class=titleImg>
						结果报告
					</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>
						<input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio1();">&nbsp;&nbsp;&nbsp;&nbsp;邮件功能
					</td>
					<td class=input>
						<input  type="file" name="fileName" size="50%">
					</td>
					<td class=title>
						<input value="上传邮件清单" type="button">
					</td>
				</tr>
				<tr class=common>
					<td class=title>
						<input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio1();">&nbsp;&nbsp;&nbsp;&nbsp;手机短信功能
					</td>
					<td class=input>
						<input  type="file" name="fileName" size="50%">
					</td>
					<td class=title>
						<input value="上传彩信清单" type="button">
					</td>
				</tr>
			</table>
			<hr>
			<table>
				<tr>
					<td class=title>
						<input class=cssButton value="保存配置" type="button">
					</td>
					<td class=title>
						<input class=cssButton value="修改配置" type="button">
					</td>
					<td class=title>
						<input class=cssButton value="删除配置" type="button">
					</td>
					<td class=title>
						<input class=cssButton value=" 返 回 " type="button">
					</td>
				</tr>
			</table>

			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
