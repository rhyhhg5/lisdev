
<%
	//程序名称：KnowledgeBaseGuideInput.jsp
	//程序功能：
	//创建日期：2012-11-28 
	//创建人  ：gy
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%%>

<script>
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

		<%@include file="KnowledgeBaseGuideInit.jsp"%>
		<SCRIPT src="KnowledgeBaseGuideInput.js"></SCRIPT>

	</head>

	<body onload="initForm()">
		<form action="" method=post name=fm target="fraSubmit">

			<Div id="divKnowledgeBaseGuide" style="display: ''">
				<table class=common>
					<TR>
						<td class="titleImg">
							任务计划信息
						</td>
					</TR>
					<TR class=common>
						<TD class=title>
							监控任务编码
						</TD>
						<TD class=input8>
							<Input class=common8 readonly name=TaskCode>
						</TD>
						<TD class=title>
							监控任务名称
						</TD>
						<TD class=input8>
							<Input class=common8 readonly name=TaskName>
						</TD>
						<TD class=title>
						</TD>
						<TD class=input8>
						</TD>
				</table>
				<TR>
					<INPUT VALUE=" 查 询 " class=cssButton TYPE=button name=addbutton onclick="query();">
				</TR>
				<table class=common>
					<TR>
					</TR>
					<TR>
						<TD text-align: left colSpan=1>
							<span id="spanKnowledgeBaseGuideGrid"> </span>
						</TD>
					</TR>
				</table>

				<Div id="divPage" align=center style="display: '' ">
					<INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage.firstPage();">
					<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
					<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
					<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
				</Div>
			</Div>

			<input type="hidden" name="fmAction" value="">

			<Div id="divTaskPlanButton">
				<TR>
					<INPUT VALUE="下   载" class=cssButton TYPE=button name=addbutton onclick="DownLoad();"
				</TR>
			</Div>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
