<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LCRecoilInput.jsp
//程序功能：共保抽档
//创建日期：2008-10-29 16:18:36
//创建人  ：fuxin 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GB2312" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.utility.*" %>
<%
    GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-60";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LCRecoilInput.js"></SCRIPT>  
 <%@include file="LCRecoilInit.jsp"%> 
 
<script language="javascript">
	var managecom = <%=tG.ManageCom%>;
   function initDate(){
		
  }
   </script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body  onload="initDate();initForm();initElementtype();">    
  <form action="./LCRecoilSave.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        查询条件：
      </td>
     </tr>
    </table>
    <Div  id= "divAlivePayMulti" style= "display: ''">
      <Table  class= common>
      	<tr>
      		<td class= title>抽档止期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=EndDate ></td>
      		<td class= title>任务号</td>
      		<td class= input><Input class= common name=Batchno></td>
      		<td class= title>操作机构</td>
      		<td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD></td>
      		<td class= title>状态</td>
      		<td class= input><Input class= "codeno"  name=State CodeData="0|^1|待反冲^2|已反冲" value ="1" verify="给付状态|notnull" ondblclick="return showCodeListEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PayTaskStateName></td>
      		</tr>			
      </Table>    
    </Div> 
    	<DIV id=DivClaimHead STYLE="display:''">   
	<table>
			<tr>
				<td>
				<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivClaim);">
				</td>
				<td class="titleImg">收、付费方式
				</td>
			</tr>
	</table>
	</Div>      

<DIV id=DivClaim STYLE="display:''"> 
	<table  class= common>
   <tr class=common id="trPayMode">
    <td  class= title>交费/付费方式</td>
		<td  class= input>
			<select name="PayMode" style="width: 128; height: 23"  >
				<option value = "0">请选择缴费方式</option>
				<%
				String [] payMode = {"2","3","10","11"};
				String tSql = "";
				String payModeName = "";
				for(int i=0;i<payMode.length;i++)
				{
				  tSql = "select codename('paymode','" + payMode[i] + "') from ldcode";
				  payModeName = new ExeSQL().getOneValue(tSql);
				%>
				<option value = <%=payMode[i]%>><%=payModeName%></option>
				<%
				}
				%>
			</select>
		</td>
		<TR class=common>  
          <TD  class= title>
              本方银行
          </TD>
          <TD  class= input>
          <Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno  verify="本方银行|NotNull"
            ondblclick="return showCodeList('paybankcode',[this,BankCodeBak],[0,1]);"
            onkeyup="return showCodeListKey('paybankcode',[this,BankCodeName],[0,1]);" readonly=true><input class=codename name=BankCodeBak readonly=true  elementtype=nacessary>
          </TD>
          <TD  class= title>
             本方银行账号
          </TD>
          <TD  class= input>
            <Input NAME=BankAccNo CodeData="" MAXLENGTH=20 CLASS=code verify="本方银行账号|NotNull"
             ondblclick="return showCodeList('paybankaccno',[this],[0],null,fm.BankCode.value,'bankcode');"
             onkeyup="return showCodeListKey('paybankaccno',[this],[0],null,fm.BankCode.value,'bankcode');" readonly=true  elementtype=nacessary>
            <input name="BankAccNoBak" type=hidden>
          </TD>
        </TR> 
	<!--	
	  </tr>
			<TD CLASS=title>银行</TD>
			<TD CLASS=input>
				<Input NAME=BankCode VALUE="" CLASS="codeNo" MAXLENGTH=20  ondblclick="return showCodeList('bank',[this,BankCodeName], [0,1]);" onkeyup="return showCodeListKey('bank',[this,BankCodeName], [0,1]);" ><Input NAME=BankCodeName VALUE="" CLASS="codeName" readonly >
				<input name="BankCodeBak" type=hidden>
			</TD>
			<TD CLASS=title width="109" >银行账号</TD>
			<TD CLASS=input>
				<Input NAME=BankAccNo VALUE="" CLASS=common verify="银行帐号|len<=40">
				<input name="BankAccNoBak" type=hidden>
			</TD>
		</TR>
		<!--<TR><TD><input type=button value="银行查询" class=cssButton onclick="querybank();"></TD></TR>   -->
	</Table> 	          
</DIV>     				
    <br>	               
        <INPUT VALUE="查询可反冲数据" class = cssbutton TYPE=button onclick="easyQueryClick();"> 
    <br><br>
        <Div  id= "divLLAppClaimReasonGrid" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanLCISPayGetGrid" >
            </span>
          </TD>
        </TR>
      </table>	
         <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">      
    </Div> 
        <table>
        <tr>
		 <td>
		   <input class=cssButton style='width:80px;' type=button value="财务确认" onclick="Confirm();">
		 </td>
		 <td>
		     	<input class=cssButton type=button value="打印明细" onclick="CIPrint();">
		     </td>
        </TR>  
        
       
    </table>
    <input type=hidden name=operate>
    <INPUT type= "hidden" name= "Claimmoney" value="">  
    <INPUT type= "hidden" name= "BatchNo1" value="">
    <INPUT type= "hidden" name= "Type" value="">
    
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html> 
