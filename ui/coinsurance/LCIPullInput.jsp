<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LCIPullInput.jsp
//程序功能：共保抽档
//创建日期：2008-10-24
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.utility.*" %>
<%
    GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-1";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
   //添加++
  // String tSQL = " 1 and ManageCom ='"+Comcode+"'  ";
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LCIPullInput.js"></SCRIPT>  
 <%@include file="LCIPullInit.jsp"%> 
 
<script language="javascript">
   function initDate(){
      fm.StartDate.value="<%=afterdate%>";
      fm.EndDate.value="<%=afterdate%>";
   }
    
   </script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body  onload="initDate();initForm();initElementtype();">    
  <form action="" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,LCISPayGet);">
    		</td>
    		<td class= titleImg>
    			共保抽档 
    		</td>
    	</tr>
    </table>
    <table class= common id="LCISPayGet" >
      	<TR  class= common>
          <TD  class= title8>共保公司</TD>
          <TD  class= input8> <Input class="codeno" name=AgentCom verify="共保公司|NOTNULL" ondblclick="return showCodeList('coinsurancecom',[this,AgentComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('coinsurancecom',[this,AgentComName],[0,1],null,null,null,1);"><input class=codename name=AgentComName elementtype=nacessary></TD>          
          <TD  class= title8>抽档类型</TD>
          <TD  class= input8> <input class="codeno" CodeData="0|5^01|新单^02|续期^03|保全^04|理赔赔款^05|理赔调查费"  verify="抽档类型|notnull" name=OtherNoType ondblclick="return showCodeListEx('OtherNoType',[this,TypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('OtherNoType',[this,TypeName],[0,1]);" ><input class=codename name=TypeName elementtype=nacessary></TD> 
          <TD class= input8> </TD>
          <TD class= input8> </TD>
        </TR>
		   <TR class= common>
		     <TD class= title8>结算起期</TD><TD class= "input"><Input class="input" name="StartDate" readonly ></TD>
		     <TD class= title8>结算止期</TD><TD class= "input"><Input class="input" name="EndDate" readonly ></TD>
		     <TD class= title8>从共保方总金额</TD><TD class= "input"><Input class="input" name="TotalAmount" readonly ></TD>
		     <TD class= input8> </TD>
		     <TD class= input8> </TD>
		   </TR>
		 </table>
		 <table>
		   <TR class= common>
		     <TD class= Input >
		       <input class=cssButton type=button value="查  询" onclick="SearchLCISPayGet()">
		       <input class=cssButton type=button value="抽  档" onclick="PullGrpCont()">
		     </TD>
		   </TR>
    </table>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLAppClaimReasonGrid);">
    		</td>
    		<td class= titleImg>
    			抽档确认 
    		</td>
    	</tr>
    </table>
    <Div  id= "divLLAppClaimReasonGrid" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanLCISPayGetGrid" >
            </span>
          </TD>
        </TR>
      </table>	
         <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
         <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">      
    </Div> 
    <table>
      <tr>
   		 <td>
		     <input class=cssButton type=button value="打印明细" onclick="CIPrint();">
		     <input class=cssButton type=button value="抽档确认" onclick="Confirm();">
		   </td>
		  <TR>
		  </TR>
      <TR><TD>备注说明：</TD><TR>
      <TR><TD>抽档流程：</TD><TR>
      <TR><TD>抽档-》打印明细-》抽档确认</TD><TR>
      <TR><TD>1、共保公司第一次抽档，结算起期为空。</TD><TR>
      <TR><TD>2、结算止期为操作日期的前一天。</TD><TR>
      <TR><TD>3、选择共保公司及抽档类型，点击“抽档”，可以抽取页面满足条件的数据；点击“查询”，可以查询满足条件的已经抽档、待确认的数据，选择一条数据，可以进行“打印明细”、“抽档确认”操作。 </TD><TR>
      <TR><TD>4、抽档确认前可以重复点击“抽档”，实现重抽的目的。 </TD><TR>
      <TR><TD>5、已经抽档确认的数据，不能再进行更改。 </TD><TR>
    </table>
    <input type=hidden name=operate>
    <input type=hidden name=SearchType>
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    
</body>
</html> 
