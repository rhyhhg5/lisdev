<%
//程序名称：
//程序功能：
//创建日期：2008-11-16
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>

<%
    // 页面入口参数。
    String tPrtNo = request.getParameter("PrtNo");
    String tMissionId = request.getParameter("MissionId");
    String tSubMissionId = request.getParameter("SubMissionId");
    String tProcessId = request.getParameter("ProcessId");
    String tActivityId = request.getParameter("ActivityId");
    String tActivityStatus = request.getParameter("ActivityStatus");
    String tManageCom = request.getParameter("ManageCom");
    // ---------------------
%>


<script language="JavaScript">

var mPrtNo = "<%=tPrtNo%>";
var mMissionId = "<%=tMissionId%>";
var mSubMissionId = "<%=tSubMissionId%>";
var mProcessId = "<%=tProcessId%>";
var mActivityId = "<%=tActivityId%>";
var mActivityStatus = "<%=tActivityStatus%>";
var mManageCom = "<%=tManageCom%>";

function initInpBox()
{
    try
    {
        fm.PrtNo.value = mPrtNo;
        
        fm.MissionId.value = mMissionId;
        fm.SubMissionId.value = mSubMissionId;
        fm.ProcessId.value = mProcessId;
        fm.ActivityId.value = mActivityId;
        fm.ActivityStatus.value = mActivityStatus;
    }
    catch(ex)
    {
        alert("初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInpBox();
        initAvailableCertifyListGrid();
        initCoInsuranceParamGrid();
        initSettlementCertifyContListGrid();
        querySettlementCertifyContList();
        initElementtype();
        showAllCodeName();
        initISTODS(mManageCom);
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

/**
 * 待结算导入单证清单。
 */
function initAvailableCertifyListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="单证号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        iArray[1][21]="CardNo";
        
        iArray[2]=new Array();
        iArray[2][0]="单证类别";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        iArray[2][21]="CertifyCode";
        
        iArray[3]=new Array();
        iArray[3][0]="被保人姓名";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        iArray[3][21]="InsuredName";
        
        iArray[4]=new Array();
        iArray[4][0]="证件号";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        iArray[4][21]="IdNo";
        
        iArray[5]=new Array();
        iArray[5][0]="保费";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        iArray[5][21]="Perm";
        
        iArray[6]=new Array();
        iArray[6][0]="单证状态代码";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=3;
        iArray[6][21]="CardStatus";
        
        iArray[7]=new Array();
        iArray[7][0]="单证状态";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        iArray[7][21]="CardStatusName";


        AvailableCertifyListGrid = new MulLineEnter("fm", "AvailableCertifyListGrid"); 

        AvailableCertifyListGrid.mulLineCount = 0;   
        AvailableCertifyListGrid.displayTitle = 1;
        AvailableCertifyListGrid.canSel = 0;
        AvailableCertifyListGrid.canChk = 1;
        AvailableCertifyListGrid.hiddenSubtraction = 1;
        AvailableCertifyListGrid.hiddenPlus = 1;
        AvailableCertifyListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化AvailableCertifyListGrid时出错：" + ex);
    }
}

/**
 * 结算单下单证清单。
 */
function initSettlementCertifyContListGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="单证号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        iArray[1][21]="CardNo";
        
        iArray[2]=new Array();
        iArray[2][0]="单证类别";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        iArray[2][21]="CertifyCode";
        
        iArray[3]=new Array();
        iArray[3][0]="被保人姓名";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        iArray[3][21]="InsuredName";
        
        iArray[4]=new Array();
        iArray[4][0]="证件号";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        iArray[4][21]="IdNo";
        
        iArray[5]=new Array();
        iArray[5][0]="保费";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        iArray[5][21]="Perm";
        
        iArray[6]=new Array();
        iArray[6][0]="单证状态代码";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=3;
        iArray[6][21]="CardStatus";
        
        iArray[7]=new Array();
        iArray[7][0]="单证状态";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        iArray[7][21]="CardStatusName";


        SettlementCertifyContListGrid = new MulLineEnter("fm", "SettlementCertifyContListGrid"); 

        SettlementCertifyContListGrid.mulLineCount = 0;   
        SettlementCertifyContListGrid.displayTitle = 1;
        SettlementCertifyContListGrid.canSel = 0;
        SettlementCertifyContListGrid.canChk = 1;
        SettlementCertifyContListGrid.hiddenSubtraction = 1;
        SettlementCertifyContListGrid.hiddenPlus = 1;
        SettlementCertifyContListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化SettlementCertifyContListGrid时出错：" + ex);
    }
}

function initCoInsuranceParamGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=10;
        iArray[0][3]=0;
        
        iArray[1]=new Array();
        iArray[1][0]="共保机构代码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=2;
        iArray[1][4]="CoInsuranceCom";
        iArray[1][5]="1|2";
        iArray[1][6]="0|1";
        iArray[1][9]="共保机构代码|NotNull";
        iArray[1][15]="1"
        iArray[1][16]="1 and ManageCom like #" + fm.ManageCom.value +"%#";
        
        iArray[2]=new Array();
        iArray[2][0]="共保机构名称";
        iArray[2][1]="150px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="负担比例";
        iArray[3][1]="60px";
        iArray[3][2]=100;
        iArray[3][3]=1;
        iArray[3][9]="负担比例|NotNull|Num";
        
        
        CoInsuranceParamGrid = new MulLineEnter("fm", "CoInsuranceParamGrid");
        
        CoInsuranceParamGrid.mulLineCount = 0;   
        CoInsuranceParamGrid.displayTitle = 1;
        CoInsuranceParamGrid.locked = 0;
        CoInsuranceParamGrid.canSel = 0;
        CoInsuranceParamGrid.canChk = 0;
        CoInsuranceParamGrid.hiddenPlus = 0;
        CoInsuranceParamGrid.hiddenSubtraction = 0;
        CoInsuranceParamGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>

