<%
//程序名称：ReProposalPrtInit.jsp
//程序功能：
//创建日期：2003-04-3 11:10:36
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ManageCom;
String strOperator =globalInput.Operator;
System.out.println("strOperator is "+strOperator);
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{

    try
    {
        // 初始化
        fmSave.all('fmOperator').value = '<%=strOperator%>';
        fm.all('ManageCom').value = '<%=strManageCom%>';
        //fm.CertifyCode.value = 'HB07/07B';
        if(fm.all('ManageCom').value==86){
            fm.all('ManageCom').readOnly=false;
        }
        else{
            fm.all('ManageCom').readOnly=true;
        }
        if(fm.all('ManageCom').value!=null)
        {
            var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('ManageComName').value=arrResult[0][0];
            }
        }
    }
    catch(ex)
    {
        alert("在CertifyTakebackInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
  try
  {
    initInpBox();
	  initPolGrid();
	  
  }
  catch(re)
  {
    alert("CertifyTakebackInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var PolGrid;          //定义为全局变量，提供给displayMultiline使用

// 保单信息列表的初始化
// 保单信息列表的初始化
function initPolGrid()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="单证编码";         		//列名
      iArray[1][1]="82px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="单证号码";         		//列名
      iArray[2][1]="82px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="单证状态";         		//列名
      iArray[3][1]="72px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="合同号码";         		//列名
      iArray[4][1]="72px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][9]="合同号码|len<=20";//校验
      
      iArray[5]=new Array();
      iArray[5][0]="单证状态编码";         		//列名
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=30;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      
      PolGrid = new MulLineEnter( "fmSave" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 30;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canChk = 1;
      PolGrid.canSel = 0;
      PolGrid.hiddenPlus = 1;
      PolGrid.selBoxEventFuncName ="Polinit";
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>