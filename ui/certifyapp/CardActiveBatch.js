//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 卡激活日清单。
 */
function queryCardActiveInfoList()
{
    if(!verifyInput2())
    {
        return false;
    }
    
    var cActiveDate = fm.ActiveDate.value;

    var tStrSql = ""
        + " select licail.BatchNo, licail.CardNo, licail.CValidate, "
        + " licail.SequenceNo, licail.Name, licail.Sex, licail.Birthday, "
        + " licail.ActiveDate, CodeName('cardactdealstate', licail.DealFlag) DealFlag, licail.DealDate, licail.State "
        + " from LICardActiveInfoList licail "
        + " where 1 = 1 "
        + " and licail.ActiveDate = '" + cActiveDate + "' "
        ;
    
    turnPage1.pageDivName = "divCardActiveInfoListGridPage";
    turnPage1.queryModal(tStrSql, CardActiveInfoListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有该日网站激活卡单信息！");
        return false;
    }
    
    return true;
}

/**
 * 激活处理。
 */
function importBatch()
{    
    fm.btnImport.disabled = true;
    
    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.action = "./CardActiveBatchSave.jsp";
    fm.submit();

    fm.action = "";
}

/**
 * 申请导入批次号提交后动作。
 */
function afterImportSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("申请批次失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        queryCardActiveInfoList();
    }

    fm.btnImport.disabled = false;
}

