<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
  <meta http-equiv="Content-type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="CertifyContQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CertifyContQueryInit.jsp"%>
  <title>卡折承保信息查询</title>
</head>
<body onload="initForm();" >
  <form action="./CertifyContQuerySave.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人保单信息部分 -->
    <table class=common border=0 width=100%>
      <tr><td class=titleImg align= center>请输入卡折承保查询条件：</td></tr>
    </table>
    <table class=common align=center>
      <tr class=common>
        <td class=title>管理机构</td>
        <td class=input><input class="codeNo" name=manageCom ondblclick="return showCodeList('ComCode',[this,manageComName],[0,1],null,null,null,1);" verify="管理机构|code:comcode&NOTNULL" elementtype=nacessary ><input class=codename name=manageComName readonly ></td>
        <td class=title>单证类型</td>
        <td class=input><input class="codeNo" name=certifyCode ondblclick="return showCodeList('CertifyCodeD',[this,certifyName],[0,1]);" ><input class=codename name=certifyName readonly ></td>
        <td class=title>业务类型</td>
        <td class=input><input class="codeNo" name=cardType CodeData="0|^0|卡单业务^1|撕单业务" ondblClick="showCodeListEx('CardTypeList',[this,cardTypeName],[0,1]);" ><input class=codename name=cardTypeName readonly ></td>
      </tr>
      <tr class=common>
        <td class=title>统计起始时间</td>
        <td class=input><input class="coolDatePicker" name=startSignDate verify="统计起期|notnull&Date" elementtype=nacessary ></td>
        <td class=title>统计终止时间</td>
        <td class=input><input class="coolDatePicker" name=endSignDate verify="统计止期|notnull&Date" elementtype=nacessary ></td>
      </tr>
    </table>
    <input type="hidden" class="common" name="querySql" >
    <input type="button" class="cssButton" value="查询卡折承保信息" onclick="easyQueryClick();">
    <input type="button" class="cssButton" value="下载清单" onclick="downloadList();">
    <br>
    <table>
      <tr>
        <td class=common>
          <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyContQuery);">
        </td>
        <td class=titleImg>卡折承保信息列表</td>
      </tr>
    </table>
    <div id= "divCertifyContQuery" style= "display: ''">
      <table class=common>
        <tr class=common>
          <td text-align: left colSpan=1>
            <span id="spanCertifyContQueryGrid" ></span>
          </td>
        </tr>
      </table>
      <div id= "divPage" align=center style= "display: 'none' ">
        <input value="首  页" class="cssButton" type=button onclick="turnPage.firstPage();">
        <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
        <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
        <input value="尾  页" class="cssButton" type=button onclick="turnPage.lastPage();">
      </div>
    </div>
  </form>
  <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
