<%
//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initCertifyListGrid();
        
        fm.all('ManageCom').value = <%=strManageCom%>;

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initCertifyListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="保险卡号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="结算单号";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="管理机构";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="生效日期";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="激活时间";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="单证编码";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="产品名称";
        iArray[7][1]="150px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="保障期间";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="保费";
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=0;
        
        iArray[10]=new Array();
        iArray[10][0]="保额";
        iArray[10][1]="80px";
        iArray[10][2]=100;
        iArray[10][3]=0;
        
        iArray[11]=new Array();
        iArray[11][0]="被保人姓名";
        iArray[11][1]="120px";
        iArray[11][2]=100;
        iArray[11][3]=0;
        
        iArray[12]=new Array();
        iArray[12][0]="性别";
        iArray[12][1]="60px";
        iArray[12][2]=100;
        iArray[12][3]=0;
        
        iArray[13]=new Array();
        iArray[13][0]="出生日期";
        iArray[13][1]="80px";
        iArray[13][2]=100;
        iArray[13][3]=0;
        
        iArray[14]=new Array();
        iArray[14][0]="证件类型";
        iArray[14][1]="80px";
        iArray[14][2]=100;
        iArray[14][3]=0;
        
        iArray[15]=new Array();
        iArray[15][0]="证件号";
        iArray[15][1]="150px";
        iArray[15][2]=100;
        iArray[15][3]=0;
        
        iArray[16]=new Array();
        iArray[16][0]="联系地址";
        iArray[16][1]="150px";
        iArray[16][2]=100;
        iArray[16][3]=0;
        
        iArray[17]=new Array();
        iArray[17][0]="邮政编码";
        iArray[17][1]="150px";
        iArray[17][2]=100;
        iArray[17][3]=0;
        
        iArray[18]=new Array();
        iArray[18][0]="电话";
        iArray[18][1]="150px";
        iArray[18][2]=100;
        iArray[18][3]=0;
        
        iArray[19]=new Array();
        iArray[19][0]="手机";
        iArray[19][1]="150px";
        iArray[19][2]=100;
        iArray[19][3]=0;
        
        iArray[20]=new Array();
        iArray[20][0]="电子邮箱";
        iArray[20][1]="150px";
        iArray[20][2]=100;
        iArray[20][3]=0;
        
        iArray[21]=new Array();
        iArray[21][0]="保险卡状态";
        iArray[21][1]="80px";
        iArray[21][2]=100;
        iArray[21][3]=0;
        
        iArray[22]=new Array();
        iArray[22][0]="实名化状态";
        iArray[22][1]="80px";
        iArray[22][2]=100;
        iArray[22][3]=0;


        CertifyListGrid = new MulLineEnter("fm", "CertifyListGrid");

        CertifyListGrid.mulLineCount = 0;   
        CertifyListGrid.displayTitle = 1;
        CertifyListGrid.canSel = 0;
        CertifyListGrid.hiddenSubtraction = 1;
        CertifyListGrid.hiddenPlus = 1;
        CertifyListGrid.canChk = 0;
        CertifyListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化CertifyListGrid时出错：" + ex);
    }
}
</script>

