<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2008-11-18
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
System.out.println("SettlementCertifyContListSave.jsp start ...");

CErrors tError = null;
String FlagStr = "Fail";
String Content = "";

GlobalInput tG = (GlobalInput)session.getValue("GI");

String tOperatorFlag = request.getParameter("fmOperatorFlag");
String tPrtNo = request.getParameter("PrtNo");
String tQuerySql = request.getParameter("querySql");

String tOperateType = null;
String tOperateMullineName = null;

// 业务处理前，是否通过相关数据完整性校验的标志。
boolean tChkFlag = false;

try
{
    if("InsertAll".equals(tOperatorFlag))
    {
        tOperateType = "Insert";
        tChkFlag = true;
    }
    else
    {
        Content = "未找到对应操作[" + tOperatorFlag + "]。";
        FlagStr = "Fail";
        tChkFlag = false;
    }
    
    if(tChkFlag)
    {    
        RSWrapper tRSWrapper = new RSWrapper();
        SSRS tResult = new SSRS();
        LICertifySet tLICertifySet = new LICertifySet();
        tRSWrapper.prepareData(null,tQuerySql);
        try
        {
			do
			{
				tResult = tRSWrapper.getSSRS();
				if (tResult != null && tResult.MaxRow > 0) {
					for (int idx = 1; idx <= tResult.MaxRow; idx++)
			        {
			            String[] tCardInfo = tResult.getRowData(idx);
			            
			            LICertifySchema tLICertifySchema = new LICertifySchema();
			            
			            tLICertifySchema.setCardNo(tCardInfo[0]);
			            tLICertifySchema.setCertifyStatus(tCardInfo[5]);
			            tLICertifySet.add(tLICertifySchema);
			        }
				}
			}
			while (tResult != null && tResult.MaxRow > 0);
		}
        catch (Exception ex)
        {
            ex.printStackTrace();
            tRSWrapper.close();
        }finally{
        	try{
        		tRSWrapper.close();
        	}catch(Exception ex){}
        }
        
        //SSRS tResult = new ExeSQL().execSQL(tQuerySql); // 前台查询Sql中，CardNo字段，第一列
        
        //LICertifySet tLICertifySet = new LICertifySet();
        
        //for (int idx = 1; idx <= tResult.MaxRow; idx++)
        //{
        //    String[] tCardInfo = tResult.getRowData(idx);
            
        //    LICertifySchema tLICertifySchema = new LICertifySchema();
            
        //    tLICertifySchema.setCardNo(tCardInfo[0]);
        //    tLICertifySchema.setCertifyStatus(tCardInfo[5]);
        //    tLICertifySet.add(tLICertifySchema);
        //}
        
        VData tVData = new VData();
        
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("PrtNo", tPrtNo);
        
        tVData.add(tTransferData);
        tVData.add(tLICertifySet);
        tVData.add(tG);
        
        SettlementCertifyContListUI tSettlementCertifyContListUI = new SettlementCertifyContListUI();
        if(!tSettlementCertifyContListUI.submitData(tVData, tOperateType))
        {
            Content = " 处理单证失败，原因是: " + tSettlementCertifyContListUI.mErrors.getFirstError();
            FlagStr = "Fail";
        }
        else
        {
            Content = " 处理单证成功！";
            FlagStr = "Succ";
        }
    }
}
catch (Exception e)
{
    Content = " 处理单证失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("SettlementCertifyContListSave.jsp end ...");

%>

<html>
<script language="javascript">
    parent.fraInterface.afterDealCertifyContSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
