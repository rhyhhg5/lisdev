<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
String strBatchNo = request.getParameter("BatchNo");
%>

<script language="JavaScript">
var mBatchNo = "<%=strBatchNo%>";

function initInpBox()
{
    try
    {
        //fm.all("ManageCom").value = "";
        fmImport.BatchNo.value = mBatchNo;
    }
    catch(ex)
    {
        alert("初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInpBox();
        initImportBatchGrid();
        initImportErrLogGrid();

        //fm.all('ManageCom').value = <%=strManageCom%>;
        
        initBatchNoInput(fmImport.BatchNo.value);
        fm.ImportBatchNo.value = fmImport.BatchNo.value;
        
        queryBatchList();

        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

/**
 * 导入确认列表
 */
function initImportBatchGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="销售渠道";
        iArray[2][1]="60px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="中介机构";
        iArray[3][1]="120px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="业务员代码";
        iArray[4][1]="60px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="业务员姓名";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="单证号";
        iArray[6][1]="120px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="管理机构";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="单证类型";
        iArray[8][1]="120px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="保费";
        iArray[9][1]="60px";
        iArray[9][2]=100;
        iArray[9][3]=0;
        
        iArray[10]=new Array();
        iArray[10][0]="保额";
        iArray[10][1]="60px";
        iArray[10][2]=100;
        iArray[10][3]=0;
        
        iArray[11]=new Array();
        iArray[11][0]="档次";
        iArray[11][1]="60px";
        iArray[11][2]=100;
        iArray[11][3]=0;
        
        iArray[12]=new Array();
        iArray[12][0]="份数";
        iArray[12][1]="60px";
        iArray[12][2]=100;
        iArray[12][3]=0;
        
        iArray[13]=new Array();
        iArray[13][0]="生效日期";
        iArray[13][1]="120px";
        iArray[13][2]=100;
        iArray[13][3]=0;
        
        iArray[14]=new Array();
        iArray[14][0]="被保人姓名";
        iArray[14][1]="80px";
        iArray[14][2]=100;
        iArray[14][3]=0;
        
        iArray[15]=new Array();
        iArray[15][0]="性别";
        iArray[15][1]="60px";
        iArray[15][2]=100;
        iArray[15][3]=0;
        
        iArray[16]=new Array();
        iArray[16][0]="出生日期";
        iArray[16][1]="80px";
        iArray[16][2]=100;
        iArray[16][3]=0;
        
        iArray[17]=new Array();
        iArray[17][0]="证件类别";
        iArray[17][1]="60px";
        iArray[17][2]=100;
        iArray[17][3]=0;
        
        iArray[18]=new Array();
        iArray[18][0]="证件号码";
        iArray[18][1]="120px";
        iArray[18][2]=100;
        iArray[18][3]=0;
        
        iArray[19]=new Array();
        iArray[19][0]="导入时间";
        iArray[19][1]="120px";
        iArray[19][2]=100;
        iArray[19][3]=0;
        
        iArray[20]=new Array();
        iArray[20][0]="单证状态";
        iArray[20][1]="120px";
        iArray[20][2]=100;
        iArray[20][3]=0;

        ImportBatchGrid = new MulLineEnter("fm", "ImportBatchGrid"); 

        ImportBatchGrid.mulLineCount = 0;   
        ImportBatchGrid.displayTitle = 1;
        ImportBatchGrid.canSel = 0;
        ImportBatchGrid.hiddenSubtraction = 1;
        ImportBatchGrid.hiddenPlus = 1;
        ImportBatchGrid.canChk = 0;
        ImportBatchGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ResultGrid时出错：" + ex);
    }
}

/**
 * 导入批次错误日志别表。
 */
function initImportErrLogGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="单证号";
        iArray[2][1]="60px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="错误日志";
        iArray[3][1]="180px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="导入时间";
        iArray[4][1]="120px";
        iArray[4][2]=100;
        iArray[4][3]=0;


        ImportErrLogGrid = new MulLineEnter("fm", "ImportErrLogGrid"); 

        ImportErrLogGrid.mulLineCount = 0;   
        ImportErrLogGrid.displayTitle = 1;
        ImportErrLogGrid.canSel = 0;
        ImportErrLogGrid.hiddenSubtraction = 1;
        ImportErrLogGrid.hiddenPlus = 1;
        ImportErrLogGrid.canChk = 0;
        ImportErrLogGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ImportErrLogGrid时出错：" + ex);
    }
}
</script>