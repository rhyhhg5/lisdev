//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		easyQueryClick();
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}

// 查询按钮
function easyQueryClick()
{
    // 初始化表格
    initPolGrid();
    if(!verifyInput2())
        return false;
    var StartNo = fm.StartNo.value;
    var EndNo = fm.EndNo.value;
    if(StartNo == null || StartNo == "" )
    {
        alert("单证起始号码必须填写！");
        return false;
    }
    if(EndNo == null || EndNo == "" )
    {
        alert("单证终止号码必须填写！");
        return false;
    }
    if(isNaN(StartNo)){
    	alert("单证起始号码必须是数字");
    	return false;
    }
    if(!isInteger(StartNo)){
    	alert("单证起始号码必须为正整数！");
    	return false;
    }
    if(isNaN(EndNo)){
    	alert("单证终止号码必须是数字");
    	return false;
    }
    if(!isInteger(EndNo)){
    	alert("单证终止号码必须为正整数！");
    	return false;
    }
    if(EndNo<StartNo){
    	alert("单证终止号码不能小于单证起始号码！");
    	return false;
    }
    
    if((EndNo-StartNo)>30){
    	alert("每次最多查询30个单证！");
    	return false;
    }
    
    // 书写SQL语句
    var manageCOM = fm.ManageCom.value;
    var strSQL = "";
    var srtASQL = "";
    var aResult = "";
    var getMngCom = "";
    for(var i=StartNo;i<=EndNo;i++){
    	var len = StartNo.length;
    	i = i.toString();
    	while(i.length != len){
    		 i = "0" + i;
    	}
    	if(strSQL==""){
    		strSQL = " select certifycode,'"+i+"',"
    		        +" (case StateFlag when '2' then '作废' when '3' then '遗失' when '4' then '销毁' when '5' then '正常回销' when '6' then '总部领用' when '7' then '已发放' when '8' then '已领用' when '9' then '空白回销' else '未领用' end), "
    		        +" (select contno from LCCertifyTakeBack where cardno = '"+i+"' and state = '1'), "
    		        +" StateFlag "
    		        +" from lzcard where startno<='"+i+"' and endno>='"+i+"'"
    		        + getWherePart('CertifyCode', 'CertifyTakeBackCode');
    		srtASQL = "select receivecom from lzcard where startno<='"+i+"' and endno>='"+i+"'"
    				+ getWherePart('CertifyCode', 'CertifyTakeBackCode');
    		aResult = easyExecSql(srtASQL);
    		if(aResult){
    			getMngCom = aResult[0][0].substr(1);
    			getHead = aResult[0][0].substr(0,1);
    			if("A" == getHead){
	    			if(manageCOM.length == 4){
	    				if(getMngCom == "86" || getMngCom.length == 4){
	    					strSQL += " and '"+manageCOM+"' = substr(receivecom,2)";
	    				}
	    				if(getMngCom.length == 8){
	    					if(manageCOM != getMngCom.substring(0,4)){
	    						strSQL += " and '"+manageCOM+"' = substr(receivecom,2,4)";
	    					}
	    				}
	    			}
	    			if(manageCOM.length == 8){
	    				var begin4manageCOM = manageCOM.substr(0,4);
//	    				strSQL += " and '"+manageCOM+"' = substr(receivecom,2)";
	    				if(getMngCom.length == 8){
	    					strSQL += " and '"+manageCOM+"' = '"+getMngCom+"'";
	    				}
	    				if(getMngCom == "86" || getMngCom.length == 4){
	    					strSQL += " and '"+begin4manageCOM+"' = '"+getMngCom+"'";
	    				}
	    			}
    			}
    			if("B" == getHead){
    				if(manageCOM.length == 8){
    					strSQL += " and '"+manageCOM+"' = (select comcode from lduser where usercode=substr(receivecom,2))";
    				}
    				if(manageCOM.length == 4){
    					strSQL += " and '"+manageCOM+"' = substr((select comcode from lduser where usercode=substr(receivecom,2)),1,4)";
    				}
    			}
    			if("D" == getHead){
    				if(manageCOM.length == 8){
    					strSQL += " and '"+manageCOM+"' = (select managecom from laagent where agentcode=substr(receivecom,2))";
    				}
    				if(manageCOM.length == 4){
    					strSQL += " and '"+manageCOM+"' = substr((select managecom from laagent where agentcode=substr(receivecom,2)),1,4)";
    				}
    			}
    			if("E" == getHead){
    				if(manageCOM.length == 8){
    					strSQL += " and '"+manageCOM+"' = (select managecom from lacom where agentcom=substr(receivecom,2))";
    				}
    				if(manageCOM.length == 4){
    					strSQL += " and '"+manageCOM+"' = substr((select managecom from lacom where agentcom=substr(receivecom,2)),1,4)";
    				}
    			}
    		}
    	}else{
    		strSQL += " union ";
    		strSQL += "select certifycode, '"+i+"',"
    			    +" (case StateFlag when '2' then '作废' when '3' then '遗失' when '4' then '销毁' when '5' then '正常回销' when '6' then '总部领用' when '7' then '已发放' when '8' then '已领用' when '9' then '空白回销' else '未领用' end), "
    			    +" (select contno from LCCertifyTakeBack where cardno = '"+i+"' and state = '1'), "
    			    +" StateFlag "
    				+" from lzcard where  startno<='"+i+"' and endno>='"+i+"'"
    				+ getWherePart('CertifyCode', 'CertifyTakeBackCode');
    		srtASQL = "select receivecom from lzcard where startno<='"+i+"' and endno>='"+i+"'"
					+ getWherePart('CertifyCode', 'CertifyTakeBackCode');
			aResult = easyExecSql(srtASQL);
			if(aResult){
				getMngCom = aResult[0][0].substr(1);
				getHead = aResult[0][0].substr(0,1);
				if("A" == getHead){
					if(manageCOM.length == 4){
						if(getMngCom == "86" || getMngCom.length == 4){
							strSQL += " and '"+manageCOM+"' = substr(receivecom,2)";
						}
						if(getMngCom.length == 8){
							if(manageCOM != getMngCom.substring(0,4)){
								strSQL += " and '"+manageCOM+"' = substr(receivecom,2,4)";
							}
						}
					}
					if(getMngCom == "86" || getMngCom.length == 4){
						var begin4manageCOM = manageCOM.substr(0,4);
//	    				strSQL += " and '"+manageCOM+"' = substr(receivecom,2)";
	    				if(getMngCom.length == 8){
	    					strSQL += " and '"+manageCOM+"' = '"+getMngCom+"'";
	    				}
	    				if(getMngCom == "86" || getMngCom.length == 4){
	    					strSQL += " and '"+begin4manageCOM+"' = '"+getMngCom+"'";
	    				}
	    			}
				}
				if("B" == getHead){
    				if(manageCOM.length == 8){
    					strSQL += " and '"+manageCOM+"' = (select comcode from lduser where usercode=substr(receivecom,2))";
    				}
    				if(manageCOM.length == 4){
    					strSQL += " and '"+manageCOM+"' = substr((select comcode from lduser where usercode=substr(receivecom,2)),1,4)";
    				}
    			}
    			if("D" == getHead){
    				if(manageCOM.length == 8){
    					strSQL += " and '"+manageCOM+"' = (select managecom from laagent where agentcode=substr(receivecom,2))";
    				}
    				if(manageCOM.length == 4){
    					strSQL += " and '"+manageCOM+"' = substr((select managecom from laagent where agentcode=substr(receivecom,2)),1,4)";
    				}
    			}
    			if("E" == getHead){
    				if(manageCOM.length == 8){
    					strSQL += " and '"+manageCOM+"' = (select managecom from lacom where agentcom=substr(receivecom,2))";
    				}
    				if(manageCOM.length == 4){
    					strSQL += " and '"+manageCOM+"' = substr((select managecom from lacom where agentcom=substr(receivecom,2)),1,4)";
    				}
    			}
			}
    	}
    }
    strSQL +=" order by 2";
    //prompt('',strSQL);
    turnPage.pageLineNum = 30;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 30);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    fm.querySql.value = strSQL;
}

function savepol()
{
    if(!beforeSubOrUp()){
    	return false;
    }
    if(!beforeSub()){
    	return false;
    }
    fmSave.all('fmtransact').value = "INSERT";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fmSave.submit();
}
function updatepol()
{
    if(!beforeSubOrUp()){
    	return false;
    }
    fmSave.all('fmtransact').value = "UPDATE";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fmSave.submit();
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

function downloadClick()
{
	if(PolGrid.mulLineCount==0)
    {
        alert("列表中没有数据可下载");
    }
    var oldAction = fm.action;
    fm.action = "ProposalTakeback.jsp";
    fm.submit();
    fm.action = oldAction;
}
function beforeSubOrUp()
{
	var lineCount = 0;
	var selLineCount=0;
	lineCount = PolGrid.mulLineCount;
	for (var i=0;i<lineCount;i++){
		if (PolGrid.getChkNo(i)){
			selLineCount=selLineCount+1;
			var tCardNo = PolGrid.getRowColData(i,2);
			var tContNo = PolGrid.getRowColData(i,4);
			if(tContNo ==null || tContNo==""){
				alert("对于选中的单证号码"+tCardNo+"，保单合同号码不能为空！");
				return false;
			}
			var tSql = " select 1 from lccont where appflag = '1' and contno = '"+tContNo+"' "
			         + " union all select 1 from lcgrpcont where appflag = '1' and grpcontno = '"+tContNo+"' "
			         + " union all select 1 from lbcont where appflag in ('1','3') and contno = '"+tContNo+"' "
			         + " union all select 1 from lbgrpcont where appflag in ('1','3') and grpcontno = '"+tContNo+"'";
			var aResult = easyExecSql(tSql,1,0,1);
			if(!aResult){
				alert("保单[合同号码："+tContNo+"]未签单，或无该保单！");
				return false;
			}
			var strComSQL = "select sendoutcom,receivecom from lzcard where 1=1 "
				  + getWherePart('CertifyCode', 'CertifyTakeBackCode')
				  + " and startno='"+tCardNo+"'";
			var comResult = easyExecSql(strComSQL);
			var sendOutCom = "";
			var receiveCom = "";
			if(comResult){
				sendOutCom = comResult[0][0];
				receiveCom = comResult[0][1];
			}
			var getHead = sendOutCom.substr(0,1);
			var getMngCom = sendOutCom.substr(1);
			var manageCOM = fm.ManageCom.value;
			var getComSQL = "";
			var getComResult = "";
			if(receiveCom == "SYS"){
				if("A" == getHead){
					if(manageCOM.length==4){
						if(getMngCom=="86"){
							alert("将该单核销的机构是：86，您选择的机构是"+manageCOM+"您不能操作！");
							return false;
						}
						if(getMngCom.length==4 && getMngCom!=manageCOM){
							alert("将该单核销的机构是："+getMngCom+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
						if(getMngCom.length==8 && getMngCom.substr(0,4) != manageCOM){
							alert("将该单核销的机构是："+getMngCom+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
					}
					if(manageCOM.length==8){
						if(getMngCom=="86"){
							alert("将该单核销的机构是：86，您选择的机构是"+manageCOM+"您不能操作！");
							return false;
						}
						if(getMngCom.length==4 && getMngCom!=manageCOM.substr(0,4)){
							alert("将该单核销的机构是："+getMngCom+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
						if(getMngCom.length==8 && getMngCom!=manageCOM){
							alert("将该单核销的机构是："+getMngCom+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
					}
				}
				if("B" == getHead){
					getComSQL = "select comcode from lduser where usercode='"+getMngCom+"'";
					getComResult = easyExecSql(getComSQL);
					var getComCode = "";
					if(getComResult){
						getComCode = getComResult[0][0];
					}
					if(manageCOM.length==4){
						if(getComCode.length==8 && getComCode.substr(0,4) != manageCOM){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
						if(getComCode.length==4 && getComCode != manageCOM){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
					}
					if(manageCOM.length==8){
						if(getComCode.length!=8){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
						if(getComCode.length==8 && getComCode != manageCOM){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
					}
    			}
    			if("D" == getHead){
    				getComSQL = "select managecom from laagent where agentcode='"+getMngCom+"'";
					getComResult = easyExecSql(getComSQL);
					var getComCode = "";
					if(getComResult){
						getComCode = getComResult[0][0];
					}
					if(manageCOM.length==4){
						if(getComCode.length==8 && getComCode.substr(0,4) != manageCOM){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
						if(getComCode.length==4 && getComCode != manageCOM){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
					}
					if(manageCOM.length==8){
						if(getComCode.length!=8){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
						if(getComCode.length==8 && getComCode != manageCOM){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
					}
    			}
    			if("E" == getHead){
    				getComSQL = "select managecom from lacom where agentcom='"+getMngCom+"'";
					getComResult = easyExecSql(getComSQL);
					var getComCode = "";
					if(getComResult){
						getComCode = getComResult[0][0];
					}
					if(manageCOM.length==4){
						if(getComCode.length==8 && getComCode.substr(0,4) != manageCOM){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
						if(getComCode.length==4 && getComCode != manageCOM){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
					}
					if(manageCOM.length==8){
						if(getComCode.length!=8){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
						if(getComCode.length==8 && getComCode != manageCOM){
							alert("将该单核销的机构是："+getComCode+",您选择的机构是"+manageCOM+",您不能操作！");
							return false;
						}
					}
    			}
			}
		}
	}
	if(selLineCount==0){
		alert("请选择需处理的单证信息！");
		return false;
	}
	return true;
}

function beforeSub(){
	var lineCount = 0;
	lineCount = PolGrid.mulLineCount;
	for (var i=0;i<lineCount;i++){
		if (PolGrid.getChkNo(i)){
			var tCardNo = PolGrid.getRowColData(i,2);
			var tSql = "select 1 from LCCertifyTakeBack where cardno = '"+tCardNo+"' ";
			var aResult = easyExecSql(tSql,1,0,1);
			if(aResult){
				alert("该单证["+tCardNo+"]已于保单关联，不能进行保存操作！");
				return false;
			}
		}
	}
	return true;
}



