//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询事件
function easyQueryClick()
{
    var strSQL = " select tmp.ManageCom,tmp.PrtNo, tmp.CertifyCode, tmp.CertifyName, "
        + " getUniteCode(tmp.AgentCode), tmp.AgentCom, tmp.MakeDate, tmp.SignDate, tmp.CValiDate, "
        + " sum(tmp.Prem) "
        + " from ( "
        + "select distinct b.ManageCom,a.PrtNo, a.CertifyCode, "
        + "(select CertifyName from LMCertifyDes c where a.CertifyCode = c.CertifyCode), "
        + "b.AgentCode, b.AgentCom, a.MakeDate, b.SignDate, b.CValiDate, b.Prem "
        + "from LICertify a, LCGrpCont b "
        + "where a.PrtNo = b.PrtNo "
        + getWherePart('b.ManageCom', 'manageCom', 'like')
        + getWherePart('a.CertifyCode', 'certifyCode')
        + getWherePart('getUniteCode(b.AgentCode)', 'agentCode')
        + getWherePart('b.AgentCom', 'agentCom')
        + getWherePart('a.PrtNo', 'prtNo')
        + getWherePart('b.SignDate', 'startSignDate', '>=')
        + getWherePart('b.SignDate', 'endSignDate', '<=')
        + getWherePart('a.MakeDate', 'startImportDate', '>=')
        + getWherePart('a.MakeDate', 'endImportDate', '<=')
        + getWherePart('b.CValiDate', 'startCValiDate', '>=')
        + getWherePart('b.CValiDate', 'endCValiDate', '<=')
        + " ) as tmp  "
        + " group by tmp.ManageCom,tmp.PrtNo, tmp.CertifyCode, tmp.CertifyName, tmp.AgentCode,  "
        + " tmp.AgentCom, tmp.MakeDate, tmp.SignDate, tmp.CValiDate "
        + " with ur ";

    turnPage.queryModal(strSQL, CertifyPremQueryGrid);

    fm.querySql.value = strSQL;
}

//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && CertifyPremQueryGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "CertifyPremQueryDownload.jsp";
        fm.target = "_blank";
        fm.submit();
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
