<%
//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initCertifySettleListGrid();
        
        fm.all('ManageCom').value = <%=strManageCom%>;

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initCertifySettleListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="结算单号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="申请日期";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="申请机构";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="申请人";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="MissionId";
        iArray[5][1]="150px";
        iArray[5][2]=100;
        iArray[5][3]=3;
        
        iArray[6]=new Array();
        iArray[6][0]="SubMissionId";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=3;
        
        iArray[7]=new Array();
        iArray[7][0]="ProcessId";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=3;
        
        iArray[8]=new Array();
        iArray[8][0]="ActivityId";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=3;
        
        iArray[9]=new Array();
        iArray[9][0]="ActivityStatus";
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=3;


        CertifySettleListGrid = new MulLineEnter("fm", "CertifySettleListGrid"); 

        CertifySettleListGrid.mulLineCount = 0;   
        CertifySettleListGrid.displayTitle = 1;
        CertifySettleListGrid.canSel = 1;
        CertifySettleListGrid.hiddenSubtraction = 1;
        CertifySettleListGrid.hiddenPlus = 1;
        CertifySettleListGrid.canChk = 0;
        CertifySettleListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ResultGrid时出错：" + ex);
    }
}
</script>

