
<%
	//程序名称：
	//程序功能：
	//创建日期：2008-12-12
	//创建人  ：LY
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initCardActiveInfoListGrid();        
        fm.all('ManageCom').value = <%=strManageCom%>;
        
        showAllCodeName();
        initElementtype();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initCardActiveInfoListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="卡号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="约定生效日期";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="被保人流水号";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="姓名";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=3;
        
        iArray[5]=new Array();
        iArray[5][0]="性别";
        iArray[5][1]="40px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="出生日期";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="网站激活日期";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="处理状态";
        iArray[8][1]="180px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="处理日期";
        iArray[9][1]="100px";
        iArray[9][2]=100;
        iArray[9][3]=0;
        
        iArray[10]=new Array();
        iArray[10][0]="状态";
        iArray[10][1]="60px";
        iArray[10][2]=100;
        iArray[10][3]=3;

        CardActiveInfoListGrid = new MulLineEnter("fm", "CardActiveInfoListGrid"); 

        CardActiveInfoListGrid.mulLineCount = 0;   
        CardActiveInfoListGrid.displayTitle = 1;
        CardActiveInfoListGrid.canSel = 1;
        CardActiveInfoListGrid.hiddenSubtraction = 1;
        CardActiveInfoListGrid.hiddenPlus = 1;
        CardActiveInfoListGrid.canChk = 0;
        CardActiveInfoListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化CardActiveInfoListGrid时出错：" + ex);
    }
}
</script>

