//程序名称：
//程序功能：
//创建日期：2008-12-03
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryCertifySettlementPayPrint()
{
    if(!verifyInput2())
    {
        return false;
    }

    var tStrSql = ""
        + " select lopm.PrtSeq, lgc.PrtNo, lgc.ProposalGrpContNo, lgc.GrpContNo, "
        + " lgc.AgentCom, getUniteCode(lgc.AgentCode), "
        + " (select Name from LAAgent laa where laa.AgentCode = lgc.AgentCode) AgentName, "
        + " lgc.Prem, "
        + " CodeName('paymode', lgc.PayMode) PayMode, "
        + " (case lopm.StateFlag when '0' then '未打印' when '1' then '已打印' end) StateFlag, "
        + " lopm.Code "
        + " from LOPrtManager lopm "
        + " inner join LCGrpCont lgc on lgc.GrpContNo = lopm.OtherNo and lopm.OtherNoType = '11' "
        + " inner join LWMission lwm on lwm.MissionProp1 = lgc.GrpContNo "
        + " where 1 = 1 "
        + " and lopm.Code = 'LC01' "
        + " and lwm.ActivityId = '0000011002' "
        + getWherePart("lgc.PrtNo", "PrtNo")
        + getWherePart("lopm.StateFlag", "PrintStateFlag")
        ;
    
    turnPage1.pageDivName = "divCSLPayPrintGridPage";
    turnPage1.queryModal(tStrSql, CSLPayPrintGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("未找到相关通知书！");
        return false;
    }
    
    return true;
}

function printPolpdf()  
{
    var tRow = CSLPayPrintGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }

    var tRowDatas = CSLPayPrintGrid.getRowData(tRow);
    
    var tPrtSeq = tRowDatas[0];
    var tPrtNo = tRowDatas[1];
    var tCode = tRowDatas[10];
    
    //fm.btnPrintPdf.disabled = true;
    
    fm.fmtransact.value = "PRINT";
    var tUrl = ""
        + "../uw/PrintPDFSave.jsp"
        + "?Code=0" + tCode
        + "&PrtSeq=" + tPrtSeq
        + "&PrtNo=" + tPrtNo
        + "&RePrintFlag=" + mRePrintFlag
        ;
    fm.action = tUrl;
    fm.submit();
    
    fm.action = "";
    fm.fmtransact.value = "";

    return true;
}

/**
 * 申请单证结算单提交后动作。
 */
function afterSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("打印失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    //fm.btnPrintPdf.disabled = false;
}


function printPolpdfNew()
{
    var tRow = CSLPayPrintGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }

    var tRowDatas = CSLPayPrintGrid.getRowData(tRow);
    
    var tPrtSeq = tRowDatas[0];
    var tPrtNo = tRowDatas[1];
    var tCode = tRowDatas[10];

    fm.fmtransact.value = "PRINT";
    fm.target = "fraSubmit";
    fm.action = "../uw/PDFPrintSave.jsp?Code=" + tCode + "&PrtSeq=" + tPrtSeq;
    fm.submit();
    
    return true;
}

/**
 * 申请单证结算单提交后动作。
 */
function afterSubmit2(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("打印失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    //fm.btnPrintPdf.disabled = false;
}