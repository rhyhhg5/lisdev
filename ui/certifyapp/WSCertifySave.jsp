<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2009-05-04
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.*"%>


<%
System.out.println("WSCertifySave.jsp start ...");

CErrors tError = null;
String FlagStr = "Fail";
String Content = "";


GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{
    String[] tCardNos = request.getParameterValues("CertifyListGrid1");
    String[] tSels = request.getParameterValues("InpCertifyListGridSel");
    
    LICertifySchema tLICertifySchema = null;
    
    for (int idx = 0; idx < tSels.length; idx++)
    {
        if(tSels[idx].equals("1"))
        {
            String tCardNo = tCardNos[idx];
            tLICertifySchema = new LICertifySchema();
            tLICertifySchema.setCardNo(tCardNo);
            break;
        }
    }
    
    VData tVData = new VData();

    TransferData tTransferData = new TransferData();

    tVData.add(tLICertifySchema);
    tVData.add(tTransferData);
    tVData.add(tGI);
    
    WSCertifyUI tWSCertifyUI = new WSCertifyUI();
    if(!tWSCertifyUI.submitData(tVData, "WSCertify"))
    {
        Content = " 处理失败，原因是: " + tWSCertifyUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 处理成功！";
        FlagStr = "Succ";
    }

}
catch (Exception e)
{
    Content = " 处理失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("WSCertifySave.jsp end ...");

%>

<html>
<script language="javascript">
    parent.fraInterface.afterWSSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
