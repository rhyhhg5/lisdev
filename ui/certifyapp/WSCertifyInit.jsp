<%
//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initCertifyListGrid();
        initCertInsuListGrid();
        
        fm.all('ManageCom').value = <%=strManageCom%>;

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initCertifyListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="保险卡号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="结算单号";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="生效日期";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="激活时间";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="第一被保人姓名";
        iArray[5][1]="120px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="第一被保人证件号";
        iArray[6][1]="150px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="被保人数";
        iArray[7][1]="60px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="保险卡状态";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="实名化状态";
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=0;


        CertifyListGrid = new MulLineEnter("fm", "CertifyListGrid");

        CertifyListGrid.mulLineCount = 0;   
        CertifyListGrid.displayTitle = 1;
        CertifyListGrid.canSel = 1;        
        CertifyListGrid.selBoxEventFuncName = "clkCertInfo";
        CertifyListGrid.hiddenSubtraction = 1;
        CertifyListGrid.hiddenPlus = 1;
        CertifyListGrid.canChk = 0;
        CertifyListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化CertifyListGrid时出错：" + ex);
    }
}

function initCertInsuListGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="保险卡号";
        iArray[1][1]="60px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="被保人姓名";
        iArray[2][1]="120px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="被保人姓别";
        iArray[3][1]="60px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="被保人出生日期";
        iArray[4][1]="60px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="被保人证件类别";
        iArray[5][1]="60px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="被保人证件号";
        iArray[6][1]="120px";
        iArray[6][2]=100;
        iArray[6][3]=0;


        CertInsuListGrid = new MulLineEnter("fm", "CertInsuListGrid");

        CertInsuListGrid.mulLineCount = 0;
        CertInsuListGrid.displayTitle = 1;
        CertInsuListGrid.canSel = 0;
        CertInsuListGrid.hiddenSubtraction = 1;
        CertInsuListGrid.hiddenPlus = 1;
        CertInsuListGrid.canChk = 0;
        CertInsuListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化CertInsuListGrid时出错：" + ex);
    }
}
</script>

