<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.certifybusiness.api.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>


<%
System.out.println("ICardInsu.jsp start ...");

CErrors tError = null;
String FlagStr = "Fail";
String Content = "";

String tCardNo = request.getParameter("CardNo");
String tCValidate = request.getParameter("CValidate");
String tActiveDate = request.getParameter("ActiveDate");
String tOperator = request.getParameter("Operator");

System.out.println("tCardNo = " + tCardNo);

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("CardNo", tCardNo);
    tTransferData.setNameAndValue("CValidate", tCValidate);
    tTransferData.setNameAndValue("ActiveDate", tActiveDate);
    tTransferData.setNameAndValue("Operator", tOperator);
    
    tVData.add(tTransferData);
    tVData.add(tGI);
    
    CardInfoDeal tCardInfoDeal = new CardInfoDeal();
    if(!tCardInfoDeal.submitData(tVData, null))
    {
        Content = " 卡激活数据处理失败，原因是: " + tCardInfoDeal.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 卡激活数据处理成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 卡激活数据处理失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("ICardInsu.jsp end ...");

%>