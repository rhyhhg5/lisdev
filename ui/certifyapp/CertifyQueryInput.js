//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询事件
function easyQueryClick()
{
	//查询前的校验
	if(!checkData())
	{
		return false;
	}
	
    var insuredNameSQL = "";//被保险人姓名查询
    if(fm.insuredName.value != null && fm.insuredName.value != "")
        insuredNameSQL = "and exists (select 1 from LICertifyInsured b where a.CardNo = b.CardNo "
            + "and Name like '%" + fm.insuredName.value + "%') ";
    var signDateSQL = "";//签单日期
    if(fm.clearStartDate.value != "" || fm.clearEndDate.value != "")
    {
        signDateSQL = "and exists (select 1 from LCGrpCont b where a.PrtNo = b.PrtNo "
            + getWherePart('SignDate', 'clearStartDate', ">=")
            + getWherePart('SignDate', 'clearEndDate', "<=")
            + " ) "
            ;
    }
    if(fm.signStartDate.value != "" || fm.signEndDate.value != "")
    {
        signDateSQL = "and exists (select 1 from LCGrpCont b where a.PrtNo = b.PrtNo "
            + getWherePart('SignDate', 'signStartDate', ">=")
            + getWherePart('SignDate', 'signEndDate', "<=")
            + " ) "
            ;
    }
    
    var tSettleStateSql = "";
    var tSettleState = fm.SettleState.value;
    if(tSettleState == "1")
    {
        tSettleStateSql = " and State in ('00', '01', '02', '03') ";
    }
    else(tSettleState == "2")
    {
        tSettleStateSql = " and State in ('04') ";
    }
    
    var strSQL = "select BatchNo, ManageCom, PrtNo, CardNo, CodeName('certifycontstate', State), getUniteCode(AgentCode), "
        + "AgentCom, (select Name from LICertifyInsured b where a.CardNo = b.CardNo and b.SequenceNo = '1'), "
        +"(select occupationname from LICertifyInsured b,ldoccupation l1 where a.CardNo = b.CardNo and b.occupationcode=l1.occupationcode and b.SequenceNo = '1') occupationname,"
        +"(select occupationtype from LICertifyInsured b where a.CardNo = b.CardNo and b.SequenceNo = '1') occupationtype,"
        +"(select occupationcode from LICertifyInsured b where a.CardNo = b.CardNo and b.SequenceNo = '1') occupationcode,"
        +"Prem,"
        + "CertifyCode, (select CertifyName from LMCertifyDes b where a.CertifyCode = b.CertifyCode), "
        + "MakeDate, (select SignDate from LCGrpCont b where a.PrtNo = b.PrtNo), "
        + "(select SignDate from LCGrpCont b where a.PrtNo = b.PrtNo), a.CValidate," 
        +" (select COALESCE(grpcontno,'') from lcgrpcont where grpcontno=a.grpcontno and appflag='1') "
        + "from LICertify a "
        + "where 1 = 1 " + insuredNameSQL + signDateSQL
        + getWherePart('ManageCom', 'manageCom', 'like')
        + getWherePart('CertifyCode', 'certifyCode')
        + getWherePart('CardNo', 'cardNo')
        + getWherePart('State', 'state')
        + getWherePart('PrtNo', 'payNo')
        + getWherePart('getUniteCode(AgentCode)', 'agentCode')
        + getWherePart('AgentCom', 'agentCom')
        + getWherePart('MakeDate', 'importStartDate', ">=")
        + getWherePart('MakeDate', 'importEndDate', "<=")
        + getWherePart('BatchNo', 'BatchNo')
        + "order by PrtNo";
//prompt('',strSQL);
    turnPage.queryModal(strSQL, CertifyQueryGrid);

    fm.querySql.value = strSQL;
}

//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && CertifyQueryGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "CertifyQueryDownload.jsp";
        fm.target = "_blank";
        fm.submit();
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}

//查询前的校验
function checkData()
{
	var tPrtNo = fm.payNo.value; 						//结算单号 
	var tBatchNo = fm.BatchNo.value; 					//导入批次号
	var tCardNo = fm.cardNo.value;						//单证流水号，即卡号
	var tImportStartDate = fm.all('importStartDate').value;	//导入日期起期
	var tImportEndDate = fm.all('importEndDate').value;		//导入日期止期
	var tClearStartDate = fm.all('clearStartDate').value;		//结算日期起期
	var tClearEndDate = fm.all('clearEndDate').value;			//结算日期止期
	var tSignStartDate = fm.all('signStartDate').value;		//签单日期起期
	var tSignEndDate = fm.all('signEndDate').value;			//签单日期止期
	
	if((tPrtNo == null || tPrtNo == "") 
		&& (tBatchNo == null || tBatchNo == "") 
		&& (tCardNo == null || tCardNo == ""))
	{
		if((tImportStartDate == null || tImportStartDate == "")
			&& (tImportEndDate == null || tImportEndDate == "")
			&& (tClearStartDate == null || tClearStartDate == "")
			&& (tClearEndDate == null || tClearEndDate == "")
			&& (tSignStartDate == null || tSignStartDate == "")
			&& (tSignEndDate == null || tSignEndDate == ""))
		{
			alert("请选择导入起止日期、结算起止日期和签单起止日期三组日期中的一组作为查询条件，或直接输入结算单号、导入批次号和单证流水号三者之一进行查询！");
			return false;
		}
		
		if((tImportStartDate == null || tImportStartDate == "")
			&& (tImportEndDate != null && tImportEndDate != ""))
		{
			alert("请选择导入日期起期！");
			return false;
		}
		
		if((tImportStartDate != null && tImportStartDate != "")
			&& (tImportEndDate == null || tImportEndDate == ""))
		{
			alert("请选择导入日期止期！");
			return false;
		}
		
		if((tClearStartDate == null || tClearStartDate == "")
			&& (tClearEndDate != null && tClearEndDate != ""))
		{
			alert("请选择结算日期起期！");
			return false;
		}
		
		if((tClearStartDate != null && tClearStartDate != "")
			&& (tClearEndDate == null || tClearEndDate == ""))
		{
			alert("请选择结算日期止期！");
			return false;
		}
		
		if((tSignStartDate == null || tSignStartDate == "")
			&& (tSignEndDate != null && tSignEndDate != ""))
		{
			alert("请选择签单日期起期！");
			return false;
		}
		
		if((tSignStartDate != null && tSignStartDate != "")
			&& (tSignEndDate == null || tSignEndDate == ""))
		{
			alert("请选择签单日期止期！");
			return false;
		}
		
		if(tImportStartDate != null && tImportStartDate != ""
			&& tImportEndDate != null && tImportEndDate != "")
		{
			if(dateDiff(tImportStartDate,tImportEndDate,"M")>3)
			{
				alert("导入日期统计期最多为三个月！");
				return false;
			}
		}
		
		if(tClearStartDate != null && tClearStartDate != ""
			&& tClearEndDate != null && tClearEndDate != "")
		{
			if(dateDiff(tClearStartDate,tClearEndDate,"M")>3)
			{
				alert("结算日期统计期最多为三个月！");
				return false;
			}
		}
		
		if(tSignStartDate != null && tSignStartDate != ""
			&& tSignEndDate != null && tSignEndDate != "")
		{
			if(dateDiff(tSignStartDate,tSignEndDate,"M")>3)
			{
				alert("签单日期统计期最多为三个月！");
				return false;
			}
		}
	}
	return true;
}
