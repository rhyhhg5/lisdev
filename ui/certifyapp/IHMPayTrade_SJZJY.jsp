<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.cbsws.xml.ctrl.blogic.*"%>

<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//修改人  ：
System.out.println("IHMPayTrade_SJZJY.jsp start ...");

String FlagStr = "0";
String Content = "";

String strResult = null;


//设置request的接受字符集
String tBatchNo = request.getParameter("BATCHNO");
String tManageCom = request.getParameter("MANAGECOM");
String tSaleChnl = request.getParameter("SALECHNL");
String tWrapCode = request.getParameter("WRAPCODE");
String tWrapName = request.getParameter("WRAPNAME");
String tSalesCount = request.getParameter("SALESCOUNT");

String tFinanceNum = request.getParameter("FINANCE_NUM");
String tChargeAccount = request.getParameter("CHARGE_ACCOUNT");
String tChargeAmount = request.getParameter("CHARGE_AMOUNT");
String tConfMakeDate = request.getParameter("CONFMAKE_DATE");
String tConfMakeTime = request.getParameter("CONFMAKE_TIME");
String tIdentityKey = request.getParameter("FINANCE_PASS");

System.out.println("BatchNo = " + tBatchNo);
System.out.println("FinanceNum = " + tFinanceNum);

try
{
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    
    tTransferData.setNameAndValue("BatchNo", tBatchNo);
    tTransferData.setNameAndValue("ManageCom", tManageCom);
    tTransferData.setNameAndValue("SaleChnl", tSaleChnl);
    tTransferData.setNameAndValue("WrapCode", tWrapCode);
    tTransferData.setNameAndValue("WrapName", tWrapName);
    tTransferData.setNameAndValue("SalesCount", tSalesCount);
    
    tTransferData.setNameAndValue("FinanceNum", tFinanceNum);
    tTransferData.setNameAndValue("ChargeAccount", tChargeAccount);
    tTransferData.setNameAndValue("ChargeAmount", tChargeAmount);
    tTransferData.setNameAndValue("ConfMakeDate", tConfMakeDate);
    tTransferData.setNameAndValue("ConfMakeTime", tConfMakeTime);
    tTransferData.setNameAndValue("IdentityKey", tIdentityKey);
    
    tVData.add(tTransferData);
    
    HMCertPayTradeUI tBL = new HMCertPayTradeUI();
    if(!tBL.submitData(tVData, "HMPay"))
    {
        Content = tBL.mErrors.getFirstError();
        FlagStr = "0";
    }
    else
    {
        Content = "处理成功";
        FlagStr = "1";
    }
}
catch(Exception ex)
{
	System.out.println("处理异常！");
	Content = "处理异常";
    FlagStr = "0";
}
strResult = FlagStr + "|" + Content;

System.out.println("IHMPayTrade_SJZJY.jsp end ...");

%>
<%=strResult%>