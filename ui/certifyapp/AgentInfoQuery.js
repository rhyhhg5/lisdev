 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //initPolGrid();
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

// 查询按钮
function easyQueryClick()
{
    initAgentGrid();
    
    var tManageCom = fm.ManageCom.value;
    
    var tStrSql = ""
        + " select laa.groupAgentCode, laa.Name, laa.IdNo, "
        + " laa.ManageCom, labg.BranchAttr, labg.Name, CodeName('agentstate', laa.AgentState) "
        + " from LAAgent laa "
        + " inner join LABranchGroup labg on laa.AgentGroup = labg.AgentGroup "
        + " where 1 = 1 "
        + " and laa.ManageCom like '" + tManageCom + "%' "
        + getWherePart('laa.groupAgentCode', 'AgentCode', 'like')
        + getWherePart('labg.BranchAttr', 'AgentGroup', 'like')
        + getWherePart('laa.Name', 'Name', 'like')
        + getWherePart('laa.Sex', 'Sex')
        + getWherePart('laa.Birthday','Birthday')
        + getWherePart('laa.IDNo', 'IDNo')
        + getWherePart('laa.EmployDate', 'EmployDate')
        + " order by AgentCode "
        ;
    
    turnPage.pageDivName = "divAgentGridPage";
    turnPage.queryModal(tStrSql, AgentGrid);
    
    if (!turnPage.strQueryResult)
    {
        alert("未找到相关信息！");
        return false;
    }
    
    return true;
}