//程序名称：
//程序功能：
//创建日期：2009-6-8
//创建   ：guzg
//更新记录:  于明明   更新日期  2015-1-14  更新原因/内容 1、单选框改为复选框。
//2、一页显示30条记录。
//3、增加“批次整体删除”按钮，点此按钮后，弹出提示框，提示“该操作将会将批次号为XXXXXXXXXX下的所有卡折，是否继续？”，选择“是”，删除整个批次下的卡折。选择“否”，则取消操作。

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;

window.onfocus = myonfocus;

/**
 * 查询已经录入但未结算的清单。
 */
function queryCertify()
{
    if(!verifyInput2())
    {
        return false;
    }

    /* 1) licertify中state必须为('01','02','03',)
     * 2) licertify.grpcontno/licertify.proposalgrpcontno 必须为空
     * 3) 如果licertify.prtno非空，licertify.prtno查lcgrpcont表要没有记录
     * 4) 激活标志必须未被网站激活，licertify.activeflag in ('00', '01')
     */
    
    var tStrSql = ""
        + " select a.batchno,a.managecom,a.cardno,'',a.makedate from licertify a where a.state in('01','02','03') "
        //+ " and a.activeflag in ('00', '01') "
        + " and a.grpcontno is null and a.proposalgrpcontno is null "
        + " and (a.prtno is null or a.prtno not in(select prtno from lcgrpcont))   "
        + getWherePart("a.batchno", "BatchNo")
        + getWherePart("a.managecom", "ManageCom",'like')
        + getWherePart("a.cardno", "CardNo")
        + getWherePart("a.makedate", "Date")
        + "union select a.batchno,a.managecom,a.cardno,a.prtno,a.makedate from licertify a where a.state in ('03','04') "
        //+ " and a.activeflag in ('00', '01') "
        + getWherePart("a.batchno", "BatchNo")
        + getWherePart("a.managecom", "ManageCom",'like')
        + getWherePart("a.cardno", "CardNo")
        + getWherePart("a.makedate", "Date")
        + " with ur ";
    turnPage1.pageLineNum = 30;//查询后显示几条数据
    turnPage1.pageDivName = "divCertifyPage";
    turnPage1.queryModal(tStrSql, CertifyGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有可以删除的卡折!");
        return false;
    }
    
    return true;
}

/**
 * 
 * 批次整体删除
 */
function delAllImportBatch(){
	var checkedRowNum=0;
	var rowNum=CertifyGrid.mulLineCount;
	  var count = -1;

	   for(var i=0;i<rowNum;i++){
      	if(CertifyGrid.getChkNo(i))
	        {    
      		    count =i;
      		  checkedRowNum+=1;
      		var tRowDatas = CertifyGrid.getRowData(count);
	        }
	    } //end of for
		   if(checkedRowNum == 0){
		    	alert("请选择一条记录");
		    	return false;
		    }
	var tBatchNo = tRowDatas[0];
	var tManagecom = tRowDatas[1];
	var tCardNo = tRowDatas[2];
	var strSQL = "select state from licertify where cardno = '" + tCardNo + "'";
	var arrResult = easyExecSql(strSQL);
	
	   if(checkedRowNum>1){
		   alert("只能选一条记录");
	        return false;
	   }
	 
	   if(checkedRowNum==1){
		   if(confirm("该操作将会将批次号为["+tBatchNo+"]下的所有卡折删除")){			 
			   if(arrResult)
	    	    {   
	    	    	if(arrResult[0][0] == '02'){
	    	    		alert("单证号["+tCardNo+"]已加入结算单,如果想删除,请去卡折结算中进行从结算单中取消操作,然后再进行卡折删除操作!");
	    	    		return false;
	    	    	}else if(arrResult[0][0] == '03'){
	    	    		alert("单证号["+tCardNo+"]已经做过结算,如果想删除,请去团体保单中进行新单删除操作,然后在重新做卡折结算操作!");
	    	    		return false;
	    	    	}else if(arrResult[0][0] == '04'){
	    	    		alert("单证号["+tCardNo+"]已经做过结算,并已签单，不可删除!");
	    	    		return false;
	    	    	}
	    	     } 	   
		   }
		  
	   }
	    if (confirm("您确定要删除选中的批次吗？")) {
	    	fm.action = "./CertifyImportBatchAllDelSave.jsp";
            fm.submit();
           }
}   

/**
 * 卡折删除。
 * 
 */
  function delImportBatch(){
	var checkedRowNum=0;  
	var rowNum=CertifyGrid.mulLineCount;
	  var count = -1;
	    for(var i=0;i<rowNum;i++){
	    	if(CertifyGrid.getChkNo(i))
	        {
			    		count = i;
			    		checkedRowNum=checkedRowNum+1;
				    	    var tRowDatas = CertifyGrid.getRowData(count);
				    	    //var tBatchNo = tRowDatas[0];
				    	    var tManagecom = tRowDatas[1];
				    	    var tCardNo = tRowDatas[2];
				    	    var strSQL="select state from licertify where cardno = '"+tCardNo+"'";
				    	    var arrResult=easyExecSql(strSQL);
				    	    if(arrResult)
				    	    {   
				    	    	if(arrResult[0][0] == '02'){
				    	    		alert("单证号["+tCardNo+"]已加入结算单,如果想删除,请去卡折结算中进行从结算单中取消操作,然后再进行卡折删除操作!");
				    	    		return false;
				    	    	}else if(arrResult[0][0] == '03'){
				    	    		alert("单证号["+tCardNo+"]已经做过结算,如果想删除,请去团体保单中进行新单删除操作,然后在重新做卡折结算操作!");
				    	    		return false;
				    	    	}else if(arrResult[0][0] == '04'){
				    	    		alert("单证号["+tCardNo+"]已经做过结算,并已签单，不可删除!");
				    	    		return false;
				    	    	}
				    	     } 	   
	             }
	    	
	    } 
	    
	    if(checkedRowNum==0){
	    	alert("请选择一条记录");
	    	return false;
	    }
	    if (confirm("您确定要删除选中的批次吗？")) {
	    	fm.action = "./CertifyImportBatchDelSave.jsp";
            fm.submit();
           }

  }
/*
function delImportBatch()
{  
	
    var tRow = CertifyGrid.getSelNo() - 1;  单选框
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    var tRowDatas = CertifyGrid.getRowData(tRow);
    var tBatchNo = tRowDatas[0];
    var tManagecom = tRowDatas[1];
    var tCardNo = tRowDatas[2];
    var strSQL="select state from licertify where cardno = '"+tCardNo+"'";
    var arrResult=easyExecSql(strSQL);
    
    if(arrResult)
    {
    	if(arrResult[0][0] == '02'){
    		alert("单证号["+tCardNo+"]已加入结算单,如果想删除,请去卡折结算中进行从结算单中取消操作,然后再进行卡折删除操作!");
    		return false;
    	}else if(arrResult[0][0] == '03'){
    		alert("单证号["+tCardNo+"]已经做过结算,如果想删除,请去团体保单中进行新单删除操作,然后在重新做卡折结算操作!");
    		return false;
    	}else if(arrResult[0][0] == '04'){
    		alert("单证号["+tCardNo+"]已经做过结算,并已签单，不可删除!");
    		return false;
    	}
    }
    
    if (confirm("您确定要删除选中的批次吗？")) {
    	fm.action = "./CertifyImportBatchDelSave.jsp"
			        + "?BatchNo=" + tBatchNo
			        + "&CardNo=" + tCardNo;

    	fm.submit();
    }
}
*/
/**
 * 显示成功或失败页面
 */
function afterSubmit(flag, content)
{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	queryCertify();
}