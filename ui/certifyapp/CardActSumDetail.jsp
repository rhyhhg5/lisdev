<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="CardActSumDetail.js"></SCRIPT>   
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
</head>

<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate();   
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
  String AheadDays="30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  String SubDate=tD.getString(AfterDate);  
  String tSubYear=StrTool.getVisaYear(SubDate);
  String tSubMonth=StrTool.getVisaMonth(SubDate);
  String tSubDate=StrTool.getVisaDay(SubDate);               	               	
%>

<SCRIPT>
  var CurrentYear=<%=tCurrentYear%>;  
  var CurrentMonth=<%=tCurrentMonth%>;  
  var CurrentDate=<%=tCurrentDate%>;
  var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
  var SubYear=<%=tSubYear%>;  
  var SubMonth=<%=tSubMonth%>;  
  var SubDate=<%=tSubDate%>;
  var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
  var managecom = <%=tGI.ManageCom%>;
  var operator = '<%=tGI.Operator%>';
  var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
  var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
</SCRIPT> 

<body onload="initElementtype();initForm();">
	<form action="" method=post name=fm target="fraSubmit">
		<table class = "common">
			<tr class = "common">
				<td class="title">管理机构</td>
            	<td class="input">
            		<input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
            	</td>
            	<td class="title">查询日期起</td>
	            <td class="input">
	                <input class="coolDatePicker" dateFormat="short" name="StartMakeDate" />
	            </td>
	            <td class="title">查询日期止</td>
	            <td class="input">
	                <input class="coolDatePicker" dateFormat="short" name="EndMakeDate" />
	            </td>
			</tr>
			<tr class = "common">
				<td class="title">业务类型</td>
	            <td class="input">
	                <input class="codeNo" name="CardOperateType" ondblclick="return showCodeList('cardoperatetype',[this,CardOperateTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('cardoperatetype',[this,CardOperateTypeName],[0,1],null,null,null,1);" /><input class="codename" name="CardOperateTypeName" readonly="readonly" />
	            </td>
	            <td class="title">保费类型</td>
	            <td class="input">
	                <input class="codeNo" name="CardPremType" verify="类型|NOTNULL" CodeData="0|^0|承保保费^1|结算保费" ondblclick="return showCodeListEx('PremTypeList',[this,CardPremTypeName],[0,1]);" /><input class="codename" name="CardPremTypeName" readonly="readonly" />
	            </td>
			</tr>
		</table>
		
		<br />
		
    	<input value="下  载" class="cssButton" type="button" onclick="submitForm();" />
    	
    	<hr />
    	
    	<div>
        <font color="red">
            <ul>报表说明：
	            <li><b>清单中不包含查询日期为当天的数据。</b></li>
                <li>按照查询日期为口径的激活卡汇总清单报表。</li>
                <li>激活卡业务-查询日期统计口径：这里指客户从电子商务激活的日期。</li>
                <li>B2B业务-查询日期统计口径：这里指从电子商务售出的日期。</li>
            </ul>
            <ul>名词解释：
                <li>暂无</li>
            </ul>
            <ul>统计条件：
                <li>管理机构必须填写。</li>
                <li>查询日期段间隔不超过三个月。</li>
            </ul>
        </font>
    </div>
    	
    	<input name="querySql" type="hidden" />
    	<input type="hidden" name=op value="" />
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>