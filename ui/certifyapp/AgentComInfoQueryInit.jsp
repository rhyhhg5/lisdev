<%
//程序名称：
//程序功能：
//创建日期：2008-12-19
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>
     
<script language="JavaScript">

function initInpBox()
{ 
  try
  {                           
    fm.all('AgentCom').value = '';
    fm.all('Name').value = '';
    fm.all('ACType').value = '';
    fm.all('Corporation').value = '';
    fm.all('ChiefBusiness').value = '';
    fm.all('SellFlag').value = '';
    fm.all('BankAccNo').value = '';
    fm.all('BranchType').value = '';
    fm.all('BranchType2').value = '';
    
    fm.all('ManageCom').value = <%=strManageCom%>;
  }
  catch(ex)
  {
    alert("在InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();   
    initComGrid();
    
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化ComGrid
 ************************************************************
 */
function initComGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="专业代理机构编码";         //列名
    iArray[1][1]="120px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="专业代理机构名称";         //列名
    iArray[2][1]="200px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="负责管理机构";         //列名
    iArray[3][1]="80px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="上级代理机构";         //列名
    iArray[4][1]="0px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=3;         //是否允许录入，0--不能，1--允许
        
    iArray[5]=new Array();
    iArray[5][0]="中介专员代码";         //列名
    iArray[5][1]="80px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="中介专员名称";         //列名
    iArray[6][1]="80px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="销售资格";         //列名
    iArray[7][1]="60px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="是否停业";         //列名
    iArray[8][1]="60px";         //宽度
    iArray[8][2]=100;         //最大长度
    iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
 
    ComGrid = new MulLineEnter( "fm" , "ComGrid" ); 

    //这些属性必须在loadMulLine前
    ComGrid.mulLineCount = 0;
    ComGrid.hiddenPlus = 1;
    ComGrid.hiddenSubtraction = 1;
    ComGrid.displayTitle = 1;
    ComGrid.canSel=0;
    ComGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
        alert("初始化ComGrid时出错："+ ex);
  }
}

</script>