<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.certifybusiness.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.utility.*" %>
<%@page import="com.sinosoft.lis.schema.LCCoInsuranceParamSchema" %>
<%@page import="com.sinosoft.lis.vschema.LCCoInsuranceParamSet" %>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%
System.out.println("CertifyContSettlementSave.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

CErrors tError = null;

try
{
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    String tOperatorFlag = request.getParameter("fmOperatorFlag");
    
    String tMissionId = request.getParameter("MissionId");
    String tSubMissionId = request.getParameter("SubMissionId");
    String tProcessId = request.getParameter("ProcessId");
    String tActivityId = request.getParameter("ActivityId");
    String tActivityStatus = request.getParameter("ActivityStatus");
    
    
    String tPrtNo = request.getParameter("PrtNo");
    String tManageCom = request.getParameter("GrpContManageCom");
    String tCertifyCode = request.getParameter("CertifyCode");
    String tSaleChnl = request.getParameter("GrpContSaleChnl");
    String tAgentCode = request.getParameter("GrpContAgentCode");
    String tAgentCom = request.getParameter("GrpContAgentCom");
    String tPayMode = request.getParameter("PayMode");
    String tPayIntv = request.getParameter("PayIntv");
    String tCValidate = request.getParameter("CValidate");
    String tCInValidate = request.getParameter("CInValidate");
    
    String tAppntName = request.getParameter("AppntName");
    tAppntName = StrTool.GBKToUnicode(tAppntName);
    
    // 集团交叉业务
    String tCrs_SaleChnl = request.getParameter("Crs_SaleChnl_KZ");
    String tCrs_BussType = request.getParameter("Crs_BussType_KZ");
    String tGrpAgentCom = request.getParameter("GrpAgentCom_KZ");
    String tGrpAgentCode = request.getParameter("GrpAgentCode_KZ");
    String tGrpAgentIDNo = request.getParameter("GrpAgentIDNo_KZ");
    
    String tGrpAgentName = request.getParameter("GrpAgentName_KZ");
    String Flag = request.getParameter("Flag");
    String ZXFlag = request.getParameter("ZXFlag");
    String ZXAgentCode = request.getParameter("ZXAgentCode");
    System.out.println("中介转直销标识："+ZXFlag+",业务员编码："+ZXAgentCode);
    if("1".equals(ZXFlag)){//中介转直销
    	tSaleChnl = "02";
    	tAgentCode = ZXAgentCode;
    	tAgentCom = "";
    }
    
    
    tGrpAgentName = StrTool.GBKToUnicode(tGrpAgentName);
    // --------------------
    
    
    TransferData tTransferData = new TransferData();
    
    tTransferData.setNameAndValue("PrtNo", tPrtNo);
    tTransferData.setNameAndValue("ManageCom", tManageCom);
    tTransferData.setNameAndValue("CertifyCode", tCertifyCode);
    tTransferData.setNameAndValue("SaleChnl", tSaleChnl);
    tTransferData.setNameAndValue("AgentCode", tAgentCode);
    tTransferData.setNameAndValue("AgentCom", tAgentCom);
    tTransferData.setNameAndValue("PayMode", tPayMode);
    tTransferData.setNameAndValue("PayIntv", tPayIntv);
    tTransferData.setNameAndValue("CValidate", tCValidate);
    tTransferData.setNameAndValue("CInValidate", tCInValidate);
    
    tTransferData.setNameAndValue("AppntName", tAppntName);
    
    tTransferData.setNameAndValue("MissionID", tMissionId);
    tTransferData.setNameAndValue("SubMissionID", tSubMissionId);
    
    tTransferData.setNameAndValue("Crs_SaleChnl", tCrs_SaleChnl);
    tTransferData.setNameAndValue("Crs_BussType", tCrs_BussType);
    tTransferData.setNameAndValue("GrpAgentCom", tGrpAgentCom);
    tTransferData.setNameAndValue("GrpAgentCode", tGrpAgentCode);
    tTransferData.setNameAndValue("GrpAgentName", tGrpAgentName);
    tTransferData.setNameAndValue("GrpAgentIDNo", tGrpAgentIDNo);
    tTransferData.setNameAndValue("Flag", Flag);
    LCCoInsuranceParamSet tLCCoInsuranceParamSet = new LCCoInsuranceParamSet();
    if(!"".equals(Flag)&&null!= Flag)
    {
        if(Flag.equals("1")||Flag == "1")
        {
            String tRows[] = request.getParameterValues("CoInsuranceParamGridNo");
		    String tCIAgentComCodes[] = request.getParameterValues("CoInsuranceParamGrid1");
		    String tCIAgentComNames[] = request.getParameterValues("CoInsuranceParamGrid2");
		    String tCIRates[] = request.getParameterValues("CoInsuranceParamGrid3");
		    
		    for(int i = 0; i < tRows.length; i++)
		    {
		        LCCoInsuranceParamSchema tTmpLCCoInsuranceParamSchema = new LCCoInsuranceParamSchema();
		        tTmpLCCoInsuranceParamSchema.setPrtNo(tPrtNo);
		        
		        tTmpLCCoInsuranceParamSchema.setAgentCom(tCIAgentComCodes[i]);
		        tTmpLCCoInsuranceParamSchema.setAgentComName(tCIAgentComNames[i]);
		        tTmpLCCoInsuranceParamSchema.setRate(tCIRates[i]);

		        tLCCoInsuranceParamSet.add(tTmpLCCoInsuranceParamSchema);
		    }
        }
    }
    
    VData tVData = new VData();
    tVData.add(tG);
    tVData.add(tTransferData);
    tVData.add(tLCCoInsuranceParamSet);
    
    //by gzh 20130408 增加对综合开拓数据的处理
  	String tAssistSaleChnl = request.getParameter("AssistSaleChnl");
  	String tAssistAgentCode = request.getParameter("AssistAgentCode");
	LCExtendSchema tLCExtendSchema = new LCExtendSchema();
	tLCExtendSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCExtendSchema.setAssistSalechnl(tAssistSaleChnl);
	tLCExtendSchema.setAssistAgentCode(tAssistAgentCode);
	tVData.add(tLCExtendSchema);
  
  //by gzh 20130408 增加对综合开拓数据的处理 end

    
    //CertifyGrpContUI tCertifyGrpContUI = new CertifyGrpContUI();
    
    GrpTbWorkFlowUI tTbWorkFlowUI = new GrpTbWorkFlowUI();
    //boolean bl= tTbWorkFlowUI.submitData( tVData, tActivityId);
    
    //if (!tCertifyGrpContUI.submitData(tVData, tOperatorFlag))
    if (!tTbWorkFlowUI.submitData( tVData, "0000011001"))
    {
        Content = " 结算单保存失败! 原因是: " + tTbWorkFlowUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 结算单保存成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 结算单保存失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("CertifyContSettlementSave.jsp End ...");
%>

<html>
<script language="javascript">
    parent.fraInterface.afterCreateGrpContSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
