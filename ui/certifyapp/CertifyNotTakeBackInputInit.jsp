<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initCertifyGrid();
	divPage.style.display="";                 
}
var CertifyGrid;
function initCertifyGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         	//列名
    iArray[0][1]="30px";         	//列名    
    iArray[0][3]=0;         		//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="管理机构";         	 //列名
    iArray[1][1]="60px";            //列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="单证编码";         	
    iArray[2][1]="60px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="单证名称";         	  //列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="单证下发时间";      //列名
    iArray[4][1]="60px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
     
    iArray[5]=new Array();
    iArray[5][0]="起始号";      //列名
    iArray[5][1]="60px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
     
    iArray[6]=new Array();
    iArray[6][0]="终止号";      //列名
    iArray[6][1]="60px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
     
    iArray[7]=new Array();
    iArray[7][0]="领用人";      //列名
    iArray[7][1]="60px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[8]=new Array();
    iArray[8][0]="目前单证所在管理机构";      //列名
    iArray[8][1]="80px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="状态";      //列名
    iArray[9][1]="80px";            	//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
    CertifyGrid = new MulLineEnter("fm", "CertifyGrid"); 
    //设置Grid属性
    CertifyGrid.mulLineCount = 0;
    CertifyGrid.displayTitle = 1;
    CertifyGrid.locked = 1;
    CertifyGrid.canSel = 0;
    CertifyGrid.canChk = 0;
    CertifyGrid.hiddenSubtraction = 1;
    CertifyGrid.hiddenPlus = 1;
    CertifyGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
                                      

</script>
