//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询事件
function easyQueryClick()
{
    if(!verifyInput())
        return false;
    
    if(dateDiff(fm.startSignDate.value, fm.endSignDate.value, "M") > 3)
    {
        alert("统计期最多为三个月！");
        return false;
    }
    
    var strSQL = "select a.ManageCom, (select Name from LDCom c where a.ManageCom = c.ComCode), "
        + "    b.CertifyCode, b.CertifyName, count(1), sum(Peoples2), sum(a.Prem) "
        + "from LCGrpCont a, LMCertifyDes b "
        + "where a.CardFlag = '2' and a.ManageCom like '" + comCode + "%'"
        + "    and a.SignDate between '" + fm.startSignDate.value + "' and '" + fm.endSignDate.value + "' "
        + "    and b.CertifyClass = 'D' "
        + "    and exists (select 1 from LICertify c where a.PrtNo = c.PrtNo and b.CertifyCode = c.CertifyCode) "
        + getWherePart('a.ManageCom', 'manageCom', 'like')
        + getWherePart('b.CertifyCode', 'certifyCode')
        + getWherePart('b.operatetype', 'cardType')
        + "group by a.ManageCom, b.CertifyCode, b.CertifyName with ur"
        ;

    turnPage.queryModal(strSQL, CertifyContQueryGrid);

    fm.querySql.value = strSQL;
}

//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && CertifyContQueryGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "CertifyContQueryDownload.jsp";
        fm.target = "_blank";
        fm.submit();
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
