//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


/**
 * 提交表单
 */
function submitForm()
{
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}


/**
 * 提交后操作,服务器数据返回后执行的操作
 */
function afterSubmit(FlagStr, content)
{
    window.focus();
    showInfo.close();
    
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
    showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    if (FlagStr == "Succ")
    {
        alert("Succ");
    }
}

/**
 * 申请单证导入批次号。
 */
function applyNewBatchNo()
{
    fmImport.btnBatchNoApply.disabled = true;
    
    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fmImport.action = "./CertifyBatchImportApply.jsp";
    fmImport.submit();

    fmImport.action = "";
}

/**
 * 申请提交后动作。
 */
function afterApplyNewBatchNoSubmit(FlagStr, Content, cBatchNo)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("申请批次失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    initBatchNoInput(cBatchNo);
    fmImport.btnBatchNoApply.disabled = false;
}

/**
 * 初始化批次号。
 */
function initBatchNoInput(cBatchNo)
{    
    if(cBatchNo != null && cBatchNo != "")
    {
        fmImport.BatchNo.value = cBatchNo;
    }
    else
    {
        fmImport.BatchNo.value = "";
    }
}

/**
 * 导入清单。
 */
function importCertifyList()
{
    fmImport.btnImport.disabled = true;
    
    var tBatchNo = fmImport.BatchNo.value;
    
    var showStr = "正在导入清单数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fmImport.action = "./CertifyListImportSave.jsp?BatchNo=" + tBatchNo;
    fmImport.submit();

    fmImport.action = "";
}

/**
 * 导入清单提交后动作。
 */
function afterImportCertifyList(FlagStr, Content, cBatchNo)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("导入批次失败，请尝试重新进行申请。");
        
        fm.ImportErrBatchNo.value = cBatchNo;
        queryImportErrLog();
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        fm.ImportBatchNo.value = cBatchNo;
        queryBatchList();
    }
    
    fmImport.btnImport.disabled = false;
}

function queryBatchList()
{
    var tBatchNo = fm.ImportBatchNo.value;
    
    var tStrSql = ""
        + " select "
        + " lict.BatchNo, lict.SaleChnl, lict.AgentCom, getUniteCode(lict.AgentCode), lict.AgentName, "
        + " lict.CardNo, lict.ManageCom, lict.CertifyCode, "
        + " lict.Prem, lict.Amnt, lict.Mult, lict.Copys, "
        + " lict.CValidate, "
        + " licti.Name, varchar(licti.Sex), varchar(licti.Birthday), licti.IdType, licti.IdNo, "
        + " varchar(lict.MakeDate) || ' ' || lict.MakeTime MakeDate, "
        + " CodeName('cardstatus', lict.CertifyStatus) "
        + " from LICertify lict "
        + " left join LICertifyInsured licti on licti.CardNo = lict.CardNo "
        + " where 1 = 1 "
        + " and lict.BatchNo = '" + tBatchNo + "' "
        + " and lict.State = '00' "
        + " union all "
        + " select "
        + " libct.BatchNo, libct.SaleChnl, libct.AgentCom, getUniteCode(libct.AgentCode), libct.AgentName, "
        + " libct.CardNo, libct.ManageCom, libct.CertifyCode, "
        + " libct.Prem, libct.Amnt, libct.Mult, libct.Copys, "
        + " libct.CValidate, "
        + " '' Name, '' Sex, '' Birthday, '' IdType, '' IdNo, "
        + " varchar(libct.MakeDate) || ' ' || libct.MakeTime MakeDate, "
        + " CodeName('cardstatus', libct.CertifyStatus) "
        + " from LIBCertify libct "
        + " where 1 = 1 "
        + " and libct.BatchNo = '" + tBatchNo + "' "
        + " and libct.State = '00' "
        + " order by BatchNo, CardNo "
        ;

    turnPage1.pageDivName = "divImportBatchGridPage";
    turnPage1.queryModal(tStrSql, ImportBatchGrid);

    if (!turnPage1.strQueryResult)
    {
        fm.btnImportConfirm.disabled = true;
        fm.btnImportDelete.disabled = true;
        alert("没有该批次导入信息！");
        return false;
    }
    queryBatchInfo();
    
    return true;
}


function queryBatchInfo()
{
    var tBatchNo = fm.ImportBatchNo.value;
    
    var tStrSql = ""
        + " select "
        + " tmp.BatchNo, count(distinct tmp.CardNo), sum(tmp.Prem), "
        + " sum(tmp.WTCertFlag), sum(tmp.WTSumPrem) "
        + " from "
        + " ( "
        + " select info.BatchNo, info.CardNo, info.Prem, "
        + " (case info.CertFlag when 'WT' then 1 else 0 end) WTCertFlag, "
        + " (case info.CertFlag when 'WT' then info.Prem else 0 end) WTSumPrem "
        + " from "
        + " ("
        + " select lict.BatchNo, lict.CardNo, nvl(lict.Prem, 0) Prem, "
        + " 'CB' CertFlag "
        + " from LICertify lict "
        + " where 1 = 1 "
        + " and lict.BatchNo = '" + tBatchNo + "' "
        + " union all "
        + " select libct.BatchNo, ('WT_' || libct.CardNo) CardNo, (-1 * nvl(libct.Prem, 0)) Prem, "
        + " 'WT' CertFlag "
        + " from LIBCertify libct "
        + " where 1 = 1 "
        + " and libct.BatchNo = '" + tBatchNo + "' "
        + " ) as info "
        + " ) as tmp "
        + " group by tmp.BatchNo "
        ;
    
    var arr = easyExecSql(tStrSql);
    if(arr)
    {
        fm.ConfirmBatchNo.value = arr[0][0];
        fm.SumImportBatchCount.value = arr[0][1];
        fm.SumImportBatchPrem.value = arr[0][2];
        fm.SumImportWTCount.value = arr[0][3];
        fm.SumImportWTPrem.value = arr[0][4];
        
        fm.btnImportConfirm.disabled = false;
        fm.btnImportDelete.disabled = false;
    }
    else
    {
        fm.btnImportConfirm.disabled = true;
        fm.btnImportDelete.disabled = true;
        
        fm.ConfirmBatchNo.value = "";
        fm.SumImportBatchCount.value = "";
        fm.SumImportBatchPrem.value = "";
        fm.SumImportWTCount.value = "";
        fm.SumImportWTPrem.value = "";
    }
}

/**
 * 批次导入确认。
 */
function confirmImport()
{
    fm.btnImportConfirm.disabled = true;
    fm.btnImportDelete.disabled = true;
    
    var showStr = "正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.fmOperatorFlag.value = "ImportConfirm";
    fm.action = "./CertifyListImportConfirmSave.jsp";
    fm.submit();

    fm.fmOperatorFlag.value = "";
    fm.action = "";
}

/**
 * 批次导入删除。
 */
function delImport()
{
    fm.btnImportConfirm.disabled = true;
    fm.btnImportDelete.disabled = true;
    
    var showStr = "正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.fmOperatorFlag.value = "ImportDelete";
    fm.action = "./CertifyListImportConfirmSave.jsp";
    fm.submit();

    fm.fmOperatorFlag.value = "";
    fm.action = "";
}


/**
 * 批次导入确认后动作。
 */
function afterConfirmImport(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("导入批次失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        fm.ImportBatchNo.value = "";
        
        fm.ConfirmBatchNo.value = "";
        fm.SumImportBatchCount.value = "";
        fm.SumImportBatchPrem.value = "";
        fm.SumImportWTCount.value = "";
        fm.SumImportWTPrem.value = "";
    }
    
    queryBatchList();
}


function queryImportErrLog()
{
    var tBatchNo = fm.ImportErrBatchNo.value;
    
    var tStrSql = ""
        + " select licil.BatchNo, licil.CardNo, licil.ErrorInfo, varchar(licil.MakeDate) || ' '|| licil.MakeTime "
        + " from LICertifyImportLog licil "
        + " where 1 = 1 "
        + " and licil.BatchNo = '" + tBatchNo + "' "
        + " and licil.ErrorType = '1' "
        + " and not exists (select 1 from LICertifyImportLog where BatchNo = '" + tBatchNo + "' and ErrorType = 'E') "
        + " order by varchar(licil.MakeDate) || ' '|| licil.MakeTime desc, licil.CardNo "
        ;

    turnPage2.pageDivName = "divImportErrLogGridPage";
    turnPage2.queryModal(tStrSql, ImportErrLogGrid);

    if (!turnPage2.strQueryResult)
    {
        alert("没有该批次导入报错信息！");
        return false;
    }
    
    return true;
}


function viewChkWnd()
{
    var cUrl = "./CertifyChkErrMain.jsp?BatchNo=" + fm.ConfirmBatchNo.value;
    window.open(cUrl, "window1");
}


function goBack()
{
    window.location.replace("./CertifyImportBatchApply.jsp");
}

