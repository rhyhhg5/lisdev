//程序名称：
//程序功能：
//创建日期：2009-11-12
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 卡激活日清单。
 */
function queryCardActiveInfoList()
{
    if(fm.CardNo.value == "")
    {
    	alert("请输入卡号!!");
        return false;
    }
    
    var cCardNo = fm.CardNo.value;

    var tStrSql = ""
        + " select licail.CardNo, licail.CValidate, "
        + " licail.SequenceNo, licail.Name,case licail.Sex when '0' then '男' when '1' then '女' end, licail.Birthday, "
        + " licail.ActiveDate, CodeName('cardactdealstate', licail.DealFlag) DealFlag, licail.DealDate, licail.State "
        + " from LICardActiveInfoList licail "
        + " where 1 = 1 "
        + " and licail.CardNo = '" + cCardNo + "' and dealflag != '00' "
        + " and (batchno is null or trim(batchno) = '') "
        + " and cardno in(select cardno from licertify where wsstate = '00')"
        ;
    
    turnPage1.pageDivName = "divCardActiveInfoListGridPage";
    turnPage1.queryModal(tStrSql, CardActiveInfoListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有该日网站激活卡单信息！");
        return false;
    }
    
    return true;
}

/**
 * 激活处理。
 */
function submitData()
{    
	var tRow = CardActiveInfoListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }    
    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.action = "./CardActiveRenewSave.jsp";
    fm.submit();
}


function afterImportSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("先激活卡补处理失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        queryCardActiveInfoList();
        var strSQL =  " select activedate from licardactiveinfolist where CardNo='" + fm.all( 'CardNo' ).value + "'";
  		var arrResult = easyExecSql(strSQL);
  		if (arrResult != null) {
  		      alert("请去菜单:承保处理--->卡式业务--->批量卡激活处理重新进行激活,日期请选择:["+arrResult[0][0]+"]");
  		}
    }
}