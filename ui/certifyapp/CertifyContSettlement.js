//程序名称：
//程序功能：
//创建日期：2008-11-16
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


/**
 * 查询待结算单证。
 */
function queryAvailableCertifyContList()
{
    if(!verifyInput2())
    {
        return false;
    }
    
    // 校验中介渠道，必须填写中介机构代码。
    var tSaleChnl = fm.SaleChnl.value;
    if(tSaleChnl == '03' || tSaleChnl == '10' || tSaleChnl == '15' || tSaleChnl == '20')
    {
        var tTmpAgentCom = fm.AgentCom.value;
        if(tTmpAgentCom == null || tTmpAgentCom == "")
        {
            alert("中介渠道，必须填写中介机构代码。");
            return false;
        }
    }
    // --------------------
    
    if(fm.MixComFlag.checked == true)
    {
        if(fm.Crs_SaleChnl.value == "")
        {
            alert("如果是集团交叉,交叉销售渠道不能为空,请填写!");
            fm.all('Crs_SaleChnl').focus();
            return false;
        }
        if(fm.Crs_BussType.value == "")
        {
            alert("如果是集团交叉,交叉销售业务类型不能为空,请填写!");
            fm.all('Crs_BussType').focus();
            return false;
        }
        if(fm.GrpAgentCom.value == "")
        {
            alert("如果是集团交叉,对方机构代码不能为空,请填写!");
            fm.all('GrpAgentCom').focus();
            return false;
        }
        if(fm.GrpAgentCode.value == "")
        {
            alert("如果是集团交叉,对方业务员代码不能为空,请填写!");
            fm.all('GrpAgentCode').focus();
            return false;
        }
    }
    
    var tManageCom = fm.ManageCom.value;
    
    if(fm.MixComFlag.checked == false) 
    {
        var tStrSql = ""
        + " select lict.CardNo, lict.CertifyCode, licti.Name, licti.IdNo, lict.Prem, lict.CertifyStatus, CodeName('cardstatus', lict.CertifyStatus) CertifyStatus "
        + " from LICertify lict "
        + " left join LICertifyInsured licti on lict.CardNo = licti.CardNo and licti.SequenceNo = '1' "
        + " where 1 = 1 "
        + " and lict.ManageCom = '" + tManageCom + "' "
        + " and lict.PrtNo is null "
        + " and lict.State = '01' "
        + " and lict.Crs_SaleChnl is null "
        + " and lict.Crs_BussType is null "
        + " and lict.GrpAgentCom is null "
        + " and lict.GrpAgentCode is null "
        + " and lict.GrpAgentName is null "
        + " and lict.GrpAgentIDNo is null "
        + getWherePart("lict.SaleChnl", "SaleChnl")
        + getWherePart("lict.CertifyCode", "CertifyTypeCode")
        // + getWherePart("lict.AgentCode", "AgentCode")
        // + getWherePart("lict.AgentCom", "AgentCom")
        + getWherePart("lict.BatchNo", "BatchNo")
        + getWherePart("lict.CardNo", "StartCardNo", ">=")
        + getWherePart("lict.CardNo", "EndCardNo", "<=")
        + getWherePart("lict.CValidate", "StartCValidate", ">=")
        + getWherePart("lict.CValidate", "EndCValidate", "<=")
        ;
        
	    var tAgentCom = fm.AgentCom.value;
	    if(tAgentCom != null && tAgentCom != "")
	    {
	        tStrSql += getWherePart("lict.AgentCom", "AgentCom");
	    }
	    else
	    {
	        tStrSql += getWherePart("lict.AgentCode", "AgentCode");
	    }
        
        tStrSql += " union all "
        + " select lict.CardNo, lict.CertifyCode, '' Name, '' IdNo, lict.Prem, lict.CertifyStatus, CodeName('cardstatus', lict.CertifyStatus) CertifyStatus "
        + " from LIBCertify lict "
        + " where 1 = 1 "
        + " and lict.ManageCom = '" + tManageCom + "' "
        + " and lict.PrtNo is null "
        + " and lict.State = '01' "
        + " and lict.Crs_SaleChnl is null "
        + " and lict.Crs_BussType is null "
        + " and lict.GrpAgentCom is null "
        + " and lict.GrpAgentCode is null "
        + " and lict.GrpAgentName is null "
        + " and lict.GrpAgentIDNo is null "
        + getWherePart("lict.SaleChnl", "SaleChnl")
        + getWherePart("lict.CertifyCode", "CertifyTypeCode")
        // + getWherePart("lict.AgentCode", "AgentCode")
        // + getWherePart("lict.AgentCom", "AgentCom")
        + getWherePart("lict.BatchNo", "BatchNo")
        + getWherePart("lict.CardNo", "StartCardNo", ">=")
        + getWherePart("lict.CardNo", "EndCardNo", "<=")
        + getWherePart("lict.CValidate", "StartCValidate", ">=")
        + getWherePart("lict.CValidate", "EndCValidate", "<=")
        ;
        
        if(tAgentCom != null && tAgentCom != "")
        {
            tStrSql += getWherePart("lict.AgentCom", "AgentCom");
        }
        else
        {
            tStrSql += getWherePart("lict.AgentCode", "AgentCode");
        }
        
        tStrSql += " order by CardNo ";
    }
    
    if(fm.MixComFlag.checked == true) 
    {
        var tStrSql = ""
        + " select lict.CardNo, lict.CertifyCode, licti.Name, licti.IdNo, lict.Prem, lict.CertifyStatus, CodeName('cardstatus', lict.CertifyStatus) CertifyStatus "
        + " from LICertify lict "
        + " left join LICertifyInsured licti on lict.CardNo = licti.CardNo and licti.SequenceNo = '1' "
        + " where 1 = 1 "
        + " and lict.ManageCom = '" + tManageCom + "' "
        + " and lict.PrtNo is null "
        + " and lict.State = '01' "
        + getWherePart("lict.SaleChnl", "SaleChnl")
        + getWherePart("lict.Crs_SaleChnl", "Crs_SaleChnl")
        + getWherePart("lict.Crs_BussType", "Crs_BussType")
        + getWherePart("lict.GrpAgentCom", "GrpAgentCom")
        + getWherePart("lict.GrpAgentCode", "GrpAgentCode")
        + getWherePart("lict.GrpAgentName", "GrpAgentName")
        + getWherePart("lict.GrpAgentIDNo", "GrpAgentIDNo")
        + getWherePart("lict.CertifyCode", "CertifyTypeCode")
        // + getWherePart("lict.AgentCode", "AgentCode")
        // + getWherePart("lict.AgentCom", "AgentCom")
        + getWherePart("lict.BatchNo", "BatchNo")
        + getWherePart("lict.CardNo", "StartCardNo", ">=")
        + getWherePart("lict.CardNo", "EndCardNo", "<=")
        + getWherePart("lict.CValidate", "StartCValidate", ">=")
        + getWherePart("lict.CValidate", "EndCValidate", "<=")
        ;
        
	    var tAgentCom = fm.AgentCom.value;
	    if(tAgentCom != null && tAgentCom != "")
	    {
	        tStrSql += getWherePart("lict.AgentCom", "AgentCom");
	    }
	    else
	    {
	        tStrSql += getWherePart("lict.AgentCode", "AgentCode");
	    }
        
        tStrSql += " union all "
        + " select lict.CardNo, lict.CertifyCode, '' Name, '' IdNo, lict.Prem, lict.CertifyStatus, CodeName('cardstatus', lict.CertifyStatus) CertifyStatus "
        + " from LIBCertify lict "
        + " where 1 = 1 "
        + " and lict.ManageCom = '" + tManageCom + "' "
        + " and lict.PrtNo is null "
        + " and lict.State = '01' "
        + getWherePart("lict.SaleChnl", "SaleChnl")
        + getWherePart("lict.Crs_SaleChnl", "Crs_SaleChnl")
        + getWherePart("lict.Crs_BussType", "Crs_BussType")
        + getWherePart("lict.GrpAgentCom", "GrpAgentCom")
        + getWherePart("lict.GrpAgentCode", "GrpAgentCode")
        + getWherePart("lict.GrpAgentName", "GrpAgentName")
        + getWherePart("lict.GrpAgentIDNo", "GrpAgentIDNo")
        + getWherePart("lict.CertifyCode", "CertifyTypeCode")
        // + getWherePart("lict.AgentCode", "AgentCode")
        // + getWherePart("lict.AgentCom", "AgentCom")
        + getWherePart("lict.BatchNo", "BatchNo")
        + getWherePart("lict.CardNo", "StartCardNo", ">=")
        + getWherePart("lict.CardNo", "EndCardNo", "<=")
        + getWherePart("lict.CValidate", "StartCValidate", ">=")
        + getWherePart("lict.CValidate", "EndCValidate", "<=")
        ;
        
        if(tAgentCom != null && tAgentCom != "")
        {
            tStrSql += getWherePart("lict.AgentCom", "AgentCom");
        }
        else
        {
            tStrSql += getWherePart("lict.AgentCode", "AgentCode");
        }
        
        tStrSql += " order by CardNo ";
    }
    
    fm.querySql.value = tStrSql;
//prompt('',tStrSql);
    turnPage1.pageLineNum = 100;
    turnPage1.pageDivName = "divAvailableCertifyListGridPage";
    turnPage1.queryModal(tStrSql, AvailableCertifyListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        fm.btnAddCertifyCont.disabled = true;
        fm.btnAddAllCertifyCont.disabled = true;
        return false;
    }
    else
    {
        fm.btnAddCertifyCont.disabled = false;        
        fm.btnAddAllCertifyCont.disabled = false;
    }
    
    return true;
}

/**
 * 查看结算单下所有单证。
 */
function querySettlementCertifyContList()
{
    var tPrtNo = fm.PrtNo.value;
    
    var tStrSql = ""
        + " select lict.CardNo, lict.CertifyCode, licti.Name, licti.IdNo, lict.Prem, lict.CertifyStatus, CodeName('cardstatus', lict.CertifyStatus) CertifyStatus "
        + " from LICertify lict "
        + " left join LICertifyInsured licti on lict.CardNo = licti.CardNo and licti.SequenceNo = '1' "
        + " where 1 = 1 "
        + " and lict.PrtNo = '" + tPrtNo + "' "
        + " union all "
        + " select lict.CardNo, lict.CertifyCode, '' Name, '' IdNo, lict.Prem, lict.CertifyStatus, CodeName('cardstatus', lict.CertifyStatus) CertifyStatus "
        + " from LIBCertify lict "
        + " where 1 = 1 "
        + " and lict.PrtNo = '" + tPrtNo + "' "
        ;

    turnPage2.pageDivName = "divSettlementCertifyContListGridPage";
    turnPage2.queryModal(tStrSql, SettlementCertifyContListGrid);

    if (!turnPage2.strQueryResult)
    {   
        fm.btnDelCertifyCont.disabled = true;
        fm.btnCreateGrpCont.disabled = true;
        
        fm.ManageCom.value = "";
        fm.ManageComName.value = "";
        document.getElementById("ManageCom").disabled = false;
        
        fm.CertifyTypeCode.value = "";
        fm.CertifyTypeCodeName.value = "";
        document.getElementById("CertifyTypeCode").disabled = false;
        
        fm.AgentCode.value = "";
        fm.GroupAgentCode.value="";
        document.getElementById("AgentCode").disabled = false;
        document.getElementById("GroupAgentCode").disabled = false;
        
        fm.AgentCom.value = "";
        fm.AgentComName.value = "";
        //修改中介机构的显示问题
        fm.all("AgentComTitleID").style.display = "none";
        fm.all("AgentComInputID").style.display = "none";
        fm.all("AgentComTitleID1").style.display = "none";
        fm.all("AgentComInputID1").style.display = "none";
        //----
        document.getElementById("AgentCom").disabled = false;
        
        fm.SaleChnl.value = "";
        fm.SaleChnlName.value = "";
        document.getElementById("SaleChnl").disabled = false;
        
        fm.Crs_SaleChnl.value = "";
        fm.Crs_SaleChnlName.value = "";
        document.getElementById("Crs_SaleChnl").disabled = false;
        
        fm.Crs_BussType.value = "";
        fm.Crs_BussTypeName.value = "";
        document.getElementById("Crs_BussType").disabled = false;

        fm.GrpAgentCom.value = "";
        document.getElementById("GrpAgentCom").disabled = false;

        fm.GrpAgentCode.value = "";
        document.getElementById("GrpAgentCode").disabled = false;

        fm.GrpAgentName.value = "";
        document.getElementById("GrpAgentName").disabled = false;

        fm.GrpAgentIDNo.value = "";
        document.getElementById("GrpAgentIDNo").disabled = false;
        
        document.getElementById("MixComFlag").disabled = false;

        AvailableCertifyListGrid.clearData();
        fm.btnAddCertifyCont.disabled = true;
        fm.btnAddAllCertifyCont.disabled = true;
    }
    else
    {
        var tStrSql = ""
            + " select distinct lict.ManageCom, lict.CertifyCode, lict.AgentCode, lict.SaleChnl, lict.AgentCom,getUniteCode(lict.AgentCode) "
            + " from LICertify lict "
            + " where 1 = 1 "
            + " and lict.PrtNo = '" + tPrtNo + "' "
            + " union all "
            + " select distinct lict.ManageCom, lict.CertifyCode, lict.AgentCode, lict.SaleChnl, lict.AgentCom,getUniteCode(lict.AgentCode) "
            + " from LIBCertify lict "
            + " where 1 = 1 "
            + " and lict.PrtNo = '" + tPrtNo + "' "
            ;
            
        var tArrResult = easyExecSql(tStrSql);
        
        if(tArrResult)
        {        
            fm.ManageCom.value = tArrResult[0][0];
            document.getElementById("ManageCom").disabled = true;
            
            fm.CertifyTypeCode.value = tArrResult[0][1];
            document.getElementById("CertifyTypeCode").disabled = true;
            
            if(tArrResult[0][2] != null && tArrResult[0][2] != "")
            {
                fm.AgentCode.value = tArrResult[0][2];
                fm.GroupAgentCode.value = tArrResult[0][5];
                document.getElementById("GroupAgentCode").disabled = true;
            }
            else
            {
                document.getElementById("GroupAgentCode").disabled = false;
            }
            
            fm.SaleChnl.value = tArrResult[0][3];
            document.getElementById("SaleChnl").disabled = true;
            
            //修改中介机构的显示问题
            if(fm.SaleChnl.value != null && fm.SaleChnl.value != "")
            {
            	if(fm.SaleChnl.value == "03" || fm.SaleChnl.value == "10" || fm.SaleChnl.value == "15" || fm.SaleChnl.value == "20")
            	{
            		fm.all("AgentComTitleID").style.display = "";
            		fm.all("AgentComInputID").style.display = "";
            		fm.all("AgentComTitleID1").style.display = "none";
            		fm.all("AgentComInputID1").style.display = "none";
            	}
            }
            //------------------------------------
            
            fm.AgentCom.value = tArrResult[0][4];
            document.getElementById("AgentCom").disabled = true;
        }
        
        fm.btnDelCertifyCont.disabled = false;
        fm.btnCreateGrpCont.disabled = false;
        
        
        var strSQL = ""
            + " select lict.Crs_SaleChnl,lict.Crs_BussType, lict.GrpAgentCom, lict.GrpAgentCode, lict.GrpAgentName,lict.GrpAgentIDNo "
            + " from LICertify lict "
            + " where 1 = 1 "
            + " and lict.PrtNo = '" + tPrtNo + "' and lict.Crs_SaleChnl is not null and lict.Crs_BussType is not null"
            + " and lict.GrpAgentCom is not null and lict.GrpAgentCode is not null "
            ;
        if(fm.GrpAgentName.value != "")
        {
            strSQL = strSQL + " and lict.GrpAgentName is not null ";
        }
        if(fm.GrpAgentIDNo.value != "")
        {
            strSQL = strSQL + " and lict.GrpAgentIDNo is not null ";
        }  
        
        strSQL += " union all "
            + " select lict.Crs_SaleChnl,lict.Crs_BussType, lict.GrpAgentCom, lict.GrpAgentCode, lict.GrpAgentName,lict.GrpAgentIDNo "
            + " from LIBCertify lict "
            + " where 1 = 1 "
            + " and lict.PrtNo = '" + tPrtNo + "' and lict.Crs_SaleChnl is not null and lict.Crs_BussType is not null"
            + " and lict.GrpAgentCom is not null and lict.GrpAgentCode is not null "
            ;
        if(fm.GrpAgentName.value != "")
        {
            strSQL = strSQL + " and lict.GrpAgentName is not null ";
        }
        if(fm.GrpAgentIDNo.value != "")
        {
            strSQL = strSQL + " and lict.GrpAgentIDNo is not null ";
        }  
        
         //反显交叉销售   
        var mArrResult = easyExecSql(strSQL);
        if(mArrResult)
        {   
    		fm.MixComFlag.checked = true;
    		fm.MixComFlag.disabled = true;
    		fm.all('GrpAgentComID').style.display = "";
	        fm.all('GrpAgentTitleID').style.display = "";
	        fm.all('GrpAgentTitleIDNo').style.display = "";
	        fm.Crs_SaleChnl.value = mArrResult[0][0];
	        fm.Crs_BussType.value = mArrResult[0][1];
	        fm.GrpAgentCom.value = mArrResult[0][2];
	        fm.GrpAgentCode.value = mArrResult[0][3];
	        fm.GrpAgentName.value = mArrResult[0][4];
	        fm.GrpAgentIDNo.value = mArrResult[0][5];
	        fm.Crs_SaleChnl.disabled = true;
	        fm.Crs_BussType.disabled = true;
	        fm.GrpAgentCom.disabled = true;
	        fm.GrpAgentCode.disabled = true;
	        fm.GrpAgentName.disabled = true;
	        fm.GrpAgentIDNo.disabled = true;
	        
	        var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
		  	var arrResult1 = easyExecSql(strSql);
		  	if (arrResult1 != null) {
		       	fm.GrpAgentComName.value = arrResult1[0][0];
		  	}
		  	else{  
		       	fm.GrpAgentComName.value = "";
		  	}
        }
        queryAvailableCertifyContList();
    }
    
    querySettlementInfo(tPrtNo);
}

/**
 * 增加单证到结算单
 */
function addCertifyCont()
{
    var checkedRowNum = 0;
    var rowNum = AvailableCertifyListGrid.mulLineCount;

    for (var i = 0; i < rowNum; i++)
    {
        if(checkedRowNum > 1)
        {
            break;
        }

        if(AvailableCertifyListGrid.getChkNo(i))
        {
            checkedRowNum += 1;
        }
    }

    if(checkedRowNum < 1)

    {
        alert("请至少选择一个单证!");
        return false;
    }
  
    //校验此批单证是否是由于一个账号在 两个地方同时登陆,发生问题
    if(!checkACLG())
    {
    	return false;
    }

    //2017-02-08 赵庆涛
    //社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
    if(!checkCrs_SaleChnl()){
     	return false;
    }

    //校验标准个险集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    //-----------------------
    
    if (confirm("您确实想添加该记录到当前结算单吗?"))
    {
        fm.btnAddCertifyCont.disabled = true;
        fm.btnAddAllCertifyCont.disabled = true;

        var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
        showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

        fm.action = "SettlementCertifyContListSave.jsp";
        fm.fmOperatorFlag.value = "Insert";
        fm.submit();
        fm.fmOperatorFlag.value = "";
        fm.action = "";
    }
    
}

function addAllCertifyCont()
{
	
    //校验此批单证是否是由于一个账号在 两个地方同时登陆,发生问题
    if(!checkACLG())
    {
    	return false;
    }
	
    if (confirm("您确实想添加全部记录到当前结算单吗?"))
    {
        fm.btnAddCertifyCont.disabled = true;
        fm.btnAddAllCertifyCont.disabled = true;

        var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
        showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

        fm.action = "SettlementCertifyContList_AllSave.jsp";
        fm.fmOperatorFlag.value = "InsertAll";
        fm.submit();
        fm.fmOperatorFlag.value = "";
        fm.action = "";
    }

    return true;
}

/**
 * 处理单证提交后动作。
 */
function afterDealCertifyContSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    if (FlagStr == "Succ")
    {
        //alert("Succ");
    }
    
    queryAvailableCertifyContList();
    querySettlementCertifyContList();
}

/**
 * 从结算单取消指定单证。
 */
function delCertifyCont()
{
    var checkedRowNum = 0;
    var rowNum = SettlementCertifyContListGrid.mulLineCount;

    for (var i = 0; i < rowNum; i++)
    {
        if(checkedRowNum > 1)
        {
            break;
        }

        if(SettlementCertifyContListGrid.getChkNo(i))
        {
            checkedRowNum += 1;
        }
    }

    if(checkedRowNum < 1)

    {
        alert("请至少选择一个单证!");
        return false;
    }
    
     //校验此批单证是否是由于一个账号在 两个地方同时登陆,发生问题
    if(!checkSCCLG())
    {
    	return false;
    }
    
    if (confirm("您确实想将该记录从当前结算单中取消吗?"))
    {
        fm.btnDelCertifyCont.disabled = true;

        var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
        showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

        fm.action = "SettlementCertifyContListSave.jsp";
        fm.fmOperatorFlag.value = "Delete";
        fm.submit();
        fm.fmOperatorFlag.value = "";
        fm.action = "";
    }
    
}

/**
 * 查询清单信息
 */
function querySettlementInfo(cPrtNo)
{    
    var tStrSql = ""
        + " select "
        + " nvl(sum(tmp.Prem), 0) SumPrem, count(1) SumCount, "
        + " nvl(sum(tmp.CBPrem), 0) CBSumPrem, nvl(sum(tmp.CBCount), 0) CBSumCount, "
        + " nvl(sum(tmp.WTPrem), 0) WTSumPrem, nvl(sum(tmp.WTCount), 0) WTSumCount, "
        + " '' "
        + " from "
        + " ("
        + " select "
        + " (case CardInfo.CertFlag when 'CB' then CardInfo.Prem when 'WT' then (-1 * CardInfo.Prem) else 0 end) Prem, "
        + " (case CardInfo.CertFlag when 'CB' then 1 else 0 end) CBCount, "
        + " (case CardInfo.CertFlag when 'WT' then 1 else 0 end) WTCount, "
        + " (case CardInfo.CertFlag when 'CB' then CardInfo.Prem else 0 end) CBPrem, "
        + " (case CardInfo.CertFlag when 'WT' then CardInfo.Prem else 0 end) WTPrem "
        + " from "
        + " ( "
        + " select "
        + " 'CB' CertFlag, lict.Prem "
        + " from LICertify lict "
        + " where 1 = 1 "
        + " and lict.PrtNo = '" + cPrtNo + "' "
        + " union all "
        + " select "
        + " 'WT' CertFlag, libct.Prem "
        + " from LIBCertify libct "
        + " where 1 = 1 "
        + " and libct.PrtNo = '" + cPrtNo + "' "
        + " ) as CardInfo "
        + " ) as tmp "
        ;
        
    var tArrResult = easyExecSql(tStrSql);
    
    if(tArrResult)
    {
        fm.SumCertifyContPrem.value = tArrResult[0][0];
        fm.SumCertifyContCount.value = tArrResult[0][1];
        fm.SumCBCertifyContPrem.value = tArrResult[0][2];
        fm.SumCBCertifyContCount.value = tArrResult[0][3];
        fm.SumWTCertifyContPrem.value = tArrResult[0][4];
        fm.SumWTCertifyContCount.value = tArrResult[0][5];
    }
}
//校验中介机构是否是共保机构
function checkAgentCom() 
{
	var tPrtNo = fm.PrtNo.value;
	
	var tSaleChnl = fm.SaleChnl.value;
	var tAgentCom = fm.AgentCom.value;
	
	if(tSaleChnl != null && tSaleChnl != "" && tSaleChnl == "03")
	{
		if(tAgentCom != null && tAgentCom != "")
		{
			var strSQL = "select actype from lacom where agentcom = '" + tAgentCom + "' ";
			var arrResult = easyExecSql(strSQL);
    		if(arrResult != null && arrResult == "05")
    		{
    			alert("团险中介的中介机构不能为共保的机构!");
    			return false;
    		}
		}
	}
		return true;
}
/**
 * 创建结算单对应的团体无名单
 */
function createGrpCont()
{
    if(!checkSaleChnlInfo())
    {
    	return false;
    }
    if(!checkMarkettypeSalechnl())
    {
    	return false;
    }
    if(!checkRiskSalechnl())
    {
    	return false;
    }
    
    //中介机构的校验
	if(!checkAgentCom())
	{
		return false;
	}
	
	//校验保单和退保单要同时加入到结算清单，并且结算总保费要大于零。
	if(!checkBandC())
	{
		return false;
	}
	
    if(!checkCrsBussParams())
    {
        return false;
    }
    
    document.getElementById("CValidate").verify = "团单生效日期|date&notnull";
    document.getElementById("CInValidate").verify = "团单终止日期|date&notnull";
    
    if(!verifyInput2())
    {
        document.getElementById("CValidate").verify = "";
        document.getElementById("CInValidate").verify = "";
        return false;
    }
    else
    {
        document.getElementById("CValidate").verify = "";
        document.getElementById("CInValidate").verify = "";
    }
    
    // 校验生效日期不得晚于终止日期
    try
    {
        var tCValidate = fm.CValidate.value;
        var tCInValidate = fm.CInValidate.value;
        if(dateDiff(tCValidate, tCInValidate, "D") < 0)
        {
            alert("生效日期不得晚于终止日期。");
            return false;
        }
    }
    catch(e)
    {
        alert("日期格式异常。");
        return false;
    }
    // --------------------
    
    if(!checkCoInsuranceRate())
    {
        return false;
    }
    
    if(!checkISTODS())
    {
    	return false;
    }
    
    //校验此批单证是否是由于一个账号在 两个地方同时登陆,发生问题
    if(!checkSCCLG())
    {
    	return false;
    }
    
    //校验停售
    if(!checkStopWrap()){
    	return false;
    }
    
    //校验综合开拓
    if(!ExtendCheck()){
    	return false;
    }
    
    if (confirm("您确实想对当前结算单进行结算吗?"))
    {
        fm.btnCreateGrpCont.disabled = true;

        var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
        showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

        fm.GrpContSaleChnl.value = fm.SaleChnl.value;
        fm.GrpContManageCom.value = fm.ManageCom.value;
        fm.CertifyCode.value = fm.CertifyTypeCode.value;
        fm.GrpContAgentCode.value = fm.AgentCode.value;
        fm.GrpContAgentCom.value = fm.AgentCom.value;
        
        fm.Crs_SaleChnl_KZ.value = fm.Crs_SaleChnl.value;
        fm.Crs_BussType_KZ.value = fm.Crs_BussType.value;
        fm.GrpAgentCom_KZ.value = fm.GrpAgentCom.value;
        fm.GrpAgentCode_KZ.value = fm.GrpAgentCode.value;
        fm.GrpAgentName_KZ.value = fm.GrpAgentName.value;
        fm.GrpAgentIDNo_KZ.value = fm.GrpAgentIDNo.value;
        if(fm.CoInsuranceFlag.checked == true)
        {
            fm.Flag.value = "1";
        }
        if(fm.ISTODS.checked == true)
        {
            fm.ZXFlag.value = "1";
        }else{
        	fm.ZXFlag.value = "0";
        }
        
        fm.action = "CertifyContSettlementSave.jsp";
        fm.fmOperatorFlag.value = "Create";
        fm.submit();
        fm.fmOperatorFlag.value = "";
        fm.action = "";
    }
}

/**
 * 创建结算单对应的团体无名单提交后动作。
 */
function afterCreateGrpContSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    if (FlagStr == "Succ")
    {
        //alert("Succ");
        window.location = "../certifyapp/CertifySettlementApply.jsp";
    }
    else
    {
        //alert("Fail");
        fm.btnCreateGrpCont.disabled = false;
    }
}

//查询业务员信息
function queryAgent()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        return ;
    }
    if(fm.all('SaleChnl').value == "")
    {
        alert("请先录入售出渠道信息！");
        return ;
    }
    var saleChnl = (fm.SaleChnl != null && fm.SaleChnl != "undefined") ? fm.SaleChnl.value : "";
    var agentCom = (fm.AgentCom != null && fm.AgentCom != "undefined") ? fm.AgentCom.value : "";

    // 交叉渠道，个险直销人员可以销售团险产品。
    var branchType = 2;
    if(saleChnl == '06')
        branchType = 1;
    
    var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
        + "&SaleChnl=" + saleChnl + "&AgentCom=" + agentCom + "&branchtype=" + branchType;
    var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
}

//查询返回时执行的函数,查询返回一个2维数组
function afterQuery2(arrResult) {
    if(arrResult != null) {
        fm.AgentCode.value = arrResult[0][0];
        fm.AgentName.value = arrResult[0][5];
        fm.GroupAgentCode.value=arrResult[0][95];
        //fm.AgentGroup.value = arrResult[0][1];
    }
}

//CodeQuery执行后的响应事件
function afterCodeSelect(codeName, field)
{
	
	//关闭社保渠道勾选交叉销售功能（社保渠道不得选择交叉销售业务）
	if(codeName=="unitesalechnl"){
		fm.all("MixComFlag").style.display="";
		fm.MixComFlag.checked = false;
		if(fm.SaleChnl.value=="18"||fm.SaleChnl.value=="20"){
			fm.all("MixComFlag").style.display="none";
			fm.MixComFlag.checked = false;
		}
		isMixCom();
	}
	
    if(codeName == "AgentCom")
    {
        //查询中介机构对应的业务员
        var tStrSql = "select b.AgentCode, b.Name, b.AgentGroup,b.groupagentcode from LAComToAgent a, LAAgent b "
            + "where a.AgentCom = '" + field.value + "' and a.AgentCode = b.AgentCode";
        var arrResult = easyExecSql(tStrSql);
        if(arrResult)
        {
            fm.AgentCode.value = arrResult[0][0];
            fm.AgentName.value = arrResult[0][1];
            fm.GroupAgentCode.value=arrResult[0][2];
            //fm.AgentGroup.value = arrResult[0][2];
        }
    }

    if(codeName=="SaleChnl" || codeName == "unitesalechnl")
	{
	  if(field.value == "04" )
	  {
	  	
	    fm.all("AgentComTitleID").style.display = "";
	    fm.all("AgentComInputID").style.display = "";
	  }
	  else
	  {
	    fm.all("AgentComTitleID").style.display = "none";
	    fm.all("AgentComInputID").style.display = "none";
	  }
	  if(field.value == "03" || field.value == "10" || field.value == "11" || field.value == "12" || field.value == "15"|| field.value == "20"){
        if(field.value == "11" || field.value == "12")
        {
	  	    fm.all("AgentComTitleID1").style.display = "";
        }
        else
        {
            fm.all("AgentComTitleID1").style.display = "none";
        }
	    fm.all("AgentComTitleID1").style.display = "";
	    fm.all("AgentComInputID1").style.display = "";
	    var tBranchType="#2";
	    var tBranchType2="";
	    if (field.value == "11"){
	    	tBranchType2="04";	    	
	    }else if (field.value == "12"){
	    	tBranchType2="04";
	    }else if (field.value == "03"){
            tBranchType2="02#,#04";
        }else if (field.value == "10"){
        	tBranchType = "#1";
        	tBranchType2 = "02#,#04";
        }else if (field.value == "15"){
        	tBranchType = "#5";
        	tBranchType2 = "01";
        }else if (field.value == "20"){
        	tBranchType = "#6";
        	tBranchType2 = "02";
        }
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
	  }else{
	    fm.all("AgentComTitleID1").style.display = "none";
	    fm.all("AgentComInputID1").style.display = "none";
	  }
	  
	  fm.AgentCom.value = "";
	  fm.AgentCode.value = "";
	  fm.GroupAgentCode.value = "";
	  fm.AgentCom1.value = "";
	  fm.GrpAgentCom.value = "";
	  fm.GrpAgentCode.value = "";
	  fm.GrpAgentName.value = "";
	}
}

function goBack()
{
    //location.href = "./CertifySettlementApply.jsp";
    window.location.replace("./CertifySettlementApply.jsp");
}

/**
 * 集团交叉要素校验
 */
function checkCrsBussParams()
{
    var tCrs_SaleChnl = trim(fm.Crs_SaleChnl.value);
    var tCrs_BussType = trim(fm.Crs_BussType.value);
    
    var tGrpAgentCom = trim(fm.GrpAgentCom.value);
    var tGrpAgentCode = trim(fm.GrpAgentCode.value);
    var tGrpAgentName = trim(fm.GrpAgentName.value);
    var tGrpAgentIDNo = trim(fm.GrpAgentIDNo.value);
    
    if(tCrs_SaleChnl != null && tCrs_SaleChnl != "")
    {
        if(tCrs_BussType == null || tCrs_BussType == "")
        {
            alert("选择集团交叉渠道时，集团交叉业务类型不能为空。");
            return false;
        }
        if(tGrpAgentCom == null || tGrpAgentCom == "")
        {
            alert("选择集团交叉渠道时，对方业务员机构不能为空。");
            return false;
        }
        if(tGrpAgentCode == null || tGrpAgentCode == "")
        {
            alert("选择集团交叉渠道时，对方业务员代码不能为空。");
            return false;
        }
        if(tGrpAgentName == null || tGrpAgentName == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名不能为空。");
            return false;
        }
        if(tGrpAgentIDNo == null || tGrpAgentIDNo == "")
        {
            alert("选择集团交叉渠道时，对方业务员身份证不能为空。");
            return false;
        }
    }
    else
    {
        if(tCrs_BussType != null && tCrs_BussType != "")
        {
            alert("未选择集团交叉渠道时，集团交叉业务类型不能填写。");
            return false;
        }
        if(tGrpAgentCom != null && tGrpAgentCom != "")
        {
            alert("未选择集团交叉渠道时，对方业务员机构不能为空。");
            return false;
        }
        if(tGrpAgentCode != null && tGrpAgentCode != "")
        {
            alert("未选择集团交叉渠道时，对方业务员代码不能为空。");
            return false;
        }
        if(tGrpAgentName != null && tGrpAgentName != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名不能为空。");
            return false;
        }
        if(tGrpAgentIDNo != null && tGrpAgentIDNo != "")
        {
            alert("未选择集团交叉渠道时，对方业务员身份证不能为填写 。");
            return false;
        }
    }
    
    return true;
}

//显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value = "";
        fm.all('Crs_SaleChnlName').value = "";
        fm.all('Crs_BussType').value = "";
        fm.all('Crs_BussTypeName').value = "";
        fm.all('GrpAgentCom').value = "";
        fm.all('GrpAgentComName').value = "";
        fm.all('GrpAgentCode').value = "";
        fm.all('GrpAgentName').value = "";
        fm.all('GrpAgentIDNo').value = "";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}

//显示共保信息
function isCoInsurance()
{
    if(fm.CoInsuranceFlag.checked == true)
    {   
        fm.all('DivLCContButton').style.display = "";
        fm.all('divCoInsuranceParam').style.display = "";
    }
    if(fm.CoInsuranceFlag.checked == false)
    {
        fm.all('DivLCContButton').style.display = "none";
        fm.all('divCoInsuranceParam').style.display = "none";
        initCoInsuranceParamGrid();

    }
}

//择对方机构代码时,显示对方机构名称.
function GetGrpAgentName()
{
	var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     fm.GrpAgentComName.value = "";
	}
}

//根据机构代码显示机构名称
function getAgentName()
{
    var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     alert("对方机构代码有错误,请修改");
	     fm.GrpAgentCom.value = "";
	     return false;
	}
}

function checkCoInsuranceRate()
{
    var tSumRate = 0;
    
    var tRows = CoInsuranceParamGrid.mulLineCount;
    for(var i = 0; i < tRows; i++)
    {
        var tTmpRateOfOne = CoInsuranceParamGrid.getRowColData(i, 3);
        tSumRate = accAdd(tSumRate, tTmpRateOfOne);
    }
    
    if(tSumRate > 1)
    {
        alert("所有负担比例累加之和不能超过1。");
        return false;
    }
    return true;
}

function accAdd(arg1,arg2)
{
    var r1,r2,m;
    try{r1=arg1.toString().split(".")[1].length;}catch(e){r1=0;}
    try{r2=arg2.toString().split(".")[1].length;}catch(e){r2=0;}
    m=Math.pow(10,Math.max(r1,r2));
    return (arg1*m+arg2*m)/m;
}



//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryGrpAgent()
{
    if(fm.all('GrpAgentCode').value == "" && fm.all('GrpAgentIDNo').value == "" )
    {  
        var tGrpAgentCom = (fm.GrpAgentCom != null && fm.GrpAgentCom != "undefined") ? fm.GrpAgentCom.value : "";
        var tGrpAgentName = (fm.GrpAgentName != null && fm.GrpAgentName != "undefined") ? fm.GrpAgentName.value : "";
        var tGrpAgentIDNo = (fm.GrpAgentIDNo != null && fm.GrpAgentIDNo != "undefined") ? fm.GrpAgentIDNo.value : "";
        var strURL = "../sys/GrpAgentCommonQueryMain.jsp?GrpAgentCom=" + tGrpAgentCom +
                     "&GrpAgentName=" + tGrpAgentName + "&GrpAgentIDNo=" + tGrpAgentIDNo;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCode').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Sales_Cod='" + fm.all('GrpAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.GrpAgentCode.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }else if(fm.all('GrpAgentIDNo').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Id_No='" + fm.all('GrpAgentIDNo').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("身份证号码为:[" +  fm.GrpAgentIDNo.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery3(arrResult)
{  
  if(arrResult!=null)
    {  	
  	fm.GrpAgentCode.value = arrResult[0][0];
  	fm.GrpAgentName.value = arrResult[0][1];
    fm.GrpAgentIDNo.value = arrResult[0][2];
  }
}

//执行查询交叉销售对方机构代码 date 20101117 by gzh
function queryGrpAgentCom()
{
	if(fm.all('Crs_SaleChnl').value == "" || fm.all('Crs_SaleChnl').value == null)
	{
		alert("请先选择交叉销售渠道！！");
		return false;
	}
    if(fm.all('GrpAgentCom').value == "")
    {  
        var tCrs_SaleChnl =  fm.Crs_SaleChnl.value;
        var tCrs_SaleChnlName = fm.Crs_SaleChnlName.value;
        var strURL = "../sys/GrpAgentComQueryMain.jsp?Crs_SaleChnl="+tCrs_SaleChnl+"&Crs_SaleChnlName="+tCrs_SaleChnlName;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentComQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCom').value != "")
    {	
        var strGrpSql = "select grpagentcom,Under_Orgname  from lomixcom  where  grpagentcom ='"+fm.GrpAgentCom.value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery4(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("机构代码为:[" +  fm.GrpAgentCom.value + "]的机构不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery4(arrResult)
{  
  if(arrResult!=null)
    { 	
  	fm.GrpAgentCom.value = arrResult[0][0];
  	fm.GrpAgentComName.value = arrResult[0][1];
  }
}


//选择交叉销售后对交叉销售各不为空项的校验
    // by gzh 20101118
function MixComCheck()
{    
    if(fm.MixComFlag.checked == true)
    {
    	if(fm.Crs_SaleChnl.value == "" || fm.Crs_SaleChnl.value == null)
    	{
    		alert("选择交叉销售时，交叉销售渠道不能为空，请核查！");
    		fm.all('Crs_SaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.Crs_BussType.value == "" || fm.Crs_BussType.value == null)
    	{
    		alert("选择交叉销售时，交叉销售业务类型不能为空，请核查！");
    		fm.all('Crs_BussType').focus();
    		return false;
    	}
    	if(fm.GrpAgentCom.value == "" || fm.GrpAgentCom.value == null)
    	{
    		alert("选择交叉销售时，对方机构代码不能为空，请核查！");
    		fm.all('GrpAgentCom').focus();
    		return false;
    	}
    	/**
    	if(fm.GrpAgentComName.value == "" || fm.GrpAgentComName.value == null)
    	{
    		alert("选择交叉销售时，对方机构名称不能为空，请核查！");
    		fm.all('GrpAgentComName').focus();
    		return false;
    	}
    	*/
    	if(fm.GrpAgentCode.value == "" || fm.GrpAgentCode.value == null)
    	{
    		alert("选择交叉销售时，对方业务员代码不能为空，请核查！");
    		fm.all('GrpAgentCode').focus();
    		return false;
    	}
    	if(fm.GrpAgentName.value == "" || fm.GrpAgentName.value == null)
    	{
    		alert("选择交叉销售时，对方业务员姓名不能为空，请核查！");
    		fm.all('GrpAgentName').focus();
    		return false;
    	}
    	if(fm.GrpAgentIDNo.value == "" || fm.GrpAgentIDNo.value == null)
    	{
    		alert("选择交叉销售时，对方业务员身份证号码不能为空，请核查！");
    		fm.all('GrpAgentIDNo').focus();
    		return false;
    	}
      	return true;
    }else{
    	return true;
    }
}    
    // --------------------
    
//校验卡折业务 集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   by 赵庆涛 2016-07-07
function checkCrsBussSaleChnl()
{
	if(fm.Crs_SaleChnl.value != null && fm.Crs_SaleChnl.value != "" && fm.Crs_BussType.value == "01")
	{
		if(fm.SaleChnl.value != null && fm.SaleChnl.value != ""){
			var strSQL = "select 1 from ldcode1 where codetype='crssalechnl' and code='01' and code1='"+ fm.SaleChnl.value+ "'";
			var strSQL1= "select 1 from LACom where AgentCom ='"+ fm.AgentCom.value +"' and BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and AcType NOT in ('05') "
			var arrResult = easyExecSql(strSQL);
			var arrResult1 = easyExecSql(strSQL1);
		    if (arrResult == null||arrResult1 == null)
		    {
		        alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为中介渠道并且机构必须为对应的中介机构！");
		        return false;
		    }
		}
	}
	return true;
}

function checkCrs_SaleChnl(){
	var tCrs_BussType=fm.Crs_BussType.value;
	var tSaleChnl=fm.SaleChnl.value;
	if((tCrs_BussType=="01"||tCrs_BussType=="02")&&(tSaleChnl=="16"||tSaleChnl=="18")){
		alert("社保直销及社保综拓渠道出单若选择交叉销售，交叉销售业务类型不能为相互代理或联合展业！");
		return false;
	}
	return true;
}

////校验保单和退保单要同时加入到结算清单，并且结算总保费要大于零。
function checkBandC(){
	var tPrtNo = fm.PrtNo.value;
	
	var strBSql = "select cardno from libcertify where prtno = '"+tPrtNo+"' and state = '02' ";
	var arrBResult = easyExecSql(strBSql);
	if(arrBResult){//该结算单存在退保数据
		for(var i=0;i<arrBResult.length;i++){
			var tBCardNo = arrBResult[i][0];
			var strSql1 = "select 1 from licertify where prtno = '"+tPrtNo+"' and state = '02' and cardno = '"+tBCardNo+"' ";
			var arrResult1 = easyExecSql(strSql1);
			if(!arrResult1){
				alert("["+tBCardNo+"]退保保单对应的承保保单没有加入到当前结算单。");
				return false;
			}
		}
	}
	
	var strCSql = "select cardno from licertify where prtno = '"+tPrtNo+"' and state = '02' ";
	var arrCResult = easyExecSql(strCSql);
	if(!arrCResult){
		alert("获取结算数据失败！");
		return false;
	}
	for(var i=0;i<arrCResult.length;i++){
		var strSql1 = "select 1 from libcertify where cardno = '"+arrCResult[i][0]+"' and state = '01' ";
		var arrResult1 = easyExecSql(strSql1);
		if(arrResult1){
   			alert("["+arrCResult[i][0]+"]承保保单对应的退保保单没有加入到当前结算单。");
			return false;
    	}
	}
	
    var tSql1 = "select nvl(sum(prem),0) from licertify where prtno = '"+tPrtNo+"' and state = '02' ";
    var arr1 = easyExecSql(tSql1);
    if(arr1[0][0]=="0"){
    	alert("获取该结算单的总保费失败！");
    	return false;
    }
    var tSql2 = "select nvl(sum(prem),0) from libcertify where prtno = '"+tPrtNo+"' and state = '02' ";
    var arr2 = easyExecSql(tSql2);
    if(parseFloat(arr1[0][0])<=parseFloat(arr2[0][0])){
    	alert("结算单总保费应大于零。");
		return false;
    }
    return true;
}

function queryZXAgent()
{
    if(fm.ISTODS.checked == false)
    {
    	alert("请先选择“转为直销业务”！");
        return ;
    }
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        return ;
    }
    var saleChnl = '02';

    // 交叉渠道，个险直销人员可以销售团险产品。
    var branchType = 2;
    
    var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
        + "&SaleChnl=" + saleChnl + "&branchtype=" + branchType+"&specFlag=ZX";
    var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
}
//查询返回时执行的函数,查询返回一个2维数组
function afterQuery5(arrResult) {
    if(arrResult != null) {
        fm.ZXAgentCode.value = arrResult[0][0];
        fm.ZXAgentName.value = arrResult[0][5];
        fm.ZXGroupAgentCode.value=arrResult[0][95];
        //fm.AgentGroup.value = arrResult[0][1];
    }
}
function checkISTODS(){
	if(fm.ISTODS.checked == true)
	{
	    var comSQL="";
	    var comValue=fm.ManageCom.value;
        if(comValue.length>=4){
            comSQL = " or code1=substr("+comValue+",1,4)";
        }
        var strITSql = "select RiskWrapPlanName from ldcode1 where codetype = 'istodsZX' and code='ZX' and (code1='"+comValue+"'"+comSQL+" ) ";	
		var arrITResult = easyExecSql(strITSql);
		//if(fm.ManageCom.value != "86320000" && fm.ManageCom.value.substr(0,4) != "8613" && fm.ManageCom.value.substr(0,4) != "8633" ){
		if(!arrITResult){
			alert(arrITResult[0][0]);
        	return false;
		}
		if(fm.ZXAgentCode.value	== null || fm.ZXAgentCode.value	== '' || fm.ZXGroupAgentCode.value	== null || fm.ZXGroupAgentCode.value	== '')
		{
			alert("中介转直销的业务员编码不能为空！");
        	return false;
		}
	}
	return true;
}
function initISTODS(mManageCom)
{
    var comSQL="";
    if(mManageCom.length>=4){
        comSQL = " or code1=substr("+mManageCom+",1,4)";
    }
    var strITSql = "select 1 from ldcode1 where codetype = 'istodsZX' and code='ZX' and (code1='"+mManageCom+"'"+comSQL+" ) ";
    var arrITResult = easyExecSql(strITSql);
    //add by zjd 8633浙江全省
	//if(mManageCom == "86320000" || mManageCom.substr(0,4) == "8613" || mManageCom.substr(0,4) == "8633"){
	if(arrITResult){
		fm.all("DivISTODS").style.display = "";
	}
}

function checkACLG(){
	//校验此批单证是否是由于一个账号在 两个地方同时登陆，造成的重复加入结算单
	for(var i = 0; i < AvailableCertifyListGrid.mulLineCount; i++ )
	{
		if( AvailableCertifyListGrid.getChkNo(i) == true )
		{
			var tCardNo = AvailableCertifyListGrid.getRowColData(i,1);
			var strSQL= " select 1 " +
					"from lcgrpcont a " +
					"where a.prtno =(select b.prtno " +
					                "from licertify b " +
					                "where CardNo = '"+tCardNo+"')";
			var prtno1 = easyExecSql(strSQL); 
					
			if(prtno1 == "1"){
				alert("卡号"+tCardNo+"已经结算，不能再次进行处理!"); 
				return false;
			}
		}
	}
	
	//校验此批单证是否是由于一个账号在 两个地方同时登陆，一个地方结算过，阻止另一个地方继续结算
	var tPrtNo = fm.PrtNo.value;
	
	var strSQL= " select 1 " +
	            "from lcgrpcont a " +
	            "where a.prtno = '"+tPrtNo+"'";
	var prtno2 = easyExecSql(strSQL); 
	
	if(prtno2 == "1"){
		alert("结算单号"+tPrtNo+"已经结算，不能再次进行处理!"); 
		return false;
	}
	
	//校验多台电脑，不同用户，不同结算单号处理相同卡折
	for(var i = 0; i < AvailableCertifyListGrid.mulLineCount; i++ )
	{
		if( AvailableCertifyListGrid.getChkNo(i) == true )
		{
			var tCardNo = AvailableCertifyListGrid.getRowColData(i,1);
			var strSQL= " select prtno " +
					    "from licertify " +
					    "where CardNo = '"+tCardNo+"'";
			
			var prtno1 = easyExecSql(strSQL); 
			if(prtno1!=""){
				alert("卡号"+tCardNo+"已经在别处加入结算单，不能再次进行处理!"); 
				return false;
			}
		}
	}
	return true;
}


function checkSCCLG(){
//校验此批单证是否是由于一个账号在 两个地方同时登陆，造成的重复加入结算单
	for(var i = 0; i < SettlementCertifyContListGrid.mulLineCount; i++ )
	{
		if( SettlementCertifyContListGrid.getChkNo(i) == true )
		{
			var tCardNo = SettlementCertifyContListGrid.getRowColData(i,1);
			var strSQL= " select 1 " +
					"from lcgrpcont a " +
					"where a.prtno =(select b.prtno " +
					                "from licertify b " +
					                "where CardNo = '"+tCardNo+"')";
			var prtno1 = easyExecSql(strSQL); 
					
			if(prtno1 == "1"){
				alert("卡号"+tCardNo+"已经结算，不能再次进行处理!"); 
				return false;
			}
		}
	}
	
	//校验此批单证是否是由于一个账号在 两个地方同时登陆，一个地方结算过，阻止另一个地方继续结算
	var tPrtNo = fm.PrtNo.value;
	
	var strSQL= " select 1 " +
	            "from lcgrpcont a " +
	            "where a.prtno = '"+tPrtNo+"'";
	var prtno2 = easyExecSql(strSQL); 
	
	if(prtno2 == "1"){
		alert("结算单号"+tPrtNo+"已经结算，不能再次进行处理!"); 
		return false;
	}
	
	//校验多台电脑，不同用户，不同结算单号处理相同卡折
	for(var i = 0; i < AvailableCertifyListGrid.mulLineCount; i++ )
	{
		if( AvailableCertifyListGrid.getChkNo(i) == true )
		{
			var tCardNo = AvailableCertifyListGrid.getRowColData(i,1);
			var strSQL= " select prtno " +
					    "from licertify " +
					    "where CardNo = '"+tCardNo+"'";
			var prtno1 = easyExecSql(strSQL);
			if(prtno1!=""){
				alert("卡号"+tCardNo+"已经在别处加入结算单，不能再次进行处理!"); 
				return false;
			}
		}
	}
	return true;
}
//------------------------------
function checkStopWrap(){
	var tPrtNo = fm.PrtNo.value;
	var tStrSql = " select distinct lrw.riskwrapcode,lic.salechnl from ldriskwrap lrw "
				+ " inner join lmcardrisk lcr on lcr.riskcode = lrw.riskwrapcode "
				+ " inner join licertify lic on lic.certifycode = lcr.certifycode "
				+ " where lic.prtno = '"+tPrtNo+"' ";
	var tArrResult = easyExecSql(tStrSql);
	if(tArrResult){
		var tCurrent = getCurrentDate();
		for(var i=0;i<tArrResult.length;i++){
			var tWrapCode = tArrResult[i][0];
			var tSaleChnl = tArrResult[i][1];
			var strSQL = "select 1 from ldcode1 where codetype = 'stopwrap' and code = '"+tWrapCode+"' and code1 = '"+tSaleChnl+"' ";
			var arrResult = easyExecSql(strSQL);
			if(arrResult){
				alert("本次结算的套餐编码为["+tWrapCode+"],该套餐已停售，不允许进行结算！");
				return false;
			}
		}
	}
	return true;
}
//显示或者综合开拓
function isExtend()
{
    if(fm.ExtendFlag.checked == true)
    {
        fm.all('ExtendID').style.display = "";
        fm.all('AssistSaleChnl').value ="";
        fm.all('AssistSaleChnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value ="";
    }
    if(fm.ExtendFlag.checked == false)
    {
        fm.all('AssistSaleChnl').value ="";
        fm.all('AssistSaleChnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value ="";
        fm.all('ExtendID').style.display = "none";
    }
}
//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryAssistAgent()
{
    if(fm.all('AssistSaleChnl').value == "" || fm.all('AssistSaleChnl').value == null)
    {
    	alert("请先选择协助销售渠道！");
    	return false;
    }
    if(fm.all('AssistAgentCode').value == "" )
    {  
        var tAssistSaleChnl = (fm.AssistSaleChnl != null && fm.AssistSaleChnl != "undefined") ? fm.AssistSaleChnl.value : "";
        var strURL = "../sys/AssistAgentCommonQueryMain.jsp?AssistSaleChnl=" + tAssistSaleChnl;        
        //alert(strURL);
        var newWindow = window.open(strURL, "AssistAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('AssistAgentCode').value != "")
    {	
        var strGrpSql = "select agentcode,name from LAAgent where agentcode ='" + fm.all('AssistAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery6(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.AssistAgentCode.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}
function afterQuery6(arrResult){
  if(arrResult!=null) {
    fm.AssistAgentCode.value = arrResult[0][0];
    fm.AssistAgentName.value = arrResult[0][1];
  }
}
function ExtendCheck()
{    
    if(fm.ExtendFlag.checked == true)
    {
    	if(fm.AssistSaleChnl.value == "" || fm.AssistSaleChnl.value == null)
    	{
    		alert("选择综合开拓时，协助销售渠道不能为空，请核查！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.AssistAgentCode.value == "" || fm.AssistAgentCode.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员代码不能为空，请核查！");
    		fm.all('AssistAgentCode').focus();
    		return false;
    	}
    	if(fm.AssistAgentName.value == "" || fm.AssistAgentName.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员姓名不能为空，请核查！");
    		fm.all('AssistAgentName').focus();
    		return false;
    	}
    	var tSQL = "select code1,codename from ldcode1 where codetype = 'salechnl' and code = '"+fm.AssistSaleChnl.value+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("销售渠道与业务员代码不匹配！");
			return false;
		}
		var tSQL1 = "select 1 from laagent where agentcode = '"+fm.AssistAgentCode.value+"' and BranchType = '"+arrResult[0][0]+"' and BranchType2 = '"+arrResult[0][1]+"'";
		var arrResult1 = easyExecSql(tSQL1);
		if(!arrResult1){
			alert("销售渠道与业务员代码不匹配，请核查！");
			return false;
		}
      	return true;
    }else{
    	return true;
    }
}
function initExtend(){
	var tSql = "select AssistSalechnl,AssistAgentCode from LCExtend where prtno='" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.ExtendFlag.checked = true;
			fm.all('ExtendID').style.display = "";
			fm.AssistSaleChnl.value = arrResult[0][0];
			fm.AssistAgentCode.value = arrResult[0][1];
			var  tSql1 = "select name from LAAgent where agentcode ='" + arrResult[0][1] + "'";
			var arrResult1 = easyExecSql(tSql1);
			fm.AssistAgentName.value = arrResult1[0][0];
		}
	}
}
//2128 by gzh
function checkSaleChnlInfo(){
	var tSQL = "select managecom,agentcom,agentcode,salechnl from licertify where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单数据失败！");
		return false;
	}
	var tManageCom = arr[0][0];
	var tAgentCom = arr[0][1];
	var tAgentCode = arr[0][2];
	var tSaleChnl = arr[0][3];
	
	var tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
	var arrCode = easyExecSql(tSQLCode);
	if(!arrCode){
		alert("业务员与管理机构不匹配！");
		return false;
	}
	if(tSaleChnl == "02" || tSaleChnl == "03"){
		var agentCodeSql = " select 1 from LAAgent a where a.AgentCode = '"+ tAgentCode+ "'" 
	                 + " and a.BranchType = '2' and a.BranchType2 in ('01','02') "
	                 + " union all "
					 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					 + " and '03' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
	    var arrAgentCode = easyExecSql(agentCodeSql);
	    if(!arrAgentCode){
	    	alert("业务员和销售渠道不匹配！");
			return false;
	    }
	}else{
		var agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
	                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
                     + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
                     + " union all "
					 + " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					 + " and '10' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
	    var arrAgentCode = easyExecSql(agentCodeSql);
	    if(!arrAgentCode){
	    	alert("业务员和销售渠道不匹配！");
			return false;
	    }
	}
	if(tAgentCom != "" && tAgentCom != null && tAgentCom != "null"){
		var tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
		var arrCom = easyExecSql(tSQLCom);
		if(!arrCom){
			alert("中介机构与管理机构不匹配！");
			return false;
		}
		var tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
		var arrComCode = easyExecSql(tSQLComCode);
		if(!arrComCode){
			alert("业务员与中介机构不匹配！");
			return false;
		}
	}
	return true;
}
function checkMarkettypeSalechnl(){
	var tSQL = "select salechnl from licertify where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单数据失败！");
		return false;
	}
	var tSaleChnl = arr[0][0];
	var tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '1' and code1 = '"+tSaleChnl+"' ";
	var arrCheck = easyExecSql(tcheckSQL);
	if(!arrCheck){
		alert("市场类型和销售渠道不匹配，请核查！");
		return false;
	}
	return true;
}
function checkRiskSalechnl(){
	var tSQL = "select salechnl,certifycode from licertify where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取结算单数据失败！");
		return false;
	}
	var tSaleChnl = arr[0][0];
	var tCertifyCode = arr[0][1];
	var tRiskSQL = "select riskcode from ldriskwrap where riskwrapcode = (select riskcode from lmcardrisk where certifycode = '"+tCertifyCode+"' ) ";
	var arrRiskCode = easyExecSql(tRiskSQL);
	if(!arrRiskCode){
		alert("获取结算单险种数据失败！");
		return false;
	}
	for(var i=0;i<arrRiskCode.length;i++){
		var tRiskCode = arrRiskCode[i][0];
		var tcheckSQL = "select codename from ldcode where codetype = 'unitesalechnlrisk' and code = '"+tRiskCode+"' ";
		var arrCheck = easyExecSql(tcheckSQL);
		if(!arrCheck){
			if(tSaleChnl == "16"){
				alert("险种"+tRiskCode+"为非社保险种，与销售渠道"+tSaleChnl+"不匹配！");
				return false;
			}
		}else{
			if(arrCheck[0][0] != "all" && arrCheck[0][0] != tSaleChnl){
				alert("险种"+tRiskCode+"与销售渠道"+tSaleChnl+"不匹配！");
				return false;
			}
		}
	}
	return true;
}
// end 2128
//双击对方业务员框要显示的页面
function otherSalesInfo(){
	//alert("双击对方业务员代码");
	window.open("../sys/MixedSalesAgentMain.jsp?ManageCom="+fm.ManageCom.value);
}
//对方业务员页面查询完“返回”按钮，接收数据
function afterQueryMIX(arrResult){
	if(arrResult!=null){
		var arr = arrResult.split("#");
		fm.GrpAgentCom.value = arr[0]; //对方机构代码
		//fm.GrpAgentComName.value = arr[1]; //对方机构名称
		fm.GrpAgentCode.value = arr[2]; //对方业务员代码
		fm.GrpAgentName.value = arr[3]; //对方业务员姓名
		fm.GrpAgentIDNo.value = arr[4]; //对方业务员身份证
		
	}else{
		alert("返回过程出现异常，请重新操作");
	}	
}