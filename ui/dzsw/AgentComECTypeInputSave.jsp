<%@page import="dzsw.InterUI"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="dzsw.*"%>

<%
//输入参数
	String sSerNo[] = request.getParameterValues("AgentGridNo");
	String tChk[] = request.getParameterValues("InpAgentGridChk");//判断是否被选中InpXxxxChk
	//获取当前页每一个 AgentCom  ManageCom SalechnlCode AcTypeCode
	String tManageComs[] = request.getParameterValues("AgentGrid1");  //管理机构代码
	String tAgentCom[] = request.getParameterValues("AgentGrid5");//中介机构编码
	String tSalechnlCode[] = request.getParameterValues("AgentGrid10");//中介所属销售渠道编码
	String tAcTypeCode[] = request.getParameterValues("AgentGrid11");//中介机构类型编码
	
	LAComECTypeSet tLAComECTypeSet = new LAComECTypeSet();
	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String strManageCom = tG.AgentCom;
	//执行动作：insert 添加纪录       delete 删除记录 
	transact = request.getParameter("fmtransact");
	System.out.println(transact);
	if ("Guishu".equals(transact)) {
		for (int nIndex = 0; nIndex < sSerNo.length; nIndex++) {
			if ("1".equals(tChk[nIndex])) {
				LAComECTypeSchema tLAComECTypeSchema = new LAComECTypeSchema();
				tLAComECTypeSchema.setManageCom(tManageComs[nIndex]);
				tLAComECTypeSchema.setSalechnl(tSalechnlCode[nIndex]);
				tLAComECTypeSchema.setAcType(tAcTypeCode[nIndex]);
				tLAComECTypeSchema.setAgentCom(tAgentCom[nIndex]);
				tLAComECTypeSet.add(tLAComECTypeSchema);
			}
		}
	} else if ("QGuishu".equals(transact)) {
		for (int nIndex = 0; nIndex < sSerNo.length; nIndex++) {
			if ("1".equals(tChk[nIndex])) {
				LAComECTypeSchema tLAComECTypeSchema = new LAComECTypeSchema();
				tLAComECTypeSchema.setManageCom(tManageComs[nIndex]);
				tLAComECTypeSchema.setSalechnl(tSalechnlCode[nIndex]);
				tLAComECTypeSchema.setAcType(tAcTypeCode[nIndex]);
				tLAComECTypeSchema.setAgentCom(tAgentCom[nIndex]);
				tLAComECTypeSet.add(tLAComECTypeSchema);
			}
		}
	}

	// 准备向后台传输数据 VData
	VData tVData = new VData();
	FlagStr = "";
	tVData.add(tG);
	tVData.addElement(tLAComECTypeSet);
	InterUI tInterUI = new InterUI();
	try {
		tInterUI.submitData(tVData, transact);
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	if (!FlagStr.equals("Fail")) {
		tError = tInterUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Succ";
		} else {
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
