var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
turnPage1.pageLineNum = 20;
var showInfo;
window.onfocus = myonfocus;// 使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		} catch (ex) {
			showInfo = null;
		}
	}
}
function beforeSubmit() {
	if (fm.AgentCom.value == null || fm.AgentCom.value == ""
			|| fm.AgentCom.value == "null") {
		alert("中介机构编码不能为空！");
		return false;
	}
	var tWSQL = "select 1 from lacomectype where AgentCom = '"
			+ fm.AgentCom.value + "'";
	var strWResult = easyExecSql(tWSQL);
	if (!strWResult) {
		alert("获取中介机构编码[" + fm.AgentCom.value + "]信息失败，请核查中介机构编码是否正确！");
		return false;
	}
	return true;
}

// 触发归属stopAgent

function stopAgent() {
	fm.fmtransact.value = "Guishu";
	if (!beforeUpAndDel()) {
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
// 触发归属stopAgent
function qstopAgent() {
	fm.fmtransact.value = "QGuishu";
	if (!beforeUpAndDel()) {
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}
// 在JavaScript中从MulLine取值
function beforeUpAndDel() {
	// 判定是否有选择
	var count = 0;
	var tGuishuCount = 0;
	var tQGuishuCount = 0;
	var tErrors = "";
	for (var i = 0; i < AgentGrid.mulLineCount; i++) {
		if (AgentGrid.getChkNo(i) == true) {
			count++;
			var tRowData = AgentGrid.getRowData(i);
			var tComCode = tRowData[0];// 管理机构
			var tName = tRowData[1];// 中介机构名称
			var tStateFlag = tRowData[8];// 是否已归属
			var tFlag = fm.fmtransact.value; // 一个隐藏的控件，用在js中赋值，传到后面的save页面
			if (tFlag == 'Guishu') {
				if (tStateFlag == '否') {
					tGuishuCount++;
				} else {
					tQGuishuCount++;
					tErrors = tErrors + "中介机构：" + tName + "的状态为【是】，不可进行归属处理！\n";
				}
			} else if (tFlag == 'QGuishu') {
				if (tStateFlag == '否') {
					tGuishuCount++;
					tErrors = tErrors + "中介机构：" + tName
							+ "的归属状态为【否】，不可进行取消归属处理！\n";
				} else {
					tQGuishuCount++;
				}
			}
		}
	}
	if (count == 0) {
		alert("请选择需要处理的中介机构！");
		return false;
	}
	if (tGuishuCount > 0 && tQGuishuCount > 0) {
		alert("处理的中介机构中，存在已归属和未归属两种状态，不能同时处理！");
		return false;
	}
	if (tErrors != "") {
		alert("进行处理时：\n" + tErrors);
		return false;
	}
	return true;
}

// 点击查询 将符合要求的险种显示出来
function queryAgent() {
	initAgentGrid();
	var tTJ = "";
	if (fm.StateFlag.value == 'Y') {
		tTJ = "and exists (select 1 from lacomectype where agentcom = lac.agentcom and agentcomtype = '01') ";
	}
	if (fm.StateFlag.value == 'N') {
		tTJ = "and not exists (select 1 from lacomectype where agentcom = lac.agentcom and agentcomtype = '01') ";
	}

	var strSql = "select lac.managecom,"
			+ "(select name from ldcom where comcode = lac.managecom),"
			+ "ldc1.codealias,"
			+ "(select codename from ldcode where codetype = 'actype' and code = lac.actype),"
			+ " lac.agentcom,lac.name,"
			+ "(case when lac.SellFlag = 'N' then '否' when lac.sellflag = 'Y' then '是' end),"
			+ "(case when lac.endflag = 'Y' then '无效' when lac.endflag = 'N' then '有效' end),"
			+ "(case when exists (select 1 from lacomectype where agentcom = lac.agentcom and agentcomtype = '01') then '是' else '否' end ), "
			+ "ldc1.code,"// 中介所属销售渠道编码
			+ "(select code from ldcode where codetype = 'actype' and code = lac.actype)"// actype编码
			+ "from lacom lac "
			+ "inner join ldcode1 ldc1 on lac.branchtype = ldc1.code1 and lac.branchtype2 = ldc1.codename "
			+ " where 1=1 and ldc1.codetype = 'comsalechnl' and  ldc1.code !='04' and  ldc1.code !='08'"
			+ tTJ // 处理是否已归属 由 if逻辑进行判断取值
			+ getWherePart('ManageCom', 'ComCode', 'like') // 管理机构
			+ getWherePart('ldc1.code', 'SellFlag') // 中介所属销售渠道
			+ getWherePart('AcType', 'ACType') // 中介机构类型
			+ getWherePart('AgentCom', 'AgentCom') // 中介机构编码
			+ getWherePart('Name', 'Name', 'like') // 中介机构名称
	;

	var strResult = easyExecSql(strSql);
	if (!strResult) {
		alert("未查询到已配置的该中介机构的信息！");
		return false;
	}
	turnPage1.queryModal(strSql, AgentGrid);
}