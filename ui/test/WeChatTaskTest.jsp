<%@page contentType="text/html;charset=GBK"%>
<%
	//程序名称：WeChatTaskTest.jsp
	//程序功能：微信提醒批处理测试
	//创建日期：2017-09-13 10:10
	//创建人  ：LZJ
	//更新记录：更新人    更新日期     更新原因/内容
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="WeChatTaskTest.js"></SCRIPT>
		<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
     
     	<title>微信提醒批处理测试</title>       
     </head>
        
     <body>
     	<form action="WeChatTaskTestSave.jsp" name = "fm" target="fraSubmit">
	     	<input value='发送消息' type='submit'> 
     	</form>
     </body>
</html>