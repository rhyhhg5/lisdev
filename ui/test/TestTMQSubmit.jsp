<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
	//程序名称：AllPBqQuerySubmit.jsp
	//程序功能：
	//创建日期：2003-1-20
	//创建人  ：lh
	//更新记录：  更新人    更新日期     更新原因/内容
	//
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>

<%
	//页面有效区
	LCContStateSchema tLCContStateSchema = new LCContStateSchema();
	LCContStateSet tLCContStateSet = new LCContStateSet();
	TransferData tTransferData = new TransferData();
	TMQUI tTMQUI = new TMQUI();
	TMQBL tTMQBL = new TMQBL();
	String FlagStr = "";
	String Content = "";
	String tContNo = "";
	tContNo = request.getParameter("ContNo");
	String tStateType = "";
	String tPolNo = "";
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	String Operator = tGI.Operator; //保存登陆管理员账号
	String ManageCom = tGI.ComCode; //保存登陆区站,ManageCom内存中登陆区站代码
	CErrors tError = null;
	//System.out.println("进入submit.jsp");

	if (tGI == null) {
		System.out.println("页面失效,请重新登陆");
		FlagStr = "Fail";
		Content = "页面失效,请重新登陆";
	}

	//参数格式=” Inp+MulLine对象名+Sel” 
	//获取所需要的参数 tPolNo  tStateType
	String tChk[] = request.getParameterValues("InpTMQGridChk");
	for (int i = 0; i < tChk.length; i++) {
		if (tChk[i].equals("1")) {
			//System.out.println(i);
			//System.out.println(tContNo);
			String tGrid2[] = request.getParameterValues("TMQGrid2");
			tPolNo = tGrid2[i];
			String tGrid3[] = request.getParameterValues("TMQGrid3"); //得到序号列的所有值
			tStateType = tGrid3[i];
			System.out.println(tPolNo);
			System.out.println(tStateType);
			tLCContStateSchema.setContNo(tContNo);
			tLCContStateSchema.setPolNo(tPolNo);
			tLCContStateSchema.setStateType(tStateType);
			tLCContStateSet.add(tLCContStateSchema);
			System.out.println("tLCContStateSet"+tLCContStateSet);
		}
	}
	//准备传输数据 VData
	VData tVData = new VData();
	tVData.add(tLCContStateSet);
	tVData.add(tTransferData);
	tVData.add(tGI);
	//System.out.println(tVData);
	//后面要执行的动作：添加，修改，删除
	String transact = request.getParameter("Transact");
	try {
		if (transact.equals("UPDATE")) {
			//System.out.println("进入------");
			//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			if (!tTMQUI.submitData(tVData, transact)) {
				System.out.println("修改失败---tTMQUI.submitData(tVData, transact)错误");
				FlagStr = "Fail";
			} else {
				Content = " 修改回执回销日期成功！";
				FlagStr = "Succ";
			}
		}
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
