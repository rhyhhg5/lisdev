//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 
var turnPageItem = new turnPageClass(); 
var mainState="0";
var tContNo="";
var cPolNo="";
var arrDataSet;
var alertflag = "0"; //10-29
var value = "";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
var size;//查询数据的条条数
var count;//选中的行号
/*function reportDetailClick(){
	for(var i=0;i<size;i++){
		count[i] = TMQGrid.getChkNo(i);
		alert(count[i]);
	}
}*/

//失效终止按钮
function EndFail(){
	for(var i=0;i<size;i++){
		if(TMQGrid.getChkNo(i)){
			count=i;
			//alert(count);
			value=TMQGrid.getRowColDataByName(count,"StateType");
			tPolNo = TMQGrid.getRowColDataByName(count,"PolNo");
			//alert(value);
			//alert(tPolNo);
			if(value!="Terminate"){
				alert("该保单状态不符合要求");
				return false;
			}
			//检验是否查到的数据stateflag为3
			var sql = "select * from lccont a,lcpol b where a.stateflag='3' and a.stateflag=b.stateflag " +
			"and a.contno='"+tContNo+"' and b.polno='"+tPolNo+"'";
			arrResult = easyExecSql(sql);    
			//alert("arrResult"+arrResult);
			if (arrResult == null) 
			{
				alert("查询失败！");
				return false;
			} 
			fm.all('Transact').value ="UPDATE";
			//alert("更改成功");
			fm.action = "./TestTMQSubmit.jsp";
			fm.submit();
		}
	}
}

//失效中止按钮
function CancelFail() {
	for ( var i = 0; i < size; i++) {
		if (TMQGrid.getChkNo(i)) {
			count = i;
			//alert(count);
			value = TMQGrid.getRowColDataByName(count, "StateType");
			tPolNo = TMQGrid.getRowColDataByName(count, "PolNo");
			//alert(value);
			//alert(tPolNo);
			if (value != "Available") {
				alert("该保单状态不符合要求");
				return false;
			}
			// 检验是否查到的数据stateflag为2
			var sql = "select * from lccont a,lcpol b where a.stateflag='2' and a.stateflag=b.stateflag "
					+ "and a.contno='"
					+ tContNo
					+ "' and b.polno='"
					+ tPolNo
					+ "'";
			arrResult = easyExecSql(sql);
			// alert("arrResult"+arrResult);
			if (arrResult == null) {
				alert("该保单状态不符合要求");
				return false;
			}

			fm.all('Transact').value = "UPDATE";
			// alert("更改成功");
			fm.action = "./TestTMQSubmit.jsp";
			fm.submit();
		}
	}
}

// 查询按钮
function easyQueryClick()
{
	tContNo=fm.all('ContNo').value;
	if(tContNo==''){
		alert("条件不能为空！");
	}
	// 初始化表格
	initTMQGrid();
	var strSQL = "";
	strSQL ="select a.contno,a.polno,a.statetype,a.startdate from lccontstate a " +
			"where a.contno='"+tContNo+"'" +
			"and exists (select * from lcpol where contno=a.contno and stateflag in('2','3')) " +
			"with ur";
	
	//查询SQL，返回结果字 符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
//alert(turnPage.strQueryResult);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
       if(alertflag!="1")     //
          alert("查询失败,数据库中无此记录！"); //10-29  yz
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = TMQGrid; 
          
  //保存SQL语句
  turnPage.strQuerySql = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  size = arrDataSet.length;
  //alert("size="+size);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function afterSubmit( FlagStr, content,Result )
{
	//alert("------------afterSubmit---------");
  if (FlagStr == "Fail" )
  {   
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	         
				fm.all('ContNo').value ="";	  
				if(fm.all("Transact").value=="UPDATE") //修改內容
				{ 	
				initForm();	
				alert("操作成功");
				//easyQueryClick();
			 }
			    
			    /*var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");*/  
  }
  	
  
}
