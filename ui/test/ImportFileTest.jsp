<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletRequestContext" %>

<%
    System.out.println("TaxListImportSave.jsp Begin ...");

//    String tApplyBatchNo = request.getParameter("BatchNo");
    GlobalInput tG = (GlobalInput)session.getValue("GI");

    String FlagStr = "Fail";
    String Content = "";

/** 文件上传成功标志。 */
    boolean tSucFlag = false;

/** 是否可以上传文件标志。 */
    boolean tCanUploadFlag = false;

    String mImportPath = null;
    String mFileName = null;

//    String tTaxImportDir = new ExeSQL().getOneValue("select sysvarvalue from LDSysVar where SysVar = 'TaxImportDir'");

    String tBatchNo = "";

//    if(tTaxImportDir!=null && !"".equals(tTaxImportDir)){
//        tCanUploadFlag = true;
//    }else{
//        Content = "未找到清单上传临时目录！";
//    }

    mImportPath = application.getRealPath("").replace('\\','/') + '/';
//    mImportPath += tTaxImportDir;
    mImportPath=mImportPath+"temp/TaxImportTmp/";
    System.out.println("mImportPath:" + mImportPath);
    System.out.println("...开始上载文件");

//Initialization
    DiskFileUpload fu = new DiskFileUpload();
    DiskFileItemFactory factory = new DiskFileItemFactory();
    factory.setSizeThreshold(4096);

    //设置当内容大于内存缓冲区的域值时，设置文件的存储位置。
    factory.setRepository(new File(mImportPath));
    //ServletFileUpload upload = new ServletFileUpload(factory);
    //DiskFileUpload upload = new DiskFileUpload(factory);
    //设置上传文件的最大值。
    //upload.setSizeMax(1000000);
    System.out.println("...fu");
//设置允许用户上传文件大小,单位:字节
    fu.setSizeMax(10000000);
    System.out.println("...fu1");
//maximum size that will be stored in memory?
//设置最多只允许在内存中存储的数据,单位:字节
    fu.setSizeThreshold(4096);
    System.out.println("...fu2");
//设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
    fu.setRepositoryPath(mImportPath);
    System.out.println("...fu3");

//开始读取上传信息
    List fileItems = null;
    try{
        System.out.println(new ServletRequestContext(request));
        System.out.println(request.getParameter("FileName"));
        //System.out.println(upload.parseRequest(new ServletRequestContext(request)).toString());
        //List /* FileItem */ tt=upload.parseRequest(new ServletRequestContext(request));
//        fileItems = upload.parseRequest(new ServletRequestContext(request));
System.out.println("=====开始了 =====");
fileItems = fu.parseRequest(request);
System.out.println("=====过去了 =====");
        //System.out.println(tt);
        System.out.println("ttttttttttt");
        System.out.println("...fu4");
        tSucFlag = true;
    }catch(Exception e){
        System.out.println("...fu5");
        tSucFlag = false;
        e.printStackTrace();

        System.out.println("...fu5");
    }

// 依次处理每个上传的文件
    System.out.println("开始在服务器上创建文件");

    Iterator iter = fileItems.iterator();
    while(iter.hasNext()){
        FileItem item = (FileItem)iter.next();
        //忽略其他不是文件域的所有表单信息
        if(!item.isFormField()){
            String name = item.getName();
            System.out.println("name:" + name);

            long size = item.getSize();

            if(name==null || "".equals(name) && size==0){
                continue;
            }

            mFileName = name.substring(name.lastIndexOf("\\") + 1);
            
            System.out.println("mFileName==="+mFileName);

            //保存上传的文件到指定的目录
            try{
                item.write(new File(mImportPath + mFileName));
                tSucFlag = true;
            }catch(Exception e){
                tSucFlag = false;
                e.printStackTrace();
                System.out.println("upload file error ...");
            }
        }
    }

    if(tSucFlag){
//        tBatchNo = mFileName.substring(0, mFileName.lastIndexOf("."));
//        if(tBatchNo!=null && tBatchNo.equals(tApplyBatchNo)){
//            VData tVData = new VData();
//            TransferData tTransferData = new TransferData();
//            tTransferData.setNameAndValue("FileName",mFileName);
//            tTransferData.setNameAndValue("FilePath",mImportPath);
//            tTransferData.setNameAndValue("BatchNo",tBatchNo);
//            tVData.add(tTransferData);
//            tVData.add(tG);
//            //需要创建类，先用着
//            AddTaxList tAddTaxList = new AddTaxList(tBatchNo,tG);
//            if(!tAddTaxList.doAdd(mImportPath,mFileName)){
//                Content = " 导入清单失败! 原因详见“导入批次错误日志”！";
//                FlagStr = "Fail";
//            }else{
                Content = " 导入清单成功！";
                FlagStr = "Succ";
//            }
//        }else{
//            Content = " 批次号与上传文件名不符，请修改模版命名规则后，尝试重新上传！";
//            FlagStr = "Fail";
//        }
        System.out.println("导入清单成功！");
    }

    System.out.println("TaxListImportSave.jsp End ...");
%>

<%--<html>--%>
<%--<script language="javascript">--%>
    <%--parent.fraInterface.afterImportTaxList("<%=FlagStr%>", "<%=Content%>", "<%=tBatchNo%>");--%>
<%--</script>--%>
<%--</html>--%>