/*
  //创建人：韦力
*/
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var orderSql = "";
var sql="";


function checkDate()
{
	if(!isDate(fm.StartDate.value))
	{
			alert("起始日是必录且应为日期格式yyyy-mm-dd")
			return false;
	}
  
  if(!isDate(fm.EndDate.value))
 	{
 		alert("起始日是必录且应为日期格式yyyy-mm-dd")
 		return false;
 	}
 	
	return true;
}
function displayQueryResult(strResult) 
{
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置

  strResult = Conversion(strResult);
  var filterArray          = new Array(0,12,1);

  //保存查询结果字符串
  turnPage.strQueryResult  = strResult;

  //使用模拟数据源
  turnPage.useSimulation   = 1;

  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  //alert(turnPage.arrDataCacheSet);


  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BillGrid;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) 
  {
    try 
    { 
      window.divPage.style.display = ""; 
    } catch(ex) 
    { }
  } 
  else 
  {
    try 
    { 
      window.divPage.style.display = "none"; 
    } catch(ex) 
    { }
  }

  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;

}


function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;

    showInfo.close();
    if (FlagStr == "Fail" )
    {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    }
    else
    {
    //	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		//	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    	showDiv(inputButton,"false");

    	//执行下一步操作
    }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}

//根据起始日期进行查询出要该日期范围内的成功件数
function showSerialNo()
{
  var manageComCheck = fm.ManageCom.value;
  if(manageComCheck.indexOf(ComCode) < 0)
  {
    alert("您只能操作本机构及以下机构的保单");
    return false;
  }
  
	if(!checkDate())
	{
		return false;
	}
	
	var codeType;
	if(mLoadFlag == "G")
	{
	  codeType = fm.GrpCode.value;
	}
	else
	{
	  codeType = fm.Code.value;
	}
	
	var codeStr = null;
	if (codeType == "0" || codeType == "" || codeType == null) 
	{
	  if(mLoadFlag != "G")
	  {
	    codeStr = " AND ( a.Code = '42' OR a.Code = '21' ) ";
	  }
		else
		{
		  codeStr = " AND ( a.Code = '42' OR a.Code = '21' OR a.Code = '58') "
		}
	}
	else
	{
	  codeStr = " AND a.Code = '" + codeType + "' ";
	}
	
  queryShort();
  
  var strSQL = "";
  
	var agentSQL = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		agentSQL = " and a.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
	}
  
  if(mLoadFlag != "G")
  {
    strSQL = "select varchar(c.MakeDate), b.manageCom, b.agentGroup, b.contNo, b.appntName, "
        + "   (select mobile from LCAddress where customerNo = b.appntNo and addressNo = d.addressNo), "
        + "   (select postalAddress from LCAddress where customerNo = b.appntNo and addressNo = d.addressNo),"
        + "   c.getNoticeNo, varchar(c.sumDuePayMoney), varchar(b.payToDate), varchar(c.payDate),"
        + "   (select codeName from LDCode where codeType = 'paymode' and code = b.payMode), "
        + "   a.prtSeq, a.standbyFlag1, "
        + "   (select codeName from LDCode where codeType = 'pausecode' and code=a.code), "
        + "   getUniteCode(b.agentCode), (select mobile from LAAGent where agentCode = b.agentCode), "
        + "   (select name from LAAGent where agentCode = b.agentCode) "
        + "   ,a.makedate,(case when (select agentstate from laagent where agentcode=b.agentcode)>='06' then '孤儿单' else '业务员在职' end)  "
        + "from LOPRTManager a, LCCont b, LJSPayB c, LCAppnt d "
        + "where a.otherNo = b.ContNo "
        + "   and b.contNo = c.otherNo "
        + "   and b.contNo = d.contNo "
        + "   and a.standbyFlag3 = c.getNoticeNo ";
    strSQL = strSQL 
        + "   and a.ManageCom like '" + fm.ManageCom.value + "%%' "
        + getWherePart( 'a.MakeDate','StartDate','>=' )
	      + getWherePart( 'a.MakeDate','EndDate','<=')
//	      + getWherePart( 'b.AgentCode','AgentCode')
	      + agentSQL
	      + codeStr;
 	  
 	  
 	  //查询满期保单
 		strSQL = strSQL
 		    + "union "
 		    + "select '', b.manageCom, b.agentGroup, b.contNo, b.appntName, "
        + "   (select mobile from LCAddress where customerNo = b.appntNo and addressNo = d.addressNo), "
        + "   (select postalAddress from LCAddress where customerNo = b.appntNo and addressNo = d.addressNo),"
        + "   '', '', '', '',"
        + "   (select codeName from LDCode where codeType = 'paymode' and code = b.payMode), "
        + "   a.prtSeq, a.standbyFlag1, "
        + "   (select codeName from LDCode where codeType = 'pausecode' and code=a.code), "
        + "   getUniteCode(b.agentCode), (select mobile from LAAGent where agentCode = b.agentCode), "
        + "   (select name from LAAGent where agentCode = b.agentCode) "
        + "   ,a.makedate,(case when (select agentstate from laagent where agentcode=b.agentcode)>='06' then '孤儿单' else '业务员在职' end)  "
        + "from LOPRTManager a, LCCont b, LCAppnt d "
        + "where a.otherNo = b.ContNo "
        + "   and b.contNo = d.contNo "
        + "   and a.code = '21'";
    strSQL = strSQL 
        + "   and a.ManageCom like '" + fm.ManageCom.value + "%%' "
        + getWherePart( 'a.MakeDate','StartDate','>=' )
	      + getWherePart( 'a.MakeDate','EndDate','<=')
//	      + getWherePart( 'b.AgentCode','AgentCode')
	      + agentSQL
	      + codeStr;;
   }
   else
   {
     strSQL = "select varchar(c.MakeDate), b.manageCom, b.agentGroup, b.GrpcontNo, b.grpName, "
        + "   (select phone1 from LCGrpAddress where customerNo = b.appntNo and addressNo = d.addressNo), "
        + "   (select postalAddress from LCGrpAddress where customerNo = b.appntNo and addressNo = d.addressNo),"
        + "   c.getNoticeNo, varchar(c.sumDuePayMoney), varchar(c.startPayDate), varchar(c.payDate),"
        + "   (select codeName from LDCode where codeType = 'paymode' and code = b.payMode), "
        + "   a.prtSeq, a.StandbyFlag1, "
        + "   (select codeName from LDCode where codeType = 'pausecode' and code=a.code), "
        + "   getUniteCode(b.agentCode), (select mobile from LAAGent where agentCode = b.agentCode), "
        + "   (select name from LAAGent where agentCode = b.agentCode)"
        + "   ,a.makedate,(case when (select agentstate from laagent where agentcode=b.agentcode)>='06' then '孤儿单' else '业务员在职' end)  "
        + "from LOPRTManager a, LCGrpCont b, LJSPayB c, LCGrpAppnt d "
        + "where a.otherNo = b.GrpContNo "
        + "   and b.GrpcontNo = d.GrpcontNo "
        + "   and a.standbyFlag3 = c.getNoticeNo ";
        
      strSQL = strSQL 
        + "   and a.ManageCom like '" + fm.ManageCom.value + "%%' "
        + getWherePart( 'a.MakeDate','StartDate','>=' )
	      + getWherePart( 'a.MakeDate','EndDate','<=')
//	      + getWherePart( 'b.AgentCode','AgentCode')
	      + agentSQL
	      + codeStr;
 	  
 	  
 	    //查询满期保单
 		  strSQL = strSQL
        
        + "union "
        
        
        //查询满期保单
        + "select '', b.manageCom, b.agentGroup, b.GrpcontNo, b.grpName, "
        + "   (select phone1 from LCGrpAddress where customerNo = b.appntNo and addressNo = d.addressNo), "
        + "   (select postalAddress from LCGrpAddress where customerNo = b.appntNo and addressNo = d.addressNo),"
        + "   '', '', '', '',"
        + "   (select codeName from LDCode where codeType = 'paymode' and code = b.payMode), "
        + "   a.prtSeq, a.StandbyFlag1, "
        + "   (select codeName from LDCode where codeType = 'pausecode' and code=a.code), "
        + "   getUniteCode(b.agentCode), (select mobile from LAAGent where agentCode = b.agentCode), "
        + "   (select name from LAAGent where agentCode = b.agentCode)"
        + "   ,a.makedate,(case when (select agentstate from laagent where agentcode=b.agentcode)>='06' then '孤儿单' else '业务员在职' end)  "
        + "from LOPRTManager a, LCGrpCont b, LCGrpAppnt d "
        + "where a.otherNo = b.GrpContNo "
        + "   and b.GrpcontNo = d.GrpcontNo "
        + "   and a.code = '21'";
        
      
    strSQL = strSQL 
        + "   and a.ManageCom like '" + fm.ManageCom.value + "%%' "
        + getWherePart( 'a.MakeDate','StartDate','>=' )
	      + getWherePart( 'a.MakeDate','EndDate','<=')
//	      + getWherePart( 'b.AgentCode','AgentCode')
	      + agentSQL
	      + codeStr;
   }
   
   strSQL = "select * from (" + strSQL + ") as zgm" 
 				+ orderSql;
  
	sql=strSQL;
	
  initBillGrid();
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, BillGrid);
	
	if(BillGrid.mulLineCount == 0)
	{
	  alert("没有查询到符合条件的数据");
	  return false;
	}
	
	showCodeNameManageCom();
	showCodeNameAgentGroup();
}

 //将管理机构编码显示为下面的格式汉字：二级机构名称——三级机构名称
function showCodeNameManageCom()
{
  for(var i = 0; i < BillGrid.mulLineCount; i++)
	{
	  var manageCom = BillGrid.getRowColDataByName(i, "ManageCom");
	  var sqlTemp = "select name from LDCom ";
	  
	  var sql3 = sqlTemp + "where comCode = '" + manageCom + "' ";  //查询3级机构
	  var manageComName3 = easyExecSql(sql3);
	  
	  var sql2 = sqlTemp + "where comCode = '" + manageCom.substring(0, 4) + "' ";  //查询2级机构
	  var manageComName2 = easyExecSql(sql2);
	  if(manageComName2 == null || manageComName2 == "null")
	  {
	    manageComName2 = "";
	  }
	  
	  BillGrid.setRowColDataByName(i, "ManageCom", (manageComName2 + " " + manageComName3));
	}
}

//将营销部门编码显示为下面格式汉字：营业部名称（营销服务部）－营业区名称－营业处名称
//若团单只有营业部
function showCodeNameAgentGroup()
{
  for(var i = 0; i < BillGrid.mulLineCount; i++)
	{
	  var agentGroup = BillGrid.getRowColDataByName(i, "AgentGroup");
	  
	  var agentGroupName3 = "";  //查询营业处（团单为营业部）
	  var agentGroupName2 = "";  //查询营业区
	  var agentGroupName1 = "";  //查询营业部
	  
	  var sqlTemp = "select Name, upBranch from LABranchGroup ";
	  
	  //查询营业处（团单为营业部）
	  var sql3 = sqlTemp + "where agentGroup = '" + agentGroup + "' ";  
	  var rs = easyExecSql(sql3);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    agentGroupName3 = rs[0][0];
	    var upBranch = rs[0][1];
	    
	    //查询营业区
	    var sql2 = sqlTemp + "where agentGroup = '" + upBranch + "' ";  
	    rs = easyExecSql(sql2);
	    if(rs && rs[0][0] != "" && rs[0][0] != "null")
  	  {
  	    agentGroupName2 = rs[0][0];
  	    upBranch = rs[0][1];
  	    
  	    //查询营业部
  	    var sql1 = sqlTemp + "where agentGroup = '" + upBranch + "' ";  
  	    rs = easyExecSql(sql1);
  	    if(rs && rs[0][0] != "" && rs[0][0] != "null")
    	  {
    	    agentGroupName1 = rs[0][0];
    	  }
  	  }
	  }
	  
	  BillGrid.setRowColDataByName(i, "AgentGroup", 
	    (agentGroupName1 + " " + agentGroupName2 + " " + agentGroupName3));
	}
}

//根据选中的批次号、银行编码、管理机构进行查询并且执行打印功能；
function printBill()
{
	queryShort();
	
	if(!checkDate())
	{
		return false;
	}
	
	if (BillGrid.mulLineCount == 0)
	{
		alert("没有需要打印的清单");
		return false;
	}else
	
			fm.all("sql").value=sql;
			if(mLoadFlag == "G")
	         {
              fm.action = "NewPPolPauseListPrint.jsp?LF=G";
	         }
	         else
	         {
	         	fm.action = "NewPPolPauseListPrint.jsp?LF=H";
	         }
			
			fm.target = "_blank";
			fm.submit();
}


//排序
function queryShort()
{	
		orderSql ="";
	
    //排序1
    switch(fm.all('Order1').value)
    {
		// 投保人
		case '0':
		orderSql = " Order By 5";
		break;
		//失效日期
		case '1':				
		orderSql = " Order By 19";
		break;
		//业务员
		case '2':
		orderSql = " Order By 16";
		break;
		//业务部门
		case '3':
		orderSql = " Order By 2";
		break;
    }
    //排序2		   
    switch(fm.all('Order2').value)
    {
		// 投保人
		case '0':
		orderSql = orderSql + ", 5";
		break;
		//失效日期
		case '1':				
		orderSql = orderSql + ", 19";
		break;
		//业务员
		case '2':
		orderSql = orderSql + ",16";
		break;
		//业务部门
		case '3':
		orderSql = orderSql + ",2";
		break;
    }
    //排序3
    switch(fm.all('Order3').value)
    {
		// 投保人
		case '0':
		orderSql = orderSql + ", 5";
		break;
		//失效日期
		case '1':				
		orderSql = orderSql + ", 19";
		break;
		//业务员
		case '2':
		orderSql = orderSql + ",16";
		break;
		//业务部门
		case '3':
		orderSql = orderSql + ",2";
		break;
   }
	   
}

//选择打印通知书
function printNotice()
{
  if(BillGrid.mulLineCount == 0)
  {
    alert("请先查询");
    return false;
  }
  
  var count = 0;   //选中的保单数
  var prtSeq = "";
  for (i = 0; i < BillGrid.mulLineCount; i++)
	{
		if (BillGrid.getChkNo(i)) 
		{
			count = count + 1;
			prtSeq = BillGrid.getRowColDataByName(i, "PrtSeq");
		}
	}
	
	if(count == 0)
	{
	  alert("请选择保单");
	  return false;
	}
	
	//单打
	if(count == 1)
	{
	  fm.action = "PrintPauseNotice.jsp?PrtSeq=" + prtSeq;
	  fm.target = "_blank";
	  fm.submit();
	}
  else
  {
    alert("尚未实现，请选择一个保单进行打印。");
  }
}

//全部打印通知书
function printNoticeAll()
{
  alert("尚未实现");
}