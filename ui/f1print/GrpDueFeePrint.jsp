<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskGetPrint.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
		System.out.println("start");
		CError cError = new CError( );
		boolean operFlag=true;
		String tRela  = "";
		String FlagStr = "";
		String Content = "";
		String strOperation = "";
		String GrpContNo="";
		
		String GetNoticeNo=request.getParameter("GetNoticeNo");
		LJSPayBDB  tLJSPayBDB=new LJSPayBDB();
		tLJSPayBDB.setGetNoticeNo(GetNoticeNo);
		
			if(tLJSPayBDB.getInfo())
			{
				GrpContNo=tLJSPayBDB.getOtherNo();
		  }else
					{
						Content="没有数据";
					}
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput)session.getValue("GI");
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		
		tLCGrpContSchema.setGrpContNo(GrpContNo);
		System.out.println("tLCGrpContSchema.getGrpContNo():" + tLCGrpContSchema.getGrpContNo());
		
		VData mResult = new VData();
		CErrors mErrors = new CErrors();
		
		VData tVData = new VData();
		tVData.addElement(tLCGrpContSchema);
		tVData.addElement(tG);
		tVData.addElement(tLJSPayBDB);
		
		GrpDueFeePrintBL tGrpDueFeePrintBL = new GrpDueFeePrintBL();
		XmlExport txmlExport = new XmlExport();
    if(!tGrpDueFeePrintBL.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tGrpDueFeePrintBL.mErrors.getFirstError().toString();
    }
    else
    {
			mResult = tGrpDueFeePrintBL.getResult();
			txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
			if(txmlExport==null)
			{
				operFlag=false;
				Content="没有得到要显示的数据文件";
			}
		}

ExeSQL tExeSQL = new ExeSQL();
//获取临时文件名
String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
String strFilePath = tExeSQL.getOneValue(strSql);
String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
//获取存放临时文件的路径
//strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
//String strRealPath = tExeSQL.getOneValue(strSql);
String strRealPath = application.getRealPath("/").replace('\\','/');
String strVFPathName = strRealPath + "//" +strVFFileName;

CombineVts tcombineVts = null;	
if (operFlag==true)
{
	//合并VTS文件
	String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
  System.out.println("-------------------------");
	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
	tcombineVts.output(dataStream);

	//把dataStream存储到磁盘文件
	System.out.println("存储文件到"+strVFPathName);
	AccessVtsFile.saveToFile(dataStream,strVFPathName);
	System.out.println("==> Write VTS file to disk ");

	System.out.println("===	 : "+strVFFileName);
	//本来打算采用get方式来传递文件路径
	response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
	
	}	else	
	{
    	FlagStr = "Fail";

%>

	<html>
	
	 <script language="javascript">
		alert("<%=Content%>");
		top.close();
	</script>
	</html>
<%
  }
%>
