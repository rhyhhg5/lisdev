<%
//程序名称：LAChannelWeekInput.jsp
//程序功能：机构渠道周报表
//创建日期：2008-4-14
//创建人  ：sy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>   
<%
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%> 
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAChannelWeekInput.js"></SCRIPT>   
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAChannelWeekInit.jsp"%>     
  <title>机构渠道周报表</title>
</head>      
<body  onload="initForm();initElementtype();" >    
  <form action= "./LAChannelWeekReport.jsp" method=post name=fm target="fraSubmit">
  <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		查询统计条件
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
   <Table class= common>
     <TR class= common> 
          <TD  class= title>保费口径</TD>
          <TD  class= input>
            <Input  class="codeno" name=PremType verify="保费口径|NOTNULL" CodeData = "0|^0|有效保费|^1|承保保费|^2|撤保保费"
       		 ondblclick = "return showCodeListEx('PremType',[this,PremTypeName],[0,1]);"
       		 onkeyup = "return showCodeListKeyEx('PremType',[this,PremTypeName],[0,1]);"><Input class="codename" name= PremTypeName elementtype=nacessary>
          </TD>
          <td class= title>统计层级</td>
          <TD class=input>
         		<Input  class="codeno" name=Level verify="统计层级|NOTNULL" CodeData = "0|^0|分公司层级|^1|支公司层级"
       		 ondblclick = "return showCodeListEx('Level',[this,LevelName],[0,1]);"
       		 onkeyup = "return showCodeListKeyEx('Level',[this,LevelName],[0,1]);"><Input class="codename" name= LevelName elementtype=nacessary>
          </TD>
     </TR>
     <tr class=common>
		      <TD  class= title>统计起期</TD>
		      <TD  class= input>
		       <Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="统计起期|NOTNULL" elementtype=nacessary>
		      </TD>
       		<TD  class= title>统计止期</TD>
       		<TD  class= input>
       		  <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="统计止期|NOTNULL" elementtype=nacessary>
       		</TD>
     </TR>
     <tr class=common>
			    <TD  class= title>交费方式</TD>
       		<TD  class= input>
       		 <Input  class="codeno" name=PayIntv  CodeData = "0|^0|趸交|^1|期交"
       		 ondblclick = "return showCodeListEx('PayIntv',[this,PayIntvName],[0,1]);"
       		 onkeyup = "return showCodeListKeyEx('PayIntv',[this,PayIntvName],[0,1]);"><Input class="codename" name= PayIntvName readonly=true>
       		</TD>
       		<TD class=title>交费年限</TD>
       		<td class=input>
       		<Input class="codeno" name = PayYears  CodeData = "0|^0|3年|^1|5年|^2|8年|^3|10年|^4|全部"
       		 ondblclick = "return showCodeListEx('PayYears',[this,PayYearsName],[0,1]);"
       		 onkeyup = "return showCodeListKeyEx('PayYears',[this,PayYearsName],[0,1]);"><Input class="codename" name= PayYearsName readonly=true>
       		 </td>       		
     </TR>    
     <tr class=common>
			    <TD  class= title>险种</TD>
       		<TD  class= input>
       		 <input class="codeno" name = RiskCode 
          ondblclick="return showCodeList('riskcode',[this,RiskName],[0,1],null,null,null,null,300);"
          onkeyup="return showCodeListKey('riskcode',[this,RiskName],[0,1]);"><Input name=RiskName readOnly=true class=codename>
       		</TD>
       		<TD class=title></TD>
       		<td class=input>
       		</td>       		
     </TR>
   	</Table>  
   	</Div>
    <INPUT VALUE="打  印" class= cssbutton TYPE=button onclick="submitForm();"> 	
    <input type=hidden name=BranchType value=<%=BranchType%>>
    <input type=hidden name=BranchType2 value=<%=BranchType2%>>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html> 	
