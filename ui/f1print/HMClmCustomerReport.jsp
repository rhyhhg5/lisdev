<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：HMClmCustomerReport.jsp
//程序功能：被保险人理赔数据统计
//创建日期：2010-09-25
//创建人  ：caixd
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="HMClmCustomerReport.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<script>
	function initManageCom(){
    fm.ManageCom.value="<%=tG1.ManageCom%>";
    showOneCodeNametoAfter("comcode","ManageCom","ManageComName");
  }
</script>
</head>
<body onload="initManageCom();initElementtype();" >    
  <form action="./HMClmCustomerReportSave.jsp" method=post name=fm target="f1print">
    <table >
	<tr>
		<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQuery);">
		</td>
		<td class= titleImg>
			查询条件：
		</td>
	</tr>
  	</table>
  	<div id="divQuery">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
	          <TD  class= title>统计机构</TD>
	          <TD  class= input> 
	          	<Input class="codeno" name=ManageCom verify="统计机构|code:comcode&NOTNULL&INT" 
	          			ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
	          			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
	          	><input class=codename name=ManageComName elementtype=nacessary>
	          </TD>  
	          <TD  class= title>业务渠道</TD>
	          <TD  class= input> 
	          	<Input class="codeno" name=SaleChnl CodeData="0|2^A|团险-社保补充^B|团险-企业补充^C|团险-其他^D|个险业务"
	          			ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlComName],[0,1],null,null,null,1);" 
	          			onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlComName],[0,1],null,null,null,1);"
	          	><input class=codename name=SaleChnlComName >
	          </TD>
	          <TD  class= title>责任类型</TD>
	          <TD  class= input> 
	          	<Input class="codeno" name=DudyKind CodeData="0|2^1|住院费用型^2|住院定额型^5|门急诊费用型^6|社会医疗补充类型^8|其他"
	          			ondblclick="return showCodeListEx('DudyKind',[this,DudyKindName],[0,1],null,null,null,1);" 
	          			onkeyup="return showCodeListKeyEx('DudyKind',[this,DudyKindName],[0,1],null,null,null,1);"
	          	><input class=codename name=DudyKindName >
	          </TD>
	        </TR>
	           
	        <TR  class= common> 
	          <TD class= title>险    种</TD>
	          <TD class= input> 
	          	<Input class="codeno" name=RiskCode
	          			ondblclick="return showCodeList('ylriskcode',[this,RiskCodeName],[0,1],null,null,null,1);" 
	          			onkeyup="return showCodeListKey('ylriskcode',[this,RiskCodeName],[0,1],null,null,null,1);"
	          	><input class=codename name=RiskCodeName >
	          </TD>
	          <TD class= title>病    种</TD>
	           <TD class= input> 
	          	<Input class="codeno" name=Disease 
	          			ondblclick="return showCodeList('lldiseas',[this,DiseaseName],[1,0],null,fm.DiseaseName.value,'ICDName',1);" 
	          			onkeyup="return showCodeListKey('lldiseas',[this,DiseaseName],[1,0],null,fm.DiseaseName.value,'ICDName',1);"
	          	><input class=codename name=DiseaseName >
	          </TD>
	          <TD class= title>医    院</TD>
	          <TD class= input> 
	          	<Input class="codeno" name=Hospit 
	          			ondblclick="return showCodeList('llhospiquery',[this,HospitName],[0,1],null,fm.HospitName.value,'hospitname',1);" 
	          			onkeyup="return showCodeListKey('llhospiquery',[this,HospitName],[0,1],null,fm.HospitName.value,'hospitname',1);"
	          	><input class=codename name=HospitName >
	          </TD>
	        </TR>
	        
	        <TR  class= common> 
	          <TD class= title>业务员姓名</TD>
	          <TD class= input><Input class=common name="AgentCode"></TD>	
	          <TD class= title>被保险人黑名单</TD>
	          <TD class= input> 
	          	<Input class="codeno" name=BlackCus CodeData="0|2^1|是^2|否"
	          			ondblclick="return showCodeListEx('BlackCus',[this,BlackCusName],[0,1],null,null,null,1);" 
	          			onkeyup="return showCodeListKeyEx('BlackCus',[this,BlackCusName],[0,1],null,null,null,1);"
	          	><input class=codename name=BlackCusName >
	          </TD>
	          <TD class= title>单位名称</TD>
    	      <TD class= input><Input class=common name="GrpName"></TD>	
	        </TR> 
	        
	        <TR  class= common>        	          
			  <TD  class= title>统计起期</TD>
	          <TD  class= input> <Input name=StartDate verify="统计起期|code:StartDate&NOTNULL&Date" 
	          class='coolDatePicker' dateFormat='short' elementtype=nacessary  > </TD> 
	          <TD  class= title>统计止期</TD>
	          <TD  class= input> <Input name=EndDate verify="统计止期|code:EndDate&NOTNULL&Date" 
	          class='coolDatePicker' dateFormat='short' elementtype=nacessary > </TD> 
	          <TD class= title>保单号码</TD>
    	      <TD class= input><Input class=common name="GrpContNo" ></TD>	          
	       	</TR>	       		       	
	    </table>
	    <table class= common>
	    <TR  class= common> 	            	    
    	     <TD class= title>住院定额型保险给付金额>=</TD>
    	     <TD class= input><Input class=common name="GiveMoney" verify="给付金额|code:GiveMoney&NUM"></TD>	        
    	     <TD class= title>费用型保险住院客户累计赔付>=</TD>
    	     <TD class= input><Input class=common name="HisMoney" verify="累计赔付|code:HisMoney&NUM"></TD>	    	     	     
	        </TR>
	        
	        <TR  class= common> 	         	
    	     <TD class= title>住院定额型保险住院天数>=</TD>
    	     <TD class= input><Input class=common name="GiveDate" verify="给付金额|code:GiveMoney&INT"></TD>	
    	     <TD class= title>保单年度住院次数>=</TD>
    	     <TD class= input><Input class=common name="hospitCount" verify="住院次数|code:hospitCount&INT"></TD>  
	        </TR>	        	       
	   </table>
	    <TR  class= common> 
	         <TD class= title>风险保额>=</TD>
    	     <TD class= input><Input class=common name="RiskAmnt" verify="风险保额|code:RiskAmnt&NUM"></TD>	
    	     <TD class= title>(1或1/2年收入住院客户)</TD>
    	</TR>
    </div>
	<br>
    <table class=common>
    	<tr class=common>
          <TD class= title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="downLoad()"></TD>
			</tr>
	</table>
		<hr>
	<table >
	<tr>
		<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRemark);">
		</td>
		<td class= titleImg>
			字段说明：
		</td>
	</tr>
  	</table>
	<div id="divRemark" style= "display: ''">
		<TD class= title>【客户号】：出险人客户号</TD><br>
		<TD class= title>【被保险人】：出险人姓名</TD><br>
		<TD class= title>【身份证号或出生日期】：出险人身份证号或出生日期</TD><br>
		<TD class= title>【客户有效性】：无法判断客户在统计区间内的有效性，暂时空着不处理</TD><br>
		<TD class= title>【联系电话】：投保人联系电话</TD><br>
		<TD class= title>【住址】：投保人住址</TD><br>
		<TD class= title>【投保企业（团险）】：团险保单的投保单位名称</TD><br>
		<TD class= title>【所属业务员】：保单所属业务员的姓名</TD><br>
		<TD class= title>【业务员编码】：保单所属业务员的编码</TD><br>
		<TD class= title>【业务员联系电话】：保单所属业务员的联系电话</TD><br>
		<TD class= title>【一年内赔付金额】：出险人最近一年内累计赔付金额</TD><br>
		<TD class= title>【一年内住院天数】：出险人最近一年内累计住院天数</TD><br>
		<TD class= title>【一年内住院次数】：出险人最近一年内累计住院次数</TD><br>

  	</div>
	<Input type="hidden" class= common name="RiskRate" >
	<input type="hidden" name=op value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 