<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LAStatisticReportInput.jsp
//程序功能：F1报表生成
//创建日期：2005-11-10 9:16
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LAStatisticReportInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();" >    
  <form action="./LAStatisticReport.jsp" method=post name=fm target="f1print">
    <table class= common border=0 width=100%>
      <TR class= common>
        <TD class= title>
           统计起期
        </TD>
        <TD class= input>
         <Input class= "coolDatePicker" dateFormat="short" name=StartDate  verify="起期|NOTNULL&DATE" elementtype=nacessary  >  
        </TD>
        <TD class= title>
           统计止期
        </TD>
        <TD class= input>
         <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="止期|NOTNULL&DATE" elementtype=nacessary >  
        </TD>
		  </TR>
    </table>

    <input type="hidden" name=Operate value="">
    <input type="hidden" name=name value="">
    <input type=hidden name=AgentGroup value=''>   
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="PolPrint()">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 