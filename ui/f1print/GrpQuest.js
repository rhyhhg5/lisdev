//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		     if(RePrintFlag==1){
				if(PolGrid.getRowColData(tSel-1,11)=="未打"){
					alert("该单已在打印池中，不需要发送重打！");
					return false;
					}
		}
		PrtSeq = PolGrid.getRowColData(tSel-1,1);
		
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tMissionID = PolGrid.getRowColData(tSel-1,13);
		tSubMissionID = PolGrid.getRowColData(tSel-1,14);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);
		tContNo = easyExecSql("select grpcontno from lcgrpcont where prtno='"+tPrtNo+"'");
		//alert(tContNo);
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.ContNo.value = tContNo ;
		if(RePrintFlag==1){
			fmSave.target="fraSubmit";
			}
		//fmSave.target = "../f1print";
		fmSave.action="./GrpQuestF1pSave.jsp";
		fmSave.fmtransact.value = "CONFIRM";
		fmSave.submit();
	}
	
}
//pdf提交，保存按钮对应操作
function printPolpdf()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		PrtSeq = PolGrid.getRowColData(tSel-1,1);
		
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tMissionID = PolGrid.getRowColData(tSel-1,9);
		tSubMissionID = PolGrid.getRowColData(tSel-1,10);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);
		tContNo = easyExecSql("select grpcontno from lcgrpcont where prtno='"+tPrtNo+"'");
		//alert(tContNo);
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.ContNo.value = tContNo ;
		fmSave.fmtransact.value = "PRINT";
		//fmSave.target = "../f1print";
		fmSave.action="../uw/PrintPDFSave.jsp?Code=054";
		fmSave.submit();
	}
}
function printvts()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		PrtSeq = PolGrid.getRowColData(tSel-1,1);
		
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tMissionID = PolGrid.getRowColData(tSel-1,13);
		tSubMissionID = PolGrid.getRowColData(tSel-1,14);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);
		tContNo = easyExecSql("select contno from lccont where prtno='"+tPrtNo+"' and conttype='1'");
		//alert(ContNo);
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.ContNo.value = tContNo ;
		fmSave.fmtransact.value = "PRINT";
		//fmSave.target = "../f1print";
		fmSave.action="../uw/UWIssue.jsp?viewflag=1";
		fmSave.submit();
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	if(!verifyInput2())
	return false;
	
	// 书写SQL语句
	var strSQL = "";		
	// 书写SQL语句
	var tSeltype="";
	if (fm.all('SelType').value=="0")
			{
				tSeltype="and a.PrtSeq not in (select doccode from es_doc_main where subtype='TB23' )";
			}
	else if(fm.all('SelType').value=="1")
			{
				tSeltype="and a.PrtSeq in (select doccode from es_doc_main where subtype='TB23' )";
			}
	else
			{
				tSeltype="";
			}
	var tStateFlag="";
	if(fm.all('StateFlag').value=="null"||fm.all('StateFlag').value==null||fm.all('StateFlag').value=="")
 	 		{
    			tStateFlag="";
 	 		}
  else{
			if (fm.all('StateFlag').value=="0")
	  			{
		  				tStateFlag=" and a.stateflag='0' ";
	  			}
	  	else if (fm.all('StateFlag').value=="9")
	  			{
		 	 				tStateFlag="";
	  			}
	  	else
	  			{
	  			tStateFlag=" and a.stateflag<>'0' ";
	  			}
			}
	strSQL = "SELECT a.PrtSeq,b.PrtNo,b.PolApplyDate,b.CValiDate,b.GrpName,b.prem,getUniteCode(b.Agentcode),b.ManageCom,a.makedate "
						+ ",(select makedate from es_doc_main where doccode=a.PrtSeq and subtype='TB23'),(case when a.stateflag='0' then '未打' else '已打' end),'','','',printtimes "
		        + " FROM loprtmanager a,lcgrpcont b WHERE a.otherno = b.grpcontno  "  
	        	+ "and a.Code = '54' "
	        	+ " and b.uwflag not in ('4','9')"
	        	+ getWherePart('b.PrtNo', 'PrtNo') 
		        + getWherePart('b.contno', 'ContNo') 
	    			+ getWherePart('b.ManageCom', 'ManageCom', 'like')
			    	+ getWherePart('getUniteCode(b.Agentcode)','AgentCode')
			    	+ getWherePart('b.AppntName','AppntName')
			    	+ getWherePart('a.Makedate','MakeDate')
			    	+ getWherePart('b.CvaliDate','CvaliDate')
			    	+ getWherePart('b.PolApplyDate','PolApplyDate')
			    	+ tSeltype
			    	+ tStateFlag
			    	+" with ur"
			      ;
	if(RePrintFlag == "1"){
		strSQL = "SELECT a.PrtSeq,b.PrtNo,b.PolApplyDate,b.CValiDate,b.GrpName,b.prem,getUniteCode(b.Agentcode),b.ManageCom,a.makedate "
						+ ",(select makedate from es_doc_main where doccode=a.PrtSeq and subtype='TB23'),(case when a.stateflag='0' then '未打' else '已打' end), "
						+ "'','','',printtimes"
		        + " FROM loprtmanager a,lcgrpcont b WHERE a.otherno = b.grpcontno  "  
	        	+ "and a.Code = '54' "
	        	+ " and b.uwflag not in ('4','9')"
	        	+ getWherePart('b.PrtNo', 'PrtNo') 
		        + getWherePart('b.contno', 'ContNo') 
	    			+ getWherePart('b.ManageCom', 'ManageCom', 'like')
			    	+ getWherePart('getUniteCode(b.Agentcode)','AgentCode')
			    	+ getWherePart('b.AppntName','AppntName')
			    	+ getWherePart('a.Makedate','MakeDate')
			    	+ getWherePart('b.CvaliDate','CvaliDate')
			    	+ getWherePart('b.PolApplyDate','PolApplyDate')
			    	+ tSeltype
			    	+ tStateFlag
			    	+ " with ur"
				    ;

	}
	turnPage.strQueryResult  = easyQueryVer3(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要打印的问题件通知书！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //tArr = chooseArray(arrDataSet,[0,1,3,4]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}


function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}

function printPolpdfNew()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		PrtSeq = PolGrid.getRowColData(tSel-1,1);
		
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tMissionID = PolGrid.getRowColData(tSel-1,9);
		tSubMissionID = PolGrid.getRowColData(tSel-1,10);
		tPrtNo = PolGrid.getRowColData(tSel-1,2);
		tContNo = easyExecSql("select grpcontno from lcgrpcont where prtno='"+tPrtNo+"'");
		//alert(tContNo);
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.PrtNo.value = tPrtNo;
		fmSave.ContNo.value = tContNo ;
		fmSave.fmtransact.value = "only";
		var showStr="正在准备打印数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fmSave.target = "fraSubmit";
		fmSave.action="../uw/PDFPrintSave.jsp?Code=54&OtherNo="+tContNo+"&OldPrtSeq="+tPrtSeq;;
		fmSave.submit();
	}
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}