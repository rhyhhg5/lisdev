<%
//程序名称：BankDataQuery.jsp
//程序功能：财务报盘数据查询
//创建日期：2004-10-20
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
%>


<html>    
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
  
  <SCRIPT src="./BaoBankDataQuery.js"></SCRIPT>   
  <%@include file="./BaoBankDataQueryInit.jsp"%>   
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>报盘数据查询 </title>
</head>      
 
<body  onload="initForm();initElementtype();" >
  <form method=post action=./BaoBankDataQueryListPrint.jsp name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>管理机构</TD>
          <TD  class= input>
            <Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
          </TD>
		      <TD  class= title>银行代码</TD>
		      <TD  class= input>
		        <Input CLASS="codeNo" name=BankCode verify="银行代码|notnull&code:Bank" ondblclick="return showCodeList('Bank',[this,BankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank', [this,BankName],[0,1],null,null,null,1);"><input class=codename name=BankName readonly=true >
		      </TD>
       		<TD  class= title width="25%">应缴起期</TD>
       		<TD  class= input width="25%">
       		  <Input class= "coolDatePicker" dateFormat="short" name=SendDate verify="应缴起期|NOTNULL" elementtype="nacessary">
       		</TD>
			<TD  class= title width="25%">应缴止期</TD>
       		<TD  class= input width="25%">
       		  <Input class= "coolDatePicker" dateFormat="short" name=SendEndDate verify="应缴止期|NOTNULL" elementtype="nacessary">
       		</TD>
     </TR>
     <TR  class= common>
          <TD  class= title>保单号</TD>
          <TD  class= input><Input class= common name=ContNo></TD>
          <TD  class= title>银行账号</TD>
          <TD  class= input><Input class= common name=BankAccNo></TD>
		  <TD  class= title>报盘状态</TD>
          <TD  class= input><Input class="codeNo" name="BankState" CodeData="0|^0|全部^1|已抽档未抽盘^2|已抽盘未发盘^3|已发盘未回盘^4|已回盘未处理^5|已回盘处理" verify="报盘状态|&code:BankState"  ondblclick="return showCodeListEx('BankState',[this,BankStateName],[0,1]);" onkeyup="return showCodeListEx('BankState',[this,BankStateName],[0,1]);"><Input class="codeName" name="BankStateName"  elementtype="nacessary" readonly></TD>
          <TD  class= title>回盘结果</TD>
          <TD  class= input><Input class="codeNo" name="BankSuccFlag" CodeData="0|^0|全部^1|成功^2|未成功" verify="回盘结果|&code:BankSuccFlag"  ondblclick="return showCodeListEx('BankSuccFlag',[this,BankSuccName],[0,1]);" onkeyup="return showCodeListEx('BankSuccFlag',[this,BankSuccName],[0,1]);"><Input class="codeName" name="BankSuccName"  elementtype="nacessary" readonly></TD>
     </TR>
      <TR  class= common>
		  <TD  class= title>销售渠道</TD>
          <TD  class= input><Input class="codeNo" name="SaleChnl" CodeData="0|^0|全部^1|个险^2|银保" verify="销售渠道|&code:SaleChnl"  ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><Input class="codeName" name="SaleChnlName"  elementtype="nacessary" readonly></TD>   
     </TR>
   	</Table>  
   	<p>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="strsql" name="strsql">
    <!--数据区-->
    <INPUT VALUE="查   询" class= cssbutton TYPE=button onclick="easyQuery()"> 	
	<INPUT VALUE="清单下载" class= cssbutton TYPE=button onclick="easyPrint()">
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCodeGrid);">
    		</td>
    		<td class= titleImg>
    			 报盘数据清单：
    	</tr>
    </table>
  	<Div  id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
									<span id="spanCodeGrid" >
									</span> 
		  				</td>
					</tr>
    		</table>
		<center>
      	<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage1.firstPage();"> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage1.previousPage(); "> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage1.nextPage(); "> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage1.lastPage(); "> 
		</center>
  	</div>
  	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divZhuShi);">
    		</td>
    		<td class= titleImg>
    			 相关注释：
    	</tr>
    </table>
  	<Div  id= "divZhuShi" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
									<span id="spanZhuShi" >
									     <font color=red>
												   1.已抽档未抽盘:已【续期抽档】但未【生成发盘数据】。</br>
										
										           2.已抽盘未发盘:已【生成发盘数据】但未【生成发盘文件】。</br> 
										
										           3.已发盘未回盘:已【生成发盘文件】但【发盘文件未返回】。</br>
										
										           4.已回盘未处理:已【发盘文件已返回】但未【返回处理】。</br>
										
										           5.已回盘处理:已【返回处理】，发回盘流程结束。</br>
										
										           6.抽盘日期:生成发盘数据时间。</br>
										
										           7.发盘日期:生成发盘文件时间。</br>
										
										           8.允许发盘开始时间:此续期应收记录允许财务抽盘的最早时间。</br>
										
										           9.允许发盘截止时间：此续期应收记录允许财务抽盘的最晚时间。</br>
										           
										           10.回盘时间:发盘文件已经返回但是没有做返回处理。</br>
										           
										           11.回盘处理时间:回盘处理完成时间，表示发回盘流程结束。</br>
					                       </font>				
									
									</span> 
		  				</td>
					</tr>
    	</table>
  	</div>
  	
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 	
<script>
	<!--选择机构：只能查询本身和下级机构-->
	var codeSql = "1  and code like #"+<%=Branch%>+"%#";
</script>