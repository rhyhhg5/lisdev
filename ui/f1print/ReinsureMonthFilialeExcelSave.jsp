
<jsp:directive.page
	import="com.sinosoft.lis.certify.SysOperatorNoticeBL" />
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：按险种打印操作员日结
	//程序功能：
	//创建日期：2013-9-18
	//创建人  ：yan
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
	System.out.println("start ReinsureMonthFilialeExcelSave...X9-ZBYJ");
	String sWageNo = "";
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	//输出参数
	CError cError = new CError();
	//后面要执行的动作：添加，修改，删除
	String FlagStr = "";
	String Content = "";
	String tOutXmlPath="";  //文件路径
	String strOperation = "";//用来判断是收费还是付费
	String strOpt = "";//LYS 用来记录是暂收还是预收
	strOperation = request.getParameter("fmtransact");
	strOpt = request.getParameter("Opt");
	if(strOpt.endsWith("Month")){
		sWageNo = request.getParameter("StartMonth");
	}
	
	//输出打印信息：
	System.out.println("X9-ZBYJ打印类型是:" + strOpt+";打印日期：" + sWageNo);
	System.out.println("Operator："+tG.Operator+";ManageCom:"+tG.ManageCom);
	
	VData tVData = new VData();	
	tVData.addElement(sWageNo);
	tVData.addElement(tG);

	//   X9-再保月结单（分公司） ReinsureFilialeMonth
	if (strOpt.equals("ReinsureFilialeMonth")) {
		System.out.println("strOpt的标志是:" + strOpt);
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
		String tDate=dateFormat.format(new Date());
		String tTime=timeFormat.format(new Date());
		String fileName=tDate.replace("-","")+tTime.replace(":","")+"-X9F-"+tG.Operator+".xls";
		System.out.println("文件名称："+fileName);
		tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
		System.out.println("文件路径:" + tOutXmlPath);
		
		tVData.addElement(tOutXmlPath);
		ReinsureMonthFilialeExcelBL tReinsureMonthFilialeExcelBL = new ReinsureMonthFilialeExcelBL();
		if (!tReinsureMonthFilialeExcelBL.submitData(tVData, strOperation)) {
		    System.out.println("报错信息：");
            Content = "报表下载失败，原因是:" + tReinsureMonthFilialeExcelBL.mErrors.getFirstError();
            FlagStr = "Fail";
		}
	}
	
      String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
      File file = new File(tOutXmlPath);
      
      response.reset();
      response.setContentType("application/octet-stream"); 
      response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
      response.setContentLength((int) file.length());
  
      byte[] buffer = new byte[10000];
      BufferedOutputStream output = null;
      BufferedInputStream input = null;    
      //写缓冲区
      try{
      	  output = new BufferedOutputStream(response.getOutputStream());
          input = new BufferedInputStream(new FileInputStream(file));
    
	      int len = 0;
	      while((len = input.read(buffer)) >0)
	      {
	          output.write(buffer,0,len);
	      }
	      input.close();
	      output.close();
      }
      catch (Exception e) 
      {
        e.printStackTrace();
       } // maybe user cancelled download
      finally 
      {
          if (input != null) input.close();
          if (output != null) output.close();
          file.delete();
      }
	  if (!FlagStr.equals("Fail"))
	  {
	    Content = "打印报表成功！";
	    FlagStr = "Succ";
	  }
%>
<html>
   <script language="javascript">  
    alert('<%=Content %>');
    top.close();
   </script>
</html>