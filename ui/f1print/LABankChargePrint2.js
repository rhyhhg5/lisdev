//               该文件中包含客户端需要处理的函数和事件
//程序名称：LABComRateCommSetInput.js
//程序功能：
//创建时间：2008-01-24
//创建人  ：Huxl
//更新记录：  更新人    更新日期     	更新原因/内容


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";
	}
}

function getAgentCom(cObj,cName)
{
	if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输管理机构！");
  	return false;
  } 
  var strsql =" 1  and managecom  like #" + fm.all('ManageCom').value + "%#   " ;
  showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,'1',1);

}									
function printPay()
{       
        if(check()){
            fm.action="./LABankChargePrint2Save.jsp";
            submitForm();
            showInfo.close();
        }
}


//下载按钮对应操作
function submitForm()
{
      var i = 0;
      var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    //    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      //fm.hideOperate.value=mOperate;
      //if (fm.hideOperate.value=="")
      //{
      //  alert("操作控制数据丢失！");
      //}
      //showSubmitFrame(mDebug);
      fm.submit(); //提交
}

function check(){
    var mngcom = fm.all("ManageCom").value;
    
   if(!(mngcom.substr(0,4) == "8644")){
      alert("您的管理机构为空，因此不能打印，请您谅解。");
      return false;
   }
   if(fm.all("AgentCom").value == "" || fm.all("AgentCom").value == null) {
     alert("代理机构为空，请您选择机构编码。");
     return false;
   }
   if(!verifyInput()){
     return false;
   }

   return true ;
}