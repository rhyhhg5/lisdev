<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="BodyCheckPrintInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="BodyCheckPrintInit.jsp"%>
<title>打印体检通知书 </title>
</head>
<body  onload="initForm();" >
  <form action="./BodyCheckPrintQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>  印刷号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo verify="印刷号码"> </TD>
          <TD  class= title> 投保人</TD>
          <TD  class= input><Input class="common" name=AppntName verify="投保人|len<=20"></TD>
         <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
          </TR> 
         <TR  class= common>
         	<TD  class= title>  生效日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=CvaliDate verify="生效日期|date"> </TD>
          <TD  class= title>  下发日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=MakeDate verify="下发日期|date">   </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   
         </TR> 
        </TR>     
    </table>
          <INPUT VALUE="查询" class= cssbutton TYPE=button onclick="easyQueryClick();"> 
  </form>
  <form action="./BodyCheckPrintSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 体检信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
			<INPUT VALUE="首页" class= cssbutton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= cssbutton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= cssbutton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= cssbutton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<p>
      <INPUT VALUE="打印体检通知书" class= cssbutton TYPE=button onclick="printPol();"> 
  	</p>  
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
        <tr  class= common>
          <td  class= title>印刷号</td>
          <td  class= input>  
            <Input class= common name=PrtNo1 > 
          </td>
          <td  class= title>体检通知书号</td>
          <td  class= input>  
            <Input class= common name=PrtSeq1 > 
          </td>
          <td  class= title>保全受理号</td>
          <td  class= input>  
            <Input class= common name=EdorNo >
          </td>
        </tr>
        <tr  class= common>
          <td  class= title>被保人客户号</td>
          <td  class= input>  
            <Input class= common name=InsuredNo1 >  
          </td>   
          <td  class= title>管理机构</td>
          <td  class= input>  
            <Input class="codeNo" name=ManageCom1 ondblclick="return showCodeList('station',[this,ManageComName1], [0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName1], [0,1]);"><Input class="codeName" name=ManageComName1 readonly >
          </td> 
          <td  class= title>下发日期</td>
          <td  class= input>  
            <Input class= common name=SendDate> 
          </td> 
         <!--
          <td  class= title>打印状态</td>
          <td  class= input>  
            <Input class="codeNo" name=HealthPrintFlag CodeData="0|^0|未打印^1|已打印" value="0" ondblclick="return showCodeListEx('HealthPrintFlag',[this,HealthPrintFlagName], [0,1]);" onkeyup="return showCodeListEx('HealthPrintFlag',[this,HealthPrintFlagName], [0,1]);"><Input class="codeName" name=HealthPrintFlagName readonly value="未打印">   
          </td>
          -->
        </tr>     
    </table>
    <input value="查  询" class= cssButton type=button onclick="queryHealthPrint();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHealth);">
    		</td>
    		<td class= titleImg>
    			 体检通知书信息
    		</td>
    	</tr>
    </table>
  	<div id="divHealth" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanHealthGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<div id= "divPage" align=center style= "display: 'none' ">
  			<input value="首  页" class=cssButton type=button onclick="turnPage2.firstPage();"> 
        <input value="上一页" class=cssButton type=button onclick="turnPage2.previousPage();"> 					
        <input value="下一页" class=cssButton type=button onclick="turnPage2.nextPage();"> 
        <input value="尾  页" class=cssButton type=button onclick="turnPage2.lastPage();"> 
      </div>					
  	</div>
  	<p>
      <input value="打印保全体检通知书" class=cssButton type=button onclick="printNotice();"> 
  	</p>
  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="PrtSeq" name="PrtSeq">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
