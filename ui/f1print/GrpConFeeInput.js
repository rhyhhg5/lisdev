//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

//个单发票打印
function PPrint()
{
  if (verifyInput() == false) return false;  
  
	fm.action="ConFeeF1PSave.jsp";
	fm.target="f1print";
	fm.fmtransact.value="CONFIRM";
	//alert("PPrint");
	//submitForm();
	fm.submit();
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

// 查询按钮
function easyQueryClick() {
	// 书写SQL语句			     
	var strSql = "select t.PrtSeq,t.StandbyFlag2, t.otherno, b.GrpName, a.PayDate, a.SumDuePayMoney, a.bankcode, a.bankaccno, a.accname "
	           + " from LJSPay a, LDGrp b ,LOPRTManager t where a.AppntNo=b.GrpNo and a.otherno=t.otherno "
	           + " and a.OtherNoType='1' and t.OtherNoType='01' and t.StateFlag='0' and t.Code='31'"
          	 + getWherePart("a.GetNoticeNo", "GetNoticeNo2")
          	 + getWherePart("a.OtherNo", "OtherNo")
          	 + getWherePart("a.BankCode", "BankCode")
          	 + getWherePart("a.SumDuePayMoney", "SumDuePayMoney");
  
  if (fm.AppntName.value != "") strSql = strSql + " and a.appntno in (select c.GrpNo from LDGrp c where GrpName='" + fm.AppntName.value + "')";
  if (fm.PrtNo.value != "") strSql = strSql + " and a.otherno in (select GrpPolNo from lcgrppol where prtno='" + fm.PrtNo.value + "')";

	turnPage.queryModal(strSql, BankGrid);
	
	if(BankGrid.mulLineCount ==0)
	{
		alert("没有符合条件的数据");
	}
}


function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
	  fm.PrtSeq.value = turnPage.arrDataCacheSet[index][0];
  }
}
