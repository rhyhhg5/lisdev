<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：
//程序功能：
//创建日期：2003-1-15
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="LCPolBillInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LCPolBillInit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form  method=post name=fm target="fraSubmit">
    <table>
    	<tr> 
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
    		</td>
    		 <td class= titleImg>
        		输入查询的条件
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLCPol" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title width="25%">
            起始日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=StartDay verify="起始日期|NOTNULL">
          </TD>
          <TD  class= title width="25%">
            结束日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=EndDay verify="结束日期|NOTNULL">
          </TD>  
        </TR>
        <TR  class= common>
          <TD  class= title width="25%">
            起始时间
          </TD>
          <TD  class= input width="25%">
            <Input class= common name=StartTime>
          </TD>
          <TD  class= title width="25%">
            结束时间
          </TD>
          <TD  class= input width="25%">
            <Input class= common name=EndTime>
          </TD>  
        </TR>
        <TR  class= common>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
	     		</TD> 
	     		<TD  class= title>
            销售渠道 
          </TD>
          <TD  class= input>
            <Input class="codeno" name=SaleChnl11  CodeData="0|^02|个人营销^03|银行代理" ondblclick="showCodeListEx('SaleChnl11',[this,SaleChnl11Name],[0,1]);"  onkeyup="showCodeListKeyEx('SaleChnl11',[this,SaleChnl11Name],[0,1]);"><input class=codename name=SaleChnl11Name readonly=true >
          </TD>       
        </TR>        
        <tr>
        <TD  class= title>
            展业机构编码 
          </TD>
          <TD  class= input>
            <Input class=common name=AgentGroup >
            <input name="btnQueryCom" class="common" type="button" value="组别查询" onclick="queryCom()" style="width:100; ">
          </TD> 
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
        </tr>
        <TR  class= common>
          <TD  class= title>
            起始保单号
          </TD>
          <TD  class= input>
            <Input class= common name=StartPolNo >
          </TD>
          <TD  class= title>
            终止保单号
          </TD>
          <TD  class= input>
            <Input class= common name=EndPolNo >
          </TD>	     		  
        </TR>
        <TR>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="codeno" name=RiskCode ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('RiskCode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true >
          </TD>  
          <TD  class= title>
            印刷号
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>  
        </TR>
        <TR>
          <TD  class= title>
            投保人姓名
          </TD>
          <TD  class= input>
            <Input class= common name=AppntName >
          </TD>  
          <TD  class= title>
            被保人姓名
          </TD>
          <TD  class= input>
            <Input class= common name=InsuredName >
          </TD>  
        </TR>     
     </table> 
     <table  class= common>
        <TR  class= common>
        	<TD  class= input >
				<input class= common type=Button name="BillPrint" value="保单交接清单打印" onclick="fnBillPrint()">
        	</TD>
        	<TD>
        		<input class= common type=Button name="BillPrintBank" value="银行打印保单交接清单" onclick="fnBillPrintBank()">
        	</TD>
        </TR>
     </table>        
    </Div>
      
        <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>