<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LABranchWage.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
  		System.out.println("tManageCom"+tManageCom);
%>
 <script>
   var msql=" 1 and   char(length(trim(comcode)))<=#4# ";
 var msql1=" 1 and   branchtype=#2#  and branchtype2=#01# and endflag=#N#";

</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LAGrpAgentDetailInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();" >    
  <form action="./LAGrpAgentDetailReport.jsp" method=post name=fm target="f1print">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			 销售人员信息细则查询报表
    		</td>
    	</tr>
      </table>
    <table class= common border=0 width=100%>
      	<TR  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,1,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,1,1);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD> 
          <TD  class= title>
            团队代码 
          </TD>
          <TD  class= input>
            <Input  class='codeno'  name=BranchAttr verify = "团队代码|code:branchattr"  
             ondblclick="return showCodeList('branchattr',[this,BranchName],[0,1],null,msql1,1,1);" 
             onkeyup="return showCodeListKey('branchattr',[this,BranchName],[0,1],null,msql1,1,1);"
            ><Input  class='codename' name=BranchName > 
          </TD>            
        </TR>
        <TR  class= common>
          <TD  class= title>
             期初
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=StartDate  verify="起期|NOTNULL" elementtype=nacessary  >  
          </TD>
          <TD  class= title>
             期末
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="止期|NOTNULL" elementtype=nacessary >  
          </TD>
    
        </TR>
        <TR  class= common>
          <TD  class= title>
             状态
          </TD>
          <TD  class= input>
           <Input class='codeno' 　 name=AgentState    verify="状态|code:BranchOrigin" 
           CodeData="0|^01|在职|^02|离职 " ;
            
             ondblClick="   showCodeListEx('BranchOrigin',[this,StateName],[0,1]) ; "
             onkeyup="      showCodeListKeyEx('BranchOrigin',[this,StateName],[0,1]) ; "
          ><Input class=codename name=StateName elementtype=nacessary>  
          </TD>
          
    
         </TR>         
       
    </table>

    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type=hidden name=AgentGroup value=''>
    <input type=hidden name=Managecom2  value ='<%=tManageCom%>'>   
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">

		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->   </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 