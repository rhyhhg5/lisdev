//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    if(!beforeSubmit())
    {
      return false;
    }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
	showInfo.close();
}
function beforeSubmit()
{
  //日期检验
  var startdate = fm.all('StartDate').value;
  var enddate = fm.all('EndDate').value
  var flag=compareDate(startdate,enddate);
  if(flag==1)
  {
    alert("开始日期不能大于结束日期!");
    return false;
  }
  return true;
}
function checkBranchAttr()
{
   var sql = "select agentgroup from labranchgroup where branchattr='"+fm.BranchAttr.value+"' and branchtype='2' and branchtype2='01' and endflag='N'";
   var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if (!strQueryResult) 
    {
      alert("没有此销售单位！");  
      fm.BranchAttr.value="";
      return;
    }	
    var arr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentGroup').value=arr[0][0];
    return true;
}
function checkAgentCode()
{
//   var sql = "select * from laagent where agentstate<='02' and branchtype='1' and branchtype2='01'";
   var sql=" select agentcode  from laagent   where  groupagentcode='"+fm.GroupAgentCode.value+"' "
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
//    alert(fm.all('AgentCode').value);
//   var ssql = "select a.managecom,(select branchattr from labranchgroup where agentgroup=a.agentgroup) from latree a where a.agentcode='"+fm.all('AgentCode').value+"'";
//    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
//    if(!strQueryResult)
//    {
//     alert("代理人行政信息查询失败！");
//     return false;
//    }
//    else
//    {
//        var arr = decodeEasyQueryResult(strQueryResult);
//	   if(fm.all('BranchAttr').value!=null||fm.all('BranchAttr').value!="")
//	   {
//	      if(arr[0][1]!=fm.all('BranchAttr').value)
//	      {
//	        alert("该团队下无此代理人！");
//	        return false;
//	      }
//	   }
//	   
//   }

}
function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}