<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>   
<title>业绩查询表</title> 
<%
//程序名称：LARiskWage.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
  
   
%>

 <script>
   var msql=" 1 and branchtype=#2#  ";
  var tStrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode and riskprop = #G#)";
</script>

<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="LAGrpRiskWage.js"></SCRIPT>   
<%@include file="LAGrpRiskWageInit.jsp"%>

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
 
<body   onload="initForm() " > 
  <form action="./LAGrpRiskWageReport.jsp" method=post name=fm target="f1print">
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			 业绩查询表
    		</td>
    	</tr>
      </table>
    <table class= common border=0 width=100%>

      <TR  class= common>
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,4,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,4,'char(length(trim(comcode)))',1);"
            ><Input  class='codename' name=ManageComName > 
          </TD> 
          <TD  class= title>
            险种 
          </TD>
          <TD  class= input>
            <Input class='codeno' name=RiskCode verify = "险种|notnull&code:ComCode" 
            ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1],null,tStrSql,1,1);" 
            onkeyup="return showCodeListKey('RiskCode',[this,RiskCodeName],[0,1],null,tStrSql,1,1);" 
            ><Input  class='codename' name=RiskCodeName  > 
          </TD> 
       </TR>       

       <TR  class= common>
          <TD  class= title>
             期初
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=StartDate  verify="起期|NOTNULL" elementtype=nacessary  >  
          </TD>
          <TD  class= title>
             期末
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="止期|NOTNULL" elementtype=nacessary >  
          </TD>
    
        </TR>
       <TR  class= common>
          <TD  class= title>
             业务性质
          </TD>
         <TD  class= input>
            <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1]);" 
             onkeyup="return showCodeListKey('BranchType2',[this,BranchType2Name],[0,1]);"
              ><Input class=codename name=BranchType2Name >
          <TD  class= title>
             业务来源
          </TD>
         <TD  class= input>
            <Input class='codeno' name=BranchOrigin   verify="加扣款类型|NOTNULL&code:BranchOrigin"
             CodeData="0|^01|直销队伍|^02|中介渠道|^03|公司业务|^04|综合开拓个销团" ;
            
             ondblClick="   showCodeListEx('BranchOrigin',[this,BranchOriginName],[0,1]) ; "
             onkeyup="      showCodeListKeyEx('BranchOrigin',[this,BranchOriginName],[0,1]) ; "
          ><Input class=codename name=BranchOriginName >
          </TD>    
    
        </TR>        
       <TR  class= common>
          <TD  class= title>
             业务特点
          </TD>
         <TD  class= input>
            <Input class='codeno' name=BranchPoint 	  verify="加扣款类型|NOTNULL&code:BranchPoint"
         CodeData=<%="0|^01|常规业务|^02|大项目|^03|结合市场 "%> MaxLength=1
             ondblclick="return showCodeListEx('BranchPoint',[this,BranchPointName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('BranchPoint',[this,BranchPointName],[0,1]);"
             ><Input class=codename name=BranchPointName >
          <TD  class= title>
             销售队伍
          </TD>
         <TD  class= input>
            <Input class='codeno' name=AgentGrade  
            <Input class='codeno' name=RiskCode verify = "销售队伍|notnull&code:ComCode" 
             ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1,1);" 
             onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1,1);"
             ><Input class=codename name=AgentGradeName >
          </TD>  
    
        </TR>    
       <TR  class= common>
           <TD  class= title>
            客户规模
          </TD>
          <TD  class= input>
            <Input class='codeno' name=CustomerNO  verify="客户规模|NOTNULL&code:CustomerNO"
             CodeData=<%="0|^01|20人以下|^02|20-50人|^03|50到100人|^04|100到300人|^05|300到500人|^06|500到1000人|^07|1000到2000人|^08|2000人以上 "%> 
             MaxLength=1
             ondblclick="return showCodeListEx('CustomerNO',[this,CustomerNOName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('CustomerNO',[this,CustomerNOName],[0,1]);"
             ><Input class=codename name=CustomerNOName >
          </TD>       
       </TR>                     
    </table>

   
    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type="hidden" name=AgentGroup value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 