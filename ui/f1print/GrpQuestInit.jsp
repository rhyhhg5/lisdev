<%
//程序名称：MeetInit.jsp
//程序功能：
//创建日期：2003-03-04
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>                            

<script language="JavaScript">

var PolGrid;          //定义为全局变量，提供给displayMultiline使用

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {
  	fm.reset();                                   
  }
  catch(ex)
  {
    alert("在MeetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
  initInpBox();
	initPolGrid();
	if(RePrintFlag=="1")
			{
					fm.SelType.style.display="";
					fm.SelTypeName.style.display="";
					fm.SelTypeName2.style.display="none";
					fm.StateFlag.style.display="";
					fm.StateFlagName.style.display="";
					fm.StateFlagName2.style.display="none";
					fmSave.all('printissue').style.display="none";
					fmSave.all('reprintissue').style.display="";
					
			}
	else
			{
					fm.SelType.style.display="";
					fm.SelTypeName.style.display="";
					fm.SelTypeName2.style.display="none";
					fm.StateFlag.style.display="";
					fm.StateFlagName.style.display="";
					fm.StateFlagName2.style.display="none";
					fmSave.all('printissue').style.display="";
					fmSave.all('reprintissue').style.display="none";
			}	
	fm.all('ManageCom').value = <%=strManageCom%>;
	    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
  }
  catch(re)
  {
    alert("MeetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 投保单信息列表的初始化
function initPolGrid()
{                               
  var iArray = new Array();
      
  try {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            	//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="流水号";         		//列名
	  iArray[1][1]="10px";            	//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[2]=new Array();
	  iArray[2][0]="印刷号";       		//列名
	  iArray[2][1]="80px";            	//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[3]=new Array();
	  iArray[3][0]="申请日期";       		//列名
	  iArray[3][1]="80px";            	//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="生效日期";       		//列名
	  iArray[4][1]="80px";            	//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  	  
	  iArray[5]=new Array();
	  iArray[5][0]="投保人";       		//列名
	  iArray[5][1]="60px";            	//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[6]=new Array();
	  iArray[6][0]="总保费";       		//列名
	  iArray[6][1]="60px";            	//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许	  
	  
	  iArray[7]=new Array();
	  iArray[7][0]="业务员代码";         	//列名
	  iArray[7][1]="75px";            	//列宽
	  iArray[7][2]=100;            			//列最大值
	  iArray[7][3]=2; 
    iArray[7][4]="AgentCode";              	        //是否引用代码:null||""为不引用
    iArray[7][5]="3";              	                //引用代码对应第几列，'|'为分割符
    iArray[7][9]="业务员代码|code:AgentCode&NOTNULL";
    iArray[7][18]=250;
    iArray[7][19]= 0 ;
    
    iArray[8]=new Array();
	  iArray[8][0]="管理机构";         	//列名
	  iArray[8][1]="60px";            	//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=2; 
    iArray[8][4]="station";              	        //是否引用代码:null||""为不引用
    iArray[8][5]="3";              	                //引用代码对应第几列，'|'为分割符
    iArray[8][9]="管理机构|code:station&NOTNULL";
    iArray[8][18]=250;
    iArray[8][19]= 0 ;
	
	  iArray[9]=new Array();
	  iArray[9][0]="下发日期";        //列名
	  iArray[9][1]="80px";            	//列宽
	  iArray[9][2]=200;            			//列最大值
	  iArray[9][3]=0; 									//是否允许输入,1表示允许，0表示不允许
      
    iArray[10]=new Array();
	  iArray[10][0]="回销日期";        //列名
	  iArray[10][1]="80px";            	//列宽
	  iArray[10][2]=200;            			//列最大值
	  iArray[10][3]=0; 									//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[11]=new Array();
	  iArray[11][0]="是否已打";        //列名
	  iArray[11][1]="50px";            	//列宽
	  iArray[11][2]=200;            			//列最大值
	  iArray[11][3]=0; 									//是否允许输入,1表示允许，0表示不允许

      
    iArray[12]=new Array();
	  iArray[12][0]="投保单印刷号";         	//列名
	  iArray[12][1]="0px";            	//列宽
	  iArray[12][2]=100;            			//列最大值
	  iArray[12][3]=3; 
	  
    iArray[13]=new Array();
	  iArray[13][0]="工作流任务编码";         	//列名
	  iArray[13][1]="0px";            	//列宽
	  iArray[13][2]=100;            			//列最大值
	  iArray[13][3]=3; 
	  
	  iArray[14]=new Array();
	  iArray[14][0]="工作流子任务编码";         	//列名
	  iArray[14][1]="0px";            	//列宽
	  iArray[14][2]=100;            			//列最大值
	  iArray[14][3]=3; 
    
	  iArray[15]=new Array();
	  iArray[15][0]="打印次数";         	//列名
	  iArray[15][1]="50px";            	//列宽
	  iArray[15][2]=100;            			//列最大值
	  iArray[15][3]=0; 
      
	  PolGrid = new MulLineEnter( "fmSave" , "PolGrid" ); 
	  //这些属性必须在loadMulLine前
	  PolGrid.mulLineCount = 10;   
	  PolGrid.displayTitle = 1;
	  PolGrid.canSel = 1;
	  PolGrid.hiddenPlus = 1;
	  PolGrid.hiddenSubtraction = 1;
    PolGrid.locked = 1;
	  PolGrid.loadMulLine(iArray);  
	
	} catch(ex) {
		alert(ex);
	}
}

</script>