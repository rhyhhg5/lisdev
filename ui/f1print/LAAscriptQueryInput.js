//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	 
   if (verifyInput() == false)
    return false;
   
   	 
    if( !beforeSubmit())
   {
   	return false;
   	
   	}	

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	var tsql1="";
  var tsql2="";
  if(fm.all('ContNo').value!=""&&fm.all('ContNo').value!=null&&fm.all('GrpContNo').value!=""&&fm.all('GrpContNo').value!=null)
 	{
 	}
 	else {
 	if(fm.all('ContNo').value!=""&&fm.all('ContNo').value!=null)
 	{
 	tsql2=" and 1=2 ";
 	}
 	if(fm.all('GrpContNo').value!=""&&fm.all('GrpContNo').value!=null)
 	{
 	tsql1=" and 1=2 ";
 	}
 	}
  var strSql = "";

  strSql = "select a.grpcontno,a.contno,b.appntname,case when b.appntsex='0' then '男' else '女' end,(select year((current date)-(b.appntbirthday)) from dual),"
    + "  (select e.mobile from lcaddress e where e.customerno = b.appntno and e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno)),"
    + " c.name,getUniteCode(a.agentold),a.branchattr,"
    +" (select name from laagent where agentcode=a.agentnew),getUniteCode(a.agentnew),"
    +" (select b.branchattr from labranchgroup b where b.agentgroup = (select branchcode from laagent where agentcode=a.agentnew))h ,a.modifydate"
    +"  ,a.ascriptiondate,(select e.postaladdress from lcaddress e where e.customerno = b.appntno and e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno))"
    + " from laascription a ,lccont b,laagent c where b.contno=a.contno  "
    +"and a.agentold=c.agentcode and a.validflag='N' and a.ascripstate='3' "
    +"and a.branchtype='1' and a.branchtype2='01' and a.grpcontno is null "
    +"and a.managecom like '"+fm.all('ManageCom').value +"%'"
    // + getWherePart('a.ManageCom','ManageCom','like')
	  + getWherePart('c.groupagentcode','AgentCode')
	  + getWherePart('c.Name','Name')
	  + getWherePart('a.ContNo','ContNo')
	  + getWherePart('a.BranchAttr','BranchAttr')
	  + getWherePart('a.modifydate','MakeDate')
	  +tsql1
	  +
	  " union  select a.grpcontno,a.contno,b.grpname,'-',0,"
    + "  (select e.phone1 from lcgrpaddress e where e.customerno = b.appntno "
    +" and e.addressno=(select addressno from lcgrpappnt where a.grpcontno=lcgrpappnt.grpcontno)),"
    + " c.name,getUniteCode(a.agentold),a.branchattr,"
    +" (select name from laagent where agentcode=a.agentnew),getUniteCode(a.agentnew),"
    +" (select b.branchattr from labranchgroup b where b.agentgroup = (select branchcode from laagent where agentcode=a.agentnew))h"
    +"  ,a.modifydate ,a.ascriptiondate,"
    +"(select e.grpaddress from lcgrpaddress e where e.customerno = b.appntno and e.addressno=(select addressno from lcgrpappnt where a.grpcontno=lcgrpappnt.grpcontno))"
    + " from laascription a ,lcgrpcont b,laagent c where b.grpcontno=a.grpcontno  "
    +"and a.agentold=c.agentcode and a.validflag='N' and a.ascripstate='3' "
    +"and a.branchtype='1' and a.branchtype2='01'  and a.contno is null "
    +"and a.managecom like '"+fm.all('ManageCom').value +"%'"
    // + getWherePart('a.ManageCom','ManageCom','like')
	  + getWherePart('c.groupagentcode','AgentCode')
	  + getWherePart('c.Name','Name')
	  + getWherePart('a.GrpContNo','GrpContNo')
	  + getWherePart('a.BranchAttr','BranchAttr')
	  + getWherePart('a.modifydate','MakeDate') 
	  +tsql2 ;	
  fm.all('querySQL').value=strSql;	
	fm.submit();
	showInfo.close();
}
function beforeSubmit()
{
 
	return true;
	
}


function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function easyQueryClick() 
{
　if (verifyInput() == false)
    return false;
  //此处书写SQL语句			     
  var tManageCom=getManageComLimit();
  
  
   var tsql1="";
   var tsql2="";
   if(fm.all('ContNo').value!=""&&fm.all('ContNo').value!=null&&fm.all('GrpContNo').value!=""&&fm.all('GrpContNo').value!=null)
 	{
 	}
 	else {
 	if(fm.all('ContNo').value!=""&&fm.all('ContNo').value!=null)
 	{
 	tsql2=" and 1=2 ";
 	}
 	if(fm.all('GrpContNo').value!=""&&fm.all('GrpContNo').value!=null)
 	{
 	tsql1=" and 1=2 ";
 	}
 	}
  var strSql = "";

    strSql = "select a.grpcontno,a.contno,b.appntname,case when b.appntsex='0' then '男' else '女' end,(select year((current date)-(b.appntbirthday)) from dual),"
    + "  (select e.mobile from lcaddress e where e.customerno = b.appntno and e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno)),"
    + " c.name,getUniteCode(a.agentold),a.branchattr,"
    +" (select name from laagent where agentcode=a.agentnew),getUniteCode(a.agentnew),"
    +" (select b.branchattr from labranchgroup b where b.agentgroup = (select branchcode from laagent where agentcode=a.agentnew))h ,a.modifydate"
    +"  ,a.ascriptiondate,(select e.postaladdress from lcaddress e where e.customerno = b.appntno and e.addressno=(select addressno from lcappnt where a.contno=lcappnt.contno))"
    + " from laascription a ,lccont b,laagent c where b.contno=a.contno  "
    +"and a.agentold=c.agentcode and a.validflag='N' and a.ascripstate='3' "
    +"and a.branchtype='1' and a.branchtype2='01' and a.grpcontno is null "
    +"and a.managecom like '"+fm.all('ManageCom').value +"%'"
    // + getWherePart('a.ManageCom','ManageCom','like')
	  + getWherePart('c.groupagentcode','AgentCode')
	  + getWherePart('c.Name','Name')
	  + getWherePart('a.ContNo','ContNo')
	  + getWherePart('a.BranchAttr','BranchAttr')
	  + getWherePart('a.modifydate','MakeDate')
	  +tsql1
	  +
	  " union  select a.grpcontno,a.contno,b.grpname,'-',0,"
    + "  (select e.phone1 from lcgrpaddress e where e.customerno = b.appntno "
    +" and e.addressno=(select addressno from lcgrpappnt where a.grpcontno=lcgrpappnt.grpcontno)),"
    + " c.name,getUniteCode(a.agentold),a.branchattr,"
    +" (select name from laagent where agentcode=a.agentnew),getUniteCode(a.agentnew),"
    +" (select b.branchattr from labranchgroup b where b.agentgroup = (select branchcode from laagent where agentcode=a.agentnew))h"
    +"  ,a.modifydate ,a.ascriptiondate,"
    +"(select e.grpaddress from lcgrpaddress e where e.customerno = b.appntno and e.addressno=(select addressno from lcgrpappnt where a.grpcontno=lcgrpappnt.grpcontno))"
    + " from laascription a ,lcgrpcont b,laagent c where b.grpcontno=a.grpcontno  "
    +"and a.agentold=c.agentcode and a.validflag='N' and a.ascripstate='3' "
    +"and a.branchtype='1' and a.branchtype2='01'  and a.contno is null "
    +"and a.managecom like '"+fm.all('ManageCom').value +"%'"
    // + getWherePart('a.ManageCom','ManageCom','like')
	  + getWherePart('c.groupagentcode','AgentCode')
	  + getWherePart('c.Name','Name')
	  + getWherePart('a.GrpContNo','GrpContNo')
	  + getWherePart('a.BranchAttr','BranchAttr')
	  + getWherePart('a.modifydate','MakeDate') 
	  +tsql2 ;	  
 //alert(strSql);
   turnPage.queryModal(strSql, LACrossGrid);
  if (!turnPage.strQueryResult) 
  {
    alert("没有符合条件的查询信息！");
    return false;
  }
}

function afterCodeSelect( cCodeName, Field )
{
　	  
  if(cCodeName=="comcode"){
  	 fm.all("BranchAttr").value="";
  	 fm.all("BranchName").value="";
  	if(fm.all("ManageCom").value=='86' )
  	{
  		fm.all("BranchAttr").disabled=true;
  	}
       else
       	{
       		var tManageCom=fm.all("ManageCom").value;
       		fm.all("BranchAttr").disabled=false;
       		msql1=" 1 and   branchtype=#2#  and branchtype2=#01#  and managecom like #"+tManageCom+"%# and endflag=#N#";
       	}
}
 if(cCodeName=="branchattr")
 {
   if(fm.all("ManageCom").value==null || fm.all("ManageCom").value=='')
    {
      alert("请先录入管理机构");
      fm.all("BranchAttr").value='';
       fm.all("BranchName").value='';
       return false;
  
     }
}
}        
                                                                                                                   