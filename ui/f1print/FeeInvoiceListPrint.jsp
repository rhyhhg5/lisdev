<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContactCompareListPrint.jsp
//程序功能：
//创建日期：2010-09-21
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "FeeInvoiceList_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    //服务器
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    //本地
    //String tOutXmlPath = filePath +downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    FeeInvoiceListPrintBL tbl =new FeeInvoiceListPrintBL();
    GlobalInput tGlobalInput = new GlobalInput();
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("EnterAccDate", request.getParameter("EnterAccDate"));
    tTransferData.setNameAndValue("EnterAccDateEnd", request.getParameter("EnterAccDateEnd"));
    tTransferData.setNameAndValue("IncomeType", request.getParameter("IncomeType"));
    tTransferData.setNameAndValue("SaleChnl", request.getParameter("SaleChnl"));
    tTransferData.setNameAndValue("PrtNo", request.getParameter("PrtNo"));
    tTransferData.setNameAndValue("IncomeNo", request.getParameter("IncomeNo"));
    tTransferData.setNameAndValue("MngCom", request.getParameter("MngCom"));
    tTransferData.setNameAndValue("AgentCode", request.getParameter("AgentCode"));
    tTransferData.setNameAndValue("InvoiceState", request.getParameter("InvoiceState"));
    tTransferData.setNameAndValue("CardFlag", request.getParameter("CardFlag"));
    tTransferData.setNameAndValue("AgentCom", request.getParameter("AgentCom"));
    tTransferData.setNameAndValue("AgentComAgentCode", request.getParameter("AgentComAgentCode"));
    tTransferData.setNameAndValue("BankContFlag", request.getParameter("BankContFlag"));
    VData tData = new VData();
    tData.add(tG);
    tData.add(tTransferData);

    CreateExcelList tCreateExcelList=new CreateExcelList();
    tCreateExcelList=tbl.getsubmitData(tData,"");
	if(tCreateExcelList==null){
   		errorFlag=false;
    	System.out.println("EXCEL生成失败！");
    }else{
       		errorFlag=true;
    }
	System.out.println("1");
    if(errorFlag)
    {
        //写文件到磁盘
        try
        {
            tCreateExcelList.write(tOutXmlPath);
        }
        catch(Exception e)
        {
            errorFlag = false;
            System.out.println(e);
        }
    }
    System.out.println("2");
    //返回客户端
    if(errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(!errorFlag)
    {
%>
<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>