<%@page contentType="text/html; charset=GBK" %><%@
page import="java.io.*"%><%@
page import="com.sinosoft.utility.*"%><%@
page import="com.sinosoft.lis.schema.*"%><%@
page import="com.sinosoft.lis.vschema.*"%><%@
page import="com.sinosoft.lis.pubfun.*"%><%@
page import="java.util.*" %><%
	try {
		String strTemplatePath = application.getRealPath("f1print/template/") + "/";
		F1PrintParser fp = null;
		
		InputStream ins = (InputStream)session.getValue("PrintStream");
		
		if( ins == null ) {
			XmlExport xmlExport = new XmlExport();

			xmlExport.createDocument("nofound.vts", "printer");
  		
  		fp = new F1PrintParser(xmlExport.getInputStream(), strTemplatePath);
		} else {
			fp = new F1PrintParser(ins, strTemplatePath);
		}
	  
    if( !fp.output(response.getOutputStream()) ) {
      System.out.println("F1PrintDemo.jsp : fail to parse print data");
    }
    
  }catch(java.net.MalformedURLException urlEx){
    urlEx.printStackTrace();
  }catch(java.io.IOException ioEx){
    ioEx.printStackTrace();
  }catch(Exception ex){
  	ex.printStackTrace();
  }
  
  session.putValue("PrintStream", null);
%>