<%
//程序名称：BankDataQuery.jsp
//程序功能：财务报盘数据查询
//创建日期：2004-10-20
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
%>


<html>    
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
  
  <SCRIPT src="QYBankDataQuery.js"></SCRIPT>   
  <%@include file="QYBankDataQueryInit.jsp"%>   
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>提取新契约手机号 </title>
</head>      
 
<body  onload="initForm();initElementtype();" >
  <form method=post name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>管理机构</TD>
          <TD  class= input>
            <Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
          </TD>
		      <TD  class= title>银行代码</TD>
		      <TD  class= input>
		        <Input CLASS="codeNo" name=BankCode verify="银行代码|notnull&code:Bank" ondblclick="return showCodeList('Bank',[this,BankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank', [this,BankName],[0,1],null,null,null,1);"><input class=codename name=BankName readonly=true >
		      </TD>
       		<TD  class= title width="25%">发盘起期</TD>
       		<TD  class= input width="25%">
       		  <Input class= "coolDatePicker" dateFormat="short" elementtype=nacessary name=SendDate verify="发盘日期|NOTNULL">
       		</TD>
			<TD  class= title width="25%">发盘止期</TD>
       		<TD  class= input width="25%">
       		  <Input class= "coolDatePicker" dateFormat="short" elementtype=nacessary name=SendEndDate verify="发盘日期|NOTNULL">
       		</TD>
     </TR>
     <TR  class= common>
          <TD  class= title>保单号</TD>
          <TD  class= input><Input class= common name=PolNo></TD>
          <TD  class= title>帐户名</TD>
          <TD  class= input><Input class= common name=AccName></TD>
		  <TD  class= title>流水号</TD>
          <TD  class= input><Input class= common name=SerialNo></TD>
          <TD  class= title>是否成功</TD>
          <TD  class= input><Input class="codeNo" name="BankSuccFlag" CodeData="0|^1|成功^2|未成功" verify="是否成功报盘|&code:BankSuccFlag"  ondblclick="return showCodeListEx('BankSuccFlag',[this,BankSuccName],[0,1]);" onkeyup="return showCodeListEx('BankSuccFlag',[this,BankSuccName],[0,1]);"><Input class="codeName" name="BankSuccName"  readonly></TD>
     </TR>
   	</Table>  
   	<p>
    <input type=hidden id="fmtransact" name="fmtransact">
    <!--数据区-->
    <INPUT VALUE="查  询" class= cssbutton TYPE=button onclick="easyQuery()">
    <INPUT VALUE="下  载" class= cssbutton TYPE=button onclick="download();">
    <Input type="hidden" name="Sql" />

  	<Div  id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
									<span id="spanCodeGrid" >
									</span> 
		  				</td>
					</tr>
    		</table>
		<center>
      	<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage.firstPage();showCodeName(); "> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage.previousPage();showCodeName(); "> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage.nextPage();showCodeName(); "> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage.lastPage();showCodeName(); "> 
		</center>
  	</div>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 	
<script>
	<!--选择机构：只能查询本身和下级机构-->
	var codeSql = "1  and code like #"+<%=Branch%>+"%#";
</script>