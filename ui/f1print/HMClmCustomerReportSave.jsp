<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：HMClmCustomerReportSave.jsp
//程序功能：被保险人理赔数据统计
//创建日期：2010-9
//创建人  ：caixd
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 
 CErrors mErrors =new CErrors();
 HMClmCustomerReportUI tHMClmCustomerReportUI = new HMClmCustomerReportUI();

 //输出参数
 String Content = "";
 String transact = "";
 boolean operFlag=true;
 
 String tManageCom = request.getParameter("ManageCom"); 
 String tSaleChnl = request.getParameter("SaleChnl"); 
 String tSaleChnlComName = request.getParameter("SaleChnlComName"); 
 String tDudyKind = request.getParameter("DudyKind"); 
 String tRiskCode = request.getParameter("RiskCode"); 
 String tDisease = request.getParameter("Disease"); 
 String tHospit = request.getParameter("Hospit"); 
 String tAgentCode = request.getParameter("AgentCode"); 
 String tBlackCus = request.getParameter("BlackCus"); 
 String tGrpName = request.getParameter("GrpName"); 
 String tStartDate = request.getParameter("StartDate"); 
 String tEndDate = request.getParameter("EndDate"); 
 String tGrpContNo = request.getParameter("GrpContNo"); 
 String tGiveMoney = request.getParameter("GiveMoney"); 
 String tHisMoney = request.getParameter("HisMoney"); 
 String tGiveDate = request.getParameter("GiveDate"); 
 String thospitCount = request.getParameter("hospitCount"); 
 String tRiskAmnt = request.getParameter("RiskAmnt"); 
 System.out.println("tSaleChnlComName-------------"+tSaleChnlComName);
 TransferData tTransferData = new TransferData();
 
 tTransferData.setNameAndValue("ManageCom",tManageCom);
 tTransferData.setNameAndValue("SaleChnl",tSaleChnl);
 tTransferData.setNameAndValue("SaleChnlComName",tSaleChnlComName);
 tTransferData.setNameAndValue("DudyKind",tDudyKind);
 tTransferData.setNameAndValue("RiskCode",tRiskCode);
 tTransferData.setNameAndValue("Disease",tDisease);
 tTransferData.setNameAndValue("Hospit",tHospit);
 tTransferData.setNameAndValue("AgentCode",tAgentCode);
 tTransferData.setNameAndValue("BlackCus",tBlackCus);
 tTransferData.setNameAndValue("GrpName",tGrpName);
 tTransferData.setNameAndValue("StartDate",tStartDate);
 tTransferData.setNameAndValue("EndDate",tEndDate);
 tTransferData.setNameAndValue("GrpContNo",tGrpContNo);
 tTransferData.setNameAndValue("GiveMoney",tGiveMoney);
 tTransferData.setNameAndValue("HisMoney",tHisMoney);
 tTransferData.setNameAndValue("GiveDate",tGiveDate);
 tTransferData.setNameAndValue("hospitCount",thospitCount);
 tTransferData.setNameAndValue("RiskAmnt",tRiskAmnt);

  RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);//报表下载
  VData mResult = new VData();
  XmlExport txmlExport = new XmlExport();

  //准备传输数据VData
  VData tVData = new VData();       
  tVData.add(tG);
  tVData.add(tTransferData);
  if(!tHMClmCustomerReportUI.submitData(tVData,transact))
  {
	  operFlag = false;
	  mErrors=tHMClmCustomerReportUI.mErrors;
  	  Content = "打印失败，原因是:"+ mErrors.getFirstError();
  	  System.out.println("Content=========="+Content);
  }
  else
  {
	    mResult = tHMClmCustomerReportUI.getResult();			
	  	txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  	  
	  	TransferData tvtsStyleParme = new TransferData();
		tvtsStyleParme = (TransferData)mResult.getObjectByObjectName("TransferData",0);
		
	  	if (txmlExport == null)
	  	  {
	  		operFlag = false;
	  	    System.out.println("========================null");
	  	  }	  
	 
	  	  ExeSQL tExeSQL = new ExeSQL();
		  //获取临时文件名
	  	  String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	  	  String strFilePath = tExeSQL.getOneValue(strSql);
	  	  String filenameB=tG.Operator + "_" + FileQueue.getFileName()+".vts";  //报表下载添加
	  	  String strVFFileName = strFilePath + filenameB;
	  	  System.out.println("strVFFileName-->>:"+strVFFileName);

	  	  String strRealPath = application.getRealPath("/").replace('\\','/');
	  	  String strVFPathName = strRealPath +"//"+ strVFFileName;

	  	  CombineVts tcombineVts = null;
	  	  if (operFlag==true)
	  	  {
	  		VData vtsStyleParmes = new VData();
	  		vtsStyleParmes.add(tvtsStyleParme); 
	  		  
	  		//合并VTS文件
	  		String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "//";
	  		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
	  		//tcombineVts.setVtsStyleParam(true,vtsStyleParmes);
	  		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
	  		tcombineVts.output(dataStream);

	  		//把dataStream存储到磁盘文件
	  		System.out.println("存储文件到================"+strVFPathName);
	  		AccessVtsFile.saveToFile(dataStream,strVFPathName);
	  		System.out.println("==> Write VTS file to disk ");
	  		session.putValue("RealPath", strVFPathName);
	  		session.putValue("flag", "UIS");	 
	  		session.putValue("strRealPath",strRealPath);
	  		System.out.println("RealPath:"+strVFPathName);	  		
	  		session.putValue("PrintStream", txmlExport.getInputStream());	
	  		rpt.updateReportMetaData(filenameB); //报表下载
	  		request.getRequestDispatcher("../f1print/GetF1PrintJ1.jsp?RealPath="+strVFPathName+"").forward(request,response);
	  		
	  		}
		 }	  	
%>
	  	<html>
	  	<%@page contentType="text/html;charset=GBK" %>
	  	<script language="javascript">	
	  		alert("<%=Content%>");
	  		top.close(); 		
	  	</script>
	
</html>
