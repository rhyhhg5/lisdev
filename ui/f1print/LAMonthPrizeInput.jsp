<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="LAMonthPrizeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAMonthPrizeInit.jsp"%>
  <title>业务员月度提奖统计表</title>   
</head>
<body  onload="initForm();initElementtype(); " >
  <form action="./LAMonthPrizeReport.jsp" method=post name=fm target="f1print">
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
       <TR  class= common>    
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom
           ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   
            
       </TR>     
       <TR class=common>
       <TD class=title>销售机构</TD>
       <TD class=input>
       <input class=common name=BranchAttr></TD>
       <TD class=title>业务员代码</TD>
       <TD class=input>
       <Input class=common name=GroupAgentCode onchange="return checkAgentCode()">
       </TD>
       </TR>
      	<TR  class= common>      	  
          <TD class= title>
          开始年月
          </TD>
          <TD  class= input width="25%">
            <Input class= common name=StartMonth verify="开始年月|NOTNULL&YYYYMM" elementtype=nacessary>
          </TD> 
          <TD class= title>
          结束年月
          </TD>
          <TD  class= input width="25%">
            <Input class= common name=EndMonth verify="结束年月|NOTNULL&YYYYMM" elementtype=nacessary >
            <font color="red" >('YYYYMM')</font>
          </TD> 
          </TR>    
    </table>       
<table  class= common>
        <TR class= common> 
		<TD  class= input width="26%">
			<input class= cssButton type=Button value="打  印" onclick="submitForm();">
		</TD>			
	</TR>    	 
      </table>      
      <input type=hidden id="Operate" name="Operate"> 
      <input type=hidden name=AgentCode value=''>     
  </form> 
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
