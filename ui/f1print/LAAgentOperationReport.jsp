<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentOperationReport.jsp
//程序功能：F1报表生成
//创建日期：2006-02-20
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%
  String FlagStr = "";
  String Content = "";
  System.out.println("ssssss");
  
  //得到excel文件的保存路径
    String sql = "select SysvarValue "
                + "from ldsysvar "
                + "where sysvar='SaleXmlPath'";
    ExeSQL tExeSQL = new ExeSQL();
    String subPath = tExeSQL.getOneValue(sql);
    
  Calendar cal = new GregorianCalendar();
  String year = String.valueOf(cal.get(Calendar.YEAR));
  String month=String.valueOf(cal.get(Calendar.MONTH)+1);
  String date=String.valueOf(cal.get(Calendar.DATE));
  String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String now = year + month + date + hour + min + sec + "_" ;
  
  String  millis = String.valueOf(System.currentTimeMillis());  
  String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".csv";
  subPath+=fileName;
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
  String realPath=application.getRealPath("/")+"/"+subPath;
  System.out.println("..........download jsp getrealpath here :"+application.getRealPath("vtsfile"));
  //String tType = "3";
  System.out.println("OutXmlPath:" + tOutXmlPath);
  
    //接收信息，并作校验处理。
  //输入参数
  LAAgentOperationRBL tLAAgentOperationRBL   = new LAAgentOperationRBL();
  //输出参数
  CErrors tError = null;
  //String tRela  = "";                
  boolean operFlag=true;
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	try{
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   String state = "0";
   String flag = "0";
   String TAgentState1 =  "";
   String TAgentState2 = "";
   String AgentState1 = "";
   String AgentState2 = "";
   tG=(GlobalInput)session.getValue("GI");
   String tManageCom=tG.ManageCom;
   String ManageCom = request.getParameter("ManageCom");
   String sql1="select name from ldcom where comcode='"+ManageCom+"'";
   ExeSQL tExeSQL1 = new ExeSQL();
   String tName = tExeSQL1.getOneValue(sql1);
   System.out.println(ManageCom+"名字："+tName);

  //接受参数

	String tOperationBranch = request.getParameter("OperationBranch");
	String tBranchType = request.getParameter("BranchType");
	String tBranchType2 = request.getParameter("BranchType2");
	String tAgentState = request.getParameter("AgentState");
	String tAgentGrade = request.getParameter("AgentGrade");
 	System.out.println("参数：销售团队代码"+tOperationBranch+"统计状态"+tAgentState); 

	String YYMMDD = "";
	String tAgentGroup = "";
	
if(tOperationBranch !=null && !tOperationBranch.equals("")){
	state="0";
}else{
state="1";
tOperationBranch="zxz";
}

if(tAgentState.equals("01"))
{ 
TAgentState1 = "06";
AgentState1 = "2";
TAgentState2 = "zxc";
AgentState2 = "2";
}
else if (tAgentState.equals("02"))
{
TAgentState1 = "zxc";
AgentState1 = "1";
TAgentState2 = "06";
AgentState2 = "1";
}
else
	{
TAgentState1 = "zxc";
AgentState1 = "1";
TAgentState2 = "zxc";
AgentState2 = "2";
	}
   XmlExport txmlExport = new XmlExport();
   VData tVData = new VData();
   VData mResult = new VData();
   // 准备传输数据 VData
    tVData.add(ManageCom);
	tVData.add(tOperationBranch);
	tVData.add(tBranchType);
	tVData.add(tBranchType2);
	tVData.add(tAgentState);
	tVData.add(TAgentState1);
	tVData.add(TAgentState2);
	tVData.add(TAgentState2);
	tVData.add(AgentState2);
	tVData.add(tName);
	tVData.add(state);
	tVData.add(tG);
    tVData.add(realPath);
    tVData.add(tAgentGrade);
    
  	 //调用打印的类
  	 System.out.println(".............begin submit here");
    if (!tLAAgentOperationRBL.submitData(tVData,"PRINT")){
	      FlagStr="Fail";
	      Content=tLAAgentOperationRBL.mErrors.getFirstError().toString();    
    }
	 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
	 	  //File file = new File(tOutXmlPath);
	 	  File file = new File(realPath);
	 	  System.out.println("..................report jsp here ");
	      response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[4096];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {      
              if (input != null) input.close();
              if (output != null) output.close();
              //file.delete();
          }
  }catch(Exception ex){
  	ex.printStackTrace();
  }
  if(FlagStr.equals("Fail")){

%>
<html>
<script language="javascript">
	alert(<%=Content%>);
	//top.close();
</script>
</html>
<%}%>