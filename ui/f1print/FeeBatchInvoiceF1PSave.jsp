<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.pubfun.SysMaxNoPicch.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
	//tG.ClientIP = request.getRemoteAddr();
	tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
	tG.ServerIP =tG.GetServerIP();
  
  
  CError cError = new CError( );

  String FlagStr = "True";
  String Content = "操作成功";
  String strOperation = "";
  String PrtSeq = "";
  String ContStr ="";

  strOperation = request.getParameter("fmtransact");
  System.out.println("strOperation => "+strOperation);

  String tChk[]=request.getParameterValues("InpPolGridChk");
  String tIncomeNo[] = request.getParameterValues("PolGrid4");
  String tPayNo[] = request.getParameterValues("PolGrid3");
  String tType[] = request.getParameterValues("PolGrid5");

  CErrors mErrors = new CErrors();

  String certifyCode 	 = request.getParameter("CertifyCode");  
  
  String hPerson =StrTool.unicodeToGBK(request.getParameter("HPerson"));
  String cPerson =StrTool.unicodeToGBK(request.getParameter("CPerson"));
  String remark =StrTool.unicodeToGBK(request.getParameter("Remark")); 
  String tStartNo = StrTool.unicodeToGBK(request.getParameter("StaNo"));
  String tEndNo = StrTool.unicodeToGBK(request.getParameter("EndNo"));
  String tCertifyFlag = "";
  String tPrintAmount = "0";
  String tPayer = "";
  if (request.getParameterValues("CertifyFlag") != null){
  	tCertifyFlag = "1";
	}
	
  String tFPDM = request.getParameter("FPDM");
  String tInvoiceState = request.getParameter("InvoiceState");

  System.out.println("strOperation : "+strOperation);   	 	  
  try
  {    
		if(tChk!=null){
			LJAPaySet tLJAPaySet = new LJAPaySet();
			for(int i=0;i<tChk.length;i++){
				if(tChk[i]!=null && tChk[i].equals("1")){			
  				LJAPaySchema tLJAPaySchema = new LJAPaySchema();
      		tLJAPaySchema.setPayNo(tPayNo[i]);  
      		tLJAPaySet.add(tLJAPaySchema);
       	}
		 	}
		 	System.out.println("strOperation : 11"); 
		 	if("5".equals(tInvoiceState)){
	        	tPrintAmount = request.getParameter("PrintAmount");
	        	tPayer = StrTool.unicodeToGBK(request.getParameter("Payer"));
	        	       	
	        }
		 	LZCardSchema tLZCardSchema = new LZCardSchema();
		 	tLZCardSchema.setCertifyCode(certifyCode);
		 	tLZCardSchema.setStartNo(tStartNo);
		 	tLZCardSchema.setEndNo(tEndNo);
		 	
		 	TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("CertifyCode",certifyCode);
  		tTransferData.setNameAndValue("HPerson",cPerson);
  		tTransferData.setNameAndValue("CPerson",hPerson);
  		tTransferData.setNameAndValue("Remark",remark);
  		tTransferData.setNameAndValue("CertifyFlag",tCertifyFlag);
  		tTransferData.setNameAndValue("FPDM",tFPDM);
  		tTransferData.setNameAndValue("InvoiceState",tInvoiceState);
   		tTransferData.setNameAndValue("PrintAmount",tPrintAmount);
		tTransferData.setNameAndValue("Payer",tPayer);
  		System.out.println("strOperation : 22"); 
		 	VData mVData = new VData();
      mVData.addElement(tLJAPaySet);
      mVData.addElement(tLZCardSchema);
  	  mVData.addElement(tG);
  	  mVData.addElement(tTransferData);
  	  
  	   if(strOperation.equals("notbatch"))
  		{ 
   			 LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
             for(int i=1;i<=tLJAPaySet.size();i++){
            LJAPaySchema tLJAPaySchema=tLJAPaySet.get(i);
     
             try{
            LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
            String tLimit = PubFun.getNoLimit(tG.ManageCom);
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManager2Schema.setPrtSeq(prtSeqNo);
            tLOPRTManager2Schema.setOtherNo(tLJAPaySchema.getPayNo());
            tLOPRTManager2Schema.setOtherNoType("05");
            tLOPRTManager2Schema.setCode(PrintManagerBL.CODE_NOTINVOICE);
            tLOPRTManager2Schema.setManageCom(tG.ManageCom);
            tLOPRTManager2Schema.setAgentCode(tLJAPaySchema.getAgentCode());
            tLOPRTManager2Schema.setReqCom(tG.ManageCom);
            tLOPRTManager2Schema.setReqOperator(tG.Operator);
            tLOPRTManager2Schema.setExeOperator(tG.Operator);
            tLOPRTManager2Schema.setPrtType("0");
            tLOPRTManager2Schema.setStateFlag("1");
            tLOPRTManager2Schema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManager2Schema.setMakeTime(PubFun.getCurrentTime());
          	tLOPRTManager2Schema.setPrintAmount(tPrintAmount);
            tLOPRTManager2Schema.setPayer(tPayer);
            tLOPRTManager2Schema.setStandbyFlag5(tInvoiceState);
            
            LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
            tLOPRTManager2DB.setSchema(tLOPRTManager2Schema);
            tLOPRTManager2DB.insert();
          }catch(Exception e){
            e.fillInStackTrace(); 
    	   cError.moduleName = "FeeBatchInvoiceF1PSave";
    	   cError.functionName = "insert";
    	   cError.errorMessage = "LOPRTManager2DB.insert发生错误，但是没有提供详细的出错信息";
    	   mErrors.addOneError(cError);
    	   System.out.println("cError-->"+cError);
          }

         }
 		 }else{	 	  
  	  System.out.println("strOperation : 33"); 
  		
			FeeBatchInvoiceF1PUI tFeeBatchInvoiceF1PUI = new FeeBatchInvoiceF1PUI();
    	if(!tFeeBatchInvoiceF1PUI.submitData(mVData,strOperation))
    	{
    	   mErrors.copyAllErrors(tFeeBatchInvoiceF1PUI.mErrors);
    	   cError.moduleName = "FeeBatchInvoiceF1PSave";
    	   cError.functionName = "submitData";
    	   cError.errorMessage = "FeeBatchInvoiceF1PUI发生错误，但是没有提供详细的出错信息";
    	   mErrors.addOneError(cError);
    	   System.out.println("cError-->"+cError);
    	   FlagStr = "Fail";
    	   Content = strOperation+"失败，原因是:" + mErrors.getFirstError();
    	}
    	}
		}//end if(chk!=null)
	}
	catch(Exception ex)
	{
	  FlagStr = "Fail";
		Content = "打印失败，数据异常。";
  }
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>


 

