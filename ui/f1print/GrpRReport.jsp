<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="GrpRReport.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpRReportInit.jsp"%>
  <title>打印团体契调通知书 </title>
</head>
<body  onload="initForm();" >
  <form action="./FirstPayQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入投保单查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>  印刷号   </TD>
          <TD  class= input>  <Input class= common name=prtno verify="印刷号码|int&len>=11&len<=12"> </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   

         <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode  ondblclick="return showCodeList('AgentCodet2',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
        </TR> 
    </table>
          <INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();"> 
  </form>
  <form action="./GrpRReportF1pSave.jsp" method=post name=fmSave target="f1print">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 投保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton  TYPE=button onclick="getLastPage();"> 	
  	</div>
  	<p>
      <INPUT VALUE="打印团体契调通知书" class= CssButton TYPE=button onclick="printPol();">
  	</p>
  	<input type=hidden id="fmtransact" name="fmtransact">
  	
  	<input type=hidden id="PolNo" name="PolNo">
  	<input type=hidden id="PrtSeq" name="PrtSeq">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>