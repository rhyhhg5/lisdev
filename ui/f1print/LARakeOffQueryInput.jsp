<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html> 
<%
//程序名称：LARakeOffQueryInput.jsp
//程序功能：营销员佣金查询
//创建日期：2005-5-25 14:51
//创建人  ： LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="LARakeOffQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <title> 无主管销售单位 </title>
</head>
<body>
  <form method=post name=fm target="fraSubmit" action="./LARakeOffQuery.jsp">
    <!-- 集体信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			  <td class= titleImg align= center>请输入查询条件：</td>
		  </tr>
	  </table>
    <table  class= common align=center>
     <TR  class= common>
       <TD  class= title>
         管理机构
       </TD>
       <TD  class= input>
         <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
       </TD>
       <TD  class= title>
         销售机构
       </TD>
       <TD  class= input>
         <Input class="common" name=BranchAttr>
       </TD>
     </TR>
     <TR  class= common>
       <TD  class= title>
         营销员编码
       </TD>
       <TD  class= input>
         <Input class="common" name=AgentCode>
       </TD>
       <TD  class= title>
         佣金年月（YYYYMM）
       </TD>
       <TD  class= input>
         <!--<Input class='coolDatePicker' name=RakeOffDate dateFormat='short'>-->
         <Input class='common' name=RakeOffDate>
       </TD>
      </TR>
    </table>
    <INPUT TYPE="hidden" NAME="op" VALUE="">
    <INPUT TYPE="hidden" NAME="pmWhere" VALUE="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		<!--<INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()">-->
		<!--<INPUT VALUE="test" class="cssButton" TYPE="button" onclick="aa()">-->
		
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>