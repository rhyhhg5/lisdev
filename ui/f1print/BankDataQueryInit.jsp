<%
//程序名称：BankDataQuery.jsp
//程序功能：财务报盘数据查询
//创建日期：2004-10-20
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容
%>                         
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
	//System.out.println(curDate);
%> 

<script language="JavaScript">

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";

function initForm()
{
  try
  {
    initInpBox();
    initCodeGrid();
  }
  catch(re)
  {
    alert("BankDataQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCodeGrid()    							//函数名为init+MulLine的对象名ObjGrid
{                           						
	var iArray = new Array(); 						//数组放置各个列
	try
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         			//列名
    	iArray[0][1]="30px";         			//列宽
    	iArray[0][2]=100;         				//列最大值
    	iArray[0][3]=0;         					//1表示允许该列输入，0表示只读且不响应Tab键
    																		//2表示为容许输入且颜色加深
    																		
        iArray[1]=new Array();
	    iArray[1][0]="序列";    			//列名（序号列，第1列）
	    iArray[1][1]="0px";  						//列宽
	    iArray[1][2]=100;      						//列最大值
	    iArray[1][3]=3;   								//1表示允许该列输入，0表示只读且不响应Tab键
	                   										//2表示为容许输入且颜色加深.

	    iArray[2]=new Array();
	    iArray[2][0]="管理机构";    			//列名（序号列，第1列）
	    iArray[2][1]="100px";  						//列宽
	    iArray[2][2]=100;      						//列最大值
	    iArray[2][3]=0;   								//1表示允许该列输入，0表示只读且不响应Tab键
	                   										//2表示为容许输入且颜色加深.
	                                    	
	    iArray[3]=new Array();          	
	    iArray[3][0]="营销部门";  					//列名（第2列）
	    iArray[3][1]="80px";  						//列宽
	    iArray[3][2]=100;        					//列最大值
	    iArray[3][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	       																//后续可以添加N列，如上设置
	    iArray[4]=new Array();          	
	    iArray[4][0]="保单号";  				//列名（第2列）
	    iArray[4][1]="80px";  						//列宽
	    iArray[4][2]=100;        					//列最大值
	    iArray[4][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	       																//后续可以添加N列，如上设置
	
	    iArray[5]=new Array();          	
	    iArray[5][0]="投保人";  				//列名（第2列）
	    iArray[5][1]="80px";  						//列宽
	    iArray[5][2]=100;        					//列最大值
	    iArray[5][3]=0;       						//是否允许输入,1表示允许，0表示不允许	   
//	    iArray[4][4]="bank";              	        //是否引用代码:null||""为不引用
//	    iArray[4][5]="3";              	                //引用代码对应第几列，'|'为分割符
//	    iArray[4][9]="银行名称|code:bank&NOTNULL";
//	    iArray[4][18]=250;
//	    iArray[4][19]= 0 ;   																//后续可以添加N列，如上设置

	    iArray[6]=new Array();          	
	    iArray[6][0]="联系电话";  				//列名（第2列）
	    iArray[6][1]="80px";  						//列宽
	    iArray[6][2]=100;        					//列最大值
	    iArray[6][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	       																//后续可以添加N列，如上设置
		iArray[7]=new Array();          	
	    iArray[7][0]="保单生效日";  				//列名（第2列）
	    iArray[7][1]="80px";  						//列宽
	    iArray[7][2]=100;        					//列最大值
	    iArray[7][3]=0;       						//是否允许输入,1表示允许，0表示不允许

//		iArray[8]=new Array();          	
//	    iArray[8][0]="应交险种";  				//列名（第2列）
//	    iArray[8][1]="80px";  						//列宽
//	    iArray[8][2]=100;        					//列最大值
//	    iArray[8][3]=0;       						//是否允许输入,1表示允许，0表示不允许

		iArray[8]=new Array();          	
	    iArray[8][0]="应收保费";  				//列名（第2列）
	    iArray[8][1]="80px";  						//列宽
	    iArray[8][2]=100;        					//列最大值
	    iArray[8][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[9]=new Array();          	
	    iArray[9][0]="帐户名";  				//列名（第2列）
	    iArray[9][1]="80px";  						//列宽
	    iArray[9][2]=100;        					//列最大值
	    iArray[9][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[10]=new Array();          	
	    iArray[10][0]="银行名称";  				//列名（第2列）
	    iArray[10][1]="80px";  						//列宽
	    iArray[10][2]=100;        					//列最大值
	    iArray[10][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[11]=new Array();          	
	    iArray[11][0]="发盘日期";  				//列名（第2列）
	    iArray[11][1]="80px";  						//列宽
	    iArray[11][2]=100;        					//列最大值
	    iArray[11][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[12]=new Array();          	
	    iArray[12][0]="回盘日期";  				//列名（第2列）
	    iArray[12][1]="80px";  						//列宽
	    iArray[12][2]=100;        					//列最大值
	    iArray[12][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[13]=new Array();          	
	    iArray[13][0]="金额";  				//列名（第2列）
	    iArray[13][1]="80px";  						//列宽
	    iArray[13][2]=100;        					//列最大值
	    iArray[13][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[14]=new Array();          	
	    iArray[14][0]="是否成功";  				//列名（第2列）
	    iArray[14][1]="80px";  						//列宽
	    iArray[14][2]=100;        					//列最大值
	    iArray[14][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[15]=new Array();          	
	    iArray[15][0]="扣款失败原因";  				//列名（第2列）
	    iArray[15][1]="80px";  						//列宽
	    iArray[15][2]=100;        					//列最大值
	    iArray[15][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[16]=new Array();          	
	    iArray[16][0]="应收日";  				//列名（第2列）
	    iArray[16][1]="80px";  						//列宽
	    iArray[16][2]=100;        					//列最大值
	    iArray[16][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[17]=new Array();          	
	    iArray[17][0]="交费截止日期";  				//列名（第2列）
	    iArray[17][1]="80px";  						//列宽
	    iArray[17][2]=100;        					//列最大值
	    iArray[17][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[18]=new Array();          	
	    iArray[18][0]="营销机构";  				//列名（第2列）
	    iArray[18][1]="80px";  						//列宽
	    iArray[18][2]=100;        					//列最大值
	    iArray[18][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[19]=new Array();          	
	    iArray[19][0]="业务员代码";  				//列名（第2列）
	    iArray[19][1]="80px";  						//列宽
	    iArray[19][2]=100;        					//列最大值
	    iArray[19][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[20]=new Array();          	
	    iArray[20][0]="业务员姓名";  				//列名（第2列）
	    iArray[20][1]="80px";  						//列宽
	    iArray[20][2]=100;        					//列最大值
	    iArray[20][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[21]=new Array();          	
	    iArray[21][0]="业务员电话";  				//列名（第2列）
	    iArray[21][1]="80px";  						//列宽
	    iArray[21][2]=100;        					//列最大值
	    iArray[21][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[22]=new Array();          	
	    iArray[22][0]="应收号";  				//列名（第2列）
	    iArray[22][1]="0px";  						//列宽
	    iArray[22][2]=100;        					//列最大值
	    iArray[22][3]=3;       						//是否允许输入,1表示允许，0表示不允许
	    iArray[22][21]="PayCode";
	    

	    //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
	    CodeGrid= new MulLineEnter( "fm" , "CodeGrid" ); 
	    //设置属性区 (需要其它特性，在此设置其它属性)
	    CodeGrid.mulLineCount = 0 ; 		//行属性：设置行数=10    
	    CodeGrid.displayTitle = 1;   		//标题属性：1显示标题(缺省值), 0隐藏标题        
	    //对象初始化区：调用对象初始化方法，属性必须在此前设置
	    CodeGrid.hiddenSubtraction=1;
	    CodeGrid.hiddenPlus=1;
	    //CodeGrid.canChk=1;
	    CodeGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{ alert(ex); }
}

function initInpBox()
{ 
  try
  {
	fm.all('ManageCom').value = manageCom;
	var sql1 = "select Name from LDCom where ComCode = '" + manageCom + "'";
    fm.all('ManageComName').value = easyExecSql(sql1);
	fm.all('BankCode').value = '';
	var sql1 = "select Current Date - 3 month from dual ";
	fm.all('SendDate').value = easyExecSql(sql1);
	fm.all('SendEndDate').value=tCurrentDate;
	fm.all('PolNo').value = '';
	fm.all('AccName').value = '';
	fm.all('SerialNo').value = '';
	fm.all('BankSuccFlag').value = '';
	showAllCodeName();
  }
  catch(ex)
  {
    alert("BankDataQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}    
    
</script>