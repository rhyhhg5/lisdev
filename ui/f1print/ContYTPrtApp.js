//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{ 
		var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

		arrReturn = getQueryResult();
		PrtSeq = PolGrid.getRowColData(tSel-1,1);
		
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		fmSave.target="f1print";
		fmSave.PrtSeq.value = PrtSeq;
		fmSave.PolNo.value = arrReturn[0][1];
		fmSave.fmtransact.value = "CONFIRM";
/*
		if(RePrintFlag==1){
			fmSave.target = "fraSubmit";
			}
*/
		fmSave.action="./DeferAppF1PSave.jsp?LoadFlag=Defer&RePrintFlag="+RePrintFlag;
		fmSave.submit();
		showInfo.close();
		if(RePrintFlag!=1){
			easyQueryClick();
			}
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
  }
}

function afterSubmit2( FlagStr, content )
{
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";	
	var tStateFlag="";
	if(fm.all('StateFlag').value=="null"||fm.all('StateFlag').value==null||fm.all('StateFlag').value=="")
 	 		{
    			tStateFlag="";
 	 		}
  else{
			if (fm.all('StateFlag').value=="0")
	  			{
		  				tStateFlag=" and a.stateflag='0' ";
	  			}
	  	else if (fm.all('StateFlag').value=="9")
	  			{
		 	 				tStateFlag="";
	  			}
	  	else
	  			{
	  			tStateFlag=" and a.stateflag<>'0' ";
	  			}
			}	
	// 书写SQL语句
	 strSQL = "select a.PrtSeq,b.prtno,b.polapplydate,b.CvaliDate,b.AppntName,b.prem,(select getUniteCode(b.AgentCode) from dual),b.managecom,a.makedate,"
		      + "'',''"
		      + ",'','','',b.contno from loprtmanager a,LCCont b where 1=1 "
					+ " and a.code='07_YT'"
					+ " and a.otherno=b.contno ";//2007-10-22
	if(fm.agentGroup.value != null && fm.agentGroup.value != "")//2007-10-22
			strSQL += " and b.AgentGroup in (select agentgroup from LABranchGroup bg where bg.agentGroup = '" + fm.agentGroup.value + "') ";
	//2014-11-4  杨阳
	//把新的业务员编码转成旧的
	if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
		strSQL+=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
	}
	strSQL += getWherePart('b.prtno', 'PrtNo') 
	        + getWherePart('a.otherno', 'ContNo') 
					+ getWherePart('b.ManageCom', 'ManageCom', 'like')
			    + getWherePart('a.MakeDate','MakeDate')
			    + getWherePart('b.AppntName','AppntName')
			    + getWherePart('b.CvaliDate','CvaliDate')
			    + getWherePart('b.polapplydate','PolApplyDate')
			    + " and a.stateflag='0' "
			    + " with ur "
					;
	if(RePrintFlag=="1")
	{
		strSQL = "select a.PrtSeq,b.prtno,b.polapplydate,b.CvaliDate,b.AppntName,b.prem,(select getUniteCode(b.AgentCode) from dual),b.managecom,a.makedate,"
		      + "'',(case when a.stateflag='0' then '未打' else '已打' end) "
		      + ",'','','',b.contno from loprtmanager a,LCCont b where 1=1 "
					//+ " and a.code='06'"
                    + " and a.code='07_YT'"
                    // + " and b.uwflag in ('1', '8', 'a') "
					+ " and a.otherno=b.contno ";//2007-10-22
		if(fm.agentGroup.value != null && fm.agentGroup.value != "")//2007-10-22
			strSQL += " and b.AgentGroup in (select agentgroup from LABranchGroup bg where bg.agentGroup = '" + fm.agentGroup.value + "') ";
		//2014-11-4  杨阳
		//把新的业务员编码转成旧的
		if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
			strSQL+=" and b.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
		}
		strSQL += getWherePart('b.prtno', 'PrtNo') 
	        + getWherePart('a.otherno', 'ContNo') 
					+ getWherePart('b.ManageCom', 'ManageCom', 'like')
			    + getWherePart('a.MakeDate','MakeDate')
			    + getWherePart('b.AppntName','AppntName')
			    + getWherePart('b.CvaliDate','CvaliDate')
			    + getWherePart('b.polapplydate','PolApplyDate')
			    + tStateFlag
			    + " with ur "
					;
	}
		  
	turnPage.strQueryResult  = easyQueryVer3(strSQL);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要打印的溢缴通知书！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //tArr = chooseArray(arrDataSet,[0,1,3,4]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  	fm.agentGroup.value = arrResult[0][0];
  }
}

//下拉框选择后执行   zhangjianbao   2007-11-16
function afterCodeSelect(cName, Filed)
{	
  if(cName=='statuskind')
  {
    saleChnl = fm.saleChannel.value;
	}
}

//选择营销机构前执行   zhangjianbao   2007-11-16
function beforeCodeSelect()
{
	if(saleChnl == "") alert("请先选择销售渠道");
}

//pdf提交，保存按钮对应操作
function printPolpdf()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();
  //var testPol = PolGrid.getRowColData();  
  //alert(tSel);
  if( tSel == 0 || tSel == null )
  {
        alert( "请先选择一条记录，再点击返回按钮。" );
        return;
    }
    else
    {
        arrReturn = getQueryResult();
            if( null == arrReturn ) {
              alert("无效的数据");
            return;
           }
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        //arrReturn = getQueryResult();
        //ContNo=arrReturn[0][0];
        tPrtSeq = PolGrid.getRowColData(tSel-1,1);
        tMissionID = PolGrid.getRowColData(tSel-1,13);
        tSubMissionID = PolGrid.getRowColData(tSel-1,14);
        tPrtNo = PolGrid.getRowColData(tSel-1,2);
        tContNo = PolGrid.getRowColData(tSel-1,15);
        //alert(ContNo);
        fmSave.PrtSeq.value = tPrtSeq;
        //fmSave.MissionID.value = tMissionID;
        //fmSave.SubMissionID.value = tSubMissionID;
        //fmSave.PrtNo.value = tPrtNo;
        //fmSave.ContNo.value = tContNo ;
        fmSave.fmtransact.value = "PRINT";
        fmSave.target = "fraSubmit";
        fmSave.action="../uw/PDFPrintSave.jsp?Code=07_YT&OtherNo=" + tContNo + "&PrtSeq" + tPrtSeq;
        fmSave.submit();
        showInfo.close();
    }
}
