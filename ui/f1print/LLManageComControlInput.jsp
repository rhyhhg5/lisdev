<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LAMarketInput.jsp
	//程序功能：F1报表生成
	//创建日期：2007-11-13
	//创建人  ：xx
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String tManageCom = tG.ManageCom;
	int len = tManageCom.length();
	String CurrentDate = PubFun.getCurrentDate();

	String AheadDays = "-90";
	FDate tD = new FDate();
	Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate), Integer
			.parseInt(AheadDays), "D", null);
	FDate fdate = new FDate();
	String afterdate = fdate.getString(AfterDate);
%>
<script language="javascript">
   function initDate(){
      fm.ManageCom.value=<%=tManageCom%>;
      fm.StartDate.value="<%=afterdate%>";
      fm.EndDate.value="<%=CurrentDate%>";
   }
   </script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLManageComControlInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initDate();">
<form action="./LLManageComControlSave.jsp" method=post name=fm
	target="f1print">
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLAAgent1);"></td>
		<td class=titleImg>工作量管控报表</td>
	</tr>
</table>
<table class=common border=0 width=100%>
	<TR class=common>
		<TD class=title>管理机构</TD>
		<TD class=input><Input class="codeno" name=ManageCom
			verify="管理机构|code:comcode&NOTNULL&INT"
			ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);"
			onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input
			class=codename name=ComName></TD>

		<TD class=title>案件类型</TD>
		<TD class=input><input class="codeno"
			CodeData="0|4^1|常规案件^2|批量案件^3|简易理赔案件^4|批量受理导入案件" name=ContType
			ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);"
			onkeydown="QueryOnKeyDown();"
			onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);"><input
			class=codename name=ContTypeName></TD>
		<TD class=title>理赔用户</TD>
		<TD class=input><input class="codeno" name=UserCode
			ondblclick="return showCodeList('optname',[this,UserName],[0,1],null,null,null,1,180);"
			onkeyup="return showCodeListKey('optname',[this,UserName],[0,1]);"><input
			class=codename name=UserName></TD>
	</TR>
	<TR class=common>
		<TD class=title>统计起期</TD>
		<TD class=input><Input class="coolDatePicker" dateFormat="short"
			name=StartDate verify="统计起期|DATE"></TD>
		<TD class=title>统计止期</TD>
		<TD class=input><Input class="coolDatePicker" dateFormat="short"
			name=EndDate verify="统计止期|Date"></TD>
	</TR>
	<TR class=common>
		<TD class=title>统 计</TD>
		<TD class=input8><input class=codeno
			CodeData="0|6^0|用户工作量管控报表^1|机构工作量管控报表" name=DealWith
			ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);"
			onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);"><input
			class=codename name=DealWithName></TD>
	</TR>
</table>
<table>
	<TR>
		<TD><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divCare1);"></TD>
		<TD class=titleImg>案件类型说明：</TD>
	</TR>
</table>
<Div id="divCare1" style="display: ''">
<tr class="common">
	<td class="title">常规案件：同过正常个案受理或批次下个案单独受理</td>
</tr>
<br>
<tr class="common">
	<td class="title">批量案件：批次下通过批量导入受理的案件</td>
</tr>
<br>
<tr class="common">
	<td class="title">简易理赔案件：通过简易理赔受理的案件</td>
</tr>
<br>
<tr class="common">
	<td class="title">批量受理导入案件：批次下通过批量受理导入的案件</td>
</tr>
<br>
</Div>
<table>
	<TR>
		<TD><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divCare);"></TD>
		<TD class=titleImg>用户工作量管控统计字段说明：</TD>
	</TR>
</table>
<Div id="divCare" style="display: ''">
<tr class="common">
	<td class="title">受理:案件处理人为统计用户且受理日期在统计期间内的目前为受理状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">扫描:案件处理人为统计用户且受理日期在统计期间内的目前为扫描状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">检录:案件处理人为统计用户且受理日期在统计期间内的目前为检录状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">理算：案件处理人为统计用户且受理日期在统计期间内的目前为理算状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">审批：当前待审批人为统计用户且受理日期在统计期间内目前为审批状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">审定：案件处理人为统计用户且受理日期在统计期间内的目前为审定状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">调查：案件处理人为统计用户且受理日期在统计期间内的目前为调查状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">查讫：案件处理人为统计用户且受理日期在统计期间内的目前为查讫状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">抽检：当前待处理人为统计用户且受理日期在统计期间内目前为抽检状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">撤件：案件处理人为统计用户且受理日期在统计期间内的目前为撤件状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">延迟：案件处理人为统计用户且受理日期在统计期间内的目前为延迟状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">回退：回退人为统计用户且受理日期在统计期间内的管理机构内未结案案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">理赔二核：案件处理人为统计用户且受理日期在统计期间内的目前为理赔二核状态的管理机构内案件数</td>
</tr>
<br>
<tr class="common">
	<td class="title">合计：受理案件数+扫描案件数+检录案件数+理算案件数+审批案件数+审定案件数+调查案件数+查讫案件数+抽检案件数+延迟案件数+理赔二核案件数</td>
</tr>
<br>
</Div>
<input type="hidden" name=op value=""> <!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->
</form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
