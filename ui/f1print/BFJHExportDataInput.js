//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var End = fm.EndDate.value;
	var cur = fm.CurrentDate.value;	
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
	fm.all('divSpanLACommisionGrid').style.display='';
	fm.all('divSpanLACommisionGrid1').style.display='none';
    //首先检验录入框
  if(!verifyInput()) return false;
  var tStartDate=fm.all('StartDate').value;
  var tEndDate=fm.all('EndDate').value;   
 var  sql= "select  aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,pp,qq,rr,ss,tt,sum(uu),vv from ("
+"select case when b.transtype='WT'  then '2' when b.transtype='ZC' and b.transmoney<0 then '2'  else '1' end aa,'1' bb,a.contno cc,'020' dd,a.polapplydate ee,a.riskcode ff"
+",e.riskname gg,value((select value(trim(banknode),'') from lkcodemapping where agentcom=b.agentcom fetch "
+"first 1 rows only),'') hh,'' ii,case when e.risktype4='4' then '101' when e.risktype='A' then '104' when e.risktype='H' and e.risktype4<>'4' then '103' when e.risktype='L' and e.risktype4='2' then '100' else "
+"'999' end jj,a.prtno kk,a.appntname ll,"
+"(select idno from lcappnt where contno=a.contno and appntno=a.appntno) mm,"
+"a.cvalidate nn,case when a.payintv=0 then '1' else '2' end oo,"
+"decimal(abs(a.prem),12,2) pp,case when a.payintv=0 then a.prem else decimal(abs(a.prem*a.payyears),12,2) end qq,"
+"a.insuyear rr,case when a.payintv=0 then 1 else a.payyears end ss,value((select decimal(chargerate*100,12,2) from lacharge where commisionsn=b.commisionsn),0) tt,"
+"value((select decimal(abs(charge),12,2) from lacharge where commisionsn=b.commisionsn),0) uu,current date vv "
+"from lcpol a,lacommision b,laagent d,lmriskapp e "
+"where a.contno=b.contno and a.riskcode=b.riskcode and "
+"b.agentcode=d.agentcode and b.riskcode=e.riskcode "
+"and a.managecom like '8611%' "
+"and a.agentcom like 'PY004%' "
+"and a.polapplydate>='"+tStartDate+"' and a.polapplydate<='"+tEndDate+"' "
+"and a.salechnl='04' "
+"and a.conttype='1' "
+"union "
+"select case when b.transtype='WT' then '2' when b.transtype='ZC' and b.transmoney<0 then '2'  else '1' end aa,'1' bb,a.contno cc,'020' dd,a.polapplydate ee,a.riskcode ff,"
+"e.riskname gg,value((select value(trim(banknode),'') from lkcodemapping where agentcom=b.agentcom fetch "
+"first 1 rows only),'') hh,'' ii,"
+"case when e.risktype4='4' then '101' when e.risktype='A' then '104' when e.risktype='H' and e.risktype4<>'4' then '103' when e.risktype='L' and e.risktype4='2' then '100'  else "
+"'999' end jj,a.prtno kk,a.appntname ll,"
+"(select idno from lcappnt where contno=a.contno and appntno=a.appntno) mm,"
+"a.cvalidate nn,case when a.payintv=0 then '1' else '2' end oo,"
+"decimal(abs(a.prem),12,2) pp,case when a.payintv=0 then a.prem else decimal(abs(a.prem*a.payyears),12,2) end qq,"
+"a.insuyear rr,case when a.payintv=0 then 1 else a.payyears end ss,value((select decimal(chargerate*100,12,2) from lacharge where commisionsn=b.commisionsn),0) tt,"
+"value((select decimal(abs(charge),12,2) from lacharge where commisionsn=b.commisionsn),0) uu,current date vv  "
+"from lbpol a,lacommision b,laagent d,lmriskapp e "
+"where a.contno=b.contno and a.riskcode=b.riskcode and "
+"b.agentcode=d.agentcode and b.riskcode=e.riskcode "
+"and a.managecom like '8611%' "
+"and a.agentcom like 'PY004%' "
+"and a.polapplydate>='"+tStartDate+"' and a.polapplydate<='"+tEndDate+"' "
+"and a.salechnl='04' "
+"and a.conttype='1' "
+"union "
+"select '3' aa,'1' bb,a.contno cc,'020' dd,a.polapplydate ee,a.riskcode ff,e.riskname gg,"
+"value((select value(trim(banknode),'') from lkcodemapping where agentcom=a.agentcom fetch first 1 rows only),'') hh,'' ii,"
+"case when e.risktype4='4' then '101' when e.risktype='A' then '104' when e.risktype='H' and e.risktype4<>'4' then '103'  when e.risktype='L' and e.risktype4='2' then '100' else '999' end jj,"
+"a.prtno kk,a.appntname ll,"
+"(select idno from lcappnt where contno=a.contno and appntno=a.appntno) mm,"
+"a.cvalidate nn,case when a.payintv=0 then '1' else '2' end oo,"
+"decimal(abs(a.prem),12,2) pp,case when a.payintv=0 then a.prem else decimal(abs(a.prem*a.payyears),12,2) end qq,"
+"a.insuyear rr,case when a.payintv=0 then 1 else a.payyears end ss,0 tt,"
+"0 uu,current date vv  "
+"from lbpol a,lpedoritem b,laagent d,lmriskapp e "
+"where a.contno=b.contno and "
+"a.agentcode=d.agentcode and a.riskcode=e.riskcode "
+"and a.managecom like '8611%' "
+"and a.agentcom like 'PY004%' "
+"and b.edortype in ('XT','CT') "
+"and a.polapplydate>='"+tStartDate+"' and a.polapplydate<='"+tEndDate+"' "
+"and a.salechnl='04' "
+"and a.conttype='1' ) as aaaaa group by aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll"
+",mm,nn,oo,pp,qq,rr,ss,tt,vv order by ee,cc,aa with ur";   
	turnPage.queryModal(sql, LACommisionGrid);   	
	
}
function easyQueryClick1()
{
	fm.all('divSpanLACommisionGrid').style.display='none';
	fm.all('divSpanLACommisionGrid1').style.display='';
    //首先检验录入框
  if(!verifyInput()) return false;   
 
  var  sql= 
           "select (select name from lacom where agentcom=LACharge.AgentCom) comName,(select name from ldcom where comcode=LACharge.ManageCom) ManageName,"
 					+"(select name from labranchgroup where agentgroup=LACOMMISION.agentgroup)  groupname, "
 					+"LACOMMISION.agentcode, "
 					+"(select  name from laagent where agentcode=LACOMMISION.agentcode) agentName  , "
 					+"LACharge.ContNo, "
 					+"(select  appntname  from lccont where contno=LACharge.contno union select  appntname  from lbcont where contno=LACharge.contno) appntname, "
 					+"LACharge.RiskCode, sum(LACharge.TransMoney), LACOMMISION.tenteraccdate,LACOMMISION.signdate,(select CInValiDate from lccont where contno =LACharge.contno union select CInValiDate from lbcont where contno =LACharge.contno ) CInValiDate  " 			
 					+"from 	LACharge ,LACOMMISION where 1=1 "
 					+ " AND LACharge.CommisionSN = LACOMMISION.CommisionSN"
          + " and LACharge.GrpContNo = '00000000000000000000' and LACharge.WageNo>='"+fm.all('WageNo').value+"'and LACharge.WageNo<='"+fm.all('WageNo1').value+"' " 
          + getWherePart('LACharge.ManageCom', 'ManageCom')
          + getWherePart('LACharge.BranchType', 'BranchType')
          + getWherePart('LACharge.AgentCom', 'AgentCom') 
          + " group by LACharge.AgentCom,LACharge.ManageCom,LACOMMISION.agentgroup,LACOMMISION.agentcode,"  
          + "  LACharge.RiskCode,  LACOMMISION.tenteraccdate,LACOMMISION.signdate,lacharge.contno"     	 
          	 ;  
	turnPage.queryModal(sql, LACommisionGrid1); 
}
function afterCodeSelect(codeName,Field)
{
	if(codeName == "BranchType2")
	{
          var  sql=" select enddate+1 day  from  lawagelog  where 1=1 and  wageno=(select max(wageno) from lawagelog where branchtype='"+fm.BranchType.value+"' "
                     +"  and  branchtype2='"+fm.BranchType2.value+"' and managecom='"+fm.ManageCom.value+"') "
                     + getWherePart('ManageCom', 'ManageCom')
                     + getWherePart('BranchType', 'BranchType')
          	     + getWherePart('BranchType2', 'BranchType2');
          var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
       
   //查询成功则拆分字符串，返回二维数组
   
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);
	
          if( tArr != null )
	  {
           fm.all('StartDate').value= tArr[0][0];                                   
             
          }
	}
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("计算起期必须小于等于计算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}

function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#2# and BranchType2=#02# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}
function doDownLoad(){
	if(!verifyInput()) return false;   
 var tStartDate=fm.all('StartDate').value;
  var tEndDate=fm.all('EndDate').value;  
 var  sql= "select aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,pp,qq,rr,ss,tt,sum(uu),vv from ("
+"select case when b.transtype='WT' then '2' when b.transtype='ZC' and b.transmoney<0 then '2' else '1' end aa,'1' bb,a.contno cc,'020' dd,a.polapplydate ee,a.riskcode ff"
+",e.riskname gg,value((select value(trim(banknode),'') from lkcodemapping where agentcom=b.agentcom fetch "
+"first 1 rows only),'') hh,'' ii,case when e.risktype4='4' then '101' when e.risktype='A' then '104' when e.risktype='H' and e.risktype4<>'4' then '103' when e.risktype='L' and e.risktype4='2' then '100'  else "
+"'999' end jj,a.prtno kk,a.appntname ll,"
+"(select idno from lcappnt where contno=a.contno and appntno=a.appntno) mm,"
+"a.cvalidate nn,case when a.payintv=0 then '1' else '2' end oo,"
+"decimal(abs(a.prem),12,2) pp,case when a.payintv=0 then a.prem else decimal(abs(a.prem*a.payyears),12,2) end qq,"
+"a.insuyear rr,case when a.payintv=0 then 1 else a.payyears end ss,value((select decimal(chargerate*100,12,2) from lacharge where commisionsn=b.commisionsn),0) tt,"
+"value((select decimal(abs(charge),12,2) from lacharge where commisionsn=b.commisionsn),0) uu,current date vv "
+"from lcpol a,lacommision b,laagent d,lmriskapp e "
+"where a.contno=b.contno and a.riskcode=b.riskcode and "
+"b.agentcode=d.agentcode and b.riskcode=e.riskcode "
+"and a.managecom like '8611%' "
+"and a.agentcom like 'PY004%' "
+"and a.polapplydate>='"+tStartDate+"' and a.polapplydate<='"+tEndDate+"' "
+"and a.salechnl='04' "
+"and a.conttype='1' "
+"union "
+"select case when b.transtype='WT' then '2' when b.transtype='ZC' and b.transmoney<0 then '2' else '1' end aa,'1' bb,a.contno cc,'020' dd,a.polapplydate ee,a.riskcode ff,"
+"e.riskname gg,value((select value(trim(banknode),'') from lkcodemapping where agentcom=b.agentcom fetch "
+"first 1 rows only),'') hh,'' ii,"
+"case when e.risktype4='4' then '101' when e.risktype='A' then '104' when e.risktype='H' and e.risktype4<>'4' then '103' when e.risktype='L' and e.risktype4='2' then '100'  else "
+"'999' end jj,a.prtno kk,a.appntname ll,"
+"(select idno from lcappnt where contno=a.contno and appntno=a.appntno) mm,"
+"a.cvalidate nn,case when a.payintv=0 then '1' else '2' end oo,"
+"decimal(abs(a.prem),12,2) pp,case when a.payintv=0 then a.prem else decimal(abs(a.prem*a.payyears),12,2) end qq,"
+"a.insuyear rr,case when a.payintv=0 then 1 else a.payyears end ss,value((select decimal(chargerate*100,12,2) from lacharge where commisionsn=b.commisionsn),0) tt,"
+"value((select decimal(abs(charge),12,2) from lacharge where commisionsn=b.commisionsn),0) uu,current date vv  "
+"from lbpol a,lacommision b,laagent d,lmriskapp e "
+"where a.contno=b.contno and a.riskcode=b.riskcode and "
+"b.agentcode=d.agentcode and b.riskcode=e.riskcode "
+"and a.managecom like '8611%' "
+"and a.agentcom like 'PY004%' "
+"and a.polapplydate>='"+tStartDate+"' and a.polapplydate<='"+tEndDate+"' "
+"and a.salechnl='04' " 
+"and a.conttype='1' "
+"union "
+"select '3' aa,'1' bb,a.contno cc,'020' dd,a.polapplydate ee,a.riskcode ff,e.riskname gg,"
+"value((select value(trim(banknode),'') from lkcodemapping where agentcom=a.agentcom fetch first 1 rows only),'') hh,'' ii,"
+"case when e.risktype4='4' then '101' when e.risktype='A' then '104' when e.risktype='H' and e.risktype4<>'4' then '103' when e.risktype='L' and e.risktype4='2' then '100'  else '999' end jj,"
+"a.prtno kk,a.appntname ll,"
+"(select idno from lcappnt where contno=a.contno and appntno=a.appntno) mm,"
+"a.cvalidate nn,case when a.payintv=0 then '1' else '2' end oo,"
+"decimal(abs(a.prem),12,2) pp,case when a.payintv=0 then a.prem else decimal(abs(a.prem*a.payyears),12,2) end qq,"
+"a.insuyear rr,case when a.payintv=0 then 1 else a.payyears end ss,0 tt,"
+"0 uu,current date vv  "
+"from lbpol a,lpedoritem b,laagent d,lmriskapp e "
+"where a.contno=b.contno and "
+"a.agentcode=d.agentcode and a.riskcode=e.riskcode "
+"and a.managecom like '8611%' "
+"and a.agentcom like 'PY004%' "
+"and b.edortype in ('XT','CT') "
+"and a.polapplydate>='"+tStartDate+"' and a.polapplydate<='"+tEndDate+"' "
+"and a.salechnl='04' "
+"and a.conttype='1' ) as aaaaa group by aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll"
+",mm,nn,oo,pp,qq,rr,ss,tt,vv order by ee,cc,aa with ur"; 
  fm.all('querySQL').value=sql;
  fm.submit();
}