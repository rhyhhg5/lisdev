<%
//程序名称：LLManageComControlSave.jsp
//程序功能：工作量管控报表-机构工作量管控报表
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="java.io.*"%>

<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
ReportUI
</title>
<head>
</head>
<body>
<%
System.out.println("=======工作量管控报表-机构工作量管控报表===========");
 
boolean operFlag = true;
String flag = "0";
String FlagStr = "";
String Content = "";
CError cError = new CError( );
CErrors tError = null;
XmlExport txmlExport = new XmlExport();
CombineVts tcombineVts = null;
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String tManageCom=tG.ManageCom;
String ManageCom = request.getParameter("ManageCom");
String tContType = request.getParameter("ContType");
String fileNameB = tG.Operator + "_" + FileQueue.getFileName()+".vts";
String tStartDate = request.getParameter("StartDate");
String tEndDate = request.getParameter("EndDate");
//待定 隐藏的参数

System.out.println("=======print ManageCom==========="+ManageCom);
TransferData tTransferData= new TransferData();
//传参
tTransferData.setNameAndValue("tManageCom",ManageCom);
tTransferData.setNameAndValue("tContType",tContType);
tTransferData.setNameAndValue("tFileNameB",fileNameB);
tTransferData.setNameAndValue("tFileName","机构调查案件明细");
tTransferData.setNameAndValue("tOperator",tG.Operator);
tTransferData.setNameAndValue("tStartDate",tStartDate);
tTransferData.setNameAndValue("tEndDate",tEndDate);
 
RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);

VData tVData = new VData();
tVData.addElement(tG);
tVData.addElement(tTransferData);
          
//LAMarketClaimUI tLAMarketUI = new LAMarketClaimUI(); 
LLManageComControlBL tLLManageComControlBL = new LLManageComControlBL();
    
  try{
    if(!tLLManageComControlBL.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tLLManageComControlBL.mErrors.getFirstError();  
   		System.out.println(Content);
    }
        VData mResult = tLLManageComControlBL.getResult();
    System.out.println("=======qwewewe===========");	
	  txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  if (txmlExport==null)
	  {
	    System.out.println("null");
	  }
	  System.out.println("开始打开报表!");
	  
	  ExeSQL tExeSQL = new ExeSQL();
		//获取临时文件名
		String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
		String strFilePath = tExeSQL.getOneValue(strSql);
		String strVFFileName = strFilePath + fileNameB;
		//获取存放临时文件的路径
		String strRealPath = application.getRealPath("/").replace('\\','/');
		String strVFPathName = strRealPath +"//"+ strVFFileName;
	  System.out.println("null12");
	  //合并VTS文件
		String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
	System.out.println("null13");
		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);
	System.out.println("null14");
		//把dataStream存储到磁盘文件
		AccessVtsFile.saveToFile(dataStream,strVFPathName);
		System.out.println("==> Write VTS file to disk ");
	System.out.println("null15");
		System.out.println("===strVFFileName : "+strVFFileName);
		
		rpt.updateReportMetaData(fileNameB);
		//本来打算采用get方式来传递文件路径
		response.sendRedirect("../uw/GetF1PrintJ1.jsp?Code=03&RealPath="+strVFPathName);
	}
	catch(Exception ex)
	{
		Content = "失败，原因是:" + ex.toString();
    FlagStr = "Fail";
    tError = tLLManageComControlBL.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 处理失败! ";
      FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();	
	 	
</script>
</html>
<%
  }
%>