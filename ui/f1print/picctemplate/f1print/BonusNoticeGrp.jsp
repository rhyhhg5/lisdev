 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="BonusNoticeGrp.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BonusNoticeGrpInit.jsp"%>
  <title>打印红利通知书 </title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>  团体保单号码   </TD>
          <TD  class= input>  <Input class= common name=GrpPolNo > </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">  </TD>   
        </TR> 
        
      	<TR  class= common>
          <TD  class= title>  险种编码   </TD>
          <TD  class= input>  <Input class="code" name=RiskCode ondblclick="return showCodeList('riskcode',[this]);" onkeyup="return showCodeListKey('riskcode',[this]);">  </TD>   
       	  <TD  class= title>  红利分配会计年度   </TD>
          <TD  class= input>  <Input class= common name=FiscalYear > </TD>          
       </TR>   
            
        <!--
        <TR  class= common>
         <TD  class= title> 代理人编码 </TD>
          <TD  class= input>  <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this]);" onkeyup="return showCodeListKey('AgentCode',[this]);">   </TD> 
         <TD  class= title> 代理人组别 </TD>
          <TD  class= input>  <Input class="code" name=AgentGroup ondblclick="return showCodeList('AgentGroup',[this]);" onkeyup="return showCodeListKey('AgentGroup',[this]);">   </TD>
        </TR>
        --> 
        
    </table>
          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
  </form>
  <form action="./BonusNoticeGrpF1PSave.jsp" method=post name=fmSave target="f1print">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBonusGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common  TYPE=button onclick="getLastPage();"> 	
  	</div>
  	<p>
      <INPUT VALUE="打印企业帐户年度报告" class= common TYPE=button onclick="printPolBonus();">
  	</p>

  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="PrtSeq" name="PrtSeq">
  </form>
  
  <form action="./BonusNoticeGrpList.jsp" method=post name=fmList target="f1print">
  	<table  class= common>
       <TD  class= input>  <Input class= common type=hidden name=PrtSeq2 > </TD>        
      	<TR  class= common>
          <TD  class= title>  团体保单号码   </TD>
          <TD  class= input>  <Input class= common name=GrpPolNoForList > </TD>
          <TD  class= title>  红利分配会计年度   </TD>
          <TD  class= input>  <Input class= common name=FiscalYearForList  > </TD>          
       </TR>
       <tr  class= common>
  	      <p>
           <INPUT VALUE="打印团体下个人帐户年度清单" class= common TYPE=button onclick="printPolBonusForList();">
  	      </p>
       </tr>	
    </table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>