
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.f1print.*"%>
<%@page contentType="text/html;charset=GBK"%>
<%!String handleFunction(HttpSession session, HttpServletRequest request) {
		String tManageCom = request.getParameter("ManageCom");
			System.out.println(" ManageCom = " + tManageCom);
		String tAction = request.getParameter("Action");

		String strOperation = "UPDATE";

		GlobalInput globalInput = new GlobalInput();

		if ((GlobalInput) session.getValue("GI") == null) {
			return "网页超时或者是没有操作员信息，请重新登录";
		} else {
			globalInput.setSchema((GlobalInput) session.getValue("GI"));
		}

		PillotConfigBL tPillotConfigBL = new PillotConfigBL();
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("ManageCom",tManageCom);
		transferData.setNameAndValue("mAction", tAction);

		VData vData = new VData();
		vData.add(globalInput);
		vData.add(transferData);

		try {
			if (!tPillotConfigBL.submitData(vData, strOperation)) {
				if (tPillotConfigBL.mErrors.needDealError()) {
					return tPillotConfigBL.mErrors.getFirstError();
				} else {
					return "保存失败，但是没有详细的原因";
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return ex.getMessage();
		}
		return "";
	}%>
<%
	String FlagStr = "";
	String Content = "";

	try {
		Content = handleFunction(session, request);

		if (Content.equals("")) {
			FlagStr = "Succ";
			Content = "操作成功";
		} else {
			FlagStr = "Fail";
		}
	} catch (Exception ex) {
		ex.printStackTrace();
	}
%>
<html>
	<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

