//               该文件中包含客户端需要处理的函数和事件
var arrDataSet;
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass(); 
var mIncomeNo = "";
var mPayNo = "";
var multLineFlag = true;

window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try 
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function submitForm()
{
  if(verifyInput()) 
  {
  	//if(fm.StartNo.value==""){
  	//	alert("未在单证管理系统中对业管部发票打印用户发放发票号段动作,不能进行发票打印!");
  	//	return false;
  	//}else{
  	fm.submit();
  //	} //提交
  }
  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, tCode )
{
	if ((tCode!=null||tCode!=""))
	{
		freshNum();
	}
	else
	{
		showInfo.close();
	  if (FlagStr == "Fail" )
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }
	  else
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
	  	//parent.fraInterface.initForm();
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	
	    showDiv(operateButton,"true");
	    showDiv(inputButton,"false");
	    //执行下一步操作
	  }
	}
}

/***************************************************************
*		程序功能:打印后更新主页面单证序号
*
****************************************************************/
function freshNum() 
{	
		var strSQL = "";
		if (fm.MngCom.value.length<=4)
		{
			strSQL = "select min(StartNo) from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='A"+fm.MngCom.value+"' and StateFlag in ('0','7')";
		}
		else
		{
			strSQL = "select min(StartNo) from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='B"+fm.Operator.value+"' and StateFlag in ('0','7')";
		}
	
		var arrResult = easyExecSql(strSQL);
		//alert(arrResult[0]);
		if (arrResult==null||arrResult=="")
		{
			alert("没有可用发票！"); 
		} 
		else 
		{
			fm.StartNo.value=arrResult; 
		}
}

// 查询按钮
function easyQueryClick()
{	
  //submitForm();
 
  sendValue();
  if(!verifyInput()) 
  {
  	return false;
  }
  
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select PayNo,IncomeNo,IncomeType,SumActuPayMoney,PayDate,ConfDate,Operator,a.ManageCom,a.AgentCode from LJAPay a where 1=1 ";			 
				 //+ getWherePart( 'PayNo' )
	if (fm.PrtNo.value!=null && fm.PrtNo.value != "")
	{ 
		if (fm.IncomeType.value == "1")
		{
			strSQL = strSQL + " and exists (select '1' from lcgrpcont where grpcontno=a.incomeno and prtno = '"+fm.PrtNo.value+"')";
		}
		else if (fm.IncomeType.value == "2") 
		{  //modify by yingxl 2008-05-28
		  if(fm.BankContFlag.value == "1")
		  {
		    strSQL = strSQL + " and exists (select '1' from lccont where contno=a.incomeno and prtno = '"+fm.PrtNo.value+"'  and cardflag = '9' and salechnl = '04')";
		  }
		  if(fm.BankContFlag.value == "2")
		  { 
		    strSQL = strSQL + " and exists (select '1' from lccont where contno=a.incomeno and prtno = '"+fm.PrtNo.value+"' )";
		    strSQL = strSQL + " and not exists (select '1' from lccont where contno=a.incomeno and cardflag = '9' )";
		    
		  }
		  if(fm.BankContFlag.value == "3")
		  {
		    strSQL = strSQL + " and exists (select '1' from lccont where contno=a.incomeno and prtno = '"+fm.PrtNo.value+"')";
	    }
	    //end modify by yingxl 2008-05-28
	  }
	}
	else 
	{
	  if (fm.IncomeType.value == "2")
		{
			if(fm.BankContFlag.value == "1")
		  {
		    strSQL = strSQL + " and exists (select '1' from lccont where contno=a.incomeno and cardflag = '9' and salechnl = '04')";
		  }
		  if(fm.BankContFlag.value == "2")
		  { 
		    strSQL = strSQL + " and not exists (select '1' from lccont where contno=a.incomeno and  cardflag = '9' )";
		  }
		  if(fm.BankContFlag.value == "3")
		  {
		    strSQL = strSQL + " and exists (select '1' from lccont where contno=a.incomeno )";//where prtno = '"+fm.PrtNo.value+"'
	    }
	  } 
	}	//modify by yingxl 2008-05-28 银保通业务判断结束
		
	if(fm.IncomeType.value == "15")
	{			
		strSQL = " select a.PayNo,a.IncomeNo,b.ContNo,a.IncomeType,b.SumActuPayMoney,a.PayDate,a.ConfDate,a.Operator,a.ManageCom,a.AgentCode from LJAPay a,LJAPayPerson b where 1=1 and a.PayNo=b.PayNo "
		+ getWherePart( 'b.ContNo' ,'PrtNo');
	}		 
	if(fm.IncomeType.value == "3")
	{			
		strSQL = strSQL + " and a.IncomeType in('3','13') ";
	}
	else
	{	
		strSQL = strSQL + getWherePart( 'a.IncomeType', 'IncomeType');
	}
	strSQL = strSQL	 + getWherePart( 'a.IncomeNo','IncomeNo' )
	       + getWherePart( 'a.ConfDate' ,'EnterAccDate')				 
				 + getWherePart( 'a.AgentCode','AgentCode' )
         +" and  not exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='35' and stateflag='1')";
	if (fm.MngCom.value == null || fm.MngCom.value == "" )
	{	
	}	
	else
	{
		if(fm.IncomeType.value != "15")
	  {			
		   strSQL = strSQL + getWherePart( 'a.ManageCom','MngCom','like' );
	  }else
	  {
	  	 strSQL = strSQL + getWherePart( 'a.ManageCom','MngCom' );
	  }
	}
	if(fm.IncomeType.value == "15")
	{			
		strSQL = strSQL + "order by a.IncomeNo,a.PayDate,a.ConfDate,b.contno";
	}else{
		strSQL = strSQL + "order by a.IncomeNo,a.PayDate,a.ConfDate";
	}	
	

	if(RePrintFlag == "1") //RePrintFlag记录是否重打
	{
		strSQL = "select PayNo,IncomeNo,IncomeType,SumActuPayMoney,PayDate,ConfDate,Operator,a.ManageCom,a.AgentCode from LJAPay a where 1=1 ";			 
				 //+ getWherePart( 'PayNo' )
		if (fm.PrtNo.value!=null && fm.PrtNo.value != "")
		{
			if (fm.IncomeType.value == "1")
			{
				strSQL = strSQL + " and exists (select '1' from lcgrpcont where grpcontno=a.incomeno and prtno = '"+fm.PrtNo.value+"')";
			}
			else if (fm.IncomeType.value == "2") 
			{ 
			  strSQL = strSQL + " and exists (select '1' from lccont where contno=a.incomeno and prtno = '"+fm.PrtNo.value+"')";
		  }
		}
		if(fm.IncomeType.value == "15")
	 {			
		strSQL = " select a.PayNo,a.IncomeNo,b.ContNo,a.IncomeType,b.SumActuPayMoney,a.PayDate,a.ConfDate,a.Operator,a.ManageCom,a.AgentCode from LJAPay a,LJAPayPerson b where 1=1 and a.PayNo=b.PayNo "
		+ getWherePart( 'b.ContNo' ,'PrtNo');
	 }	
		if(fm.IncomeType.value == "3")
		{			
			strSQL = strSQL + " and IncomeType in('3','13') ";
		}
		else
		{	
			strSQL = strSQL + getWherePart( 'a.IncomeType', 'IncomeType');
		}	 
		strSQL = strSQL	 + getWherePart( 'a.IncomeNo','IncomeNo' )
	       + getWherePart( 'a.ConfDate' ,'EnterAccDate')				 
				 + getWherePart( 'a.AgentCode','AgentCode' )		
				 +" and exists (select '1' from LOPRTManager2 b where otherno=a.payno and code='35' and stateflag='1')";
					 	
		if (fm.MngCom.value == null || fm.MngCom.value == "" )
		{	
		}	
		else
		{
			if(fm.IncomeType.value != "15")
		  {			
			   strSQL = strSQL + getWherePart( 'a.ManageCom','MngCom','like' );
		  }else
		  {
		  	 strSQL = strSQL + getWherePart( 'a.ManageCom','MngCom' );
		  }			
		}				
		if(fm.IncomeType.value == "15")
		{			
			strSQL = strSQL + "order by a.IncomeNo,a.PayDate,a.ConfDate,b.contno";
		}else{
			strSQL = strSQL + "order by a.IncomeNo,a.PayDate,a.ConfDate";
		}	
		
	}
	fm.SQLstr.value = strSQL;
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL); 
    
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
    //alert("查询失败！");
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=没有查询到符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0; 
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);   
}

//发票打印
function PPrint()
{
	var tIncomeNo = "";
	var tempIncomeNo = "";	
	var arrReturn = new Array();
  if (PolGrid.mulLineCount==0){
	 	   alert("不存在满足条件的发票信息！");
	 	   return;
	 }
	 if(PolGrid.getRowColData(0,1)==null ||PolGrid.getRowColData(0,1)=="" ){
	 	 alert("不存在要打印的发票信息,请查询发票信息！");
	 	 return;
	 }	 	
	if (fm.IncomeType.value !='15'){			
		
		var tSel = PolGrid.getSelNo();
		
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击发票打印按钮。" );
		else
		{
			arrReturn = getQueryResult();
		  var cPayNo = PolGrid.getRowColData(tSel - 1,1);		
			if (cPayNo == "")
			    return;
			}  
		fm.PayNo.value = cPayNo;		
		fm.IncomeType.value = arrReturn[0][2];
	}
	if (fm.IncomeType.value =='15')	
	{	
		
		 for(var i=0;i< PolGrid.mulLineCount;i++ ){			 		 	 	
		 	 if(PolGrid.getChkNo(i,PolGrid)){  		
		  		if (tIncomeNo == ""){
		  		 	 tempIncomeNo = PolGrid.getRowColData(i ,2);
		  		 	
		  		 }
		  		 tIncomeNo = PolGrid.getRowColData(i ,2);		  		 		   			
		  		 if (tempIncomeNo != tIncomeNo)
		  		 {  	  	
				 	  	 alert("请选择同一个结算单进行打印！");
				 	  	 return ;
				 	 }		 	
  	   }	  
		 }
	}
			
  if(RePrintFlag == "1")
  {
  	fm.action="FeeInvoiceF1PSave.jsp?RePrintFlag=1";
 	  fm.target="f1print";
	  fm.fmtransact.value="CONFIRM";
	  submitForm();
  }
  	else
  {
	  fm.action="FeeInvoiceF1PSave.jsp";
	  fm.target="f1print";
	  fm.fmtransact.value="CONFIRM";
	  submitForm();
	}

}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function showAppntInfo() {
  var strSql = "";
  if (fm.IncomeType.value == "1" )
  {
  	strSql = "select distinct GrpName,RiskName,a.riskcode from lcgrppol a,lmriskapp b where GrpContNo = '" 
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2) 
             + "' and a.riskcode=b.riskcode order by a.riskcode";
  }
  if (fm.IncomeType.value == "2" )
  { 
    strSql = "select distinct (select name from ldperson where customerno=a.appntno) appntname,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
             + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
             + "when '530301' then (select riskwrapplanname from ldcode1 "
             + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,a.paycount from ljapayperson a,lmriskapp b,lccont c where c.ContNo = a.ContNo " 
             + "and a.ContNo = '"+PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2) 
             +"' and a.confdate='"+PolGrid.getRowColData(PolGrid.getSelNo() - 1, 6)
             + "' and a.riskcode=b.riskcode order by a.paycount desc";     
  }        
  if (fm.IncomeType.value == "3" )
  {
  	if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3) == "3")
  	{
    strSql = "select distinct b.GrpName,RiskName,b.riskcode from LJAGetEndorse a, lcgrppol b, lmriskapp c where ActuGetNo = '" 
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1) 
             + "' and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode order by b.riskcode";     
    }
    if (PolGrid.getRowColData(PolGrid.getSelNo() - 1, 3) == "13")
    {       
    	strSql = "Select distinct b.GrpName,RiskName,b.riskcode from ljagetendorse a,lcgrppol b,lmriskapp c "
             +"where actugetno in ( Select Btactuno from ljaedorbaldetail "
             +"where actuno in ( Select getnoticeno from LJAPay where payno='"
             +PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1)+"' )) "
             +"and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode order by b.riskcode "
    }           
  }             
  if (fm.IncomeType.value == "10" )
  {             
    strSql = "select distinct AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
             + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
             + "when '530301' then (select riskwrapplanname from ldcode1 "
             + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode from LJAGetEndorse a, lcpol b, lmriskapp c where ActuGetNo = '"
             + PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1) 
             + "' and a.contno=b.contno and b.riskcode=c.riskcode order by b.riskcode";          
  }                
 //alert(strSql);      
  var arrResult = easyExecSql(strSql);
  if(arrResult== null){ return ;}
  fm.all("AppntName").value = arrResult[0][0];
  if (arrResult.length>1)
  {
      fm.all("RiskName").value = arrResult[0][1]+"等";
  }
  else
  	{
  		fm.all("RiskName").value = arrResult[0][1];
  	}
  
  
}
//显示定额单证的信息
function showDAppntInfo() {
	
  
  var strPayNo = "";
  var strIncomeNo = ""; 
  mIncomeNo = "";
  var  ContStr = "";
  for (var i=0;i< PolGrid.mulLineCount;i++){
  	if(PolGrid.getChkNo(i,PolGrid)){
  		
  		if (mIncomeNo == ""){
  		 	 strIncomeNo = PolGrid.getRowColData(i ,2);		
  		 }
  		 mIncomeNo = PolGrid.getRowColData(i ,2); 		
   		 strPayNo = PolGrid.getRowColData(i ,1);
  		 if (mIncomeNo != strIncomeNo)
  		 {  
  		 	   multLineFlag = false;	  	
		 	  	 alert("请选择同一个结算单进行打印！");
		 	  	 return ;
		 	 }		 	
  	}
  } 
  nameGet();
  fm.PayNo.value = strPayNo;
  fm.IncNo.value = mIncomeNo;
}

/*********************************************************************
 *  取得险种名称
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function nameGet() {
	var strSql = "select distinct '',b.RiskName,b.riskcode from lmcardrisk a,lmriskapp b,LZCardPay c,LMCertifyDes d where c.PayNo = '" 
             + mIncomeNo
             + "' and a.riskcode=b.riskcode and d.CertifyCode=a.CertifyCode and d.subcode=c.cardtype "
             + " order by  b.riskcode";
  var arrResult = easyExecSql(strSql);
  
  if (arrResult == null){return;}
  fm.all("AppntName").value = arrResult[0][0];
  
  if (arrResult.length>1)
  {
     fm.all("RiskName").value = arrResult[0][1]+"等";
  }
  else
	{
		 fm.all("RiskName").value = arrResult[0][1];
	}  
}
/*********************************************************************
 *  选择暂交费类型后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect(cCodeName, Field) {	 		
	 	 
	 	fm.all('PayNo').value = '';
    fm.all('IncomeNo').value = '';    
    fm.all('EnterAccDate').value = '';   
    fm.all('AgentCode').value = '';
    fm.all('HPerson').value = '';
    fm.all('CPerson').value = '';
    fm.all('Remark').value = '';	  
   
	 initPolGrid();	 
	
	 if (fm.all("IncomeType").value =='')
	 {
	 	  fm.all("IncomeType").value = fm.all("IncomeType1").value ;
	 }
	 if(fm.all("IncomeType").value =="15"){
	    divFeeNoJS.style.display="";
	    divFeeOthJS.style.display="";
	    divFeeNo.style.display="none";
	    divFeeOth.style.display="none";	    
	    fm.all("IncomeType1").value =fm.all("IncomeType").value;
	    fm.all("IncomeType").value="";
	    divFeeButtonJS.style.display="";	
	    divFeeButton.style.display="none";	 	   
	    
	 }else{
		 	
		 	divFeeNoJS.style.display="none";
	 	  divFeeOthJS.style.display="none";
	 	  divFeeButtonJS.style.display="none";	
		  divFeeNo.style.display="";
		  divFeeOth.style.display="";
		  divFeeButton.style.display="";			 
	}	
	
	if(fm.all("IncomeType").value =="2"){
		fm.all("BankContFlag").disabled=false;
		fm.all("BankContFlagName").disabled=false;
	}else{
		if(fm.all("IncomeType").value !=""){
			fm.all("BankContFlag").disabled=true;
			fm.all("BankContFlagName").disabled=true;
			fm.all("BankContFlag").value="3";
			fm.all("BankContFlagName").value="全部";
		}
	}
	 	 
	
}

/*********************************************************************
 *  全部打印
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function allPrint() {
	 var tIncomeNo = "";
	 var tPayNo = "";
	 var tempIncomeNo = "";
	 var ContStr =""; 
	 var tempPayNo = "";	 
	 
	 if (PolGrid.mulLineCount==0){
	 	   alert("不存在满足条件的单证信息！");
	 	   return;
	 }
	 if(PolGrid.getRowColData(0,1)==null ||PolGrid.getRowColData(0,1)=="" ){
	 	 alert("不存在要打印的单证信息,请查询单证信息！");
	 	 return;
	 }
	 tIncomeNo = PolGrid.getRowColData(0,2) ;
	 tPayNo = PolGrid.getRowColData(0,1) ;
	 ContStr = "'"+ tPayNo +"'";
	 for(var i=1;i< PolGrid.mulLineCount;i++ ){
		 	tempIncomeNo = PolGrid.getRowColData(i,2); 	
			if (tempIncomeNo!=tIncomeNo){
		 	   alert("请选择同一个结算单进行打印！");
		 	   return;
		  }		  
		  ContStr = ContStr + ",'" + PolGrid.getRowColData(i,1)+"'";
		  
	}
	fm.IncNo.value = tIncomeNo;
  fm.PayNo.value = tPayNo;

	if(RePrintFlag == "1")
  {
  	fm.action="FeeInvoiceF1PSave.jsp?RePrintFlag=1&AllPrintFlag=1&ContStr="+ContStr;
 	  fm.target="f1print";
	  fm.fmtransact.value="CONFIRM";
	  submitForm();
  }
  	else
  {
	  fm.action="FeeInvoiceF1PSave.jsp?AllPrintFlag=1&ContStr="+ContStr;
	  fm.target="f1print";
	  fm.fmtransact.value="CONFIRM";
	  submitForm();
	}
}
/*********************************************************************
 *  传送值
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function sendValue() {	
	 if(divFeeNoJS.style.display==''){
	    fm.all('IncomeNo').value = fm.all('IncomeNo1').value;
      fm.all('IncomeType').value = fm.all('IncomeType1').value;
      fm.all('EnterAccDate').value = fm.all('EnterAccDate1').value;
      fm.all('MngCom').value = fm.all('MngCom1').value;
	    fm.all('AgentCode').value = fm.all('AgentCode1').value;
	    fm.all('PrtNo').value = fm.all('PrtNo1').value;
	 }
}