<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContactCompareListPrint.jsp
//程序功能：
//创建日期：2019-07-31
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "核保问题清单_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    //服务器
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    //本地
    //String tOutXmlPath = filePath +downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String tStartDate = request.getParameter("StartDate");
    String tEndDate = request.getParameter("EndDate");
    String tManageCom = request.getParameter("ManageCom");
    
    ProblemListPrintBL tbl =new ProblemListPrintBL();
    GlobalInput tGlobalInput = new GlobalInput();
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("StartDate", tStartDate);
    tTransferData.setNameAndValue("EndDate", tEndDate);
    tTransferData.setNameAndValue("ManageCom", tManageCom);
    VData tData = new VData();
    tData.add(tG);
    tData.add(tTransferData);

    CreateExcelList tCreateExcelList=new CreateExcelList();
    tCreateExcelList=tbl.getsubmitData(tData,"PRINT");
	if(tCreateExcelList==null){
   		errorFlag=false;
    	System.out.println("EXCEL生成失败！");
    }else{
       		errorFlag=true;
    }
    
    if(errorFlag)
    {
        //写文件到磁盘
        try
        {
            tCreateExcelList.write(tOutXmlPath);
        }
        catch(Exception e)
        {
            errorFlag = false;
            System.out.println(e);
        }
    }
    //返回客户端
    if(errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(!errorFlag)
    {
%>
<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>