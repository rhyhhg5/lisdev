<%@ page contentType="application/x-xls;charset=GBK"%>
<%@ page import="java.io.File,java.io.FileInputStream"%>
<%@ page import="javax.servlet.ServletOutputStream"%>
<%
//若使用字节流进行文件下载，应该首先清空一下out对象，默认是字符流
request.setCharacterEncoding("GBK");
response.setCharacterEncoding("GBK");
String filename=request.getParameter("filename");
String filenamepath=request.getParameter("filenamepath");
//filename=new String(filename.getBytes("UTF-8"), "GBK");
filename=java.net.URLDecoder.decode(filename, "UTF-8");
filenamepath=java.net.URLDecoder.decode(filenamepath, "UTF-8");
//filenamepath=new String(filenamepath.getBytes("UTF-8"), "GBK");
System.out.println("filename="+filename);
System.out.println("filenamepath="+filenamepath);
if(filenamepath.contains("./")||filenamepath.contains("../")||filenamepath.contains("%")){
	  return;
}
out.clear(); 
out = pageContext.pushBody();
//通过设置头标题来显示文件传送到前端浏览器的文件信息
response.setHeader("Content-disposition","attachment; filename="+java.net.URLEncoder.encode(filename, "UTF-8")); 
File file = new File(filenamepath);
response.setHeader("Content_Length",String.valueOf(file.length()));
FileInputStream input = null;
ServletOutputStream output = null;
try{
   input = new FileInputStream(file);
   output = response.getOutputStream();
   byte size[] = new byte[1024];
   int length;
   while((length=input.read(size))!=-1){
    output.write(size);
   }
}catch(Exception e){
   System.out.print(e.getMessage());
}finally{
   try{
    input.close();
    output.flush();
    output.close();
   }catch(Exception ex){
  
   }
}

%>