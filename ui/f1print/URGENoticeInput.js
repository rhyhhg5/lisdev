//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;
var SqlPDF = "";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();

  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		PrtSeq = PolGrid.getRowColData(tSel-1,1);
		var tCode = PolGrid.getRowColData(tSel-1,10);	
	
		fmSave.CodeType.value = tCode;
		fmSave.PrtSeq.value = PrtSeq;
		fmSave.fmtransact.value = "CONFIRM";
		fmSave.target = "_blank";	
		fmSave.action="./URGENoticeSave.jsp?RePrintFlag="+RePrintFlag;
		fmSave.submit();
		showInfo.close();				
	}
}
//zsjing于2007.06.19添加
function printPolpdf()
{	
	if (PolGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
  var i = 0;
  var count=0;
  var tChked = new Array();
  //var tSel = PolGrid.getSelNo();
  for (i=0;i<PolGrid.mulLineCount;i++)
  {
  	if (PolGrid.getChkNo(i)==true)
  	{
  		tChked[count++]=i;
  	}
  }
  if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}  
	else
	{
		//when '11' then '催办体检通知书' when '17' then '催办首期交费通知书' when '18' then '催办问题件通知书' when '19' then '催办承保计划变更通知书' end 
		tPrtSeq = PolGrid.getRowColData(tChked[0],1);
		tPrtNo = PolGrid.getRowColData(tChked[0],2);
		var tCode1 = PolGrid.getRowColData(tChked[0],10);	
		//alert(tCode1);
		var tCode="";
		fmSave.CodeType.value = tCode1;
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.PrtNo.value = tPrtNo;
		//alert(fmSave.PrtNo.value);
		fmSave.fmtransact.value = "PRINT";
		
		fmSave.action="../uw/PrintPDFSave.jsp?RePrintFlag="+RePrintFlag+"&Code=0"+tCode1;
		fmSave.submit();
	}
}

function printPolpdfbat()
{	
   if (PolGrid.mulLineCount == 0)
	  {
		  alert("打印列表没有数据");
		  return false;
	  }
	 var count = 0;
	 var tChked = new Array();
	 for(var i = 0; i < PolGrid.mulLineCount; i++)
	 {
		  if(PolGrid.getChkNo(i) == true)
		  {
			   tChked[count++] = i;
		  }
	 }
	 if(tChked.length == 1)
	 {
		 alert("请选择多条记录");
		 return false;	
	 }
	//alert(tChked.length);
	 if(tChked.length == 0)
	 {
	 	
		fmSave.action="./URGENoticeInsAllBatPrt.jsp?strsql="+SqlPDF;
		fmSave.submit();
	 }
	 else
	 {
	 //	alert("1");
		fmSave.action="./URGENoticeInsForBatPrt.jsp";
		fmSave.submit();
	 }
	
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	return arrSelected;	
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}


// 查询按钮
function easyQueryClick()
{

initPolGrid();
	
	var tStateFlag="";
	if(fm.all('StateFlag').value=="null"||fm.all('StateFlag').value==null||fm.all('StateFlag').value=="")
 	 		{
    			tStateFlag="";
 	 		}
  else{
			if (fm.all('StateFlag').value=="0")
	  			{
		  				tStateFlag=" and a.stateflag='0' ";
	  			}
	  	else if (fm.all('StateFlag').value=="9")
	  			{
		 	 				tStateFlag="";
	  			}
	  	else
	  			{
	  			tStateFlag=" and a.stateflag<>'0' ";
	  			}
			}
	
		var tLoadFlag=" (case a.stateflag when '0' then '未打印' else '已打印' end) , ";
		var tCodeType=" (case a.code when '11' then '催办体检通知书' when '17' then '催办首期交费通知书' when '18' then '催办问题件通知书' when '19' then '催办承保计划变更通知书' end ) " 
		
	// 书写SQL语句
	var strSQL = "";
	var strSQL2="";
		strSQL = "SELECT a.PrtSeq ,b.PrtNo ,b.PolApplyDate ,b.CValiDate ,b.appntname,( select getUniteCode(b.AgentCode) from dual),a.ManageCom ,"
	+ "a.makedate , b.prem,a.code,"	
	+ tLoadFlag
	+ tCodeType
	+ ",b.contno "
	+ "FROM LOPRTManager a,LCCont b WHERE  a.Code in( '11','17','18','19') " 	
	+ "and a.OtherNo = b.proposalContNo "		
	+ "and b.Appflag not in ('1') and b.uwflag not in ('1','8','a') and b.Conttype='1'  "
	+ " AND a.StateFlag = '0'"
	+ " AND a.PrtType = '0'";//2007-10-22
	if(fm.agentGroup.value != null && fm.agentGroup.value != "")//2007-10-22
		strSQL += " and b.AgentGroup in (select agentgroup from LABranchGroup bg where bg.branchattr = '" + fm.agentGroup.value + "') ";
	//2014-11-4  杨阳
	//把新的业务员编码转成旧的
	if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
		strSQL+=" and a.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
	}
	strSQL +=  getWherePart('b.CvaliDate', 'CvaliDate') 
	+ getWherePart('b.PrtNo', 'PrtNo')	
	+ getWherePart('b.AppntName', 'AppntName')	
	+ getWherePart('a.ManageCom', 'ManageCom', 'like')
	+ getWherePart('a.MakeDate','MakeDate')
	+ getWherePart('b.PolApplyDate','PolApplyDate')
	+ getWherePart('a.Code','NoticeType');
	+  " with ur ";
	
	strSQL2 = "SELECT a.PrtSeq ,b.PrtNo ,b.PolApplyDate ,b.CValiDate ,b.appntname,( select getUniteCode(b.AgentCode) from dual),a.ManageCom ,"
	+ "a.makedate , b.prem,a.code,"	
	+ tLoadFlag
	+"a.code,b.contno "
	+ "FROM LOPRTManager a,LCCont b WHERE  a.Code in( '11','17','18','19') " 	
	+ "and a.OtherNo = b.proposalContNo "		
	+ "and b.Appflag not in ('1') and b.uwflag not in ('1','8','a') and b.Conttype='1'  "
	+ " AND a.StateFlag = '0'"
	+ " AND a.PrtType = '0'";//2007-10-22
	if(fm.agentGroup.value != null && fm.agentGroup.value != "")//2007-10-22
		strSQL += " and b.AgentGroup in (select agentgroup from LABranchGroup bg where bg.branchattr = '" + fm.agentGroup.value + "') ";
	//2014-11-4  杨阳
	//把新的业务员编码转成旧的
	if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
		strSQL+=" and a.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
	}
	strSQL += getWherePart('b.CvaliDate', 'CvaliDate') 
	+ getWherePart('b.PrtNo', 'PrtNo')	
	+ getWherePart('b.AppntName', 'AppntName')	
	+ getWherePart('a.ManageCom', 'ManageCom', 'like')
	+ getWherePart('a.MakeDate','MakeDate')
	+ getWherePart('b.PolApplyDate','PolApplyDate')
	+ getWherePart('a.Code','NoticeType');
	+ " with ur "
	;

	if(RePrintFlag == "1"){
			// 书写SQL语句
	
		strSQL = "SELECT a.PrtSeq ,b.PrtNo ,b.PolApplyDate ,b.CValiDate ,b.appntname,( select getUniteCode(b.AgentCode) from dual),a.ManageCom ,"
	+ "a.makedate , b.prem,a.code,"	
	+ tLoadFlag
	+ tCodeType
	+ ",b.contno "
	+ "FROM LOPRTManager a,LCCont b WHERE  a.Code in( '11','17','18','19') " 	
	+ "and a.OtherNo = b.proposalContNo "		
	+ "and b.Appflag not in ('4','1')  and b.Conttype='1'  "
	+ tStateFlag
	+ " AND a.PrtType = '0'";//2007-10-22
	if(fm.agentGroup.value != null && fm.agentGroup.value != "")//2007-10-22
		strSQL += " and b.AgentGroup in (select agentgroup from LABranchGroup bg where bg.agentGroup = '" + fm.agentGroup.value + "') ";
	//2014-11-4  杨阳
	//把新的业务员编码转成旧的
	if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
		strSQL+=" and a.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
	}
	strSQL += getWherePart('b.CvaliDate', 'CvaliDate') 
	+ getWherePart('b.PrtNo', 'PrtNo')	
	+ getWherePart('b.AppntName', 'AppntName')	
	+ getWherePart('a.ManageCom', 'ManageCom', 'like')
	+ getWherePart('a.MakeDate','MakeDate')
	+ getWherePart('b.PolApplyDate','PolApplyDate')
	+ getWherePart('a.Code','NoticeType');
	+  " with ur ";
	
	strSQL2 = "SELECT a.PrtSeq ,b.PrtNo ,b.PolApplyDate ,b.CValiDate ,b.appntname,( select getUniteCode(b.AgentCode) from dual),a.ManageCom ,"
	+ "a.makedate , b.prem,a.code,"	
	+ tLoadFlag
	+"a.code,b.contno "
	+ "FROM LOPRTManager a,LCCont b WHERE  a.Code in( '11','17','18','19') " 	
	+ "and a.OtherNo = b.proposalContNo "		
	+ "and b.Appflag not in ('4','1') and b.Conttype='1'  "
	+ tStateFlag
	+ " AND a.PrtType = '0'";//2007-10-22
	if(fm.agentGroup.value != null && fm.agentGroup.value != "")//2007-10-22
		strSQL += " and b.AgentGroup in (select agentgroup from LABranchGroup bg where bg.agentGroup = '" + fm.agentGroup.value + "') ";
	//2014-11-4  杨阳
	//把新的业务员编码转成旧的
	if(fm.AgentCode.value != "" && fm.AgentCode.value != null){
		strSQL+=" and a.Agentcode = (select getAgentCode("+fm.AgentCode.value+") from dual) "	;
	}
	strSQL += getWherePart('b.CvaliDate', 'CvaliDate') 
	+ getWherePart('b.PrtNo', 'PrtNo')	
	+ getWherePart('b.AppntName', 'AppntName')	
	+ getWherePart('a.ManageCom', 'ManageCom', 'like')
	+ getWherePart('a.MakeDate','MakeDate')
	+ getWherePart('b.PolApplyDate','PolApplyDate')
	+ getWherePart('a.Code','NoticeType');
	+  " with ur ";
	}

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要打印的催办通知书！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 //tArr=chooseArray(arrDataSet,[0]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
  SqlPDF = strSQL2;
}


function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  	fm.agentGroup.value = arrResult[0][0];
  }
}

//营销机构
function queryBranch()
{
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

//下拉框选择后执行   zhangjianbao   2007-11-16
function afterCodeSelect(cName, Filed)
{	
  if(cName=='statuskind')
  {
    saleChnl = fm.saleChannel.value;
	}
}

//选择营销机构前执行   zhangjianbao   2007-11-16
function beforeCodeSelect()
{
	if(saleChnl == "") alert("请先选择销售渠道");
}
function printPolpdfNew()
{	
	if (PolGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
  var i = 0;
  var count=0;
  var tChked = new Array();
  //var tSel = PolGrid.getSelNo();
  for (i=0;i<PolGrid.mulLineCount;i++)
  {
  	if (PolGrid.getChkNo(i)==true)
  	{
  		tChked[count++]=i;
  	}
  }
  if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}  
	else
	{
		//when '11' then '催办体检通知书' when '17' then '催办首期交费通知书' when '18' then '催办问题件通知书' when '19' then '催办承保计划变更通知书' end 
		tPrtSeq = PolGrid.getRowColData(tChked[0],1);
		tPrtNo = PolGrid.getRowColData(tChked[0],2);
		var tCode1 = PolGrid.getRowColData(tChked[0],10);	
		//alert(tCode1);
		var tCode="";
		fmSave.CodeType.value = tCode1;
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.PrtNo.value = tPrtNo;
		//alert(fmSave.PrtNo.value);
		fmSave.fmtransact.value = "only";
	  var showStr="正在准备打印数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fmSave.target = "fraSubmit";
		fmSave.action="../uw/PDFPrintSave.jsp?Code=11&OtherNo="+PolGrid.getRowColData(tChked[0],13)+"&StandbyFlag1="+tCode1+"&OldPrtSeq="+tPrtSeq;;
		fmSave.submit();
	}
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}
