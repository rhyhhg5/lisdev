<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    

<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>

<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="AntiMoneyLaundryPInput.js"></SCRIPT>   

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

<%@include file="AntiMoneyLaundryPInit.jsp"%>
 
   
</head>
<body onload="initForm();initElementtype();">
<form action="AntiMoneyLaundrySave.jsp" method=post name=fm
    target="fraSubmit">

<table>
    <tr>
        <td class="titleImg">反洗钱报表</td>
    </tr>
</table>
<TABLE class="common">
    <tr class="common">
        <TD  class= title>
        管理机构
         </TD>
         <TD  class= input>
          <Input class="codeNo"  name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary >
         </TD>
        <td class="title">开始日期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="StartDate" verify="开始日期|notnull&date" elementtype=nacessary>
        </td>
        <td class="title">结束日期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="EndDate" verify="结束日期|notnull&date" elementtype=nacessary>
        </td>
    </tr>
    <!--  tr class="common">
        <td class="title">提数机构</td>
        <TD  class= input>
             <Input class=codeno name=BankType verify="提数机构|NOTNULL" CodeData="0|^1|人民银行^2|保监会" ondblClick="showCodeListEx('BankType',[this,BankTypeName],[0,1]);"onkeyup="showCodeListKeyEx('BankType',[this,BankTypeName],[0,1]);"><input class=codename name=BankTypeName readonly=true elementtype="nacessary">
        </TD>
        <td class="title"></td>
        <td class="input"></td>
        <td class="title"></td>
        <TD class= input></TD>
    </tr-->
</TABLE>
<br>
<table>
    <tr>
        <td class="titleImg">一、《金融机构客户身份识别情况统计表》——识别客户</td>
    </tr>
</table>
    <TABLE class="common">
        <tr class="common">
           <td> 
              <input value="对公客户-新客户-总数" type=button onclick="CalNewSumNum()" class="cssButton" type="button">
          <Input class= common name=NewSumNum readonly></td>
          
           <td> 
              <input value="对公客户-其他情形-总数" type=button onclick="CalSumNum()" class="cssButton" type="button">
          <Input class= common name=SumNum readonly></td>
        </tr>
        <tr class="common">
           <td> 
              <input value="对公客户-新客户-通过第三方识别数" type=button onclick="CalNewOtherNum()" class="cssButton" type="button">
          <Input class= common name=NewOtherNum readonly></td>
          
           <td> 
              <input value="对公客户-其他情形-通过第三方识别数" type=button onclick="CalOtherNum()" class="cssButton" type="button">
          <Input class= common name=OtherNum readonly></td>
        </tr>
        <tr class="common">
           <td> 
              <input value="对公客户-新客户-受益人数" type=button onclick="CalNewNum()" class="cssButton" type="button">
          <Input class= common name=NewNum  readonly></td>
          
           <td> 
              <input value="对公客户-其他情形-受益人数" type=button onclick="CalNum()" class="cssButton" type="button">
          <Input class= common name=Num  readonly></td>
        </tr>
         <tr class="common"></tr>
         <tr class="common"></tr>
         <tr class="common"></tr>
         <tr class="common"></tr>
        <tr class="common">
           <td> 
              <input value="对私客户-新客户-总数" type=button onclick="CalNewSumNum2()" class="cssButton" type="button">
          <Input class= common name=NewSumNum2 readonly></td>
          
           <td> 
              <input value="对私客户-其他情形-总数" type=button onclick="CalSumNum2()" class="cssButton" type="button">
          <Input class= common name=SumNum2 readonly></td>
        </tr>
        <tr class="common">
           <td> 
              <input value="对私客户-新客户-通过第三方识别数" type=button onclick="CalNewOtherNum2()" class="cssButton" type="button">
          <Input class= common name=NewOtherNum2 readonly></td>
          
           <td> 
              <input value="对私客户-其他情形-通过第三方识别数" type=button onclick="CalOtherNum2()" class="cssButton" type="button">
          <Input class= common name=OtherNum2 readonly></td>
        </tr>
        <tr class="common">
           <td> 
              <input value="对私客户-新客户-居民-合计" type=button onclick="CalNewResNum()" class="cssButton" type="button">
          <Input class= common name=NewResNum readonly></td>
          
           <td> 
              <input value="对私客户-其他情形-居民-合计" type=button onclick="CalResNum()" class="cssButton" type="button">
          <Input class= common name=ResNum readonly></td>
        </tr>
        <tr class="common">
           <td> 
              <input value="对私客户-新客户-非居民-合计" type=button onclick="CalNewNotResNum()" class="cssButton" type="button">
          <Input class= common name=NewNotResNum readonly></td>
          
           <td> 
              <input value="对私客户-其他情形-非居民-合计" type=button onclick="CalNotResNum()" class="cssButton" type="button">
          <Input class= common name=NotResNum readonly></td>
        </tr>
        
    </TABLE>
    
    <table>
    <tr>
        <td class="titleImg">二、《金融机构客户身份识别情况统计表》——重新识别客户</td>
    </tr>
</table>
    <TABLE class="common">
    <tr class="common">
           <td> 
              <input value="对公客户-变更重要信息-总数" type=button onclick="CalImpSum()" class="cssButton" type="button">
          <Input class= common name=ImpSum readonly></td>
          
           <td> 
              <input value="对公客户-变更重要信息-涉及受益人的" type=button onclick="CalImpNum()" class="cssButton" type="button">
          <Input class= common name=ImpNum  readonly></td>
        </tr>
        <tr class="common">
           <td> 
              <input value="对私客户-变更重要信息-总数" type=button onclick="CalNewImpSum()" class="cssButton" type="button">
          <Input class= common name=NewImpSum readonly></td>
          
           <td> 
              <input value="对私客户-变更重要信息-居民" type=button onclick="CalNewImpResNum()" class="cssButton" type="button">
          <Input class= common name=NewImpResNum readonly></td>
        </tr>
        <tr class="common">
           <td> 
              <input value="对私客户-变更重要信息-非居民" type=button onclick="CalNewImpNotResNum()" class="cssButton" type="button">
          <Input class= common name=NewImpNotResNum  readonly></td>
          
          
        </tr>
    </TABLE>
    
<br>
<input type="hidden" name="PrtFlag" value=""></form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
