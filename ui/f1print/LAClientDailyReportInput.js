//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	var i = 0;
  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
  showInfo.close();
}

// 打印按钮点击事件
function PolPrint()
{
	if(!beforeSubmit())
	{
		return false;
	}

	submitForm();
}

// 进行验证
function beforeSubmit()
{
	var tD,tDate;
	tD = new Date();
	tDate = tD.getYear() +"-"+ (""+(tD.getMonth()+101)).substring(1,3) +"-"+ (""+(tD.getDate()+100)).substring(1,3);
	//alert(tDate);
	
	var tInputValue;
	tInputValue = document.fm.EndDate.value;
	if(tInputValue == "" || tInputValue == null)
	{
		alert("请选择统计日期！");
		return false;
	}
	
	// 验证录入框时候是一个正常的时间
	//alert(tInputValue);
	if(!isDate(tInputValue))
	{
		alert("请输入一个合法的统计日期格式为：'YYYY-MM-DD'");
		return false;
	}
	
	if(tInputValue > tDate)
	{
		alert("统计的日期必须在当天以前！");
		return false;
	}
	
	return true;
}

function getDailyType(cObj,cName)
{
	var tManageCom = document.fm.ManageCom.value;
	
  var strsql =" 1 and othersign > #" + ("" + (tManageCom.length + 100)).substr(1,2) + "#";
  showCodeList('dailytype',[cObj,cName],[0,1],null,strsql,'1',1);
}