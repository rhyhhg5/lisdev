
<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>

<html>
<%
//程序名称：ContactCompareInput.jsp
//程序功能：联系人费用变更
//创建日期：2010-9-12
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="ContactCompareRealInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="ContactCompareRealInit.jsp"%>
	</head>
	<body  onload="initForm();" >
  	<form action="./ContactCompareRealPrint.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
    		
    	<Div  id= "divLLReport1" style= "display: ''">    	
      	<table  class= common>
		  <TR  class= common>
		  <TD class= title>
				机构
			</TD>
			<TD class= input>
				  <Input class="codeNo" name="ManageCom" readOnly " ondblclick="return showCodeList('comcode',[this,ManageComName], [0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName], [0,1]);" ><Input class="codeName" name="ManageComName" elementtype="nacessary" value="" readonly >
			</TD>
		    <TD  class= title>
        	统计起期
      	</TD>    	
      	<TD  class= input>
        	<input class="coolDatePicker" dateFormat="short" name="StartDate" >
      	</TD>
      	<TD  class= title>
        	统计止期
      	</TD>
      	<TD  class= input>
        	<input class="coolDatePicker" dateFormat="short" name="EndDate" >
      	</TD>
		</TR>
		<TR  class= common>
		<TD  class= title>
        	统计类型
      	</TD>
		<TD  class= input>
		   <Input class="codeNo" name="ContType" CodeData="0|^1|个单^2|团单"  ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1]);" onkeyup="return showCodeListEx('ContType',[this,ContTypeName],[0,1]);"><Input class="codeName" name="ContTypeName"  elementtype="nacessary" readonly>
		 </TD>
		</TR>
	</table>
	<br><!--
		<input class=cssButton type=button value=" 查 询 " onclick="query();">
		--><input class=cssButton type=button value="清单下载" onclick="download();">
	<br>
    <!--<table  class= common><table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanBillGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		  <Div  id= "divPage2" align=center style= "display: '' ">     
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();showCodeNameManageCom();showCodeNameAgentGroup();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();showCodeNameManageCom();showCodeNameAgentGroup();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();showCodeNameManageCom();showCodeNameAgentGroup();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();showCodeNameManageCom();showCodeNameAgentGroup();">
		 </Div>
  	</Div>
		--><input type=hidden id="sql" name="sql" value="">
		<input type=hidden id="tContType" name="tContType" value="PRINT">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
