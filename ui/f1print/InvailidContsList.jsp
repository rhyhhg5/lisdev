<%
    GlobalInput GI = new GlobalInput();
    GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>

<html>
<%
//程序名称：InvailidContsList.jsp
//程序功能：万能险保单失效清单
//创建日期：2008-6-24
//创建人  ：张彦梅
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	
  	<SCRIPT src="InvailidContsList.js"></SCRIPT>
  	<%@include file="InvailidContsListInit.jsp"%>
	</head>

	<body  onload="initForm();" >
  	<form action="PrintPauseContNotice.jsp" method=post name=fm target="fraSubmit">
    	<Div  id= "divLLReport1" style= "display: ''">    	
      	<table  class= common>
		  <TR  class= common>
		    <TD  class= title>
        	失效日期范围：开始日期
      	</TD>    	
      	<TD  class= input>
        	<input class="coolDatePicker" dateFormat="short" name="StartDate" >
      	</TD>
      	<TD  class= title>
        	结束日期
      	</TD>
      	<TD  class= input>
        	<input class="coolDatePicker" dateFormat="short" name="EndDate" >
      	</TD>
      	
        <TD>
      	<input class=cssButton type=button value="查  询" onclick="return showSerialNo();">
        </TD> 
		</TR>
		<TR class= common>
			<TD class= title>
				机构
			</TD>
			<TD class= input>
				  <Input class="codeNo" name="ManageCom" readOnly " ondblclick="return showCodeList('comcode',[this,ManageComName], [0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName], [0,1]);" ><Input class="codeName" name="ManageComName" elementtype="nacessary" value="" readonly >
			</TD>
			<TD class= title>
				业务员代码
			</TD>
			<TD class= input>
				<input type=text name=AgentCode class=common verify="业务员代码|len<10">
			</TD>
			<td></td>
		</TR>
		<tr>
			<TD class= title>
				保单号
			</TD>
			<TD class= input id="CodeID" style="display: ''">
			  <input type=text name=ContNo class=common >
			</TD>
			<TD class= title>
				投保人
			</TD>
			<TD class= input id="CodeID" style="display: ''">
			  <input type=text name=ApntNane class=common >
			</TD>
			<td></td>
		</tr>
      </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanBillGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<center>
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();showCodeNameManageCom();showCodeNameAgentGroup();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();showCodeNameManageCom();showCodeNameAgentGroup();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();showCodeNameManageCom();showCodeNameAgentGroup();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();showCodeNameManageCom();showCodeNameAgentGroup();">
	 	</center>
		 </Div>
  	</Div>
  		<input type=hidden name="querySql">
  		<input type=hidden name="queryStartDate">
  		<input type=hidden name="queryEndDate">
  		<input type=hidden id="fmtransact" name="fmtransact">
		<input class=cssButton type=button value="打印清单" onclick="printListing();">
		<input class=cssButton type=button value="选择打印通知书" onclick="printNotice();">	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>