<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序功能：F1报表生成
//创建日期：2007-07-20
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
%>
 <script>
   var msql=" 1 and   char(length(trim(comcode)))<=#8# ";
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LAOrphanQryInput.js"></SCRIPT>  
<%@include file="./LAOrphanQryInit.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%> 
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body  onload="initForm();initElementtype();" >   
  <form action="./LAOrphanQryReport.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			 孤儿单信息查询报表
    		</td>
    	</tr>
      </table>
    <table class= common border=0 width=100%>
      	<TR  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,1,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,1,1);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>        
        <TD class= title>
		  销售单位代码
		</TD>
		<TD class= input>
			<Input class=common name=BranchAttr verify="销售机构" onchange="return checkBranchAttr();" >
		</TD>
         </TR>         
       <TR  class= common>
  	    <TD  class= title>
            代理人员编码
          </TD>          
          <TD  class= input>
            <Input class=common name=AgentCode verify="代理人员编码">
          </TD>  
          <td  class= title>
		   代理人姓名
		</td>
        <td  class= input>
		  <input name=Name class= common verify="代理人姓名" >
		</td> 
   </TR>
   <TR  class= common>
       <TD  class= title>
          离职时间 
        </TD>
        <TD  class= input> 
          <Input  class=common name=OutWorkDate verify="离职时间|Date" >
        </TD>
         <TD  class= title>
          保单号 
        </TD>
        <TD  class= input> 
          <Input  class=common name=ContNo verify="保单号" >
        </TD>
    </TR>
    </table>
    <INPUT VALUE="按保单查询" class="cssButton" TYPE="button" onclick="easyQueryClick();">  
    <INPUT VALUE="按保单查询下载" class="cssButton" TYPE="button" onclick="submitForm();">
    <INPUT VALUE="按险种查询" class="cssButton" TYPE="button" onclick="easyQueryClickR();">  
    <INPUT VALUE="按险种查询下载" class="cssButton" TYPE="button" onclick="submitFormR();">
    <Div id="divLACross1" style="display:'none'">
    <table class=common>
    	<tr class=common>
	  <td text-align:left colSpan=1>
  	   <span id="spanLACrossGrid">
  	   </span> 
	 </td>
	</tr>
    </table>
    </div>
      <Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 				
    </div>
     <Div id="divLACross2" style="display:'none'">
    <table class=common>
    	<tr class=common>
	  <td text-align:left colSpan=1>
  	   <span id="spanLARiskGrid">
  	   </span> 
	 </td>
	</tr>
    </table>
    </div>
     <Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage1.lastPage();"> 				
    </div>
  
    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type=hidden name=AgentGroup value=''>
    <input type=hidden name=Flag value=''>
     <input type=hidden class=Common name=querySql >  
　　
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 