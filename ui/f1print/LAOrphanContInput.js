//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	 
   if (verifyInput() == false)
    return false;
   
   	 
    if( !beforeSubmit())
   {
   	return false;
   	
   	}	

  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function beforeSubmit()
{
 
	return true;
	
}
function checkBranchAttr()
{
	
 if(fm.all("ManageCom").value==null || fm.all("ManageCom").value=='')
 {
   alert("请先录入管理机构");
   fm.all("BranchAttr").value='';
   return false;
 }
 var sql=" select agentgroup  from labranchgroup  where branchattr='"+fm.BranchAttr.value+"'  and  branchtype='1'	"
           +" and branchtype2='01'  and endflag='N'  "
           + getWherePart("ManageCom","ManageCom",'like')	;
     var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
 // alert(sql);
   if (!strQueryResult) 
    {
      alert("此管理机构没有此销售单位！");  
      fm.BranchAttr.value="";
      return;
    }	
    var arr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentGroup').value=arr[0][0];
    
    return true;
}	

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function easyQueryClick() 
{
　if (verifyInput() == false)
    return false;
  //此处书写SQL语句			     
  
//  var tManageCom=getManageComLimit();
//  var strSql = "select c.contno,(select appntname from lccont where contno=c.contno),(select cvalidate from lccont where contno=c.contno), "
//  + "c.agentcode,(select name from laagent where agentcode=c.agentcode),c.managecom,d.branchattr  "
//  + " , (select f.mobile from lcaddress f where f.customerno = e.appntno  and  f.addressno=(select addressno from lcappnt where lcappnt.contno=e.contno))"
//  + " , (select g.postaladdress from lcaddress g where g.customerno = e.appntno and g.addressno=(select addressno from lcappnt where lcappnt.contno=e.contno)) "
//  + " , (select f.phone from lcaddress f where f.customerno = e.appntno  and  f.addressno=(select addressno from lcappnt where lcappnt.contno=e.contno))"
//  + " , (case (select appntsex from lcappnt where contno=c.contno)  when '0' then '男'  else '女' end) "
//  + " ,  e.prem,(case e.stateflag when '0' then '投保' when '1' then '承保有效' when '2' then '失效中止' else '终止' end)"
//  + " from laagent b,laorphanpolicy c,labranchgroup d ,lccont e"
//  + " where c.agentcode=b.agentcode and  d.agentgroup=b.agentgroup  and c.agentgroup=d.agentgroup and d.branchtype='1' and d.branchtype2='01' and"　　　　　　
//  + " b.branchtype='1' and b.branchtype2='01' and b.agentstate>='06' and e.contno=c.contno" 
//  	    + getWherePart('c.ManageCom', 'ManageCom','like')
//  	    + getWherePart('d.BranchAttr', 'BranchAttr')
//  	    + getWherePart('c.AgentCode', 'AgentCode')
//  	    + getWherePart('b.Name', 'Name')
//  	    + getWherePart('b.OutWorkDate', 'OutWorkDate')
//  	    + getWherePart('c.ContNo', 'ContNo')
//  	    + " order by c.managecom,d.BranchAttr" ;
//      alert(strSql);
  var strAgent = " ";
  if(fm.AgentCode.value != null && fm.AgentCode.value != ""){
  	strAgent = " and c.AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
  }
  var strSql = "select c.contno,e.appntname,e.cvalidate, getUniteCode(c.agentcode),"
+"b.name,c.managecom,d.branchattr  , "
+"(select f.mobile from lcaddress f where f.customerno = e.appntno  "
+"and  f.addressno=(select addressno from lcappnt where lcappnt.contno=e.contno)) , "
+"(select g.postaladdress from lcaddress g where g.customerno = e.appntno "
+" and g.addressno=(select addressno from lcappnt where lcappnt.contno=e.contno))  , "
+" (select f.phone from lcaddress f where f.customerno = e.appntno  "
+"and  f.addressno=(select addressno from lcappnt where lcappnt.contno=e.contno)) , "
+" (case (select appntsex from lcappnt where contno=c.contno)  when '0' then '男'  else '女' end), "
+"  e.prem,(case e.stateflag when '0' then '投保' when '1' then '承保有效' when '2' "
+"  then '失效中止' else '终止' end) ,'个单' "
+"  from laagent b,laorphanpolicy c,labranchgroup d ,lccont e  "
+"  where c.agentcode=b.agentcode and  d.agentgroup=b.branchcode   "
+"  and c.agentgroup=d.agentgroup and d.branchtype='1' and d.branchtype2='01'  "
+"  and b.branchtype='1' and b.branchtype2='01' and b.agentstate>='06' and e.contno=c.contno "
+" and c.reasontype='0'"
   + getWherePart('c.ManageCom', 'ManageCom','like')
  	    + getWherePart('d.BranchAttr', 'BranchAttr')
  	    // getWherePart('c.AgentCode', 'AgentCode')
  	    + strAgent
  	    + getWherePart('b.Name', 'Name')
  	    + getWherePart('b.OutWorkDate', 'OutWorkDate')
  	    + getWherePart('c.ContNo', 'ContNo')
+"union "
+"select c.contno,e.GRPNAME,e.cvalidate, getUniteCode(c.agentcode),"
+"b.name,c.managecom,d.branchattr, '-' , "
+"(select g.grpaddress from lcgrpaddress g where g.customerno = e.appntno "
+" and g.addressno=(select addressno from lcgrpappnt where lcgrpappnt.grpcontno=e.grpcontno))  , "
+" (select f.phone1 from lcgrpaddress f where f.customerno = e.appntno  "
+" and  f.addressno=(select addressno from lcgrpappnt where lcgrpappnt.grpcontno=e.grpcontno)) ,  "
+"'-' ,"
+"  e.prem,(case e.stateflag when '0' then '投保' when '1' then '承保有效' when '2' "
+"  then '失效中止' else '终止' end) ,'团单'"
+"  from laagent b,laorphanpolicy c,labranchgroup d ,lcgrpcont e "
+"  where c.agentcode=b.agentcode and  d.agentgroup=b.branchcode  "
+"  and c.agentgroup=d.agentgroup and d.branchtype='1' and d.branchtype2='01' "
+"  and b.branchtype='1' and b.branchtype2='01' and b.agentstate>='06' and e.grpcontno=c.contno "
+" and c.reasontype='1'"
   + getWherePart('c.ManageCom', 'ManageCom','like')
  	    + getWherePart('d.BranchAttr', 'BranchAttr')
  	    //+ getWherePart('c.AgentCode', 'AgentCode')
  	    + strAgent
  	    + getWherePart('b.Name', 'Name')
  	    + getWherePart('b.OutWorkDate', 'OutWorkDate')
  	    + getWherePart('c.ContNo', 'ContNo');
  turnPage.queryModal(strSql, LACrossGrid);
  if (!turnPage.strQueryResult) 
  {
    alert("没有符合条件的查询信息！");
    return false;
  }
}

function afterCodeSelect( cCodeName, Field )
{
　	  
  if(cCodeName=="comcode"){
  	 fm.all("BranchAttr").value="";
  	 fm.all("BranchName").value="";
  	if(fm.all("ManageCom").value=='86' )
  	{
  		fm.all("BranchAttr").disabled=true;
  	}
       else
       	{
       		var tManageCom=fm.all("ManageCom").value;
       		fm.all("BranchAttr").disabled=false;
       		msql1=" 1 and   branchtype=#2#  and branchtype2=#01#  and managecom like #"+tManageCom+"%# and endflag=#N#";
       	}
}
 if(cCodeName=="branchattr")
 {
   if(fm.all("ManageCom").value==null || fm.all("ManageCom").value=='')
    {
      alert("请先录入管理机构");
      fm.all("BranchAttr").value='';
       fm.all("BranchName").value='';
       return false;
  
     }
}
}        
                                                                                                                   