<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LAMediPremInput.jsp
//程序功能：F1报表生成
//创建日期：2007-11-21
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LAMediPremInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();" >    
  <form action="./LAMediPremSave.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name="ManageCom"ManageCom verify = "管理机构|code:ComCode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName > 
          </TD> 
    
    <TD  class= title>
       险种
    </TD>
    <TD  class= input>
       <Input class= 'codeno' name= "RiskCode" verify="险种|code:riskcode" 
       ondblclick="return showCodeList('RiskCode',[this,RiskCodename],[0,1]);"
       onkeyup="return showCodeList('RiskCode',[this,RiskCodename],[0,1]);"
      ><Input class=codename name=RiskCodename readOnly  >
    </TD>	           
        </TR>
       
        <TR  class= common>
          <TD  class= title>
             查询起期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name="StartDate"   >  
          </TD>
          <TD  class= title>
             查询止期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name="EndDate" >  
          </TD>
    
  </TR>
      
    </table>

    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type=hidden name=AgentGroup1 value=''>   
    <input type=hidden name=AgentGroup2 value=''>  
    <input type=hidden name=AgentGroup3 value=''>  
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->   </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 