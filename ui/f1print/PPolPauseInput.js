/*
  //创建人：刘岩松
*/
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();
var orderSql = "";
var sql="";
function checkDate(){
	if(fm.StartDate.value==''||fm.EndDate.value=='')
		{
			alert("您好，起始日与截止日是必须填写的内容！")
			return false;
		}
	else 
		if(!isDate(fm.StartDate.value)||!isDate(fm.EndDate.value))
	 	{
	 		alert("您好，您输入的日期格式不正确。")
	 	}
	 	else
		return true;
}
function displayQueryResult(strResult) {
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置

  strResult = Conversion(strResult);
  var filterArray          = new Array(0,12,1);

  //保存查询结果字符串
  turnPage.strQueryResult  = strResult;

  //使用模拟数据源
  turnPage.useSimulation   = 1;

  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  //alert(turnPage.arrDataCacheSet);


  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BillGrid;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }

  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;

}


function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;

    showInfo.close();
    if (FlagStr == "Fail" )
    {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    }
    else
    {
    //	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		//	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    	showDiv(inputButton,"false");

    	//执行下一步操作
    }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}

//根据起始日期进行查询出要该日期范围内的成功件数
function showSerialNo()
{	 
		if(!checkDate())
		{
		return false;
	  }
	  
	  var codeType = fm.GrpCode.value;
	  var codeStr = null;
	  if (codeType == "42") {	
	    codeStr = " AND a.Code = '42' ";
	  } else if (codeType == "21") {
		  codeStr = " AND a.Code = '21' ";
	  } else if (codeType == "0" || codeType == null || codeType == "") {
		  codeStr = " AND ( a.Code = '42' OR a.Code = '21' OR a.Code = '58' ) ";
	  } else if (codeType == "58") {
	  	codeStr = " AND a.Code = '58' ";
	  } else {
	  	alert("失效类型输入值无效，请双击选择合适的选项");
	  	return false;
	  }
	  
   queryShort();
   var strSQL = "select a.PrtSeq,b.GrpName,a.OtherNo,a.StandbyFlag2,a.StandbyFlag1,a.MakeDate,a.ReqOperator, "
              + " (select Name from LAAgent where AgentCode = b.AgentCode),(select Name from LDCom where ComCode = b.ManageCom),  case a.Code when '42' then '未交费暂停' when '21' then '保单满期' when '58' then '未结算暂停' else '不明状态' end "
   						+ " FROM LOPRTManager a,LCGrpCont b WHERE"
   						+" 1=1 AND b.ManageCom like '"+manageCom+"%%' AND a.otherNo = b.GrpContNo"
  						+ getWherePart( 'a.MakeDate','StartDate','>=' )
  			      + getWherePart( 'a.MakeDate','EndDate','<=')
  			      + getWherePart( 'a.ManageCom','ManageCom')
  			      + getWherePart( 'a.AgentCode','AgentCode')
  			      + codeStr
  			      + " group by a.PrtSeq,b.GrpName,a.OtherNo,a.StandbyFlag2,a.StandbyFlag1,a.MakeDate,a.ReqOperator,b.AgentCode,b.ManageCom, a.Code"
			 				+ orderSql
							;
	sql=strSQL;
  if(!verifyInput()) 
  {
  	return false;
  }
  initBillGrid();		
 	
 	
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (!turnPage.strQueryResult) 
 	{
 		alert("您好，没有符合要求的数据！");
    	return false;
  	}
  	
  	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  	turnPage.pageDisplayGrid = BillGrid;    
  	turnPage.strQuerySql     = strSQL; 
  	turnPage.pageIndex       = 0;  
  	var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  	return true;

  
}

//根据选中的批次号、银行编码、管理机构进行查询并且执行打印功能；
function PrintBill()
{
	queryShort();
	
	if (BillGrid.mulLineCount == 0)
	{
		alert("您好，目前没有需要打印的清单");
		return false;
	}else
			fm.all("sql").value=sql;
			fm.action = "NewPPolPauseListPrint.jsp";
			fm.target = "_blank";
			fm.submit();
}

//排序
function queryShort()
{	
	orderSql ="";
	
    //排序1
    switch(fm.all('Order1').value)
    {
		// 投保人
		case '0':
		orderSql = " Order By b.GrpName";
		break;
		//失效日期
		case '1':				
		orderSql = " Order By a.MakeDate";
		break;
		//业务员
		case '2':
		orderSql = " Order By b.AgentCode";
		break;
		//业务部门
		case '3':
		orderSql = " Order By b.ManageCom";
		break;
    }
    //排序2		   
    switch(fm.all('Order2').value)
    {
		// 投保人
		case '0':
		orderSql = orderSql + ", b.GrpName";
		break;
		//失效日期
		case '1':				
		orderSql = orderSql + ", a.MakeDate";
		break;
		//业务员
		case '2':
		orderSql = orderSql + ",b.AgentCode";
		break;
		//业务部门
		case '3':
		orderSql = orderSql + ",b.ManageCom";
		break;
    }
    //排序3
    switch(fm.all('Order3').value)
    {
		// 投保人
		case '0':
		orderSql = orderSql + ", b.GrpName";
		break;
		//失效日期
		case '1':				
		orderSql = orderSql + ", a.MakeDate";
		break;
		//业务员
		case '2':
		orderSql = orderSql + ",b.AgentCode";
		break;
		//业务部门
		case '3':
		orderSql = orderSql + ",b.ManageCom";
		break;
   }
	   
}