<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>

<% 
    CError cError = new CError( );
    boolean operFlag=true;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	  
  String tManageCom=request.getParameter("ManageCom");
  String tStartDate=request.getParameter("StartDate");
  String tEndDate=request.getParameter("EndDate");
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  VData tVData = new VData();
  VData mResult = new VData();
  CErrors mErrors = new CErrors();

  tVData.addElement(tG);
  tVData.addElement(tManageCom);
  tVData.addElement(tStartDate);
  tVData.addElement(tEndDate);
         
         
  LAAgencyAchievementUI tLAAgencyAchievementUI = new LAAgencyAchievementUI();
  
  System.out.println("Start 后台处理...");   
  try{
   if(!tLAAgencyAchievementUI.submitData(tVData,"PRINT"))
   {
      mErrors.copyAllErrors(tLAAgencyAchievementUI.mErrors);
      cError.moduleName = "LAStatisticReport.jsp";
      cError.functionName = "submitData";
      cError.errorMessage = "tLAStatisticReportUI";
      mErrors.addOneError(cError);
    }
    System.out.println("取数完毕...准备打印报表...");
	  XmlExport txmlExport = new XmlExport();
	  mResult = tLAAgencyAchievementUI.getResult();
	  txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  if (txmlExport==null)
	  {
	    System.out.println("null");
	  }
	  System.out.println("开始打开报表!");
    ExeSQL tExeSQL = new ExeSQL();
    //获取临时文件名
    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
    String strFilePath = tExeSQL.getOneValue(strSql);
    String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
    //获取存放临时文件的路径
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strVFPathName = strRealPath +"//"+ strVFFileName;
    System.out.println("null12");
    //合并VTS文件
   CombineVts tcombineVts = null;
   String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
   tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
   ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
   tcombineVts.output(dataStream);
   //把dataStream存储到磁盘文件
   AccessVtsFile.saveToFile(dataStream,strVFPathName);
   System.out.println("===strVFFileName : "+strVFFileName);
   
   response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=07&RealPath="+strVFPathName);
 }

catch(Exception ex)
{
    Content = "失败，原因是:" + ex.toString();
    FlagStr = "Fail";
    mErrors = tLAAgencyAchievementUI.mErrors;
    if (!mErrors.needDealError())
    {                          
      Content = " 处理失败! ";
      FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + mErrors.getFirstError();
    	FlagStr = "Fail";
    }
%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();	
	 	
</script>
</html>
<%
  }
%>