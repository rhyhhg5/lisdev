<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<% 
//程序名称：
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<script>
	var RePrintFlag = "<%=request.getParameter("RePrintFlag")%>";//记录是否重打
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="FeeInvoiceInput.js"></SCRIPT>    
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="FeeInvoiceInit.jsp"%>
<%
   String PayName = request.getParameter("IncType");
    
%>   
</head>
<body  onload="initForm();" >    
  <form  method=post name=fm target="fraSubmit">
    <table class= common border=0 width=100%>
    	<tr>     		
    		 <td class= titleImg>
        		输入查询条件
       		 </td>   		      
    	</tr>
    </table>   
    <Div  id= "divFeeOth" style= "display: ''"> 
    <table class= common border=0 width=100%>
      <TR  class= common> 
        <TD  class= title>  实收号码类型  </TD>
        <TD  class= input>	<Input class=codeno name=IncomeType elementtype=nacessary verify="实收号码类型|NOTNULL" CodeData="0|^1|集体保单号^2|个人保单号^3|团单批单号^10|个单批单号^15|结算单号" ondblClick="showCodeListEx('FeeIncomeType1',[this,IncomeTypeName],[0,1]);" onkeyup="showCodeListKeyEx('FeeIncomeType1',[this,IncomeTypeName],[0,1]);"><input class=codename name =IncomeTypeName></TD> 
        <TD  class= title>  到账日期 </TD>
        <TD  class= input>	<Input class= "coolDatePicker" dateFormat="short" name=EnterAccDate >  </TD>         
        <TD  class= title>  投保单印刷号</TD>
        <TD  class= input>  <Input class= common name=PrtNo ></TD> 
        </TR>
     </table>
    </Div>
     <Div  id= "divFeeOthJS" style= "display: 'none'"> 
     <table class= common border=0 width=100%>
      <TR  class= common> 
        <TD  class= title>  实收号码类型  </TD>
        <TD  class= input>	<Input class=codeno name=IncomeType1  CodeData="0|^1|集体保单号^2|个人保单号^3|团单批单号^10|个单批单号^15|结算单号" ondblClick="showCodeListEx('FeeIncomeType1',[this,IncomeTypeName],[0,1]);" onkeyup="showCodeListKeyEx('FeeIncomeType1',[this,IncomeTypeName],[0,1]);"><input class=codename name =IncomeTypeName></TD> 
        <TD  class= title>  到账日期 </TD>
        <TD  class= input>	<Input class= "coolDatePicker" dateFormat="short" name=EnterAccDate1 >  </TD>         
        <TD  class= title>  卡单号</TD>
        <TD  class= input>  <Input class= common name=PrtNo1 ></TD> 
        </TR>
     </table>
    </Div>
    <Div  id= "divFeeNo" style= "display: ''"> 
     <table class= common border=0 width=100%>         
      <TR  class= common>         
        <TD  class= title> 实收号码</TD> 
        <TD  class= input> <Input class= common name=IncomeNo > </TD>
        </div>
        <TD  class= title>  管理机构   </TD>
        <TD  class= input>
        <Input class="codeno" name=MngCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,MngComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,MngComName],[0,1],null,null,null,1);"><input class=codename name =MngComName></TD> 
        <TD  class= title> 代理人编码  </TD> 
        <TD  class= input> <Input class="common" name=AgentCode >  </TD>
      </TR>     
    </table>
   </DIV> 
   <Div  id= "divFeeNoJS" style= "display: 'none'"> 
     <table class= common border=0 width=100%>         
      <TR  class= common>         
        <TD  class= title> 结算单号</TD> 
        <TD  class= input> <Input class= common name=IncomeNo1 > </TD>
        </div>
        <TD  class= title>  管理机构   </TD>
        <TD  class= input>
        <Input class="codeno" name=MngCom1 verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,MngComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,MngComName],[0,1],null,null,null,1);"><input class=codename name =MngComName></TD> 
        <TD  class= title> 代理人编码  </TD> 
        <TD  class= input> <Input class="common" name=AgentCode1 >  </TD>
      </TR>     
    </table>
   </DIV>  
   <!--add by yingxl 2008-05-28-->
   <Div  id= "YinBaoTypeDiv" style= "display: ''"> 
     <table class= common border=0 width=100%>         
      <TR  class= common> 
      	<TD  class= title>银保通业务</TD>
        <TD  class= input>	
					<Input class=codeNo name=BankContFlag CodeData="0|^1|是^2|否^3|全部" ondblclick="return showCodeListEx('BankContFlag',[this,BankContFlagName],[0,1]);" onkeyup="return showCodeListKey('BankContFlag',[this,BankContFlagName],[0,1]);" value="2"><input class=codename name=BankContFlagName value="否" readonly=true>
				</TD>      	   
        <TD  class= title></TD> 
        <TD  class= input>  </TD>
        
        <TD  class= title> </TD> 
        <TD  class= input> </TD>
      </TR>     
    </table>
   </DIV> 
   <!--end add by yingxl 2008-05-28-->
    <Div  id= "divFeeButton" style= "display: 'none'">   
		<INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="easyQueryClick();"> 			
		
  	</Div>          
    <Div  id= "divFeeButtonJS" style= "display: 'none'">   
		<INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="easyQueryClick();"> 			
		<INPUT VALUE="全  打" class="cssButton" TYPE=button name="PrintAll" onclick="allPrint();"> 					  
  	</Div> 
  	<input TYPE=hidden  name=PayNo> 
  	<input TYPE=hidden  name=IncNo> 
  	<input TYPE=hidden name= SQLstr>    
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJAPay1);">
    		</td>
    		<td class= titleImg>
    			 费用信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLJAPay1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="turnPage.lastPage();">				
  	</div>
    </div>
        <input type=hidden id="fmtransact" name="fmtransact">
        

  	<Div  id= "divRemark" style= "display: ''">
      	<table  class= common>
    	<tr>     		
    		 <td class= titleImg>
        		额外信息录入
       		 </td>   		      
    	</tr> 
       		<TR  class= common>
          <TD class= title>
            付款人
          </TD>  
          <TD class= input>
            <Input name="AppntName" class= common >
          </TD>  
          <TD class= title>
            险种
          </TD>  
          <TD class= input>
            <Input name="RiskName" class= common >
          </TD> 
          <TD class= title>
          	发票序号
          </TD>
          <TD class= title>
          	<input class="readonly" name="StartNo" style="display:''" ReadOnly >
          </TD>           
  			</TR>              	     		
       		<TR  class= common>
          <TD class= title>
            经手人
          </TD>  
          <TD class= input>
            <Input name="HPerson" class= common >
          </TD>  
          <TD class= title>
            复核人
          </TD>  
          <TD class= input>
            <Input name="CPerson" class= common >
          </TD> 
          <TD class= title>
            附注
          </TD>
          <TD class= input>
            <Input name="Remark" class= common >
          </TD>           
  			</TR>                                 		
    	</table>
		<INPUT VALUE="发票打印" class= cssButton TYPE=button onclick="PPrint();"> 
		
		<INPUT TYPE="hidden" name="CertifyCode"> 
		<INPUT TYPE="hidden" name="Operator" value="<%=tG.Operator%>"> 
		
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 