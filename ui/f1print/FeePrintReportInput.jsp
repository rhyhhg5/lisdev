<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：FeePrintReportInput.jsp
//程序功能：
//创建日期：2008-04-03 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<head >
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="FeePrintReportInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FeePrintReportInit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./FeeBatchPrintSave.jsp" method=post name=fm target="fraSubmit">  
    <table>
    	<tr class=common>
    		<td class=common>
    			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription);">
    		<td class=titleImg>查询条件</td>
    	</tr>
    </table>
 <Div  id= "divLAAscription" style= "display: ''">      
    <table  class= common> 
			<TR  class= common> 
          <td  class= title>
		        保单类型
					</td>
        	<td  class= input>
          	<Input class=codeNo name=IncomeType value='2' CodeData="0|^1|团险^2|个险" verify="保单类型|NOTNULL" ondblclick="return showCodeListEx('IncomeType',[this,IncomeTypeName],[0,1]);" onkeyup="return showCodeListKey('IncomeType',[this,IncomeTypeName],[0,1]);"><input class=codename name=IncomeTypeName value='个险' readonly=true>  
          </td> 
          <td  class= title>
          	管理机构
					</td>
        	<td  class= input>
		  			<Input class="codeno" name=MngCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,MngComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,MngComName],[0,1],null,null,null,1);"><input class=codename name =MngComName>
          </td> 
          <TD  class= title> 
         		合同号
         	</TD> 
        	<TD  class= input> <Input class= common name=IncomeNo > </TD>    
			</TR> 
		   
		  <TR  class= common> 
          <td  class= title>
		        投保单印刷号 
					</td>
        	<td  class= input>
		  			<Input class= common name=PrtNo >
          </td>   
          <td  class= title>
		        组别
					</td>
        	<td  class= input>
		  			 <Input class= common name=BranchChar >
          </td> 
        	<td  class= title>
		        代理人编码
					</td>
        	<td  class= input>
		  			 <Input class= common name=AgentCode >
          </td>  
			</TR> 

			<TR  class= common> 
					<TD  class= title>  起始到账日期</TD>
        	<TD  class= input>	<Input class= "coolDatePicker" dateFormat="short" name=EnterAccDate >  </TD>        
        	<TD  class= title> 截至到账日期</TD> 
        	<TD  class= input> <Input class= "coolDatePicker" dateFormat="short" name=EnterAccDateEnd > </TD>
					<td  class= title>
		        销售渠道
					</td>
        	<td  class= input>
		  			 <Input class=codeNo name=SaleChnl  CodeData="0|^01|个险直销^02|团险直销^03|中介^04|银行代理^06|交叉销售^07|职团开拓^08|其他" ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName readonly=true>
          </td> 
          
          
        	
             
			</TR>
			<TR  class= common> 
					<td  class= title>
		        发票状态 
					</td>
        	<td  class= input>
		  			<Input class=codeNo name=InvoiceState value='2'  CodeData="0|^1|未打印^2|已打印^3|全部" ondblclick="return showCodeListEx('InvoiceState',[this,InvoiceStateName],[0,1]);" onkeyup="return showCodeListKeyEx('InvoiceState',[this,InvoiceStateName],[0,1]);"><input class=codename name=InvoiceStateName value='已打印' readonly=true>
          </td> 
					<td  class= title>
		        
					</td>
        	<td  class= input>
		  			 
          </td> 
          
          <td  class= title>
		        
					</td>
        	<td  class= input>
		  			
          </td>       	
             
			</TR>
	 </table>
	</div>	
	<input type =button class=cssButton value="打 印" onclick="submitForm();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
