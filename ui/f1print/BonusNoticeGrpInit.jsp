 <%
//程序名称：FirstPayInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>                            

<script language="JavaScript">

var BonusGrid;          //定义为全局变量，提供给displayMultiline使用

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {
  	fm.reset();                                   
  }
  catch(ex)
  {
    alert("在BonusNoticeInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
	initBonusGrid();
	fm.all('ManageCom').value = <%=strManageCom%>;
  }
  catch(re)
  {
    alert("BonusNoticeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initBonusGrid()
{                               
  var iArray = new Array();
      
  try {
/*  
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            	//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="流水号";         		//列名
	  iArray[1][1]="140px";            	//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[2]=new Array();
	  iArray[2][0]="保单号码";       		//列名
	  iArray[2][1]="140px";            	//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="险种编码";       		//列名
	  iArray[3][1]="80px";            	//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  

	  iArray[4]=new Array();
	  iArray[4][0]="领取方式";       		//列名
	  iArray[4][1]="60px";            	//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[5]=new Array();
	  iArray[5][0]="管理机构";       		//列名
	  iArray[5][1]="100px";            	//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[6]=new Array();
	  iArray[6][0]="应分配日期";       		//列名
	  iArray[6][1]="100px";            	//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

*/      

	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            	//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="流水号";         		//列名
	  iArray[1][1]="140px";            	//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[2]=new Array();
	  iArray[2][0]="保单号码";       		//列名
	  iArray[2][1]="140px";            	//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="管理机构";       		//列名
	  iArray[3][1]="80px";            	//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[4]=new Array();
	  iArray[4][0]="应分配日期";       		//列名
	  iArray[4][1]="100px";            	//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0; 

      iArray[5]=new Array();
	  iArray[5][0]="分配会计年度";       		//列名
	  iArray[5][1]="100px";            	//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0; 

      iArray[6]=new Array();
	  iArray[6][0]="险种编码";       		//列名
	  iArray[6][1]="100px";            	//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0; 

      
	  BonusGrid = new MulLineEnter( "fmSave" , "BonusGrid" ); 
	  //这些属性必须在loadMulLine前
	  BonusGrid.mulLineCount = 10;   
	  BonusGrid.displayTitle = 1;
	  BonusGrid.canSel = 1;
      BonusGrid.locked = 1;
	  BonusGrid.loadMulLine(iArray);  
	
	} catch(ex) {
		alert(ex);
	}
}

</script>