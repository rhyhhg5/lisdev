<%
//程序名称：LLWorkEfficientStaSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>



<%
  String FlagStr = "";
  String Content = "";
    System.out.println("ssssss");
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  Calendar cal = new GregorianCalendar();
  String year = String.valueOf(cal.get(Calendar.YEAR));
  String month=String.valueOf(cal.get(Calendar.MONTH)+1);
  String date=String.valueOf(cal.get(Calendar.DATE));
  String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String now = year + month + date + hour + min + sec + "_" ;
  
  String  millis = String.valueOf(System.currentTimeMillis());  
  String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
  //String tType = "3";
  System.out.println("OutXmlPath:" + tOutXmlPath);
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
  tTransferData.setNameAndValue("querySql",request.getParameter("querySql"));
  String ManageCom = request.getParameter("ManageCom");
  String sql1="select name from ldcom where comcode='"+ManageCom+"'";
  String sql2="select name from ldcom where comcode='"+tG.ManageCom+"'";
  ExeSQL tExeSQL1 = new ExeSQL();
  String tName = tExeSQL1.getOneValue(sql1);
  String tName2 = tExeSQL1.getOneValue(sql2);
  tTransferData.setNameAndValue("tName",tName);
  tTransferData.setNameAndValue("tName2",tName2);
  tTransferData.setNameAndValue("StartDate",request.getParameter("StartDate"));
  tTransferData.setNameAndValue("EndDate",request.getParameter("EndDate"));
  //tTransferData.setNameAndValue("Type",tType); 
  System.out.println(tTransferData.getValueByName("querySql"));
  try
  {
      VData vData = new VData();
  	  vData.add(tG);
  	  vData.add(tTransferData);
  	  LAMaContBL tLAMaContBL = new LAMaContBL();
      if (!tLAMaContBL.submitData(vData, ""))
      {
          Content = "报表下载失败，原因是:" + tLAMaContBL.mErrors.getFirstError();
          FlagStr = "Fail";
      }
	  else
	  {
	 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
	 	  File file = new File(tOutXmlPath);
	      response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
          byte[] buffer = new byte[4096];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
              file.delete();
          }
	   }
	}
	catch(Exception ex)
	{
	  ex.printStackTrace();
	}
  
  if (!FlagStr.equals("Fail"))
  {
  	Content = "";
  	FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>