<%@page contentType="text/html;charset=GBK" %>
<%
//name : PrintDrawNoticeSave.jsp
//functon : Receive data and transfer data
//Creator  ：刘岩松
//Date ：2003-11-12
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.f1print.*"%>
 <%@page import="java.io.*"%>
 <%
  System.out.println("开始执行PrintDrawNoticeSave.jsp");

  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  //String IP = request.getRemoteAddr();
  String IP = request.getHeader("X-Forwarded-For");
		if(IP == null || IP.length() == 0) { 
		   IP = request.getRemoteAddr(); 
		}
  System.out.println("获取的IP地址是"+IP);
  String Path= application.getRealPath("sys")+File.separator;
  String FileName = Path+"AppConfig.properties";
  ConfigInfo.SetConfigPath(FileName);
  LCPolSet tLCPolSet = new LCPolSet();

  String t_PrintFlag = request.getParameter("PrintFlag");
  PrintDrawNoticeUI tPrintDrawNoticeUI = new PrintDrawNoticeUI();
  VData tVData = new VData();
  VData mResult = new VData();
  if(t_PrintFlag.equals("0"))
  {
    String t_PolNo = request.getParameter("PolNo");

    System.out.println("打印标志是"+t_PrintFlag);
    System.out.println("保单号码是"+t_PolNo);
    try
    {
      tVData.addElement(t_PrintFlag);
      tVData.addElement(t_PolNo);
      tPrintDrawNoticeUI.submitData(tVData,"SINGLE_P");
    }
    catch(Exception ex)
    {
      Content = "打印失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
    mResult = tPrintDrawNoticeUI.getResult();
  XmlExport txmlExport = new XmlExport();
  txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
  if (txmlExport==null)
  {
    System.out.println("null");
    tError = tPrintDrawNoticeUI.mErrors;
    Content = "打印失败,原因是＝＝"+tError.getFirstError();
    FlagStr = "Fail";
  }
  else
  {
    System.out.println("Save中的打印成功!!!!");
    Content = "打印成功!";
    FlagStr = "Succ";
    session.putValue("PrintStream", txmlExport.getInputStream());
    System.out.println("put session value");
    response.sendRedirect("../f1print/GetF1Print.jsp");
  }
  System.out.println("结束！！！");
  }
  if(t_PrintFlag.equals("1"))
  {
    String[] tChk = request.getParameterValues("InpPolGridChk");
    String[] strNumber=request.getParameterValues("PolGridNo");
	  String[] strPolNo=request.getParameterValues("PolGrid2");

    int intLength=0;

    if(strNumber!=null)
    intLength=strNumber.length;
    for(int i=0;i<intLength;i++)
    {
      if(tChk[i].equals("0")) //未选
        continue;
      LCPolSchema tLCPolSchema = new LCPolSchema();
      tLCPolSchema.setPolNo(strPolNo[i]);
      System.out.println("在jsp中所得到的PolNo是"+tLCPolSchema.getPolNo());
      tLCPolSet.add(tLCPolSchema);
    }

    try
    {
      tVData.addElement(t_PrintFlag);
      tVData.addElement(IP);
      tVData.addElement(tLCPolSet);
      tPrintDrawNoticeUI.submitData(tVData,"Banch_P");
      Content = "打印成功";
      FlagStr = "Succ";
    }
    catch(Exception ex)
    {
      Content = "打印失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
  }
      System.out.println("------end------");
       System.out.println(FlagStr);
       System.out.println(Content);
  %>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

