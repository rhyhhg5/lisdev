<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：DataIntoLACommision.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();    
    String BranchType="";
    String BranchType2="";
    String strSql= " 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'"; 
%>
<script>
	 var  mBranchType =  '<%=BranchType%>';
	 //var  mBranchType2 =  '<%=BranchType2%>';
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="BFNHExportDataInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="BFNHExportDataInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./BFNHExportDataReport.jsp" method=post name=fm target="f1print">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		北分农行导数
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom value="8611" readonly verify="管理机构|notnull&code:comcode"><Input class=codename name=ManageComName value="北京分公司" readOnly >
          </TD>          
           <TD class = title>
             银行机构
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentCom readonly  ><Input class=codename name=AgentComName readOnly >
          </TD>
        </TR>
        <TR  class = common>
           <TD  class = title>
           统计起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="起始年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            统计止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="终止年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          
        </TR>          
      </table>       
         <font color="red">备注：统计日期是指保单投保日期</font> 
         <Table class="common" align=center>
  			<Tr>
    			<Td class=button>
      				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="easyQueryClick()">
    				<INPUT VALUE="导出txt文件" class="cssButton" TYPE="button" onclick="doDownLoad()"> 
    			</Td>
  			</Tr>
		</Table>
         <!-- 
         <input type =button class=common value="银保保费明细查询" onclick="easyQueryClick1();">
         <input type =button class=cssButton value="查询" onclick="easyQueryClick();">
          --> 
          <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">
          <input type="hidden" class=input name=BranchType2 value="<%=BranchType2%>">           
    	  <input type=hidden name=querySQL value=""> 
    </Div>
    
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <Div id= "divSpanLACommisionGrid" align=center style= "display: '' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
	<Div id= "divSpanLACommisionGrid1" align=center style= "display: 'none' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid1">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
    	<Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      </div>
      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>