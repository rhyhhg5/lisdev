<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//name       ：PNoticeBill
//function   ：Print Notice Bill
//Create Date：2003-04-16
//Creator    ：刘岩松

%>
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  	<SCRIPT src="PNoticeBillInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="PNoticeBillInputInit.jsp"%>
	</head>

<body  onload="initForm();" >
  	<form action="./PNoticeBillSave.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->

    	<Div  id= "divLLReport1" style= "display: ''">
      	<table  class= common>
          <TR  class= common>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
            <!--TD  class= title></TD>
            <TD  class= input--><Input class="code" name=AgentGroup type=hidden ondblclick="return showCodeList('AgentGroup',[this]);" onkeyup="return showCodeListKey('AgentGroup',[this]);">   <!--/TD-->
   
          <TD  class= title>开始日期	</TD>
          <TD  class= input><input class="coolDatePicker" dateFormat="short" name="StartDate" >	</TD>
          </TR>
          <TR  class= common>  
          <TD  class= title>结束日期</TD>
          <TD  class= input>	<input class="coolDatePicker" dateFormat="short" name="EndDate" >	</TD>
          <!--TD  class= title></TD>
            <TD class="input" nowrap="true" type=hidden -->
               <Input class="common" name="BranchGroup" type=hidden >
			   <input name="btnQueryBranch" class="common" type=hidden type="button" value="?" onclick="queryBranch()" style="width:20">
            <!--/TD-->  
          <TD  class= title> 打印状态 </TD>
          <TD  class= input><Input class=codeno name=StateFlag verify="打印状态|NOTNULL" CodeData="0|^0|提交^1|完成^2|打印单据已回复^3|已发催办通知书" ondblClick="showCodeListEx('RgtObjRegister',[this,StateFlagName],[0,1]);" onkeyup="showCodeListKeyEx('RgtObjRegister',[this,StateFlagName],[0,1]);"><input class=codename name=StateFlagName readonly=true > </TD>
          <TD><Input Type=hidden name=StrSQL></TD>
        </TR>
     	 <!--TR  class= common>
          <TD><input class=common type=button value="打印通知书清单" onclick="PrintNoticeBill()"> </TD>
         </TR-->
 		</table>
         </div>
      <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
</form>
<form action="" method=post name=fmSave target="f1print">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 投保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 	
  	</div>
  	<p>
      <INPUT VALUE="打印通知书清单" class= common TYPE=button onclick="PrintNoticeBill();">
  	</p>
  	
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
