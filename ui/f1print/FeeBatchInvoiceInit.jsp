<%
//程序名称：FeeBatchInvoiceInit.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>  
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {     
    fm.all('PayNo').value = '';
    fm.all('IncomeNo').value = '';
    fm.all('IncomeType').value = '';
    fm.all('EnterAccDate').value = '';
    fm.all('MngCom').value = '<%=tG.ManageCom%>';
    
    fm.all('AgentCode').value = '';
    fm.all('HPerson').value = '';
    fm.all('CPerson').value = '';
    fm.all('Remark').value = '';
    fm.all('PrtNo').value = '';
    
    fm.all('IncomeNo1').value = '';
    fm.all('IncomeType1').value = '';
    fm.all('EnterAccDate1').value = '';
    fm.all('MngCom1').value = '<%=tG.ManageCom%>';
    fm.all('AgentCode1').value = '';
    fm.all('PrtNo1').value = '';
    fm.all('Payer').value = '';
    fm.all('PrintAmount').value = '';
    var strSQL = "select Name from LDCom where ComCode = '" + fm.all('MngCom').value + "'";
    var tMngComName = easyExecSql(strSQL);
    fm.all('MngComName').value = tMngComName;
    fm.all('MngComName1').value = tMngComName;
    
    showAllCodeName();
   
  
    /*if (fm.IncomeType.value=='15'){
    	 divFeePay.style.display="";
    	 divFeeOth.style.display="none";
    }else
    {
       divFeePay.style.display="none";
    	 divFeeOth.style.display="";	
    }*/
    

    var strSQL = "select codename from ldcode where codetype='invoicetype' and  Code ='"+fm.MngCom.value+"' "
    	+ " and (othersign is null or othersign ='0' )"
    ;
    var arrResult = easyExecSql(strSQL);
    
    if (arrResult==null||arrResult=="")
		{
			alert("没有属于该机构的发票类型，请联系系统管理员定义发票类型！"); 
		} 
		else 
		{
			fm.all('CertifyCode').value = arrResult; 
		}
		
    strSQL = "";
		if (fm.MngCom.value.length<=4)
		{
			strSQL = "select min(StartNo) from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='A"+fm.MngCom.value+"' and StateFlag in ('0','7')";
		}
		else
		{
			strSQL = "select min(StartNo) from LZCARD WHERE CertifyCode='"
    	+fm.CertifyCode.value+"' and ReceiveCom='B"+fm.Operator.value+"' and StateFlag in ('0','7')";
		}
	
		arrResult = easyExecSql(strSQL);
		//alert(arrResult[0]);
		if (arrResult==null||arrResult=="")
		{			
			alert("没有可用发票！"); 
		} 
		else 
		{
			fm.StartNo.value=arrResult; 
		}
  }
  catch(ex)
  {
    alert("在FeeInvoiceInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  { 
  }
  catch(ex)
  {
    alert("在FeeInvoiceInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{

  try
  {
    initInpBox();   
    initSelBox();   
    initPolGrid(); 
    initStartGrid();
    StartList();
  }
  catch(re)
  {
    alert("FeeInvoiceInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
     
      if (fm.IncomeType.value=='15'){
      	
      	iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            			//列最大值
	      iArray[0][3]=0;
	      
      	iArray[1]=new Array();
	      iArray[1][0]="投保单号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[1][1]="100px";            		//列宽
	      iArray[1][2]=100;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[2]=new Array();
	      iArray[2][0]="客户姓名";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[2][1]="150px";            		//列宽
	      iArray[2][2]=100;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[3]=new Array();
	      iArray[3][0]="交费收据号码";         		//列名
	      iArray[3][1]="0px";            		//列宽
	      iArray[3][2]=100;            			//列最大值
	      iArray[3][3]=3; 									//是否允许输入,1表示允许，0表示不允许         

	
	      iArray[4]=new Array();
	      iArray[4][0]="结算单号";         		//列名
	      iArray[4][1]="100px";            		//列宽
	      iArray[4][2]=100;            			//列最大值
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[5]=new Array();
	      iArray[5][0]="卡单号";         		//列名
	      iArray[5][1]="100px";            		//列宽
	      iArray[5][2]=100;            			//列最大值
	      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[6]=new Array();
	      iArray[6][0]="实收号码类型";         		//列名
	      iArray[6][1]="80px";            		//列宽
	      iArray[6][2]=200;            			//列最大值
	      iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	      iArray[6][10] = "NoType1";
	      iArray[6][11] = "0|^1|集体保单号^2|个人保单号^3|团单批单号^10|个单批单号^15|结算单号";
	      iArray[6][12] = "3";
	      iArray[6][18]=300;
	      iArray[6][19] = "0";
      	
      	iArray[7]=new Array();
	      iArray[7][0]="总实交金额";         		//列名
	      iArray[7][1]="80px";            		//列宽
	      iArray[7][2]=200;            			//列最大值
	      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[8]=new Array();
	      iArray[8][0]="交费日期";         		//列名
	      iArray[8][1]="70px";            		//列宽
	      iArray[8][2]=100;            			//列最大值
	      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[9]=new Array();
	      iArray[9][0]="确认日期";         		//列名
	      iArray[9][1]="80px";            		//列宽
	      iArray[9][2]=100;            			//列最大值
	      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
		    iArray[10]=new Array();
	      iArray[10][0]="操作员";         		//列名
	      iArray[10][1]="70px";            		//列宽
	      iArray[10][2]=70;            			//列最大值
	      iArray[10][3]=0; 									//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[11]=new Array();
	      iArray[11][0]="管理机构";         		//列名
	      iArray[11][1]="80px";            		//列宽
	      iArray[11][2]=100;            			//列最大值
	      iArray[11][3]=2; 
	      iArray[11][4]="station";              	        //是否引用代码:null||""为不引用
	      iArray[11][5]="8";              	                //引用代码对应第几列，'|'为分割符
	      //iArray[9][9]="出单机构|code:station&NOTNULL";
	      iArray[11][18]=250;
	      iArray[11][19]= 0 ; 
	      
	      iArray[12]=new Array();
	      iArray[12][0]="代理人编码";         		//列名
	      iArray[12][1]="80px";            		//列宽
	      iArray[12][2]=100;            			//列最大值
	      iArray[12][3]=2; 									//是否允许输入,1表示允许，0表示不允许
	      iArray[12][4]="AgentCode";              	        //是否引用代码:null||""为不引用
	      iArray[12][5]="9";              	                //引用代码对应第几列，'|'为分割符
	      //iArray[10][9]="代理人编码|code:AgentCode&NOTNULL";
	      iArray[12][18]=250;
	      iArray[12][19]= 0 ;
	      
	      iArray[13]=new Array();
	      iArray[13][0]="代理人所在的组";         		//列名
	      iArray[13][1]="100px";            		//列宽
	      iArray[13][3]= 0 ;
	      
	      iArray[14]=new Array();
	      iArray[14][0]="渠道";         		//列名
	      iArray[14][1]="70px";            		//列宽
	      iArray[14][3]= 0 ;
	     
      	
      }else{
      	iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            			//列最大值
	      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
				
				iArray[1]=new Array();
	      iArray[1][0]="投保单号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[1][1]="100px";            		//列宽
	      iArray[1][2]=100;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[2]=new Array();
	      iArray[2][0]="客户姓名";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[2][1]="150px";            		//列宽
	      iArray[2][2]=100;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      
	      iArray[3]=new Array();
	      iArray[3][0]="交费收据号码";         		//列名
	      iArray[3][1]="100px";            		//列宽
	      iArray[3][2]=100;            			//列最大值
	      iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[4]=new Array();
	      iArray[4][0]="实收号码";         		//列名
	      iArray[4][1]="130px";            		//列宽
	      iArray[4][2]=100;            			//列最大值
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[5]=new Array();
	      iArray[5][0]="实收号码类型";         		//列名
	      iArray[5][1]="80px";            		//列宽
	      iArray[5][2]=200;            			//列最大值
	      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	      iArray[5][10] = "NoType1";
	      iArray[5][11] = "0|^1|集体保单号^2|个人保单号^3|团单批单号^10|个单批单号^15|结算单号";
	      iArray[5][12] = "3";
	      iArray[5][18]=300;
	      iArray[5][19] = "0";
      	
      	iArray[6]=new Array();
	      iArray[6][0]="总实交金额";         		//列名
	      iArray[6][1]="80px";            		//列宽
	      iArray[6][2]=200;            			//列最大值
	      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	      iArray[7]=new Array();
	      iArray[7][0]="交费日期";         		//列名
	      iArray[7][1]="70px";            		//列宽
	      iArray[7][2]=100;            			//列最大值
	      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[8]=new Array();
	      iArray[8][0]="确认日期";         		//列名
	      iArray[8][1]="100px";            		//列宽
	      iArray[8][2]=100;            			//列最大值
	      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
		    iArray[9]=new Array();
	      iArray[9][0]="操作员";         		//列名
	      iArray[9][1]="70px";            		//列宽
	      iArray[9][2]=100;            			//列最大值
	      iArray[9][3]=0; 									//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[10]=new Array();
	      iArray[10][0]="管理机构";         		//列名
	      iArray[10][1]="80px";            		//列宽
	      iArray[10][2]=100;            			//列最大值
	      iArray[10][3]=2; 
	      iArray[10][4]="station";              	        //是否引用代码:null||""为不引用
	      iArray[10][5]="8";              	                //引用代码对应第几列，'|'为分割符
	      //iArray[8][9]="出单机构|code:station&NOTNULL";
	      iArray[10][18]=250;
	      iArray[10][19]= 0 ; 
	        
	      iArray[11]=new Array();
	      iArray[11][0]="代理人编码";         		//列名
	      iArray[11][1]="100px";            		//列宽
	      iArray[11][2]=100;            			//列最大值
	      iArray[11][3]=0; 									//是否允许输入,1表示允许，0表示不允许
	      iArray[11][4]="AgentCode";              	        //是否引用代码:null||""为不引用
	      iArray[11][5]="9";              	                //引用代码对应第几列，'|'为分割符
	      //iArray[9][9]="代理人编码|code:AgentCode&NOTNULL";
	      iArray[11][18]=250;
	      iArray[11][19]= 0 ;  
	      
	      iArray[12]=new Array();
	      iArray[12][0]="代理人所在的组";         		//列名
	      iArray[12][1]="100px";            		//列宽
	      iArray[12][3]= 0 ;       
				
				iArray[13]=new Array();
	      iArray[13][0]="渠道";         		//列名
	      iArray[13][1]="70px";            		//列宽
	      iArray[13][3]= 0 ;  
      }     
	   

      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;

      PolGrid.canChk = 1;
     
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initStartGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      	iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            			//列最大值
	      iArray[0][3]=0;
	      
      	iArray[1]=new Array();
	      iArray[1][0]="发票类型";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[1][1]="80px";            		//列宽
	      iArray[1][2]=80;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      iArray[1][14]=fm.all('CertifyCode').value;
	      
	      iArray[2]=new Array();
	      iArray[2][0]="起始号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[2][1]="100px";            		//列宽
	      iArray[2][2]=80;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[3]=new Array();
	      iArray[3][0]="截至号";         		//列名
	      iArray[3][1]="100px";            		//列宽
	      iArray[3][2]=80;            			//列最大值
	      iArray[3][3]=0; 									//是否允许输入,1表示允许，0表示不允许         

	
	      iArray[4]=new Array();
	      iArray[4][0]="数量";         		//列名
	      iArray[4][1]="100px";            		//列宽
	      iArray[4][2]=60;            			//列最大值
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	   


      	StartGrid = new MulLineEnter( "fm" , "StartGrid" ); 
      	//这些属性必须在loadMulLine前
      	StartGrid.mulLineCount = 20;   
      	StartGrid.displayTitle = 1;
      	StartGrid.locked = 1;
      	StartGrid.hiddenSubtraction = 1;
      
      	StartGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>