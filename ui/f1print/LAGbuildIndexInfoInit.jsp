<%
//程序名称：LAGbuildIndexInfoInit.jsp
//程序功能：
//创建日期：2014-06-20 10:07
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';	
    fm.all('IndexCalNo').value='';
    fm.all('BranchAttr').value='';
    fm.all('AgentCode').value ='';
    fm.all('BranchType').value='1';
    fm.all('BranchType2').value='01';
  }
  catch(ex)
  {
    alert("在LAGbuildIndexInfoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {

  }
  catch(ex)
  {
    alert("在LAGbuildIndexInfoInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    
    initInpBox();
    initSelBox();    
	initIndexInfoGrid1();
	  
  }
  catch(re)
  {
    alert("LAGbuildIndexInfoInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initIndexInfoGrid1()
{                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名        
		
		iArray[1]=new Array();
        iArray[1][0]="机构编码";         //列名
        iArray[1][1]="90px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
		
		
        iArray[2]=new Array();
        iArray[2][0]="机构名称";         //列名
        iArray[2][1]="90px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="团队编码";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[4]=new Array();
        iArray[4][0]="团队名称";         //列名
        iArray[4][1]="80px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="代理人编码";         //列名
        iArray[5][1]="70px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="代理人姓名";         //列名
        iArray[6][1]="80px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[7]=new Array();
        iArray[7][0]="代理人职级";         //列名
        iArray[7][1]="70px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="团建起始日期";         //列名
        iArray[8][1]="80px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="薪资年月";         //列名
        iArray[9][1]="70px";         //宽度
        iArray[9][2]=120;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
　　     iArray[10]=new Array();
        iArray[10][0]="考核期";         //列名
        iArray[10][1]="80px";         //宽度
        iArray[10][2]=120;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[11]=new Array();
        iArray[11][0]="季度个人新单期缴保费";         //列名
        iArray[11][1]="130px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[12]=new Array();
        iArray[12][0]="季度个人新单传统期缴保费";         //列名
        iArray[12][1]="150px";         //宽度
        iArray[12][2]=100;         //最大长度
        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[13]=new Array();
        iArray[13][0]="季度末架构人力";         //列名
        iArray[13][1]="90px";         //宽度
        iArray[13][2]=100;         //最大长度
        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许        
        
        iArray[14]=new Array();
        iArray[14][0]="季度内标准化营销人员";         //列名
        iArray[14][1]="130px";         //宽度
        iArray[14][2]=100;         //最大长度
        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[15]=new Array();
        iArray[15][0]="团队月均活动率";         //列名
        iArray[15][1]="100px";         //宽度
        iArray[15][2]=100;         //最大长度
        iArray[15][3]=0;         //是否允许录入，0--不能，1--允许        
        
        iArray[16]=new Array();
        iArray[16][0]="营业处季度新单期缴保费";         //列名
        iArray[16][1]="140px";         //宽度
        iArray[16][2]=100;         //最大长度
        iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[17]=new Array();
        iArray[17][0]="营业处季度新单传统期缴保费";         //列名
        iArray[17][1]="150px";         //宽度
        iArray[17][2]=100;         //最大长度
        iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[18]=new Array();
        iArray[18][0]="季度团建考核是否达标";         //列名
        iArray[18][1]="130px";         //宽度
        iArray[18][2]=100;         //最大长度
        iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[19]=new Array();
        iArray[19][0]="个人年度新单期缴保费";         //列名
        iArray[19][1]="130px";         //宽度
        iArray[19][2]=100;         //最大长度
        iArray[19][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[20]=new Array();
        iArray[20][0]="个人年度新单传统期缴保费";         //列名
        iArray[20][1]="130px";         //宽度
        iArray[20][2]=100;         //最大长度
        iArray[20][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[21]=new Array();
        iArray[21][0]="营业处年度新单期缴保费";         //列名
        iArray[21][1]="140px";         //宽度
        iArray[21][2]=100;         //最大长度
        iArray[21][3]=0;         //是否允许录入，0--不能，1--允许  
        
        iArray[22]=new Array();
        iArray[22][0]="营业处年度新单传统期缴保费";         //列名
        iArray[22][1]="140px";         //宽度
        iArray[22][2]=100;         //最大长度
        iArray[22][3]=0;         //是否允许录入，0--不能，1--允许  
        
        iArray[23]=new Array();
        iArray[23][0]="年度末架构人力";   //包含回算人力
        iArray[23][1]="110px";         //宽度
        iArray[23][2]=100;         //最大长度         
        iArray[23][3]=0;         //是否允许录入，0--不能，1--允许       
               
        iArray[24]=new Array();
        iArray[24][0]="年度团建考核是否达标";          
        iArray[24][1]="140px";         //宽度
        iArray[24][2]=100;         //最大长度
        iArray[24][3]=0;         //是否允许录入，0--不能，1--允许
        
     
        
        
        IndexInfoGrid1 = new MulLineEnter( "fm" , "IndexInfoGrid1" ); 

        //这些属性必须在loadMulLine前
        IndexInfoGrid1.mulLineCount = 0;   
        IndexInfoGrid1.displayTitle = 1;
        IndexInfoGrid1.locked=1;
        IndexInfoGrid1.canSel=0;
        IndexInfoGrid1.canChk=0;
        IndexInfoGrid1.loadMulLine(iArray);  
	      //IndexInfoGrid1.selBoxEventFuncName = "selectItem";
      }
      catch(ex)
      {
        alert("初始化IndexInfoGrid1时出错："+ ex);
      }
}


</script>