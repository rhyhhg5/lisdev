<%
//程序名称：DataIntoLACommisionInit.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '8611';
    fm.all('AgentCom').value = 'PY004';
    fm.all('AgentComName').value = '建行';
    //fm.all('BranchType').value = mBranchType;
   	//fm.all('BranchType2').value = mBranchType2;
  }
  catch(ex)
  {
    alert("DataIntoLACommisionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
   // alert(121);
    initLACommisionGrid(); 
    initLACommisionGrid1();
   
  }
  catch(re)
  {
    alert("DataIntoLACommisionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var LACommisionGrid;
var LACommisionGrid1;
function initLACommisionGrid() {  
//alert(12);                            
  var iArray = new Array();
//  alert(12);
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="退保标志";       		//列名
    iArray[1][1]="90px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="保险类别";         		//列名
    iArray[2][1]="70px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    
    iArray[3]=new Array();
    iArray[3][0]="保单号";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许    
    
    iArray[4]=new Array();
    iArray[4][0]="保险公司编号";         		//列名
    iArray[4][1]="70px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array();
    iArray[5][0]="投保日期";         		//列名
    iArray[5][1]="70px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许
           
    iArray[6]=new Array();
    iArray[6][0]="产品编号";         		//列名
    iArray[6][1]="80px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="产品名称";         		//列名
    iArray[7][1]="70px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="机构代码";         		//列名
    iArray[8][1]="60px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="营销员";         		//列名
    iArray[9][1]="60px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="产品种类";         		//列名
    iArray[10][1]="70px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="投保单号";         		//列名
    iArray[11][1]="70px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="投保人姓名";         		//列名
    iArray[12][1]="70px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();
    iArray[13][0]="身份证号码";         		//列名
    iArray[13][1]="70px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();
    iArray[14][0]="保单生效日期";         		//列名
    iArray[14][1]="70px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[15]=new Array();
    iArray[15][0]="趸缴/期缴标志";         		//列名
    iArray[15][1]="70px";         		//宽度
    iArray[15][3]=100;         		//最大长度
    iArray[15][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[16]=new Array();
    iArray[16][0]="首期保费";         		//列名
    iArray[16][1]="70px";         		//宽度
    iArray[16][3]=100;         		//最大长度
    iArray[16][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[17]=new Array();
    iArray[17][0]="保费金额";         		//列名
    iArray[17][1]="70px";         		//宽度
    iArray[17][3]=100;         		//最大长度
    iArray[17][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[18]=new Array();
    iArray[18][0]="保险年期";         		//列名
    iArray[18][1]="70px";         		//宽度
    iArray[18][3]=100;         		//最大长度
    iArray[18][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[19]=new Array();
    iArray[19][0]="缴费年期";         		//列名
    iArray[19][1]="70px";         		//宽度
    iArray[19][3]=100;         		//最大长度
    iArray[19][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[20]=new Array();
    iArray[20][0]="手续费率";         		//列名
    iArray[20][1]="70px";         		//宽度
    iArray[20][3]=100;         		//最大长度
    iArray[20][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[21]=new Array();
    iArray[21][0]="手续费";         		//列名
    iArray[21][1]="70px";         		//宽度
    iArray[21][3]=100;         		//最大长度
    iArray[21][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[22]=new Array();
    iArray[22][0]="统计日期";         		//列名
    iArray[22][1]="70px";         		//宽度
    iArray[22][3]=100;         		//最大长度
    iArray[22][4]=0;         		//是否允许录入，0--不能，1--允许
    
   LACommisionGrid = new MulLineEnter( "fm" , "LACommisionGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACommisionGrid.mulLineCount = 0;   
    LACommisionGrid.displayTitle = 1;
    LACommisionGrid.hiddenPlus = 1;
    LACommisionGrid.hiddenSubtraction = 1;
    LACommisionGrid.canSel = 0;
    LACommisionGrid.canChk = 0;
  //  LACommisionGrid.selBoxEventFuncName = "showOne";
  
    LACommisionGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACommisionGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
function initLACommisionGrid1() {  
//alert(12);                            
  var iArray = new Array();
//  alert(12);
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="退保标志";       		//列名
    iArray[1][1]="90px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="保险类别";         		//列名
    iArray[2][1]="70px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许  
    
    
    iArray[3]=new Array();
    iArray[3][0]="保单号";         		//列名
    iArray[3][1]="80px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许    
    
    iArray[4]=new Array();
    iArray[4][0]="保险公司编号";         		//列名
    iArray[4][1]="70px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array();
    iArray[5][0]="投保日期";         		//列名
    iArray[5][1]="70px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许
           
    iArray[6]=new Array();
    iArray[6][0]="产品编号";         		//列名
    iArray[6][1]="80px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="产品名称";         		//列名
    iArray[7][1]="70px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="机构代码";         		//列名
    iArray[8][1]="60px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="营销员";         		//列名
    iArray[9][1]="60px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="产品种类";         		//列名
    iArray[10][1]="70px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="投保单号";         		//列名
    iArray[11][1]="70px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="投保人姓名";         		//列名
    iArray[12][1]="70px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();
    iArray[13][0]="身份证号码";         		//列名
    iArray[13][1]="70px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[14]=new Array();
    iArray[14][0]="保单生效日期";         		//列名
    iArray[14][1]="70px";         		//宽度
    iArray[14][3]=100;         		//最大长度
    iArray[14][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[15]=new Array();
    iArray[15][0]="趸缴/期缴标志";         		//列名
    iArray[15][1]="70px";         		//宽度
    iArray[15][3]=100;         		//最大长度
    iArray[15][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[16]=new Array();
    iArray[16][0]="首期保费";         		//列名
    iArray[16][1]="70px";         		//宽度
    iArray[16][3]=100;         		//最大长度
    iArray[16][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[17]=new Array();
    iArray[17][0]="保费金额";         		//列名
    iArray[17][1]="70px";         		//宽度
    iArray[17][3]=100;         		//最大长度
    iArray[17][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[18]=new Array();
    iArray[18][0]="保险年期";         		//列名
    iArray[18][1]="70px";         		//宽度
    iArray[18][3]=100;         		//最大长度
    iArray[18][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[19]=new Array();
    iArray[19][0]="缴费年期";         		//列名
    iArray[19][1]="70px";         		//宽度
    iArray[19][3]=100;         		//最大长度
    iArray[19][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[20]=new Array();
    iArray[20][0]="手续费率";         		//列名
    iArray[20][1]="70px";         		//宽度
    iArray[20][3]=100;         		//最大长度
    iArray[20][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[21]=new Array();
    iArray[21][0]="手续费";         		//列名
    iArray[21][1]="70px";         		//宽度
    iArray[21][3]=100;         		//最大长度
    iArray[21][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[22]=new Array();
    iArray[22][0]="统计日期";         		//列名
    iArray[22][1]="70px";         		//宽度
    iArray[22][3]=100;         		//最大长度
    iArray[22][4]=0;         		//是否允许录入，0--不能，1--允许
    
    
   LACommisionGrid1 = new MulLineEnter( "fm" , "LACommisionGrid1" ); 
    //这些属性必须在loadMulLine前
 
    LACommisionGrid1.mulLineCount = 0;   
    LACommisionGrid1.displayTitle = 1;
    LACommisionGrid1.hiddenPlus = 1;
    LACommisionGrid1.hiddenSubtraction = 1;
    LACommisionGrid1.canSel = 0;
    LACommisionGrid1.canChk = 0;
  //  LACommisionGrid.selBoxEventFuncName = "showOne";
  
    LACommisionGrid1.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACommisionGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
