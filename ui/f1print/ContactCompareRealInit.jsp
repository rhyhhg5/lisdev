<%
//程序名称：ContactCompareInit.jsp
//程序功能：
//创建日期：2006-10-03
//创建人  ：韦力
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
  String mCurDate = PubFun.getCurrentDate();
  String sql = "select date('" + mCurDate + "') - 10 days from dual ";
  String mStartDate = new ExeSQL().getOneValue(sql);
  if(mStartDate.equals("") || mStartDate.equals("null"))
  {
    mStartDate = "";
  }
  
  String mLoadFlag = request.getParameter("LoadFlag");
%>                            

<script language="JavaScript"><!--

var mLoadFlag = "<%= mLoadFlag%>";

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
    fm.StartDate.value = "<%=mStartDate%>";
    fm.EndDate.value = "<%=mCurDate%>";
    fm.ManageCom.value = ComCode;
    showAllCodeName();
  }
  catch(ex)
  {
    alert("ContactCompareInit.jspInitInpBox函数中发生异常:初始化界面错误!" + ex.message);
  }      
}                                      

function initForm()
{
  try
  {
    initInpBox();
//	initBillGrid();
  }
  catch(re)
  {
    alert("ContactCompareInit.jspInitForm函数中发生异常:初始化界面错误!");
  }
}

// 清单信息列表的初始化
function initBillGrid()
{
  var iArray = new Array();
    
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="销售渠道";         		//列名
    iArray[1][1]="70px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="网点";         		//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="业务员姓名";         		//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="业务员代码";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  	iArray[5]=new Array();
    iArray[5][0]="保单号";         		//列名
    iArray[5][1]="50px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  	iArray[6]=new Array();
    iArray[6][0]="投保单号";         		//列名
    iArray[6][1]="70px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="归档号";         		//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  	iArray[8]=new Array();
    iArray[8][0]="投保人";         		//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  	iArray[9]=new Array();
    iArray[9][0]="保费";         		//列名
    iArray[9][1]="70px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="联系电话";         		//列名
    iArray[10][1]="70px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="问题";         		//列名
    iArray[11][1]="300px";            		//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[11][21]="PayDate";
    
    BillGrid = new MulLineEnter( "fm" , "BillGrid" ); 
    //这些属性必须在loadMulLine前
    BillGrid.mulLineCount = 0;   
    BillGrid.displayTitle = 1;
    BillGrid.canChk=1;
    BillGrid.locked = 1;
    BillGrid.loadMulLine(iArray);  
    BillGrid.detailInfo="单击显示详细信息";
    //BillGrid.detailClick=RegisterDetailClick;
  }
  catch(ex)
  {
    alert(ex.messate);
  }
}

--></script>