<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：XQBankSuccPrint.jsp
//程序功能：实现打印的功能
//创建人  ：刘岩松
//创建日期：2004-4-29
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.f1print.*"%>
<%
  System.out.println("开始执行打印操作");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  //初始化全局变量，从前台承接数据
   String strStartDate=request.getParameter("StartDate");                //开始日期
   String strEndDate=request.getParameter("EndDate");                  //结束日期
	 String strStation = request.getParameter("Station");
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
	LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
  ShowPolPauseUI tShowPolPauseUI = new ShowPolPauseUI();

  VData tVData = new VData();
  VData mResult = new VData();
  try
  {
    tVData.clear();
    tVData.addElement(strStartDate);
    tVData.addElement(strEndDate);
    //tVData.addElement(strAgentState);
    //tVData.addElement(strPremType);
    //tVData.addElement(strFlag);
    tVData.addElement(strStation);
    //tVData.addElement(tG);
    tShowPolPauseUI.submitData(tVData,"SHOW");
  }
  catch(Exception ex)
  {
    Content = "PRINT"+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  mResult = tShowPolPauseUI.getResult();
 	mLJSPayPersonSet.set((LJSPayPersonSet)mResult.getObjectByObjectName("LJSPayPersonSet",0));
	int n = mLJSPayPersonSet.size();
	System.out.println("get report "+n);
	if(n == 0)
	{
		Content = " 没有要查询的数据" ;
    FlagStr = "Fail";
	}
	else{
	
	String Strtest = "0|" + n + "^" + mLJSPayPersonSet.encode();
	System.out.println("QueryResult: " + Strtest);
	%>
	<script language="javascript">
	try 
	{
	  parent.fraInterface.displayQueryResult('<%=Strtest%>');
	}
	catch(ex) {}		   	
 </script>
		<% 
	}
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tShowPolPauseUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>