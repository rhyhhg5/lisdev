
<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>

<html>
<%
//程序名称：PPolPauseindividualInput
//程序功能：保单失效清单
//创建日期：2006-3-02
//创建人  ：韦力
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="PPolPauseindividualInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="PPolPauseindividualInit.jsp"%>
	</head>

	<body  onload="initForm();" >
  	<form action="./NewPPolPauseListPrint.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
    		
    	<Div  id= "divLLReport1" style= "display: ''">    	
      	<table  class= common>
		  <TR  class= common>
		    <TD  class= title>
        	失效日期范围：开始日期
      	</TD>    	
      	<TD  class= input>
        	<input class="coolDatePicker" dateFormat="short" name="StartDate" >
      	</TD>
      	<TD  class= title>
        	结束日期
      	</TD>
      	<TD  class= input>
        	<input class="coolDatePicker" dateFormat="short" name="EndDate" >
      	</TD>
      	
        <TD>
      	<input class=cssButton type=button value="查  询" onclick="showSerialNo()">
        </TD> 
		</TR>
		<TR class= common>
			<TD class= title>
				机构
			</TD>
			<TD class= input>
				  <Input class="codeNo" name="ManageCom" readOnly " ondblclick="return showCodeList('comcode',[this,ManageComName], [0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName], [0,1]);" ><Input class="codeName" name="ManageComName" elementtype="nacessary" value="" readonly >
			</TD>
			<TD class= title>
				业务员代码
			</TD>
			<TD class= input>
				<input type=text name=AgentCode class=common verify="业务员代码|len<10">
			</TD>
			<TD class= title>
				失效类型
			</TD>
			<TD class= input id="CodeID" style="display: ''">
			  <Input class="codeNo" name="Code" value="0" CodeData="0|^42|未交费失效^21|保单满期^0|全部" readOnly verify="Ё效类型|notnull&code:CodeType" ondblclick="return showCodeListEx('CodeTypeP',[this,CodeTypeName], [0,1]);" onkeyup="return showCodeListKey('CodeTypeP',[this,CodeTypeName], [0,1]);" ><Input class="codeName" name="CodeTypeName" elementtype="nacessary" value="全部" readonly >
			</TD>
			<TD class= input id="GrpCodeID"  style="display: ''">
			  <Input class="codeNo" name="GrpCode" value="0" CodeData="0|^42|未交费暂停^58|未结算暂停^21|保单满期^0|全部" readOnly verify="Ё效类型|notnull&code:GrpCodeType" ondblclick="return showCodeListEx('CodeTypeG',[this,GrpCodeTypeName], [0,1]);" onkeyup="return showCodeListKey('CodeTypeG',[this,GrpCodeTypeName], [0,1]);" ><Input class="codeName" name="GrpCodeTypeName" elementtype="nacessary" value="全部" readonly >
			</TD>
		</TR>
      <table  class= common>
	    <table  class= common>
		  <TR class= common>
		     <TD class= title> 排序1 </td>	 
			  <TD  class= input>	
			    <Input class="codeNo" name="Order1" CodeData="0|^0|投保人^1|失效日期^2|业务员^3|业务部门" readOnly " ondblclick="return showCodeListEx('Order',[this,Order1Name], [0,1]);" onkeyup="return showCodeListKey('Order',[this,Order1Name], [0,1]);" ><Input class="codeName" name="Order1Name" elementtype="nacessary" value="" readonly >
			  </TD>						
			  <TD class= title>排序2</td>
			  <TD  class= input>
				  <Input class="codeNo" name="Order2" CodeData="0|^0|投保人^1|失效日期^2|业务员^3|业务部门" readOnly " ondblclick="return showCodeListEx('Order',[this,Order2Name], [0,1]);" onkeyup="return showCodeListKey('Order',[this,Order2Name], [0,1]);" ><Input class="codeName" name="Order2Name" elementtype="nacessary" value="" readonly >
			  </TD> 
			   <TD class= title>排序3</td>
			  <TD  class= input>
				  <Input class="codeNo" name="Order3" CodeData="0|^0|投保人^1|失效日期^2|业务员^3|业务部门" readOnly " ondblclick="return showCodeListEx('Order',[this,Order3Name], [0,1]);" onkeyup="return showCodeListKey('Order',[this,Order3Name], [0,1]);" ><Input class="codeName" name="Order3Name" elementtype="nacessary" value="" readonly >
			  </TD>  
		  </tr>       	
 	</table>

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanBillGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		  <Div  id= "divPage2" align=center style= "display: '' ">     
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();showCodeNameManageCom();showCodeNameAgentGroup();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();showCodeNameManageCom();showCodeNameAgentGroup();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();showCodeNameManageCom();showCodeNameAgentGroup();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();showCodeNameManageCom();showCodeNameAgentGroup();">
		 </Div>
  	</Div>
		<input class=cssButton type=button value="打印清单" onclick="printBill();">
		<input class=cssButton type=button value="选择打印通知书" onclick="printNotice();">
		<!-- 
		<input class=cssButton type=button value="全部打印通知书" onclick="printNoticeAll();">
		 -->
		<input type=hidden id="sql" name="sql" value="">
        <Input type=hidden name=LoadFlag> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
