<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BankDataQueryListPrint.jsp
//程序功能：
//创建日期：2009-08-31
//创建人  ：xp  改造成EXCEL下载方式
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "报盘数据明细_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String tSQL = request.getParameter("strsql");
	System.out.println("打印查询:"+tSQL);
	
    //设置表头
    String[][] tTitle = {{"序号", "管理机构", "营销部门", "保单号", "应缴日期", "帐户名", "银行名称", "抽盘日期", "发盘日期", "发盘金额", "状态","回盘时间","回盘处理时间", "回盘结果", "转账成功金额", "不成功原因", "营销机构", "业务员代码", "业务员姓名", "允许发盘开始时间", "允许发盘截止时间", "发盘批次号", "银行账号","销售渠道" }};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(tSQL,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>
<%-- 
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  XmlExport txmlExport = null;   
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  String tstrsql = request.getParameter("strsql");
  System.out.println("在天愿作比翼鸟，在地愿作同圈猪！");

  TransferData tTransferData= new TransferData();
  tTransferData.setNameAndValue("strsql",tstrsql);

  VData tVData = new VData();
  tVData.addElement(tG);
  tVData.addElement(tTransferData);
          
  IndiDueFeePolListPrintUI tIndiDueFeePolListPrintUI = new IndiDueFeePolListPrintUI(); 
  if(!tIndiDueFeePolListPrintUI.submitData(tVData,"PRINT"))
  {
     operFlag = false;
     Content = tIndiDueFeePolListPrintUI.mErrors.getErrContent();                
  }
  else
  {    
	  VData mResult = tIndiDueFeePolListPrintUI.getResult();			
	  txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

	  if(txmlExport==null)
	  {
	   	operFlag=false;
	   	Content="没有得到要显示的数据文件";	  
	  }
	}
	
	System.out.println(operFlag);
	if (operFlag==true)
	{
		ExeSQL tExeSQL = new ExeSQL();
    //获取临时文件名
    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
    String strFilePath = tExeSQL.getOneValue(strSql);
    String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
    //获取存放临时文件的路径
    //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
    //String strRealPath = tExeSQL.getOneValue(strSql);
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strVFPathName = strRealPath + "//" +strVFFileName;
    
    CombineVts tcombineVts = null;	
    
    String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
  	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
  
  	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
  	tcombineVts.output(dataStream);
      	
  	//把dataStream存储到磁盘文件
  	//System.out.println("存储文件到"+strVFPathName);
  	AccessVtsFile.saveToFile(dataStream,strVFPathName);
    System.out.println("==> Write VTS file to disk ");
          
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
	}
	else
	{
    	FlagStr = "Fail";
--%>   	

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>