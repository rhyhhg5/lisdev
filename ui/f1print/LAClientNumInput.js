//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//xqq  2014-11-27
function checkAgentCode(){
var sql=" select agentcode  from laagent   where  1=1 "
            + getWherePart("GroupAgentCode","GroupAgentCode")
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
}


//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var tAgentState=fm.all('AgentState').value;
    var tAgentKind=fm.all('AgentKind').value;
    var stSQL = "";
	strSQL = "select aa,bb,cc,dd,ee,sum(ff),hh,ii from ("
	+ " select a1 aa,b1 bb,c1 cc,d1 dd,e1 ee,count(distinct f1) ff,h1 hh,i1 ii from (select b.name a1,b.comcode b1,c.name c1,getUniteCode(c.agentcode) d1,e.gradename e1,a.appntno f1, "
	+"case when c.agentstate<'03' then '在职' when c.agentstate>'05' then '离职确认' else '离职登记' end h1,"
	+"case when d.indueformflag='Y' then '已转正' else '未转正' end i1 "
	+ " from lacommision a,ldcom b,laagent c,latree d,laagentgrade e,labranchgroup f "
	+ " where a.managecom=b.comcode and a.agentcode=c.agentcode"
	+ " and a.agentcode=d.agentcode and d.agentgrade=e.gradecode and c.agentgroup=f.agentgroup and a.branchtype='2' and a.branchtype2='01' "
	+" and a.managecom like '"+fm.all('ManageCom').value+"%' "
	+" and a.tmakedate>='"+fm.all('StartDate').value+"' "
	+" and a.tmakedate<='"+fm.all('EndDate').value+"' "
	+"and not exists (select * from lagrpcommisiondetail where grpcontno =a.grpcontno) "
	+getWherePart('a.agentcode','AgentCode')
	+getWherePart('a.branchattr','BranchAttr');
	if(tAgentState!=null && tAgentState!=''){
		if(tAgentState=='01'){
			strSQL += " and c.agentstate<'03' ";
		}
		if(tAgentState=='02'){
			strSQL += " and c.agentstate>'02' ";
		}
	}
	if(tAgentKind!=null && tAgentKind!=''){
		if(tAgentKind=='01'){
			strSQL += " and f.state<>'1' ";
		}
		if(tAgentKind=='02'){
			strSQL += " and f.state='1' ";
		}
	}
	strSQL += ") as bbb group by a1,b1,c1,d1,e1,h1,i1"
	+ " union all select b.name aa,b.comcode bb,c.name cc,getUniteCode(c.agentcode) dd,e.gradename ee,a.k1 ff, "
	+"case when c.agentstate<'03' then '在职' when c.agentstate>'05' then '离职确认' else '离职登记' end hh,"
	+"case when d.indueformflag='Y' then '已转正' else '未转正' end ii "
	+ " from lacommision a,ldcom b,laagent c,latree d,laagentgrade e,labranchgroup f "
	+ " where a.managecom=b.comcode and a.agentcode=c.agentcode"
	+ " and a.agentcode=d.agentcode and d.agentgrade=e.gradecode and c.agentgroup=f.agentgroup and a.branchtype='2' and a.branchtype2='01' "
	+" and a.managecom like '"+fm.all('ManageCom').value+"%' "
	+" and a.tmakedate>='"+fm.all('StartDate').value+"' "
	+" and a.tmakedate<='"+fm.all('EndDate').value+"' "
	+"and exists (select * from  lagrpcommisiondetail where grpcontno =a.grpcontno) "
	+getWherePart('a.agentcode','AgentCode')
	+getWherePart('a.branchattr','BranchAttr');
	if(tAgentState!=null && tAgentState!=''){
		if(tAgentState=='01'){
			strSQL += " and c.agentstate<'03' ";
		}
		if(tAgentState=='02'){
			strSQL += " and c.agentstate>'02' ";
		}
	}
	if(tAgentKind!=null && tAgentKind!=''){
		if(tAgentKind=='01'){
			strSQL += " and f.state<>'1' ";
		}
		if(tAgentKind=='02'){
			strSQL += " and f.state='1' ";
		}
	}
	strSQL += ") as aaa group by aa,bb,cc,dd,ee,hh,ii order by aa,bb,dd ";
	
    fm.querySql.value = strSQL;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}