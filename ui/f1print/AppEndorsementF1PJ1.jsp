<%@page contentType="text/html; charset=GBK" %>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.f1j.ss.BookModelImpl"%>
<%@page import="com.f1j.util.F1Exception"%>

<%
    LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    tLDSysVarDB.setSysVar("VTSFilePath");
    tLDSysVarDB.getInfo();
    String vtsPath = tLDSysVarDB.getSysVarValue();
    
    String showToolBar = request.getParameter("showToolBar");
    
    if(showToolBar == null || showToolBar.equals("")) {
        showToolBar = "true";
    }
    
    if (vtsPath == null)
    {
      vtsPath = "vtsfile/";
    }
    String strTemplatePath = "";
    
    GlobalInput tGI = (GlobalInput)session.getValue("GI");
    
    String filePath = application.getRealPath("/").replace('\\', '/') + "/" +  vtsPath;
    String fileName = "BQ" + FileQueue.getFileName()+".vts";
    String realPath = filePath + fileName;
    System.out.println("\n\nREAL:" + realPath);
     
    int len = 0; //批单数
		String edorAcceptNo = request.getParameter("EdorAcceptNo");		
    try
    {
      //保全批单 + 现金价值表
      //新增险种现金价值表
      LPPolDB tLPPolDB = new LPPolDB();
      //tLPPolDB.setEdorNo(edorAcceptNo);
      //tLPPolDB.setEdorType(BQ.EDORTYPE_NS);
      //LPPolSet tLPPolSet = tLPPolDB.query();
      
      //新增险种现金价值表
      String sql22 = "select a.* from LPPol a, LMCalMode b "
                  + "where a.riskCode = b.riskCode "
                  + "   and b.type = 'X' "  //现价表
                  + "   and edorNo = '" + edorAcceptNo + "' "
                  + "   and edorType = '" + BQ.EDORTYPE_NS + "' ";
      LPPolSet tLPPolSet = tLPPolDB.executeQuery(sql22);
      
      String[] strVFPathName = new String[tLPPolSet.size() + 1];  //存储批单文件名
      System.out.println("\n\n\n\n\n\n\n\n" + strVFPathName.length);
      
      InputStream ins = null;
      String sql = "select * from LPEDORAPPPRINT where EdorAcceptNo = '" + edorAcceptNo + "'"; 
      Connection conn = DBConnPool.getConnection();
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);
      
      if (rs.next())
      {
        Blob tBlob = rs.getBlob("EdorInfo");
        ins = tBlob.getBinaryStream();
      
      }
      rs.close();
      stmt.close();
      conn.close();    
      
                
            
      //合并VTS文件 
      String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
      ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
      CombineVts tcombineVts = new CombineVts(ins, templatePath);
      tcombineVts.output(dataStream);
      
      //把dataStream存储到磁盘文件
      strVFPathName[len] = "PrtApp"+String.valueOf(len) + ".vts";
      AccessVtsFile.saveToFile(dataStream,strVFPathName[len]);
      //AccessVtsFile.saveToFile(dataStream, realPath);
      //response.sendRedirect("GetF1PrintJ1_new.jsp?RealPath=" + realPath);
      len++;
      
      
      
      //新增险种现金价值表
      for(int i = 1; i <= tLPPolSet.size(); i++)
      {
        VData data = new VData();
        data.add(tGI);
        data.add(tLPPolSet.get(i));
      
        PrtCashValueTableBL bl = new PrtCashValueTableBL(i);
        bl.setRowCount(43);   //每栏显示的行数
        XmlExport tXmlExport = bl.getXmlExport(data, "");
        System.out.println(tXmlExport == null);
        if(tXmlExport != null)
        {
          strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
          CombineVts tcombineVts2 = new CombineVts(tXmlExport.getInputStream(),strTemplatePath);
          ByteArrayOutputStream dataStream2 = new ByteArrayOutputStream();
          tcombineVts2.output(dataStream2);
          
           //把dataStream存储到磁盘文件
          strVFPathName[len] = "PrtApp"+String.valueOf(len) + ".vts";
          AccessVtsFile.saveToFile(dataStream2,strVFPathName[len]);
          len++;
        }
      }
      
        VtsFileCombine vtsfilecombine = new VtsFileCombine();
        BookModelImpl tb = vtsfilecombine.dataCombine(strVFPathName);
        try
        {  
           System.out.println("=================" + realPath);
           vtsfilecombine.write(tb, realPath);
           System.out.println("xxxxxxxxxxxx");
        }
        catch (IOException ex)
        {
        }
        catch (F1Exception ex)
        {
        }
      session.putValue("RealPath", realPath);
      response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath=" + realPath+"&showToolBar="+showToolBar);
    }
    catch (Exception ex)
    {
        ex.printStackTrace();
    }
%>