//
//程序名称：BankDataQuery.js
//程序功能：财务报盘数据查询
//创建日期：2004-10-20
//创建人  ：wentao
//更新记录：  更新人    更新日期     更新原因/内容

//var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mSql="";

//简单查询
function easyQuery()
{
	var sDate = fm.all('SendDate').value;
	var sEndDate = fm.all('SendEndDate').value;
	var sBankSucc = fm.all('BankSuccFlag').value;
	var strSqlCon = "";
	
	if (sDate == "")
	{
			alert("请输入发盘起期！");
			return;
	}
	if (sEndDate == "")
	{
			alert("请输入发盘止期！");
			return;
	}
	
	//成功sql
	var sqlSucc = "select (select name from ldcom where comcode = a.comcode) 管理机构, "
		        + "(select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12) fetch first 1 row only) "
		        + "from lccont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno "
		        + "union "
		        + "select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12) fetch first 1 row only) "
		        + "from lbcont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno fetch first 1 row only) 营销部门, "
		        + "polno 保单号, " 
		        + "(select appntname from lccont where contno = a.polno union select appntname from lbcont where contno = a.polno fetch first 1 row only) 投保人, "
		        + "(select case when lcad.phone is not null then lcad.phone when lcad.homephone is not null then lcad.homephone "
		        + "else mobile end from lcappnt lcap, lcaddress lcad where lcap.appntno = lcad.customerno and lcap.addressno = lcad.addressno "
		        + "and lcap.contno = a.polno union select case when lcad.phone is not null then lcad.phone when lcad.homephone is not null then lcad.homephone "
		        + "else mobile end from lbappnt lbap, lcaddress lcad where lbap.appntno = lcad.customerno "
		        + "and lbap.addressno = lcad.addressno and lbap.contno = a.polno fetch first 1 row only) 联系电话, "
		        + "(select cvalidate from lccont where contno = a.polno union select cvalidate from lbcont where contno = a.polno fetch first 1 row only) 保单生效日, "
//		        + " GETRISKBYGETNOTICENO(a.paycode) 应交险种, "
		        + "PayMoney 应收保费,AccName 帐户名, (select bankname from ldbank where bankcode = a.BankCode) 银行名称, "
		        + "lyl.SendDate 发盘日期,bankdealdate 回盘日期, PayMoney 金额 ,'成功' 是否成功, (select codename from ldcode1 where codetype='bankerror' and code=a.bankcode and code1=a.banksuccflag ) 扣款失败原因, "
		        + "(select lastpaytodate from ljspaypersonb where getnoticeno=a.paycode fetch first 1 row only) 应收日, "
				+ "(select paydate from ljspayb where getnoticeno=a.paycode fetch first 1 row only) 缴费截至日期,"
				+ "(select z.name from lccont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno "
		        + "union select z.name from lbcont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno fetch first 1 row only) 营销机构, "
				+ " (select getUniteCode(agentcode) from lccont where contno=a.polno union select getUniteCode(agentcode) from lbcont where contno=a.polno fetch first 1 row only) 业务员代码, "
				+ "(select y.name from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.polno "
				+ "union select y.name from lbcont x,laagent y where x.agentcode=y.agentcode "
				+ "and  x.contno = a.polno fetch first 1 row only) 业务员姓名, "
				+ "(select case when y.mobile is null then y.phone else y.mobile end "
				+ "from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.polno "
				+ "union select case when y.mobile is null then y.phone else y.mobile end "
				+ "from lbcont x,laagent y where x.agentcode=y.agentcode "
				+ "and x.contno = a.polno fetch first 1 row only) 业务员电话, "
				+ "a.paycode "
				+ "from Lybanklog lyl,LYReturnFromBank a " 
				+ "Inner Join Laagent Laa On Laa.Agentcode = a.Agentcode "
				+ "where 1=1 and a.notype='2' and a.Dealtype='S' and a.serialno = lyl.serialno "
				+ "and a.comcode like '" + fm.ManageCom.value + "%' " 
				+ "and a.bankcode like '" +fm.BankCode.value+"%'" 
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('SerialNo','SerialNo')	
				+ " and a.BankSuccFlag = ("
				+ " select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1)"
				+ " from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode "
				+ " union "
				+ " select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) "
				+ " from ldbank where  cansendflag = '1' and bankcode in (select bankunitecode from ldbankunite where bankcode=lyl.bankcode)"
				+ " fetch first 1 row only)"
				+ " And Not Exists(Select 1 From Lysendtobank Where Paycode = a.Paycode And Serialno = a.Serialno) "
				+ " and lyl.SendDate >='" + sDate + "'"
				+ " and lyl.SendDate <='" + sEndDate + "'" 
				 ;
				 
	var sqlSuccB = "select (select name from ldcom where comcode = a.comcode) 管理机构, "
		        + "(select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12) fetch first 1 row only) "
		        + "from lccont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno "
		        + "union "
		        + "select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12) fetch first 1 row only) "
		        + "from lbcont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno fetch first 1 row only) 营销部门, "
		        + "polno 保单号, " 
		        + "(select appntname from lccont where contno = a.polno union select appntname from lbcont where contno = a.polno fetch first 1 row only) 投保人, "
		        + "(select case when lcad.phone is not null then lcad.phone when lcad.homephone is not null then lcad.homephone "
		        + "else mobile end from lcappnt lcap, lcaddress lcad where lcap.appntno = lcad.customerno and lcap.addressno = lcad.addressno "
		        + "and lcap.contno = a.polno union select case when lcad.phone is not null then lcad.phone when lcad.homephone is not null then lcad.homephone "
		        + "else mobile end from lbappnt lbap, lcaddress lcad where lbap.appntno = lcad.customerno "
		        + "and lbap.addressno = lcad.addressno and lbap.contno = a.polno fetch first 1 row only) 联系电话, "
		        + "(select cvalidate from lccont where contno = a.polno union select cvalidate from lbcont where contno = a.polno fetch first 1 row only) 保单生效日, "
//		        + " GETRISKBYGETNOTICENO(a.paycode) 应交险种, "
		        + "PayMoney 应收保费,AccName 帐户名, (select bankname from ldbank where bankcode = a.BankCode) 银行名称, "
		        + "lyl.SendDate 发盘日期,bankdealdate 回盘日期, PayMoney 金额 ,'成功' 是否成功, (select codename from ldcode1 where codetype='bankerror' and code=a.bankcode and code1=a.banksuccflag ) 扣款失败原因, "
		        + "(select lastpaytodate from ljspaypersonb where getnoticeno=a.paycode fetch first 1 row only) 应收日, "
				+ "(select paydate from ljspayb where getnoticeno=a.paycode fetch first 1 row only) 缴费截至日期,"
				+ "(select z.name from lccont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno "
		        + "union select z.name from lbcont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno fetch first 1 row only) 营销机构, "
				+ " (select getUniteCode(agentcode) from lccont where contno=a.polno union select getUniteCode(agentcode) from lbcont where contno=a.polno fetch first 1 row only) 业务员代码, "
				+ "(select y.name from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.polno "
				+ "union select y.name from lbcont x,laagent y where x.agentcode=y.agentcode "
				+ "and  x.contno = a.polno fetch first 1 row only) 业务员姓名, "
				+ "(select case when y.mobile is null then y.phone else y.mobile end "
				+ "from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.polno "
				+ "union select case when y.mobile is null then y.phone else y.mobile end "
				+ "from lbcont x,laagent y where x.agentcode=y.agentcode "
				+ "and x.contno = a.polno fetch first 1 row only) 业务员电话, "
				+ "a.paycode "
				+ "from Lybanklog lyl,LYReturnFromBankb a " 
				+ "Inner Join Laagent Laa On Laa.Agentcode = a.Agentcode "
				+ "where 1=1 and a.notype='2' and a.Dealtype='S' and a.serialno = lyl.serialno "
				+ "and a.comcode like '" + fm.ManageCom.value + "%' " 
				+ "and a.bankcode like '" +fm.BankCode.value+"%'" 
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('SerialNo','SerialNo')	
				+ " and a.BankSuccFlag = ("
				+ " select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1)"
				+ " from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode "
				+ " union "
				+ " select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) "
				+ " from ldbank where  cansendflag = '1' and bankcode in (select bankunitecode from ldbankunite where bankcode=lyl.bankcode)"
				+ " fetch first 1 row only)"
				+ " And Not Exists(Select 1 From Lysendtobank Where Paycode = a.Paycode And Serialno = a.Serialno) "
				+ " and lyl.SendDate >='" + sDate + "'"
				+ " and lyl.SendDate <='" + sEndDate + "'" 
				 ;
				
	//未成功sql
	var sqlFailB = "select (select name from ldcom where comcode = a.comcode) 管理机构, "
		        + "(select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12)) "
		        + "from lccont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno "
		        + "union "
		        + "select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12)) "
		        + "from lbcont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno fetch first 1 row only) 营销部门, "
		        + "polno 保单号, " 
		        + "(select appntname from lccont where contno = a.polno union select appntname from lbcont where contno = a.polno fetch first 1 row only) 投保人, "
		        + "(select case when lcad.phone is not null then lcad.phone when lcad.homephone is not null then lcad.homephone "
		        + "else mobile end from lcappnt lcap, lcaddress lcad where lcap.appntno = lcad.customerno and lcap.addressno = lcad.addressno "
		        + "and lcap.contno = a.polno union select case when lcad.phone is not null then lcad.phone when lcad.homephone is not null then lcad.homephone "
		        + "else mobile end from lbappnt lbap, lcaddress lcad where lbap.appntno = lcad.customerno "
		        + "and lbap.addressno = lcad.addressno and lbap.contno = a.polno fetch first 1 row only) 联系电话, "
		        + "(select cvalidate from lccont where contno = a.polno union select cvalidate from lbcont where contno = a.polno fetch first 1 row only) 保单生效日, "
//		        + " GETRISKBYGETNOTICENO(a.paycode) 应交险种, "
		        + "PayMoney 应收保费,AccName 帐户名, (select bankname from ldbank where bankcode = a.BankCode) 银行名称, "
		        + "lyl.SendDate 发盘日期,bankdealdate 回盘日期, PayMoney 金额 ,'未成功' 是否成功, (select codename from ldcode1 where codetype='bankerror' and code=a.bankcode and code1=a.banksuccflag ) 扣款失败原因, "
		        + "(select lastpaytodate from ljspaypersonb where getnoticeno=a.paycode fetch first 1 row only) 应收日, "
				+ "(select paydate from ljspayb where getnoticeno=a.paycode fetch first 1 row only) 缴费截至日期,"
				+ "(select z.name from lccont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno "
		        + "union select z.name from lbcont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno) 营销机构, "
				+ " (select getUniteCode(agentcode) from lccont where contno=a.polno union select getUniteCode(agentcode) from lbcont where contno=a.polno fetch first 1 row only) 业务员代码, "
				+ "(select y.name from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.polno "
				+ "union select y.name from lbcont x,laagent y where x.agentcode=y.agentcode "
				+ "and  x.contno = a.polno fetch first 1 row only) 业务员姓名, "
				+ "(select case when y.mobile is null then y.phone else y.mobile end "
				+ "from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.polno "
				+ "union select case when y.mobile is null then y.phone else y.mobile end "
				+ "from lbcont x,laagent y where x.agentcode=y.agentcode "
				+ "and x.contno = a.polno fetch first 1 row only) 业务员电话, "
				+ "a.paycode "
				+ "from Lybanklog lyl,LYReturnFromBankb a " 
				+ "Inner Join Laagent Laa On Laa.Agentcode = a.Agentcode "
				+ "where 1=1 and a.notype='2' and a.Dealtype='S' and a.serialno = lyl.serialno "
				+ "and a.comcode like '" + fm.ManageCom.value + "%' " 
				+ "and a.bankcode like '" +fm.BankCode.value+"%'" 
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('SerialNo','SerialNo')	
				+ " and a.BankSuccFlag = ("
				+ " select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1)"
				+ " from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode "
				+ " union "
				+ " select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) "
				+ " from ldbank where  cansendflag = '1' and bankcode in (select bankunitecode from ldbankunite where bankcode=lyl.bankcode)"
				+ " fetch first 1 row only)"
				+ " And Not Exists(Select 1 From Lysendtobank Where Paycode = a.Paycode And Serialno = a.Serialno) "
				+ " and lyl.SendDate >='" + sDate + "'"
				+ " and lyl.SendDate <='" + sEndDate + "'" 
				 ;
 var sqlFail = "select (select name from ldcom where comcode = a.comcode) 管理机构, "
		        + "(select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12)) "
		        + "from lccont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno "
		        + "union "
		        + "select (select name from labranchgroup where agentgroup=substr(z.branchseries,1,12)) "
		        + "from lbcont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno fetch first 1 row only) 营销部门, "
		        + "polno 保单号, " 
		        + "(select appntname from lccont where contno = a.polno union select appntname from lbcont where contno = a.polno fetch first 1 row only) 投保人, "
		        + "(select case when lcad.phone is not null then lcad.phone when lcad.homephone is not null then lcad.homephone "
		        + "else mobile end from lcappnt lcap, lcaddress lcad where lcap.appntno = lcad.customerno and lcap.addressno = lcad.addressno "
		        + "and lcap.contno = a.polno union select case when lcad.phone is not null then lcad.phone when lcad.homephone is not null then lcad.homephone "
		        + "else mobile end from lbappnt lbap, lcaddress lcad where lbap.appntno = lcad.customerno "
		        + "and lbap.addressno = lcad.addressno and lbap.contno = a.polno fetch first 1 row only) 联系电话, "
		        + "(select cvalidate from lccont where contno = a.polno union select cvalidate from lbcont where contno = a.polno fetch first 1 row only) 保单生效日, "
//		        + " GETRISKBYGETNOTICENO(a.paycode) 应交险种, "
		        + "PayMoney 应收保费,AccName 帐户名, (select bankname from ldbank where bankcode = a.BankCode) 银行名称, "
		        + "lyl.SendDate 发盘日期,bankdealdate 回盘日期, PayMoney 金额 ,'未成功' 是否成功, (select codename from ldcode1 where codetype='bankerror' and code=a.bankcode and code1=a.banksuccflag ) 扣款失败原因, "
		        + "(select lastpaytodate from ljspaypersonb where getnoticeno=a.paycode fetch first 1 row only) 应收日, "
				+ "(select paydate from ljspayb where getnoticeno=a.paycode fetch first 1 row only) 缴费截至日期,"
				+ "(select z.name from lccont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno "
		        + "union select z.name from lbcont x,laagent y,labranchgroup z where "
		        + "x.agentcode=y.agentcode and y.agentgroup=z.agentgroup and x.contno = a.polno) 营销机构, "
				+ " (select getUniteCode(agentcode) from lccont where contno=a.polno union select getUniteCode(agentcode) from lbcont where contno=a.polno fetch first 1 row only) 业务员代码, "
				+ "(select y.name from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.polno "
				+ "union select y.name from lbcont x,laagent y where x.agentcode=y.agentcode "
				+ "and  x.contno = a.polno fetch first 1 row only) 业务员姓名, "
				+ "(select case when y.mobile is null then y.phone else y.mobile end "
				+ "from lccont x,laagent y where x.agentcode=y.agentcode and x.contno = a.polno "
				+ "union select case when y.mobile is null then y.phone else y.mobile end "
				+ "from lbcont x,laagent y where x.agentcode=y.agentcode "
				+ "and x.contno = a.polno fetch first 1 row only) 业务员电话, "
				+ "a.paycode "
				+ "from Lybanklog lyl,LYReturnFromBank a " 
				+ "Inner Join Laagent Laa On Laa.Agentcode = a.Agentcode "
				+ "where 1=1 and a.notype='2' and a.Dealtype='S' and a.serialno = lyl.serialno "
				+ "and a.comcode like '" + fm.ManageCom.value + "%' " 
				+ "and a.bankcode like '" +fm.BankCode.value+"%'" 
				+ getWherePart('polno','PolNo','=','0')
				+ getWherePart('accname','AccName','=','0')
				+ getWherePart('SerialNo','SerialNo')	
				+ " and a.BankSuccFlag = ("
				+ " select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1)"
				+ " from ldbank where  cansendflag = '1' and bankcode = lyl.bankcode "
				+ " union "
				+ " select substr(agentpaysuccflag, 1, length(agentpaysuccflag)-1) "
				+ " from ldbank where  cansendflag = '1' and bankcode in (select bankunitecode from ldbankunite where bankcode=lyl.bankcode)"
				+ " fetch first 1 row only)"
				+ " And Not Exists(Select 1 From Lysendtobank Where Paycode = a.Paycode And Serialno = a.Serialno) "
				+ " and lyl.SendDate >='" + sDate + "'"
				+ " and lyl.SendDate <='" + sEndDate + "'" 
				 ;
	// 书写SQL语句
	if (sBankSucc=='1')
	{
		var strSQL = " select int(ROW_NUMBER() OVER()) 序号列, lbr.* from (" + sqlSucc + " order by polno) lbr " +
				" union " +
				" select int(ROW_NUMBER() OVER()) 序号列, lbr.* from (" + sqlSucc + " order by polno) lbr " +
				"with ur";
	}
	else if (sBankSucc=='2')
	{
	    var strSQL = " select int(ROW_NUMBER() OVER()) 序号列, lbr.* from (" + sqlFailB + " order by polno) lbr " +
	    		" union  " +
	    		" select int(ROW_NUMBER() OVER()) 序号列, lbr.* from (" + sqlFail + " order by polno) lbr " +
	    		"with ur";
	}
	else
	{
	    var strSQL = " select int(ROW_NUMBER() OVER()) 序号列, lbr.* from (" + sqlSucc + " union " + sqlFailB + " order by 保单号) lbr " +
	    		" union " +
	    		" select int(ROW_NUMBER() OVER()) 序号列, lbr.* from (" + sqlSucc + " union " + sqlFail + " order by 保单号) lbr " +
	    		" with ur";
	}
	mSql=strSQL;
	turnPage1.queryModal(strSQL, CodeGrid);  
	showCodeName(); 
}

function easyPrint()
{	
//	easyQueryPrint(2,'CodeGrid','turnPage');
	if( CodeGrid.mulLineCount == 0)
	{
		alert("没有回盘信息");
		return false;
	}
	fm.all('strsql').value=mSql;
	//回盘信息查询打印
	fm.submit();
}
