
<jsp:directive.page
	import="com.sinosoft.lis.certify.SysOperatorNoticeBL" />
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：保监会 赔款支出报表
	//程序功能：
	//创建日期：2013-12-6
	//创建人  ：y
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	System.out.println("start TClaimExcelSave...TClaim");
 
	String mDay[] = new String[2];
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	String tOutXmlPath="";  //文件路径
	String strOperation = "";	//报表打印标记-PRINT
	String strOpt = "";		//TClaim-->赔款支出数据表
	String CSVFileName = "";
	String sManageCom = "";  
	String sRiskCode = ""; 
	strOperation = request.getParameter("fmtransact");
	strOpt = request.getParameter("Opt");
	mDay[0] = request.getParameter("StartDate");   //打印首期
	mDay[1] = request.getParameter("EndDate");     //打印止期
	sManageCom = request.getParameter("ManageCom");		//打印公司代码
	sRiskCode = request.getParameter("RiskCode");		//打印险种代码
	
	boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	CError cError = new CError( );
	CErrors tError = null;

	System.out.println("打印类型是：" + strOpt+";打印日期：" + mDay[0]+"至"+mDay[1]);  //TClaim-->赔款支出数据表

	VData tVData = new VData();	
	tVData.addElement(mDay);
	tVData.addElement(tG);
	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
	String tDate=dateFormat.format(new Date());
	String tTime=timeFormat.format(new Date());
	String fileName=tDate.replace("-","")+tTime.replace(":","")+"-CL-"+tG.Operator;
	System.out.println("文件名称："+fileName);
//	tOutXmlPath = application.getRealPath("vtsfile").replace("\\","/") + "/" + fileName;
	tOutXmlPath = application.getRealPath("vtsfile").replace("\\","/") + "/";
	System.out.println("文件路径:" + tOutXmlPath);
	tVData.addElement(tOutXmlPath);
	tVData.addElement(fileName);
	tVData.addElement(sManageCom);
	tVData.addElement(sRiskCode);
	
	RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);

	TClaimExcelBL tTClaimExcelBL = new TClaimExcelBL();
    if(!tTClaimExcelBL.submitData(tVData,strOperation)){
       	operFlag = false;
    }
    if (FlagStr == "") {
    	tError = tTClaimExcelBL.mErrors;
    		if (!tError.needDealError()) {
    			Content = " 保存成功! ";
    			FlagStr = "Success";
    		    System.out.println("--------成功----------");   
	            CSVFileName = fileName + ".csv";

	  			if ("".equals(CSVFileName)) {
					operFlag = false;
					Content = "没有得到要显示的数据文件";  
					return;
 				 	}
    			
    			
    		} else {
    			Content = " 保存失败，原因是:" + tError.getFirstError();
    			FlagStr = "Fail";
    		
    			%>
    			<%=Content%>
    			<%
    			return;
    		}
    }
    
    Readhtml rh = new Readhtml();

	String realpath = application.getRealPath("/").substring(0,application.getRealPath("/").length()).replace("\\","/");//UI地址
	String temp = realpath.substring(realpath.length() - 1, realpath.length());
	if (!temp.equals("/")) {
		realpath = realpath + "/";
	}
	System.out.println("realpath:"+realpath);
	rpt.updateReportMetaData(StrTool.replace(CSVFileName, ".csv", ".zip"));//rpt.updateReportMetaData(StrTool.replace(CSVFileName, ".csv", ".zip"));
	String outpathname=realpath+"vtsfile/"+CSVFileName;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	System.out.println("filename:"+StrTool.replace(CSVFileName, ".csv", ".zip"));
	try {
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	String[] InputEntry = new String[1];
	InputEntry[0] = outpathname;
	System.out.println("outpathname:"+InputEntry[0]);
	String tZipFile = StrTool.replace(outpathname, ".csv", ".zip");
	System.out.println("tZipFile == " + tZipFile);
	rh.CreateZipFile(InputEntry, tZipFile);

%>

<html>
<%@page contentType="text/html;charset=GBK"%>
<a
	href="../f1print/CJDownLoad.jsp?filename=<%=StrTool.replace(CSVFileName, ".csv", ".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>
