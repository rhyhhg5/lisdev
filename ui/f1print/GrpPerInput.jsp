<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = "";
	try
	{
		tContNo = request.getParameter( "ContNo" );
		
		//默认情况下为集体保单
		if( tContNo == null || tContNo.equals( "" ))
			tContNo = "00000000000000000000";
	}
	
	catch( Exception e1 )
	{
		tContNo = "00000000000000000000";
			System.out.println("---contno:"+tContNo);

	}
	System.out.println("---contno:"+tContNo);
%>
<script>
	var contNo = "<%=tContNo%>";  //个人单的查询条件.
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="GrpPerInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpPerInit.jsp"%>
  <title>集体保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 保单号码  </TD>
          <TD  class= input>  <Input class= common name=GrpPolNo >  </TD>
          <TD  class= title>  印刷号码  </TD>
          <TD  class= input>  <Input class= common name=PrtNo >  </TD>
           <TD  class= title> 险种编码 </TD>
          <TD  class= input> <Input class="codeno" name=RiskCode ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('RiskCode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true >  </TD>

        </TR>
        <TR  class= common>
           <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   

        <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCodet" ondblclick="return showCodeList('AgentCodet2',[this,AgentCodeName,AgentGroup],[0,1,3],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet2',[this,AgentCodeName,AgentGroup],[0,1,3]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 

          <TD  class= title>  </TD>
          <TD  class= input> <Input class="code" type=hidden name=AgentGroup ondblclick="return showCodeList('AgentGroup',[this]);" onkeyup="return showCodeListKey('AgentGroup',[this]);"> </TD>
        </TR>
        
    </table>
          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
         
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common  TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 	
      </div>
       	<br/>
       <INPUT VALUE="集体下个人保险凭证打印" class= common TYPE=button onclick="returnParent();"> 									
  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
