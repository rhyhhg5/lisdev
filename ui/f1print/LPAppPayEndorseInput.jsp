<%@page contentType="text/html;charset=GBK" %>
<html>
<%
    GlobalInput tpGI = new GlobalInput();
	tpGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tpGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tpGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="LPAppPayEndorse.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="LPAppPayEndorseInit.jsp"%>
  <%@include file = "../bq/ManageComLimit.jsp"%>

  <title>保全交费通知书查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./LCPolQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLCPol1" style= "display: ''">
      <table  class= common>
       <TR class=common>
          <TD  class= title> 保全受理号 </TD>
          <TD  class= input> <Input class= common name=EdorAcceptNo > </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">  </TD>   
       </TR> 
       <TR  class= common>
          <TD  class= title> 代理人编码 </TD>
          <TD  class= input>  <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this]);" onkeyup="return showCodeListKey('AgentCode',[this]);">   </TD> 
          <TD  class= title> 代理人组别 </TD>
          <TD  class= input>  <Input class="code" name=AgentGroup ondblclick="return showCodeList('AgentGroup',[this]);" onkeyup="return showCodeListKey('AgentGroup',[this]);">   </TD>
          <TD  class= title> 展业机构 </TD>
          <td class="input" nowrap="true">
            <Input class="common" name="BranchGroup">
			<input name="btnQueryBranch" class="common" type="button" value="?" onclick="queryBranch()" style="width:20">
          </TD>  
        </TR>  
      </table>
    </Div>
          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
          <!--INPUT VALUE="返回" TYPE=button onclick="returnParent();"--> 			
  </form>		
  <form action="./EdorAppFeeF1PSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol2);">
    		</td>
    		<td class= titleImg>
    			 保全交费信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanLCPolGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
       <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 						
  	</div>
  	<br/>
  	<INPUT VALUE="打印保全交费通知书" class= common TYPE=button onclick="PrintEdor();"> 
  	<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="GetNoticeNo" name="GetNoticeNo">
		<input type=hidden id="OtherNo" name="OtherNo">
		<input type=hidden id="AppntNo" name="AppntNo">
		<input type=hidden id="SumDuePayMoney" name="SumDuePayMoney">
		<input type=hidden id="Agentcode1" name="Agentcode1">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
