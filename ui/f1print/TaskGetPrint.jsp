<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskGetPrint.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  LDSysVarDB tLDSysVarDB = new LDSysVarDB();
  tLDSysVarDB.setSysVar("VTSFilePath");
  tLDSysVarDB.getInfo();
  String vtsPath = tLDSysVarDB.getSysVarValue();
  if (vtsPath == null)
  {
    vtsPath = "vtsfile/";
  }
  
  String filePath = application.getRealPath("/").replace('\\', '/') + "/" + vtsPath;
  String fileName = "TASK" + FileQueue.getFileName()+".vts";
  String realPath = filePath + fileName;
  
    CError cError = new CError( );
    boolean operFlag=true;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	  
  String workNo=request.getParameter("WorkNo");
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  LGWorkSchema tLGWorkSchema = new LGWorkSchema();
  
  tLGWorkSchema.setWorkNo(workNo);
  tLGWorkSchema.setAcceptNo(workNo);
  
  System.out.println("tLGWorkSchema.getWorkNo():" + tLGWorkSchema.getWorkNo());
  
  VData tVData = new VData();
  VData mResult = new VData();
  CErrors mErrors = new CErrors();
  
  tVData.addElement(tLGWorkSchema);
  tVData.addElement(tG);
         
  TaskPrintUI tTaskPrintUI = new TaskPrintUI();
  XmlExport txmlExport = new XmlExport();    
  if(!tTaskPrintUI.submitData(tVData,"PRINT"))
  {
    operFlag = false;
    Content = tTaskPrintUI.mErrors.getFirstError().toString();                 
  }
  else
  {    
    mResult = tTaskPrintUI.getResult();			
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if(txmlExport==null)
    {
    	operFlag=false;
    	Content="没有得到要显示的数据文件";	  
    }
  }

	if (operFlag==true)
	{
    //合并VTS文件 
    String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);
    
    //把dataStream存储到磁盘文件
    AccessVtsFile.saveToFile(dataStream, realPath);
    response.sendRedirect("GetF1PrintJ1_new.jsp?RealPath=" + realPath);
	}
	else
	{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
	
	//window.opener.afterSubmit("<%=FlagStr%>","<%=Content%>");	
	
</script>
</html>
<%
  	}

%>