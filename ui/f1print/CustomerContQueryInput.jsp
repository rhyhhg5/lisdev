<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：CustomerContQueryInput.jsp
//程序功能：新增保单客户信息查询，包括：投保人信息、被保人信息、被保人保费信息
//创建日期：2008-07-01
//创建人  ：苗祥征
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>

<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>
<script>	        //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	function getSql(){
		var tempSql="1 and managecom like #"+fm.all('ManageCom').value+"%# ";
		return tempSql;
	}
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<%@include file="CustomerContQueryInit.jsp"%>
<SCRIPT src="CustomerContQueryInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>

<body  onload="initForm();initElementtype();" >
  <form  action="./CustomerContQueryReport.jsp" method=post name=fm target="f1print">
    
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class = codename name = ManageComName elementtype=nacessary></TD>          
     	<TD  class= title>销售单位代码</TD>
          <TD  class= input> <Input class="common" name=BranchAttr  ></TD>          
     	
    </TR>
    <TR  class= common>
          <TD  class= title>
            业务员代码
          </TD>
          <TD  class= input>
            <Input class=codeNo name=AgentCode  ondblclick="return showCodeList('AgentCode',[this,AgentCodeName],[0,1],null,getSql(),'1');" onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeName],[0,1],null,getSql(),'1');"><input class=codename name=AgentCodeName elementtype=nacessary >
          </TD>
          <TD  class= title>
     类型
    </TD>
    <TD  class= input>
      <Input class= codeno name=QYType verify="|len<=20" CodeData= "0|^投保人方式查询|1^被保人方式查询|2^被保人保费方式查询|3" ondblClick= "showCodeListEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);" onkeyup= "showCodeListKeyEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);"><Input class= 'codename' name=QYType_ch  elementtype=nacessary >
    </TD>
        </TR>        
    </table>

    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>