<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%
    boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	XmlExport txmlExport = null;   
	GlobalInput tG = (GlobalInput)session.getValue("GI");

	String tCardType = request.getParameter("CardType"); //卡号类型
	String tCardNo = request.getParameter("CardNo");  //卡号
  String tHandleDate = request.getParameter("HandleDate"); //下发日期
	String tState = request.getParameter("State");  //状态
	String tAppntNo = request.getParameter("AppntNo"); //投保人
	String tInsuredNo = request.getParameter("InsuredNo");  //被保人
	String tManageCom = request.getParameter("ManageCom"); //管理机构
	String tAgentCode = request.getParameter("AgentCode"); //代理人
	String tCValiDate1 = request.getParameter("CValiDate1");  //起始生效日期
	String tCValiDate2 = request.getParameter("CValiDate2"); //终止生效日期
	String tOperator = request.getParameter("Operator");  //操作人员
	String tMakeDate1 = request.getParameter("MakeDate1"); //起始操作日期
	String tMakeDate2 = request.getParameter("MakeDate2");  //终止操作日期
	String tSQL = request.getParameter("SQL");  
	
    System.out.println(tCardType);
	  System.out.println(tCardNo);
    System.out.println(tHandleDate);
    System.out.println(tState);
	  System.out.println(tAppntNo);
    System.out.println(tInsuredNo);
    System.out.println(tManageCom);
	  System.out.println(tAgentCode);
    System.out.println(tCValiDate1);
    System.out.println(tCValiDate2);
	  System.out.println(tOperator);
    System.out.println(tMakeDate1);
    System.out.println(tMakeDate2);
    System.out.println(tSQL);
    
    TransferData tTransferData= new TransferData();
	tTransferData.setNameAndValue("CardType",tCardType);
	tTransferData.setNameAndValue("CardNo",tCardNo);
	tTransferData.setNameAndValue("HandleDate",tHandleDate);
	tTransferData.setNameAndValue("State",tState);
	tTransferData.setNameAndValue("AppntNo",tAppntNo);
	tTransferData.setNameAndValue("InsuredNo",tInsuredNo);
	tTransferData.setNameAndValue("ManageCom",tManageCom);
	tTransferData.setNameAndValue("AgentCode",tAgentCode);
	tTransferData.setNameAndValue("CValiDate1",tCValiDate1);
	tTransferData.setNameAndValue("CValiDate2",tCValiDate2);
	tTransferData.setNameAndValue("Operator",tOperator);
	tTransferData.setNameAndValue("MakeDate1",tMakeDate1);
  tTransferData.setNameAndValue("MakeDate2",tMakeDate2); 
  tTransferData.setNameAndValue("SQL",tSQL); 


	VData tVData = new VData();
    tVData.addElement(tG);
	tVData.addElement(tTransferData);
          
    CardQueryReportUI tCardQueryReportUI = new CardQueryReportUI(); 
    if(!tCardQueryReportUI.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tCardQueryReportUI.mErrors.getErrContent();                
    }
    else
    {    
		VData mResult = tCardQueryReportUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	System.out.println(operFlag);
	if (operFlag==true)
	{
		session.putValue("PrintStream", txmlExport.getInputStream());
		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
	}
	else
	{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>