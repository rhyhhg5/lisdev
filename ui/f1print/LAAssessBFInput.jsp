<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LAAssessBFInput.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
  String BranchType=request.getParameter("BranchType");
		String BranchType2=request.getParameter("BranchType2");
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LAAssessBFInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LAAssessBFInit.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body onload="initForm();initElementtype();" >    
  <form action="./LAAssessBFInputReport.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
           <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'#1# and (length(trim(comcode))=8 or length(trim(comcode))=4) ','char(1)',1);"  
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,'#1# and (length(trim(comcode))=8 or length(trim(comcode))=4) ','char(1)',1);"  
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD> 
          <TD  class= title>
            营销员代码  
          </TD>
          <TD  class= input>
            <Input  class=common  name=AgentCode  >
          </TD>            
        </TR>
        <TR  class= common>
          <TD  class= title>
             考核年
          </TD>
          <TD  class= input>
           <Input  class=common   name=AssessYear  verify="考核年|NOTNULL&int&len=4" elementtype=nacessary  >  
          </TD>
          <TD  class= title>
             考核月
          </TD>
          <TD  class= input>
           <Input    class=common name=AssessMonth verify="考核月|NOTNULL&int&len=2" elementtype=nacessary >  
          </TD>
    
       </TR>
       <TR class=input>     
         <TD class=common>
          <input type =button class=cssbutton value="查  询" onclick="easyQueryClick();">  
          <INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">  
        </TD>
       </TR> 
    </table>
    <p> <font color="#ff0000">注：若维持差额为0,代表该考核项达到维持的要求;晋升差额为0,代表该考核项达到晋升要求 </font></p>
    <Div  id= "divAgentQuery" style= "display: ''">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span> 
  	</TD>
      </TR>
     </Table>					
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
   </Div>	  
    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type=hidden name=AgentGroup1 value=''>   
    <input type=hidden name=AgentGroup2 value=''>  
    <input type=hidden name=AgentGroup3 value=''>  
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchType2 value=''>
		<input type=hidden name=Operate value="">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->   </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 