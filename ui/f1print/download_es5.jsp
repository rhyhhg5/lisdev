<%@ page contentType="application/x-xls;charset=GBK"%>
<%@page import="java.io.*"%>  
<%  
  //关于文件下载时采用文件流输出的方式处理：  
  //加上response.reset()，并且所有的％>后面不要换行，包括最后一个；  
   
  response.reset();//可以加也可以不加  
  response.setContentType("application/x-download");  
   
  
//若使用字节流进行文件下载，应该首先清空一下out对象，默认是字符流
  request.setCharacterEncoding("GBK");
  //response.setCharacterEncoding("GBK");
  String filename=request.getParameter("filename");
  String filenamepath=request.getParameter("filenamepath");
  //filename=new String(filename.getBytes("UTF-8"), "GBK");
  filename=java.net.URLDecoder.decode(filename, "UTF-8");
  filenamepath=java.net.URLDecoder.decode(filenamepath, "UTF-8");
  //filenamepath=new String(filenamepath.getBytes("UTF-8"), "GBK");
  System.out.println("filename="+filename);
  System.out.println("filenamepath="+filenamepath);
  
  if(filenamepath.contains("./")||filenamepath.contains("../")||filenamepath.contains("%")){
	  return;
}
String filedownload = filenamepath+filename;  
String filedisplay = filename;  

  response.addHeader("Content-Disposition","attachment;filename=" + filedisplay);  
   
  java.io.OutputStream outp = null;  
  java.io.FileInputStream in = null;  
  try  
  {  
  outp = response.getOutputStream();  
  in = new FileInputStream(filedownload);  
   
  byte[] b = new byte[1024];  
  int i = 0;  
   
  while((i = in.read(b)) > 0)  
  {  
  outp.write(b, 0, i);  
  }  
//    
outp.flush();  
//要加以下两句话，否则会报错  
//java.lang.IllegalStateException: getOutputStream() has already been called for //this response    
out.clear();  
out = pageContext.pushBody();  
}  
  catch(Exception e)  
  {  
  System.out.println("Error!");  
  e.printStackTrace();  
  }  
  finally  
  {  
  if(in != null)  
  {  
  in.close();  
  in = null;  
  }  
//这里不能关闭    
//if(outp != null)  
  //{  
  //outp.close();  
  //outp = null;  
  //}  
  }  
%>  