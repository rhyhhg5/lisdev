<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：HMExamHospSetReport.jsp
//程序功能：核保体检医院设置情况统计
//创建日期：2010-04-1
//创建人  ：chenxw
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="HMExamHospSetReport.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<script>
	function initManageCom(){
    fm.ManageCom.value="<%=tG1.ManageCom%>";
    showOneCodeNametoAfter("comcode","ManageCom","ComName");
  }
</script>
</head>
<body onload="initManageCom();initElementtype();" >    
  <form action="./HMExamHospSetReportPrint.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> 
          	<Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&INT" 
          			ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" 
          			onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"
          	><input class=codename name=ComName elementtype=nacessary>
          </TD>          
		  <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short'> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short'> </TD> 
       	</TR>
    </table>
    <hr>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="downLoad()"></TD>
			</tr>
		</table>
	<Input type="hidden" class= common name="RiskRate" >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 