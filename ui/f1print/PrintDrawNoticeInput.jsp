<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//name       ：PrintDrawNoticeInput.jsp
//function   ：打印满期生存领取的通知书
//Create Date：2003-11-12
//Creator    ：刘岩松

%>

<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="PrintDrawNoticeInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="PrintDrawNoticeInit.jsp"%>
	</head>

<body  onload="initForm();" >

  	<form action="./PrintDrawNoticeSave.jsp" method=post name=fm target="fraSubmit">

		<%@include file="../common/jsp/InputButton.jsp"%>
    		<table  class= common>
          <TR  class= common>
            <TD  class= title>开始日期	</TD>
            <TD  class= input><input class="coolDatePicker" dateFormat="short" name="StartDate" >	</TD>

            <TD  class= title>结束日期</TD>
            <TD  class= input>	<input class="coolDatePicker" dateFormat="short" name="EndDate" >	</TD>
            <input class=common name = "PolNo" type = hidden>
            <Input class=common type = hidden name="PrintFlag" >
            <!--"PringFlag"用来记录打印信息，是单张打印还是批量打印.PrintFlag="0"表示是单张打印，
            若PrintFlag＝“1”表示是批量打印-->
          </TR>
        </table>
        <INPUT VALUE="查询" class= common TYPE=button onclick="DrawNoticeQuery();">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 生存领取通知书信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();">
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();">
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();">
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();">
  	</div>
  	<p>
     <INPUT VALUE = "单张打印通知书" CLASS= common TYPE =button ONCLICK="SinglePrint()">
      <INPUT VALUE = "批量打印通知书" CLASS=common TYPE =button ONCLICK="BatchPrint()">
  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<body>
<form action="./PrintDrawNoticeSave.jsp" method=post name=fm1 target="fraSubmit">
</form>
</body>
</html>
