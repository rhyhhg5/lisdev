<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  


  <%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：ContsInGracePeriod.jsp
//程序功能：列举进入宽限期的万能险保单信息（如客户帐户价值不足以扣除当月费用，则保单进入宽限期 ，宽限期为60天）
//创建日期：2008-06-13 
//创建人  ：张彦梅  电话：13146281663
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="./ContsInGracePeriod.js"></SCRIPT>  
  <%@include  file="ContsInGracePeriodInit.jsp" %>
 
 
</head>
 <%
     GlobalInput tGI = new GlobalInput();
     tGI=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
     String CurrentDate= PubFun.getCurrentDate();   
     String tCurrentYear=StrTool.getVisaYear(CurrentDate);
     String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
     String tCurrentDate=StrTool.getVisaDay(CurrentDate); 
     
     String AheadMonth="-1";
     FDate tD=new FDate();
     Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadMonth),"M",null);
     String SubDate=tD.getString(AfterDate);  
     String tSubYear=StrTool.getVisaYear(SubDate);
     String tSubMonth=StrTool.getVisaMonth(SubDate);
     String tSubDate=StrTool.getVisaDay(SubDate);  
 
 %>  
 <script>
  
  var CurrentYear=<%=tCurrentYear%>;  
  var CurrentMonth=<%=tCurrentMonth%>;  
  var CurrentDate=<%=tCurrentDate%>;
  var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
 
  var SubYear=<%=tSubYear%>;  
  var SubMonth=<%=tSubMonth%>;  
  var SubDate=<%=tSubDate%>;
  var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
  var managecom = <%=tGI.ManageCom%>;
 
 
 </script>

 <body  onload="initForm();" >
 	<form action="PrintContsInGracePeriod.jsp" method="post" name="fm" target="fraSubmit" id="fm">
 	<table>
 		<tr>
 			<td class="common">开始日期</td>
 			<td class=common>
 				<input class="coolDatePicker" dateFormat="short" ID="aBeginDate" name="aBeginDate" >
 			</td>
 			<td class="common">结束日期</td>
 			<td class=common>
 				<input class="coolDatePicker" dateFormat="short" ID="aEndDate" name="aEndDate" >
 			</td>
 			<td class="common">机构</td>
 			<td class="input">
 				<Input class= "codeno" ID="ManageCom" name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName>
 			</td> 	
 			<td class="common">
 				
 			</td> 		
 		</tr>
 		<tr>
 			<td class="common">业务员代码</td>
 			<td class="input">
 				<Input class=common id="AgentCode" name=AgentCode>
 			</td>
 			<td class="common">保单号</td>
 			<td class="input">
 				<Input class=common id="ContNo" name=ContNo>
 			</td>
 			<td class="common">投保人</td>
 			<td class="input">
 			 	<Input class=common id="AppntName" name=AppntName>
 			</td>
 			<td class="common">
 				<INPUT VALUE="查询宽限期保单" class = cssbutton TYPE=button onclick="easyQueryClick();">  
 			</td> 
 		</tr>
 	</table>
 		<Div  id= "GracePeriodContsDiv" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGracePeriodContsGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <center>    	
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
		</center>  	
  	</div> 
  	<table>
  	<tr>
  	    <td> 	         
  	    <input class=cssButton type=button value="打印个人账户对帐单" onclick="printBill();">
		<input class=cssButton type=button value="打印交费通知书" onclick="printNotice();">
		<input type=hidden id="sql" name="sql" value="">
  	    </td>
  	</tr>
  	</table>         
 	</form>
 	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
 </body>
  
 </html>