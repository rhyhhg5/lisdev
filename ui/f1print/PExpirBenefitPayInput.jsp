<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：PExpirBenefitPayInput.jsp
 //程序功能：满期金计提数据抽取
 //创建日期：2015-8-18
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
// ;initElementtype()
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="PExpirBenefitPayjs.js"></SCRIPT>
</head>
<body  onload="initForm();">
<form action="./PExpirBenefitPaySave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >满期金计提数据抽取</td>
  </tr>
</table>
<table  class= common>
       <TR  class= common>
          <TD  class= title>  起始时间</TD>
          <TD  class= input>
            <Input class= "coolDatePicker" verify="起始时间|notnull&date" dateFormat="short" name=StartDay >
          </TD>
          <TD  class= title> 结束时间</TD>
          <TD  class= input>
            <Input class= "coolDatePicker" verify="结束时间|notnull&date"  dateFormat="short" name=EndDay >
          </TD>
        </TR> 
           <TR  class= common>		
   			<TD class= title>满期保单号</TD>
        <TD class= input>
        	<Input class= common name=ContNo> 
        </TD>
        </TR> 
</table>
<br>
<input value="数据抽取" type=button onclick="DataExt()" class="cssButton" type="button"> 
<hr>
<div id="div1" style="display: none;">
<table>
  <tr>
    <td class="titleImg" ></td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="span" >
     </span> 
      </td>
   </tr>
</table>
</div>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
