//程序名称：CustomerContQueryInpur.js
//程序功能：
//创建日期：2008-07-01
//创建人  ：苗祥征
//更新记录：  更新人    更新日期     更新原因/内容
var showinfo;
function submitForm()
{   
  if(!beforeSubmit()){
  	return false;
  }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function beforeSubmit(){
	if(fm.ManageCom.value==''||fm.ManageCom.value==null){
		alert('管理机构不能为空！');
		return false;
	}
	if(fm.AgentCode.value==''||fm.AgentCode.value==null){
		alert('业务员代码不能为空！');
		return false;
	}else{
	  	var strSQL = "";
	strSQL = "select agentcode from laagent where 1=1 "
	+ getWherePart('GroupAgentCode','AgentCode');
	var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
	 var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
  
    fm.all('AgentCode').value = tArr[0][0];
	}
	if(fm.QYType.value==''||fm.QYType.value==null){
		alert('打印报表类型不能为空！');
		return false;
	}
	return true;
}