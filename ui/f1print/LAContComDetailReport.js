//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
   if (verifyInput() == false)
     return false;
   if( !beforeSubmit())
   {
   	return false;
   }
  
    //校验时间
   if( !beforeSubmit2())
   {
   	return false;
   }
   	
	// 取得制表机构
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
  fm.target = "f1print";
  fm.all('op').value = '';
  fm.submit();
  showInfo.close();
  fm.reset();
}
function beforeSubmit()
{
  if(fm.all("StartDate2").value!=null && fm.all("StartDate2").value!="")
  {
    if(fm.all("StartDate2").value<fm.all("StartDate1").value)
    {	
  	alert("交单起期不能大于承保起期！");
  	return false;
  		
    }
    if(fm.all("StartDate2").value>fm.all("EndDate1").value)
    {	
  	alert(" 承保起期不能大于交单止期！");
  	return false; 		
    }	
    if(fm.all("StartDate2").value>fm.all("EndDate2").value)
    {	
  	alert(" 承保起期不能大于承保止期！");
  	return false; 		
    }	
  }
  if(fm.all("StartDate1").value>fm.all("EndDate1").value)
  {	
  	alert("交单起期不能大于交单起期！");
  	return false;
  		
  }
  if(fm.all("EndDate2").value<=fm.all("EndDate1").value)
  {	
  	alert("承保止期因该大于交单止期！");
  	return false;
  		
  }	
	
  return true;
}

function beforeSubmit2()
{
	  
   //时间间隔不能超过六个月
   var mStartDate1= fm.all('StartDate1').value;
   var mEndDate1= fm.all('EndDate1').value;
   var months1=dateDiff(mStartDate1,mEndDate1,'M');
   if(months1>6)
   {
      alert("查询的交单日期时间间隔大于6个月，请重新输入日期！");  
      fm.StartDate1.value="";
      fm.EndDate1.value="";
      return  false;
    }	   
 
	if(fm.all("StartDate2").value!=null && fm.all("StartDate2").value!="")
  {
   var mStartDate2= fm.all('StartDate2').value;
   var mEndDate2= fm.all('EndDate2').value;
   var months2=dateDiff(mStartDate2,mEndDate2,'M');
   if(months2>6)
   {
      alert("查询的承保日期时间间隔大于6个月，请重新输入日期！");  
      fm.StartDate2.value="";
      fm.EndDate2.value="";
      return  false;
    }	 
  }  
  return true;
}

function checkBranchAttr()
{
   var sql=" select agentgroup,branchlevel  from labranchgroup  where branchattr='"+fm.BranchAttr.value+"'  and  branchtype='1'	"
           +" and branchtype2='01'    "
           + getWherePart("ManageCom","ManageCom",'like')	;
   var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
 // alert(sql);
   if (!strQueryResult) 
   {
      alert("此管理机构没有此销售单位！");  
      fm.BranchAttr.value="";
      return;
    }	
    var arr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentGroup').value=arr[0][0];
    
}	
 

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
	fm.reset();
}