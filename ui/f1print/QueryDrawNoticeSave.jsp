<%@page contentType="text/html;charset=GBK" %>
<%
//name : PrintDrawNoticeSave.jsp
//functon : Receive data and transfer data
//Creator  ：刘岩松
//Date ：2003-11-12
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.f1print.*"%>
<%
  System.out.println("开始执行PrintDrawNoticeSave.jsp");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  String strStartDate = request.getParameter("StartDate");
  String strEndDate   = request.getParameter("EndDate");
  System.out.println("开始日期是"+strStartDate);
  System.out.println("结束日期是"+strEndDate);
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  VData tVData = new VData();
  tVData.addElement(strStartDate);
	tVData.addElement(strEndDate);
  tVData.addElement(tG);
  QueryDrawNoticeUI tQueryDrawNoticeUI = new QueryDrawNoticeUI();
  if (!tQueryDrawNoticeUI.submitData(tVData,"QUERY"))
   {
       Content = " 查询失败，原因是: " + tQueryDrawNoticeUI.mErrors.getError(0).errorMessage;
       FlagStr = "Fail";
	}
  else
  {
    tVData.clear();
    tVData = tQueryDrawNoticeUI.getResult();
    LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
    tLJSGetDrawSet.set((LJSGetDrawSet)tVData.getObjectByObjectName("LJSGetDrawSet",0));
    int n = tLJSGetDrawSet.size();
    System.out.println("get report "+n);
    String Strtest ="0|" + n + "^"+tLJSGetDrawSet.encode();
    System.out.println("Strtest==="+Strtest);
    %>
      <script language="javascript">
      try
      {
        parent.fraInterface.displayQueryResult('<%=Strtest%>');
      } catch(ex) {}
      </script>
    <%
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tQueryDrawNoticeUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 查询成功! ";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 查询失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
  
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">	
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>