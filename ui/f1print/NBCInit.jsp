<%
//程序名称：NBCInit.jsp
//程序功能：
//创建日期：2003-03-04
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
var PolGrid;          //定义为全局变量，提供给displayMultiline使用

// 输入框的初始化（单记录部分）
function initInpBox()
{
	try
	{
		fm.reset();
	}
	catch(ex)
	{
		alert("在NBCInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm()
{
	try
	{
		initInpBox();
		initPolGrid();
		fm.all('ManageCom').value = <%=strManageCom%>;
		  		  if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}  
		showCodeName();
	}
	catch(re)
	{
		alert("NBCInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

// 保单信息列表的初始化
function initPolGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=10;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="流水号";
		iArray[1][1]="140px";
		iArray[1][2]=100;
		iArray[1][3]=3;

		iArray[2]=new Array();
		iArray[2][0]="通知书号码";
		iArray[2][1]="140px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="业务员代码";
		iArray[3][1]="100px";
		iArray[3][2]=100;
		iArray[3][3]=0;


		iArray[4]=new Array();
		iArray[4][0]="业务员组别";
		iArray[4][1]="0px";
		iArray[4][2]=200;
		iArray[4][3]=3;

		iArray[5]=new Array();
		iArray[5][0]="展业机构";
		iArray[5][1]="120px";
		iArray[5][2]=200;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="管理机构";
		iArray[6][1]="120px";
		iArray[6][2]=100;            
		iArray[6][3]=0;

		
		iArray[7]=new Array();
		iArray[7][0]="保单号码";
		iArray[7][1]="150px";
		iArray[7][2]=200;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="退费金额";
		iArray[8][1]="100px";
		iArray[8][2]=200;
		iArray[8][3]=0;

		iArray[9]=new Array();
		iArray[9][0]="退费类型";
		iArray[9][1]="0px";
		iArray[9][2]=200;
		iArray[9][3]=3;

		PolGrid = new MulLineEnter( "fmSave" , "PolGrid" );
		//这些属性必须在loadMulLine前
		PolGrid.mulLineCount = 10;
		PolGrid.displayTitle = 1;
		PolGrid.hiddenPlus = 1;
		PolGrid.hiddenSubtraction = 1;
		PolGrid.canSel = 1;
		PolGrid.locked = 1;
		PolGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>