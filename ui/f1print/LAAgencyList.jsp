<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LABranchWage.jsp
//程序功能：F1报表生成
//创建日期：2007-11-23
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>

<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="LAAgencyList.js"></SCRIPT>   

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

<%@include file="LAAgencyListInit.jsp"%>
 
   
</head>
<body onload="initForm();initElementtype();" >    
  <form action="./LAAgencyListSave.jsp"  method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom  
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName> 
          </TD>           
        </TR>
        <TR>
          <TD  class= title>
             中介专员编码
          </TD>
          <TD  class= input>
          <Input  class=common  name=AgentCode  >
          </TD>
          
         <TD  class= title>
            中介专员姓名
          </TD>
          <TD  class= input>
           <Input  class=common  name=AgentName  >
          </TD>
          
    
        </TR>
        <TR>
          <TD  class= title>
            查询起期 
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=StartDate  >  
          </TD> 
          <TD  class= title>
            查询止期 
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EndDate   >  
          </TD> 
     
    </table>

    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type=hidden name=AgentGroup value=''>   
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->   </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html> 
