//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
	var i = 0;
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}
	else
	{
//		arrReturn = getQueryResult();
		ActuGetNo = PolGrid.getRowColData(tSel-1,2);


		fmSave.target="f1print";
		fmSave.ActuGetNo.value = ActuGetNo ;
		fmSave.PrtSeq.value = PolGrid.getRowColData(tSel-1,1);;
		fmSave.OtherNo.value = PolGrid.getRowColData(tSel-1,7);
		fmSave.OtherNoType.value = PolGrid.getRowColData(tSel-1,9);
		fmSave.BackFee.value = PolGrid.getRowColData(tSel-1,8);
		if(LoadFlag=="2")
		{
			
			fmSave.fmtransact.value = "CONFIRM";
			fmSave.action="./NBCF2PSave.jsp";
			fmSave.submit();
		}
		else
		{
				fmSave.fmtransact.value = "CONFIRM";
				fmSave.submit();	
		}
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	{
		return arrSelected;
	}
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	if(LoadFlag=="null"||LoadFlag=="")
	{
				//个单新契约退费通知书
			var strSQL = "";
			var strManageComWhere = " ";
		
			if( fm.BranchGroup.value != '' ) {
				strManageComWhere += " AND LAAgent.AgentGroup IN ( SELECT AgentGroup FROM LABranchGroup WHERE BranchAttr LIKE '" + fm.BranchGroup.value + "%%') ";
			}
		
			if(fm.FeeBackType.value=="")
			{
				alert("请选择退费类型");
				return false;
			}
			else
			{
				if(fm.FeeBackType.value=="1")
				{
					strSQL = "SELECT LOPRTManager.PrtSeq, LOPRTManager.OtherNo,LOPRTManager.AgentCode, LAAgent.AgentGroup,LABranchGroup.BranchAttr,LOPRTManager.ManageCom,ljaget.otherno,ljaget.sumgetmoney,ljaget.othernotype FROM LOPRTManager,LAAgent,LABranchGroup,ljaget WHERE LOPRTManager.Code = '08' and loprtmanager.otherno=ljaget.actugetno "
						+ "and LAAgent.AgentCode = LOPRTManager.AgentCode "
						+ "and LABranchGroup.AgentGroup = LAAgent.AgentGroup"
						+ " and ljaget.othernotype = '4' "
						+ getWherePart('ljaget.otherno', 'PolNo')
						+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
						+ getWherePart('LOPRTManager.AgentCode','AgentCode')
					+ getWherePart('LAAgent.AgentGroup','AgentGroup')
					+ " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'"//登陆机构权限控制
					+ strManageComWhere
					+" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";
					type="before";
			}
			else if(fm.FeeBackType.value=="2")
			{
				strSQL = "SELECT LOPRTManager.PrtSeq, LOPRTManager.OtherNo,LOPRTManager.AgentCode, LAAgent.AgentGroup,LABranchGroup.BranchAttr,LOPRTManager.ManageCom,ljaget.otherno,ljaget.sumgetmoney,ljaget.othernotype FROM LOPRTManager,LAAgent,LABranchGroup,ljaget WHERE LOPRTManager.Code = '08' and loprtmanager.otherno=ljaget.actugetno "
					+ " and LAAgent.AgentCode = LOPRTManager.AgentCode "
					+ " and LABranchGroup.AgentGroup = LAAgent.AgentGroup"
					+ " and ljaget.othernotype = '6' "
					+ getWherePart('ljaget.otherno', 'PolNo')
					+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
					+ getWherePart('LOPRTManager.AgentCode','AgentCode')
					+ getWherePart('LAAgent.AgentGroup','AgentGroup')
					+ " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'"//登陆机构权限控制
					+ strManageComWhere
					+" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";
					type="after";
			}
		}
	
		turnPage.queryModal(strSQL,PolGrid,0,1);
	}
	
	//团体新契约退费通知书
	if(LoadFlag=="2")
	{
			// 书写SQL语句
			var strSQL = "";
			var strManageComWhere = " ";
		
			if( fm.BranchGroup.value != '' ) {
				strManageComWhere += " AND LAAgent.AgentGroup IN ( SELECT AgentGroup FROM LABranchGroup WHERE BranchAttr LIKE '" + fm.BranchGroup.value + "%%') ";
			}
		
			if(fm.FeeBackType.value=="")
			{
				alert("请选择退费类型");
				return false;
			}
			else
			{
				if(fm.FeeBackType.value=="1")
				{
					strSQL = "SELECT LOPRTManager.PrtSeq, LOPRTManager.OtherNo,LOPRTManager.AgentCode, LAAgent.AgentGroup,LABranchGroup.BranchAttr,LOPRTManager.ManageCom,ljaget.otherno,ljaget.sumgetmoney,ljaget.othernotype FROM LOPRTManager,LAAgent,LABranchGroup,ljaget WHERE LOPRTManager.Code = '08' and loprtmanager.otherno=ljaget.actugetno "
						+ "and LAAgent.AgentCode = LOPRTManager.AgentCode "
						+ "and LABranchGroup.AgentGroup = LAAgent.AgentGroup"
						+ " and ljaget.othernotype = '4' "
						+ getWherePart('ljaget.otherno', 'PolNo')
						+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
						+ getWherePart('LOPRTManager.AgentCode','AgentCode')
					+ getWherePart('LAAgent.AgentGroup','AgentGroup')
					+ " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'"//登陆机构权限控制
					+ strManageComWhere
					+" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";
					type="before";
			}
			else if(fm.FeeBackType.value=="2")
			{
				strSQL = "SELECT LOPRTManager.PrtSeq, LOPRTManager.OtherNo,LOPRTManager.AgentCode, LAAgent.AgentGroup,LABranchGroup.BranchAttr,LOPRTManager.ManageCom,ljaget.otherno,ljaget.sumgetmoney,ljaget.othernotype FROM LOPRTManager,LAAgent,LABranchGroup,ljaget WHERE LOPRTManager.Code = '08' and loprtmanager.otherno=ljaget.actugetno "
					+ " and LAAgent.AgentCode = LOPRTManager.AgentCode "
					+ " and LABranchGroup.AgentGroup = LAAgent.AgentGroup"
					+ " and ljaget.othernotype = '8' "
					+ getWherePart('ljaget.otherno', 'PolNo')
					+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
					+ getWherePart('LOPRTManager.AgentCode','AgentCode')
					+ getWherePart('LAAgent.AgentGroup','AgentGroup')
					+ " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'"//登陆机构权限控制
					+ strManageComWhere
					+" AND LOPRTManager.StateFlag = '0' AND LOPRTManager.PrtType = '0'";
					type="after";
			}
		}
	
		turnPage.queryModal(strSQL,PolGrid,0,1);
	}	

}


function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{

  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}