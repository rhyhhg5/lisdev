<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LACustomNumInput.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：sgh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LACustomNumInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body onload="initElementtype();" >    
  <form action="./LACustomNumReport.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom 
            ondblclick="return showCodeList('ComCode0',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode0',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName readOnly > 
          </TD>
          </TR>         
          <TR  class=common>              
          <TD  class= title>
            销售单位代码 
          </TD>
          <TD  class= input>
            <Input  class=common  name=BranchAttr onchange="return checkBranchAttr()">
          </TD> 
          <TD  class= title>
            营销员代码  
          </TD>
          <TD  class= input>
            <Input  class=common  name=GroupAgentCode onchange = "return checkAgentCode()">
          </TD> 
           
        </TR>
          <TR  class= common>
          <TD  class= title>
             查询起期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=StartDate  verify="起期|NOTNULL" elementtype=nacessary  >  
          </TD>
          <TD  class= title>
             查询止期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="止期|NOTNULL" elementtype=nacessary >  
          </TD>  
  </TR>
    </table>
       <input type="hidden" name=AgentGroup value="">
       <Input type="hidden" name=BranchType value="">
       <Input type="hidden" name=BranchType2 value="">
       <input type="hidden" name=AgentCode value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->   </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 