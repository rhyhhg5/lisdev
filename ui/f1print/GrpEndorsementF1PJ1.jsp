<%@page contentType="text/html; charset=GBK" %>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%
    LDSysVarDB tLDSysVarDB = new LDSysVarDB();
  
    String strSql1 = "select * from ldsysvar where Sysvar='VTSFilePath'";
    LDSysVarSet tLDSysVarSet = tLDSysVarDB.executeQuery(strSql1);    
    LDSysVarSchema tLDSysVarSchema = tLDSysVarSet.get(1);
    String strFilePath = tLDSysVarSchema.getV("SysVarValue");
    
    String showToolBar = request.getParameter("showToolBar");
    
    if(showToolBar == null || showToolBar.equals("")) {
        showToolBar = "true";
    }
    
    String strRealPath = application.getRealPath("/").replace('\\', '/') + "/";
    String strVFFileName = strFilePath + FileQueue.getFileName()+".vts";
    String strVFPathName = strRealPath + strVFFileName;
     
		GlobalInput tG = (GlobalInput)session.getAttribute("GI");
		String strEdorNo = request.getParameter("EdorNo");
		String strErrInfo = "";

		InputStream ins = null;

   if ( strEdorNo != null && !strEdorNo.equals("")) 
   {  // 合并VTS模板文件与数据文件存入服务器磁盘中
        // 建立数据库连接
      try
      {
         Connection conn = DBConnPool.getConnection();
         Statement stmt = null;
         ResultSet rs = null;
         if( conn == null ) 
         {
             strErrInfo = "连接数据库失败";
         } 
         else
         {
						  
						stmt = conn.createStatement();
						String sql = "SELECT * FROM LPEDORPRINT WHERE EdorNo = '" + strEdorNo + "'"; 
						rs = stmt.executeQuery(sql);
						if (rs.next())
						{
						  //输出数据文件
						  Blob tBlob = rs.getBlob("EdorInfo");
						  ins = tBlob.getBinaryStream();		
						}
						rs.close();
						stmt.close();
						conn.close();              
	                
	        //合并VTS文件 
	        String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
	        CombineVts tcombineVts = new CombineVts(ins, strTemplatePath);
	       
	        ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
	        tcombineVts.output(dataStream);
	        
	        //把dataStream存储到磁盘文件
	        AccessVtsFile.saveToFile(dataStream, strVFPathName);
	            System.out.println("FFF-----:" + strVFPathName);
	        response.sendRedirect("./GetF1PrintJ1_new.jsp?RealPath=" + strVFPathName+"&showToolBar="+showToolBar);
        }
      }
      catch (Exception ex)
      {
          ex.printStackTrace();
      }
    }    
		else
		{
			strErrInfo = "没有输入保全号";
		}
%>
<%  
	  session.setAttribute("EdorNo", strEdorNo); 	
%>
