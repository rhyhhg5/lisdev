var turnPage = new turnPageClass();
// 查询按钮
function easyQueryClick()
{
    if (!verifyInput()){
        return false;
    }
    // 书写SQL语句
    var strSQL = "";
    var sSql1 = "";
    var eSql1 = "";
    
    if (fm.all("StartDate").value != null && fm.all("StartDate").value != "") {
         sSql1 = " and pm2.makedate>='" + fm.all("StartDate").value + "' ";
    }
    
    if (fm.all("EndDate").value != null && fm.all("EndDate").value != "") {
         eSql1 = " and pm2.makedate<='" + fm.all("EndDate").value + "' ";
    }
    
    strSQL = "select ap.incomeno,ap.incometype,ap.sumactupaymoney,pm2.makedate,pm2.printamount,pm2.payer,pm2.reqoperator from LOPRTManager2 pm2 ,ljapay ap "
            + "where pm2.otherno=ap.payno and pm2.standbyflag5='5' "
            + getWherePart('ap.IncomeType','IncomeType')
            + getWherePart('ap.IncomeNo','IncomeNo')
            + getWherePart('pm2.reqoperator','Operator')
            + getWherePart('ap.ManageCom','ManageCom','like') + sSql1 + eSql1;
             

     //查询SQL，返回结果字符串
     var strSqlTemp=easyQueryVer3(strSQL, 1, 0, 1); 
     turnPage.strQueryResult=strSqlTemp;
     turnPage.pageLineNum = 20;
     
     if(!turnPage.strQueryResult)
     {
        window.alert("没有查询记录!");
        InvoiceGrid.clearData();
        return false;
     }
     else
     {
        fm.sql.value = strSQL;
        turnPage.queryModal(strSQL,InvoiceGrid);
     }
  
  return true;
}

function download(){
    if(InvoiceGrid.mulLineCount == 0 || InvoiceGrid.mulLineCount == null)

  {
    alert("没有需要下载的数据，请先查询！");
    return false;
  }
  submitForm();
}

function submitForm() {
  //检验录入框
    if (!verifyInput()){
        return false;
    }
    fm.submit(); //提交
}