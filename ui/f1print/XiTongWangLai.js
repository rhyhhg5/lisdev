//程序名称：ShieldBorUnevenInput.js 
//程序功能：接口表数据维护
//创建日期：2015-06-03 
//创建人  ：m
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();//日结试算相关数据

var mFlag = "0";
var showInfo;
var mOperate="";

function beforesubmit(){
	 
	if(fm.StartDay.value==''||fm.StartDay.value==null){
			alert("请选择起始日期");
			fm.StartDay.focus();
			return false;
	}
if(fm.EndDay.value==''||fm.EndDay.value==null){
			alert("请选择终止日期");
			fm.EndDay.focus();
			return false;
	}
	return true;
}

function XiTongWangLaiKeMuQuery()
{ 	
		if(beforesubmit()){
	 var i = 0;
	 var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
    var XiTongWangLaiKeMusql="";
    XiTongWangLaiKeMusql = "select contno,accountdate,sum(case finitemtype when 'C' then -summoney else summoney end )  from fivoucherdatadetail  " +
    		"where 1=1 and accountdate between '"+fm.StartDay.value+"' and '"+fm.EndDay.value+"'  " +
    				"and accountcode = '3001020000' and checkflag='00' " +
    				"group by contno,accountdate " +
    				"having sum(case finitemtype when 'C' then -summoney else summoney end )<>0 " +
    				"with ur";
	 turnPage.pageDivName = "XiTongWangLaiKeMuDataGrid";
     turnPage.queryModal(XiTongWangLaiKeMusql,XiTongWangLaiKeMuDataGrid); 
     showInfo.close(); 
	
	if(XiTongWangLaiKeMuDataGrid.mulLineCount=="0"){
		alert("没有查询到数据");
	}	
	return true;		
	}
   }


function afterSubmit( FlagStr, content )
{
	  showInfo.close();
	  if (FlagStr == "Fail" )
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }
	  else
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	    //执行下一步操作
	  }
}
function showRecord(strRecord)
{
	 alert("3333333333333333333333333333333333");
  //保存查询结果字符串
  turnPage.strQueryResult  = strRecord;

//alert(strRecord);
  
  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;  
    
  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray = new Array(0,9,4,1,7);
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  //初始化的对象
  	 turnPage.pageDisplayGrid = CheckQueryDataGrid;       
              
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
	
}






