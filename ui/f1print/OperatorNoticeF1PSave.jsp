<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%

  GlobalInput tG = (GlobalInput)session.getAttribute("GI");

  LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();

  tLOPRTManagerSchema.setPrtSeq(request.getParameter("PrtSeq"));
  
  String PrtSeq = tLOPRTManagerSchema.getPrtSeq();

  CError cError = new CError( );

  //后面要执行的动作：添加，修改，删除


	VData tVData = new VData();
  VData mResult = new VData();
    
  tVData.addElement(tG);
  tVData.addElement(tLOPRTManagerSchema);

	String strErrMsg = "";
	boolean Flag=true;
		
  OperatorNoticeF1PUI tOperatorNoticeF1PUI = new OperatorNoticeF1PUI();
	if(!tOperatorNoticeF1PUI.submitData(tVData,"CONFIRM")) {
		if( tOperatorNoticeF1PUI.mErrors.needDealError() ) {
			Flag=false;
			strErrMsg = tOperatorNoticeF1PUI.mErrors.getFirstError();
		} else {
			strErrMsg = "tOperatorNoticeF1PUI发生错误，但是没有提供详细的出错信息";
		}
%>
		<script language="javascript">
			alert('<%= strErrMsg %>');
			window.opener = null;
			window.close();
		</script>
<%
		return;
  }

  mResult = tOperatorNoticeF1PUI.getResult();
  
	XmlExport txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	
	if (txmlExport==null) {
	  System.out.println("null");
	}
	session.setAttribute("PrintNo",PrtSeq );
  session.setAttribute("PrintType","14" );
	session.setAttribute("PrintStream", txmlExport.getInputStream());
	System.out.println("put session value");
	response.sendRedirect("GetF1Print.jsp");
%>