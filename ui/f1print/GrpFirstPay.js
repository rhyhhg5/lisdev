//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var manageCom;
var SqlPDF;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		arrReturn = getQueryResult();
		PrtSeq = PolGrid.getRowColData(tSel-1,1);
		
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
		
		fmSave.PrtSeq.value = PrtSeq;
		fmSave.PolNo.value = arrReturn[0][1];
		fmSave.fmtransact.value = "CONFIRM";
		fmSave.submit();
		
	}
}
//zhoushujing于2007.07.05增加下面的函数
function printPolpdf()	
{
  var i = 0;  
  var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	else
	{
		PrtSeq = PolGrid.getRowColData(tChked[0],1);
		PrtNo = PolGrid.getRowColData(tChked[0],2);		
		fmSave.PrtSeq.value = PrtSeq;
		fmSave.PrtNo.value = PrtNo;	
		var contno = easyExecSql(" select distinct grpcontno from (select grpcontno,modifydate from lcgrpcont where PrtNo='"+PrtNo+"' order by modifydate fetch first rows only) as x");
		fmSave.fmtransact.value = "PRINT";
		fmSave.target = "fraSubmit";
		
	   var verifySql ="select 1  from lcgrppol a where a.prtno='"+PrtNo+"' and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G') ";
        var arrVerifyResult = easyExecSql(verifySql);       
        if(arrVerifyResult!=null &&arrVerifyResult[0][0]=="1"){
        fmSave.action = "../uw/PDFPrintSave.jsp?Code=ULI_57&OtherNo="+contno;
        }else{
        fmSave.action = "../uw/PDFPrintSave.jsp?Code=57&OtherNo="+contno;
        }
		fmSave.submit();
		
		
	}
}
//zhoushujing于2007.07.05增加下面的函数
function printPolpdfbat()
{
	if (PolGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length <= 1)
	{
		alert("请选择多条记录");
		return false;	
	}


	fmSave.fmtransact.value = "PRINT";
    fmSave.target = "fraSubmit";
	fmSave.action="./GrpFirstPayForBatPrt.jsp";
	fmSave.submit();

}
function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
		if(!verifyInput2())
	return false;

	var strManageComWhere = " ";
	if( fm.BranchGroup.value != '' ) {
		strManageComWhere += " AND LAAgent.AgentGroup IN" + 
												" ( SELECT AgentGroup FROM LABranchGroup WHERE BranchAttr LIKE '" +
												fm.BranchGroup.value + "%%') ";
	}
		var strSQL = "";
		
		if(fm.PayNoticeFlag.value=="1"){	
	strSQL = "SELECT LOPRTManager.PrtSeq, LCGrpCont.prtno,getUniteCode(LOPRTManager.AgentCode), LAAgent.AgentGroup,LABranchGroup.BranchAttr,LOPRTManager.ManageCom FROM LOPRTManager,LAAgent,LABranchGroup,LCGrpCont WHERE LOPRTManager.Code = 'ULI_57' " 
	+ "and LAAgent.AgentCode = LOPRTManager.AgentCode "
	+ "and LABranchGroup.AgentGroup = LAAgent.AgentGroup "
	+ "and LOPRTManager.OtherNo = LCGrpCont.Proposalgrpcontno "	
	+ "and LCGrpCont.uwflag in ('4','9') "
	+ getWherePart('LCGrpCont.prtno', 'prtno') 
	+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
	+ getWherePart('getUniteCode(LOPRTManager.AgentCode)','AgentCode')
	+ getWherePart('LABranchGroup.AgentGroup','AgentGroup')
	+ getWherePart('LAAgent.AgentGroup','AgentGroup')
	+ getWherePart('LOPRTManager.MakeDate','MakeDate')
	+ getWherePart('LOPRTManager.stateflag','StateFlag')
	+ " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'"
	+ strManageComWhere
	+" AND LOPRTManager.PrtType = '0'";
	
	}else if(fm.PayNoticeFlag.value=="2"){  
    strSQL = "SELECT LOPRTManager.PrtSeq, LCGrpCont.prtno,getUniteCode(LOPRTManager.AgentCode), LAAgent.AgentGroup,LABranchGroup.BranchAttr,LOPRTManager.ManageCom FROM LOPRTManager,LAAgent,LABranchGroup,LCGrpCont WHERE LOPRTManager.Code = '57' " 
    + "and LAAgent.AgentCode = LOPRTManager.AgentCode "
    + "and LABranchGroup.AgentGroup = LAAgent.AgentGroup "
    + "and LOPRTManager.OtherNo = LCGrpCont.Proposalgrpcontno " 
    + "and LCGrpCont.uwflag in ('4','9') "
    + "and LCGrpCont.CardFlag in ('4') "
    + getWherePart('LCGrpCont.prtno', 'prtno') 
    + getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
    + getWherePart('getUniteCode(LOPRTManager.AgentCode)','AgentCode')
    + getWherePart('LABranchGroup.AgentGroup','AgentGroup')
    + getWherePart('LAAgent.AgentGroup','AgentGroup')
    + getWherePart('LOPRTManager.MakeDate','MakeDate')
    + getWherePart('LOPRTManager.stateflag','StateFlag')
    + " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'"
    + strManageComWhere
    +" AND LOPRTManager.PrtType = '0'";
    
    }else{
	// 书写SQL语句
	strSQL = "SELECT LOPRTManager.PrtSeq, LCGrpCont.prtno,getUniteCode(LOPRTManager.AgentCode), LAAgent.AgentGroup,LABranchGroup.BranchAttr,LOPRTManager.ManageCom FROM LOPRTManager,LAAgent,LABranchGroup,LCGrpCont WHERE LOPRTManager.Code = '57' " 
	+ "and LAAgent.AgentCode = LOPRTManager.AgentCode "
	+ "and LABranchGroup.AgentGroup = LAAgent.AgentGroup "
	+ "and LOPRTManager.OtherNo = LCGrpCont.Proposalgrpcontno "	
	+ "and LCGrpCont.uwflag in ('4','9') "
    + "and (LCGrpCont.CardFlag is null or exists ( select 1 from ldcode where codetype = 'cardflag3' and code = LCGrpCont.CardFlag )) "
	+ getWherePart('LCGrpCont.prtno', 'prtno') 
	+ getWherePart('LOPRTManager.ManageCom', 'ManageCom', 'like')
	+ getWherePart('getUniteCode(LOPRTManager.AgentCode)','AgentCode')
	+ getWherePart('LABranchGroup.AgentGroup','AgentGroup')
	+ getWherePart('LAAgent.AgentGroup','AgentGroup')
	+ getWherePart('LOPRTManager.MakeDate','MakeDate')
	+ getWherePart('LOPRTManager.stateflag','StateFlag')
	+ " AND LOPRTManager.ManageCom LIKE '" + comcode + "%%'"
	+ strManageComWhere
	+" AND LOPRTManager.PrtType = '0'";
	}
	if(DegreeType!=null && DegreeType!="null"){
		strSQL +=" and LCGrpCont.CardFlag='0'";
	}
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  //alert(strSQL);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有要打印的首期交费通知书！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  SqlPDF=strSQL;
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  
  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  }
}


function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }
 if(cCodeName=="agentgroup2"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
 
}