//               该文件中包含客户端需要处理的函数和事件
var showInfo;

//识别用户

//对公客户新客户总数
function CalNewSumNum()
{
	var tValue0 = "select count(1) from lcgrpcont where cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + fm.ManageCom.value + "%'  with ur";
	var tBankValue0 = "select count(1) from lcgrpcont where cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + fm.ManageCom.value + "%' with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tBankValue0Result = easyExecSql(tBankValue0);
	var nValue0 = 0;
	var nBankValue0 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tBankValue0Result!= null && tBankValue0Result != "" && tBankValue0Result !="null"))
    {
    	nBankValue0 = parseInt(tBankValue0Result,10); 
    }
    fm.NewSumNum.value = nValue0+nBankValue0;

}

//对公客户其它情况总数
function CalSumNum()
{
	var tValue0 = "select count(t.cou) from (select endorsementno as cou from ljagetendorse where  feeoperationtype in ('CT','XT','WT') and grpcontno <> '00000000000000000000' and makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and managecom like '" + fm.ManageCom.value + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur";
	var tValue1 = "select count(t.cou) from (select otherno as cou from ljagetclaim where  makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and grpcontno <> '00000000000000000000' and managecom like '" + fm.ManageCom.value + "%' group by otherno having  sum(pay)  >= 10000) as t  with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.SumNum.value = nValue0+nValue1;

}

//对公新客户第三方识别
function CalNewOtherNum()
{
	var tValue0 = "select count(1) from lcgrpcont where cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and paymode in ('1','14','16') and prem >= 20000 and salechnl in ('03','10') and managecom like '" + fm.ManageCom.value + "%'  with ur";
	var tValue1 = "select count(1) from lcgrpcont where cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and paymode not in ('1','14','16') and prem >= 200000 and salechnl in ('03','10') and managecom like '" + fm.ManageCom.value + "%' with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.NewOtherNum.value = nValue0+nValue1;

}


//对公其它情况第三方识别
function CalOtherNum()
{
	var tValue0 = "select count(t.cou) from (select endorsementno as cou from ljagetendorse lendor where  feeoperationtype in ('CT','XT','WT') and grpcontno <> '00000000000000000000' and makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and exists (select 1 from lbpol where polno = lendor.polno and salechnl in ('03','10')) and managecom like '" + fm.ManageCom.value + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur";
	var tValue1 = "select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and grpcontno <> '00000000000000000000' and exists (select 1 from lcgrpcont where grpcontno = lclaim.grpcontno and salechnl in ('03','10') and managecom like '" + fm.ManageCom.value + "%' union all select 1 from lbgrpcont where grpcontno = lclaim.grpcontno and salechnl in ('03','10') ) and managecom like '" + fm.ManageCom.value + "%'  group by otherno having  sum(pay)  >= 10000) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.OtherNum.value = nValue0+nValue1;

}

//对公新客户受益人数
function CalNewNum()
{
//	var tValue0 = "select count(t.cou) from (select endorsementno as cou from ljagetendorse lendor where  feeoperationtype in ('CT','XT','WT') and grpcontno <> '00000000000000000000' and makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and exists (select 1 from lbpol where polno = lendor.polno and salechnl in ('03','10')) and managecom like '" + fm.ManageCom.value + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur";
//	var tValue1 = "select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and grpcontno <> '00000000000000000000' and exists (select 1 from lcgrpcont where grpcontno = lclaim.grpcontno and salechnl in ('03','10') and managecom like '" + fm.ManageCom.value + "%' union all select 1 from lbgrpcont where grpcontno = lclaim.grpcontno and salechnl in ('03','10') ) and managecom like '" + fm.ManageCom.value + "%'  group by otherno having  sum(pay)  >= 10000) as t with ur";
//	var tValue0Result = easyExecSql(tValue0);
//	var tValue1Result = easyExecSql(tValue1);
//	var nValue0 = 0;
//	var nValue1 = 0;
//    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
//     {
//        nValue0 = parseInt(tValue0Result,10); 
//     }
//    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
//    {
//    	nValue1 = parseInt(tValue1Result,10); 
//    }
    fm.NewNum.value = 0;

}

//对公其它情况受益人数
function CalNum()
{
//	var tValue0 = "select count(t.cou) from (select endorsementno as cou from ljagetendorse lendor where  feeoperationtype in ('CT','XT','WT') and grpcontno <> '00000000000000000000' and makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and exists (select 1 from lbpol where polno = lendor.polno and salechnl in ('03','10')) and managecom like '" + fm.ManageCom.value + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur";
//	var tValue1 = "select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and grpcontno <> '00000000000000000000' and exists (select 1 from lcgrpcont where grpcontno = lclaim.grpcontno and salechnl in ('03','10') and managecom like '" + fm.ManageCom.value + "%' union all select 1 from lbgrpcont where grpcontno = lclaim.grpcontno and salechnl in ('03','10') ) and managecom like '" + fm.ManageCom.value + "%'  group by otherno having  sum(pay)  >= 10000) as t with ur";
//	var tValue0Result = easyExecSql(tValue0);
//	var tValue1Result = easyExecSql(tValue1);
//	var nValue0 = 0;
//	var nValue1 = 0;
//    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
//     {
//        nValue0 = parseInt(tValue0Result,10); 
//     }
//    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
//    {
//    	nValue1 = parseInt(tValue1Result,10); 
//    }
    fm.Num.value = 0;

}

//对私新客户总数
function CalNewSumNum2()
{
	var tValue0 = "select count(1) from lccont where conttype = '1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and paymode in ('1','14','16') and prem >= 20000 and managecom like '" + fm.ManageCom.value + "%' with ur";
	var tValue1 = "select count(1) from lccont where conttype = '1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and paymode not in ('1','14','16') and prem >= 200000 and managecom like '" + fm.ManageCom.value + "%' with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.NewSumNum2.value = nValue0+nValue1;

}

//对私其它情况总数
function CalSumNum2()
{
	var tValue0 = "select count(t.cou) from (select endorsementno as cou from ljagetendorse where  feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and managecom like '" + fm.ManageCom.value + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur";
	var tValue1 = "select count(t.cou) from (select otherno as cou from ljagetclaim where  makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and grpcontno = '00000000000000000000' and managecom like '" + fm.ManageCom.value + "%' group by otherno having  sum(pay)  >= 10000 ) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.SumNum2.value = nValue0+nValue1;

}

//对私新客户第三方识别
function CalNewOtherNum2()
{
	var tValue0 = "select count(1) from lccont where conttype = '1'  and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and paymode in ('1','14','16') and prem >= 20000  and salechnl in ('03','10') and managecom like '" + fm.ManageCom.value + "%' with ur";
	var tValue1 = "select count(1) from lccont where conttype = '1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and paymode not in ('1','14','16') and prem >= 200000 and salechnl in ('03','10') and managecom like '" + fm.ManageCom.value + "%' with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.NewOtherNum2.value = nValue0+nValue1;

}

//对私其它情况第三方识别
function CalOtherNum2()
{
	var tValue0 = "select count(t.cou) from (select endorsementno as cou from ljagetendorse lendor where  feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and exists (select 1 from lbpol where polno = lendor.polno and salechnl in ('03','10')) and managecom like '" + fm.ManageCom.value + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur";
	var tValue1 = "select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and grpcontno = '00000000000000000000' and exists (select 1 from lccont where contno = lclaim.contno and salechnl in ('03','10') and managecom like '" + fm.ManageCom.value + "%' union all select 1 from lbcont where contno = lclaim.contno and salechnl in ('03','10') ) and managecom like '" + fm.ManageCom.value + "%'  group by otherno having  sum(pay)  >= 10000) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.OtherNum2.value = nValue0+nValue1;

}


//对私新客户居民
function CalNewResNum()
{
	var tValue0 = "select count(t.cou) from (select (lca.contno) as cou from lccont lcc ,LCAppnt lca where lcc.appntno = lca.AppntNo and lcc.contno = lca.contno and (lca.NativePlace = 'ML' or lca.NativePlace is null) and lcc.conttype = '1' and lcc.cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and lcc.paymode in ('1','14','16') and lcc.prem >= 20000 and lcc.managecom like '" + fm.ManageCom.value + "%' group by lca.contno) as t with ur";
	var tValue1 = "select count(t.cou) from (select (lca.contno) as cou from lccont lcc ,LCAppnt lca where lcc.appntno = lca.AppntNo and lcc.contno = lca.contno and (lca.NativePlace = 'ML' or lca.NativePlace is null) and lcc.conttype = '1' and lcc.cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and lcc.paymode not in ('1','14','16') and lcc.prem >= 200000 and lcc.managecom like '" + fm.ManageCom.value + "%' group by lca.contno) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.NewResNum.value = nValue0+nValue1;

}

//对私其它情况居民
function CalResNum()
{
	var tValue0 = "select count(t.cou) from (select endorsementno as cou from ljagetendorse lget where exists (select 1 from lcappnt lca where  lca.contno = lget.contno and lca.appntno = lget.appntno and (lca.NativePlace = 'ML' or lca.NativePlace is null) union all  select 1 from lbappnt lca where  lca.contno = lget.contno and lca.appntno = lget.appntno and (lca.NativePlace = 'ML' or lca.NativePlace is null))    and feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and managecom like '" + fm.ManageCom.value + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur";
	var tValue1 = "select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and grpcontno = '00000000000000000000'   and managecom like '" + fm.ManageCom.value + "%' and exists (select 1 from lcinsured lci where lci.contno = lclaim.contno and (lci.NativePlace = 'ML' or lci.NativePlace is null) union  select 1 from lbinsured lci where lci.contno = lclaim.contno and (lci.NativePlace = 'ML' or lci.NativePlace is null)) group by otherno having  sum(pay)  >= 10000) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.ResNum.value = nValue0+nValue1;

}

//对私新客户非居民
function CalNewNotResNum()
{
	var tValue0 = "select count(t.cou) from (select (lca.contno) as cou from lccont lcc ,LCAppnt lca where lcc.appntno = lca.AppntNo and lcc.contno = lca.contno and (lca.NativePlace <> 'ML' and lca.NativePlace is not null) and lcc.conttype = '1' and lcc.cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and lcc.paymode in ('1','14','16') and lcc.prem >= 20000 and lcc.managecom like '" + fm.ManageCom.value + "%' group by lca.contno) as t with ur";
	var tValue1 = "select count(t.cou) from (select (lca.contno) as cou from lccont lcc ,LCAppnt lca where lcc.appntno = lca.AppntNo and lcc.contno = lca.contno and (lca.NativePlace <> 'ML' and lca.NativePlace is not null) and lcc.conttype = '1' and lcc.cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'  and lcc.paymode not in ('1','14','16') and lcc.prem >= 200000 and lcc.managecom like '" + fm.ManageCom.value + "%' group by lca.contno) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.NewNotResNum.value = nValue0+nValue1;

}

//对私其它情况非居民
function CalNotResNum()
{
	var tValue0 = "select count(t.cou) from (select endorsementno as cou from ljagetendorse lget where exists (select 1 from lcappnt lca where  lca.contno = lget.contno and lca.appntno = lget.appntno and (lca.NativePlace <> 'ML' and lca.NativePlace is not null) union all  select 1 from lbappnt lca where  lca.contno = lget.contno and lca.appntno = lget.appntno and (lca.NativePlace <> 'ML' and lca.NativePlace is not null))    and feeoperationtype in ('CT','XT','WT') and grpcontno = '00000000000000000000' and makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and managecom like '" + fm.ManageCom.value + "%' group by endorsementno having sum(getmoney) <= -10000) as t with ur";
	var tValue1 = "select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and grpcontno = '00000000000000000000'   and managecom like '" + fm.ManageCom.value + "%' and exists (select 1 from lcinsured lci where lci.contno = lclaim.contno and (lci.NativePlace <> 'ML' and lci.NativePlace is not null) union  select 1 from lbinsured lci where lci.contno = lclaim.contno and (lci.NativePlace <> 'ML' or lci.NativePlace is not null)) group by otherno having  sum(pay)  >= 10000) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.NotResNum.value = nValue0+nValue1;

}

/************************************************华丽的分割线***********************************************/

//重新识别用户

//对公总数
function CalImpSum()
{
	var tValue0 = "select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lcgrpcont lcg where lcg.grpcontno=lja.grpcontno and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'   and managecom like '" + fm.ManageCom.value + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AE','CM','CC','PC') and lapp.edorstate='0' and lapp.confdate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' group by lja.edoracceptno)as t with ur";
	//var tValue1 = "select count(t.cou) from (select otherno as cou from ljagetclaim lclaim where  makedate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' and grpcontno = '00000000000000000000'   and managecom like '" + fm.ManageCom.value + "%' and exists (select 1 from lcinsured lci where lci.contno = lclaim.contno and (lci.NativePlace <> 'ML' and lci.NativePlace is not null) union  select 1 from lbinsured lci where lci.contno = lclaim.contno and (lci.NativePlace <> 'ML' or lci.NativePlace is not null)) group by otherno having  sum(pay)  >= 10000) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	//var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
//    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
//    {
//    	nValue1 = parseInt(tValue1Result,10); 
//    }
    fm.ImpSum.value = nValue0;

}

//对公涉及受益人
function CalImpNum()
{
//	var tValue0 = "select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno  and conttype='1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'   and managecom like '" + fm.ManageCom.value + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AE','CM','CC','PC') and lapp.edorstate='0' and lapp.confdate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' group by lja.edoracceptno) as t with ur";
//	var tValue1 = "select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'   and managecom like '" + fm.ManageCom.value + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype ='BC' and lapp.edorstate='0' and lapp.confdate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' group by lja.edoracceptno) as t with ur";
//	var tValue0Result = easyExecSql(tValue0);
//	var tValue1Result = easyExecSql(tValue1);
//	var nValue0 = 0;
//	var nValue1 = 0;
//    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
//     {
//        nValue0 = parseInt(tValue0Result,10); 
//     }
//    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
//    {
//    	nValue1 = parseInt(tValue1Result,10); 
//    }
    fm.ImpNum.value = 0;

}

//对私总数
function CalNewImpSum()
{
	var tValue0 = "select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno  and conttype='1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'   and managecom like '" + fm.ManageCom.value + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AE','CM','CC','PC') and lapp.edorstate='0' and lapp.confdate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' group by lja.edoracceptno) as t with ur";
	var tValue1 = "select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'   and managecom like '" + fm.ManageCom.value + "%')  and lja.edoracceptno=lapp.edoracceptno and lja.edortype ='BC' and lapp.edorstate='0' and lapp.confdate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' group by lja.edoracceptno) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.NewImpSum.value = nValue0+nValue1;

}

//对私居民
function CalNewImpResNum()
{
	var tValue0 = "select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'   and managecom like '" + fm.ManageCom.value + "%' and exists (select 1 from lcappnt where contno = lcg.contno and appntno = lcg.appntno and (NativePlace = 'ML' or NativePlace is null )) )  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AE','CM','CC','PC') and lapp.edorstate='0' and lapp.confdate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' group by lja.edoracceptno) as t with ur";
	var tValue1 = "select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'   and managecom like '" + fm.ManageCom.value + "%' and exists (select 1 from lcappnt where contno = lcg.contno and appntno = lcg.appntno and (NativePlace = 'ML' or NativePlace is null )) )  and lja.edoracceptno=lapp.edoracceptno and lja.edortype ='BC' and lapp.edorstate='0' and lapp.confdate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' group by lja.edoracceptno) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.NewImpResNum.value = nValue0+nValue1;

}

//对私非居民
function CalNewImpNotResNum()
{
	var tValue0 = "select sum(t.num) from (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'   and managecom like '" + fm.ManageCom.value + "%' and exists (select 1 from lcappnt where contno = lcg.contno and appntno = lcg.appntno and (NativePlace <> 'ML' and NativePlace is not null )) )  and lja.edoracceptno=lapp.edoracceptno and lja.edortype in ('AE','CM','CC','PC') and lapp.edorstate='0' and lapp.confdate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' group by lja.edoracceptno) as t with ur";
	var tValue1 = "select sum(t.num)from  (select COUNT(1) as num from lpedoritem lja,lpedorapp lapp where exists (select 1 from lccont lcg where lcg.contno=lja.contno and conttype='1' and cvalidate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "'   and managecom like '" + fm.ManageCom.value + "%' and exists (select 1 from lcappnt where contno = lcg.contno and appntno = lcg.appntno and (NativePlace <> 'ML' and NativePlace is not null )) )  and lja.edoracceptno=lapp.edoracceptno and lja.edortype ='BC' and lapp.edorstate='0' and lapp.confdate between '" + fm.StartDate.value + "' and '" + fm.EndDate.value + "' group by lja.edoracceptno) as t with ur";
	var tValue0Result = easyExecSql(tValue0);
	var tValue1Result = easyExecSql(tValue1);
	var nValue0 = 0;
	var nValue1 = 0;
    if ((tValue0Result!= null && tValue0Result != "" && tValue0Result !="null"))
     {
        nValue0 = parseInt(tValue0Result,10); 
     }
    if ((tValue1Result!= null && tValue1Result != "" && tValue1Result !="null"))
    {
    	nValue1 = parseInt(tValue1Result,10); 
    }
    fm.NewImpNotResNum.value = nValue0+nValue1;

}




