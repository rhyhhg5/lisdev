<%
//程序名称：LADimissionContInit.jsp
//程序功能：功能描述
//创建日期：　
//创建人  　
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ManageCom').value = "";
  
    
  }
  catch(ex) {
    alert("LADimissionContInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLACrossGrid();  
  }
  catch(re) {
    alert("LADimissionContInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LACrossGrid;
function initLACrossGrid() {                               
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";         		//列名
    iArray[1][1]="100px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构代码";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="原代理人";         		//列名
    iArray[3][1]="100px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="原代理人姓名";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;    
    
    LACrossGrid = new MulLineEnter( "fm" , "LACrossGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACrossGrid.mulLineCount = 0;   
    LACrossGrid.displayTitle = 1;
    LACrossGrid.hiddenPlus = 1;
    LACrossGrid.hiddenSubtraction = 1;
    LACrossGrid.canSel = 1;
    LACrossGrid.canChk = 0;
    LACrossGrid.selBoxEventFuncName = "showOne";
  
    LACrossGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACrossGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
</script>
