<%@include file="../common/jsp/UsrCheck.jsp"%>
<script>
 var tsql =" 1  and char(length(trim(comcode))) in (#4#,#8#) and comcode like #8644%# " ;
 var msql =" 1  and char(length(trim(comcode))) =#8# " ;
</script> 
<html>
<%
//程序名称：LABComRateCommSetInput.jsp
//程序功能：银代手续费比例录入界面
//创建时间：2008-01-24
//创建人  ：Huxl

%>
<%@page contentType="text/html;charset=GBK" %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LABankChargePrint2Init.jsp"%>
   <SCRIPT src="LABankChargePrint2.js"></SCRIPT>
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./LABankChargePrint2Save.jsp" method=post name=fm target="fraSubmit">
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
  <table class=common >
   	<TD  class= title>管理机构</TD>
    <TD  class= input>
      <Input class="codeno" name=ManageCom verify="管理机构|notnull"
        ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,1);"  
           onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,1);"
        ><input class=codename name=ManageComName readonly=true elementtype=nacessary>
      </TD>
   	<td  class= title> 
   	代理机构 
   	</td>
     <td  class= input>
          <input class='codeno' name=AgentCom id="AgentCom"  verify="代理机构|NOTNULL"
           ondblclick="return getAgentCom(this,AgentComName);"
           onkeyup="return getAgentCom(this,AgentComName);"
         ><input class=codename readonly  name=AgentComName  elementtype=nacessary > 
         </td>   
      </TR>            
   <tr class=common>		
          <td  class= title> 查询年月 </td>
          <TD  class= input> <Input class= "common" name=WageNo verify="查询年月|YYYYMM" elementtype=nacessary><font color="red" >('YYYYMM')例如：201101</font></TD>  		
  </tr>
  </table>  
  </div>
  <table>
   <tr>      
     	<td>
    		<input type=button value="下  载" class=cssButton onclick="printPay();"> 
    	</td>
   </tr>      
  </table>
 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




