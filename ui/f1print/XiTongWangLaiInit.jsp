<% 
//程序名称：XiTongWangLaiInit.jsp
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

function initInpBox()
{
  try
  {

	fm.all('StartDay').value = '';
	fm.all('EndDay').value = '';
  }
  catch(ex)
  {
    alert("TestInterfaceNotRunInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                            

function initForm()
{
  try
  {
    initInpBox();
//    initCodeQuery();
    initXiTongWangLaiKeMuDataGrid();  //初始化共享工作池
  }
  catch(re)
  {
    alert("TestInterfaceNotRunInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}



function initXiTongWangLaiKeMuDataGrid()
{                                  
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="38px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="日期";         		//列名
      iArray[2][1]="50px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="金额";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          
      XiTongWangLaiKeMuDataGrid = new MulLineEnter( "fm" , "XiTongWangLaiKeMuDataGrid" ); 
      //这些属性必须在loadMulLine前
      XiTongWangLaiKeMuDataGrid.mulLineCount = 10;   
      XiTongWangLaiKeMuDataGrid.displayTitle = 1;
      XiTongWangLaiKeMuDataGrid.locked = 1;
	  XiTongWangLaiKeMuDataGrid.canSel = 1;
      XiTongWangLaiKeMuDataGrid.canChk = 0;      
      XiTongWangLaiKeMuDataGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>
