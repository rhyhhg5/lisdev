<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var RePrintFlag = "<%=request.getParameter("RePrintFlag")%>";//记录是否重打
	var saleChnl = "";//销售渠道   zhangjianbao   2007-11-16
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="URGENoticeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="URGENoticeInit.jsp"%>
  <title>打印催办通知书 </title>   
</head>
<body  onload="initForm();" >
  <form action="./URGENoticeQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
   <table  class= common align=center>
      	<TR  class= common>
          <!--<TD  class= title>  投保单号码   </TD>
          <TD  class= input> --> <Input class= common name=ContNo  type="hidden">
          <TD  class= title>  印 刷 号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo verify="印刷号码|int"> </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
         </TR> 
         <TR  class= common>
         	<TD  class= title> 投保人</TD>
          <TD  class= input><Input class="common" name=AppntName verify="投保人|len<=20"></TD>
          <TD  class= title>  生效日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=CvaliDate verify="生效日期|date"> </TD>
          <TD  class= title> 下发日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=MakeDate verify="下发日期|date">   </TD>
         </TR> 
         <TR>
         	<TD  class= title> 申请日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=PolApplyDate verify="申请日期|date">   </TD>
          <TD  class= title> 通知书类型  </TD>
          <TD  class= input> <Input name=NoticeType class=codeNo CodeData="0|4^11|催办体检^19|催办承保计划变更^17|催办首期交费^18|催办问题件" ondblclick="showCodeListEx('NoticeType',[this,NoticeTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('NoticeType',[this,NoticeTypeName],[0,1]);" verify="操作类型|NOTNULL"><input class=codename name=NoticeTypeName readonly=true >  </TD>         
          <TD  class= title> 是否已打印</TD>
          <TD  class= input><Input class="codeNo" name=StateFlag CodeData="0|4^0|未打印^1|已打印^9|所有" ondblclick="showCodeListEx('StateFlag',[this,StateFlagName],[0,1]);" onkeyup="return showCodeListKey('StateFlag',[this,StateFlagName],[0,1]);"><input class=codename name=StateFlagName readonly=true ><input class=readonly name=StateFlagName2 value="未打印" readonly=true >  </TD>
         </TR>
       <tr>
         	<TD  class= title> 销售渠道 </TD>
          <TD  class= input><Input class="codeNo" name=saleChannel ondblclick="return showCodeList('statuskind',[this,saleChannelName],[0,1]);" onkeyup="return showCodeListKey('statuskind',[this,saleChannelName],[0,1]);"><input class=codename name=saleChannelName readonly=true ></TD>
          <TD  class= title> 营销机构 </TD>
          <TD  class= input><Input class="codeNo" name=branchAttr readonly ondblclick="beforeCodeSelect();return showCodeList('branchattrandagentgroup',[this,branchAttrName,agentGroup],[0,1,2],null,saleChnl,'saleChannel',1,200);"><input class=codename name=branchAttrName readonly=true ></TD>
       </tr>
    </table>
  	<input type=hidden name="agentGroup">
  	<input type=hidden name="condition"> 
    <INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();"> 
  </form>
  <form action="./URGENoticeSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 催办信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
			<INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<p>
      <INPUT VALUE="打印催办通知书(vts)" class= CssButton TYPE=button onclick="printPol();"> 
      <INPUT VALUE="打印催办通知书(pdf)" class= CssButton TYPE=hidden onclick="printPolpdf();"> 
      <INPUT VALUE="打印催办通知书(pdf)" class= CssButton TYPE=button onclick="printPolpdfNew();"> 
      <INPUT VALUE="批打催办通知书(pdf)" class= CssButton TYPE=button onclick="printPolpdfbat();"> 
  	</p>  	
  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="PrtSeq" name="PrtSeq"> 
  	<input type=hidden id="CodeType" name="CodeType">  
  	<input type=hidden id="PrtNo" name="PrtNo"> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
      	<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form id=printform target="printfrm" method = post action="">
      		<input type=hidden name=filename value=""/>
  		</form>
</body>
</html> 
