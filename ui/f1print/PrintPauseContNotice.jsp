<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PrintPauseContNotice.jsp 
//程序功能：打印失效中止的保单
//创建日期：2008-6-25 
//创建人  ：张 彦梅
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  boolean operFlag = true; 
    
  String contNo = request.getParameter("contNo"); 
  String manageCom = request.getParameter("ManageCom");
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("ContNo", contNo);

  tTransferData.setNameAndValue("ManageCom", manageCom);
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  System.out.println(tG.Operator);
  
  VData data = new VData();
  data.add(tG);
  data.add(tTransferData);
   PrintPauseContNoticeUI tPrintPauseContNoticeUI = new PrintPauseContNoticeUI();
  XmlExport txmlExport = tPrintPauseContNoticeUI.getXmlExport(data, "");  //得到打印格式的document
  System.out.println("huxl"+txmlExport);  
  if(txmlExport == null)
  {
    operFlag = false;
            
  }

	if (operFlag==true)
	{
	  ExeSQL tExeSQL = new ExeSQL();
    //获取临时文件名
    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
    String strFilePath = tExeSQL.getOneValue(strSql);
    String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
    //获取存放临时文件的路径
    //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
    //String strRealPath = tExeSQL.getOneValue(strSql);
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strVFPathName = strRealPath + "//" +strVFFileName;
    
    CombineVts tcombineVts = null;	
    
    String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
  	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
  
  	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
  	tcombineVts.output(dataStream);
      	
  	//把dataStream存储到磁盘文件
  	//System.out.println("存储文件到"+strVFPathName);
  	AccessVtsFile.saveToFile(dataStream,strVFPathName);
    System.out.println("==> Write VTS file to disk ");
          
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
	}
	else
	{
    	
%>
<html>
<script language="javascript">	
	
	top.close();
</script>
</html>
<%
  	}
%>