<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAChannelWeekReport.jsp
//程序功能：
//创建日期：2008-04-14 18:03:35
//创建人  ：sy程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
  <%@page import="java.io.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAChannelWeekReportBL tLAChannelWeekReportBL   = new LAChannelWeekReportBL();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  boolean operFlag=true;
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  
  String tLevel = request.getParameter("Level");
  String tRiskCode = request.getParameter("RiskCode");
  String tStartDate = request.getParameter("StartDate");
  String tEndDate = request.getParameter("EndDate");
  String tPayIntv = request.getParameter("PayIntv");
  String tPayYears = request.getParameter("PayYears");
  String tPremType= request.getParameter("PremType");
  String tBranchType= request.getParameter("BranchType");
  String tBranchType2= request.getParameter("BranchType2");
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("Level",tLevel); 
  tTransferData.setNameAndValue("RiskCode",tRiskCode); 
  tTransferData.setNameAndValue("StartDate",tStartDate); 
  tTransferData.setNameAndValue("EndDate",tEndDate); 
  tTransferData.setNameAndValue("PayIntv",tPayIntv); 
  tTransferData.setNameAndValue("PayYears",tPayYears); 
  tTransferData.setNameAndValue("PremType",tPremType); 
  tTransferData.setNameAndValue("BranchType",tBranchType); 
  tTransferData.setNameAndValue("BranchType2",tBranchType2); 
  
  XmlExport txmlExport = new XmlExport();
  VData tVData = new VData();
 	VData mResult = new VData();
  try
  {
  	// 准备传输数据 VData
		tVData.add(tTransferData);
  	tVData.add(tG);
  	 //调用打印的类
    if (!tLAChannelWeekReportBL.submitData(tVData,"PRINT")){
	      operFlag=false;
	      Content=tLAChannelWeekReportBL.mErrors.getFirstError().toString();    
    }else{
    		mResult = tLAChannelWeekReportBL.getResult();
	      txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	      if(txmlExport==null){
	      		operFlag=false;
	      		Content="没有得到要显示的数据文件";
	      }
    }
	}catch(Exception ex){
		Content = "打印失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
  
	ExeSQL tExeSQL = new ExeSQL();
	//获取临时文件名
	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	String strFilePath = tExeSQL.getOneValue(strSql);
	String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
	//获取存放临时文件的路径
	String strRealPath = application.getRealPath("/").replace('\\','/');
	String strVFPathName = strRealPath +"/"+ strVFFileName;
	CombineVts tcombineVts = null;
	
	if (operFlag==true){
		//合并VTS文件
		String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);
		//把dataStream存储到磁盘文件
		AccessVtsFile.saveToFile(dataStream,strVFPathName);
		System.out.println("==> Write VTS file to disk ");		
		System.out.println("===strVFFileName : "+strVFFileName);
		//本来打算采用get方式来传递文件路径
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="+strVFPathName);
	}else{
		FlagStr = "Fail";
	%>
<html>
<script language="javascript">
	alert("<%=Content%>");
	top.opener.focus();
	top.close();		
</script>
</html>
<%
}
%>