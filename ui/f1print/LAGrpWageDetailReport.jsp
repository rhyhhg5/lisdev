<%
//程序名称：LAGrpWageDetailReport.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
ReportUI
</title>
<head>
</head>
<body>
<%
String flag = "0";
String FlagStr = "";
String Content = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String tManageCom=tG.ManageCom;
String ManageCom = request.getParameter("ManageCom");
String sql="select name from ldcom where comcode='"+ManageCom+"'";
ExeSQL tExeSQL = new ExeSQL();
String tName = tExeSQL.getOneValue(sql);
 if (!ManageCom.equals("86")){
     tName = tName + "分公司";
 }

//设置模板名称

String FileName ="LAGrpWageDetail";

String TWageYear = request.getParameter("WageYear");
String TWageMonth = request.getParameter("WageMonth");
String tBranchAttr=  request.getParameter("BranchAttr");
if ("".equals(tBranchAttr) || tBranchAttr==null)
{
  tBranchAttr="86";
}

JRptList t_Rpt = new JRptList();
String tOutFileName = "";
if(flag.equals("0"))
{
	t_Rpt.m_NeedProcess = false;
	t_Rpt.m_Need_Preview = false;
	t_Rpt.mNeedExcel = true;
	String YYMMDD = "";
	YYMMDD = TWageYear + "年"+TWageMonth + "月";
	t_Rpt.AddVar("YYMMDD", YYMMDD);
	if(TWageMonth.length()==1) TWageMonth="0"+TWageMonth;
	TWageYear=TWageYear+TWageMonth;
	String CurrentDate = PubFun.getCurrentDate();
	CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
	
	t_Rpt.AddVar("tName", tName);
	t_Rpt.AddVar("TWageYear", TWageYear);
	t_Rpt.AddVar("TWageMonth", TWageMonth);
	t_Rpt.AddVar("tBranchAttr", tBranchAttr);
	t_Rpt.AddVar("ManageCom",ManageCom);
	
	YYMMDD = "";
	YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
	      + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
	       + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
	       
	t_Rpt.AddVar("MakeDate", YYMMDD);
	t_Rpt.Prt_RptList(pageContext,FileName);
	tOutFileName = t_Rpt.mOutWebReportURL;
	String strVFFileName = FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
	String strRealPath = application.getRealPath("/web/Generated").replace('\\','/');
	String strVFPathName = strRealPath +"/"+ strVFFileName;
	System.out.println("strVFPathName : "+ strVFPathName);
	System.out.println("=======Finshed in JSP===========");
	System.out.println(tOutFileName);
	
	response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+"&RealPath="+strVFPathName);
}
%>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{

  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >
