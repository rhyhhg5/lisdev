/*
  //创建人：张彦梅
*/
var showInfo;

var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();
var sql="";
var turnPage2 = new turnPageClass();


function printListing()
{
  if(BillGrid.mulLineCount == 0)
  {
    alert("请先查询");
    return false;
  }
  
  if( fm.querySql.value == "" )
  {
  	alert("数据错误，请重新查询");
    return false;
  }  

  fm.action="./InvailidContsListPrint.jsp";
  fm.target="f1print";
  fm.fmtransact.value="PRINT";
  fm.submit(); //提交
}


function checkDate()
{
	if(!isDate(fm.StartDate.value))
	{
			alert("起始日是必录且应为日期格式yyyy-mm-dd")
			return false;
	}
  
  if(!isDate(fm.EndDate.value))
 	{
 		alert("起始日是必录且应为日期格式yyyy-mm-dd")
 		return false;
 	}
 	
	return true;
}

//根据起始日期进行查询出要该日期范围内的成功件数
function showSerialNo()
{
	//if(!checkDate())
	//{
	//	return false;
	//}
  var strSQL = "";
    strSQL =  " select LCContState.startDate , LCCont.manageCom,Lccont.agentGroup,LCCont.ContNo,Lccont.appntName,"
            + " LCAddress.mobile,LCAddress.postalAddress ,LCPol.prem ,LCCont.payintv,LCCont.PayMode,LCCont.agentCode,LAAGent.mobile ,LAAGent.name  "
            + " from LCCont,LCAppnt,LCContState,LCPol,LCAddress,LAAGent  "
            + " where LCCont.StateFlag='2'"
            + " and LCContState.StateType='Available' "
            + " and LCContState.State='1'"
            + " and LCContState.StateReason='02' "
            + " and LCContState.GrpContNo='000000'"
            + " and LCContState.polno='000000'"
            + " and LCContState.contNo=lccont.contNo"
            + " and LCPol.polNo=LCPol.mainPolno"
            + " and lcpol.contno=lccont.contNo"
            + " and LCAddress.customerNo = Lccont.appntNo"
            + " and LCAddress.addressNo = LCAppnt.addressNo "
            + " and LAAGent.agentCode = LCCont.agentCode"
            + " and lcappnt.contno=lccont.contNo"
            + " and lccont.contNo in (select distinct Lcpol.contno from lmriskapp,LCPOL where lcpol.riskcode=lmriskapp.riskcode  and LmriskApp.RiskType4='4' )"                        
            + " and lccont.ManageCom like '" + fm.all('ManageCom').value + "%%' "
            + getWherePart('LCContState.startDate','StartDate','>=')
            + getWherePart('LCContState.startDate','EndDate','<=')
            + getWherePart('LCCont.AgentCode','AgentCode')
            + getWherePart('LCCont.ContNo','ContNo')
            + getWherePart('LCCont.AppntName','ApntNane');    
	turnPage.queryModal(strSQL, BillGrid);
	
	
	if(BillGrid.mulLineCount == 0)
	{
	  alert("没有查询到符合条件的数据");
	  return false;
	}
	
	fm.querySql.value = strSQL;
	fm.queryStartDate.value = fm.all('StartDate').value ; 
	fm.queryEndDate.value = fm.all('EndDate').value ; 
	
	//showCodeNameManageCom();
	//showCodeNameAgentGroup();
	return true;
}

 //将管理机构编码显示为下面的格式汉字：二级机构名称——三级机构名称
function showCodeNameManageCom()
{
  for(var i = 0; i < BillGrid.mulLineCount; i++)
	{
	  var manageCom = BillGrid.getRowColDataByName(i, "ManageCom");
	  var sqlTemp = "select name from LDCom ";
	  
	  var sql3 = sqlTemp + "where comCode = '" + manageCom + "' ";  //查询3级机构
	  var manageComName3 = easyExecSql(sql3);
	  
	  var sql2 = sqlTemp + "where comCode = '" + manageCom.substring(0, 4) + "' ";  //查询2级机构
	  var manageComName2 = easyExecSql(sql2);
	  if(manageComName2 == null || manageComName2 == "null")
	  {
	    manageComName2 = "";
	  }
	  
	  BillGrid.setRowColDataByName(i, "ManageCom", (manageComName2 + " " + manageComName3));
	}
}

//将营销部门编码显示为下面格式汉字：营业部名称（营销服务部）－营业区名称－营业处名称
//若团单只有营业部
function showCodeNameAgentGroup()
{
  for(var i = 0; i < BillGrid.mulLineCount; i++)
	{
	  var agentGroup = BillGrid.getRowColDataByName(i, "AgentGroup");
	  
	  var agentGroupName3 = "";  //查询营业处（团单为营业部）
	  var agentGroupName2 = "";  //查询营业区
	  var agentGroupName1 = "";  //查询营业部
	  
	  var sqlTemp = "select Name, upBranch from LABranchGroup ";
	  
	  //查询营业处（团单为营业部）
	  var sql3 = sqlTemp + "where agentGroup = '" + agentGroup + "' ";  
	  var rs = easyExecSql(sql3);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    agentGroupName3 = rs[0][0];
	    var upBranch = rs[0][1];
	    
	    //查询营业区
	    var sql2 = sqlTemp + "where agentGroup = '" + upBranch + "' ";  
	    rs = easyExecSql(sql2);
	    if(rs && rs[0][0] != "" && rs[0][0] != "null")
  	  {
  	    agentGroupName2 = rs[0][0];
  	    upBranch = rs[0][1];
  	    
  	    //查询营业部
  	    var sql1 = sqlTemp + "where agentGroup = '" + upBranch + "' ";  
  	    rs = easyExecSql(sql1);
  	    if(rs && rs[0][0] != "" && rs[0][0] != "null")
    	  {
    	    agentGroupName1 = rs[0][0];
    	  }
  	  }
	  }
	  
	  BillGrid.setRowColDataByName(i, "AgentGroup", 
	    (agentGroupName1 + " " + agentGroupName2 + " " + agentGroupName3));
	}
}

//根据选中的批次号、银行编码、管理机构进行查询并且执行打印功能；





//选择打印通知书
function printNotice()
{
  if(BillGrid.mulLineCount == 0)
  {
    alert("请先查询");
    return false;
  }

  var count = 0 ; 
  var contNo = "";
  var manageCom =""; 
  
  for (i = 0; i < BillGrid.mulLineCount; i++)
  {
	if (BillGrid.getChkNo(i)) 
	{
		count = count + 1;	
		manageCom =BillGrid.getRowColDataByName(i, "ManageCom");
		contNo = BillGrid.getRowColDataByName(i, "ContNo");
	}
  }
	
  if( count == 0 )
  {
  	alert("请选择一条保单");
	return false;
  }
  else if( count > 1 )
  {
  	alert("不能同时打印多张通知书，请选择一条保单");
  	return false
  }
  else
  {
  	var strUrl="./PrintPauseContNotice.jsp?contNo=" +contNo+"&ManageCom="+manageCom;
  	fm.action =strUrl ;
  	fm.target = "_blank";
  	fm.submit();
  }
}

