
<%
//程序名称：LAChannelInput.jsp
//程序功能：渠道报表
//创建日期：2007-11-20
//创建人  ：sgh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Managecom2 = tG.ManageCom;
	System.out.println("tManageCom" + Managecom2);
%>
<script>
 var strsql =" 1 and  comcode Like #"+'<%=Managecom2%>'+"%# " ;

</script>

<html>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="./LAChnlPremInput.js"></SCRIPT>

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LAChnlPremInit.jsp"%>
<title>渠道保费明细报表</title>
</head>
<body onload="initForm();initElementtype();">
<form action="./LAChnlPremReport.jsp" method=post name=fm
	target="fraSubmit">
<table>
	<tr>
		<td><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divAgent1);"></td>
		<td class=titleImg>渠道保费明细统计条件</td>
	</tr>
</table>


<Div id="divAgent1" style="display: ''">
<Table class=common>
	<TR class=common>
		<TD class=title>管理机构</TD>
		<TD class=input><Input class="codeno" name=ManageCom
			verify="管理机构|NOTNULL"
			ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,strsql,null,1);"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,strsql,null,1);"><input
			class=codename name=ManageComName readonly=true elementtype=nacessary>
		</TD>
		<td class=title>级别</td>
		<td class=input><input name=BankType2 class=codeno
			verify="级别|code:BankType2"
			ondblClick="showCodeList('BankType2',[this,BankType2Name],[0,1])"
			onkeyup="showCodeListKey('BankType2',[this,BankType2Name],[0,1])"><Input
			class=codename name=BankType2Name readOnly></td>
		<td class=title>银行机构</td>
		<td class=input><input class=codeno name=AgentCom verify="银行机构编码"
			ondblclick="return getAgentCom(this,AgentComName);"
			onkeyup="return getAgentCom(this,AgentComName);"><input
			class=codename readonly name=AgentComName></td>
	</TR>
	<TR class=common>
		<td class=title>险种</td>
		<TD class=input><input class="codeno" name=RiskCode
			ondblclick="return showCodeList('allbankriskcode',[this,RiskName],[0,1],null,null,null,null,300);"
			onkeyup="return showCodeListKey('allbankriskcode',[this,RiskName],[0,1],null,null,null,null,300);"><Input
			name=RiskName readOnly=true class=codename></TD>
		<TD class=title>业务员代码</TD>
		<TD class=input><Input class=codeno name=GroupAgentCode
			ondblclick="return getSiteManager(this,AgentName);"
			onkeyup="return getSiteManager(this,AgentName);"><Input
			class=codename readonly name=AgentName onchange = "return checkAgentCode()"></TD>
		<TD class=title>保单状态</TD>
		<TD class=input><Input class="codeno" name=State verify="保单状态"
			CodeData="0|^0|有效|^1|撤保"
			ondblclick="return showCodeListEx('State',[this,StateName],[0,1]);"
			onkeyup="return showCodeListKeyEx('State',[this,StateName],[0,1]);"><input
			class=codename name=StateName readonly=true></TD>
	</TR>
	<TR class=common>
		<TD class=title>统计起期</TD>
		<TD class=input><Input class="coolDatePicker" dateFormat="short"
			name=StartDate verify="统计起期"></TD>
		<TD class=title>统计止期</TD>
		<TD class=input><Input class="coolDatePicker" dateFormat="short"
			name=EndDate verify="统计止期"></TD>
		<TD class=title>缴费方式</TD>
		<td class=input><Input class="codeno" name=PayYears
			CodeData="0|^0|趸缴|^1|期缴"
			ondblclick="return showCodeListEx('PayYears',[this,PayYearsName],[0,1]);"
			onkeyup="return showCodeListKeyEx('PayYears',[this,PayYearsName],[0,1]);"><Input
			class="codename" name=PayYearsName readonly=true></td>
	</TR>
	<tr>
		<TD class=title>回执回销状态</TD>
		<TD class=input><Input class="codeno" name=GetPolState
			verify="回执回销状态" CodeData="0|^0|已回执回销|^1|未回执回销"
			ondblclick="return showCodeListEx('GetPolState',[this,GetPolStateName],[0,1]);"
			onkeyup="return showCodeListKeyEx('GetPolState',[this,GetPolStateName],[0,1]);"><input
			class=codename name=GetPolStateName readonly=true></TD>
		<TD class=title>收费性质</TD>
		<TD class=input><Input class="codeno" name=GetState verify="收费性质"
			CodeData="0|^0|新单|^1|续期|^2|续保"
			ondblclick="return showCodeListEx('GetState',[this,GetStateName],[0,1]);"
			onkeyup="return showCodeListKeyEx('GetState',[this,GetStateName],[0,1]);"><input
			class=codename name=GetStateName readonly=true></TD>
		<TD class=title>保单年度</TD>
		<TD class=input><Input class="codeno" name=PayYear verify="保单年度"
			CodeData="0|^1|第一年|^2|第二年|^3|第三年|^4|第四年|^5|第五年|^6|非第一年新单"
			ondblclick="return showCodeListEx('PayYear',[this,PayYearName],[0,1]);"
			onkeyup="return showCodeListKeyEx('PayYear',[this,PayYearName],[0,1]);"><input
			class=codename name=PayYearName readonly=true></TD>
	</tr>
	<tr class=common>
		<TD class=title>是否交叉销售</TD>
		<td class=title><Input class="codeno" name=saleFlag
			verify="是否交叉销售" CodeData="0|^0|否|^1|是"
			ondblclick="return showCodeListEx('saleFlag',[this,saleFlagName],[0,1]);"
			onkeyup="return showCodeListKeyEx('saleFlag',[this,saleFlagName],[0,1]);"><input
			class=codename name=saleFlagName readonly=true></td>
		<TD  class= title>投保人联系方式</TD>
        <TD  class= input><Input class= "common"  name=phoneNumber></TD>
        <TD  class= title>投保人年龄</TD>
        <TD  class= input><Input class= "common"  name=age></TD>
	</tr>
	<tr class=common>
		<TD class=title>是否自助终端出单</TD>
		<td class=title><Input class="codeno" name=outFlag
			verify="是否自助终端出单" CodeData="0|^0|否|^1|是"
			ondblclick="return showCodeListEx('outFlag',[this,outFlagName],[0,1]);"
			onkeyup="return showCodeListKeyEx('outFlag',[this,outFlagName],[0,1]);"><input
			class=codename name=outFlagName readonly=true></td>			
	</tr>
</Table>
</Div>
<input type=hidden name=Managecom2 value='<%=Managecom2%>'> <input
	type=hidden name=Bank value=''>
	<input type=hidden name=AgentCode value=''>
	 <INPUT VALUE="打  印"
	class=cssbutton TYPE=button onclick="submitForm();"> <span
	id="spanCode" style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
