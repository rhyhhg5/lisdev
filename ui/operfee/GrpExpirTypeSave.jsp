<%
//程序名称：GrpExpirTypeSave.jsp
//程序功能：
//创建日期：2008-11-18
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  String flag = "";
  String content = "";
  String queryType = request.getParameter("queryType");
  System.out.println("QueryType：："+queryType);
  String grpContNo = request.getParameter("TempGrpContNo");
  System.out.println("GrpContNo：："+grpContNo);
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("queryType", queryType);
  tTransferData.setNameAndValue("grpContNo", grpContNo);
  VData tVData = new VData();
  tVData.add((GlobalInput)session.getValue("GI"));
  tVData.add(tTransferData);
  GrpExpirTypeSaveBL tGrpExpirTypeSaveBL = new GrpExpirTypeSaveBL();
  if(!tGrpExpirTypeSaveBL.submitData(tVData, ""))
  {
    flag = "Fail";
    content = "给付方式录入处理失败";
  }
  else
  {
    flag = "Succ";
    content = "给付方式录入处理成功";
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSaveType("<%=flag%>", "<%=content%>");
</script>
</html>