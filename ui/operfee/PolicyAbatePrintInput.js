/*
  //创建人：fuxin
*/
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var orderSql = "";
var sql="";


function checkDate()
{
	if(!isDate(fm.StartDate.value))
	{
			alert("起始日是必录且应为日期格式yyyy-mm-dd")
			return false;
	}
  
  if(!isDate(fm.EndDate.value))
 	{
 		alert("起始日是必录且应为日期格式yyyy-mm-dd")
 		return false;
 	}
 	
	return true;
}
function displayQueryResult(strResult) 
{
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置

  strResult = Conversion(strResult);
  var filterArray          = new Array(0,12,1);

  //保存查询结果字符串
  turnPage.strQueryResult  = strResult;

  //使用模拟数据源
  turnPage.useSimulation   = 1;

  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);

  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  //alert(turnPage.arrDataCacheSet);


  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BillGrid;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) 
  {
    try 
    { 
      window.divPage.style.display = ""; 
    } catch(ex) 
    { }
  } 
  else 
  {
    try 
    { 
      window.divPage.style.display = "none"; 
    } catch(ex) 
    { }
  }

  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;

}


function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;

    showInfo.close();
    if (FlagStr == "Fail" )
    {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    }
    else
    {
    //	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		//	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    	showDiv(inputButton,"false");

    	//执行下一步操作
    }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}

//根据起始日期进行查询出要该日期范围内的成功件数
function showSerialNo()
{
  var manageComCheck = fm.ManageCom.value;
  if(manageComCheck.indexOf(ComCode) < 0)
  {
    alert("您只能操作本机构及以下机构的保单");
    return false;
  }
 
	if(!checkDate())
	{
		return false;
	}
	
	
  var strSQL = "";
  var strSQL11 = "";
  if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
	 strSQL11 = " and a.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
  }
    strSQL =" select a.makedate,(select name from ldcom where b.managecom = comcode),(select name from labranchgroup where b.agentgroup = agentgroup),b.contNo,b.appntName "
					 +" ,(select mobile from LCAddress where  customerNo = b.appntNo and addressNo = d.addressNo) "
					 +" ,(select postalAddress from LCAddress where customerNo = b.appntNo and addressNo = d.addressNo) "
					 +" ,(select getnoticeno from ljspayb where otherno = b.contno and getnoticeno=standbyFlag3) ,b.prem "
					 //+" ,(select sumDuePayMoney from ljspayb where otherno = b.contno and getnoticeno=standbyFlag3 ) "
					 +" ,varchar(b.payToDate) "
					 +" ,(select payDate from ljspayb where otherno = b.contno and getnoticeno=standbyFlag3) "
					 +" ,(select codeName from LDCode where codeType = 'paymode' and code = b.payMode), a.prtSeq,a.standbyFlag1 "
					 +" ,(select codeName from LDCode where codeType = 'pausecode' and code = a.code) "
					 +" , getUniteCode(b.agentCode),(select mobile from LAAGent where agentCode = b.agentCode) "
					 +" ,(select name from LAAGent where agentCode = b.agentCode) "
					 +" from LOPRTManager a, LCCont b, LCAppnt d "
					 +" where a.otherNo = b.ContNo "
					 +" and b.contNo = d.contNo and a.otherno = d.contno "
					 +" and b.ManageCom like '"+fm.all('ManageCom').value+"%'"
					 + getWherePart( 'a.MakeDate ','StartDate','>=') 
					 + getWherePart( 'a.MakeDate ','EndDate','<=') 
					 + getWherePart( 'b.PayToDate','StartPayToDate','>=') 
					 + getWherePart( 'b.PayToDate','EndPayToDate','<=')
					 + getWherePart( 'b.contNo ','ContNo') 
					// + getWherePart( 'b.AgentCode ','AgentCode') 
					 + strSQL11
					 + getWherePart( 'a.StateFlag','PrintFlag') 
					 +" AND a.Code = '42' "
					 +" group by b.agentGroup, b.agentCode,b.manageCom,b.contNo,b.appntno,b.appntName,d.addressNo,b.payToDate, a.prtSeq,a.standbyFlag1,a.makedate,standbyFlag3,paymode,code,b.prem "
  				 ;
	sql=strSQL;
	
  initBillGrid();
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, BillGrid);
	
	if(BillGrid.mulLineCount == 0)
	{
	  alert("没有查询到符合条件的数据");
	  return false;
	}
	
	//showCodeNameManageCom();
	//showCodeNameAgentGroup();
}

 //将管理机构编码显示为下面的格式汉字：二级机构名称——三级机构名称
function showCodeNameManageCom()
{
  for(var i = 0; i < BillGrid.mulLineCount; i++)
	{
	  var manageCom = BillGrid.getRowColDataByName(i, "ManageCom");
	  var sqlTemp = "select name from LDCom ";
	  
	  var sql3 = sqlTemp + "where comCode = '" + manageCom + "' ";  //查询3级机构
	  var manageComName3 = easyExecSql(sql3);
	  
	  var sql2 = sqlTemp + "where comCode = '" + manageCom.substring(0, 4) + "' ";  //查询2级机构
	  var manageComName2 = easyExecSql(sql2);
	  if(manageComName2 == null || manageComName2 == "null")
	  {
	    manageComName2 = "";
	  }
	  
	  BillGrid.setRowColDataByName(i, "ManageCom", (manageComName2 + " " + manageComName3));
	}
}

//将营销部门编码显示为下面格式汉字：营业部名称（营销服务部）－营业区名称－营业处名称
//若团单只有营业部
function showCodeNameAgentGroup()
{
  for(var i = 0; i < BillGrid.mulLineCount; i++)
	{
	  var agentGroup = BillGrid.getRowColDataByName(i, "AgentGroup");
	  
	  var agentGroupName3 = "";  //查询营业处（团单为营业部）
	  var agentGroupName2 = "";  //查询营业区
	  var agentGroupName1 = "";  //查询营业部
	  
	  var sqlTemp = "select Name, upBranch from LABranchGroup ";
	  
	  //查询营业处（团单为营业部）
	  var sql3 = sqlTemp + "where agentGroup = '" + agentGroup + "' ";  
	  var rs = easyExecSql(sql3);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    agentGroupName3 = rs[0][0];
	    var upBranch = rs[0][1];
	    
	    //查询营业区
	    var sql2 = sqlTemp + "where agentGroup = '" + upBranch + "' ";  
	    rs = easyExecSql(sql2);
	    if(rs && rs[0][0] != "" && rs[0][0] != "null")
  	  {
  	    agentGroupName2 = rs[0][0];
  	    upBranch = rs[0][1];
  	    
  	    //查询营业部
  	    var sql1 = sqlTemp + "where agentGroup = '" + upBranch + "' ";  
  	    rs = easyExecSql(sql1);
  	    if(rs && rs[0][0] != "" && rs[0][0] != "null")
    	  {
    	    agentGroupName1 = rs[0][0];
    	  }
  	  }
	  }
	  
	  BillGrid.setRowColDataByName(i, "AgentGroup", 
	    (agentGroupName1 + " " + agentGroupName2 + " " + agentGroupName3));
	}
}

//根据选中的批次号、银行编码、管理机构进行查询并且执行打印功能；
function printBill()
{
	//queryShort();
	
	if (BillGrid.mulLineCount == 0)
	{
		alert("没有需要打印的清单");
		return false;
	}else
			fm.all("sql").value=sql;
			fm.action = "PolicyAbatePrintListPrint.jsp";
			fm.target = "_blank";
			fm.submit();
}



//选择打印通知书
function printNotice()
{

	if (BillGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < BillGrid.mulLineCount; i++)
	{
		if(BillGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印！");
		return false;	
	}
	var tContNo=BillGrid.getRowColData(tChked[0],4);
	fm.action = "../uw/PDFPrintSave.jsp?Code=42&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&OtherNo="+tContNo;

	fm.submit();
}

//批量打印
function printAllNotice()
{
	if (BillGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < BillGrid.mulLineCount; i++)
	{	
		if(BillGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}

	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.action="../operfee/PolicyAbatePrintAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.action="../operfee/PolicyAbatePrintForBatPrt.jsp";
		fm.submit();
	}

}

function afterSubmit2(FlagStr,Content)
{
	//showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}