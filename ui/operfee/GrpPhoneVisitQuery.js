//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	var tCusttorNo = fmOverAll.all('HideCustomerNo').value ;
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	//查询文本框数据
	//textQeury();
	//查询本通知书收费情况
	afterJisPayQuery();
  //查询客户受理情况    	 
  if (!(tCusttorNo == null || tCusttorNo=="null" || tCusttorNo=="" ))
  {  	
	    easyQueryClick();
  }

    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterOver( FlagStr, content )
{
	var tCusttorNo = fmOverAll.all('HideCustomerNo').value ;
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
   // returnTask();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	parent.window.opener.focus();
	parent.window.close();
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



function CheckDate()
{
  if(!isDate(fmMulti.all('StartDate').value)||!isDate(fmMulti.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fmMulti.all('StartDate').value,fmMulti.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

//查询文本框内容
function textQeury(){

	var arrturn = new Array();
	var strSQL = "select b.GrpName,(select getnoticeno from LGPhoneHasten where workno='" + AcceptNo + "'),c.Phone1"
		+" from lcgrpcont b,LCGrpAddress c where "
		+ "  c.CustomerNo=b.AppntNo and c.AddressNo=b.AddressNo and  b.AppntNo='" + Customer + "'"
		;
	arrturn =easyExecSql(strSQL);			
	if (arrturn == null) {
		return false;
	} else {
		try { fm.all('AppntNo').value = Customer; } catch(ex) { };
		try { fm.all("GrpName").value=arrturn[0][0]; }catch(ex){};
		try { fm.all('GetNoticeNo').value = arrturn[0][1]; } catch(ex) { };
		try { fm.all("HomePhone").value=""; }catch(ex){};
		try { fm.all('Phone').value = arrturn[0][2]; } catch(ex) { };
		try { fm.all("Mobile").value=""; }catch(ex){};
	}    

}

//查询文本框内容
function payQeury(){
	 
    var  selNo=  GrpJisPayGrid.getSelNo();
	if (selNo==null || selNo==0 )
	 {
		 alert("请选择一条应收记录");
		 return false;
	 }else
	{
		 var tGrpContNo = GrpJisPayGrid.getRowColData(selNo - 1 , 1);
		 var arrturn = new Array();
		 var strSQL = "select b.PayMode,b.BankCode,b.BankAccNo,c.paydate "
			+" from lcgrpcont b,ljspay c where b.GrpContNo=c.otherno and c.othernotype='1' and b.grpcontno='" + tGrpContNo + "'"
			;
		arrturn =easyExecSql(strSQL);			
		if (arrturn == null) {
			return false;
		} else {
			try { fm.all('PayMode').value = arrturn[0][0]; } catch(ex) { };
			try { fm.all('BankCode').value = arrturn[0][1]; } catch(ex) { };
			try { fm.all("BankAccNo").value=arrturn[0][2]; }catch(ex){};
			try { fm.all('PayDate').value = arrturn[0][3]; } catch(ex) { };
			try { fm.all('HidPayMode').value = arrturn[0][0]; } catch(ex) { };
		}    
	}
}

//选择操作
function operSel(){

	//查询成功则拆分字符串，返回二维数组
	  var arrData = new Array ();
	  arrData[0] = new Array();
	  arrData[0][1] = "0";
	  arrData[0][0] = "重设交费事项";
	  arrData[1] = new Array();
	  arrData[1][1] = "1";
	  arrData[1][0] = "终止缴费";

	  turnPage3.arrDataCacheSet = arrData;
	  
	  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
	  turnPage3.pageDisplayGrid = OperGrid;    
	  //保存SQL语句
	  turnPage3.strQuerySql     = strSQL; 
	  //设置查询起始位置
	  turnPage3.pageIndex       = 0;  
	  
	  //在查询结果数组中取出符合页面显示大小设置的数组
	  var arrDataSet           = turnPage3.getData(turnPage3.arrDataCacheSet, turnPage3.pageIndex, MAXSCREENLINES);
	  //调用MULTILINE对象显示查询结果
	  displayMultiline(arrDataSet, turnPage3.pageDisplayGrid);
	  return true;
}
// 查询按钮
function easyQueryClick()
{
	
	// 初始化表格
	initCustomerGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	if(loadFlag != "PERSONALBOXVIEW")
  {
	strSQL = "select w.WorkNo, w.TypeNo, w.Operator, w.AcceptCom, w.StatusNo " +
		 " from LGWork w, LGWorkTrace t  " +
		 " where  w.WorkNo = t.WorkNo  	 and    w.NodeNo = t.NodeNo  " +
		 " and  t.WorkBoxNo = (select WorkBoxNo from LGWorkBox where  OwnerTypeNo = '2' and OwnerNo = '" + operator + "') " + 
		 " and w.CustomerNo='" + Customer + "'" +
	     " and w.TypeNo='" + TypeNo + "'" + //为什么要写死啊，气死人了。TypeNo
		 " and w.StatusNo not in ('5','8')"	 
		 //其他类型的工单
		 " union " +
		 " select w.WorkNo, w.TypeNo, w.Operator, w.AcceptCom, w.StatusNo "+
		 " from LGWork w, LGWorkTrace t " +
		 " where  w.WorkNo = t.WorkNo and   w.NodeNo = t.NodeNo "+
		 " and (typeNo not like '03'   or customerNo is null)" +		//保全受理前扫描产生的工单
		 " and  t.WorkBoxNo = (select WorkBoxNo from LGWorkBox   where OwnerNo = '" + operator + "'  and  OwnerTypeNo = '2') " 
		 + " and w.CustomerNo='" + Customer + "'" +
		   " and w.TypeNo='" + TypeNo + "'" 
		 + " and w.StatusNo not in ('5','8')"
		;
   }else
   {//从查看页面，去掉操作员限制，只控制机构 qulq 2007-11-13 10:56
   		strSQL = "select w.WorkNo, w.TypeNo, w.Operator, w.AcceptCom, w.StatusNo " +
		 " from LGWork w, LGWorkTrace t  " +
		 " where  w.WorkNo = t.WorkNo  	 and    w.NodeNo = t.NodeNo  " +
		 " and w.CustomerNo='" + Customer + "'" +
	     " and w.TypeNo='" + TypeNo + "'" + //为什么要写死啊，气死人了。
	     " and w.StatusNo not in ('5','8')" 
		 //其他类型的工单
		 " union " +
		 " select w.WorkNo, w.TypeNo, w.Operator, w.AcceptCom, w.StatusNo "+
		 " from LGWork w, LGWorkTrace t " +
		 " where  w.WorkNo = t.WorkNo and   w.NodeNo = t.NodeNo "+
		 " and (typeNo not like '03'   or customerNo is null)" +		//保全受理前扫描产生的工单
		 " and w.CustomerNo='" + Customer + "'" +
		   " and w.TypeNo='" + TypeNo + "'" 
		 + " and w.StatusNo not in ('5','8')"  
		;
   	
   }
	turnPage.queryModal(strSQL, CustomerGrid); 
	showCodeName();
	
	
}


//催收完成后查询催收纪录
function JisPayQuery()
{
	// 初始化表格
	initGrpJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
     
		//var tGrpContNo=  GrpJisPayGrid.getRowColData(selNo-1,1);
		/*
		var strSQL ="select a.otherNo,a.SerialNo,a.GetNoticeNo,b.prem,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=b.PayMode),a.PayDate,getAgentName(b.AgentCode),ShowManageName(a.ManageCom) "
		+ " ,(select case when CancelReason='0' then '终止缴费'  when CancelReason='2' then '重设交费事项' end from ljspayb where a.GetNoticeNo=GetNoticeNo)"
		+" from ljspayb a,lcgrpcont b,LJSPayPersonb c where a.otherno=b.grpcontno and a.otherNoType='1'"
		+ " and c.GetNoticeNo=a.GetNoticeNo"
		+ " and a.DealState='0' and a.CancelReason is null"
		+ " and b.AppntNo='" + Customer + "'"
		+ " group by a.otherNo,a.SerialNo,a.GetNoticeNo,b.prem,a.PayDate,a.ManageCom,b.PayMode,b.AgentCode "
		;
*/
	var strSQL = " select a.otherNo,a.SerialNo,a.GetNoticeNo,b.prem,(select min(c.LastPayToDate) from ljspaygrpb c where c.getNoticeNo = a.getNoticeNo),"
						+ " CodeName('paymode',b.PayMode),a.PayDate,getAgentName(b.AgentCode),ShowManageName(a.ManageCom),"
						+ " (select case when CancelReason='0' then '终止缴费' when CancelReason='2' then '重设交费事项' end from ljspayb where a.GetNoticeNo=GetNoticeNo) "
						+ " from ljspayb a,lcgrpcont b,LGPhoneHasten d "
						+ " where a.otherno=b.grpcontno and a.otherNoType='1' "
						+ " and d.GetNoticeNo=a.GetNoticeNo and d.edoracceptno = b.grpcontno "
						+ " and b.AppntNo='" + Customer + "'"
						+ " and a.DealState='0'"
						//+ " and workno ='"+workno+"'"
						+ " group by a.otherNo,a.SerialNo,a.GetNoticeNo,b.prem,a.PayDate,a.ManageCom,b.PayMode,b.AgentCode"
						;
	turnPage4.queryModal(strSQL, GrpJisPayGrid); 
	showCodeName();
}


//催收完成后查询催收纪录
function afterJisPayQuery()
{
	// 初始化表格
	initGrpJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
     var  selNo=  CustomerGrid.getSelNo();
	 if (selNo==null)
	 {
		 alert("请选择一条记录");
	 }else
	{		
		var strSQL = " select a.otherNo,a.SerialNo,a.GetNoticeNo,b.prem,(select min(c.LastPayToDate) from ljspaygrpb c where c.getNoticeNo = a.getNoticeNo),"
						+ " CodeName('paymode',b.PayMode),a.PayDate,getAgentName(b.AgentCode),ShowManageName(a.ManageCom),"
						+ " (select case when CancelReason='0' then '终止缴费' when CancelReason='2' then '重设交费事项' end from ljspayb where a.GetNoticeNo=GetNoticeNo) "
						+ " from ljspayb a,lcgrpcont b,LGPhoneHasten d "
						+ " where a.otherno=b.grpcontno and a.otherNoType='1' "
						+ " and d.GetNoticeNo=a.GetNoticeNo and d.edoracceptno = b.grpcontno "
						+ " and b.AppntNo='" + Customer + "'"
						//+ " and workno ='"+workno+"'"
						+ " and a.DealState='0'"
						+ " group by a.otherNo,a.SerialNo,a.GetNoticeNo,b.prem,a.PayDate,a.ManageCom,b.PayMode,b.AgentCode"
						;
	}  
	 
	
	turnPage4.queryModal(strSQL, GrpJisPayGrid); 
}


//选择操作
function operSelSelBox(){
	var  selNo=  GrpJisPayGrid.getSelNo();
	if (selNo==null || selNo==0 )
	 {
		 alert("请选择一条应收记录");
		 return false;
	 }
}


//地址变更
function addressChange()
{
	window.open("../task/TaskEasyEdorMain.jsp?CustomerNo=" + Customer);
}
//补发收费通知书
function printNotice()
{
	var tRow = GrpJisPayGrid.getSelNo() ;	
	if (tRow == 0 || tRow == null)
	{
		alert("请选择一个保单");
		return false;
	}

	 var tGetNoticeNo=GrpJisPayGrid.getRowColData(tRow - 1,3);
	 if (tGetNoticeNo =='')
	 {
		 alert("发生错误，原应是应收记录号为空");
		 return false;
	 }
	 window.open("../operfee/GrpPrintSel.jsp?GetNoticeNo="+ tGetNoticeNo);

}


//业务员通知（函件功能）
function agentNotice()
{
	selNo =   CustomerGrid.getSelNo();
	if (selNo==null || selNo==0)
	{
		alert("请选择一条记录");
		return false;
	}else
	{
		var tWorkNo = CustomerGrid.getRowColData(selNo - 1,1);
	    window.open("../task/TaskLetterMain.jsp?workNo=" + tWorkNo);
	}
}

//结案按钮
function overCase()
{
	fmOverAll.all('HideCustomerNo').value=Customer;
	var showStr="正在，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fmOverAll.submit(); //提交
}

//保存按钮
function saveRecord()
{
	var  selNo =  OperGrid.getSelNo();
	var  selLJSNo = GrpJisPayGrid.getSelNo();
	if (selLJSNo==null || selLJSNo==0 )
	 {
		 alert("请选择一条应收记录！");
		 return false;
	 }
	
	if (selNo==null || selNo==0 )
	 {
		 alert("请选择一种操作！");
		 return false;
	 }
	
	var tOper = OperGrid.getRowColData(selNo - 1, 2);
	if (tOper == "0")
	{
		 if (!(fm.all('HidPayMode').value == fm.all('PayMode').value) && fm.all('PayMode').value==4)
		 {
			 alert("不能将收费方式改为银行转帐！");
			 return false;
		 }

		 var tGrpContNo = GrpJisPayGrid.getRowColData(selLJSNo - 1,1);
		 fmSaveAll.all('HidePayMode').value=fm.all('PayMode').value;
		 fmSaveAll.all('HidePayDate').value=fm.all('PayDate').value;
		 fmSaveAll.all('HideGrpContNo').value=tGrpContNo;
		 fmSaveAll.all('HideNoticeNo').value=GrpJisPayGrid.getRowColData(selLJSNo - 1,3);
		 var showStr="正在，请您稍候并且不要修改屏幕上的值或链接其他页面";
		 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		 fmSaveAll.submit(); //提交
	}else
	{
		var tNoticeNo = GrpJisPayGrid.getRowColData(selLJSNo - 1,3)
		fmSubmitAll.all('SubmitNoticeNo').value=tNoticeNo;
		var showStr="正在，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fmSubmitAll.submit(); //提交
	}		
	
}