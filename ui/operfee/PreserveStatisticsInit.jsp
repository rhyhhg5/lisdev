 <%
//程序功能：
//创建日期：2010-01-01 
//创建人  ：Dingjw
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                          
 <%
	  GlobalInput tGI = (GlobalInput)session.getValue("GI");
	  String ManageCom = "";
	  String CurDate="";
	  if(tGI==null)
	  {
		  ManageCom="unknown";
		  CurDate="";
	  }
	  else //页面有效
	  {
		  ManageCom =tGI.ManageCom;
		  CurDate=PubFun.getCurrentDate();
	  }
  %> 
  
  

<script language="JavaScript">

var manageCom = "<%=ManageCom%>"; //记录管理机构
var curDate= "<%=CurDate%>";

function initForm()
{
  try
  {
	  initTextbox();
	  initContPreserveGrid();  
  }
  catch(re)
  {
    alert("BatchPayInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initTextbox()
{
	document.fm.all("ManageCom").value=manageCom;
	
	var sql1 = "select Name from LDCom where ComCode = '" + manageCom + "'";
    document.fm.all('ManageComName').value = easyExecSql(sql1);

	var sql1 = "select Current Date - 3 month from dual ";
	fm.all('StartDate').value = easyExecSql(sql1);
	fm.all('EndDate').value=curDate;
    
}


function initContPreserveGrid()
{           
    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="分公司";   		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=70;            		//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许  
      
      iArray[2]=new Array();
      iArray[2][0]="险种名称";   		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=70;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许   

      iArray[3]=new Array();
      iArray[3][0]="满一年保费合计";		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="满一年保单件数";         		//列名
      iArray[4][1]="70px";            		//列宽
      iArray[4][2]=60;            	        //列最大值
      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="保费留存合计";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=60;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="保费留存件数";         		//列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=60;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      //iArray[5][4]="paymode2";              	 //是否引用代码:null||""为不引用
      //iArray[5][9]="付费方式|code:paymode2&NOTNULL"; 

      iArray[7]=new Array();
      iArray[7][0]="退保保费合计";  	//列名
      iArray[7][1]="100px";          //列宽
      iArray[7][2]=60;            	//列最大值
      iArray[7][3]=0;                //是否允许输入,1表示允许，0表示不允许
      
      
      iArray[8]=new Array();
      iArray[8][0]="退保保单件数";  	//列名
      iArray[8][1]="70px";          //列宽
      iArray[8][2]=60;            	//列最大值
      iArray[8][3]=0;                //是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="有效留存保费合计";  	//列名
      iArray[9][1]="100px";          //列宽
      iArray[9][2]=60;            	//列最大值
      iArray[9][3]=0;                //是否允许输入,1表示允许，0表示不允许
      
      
      iArray[10]=new Array();
      iArray[10][0]="有效留存件数合计";  	//列名
      iArray[10][1]="70px";          //列宽
      iArray[10][2]=60;            	//列最大值
      iArray[10][3]=0;                //是否允许输入,1表示允许，0表示不允许

         
      iArray[11]=new Array();
      iArray[11][0]="满两年保费合计";         		//列名
      iArray[11][1]="100px";          	//列宽
      iArray[11][2]=60;            	  //列最大值
      iArray[11][3]=0;                 //是否允许输入,1表示允许，0表示不允许

      iArray[12]=new Array();
      iArray[12][0]="满两年保单件数";         		//列名
      iArray[12][1]="70px";          	//列宽
      iArray[12][2]=60;            	  //列最大值
      iArray[12][3]=0;                 //是否允许输入,1表示允许，0表示不允许
      


      
      ContPreserveGrid = new MulLineEnter( "fm" , "ContPreserveGrid" );   
      ContPreserveGrid.displayTitle = 1;
      ContPreserveGrid.mulLineCount = 0;
      ContPreserveGrid.hiddenPlus = 1;
      ContPreserveGrid.hiddenSubtraction = 1;
      ContPreserveGrid.canChk =0; 
      ContPreserveGrid.loadMulLine(iArray);  

      	
      }
      catch(ex)
      {
        window.alert(ex);
      }
}
</script>
