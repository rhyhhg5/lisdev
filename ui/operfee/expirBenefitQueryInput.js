//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var expression ="" ;

var styleLength = 50;

function PersonSingle()
{
    if(beforeSubmit())
    {  
    	if (!checkValidateDate()) return false;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      	  	
      fm.submit();
    }	
}

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    //此处content可能较长，使用showModalDialog可能有问题，现将错误显示到页面
    fm.all("ErrorsInfo").innerHTML = content;
    
    content = "操作完毕，发生的错误信息见页面底部";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }

   //resetForm();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  afterContQuery();
	  afterJisPayQuery();
	  
  }
  catch(re)
  {
  	alert("在GrpDueFeeBatchInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function CheckGrpPolNo()
{

   
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function GrpMulti()
{
  if(!checkValidateDate()) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	

  fm.submit();	

}



function CheckDate()
{
  if(!isDate(fm.all('StartDate').value)||!isDate(fm.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fm.all('StartDate').value,fm.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

// 查询按钮
/*********************************************************************
 *  查询合同号
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	
	//得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return fasle;
	}
	managecom = fm.ManageCom.value;
	
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}	 //modify by fuxin  2008-5-5 9:59:59
	
	//应收时间起止期三个月校验
	var t1 = new Date(CurrentTime.replace(/-/g,"\/")).getTime();
    var t2 = new Date(SubTime.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>92)
    {
		alert("满期日起止日期不能超过3个月!");
		return false;
    }
    if(tMinus<0)
    {
		alert("满期日止期不能小于满期日起期！");
		return false;
    }
    
	if(fm.all("queryType").value=="1")
	{
		alert("请录入保单类型.");
		return false;
	}
	
	fm.all("TempQueryType").value = fm.all("queryType").value;

	if(fm.all("queryType").value=="2" )
	{
		expression ="	and a.riskcode not in ('320106','120706','330501','530301') " ;
	}
	if(fm.all("queryType").value=="3")
	{
		expression = "	and exists (select 1 from ljspayb ljs,lpedorespecialdata lpe where a.contno = ljs.otherno and ljs.othernotype='2' and ljs.getnoticeno= lpe.edoracceptno and lpe.edortype = 'XB' and detailtype ='DEALTYPE'and ljs.dealstate='3'  and lpe.edorvalue='2') " 
					+" and a.riskcode  in ('320106','120706') "
	}
	if(fm.all("queryType").value=="4" )
	{
		expression ="	and a.riskcode in ('330501','530301') " ;
	}
	
	var tCheckState = fm.all("CheckState").value;
	var subCheckSql = ""
	if(tCheckState !="")
	{
	    if(tCheckState == "0")
	    {
	       subCheckSql += "";
	    }
	    else if(tCheckState == "1" )
	    {
	       subCheckSql += " and exists (select 1 from LPEdorEspecialData where EdorType = 'MJ' and DetailType = 'CWYB' and PolNo = a.Contno and EdorValue = a.getnoticeno) ";
	    }
	    else if(tCheckState == "2")
	    {
	       subCheckSql += " and not exists (select 1 from LPEdorEspecialData where EdorType = 'MJ' and DetailType = 'CWYB' and PolNo = a.Contno and EdorValue = a.getnoticeno) ";
	    }
	}
  
    var strSQL = " select serialno, "
             + "case when exists (select 1 from LPEdorEspecialData where EdorType = 'MJ' and DetailType = 'CWYB' "
             + "and PolNo = a.Contno and EdorValue = a.getnoticeno) then '是' else '否' end, " 
             + "(select otherno from ljsget where getnoticeno =a.getnoticeno),"
             +	" '', min(operator),managecom,contno,(select name from lcinsured where contno = a.contno and insuredno = a.insuredno),"
             +	" sum(getmoney),min(a.lastgettodate),(select name from laagent where agentcode = a.agentcode),"
             + " agentgroup,(select codename('paymode',paymode) from ljsget where getnoticeno =a.getnoticeno),(''), "
             + "(case when not exists (select 1 from ljaget where actugetno = a.getnoticeno) then '待给付确认' else "
             + "(select case when b.dealstate='0' and c.confdate is null then '待给付' when  c.confdate is not null then '给付完成' "
             + "end from ljsget b ,ljaget c " 
             + "where b.getnoticeno = a.getnoticeno and c.actugetno = a.getnoticeno fetch first 1 row only) end  )"
             + " ,(case when exists (select 1 from loprtmanager where code in ('bq001','MX001','MX002','MX004') and standbyflag2 = a.getnoticeno "
             + " and printtimes >= 1) then '已打印' else '未打印' end), a.GetNoticeNo,a.RiskCode "
             + " from ljsgetdraw a where 1=1 "
             + expression 
             + subCheckSql 
             + " and (feefinatype != 'YEI' or a.riskcode = '330501') "
             + " and exists (select 1 from ljsget where a.getnoticeno = getnoticeno and othernotype ='20') "
             + " and (not exists (select 'Z' from ljsget where getnoticeno = a.getnoticeno and paymode = '5') or a.riskcode = '330501') and "
             ;
		
    var subSql = " a.lastgettodate between '"+CurrentTime+"' and '"+SubTime+"' "
    + " and not exists (select 1 from lcpol d where d.contno = a.contno" 
      + " and exists (select 1 from lmriskapp where riskcode =d.riskcode and risktype4= '4' and subriskflag<>'S')" 
      + " and not exists (select 1 from ldcode where codetype = 'jkys' and code =d.riskcode )) ";
		       
	if(fm.all("BatchDate").value !="")
			subSql += " and a.makedate = '"+fm.all("BatchDate").value+"' ";
	if(fm.all("Operator").value !="")
			subSql += " and a.Operator = '"+fm.all("Operator").value+"' ";
	if(fm.all("ManageCom").value !="")
			subSql += " and a.ManageCom like '"+fm.all("ManageCom").value+"%' ";
	if(fm.all("PayMode").value !="")
			subSql += " and exists (select getnoticeno from ljsget where getnoticeno = a.getnoticeno and PayMode ='"+fm.all("PayMode").value+"') ";
	if(fm.all("ContNo").value !="")
			subSql += " and a.contno ='"+fm.all("ContNo").value+"' ";
	if(fm.all("AppntNo").value !="")
			subSql += " and a.AppntNo ='"+fm.all("AppntNo").value+"' ";
	if(fm.all("AgentGroup").value !="")
			subSql += " and a.AgentGroup ='"+fm.all("AgentGroup").value+"' ";
	if(fm.all("AgentCode").value !="")
			subSql += " and a.AgentCode ='"+fm.all("AgentCode").value+"' ";
	if(fm.all("PayTaskState").value !="")
	{
	    if(fm.all("PayTaskState").value == "0")
	    {
	       subSql += " and exists (select 1 from ljaget where actugetno = a.getnoticeno and confdate is null) ";
	    }
	    else if(fm.all("PayTaskState").value == "1" )
	    {
	       subSql += " and exists (select 1 from ljaget where actugetno = a.getnoticeno and confdate is not null ) ";
	    }
	    else if(fm.all("PayTaskState").value == "2")
	    {
	       subSql += " and not exists (select 1 from ljaget where actugetno = a.getnoticeno) ";
	    }
	    else
	    {
	      subSql += " and exists (select getnoticeno from ljsget where getnoticeno = a.getnoticeno and dealstate ='"+fm.all("PayTaskState").value+"') ";
	    }
	}
			
	if(fm.all("PrintState").value =="0")
			subSql += " and not exists (select standbyflag2 from loprtmanager where code in ('bq001','MX001','MX002','MX004') and standbyflag2 = a.getnoticeno and printtimes >= 1)";
	if(fm.all("PrintState").value =="1")
			subSql += " and exists (select standbyflag2 from loprtmanager where code in ('bq001','MX001','MX002','MX004') and standbyflag2 = a.getnoticeno and printtimes >= 1 ) ";
			
	var tGetStartDate = fm.GetStartDate.value;
	var tGetEndDate = fm.GetEndDate.value;
	var tConfStartDate = fm.ConfStartDate.value;
	var tConfEndDate = fm.ConfEndDate.value;
	
	if(tGetStartDate != "" && tGetStartDate != null)
	{
		subSql += " and exists (select 1 from ljaget where actugetno = a.getnoticeno and makedate >= '" + tGetStartDate + "') ";
	}
	if(tGetEndDate != "" && tGetEndDate != null)
	{
		subSql += " and exists (select 1 from ljaget where actugetno = a.getnoticeno and makedate <= '" + tGetEndDate + "') ";
	}
	if(tConfStartDate != "" && tConfStartDate != null)
	{
		subSql += " and exists (select 1 from ljaget where actugetno = a.getnoticeno and confdate >= '" + tConfStartDate + "') ";
	}
	if(tConfEndDate != "" && tConfEndDate != null)
	{
		subSql += " and exists (select 1 from ljaget where actugetno = a.getnoticeno and confdate <= '" + tConfEndDate + "') ";
	}
			
	strSQL += subSql;
	subSql += expression;
	subSql += subCheckSql;
	strSQL +=" group by getnoticeno,contno,insuredno,serialno,agentcode,agentgroup,managecom,polno,a.RiskCode with ur";
	
	fm.QuerySql.value = subSql;
	turnPage2.queryModal(strSQL, LjsGetGrid);
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	if(LjsGetGrid.mulLineCount == 0)
	{
	    alert("没有符合条件的满期任务信息");
	}
	
}

/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getpoldetail()
{
    var arrReturn = new Array();
	  var tSel = IndiContGrid.getSelNo();	
	  if( tSel == 0 || tSel == null )
	  {
		   alert( "请先选择一条记录，再点击返回按钮。" );
	  }	
	  else
	  {
		   getPolInfo();
    }  	                                          
                         	               	
}
/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolInfo()
{
	
	 	  var tRow = IndiContGrid.getSelNo() - 1;	        
		var tContNo=IndiContGrid.getRowColData(tRow,1);  
		
		var strSQL = "select a.RiskSeqNo,a.InsuredName, (select riskname  from lmrisk where riskcode=a.riskcode) ,a.RiskCode, (select codeName from LDCode where codeType = 'payintv' and code = char(a.PayIntv)), "
			+ "	a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate ,a.enddate "  
			+ "from lcpol a where 1=1"
			+ "  and a.contno='" + tContNo + "' "
			+ "  and a.appflag='1' "
			+ "order by polNo "
			+ "with ur"			
			;
		
    	turnPage3.queryModal(strSQL, PolGrid); 
}


/*********************************************************************
 *  重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为resetForm()
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
  try
  {    
	  afterContQuery();
	  afterJisPayQuery();
	  
  }
  catch(re)
  {
  	alert("在LJSPayPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 



/*********************************************************************
 *  校验日期范围
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkValidateDate()
{
	//校验录入的起始日期和终止日期必须在今天之前，否则不予提交
	var startDate=fm.StartDate.value;
	var endDate=fm.EndDate.value;
	if(startDate==''||endDate=='')
	{
		alert("请录入查询日期范围！");
		return false;
	}
	if(compareDate(startDate,endDate)==1)
	{
		alert("起始日不能晚于截止日!");
		return false;
	}
	return true;
}

//点击后查询保单
function afterContQuery(contno)
{
		// 初始化表格
	initIndiContGrid();

	 var strSQL = "select a.ContNo,a.PrtNo,a.AppntName,a.CValiDate,a.cinvalidate,'',ShowManageName(a.ManageCom),a.agentgroup,a.AgentCode "
	 + " from LCCont a "
	 + " where a.AppFlag='1' and a.contno = '"+contno+"' with ur "
	 ;
	turnPage4.queryModal(strSQL, IndiContGrid); 
	initPolGrid(); 
}



/*********************************************************************
 *  投保信息列表中选择一条，查看投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function returnParent()
{
	//从客户投保信息列表中选择一条，查看投保信息

		 var tSel1 = IndiContGrid.getSelNo();
		 if( tSel1 == 0 || tSel1 == null )
	   {		
		   alert( "请先选择一条记录，或输入保单号，再点击保单明细按钮。" );
	   }else
	   {
	   	  var cContNo = IndiContGrid.getRowColData( tSel1 - 1, 1);		  
				window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType=1");	
					
	   }
	}
function getLjsGetDrawdetail()
{
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length <1 )
	{
		return false;	
	}

	var taskNo=LjsGetGrid.getRowColDataByName(tChked[0],"TaskNo");
	
	var SQL = "SELECT CONTNO, "
            + "case when exists (select 1 from LPEdorEspecialData where EdorType = 'MJ' and DetailType = 'CWYB' "
            + "and PolNo = a.Contno and EdorValue = a.getnoticeno) then '是' else '否' end, "  
            + "insuredno,'',RISKCODE,GETMONEY,(select enddate from lcpol where polno = a.polno),POLNO, " 
	        + "case DutyCode when '000000' then '初始忠诚奖' when '000001' then '外加忠诚奖' end "
	        + "FROM LJSGETDRAW a WHERE GETNOTICENO in (SELECT GETNOTICENO FROM LJSGET WHERE OTHERNOTYPE ='20' AND OTHERNO ='"+taskNo+"')";
	turnPage1.queryModal(SQL, LjsGetDrawGrid);
	for(var i = 0; i < LjsGetDrawGrid.mulLineCount; i++)
	{
		var sql = "select name from lcbnf where polno ='"+LjsGetDrawGrid.getRowColDataByName(i,"PolNo")+"'";
		var rs = easyExecSql(sql);
		var bnf = "";
		if(rs!=null)
		{
			for(var j =0;j<rs.length;j++)
			{
				bnf+=rs[j]+'、';
			}
		}
		LjsGetDrawGrid.setRowColData(i,4,bnf);
	}
	afterContQuery(LjsGetDrawGrid.getRowColData(0,1));
}

//打印给付清单
function printGetList()
{
    fm.target="_blank";
    var ManageCom=fm.all("ManageCom").value;
    if(fm.all("TempQueryType").value == "4")
    {
      if(ManageCom=="86"||trim(ManageCom)=="86")
      {
        fm.action="./expirBenefitQuery330501PrintTxt.jsp";
      }
      else
      {
          fm.action="./expirBenefitQuery330501Print.jsp";
      }
      
    }
    else
    {
      fm.action="./expirBenefitQueryInputPrint.jsp";
    }
	
	fm.submit();
}

//给付任务撤销
function getTaskCansel()
{
	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行撤销");
		return false;	
	}	
	var taskNo=LjsGetGrid.getRowColDataByName(tChked[0],"TaskNo");
	fm.action = "GetTaskCansel.jsp?taskNo="+taskNo;
	fm.submit();
}
//给付方式变更
function changePayMode()
{
  var chkNum = 0;
  var tGetNoticeNo = "";
  var tContNoForModify = "";
  var tRiskCode = "";
  for (i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if (LjsGetGrid.getChkNo(i)) 
		{
		  chkNum = chkNum + 1;
		  if (chkNum == 1)
			{
				tContNoForModify = LjsGetGrid.getRowColDataByName(i, "contNoForModify");
				// modify by hyy
				tGetNoticeNo = LjsGetGrid.getRowColDataByName(i, "GetNoticeNo");
				tRiskCode = LjsGetGrid.getRowColDataByName(i,"RiskCode");
			}
		}
	}
  
	if(chkNum == 0)
	{
	  alert("请选择需要变更的记录");
	  return false;
	}
	if(chkNum != 1)
	{
	  alert("只能选择一条记录进行变更");
	  return false;
	}
	
	if(tRiskCode=="330501")
	{
	  alert("常无忧B不能变更给付方式！");
	  fm.all("PayModeChangeID").style.display = "none";
	  return false;
	}
	fm.GetNoticeNo.value = tGetNoticeNo;
	//modify by hyy
	fm.contNoForModify.value = tContNoForModify ;
	fm.all("PayModeChangeID").style.display = "";
  
  var rs = getPayModeInfo(tGetNoticeNo);
  if(rs)
  {
    fm.PayModeOld.value = rs[0][0];
    fm.PayModeOldName.value = rs[0][1];
    fm.BankCodeOld.value = rs[0][2];
    fm.BankNameOld.value = rs[0][3];
    fm.AccNameOld.value = rs[0][4];
    fm.BankAccNoOld.value = rs[0][5];
  }
}

//变更给付方式确认
function changePayModeSubmit()
{
  if(fm.BankAccNo.value != fm.BankAccNo2.value)
  {
    alert("您两次录入的账号不同，请核对");
    return false;
  }
  
  if(fm.PayModeOld.value == fm.PayModeNew.value
    && fm.BankCodeOld.value == fm.BankCode.value
    && fm.AccNameOld.value == fm.AccName.value
    && fm.BankAccNoOld.value == fm.BankAccNo.value)
  {
    alert("你录入的新给付方式与原给付方式一致，不需要变更");
    return false;
  }
  
  if(fm.PayModeNew.value == "")
  {
    alert("请选择给付方式");
    return false;
  }
  else if(fm.PayModeNew.value == "3" || fm.PayModeNew.value == "4")
  {
    if(fm.BankCode.value == "" || fm.AccName.value == "" || fm.BankAccNo.value == "")
    {
      alert("银行账户信息必须录入完整");
      return false;
    }
  }
  else if(fm.PayModeNew.value == "1" || fm.PayModeNew.value == "2")
  {
    if(fm.BankCode.value != "" || fm.AccName.value != "" || fm.BankAccNo.value != "")
    {
      alert("您所选给付方式不需要录入帐户信息，请清空");
      return false;
    }
  }
  var rs = getPayModeInfo(fm.GetNoticeNo.value);
  if(rs[0][0] == fm.PayModeNew.value && rs[0][2] == fm.BankCode.value
    && rs[0][4] == fm.AccName.value && rs[0][5] == fm.BankAccNo.value)
  {
    alert("您录入的信息已进行了变更");
    return false;
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var fmAction = fm.action;
  fm.action = "expirBenefitChangePayModeSave.jsp";
  fm.submit();
  
  fm.action = fmAction;
  
  return true;
}

//查询给付方式信息
function getPayModeInfo(cGetNoticeNo)
{
  var sql = "select PayMode, CodeName('paymode', a.PayMode), "
          + "   BankCode, (select BankName from LDBank where BankCode = a.BankCode), "
          + "   AccName, BankAccNo "
          + "from LJSGet a "
          + "where GetNoticeNo = '" + cGetNoticeNo + "' ";
  var rs = easyExecSql(sql);
  return rs;
}

function afterCodeSelect(cCodeName, Field)
{
  if(cCodeName=="PayMode2")
  {
    if(fm.PayModeNew.value == "1" || fm.PayModeNew.value == "2")
    {
      fm.BankCode.value = "";
      fm.BankName.value = "";
      fm.AccName.value = "";
      fm.BankAccNo.value = "";
      fm.BankAccNo2.value = "";
    }
  }
}


function printOneNoticeNew()
{
	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印");
		return false;	
	}
	
	var tRiskCode = LjsGetGrid.getRowColDataByName(tChked[0],"RiskCode");
	
	if (fm.all("TempQueryType").value=="3")
	{ 
		var taskNo=LjsGetGrid.getRowColDataByName(tChked[0],"TaskNo");
		//var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'bq001' and standbyflag2=(select getnoticeno from ljsget where othernotype ='20' and otherno ='"+taskNo+"')");
	  var tGetNoticeNo=LjsGetGrid.getRowColDataByName(tChked[0],"GetNoticeNo");
	//PDF打印用
		//if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
		//{
		//	fm.all('taskNo').value = taskNo;
		//	fm.action="./expirBenefitInsForPrt.jsp?taskNo="+taskNo+"&GetNoticeNo="+tGetNoticeNo;
		//	fm.submit();
		//}
		//else
	  //  { 
		//	fm.action = "../uw/PrintPDFSave.jsp?Code=0bq001&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
		//	fm.submit();   
		//}
		var showStr="正在准备打印数据，请稍后...";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.target = "fraSubmit";
	  fm.action="../uw/PDFPrintSave.jsp?Code=bq001&StandbyFlag2="+tGetNoticeNo;
	  fm.submit();
	}
	else
	{ 
			var taskNo = LjsGetGrid.getRowColDataByName(tChked[0],"TaskNo");
			//var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'MX001' and standbyflag2=(select getnoticeno from ljsget where othernotype ='20' and otherno ='"+taskNo+"')");
			var tGetNoticeNo=LjsGetGrid.getRowColDataByName(tChked[0],"GetNoticeNo");
            var showStr="正在准备打印数据，请稍后...";
            var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
            showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
            fm.target = "fraSubmit";
	  if(tRiskCode=="330501")
	  {
	    var sql = "select substr(managecom,1,4) from ljsget where getnoticeno = '" + tGetNoticeNo + "' ";
	    var rs = easyExecSql(sql);
	    if(rs[0][0]=="8644")//广东分公司与其他分公司类型不同
	    {
	      fm.action="../uw/PDFPrintSave.jsp?Code=MX004&StandbyFlag2="+tGetNoticeNo;
	    }
	    else
	    {
	      fm.action="../uw/PDFPrintSave.jsp?Code=MX002&StandbyFlag2="+tGetNoticeNo;
	    }
	  }
	  else
	  {
	    fm.action="../uw/PDFPrintSave.jsp?Code=MX001&StandbyFlag2="+tGetNoticeNo;
	  }
      fm.submit();
	}
}
	
function printAllNoticeNew()
{
    fm.all("queryType").value = fm.all("TempQueryType").value;
	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{	
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}


	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	if (fm.all('TempQueryType').value == "3")
	{
		if(tChked.length == 0)
		{
			fm.action="./expirBenefitInsAllBatPrt.jsp";
			fm.submit();
		}
		else
		{
			fm.action="./expirBenefitInsForBatPrt.jsp";
			fm.submit();
		}
	}
	else
		{
			if(tChked.length == 0)
			{
				fm.action="./expirBenefitGetAllBatPrt.jsp";
				fm.submit();
			}
			else
			{
				fm.action="./expirBenefitGetForBatPrt.jsp";
				fm.submit();
			}
		}
}	
	
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

function print330501Data()
{
  if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印");
		return false;	
	}
	
  var tRiskCode = LjsGetGrid.getRowColDataByName(tChked[0],"RiskCode");
  var tGetNoticeNo = LjsGetGrid.getRowColDataByName(tChked[0],"GetNoticeNo");
  
  if(tRiskCode != "330501")
  {
    alert("打印批单功能只针对常无忧B保单！");
    return false;
  }
  
  //该保单还没有给付确认，请在给付确认页面打印批单
  var sql = "select 1 from ljaget where actugetno = '" + tGetNoticeNo + "' ";
  var rs = easyExecSql(sql);
  if(!rs)
  {
    alert("该保单还没有给付确认，请先给付确认后再打印批单！");
    return false;
  }
  
  var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="../uw/PDFPrintSave.jsp?Code=MX003&StandbyFlag2="+tGetNoticeNo;
  fm.submit();
}