var turnPage = new turnPageClass();
var mSql="";
var showInfo;

//下载清单
function download()
{
	if(!verifyInput2())
	{
         return false;
    }
    
	var tManageCom = fm.all('ManageCom').value;			//管理机构代码
	var tManageComName = fm.all('ManageComName').value; //机构名
	var tContType = fm.all('ContType').value;			//保单类型
	var tSaleChnl = fm.all('SaleChnl').value;			//销售渠道
	var tContinueType = fm.all('ContinueType').value;	//继续率类型
	var tOrphans = fm.all('Orphans').value;				//保单号
	var tStartDate = fm.all('StartDate').value;			//生效开始日期
	var tEndDate = fm.all('EndDate').value;				//生效结束日期
	
	//生效开始日期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("请输入生效开始日期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("生效开始日期",tStartDate))
		{
			fm.all('StartDate').focus();
			return false;
		}
	}
	//生效结束日期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("请输入生效结束日期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("生效结束日期",tEndDate))
		{
			fm.all('EndDate').focus();
			return false;
		}
	}
	//生效日期起止期三个月校验&非工作时间起止日期一年内校验
	var t1 = new Date(tStartDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    if(t2-t1<0)
    {
      alert("生效开始日期不能晚于生效结束日期 "+tEndDate)
	  return false;
    }
    
    
    var tCDate = new Date();
    var day=tCDate.getDay();
    if(day!="6" && day!="0" && isWorkTime()){// 工作日工作时间不能打印三个月的报表
	   	if(!chenkDate()){
	   		return false;
	   	}
    }else{// 非工作时间不能打印超过一年的报表
    	var interval = dateInterval(tStartDate,tEndDate);
    	if(interval > 12){
    		alert("对不起，您不能打印一年以上的报表");
    		return false;
    	}
    }
    	
    var showStr;
    if(tManageComName=="")
    {
      showStr = "正在下载数据，时间可能较长，请您稍候并且不要修改屏幕上的值或链接其他页面……";
    }
    else
    {
      showStr = "正在下载数据，管理机构类型为【" + tManageComName + "】，时间可能较长，请您稍候并且不要修改屏幕上的值或链接其他页面……";
    }
    fm.all("divInfo").style.display = "";
    fm.all("divInfo").innerHTML = "<font color=red size=6>" + showStr + "</font>";
	  
	fm.submit();
}

//日期格式校验
function checkDateFormat(tName,strValue) 
{
	if (!isDate(strValue))
	{
		alert("输入的["+tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}

