
<%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
</script>

<html>
<%
//程序名称：PrtContSuccess.jsp
//程序功能：保单失效清单
//创建日期：2007-1-15 16:28
//创建人  ：杨亚林
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="PrtOperfeePayMod.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="PrtOperfeePayModInit.jsp"%>
	</head>

<body  onload="initForm();" >
  <form method=post name=fm action="" target="fraSubmit" >
  <br></br>
    <table class= common border=0 width=100%>
    	<tr>
			  <td class= title align= center><B><%=title%></B></td>
		  </tr>
		  <tr>
			  <td><INPUT VALUE="生成报表" class = cssbutton TYPE=button onclick="prtList();"></td> 
		  </tr>
	  </table>
    <table  class= common align=center>
      <TR  class= common>
		    <TD  class= title><%=startTitleName%></TD>  
		    <TD  class= input> 
		      <Input class="coolDatePicker" dateFormat="short" name=StartDate style="width:100" verify="应收起期|notnull" elementtype="nacessary" > 
		    </TD>
		    <TD  class= title><%=endTitleName%></TD>
        <TD  class= input>
          <Input class="coolDatePicker" dateFormat="short" name=EndDate style="width:100" verify="应收止期|notnull" elementtype="nacessary" >
        </TD>          
        <TD  class= title>管理机构</TD>
        <TD  class= input>
          <Input class= "codeno"  name=ManageCom verify="管理机构|notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename style="width:80" readonly name=ManageComName elementtype="nacessary" >
        </TD>
      </TR>
      <TR  class= common style="display:none">
		    <TD  class= title>营销部：</TD>
        <TD  class= input>
          <Input class= "codeno"  name=Group03  ondblclick="return showCodeList('agentgroupbq',[this,Group03Name],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,Group03Name],[0,1],null,'03','BranchLevel',1);" ><Input class=codename style="width:80" readonly name=Group03Name>
        </TD>
		    <TD  class= title>营业区</TD>
        <TD  class= input>
          <Input class= "codeno"  name=Group02  ondblclick="return showCodeList('agentgroupbq',[this,Group02Name],[0,1],null,'02','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,Group02Name],[0,1],null,'02','BranchLevel',1);" ><Input class=codename style="width:80" readonly name=Group02Name>
        </TD>
		    <TD  class= title>营业处</TD>
        <TD  class= input>
          <Input class= "codeno"  name=Group01  ondblclick="return showCodeList('agentgroupbq',[this,Group01Name],[0,1],null,'01','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,Group01Name],[0,1],null,'01','BranchLevel',1);" ><Input class=codename style="width:80" readonly name=Group01Name>
        </TD>
        <TD  class= title>营销员</TD>
        <TD  class= input>
          <Input class= "codeno"  name=AgentCode  ondblclick="return showCodeList('agentcode',[this,AgentName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('agentcode',[this,AgentName],[0,1],null,null,null,1);" ><Input class=codename style="width:80" name=AgentName readonly>
        </TD>
      </TR>
    </table>
    <input type=hidden name= "LoadFlag">  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
