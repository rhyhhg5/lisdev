  //               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var strSql="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{	
	initQueryDataGrid();
	//判断关键定是否为空
	var strAgentCom=trim(document.fm.all("ManageCom").value);
	var strEdorState=trim(document.fm.all("EdorState").value);//给付标记
	var strStartDate=trim(document.fm.all("AppStartDate").value);
	var strEndDate=trim(document.fm.all("AppEndDate").value);
	var strFlag=trim(document.fm.all("ContType").value);//个团标记
	
	//初始化防止出错
	if(strEdorState=="undefined"||strEdorState==""||strEdorState==null)
	{
		strEdorState="2";
	}
		
	if(strFlag=="undefined"||strFlag==""||strFlag==null)
	{
		strFlag="3";
	}
	if(strStartDate==null)
	{
		window.alert("应给付开始日期！");
	}
	if(strEndDate==null)
	{
		window.alert("请输入应给付结束日期");
	}
	
	var t1 = new Date(strStartDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(strEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>92 )
    {
	  window.alert("查询起止时间不得超过3个月！")
	  return false;
    }  
	
	  if((strAgentCom!=""&&strAgentCom!=null)&&(strStartDate!=""&&strStartDate!=null)&&(strEndDate!=""&&strEndDate!=null))
	  {
			  //查询strSql语句如下
		//给付状态
		 if(strEdorState!="2")
		 {
			  //个险
			  if(strFlag=="1")
			  {
			  	
				  strSql="select "+
					" a.serialno 批次号, a.getnoticeno 给付号, a.contno 保单号, a.riskcode 险种,"+
					"(case  b.salechnl when '04' then '银保' when '13' then '银保' else '个险' end ) as 个团银种类,"+
					" b.appntname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					"(select confdate from ljaget where actugetno = a.getnoticeno) 给付确认日期,"+
					"(select confdate from ljaget where actugetno = a.getnoticeno) 财务付费日期,"+
					" sum(a.getmoney) 给付金额,"+
					"(select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.getnoticeno) 给付状态,"+
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称"+
					" from LJSGetDraw a, lccont b"+
					" where a.contno = b.contno"+
					" and a.feeoperationtype = 'EB' and feefinatype <> 'YEI' and a.GetMoney <> 0"+
					" and a.getdate between '"+ strStartDate+"' and '"+strEndDate+"'"+
					" and a.managecom like '"+strAgentCom+"%%'"+
					" and (select case when confdate is null then '0' else '1' end from ljaget" +
					" where actugetno = a.getnoticeno) = '"+strEdorState+"'"+
					" group by a.serialno,a.getnoticeno,a.contno,riskcode,b.salechnl,b.appntname,b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.managecom"+
					" order by a.getnoticeno"+
					" with ur";
			  }
			  //团险
			  else if(strFlag==2)
			  {
			  	 strSql="select "+ 
					" a.endorsementno 批次号, a.actugetno 给付号, a.grpcontno 保单号, a.riskcode 险种,"+
					" (case  b.salechnl when '04' then '银保' when '13' then '银保' else '团险' end ) as 个团银种类,"+
					" b.grpname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					" (select confdate from ljaget where actugetno = a.actugetno) 给付确认日期,"+
					" (select confdate from ljaget where actugetno = a.actugetno) 财务付费日期,"+
					" (select sum(sumgetmoney) from ljaget where actugetno = a.actugetno) 给付金额,"+
					" (select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.actugetno) 给付状态,"+
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称 "+
					" from LJAGetEndorse a ,lcgrpcont b"+
					 " where a.grpcontno = b.grpcontno"+
					   " and a.othernotype = '3'"+
					   " and a.feeoperationtype = 'MJ'"+
					   " and a.GetMoney <> 0"+
					   " and b.cinvalidate between'"+strStartDate+"'and '"+strEndDate+"'"+
					   " and a.managecom like '"+strAgentCom+"%%'"+
					   " and (select case when confdate is null then '0' else '1' end from ljaget"+
					" where actugetno = a.actugetno) = '"+strEdorState+"'"+
					" group by a.endorsementno,a.actugetno,a.grpcontno,riskcode,b.salechnl,b.grpname,"+
					" b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.actugetno,a.managecom"+
					" union all"+
					" select "+
					" a.serialno 批次号, a.getnoticeno 给付号, a.grpcontno 保单号, a.riskcode 险种,"+
					" (case  b.salechnl when '04' then '银保' when '13' then '银保' else '团险' end ) as 个团银种类,"+
					" b.grpname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					" (select confdate from ljaget where actugetno = a.getnoticeno) 给付确认日期,"+
					" (select confdate from ljaget where actugetno = a.getnoticeno) 财务付费日期,"+
					" sum(a.getmoney) 给付金额,"+
					"(select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.getnoticeno) 给付状态,"+ 
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称 "+
					" from LJSGetDraw a, lcgrpcont b "+
					" where a.grpcontno = b.grpcontno"+
					" and a.feeoperationtype = 'QB' and feefinatype <> 'YEI' and a.GetMoney <> 0"+
					" and a.getdate between '"+strStartDate+"' and '"+strEndDate+"'"+
					" and a.managecom like '"+strAgentCom+"%%'"+
					" and (select case when confdate is null then '0' else '1' end from ljaget"+
					" where actugetno = a.getnoticeno) = '"+ strEdorState+"'"+
					" group by a.serialno,a.getnoticeno,a.grpcontno,riskcode,b.salechnl,b.grpname,"+
					" b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.managecom"+
					" order by 给付号"+
					" with ur" ;
			  }
			  //全部
			  else if(strFlag==3)
			  {
			  		strSql="select"+
					" a.serialno 批次号, a.getnoticeno 给付号, a.contno 保单号, a.riskcode 险种,"+
					"(case  b.salechnl when '04' then '银保' when '13' then '银保' else '个险' end ) as 个团银种类,"+
					" b.appntname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					"(select confdate from ljaget where actugetno = a.getnoticeno) 给付确认日期,"+
					"(select confdate from ljaget where actugetno = a.getnoticeno) 财务付费日期,"+
					" sum(a.getmoney) 给付金额,"+
					"(select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.getnoticeno) 给付状态,"+
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称"+
					" from LJSGetDraw a, lccont b"+
					" where a.contno = b.contno"+
					" and a.feeoperationtype = 'EB' and feefinatype <> 'YEI' and a.GetMoney <> 0"+
					" and a.getdate between '"+ strStartDate+"' and '"+strEndDate+"'"+
					" and a.managecom like '"+strAgentCom+"%%'"+
					" and (select case when confdate is null then '0' else '1' end from ljaget" +
					" where actugetno = a.getnoticeno) = '"+strEdorState+"'"+
					" group by a.serialno,a.getnoticeno,a.contno,riskcode,b.salechnl,b.appntname,b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.managecom"+
					" union all"+
					
					" select"+ 
					" a.endorsementno 批次号, a.actugetno 给付号, a.grpcontno 保单号, a.riskcode 险种,"+
					" (case  b.salechnl when '04' then '银保' when '13' then '银保' else '团险' end ) as 个团银种类,"+
					" b.grpname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					" (select confdate from ljaget where actugetno = a.actugetno) 给付确认日期,"+
					" (select confdate from ljaget where actugetno = a.actugetno) 财务付费日期,"+
					" (select sum(sumgetmoney) from ljaget where actugetno = a.actugetno) 给付金额,"+
					" (select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.actugetno) 给付状态,"+
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称 "+
					" from LJAGetEndorse a ,lcgrpcont b"+
					 " where a.grpcontno = b.grpcontno"+
					   " and a.othernotype = '3'"+
					   " and a.feeoperationtype = 'MJ'"+
					   " and a.GetMoney <> 0"+
					   " and b.cinvalidate between'"+strStartDate+"'and '"+strEndDate+"'"+
					   " and a.managecom like '"+strAgentCom+"%%'"+
					   " and (select case when confdate is null then '0' else '1' end from ljaget"+
					" where actugetno = a.actugetno) = '"+strEdorState+"'"+
					" group by a.endorsementno,a.actugetno,a.grpcontno,riskcode,b.salechnl,b.grpname,"+
					" b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.actugetno,a.managecom"+
					" union all"+
					" select "+
					" a.serialno 批次号, a.getnoticeno 给付号, a.grpcontno 保单号, a.riskcode 险种,"+
					" (case  b.salechnl when '04' then '银保' when '13' then '银保' else '团险' end ) as 个团银种类,"+
					" b.grpname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					" (select confdate from ljaget where actugetno = a.getnoticeno) 给付确认日期,"+
					" (select confdate from ljaget where actugetno = a.getnoticeno) 财务付费日期,"+
					" sum(a.getmoney) 给付金额,"+
					"(select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.getnoticeno) 给付状态,"+ 
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称 "+
					" from LJSGetDraw a, lcgrpcont b "+
					" where a.grpcontno = b.grpcontno"+
					" and a.feeoperationtype = 'QB' and feefinatype <> 'YEI' and a.GetMoney <> 0"+
					" and a.getdate between '"+strStartDate+"' and '"+strEndDate+"'"+
					" and a.managecom like '"+strAgentCom+"%%'"+
					" and (select case when confdate is null then '0' else '1' end from ljaget"+
					" where actugetno = a.getnoticeno) = '"+ strEdorState+"'"+
					" group by a.serialno,a.getnoticeno,a.grpcontno,riskcode,b.salechnl,b.grpname,"+" b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.managecom"+
					" order by 给付号"+
					" with ur" ;
			  }
		 }
		 else//当给付状态为全部时则
		 {
		 		if(strFlag=="1")
			  {
			  		
				  strSql="select "+
					" a.serialno 批次号, a.getnoticeno 给付号, a.contno 保单号, a.riskcode 险种,"+
					"(case  b.salechnl when '04' then '银保' when '13' then '银保' else '个险' end ) as 个团银种类,"+
					" b.appntname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					"(select confdate from ljaget where actugetno = a.getnoticeno) 给付确认日期,"+
					"(select confdate from ljaget where actugetno = a.getnoticeno) 财务付费日期,"+
					" sum(a.getmoney) 给付金额,"+
					"(select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.getnoticeno) 给付状态,"+
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称"+
					" from LJSGetDraw a, lccont b"+
					" where a.contno = b.contno"+
					" and a.feeoperationtype = 'EB' and feefinatype <> 'YEI' and a.GetMoney <> 0"+
					" and a.getdate between '"+ strStartDate+"' and '"+strEndDate+"'"+
					" and a.managecom like '"+strAgentCom+"%%'"+
					" group by a.serialno,a.getnoticeno,a.contno,riskcode,b.salechnl,b.appntname,b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.managecom"+
					" order by a.getnoticeno"+
					" with ur";
			  }
			  //团险
			  else if(strFlag==2)
			  {
			  	 strSql="select "+ 
					" a.endorsementno 批次号, a.actugetno 给付号, a.grpcontno 保单号, a.riskcode 险种,"+
					" (case  b.salechnl when '04' then '银保' when '13' then '银保' else '团险' end ) as 个团银种类,"+
					" b.grpname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					" (select confdate from ljaget where actugetno = a.actugetno) 给付确认日期,"+
					" (select confdate from ljaget where actugetno = a.actugetno) 财务付费日期,"+
					" (select sum(sumgetmoney) from ljaget where actugetno = a.actugetno) 给付金额,"+
					" (select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.actugetno) 给付状态,"+
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称 "+
					" from LJAGetEndorse a ,lcgrpcont b"+
					 " where a.grpcontno = b.grpcontno"+
					   " and a.othernotype = '3'"+
					   " and a.feeoperationtype = 'MJ'"+
					   " and a.GetMoney <> 0"+
					   " and b.cinvalidate between'"+strStartDate+"'and '"+strEndDate+"'"+
					   " and a.managecom like '"+strAgentCom+"%%'"+
					" group by a.endorsementno,a.actugetno,a.grpcontno,riskcode,b.salechnl,b.grpname,"+
					" b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.actugetno,a.managecom"+
					" union all"+
					" select "+
					" a.serialno 批次号, a.getnoticeno 给付号, a.grpcontno 保单号, a.riskcode 险种,"+
					" (case  b.salechnl when '04' then '银保' when '13' then '银保' else '团险' end ) as 个团银种类,"+
					" b.grpname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					" (select confdate from ljaget where actugetno = a.getnoticeno) 给付确认日期,"+
					" (select confdate from ljaget where actugetno = a.getnoticeno) 财务付费日期,"+
					" sum(a.getmoney) 给付金额,"+
					"(select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.getnoticeno) 给付状态,"+ 
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称 "+
					" from LJSGetDraw a, lcgrpcont b "+
					" where a.grpcontno = b.grpcontno"+
					" and a.feeoperationtype = 'QB' and feefinatype <> 'YEI' and a.GetMoney <> 0"+
					" and a.getdate between '"+strStartDate+"' and '"+strEndDate+"'"+
					" and a.managecom like '"+strAgentCom+"%%'"+
					" group by a.serialno,a.getnoticeno,a.grpcontno,riskcode,b.salechnl,b.grpname,"+
					" b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.managecom"+
					" order by 给付号"+
					" with ur" ;
			  }
			  //全部
			  else if(strFlag==3)
			  {		
			  		strSql="select"+
					" a.serialno 批次号, a.getnoticeno 给付号, a.contno 保单号, a.riskcode 险种,"+
					"(case  b.salechnl when '04' then '银保' when '13' then '银保' else '个险' end ) as 个团银种类,"+
					" b.appntname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					"(select confdate from ljaget where actugetno = a.getnoticeno) 给付确认日期,"+
					"(select confdate from ljaget where actugetno = a.getnoticeno) 财务付费日期,"+
					" sum(a.getmoney) 给付金额,"+
					"(select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.getnoticeno) 给付状态,"+
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称"+
					" from LJSGetDraw a, lccont b"+
					" where a.contno = b.contno"+
					" and a.feeoperationtype = 'EB' and feefinatype <> 'YEI' and a.GetMoney <> 0"+
					" and a.getdate between '"+ strStartDate+"' and '"+strEndDate+"'"+
					" and a.managecom like '"+strAgentCom+"%%'"+
					" group by a.serialno,a.getnoticeno,a.contno,riskcode,b.salechnl,b.appntname,b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.managecom"+
					" union all"+
					
					" select"+ 
					" a.endorsementno 批次号, a.actugetno 给付号, a.grpcontno 保单号, a.riskcode 险种,"+
					" (case  b.salechnl when '04' then '银保' when '13' then '银保' else '团险' end ) as 个团银种类,"+
					" b.grpname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					" (select confdate from ljaget where actugetno = a.actugetno) 给付确认日期,"+
					" (select confdate from ljaget where actugetno = a.actugetno) 财务付费日期,"+
					" (select sum(sumgetmoney) from ljaget where actugetno = a.actugetno) 给付金额,"+
					" (select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.actugetno) 给付状态,"+
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称 "+
					" from LJAGetEndorse a ,lcgrpcont b"+
					 " where a.grpcontno = b.grpcontno"+
					   " and a.othernotype = '3'"+
					   " and a.feeoperationtype = 'MJ'"+
					   " and a.GetMoney <> 0"+
					   " and b.cinvalidate between'"+strStartDate+"'and '"+strEndDate+"'"+
					   " and a.managecom like '"+strAgentCom+"%%'"+
					" group by a.endorsementno,a.actugetno,a.grpcontno,riskcode,b.salechnl,b.grpname,"+
					" b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.actugetno,a.managecom"+
					" union all"+
					" select "+
					" a.serialno 批次号, a.getnoticeno 给付号, a.grpcontno 保单号, a.riskcode 险种,"+
					" (case  b.salechnl when '04' then '银保' when '13' then '银保' else '团险' end ) as 个团银种类,"+
					" b.grpname 投保人, b.signdate 承保日期, b.cvalidate 生效日期, b.cinvalidate 满期日,"+
					" a.makedate 给付抽档日期,"+
					" (select confdate from ljaget where actugetno = a.getnoticeno) 给付确认日期,"+
					" (select confdate from ljaget where actugetno = a.getnoticeno) 财务付费日期,"+
					" sum(a.getmoney) 给付金额,"+
					"(select case when confdate is null then '未给付' else '已给付' end from ljaget where actugetno = a.getnoticeno) 给付状态,"+ 
					" a.managecom 公司代码, (select name from ldcom where comcode = a.managecom) 公司名称 "+
					" from LJSGetDraw a, lcgrpcont b "+
					" where a.grpcontno = b.grpcontno"+
					" and a.feeoperationtype = 'QB' and feefinatype <> 'YEI' and a.GetMoney <> 0"+
					" and a.getdate between '"+strStartDate+"' and '"+strEndDate+"'"+
					" and a.managecom like '"+strAgentCom+"%%'"+
					" group by a.serialno,a.getnoticeno,a.grpcontno,riskcode,b.salechnl,b.grpname,"+" b.signdate,b.cvalidate,b.cinvalidate,a.makedate,a.managecom"+
					" order by 给付号"+
					" with ur" ;
			  }
		 	
		 }
	  }
	  else//当出现为空的情况时
	  {
  		  window.alert("请输入必填信息!");
  		  return false;
	  }

	 //查询SQL，返回结果字符串
	 turnPage.strQueryResult=easyQueryVer3(strSql, 1, 0, 1); 
	 
	 if(!turnPage.strQueryResult)
	 {
	 	window.alert("没有满足条件的记录!");
	 	return false;
	 }
	 else
	 {
	 	
	 	//成功拆分返回二维数组
	 	//var arrDataSet=decodeEasyQueryResult(turnPage.strQueryResult);
	 	//turnPage.arrDataCacheSet=arrDataSet;
	 	
	 	//以下为赋值结果
	 	//turnPage.pageDisplayGrid=DataGrid;
	 	//turnPage.strQueryResult=easyQueryVer3(strSql, 1, 0, 1); 
	 	//turnPage.strQuerySql=strSql;
	 	//
	 	
	 	//以下为提数结果
	 	//turnPage.pageIndex=0;
	 	//var arrResult=new Array();
	 	//var MAXLINE=10;
	 	//arrResult=turnPage.getData(turnPage.arrDataCacheSet,turnPage.pageIndex,MAXLINE);
	 	//diplayMultine(arrResult,turnPage.pageDisplayGrid);
	 	
	 	turnPage.queryModal(strSql,DataGrid);
	 }
	 
}

function easyPrint()
{	
	if( DataGrid.mulLineCount == 0)
	{
		window.alert("没有需要打印的信息");
		return false;
	}
	fm.all('strsql').value=strSql;
	fm.submit();
}
