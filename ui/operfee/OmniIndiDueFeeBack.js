//yangyalin

var showInfo;
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();

//查询报单信息
function queryCont()
{
  if(!checkContQuery())
  {
    return false;
  }
  
  var strSQL = "select distinct a.contNo,a.appntName,a.polApplyDate, a.payToDate, "
  				 + "	case a.AppFlag when '0' then '未签单' when '1' then '已签单' end, "
  				 + "	a.prem ,getUniteCode(a.AgentCode), b.name "
		       + "from LCCont a, LAAgent b "
           + "where 1=1 "
           + "    and a.agentCode = b.agentCode "
           + "    and exists (select 1 from lcpol where contno=a.contno and riskcode in (select riskcode from lmriskapp where riskcode not in ('332301','334801','340501','340601') and  risktype4='4')) "
           + "    and a.contType = '1' "
		       + getWherePart( 'a.contNo ','ContNo', "like") 
		       + getWherePart( 'a.AppntNo ','AppntNo', "like") 
		       + "    and a.AppFlag='1' "
		       + "    and not exists "
		       + "      (select 1 from LCContState "
		       + "      where contNo = a.contNo and polNo in('000000', '00000000000000000000') "
		       + "        and state = '1'"
		       + "        and (endDate is null or endDate > current Date)) "
		       + "order by contNo ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, ContGrid); 
  
  if(ContGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }
}

//校验查询条件是否正确
function checkContQuery()
{
  if(fm.ContNo.value == "" && fm.AppntNo.value == "")
  {
    alert("客户号和保单号不能同时为空");
    return false;
  }
  
  return true;
}

//查询续期实收记录信息
function queryLJAPay()
{
  var row = ContGrid.getSelNo() - 1;  //被选中的行数
  var contNo = ContGrid.getRowColDataByName(row, "contNo");
  
  queryAppAcc(contNo);
  
  var sql = "  select a.getNoticeNo, min(b.lastPayToDate), sum(b.sumActuPayMoney), "
            + "   (select sum(sumActuPayMoney) from LJSPayPersonB where getNoticeNo = a.getNoticeNo and payType = 'ZC'), "
            + "   min(b.curPayToDate), a.makeDate, max(c.enterAccDate), "
            + "   a.dealState, "
            + "   (select codeName from LDCode where codeType = 'dealstate' and code = a.dealState) "
            + "from LJSPayB a, LJSPayPersonB b, LJAPay c "
            + "where a.getNoticeNo = b.getNoticeNo "
            + "   and b.getNoticeNo = c.getNoticeNo "
            + "   and a.dealState = '1' "
            + "   and otherNoType = '2' "
            + "   and a.otherNo = '" + contNo + "' "
            + "group by a.getNoticeNo, a.makeDate, a.dealState "
            + "order by getNoticeNo desc ";
  turnPage3.pageDivName = "divPage3";
	turnPage3.queryModal(sql, LJAPayGrid); 
  
  if(LJAPayGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }  
  showCodeName();
}

//查询投保人账户余额
function queryAppAcc(contNo)
{
  var sql = "  select accBala "
            + "from LCAppAcc "
            + "where customerNo = "
            + "   (select appntNo from LCCont where contNo = '" + contNo + "') ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.AccBala.value = rs[0][0];
  }
}

//实收保费转出提交
function cancelRecord()
{
  if(!checkSubmit())
  {
    return false;
  }
    if(!checkIsBack())
  {
    return false;
  }
  if(!checkYueJie())
  {
    return false;
  }
  if(!checkXQ())
  {
    return false;
  }
  fm.GetNoticeNo.value = LJAPayGrid.getRowColDataByName(LJAPayGrid.getSelNo() - 1, "getNoticeNo");
  fm.tContNo.value  = ContGrid.getRowColDataByName(ContGrid.getSelNo()- 1,"contNo");
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
}

//正续期不能实收保费转出
function checkXQ()
{
    var selrow = ContGrid.getSelNo() - 1;  //被选中的行数
 	var selcontNo = ContGrid.getRowColDataByName(selrow, "contNo");
 	var payselrow = LJAPayGrid.getSelNo() - 1;  //实收被选中的行数
	var getnoticeno = LJAPayGrid.getRowColDataByName(payselrow, "getNoticeNo");
	var sqlXQ=" select * from ljspay where   otherno='"+selcontNo+"' and othernotype='2'";
	var xuQi = easyExecSql(sqlXQ);
	if(xuQi!=null&&xuQi!="null"&&xuQi!="")
	{
	  alert("此单正在续期，不能进行实收保费转出。");
	  return false;
	}
	return true;
}

//续期实收之后有月结不能实收保费转出
function checkYueJie()
{
    var selrow = ContGrid.getSelNo() - 1;  //被选中的行数
 	var selcontNo = ContGrid.getRowColDataByName(selrow, "contNo");
 	var payselrow = LJAPayGrid.getSelNo() - 1;  //实收被选中的行数
	var getnoticeno = LJAPayGrid.getRowColDataByName(payselrow, "getNoticeNo");
	var sqlYJ="select sum(money) from lcinsureacctrace where "
			+"((makedate>(select max(makedate) from lcinsureacctrace where otherno='"+getnoticeno+"' and contno='"+selcontNo+"' )) "
			+"or (makedate=(select max(makedate) from lcinsureacctrace where otherno='"+getnoticeno+"' and contno='"+selcontNo+"' ) and maketime>=(select max(maketime) from lcinsureacctrace where otherno='"+getnoticeno+"' and contno='"+selcontNo+"' ))) "
			+"and  othertype='6' and moneytype='LX' and contno='"+selcontNo+"' ";
	var yueJIE = easyExecSql(sqlYJ);
	if(yueJIE[0][0]!=0&&yueJIE[0][0]!=null&&yueJIE[0][0]!=""&&yueJIE[0][0]!="null")
	{
	  alert("此单实收后有万能月结，不能进行实收保费转出。");
	  return false;
	}
	return true;

}
//少儿险实收保费转出只能转出一次
//090616添加转出之前如果有保全则增加confirm提示
function checkIsBack()
{
	var selrow = ContGrid.getSelNo() - 1;  //被选中的行数
 	var selcontNo = ContGrid.getRowColDataByName(selrow, "contNo");
	var backsql = "select 1 from ljspayb a where dealstate='6' and otherno='"+selcontNo+"' "
	            + "and (select count(1) from ljspaypersonb where getnoticeno = a.getnoticeno and riskcode = '320106') >= 1 with ur";
	if(easyQueryVer3(backsql))
	{
	alert("此单已经做过实收保费转出,请勿重复操作");
	return false;
	}
	var payselrow = LJAPayGrid.getSelNo() - 1;  //实收被选中的行数
	var getnoticeno = LJAPayGrid.getRowColDataByName(payselrow, "getNoticeNo");
	var bqsql = "select distinct edorname from lmedoritem where edorcode in (select distinct edortype From lpedoritem a where contno ='"+selcontNo+"' " 
                   +  "and makedate>(select makedate from ljspayb where getnoticeno ='"+getnoticeno+"' ))";
    var bqinfo = easyExecSql(bqsql);
	if(bqinfo!=null)
	{
		var bqcontent="";
		for(i=0;i<bqinfo.length;i++)
		{
		bqcontent+=bqinfo[i][0]+",";
		}
	  if(confirm("该保单在该次收费后做过保全项目有:"+bqcontent+"实收保费转出有可能覆盖该操作,是否依然要继续?"))
	  {
	  return true;	
	  }
	  else
	  {
	  return false;	
	  }
	}  
    return true;
}
  
  
//检验提交数据的完整性
function checkSubmit()
{
  if(LJAPayGrid.mulLineCount == 0)
  {
    alert("没有需要转出的实收记录");
    return false;
  }
  if(LJAPayGrid.getSelNo() < 1)
  {
    alert("请选择实收记录进行转出");
    return false;
  }
  return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	  //queryLJAPay(); modify by fuxin 2008-5-6 9:49:52 IT 要求不查询
	  initForm();
	  fm.GetNoticeNo.value = "";
	  fm.tContNo.value = "";
  }
}