<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
	
    fm.all('AppntNo').value = '';
    fm.all('ContNo').value = '';
    fm.all('GetNoticeNo').value = '';
  
  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initGrpContGrid();
	initGrpJisPayGrid();
  }
  catch(re)
  {
    alert("GroupPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpContGrid()
  {                               
    var iArray = new Array();
      
      try
      {
		  iArray[0]=new Array();
		  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		  iArray[0][1]="30px";            		//列宽
		  iArray[0][2]=10;            			//列最大值
		  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[1]=new Array();
		  iArray[1][0]="保单号";         		//列名
		  iArray[1][1]="80px";            		//列宽
		  iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[2]=new Array();
		  iArray[2][0]="投保人";         		//列名
		  iArray[2][1]="150px";            		//列宽
		  iArray[2][2]=100;            			//列最大值
		  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[3]=new Array();
		  iArray[3][0]="投保日期";         		//列名
		  iArray[3][1]="70px";            		//列宽
		  iArray[3][2]=200;            			//列最大值
		  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[4]=new Array();
		  iArray[4][0]="交至日期";         		//列名
		  iArray[4][1]="70px";            		//列宽
		  iArray[4][2]=200;            			//列最大值
		  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[5]=new Array();
		  iArray[5][0]="保单状态";         		//列名
		  iArray[5][1]="60px";            		//列宽
		  iArray[5][2]=200;            			//列最大值
		  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许


		  iArray[6]=new Array();
		  iArray[6][0]="期交保费";         		//列名
		  iArray[6][1]="60px";            		//列宽
		  iArray[6][2]=200;            			//列最大值
		  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[7]=new Array();
		  iArray[7][0]="业务员";         		//列名
		  iArray[7][1]="90px";            		//列宽
		  iArray[7][2]=200;            			//列最大值
		  iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
		  
		  GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" ); 
		  //这些属性必须在loadMulLine前
		  GrpContGrid.mulLineCount = 0;   
		  GrpContGrid.displayTitle = 1;
		  GrpContGrid.locked = 1;
		  GrpContGrid.canSel = 1;
		  GrpContGrid.hiddenPlus = 1;
		  GrpContGrid.hiddenSubtraction = 1;
		  GrpContGrid.loadMulLine(iArray);  
		  GrpContGrid.selBoxEventFuncName ="afterJisPayQuery";      
      }
      catch(ex)
      {
        alert(ex);
      }
}


// 催收记录的初始化
function initGrpJisPayGrid()
  {                               
    var iArray = new Array();
      try
      {
		  iArray[0]=new Array();
		  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		  iArray[0][1]="30px";            		//列宽
		  iArray[0][2]=10;            			//列最大值
		  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[1]=new Array();
		  iArray[1][0]="应收记录号";         		//列名
		  iArray[1][1]="150px";            		//列宽
		  iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[2]=new Array();
		  iArray[2][0]="应缴时间";         		//列名
		  iArray[2][1]="100px";            		//列宽
		  iArray[2][2]=100;            			//列最大值
		  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[3]=new Array();
		  iArray[3][0]="应缴保费";         		//列名
		  iArray[3][1]="100px";            		//列宽
		  iArray[3][2]=200;            			//列最大值
		  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[4]=new Array();
		  iArray[4][0]="抽档时间";         		//列名
		  iArray[4][1]="80px";            		//列宽
		  iArray[4][2]=200;            		//列最大值
		  iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许

		  iArray[5]=new Array();
		  iArray[5][0]="处理状态";         		//列名
		  iArray[5][1]="100px";            		//列宽
		  iArray[5][2]=200;            	        //列最大值
		  iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

		  GrpJisPayGrid = new MulLineEnter( "fm" , "GrpJisPayGrid" ); 
		  //这些属性必须在loadMulLine前
		  GrpJisPayGrid.mulLineCount =0;   
		  GrpJisPayGrid.displayTitle = 1;
		  GrpJisPayGrid.canSel = 1;
		  GrpJisPayGrid.hiddenPlus = 1;
		  GrpJisPayGrid.hiddenSubtraction = 1;
		  GrpJisPayGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        alert("GroupPolQueryInit.jsp-->initGrpJisPayGrid函数中发生异常:初始化界面错误!");
      }
}

</script>