  <html> 
<%
//程序名称：GrpEndDateDeal.jsp
//程序功能：团单满期处理
//创建日期：20060901 
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="GrpEndDateDeal.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="GrpEndDateDealInit.jsp"%>
</head>
<body  onload="initForm();" >
  
<form name=fm  target=fraSubmit method=post>                        
  <div id = "test"></div>
<br></br>
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      <tr>
        <TD  class= title>
        保单到期开始时间:
        </TD>
        <TD  class= input>
          <Input class="coolDatePicker" elementtype="nacessary" dateFormat="short" name=StartDate verify="开始时间|notnull">
        </TD>
        <TD  class= title>
        保单到期结束时间
        </TD>
        <TD  class= input >
          <Input class="coolDatePicker" elementtype="nacessary" dateFormat="short" name=EndDate verify="结束时间|notnull">
        </TD>          
        <TD  class= input colspan="2">
          <INPUT class=cssButton VALUE="保单明细"  TYPE=button onclick="contDetail();">
        </TD>
	    </tr>  
    	<TR  class= common>
        <TD  class= title>
          管理机构
        </TD>
        <TD class= input>
        	<Input class="codeNo" name="ManageCom" elementtype="nacessary"  readOnly verify="上级分类|notnull" ondblclick="return showCodeList('comcode',[this,ManageComName], [0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName], [0,1]);" ><Input class="codeName" name="ManageComName" readonly >
        </TD>     	
        <TD  class= title>
          业务员代码
        </TD>
        <TD  class= input>
          <Input class="common" name="AgentCode">
        </TD>
        <TD  class= title>
          保单号
        </TD>
        <TD  class= input>
          <Input class="common" name="GrpContNo">
        </TD> 
      </TR>
      <TR  class= common>
        <TD  class= title>
          包含险种
        </TD>
        <TD  class= input>
          <input class="codeNo" name="RiskCode1" readonly ondblclick="return showCodeList('riskgrp', [this,RiskCode1Name], [0,1]);" onkeyup="return showCodeListKey('riskgrp', [this,RiskCode1Name],[0,1]);"><input class="codename" name="RiskCode1Name" >
        </TD>
        <TD  class= title>
          险种组合
        </TD>
        <TD class= input>
          <input class="codeNo" name="Condition" value="1" readonly CodeData="0|^1|或^2|和" ondblclick="return showCodeListEx('Condition', [this,ConditionName], [0,1]);" onkeyup="return showCodeListKey('Condition', [this,ConditionName],[0,1]);"><input class="codename" name="ConditionName" value="或">
        </TD>   
        <TD  class= title>
          包含险种
        </TD>
        <TD  class= input>
          <input class="codeNo" name="RiskCode2" readonly ondblclick="return showCodeList('riskgrp', [this,RiskCode2Name], [0,1]);" onkeyup="return showCodeListKey('riskgrp', [this,RiskCode2Name],[0,1]);"><input class="codename" name="RiskCode2Name" >
        </TD>        
      </TR>
      <TR  class= common>
        <TD  class= input>
          <INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="queryGrpCont();">
        </TD>
      </TR>        
    </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpEndDate);">
    		</td>
    		<td class= titleImg>
    			 团险满期保单清单：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divGrpEndDate" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpEndDateGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
  	</div>
  	<Div id= "divPage2" align="center" style= "display: 'none' ">
	    <center>    	
	      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">			
	    </center> 	
    </Div>
    
    <INPUT class=cssButton VALUE="打印清单"  TYPE=button onclick="printList();">
    <!--INPUT class=cssButton VALUE="选择打印通知书"  TYPE=button onclick="printMult();"-->
    <INPUT class=cssButton VALUE="选择打印通知书"  TYPE=button onclick="printMultNew();">
    <INPUT class=cssButton VALUE="全部打印通知书"  TYPE=button onclick="printAll();">
    
    
    <INPUT type= "hidden" name= "UserManageCom" value= "">
    <INPUT type= "hidden" name= "Sql" value= "">
    <INPUT type= "hidden" name= "Sql1" value= "">
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>
</body>
</html>
