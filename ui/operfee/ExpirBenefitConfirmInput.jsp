<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ExpirBenefitConfirmInput.jsp
//程序功能：给付确认页面
//创建日期：2010-05-07 16:06
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	
  <SCRIPT src="ExpirBenefitConfirmInput.js"></SCRIPT>
  <%@include file="./ExpirBenefitConfirmInit.jsp"%>
</head>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate();   
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
  String AheadDays="30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  String SubDate=tD.getString(AfterDate);  
  String tSubYear=StrTool.getVisaYear(SubDate);
  String tSubMonth=StrTool.getVisaMonth(SubDate);
  String tSubDate=StrTool.getVisaDay(SubDate);               	               	
%>

<SCRIPT>
  var CurrentYear=<%=tCurrentYear%>;  
  var CurrentMonth=<%=tCurrentMonth%>;  
  var CurrentDate=<%=tCurrentDate%>;
  var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
  var SubYear=<%=tSubYear%>;  
  var SubMonth=<%=tSubMonth%>;  
  var SubDate=<%=tSubDate%>;
  var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
  var managecom = <%=tGI.ManageCom%>;
  var operator = '<%=tGI.Operator%>';
  var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
  var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
</SCRIPT> 

<body  onload="initForm();" >
<form action="ExpirBenefitConfirmSubmit.jsp" method=post name="fm" target="fraSubmit">
  <table>
    <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        给付保单查询：
      </td>
     </tr>
   </table>
   <Div  id= "divAlivePayMulti" style= "display: ''">
   <table  class= common align=center>
      <tr  class= common>
        <td class= title>满期日起期</td>
      	<td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      	<td class= title>满期日止期</td>
      	<td class= input><Input class="coolDatePicker" dateFormat="short" name=EndDate ></td>
        <td class= title>管理机构</td>
      	<td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><Input class=codename  name=ManageComName readonly></td>
      </tr>
      <tr  class= common>
      	<td  class= title>给付任务号</td>
        <td  class= input><input class= common name="EdorAcceptNo" ></td>
        <td class= title>保单号</td>
      	<td class= input><Input class= common name=ContNo></td>	
      	<td class= title>给付任务状态</td>
      	<td class= input><Input class= "codeno"  name=PayTaskState CodeData="0|^0|待给付^1|给付完成^2|待给付确认" verify="给付状态|notnull" ondblclick="return showCodeListEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PayTaskStateName></td>
      </tr>
      <tr>
      	  <td class= title>选择类型</td>
		  <td class= input><input class="codeNo" name="queryType" value="1" CodeData="0|^1|请选择类型^2|普通单^3|少儿单^4|常无忧B" readOnly ondblclick="return showCodeListEx('queryType',[this,queryTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListEx('queryType',[this,queryTypeName],[0,1],null,null,null,1);" readonly><Input class="codeName" name="queryTypeName" readonly></td>
		  <td  class= title>险种</td>
          <td  class= input><Input class= "codeno"  name=RiskCode1  ondblclick="return showCodeList('cangetrisk',[this,RiskName1],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('cangetrisk',[this,RiskName1],[0,1],null,null,null,1);" ><Input class=codename readonly name=RiskName1 >
          <td  class= title>是否审核通过</td>
          <td  class= input><Input class= "codeno"  name=CheckState  value = 0 CodeData="0|^0|全部^1|是^2|否" ondblclick="return showCodeListEx('CheckState',[this,CheckStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListEx('CheckState',[this,CheckStateName],[0,1],null,null,null,1);" ><Input class=codename readonly name=CheckStateName >
	  </tr>
      <tr  class= common>
        <td><Input class = cssbutton VALUE="查 询" TYPE=button onclick="queryClick();"> </td>
      </tr>
    </table>
    </Div>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    			 给付任务清单：
    		</td>
    	</tr>
    </table>
  <div  id= "divJisPay" style= "display: ''">
   	<table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1><span id="spanLjsGetGrid"></span></td>
  	  </tr>
  	  <tr>
  		<td>  					
  		  <center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
     			<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
    	  </center>
    	</td>
      </tr>
    </table>
  </div> 
  
  <br>
  <div align=left id="divLjsGetDraw" style="display: '' ">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divLjsGetDrawGrid);">
    		</td>
    		<td class= titleImg>
    			 给付明细：
    		</td>
    	</tr>
    </table>
    <div align=left id="divLjsGetDrawGrid" style="display: '' ">
      <table>
        <tr  class= common>
          <td text-align: left colSpan=1>
  		    <span id="spanLjsGetDrawGrid" ></span> 
  	      </td>
  	    </tr>
      </table>
    </div>
  </div>
  <br>
  
  <div align=left id="divDrawer" style="display: 'none' ">
  <table  class= common align=center>    		
  	<tr >
	  <td class= title colspan = 3>申请人类型
	    <input class="codeNo" name="Handler" value='0' CodeData="0|^0|投保人^1|委托代办^2|被保人" 
	           ondblclick="return showCodeListEx('handler',[this,HandlerName],[0,1],null,null,null,1);" 
	           onkeyup="return showCodeListKeyEx('handler',[this,HandlerName],[0,1],null,null,null,1);"><Input class="codeName" name="HandlerName" readonly>
	    <font color='red'>如果申请人类型为委托代办且领取金额大于1000元，则满期给付确认后发送短信至投保人、被保险人。</font>
	  </td> 
  	</tr>
  	<tr>
	  <td class= title>&nbsp给付对象&nbsp
	    <input class="codeNo" name="DrawerCode" CodeData="0|^0|投保人^1|被保人^2|转入投保人账户 " 
	           ondblclick="return showCodeListEx('DrawerCode',[this,Drawer],[0,1],null,null,null,1);" 
	           onkeyup="return showCodeListKeyEx('DrawerCode',[this,Drawer],[0,1],null,null,null,1);"><Input class="codeName" name="Drawer" readonly>
	  </td>
  	</tr>
  </table>  
  </div>
  
  <div align=left id="divAppntInfo" style="display: 'none' ">
    <hr>
    <table class= common align=center>
      <tr  class= common>
      	<td class= title>投保人客户号</td>
        <td class= input><input class= common name="AppntNoInfo" readonly></td>
        <td class= title>投保人账户余额</td>
      	<td class= input><Input class= common name="AccBalaInfo" readonly></td>	
      	<td class= title>投保人姓名</td>
      	<td class= input><Input class= common name="AppntNameInfo" readonly></td>	
      </tr>
      <tr  class= common>
      	<td class= title>投保人性别</td>
        <td class= input><input class= common name="AppntSexInfo" readonly></td>
        <td class= title>投保人生日</td>
      	<td class= input><Input class= common name="AppntBirthDayInfo" readonly></td>	
      </tr>
      <tr  class= common>
      	<td class= title>投保人证件类型</td>
        <td class= input><input class= common name="AppntIDTypeInfo" readonly></td>
        <td class= title>投保人证件号码</td>
      	<td class= input><Input class= common name="AccBaIDNoInfo" readonly></td>	
      </tr>
    </table>
    <hr>
  </div>
  
  <div align=left id="divCash" style="display: 'none' ">
    <hr>
    <table class= common align=center>
      <tr>
        <td colspan = 6>
          <font color="#FF0000">您的给付方式为“现金”</font>
        </td>
      </tr>
    </table>
    <hr>
  </div>
  
  <div align=left id="divOther" style="display: 'none' ">
    <hr>
    <table class= common align=center>
      <tr>
        <td colspan = 6>
          <font color="#FF0000">您的给付方式为“其它”</font>
        </td>
      </tr>
      <tr  class= common>
  	    <td class= title>开户银行</td>
    	<td class= input><Input class=readonly name=BankCodeShow1 style="width: 60" readonly ><input class=readonly name=BankNameShow1 readonly style="width: 140" readonly></td>
    	<td class= title>账户名</td>
    	<td class= input><Input class=readonly name=AccNameShow1 style="width: 180" readonly></td>
    	<td class= title>账号</td>
    	<td class= input><Input class=readonly name=BankAccNoShow1 style="width: 180" readonly></td>
      </tr>
    </table>
    <hr>
  </div>
    
  <div align=left id="divBank" style="display: 'none' ">
    <hr>
    <table class= common align=center>
      <tr>
        <td colspan = 6>
          <font color="#FF0000">您的给付方式为“银行转账”</font>
        </td>
      </tr>
  	  <tr  class= common>
  	    <td class= title>开户银行</td>
    	<td class= input><Input class=readonly name=BankCodeShow style="width: 60" readonly ><input class=readonly name=BankNameShow readonly style="width: 140" readonly></td>
    	<td class= title>账户名</td>
    	<td class= input><Input class=readonly name=AccNameShow style="width: 180" readonly></td>
    	<td class= title>账号</td>
    	<td class= input><Input class=readonly name=BankAccNoShow style="width: 180" readonly></td>
      </tr>   
    </table>
    <hr>
  </div>
  
  <div align=left id="divCalData" style="display: 'none' ">
  <table  class= common align=center>    		
  	<tr >
	  <td class= title>选择公式
	    <input class="codeNo" name="Formula" value='3' CodeData="0|^2|公式二^3|公式三^4|公式四^5|公式五" 
	           ondblclick="return showCodeListEx('Formula',[this,FormulaName],[0,1],null,null,null,1);" 
	           onkeyup="return showCodeListKeyEx('Formula',[this,FormulaName],[0,1],null,null,null,1);"><Input class="codeName" name="FormulaName" readonly>
	    <input type="checkbox" name="RateFlag" > 息降我不降 &nbsp&nbsp&nbsp&nbsp
	    <input class = cssbutton value="试算确认" type=button onclick="calData();">
	  </td>
	</tr>
	<tr>
	  <td>
	    <font color='red'>试算后，本次抽档金额按照试算结果进行修改。</font>
	  </td> 
  	</tr>
  </table>  
  <hr>
  </div>
  
  <div align=left id="divPayMode" style="display: 'none' ">
    <table class= common align=center>
      <tr class= common>
        <td class= title>给付方式</td>
	    <td class= input><input class="codeNo" name="PayMode" CodeData="0|^1|现金^4|银行转账^9|其它" ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);"><Input class="codeName" name="PayModeName" readonly></td>
        <td class= title>开户银行</td>
      	<td class= input><Input class= "codeno"  name="BankCode" ondblclick="return showCodeList('Bank',[this,BankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankName],[0,1],null,null,null,1);"><Input class=codename  name=BankName readonly ></td>
      	<td class= title>账户名</td>
      	<td class= input><Input class= common name="AccName" readonly></td>	
      </tr>
      <tr>
      	<td  class= title>银行帐号</td>
        <td  class= input><input class= common name="BankAccNo" ></td>
        <td  class= title>再次录入银行帐号</td>
        <td  class= input><input class= common name="BankAccNoConf" ></td>
        <td  class= input colspan = 2><input class = cssbutton value="变更确认" type=button onclick="changeConfirm();"></td>
      </tr>   
    </table>
    <hr>
  </div>	
  <table>
	<tr>
		<td class = common>
			<input class = cssbutton value="给付确认" id="DealButton" type=button onclick="dealData();">
			<input class = cssbutton value="打印批单" id="DealButton" type=button onclick="printData();">
		<!-- 	<input class = cssbutton value="给付试算" type=button onclick="openCalData();">  -->
			<input class = cssbutton value="变更给付方式" type=button onclick="changePayMode();">
			<input class = cssbutton value="给付信息审核" type=button onclick="checkConfirmData();"><font color='red'> 按钮【给付信息审核】只对常无忧（B款）生效。</font>
		</td>
	</tr>
  </table>
    <input type=hidden id="GetNoticeNo" name="GetNoticeNo">
    <input type=hidden id="PayModeShow" name="PayModeShow">
    <input type=hidden id="SerialNo" name="SerialNo">
    <input type=hidden id="TempContNo" name="TempContNo">
	<input type=hidden id="InsuredNo" name="InsuredNo">
	<input type=hidden id="AppntNo" name="AppntNo">
	<input type=hidden id="GetState" name="GetState">
	<input type=hidden id="RateFlagValue" name="RateFlagValue">
	<input type=hidden id="RiskCode" name="RiskCode">
	<input type=hidden id="Today" name="Today">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
