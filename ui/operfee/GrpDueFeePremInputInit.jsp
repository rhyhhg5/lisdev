<% 
//程序名称：GrpDueFeePremInpuInit.jsp
//程序功能：无名单续期
//创建日期：20060712
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String grpContNo = request.getParameter("grpContNo");
%>

<script language="JavaScript">

//初始化页面信息
function initForm()
{
  fm.grpContNo.value = "<%=grpContNo%>";
  initLCContPlan();
  queryLCContPlan();
}

//初始化保障计划信息列表
function initLCContPlan()
{
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="30px";            		//列宽
    iArray[1][2]=30;            			//列最大值
    iArray[1][3]=3;
    iArray[1][21] = "grpContNo";
    
    iArray[2]=new Array();
    iArray[2][0]="保障计划";         		//列名
    iArray[2][1]="30px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21] = "contPlanCode";
    
    iArray[3]=new Array();
    iArray[3][0]="人员类别";         		//列名
    iArray[3][1]="100px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21] = "contPlanName";
    
    iArray[4]=new Array();
    iArray[4][0]="被保人数";         		//列名
    iArray[4][1]="50px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21] = "peoples2";
    
    iArray[5]=new Array();
    iArray[5][0]="期交保费";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21] = "prem";
    
    iArray[6]=new Array();
    iArray[6][0]="交费人数";         		//列名
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21] = "peoples2Input";
      
    LCContPlan = new MulLineEnter( "fm" , "LCContPlan" ); 
    //这些属性必须在loadMulLine前
    LCContPlan.mulLineCount = 0;
    LCContPlan.displayTitle = 1;
    LCContPlan.locked = 1;
    LCContPlan.canSel = 0;
    LCContPlan.canChk = 1;
    LCContPlan.hiddenSubtraction = 1;
    LCContPlan.hiddenPlus = 1;
    LCContPlan.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex.message);
  }
}

</script>