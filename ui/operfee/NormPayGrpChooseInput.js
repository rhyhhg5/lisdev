
var turnPage = new turnPageClass();
var showInfo;

// 普通缓存查询
function easyQueryClick() {
/*	
  var strSql = "select SysVar, SysVarType, SysVarValue from ldsysvar where 1=1 "
	       + getWherePart( 'SysVar' )
	       + getWherePart( 'SysVarType' )
	       + getWherePart( 'SysVarValue' );
*/
  var strSql="";
  strSql="select Prem,PaytoDate,ManageFeeRate,GrpName from LCGrpPol where GrpContNo='"+fm.all('GrpContNo').value+"' and payintv=-1";   
  var arrResult=easyExecSql(strSql);
  if(arrResult==null)
  {
  alert("无符合条件的集体保单：请确认是续期不定期缴费");
  NormPayGrpChooseGrid.clearData("NormPayGrpChooseGrid"); 
  return false;	
  }
  fm.all('SumDuePayMoney').value=arrResult[0][0]||"";
  fm.all('PaytoDate').value=arrResult[0][1]||"";
  fm.all('ManageFeeRate').value=arrResult[0][2]||"";
  fm.all('GrpName').value=arrResult[0][3]||"";    
  
  strSql = "select LCPrem.PolNo,LCPrem.DutyCode,LCPrem.PayPlanCode,LCPrem.Prem,LCPrem.Prem ,LJSPayPerson.InputFlag  from LCPrem,LJSPayPerson,LCPol where LCPrem.GrpContNo='"+fm.all('GrpContNo').value+"' ";
  strSql =strSql+" and (LCPrem.UrgePayFlag='N' or LCPrem.UrgePayFlag is null) ";			
  strSql =strSql+" and LCPrem.PolNo=LJSPayPerson.PolNo";
  strSql =strSql+" and LCPrem.PolNo=LCPol.PolNo";
  strSql =strSql+" and LCPrem.DutyCode=LJSPayPerson.DutyCode";
  strSql =strSql+" and LCPrem.PayPlanCode=LJSPayPerson.PayPlanCode";
  strSql =strSql+" UNION ";
  strSql =strSql+"select LCPrem.PolNo,LCPrem.DutyCode,LCPrem.PayPlanCode,LCPrem.Prem,LCPrem.Prem ,'0' from LCPrem,lcpol where LCPrem.GrpContNo='"+fm.all('GrpContNo').value+"' ";
  strSql =strSql+" and (UrgePayFlag='N' or UrgePayFlag is null) ";
  strSql =strSql+" and LCPrem.PolNo=LCPol.PolNo";		
  strSql =strSql+" and 0=(select count(*) from LJSPayPerson where PolNo=LCPrem.PolNo and DutyCode=LCPrem.DutyCode and PayPlanCode=LCPrem.PayPlanCode)";
  strSql =strSql+" order by 6 desc"	
    

	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    NormPayGrpChooseGrid.clearData("NormPayGrpChooseGrid");  	
    alert("没有查询到数据！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = NormPayGrpChooseGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function fmSubmit()
{
    if(checkValue())
    {   
    var i = 0;
    fm.all('SaveGrpContNo').value=fm.all('GrpContNo').value;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
    fm.submit();    
    }
    	
}

function checkValue()
{
   if(!verifyInput())
     return false;
  
   return true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
//    NormPayGrpChooseGrid.clearData("NormPayGrpChooseGrid");   
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}   
        
//提交当前页数据
function submitCurData()
{

  if(NormPayGrpChooseGrid.mulLineCount==0)
    alert("当前页没有可以提交的数据");
  else{      
      fmSubmit();
      }
}

//提交所有选中数据，后台事务处理
function verifyChooseRecord()
{
    if(checkValue())
    { 
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
    fmSubmitAll.all('SubmitGrpContNo').value=fm.all('GrpContNo').value;
    fmSubmitAll.all('SubmitPayDate').value=fm.all('PayDate').value;
    fmSubmitAll.all('SubmitManageFeeRate').value=fm.all('ManageFeeRate').value;         
    fmSubmitAll.submit();
    }
}

//直接提交所有数据，后台事务处理
function submitCurDataAll()
{
    var i = 0;    
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   // alert("hello");
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
    fmSaveAll.all('SaveAllSubmitGrpContNo').value=fm.all('GrpContNo').value;
    fmSaveAll.submit();	
 	
}
//查询数据
function queryRecord()
{
   if(checkValue())
   { 
    //fm.all('PolNo').value=fm.all('GrpContNo').value;	  
    easyQueryClick();
//    fmQuery.submit();
   }
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	
	try
	{
		arrReturn = getQueryResult();
		top.opener.afterQuery( arrReturn );
	}
	catch(ex)
	{
		alert( "没有发现父窗口的afterQuery接口。" + ex );
	}
	top.close();
}

