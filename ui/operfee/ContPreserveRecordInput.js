  //               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var showInfo;
var strSql="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{	
	initContPreserveGrid();
	//判断关键定是否为空
	var strAgentCom=trim(document.fm.all("ManageCom").value);
	var riskCode=trim(document.fm.all("riskCode3").value);//险种号

	var StartDate = trim(document.fm.all("StartDate").value);
	var EndDate = trim(document.fm.all("EndDate").value);
	var strSQL1 ="";
	//初始化防止出错
	if(strAgentCom==null||strAgentCom=='')
	{
		window.alert("请选择查询机构");
		return false;
	}
	if(riskCode==null||riskCode=='')
	{
		window.alert("请选择查询险种");
		return false;
	}else if(riskCode=='0'){
		 strSQL1 = " and lcp.riskcode in ('333701','332601','333001') "; 
	}else if(riskCode=='1'){
		strSQL1 = " and lcp.riskcode in ('333701','332601') "; 
	}else{
		strSQL1 = "  and lcp.riskcode = '"+riskCode+"' ";
	}
	
			  //查询strSql语句如下
				  strSql="select (select name from ldcom where comcode=managecom), " +
						" (case when '"+riskCode+"'='0' then '' when '"+riskCode+"'='1' then '' else  "+
						" (select riskname from lmriskapp where riskcode='"+riskCode+"') end) as riskcode,  "+
				  		" sum(prem),sum(count),sum(remainPrem),sum(remainCount), "+
					" sum(edorZTPrem),sum(edorZTCount),sum(remainPrem)-sum(edorZTPrem),sum(remainCount)-sum(edorZTCount),sum(remainEndPrem),sum(remainEndCount) "+
					" from (select lcc.managecom as managecom, "+
					" sum(case when ((lcc.cvalidate + 1 years) between '"+StartDate+"' and '"+EndDate+"') then lcc.prem else 0 end ) as prem , "+
					" sum(case when ((lcc.cvalidate + 1 years) between '"+StartDate+"' and '"+EndDate+"') then 1 else 0 end ) as count, "+
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and lct.applydate between '"+StartDate+"' and '"+EndDate+"' )=1 then lcc.prem else 0 end) as remainPrem, "+
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and lct.applydate between '"+StartDate+"' and '"+EndDate+"')=1 then 1 else 0 end) as remainCount, " +
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and remainstate='2' and remainenddate between '"+StartDate+"' and '"+EndDate+"')=1 then lcc.prem else 0 end) as remainEndPrem, "+
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and remainstate='2' and remainenddate between '"+StartDate+"' and '"+EndDate+"')=1 then 1 else 0 end) as remainEndCount, "+
					" 0 as edorZTPrem,0 as edorZTCount,1 as a "+
					" from lccont lcc inner join lcpol lcp on lcc.prtno = lcp.prtno "+
					" where lcc.conttype = '1' and lcc.appflag='1'  " +
					strSQL1 +
					" and lcc.managecom like '"+strAgentCom+"%' " +
					" group by lcc.managecom,lcc.contno,1  " +
					" union all " +
					" select lcc.managecom as managecom, " +
					" sum(case when ((lcc.cvalidate + 1 years) between '"+StartDate+"' and '"+EndDate+"') then lcc.prem else 0 end ) as prem , "+
					" sum(case when ((lcc.cvalidate + 1 years) between '"+StartDate+"' and '"+EndDate+"') then 1 else 0 end ) as count,  "+
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and applydate between '"+StartDate+"' and '"+EndDate+"' )=1 then lcc.prem else 0 end) as remainPrem, "+
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and applydate between '"+StartDate+"' and '"+EndDate+"')=1 then 1 else 0 end) as remainCount, "+
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and remainstate='2' and remainenddate between '"+StartDate+"' and '"+EndDate+"')=1 then lcc.prem else 0 end) as remainEndPrem,  "+
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and remainstate='2' and remainenddate between '"+StartDate+"' and '"+EndDate+"')=1 then 1 else 0 end) as remainEndCount, " +
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and remainstate='3' and edorztdate between '"+StartDate+"' and '"+EndDate+"')=1 then lcc.prem else 0 end) as edorZTPrem, "+
					" sum(case when (select 1 from lccontremain lct where lct.contno=lcc.contno and remainstate='3' and edorztdate between '"+StartDate+"' and '"+EndDate+"')=1 then 1 else 0 end) as edorZTCount, "+
					" 2 as a from lbcont lcc inner join lbpol lcp on lcc.prtno = lcp.prtno "+
					" where lcc.conttype = '1' and lcc.appflag='1'  "+
					strSQL1 +
					" and lcc.managecom like '"+strAgentCom+"%'  "+
					" group by lcc.managecom,lcc.contno,2) f "+
					" where f.count+f.remainCount+f.remainEndCount+f.edorZTCount>0 " +
					"  group by  f.managecom with ur  ";
			


	 //查询SQL，返回结果字符串
	 turnPage.strQueryResult=easyQueryVer3(strSql, 1, 0, 1); 
	 fm.StatisticsStartDate.value=StartDate;
	 fm.StatisticsEndDate.value=EndDate;
	 if(!turnPage.strQueryResult)
	 {
	 	window.alert("没有满足条件的记录!");
	 	return false;
	 }
	 else
	 {
		 initContPreserveGrid();
	 	turnPage.queryModal(strSql,ContPreserveGrid);
	 }
}



function afterSubmit(FlagStr, content)
{
  try{ showInfo.close(); } catch (e) {};
  window.focus();     
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
}

function easyPrint()
{	
	if( ContPreserveGrid.mulLineCount == 0)
	{
		window.alert("没有需要打印的信息");
		return false;
	}
	fm.all('strsql').value=strSql;
	fm.submit();
}
