
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
  if(!checkSubmit(fm.ywtype.value))
  {
    return false;
  }
  if(!beforeSubmit())
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ; 
  fm.action="LJSUnlockSave.jsp";
  fm.submit(); //提交
}

function beforeSubmit()
{
  fm.all('Drawer3').value = trim(fm.all('Drawer3').value);
  fm.all('DrawerID3').value = trim(fm.all('DrawerID3').value);
  fm.all('BankAccNo3').value = trim(fm.all('BankAccNo3').value);
  fm.all('AccName3').value = trim(fm.all('AccName3').value);
  fm.all('Drawer4').value = trim(fm.all('Drawer4').value);
  fm.all('DrawerID4').value = trim(fm.all('DrawerID4').value);
  fm.all('BankAccNo4').value = trim(fm.all('BankAccNo4').value);
  fm.all('AccName4').value = trim(fm.all('AccName4').value);
  return true;
}
//提交后操作,服务器数据返回后执行的操作
//业务类型，BQPAY保全收费，BQGET保全付费，XQPAY续期收费，XQGET续期付费(满期给付)
function afterSubmit(FlagStr, content, transact)
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    easyQueryClick(transact);
    resetDisplay(transact); 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//Click事件，当点击“查询”按钮时触发该函数
function queryClick(flag)
{ 
  if(!beforeQuery(flag))
  {
    return false;
  }
  if(!easyQueryClick(flag))
  {
    return false;
  }
  resetDisplay(flag);
  if(!returnQuery(flag))
  {
    return false;
  }
}

function easyQueryClick(flag) 
{
  if(flag=="XQPAY")
  {
    fm.ManageComMul.value = fm.ManageCom3.value;
    fm.ContNoMul.value = fm.ContNo3.value;
    fm.GetNoticeNoMul.value = fm.GetNoticeNo3.value;
    initLJSPayXQGrid();
    var strSql = "select (select Name from LDCom where ComCode = a.ManageCom),  "
               + "OtherNo, GetNoticeNo, SumDuePayMoney, AccName, "
               + "(select BankName from LDBank where BankCode = a.BankCode), "
               + "(select max(SendDate) from LYReturnFromBankB where PayCode=a.GetNoticeNo" +
               	" union  select max(SendDate) from LYReturnFromBank where PayCode=a.GetNoticeNo fetch first 1 rows only)," +
               		" SendBankCount,(select bank_reason(GetNoticeNo) from dual) "
               + " from LJSPay a "
               + "where 1=1 "
               + getWherePart('ManageCom','ManageCom3','like')
	           + getWherePart('OtherNo','ContNo3','=')
               + getWherePart('GetNoticeNo', 'GetNoticeNo3','=')
               + " and CanSendBank = '1' and OtherNoType = '2' and SendBankCount >= 1 and getnoticeno like '31%' "
               + "order by GetNoticeNo with ur";
    turnPage3.pageDivName = "divPage3";
    turnPage3.queryModal(strSql, LJSPayXQGrid);
    if(LJSPayXQGrid.mulLineCount == 0)
    {
      document.all("divPAYMUL").style.display = "none";
    }
    else
    {
      document.all("divPAYMUL").style.display = "";
    }
  }
  else if(flag=="XQGET")
  {
    fm.ManageComMul.value = fm.ManageCom4.value;
    fm.ContNoMul.value = fm.ContNo4.value;
    fm.GetNoticeNoMul.value = fm.ActuGetNo4.value;
    initLJAGetXQGrid();
    var strSql = "select (select Name from LDCom where ComCode = a.ManageCom),  "
               + "substr(otherno,2,length(otherno)-5), ActuGetNo, SumGetMoney, AccName, "
               + "(select BankName from LDBank where BankCode = a.BankCode), "
               + "(select max(SendDate) from LYReturnFromBankB where PayCode=a.ActuGetNo)," +
               		" SendBankCount,(select bank_reason(ActuGetNo) from dual) "
               + " from LJAGet a "
               + "where 1=1 "
               + getWherePart('ManageCom','ManageCom4','like')
	           + getWherePart('substr(otherno,2,length(otherno)-5)','ContNo4','=')
               + getWherePart('ActuGetNo', 'ActuGetNo4','=')
               + " and CanSendBank = '1' and OtherNoType = '20' and SendBankCount >= 1 and PayMode = '4' "
               + "order by GetNoticeNo with ur";
    turnPage4.pageDivName = "divPage4";
    turnPage4.queryModal(strSql, LJAGetXQGrid);
    if(LJAGetXQGrid.mulLineCount == 0)
    {
      document.all("divGETMUL").style.display = "none";
    }
    else
    {
      document.all("divGETMUL").style.display = "";
    }
  }
  else
  {
    alert("业务类型错误！")
    return false;
  }
  return true;
}

function returnQuery(flag)
{
  if(flag=="XQPAY")
  {
    if(LJSPayXQGrid.mulLineCount == 0)
    {
      alert("没有查询到数据");
      return false;
    }
  }
  else if(flag=="XQGET")
  {
    if(LJAGetXQGrid.mulLineCount == 0)
    {
      alert("没有查询到数据");
      return false;
    }
  }
  else
  {
    alert("业务类型错误！")
    return false;
  }
  return true;
}

function selectOne3() 
{
  var arrReturn = getQueryResult3();
  afterQuery3(arrReturn);	          		  
}

function getQueryResult3()
{
  var arrSelected = null;
  tRow = LJSPayXQGrid.getSelNo();
  if( tRow == 0 || tRow == null )
  {
    return arrSelected;
  }
  arrSelected = new Array();
  //设置需要返回的数组
  arrSelected[0] = new Array();
  arrSelected[0] = LJSPayXQGrid.getRowData(tRow-1);
  return arrSelected;
}

function afterQuery3( arrQueryResult )
{
  var arrResult = new Array();	
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.GetNoticeNoR3.value = arrResult[0][2];
    fm.ContNoR3.value = arrResult[0][1];
    fm.GetMoney3.value = arrResult[0][3];
    fm.BankCodeName3.value = arrResult[0][5];
  }
  var sql = "select (select bankunsuccreason from LYReturnFromBankB where paycode = a.getnoticeno " 
          + "order by SendDate desc fetch first 1 row only), "
          + "(case when bankaccno is null or bankaccno = '' then '1' else '4' end), " 
          + "(select name from ldperson where customerno = a.appntno), "
          + "(select idno from ldperson where customerno = a.appntno), "
          + "bankcode,bankaccno, accname "
          + "from ljspay a where "
          + "getnoticeno = '" +arrResult[0][2]+ "' with ur";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.all('Reason3').value = rs[0][0];
    fm.all('PayMode3').value = rs[0][1];
    fm.all('Drawer3').value = rs[0][2];
    fm.all('DrawerID3').value = rs[0][3];
    fm.all('BankCode3').value = rs[0][4];
    fm.all('BankAccNo3').value = rs[0][5];
    fm.all('AccName3').value = rs[0][6];			
  }
  showAllCodeName();
}

function selectOne4() 
{
  var arrReturn = getQueryResult4();
  afterQuery4(arrReturn);	          		  
}

function getQueryResult4()
{
  var arrSelected = null;
  tRow = LJAGetXQGrid.getSelNo();
  if( tRow == 0 || tRow == null )
  {
    return arrSelected;
  }
  arrSelected = new Array();
  //设置需要返回的数组
  arrSelected[0] = new Array();
  arrSelected[0] = LJAGetXQGrid.getRowData(tRow-1);
  return arrSelected;
}

function afterQuery4( arrQueryResult )
{
  var arrResult = new Array();	
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.ActuGetNoR4.value = arrResult[0][2];
    fm.ContNoR4.value = arrResult[0][1];
    fm.GetMoney4.value = arrResult[0][3];
    fm.BankCodeName4.value = arrResult[0][5];
  }
  var sql = "select (select bankunsuccreason from LYReturnFromBankB where paycode = a.actugetno " 
          + "order by SendDate desc fetch first 1 row only), "
          + "paymode, drawer,drawerid,bankcode,bankaccno, accname "
          + "from ljaget a where "
          + "actugetno = '" +arrResult[0][2]+ "' with ur";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.all('Reason4').value = rs[0][0];
    fm.all('PayMode4').value = rs[0][1];
    fm.all('Drawer4').value = rs[0][2];
    fm.all('DrawerID4').value = rs[0][3];
    fm.all('BankCode4').value = rs[0][4];
    fm.all('BankAccNo4').value = rs[0][5];
    fm.all('AccName4').value = rs[0][6];			
  }
  
  var tGetNoticeNo = arrResult[0][2];
  sql = "select RiskCode,InsuredNo,ContNo from LJSGetDraw where GetNoticeNo = '" + arrResult[0][2] + "' ";
  var rs1 = easyExecSql(sql);
  if(rs1)
  {
    fm.RiskCode.value = rs1[0][0];
    fm.InsuredNo.value = rs1[0][1];
    fm.TempContNo.value = rs1[0][2];
  }
  
  showAllCodeName();
}

function beforeQuery(flag)
{ 
  if(flag == "XQPAY")
  {
    var tManageCom3 = trim(fm.ManageCom3.value);
    var tContNo3 = trim(fm.ContNo3.value);
    var tGetNoticeNo3 = trim(fm.GetNoticeNo3.value); 
    if(tManageCom3=="" && tContNo3=="" && tGetNoticeNo3=="")
    {
      alert("至少一个查询条件不能为空!");
      return false;
    } 
  }
  else if(flag == "XQGET")
  {
    var tManageCom4 = trim(fm.ManageCom4.value);
    var tContNo4 = trim(fm.ContNo4.value);
    var tActuGetNo4 = trim(fm.ActuGetNo4.value); 
    if(tManageCom4=="" && tContNo4=="" && tActuGetNo4=="")
    {
      alert("至少一个查询条件不能为空!");
      return false;
    } 
  }
  else 
  {
    alert("业务类型错误！")
    return false;
  }
  return true;
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

//进行两次输入的校验
var theFirstValue="";
var theSecondValue="";
function confirmBankAccNo(aObject)
{ 
  if(theFirstValue!="")
  {
    theSecondValue = aObject.value;
    if(theSecondValue=="")
    {
      alert("请再次录入银行帐号！");
      aObject.value="";
      aObject.focus();
      return;
    }
    if(theSecondValue==theFirstValue)
    {
      aObject.value = theSecondValue;
      theSecondValue="";
      theFirstValue="";
      return;
    }
    else
    {
      alert("两次录入帐号不符，请重新录入！");
      theFirstValue="";
      theSecondValue="";
      aObject.value="";
      aObject.focus();
      return;
    }
  }
  else
  {
    theFirstValue = aObject.value;
    theSecondValue="";
    if(theFirstValue=="")
    {
      return;
    }
    alert("请重新录入银行帐号！");
    aObject.value="";
    aObject.focus();
    return;
  }
}

//content ，3-收费，4-付费
function getType(content)
{
  if(content=="3")
  {
      document.all("ywtype").value = "XQPAY";
      document.all("divXQPay").style.display = "";
      document.all("divXQGet").style.display = "none"; 
  }
  else
  {
      document.all("ywtype").value = "XQGET";
      document.all("divXQPay").style.display = "none";
      document.all("divXQGet").style.display = ""; 
  }
  resetDisplay(fm.ywtype.value);
  initLJSPayXQGrid();
  initLJAGetXQGrid(); 
}

function resetDisplay(flag)
{
  if(flag=="XQPAY")
  {
    fm.GetNoticeNoR3.value = "";
    fm.ContNoR3.value = "";
    fm.GetMoney3.value = "";
    fm.Reason3.value = "";
    fm.PayMode3.value = "";
    fm.PayModeName3.value = "";
    fm.Drawer3.value = "";
    fm.DrawerID3.value = "";
    fm.BankCode3.value = "";
    fm.BankCodeName3.value = "";
    fm.BankAccNo3.value = "";
    fm.AccName3.value = "";
  }
  else if(flag=="XQGET")
  {
    fm.ActuGetNoR4.value = "";
    fm.ContNoR4.value = "";
    fm.GetMoney4.value = "";
    fm.Reason4.value = "";
    fm.PayMode4.value = "";
    fm.PayModeName4.value = "";
    fm.Drawer4.value = "";
    fm.DrawerID4.value = "";
    fm.BankCode4.value = "";
    fm.BankCodeName4.value = "";
    fm.BankAccNo4.value = "";
    fm.AccName4.value = "";
  }	
}

function checkSubmit(flag)
{
  if(flag=="XQPAY")
  {
    if(LJSPayXQGrid.mulLineCount == 0)
    {
      alert("请先查询！");
      return false;
    }
    if(trim(fm.GetNoticeNoR3.value) == "")
    {
      alert("请选择需要修改的数据!");
      return false;
    }
    if(fm.PayMode3.value == "4")
    {
      if(trim(fm.Drawer3.value) == "")
      {
        alert("缴费方式为银行转账时对方姓名不能为空!");
        fm.Drawer3.focus();
        return false;
      }
      if(trim(fm.DrawerID3.value) == "")
      {
        alert("缴费方式为银行转账时对方身份证号不能为空!");
        fm.DrawerID3.focus();
        return false;
      }
      if(trim(fm.BankCode3.value) == "")
      {
        alert("缴费方式为银行转账时对方开户银行不能为空!");
        fm.BankCode3.focus();
        return false;
      }
      if(trim(fm.BankAccNo3.value) == "")
      {
        alert("缴费方式为银行转账时对方银行帐号不能为空!");
        fm.BankAccNo3.focus();
        return false;
      }
      if(trim(fm.AccName3.value) == "")
      {
        alert("缴费方式为银行转账时对方帐户名不能为空!");
        fm.AccName3.focus();
        return false;
      }
    }
  }
  else if(flag=="XQGET")
  {
    if(LJAGetXQGrid.mulLineCount == 0)
    {
      alert("请先查询！");
      return false;
    }
    if(trim(fm.ActuGetNoR4.value) == "")
    {
      alert("请选择需要修改的数据!");
      return false;
    }
    
    //校验常无忧B
    if(fm.RiskCode.value == "330501")
    {
      if(fm.PayMode4.value != "4")
      {
        alert("常无忧B给付方式只能是银行转账！");
        return false;
      }
      var tContNo = fm.TempContNo.value;
      var tInsuredNo = fm.InsuredNo.value;
      var sql = "select name from lcinsured where contno = '" + tContNo + "' and insuredno = '" + tInsuredNo + "' ";
      
      var tInsuredName = "";
      var rs1 = easyExecSql(sql);
      if(rs1)
      {
        tInsuredName = rs1[0][0];
      }
      else
      {
        alert("查询被保人信息失败！");
        return false;
      }
      if(trim(fm.Drawer4.value)!=tInsuredName)
      {
        alert("常无忧B业务对方姓名必须是被保人姓名-"+tInsuredName);
        fm.Drawer4.focus();
        return false;
      }
      if(trim(fm.AccName4.value)!=tInsuredName)
      {
        alert("常无忧B业务对方帐户名必须是被保人姓名-"+tInsuredName);
        fm.AccName4.focus();
        return false;
      }
    }
    
    if(fm.PayMode4.value == "4")
    {
      if(trim(fm.Drawer4.value) == "")
      {
        alert("付费方式为银行转账时对方姓名不能为空!");
        fm.Drawer4.focus();
        return false;
      }
      if(trim(fm.DrawerID4.value) == "")
      {
        alert("付费方式为银行转账时对方身份证号不能为空!");
        fm.DrawerID4.focus();
        return false;
      }
      if(trim(fm.BankCode4.value) == "")
      {
        alert("付费方式为银行转账时对方开户银行不能为空!");
        fm.BankCode4.focus();
        return false;
      }
      if(trim(fm.BankAccNo4.value) == "")
      {
        alert("付费方式为银行转账时对方银行帐号不能为空!");
        fm.BankAccNo4.focus();
        return false;
      }
      if(trim(fm.AccName4.value) == "")
      {
        alert("付费方式为银行转账时对方帐户名不能为空!");
        fm.AccName4.focus();
        return false;
      }
    }
  }	
  else 
  {
    alert("业务类型错误！")
    return false;
  }
  return true;
}

function afterCodeSelect(cCodeName, Field)
{
  try	
  {
    if(cCodeName == "PayMode")	
    {
      if(fm.PayMode3.value!="4")
      {
        fm.Drawer3.value = "";
        fm.DrawerID3.value = "";
        fm.BankCode3.value = "";
        fm.BankCodeName3.value = "";
        fm.BankAccNo3.value = "";
        fm.AccName3.value = "";
      }
    }
  }
  catch(ex){}
}

function unlockMul(ywtype1)
{
  fm.ywtype1.value=ywtype1;
  if(ywtype1=="PAYMUL")
  {
    if(LJSPayXQGrid.mulLineCount == 0)
    {
      alert("请先查询！");
      return false;
    }
  }
  else if(ywtype1=="GETMUL")
  {
    if(LJAGetXQGrid.mulLineCount == 0)
    {
      alert("请先查询！");
      return false;
    }
  }
  else
  {
    alert("请先查询！");
    return false;
  }
  fm.action="LJSUnlockMulSave.jsp";
  fm.submit();
}

//批量修改提交后操作,服务器数据返回后执行的操作
//业务类型，PAYMUL续期收费，GETMUL续期付费(满期给付)
function afterSubmit1(FlagStr, content, transact)
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    easyQueryClick(fm.ywtype.value);
    resetDisplay(fm.ywtype.value); 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

function payModeHelp(){
	window.open("../finfee/PayModeHelp.jsp");
}






