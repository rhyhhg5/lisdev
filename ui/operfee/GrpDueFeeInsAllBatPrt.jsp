<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： IndiDueFeeInsAllBatPrt.jsp
//程序功能： 团险续期批量催缴批打pdf全部
//创建日期： 2007-06-15
//创建人  ： Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
	boolean operFlag=true;
	int tCount = 0;
	String Content = "";
	String tPrintServerPath = "";	
	String tsql = request.getParameter("sqlPDF");
	System.out.println("--------huxl-----------"+tsql);
	String tOutXmlFile  = application.getRealPath("")+"\\";
	LJSPayBSet tLJSPayBSet = new LJSPayBSet();

	String arrPtrList[][];
	SSRS tSSRS = new SSRS();
	ExeSQL tPrtList= new ExeSQL();
  tSSRS = tPrtList.execSQL(tsql);
  arrPtrList = tSSRS.getAllData();

	tCount = tSSRS.getMaxRow();
	System.out.println("tCount="+tCount);
	for(int j = 0; j < tCount; j++)
	{
			LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
			tLJSPayBSchema.setGetNoticeNo(arrPtrList[j][12]);
			tLJSPayBSet.add(tLJSPayBSchema);
			System.out.println("---------GetNoticeNo: "+arrPtrList[j][12]);
	}
	CErrors mErrors = new CErrors();
	GlobalInput tG = new GlobalInput();      
	tG = (GlobalInput)session.getValue("GI");
	//tG.ClientIP = request.getRemoteAddr();
	tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
	tG.ServerIP =tG.GetServerIP();
	
	LDSysVarSchema	tLDSysVarSechma =new LDSysVarSchema();
	tLDSysVarSechma.setSysVarValue(tOutXmlFile);
	
	VData tVData = new VData();
	tVData.addElement(tLDSysVarSechma);
	tVData.addElement(tG);
	tVData.addElement(tLJSPayBSet);

	//改的这行
	NewGrpDueFeeBatchPrtBL tGrpDueFeeBatchPrtBL = new NewGrpDueFeeBatchPrtBL();
	
	if(tGrpDueFeeBatchPrtBL.submitData(tVData,"batch"))
	{
//		tCount = tGrpDueFeeBatchPrtBL.getCount();
//		String tFileName[] = new String[tCount];
//		VData tResult = new VData();
//		tResult = tGrpDueFeeBatchPrtBL.getResult();
//		tFileName=(String[])tResult.getObject(0);
//	
//		String mFileNames = "";
//		for (int i = 0;i<=(tCount-1);i++)
//		{
//	  		System.out.println("~~~~~~~~~~~~~~~~~~~"+i);
//	  		System.out.println(tFileName[i]);
//	  		mFileNames = mFileNames + tFileName[i]+":";
//		}
//	  	System.out.println("===================="+mFileNames+"=======================");
//		System.out.println("=========tFileName.length==========="+tFileName.length);
//	
//		String strRealPath = application.getRealPath("/").replace('\\','/');
//		String sql = "Select sysvarvalue from LDSysvar where sysvar = 'PrintServerInterface'";
//		ExeSQL mExeSQL = new ExeSQL();
//		tPrintServerPath =  mExeSQL.getOneValue(sql);
	%>
<%-- 	<html> 	
		<script language="javascript">
			var printform = parent.fraInterface.document.getElementById("printform");
			printform.elements["filename"].value = "<%=mFileNames%>";
			printform.action = "<%=tPrintServerPath%>";
			printform.submit();
		</script>
	</html>--%>
	<%         			
	}
%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
		alert("打印成功");
		//parent.fraInterface.afterInsForBatPrt();
	}
	else
	{
		alert("<%=Content%>");
	}
</script>
</html>