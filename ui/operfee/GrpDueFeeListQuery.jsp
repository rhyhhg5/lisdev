<A HREF=""></A> <html> 
<%
//程序名称：GrpDueFeeInput.jsp
//程序功能：集体保费催收，实现数据从保费项表到应收集体表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	
        GlobalInput tGI = new GlobalInput();
        //PubFun PubFun=new PubFun();
	tGI = (GlobalInput)session.getValue("GI");
//	System.out.println("1"+tGI.Operator);
//	System.out.println("2"+tGI.ManageCom);
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);                	               	
      //  String MaxManageCom=PubFun.RCh(tGI.ManageCom,"9",8);
      //  String MinManageCom=PubFun.RCh(tGI.ManageCom,"0",8);  	
%>
<head >
  <SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;

var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT>  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpDueFeeListQuery.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="GrpDueFeeListQueryInit.jsp"%>
</head>
<body onload="initForm();">
<form name=fm action=./GrpDueFeeQuery.jsp target=fraSubmit method=post>
   
	
  <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
	     <tr class= common align=center>
          <TD  class= title>
          抽档时间范围：
          </TD>
		   <TD  class= title>
          开始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>
          终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD> 
		  
		</tr>  
		<tr class= common align=center>          
		  <TD  class= title> 操作人 </TD>
          <TD  class= input> <Input class=common name=AgentCode > </TD>
			<TD  class= title> 操作机构</TD><TD  class= input>
			<Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,comcodename],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,comcodename],[0,1],null,null,null,1);" ><Input class=codename  name=comcodename></TD>
		  <TD><INPUT VALUE="查询批次应收纪录" class = cssbutton TYPE=button onclick="easyQueryClick();">  </TD>	
	    </tr>
	  </table>
	  <br>
	  <table class= common >
        <TR  class= common>		
          <TD  class= title> 保单号 </TD>
          <TD  class= input> <Input class= common name=ProposalGrpContNo >  </TD>
          <TD  class= title>  客户号 </TD>
          <TD  class= input>  <Input class= common name=AppNo ></TD>
		  <TD class= title>催收状态</TD> 
		  <TD  class= input>
				   <Input class="codeNo" name="DealState" CodeData="0|^0|待收费^1|催收成功^2|应收作废^3|已撤销^4|待核销^5|全部" verify="交付费方式|&code:DealState"  ondblclick="return showCodeListEx('DealState',[this,PayModeName],[0,1]);" onkeyup="return showCodeListEx('DealState',[this,PayModeName],[0,1]);"><Input class="codeName" name="PayModeName"  elementtype="nacessary" readonly>
				 </TD>	
		   <TD><INPUT VALUE="查询个案应收纪录" class = cssbutton TYPE=button onclick="singleQueryClick();">  </TD>
		   </TR>    
	</table>
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 应收记录：
    		</td>
			<td class= titleImg><INPUT VALUE="查询应收团体保单" class = cssbutton TYPE=button
			onclick="contQueryClick();">    </td>
    	</tr>
    </table>
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpJisPayGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <center>    	
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
		</center>  	
  	</div>
          
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 团体保单信息
    		</td>
			<td class= titleImg><td>			
			<td class= titleImg>    <INPUT VALUE="保单明细查询" class = cssbutton style="width:100px" TYPE=button onclick="returnParent();">
            </td>
            <td class= titleImg><INPUT VALUE="打印通知书" class = cssbutton style="width:100px" TYPE=button onclick="printNotice();"> </td>
			<td class= titleImg><INPUT VALUE="打印PDF通知书" class = cssbutton style="width:100px" TYPE=hidden onclick="printInsManage();"> </td>
			<td class= titleImg><INPUT VALUE="打印PDF通知书" class = cssbutton style="width:100px" TYPE=button onclick="newprintInsManage();"> </td>
			<td class= titleImg><INPUT VALUE="批打PDF通知书" class = cssbutton style="width:100px" TYPE=hidden onclick="printInsManageBat();">  </td>	
	        <td class= titleImg><INPUT VALUE="批打PDF通知书" class = cssbutton style="width:100px" TYPE=button onclick="newprintInsManageBat();">  </td>	
	        <td class= titleImg><INPUT VALUE="打印应收清单" class = cssbutton style="width:100px" TYPE=button onclick="printList();">  </td>	
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<center>    	
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
		</center>  	
  	</div>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
    		<td class= titleImg>
    			 团体险种信息
    		</td>
    	</tr>
  </table> 	
  <Div  id= "divGrpDue1" style= "display: ''">
    <Table  class= common>
       <TR  class= common>
         <TD text-align: left colSpan=1>
            <span id="spanGrpPolGrid" ></span> 
  	     </TD>
       </TR>
    </Table>	
    <center>      				
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 				   <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
      </center>  
  </Div>	  	
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSpec);">
    		</td>
    		<td class= titleImg> 特别缴费约定</td>  			
    	</tr>
    </table>
    <Div  id= "divSpec" style= "display: "> 
      	<table  class= two>
       		<tr class=common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanSpecGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    </DIV>   		
    <Input type=hidden name=loadFlag> 
    <Input type=hidden name=GetNoticeNo> 
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  	<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>
</body>
</html>
