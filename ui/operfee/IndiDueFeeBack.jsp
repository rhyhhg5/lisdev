<%
//程序名称：IndiDueFeeBack.jsp
//程序功能：团单实收保费转出
//创建日期：2006-09-22
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head >
  <SCRIPT>
  </SCRIPT>  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="IndiDueFeeBack.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="IndiDueFeeBackInit.jsp"%>
</head>
<body onload="initForm();">
<form name=fm action="./IndiDueFeeBackSave.jsp" target=fraSubmit method=post>
  <table  class= common align=center>
    <tr class=common>
     <TD  class= title>
     客户号
     </TD>
     <TD  class= input>
       <Input class="common"  name=AppntNo>
     </TD>
     <TD  class= title>
     保单号
     </TD>
     <TD  class= input>
       <Input class="common"  name=ContNo >
     </TD>  		   
	   <TD  class= input>
        <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="queryCont();"> 
     </TD>	  
	  </tr>
	  <tr class=common>
     <TD  class= title>
     账户余额
     </TD>
     <TD  class= input colspan="3">
       <Input class="readonly" name=AccBala readonly>
     </TD>
	  </tr>
  </table>
  
  <table>
    <tr> 
      <td class= common> 
      	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divContGrid);"> 
      </td><br>
      <td class= titleImg>保单列表</td>
      <td class= common> 
        &nbsp;&nbsp;
      </td>
    </tr>
  </table>
  <Div  id= "divContGrid" style= "display: ''" align="center">  
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
  				<span id="spanContGrid" >
  				</span> 
  	  		</td>
  		</tr>
  	</table>
  	<Div id= "divPage2" align="center" style= "display: 'none' ">
  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
  		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
  		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
  		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
  	</Div> 
  </Div>
  
   <table>
    <tr> 
      <td class= common> 
      	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divContGrid);"> 
      </td><br>
      <td class= titleImg>应收记录列表</td>
      <td class= common> 
        &nbsp;&nbsp;
      </td>
    </tr>
  </table>
  <Div  id= "divContGrid" style= "display: ''" align="center">  
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
  				<span id="spanLJAPayGrid" >
  				</span> 
  	  		</td>
  		</tr>
  	</table>
  	<Div id= "divPage3" align="center" style= "display: 'none' ">
  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();"> 
  		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage();"> 					
  		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage();"> 
  		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage();"> 
  	</Div> 
  </Div>
	<br>
     <INPUT VALUE="保费转出" class = cssbutton TYPE=button onclick="cancelRecord();"> 
     <INPUT TYPE=hidden name=GetNoticeNo value="">
     <INPUT TYPE=hidden name=tContNo value="">
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
