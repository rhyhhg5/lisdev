<A HREF=""></A> <html> 
<%
//程序名称：GrpDueFeeInput.jsp
//程序功能：集体保费催收，实现数据从保费项表到应收集体表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	
        GlobalInput tGI = new GlobalInput();
        //PubFun PubFun=new PubFun();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("1"+tGI.Operator);
//	System.out.println("2"+tGI.ManageCom);
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);                	               	
      //  String MaxManageCom=PubFun.RCh(tGI.ManageCom,"9",8);
      //  String MinManageCom=PubFun.RCh(tGI.ManageCom,"0",8);  	
%>
<head >
  <SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;
var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT>  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpPayListInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="GrpPayListInputInit.jsp"%>
</head>
<body onload="initForm();">
<form name=fm  target=fraSubmit method=post>
  <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
	     <tr class=common width="100%">
          <TD  class= title>核销区间 </TD>          
           <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>至</TD>         
           <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD> 
          <TD  class= title> 管理机构 </TD>
          <TD  class= input> <Input class="code" name=ManageCom ondblclick="return showCodeList('comcode',[this]);" onkeyup="return showCodeListKey('comcode',[this]);"> </TD>          
          <TD  class= common>
           <INPUT class= cssbutton VALUE="查询"   TYPE=button onclick="multQueryClick();">        
          </TD> 
        <TR>          
        </TR>
          <TD  class= title>保单号</TD>
          <TD  class= input>
            <Input class="common"  name=GrpContNo >
          </TD> 
          <td></td>
		      <TD  class= common>
           <INPUT class= cssbutton VALUE="查  询"   TYPE=button onclick="easyQueryClick();">        
          </TD> 
		 </tr>     
  </table>
  <Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpJisPayGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <center>    	
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
		</center>  	
  	</div>
    <br> 
	   <td><INPUT VALUE="批次明细查询" class = cssbutton TYPE=button onclick="easyQueryClick();"> 
	   <TD  class= title> 条件： </TD>
     <TD  class= input>
			<Input class="codeNo" name="PrintState" CodeData="0|^0|未打印^1|已打印^2|全部" verify="交付费方式|&code:DealState"  ondblclick="return showCodeListEx('PrintState',[this,PrintName],[0,1]);" onkeyup="return showCodeListEx('PrintState',[this,PrintName],[0,1]);"><Input class="codeName" name="PrintName"  elementtype="nacessary" readonly>
		 </TD>   
  <br>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 核销记录清单:
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
   <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
  </center>  	
  </div>
   <table  class= common align=center>
    <tr >	      
	   <INPUT VALUE="打印对帐单" class = cssbutton TYPE=button onclick="printPayComp();"> 
	   <INPUT VALUE="打印清单" class = cssbutton TYPE=button onclick="printList();"> 
	</tr>
  </table>  	
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
