<A HREF=""></A> <html> 
<%
//程序名称：GrpDueFeePlanInput.jsp
//程序功能：约定缴费团单催收
//创建日期：2012-01-31
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
		GlobalInput tGI = new GlobalInput();
		tGI = (GlobalInput)session.getValue("GI");
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate); 
%>
<head >
  <SCRIPT>
var CurrentYear=<%=tCurrentYear%>;
var CurrentMonth=<%=tCurrentMonth%>;
var Operator='<%=tGI.Operator%>';
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var managecom = <%=tGI.ManageCom%>;
  </SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpDueFeeSplitInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="GrpDueFeeSplitInputInit.jsp"%>
</head>
<body onload="initForm();">
<form name=fm action=./GrpDueFeeSplitQuery.jsp target=fraSubmit method=post>
  <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 集体保单号 </TD>
          <TD  class= input> <Input class= common name=GrpContNo ></TD>
          <TD  class= title> 印刷号码 </TD>
          <TD  class= input>  <Input class= common name=PrtNo ></TD>
          <TD></TD>
        </TR>
        <TR  class= common>
      <TD  class= title> 管理机构</TD><TD  class= input>
      <Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
      <TD  class= title> 代理人编码</TD><TD  class= input>
      <Input class= "codeno"  name=AgentCode  ondblclick="return showCodeList('AgentCode',[this,AgentName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AgentCode',[this,AgentName],[0,1],null,null,null,1);" ><Input class=codename  name=AgentName></TD>
        </TR>
       
		 
         
            <Input  type=hidden name=StartDate verify="约定缴费起期|NOTNULL">
   
         
        
            <Input  type=hidden name=EndDate verify="约定缴费止期|NOTNULL">
   
	
    </table>
    <INPUT VALUE="查询可拆分的团体保单" class = cssbutton TYPE=button onclick="easyQueryClick();">
  
  	
  	</div>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 团体保单信息
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
<center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
</center>  	
  	</div>
  	  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
    		<td class= titleImg>
    			 团体保障计划缴费信息
    		</td>
    	</tr>
    </table> 	
 <Div  id= "divGrpPayDue" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanGrpPayDueGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
<center>
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage4.firstPage();">
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage4.previousPage();">
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage4.nextPage();">
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage4.lastPage();">
</center>
</Div>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
    		<td class= titleImg>
    			 团体保障计划信息
    		</td>
    	</tr>
    </table> 	
 <Div  id= "divGrpDue1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanGrpPolGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
<center>
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();">
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
</center>
</Div>
<table  class= common align=center>
      	<TR  class= common>
	<td class= title>
    			最后一期缴费日期
    	</td><TD  class= input>
 	<Input class="coolDatePicker" dateFormat="short" name=PayDate >
 	<td class= title>
    			全单的应收保费合计
    	</td><TD  class= input>
 	 <TD  class= input>  <Input class= common name=ShouldMoney readonly>
 	</TD>
 	<TD></TD>
 	</TR>
 	</table>
 	<br></br>
    <INPUT VALUE="拆 分 最 后 一 期  缴 费" TYPE=button class = cssbutton name="singleDue"  onclick="GrpSingle();">
    <br></br>
<Div  id= "divSplitResult" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanSplitResultGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
<center>
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage3.firstPage();">
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();">
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();">
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage3.lastPage();">
</center>
</Div>
	<textarea name="Infomation" id="Infomation" cols="120" rows="1"  style="border-width:0px;color: #FF0000;display: 'none' " readonly="true" > </textarea>
    <Input type=hidden name=GetNoticeNo>
    <Input type=hidden name=tGrpContNo>
    <Input type=hidden name=ProposalGrpContNo>
    <Input type=hidden name=tPrtNo>
    <Input type=hidden name=PayToDate>
    <Input type=hidden name=PlanCode>
    <Input type=hidden name=Flag value="">
    <Input type=hidden name=Cvalidate>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
