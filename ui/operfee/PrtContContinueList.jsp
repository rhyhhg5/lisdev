<%
//程序名称：PrtContContinueList.jsp
//程序功能：个险保单继续率统计,查询保单继续率,下载清单。
//创建日期：2011-02-11 
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
	//System.out.println(curDate);
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html> 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="PrtContContinueList.js"></SCRIPT>
</head>
<body  onload="initForm();initElementtype();" >
	<form method=post name=fm action="./PrtContContinueListSave.jsp" >
		<Table  class= common>
	    	<tr>
	    		<td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName elementtype=nacessary>
				</td>
				<td class= title>保单类型</td>
				<td class=input>
					<Input class= "codeno" name=ContType verify="保单类型|NOTNULL" CodeData="0|^0|全部^1|个险^2|银保^3|互动" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);" ><Input class=codename name=ContTypeName readonly elementtype=nacessary >
				</td>
				<td class= title>险种类型</td>
				<td class=input>
					<Input class= "codeno" name=RiskType verify="险种类型|NOTNULL" CodeData="0|^1|传统险^2|万能险" ondblclick="return showCodeListEx('RiskType',[this,RiskTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('RiskType',[this,RiskTypeName],[0,1]);" ><Input class=codename name=RiskTypeName readonly elementtype=nacessary>
				</td>
			</tr>
	    	<tr>
	    		<td class= title>销售渠道</td>
				<td class=input>
					<Input class= "codeno" name=SaleChnl verify="销售渠道|NOTNULL" CodeData="0|^00|全部^01|个险直销^04|银行代理^10|个险中介^13|银代直销^14|互动直销^15|互动中介" ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1]);" ><Input class=codename name=SaleChnlName readonly elementtype=nacessary>
				</td>
				<td class= title>继续率类型</td>
				<td class=input>
					<Input class= "codeno" name=ContinueType verify="继续率类型|NOTNULL" CodeData="0|^1|13个月^2|25个月" ondblclick="return showCodeListEx('ContinueType',[this,ContinueTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('ContinueType',[this,ContinueTypeName],[0,1]);" ><Input class=codename name=ContinueTypeName readonly elementtype=nacessary>
				</td>
	    	    <td class= title>保单号</td>
				<td class=input>
					<input class=common name=ContNo>
				</td>
	    	</tr>
	    	<tr>
				<td class= title>生效开始日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=StartDate elementtype=nacessary verify="生效开始日期|NOTNULL">
				</td>
				<td class= title>生效结束日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=EndDate elementtype=nacessary verify="生效结束日期|NOTNULL">
				</td>
			</tr>
		</Table>
		<Table>
			<td>
				<input value="下载报表" class = cssButton TYPE=button onclick="download();">
			</td>
		</Table>
    	<input type=hidden id="arrset" name="arrset">
    	<input type=hidden id="strsql" name="strsql">
    <table>
    	<tr >
	      <td>
	      <br>
	      <font color = red>
	        报表说明：<br>
	        1.查询条件中生效开始日期指保单生效日期的起始日期；<br>
	        2.查询条件中生效结束日期指保单生效日期的终止日期；<br>
	        3.查询结果中生效保费表示首期实际收取的长期期缴保费，不包括犹豫期退保保单，但包括解约和协议退保期缴保单保费；<br>
	        4.如果继续率类型选择13个月，查询结果中实收保费表示第二期实收保费。如果继续率类型选择25个月，查询结果中实收保费表示第三期实收保费；<br>
	        5.如果续期保费已经实收，但后续又发生实收保费转出，查询结果中实收保费为0；<br>
	        6.做过保单迁移的，查询结果中保单纪录体现在当前所在的机构；<br>
	        7.查询结果中只包括缴费频次为年缴的长期险保单，但不包括缴费年期为1年的保单。缴费年期为2年的保单，只在13个月继续率的查询结果中体现；<br>
	        8.查询结果中<b>保单服务状态，万能险基本保费以及万能险额外保费</b>仅用于继续率报表分析，不作为其它任何业务管理、人员管理及财务管理数据分析的依据。<br>
	      </font>
	      </td> 
  	  </tr>
  	</table>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script language="JavaScript">

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";

//表单初始化
function initForm()
{
	initInputBox();
}

function initInputBox()
{
try
	{
		fm.all('ManageCom').value = manageCom;
		fm.all('ContType').value = "0";
		fm.all('RiskType').value = "1";
		fm.all('SaleChnl').value = "00";
		fm.all('ContinueType').value = "1";
		
		var sql = "select current date - 15 month,current date - 12 month from dual ";
		var rs = easyExecSql(sql);
		fm.all('StartDate').value = rs[0][0];
		fm.all('EndDate').value = rs[0][1];
		
		showAllCodeName();
	}
	catch(ex)
	{
		alert("initInputBox函数中发生异常:初始化界面错误!");
	}      
}
</script>
