var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  if(!beforeSubmit())
  	return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}
 
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
           
          
          
//打印通知书
function printNotice()
{
	showInfo = window.open("./expirBenefitInsForPrt.jsp?taskNo="+taskNo);
}

function checkSelect()
{
	
	if(fm.dealRadio[0].checked == false&&fm.dealRadio[1].checked == false)
	{ 
		alert("您好，请选择处理方式后再进行处理！");
		return false;
	}
	if(fm.dealRadio[0].checked==true)
	{
		fm.deal.value="reset";
	if(fm.tranDate.value =="")
	{
		alert("您好，日期必须填写！");
		return false;
	}
	}
	else
	if(fm.dealRadio[1].checked==true)
	{
		fm.deal.value="modify";
	if(fm.dealRadiotype[0].chexked ==false&&fm.dealRadiotype[1].chexked ==false)
	{
		alert("您好，请选择处理方法！");
		return false;
	}
	if(fm.dealRadiotype[0].checked==true)
	{
		fm.dealtype.value="money"
	if(fm.getPerson.value==""||fm.appKind.value==""||fm.no.value=="")
	{
		alert("您好，领款人,申请人性质,领款人证件号是必填内容！");
		return false;
	}
	}
	else
	{
		fm.dealtype.value="accont"
	}
	}
	return true;
}


function unInit()
{
		fm.bank.value = "";
		fm.accontName.value = "";
		fm.accontNo.value = "";
	
}

//变更给付方式确认
function changePayModeSubmit()
{
	getContNo();
  if(fm.BankAccNo.value != fm.BankAccNo2.value)
  {
    alert("您两次录入的账号不同，请核对");
    return false;
  }

  if(fm.PayModeOld.value == fm.PayModeNew.value
    && fm.BankCodeOld.value == fm.BankCode.value
    && fm.AccNameOld.value == fm.AccName.value
    && fm.BankAccNoOld.value == fm.BankAccNo.value)
  {
    alert("你录入的新给付方式与原给付方式一致，不需要变更");
    return false;
  }
 
  if(fm.PayModeNew.value == "")
  {
    alert("请选择给付方式");
    return false;
  }
  else if(fm.PayModeNew.value == "3" || fm.PayModeNew.value == "4")
  {
    if(fm.BankCode.value == "" || fm.AccName.value == "" || fm.BankAccNo.value == "")
    {
      alert("银行账户信息必须录入完整");
      return false;
    }
  }
  else if(fm.PayModeNew.value == "1" || fm.PayModeNew.value == "2")
  {
    if(fm.BankCode.value != "" || fm.AccName.value != "" || fm.BankAccNo.value != "")
    {
      alert("您所选给付方式不需要录入帐户信息，请清空");
      return false;
    }
  }
  var rs = getPayModeInfo(fm.getNoticeNo.value);

  if(rs[0][0] == fm.PayModeNew.value && rs[0][2] == fm.BankCode.value
    && rs[0][4] == fm.AccName.value && rs[0][5] == fm.BankAccNo.value)
  {
    alert("您录入的信息已进行了变更");
    return false;
  }
 
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var fmAction = fm.action;
  fm.action = "expirBenefitChangePayModeSave.jsp?GetNoticeNo="+fm.getNoticeNo.value;
  fm.submit();
  
  fm.action = fmAction;
  return true;
}

//查询给付方式信息
function getPayModeInfo(cGetNoticeNo)
{
  var sql = "select PayMode, CodeName('paymode', a.PayMode), "
          + "   BankCode, (select BankName from LDBank where BankCode = a.BankCode), "
          + "   AccName, BankAccNo "
          + "from LJSGet a "
          + "where GetNoticeNo = '" + cGetNoticeNo + "' ";
  var rs = easyExecSql(sql);
  return rs;
}

function afterCodeSelect(cCodeName, Field)
{
  if(cCodeName=="PayMode2")
  {
    if(fm.PayModeNew.value == "1" || fm.PayModeNew.value == "2")
    {
      fm.BankCode.value = "";
      fm.BankName.value = "";
      fm.AccName.value = "";
      fm.BankAccNo.value = "";
      fm.BankAccNo2.value = "";
    }
  }
}
// 获得保单号 modify by fuxin 2008-5-13 21:24:07
function getContNo()
{
		  var tContNoForModify =easyExecSql("select ContNo From ljsgetdraw where getnoticeno ='"+fm.getNoticeNo.value+"'");
 		  fm.contNoForModify.value = tContNoForModify 
 		 
}
