 <% 
//程序名称：指定医院、推荐医院清单查询及打印
//程序功能：个人保全
//创建日期：2007-7-18 9:43
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK"> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="IndiHospitalQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="IndiHospitalQueryInit.jsp"%>
  <title>指定医院、推荐医院清单查询及打印</title> 
</head>
<body  onload="initForm();" >
  <form action="./IndiHospitalQuerySave.jsp" method=post name=fm target="fraSubmit">
    <table class=common >
    	<br>
    	<tr class= common>
    		<td class= title>管理机构</td>
    		<TD  class= input>
	     	<Input class=codeNo name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  
	  		</TD>  
    		<TD class= title>
    			<input class=cssButton type="button" value="查  询" onclick="queryHospital();">
    			</TD>
    		<TD class= title>
    			<input class=cssButton type="button" value="打  印" onclick="printAllHospital();">
    			</TD>
    		</tr>
    		  </table>
    	<table >
     	<tr>
    		<td>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHospital);">
    		</td>
    		 <td class= titleImg >
        		 指定医院
       	</td>
    	</tr>
    </table>
    <Div  id= "divHospital" style= "display: ''">
    	<span id="spanHospitalGrid" ></span>
    	<center>
    	<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPageHospital.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPageHospital.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPageHospital.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPageHospital.lastPage();">		
      </center>
    </Div>
    <table>
    	<tr>
    		<td>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSpecHospital);">
    		</td>
				<td class= titleImg>
        		 推荐医院
       	</td>
    	</tr>
    		</table>
    <Div  id= "divSpecHospital" style= "display: ''">
    	<span id="spanSpecHospitalGrid" ></span>
    	<center>
    	<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPageSpecHospital.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPageSpecHospital.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPageSpecHospital.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPageSpecHospital.lastPage();">	
    </center>
    </Div>

  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
	