//GrpDueFeePremInpu.js

var showInfo;

var turnPage = new turnPageClass();

//初始化保单信息
function queryLCContPlan()
{
  var sql = "  select grpContNo, contPlanCode, contPlanName, peoples2, "
            //保障计划保费
            + "   (select sum(riskPrem) "
            + "   from LCContPlanRisk "
            + "   where grpContNo = a.grpContNo "
            + "      and contPlanCode = a.contPlanCode), peoples2 "
            + "from LCContPlan a "
            + "where grpContNo = '" + fm.grpContNo.value + "' "
            + "   and contPlanCode != '11' ";
  turnPage.queryModal(sql, LCContPlan); 
  if(LCContPlan.mulLineCount == 0)
  {
    alert("没有查询到保单保障计划。");
    return false;
  }
  
  //得到录入的保费人数
  //sql = "  select contPlanCode, premInput, peoples2Input "
  //      + "from LCNoNamePremTrace "
  //      + "where grpContNo = '" + fm.grpContNo.value + "' "
  //      + "   and getNoticeNo = '" + fm.grpContNo.value + "' ";  //为生成财务应收前存的是保单号
  //var rs = easyExecSql(sql);
  //if(rs)
  //{
  //  for(var i = 0; i < rs.length; i++)
  //  {
  //    if(LCContPlan.getRowColDataByName(i, "contPlanCode") == rs[i][0])
  //    {
  //      LCContPlan.setRowColDataByName(i, "premInput", rs[i][1]);
  //      LCContPlan.setRowColDataByName(i, "peoples2Input", rs[i][2]);
  //    }
  //  }
  //}
  
  return true;
}

//取消录入
function cancel()
{
  queryLCContPlan();
}

//返回父页面
function returnParent()
{
  
  
  top.close();
}

//保存录入数据
function submitData()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    top.opener.afterPremInput("0");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
    
    top.opener.afterPremInput("1");
    top.close();
  }
}