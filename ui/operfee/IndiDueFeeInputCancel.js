//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var showInfo;
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    resetForm();
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
	  easyQueryClick();
  }
  catch(re)
  {
  	alert("在LJSPayPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function beforeSubmit()
{

    /*if(fm.all('ContNo2').value == '')
    {
    	alert("保单合同号码不能为空!");
    	return false;
    	}*/
    	var tSelNo = PolGrid.getSelNo();
	if( tSelNo == 0 || tSelNo == null )
	{
		alert( "请先选择一条记录" );
		return false;
	}	    
    return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function PersonMulti()
{
	var StartDate=fmMulti.all('StartDate').value;
	var EndDate=fmMulti.all('EndDate').value;
	if(StartDate==null||StartDate==""||EndDate==null||EndDate=="")
	{
	  alert("必须录入起止日期");
	  return false;	
	}

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");     	
  fmMulti.submit();	

}

function SpecPersonMulti()
{

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");     	
  
  fmMulti.spec.value = "1";
  
  fmMulti.submit();	
  
}

function PersonSingle()
{
    if(beforeSubmit())
    {  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      	  	
    fm.submit();
    }	
}

//取得选择了的保单信息
function easyQueryAddClick()
{
	var tSelNo = PolGrid.getSelNo();
  
  fm.ContNoSelected.value = PolGrid.getRowColDataByName(tSelNo-1,"ContNo");	
  fm.PrtNoSelected.value= PolGrid.getRowColDataByName(tSelNo-1,"PrtNo");
  fm.GetNoticeNoSelected.value=PolGrid.getRowColDataByName(tSelNo-1,"GetNoticeNo");

}

// 查询按钮
function easyQueryClick()
{
	//允许无条件查询
	/*
	if(fm.all('ContNo2').value==''&&fm.all('PrtNo').value=='')
	{
		alert("必须输入查询条件!");	
		return ;
	}
	*/
	
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	try{
	var strSQL = "  select ContNo,PrtNo,AppntName,CValiDate, b.GetNoticeNo "
      	       + "from LCCont a, LJSPay b "
      				 + "where a.contNo=b.otherNo "
      				 + "    and a.AppFlag='1' "
      				 + "    and OtherNoType='2' "
      				 + "    and (b.BankOnTheWayFlag !='1' or b.BankOnTheWayFlag is null ) "
      				 + getWherePart( 'a.ContNo','ContNo2' )
      				 + getWherePart( 'getNoticeNo' )
      				 + "    and a.ManageCom like '" + ComCode + "%%' "
      				 + "order by b.makeDate ";
	 turnPage.queryModal(strSQL, PolGrid,0,1);
	}catch(ex){alert(ex.message);}
}

