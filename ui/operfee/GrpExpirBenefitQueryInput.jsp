<%
//程序名称：GrpExpirBenefitQueryInput.jsp
//程序功能：满期给付抽档
//创建日期：2008-11-5 14:34:14  
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>

	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<html>

<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpexpirBenefitQueryInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpexpirBenefitQueryInputInit.jsp"%>
</head>
<%
	
        GlobalInput tGI = new GlobalInput();
	      tGI = (GlobalInput)session.getValue("GI");
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);               	               	
      
%>
<SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;
var operator = '<%=tGI.Operator%>';
/*
var sql = "select char(date('" + CurrentTime + "') + 15 days) from dual ";
var rs = easyExecSql(sql);
if(rs)
{
  CurrentTime = rs[0][0];
}
*/
var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT> 
<body onload="initForm1();">

<form name=fm action="" target=fraSubmit method=post>
	 <INPUT type= "hidden" name= "strsql" value="">   
	 <INPUT type= "hidden" name= "tGrpContNo" value="">
	 <input type= "hidden" name= "EdorNo" value=""> 
	<input type= "hidden" name= "EdorType" value="">   
	<input type= "hidden" name= "doType" value="Q">
	<INPUT type= "hidden" name= "otherNo" value="">
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        给付保单查询：
      </td>
     </tr>
    </table>
    <Div  id= "divAlivePayMulti" style= "display: ''">
      <Table  class= common>
      	<tr>
      		<td class= title>满期日起期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      		<td class= title>满期日止期</td>
      		<td class= input><Input class="common" dateFormat="short" name=EndDate ></td>
      		<td class= title>操作机构</td>
      		<td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD></td>
      		</tr>
      	<tr>
      		<td class= title>保单号</td>
      		<td class= input><Input class= common name=GrpContNo></td>
      		<!--
      		<td class= title>任务号</td>
      		<td class= input><Input class=common name=OtherNo></td>
      		      			
      		      			<TD class= title>
					给付方式
			</TD>
			<TD class= input>
					<Input class= "codeno"  name=PayTaskState CodeData="0|^2|团体统一给付^3|个人给付" verify="给付状态|notnull" ondblclick="return showCodeListEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayTaskState',[this,PayTaskStateName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PayTaskStateName>
			</TD>
      	<!--	<td class= title>代理人部门</td>
      		<td class= input><Input class= "codeno"  name=AgentGroup  ondblclick="return showCodeList('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" ><Input class=codename  name=AgentGroupName></td>-->
      </tr>
      <tr>
      <!--	<td class= title>代理人</td>
      	<td class= input><Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this],[0]);" onkeyup="return showCodeListKey('AgentCode',[this],[0]);"></td> -->

		</TR>
      	</tr>
      	   	<TR>
      </Table>    
    </Div>     					                                                                            
    <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="easyQueryClick();">   
<!-- 显示或隐藏信息 --> 
		<br><br>

  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    		保单信息：
    		</td>
    	</tr>
    </table> 	
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLCGrpGrid" >
  					</span>
  			  	</td>
  			</tr>
  			<tr><td>  					
  				<center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage4.firstPage();"> 
     			<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage4.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage4.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage4.lastPage();">			
    			</center>
    		</td>
    		</tr>
    	</table>     
  	</div>
  	
  	  	 <tr>
  	 	<td> <INPUT VALUE="打印清单" class = cssbutton TYPE=button onclick="PrintList();"> </td>
  	 	<td> <INPUT VALUE="单打印给付通知书" class = cssbutton TYPE=button onclick="printNotice();"> </td>
  	 	<td> <INPUT VALUE="满期任务撤销" TYPE=button class = cssbutton onclick="getTaskCansel()"> </td>
  	 	</tr>
  	 <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    		应付记录：
    		</td>
    	</tr>
    </table> 	
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLJAGetGrid" >
  					</span>
  			  	</td>
  			</tr>
  			<tr><td>  					
  				<center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage5.firstPage();"> 
     			<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage5.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage5.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage5.lastPage();">			
    			</center>
    		</td>
    		</tr>
    	</table>     
  	</div>
 	
		<br>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    		满期任务清单：
    		</td>
    	</tr>
    </table> 	
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLjsGetGrid" >
  					</span>
  			  	</td>
  			</tr>
  			<tr><td>  					
  				<center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
     			<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
    			</center>
    		</td>
    		</tr>
    	</table>     
  	</div>
  	 
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
   
    		<td class= titleImg>
    		转账信息：
    		</td>
    	</tr>
    </table> 	
  <Div  id= "divGrpDue1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanLCInsuredGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
    <center>      				
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage3.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage3.lastPage();">
    </center>  
  </Div>	 
   <Div  id= "divSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="importButton" class=cssButton value="导  入"  onclick="importInsured();">
	   		 <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
			</TR>
	 	</table>
	</Div>
 <br>                  
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        账户变更查询及打印：
      </td>
     </tr>
    </table>
       <Div  id= "divLPGrpEdorMain1" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" >
						</span> 
					</td>
				</tr>
			</table>
			<Div id= "divPage2" align=center style= "display: '' ">
			  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage(); setEdorName();"> 
			  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage(); setEdorName();"> 					
			  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage(); setEdorName();"> 
			  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage(); setEdorName();"> 
			</Div>				
  	</div>
		<INPUT class= cssbutton VALUE="变更清单" TYPE=button onclick="showInsuredList();">	            
</form>
	
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>     
      		<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>   	
</body>
</html>
