<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：NewGrpPolFeeWithDrowInput.jsp
//程序功能：个人保费催收，实现数据从保费项表到应收个人表和应收总表的流转
//创建日期：2002-07-24
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="GrpDueFeeListInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="GrpDueFeeListInputInit.jsp"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurrentDate= PubFun.getCurrentDate();   
	String tCurrentYear=StrTool.getVisaYear(CurrentDate);
	String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
	String tCurrentDate=StrTool.getVisaDay(CurrentDate);    
	GlobalInput tG1 = (GlobalInput)session.getValue("GI"); 
	String   tmanageCom = tG1.ManageCom ;  
%>
<script>
    var CurrentYear=<%=tCurrentYear%>;  
	var CurrentMonth=<%=tCurrentMonth%>;  
	var CurrentDate=<%=tCurrentDate%>;
	var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
	var manageCom = <%=tmanageCom%>
</script>
</head>
<body onload="initForm();" >
	<form name=fm action=./NewGrpPolFeeWithDrow.jsp target=fraSubmit method=post>	
		<Div id="divPersonSingle" style= "display: ''">
			<Table  class= common width="100%">
				<TR  class= common>
				    <TD  class= title>应交日期范围： </TD>
				    <TD  class= title>起始日</TD>
					<TD  class= input>
						 <Input class="coolDatePicker" dateFormat="short" name=StartDate >
					</TD>
					<TD  class= title>截止日</TD>
					<TD  class= input>
						<Input class="coolDatePicker" dateFormat="short" name=EndDate >
					</TD>					
				</TR>
			</table>
			<table class= common width="100%">
				<TR class= common>
                    <TD  class= title>催收状态</TD>
					 <TD  class= input>
				   <Input class="codeNo" name="DealState" CodeData="0|^0|待收费^1|催收成功^2|应收作废^3|已撤销^4|待核销^5|全部" verify="交付费方式|&code:DealState"  ondblclick="return showCodeListEx('DealState',[this,PayModeName],[0,1]);" onkeyup="return showCodeListEx('DealState',[this,PayModeName],[0,1]);"><Input class="codeName" name="PayModeName"  elementtype="nacessary" readonly>
				 </TD>	
				 <td></td>
				</TR>
                <TR>
				   <td class=title>
						<input class="cssButton" value="查  询" type=button onclick="queryClick()">
					</td>
					<TD>  </td>
			  </TR>
			</table>
		</Div>
		<!-- 显示或隐藏信息 --> 
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单列表：
    		</td>
    	</tr>
     </table>
  	 <Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
     <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
   </center> 
   </div>
   <br>
	<div id="divDifShow" style="display: ">
		<Table  >
			<TR  >	
				
				<td >
				<INPUT class="cssButton" VALUE="打印清单" TYPE=button onclick="printList()">
				</TD>
			</TR>
		</Table>
	</div>
</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>