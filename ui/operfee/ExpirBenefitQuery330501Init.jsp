<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
	  fm.all('ManageCom').value = managecom;
	  // 保单查询条件
	  var sql = "select current date - 3 month,current date + 30 days,current date from dual ";
      var rs = easyExecSql(sql);
      fm.all('StartDate').value = rs[0][0];
      fm.all('EndDate').value = rs[0][1];
      fm.all('Today').value = rs[0][2];
	  showAllCodeName();
  }
  catch(ex)
  {
    alert("ExpirBenefitCalInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

//显示层的初始化
function initDiv()
{ 
  try
  {
      document.all("divLjsGetDraw").style.display = "none";
  }
  catch(ex)
  {
    alert("ExpirBenefitCalInit.jsp-->initDiv函数中发生异常:初始化界面错误!");
  }      
}


function initForm()
{
  try
  {
    initInpBox();
	initLjsGetGrid();
	initLjsGetDrawGrid();
  }
  catch(re)
  {
    alert("ExpirBenefitCalInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 催收记录的初始化
function initLjsGetGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="任务批次号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="SerialNo";

      iArray[2]=new Array();
      iArray[2][0]="给付任务号";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="TaskNo"; 
      
      iArray[3]=new Array();
      iArray[3][0]="保单号";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="ContNo";

      iArray[4]=new Array();
      iArray[4][0]="管理机构";         		//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="渠道";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="保费";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="保额";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="保单给付金额合计";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=200;            	        //列最大值
      iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="应给付日期";         		//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=200;            	        //列最大值
      iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="保单业务员";         		//列名
      iArray[10][1]="80px";            		//列宽
      iArray[10][2]=200;            	        //列最大值
      iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="被保人客户号";         		//列名
      iArray[11][1]="80px";            		//列宽
      iArray[11][2]=200;            	        //列最大值
      iArray[11][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[11][21]="InsuredNo";
      
      iArray[12]=new Array();
      iArray[12][0]="给付方式";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=100;            	        //列最大值
      iArray[12][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[13]=new Array();
      iArray[13][0]="代理网点";         		//列名
      iArray[13][1]="80px";            		//列宽
      iArray[13][2]=200;            	        //列最大值
      iArray[13][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="给付任务状态";         		//列名
      iArray[14][1]="80px";            		//列宽
      iArray[14][2]=200;            	        //列最大值
      iArray[14][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[14][21]="GetState"; 
      
      iArray[15]=new Array();
      iArray[15][0]="投保人客户号";         		//列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][3]=200;            	        //列最大值
      iArray[15][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[15][21]="AppntNo"; 
      
      iArray[16]=new Array();
      iArray[16][0]="给付方式";         		//列名
      iArray[16][1]="80px";            		//列宽
      iArray[16][2]=200;            	        //列最大值
      iArray[16][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[16][21]="PayMode";
      
      iArray[17]=new Array();
      iArray[17][0]="申请人类型";         		//列名
      iArray[17][1]="80px";            		//列宽
      iArray[17][2]=200;            	        //列最大值
      iArray[17][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[17][21]="Handler"; 
      
      iArray[18]=new Array();
      iArray[18][0]="给付通知书号码";         		//列名
      iArray[18][1]="80px";            		//列宽
      iArray[18][2]=200;            	        //列最大值
      iArray[18][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
      iArray[18][21]="GetNoticeNo";                   	//是否允许输入,1表示允许，0表示不允许

	  LjsGetGrid = new MulLineEnter( "fm" , "LjsGetGrid" ); 
      //这些属性必须在loadMulLine前
      LjsGetGrid.mulLineCount =0;   
      LjsGetGrid.displayTitle = 1;
      LjsGetGrid.hiddenPlus = 1;
      LjsGetGrid.hiddenSubtraction = 1;
      //LjsGetGrid.locked = 1;
      LjsGetGrid.canChk = 0;
      LjsGetGrid.canSel = 1;
      LjsGetGrid.loadMulLine(iArray);  
      LjsGetGrid.selBoxEventFuncName ="getLjsGetDrawdetail";
      LjsGetGrid.chkBoxEventFuncName ="getLjsGetDrawdetail";

	  }
      catch(ex)
      {
        alert("ExpirBenefitCalInit.jsp-->initLjsGetGrid函数中发生异常:初始化界面错误!");
      }
}

// 给付信息明细列表的初始化
function initLjsGetDrawGrid()
  {
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="被保人";         		//列名
      iArray[2][1]="0px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="受益人";         		//列名
      iArray[3][1]="200px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许	   

	    iArray[4]=new Array();
      iArray[4][0]="给付险种";         		//列名
      iArray[4][1]="70px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][21]="RiskCode";

	    iArray[5]=new Array();
      iArray[5][0]="险种给付金额";         		//列名
      iArray[5][1]="70px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="险种满期日期";         		//列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="polno";         		//列名
      iArray[7][1]="70px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="备注";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      LjsGetDrawGrid = new MulLineEnter( "fm" , "LjsGetDrawGrid" ); 
      //这些属性必须在loadMulLine前
      LjsGetDrawGrid.mulLineCount = 0;   
      LjsGetDrawGrid.displayTitle = 1;
      LjsGetDrawGrid.locked = 1;
      LjsGetDrawGrid.canSel = 0;
      LjsGetDrawGrid.hiddenPlus = 1;
      LjsGetDrawGrid.hiddenSubtraction = 1;
      LjsGetDrawGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>