//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
  }
  resetForm();
}

//重置按钮对应操作,重新查询抽档的相关信息
function resetForm()
{
  try
  {
	  initGrpPolGrid();
	  initSplitResultGrid();
	  easyQuerySplitResult();
  }
  catch(re)
  {
  	alert("在GrpDueFeePlanInput.js-->resetForm函数中发生异常!");
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//处理个案催收
function GrpSingle()
{
	if(fm.PayDate.value==null||fm.PayDate.value==""){
	
		alert("最后一期缴费日期不能为空!");
		return false;
	}
	if(GrpPolGrid.mulLineCount==0){
		alert("没有需要抽档的保障计划!");
		return false;
	}
	if(!GrpPolGrid.checkValue()){
		return false;
	}
	
	if(!PlanCodeCheck()){
		return false;
	}
//	if(compareDate("2013-1-1",fm.all("Cvalidate").value) != 1){
//		
//		if(!ActuMoneyCheck()){
//			return false;
//		}
//	}
//		
		
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面。";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit();
}

function PlanMoneyCheck(){
	
	for(i = 0;i < GrpPolGrid.mulLineCount;i++ ){
		
		//var tPrem = GrpPolGrid.getRowColData( i, 3 );
		var tActuPrem = GrpPolGrid.getRowColData( i, 4 );
		
		if(tActuPrem = 0){
			var row = i+1;
//			var flag = confirm("第"+row+"行约定缴费金额为0，本次缴费金额录入的不是0，是否继续操作");
//			if(!flag)
			alert("第"+row+"行约定缴费金额为0，本次缴费金额录入的不能是0");
			return false;
		}
	}
	
	return true;
}

function PlanCodeCheck(){
	
	for(i = 0;i < GrpPolGrid.mulLineCount;i++ ){
		
		var tPlanCode = GrpPolGrid.getRowColData( i, 6 );
		
		var sql = "select min(paytodate) from lcgrppayplandetail a "
			+ " where int(plancode)<int('"+tPlanCode+"') "
			//+ getWherePart( 'GrpContNo','GrpContNo' )
			+ " and prtno='"+fm.all("tPrtNo").value+"' "
			+ " and state='2' " +
					" and not exists (SELECT 1 FROM ljspayb a WHERE   " +
					" a.dealstate IN ('0','4','5') " +
					" AND a.otherno=(select grpcontno from lcgrpcont where prtno=a.prtno fetch first 1 rows only) " +
					" and a.othernotype='1' ) fetch first 1 rows only with ur ";
		var result = easyExecSql(sql);
		if(result!=null&&result[0][0]!=""){
			alert("该团单在本期之前还有尚未缴费的缴费计划，计划缴费日期为"+result[0][0]+"!");
			return false;
		}
	}
	
	return true;
}



function CheckData()
{
	if(!verifyInput()){
		return false;
	}
	if((fm.all('GrpContNo').value==null||fm.all('GrpContNo').value=='')&&(fm.all('PrtNo').value==null||fm.all('PrtNo').value=='')){
		alert("集体保单号或者印刷号不能同时为空!");
		return false;
	}
	var sql = "select grpcontno from lcgrpcont a "
			+ " where payintv=-1 and appflag='1' and (StateFlag is null or StateFlag = '1') "
			+ getWherePart( 'GrpContNo','GrpContNo' )
			+ getWherePart( 'PrtNo', 'PrtNo')
			+ " and exists (select 1 from lcgrppayplan where prtno=a.prtno) with ur ";
	var result = easyExecSql(sql);
	if(result==null){
		alert("该团单不是约定缴费的团单或该保单已失效,请核对!");
		return false;
	}
	
	var sql1 = "select grpcontno from lcgrpcont a "
		+ " where payintv=-1 and appflag='1' and (StateFlag is null or StateFlag = '1') "
		+ getWherePart( 'GrpContNo','GrpContNo' )
		+ getWherePart( 'PrtNo', 'PrtNo')
		+ " and exists (select 1 from lcgrppayplan where prtno=a.prtno) and state = '03050002' with ur ";
	var result1 = easyExecSql(sql1);
	if(result1!=null){
		alert("该团单已做过团单终止保全项目，不能做续期抽档!");
		return false;
	}
	
//	var sql1 = "select 1 from LPEdorEspecialData a "
//		+ " where  edortype='01' "
//		+ getWherePart( 'EdorAcceptNo','GrpContNo' )
//		+ getWherePart( 'EdorNo', 'PrtNo')
//		+ "  with ur ";
//	var result1 = easyExecSql(sql1);
//	if(result1!=null){
//		alert("该团单已经拆分过最后一期缴费，不能再次做该操作!");
//		return false;
//	}
	return true;
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpContGrid();
	initGrpPolGrid();
	//initGrpJisPayGrid();
	initSplitResultGrid();
	easyQuerySplitResult();
	if(!CheckData()){
		return false;
	}
	getContInfo();
	getShouldMoney();
	//getJisInfo();
	getPolInfo();
	if(compareDate("2013-1-1",fm.all("Cvalidate").value) != 1)
	getGrpPayDue();
}

function easyQuerySplitResult(){
	
	var sql1 = "select State,sum(double(edorvalue)),EdorNo from LPEdorEspecialData a "
//		+ " where  edortype='01' "
		+ " where  detailType='SplitDueFee' "
		+ getWherePart( 'EdorAcceptNo','GrpContNo' )
		+ getWherePart( 'EdorNo', 'PrtNo')
		+ " group by state,EdorNo "
		+ " order by int(state) desc with ur ";
	var result1 = easyExecSql(sql1);
	if(result1!=null){
		var tPlanCode = result1[0][0];
		var tPrem = result1[0][1];
		fm.PrtNo.value = result1[0][2];
		var strSQL = " select '"+tPlanCode+"',a.paytodate,'"+tPrem+"',b.plancode " +
				" ,b.paytodate," +
				"sum(b.prem) "
	         +" from LCGrpPayPlan a right join LCGrpPayPlan b on a.prtno=b.prtno and a.contplancode=b.contplancode where  "
			 +"   a.plancode='"+tPlanCode+"' and int(b.plancode)>=int(a.plancode) "
			 + getWherePart( 'a.PrtNo','PrtNo' )
			 +" group by a.paytodate,b.plancode,b.paytodate";
   turnPage3.queryModal(strSQL, SplitResultGrid);
		
	}
	
}

function getShouldMoney(){
	
	var strSQL = " select nvl(sum(prem),0) "
        +" from lcgrppaydue a where  "
		 +" 1=1 and confstate='02' "
		 + getWherePart( 'PrtNo','tPrtNo' );
	var result = easyExecSql(strSQL);
	if(result!=null){
		fm.all("ShouldMoney").value=result[0][0];
	}
	else{
		alert("未查询到该保单的应收记录");
	}

}

// 数据返回父窗口
function returnParent()
{
	
	if( fm.all("tGrpContNo").value == null || fm.all("tGrpContNo").value == '' )
		alert( "请先查询保单信息。" );
	else
	{
		try
		{
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ fm.all("tGrpContNo").value+"&ContType=1");
		}
		catch(ex)
		{
			alert( "查询出错!" );
		}
	}
}

//查询团单信息
function getContInfo()
{
	var strSQL = " select b.GrpContNo,b.PrtNo,b.GrpName,'',b.CValiDate,(select sum(sumactupaymoney) from ljapaygrp where grpcontno=b.grpcontno and paytype='ZC'),'约定缴费',ShowManageName(b.ManageCom),db2inst1.getUniteCode(b.AgentCode),b.ProposalGrpContNo "
	         +" from LCGrpCont b where  "
			 +" 1=1 "
			+ getWherePart( 'GrpContNo','GrpContNo' )
			 + getWherePart( 'ManageCom', 'ManageCom','like')
//			 + getWherePart( 'AgentCode','AgentCode' )
			 + getWherePart( 'PrtNo','PrtNo' );
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strSQL = strSQL+" and b.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
	}
    turnPage.queryModal(strSQL, GrpContGrid);
    fm.all("tPrtNo").value=GrpContGrid.getRowColData( 0, 2 );
    fm.all("tGrpContNo").value=GrpContGrid.getRowColData( 0, 1 );
    fm.all("ProposalGrpContNo").value=GrpContGrid.getRowColData( 0, 10 );
    fm.all("Cvalidate").value=GrpContGrid.getRowColData( 0, 5 );
}



//查询收费信息
function getPolInfo()
{
		var strSQL = "select contplancode,(select contplanname from lccontplan where proposalgrpcontno=a.proposalgrpcontno and contplancode=a.contplancode), " 
					+ " prem,(select varchar(sum(prem)) from lcgrppayactu where contplancode=a.contplancode and prtno=a.prtno and plancode=a.plancode and state = '2'),"
					+ " paytodate,plancode,(select max(getnoticeno) from lcgrppayactu where prtno=a.prtno and plancode=a.plancode and state = '2')"
		            + " from lcgrppayplan a "
		            + " where prtno= '" + fm.all("tPrtNo").value + "'  "
		            + " and int(plancode)=(select max(int(plancode)) from lcgrppayplan b where prtno= '" + fm.all("tPrtNo").value + "'  )" 
		            + " and not exists (select 1 from lcgrppayplandetail c where c.prtno=a.prtno and int(c.plancode)<int(a.plancode) and c.state='2') "
		            + " and exists (select 1 from lcgrppayplandetail c where c.prtno=a.prtno and c.plancode=a.plancode and c.state='2') "  
		            + " and not exists (select 1 from ljspay c where c.getnoticeno=(select max(getnoticeno) from lcgrppayactu where prtno=a.prtno and plancode=a.plancode and state = '2')) "
		            + " order by contPlanCode";
		turnPage2.queryModal(strSQL, GrpPolGrid);
		if(GrpPolGrid.mulLineCount==0){
			document.all.singleDue.disabled=true;
			alert("保单最后一期已经抽档或者之前有未核销的期次");
			return;
		}
		else{
			document.all.singleDue.disabled=false;
		}
		var getnoticeno=GrpPolGrid.getRowColData( 0, 7 );
		
		
//		当前是否已经抽档
//		if(getnoticeno!=null&&getnoticeno!=""){
//			var auditsql = "select 1 from ljspay a "
//				+ " where getnoticeno='"+getnoticeno+"' and cansendbank='Y' with ur ";
//			var auditflag = easyExecSql(auditsql);
//			if(auditflag!=null && ""!=auditflag){
//				
//				fm.Flag.value = "Y"
//					
//			}
//			if(fm.Flag.value!="Y"){
//				document.all.singleDue.disabled=true;
//	    		fm.all("Infomation").style.display="";
//			}
//			else{
//				document.all.singleDue.disabled=false;
//	    		fm.all("Infomation").style.display="none";
//			}
//			
//		}else{
//			document.all.singleDue.disabled=false;
//    		fm.all("Infomation").style.display="none";
//		}
		fm.all("GetNoticeNo").value=getnoticeno;
}

function getGrpPayDue()
{
		var strSQL = " select contplancode,plancode,paytodate,0,prem from lcgrppayplan " +
					"	where prtno= '" + fm.all("tPrtNo").value + "' " 
					+" and int(plancode)=1 " 
					+  " union all select contplancode,plancode, " 
					+ "  paytodate,(select nvl(sum(prem),0) " +
							" from lcgrppaydue " +
							"where prtno=a.prtno and plancode=a.plancode " +
							"and contplancode=a.contplancode and confstate='02'),(select sum(prem) " +
							" from lcgrppayplan " +
							"where prtno=a.prtno and plancode=a.plancode " +
							"and contplancode=a.contplancode) "
		            + " from lcgrppaydue a "
		            + " where prtno= '" + fm.all("tPrtNo").value + "' " 
//		            		+ " and int(plancode)<=(select max(int(plancode)) from lcgrppayplan b where " +
//		            		" prtno= '" + fm.all("tPrtNo").value + "'  " +
//		            				" and paytodate between '"+fm.all("StartDate").value+"' " +
//		            						" and '"+fm.all("EndDate").value+"'" 
		            //+ " and exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='2') "  
		            //+ " and not exists (select 1 from lcgrppayplandetail c where c.prtno=b.prtno and c.plancode=b.plancode and c.state='1')) )"
		            + "  group by contplancode,plancode,paytodate,prtno " +
//		            		" union all " +
//		            		" select contplancode,plancode, " 
//					+ "  paytodate,prem,prem "
//		            + " from lcgrppaydue a "
//		            + " where prtno= '" + fm.all("tPrtNo").value + "'  " +
//		            		"and int(plancode)>(select max(int(plancode)) " +
//		            		"from lcgrppayplan b where prtno= '" + fm.all("tPrtNo").value + "'  " +
//		            				"and paytodate between '"+fm.all("StartDate").value+"' " +
//		            						"and '"+fm.all("EndDate").value+"' )" +
		            								"  order by contPlanCode,plancode";
		turnPage4.queryModal(strSQL, GrpPayDueGrid);
//		if(GrpPayDueGrid.mulLineCount==0){
//			document.all.singleDue.disabled=true;
//			alert("保单在该日期区间没有待抽档的续期!");
//			return;
//		}

}

//打印PDF前往打印管理表插入一条数据
function printPDF()
{
	if(!checkPrint())
	{
	  return false;
	}
		fm.action = "../uw/PDFPrintSave.jsp?Code=99&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+fm.all("GetNoticeNo").value;
		fm.submit();
}

function checkPrint()
{
	if(fm.all("GetNoticeNo").value==null||fm.all("GetNoticeNo").value==""){
    alert("没有查询到可打印的催收记录，请先催收！");
    return false;
  }
	if(fm.Flag.value=="Y"){
		alert("本次缴费需要进行审批，请先进行审批操作！");
	    return false;
	}
  return true;
}

