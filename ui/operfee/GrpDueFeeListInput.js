//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	 fm.all('Dif').value=0;
  }
}

//查询按钮
function queryClick(){
    var strState = fm.all('DealState').value //催收状态 3：全部
	if (strState=='5')
	{
		var strSQL = "select distinct  b.SerialNo,b.GetNoticeNo,a.GrpName,a.GrpContNo,b.SumDuePayMoney,(select codename  from ldcode where codetype='paymode' and code=a.PayMode),min(c.CurPayToDate),min(c.LastPayToDate),"
		   +"(select codename  from ldcode where codetype='dealstate' and code=b.dealstate),b.Operator"
		   + " from LCGrpCont a,LJSPayB b,LJSPayGrpB c"
           + " where a.AppFlag='1'"		 
		   + " and b.OtherNo=a.GrpContNo and b.OtherNoType='1'"
		   + " and c.GrpContNo=b.OtherNo" 
		   + " and c.LastPayToDate>='" +fm.all("StartDate").value+ "'"
		   + " and c.LastPayToDate<='" +fm.all("EndDate").value+ "'"
		   + " and b.GetNoticeNo = c.GetNoticeNo and a.managecom like '"+ manageCom+"%%'"
		   + " group by  b.SerialNo,b.GetNoticeNo,a.GrpName,a.GrpContNo,b.SumDuePayMoney,a.paymode, b.dealstate,b.Operator"
		   + " order by b.GetNoticeNo desc";
	}else
	{
		var strSQL =  "select distinct  b.SerialNo,b.GetNoticeNo,a.GrpName,a.GrpContNo,b.SumDuePayMoney,(select codename  from ldcode where codetype='paymode' and code=a.PayMode),min(c.CurPayToDate),min(c.LastPayToDate),"
		   +"(select codename  from ldcode where codetype='dealstate' and code=b.dealstate),b.Operator"
		   + " from LCGrpCont a,LJSPayB b,LJSPayGrpB c"
		   + " where   a.AppFlag='1'"		 
		   + " and b.OtherNo=a.GrpContNo and b.OtherNoType='1'"
		   + " and c.GrpContNo=b.OtherNo" 
		   + " and c.LastPayToDate>='" +fm.all("StartDate").value+ "'"
		   + " and c.LastPayToDate<='" +fm.all("EndDate").value+ "'"
		   + " and b.GetNoticeNo = c.GetNoticeNo and a.managecom like '"+ manageCom+"%%'"
		   + " and b.DealState='"+strState+ "'"
		   + " group by  b.SerialNo,b.GetNoticeNo,a.GrpName,a.GrpContNo,b.SumDuePayMoney,a.paymode, b.dealstate,b.Operator"
		   + " order by b.GetNoticeNo desc";
	}
	turnPage.queryModal(strSQL,GrpContGrid);
	
}

//打印清单
function printList()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("没有催收任务");
		return false;
	}
	var tStartDate = fm.all('StartDate').value;
	var tEndDate = fm.all('EndDate').value;
	var strDealState = fm.all('DealState').value //催收状态
	window.open("../f1print/GrpPremDueFeeListPrint.jsp?StartDate="+ tStartDate + "&EndDate=" + tEndDate + "&DealState="+strDealState);
	
}