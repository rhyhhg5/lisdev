//               该文件中包含客户端需要处理的函数和事件
var mSql = "";
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  //	parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	
  }
}




//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


// 通过保单查询
function easyQueryClick()
{
	if(fm.all("GrpContNo").value==null || fm.all("GrpContNo").value==""){
		alert("请输入保单号！");
		return false;
	}
	// 初始化表格
	initGrpContGrid();
	initGrpJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收

		var strSQL = "select distinct b.PayNo,a.GrpName,a.GrpContNo,"
	   +"(select sum(SumDuePayMoney) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),'', "
	   +"(select min(LastPayToDate) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),b.SumActuPayMoney,b.MakeDate,b.Operator"
	   + ",case when (select count(*) from LOPRTManager where OtherNoType='01' and OtherNo=a.grpcontno and Code='90' and StandbyFlag2=b.PayNo )>0 then '已打印' else '未打印' end "
	   +" from LCGrpCont a,LJAPay b"
     + " where 1=1"
	   + getWherePart( 'a.GrpContNo ','GrpContNo' ) 		
	   + dealWMD		  
	   + " AND a.GrpContNo=b.IncomeNo and b.IncomeType='1'"			  
	   + " and a.AppFlag='1' "
	   + " and b.GetNoticeNo in (select GetNoticeNo from LJSPayB WHERE OtherNoType='1' and DealState='1')"	   
     + " order by a.GrpContNo ,b.PayNo desc" 
		;


	turnPage2.queryModal(strSQL, GrpContGrid); 
	setPeoples2Input();
}

//显示缴费人数
function setPeoples2Input()
{
  for(var i = 0; i < GrpContGrid.mulLineCount; i++)
  {
    //先查无名单
    var sql = "  select sum(peoples2Input) from LCNoNamePremTrace a, LJAPay b "
              + "where a.getNoticeNo = b.getNoticeNo "
              + "   and b.payNo = '" + GrpContGrid.getRowColDataByName(i, "payNo") + "' ";
    var rs = easyExecSql(sql);
    if(rs && rs[0][0] != "" && rs[0][0] != "null" && rs[0][0] != "0")
    {
      GrpContGrid.setRowColDataByName(i, "peoples2", rs[0][0])
    }
    //若不是无名单则查保单数据
    else
    {
      sql = "  select peoples2 from LCGrpCont a, LJAPay b "
            + "where a.grpContNo = b.incomeNo "
            + "   and b.payNo = '" + GrpContGrid.getRowColDataByName(i, "payNo") + "' ";
      var rs = easyExecSql(sql);
      if(rs && rs[0][0] != "" && rs[0][0] != "null")
      {
        GrpContGrid.setRowColDataByName(i, "peoples2", rs[0][0])
      }
    }
  }
}

// 查询结果列表
function multQueryClick()
{
	
	// 初始化表格
	initGrpJisPayGrid();
	initGrpContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收

		var strSQL = "select a.SerialNo,a.MakeDate,a.Operator,"
	   +"(select case when DealState='3' then '完成' else '进行中' end from LCUrgeVerifyLog where SerialNo=a.SerialNo)"
	   +" from LJAPay a"
     + " where a.IncomeType='1'"
	   + getWherePart( 'a.GrpContNo ','GrpContNo' ) 
	   + getWherePart( 'a.MakeDate ','StartDate' ,'>=') 
	   + getWherePart( 'a.MakeDate ','EndDate','<=' ) 
	   + getWherePart( 'a.ManageCom ','ManageCom','like' ) 
	   + dealWMD.replace("a.grpContNo", "a.incomeNo")
	   + " and exists(select 'X' from LJAPayGrp where PayNo=a.PayNo and paycount>1)"	
	   + " and exists(select 'X' from LJSPayB WHERE OtherNoType='1' and DealState='1' and GetNoticeNo=a.GetNoticeNo)"	  
	   + " order by a.PayNo desc" 
		;
	      
	turnPage1.queryModal(strSQL, GrpJisPayGrid); 
	
}

//通过批次列表查询明细
function quaryList()
{
	// 初始化表格
	initGrpContGrid();

  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var rowNum=GrpJisPayGrid.mulLineCount;
	if (rowNum==0) return false;
	var tCount = 0;
	var tSerialNo = "";
   for(var i=0;i<rowNum;i++)
   {                                                                   
       if(GrpJisPayGrid.getChkNo(i))
       {
				   tCount = tCount+1;
		
				   if (tCount == 1)
				   {
					   tSerialNo = "'" + GrpJisPayGrid.getRowColData( i , 1 )+ "'";
		
				   }else
				   {
		           tSerialNo = tSerialNo + ",'" + GrpJisPayGrid.getRowColData( i , 1 )+ "'";
				   }
       }
   }
   var strState = fm.all('PrintState').value //催收状态 2：全部,0: 未打印；1：已打印
   if (strState=='2')
	 {
	 	 var strSQL = "select distinct b.PayNo,a.GrpName,a.GrpContNo,"
	   +"(select sum(SumDuePayMoney) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),'', "
	   +"(select min(LastPayToDate) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),b.SumActuPayMoney,b.MakeDate,b.Operator"
	 	 + ",case when (select count(*) from LOPRTManager where OtherNoType='01' and OtherNo=a.grpcontno and Code='90' and StandbyFlag2=b.PayNo )>0 then '已打印' else '未打印' end "
	   +" from LCGrpCont a,LJAPay b"
     + " where b.SerialNo in (" + tSerialNo + ")"  
	   + " AND a.GrpContNo=b.IncomeNo and b.IncomeType='1'"			  
	   + " and a.AppFlag='1' "
	   + dealWMD
	   + " and b.GetNoticeNo in (select GetNoticeNo from LJSPayB WHERE OtherNoType='1' and DealState='1')"
	   + " group by b.PayNo,a.GrpName,a.GrpContNo,b.SumActuPayMoney,b.MakeDate,b.Operator,b.PayNo"
     + " order by a.GrpContNo ,b.PayNo desc" 
		;	   
	 }else if (strState=='0')
	 {
		 var strSQL = "select distinct b.PayNo,a.GrpName,a.GrpContNo,"
	   +"(select sum(SumDuePayMoney) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),'', "
	   +"(select min(LastPayToDate) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),b.SumActuPayMoney,b.MakeDate,b.Operator"
	   + ",'未打印'"
	   +" from LCGrpCont a,LJAPay b"
     + " where b.SerialNo in (" + tSerialNo + ")" 
     + " and (select count(*) from LOPRTManager where OtherNoType='01' and OtherNo=a.grpcontno and Code='90'  and StandbyFlag2=b.PayNo)=0 " 
	   + " AND a.GrpContNo=b.IncomeNo and b.IncomeType='1'"			  
	   + " and a.AppFlag='1' "
	   + dealWMD
	   + " and b.GetNoticeNo in (select GetNoticeNo from LJSPayB WHERE OtherNoType='1' and DealState='1')"
	   + " group by b.PayNo,a.GrpName,a.GrpContNo,b.SumActuPayMoney,b.MakeDate,b.Operator"
     + " order by a.GrpContNo ,b.PayNo desc" 
		;	   
	}else
	{
		var strSQL = "select distinct b.PayNo,a.GrpName,a.GrpContNo,"
	   +"(select sum(SumDuePayMoney) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),'', "
	   +"(select min(LastPayToDate) from LJAPayGrp where grpcontno =a.GrpContNo and PayNo=b.PayNo),b.SumActuPayMoney,b.MakeDate,b.Operator"
	   + ",'已打印'"
	   +" from LCGrpCont a,LJAPay b"
     + " where b.SerialNo in (" + tSerialNo + ")" 
     + " and (select count(*) from LOPRTManager where OtherNoType='01' and OtherNo=a.grpcontno and Code='90'  and StandbyFlag2=b.PayNo)>0 " 
	   + " AND a.GrpContNo=b.IncomeNo and b.IncomeType='1'"			  
	   + " and a.AppFlag='1' "
	   + dealWMD
	   + " and b.GetNoticeNo in (select GetNoticeNo from LJSPayB WHERE OtherNoType='1' and DealState='1')"
	   + " group by b.PayNo,a.GrpName,a.GrpContNo,b.SumActuPayMoney,b.MakeDate,b.Operator"
     + " order by a.GrpContNo ,b.PayNo desc" 
		;	   
	}
	mSql = strSQL;//alert(easyExecSql(mSql));   
	turnPage2.queryModal(strSQL, GrpContGrid); 
	setPeoples2Input();
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		var cGrpContNo = GrpContGrid.getRowColData( tSel - 1, 1 );
		try
		{
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ cGrpContNo+"&ContType=1");
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}
//打印清单
function printList()
{
	var tGrpContNo = fm.all('GrpContNo').value;
	var tSeriaNo = "";
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	
	var tUrl = "../operfee/GrpPayListPrintEXCEL.jsp?GrpContNo="+ tGrpContNo + "&SerialNo=" + tSeriaNo + "&loadFlag=" + fm.loadFlag.value;
	window.open(tUrl, "printList");
	
}

//打印对帐单
function printPayComp()
{

	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
  var selNo = GrpContGrid.getSelNo();
  if(selNo == null || selNo == 0)
  {
  	  alert("请选择一条记录");
  	  return false;
  }
  var cPayNo = GrpContGrid.getRowColData( selNo - 1, 1 ); 
	window.open("../operfee/GrpPayCompPrint.jsp?PayNo="+ cPayNo );
}


function printPayCompPDF()
{
	//先往打印表存数
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
		
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}
	
	var cPayNo = GrpContGrid.getRowColData( tChked[0], 1 ); 
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '90' and standbyflag2='"+cPayNo+"'");

	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		fm.all('PayNo').value = GrpContGrid.getRowColData( tChked[0], 1 );
		fm.action="./GrpPayForPrtIns.jsp";
		fm.submit();
	}
	else
	{
		printPDF();
	}
}

function printPDF()
{
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
		
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}
	
	
	var cPayNo = GrpContGrid.getRowColData( tChked[0], 1 ); 
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '90' and standbyflag2='"+cPayNo+"'");
	fm.action="../uw/PrintPDFSave.jsp?Code=090&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq;
	fm.submit();
//	window.open("../uw/PrintPDFSave.jsp?Code=090&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq );	
	
}


function printBatchPDF()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	
	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	if(tChked.length == 0)
	{
		fm.all('pdfSql').value = mSql;
		fm.action="./GrpPayAllBatPrtInsNew.jsp";
		fm.submit();
	}
	else
	{
		fm.action="./GrpPayBatPrtInsNew.jsp";
		fm.submit();
	}
}

function printPayCompPDFNew()
{
	//先往打印表存数
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
		
	if(tChked.length != 1)
	{
		alert("请选择一条记录");
		return false;
	}
	
	var cPayNo = GrpContGrid.getRowColData( tChked[0], 1 ); 
	//var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '90' and standbyflag2='"+cPayNo+"'");

	//if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	//{
	//	fm.all('PayNo').value = GrpContGrid.getRowColData( tChked[0], 1 );
	//	fm.action="./GrpPayForPrtIns.jsp";
	//	fm.submit();
	//}
	//else
	//{
	//	printPDF();
	//}
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=90&StandbyFlag2="+cPayNo;
	fm.submit();
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}