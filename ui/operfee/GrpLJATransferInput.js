//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  //	parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	
  }
}




//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function CheckGrpPolNo()
{
    var tSel = GrpContGrid.getSelNo();	
    if( tSel == 0 || tSel == null )
    {
		alert( "请先选择一条记录，再点击'团单续期个案催收'按钮。" );
		return false;
    }
    var tRow = GrpContGrid.getSelNo() - 1; 
    //说明应该在这里把窗体相关值填充！杨红于2005-07-19注释
    fm.PrtNo.value=GrpContGrid.getRowColData(tRow, 2);
    fm.all('ProposalGrpContNo').value=GrpContGrid.getRowColData(tRow, 1);
    return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



function CheckDate()
{
  if(!isDate(fmMulti.all('StartDate').value)||!isDate(fmMulti.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fmMulti.all('StartDate').value,fmMulti.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

// 查询按钮
function easyQueryClick()
{
	
	// 初始化表格
	initGrpContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收

		var strSQL = "select distinct a.GrpContNo,a.GrpName,a.CValiDate,min(b.PayToDate),a.AppFlag,c.SumActuPayMoney,getUniteCode(a.AgentCode)"
		   +" from LCGrpCont a,LCCont b,LJAPay c"
           + " where 1=1"
		   + getWherePart( 'a.GrpContNo ','GrpContNo' ) 
		   + getWherePart( 'a.AppntNo ','AppntNo' ) 
		  // + getWherePart( 'c.GetNoticeNo ','GetNoticeNo' )
		   //+ " and c.RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')"
		   + " and c.IncomeType='1'"			   
		   + " and a.GrpContNo=b.GrpContNo"
		   + " and c.IncomeNo=b.GrpContNo"
		   + " and a.AppFlag='1' and b.AppFlag='1'"
		   + " group by a.GrpContNo,a.GrpName,a.CValiDate,a.AppFlag,c.SumActuPayMoney,a.AgentCode"
		;
	turnPage.queryModal(strSQL, GrpContGrid); 
	initGrpJisPayGrid();
	
}



//催收完成后查询催收纪录
function afterJisPayQuery()
{
	// 初始化表格
	initGrpJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
     var  selNo=  GrpContGrid.getSelNo();
	 if (selNo==null)
	 {
		 alert("请选择一条记录");
	 }else
	{
		var tGrpContNo=  GrpContGrid.getRowColData(selNo-1,1);
		var strSQL ="select distinct b.PayNo,a.PayDate,a.SumDuePayMoney,b.SumActuPayMoney,c.dif,min(d.PayToDate),b.MakeDate,b.EnterAccDate,'' "
	       + " from  ljSpayb a,ljapay b,LCGrpCont c,LCCont d" 
		   + " where  b.Incometype='1' AND a.OtherNoType='1'"
	       + " and b.IncomeNo='" +tGrpContNo + "'"
	       //+" and b.RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')"
		   + " and a.OtherNo=b.IncomeNo and b.IncomeNo=c.GrpContNo and d.GrpContNo=c.GrpContNo"
		   + " group by b.PayNo,a.PayDate,a.SumDuePayMoney,b.SumActuPayMoney,c.dif,b.MakeDate,b.EnterAccDate"
	  ;
	}  
	 
	
	turnPage4.queryModal(strSQL, GrpJisPayGrid); 
}


//作废应收记录按钮
function cancelRecord()
{
	var  selNo=  GrpJisPayGrid.getSelNo();
	if (selNo==null || selNo==0 )
	 {
		 alert("请选择一条应收记录");
		 return false;
	 }else
	{
		var tPayNo = GrpJisPayGrid.getRowColData(selNo - 1,1)
		fm.all('SubmitPayNo').value=tPayNo; 		
		var showStr="正在，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
	}
}