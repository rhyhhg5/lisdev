<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： IndiPayForBatPrtIns.jsp
//程序功能：
//创建日期：2005-12-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.lang.String"%>
<%
    boolean operFlag = true;
	CErrors tError = null;

	int    tCount=0;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	String tPrintServerPath = "";

	GlobalInput tG = (GlobalInput)session.getValue("GI");
	tG=(GlobalInput)session.getValue("GI");
	//tG.ClientIP = request.getRemoteAddr(); 
	tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
	tG.ServerIP =tG.GetServerIP();
	
//	String tPayNo = request.getParameter("PayNo");
//	String tPayDate = request.getParameter("PayDate");		
//	String tDueMoney = request.getParameter("DueMoney");
//	String tDif = request.getParameter("Dif");

  	transact = "batch";
//    String tOutXmlFile  = application.getRealPath("")+"\\";
//	LDSysVarSchema	tLDSysVarSechma =new LDSysVarSchema();
//	tLDSysVarSechma.setSysVarValue(tOutXmlFile);
//	LJAPaySet tLJAPaySet = new LJAPaySet();
    LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();

	try
  	{
		String tPayNo[] = request.getParameterValues("GrpContGrid1");	 
	  String tChk[] = request.getParameterValues("InpGrpContGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  
	
		tCount = tPayNo.length;
		System.out.println("----tCount:"+tCount+"-----");
		for(int j = 0; j < tCount; j++)
		{
			if(tChk[j].equals("1"))  
			{
      LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
      tLOPRTManagerSchema.setStandbyFlag2(tPayNo[j]);
      tLOPRTManagerSchema.setCode("90");
      tLOPRTManagerSet.add(tLOPRTManagerSchema);
				System.out.println("---------tPayNo: "+tPayNo[j]);
			}
  		}
  		
  	
		// 准备传输数据 VData
  		VData tVData = new VData();
		tVData.add(tLOPRTManagerSet);
		//tVData.add(tLDSysVarSechma);
  		tVData.add(tG);
  		PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
   if(!tPDFPrintBatchManagerBL.submitData(tVData,"batch")) {           
      FlagStr = "Fail";
      Content = tPDFPrintBatchManagerBL.mErrors.getFirstError().toString();
    }
    else {
       if( tPDFPrintBatchManagerBL.mErrors.getErrorCount()>0){
           Content = tPDFPrintBatchManagerBL.mErrors.getFirstError().toString();
           FlagStr = "PrintError"; 
        }else{
           Content = "打印成功！";
           FlagStr = "Succ"; 
        }         
    }

	}
	catch(Exception ex)
	{
    	Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}

%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
		alert("打印成功");
	}
	else
	{
		alert("<%=Content%>");
	}
</script>
</html>