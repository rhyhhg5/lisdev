var turnPage = new turnPageClass();
var mSql="";

//下载清单
function download()
{
	if(!verifyInput2())
	{
         return false;
    }
    
	var tManageCom = fm.all('ManageCom').value;			//管理机构代码
	var tContType = fm.all('ContType').value;			//保单类型
	var tRiskType = fm.all('RiskType').value;			//险种类型
	var tSaleChnl = fm.all('SaleChnl').value;			//销售渠道
	var tContinueType = fm.all('ContinueType').value;	//继续率类型
	var tContNo = fm.all('ContNo').value;				//保单号
	var tStartDate = fm.all('StartDate').value;			//生效开始日期
	var tEndDate = fm.all('EndDate').value;				//生效结束日期
	
	//生效开始日期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("请输入生效开始日期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("生效开始日期",tStartDate))
		{
			fm.all('StartDate').focus();
			return false;
		}
	}
	//生效结束日期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("请输入生效结束日期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("生效结束日期",tEndDate))
		{
			fm.all('EndDate').focus();
			return false;
		}
	}
	//生效日期起止期三个月校验
	var t1 = new Date(tStartDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    if(t2-t1<0)
    {
      alert("生效开始日期不能晚于生效结束日期 "+tEndDate)
	  return false;
    }
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>92 )
    {
	  alert("生效日期起止日期不能超过3个月！")
	  return false;
    }

	fm.submit();
	
}

//日期格式校验
function checkDateFormat(tName,strValue) 
{
	if (!isDate(strValue))
	{
		alert("输入的["+tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}
