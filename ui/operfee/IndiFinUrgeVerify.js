  //               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var showInfo;
var mLJASql;

// 查询保单信息
//pmType 1:单张核销;2:批次核销

function easyQueryClick(pmType) {
//modify by fuxin 2009-6-11 10:48:43 在个险续期核销屏蔽万能险种
  var strSql="";
  initNormPayCollGrid(); 
  	 
  if (pmType == "1")
  { 	
    //续期收费
    strSql = "select c.otherNo,b.AppntName,a.GetNoticeNo,c.SumDuePayMoney ,min(a.LastPayToDate),b.Dif,"
	     + "(select max(ConfMakeDate) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0'),(select codename from ldcode where codetype='paymode' and code=b.PayMode)"
	     + ",min(a.CurPayToDate),getUniteCode(a.AgentCode),c.Operator "
	     + "from LJSPayPerson a ,LCCont  b,LJSPay c "
	     + "where a.contNo = '" + fm.all('GrpContNo').value + "' "
	     + "   and c.OtherNo=b.ContNo"
	     + "   and a.ContNo=b.ContNo"
	     + " and a.getnoticeno = c.getnoticeno "
	     //+ "   and b.PayIntv > 0 "
	     + "   and c.othernotype='2' "
	     + "   and c.manageCom like '" + manageCom + "%%' "
	     + "   and exists(select RiskCode from LMRisk where (CPayFlag='Y') and riskcode=a.RiskCode )"
	     + "   and ((select count(*) from LJTempFee where TempFeeNo= c.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
//zhanggm 20100929 修改万能校验，因为系统中存在一个保单同时包含万能与普通险种。
         + " and not exists(select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.riskcode not in ('332301','334801','340501','340601') and lmriskapp.risktype4='4' and lcpol.Contno = b.ContNo ) "
	     + "group by c.otherNo,b.AppntName,a.GetNoticeNo,a.AgentCode,b.ContNo,b.Dif,b.PayMode,c.SumDuePayMoney,c.Operator "
	     
	     + " union "
	     
	     //续保收费
	     + "select c.otherNo,b.AppntName,a.GetNoticeNo,c.SumDuePayMoney ,min(a.LastPayToDate),b.Dif,"
	     + "(select max(ConfMakeDate) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0'),(select codename from ldcode where codetype='paymode' and code=b.PayMode)"
	     + ",min(a.CurPayToDate),getUniteCode(a.AgentCode),c.Operator "
	     + "from LJSPayPerson a ,LCCont  b,LJSPay c "
	     + "where c.otherno = '" + fm.all('GrpContNo').value + "' "
	     + "   and c.othernotype='2' "
	     + " and a.getnoticeno = c.getnoticeno "
	     + "   and c.manageCom like '" + manageCom + "%%' "
	     + "   and ( b.ContNo =(select distinct contNo from LCRnewStateLog where contno=c.otherNo and state > '3')) "
	     //+ "   and exists(select RiskCode from LMRisk where ( RNewFlAg!='N') and riskcode=a.RiskCode ) "
	     + "   and ((select count(*) from LJTempFee where TempFeeNo= c.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
	     + "   and a.ContNo=b.ContNo "
	     + " and a.riskcode not in('320106','120706') "
//zhanggm 20100929 修改万能校验，因为系统中存在一个保单同时包含万能与普通险种。
		 + " and not exists(select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.risktype4='4' and lcpol.Contno = b.ContNo ) "
	     + "group by c.otherNo,b.AppntName,a.GetNoticeNo,a.AgentCode,b.ContNo,b.Dif,b.PayMode,c.SumDuePayMoney,c.Operator ";
  }
  else
  { 
	  //续期收费
	  strSql = "select c.otherNo,b.AppntName,a.GetNoticeNo,c.SumDuePayMoney ,min(a.LastPayToDate),b.Dif,"
	     + "(select max(ConfMakeDate) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0'),(select codename from ldcode where codetype='paymode' and code=b.PayMode)"
	     + ",min(a.CurPayToDate),getUniteCode(a.AgentCode),c.Operator "
	     + "from LJSPayPerson a ,LCCont  b,LJSPay c "
	     + " where a.LastPayToDate <='" + fm.all('EndDate').value  + "' " 
	     + "   and c.OtherNo=b.ContNo"
	     + "   and a.ContNo=b.ContNo"
	     + " and a.getnoticeno = c.getnoticeno "
	    // + "   and b.PayIntv > 0 "
	     + "   and c.othernotype='2' "
	     + "   and c.manageCom like '" + manageCom + "%%' "
	     + "   and exists(select RiskCode from LMRisk where (CPayFlag='Y') and riskcode=a.RiskCode )"
	     + "   and ((select count(*) from LJTempFee where TempFeeNo= c.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
	     //zhanggm 20100929 修改万能校验，因为系统中存在一个保单同时包含万能与普通险种。
		 + " and not exists(select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.riskcode not in ('332301','334801','340501','340601') and lmriskapp.risktype4='4' and lcpol.Contno = b.ContNo ) "
	     + "group by c.otherNo,b.AppntName,a.GetNoticeNo,a.AgentCode,b.ContNo,b.Dif,b.PayMode,c.SumDuePayMoney,c.Operator "
	     
	     + " union all "
	     
	     //续保收费
	     + "select c.otherNo,b.AppntName,a.GetNoticeNo,c.SumDuePayMoney ,min(a.LastPayToDate),b.Dif,"
	     + "(select max(ConfMakeDate) from LJTempFee where TempFeeNo= a.GetNoticeNo  and ConfFlag='0'),(select codename from ldcode where codetype='paymode' and code=b.PayMode)"
	     + ",min(a.CurPayToDate),getUniteCode(a.AgentCode),c.Operator "
	     + "from LJSPayPerson a ,LCCont  b,LJSPay c "
	     + " where a.LastPayToDate <='" + fm.all('EndDate').value  + "' "
	     + "   and c.othernotype='2' "
	     + " and a.getnoticeno = c.getnoticeno "
	     + "   and c.manageCom like '" + manageCom + "%%' "
	     + "   and ( b.ContNo =(select distinct contNo from LCRnewStateLog where contno=c.otherNo and state > '3')) "
	     + "   and exists(select RiskCode from LMRisk where ( RNewFlAg!='N') and riskcode=a.RiskCode ) "
	     + "   and a.ContNo=b.ContNo "
	     + "   and ((select count(*) from LJTempFee where TempFeeNo= c.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
	     + " and a.riskcode not in('320106','120706') "
	     //zhanggm 20100929 修改万能校验，因为系统中存在一个保单同时包含万能与普通险种。
		 + " and not exists(select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.risktype4='4' and lcpol.Contno = b.ContNo ) "
	     + "group by c.otherNo,b.AppntName,a.GetNoticeNo,a.AgentCode,b.ContNo,b.Dif,b.PayMode,c.SumDuePayMoney,c.Operator ";
	  
   } 
   mLJASql = strSql; 
   turnPage.queryModal(strSql, NormPayCollGrid);
   if(NormPayCollGrid.mulLineCount == 0)
   {
   		alert("没有满足条件的信息！");	
   }
}   
    
function fmSubmit()
{   
    if(checkValue())
    {   
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
    fm.submit();    
    }
    	
}   
    
function checkValue()
{   
   if(!verifyInput())
     return false;
    
   return true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    afterQuery();
  }
  
}

function afterQuery() {
	
	//pmType=1 个案核销；否则为批次核销
	pmType = fm.all('NormType').value ;
	initNormPayCollGrid(); 
	
  var strSql = ""; 
  if (pmType == "1")
  {
	    strSql = "select  b.ContNo,b.AppntName,c.GetNoticeNo,c.SumActuPayMoney,(select  min(LastPayToDate) from LJAPayPerson where PayNo=c.PayNo)" 
       + ",b.Dif,(select max(ConfDate) from LJTempFee where TempFeeNo= c.GetNoticeNo and ConfFlag='1'),(select codename from ldcode where codetype='paymode' and code=b.PayMode)"
       + ",(select  min(CurPayToDate) from LJAPayperson where PayNo=c.PayNo),getUniteCode(b.AgentCode),c.Operator,c.PayNo"
       + " from LCCont  b,LJAPay c  "
       + " where b.ContNo='" + fm.all('GrpContNo').value + "' "
	     + " and b.AppFlag='1' and c.IncomeType='2' and c.IncomeNo=b.ContNo "
       + " and c.PayNo=(select max(payno) from LJAPay where IncomeNo=b.ContNo)"  
       + " and c.makedate='"+CurrentTime+ "'"    
	  ;
  }else
  {
	    strSql = "select b.ContNo,b.AppntName,c.GetNoticeNo,c.SumActuPayMoney ,(select  min(LastPayToDate) from LJAPayPerson where PayNo=c.PayNo),b.Dif,"
	     +"(select max(ConfDate) from LJTempFee where TempFeeNo= c.GetNoticeNo and ConfFlag='1'),(select codename from ldcode where codetype='paymode' and code=b.PayMode),"
	     +"(select  min(CurPayToDate) from LJAPayperson where PayNo=c.PayNo),getUniteCode(c.AgentCode),c.Operator,c.PayNo" 
       + " from LCCont  b,LJAPay c "
       + " where b.AppFlag='1' "
	     + " and c.IncomeType='2' and c.IncomeNo=b.ContNo" 
	     //+ " and (select count(*) from LJAPayperson where PayNo=c.PayNo and paycount>1 )>0"      
       + " and c.makedate='"+CurrentTime+ "'"     
       + " group by b.ContNo,b.AppntName,c.GetNoticeNo,c.SumActuPayMoney,b.PayMode,b.Dif,c.AgentCode,c.Operator,c.PayNo"
	    ;    

  }
  mLJASql = strSql;
  turnPage.queryModal(strSql, NormPayCollGrid);  
 
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("NormPayCollInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}   

//提交所有选中数据，后台事务处理
function verifyChooseRecord()
{
/*
    if (NormPayCollGrid.mulLineCount == 0)
	{
		alert("列表没有数据,请先查询！");
		return false;
	}
	*/
    if(checkValue())
    { 
       if (fm.all('GrpContNo').value =='')
       {
			   if (fm.all('EndDate').value =='')
			   {
				   alert("请输入查询条件");
			   }else
			   {
				    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
						var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
						showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
						fmSaveAll.all('SubmitPayDate').value=fm.all('EndDate').value;
						fmSaveAll.submit();	
			   }
      }
      else
	  {
			var i = 0;
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
			fmSubmitAll.all('SubmitContNo').value = fm.all('GrpContNo').value;		   
		  fmSubmitAll.all('GetNoticeNo').value = NormPayCollGrid.getRowColDataByName(0,"getNoticeNo");
			fmSubmitAll.submit();
		}
		
   }
   document.all.verifyButton.disabled=true;
}

//直接提交所有数据，后台事务处理
function submitCurDataAll()
{
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");      	
    fmSaveAll.all('SubmitContNo').value=fm.all('GrpContNo').value;
    fmSaveAll.submit();	
 	
}

//批次核销查询数据
function queryMultRecord()
{
   if(checkValue())
   { 
	   if (fm.all('EndDate').value == '')
	   {
		   alert("请选择日期");
	   }else
	   {
		   easyQueryClick("2");
		   fm.all('NormType').value = '2';
	   }
   }
}

//单张核销查询数据
function querySingleRecord()
{
   if(checkValue())
   { 
	   if (fm.all('GrpContNo').value == '')
	   {
		   alert("请输入保单号");
		   //return false;
	   }else
	   {
	   	   fm.all('NormType').value = '1'
           easyQueryClick("1");
	   }
   }
}

//打印清单
function printList()
{
	if (NormPayCollGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	
	
	//由于%无法直接通过URL传递，这里先替换成特殊字符"xxvvbbvv"，在打印页面GrpConNormPayListPrint.jsp在替换成%
	var arr = mLJASql.split("%%");
	var sql = arr[0];
	
	for(var i = 1; i < arr.length; i++)
	{
	  sql = sql + "xxvvbbvv" + arr[i];
	}
	mLJASql = sql;
    //if(checkValue())
    //{ 
        if (fm.all('GrpContNo').value =='')
       {
			   if (fm.all('EndDate').value =='')
			   {
				   alert("请输入查询条件");
			   }else
			   {
					fmPrintAll.all('PrintPayDate').value=fm.all('EndDate').value;
					window.open("../operfee/GrpConNormPayListPrint.jsp?PrintPayDate="+ fm.all('EndDate').value + "&PrintVerifyType=1" + "&LJASql="+mLJASql );
			   }
       }else
	   {
			//fmPrintAll.all('PrintGrpContNo').value=fm.all('GrpContNo').value;
			//fmPrintAll.all('PrintVerifyType').value="2"; //个案核销
			window.open("../operfee/GrpConNormPayListPrint.jsp?PrintGrpContNo="+ fm.all('GrpContNo').value + "&PrintVerifyType=2"+ "&LJASql="+mLJASql );
		}
		
    //}
		
}

//打印对帐单
function printPayComp()
{

	if (NormPayCollGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
  var selNo = NormPayCollGrid.getSelNo();
  if(selNo == null || selNo == 0)
  {
  	  alert("请选择一条记录");
  	  return false;
  }
  var cPayNo = NormPayCollGrid.getRowColData( selNo - 1, 12 ); 
  var cPayDate = NormPayCollGrid.getRowColData( selNo - 1, 7 );
  var cDueMoney = NormPayCollGrid.getRowColData( selNo - 1, 4 );//应交金额
  var cDif = NormPayCollGrid.getRowColData( selNo - 1, 6 );//余额

	window.open("../operfee/IndiPayCompPrint.jsp?PayNo="+ cPayNo+ "&PayDate=" + cPayDate+ "&DueMoney="+ cDueMoney+ "&Dif="+cDif);
}
	
