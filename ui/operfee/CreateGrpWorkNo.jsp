<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：CreateGrpWorkNo.jsp
//程序功能：
//创建日期：2008-11-13 
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
  //输出参数
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  String tGrpContNo = request.getParameter("GrpContNo");
  LCGrpContDB tLCGrpContDB = new LCGrpContDB();
  tLCGrpContDB.setGrpContNo(tGrpContNo);
  tLCGrpContDB.getInfo();
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
  tLCGrpContSchema = tLCGrpContDB.getSchema();
  VData tVData = new VData();
  tVData.add(tGI);
  tVData.add(tLCGrpContSchema);
  CreateGrpWorkNoBL tCreateGrpWorkNoBL = new CreateGrpWorkNoBL();
  if (!tCreateGrpWorkNoBL.submitData(tVData,""))
  {
    FlagStr = "Fail";
    Content = "保存失败，原因是:" + tCreateGrpWorkNoBL.mErrors.getFirstError();
    %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
    
    <%
  }
  else
  {
    FlagStr = "Succ";
    Content = tCreateGrpWorkNoBL.getWorkNo();
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterImportInsured("<%=Content%>");
</script>
</html>
<%
}
 %>
