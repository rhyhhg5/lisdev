<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
   String loadFlag = request.getParameter("loadFlag");
%>                            

<script language="JavaScript">

//返回按钮初始化
var str = "";
var loadFlag = "<%=loadFlag%>";
var dealWMD = " ";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  fm.loadFlag.value = loadFlag;
  dealWMD = " exists (select 1 from LCCont where grpContNo = a.grpContNo and polType = '1') "
  dealWMD = fm.loadFlag.value == "WMD" ? " and " + dealWMD : " and not " + dealWMD;
  
  try
  {                                   
	// 保单查询条件
	
   // fm.all('SerialNo').value = '';
    fm.all('GrpContNo').value = '';
    fm.all('PrintState').value  ='0'  
  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initGrpContGrid();
    initGrpJisPayGrid();	
  }
  catch(re)
  {
    alert("GroupPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpContGrid()
  {                               
    var iArray = new Array();
      
      try
      {
		  iArray[0]=new Array();
		  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		  iArray[0][1]="30px";            		//列宽
		  iArray[0][2]=10;            			//列最大值
		  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[1]=new Array();
		  iArray[1][0]="收据号";         		//列名
		  iArray[1][1]="80px";            		//列宽
		  iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  iArray[1][21]="payNo"; 

		  iArray[2]=new Array();
		  iArray[2][0]="投保人姓名";         		//列名
		  iArray[2][1]="150px";            		//列宽
		  iArray[2][2]=100;            			//列最大值
		  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[3]=new Array();
		  iArray[3][0]="保单号";         		//列名
		  iArray[3][1]="70px";            		//列宽
		  iArray[3][2]=200;            			//列最大值
		  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  iArray[4]=new Array();
		  iArray[4][0]="应收金额";         		//列名
		  iArray[4][1]="70px";            		//列宽
		  iArray[4][2]=200;            			//列最大值
		  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[5]=new Array();
		  iArray[5][0]="缴费人数";         		//列名
		  iArray[5][1]="70px";            		//列宽
		  iArray[5][2]=200;            			//列最大值
		  iArray[5][3]=0; 
		  iArray[5][21]="peoples2"; 

		  iArray[6]=new Array();
		  iArray[6][0]="应缴时间";         		//列名
		  iArray[6][1]="80px";            		//列宽
		  iArray[6][2]=200;            			//列最大值
		  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[7]=new Array();
		  iArray[7][0]="实收金额";         		//列名
		  iArray[7][1]="70px";            		//列宽
		  iArray[7][2]=200;            			//列最大值
		  iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许


		  iArray[8]=new Array();
		  iArray[8][0]="核销时间";         		//列名
		  iArray[8][1]="80px";            		//列宽
		  iArray[8][2]=200;            			//列最大值
		  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许


		  iArray[9]=new Array();
		  iArray[9][0]="核销人";         		//列名
		  iArray[9][1]="70px";            		//列宽
		  iArray[9][2]=200;            			//列最大值
		  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[10]=new Array();
		  iArray[10][0]="是否打印";         		//列名
		  iArray[10][1]="70px";            		//列宽
		  iArray[10][2]=200;            			//列最大值
		  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		  
		  GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" ); 
		  //这些属性必须在loadMulLine前
		  GrpContGrid.mulLineCount = 0;   
		  GrpContGrid.displayTitle = 1;
		  GrpContGrid.locked = 1;
		  GrpContGrid.canSel = 0;
		  GrpContGrid.canChk = 1;
		  GrpContGrid.hiddenPlus = 1;
		  GrpContGrid.hiddenSubtraction = 1;
		  GrpContGrid.loadMulLine(iArray);  
		  //GrpContGrid.selBoxEventFuncName ="afterJisPayQuery";      
      }
      catch(ex)
      {
        alert(ex);
      }
}
// 催收记录的初始化
function initGrpJisPayGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="核销批次号";         		//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="核销时间";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="操作人";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[4]=new Array();
      iArray[4][0]="当前状态";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许

	    GrpJisPayGrid = new MulLineEnter( "fm" , "GrpJisPayGrid" ); 
      //这些属性必须在loadMulLine前
      GrpJisPayGrid.mulLineCount =0;   
      GrpJisPayGrid.displayTitle = 1;
      GrpJisPayGrid.hiddenPlus = 1;
      GrpJisPayGrid.hiddenSubtraction = 1;
      GrpJisPayGrid.locked = 1;
      GrpJisPayGrid.canChk = 1;
      GrpJisPayGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        alert("GroupPolQueryInit.jsp-->initGrpJisPayGrid函数中发生异常:初始化界面错误!");
      }
}

</script>