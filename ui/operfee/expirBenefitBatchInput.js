//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var expression ="" ;

function PersonSingle()
{
    if(beforeSubmit())
    {  
    	if (!checkValidateDate()) return false;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      	  	
      fm.submit();
    }	
}

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,batchNo)
{
	try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    var regS = new RegExp("QQQ","g");
    content=content.replace(regS,"<br>");
    fm.all("ErrorsInfo").innerHTML = content;
	content = "个险给付抽档操作完成，发生的错误信息见页面底部";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
   resetForm(batchNo);
}



//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function CheckGrpPolNo()
{

   
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function commonPayMulti()
{
	//校验保单的收付费方式若为医疗帐号，则不能做续期抽挡
	  var tRow = IndiContGrid.getSelNo() - 1;	        
		var tContNo=IndiContGrid.getRowColData(tRow,1);  
		var strSQL="select paymode from lccont where contno='"+tContNo+"' with ur"; 
		var rs = easyExecSql(strSQL);
		if(rs!=null&&rs[0][0]=="8"){
			alert("满期保险金付费方式不能为“医保个人账户”，须变更为其他付费方式");
			return false;
		}
  if(!checkValidateDate()) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
	fm.payMode.value="Q";
  fm.submit();	
  document.all.payMulti.disabled=true;
}

function cashPayMulti()
{
	if(!checkValidateDate()) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
	fm.payMode.value="1";
  fm.submit();	
}


function CheckDate()
{
  if(!isDate(fm.all('StartDate').value)||!isDate(fm.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fm.all('StartDate').value,fm.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

// 查询按钮
/*********************************************************************
 *  查询合同号
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	
	//得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	
	managecom = fm.ManageCom.value ;
	CurrentTime=fm.all("StartDate").value;
	SubTime=fm.all("EndDate").value;
	
	//应收时间起期校验
	if(CurrentTime == "" || CurrentTime == null)
	{
		alert("满期日起期不能为空！");
		return false;
	}
	//应收时间止期校验
	if(SubTime == "" || SubTime == null)
	{
		alert("满期日止期不能为空！");
		return false;
	}
	var strSQL11 = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
	 strSQL11 = " and a.agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
	}	
	var tTodayDate=fm.Today.value;
	//应收时间起止期三个月校验
	var t1 = new Date(tTodayDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(SubTime.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>60 )
    {
		alert("抽档日期止期不能大于当前日期后60天!");
//		return false;
    }
    
	if(fm.all("queryType").value =="" || fm.all("queryType").value=="1")
	{
		alert("请录入保单类型.");
		return false ;	
	}
	
	fm.all("TempQueryType").value = fm.all("queryType").value;
	
	//SQL分为两个一个是满期给付，另一是年金给付,银保少儿不在这里抽档。需要客户做不续保结论后再抽档。--这句话已过时
	if(fm.all("queryType").value=="3")
	{
      var strSQL = "select distinct a.contno,a.appntname,a.cvalidate,a.enddate,'',a.managecom,agentgroup,db2inst1.getUniteCode(agentcode) "
                 + "from lcpol a,lcget b , lmdutygetalive c "
                 + " where a.conttype ='1' and a.appflag = '1' and a.stateflag in ('1','3') "
                 + " and a.polno = b.polno and b.getdutycode = c.getdutycode "
                 + " and c.getdutykind = '0' "
                 + " and a.enddate >='"+CurrentTime+"' and a.enddate <='"+SubTime+"'"
		// modify by fuxin 2009-3-16 16:55 少儿险只能给付一次,所以去掉续保时生成的给付数据。
                 + " and (not exists (select 1 from ljsgetdraw where ljsgetdraw.polno = a.polno and feefinatype = 'TF') or exists (select  1 from ljsgetdraw  where ljsgetdraw.polno = a.polno  and feefinatype = 'TF' having sum(getmoney)=0) ) " 
                 + " and exists (select 1 from ljspayb ljs,lpedorespecialdata lpe where a.contno = ljs.otherno "
                 + " and ljs.othernotype='2' and ljs.getnoticeno= lpe.edoracceptno and lpe.edortype = 'XB' "
                 + " and detailtype ='DEALTYPE' and ljs.dealstate='3' and lpe.edorvalue='2') " 
                 + " and a.riskcode  in ('320106','120706') "
                 + " and b.dutycode in (select dutycode from lmriskduty where riskcode in ('320106','120706')) "
                 + " and b.managecom like '" + managecom + "%' " 
				 + getWherePart( 'a.contNo ','ContNo') 
				 + getWherePart( 'a.AppntNo ','AppntNo') 
//				 + getWherePart( 'a.AgentCode ','AgentCode') 
				 + strSQL11
				 + getWherePart( 'a.AgentGroup ','AgentGroup')
				 + getWherePart( 'a.RiskCode ','RiskCode')
				 + getWherePart( 'a.SaleChnl ','SaleChnl')
				 + " with ur ";
	}
	else if(fm.all("queryType").value=="4")
	{
       var strSQL = " select distinct a.contno,a.appntname,a.cvalidate,a.enddate,'',a.managecom,agentgroup,db2inst1.getUniteCode(agentcode) "
					 + " from lcpol a,lcget b , lmdutygetalive c "
					 + " where a.conttype ='1' and a.appflag ='1' "
					 + " and a.stateflag in ('1','3')  "
					 + " and a.polno = b.polno "
//					 5年期常无忧B上线了~2012-7-19
// 					 + " and a.insuyear!=5 "
					 + " and b.getdutycode = c.getdutycode "
					 + " and c.discntflag !='9' " //没主附险
					 + " and c.getdutykind ='0' " //满期金
					 + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno) "

					 + " and riskcode in ('330501','530301') "
	                 + " and b.dutycode in (select dutycode from lmriskduty where riskcode in ('330501', '530301')) "

					 + " and b.gettodate >='"+CurrentTime+"' and b.gettodate <='"+SubTime+"' "
					 + " and b.managecom like '"+managecom+"%' " 

					 + getWherePart( 'a.contNo ','ContNo') 
					 + getWherePart( 'a.AppntNo ','AppntNo') 
//					 + getWherePart( 'a.AgentCode ','AgentCode') 
					 + strSQL11
					 + getWherePart( 'a.AgentGroup ','AgentGroup') 
					 + getWherePart( 'a.RiskCode ','RiskCode')
					 + getWherePart( 'a.SaleChnl ','SaleChnl')
					 + " with ur ";

	}
	else
	{ // modify by fuxin 2008-5-20 12:07:12 现在业务没有那么多情况只有一种给付方式，先把其它条件去掉，否则太影响效率。--这句话已过时
	
	  //过滤险种
	  var sqlrisk = ""; 
	  sqlrisk = " and a.riskcode not in ('330501','530301') "  //过滤常无忧B
			 + " and a.riskcode not in ('320106','120706') "  //过滤少儿险
//			  +" and not exists (select 1 from lcpol where riskcode = '340101' and stateFlag = '3' and contno=a.contNo) " //过滤 340101  金生无忧个人护理保险 且保单是终止状态--StateFlag='3'
			 + " and not exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4='4') "//20090915 zhanggm 过滤存在万能险保单
			 + " and not exists (select 1 from ldcode1 where code = a.riskcode and codetype = 'mainsubriskrela') "//过滤附加险
	  var hlSql = " and (a.riskcode not in (select riskcode from lmriskapp where risktype4='2') or c.getdutykind<>'0')" ;//add by lzy 20160902过滤分红险满期金，通过保全处理 
	                 //满期金，没主附险
	  var subUliRisk = ""; //批量抽档时排除含有附加万能险的保单
	  if(fm.all("ContNo").value=="" ){
		  subUliRisk=" and not exists (select 1 from lcpol where contno=a.contno  and riskcode in " //and insuredno=a.insuredno
		  			+" ( select riskcode from lmriskapp where  risktype4='4' and subriskflag='S')) ";
	  }
	    var strSQL = " select distinct a.contno,a.appntname,a.cvalidate,a.enddate,'',a.managecom,agentgroup,db2inst1.getUniteCode(agentcode) "
					 + " from lcpol a,lcget b , lmdutygetalive c "
					 + " where a.conttype ='1' and a.appflag ='1' "
//					 + " and a.stateflag in ('1','3')  "
					 + " and a.polno = b.polno "
					 + " and b.getdutycode = c.getdutycode "
					 + " and c.discntflag !='9' " //没主附险
					 + " and c.getdutykind ='0' " //满期金
					 + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno) "
					 + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))"
					 + sqlrisk 
					 + hlSql
					 + subUliRisk
					 + " and b.gettodate >='"+CurrentTime+"' and b.gettodate <='"+SubTime+"' "
					 + " and b.managecom like '"+managecom+"%' " 

					 + getWherePart( 'a.contNo ','ContNo') 
					 + getWherePart( 'a.AppntNo ','AppntNo') 
//					 + getWherePart( 'a.AgentCode ','AgentCode') 
					 +strSQL11
					 + getWherePart( 'a.AgentGroup ','AgentGroup') 
					 + getWherePart( 'a.RiskCode ','RiskCode')
					 + getWherePart( 'a.SaleChnl ','SaleChnl')
					 //满期金，附加险
					 + " union "
					 + " select distinct a.contno,a.appntname,a.cvalidate,a.enddate,'',a.managecom,agentgroup,db2inst1.getUniteCode(agentcode) "
					 + " from lcpol a,lcget b , lmdutygetalive c "
					 + " where a.conttype ='1' and a.appflag ='1' "
					 + " and a.polno = b.polno "
					 + " and b.getdutycode = c.getdutycode "
					 + " and c.discntflag !='9' " //没主附险
					 + " and c.getdutykind ='0' " //满期金
					 + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno) "
					 + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))"
					 + " and a.riskcode not in ('330501','530301') "  //过滤常无忧B
					 + " and a.riskcode not in ('320106','120706') "  //过滤少儿险
					 + " and not exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype4='4') "// 过滤万能险保单
					 + " and exists (select 1 from ldcode1 where code = a.riskcode and codetype = 'mainsubriskrela') "//附加险的满期金 
					 + hlSql
					 + subUliRisk
					 + " and b.gettodate >='"+CurrentTime+"' and b.gettodate <='"+SubTime+"' "
					 + " and b.managecom like '"+managecom+"%' " 

					 + getWherePart( 'a.contNo ','ContNo') 
					 + getWherePart( 'a.AppntNo ','AppntNo') 
//					 + getWherePart( 'a.AgentCode ','AgentCode') 
					  + strSQL11
					 + getWherePart( 'a.AgentGroup ','AgentGroup') 
					 + getWherePart( 'a.RiskCode ','RiskCode')
					 + getWherePart( 'a.SaleChnl ','SaleChnl')
					 
					 //090622拆分满期金和年金两种不同方式的查询
					 //年金，没主附险
					 + " union "
					 + " select distinct a.contno,a.appntname,a.cvalidate,a.enddate,'',a.managecom,agentgroup,db2inst1.getUniteCode(agentcode) "
					 + " from lcpol a,lcget b , lmdutygetalive c "
					 + " where a.conttype ='1' and a.appflag ='1' "
//					 + " and a.stateflag in ('1','3') "
					 + " and a.polno = b.polno "
					 + " and b.getdutycode = c.getdutycode "
					 + " and c.discntflag !='9' " //没主附险
					 + " and c.getdutykind = '12' " //年金
					 + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))"
					 + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno) "
					 
					 //判断抽档日期是否在周岁生日对应的保单周年日
					 //add by xp 修改条件字段为日期类型 2010-3-11
//					 + " and date(case when '2009' || substr(varchar(a.cvalidate), 5, 6) > '2009' || substr(varchar(a.insuredbirthday), 5, 6) "
//					 + " then substr(varchar(a.insuredbirthday + 60 year),1,4) || substr(varchar(a.cvalidate), 5, 6) "
// 					 + " else substr(varchar(a.insuredbirthday + 61 year),1,4) || substr(varchar(a.cvalidate), 5, 6) end ) "
// 					 + "between '"+CurrentTime+"' and '"+SubTime+"' " 
 					 
 					 //判断抽档日期是否在周岁生日对应的保单周年日
 		//			 + "and (case when a.insuredbirthday+60 years <= a.cvalidate + (year(a.insuredbirthday+60 years) - year(a.cvalidate)) years  "
 		//			 + "then a.cvalidate + (year(a.insuredbirthday+60 years) - year(a.cvalidate)) years  "
 		//			 + "else a.cvalidate + (year(a.insuredbirthday+61 years) - year(a.cvalidate)) years end) "
 					 
 					 + sqlrisk
 					 + subUliRisk
 					 + " and b.gettodate between '"+CurrentTime+"' and '"+SubTime+"' " 
					 + " and b.managecom like '"+managecom+"%' "  
					 
 					 //控制一年抽档一次：第一次抽档ljsgetdraw中没有数据所以默认一个初始值“2008-1-1”
 					 //抽过一次的保单在ljsgetdraw.getdate=当时抽档对应的保单周年日
 					 + " and '"+SubTime+"' >= (case (select count(1) From ljsgetdraw b where a.contno = b.contno and b.polno=a.polno) "
					 + " when 0 then date('2008-1-1') else (select max(getDate) From ljsgetdraw b where a.contno = b.contno and b.polno=a.polno) end ) "
					 
					 + getWherePart( 'a.contNo ','ContNo') 
					 + getWherePart( 'a.AppntNo ','AppntNo') 
//					 + getWherePart( 'a.AgentCode ','AgentCode') 
					 + strSQL11
					 + getWherePart( 'a.AgentGroup ','AgentGroup')
					 + getWherePart( 'a.RiskCode ','RiskCode')
					 + getWherePart( 'a.SaleChnl ','SaleChnl')
					 
					  //今生无忧3年金金给付
					 + " union "
                     + " select distinct a.contno,a.appntname,a.cvalidate,a.enddate,'',a.managecom,agentgroup,db2inst1.getUniteCode(agentcode) "
                     + " from lcpol a,lcget b , lmdutygetalive c "
                     + " where a.conttype ='1' and a.appflag ='1' "
//                     + " and a.stateflag in ('1','3') "
                     + " and a.polno = b.polno "
                     + " and b.getdutycode = c.getdutycode "
                     + " and c.discntflag !='9' " //此处不知道这么写是否正确，如果有不正确的地方请指出
                     + " and c.getdutykind = '36' " //3年金
					 + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))"
                     + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno) "   
                     + sqlrisk
                     + subUliRisk
                     + " and b.gettodate between '"+CurrentTime+"' and '"+SubTime+"' " 
                     + " and ((b.getdutykind ='0' and  b.gettodate<=b.getenddate" 
                     + "  and not exists (select 1 from ljsgetdraw where contno=b.contno and getdutycode=b.getdutycode and insuredno=b.insuredno)"
                     +"		) or( b.getdutykind !='0' and  "
                     + " ((b.gettodate<b.getenddate) or ((select max(Curgettodate) from ljsgetdraw "
                     + " where contno=b.contno and getdutycode=b.getdutycode and insuredno=b.insuredno)<=b.getenddate)))) "
                     + " and b.managecom like '"+managecom+"%' "  
                     + " and '"+SubTime+"' >= (case (select count(1) From ljsgetdraw b where a.contno = b.contno and b.polno=a.polno) "
                     + " when 0 then date('2012-5-15') else (select max(getDate) From ljsgetdraw b where a.contno = b.contno and b.polno=a.polno) end ) " //调整首次抽档的日期，如果有不正确的地方请指出  
                     + getWherePart( 'a.contNo ','ContNo') 
                     + getWherePart( 'a.AppntNo ','AppntNo') 
//                   + getWherePart( 'a.AgentCode ','AgentCode') 
                     + strSQL11
                     + getWherePart( 'a.AgentGroup ','AgentGroup')
                     + getWherePart( 'a.RiskCode ','RiskCode')
                     + getWherePart( 'a.SaleChnl ','SaleChnl')
					 
					 //处理全无忧的老年关爱金
					 //有主附险
					 + " union "
					 + " select distinct a.contno,a.appntname,a.cvalidate,a.enddate,'',a.managecom,agentgroup,db2inst1.getUniteCode(agentcode) "
					 + " from lcpol a,lcget b , lmdutygetalive c "
					 + " where a.conttype ='1' and a.appflag ='1' "
//					 + " and a.stateflag in ('1','3') "
					 + " and a.polno = b.polno "
					 + " and ((c.discntflag ='9') or (b.getdutykind='24' and c.discntflag !='9'))" //有主附险
					 + " and b.getdutycode = c.getdutycode "
					 + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))"
					 + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno) "
					 
					 //判断抽档日期是否在周岁生日对应的保单周年日
					  //add by xp 修改条件字段为日期类型 2010-3-11
//					 + " and date(case when '2009' || substr(varchar(a.cvalidate), 5, 6) > '2009' || substr(varchar(a.insuredbirthday), 5, 6) "
//					 + " then substr(varchar(a.insuredbirthday + 60 year),1,4) || substr(varchar(a.cvalidate), 5, 6) "
// 					 + " else substr(varchar(a.insuredbirthday + 61 year),1,4) || substr(varchar(a.cvalidate), 5, 6) end ) "
// 					 + " between '"+CurrentTime+"' and '"+SubTime+"'" 
 					 
 					 //判断抽档日期是否在周岁生日对应的保单周年日
// 					 + "and (case when a.insuredbirthday+60 years <= a.cvalidate + (year(a.insuredbirthday+60 years) - year(a.cvalidate)) years  "
// 					 + "then a.cvalidate + (year(a.insuredbirthday+60 years) - year(a.cvalidate)) years  "
// 					 + "else a.cvalidate + (year(a.insuredbirthday+61 years) - year(a.cvalidate)) years end) "
					 + hlSql
 					 + sqlrisk
 					 + subUliRisk
 					 + " and b.gettodate between '"+CurrentTime+"' and '"+SubTime+"' " 
					 + " and b.managecom like '"+managecom+"%' "  
 					 
 					 //控制一年抽档一次：第一次抽档ljsgetdraw中没有数据所以默认一个初始值“2008-1-1”
 					 //抽过一次的保单在ljsgetdraw.getdate=当时抽档对应的保单周年日
 					 + " and '"+SubTime+"' >= (case (select count(1) From ljsgetdraw b where a.contno = b.contno and b.polno=a.polno ) "
					 + " when 0 then date('2000-1-1') else (select max(getDate) From ljsgetdraw b where a.contno = b.contno and  b.polno=a.polno) end) "
					 
					 + getWherePart( 'a.contNo ','ContNo') 
					 + getWherePart( 'a.AppntNo ','AppntNo') 
//					 + getWherePart( 'a.AgentCode ','AgentCode') 
					 + strSQL11
					 + getWherePart( 'a.AgentGroup ','AgentGroup')
					 + getWherePart( 'a.RiskCode ','RiskCode')
					 + getWherePart( 'a.SaleChnl ','SaleChnl')
					 + " with ur ";
				
    }

	fm.QuerySql.value = strSQL; 
	turnPage2.queryModal(strSQL, IndiContGrid);
	initPolGrid();  
	contCurrentTime=CurrentTime; 
	contSubTime=SubTime; 
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	if(IndiContGrid.mulLineCount == 0)
	{
	    alert("没有符合条件的可催收保单信息");
	}
}

/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getpoldetail()
{
    var arrReturn = new Array();
	  var tSel = IndiContGrid.getSelNo();	
	  
	  if( tSel == 0 || tSel == null )
	  {
		   alert( "请先选择一条记录，再点击返回按钮。" );
	  }	
	  else
	  {
		   getPolInfo();
    }  	                                          
                         	               	
}
/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolInfo()
{
	//modify by fuxin 2008-5-5 11:21:17 少儿险险种和普通险种分开显示。
	var Condition ='';
	if(fm.queryType.value=='3')
	{
		Condition = " and a.riskcode  in ('320106','120706') " ;
	}
	else
	{
	    Condition = " and a.riskcode not in ('320106','120706') " ;
	}
	
	  var tRow = IndiContGrid.getSelNo() - 1;	        
		var tContNo=IndiContGrid.getRowColData(tRow,1);  
		
		var strSQL = "select a.RiskSeqNo,a.InsuredName, (select riskname  from lmrisk where riskcode=a.riskcode) ,a.RiskCode, (select codeName from LDCode where codeType = 'payintv' and code = char(a.PayIntv)), "
			+ "	a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate ,a.enddate "  
			+ " from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
			+ " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
			+ "  and a.contno='" + tContNo + "' "
			+ "  and a.appflag='1' "
			+ Condition
			+ " group by a.polNo,a.RiskSeqNo,a.InsuredName,a.RiskCode, 	a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate ,a.enddate,payintv  "
			+ " order by a.polNo "
			+ " with ur "			
			;
    	turnPage3.queryModal(strSQL, PolGrid); 
}


/*********************************************************************
 *  重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为resetForm()
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm(batchNo)
{
  try
  {    
//  afterContQuery();
	  afterJisPayQuery(batchNo);
	  
  }
  catch(re)
  {
  	alert("在LJSPayPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 



/*********************************************************************
 *  校验日期范围
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkValidateDate()
{
	//校验录入的起始日期和终止日期必须在今天之前，否则不予提交
	var startDate=fm.StartDate.value;
	var endDate=fm.EndDate.value;
	if(startDate==''||endDate=='')
	{
		alert("请录入查询日期范围！");
		return false;
	}
	if(compareDate(startDate,endDate)==1)
	{
		alert("起始日不能晚于截止日!");
		return false;
	}
	return true;
}

//催收完成后查询保单
function afterContQuery()
{
		// 初始化表格
	initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}
	 var strSQL = "select a.ContNo,a.PrtNo,a.AppntName,a.CValiDate,a.Prem,(select AccGetMoney from LCAppAcc where CustomerNo=a.appntno),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),db2inst1.getUniteCode(a.AgentCode),b.GetNoticeNo "
	 + "from LCCont  a, ljspay b, ljspayperson c "
	 + "where a.AppFlag='1' and b.OtherNo=a.ContNo and  b.othernotype='2' "
	 + "  and c.GetNoticeNo=b.GetNoticeNo and c.ContNo=b.otherno "
	 + "  and  b.MakeDate=current date "
	 + "  and exists"
	 + "      (select 'X' from LCPol "
	 + "      where contno=a.contno "
	 + "        and PaytoDate>='" + CurrentTime + "' "
	 + "        and PaytoDate<='"+SubTime+"' "
	 + "        and PolNo in (select PolNo from LJSPayPerson) "
	 + "      )"
	 + getWherePart( 'a.ManageCom', 'ManageCom','like')
	 + " group by a.ContNo,a.PrtNo,a.AppntName,a.CValiDate,a.Prem,a.appntno,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode"
	 + " order by b.GetNoticeNo with ur "
	 ;
	turnPage2.queryModal(strSQL, IndiContGrid); 
	initPolGrid(); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	
	if(IndiContGrid.mulLineCount == 0)
	{
	    alert("没有符合条件的可催收保单信息");
	}
	
}


//催收完成后查询催收纪录
function afterJisPayQuery(batchNo)
{
	// 初始化表格
	initLjsGetGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}		  
  var strSQL ="	select distinct a.SerialNo,a.otherno,'',min(d.contno),(select name from lcinsured where contno = d.contno and insuredno = d.insuredno) "
							+ " ,max(a.sumgetmoney),min(d.lastgettodate),getUniteCode(min(d.agentcode)),min(d.insuredno),min(a.paymode), "
							+ " nvl((select count(distinct c.getnoticeno) from ljsgetdraw b ,ljsget c "
							+ " where b.getnoticeno = c.getnoticeno and c.dealstate ='1' and b.contno = d.contno and b.insuredno =d.insuredno ),0) "
							+ ",'待给付确认',d.RiskCode,'','',d.GetNoticeNo from  ljsget a ,ljsgetdraw d "
							+ " where a.getnoticeno = d.getnoticeno and "
							+ " a.makedate=current date and a.othernotype='20' and  a.operator ='"+operator+"'"
							+ " and a.SerialNo ='"+batchNo+"' "
							+ " group by a.SerialNo,a.otherno,a.getnoticeno,d.contno,d.insuredno,d.GetNoticeNo,d.RiskCode "
							;
	 
	turnPage1.queryModal(strSQL, LjsGetGrid); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
}


/*********************************************************************
 *  投保信息列表中选择一条，查看投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function returnParent()
{
	//从客户投保信息列表中选择一条，查看投保信息

		 var tSel1 = IndiContGrid.getSelNo();
		 if( tSel1 == 0 || tSel1 == null )
	   {		
		   alert( "请先选择一条记录，或输入保单号，再点击保单明细按钮。" );
	   }else
	   {
	   	  var cContNo = IndiContGrid.getRowColData( tSel1 - 1, 1);		  
				window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType=1");	
					
	   }
	}
function getLjsGetDrawdetail()
{
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length < 1)
	{
		return false;	
	}
	var taskNo=LjsGetGrid.getRowColData(tChked[0],2);
	
	var SQL = "SELECT CONTNO,insuredno,'',RISKCODE,GETMONEY,(select enddate from lcpol where polno = LJSGETDRAW.polno),POLNO, " 
	        + "case DutyCode when '000000' then '初始忠诚奖' when '000001' then '外加忠诚奖' end "
	        + "FROM LJSGETDRAW WHERE GETNOTICENO =(SELECT GETNOTICENO FROM LJSGET WHERE OTHERNOTYPE ='20' AND OTHERNO ='"+taskNo+"')";
	turnPage.queryModal(SQL, LjsGetDrawGrid); 
	for(var i = 0; i < LjsGetDrawGrid.mulLineCount; i++)
	{
		var sql = "select name from lcbnf where polno ='"+LjsGetDrawGrid.getRowColData(i,7)+"'";
		var rs = easyExecSql(sql);
		var bnf = "";
		if(rs!=null)
		{
			for(var j =0;j<rs.length;j++)
			{
				bnf+=rs[j]+'、';
			}
		}
		LjsGetDrawGrid.setRowColData(i,3,bnf);
	}
}

function printAllNotice()
{
    fm.all('queryType').value = fm.all('TempQueryType').value;
    
	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{	
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}


	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if (fm.all('TempQueryType').value == "3")
	{
		if(tChked.length == 0)
		{
			fm.action="./expirBenefitInsAllBatPrt.jsp";
			fm.submit();
		}
		else
		{
			fm.action="./expirBenefitInsForBatPrt.jsp";
			fm.submit();
		}
	}
	else
		{
			if(tChked.length == 0)
			{
				fm.action="./expirBenefitGetAllBatPrt.jsp";
				fm.submit();
			}
			else
			{
				fm.action="./expirBenefitGetForBatPrt.jsp";
				fm.submit();
			}
		}
}

function printOneNoticeNew()
{
	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印");
		return false;	
	}
	var taskNo=LjsGetGrid.getRowColData(tChked[0],2);
	var getnoticeno = easyExecSql(" select getnoticeno from ljsget where othernotype ='20' and otherno ='"+taskNo+"'");
	
	var tRiskCode = LjsGetGrid.getRowColDataByName(tChked[0],"RiskCode");
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	if(fm.TempQueryType.value=='2')
	{
		fm.action = "../uw/PDFPrintSave.jsp?Code=MX001&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+getnoticeno;	  
	}
	if(fm.TempQueryType.value=='3')
	{
	    fm.action = "../uw/PDFPrintSave.jsp?Code=bq001&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+getnoticeno;
	}
	if(fm.TempQueryType.value=='4')
	{
	    var sql = "select substr(managecom,1,4) from ljsget where getnoticeno = '" + getnoticeno + "' ";
	    var rs = easyExecSql(sql);
	    
	    if(rs[0][0]=="8644")//广东分公司与其他分公司类型不同
	    {
	      fm.action="../uw/PDFPrintSave.jsp?Code=MX004&StandbyFlag2="+getnoticeno;
	    }
	    else
	    {
	      fm.action="../uw/PDFPrintSave.jsp?Code=MX002&StandbyFlag2="+getnoticeno;
	    }
	}
	fm.submit();
}

function afterCodeSelect( cCodeName, Field )
{
  if( cCodeName == "queryType")
  {
    switch(fm.all('queryType').value)
    {
      case "1":
      fm.all('RiskCode').value = "";
      fm.all('RiskName').value = "";
      break;        
      case "2":
      fm.all('RiskCode').value = "";
      fm.all('RiskName').value = "";
      break;
      case "3":
      fm.all('RiskCode').value = "320106";
      showAllCodeName();
      break;
      case "4":
      fm.all('RiskCode').value = "330501";
      showAllCodeName();
      break;
    }
  }
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}