//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass();


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
  }
  resetForm();
}

//重置按钮对应操作,重新查询抽档的相关信息
function resetForm()
{
  try
  {
  	getPolInfo();
  }
  catch(re)
  {
  	alert("在GrpDueFeePlanInput.js-->resetForm函数中发生异常!");
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	
	initAuditGrid();
	getContInfo();
	
	
}

// 数据返回父窗口
function returnParent()
{
	
	if( fm.all("tGrpContNo").value == null || fm.all("tGrpContNo").value == '' )
		alert( "请先查询保单信息。" );
	else
	{
		try
		{
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ fm.all("tGrpContNo").value+"&ContType=1");
		}
		catch(ex)
		{
			alert( "查询出错!" );
		}
	}
}

//查询团单信息
function getContInfo()
{
	var strSQL = "select b.grpcontno,b.cvalidate,b.ManageCom,ShowManageName(b.ManageCom), " 
		+ " (select nvl(sum(prem),0) from lcgrppayplan where  prtno=b.prtno and int(plancode)<>1)," 
		+ " (select nvl(sum(prem),0) from lcgrppayactu where  prtno=b.prtno and state = '1')," 
		+ " (select nvl(sum(prem),0) from lcgrppayactu where  prtno=b.prtno and state in ('2','3') and getnoticeno=a.getnoticeno ),"
		+ " (case  when (select count(1) from ljspay where getnoticeno=a.getnoticeno and otherno=b.grpcontno and cansendbank='Y')>0 then '待审批' " 
		+ " when (select state from LPEdorEspecialData where EdorAcceptNo=a.getnoticeno and EdorNo=b.grpcontno and EdorType='00' and DetailType='AuditResult')='01' then '审批通过'" 
		+ " when (select state from LPEdorEspecialData where EdorAcceptNo=a.getnoticeno and EdorNo=b.grpcontno and EdorType='00' and DetailType='AuditResult')='02' then '审批未通过' end),"
		+ " (select EdorValue from LPEdorEspecialData where EdorAcceptNo=a.getnoticeno and EdorNo=b.grpcontno and EdorType='00' and DetailType='AuditDate')"
		+ " from lcgrppayactu a,lcgrpcont b "
        + " where a.prtno=b.prtno "
       	+ " and (exists (select 1 from ljspay where getnoticeno=a.getnoticeno and otherno=b.grpcontno and cansendbank='Y') or" +
        		" exists ((select 1 from LPEdorEspecialData where EdorAcceptNo=a.getnoticeno and EdorNo=b.grpcontno and EdorType='00')))"
        + getWherePart( 'b.GrpContNo','GrpContNo' )
		+ getWherePart( 'b.PrtNo', 'PrtNo')
		+ getWherePart( 'b.ManageCom', 'ManageCom','like')
        + " group by b.prtno,b.grpcontno,b.ManageCom,a.getnoticeno,b.cvalidate order by b.grpcontno";
    turnPage.queryModal(strSQL, AuditGrid);
//    fm.all("tPrtNo").value=AuditGrid.getRowColData( 0, 4 );
//    fm.all("tGrpContNo").value=AuditGrid.getRowColData( 0, 1 );
   
}


