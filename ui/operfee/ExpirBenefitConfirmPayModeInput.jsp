<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ExpirBenefitConfirmPayModeInput.jsp
//程序功能：常无忧B给付方式修改页面
//创建日期：2010-07-07 16:06
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	
  <SCRIPT src="ExpirBenefitConfirmPayModeInput.js"></SCRIPT>
  <%@include file="./ExpirBenefitConfirmPayModeInit.jsp"%>
</head>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate();   
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
  String AheadDays="30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  String SubDate=tD.getString(AfterDate);  
  String tSubYear=StrTool.getVisaYear(SubDate);
  String tSubMonth=StrTool.getVisaMonth(SubDate);
  String tSubDate=StrTool.getVisaDay(SubDate);               	               	
%>

<SCRIPT>
  var CurrentYear=<%=tCurrentYear%>;  
  var CurrentMonth=<%=tCurrentMonth%>;  
  var CurrentDate=<%=tCurrentDate%>;
  var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
  var SubYear=<%=tSubYear%>;  
  var SubMonth=<%=tSubMonth%>;  
  var SubDate=<%=tSubDate%>;
  var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
  var managecom = <%=tGI.ManageCom%>;
  var operator = '<%=tGI.Operator%>';
  var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
  var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
</SCRIPT> 

<body  onload="initForm();" >
<form action="ExpirBenefitConfirmSubmit.jsp" method=post name="fm" target="fraSubmit">
  <table>
    <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        给付保单查询：
      </td>
     </tr>
   </table>
   <Div  id= "divAlivePayMulti" style= "display: ''">
   <table  class= common align=center>
      <tr  class= common>
        <td class= title>满期日起期</td>
      	<td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      	<td class= title>满期日止期</td>
      	<td class= input><Input class="coolDatePicker" dateFormat="short" name=EndDate ></td>
        <td class= title>管理机构</td>
      	<td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><Input class=codename  name=ManageComName readonly></td>
      </tr>
      <tr  class= common>
      	<td  class= title>给付任务号</td>
        <td  class= input><input class= common name="EdorAcceptNo" ></td>
        <td class= title>保单号</td>
      	<td class= input><Input class= common name=ContNo></td>	
      </tr>
      <tr  class= common>
        <td><Input class = cssbutton VALUE="查 询" TYPE=button onclick="queryClick();"> </td>
      </tr>
    </table>
    </Div>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    			 给付任务清单：
    		</td>
    	</tr>
    </table>
  <div  id= "divJisPay" style= "display: ''">
   	<table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1><span id="spanLjsGetGrid"></span></td>
  	  </tr>
  	  <tr>
  		<td>  					
  		  <center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
     			<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
    	  </center>
    	</td>
      </tr>
    </table>
  </div> 
  
  <br>
  <div align=left id="divLjsGetDraw" style="display: '' ">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divLjsGetDrawGrid);">
    		</td>
    		<td class= titleImg>
    			 给付明细：
    		</td>
    	</tr>
    </table>
    <div align=left id="divLjsGetDrawGrid" style="display: '' ">
      <table>
        <tr  class= common>
          <td text-align: left colSpan=1>
  		    <span id="spanLjsGetDrawGrid" ></span> 
  	      </td>
  	    </tr>
      </table>
    </div>
  </div>
  <br>
  
  <div align=left id="divDrawer" style="display: 'none' ">
  <table  class= common align=center>    		
  	<tr>
	  <td class= title>&nbsp给付对象&nbsp
	    <input class="codeNo" name="DrawerCode" CodeData="0|^0|投保人^1|被保人 " 
	           ondblclick="return showCodeListEx('DrawerCode',[this,Drawer],[0,1],null,null,null,1);" 
	           onkeyup="return showCodeListKeyEx('DrawerCode',[this,Drawer],[0,1],null,null,null,1);"><Input class="codeName" name="Drawer" readonly>
	  </td>
  	</tr>
  </table>  
  </div>
  
  <div align=left id="divCash" style="display: 'none' ">
    <hr>
    <table class= common align=center>
      <tr>
        <td colspan = 6>
          <font color="#FF0000">您目前的给付方式为“现金”</font>
        </td>
      </tr>
    </table>
    <hr>
  </div>
  
  <div align=left id="divOther" style="display: 'none' ">
    <hr>
    <table class= common align=center>
      <tr>
        <td colspan = 6>
          <font color="#FF0000">您目前的给付方式为“其它”</font>
        </td>
      </tr>
      <tr  class= common>
  	    <td class= title>开户银行</td>
    	<td class= input><Input class=readonly name=BankCodeShow1 style="width: 60" readonly ><input class=readonly name=BankNameShow1 readonly style="width: 140" readonly></td>
    	<td class= title>账户名</td>
    	<td class= input><Input class=readonly name=AccNameShow1 style="width: 180" readonly></td>
    	<td class= title>账号</td>
    	<td class= input><Input class=readonly name=BankAccNoShow1 style="width: 180" readonly></td>
      </tr>
    </table>
    <hr>
  </div>
    
  <div align=left id="divBank" style="display: 'none' ">
    <hr>
    <table class= common align=center>
      <tr>
        <td colspan = 6>
          <font color="#FF0000">您目前的给付方式为“银行转账”</font>
        </td>
      </tr>
  	  <tr  class= common>
  	    <td class= title>开户银行</td>
    	<td class= input><Input class=readonly name=BankCodeShow style="width: 60" readonly ><input class=readonly name=BankNameShow readonly style="width: 140" readonly></td>
    	<td class= title>账户名</td>
    	<td class= input><Input class=readonly name=AccNameShow style="width: 180" readonly></td>
    	<td class= title>账号</td>
    	<td class= input><Input class=readonly name=BankAccNoShow style="width: 180" readonly></td>
      </tr>   
    </table>
    <hr>
  </div>
  
  <div align=left id="divPayMode" style="display: 'none' ">
  
    <table>
    	<tr>
    		<td class= titleImg>
    			 给付方式变更：
    		</td>
    	</tr>
    </table>
    
    <table class= common align=center>
      <tr class= common>
        <td class= title>给付方式</td>
	    <td class= input><input class="codeNo" name="PayMode" CodeData="0|^1|现金^4|银行转账^9|其它" ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);"><Input class="codeName" name="PayModeName" readonly></td>
        <td class= title>开户银行</td>
      	<td class= input><Input class= "codeno"  name="BankCode" ondblclick="return showCodeList('Bank',[this,BankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankName],[0,1],null,null,null,1);"><Input class=codename  name=BankName readonly ></td>
      	<td class= title>账户名</td>
      	<td class= input><Input class= common name="AccName" readonly></td>	
      </tr>
      <tr>
      	<td  class= title>银行帐号</td>
        <td  class= input><input class= common name="BankAccNo" ></td>
        <td  class= title>再次录入银行帐号</td>
        <td  class= input><input class= common name="BankAccNoConf" ></td>
        <td  class= input colspan = 2></td>
      </tr>  
      <tr>
		<td class = common>
			<input class = cssbutton value="变更确认" type=button onclick="changeConfirm();">
		</td>
	</tr> 
    </table>
    <hr>
  </div>	
  
    <input type=hidden id="GetNoticeNo" name="GetNoticeNo">
    <input type=hidden id="PayModeShow" name="PayModeShow">
    <input type=hidden id="SerialNo" name="SerialNo">
    <input type=hidden id="TempContNo" name="TempContNo">
	<input type=hidden id="InsuredNo" name="InsuredNo">
	<input type=hidden id="AppntNo" name="AppntNo">
	<input type=hidden id="GetState" name="GetState">
	<input type=hidden id="RateFlagValue" name="RateFlagValue">
	<input type=hidden id="RiskCode" name="RiskCode">
	<input type=hidden id="Today" name="Today">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
