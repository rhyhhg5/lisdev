<%
//程序名称：GrpDueFeeBack.jsp
//程序功能：团单实收保费转出
//创建日期：2006-09-18
//创建人  ：QuLiqiang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head >
  <SCRIPT>
  </SCRIPT>  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpDueFeeBack.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="GrpDueFeeBackInit.jsp"%>
</head>
<body onload="initForm();">
<form name=fm action=./GrpDueFeeBackOper.jsp target=fraSubmit method=post>
    <table  class= common align=center>
	     <tr class=common>
          <TD  class= title>
          客户号
          </TD>
          <TD  class= input>
            <Input class="common"  name=cusNo  >
          </TD>
          <TD  class= title>
          保单号
          </TD>
          <TD  class= input>
            <Input class="common"  name=insNo >
          </TD>  		   
		  <TD  class= input>
             <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="easyQueryClick();"> 
          </TD>	  
		</tr>
		<tr>
			<td class = title>
				帐户余额
			</td>
			<td class = input>
			<Input class="readonly" name=AccBala readonly >
			</td>
  </table>          
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInsList);">
    		</td>
    		<td class= titleImg>
    			 保单列表:
    		</td>
    	</tr>
    </table>
  	<Div  id= "divInsList" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpInsListGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
   <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
  </center>  	
  </div>
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPayment);">
    		</td>
    		<td class= titleImg>
    			保费记录：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divPayment" style= "display:'' ">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanGrpPaymentGrid" ></span> 
  			  	</td>
  			</tr>
    	</table>     
	<center>      				
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</center>  
	</Div>	
	<br>
     <INPUT VALUE="保费转出" class = cssbutton TYPE=button onclick="cancelRecord();"> 
     <INPUT TYPE=hidden name=SubmitPayNo>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
