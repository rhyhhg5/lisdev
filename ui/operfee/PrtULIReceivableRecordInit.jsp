<%
//程序名称：PrtReceivableRecordInit.jsp
//程序功能：
//创建日期：2010-03-02 18:00
//创建人  ：Pangxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
	//System.out.println(curDate);
%> 

<script language="JavaScript">

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";

//表单初始化
function initForm()
{
	initInputBox();
  	initRableRecGrid();
}
function initRableRecGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="管理机构";       			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[1][1]="120px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="管理机构代码";   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[2][1]="100px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="营销部门";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[3][1]="120px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="保单号";	   				//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[4][1]="110px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="险种代码";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[5][1]="60px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[6]=new Array();
		iArray[6][0]="应收记录号";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[6][1]="80px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[7]=new Array();
		iArray[7][0]="投保人";	   				//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[7][1]="80px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[8]=new Array();
		iArray[8][0]="移动电话";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[8][1]="80px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[9]=new Array();
		iArray[9][0]="联系电话";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[9][1]="80px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[10]=new Array();
		iArray[10][0]="投保人联系地址";			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[10][1]="120px";            		//列宽
		iArray[10][2]=100;            			//列最大值
		iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[11]=new Array();
		iArray[11][0]="保单生效日";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[11][1]="80px";            		//列宽
		iArray[11][2]=100;            			//列最大值
		iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[12]=new Array();
		iArray[12][0]="应交保费";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[12][1]="60px";            		//列宽
		iArray[12][2]=100;            			//列最大值
		iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[13]=new Array();
		iArray[13][0]="应缴基础保费";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[13][1]="60px";            		//列宽
		iArray[13][2]=100;            			//列最大值
		iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[14]=new Array();
		iArray[14][0]="应缴额外保费";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[14][1]="60px";            		//列宽
		iArray[14][2]=100;            			//列最大值
		iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[15]=new Array();
		iArray[15][0]="实收基础保费";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[15][1]="60px";            		//列宽
		iArray[15][2]=100;            			//列最大值
		iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[16]=new Array();
		iArray[16][0]="实收额外保费";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[16][1]="60px";            		//列宽
		iArray[16][2]=100;            			//列最大值
		iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[17]=new Array();
		iArray[17][0]="应收时间";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[17][1]="80px";            		//列宽
		iArray[17][2]=100;            			//列最大值
		iArray[17][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[18]=new Array();
		iArray[18][0]="收费方式";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[18][1]="60px";            		//列宽
		iArray[18][2]=100;            			//列最大值
		iArray[18][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[19]=new Array();
		iArray[19][0]="代理人编码";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[19][1]="80px";            		//列宽
		iArray[19][2]=100;            			//列最大值
		iArray[19][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[20]=new Array();
		iArray[20][0]="代理人手机";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[20][1]="80px";            		//列宽
		iArray[20][2]=100;            			//列最大值
		iArray[20][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[21]=new Array();
		iArray[21][0]="代理人固话";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[21][1]="80px";            		//列宽
		iArray[21][2]=100;            			//列最大值
		iArray[21][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[22]=new Array();
		iArray[22][0]="代理人姓名";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[22][1]="80px";            		//列宽
		iArray[22][2]=100;            			//列最大值
		iArray[22][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[23]=new Array();
		iArray[23][0]="代理机构编码";			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[23][1]="100px";            		//列宽
		iArray[23][2]=100;            			//列最大值
		iArray[23][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[24]=new Array();
		iArray[24][0]="代理机构名称";			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[24][1]="120px";            		//列宽
		iArray[24][2]=100;            			//列最大值
		iArray[24][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[25]=new Array();
		iArray[25][0]="缴费截止日期";  			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[25][1]="80px";            		//列宽
		iArray[25][2]=100;            			//列最大值
		iArray[25][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[26]=new Array();
		iArray[26][0]="实收日期";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[26][1]="80px";            		//列宽
		iArray[26][2]=100;            			//列最大值
		iArray[26][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[27]=new Array();
		iArray[27][0]="核销日期";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[27][1]="80px";            		//列宽
		iArray[27][2]=100;            			//列最大值
		iArray[27][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[28]=new Array();
		iArray[28][0]="销售渠道";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[28][1]="60px";            		//列宽
		iArray[28][2]=100;            			//列最大值
		iArray[28][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[29]=new Array();
		iArray[29][0]="保单类型";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[29][1]="60px";            		//列宽
		iArray[29][2]=100;            			//列最大值
		iArray[29][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[30]=new Array();
		iArray[30][0]="状态";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[30][1]="60px";            		//列宽
		iArray[30][2]=100;            			//列最大值
		iArray[30][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[31]=new Array();
		iArray[31][0]="保单缴次";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[31][1]="60px";            		//列宽
		iArray[31][2]=100;            			//列最大值
		iArray[31][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[32]=new Array();
		iArray[32][0]="险种缴费年限";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[32][1]="80px";            		//列宽
		iArray[32][2]=100;            			//列最大值
		iArray[32][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[33]=new Array();
		iArray[33][0]="原代理人代码";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[33][1]="80px";            		//列宽
		iArray[33][2]=100;            			//列最大值
		iArray[33][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[34]=new Array();
		iArray[34][0]="原代理人";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[34][1]="60px";            		//列宽
		iArray[34][2]=100;            			//列最大值
		iArray[34][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[35]=new Array();
		iArray[35][0]="险种状态";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[35][1]="60px";            		//列宽
		iArray[35][2]=100;            			//列最大值
		iArray[35][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[36]=new Array();
		iArray[36][0]="保单服务状态";		   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[36][1]="80px";            		//列宽
		iArray[36][2]=100;            			//列最大值
		iArray[36][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		

		RableRecGrid = new MulLineEnter( "fm" , "RableRecGrid" );
		//这些属性必须在loadMulLine前
		RableRecGrid.mulLineCount = 0;
		RableRecGrid.displayTitle = 1;
		RableRecGrid.locked = 1;
		RableRecGrid.hiddenPlus=1;
		RableRecGrid.hiddenSubtraction=1;
		RableRecGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert("PrtReceivableRecordInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}   
}
function initInputBox()
{
try
	{
		fm.all('ManageCom').value = manageCom;
		var sql1 = "select Name from LDCom where ComCode = '" + manageCom + "'";
		fm.all('ManageComName').value = easyExecSql(sql1);
		var sql1 = "select Current Date - 3 month from dual ";
		fm.all('StartDate').value = easyExecSql(sql1);
		fm.all('EndDate').value=tCurrentDate;
		fm.all('SaleChnl').value="0";
		fm.all('DealState').value="8";
		showAllCodeName();
	}
	catch(ex)
	{
		alert("PrtReceivableRecordInit.jsp-->initInputBox函数中发生异常:初始化界面错误!");
	}      
}
</script>