
var turnPage1 = new turnPageClass();
var turnPage = new turnPageClass();
var mSql="";
function easyQuery(){

    var proposalGrpContNo=fm.all('ProposalGrpContNo').value; //保单号
    var appNo=fm.all('AppNo').value; //客户号
    var manageCom=fm.all('ManageCom').value; //管理机构
    var singleStartDate = fm.all('SingleStartDate').value;//应交开始日期
	var singleEndDate = fm.all('SingleEndDate').value; //  应交终止日期
	var agentCode = fm.all('AgentCode').value;//代理人
	var riskCode=fm.all('RiskCode').value;//险种代码
	var dealState=fm.all('DealState').value;//是否缴费
	
	if(singleStartDate==""||singleStartDate==null){
	    alert("应交开始日期不能为空");
	    return false;
	}
	if(singleEndDate==""||singleEndDate==null){
	    alert("应交终止日期不能为空");
	    return false;
	}
	
	
	
	var wherePar="";
	var strString="";
	
	var t1 = new Date(singleStartDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(singleEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>182 )
    {
	  alert("查询起止时间不能超过6个月！")
	  return false;
    }
    if(proposalGrpContNo!=""&&proposalGrpContNo!=null){
       wherePar+=" and a.contno='"+proposalGrpContNo+"' ";
    }
    if(manageCom!=""&&manageCom!=null){
       wherePar+=" and a.ManageCom like '"+manageCom+"%' ";
    }
    if(appNo!=""&&appNo!=null){
       wherePar+=" and a.appntno='"+appNo+"'";
    }
    if(agentCode!=""&&agentCode!=null){
       wherePar+=" and a.agentcode='"+agentCode+"' ";
    }
    
    if(dealState!=""&&dealState!=null){
       if(dealState=='0'){
	           wherePar+=" and a.paytodate>='"+singleStartDate+"' ";
		       wherePar+=" and a.paytodate<='"+singleEndDate+"' ";
		       strString+=" dealstate in ('0','4','6','3')";
       }
       if(dealState=='1'){
	           wherePar+=" and (select max(lastpaytodate) from ljspaypersonb where dealstate='1' and polno=a.polno)>='"+singleStartDate+"' ";
		       wherePar+=" and (select max(lastpaytodate) from ljspaypersonb where dealstate='1' and polno=a.polno)<='"+singleEndDate+"' ";	
		       strString+=" dealstate in ('1','3','6')";	    
       }
       if(dealState=='2'){
	           wherePar+=" and ((select max(lastpaytodate) from ljspaypersonb where dealstate='1' and polno=a.polno)>='"+singleStartDate+"' or  a.paytodate>='"+singleStartDate+"' )";
		    
		       wherePar+=" and ((select max(lastpaytodate) from ljspaypersonb where dealstate='1' and polno=a.polno)<='"+singleEndDate+"' or a.paytodate<='"+singleEndDate+"' )";
		       strString+=" dealstate not in ('2')";

       }   
       
    }
    else{
	    if(singleStartDate!=""&&singleStartDate!=null){
	       wherePar+=" and a.paytodate>='"+singleStartDate+"' ";
	    }
	    if(singleEndDate!=""&&singleEndDate!=null){
	       wherePar+=" and a.paytodate<='"+singleEndDate+"' ";
	    }
	}
    if(riskCode!=""&&riskCode!=null){
       wherePar+=" and a.riskCode  in (select code from ldcode1 where (code='"+riskCode+"' or code1='"+riskCode+"') and codetype='mainsubriskrela' union select code1 from ldcode1 where (code='"+riskCode+"' or code1='"+riskCode+"') and codetype='mainsubriskrela' union select '"+riskCode+"' from dual)  ";
    }
    
 


var strSQL="select "
          +"(select name from ldcom where comcode=a.ManageCom)  管理机构 ,"
          +"a.ManageCom 管理机构代码 ,"
          +"(select Name from labranchgroup where AgentGroup=a.AgentGroup) 营销部门,"
          +"a.contno 保单号,"
          +"a.riskcode 险种代码 ,"
          +"(select GetNoticeNo from ljspayb where a.ContNo =otherno and "+strString+" order by modifydate desc fetch first 1  row  only  ) 应收记录号,"
          +"a.appntname 投保人,"
          +"(select mobile from lcaddress c where customerno=a.appntno and c.addressno=(select addressno from lcappnt where contno=a.contno)) 移动电话,"
          +"(select (case when phone is null then (case when homephone is null then companyphone else homephone end) else phone end) from lcaddress c where customerno=a.appntno and c.addressno=(select addressno from lcappnt where contno=a.contno)) 联系电话,"
          +"(select PostalAddress from lcaddress c where customerno=a.appntno and c.addressno=(select addressno from lcappnt where contno=a.contno)) 投保人联系地址,"
          +"a.signdate 签单日,"
          +"a.prem 应交保费,"
          +"(select minvalue(a.Prem,minvalue(6000*a.PayIntv/12,(a.PayIntv/12)*a.Amnt/20))  FROM  dual) 基本保费,"
          +"(a.prem-(select minvalue(a.Prem,minvalue(6000*a.PayIntv/12,(a.PayIntv/12)*a.Amnt/20))  FROM  dual)) 额外保费,"
          +"a.paytodate 应收时间,"
          +"(select codename from ldcode where  codetype ='paymode' and code=a.PayMode) 收费方式,"
          +"getUniteCode(a.AgentCode)代理人编码,"
          +"(select Mobile from laagent  where AgentCode=a.AgentCode) 代理人手机,"
          +"(case when (select agentstate from laagent where agentcode=a.agentcode)>='06' then '孤儿单' else '业务员在职' end) 代理人在职状态,"
          +"(select Name from laagent  where AgentCode=a.AgentCode) 代理人姓名,"
          +"a.AgentCom 代理机构编码,"
          +"(select Name from lacom  where AgentCom=a.AgentCom) 代理机构名称,"
          +"(select paydate from ljspayb where a.ContNo =otherno and "+strString+" order by modifydate desc fetch first 1  row  only   ) 缴费截止日期,"
          +"(select max(ConfDate) from ljapay where incomeno=a.contno and getnoticeno=(select GetNoticeNo from ljspayb where a.ContNo =otherno and "+strString+" order by modifydate desc fetch first 1  row  only  ))  收费确认日期,"
          +"(select distinct codename from ldcode where  codetype in ('salechnl','lcsalechnl') and code=a.salechnl) 销售渠道,"
          +"(case when  a.conttype='1' then '个单' else '团单' end) 保单类型,"
          +" (select (case when stateflag='1' then '有效' when stateflag='2' then '失效' else '终止' end) from lccont where contno=a.contno) 状态,"
          +"(select case when (month(current date)<month(a.cvalidate) or (month(current date)=month(a.cvalidate) and day(current date)<=day(a.cvalidate))  ) then (year(current date) - year(a.cvalidate)) else (year(current date) - year(a.cvalidate)+1) end  from dual) 保单年度"
          +" from lcpol a   "
          +"where   a.salechnl in ('04','13') and a.conttype='1'  "
          +wherePar
          + " with ur " ;
         
    mSql=strSQL;
	turnPage1.queryModal(strSQL, ContGrid);  
	if( ContGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}
	//fm.sql.value =  strSQL;
	showCodeName();

}

function easyPrint()
{	
	if( ContGrid.mulLineCount == 0)
	{
		alert("没有需要打印的信息");
		return false;
	}
	fm.all('sql').value=mSql;
	fm.action="./IndiRiskPrint.jsp";
	fm.submit();
}



