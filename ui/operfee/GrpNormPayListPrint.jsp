<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%
    boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	XmlExport txmlExport = null;   
	GlobalInput tG = (GlobalInput)session.getValue("GI");

	String tPayDate = request.getParameter("PrintPayDate");
	String tGrpContNo = request.getParameter("PrintGrpContNo");
	String tVerifyType = request.getParameter("PrintVerifyType");

  System.out.println(tPayDate);
	System.out.println(tGrpContNo);
	System.out.println(tVerifyType);

    TransferData tTransferData= new TransferData();
	tTransferData.setNameAndValue("PayDate",tPayDate);
	tTransferData.setNameAndValue("GrpContNo",tGrpContNo);
	tTransferData.setNameAndValue("VerifyType",tVerifyType);

	VData tVData = new VData();
    tVData.addElement(tG);
	tVData.addElement(tTransferData);
          
    GrpNormPayListPrintUI tGrpNormPayListPrintUI = new GrpNormPayListPrintUI(); 
    if(!tGrpNormPayListPrintUI.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tGrpNormPayListPrintUI.mErrors.getErrContent();                
    }
    else
    {    
		VData mResult = tGrpNormPayListPrintUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	System.out.println(operFlag);
	if (operFlag==true)
	{
		session.putValue("PrintStream", txmlExport.getInputStream());
		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
	}
	else
	{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>