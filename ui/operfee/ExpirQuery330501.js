
function download()
{
  //得到所选机构
  if(fm.ManageCom.value == "")
  {
    alert("请选择管理机构！");
    return false;
  }
  
  if(fm.StartQueryDate.value == "" || fm.StartQueryDate.value == null)
  {
    alert("查询日期起期不能为空！");
    return false;
  }
	
  if(fm.QueryDate.value == "" || fm.QueryDate.value == null)
  {
    alert("查询日期止期不能为空！");
    return false;
  }
  
  var tStartQueryDate = fm.StartQueryDate.value;
  var tQueryDate = fm.QueryDate.value;
  //应收时间起止期三个月校验
  var t1 = new Date(tStartQueryDate.replace(/-/g,"\/")).getTime();
  var t2 = new Date(tQueryDate.replace(/-/g,"\/")).getTime();
  var tMinus = (t2-t1)/(24*60*60*1000);  
  if(tMinus>92)
  {
    alert("查询日期区间不能超过3个月!");
    return false;
  }
  fm.submit();
}

function afterDownload(outname,outpathname)
{
  var url = "../f1print/download.jsp?filename="+outname+"&filenamepath="+outpathname;
  fm.action=url;
  fm.submit();
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

function initInpBox()
{ 
  try
  {  
    var sql = "select current date - 3 month,current date from dual ";
    var rs = easyExecSql(sql);
    fm.all('StartQueryDate').value = rs[0][0];
    fm.all('QueryDate').value = rs[0][1];  
    //fm.all('ManageCom').value = managecom;
    //showAllCodeName();
  }
  catch(ex)
  {
    alert("在ExpirQuery330501.js-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
  }
  catch(re)
  {
    alert("ExpirQuery330501.js-->InitForm函数中发生异常:初始化界面错误!");
  }
}
