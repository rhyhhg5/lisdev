<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2006-03-09
//创建人  ：Wei li
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%
    boolean operFlag = true;
		String FlagStr = "";
		String Content = "";
		XmlExport txmlExport = null;   
		GlobalInput tG = (GlobalInput)session.getValue("GI");

		String tPayDate = request.getParameter("PrintPayDate");
		String tGrpContNo = request.getParameter("PrintGrpContNo");
		String tVerifyType = request.getParameter("PrintVerifyType");
    String tLJASql = request.getParameter("LJASql");
    
    //由于"%"无法直接通过URL传递，在IndiFinUrgeVerify.js中先替换成特殊字符"xxvvbbvv"，这里换成"%"
    tLJASql = tLJASql.replaceAll("xxvvbbvv", "%");
    
    System.out.println(tPayDate);
		System.out.println(tGrpContNo);
		System.out.println(tVerifyType);
		System.out.println(tLJASql);
    TransferData tTransferData= new TransferData();
		tTransferData.setNameAndValue("PayDate",tPayDate);
		tTransferData.setNameAndValue("GrpContNo",tGrpContNo);
		tTransferData.setNameAndValue("VerifyType",tVerifyType);
		tTransferData.setNameAndValue("LJASql",tLJASql);
		VData tVData = new VData();
    tVData.addElement(tG);
		tVData.addElement(tTransferData);
          
    GrpConNormPayListPrintUI tGrpConNormPayListPrintUI = new GrpConNormPayListPrintUI(); 
    if(!tGrpConNormPayListPrintUI.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tGrpConNormPayListPrintUI.mCErrors.getErrContent();                
    }
    else
    {                                        
		  VData mResult = tGrpConNormPayListPrintUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	System.out.println(operFlag);
	if (operFlag==true)
	{
  	ExeSQL tExeSQL = new ExeSQL();
  //获取临时文件名
    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
    String strFilePath = tExeSQL.getOneValue(strSql);
    String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
    //获取存放临时文件的路径
    //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
    //String strRealPath = tExeSQL.getOneValue(strSql);
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strVFPathName = strRealPath + "//" +strVFFileName;
    
    CombineVts tcombineVts = null;	

  	//合并VTS文件
  	String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
  	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
  
  	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
  	tcombineVts.output(dataStream);
  
  	//把dataStream存储到磁盘文件
  	//System.out.println("存储文件到"+strVFPathName);
  	AccessVtsFile.saveToFile(dataStream,strVFPathName);
  	System.out.println("==> Write VTS file to disk ");
  
  	System.out.println("===strVFFileName : "+strVFFileName);
  	//本来打算采用get方式来传递文件路径
  	response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
	}
	else
	{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>