<%
//IndiHospitalQueryInit.jsp
//程序功能：
//创建日期：2007-7-18 10:07
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
     GlobalInput gi = (GlobalInput) session.getValue("GI");
     String mangeCom = gi.ManageCom;
%>

<script language="JavaScript">  

function initInpBox()
{   
  fm.all('ManageCom').value = '<%=mangeCom%>';
}                                 

function initForm()
{
  try
  {
		initInpBox();
		initHospitalGrid();
		initSpecHospitalGrid();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

// 信息列表的初始化
function initHospitalGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[1]=new Array();
    iArray[1][0]="指定医院名称";					//列名1
    iArray[1][1]="120px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[2]=new Array();
    iArray[2][0]="联系地址";         			//列名2
    iArray[2][1]="120px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
 
    HospitalGrid = new MulLineEnter( "fm" , "HospitalGrid" ); 
    //这些属性必须在loadMulLine前
    HospitalGrid.mulLineCount = 0;   
    HospitalGrid.displayTitle = 1;
    HospitalGrid.hiddenPlus = 1; 
    HospitalGrid.hiddenSubtraction = 1;
    HospitalGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
}

function initSpecHospitalGrid()
{                               
	var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="推荐医院名称";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="联系地址";  
    iArray[2][1]="100px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    
	  SpecHospitalGrid = new MulLineEnter("fm", "SpecHospitalGrid"); 
    //这些属性必须在loadMulLine前
    SpecHospitalGrid.mulLineCount = 0;   
    SpecHospitalGrid.displayTitle = 1;
    //ContGrid.canChk=1;
    SpecHospitalGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    SpecHospitalGrid.hiddenSubtraction=1;
    SpecHospitalGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
}

</script>  