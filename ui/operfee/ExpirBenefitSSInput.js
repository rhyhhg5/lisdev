var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}


function cccc(Information,Information1)
{
    Information=Information.replace("公式3","标准公式 ");
    Information=Information.replace("WHL",Information1);
    var regS = new RegExp("QQQ","g");
    Information=Information.replace(regS," \n");
    fm.all("Information").value=Information;
    fm.all("Information").style.display="";
}






//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(ex)
  {
  	alert("在FailListInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{
  var tStartDate = fm.all("StartDate").value;
  var tEndDate = fm.all("EndDate").value;
  var strSQL = "select distinct d.contno 保单号, "
             + "d.appntname 投保人姓名, "
             + "(select appntidno from lccont where contno=d.contno) 投保人证件号, "
             + "(select name from lcinsured where contno = d.contno and insuredno = d.insuredno) 被保人姓名, "
             + "(select idno from lcinsured where contno = d.contno and insuredno = d.insuredno) 被保人证件号, "
             + "getUniteCode(d.agentcode) 保单业务员,d.insuredno 被保人客户号, "
             + "d.appntno 投保人客户号, "
             + "(select name from ldcom where comcode=d.managecom ) 管理机构,"
             + "(select name from lacom where agentcom=d.agentcom ) 代理机构 ,"
             + "d.amnt 保额,"
             + "d.cvalidate 生效日,"
             + "d.enddate 满期日,"
             + "d.InsuredAppAge 被保人投保年龄, "
             + "d.insuyear 保险年龄 "
             + "from lcpol d ,lcget c  "
             + "where d.polno = c.polno "
             + "and d.riskcode = '330501' and c.dutycode in (select dutycode from lmriskduty where riskcode = '330501') "
             + "and conttype = '1' and appflag = '1' "
             + getWherePart( 'c.ManageCom ','ManageCom','like') 
             + getWherePart( 'd.ContNo ','ContNo') 
             + "and exists (select 1 from lmdutygetalive where getdutycode = c.getdutycode) "
             +" and c.gettodate between '" + tStartDate + "' and '" + tEndDate + "' ";
             
  turnPage1.queryModal(strSQL, LjsGetGrid); 
}
function queryClick()
{
  //得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	
	var tStartDate = fm.all("StartDate").value;
	var tEndDate = fm.all("EndDate").value;
	
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("满期日起期不能为空！");
		return false;
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("满期日止期不能为空！");
		return false;
	}
	
	var tTodayDate=fm.Today.value;
	
	//不进行校验
	var t1 = new Date(tTodayDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>0)
    {
	//	alert("满期日期止期不能大于当前日期后30天!");
	//	return false;
    }
    
  easyQueryClick();
  if(LjsGetGrid.mulLineCount == 0)
  {
   alert("没有符合条件的给付信息");
   return false;
  }
}

function selectOne()
{
    var tSelNo = LjsGetGrid.getSelNo();
    fm.TempContNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"ContNo");
    fm.InsuredNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"InsuredNo");
    fm.AppntNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"AppntNo");
    fm.GetState.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"GetState");  
    
    fm.Amnt.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"Amnt");
    fm.CValiDate.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"CValiDate");
    fm.EndDate1.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"EndDate1");
    fm.InsuredAppAge.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"InsuredAppAge");
    fm.InsuYear.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"InsuYear");
    
       if(fm.InsuYear.value=="5")
  {
  	fm.RateFlag.checked= false;
  	fm.Formula.value="3";
  	document.getElementById("FormulaDis").style.display="none";
  }
}

function calData()
{
  if(LjsGetGrid.mulLineCount == 0)
  {
    alert("没有符合条件的保单信息");
    return false;
  }
  if(LjsGetGrid.getSelNo() < 1)
  {
    alert("请选择需要试算的保单");
    return false;
  }
  if (fm.RateFlag.checked == true)
  {
    fm.RateFlagValue.value = "Y";
  }
  else if (fm.RateFlag.checked == false)
  {
    fm.RateFlagValue.value = "N";
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
  
  var formula = fm.Formula.value;
  
  if(formula == "1")
  {
    fm.FormulaT.value = "A";
  }
  else if(formula == "2")
  {
    fm.FormulaT.value = "B";
  }
  else if(formula == "3")
  {
    fm.FormulaT.value = "C";
  } 
  else if(formula == "4")
  {
    fm.FormulaT.value = "D";
  }
  else if(formula == "5")
  {
    fm.FormulaT.value = "E";
  }
  else if(formula == "6")
  {
    fm.FormulaT.value = "F";
  }
  else if(formula == "7")
  {
    fm.FormulaT.value = "G";
  }
  else if(formula == "8")
  {
    fm.FormulaT.value = "H";
  } 
  
  fm.action = "./ExpirBenefitSSSubmit.jsp";
  fm.submit();	
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

