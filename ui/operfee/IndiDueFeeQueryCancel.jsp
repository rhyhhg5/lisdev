<%
//程序名称：IndiDueFeeQuery.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
  <%@page import="java.util.*"%>
  <%@page import="java.lang.*"%>  
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>  
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>  

<%
 
  //保存保单号  
  String ContNo = request.getParameter("ContNo2"); 
  
// 输出参数
   CErrors tError = null;
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI=(GlobalInput)session.getValue("GI");
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
  	//合同号
  	String contNo=request.getParameter("ContNoSelected");
  	String cancelMode=request.getParameter("SubmitCancelReason");
  	String getNoticeNo=request.getParameter("GetNoticeNoSelected");
  
  	LJSPaySchema tLJSPaySchema =new LJSPaySchema();
  	tLJSPaySchema.setGetNoticeNo(getNoticeNo);
  
  	TransferData tTransferData= new TransferData();
  	tTransferData.setNameAndValue("CancelMode","3");  //撤销
  	tTransferData.setNameAndValue("IsRepeal", "Y");
  
  	VData tVData = new VData();
  	tVData.addElement(tLJSPaySchema);
  	tVData.addElement(tGI);
  	tVData.addElement(tTransferData);	
  
    IndiLJSCancelUI tIndiLJSCancelUI = new IndiLJSCancelUI();
  	tIndiLJSCancelUI.submitData(tVData,"INSERT");
  	tError = tIndiLJSCancelUI.mCErrors;
    if(!tError.needDealError())
    {
     Content="处理成功！";
     FlagStr="Succ";
    }
    else
    {
     Content="处理失败！原因："+tError.getFirstError();
     FlagStr="Fail";
    }
  }//页面有效区
  Content = PubFun.changForHTML(Content);
%>                                      
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>

