<html> 
<% 
//程序名称：
//程序功能：无名单续期保费录入
//创建日期：20060712
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="./GrpDueFeePremInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GrpDueFeePremInputInit.jsp"%>
  <title>无名单续期</title>
</head>
<body  onload="initForm();" >
  <form action="./GrpDueFeePremInputSubmit.jsp" method=post name=fm target="fraSubmit">  
  <br></br>
  <center>
    <font>无名单续期保费、人数录入</font>
  </center>
  <br></br>
    <table>
      <tr>
        <td class=common>
          <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCContPlan);">
        </td>
        <td class=titleImg>保障计划信息</td>
      </tr>
    </table>
    <Div id="divLCContPlan" style="display: ''">
      <table class=common>
        <tr class=common>
          <td text-align: left colSpan=1>
            <span id="spanLCContPlan">  </span>
          </td>
        </tr>
      </table>
    </div>
      <br>
   	  <Input type =button class=cssButton value="保  存" id="submitButton" onclick="submitData()">
   	  <Input type =button class=cssButton value="取  消" onclick="cancel()">
   	  <Input type =button class=cssButton value="返  回" onclick="returnParent()">
   	  
   	  
   	<Input type=hidden name=grpContNo> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
