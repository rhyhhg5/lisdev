<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：转账失败清单查询
//创建日期：2006-10-27 16:06
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	

	<SCRIPT src="expirBenefitTransFailInput.js"></SCRIPT>
	<%@include file="./expirBenefitTransFailInit.jsp"%>
</head>
<body  onload="initForm();" >
<form action="" method=post name="fm" target="fraSubmit">

<table class= common border=0 width=100%>
	<tr>
		<td class= titleImg align= center>
			查询条件
		</td>
	</tr>
</table>
   <table  class= common align=center>
      <tr  class= common>
        <td  class= title> 机构 </td>
        <TD  class= input>
          <Input class= "codeno"  name=manageCom verify="管理机构|notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename style="width:80" readonly name=ManageComName elementtype="nacessary" >
        </TD>

        <td  class= title> 转帐日期  </td>
        <td  class= input>
          <input class="coolDatePicker" dateFormat="short" name="startDate" >
        </td>
         <td class = title>
         	至
        </td> 
        <td  class= input>
          <input class="coolDatePicker" dateFormat="short" name="endDate" >
        </td>        
      </tr>
      <tr class= common >
      	<!--
        <td  class= title> 处理状态 </td>
        <td  class= input>
          <input class="common" name="dealState" >
        </td>
        -->
        <td  class= title> 给付任务号 </td>
        <td  class= input>
          <input class= common name="edorAcceptNo" >
        </td>
      </tr>
    </table>
    <table class= common align=center>
          <INPUT class = cssbutton VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
    </table>


    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFailList);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
  	<Div  id= "divFailList" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanfailListGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	
		  <Div  id= "divPage" align=center style= "display: '' ">     
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		 </Div>
  	</Div>
<table>
	<tr>
		<td class = common>
			<input class = cssbutton value="后续处理" type=button onclick="dealData();">
		</td>
		<td class = common>
			<input class = cssbutton value="打印全部清单" type=button onclick="printAllList();">
		</td>
	</tr>
</table>


    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
	  <input type=hidden name="LoadFlag">
	  <input type=hidden name="sql">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
