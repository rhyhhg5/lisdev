<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：IndiDueFeePolListPrint.jsp
//程序功能：
//创建日期：2008-02-13
//创建人  ：Zhanggm modify by xp 改造成EXCEL下载方式
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "应收清单_险种_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    
      String sql = "set current query optimization=5";
        
      ExeSQL tExeSQL = new ExeSQL();
      tExeSQL.execUpdateSQL(sql);
    String subsql = request.getParameter("strsql");
	String tSQL = "select '1', b.makedate,ShowManageName(a.ManageCom) mana,";
        //显示部级销售机构
        tSQL += " (select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode) agen,";
        tSQL += " a.ContNo,b.GetNoticeNo,(select codename  from ldcode where codetype='dealstate' and code=b.dealstate),";
        tSQL += " a.AppntName,(select codename  from ldcode where codetype='sex' and code=a.appntsex),";
        //被保人
        tSQL += " f.InsuredName, " ;
        tSQL += " e.mobile,(case when e.phone is null then e.homephone else e.phone end), e.Postaladdress, " ;
        tSQL += " a.CValiDate,";
        tSQL += " (case when (select count(1) from lcrnewstatelog where contno=a.contno and state='6')>0 then (select min(cvalidate) from lbcont  where prtno = a.prtno) else a.cvalidate end),";
        tSQL += " nvl((select nvl(AccGetMoney,0) from LCAppAcc where CustomerNo=a.appntno),0),";
        //"应交险种"
        tSQL += " c.RiskCode,(select riskname from lmrisk where riskcode=c.riskcode) riskname, ";
    //    tSQL += " b.getnoticeno, ";
        
        //"应交保费"
        tSQL += " c.SumActuPayMoney, ";
        
        tSQL += "(select min(LastPayToDate) from ljspaypersonb where getnoticeno = b.getnoticeno),";
        tSQL += " (select codename from ldcode where codetype='paymode' and code=a.PayMode),";
        //孤儿单标志
        tSQL += " (case (select count(*) from LAOrphanpolicy where ContNo = a.ContNo) when 1 then '是' else '否' end ), " ;
        
        tSQL += " getUniteCode(a.AgentCode),d.Mobile,d.Phone, d.Name ";
        tSQL +=" ,codename('sex',d.sex),b.PayDate ";
        tSQL += " ,(select max(confmakedate) From ljtempfee where tempfeeno=b.getnoticeno ),  ";
        
        //20090326 zhanggm 增加作废原因 cbs00024806
        tSQL += " (case when (b.dealstate ='0' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno ";
		tSQL += " and riskcode in ('320106','120706')) is not null ) then '等待客户反馈' ";
		tSQL += " when (b.dealstate ='1' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno and riskcode in ('320106','120706')) is not null ) then '续保成功' ";
		tSQL += " when (b.dealstate ='3' and (select min(1) from ljspaypersonb where getnoticeno = b.getnoticeno  and riskcode in ('320106','120706')	) is not null ) then '满期给付' ";
		tSQL += " else (select codename  from ldcode where codetype='dealstate' and code=b.dealstate)  end),";
		tSQL += " (case b.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = b.Cancelreason) end)";
         
        tSQL += " from LCCont a,ljspayB b,ljspaypersonB c,laagent d ,lcaddress e, LCPol f";
        tSQL += " where 1=1 and a.conttype='1' and a.appflag='1' and a.contno=c.contno and b.otherno=c.contno and a.contno=f.contno ";//modify by lzy 20170307增加必要的查询条件，优化效率
        tSQL += " and a.AgentCode = d.AgentCode and a.appntno =e.customerno ";
        tSQL += " and e.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) ";
        tSQL += " and f.PolNo = c.PolNo ";
        tSQL += subsql;
        tSQL += "%' group by c.PolNo,a.prtno, c.RiskCode, c.SumActuPayMoney, f.InsuredName, b.makedate,a.contno,a.CValiDate,a.managecom,a.AgentCode,b.GetNoticeNo,a.PayMode,";
        tSQL += " b.dealstate,a.appntno,a.agentgroup,a.AppntName,b.SumDuePayMoney,b.paydate,e.Mobile,e.CompanyPhone,e.HomePhone,e.Postaladdress,d.Name,d.Mobile,d.Phone,a.appntsex,e.mobile,e.homephone,d.sex,b.Cancelreason,e.phone " ;
        tSQL += " order by mana,agen,b.GetNoticeNo desc, f.InsuredName with ur";
	System.out.println("打印查询:"+tSQL);
	
    //设置表头
    String[][] tTitle = {{"序", "抽档日期", "管理机构", "营销部门", "保单号",
                         "应收记录号", "催收状态", "投保人","性别", "被保人",
                         "移动电话 ","联系电话", "投保人联系地址",
                         "保单生效日","原始保单生效日","可抵余额","应交险种","险种名称","应交保费","应收时间","收费方式","孤儿单标志",
                         "代理人编码","代理人手机","代理人固话","代理人姓名","代理人性别","缴费截至日期","实交日期","催收状态","作废原因"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(tSQL,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>
<%-- 
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  XmlExport txmlExport = null;   
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  String tstrsql = request.getParameter("strsql");
  System.out.println("在天愿作比翼鸟，在地愿作同圈猪！");

  TransferData tTransferData= new TransferData();
  tTransferData.setNameAndValue("strsql",tstrsql);

  VData tVData = new VData();
  tVData.addElement(tG);
  tVData.addElement(tTransferData);
          
  IndiDueFeePolListPrintUI tIndiDueFeePolListPrintUI = new IndiDueFeePolListPrintUI(); 
  if(!tIndiDueFeePolListPrintUI.submitData(tVData,"PRINT"))
  {
     operFlag = false;
     Content = tIndiDueFeePolListPrintUI.mErrors.getErrContent();                
  }
  else
  {    
	  VData mResult = tIndiDueFeePolListPrintUI.getResult();			
	  txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

	  if(txmlExport==null)
	  {
	   	operFlag=false;
	   	Content="没有得到要显示的数据文件";	  
	  }
	}
	
	System.out.println(operFlag);
	if (operFlag==true)
	{
		ExeSQL tExeSQL = new ExeSQL();
    //获取临时文件名
    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
    String strFilePath = tExeSQL.getOneValue(strSql);
    String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
    //获取存放临时文件的路径
    //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
    //String strRealPath = tExeSQL.getOneValue(strSql);
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strVFPathName = strRealPath + "//" +strVFFileName;
    
    CombineVts tcombineVts = null;	
    
    String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
  	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
  
  	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
  	tcombineVts.output(dataStream);
      	
  	//把dataStream存储到磁盘文件
  	//System.out.println("存储文件到"+strVFPathName);
  	AccessVtsFile.saveToFile(dataStream,strVFPathName);
    System.out.println("==> Write VTS file to disk ");
          
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
	}
	else
	{
    	FlagStr = "Fail";
--%>   	

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>