<%
//程序名称：GrpPolQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {                                   
	  var sql = "select char(date('" + CurrentTime + "') - 3 month),char(date('" + CurrentTime + "') + 2 month) from dual ";
	    var rs = easyExecSql(sql);
//	    fm.all('StartDate').value = rs[0][0];
//	    fm.all('EndDate').value = rs[0][1];
	    fm.all('ExStartDate').value = rs[0][0];
	    fm.all('ExEndDate').value = rs[0][1];
	    fm.all('PrtNo').value = '';
	    fm.all('AgentCode').value = '';
		fm.all('ManageCom').value = managecom;
		
		showAllCodeName();  
  }
  catch(ex)
  {
    alert("在GroupPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    

function initForm()
{
  try
  {
	initInpBox();
	initGrpContGrid();
	initGrpPolGrid();
	initGrpPlanGrid();
  }
  catch(re)
  {
    alert("GrpDueFeePlanInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpContGrid()
  {                               
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="GrpContNoEx";

      iArray[2]=new Array();
      iArray[2][0]="印刷号";         		//列名
      iArray[2][1]="0px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="投保单位";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[4]=new Array();
      iArray[4][0]="特别约定有无";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[5]=new Array();
      iArray[5][0]="生效时间";         		//列名
      iArray[5][1]="70px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="已缴保费合计";         		//列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="余额";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="约定缴费金额";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[9]=new Array();
      iArray[9][0]="本次缴费金额";         		//列名
      iArray[9][1]="70px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="约定缴费时间";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="收费方式";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="管理机构";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[13]=new Array();
      iArray[13][0]="代理人编码";         		//列名
      iArray[13][1]="60px";            		//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="抽档时间";         		//列名
      iArray[14][1]="60px";            		//列宽
      iArray[14][2]=200;            			//列最大值
      iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[15]=new Array();
      iArray[15][0]="操作人";         		//列名
      iArray[15][1]="60px";            		//列宽
      iArray[15][2]=200;            			//列最大值
      iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[16]=new Array();
      iArray[16][0]="应收记录号";         		//列名
      iArray[16][1]="60px";            		//列宽
      iArray[16][2]=200;            			//列最大值
      iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[17]=new Array();
      iArray[17][0]="催收状态";         		//列名
      iArray[17][1]="60px";            		//列宽
      iArray[17][2]=200;            			//列最大值
      iArray[17][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" ); 
      //这些属性必须在loadMulLine前
      GrpContGrid.mulLineCount = 0;   
      GrpContGrid.displayTitle = 1;
      GrpContGrid.locked = 1;
      GrpContGrid.hiddenPlus = 0;
      GrpContGrid.canSel = 1;
      GrpContGrid.canChk = 0;
      GrpContGrid.selBoxEventFuncName = "getPolInfo";
      GrpContGrid.hiddenSubtraction = 1;
      GrpContGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initGrpPolGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保障计划";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      
      iArray[2]=new Array();
      iArray[2][0]="参保人数";   		//列名
      iArray[2][1]="90px";            		//列宽
      iArray[2][2]=100;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，

	  iArray[3]=new Array();
      iArray[3][0]="险种名称";		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="生效日期";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            		//列最大值
      iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      

	  iArray[5]=new Array();
      iArray[5][0]="交至日期";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="满期日期";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      
      
      GrpPolGrid = new MulLineEnter( "fm" , "GrpPolGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPolGrid.mulLineCount =0;   
      GrpPolGrid.displayTitle = 1;
      GrpPolGrid.locked = 1;
      GrpPolGrid.canChk = 0;
      GrpPolGrid.hiddenPlus = 1;
      GrpPolGrid.hiddenSubtraction = 1;
      GrpPolGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initGrpPlanGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            		//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="计划编码";          		//列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      
      
      iArray[2]=new Array();
      iArray[2][0]="约定缴费金额";   		//列名
      iArray[2][1]="90px";            		//列宽
      iArray[2][2]=100;            		//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，

	  iArray[3]=new Array();
      iArray[3][0]="约定缴费时间";		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      
      
      
      
      GrpPlanGrid = new MulLineEnter( "fm" , "GrpPlanGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPlanGrid.mulLineCount =0;   
      GrpPlanGrid.displayTitle = 1;
      GrpPlanGrid.hiddenPlus = 1;
      GrpPlanGrid.hiddenSubtraction = 1;
      GrpPlanGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>