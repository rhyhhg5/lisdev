<%
//程序名称：GrpexpirBenefitBatchInput.jsp
//程序功能：满期给付抽档
//创建日期：2008-11-5 14:34:14  
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>

	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<html>

<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GrpexpirBenefitNoticeInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpexpirBenefitNoticeInputInit.jsp"%>
</head>
<%
	
        GlobalInput tGI = new GlobalInput();
	    tGI = (GlobalInput)session.getValue("GI");
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);               	               	
      
%>
<SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;
var operator = '<%=tGI.Operator%>';
var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT> 
<body onload="initForm1();">

<form name=fm action="./expirBenefitBatchQuery.jsp" target=fraSubmit method=post>

    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        给付保单查询：
      </td>
     </tr>
    </table>
    <Div  id= "divAlivePayMulti" style= "display: ''">
      <Table  class= common>
      	<tr>
      		<td class= title>满期日起期</td>
      		<td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      		<td class= title>满期日止期</td>
      		<td class= input><Input class="common" dateFormat="short" name=EndDate ></td>
      		<td class= title>操作机构</td>
      		<td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></td>
      		</tr>
      	<tr>
      		<td class= title>保单号</td>
      		<td class= input><Input class= common name=GrpContNo></td>
      		<td class= title>客户号</td>
      		<td class= input><Input class=common name=AppntNo></td>
      </tr>
      </Table>    
    </Div>     					                                                                            

    <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="easyQueryClick();">   
    <INPUT VALUE="批量打印" TYPE = button class = cssbutton id="payMulti" onclick="printAllNotice();">
    <INPUT VALUE="单份打印" class = cssbutton TYPE=hidden onclick="printOneNotice();">
    <INPUT VALUE="单份打印" class = cssbutton TYPE=button onclick="printOneNoticeNew();">
    <INPUT VALUE="打印清单" class = cssbutton TYPE=button onclick="printList();">
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    			 团体保单信息：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLjsGetGrid" >
  					</span>
  			  	</td>
  			</tr>
  			<tr><td>  					
  				<center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage3.firstPage();"> 
     			<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage3.lastPage();">			
    			</center>
    		</td>
    		</tr>
    	</table>     
  	</div>
  	<table>
    <tr>
      <td class= titleImg>给付方式录入：
	  <input class="codeNo" name="queryType" value="1" CodeData="0|^1|选择类型^2|团险统一给付^3|个人给付" readOnly
    	          ondblclick="return showCodeListEx('queryType',[this,queryTypeName],[0,1],'','','',true);" 
    	          onkeyup="return showCodeListEx('queryType',[this,queryTypeName],[0,1],'','','',true);"><Input class="codeName" name="queryTypeName" readonly>
	  <input class= "codeno"  name=PayMode  readonly style= "display: 'none'" value=""
	    CodeData="0|^2|现金支票^3|转帐支票^4|银行转账^9|其他"
	    ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1]);" onkeyup="return showCodeListEx('PayMode',[this,PayModeName],[0,1]);" ><Input class=codename  name=PayModeName readonly style= "display: 'none'" value="选择付费方式">
	  <input class= "codeno"  name=GPayMode readonly style= "display: 'none'" value=""
	    CodeData="0|^2|现金支票^3|转帐支票^11|银行汇款"
	    ondblclick="return showCodeListEx('GPayMode',[this,GPayModeName],[0,1]);" onkeyup="return showCodeListEx('GPayMode',[this,GPayModeName],[0,1]);" ><Input class=codename  name=GPayModeName readonly style= "display: 'none'" value="选择付费方式">
		<INPUT VALUE="保存" class = cssbutton TYPE=button onclick="saveType();">  
	  </td>
	</tr>
  </table>
  	  	<table>
  		<tr>
  			<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCIndiCont);">
    		</td>
    		<td class= titleImg>
    			 被保人信息：
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
		<!--	<td> <INPUT VALUE="保单明细查询" class = cssbutton TYPE=button onclick="returnParent();"> </td> -->
    	</tr>
    </table>
  	<Div  id= "divLCIndiCont" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanIndiContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
    </center>  	
  	</div>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
    		<td class= titleImg>
    		转账信息：
    		</td>
    	</tr>
    </table> 	
  <Div  id= "divGrpDue1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanLCInsuredGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
    <center>      				
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
    </center>  
  </Div>	 
 <br>
	<Div  id= "divSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="importButton" class=cssButton value="导  入"  onclick="importInsured();">
	   		 <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
			</TR>
	 	</table>
	</Div>
	<input type= "hidden" name= "EdorNo" value=""> 
	<input type= "hidden" name= "EdorType" value="">
	<input type= "hidden" name= "TempGrpContNo" value="">  
	<input type= "hidden" name= "doType" value="B">
	<input type= "hidden" name= "CheckAll" value="">
</form>
	
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>     
      		<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
      		<INPUT type= "hidden" name= "EdorNo" value="">  
  		</form>   	
</body>
</html>
