var showInfo;
var turnPage = new turnPageClass();


  

function downloadList()
{   
	var startdate= fm.StartDate.value;
    var enddate= fm.EndDate.value;
    if(!verifyInput())
    {
    	return false;
    }
    if(fm.Flag.value!= "0" && fm.Flag.value!= "1" && fm.Flag.value!= "2"){
    	alert("缓缴方式填写有误，请重新填写！");
    	return false;
    }
    if(fm.SaleChnl.value!= "04" && fm.SaleChnl.value!= "13"&& fm.SaleChnl.value!= "00")
    {
    	alert("销售渠道填写有误，请重新填写！");
    	return false;
    }	
 
    //应收时间起期校验
	if(startdate == "" || startdate == null)
	{
		alert("请输入缓缴开始日期！");
		return false;
	}
	else
	{   
		if(!checkDateFormat("缓缴开始日期",startdate))
		{  
			fm.all('StartDate').focus();
			return false;
		}
	}
	//应收时间止期校验
	if(enddate == "" || enddate == null)
	{
		alert("请输入缓缴结束日期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("缓缴结束日期",enddate)){
			fm.all('EndDate').focus();
			return false;
		}
	}
	//应收时间起止期三个月校验
	var t1 = new Date(startdate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(enddate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>368 )
    {
	  alert("缓缴开始日期和缓缴结束日期之间不能超过1年！")
	  return false;
    }
    fm.action ="ExRenewaldelayList.jsp";
    fm.submit();
}


//日期格式校验
function checkDateFormat(tName,strValue) 
{  
	if (!isDate(strValue))
	{  
		alert("输入的["+tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}

function beforeSubmit()
{

}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

