var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(I,InitMoney,AddMoney,Amnt,Information)
{
  fm.all("InitRate").value=I;
  fm.all("InitMoney").value=InitMoney;
  fm.all("AddMoney").value=AddMoney;
  fm.all("Amnt").value=Amnt;
  fm.all("SumPay").value=parseFloat(InitMoney)+parseFloat(AddMoney)+parseFloat(Amnt);
  
  var a = Information.split("|");
  var col = AddBonusGrid.colCount-1;
  reInitAddBonusGrid(a.length/col);
  
  for(var i=0;i<AddBonusGrid.mulLineCount;i++)
  {
    for(var j=0;j<col;j++)
    {
      AddBonusGrid.setRowColData(i,j+1,a[i*col+j]);
    }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(ex)
  {
  	alert("在FailListInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{
  var tStartDate = fm.all("StartDate").value;
  var tEndDate = fm.all("EndDate").value;
  var strSQL = "select distinct d.contno 保单号, "
             + "d.appntname 投保人姓名, "
             + "(select appntidno from lccont where contno=d.contno) 投保人证件号, "
             + "(select name from lcinsured where contno = d.contno and insuredno = d.insuredno) 被保人姓名, "
             + "(select idno from lcinsured where contno = d.contno and insuredno = d.insuredno) 被保人证件号, "
             + "getUniteCode(d.agentcode) 保单业务员,d.insuredno 被保人客户号, "
             + "d.appntno 投保人客户号, "
             + "(select name from ldcom where comcode=d.managecom ) 管理机构,"
             + "(select name from lacom where agentcom=d.agentcom ) 代理机构 ,"
             + "d.amnt 保额,"
             + "d.cvalidate 生效日,"
             + "d.enddate 满期日,"
             + "d.InsuredAppAge 被保人投保年龄, "
             + "d.insuyear 保险年龄 "
             + "from lcpol d ,lcget c  "
             + "where d.polno = c.polno "
             + "and d.riskcode = '330501' and c.dutycode in (select dutycode from lmriskduty where riskcode = '330501') "
             + "and conttype = '1' and appflag = '1' "
             + getWherePart( 'c.ManageCom ','ManageCom','like') 
             + getWherePart( 'd.ContNo ','ContNo') 
             + "and exists (select 1 from lmdutygetalive where getdutycode = c.getdutycode) "
             +" and c.gettodate between '" + tStartDate + "' and '" + tEndDate + "' ";
             
  turnPage1.queryModal(strSQL, LjsGetGrid); 
}
function queryClick()
{
  //得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	
	var tStartDate = fm.all("StartDate").value;
	var tEndDate = fm.all("EndDate").value;
	
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("满期日起期不能为空！");
		return false;
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("满期日止期不能为空！");
		return false;
	}
	
	var tTodayDate=fm.Today.value;
	
	//不进行校验
	var t1 = new Date(tTodayDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>0)
    {
	//	alert("满期日期止期不能大于当前日期后30天!");
	//	return false;
    }
    
  easyQueryClick();
  if(LjsGetGrid.mulLineCount == 0)
  {
   alert("没有符合条件的给付信息");
   return false;
  }
}

function selectOne()
{
    var tSelNo = LjsGetGrid.getSelNo();
    fm.TempContNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"ContNo");
    fm.InsuredNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"InsuredNo");
    fm.AppntNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"AppntNo");
    
    fm.Amnt.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"Amnt");
    fm.CValiDate.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"CValiDate");
    fm.EndDate1.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"EndDate1");
    fm.InsuredAppAge.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"InsuredAppAge");
    fm.InsuYear.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"InsuYear");
    if(!calData())
    {
      return false;
    }
}

function calData()
{
  if (fm.RateFlag.checked == true)
  {
    fm.RateFlagValue.value = "Y";
  }
  else if (fm.RateFlag.checked == false)
  {
    fm.RateFlagValue.value = "N";
  }
  
  fm.action = "./ExpirBenefitSSEasySubmit.jsp";
  fm.submit();	
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

//重新催收记录的初始化
function reInitAddBonusGrid(mulLineCount)
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[1]=new Array();
      iArray[1][0]="项目";         		//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=200;            		//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="利率变动日起";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="利率变动日止";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="银行存款税后利息";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            	        //列最大值
      iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="外加忠诚奖";         		//列名
      iArray[5][1]="120px";            		//列宽
      iArray[5][2]=200;            	        //列最大值
      iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
	  AddBonusGrid = new MulLineEnter( "fm" , "AddBonusGrid" ); 
      //这些属性必须在loadMulLine前
      AddBonusGrid.mulLineCount = mulLineCount;   
      AddBonusGrid.displayTitle = 1;
      AddBonusGrid.hiddenPlus = 1;
      AddBonusGrid.hiddenSubtraction = 1;
      //LjsGetGrid.locked = 1;
      AddBonusGrid.canChk = 0;
      AddBonusGrid.canSel = 0;
      AddBonusGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        alert("重新初始化initAddBonusGrid函数中发生异常:初始化界面错误!");
      }
}