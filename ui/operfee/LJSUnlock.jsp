<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LJSUnlock.jsp
//程序功能：续期收付费信息修改录入界面
//创建日期：2009-04-10
//创建人  ：zhanggm
//页面写的比较恶心，没办法，需求就这么订的，4个功能竟然放在一个页面。
//更新记录：  更新人 zhanggm   更新日期   2009-5-7  更新原因/内容 保全续期页面拆分
//更新记录：  更新人 zhanggm  更新日期   2009-7-30  更新原因/内容 批量解锁
//更新记录：  更新人 zhanggm  更新日期   2009-8-11  更新原因/内容 可以通过复选框选择多条付费记录进行解锁，如果不选择，默认解锁列表中全部付费记录。
%>
<script>
  var tsql = "1 and code in (select code from ldcode where codetype = #paymodebqpay#)";
  var tsq2 = "1 and code in (select code from ldcode where codetype = #paymodebqget#)";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!―页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <SCRIPT src="LJSUnlock.js"></SCRIPT> 
  <%@include file="LJSUnlockInit.jsp"%> 
  
  <title>续期收付费信息修改</title>
</head>
<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form action="LJSUnlockSave.jsp" method=post name=fm target="fraSubmit">
    <table class = common align = center>
	  <tr  class= common>		    	
		<td  class= input><input type="radio" name="Type2" id="Type2" value="0" onclick=getType("3") checked>收费项目
		<input type="radio" name="Type2" id="Type2" value="1" onclick=getType("4")>付费项目</td>
		<td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </table>
  
  
  <div id="divXQPay" align=left style="display: '' ">
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>管理机构</td>
        <td  class= input><Input class= "codeno"  name=ManageCom3  ondblclick="return showCodeList('comcode',[this,ManageComName3],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName3],[0,1],null,null,null,1);" ><Input class=codename readonly name=ManageComName3 ></td>
        <td  class= title>保单号  </td>
        <td  class= input><input class=common  name=ContNo3 ></td>
        <td  class= title>收费凭证号</td>
        <td  class= input><input class=common  name=GetNoticeNo3 ></td>
      </tr>
      <tr class= common>
      <td class=button><input class=cssButton  VALUE="查  询"  TYPE=button onclick=queryClick("XQPAY")></td>
      </tr>
    </table>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLJSPayXQ);"></td>
	  <td class=titleImg>转账失败清单</td>
	</tr>
  </table>
  <div id="divLJSPayXQ" style="display:''">
	<table class=common>
      <tr class=common>
	    <td text-align:left colSpan=1><span id="spanLJSPayXQGrid"></span></td>
	  </tr>
	</table>
  </div>
  <div id="divPage3" align=center style="display: 'none' ">
	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage3.firstPage();">
	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();">
	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();">
	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage3.lastPage();">
  </div>
  <br>
  <div align=left id="divPAYMUL" style="display: 'none' ">
    <input class=cssButton  VALUE="批量解锁"  TYPE=button onclick="unlockMul('PAYMUL')">
    <font color="#FF0000">可以通过复选框选择多条收费记录进行解锁，如果不选择，默认解锁列表中全部收费记录。</font>
  </div>
  <br>
  <table  class= common align=center>
      <tr  class= common>
        <td  class= title>收费凭证号</td>
        <td  class= input><input class=readonly  name=GetNoticeNoR3 readonly></td>
        <td  class= title>保单号  </td>
        <td  class= input><input class=readonly  name=ContNoR3 readonly></td>
        <td  class= title>收费金额</td>
        <td  class= input><input class=readonly  name=GetMoney3 readonly></td>
      </tr>
      <tr class= common>
        <td class= title >失败原因</td>
        <td class= input colspan=5><input class=readonly  name=Reason3 readonly></td>
      </tr>
    </table>
    <br>
    <table  class= common align=center>
      <tr  class= common>
        <td class= title>交费方式</td>    
		<td class= input><Input class="codeNo" name="PayMode3"  verify="交付费方式|notnull&code:PayMode"  ondblclick="return showCodeList('PayMode',[this,PayModeName3],[0,1],null,tsql,'1',1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName3],[0,1],null,tsql,'1',1);"><Input class="codeName" name="PayModeName3"  elementtype="nacessary" readonly></td>
        <td  class= title>对方姓名</td>
        <td  class= input><Input class= common name=Drawer3 ></td>
        <td  class= title>对方身份证号</td>
        <td  class= input><Input class= common  name=DrawerID3 ></td>
      </tr>
      <tr  class= common>
        <td  class= title>对方开户银行</td>
        <td  class= input><Input class="codeNo" name=BankCode3 ondblclick="return showCodeList('Bank',[this,BankCodeName3],[0,1]);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName3],[0,1]);"><Input class="codeName" name=BankCodeName3 readonly ></td>
        <td  class= title>对方银行帐号</td>
        <td  class= input><Input class= common  name=BankAccNo3 onblur="confirmBankAccNo(this)"></td>
        <td class=title>对方帐户名</td>
        <td class=input><Input class= common  name=AccName3 ></td>
      </tr>
    </table>
  </div> 
  
  <div id="divXQGet" align=left style="display: 'none' ">
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>管理机构</td>
        <td  class= input><Input class= "codeno"  name=ManageCom4  ondblclick="return showCodeList('comcode',[this,ManageComName4],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName4],[0,1],null,null,null,1);" ><Input class=codename readonly name=ManageComName4 ></td>
        <td  class= title>保单号  </td>
        <td  class= input><input class=common  name=ContNo4 ></td>
        <td  class= title>付费凭证号</td>
        <td  class= input><input class=common  name=ActuGetNo4 ></td>
      </tr>
      <tr class= common>
      <td class=button><input class=cssButton  VALUE="查  询"  TYPE=button onclick=queryClick("XQGET")></td>
      </tr>
    </table>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLJAGetXQ);"></td>
	  <td class=titleImg>转账失败清单</td>
	</tr>
  </table>
  <div id="divLJAGetXQ" style="display:''">
	<table class=common>
      <tr class=common>
	    <td text-align:left colSpan=1><span id="spanLJAGetXQGrid"></span></td>
	  </tr>
	</table>
  </div>
  <div id="divPage4" align=center style="display: 'none' ">
	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage4.firstPage();">
	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage4.previousPage();">
	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage4.nextPage();">
	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage4.lastPage();">
  </div>
  <br>
  <div align=left id="divGETMUL" style="display: 'none' ">
    <input class=cssButton  VALUE="批量解锁"  TYPE=button onclick="unlockMul('GETMUL')">
    <font color="#FF0000">可以通过复选框选择多条付费记录进行解锁，如果不选择，默认解锁列表中全部付费记录。</font>
  </div>
  <br>
  <table  class= common align=center>
      <tr  class= common>
        <td  class= title>付费凭证号</td>
        <td  class= input><input class=readonly  name=ActuGetNoR4 readonly></td>
        <td  class= title>保单号  </td>
        <td  class= input><input class=readonly  name=ContNoR4 readonly></td>
        <td  class= title>付费金额</td>
        <td  class= input><input class=readonly  name=GetMoney4 readonly></td>
      </tr>
      <tr class= common>
        <td class= title >失败原因</td>
        <td class= input colspan=5><input class=readonly  name=Reason4 readonly></td>
      </tr>
    </table>
    <br>
    <table  class= common align=center>
      <tr  class= common>
        <td class= title>付费方式</td>    
		<td class= input><Input class="codeNo" name="PayMode4"  verify="交付费方式|notnull&code:PayMode"  ondblclick="return showCodeList('PayMode',[this,PayModeName4],[0,1],null,tsql,'1',1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName4],[0,1],null,tsql,'1',1);"><Input class="codeName" name="PayModeName4"  elementtype="nacessary" readonly></td>
        <td  class= title>对方姓名</td>
        <td  class= input><Input class= common name=Drawer4 ></td>
        <td  class= title>对方身份证号</td>
        <td  class= input><Input class= common  name=DrawerID4 ></td>
      </tr>
      <tr  class= common>
        <td  class= title>对方开户银行</td>
        <td  class= input><Input class="codeNo" name=BankCode4 ondblclick="return showCodeList('Bank',[this,BankCodeName4],[0,1]);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName4],[0,1]);"><Input class="codeName" name=BankCodeName4 readonly ></td>
        <td  class= title>对方银行帐号</td>
        <td  class= input><Input class= common  name=BankAccNo4 onblur="confirmBankAccNo(this)"></td>
        <td class=title>对方帐户名</td>
        <td class=input><Input class= common  name=AccName4 ></td>
      </tr>
    </table>
  </div>  
  
    <input type=hidden id="ywtype" name="ywtype" value = "XQPAY">
    <input type=hidden id="ywtype1" name="ywtype1" value = "PAYMUL">
    <input type=hidden id="ManageComMul" name="ManageComMul">
    <input type=hidden id="ContNoMul" name="ContNoMul">
    <input type=hidden id="GetNoticeNoMul" name="GetNoticeNoMul">
    <input type=hidden id="RiskCode" name="RiskCode">
    <input type=hidden id="InsuredNo" name="InsuredNo">
    <input type=hidden id="TempContNo" name="TempContNo">
    <div align=right>
      <!-- <input class=cssButton  VALUE="测试"  TYPE=button onclick=checkSubmit("XQPAY")> -->
      <p><font color="#ff0000">如果对收付方式有疑问，请您点击&quot;收付方式说明&quot;来查看收付方式的详细说明</font></p>
      <input type =button class=cssButton value="收付方式说明" onclick="payModeHelp();">
      <input class=cssButton  VALUE="确  认"  TYPE=button onclick="submitForm()">
    </div>
    
</form>
<!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
