<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：ExpirQuery330501Input.jsp
//程序功能：常B给付追踪表
//创建日期：2010-07-26 
//创建人  ：About Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>

<SCRIPT>
  var managecom = <%=tGI.ManageCom%>;
  var operator = '<%=tGI.Operator%>';
</SCRIPT>

<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="ExpirQuery330501.js"></SCRIPT>
</head>
<body  onload="initForm();" >    
  <form  method=post name=fm target="fraSubmit" action="ExpirQuery330501Download.jsp">
    <table  class= common>
      <tr> 
    	<td class= titleImg>查询条件</td>   		 
      </tr>
      <tr  class= common>
        <td  class= title>管理机构</td>
        <td  class= input><Input class= "codeno"  name=ManageCom readonly ondblclick="return showCodeList('managecom4',[this,ManageName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('managecom4',[this,ManageName],[0,1],null,null,null,1);" ><Input class=codename readonly name=ManageName></td>
        <td class= title>查询日期起期</td>
        <td  class= input><input class=coolDatePicker3 name="StartQueryDate" verify="查询日期|date&notnull" elementtype="nacessary"  style="width:130"></td>
         <td class= title>查询日期止期</td>
        <td  class= input><input class=coolDatePicker3 name="QueryDate" verify="查询日期|date&notnull" elementtype="nacessary"  style="width:130"></td>
        
      </tr>
    </table>
  <br>
  <input class=cssButton  VALUE="下载清单"  TYPE=button onclick="download();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>