package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class NormPayGrpChooseOperBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private String serNo="";//流水号 
  private String tLimit="";    

//暂收费表
  private LJTempFeeBL        mLJTempFeeBL          = new LJTempFeeBL();
  private LJTempFeeSet       mLJTempFeeSet      = new LJTempFeeSet();
//暂收费分类表
  private LJTempFeeClassSet  mLJTempFeeClassSet    = new LJTempFeeClassSet();
  private LJTempFeeClassSet  mLJTempFeeClassNewSet = new LJTempFeeClassSet();
//应收个人交费表
  private LJSPayPersonSet    mLJSPayPersonSet      = new LJSPayPersonSet();
  private LJSPayPersonSet    mLJSPayPersonNewSet   = new LJSPayPersonSet();
//实收个人交费表
  private LJAPayPersonSet    mLJAPayPersonSet      = new LJAPayPersonSet();
//实收集体交费表
  private LJAPayGrpBL        mLJAPayGrpBL          = new LJAPayGrpBL();
//实收总表
  private LJAPayBL           mLJAPayBL             = new LJAPayBL();
//个人保单表
  private LCPolBL            mLCPolBL              = new LCPolBL();
//集体保单表
  private LCGrpPolBL         mLCGrpPolBL           = new LCGrpPolBL();
//保费项表
  private LCPremSet          mLCPremSet            = new LCPremSet();
  private LCPremSet          mLCPremNewSet         = new LCPremSet();   
//保险责任表LCDuty
  private LCDutySet          mLCDutySet            = new LCDutySet();  

  //业务处理相关变量
  public NormPayGrpChooseOperBL() {
  }
  public static void main(String[] args) {

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;  	

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");
          
    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");    
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");    
      
    System.out.println("Start NormPayGrpChooseOper BL Submit...");

    NormPayGrpChooseOperBLS tNormPayGrpChooseOperBLS=new NormPayGrpChooseOperBLS();
    tNormPayGrpChooseOperBLS.submitData(mInputData,cOperate);

    System.out.println("End LJNormPayGrpChooseOper BL Submit...");

    //如果有需要处理的错误，则返回
    if (tNormPayGrpChooseOperBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tNormPayGrpChooseOperBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
   boolean tReturn =false;
   String sqlStr="";
   String GetNoticeNo="";//通知书号
   String GrpPolNo = mLCGrpPolBL.getGrpPolNo();
   String Operator = mLCGrpPolBL.getOperator();
   String ManageCom = mLCGrpPolBL.getManageCom();
   String PayDate = mLCGrpPolBL.getPayDate();   
   //产生流水号
   tLimit=PubFun.getNoLimit(mManageCom);
   serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);   	
//step one-查询数据
   
//0-查询集体保单表
      LCGrpPolDB  tLCGrpPolDB   = new LCGrpPolDB();
      LCGrpPolSet tLCGrpPolSet  = new LCGrpPolSet();
      LCGrpPolBL  tempLCGrpPolBL= new LCGrpPolBL();
      
      sqlStr = "select * from LCGrpPol where GrpPolNo='"+GrpPolNo+"' ";
      sqlStr = sqlStr+"and GrpPolNo not in (select GrpPolNo from LJSPayGrp where GrpPolNo='"+GrpPolNo+"' ";
System.out.println("sqlStr="+sqlStr);
      tLCGrpPolSet = tLCGrpPolDB.executeQuery(sqlStr);
      if(tLCGrpPolDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "NormPayGrpChooseOperBL";
      tError.functionName = "dealData";
      tError.errorMessage = "集体保单表查询失败!";
      this.mErrors.addOneError(tError);
      tLCGrpPolSet.clear();
      return false;      
      }
      else{//保存查询出来的集体保单表後用
      tempLCGrpPolBL.setSchema(tLCGrpPolSet.get(1));
      tReturn=true;
      }      
//1-查询保单表
      LCPolDB tLCPolDB   = new LCPolDB();
      LCPolSet tLCPolSet = new LCPolSet();
      sqlStr = "select * from LCPol where GrpPolNo='"+GrpPolNo+"'";
      tLCPolSet = tLCPolDB.executeQuery(sqlStr);
      if(tLCPolDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "NormPayGrpChooseOperBL";
      tError.functionName = "dealData";
      tError.errorMessage = "个人保单表查询失败!";
      this.mErrors.addOneError(tError);
      tLCPolSet.clear();
      return false;      
      }
      else{
      mLCPolBL.setSchema(tLCPolSet.get(1));
      tReturn=true;
      }
//2-查询应收个人表
      LJSPayPersonDB tLJSPayPersonDB   = new LJSPayPersonDB();
      sqlStr = "select * from LJSPayPerson where PolNo='"+PolNo+"'";
      mLJSPayPersonSet = tLJSPayPersonDB.executeQuery(sqlStr);
      if(tLJSPayPersonDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "NormPayGrpChooseOperBL";
      tError.functionName = "dealData";
      tError.errorMessage = "应收个人表查询失败!";
      this.mErrors.addOneError(tError);
      mLJSPayPersonSet.clear();
      return false;      
      }
      else{
      tReturn=true;
      }
//3-查询保费项表//根据应收个人交费表查询该表项
      LJSPayPersonBL tLJSPayPersonBL;
      LCPremSet tLCPremSet;
      LCPremBL tLCPremBL;      
      LCPremDB tLCPremDB   = new LCPremDB();       
      for(int num=1;num<=mLJSPayPersonSet.size();num++)
      {
      	tLJSPayPersonBL=new LJSPayPersonBL();
      	tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(num));
      	sqlStr="select * from LCPremSet where PolNo='"+tLJSPayPersonBL.getPolNo()+"'";
      	sqlStr=sqlStr+" and DutyCode='"+tLJSPayPersonBL.getDutyCode()+"'";
      	sqlStr=sqlStr+" and PayPlanCode='"+tLJSPayPersonBL.getPayPlanCode()+"'";
      	tLCPremSet = new LCPremSet();
      	tLCPremSet=tLCPremDB.executeQuery(sqlStr);
        if(tLCPremDB.mErrors.needDealError() == true)
         {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPremDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "NormPayGrpChooseOperBL";
          tError.functionName = "dealData";
          tError.errorMessage = "保费项表查询失败!";
          this.mErrors.addOneError(tError);
          tLCPremSet.clear();
          mLCPremSet.clear();
          return false;      
         }      	
      	tLCPremBL=new LCPremBL();
        tLCPremBL.setSchema(tLCPremSet.get(num).getSchema());
      	mLCPremSet.add(tLCPremBL);      	
      }
        tReturn=true;
//4-查询暂交费分类表
      LJTempFeeClassDB tLJTempFeeClassDB   = new LJTempFeeClassDB(); 
      sqlStr="select * from LJTempFeeClass where TempFeeNo='"+mLJTempFeeBL.getTempFeeNo()+"'";
      mLJTempFeeClassSet=tLJTempFeeClassDB.executeQuery(sqlStr);
      if(tLJTempFeeClassDB.mErrors.needDealError() == true)
       {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCPremDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "NormPayGrpChooseOperBL";
        tError.functionName = "dealData";
        tError.errorMessage = "暂交费分类表表查询失败!";
        this.mErrors.addOneError(tError);
        mLJTempFeeClassSet.clear();
        return false;      
       }
      tReturn=true;        
//step two-处理数据
    int i,iMax;
    //添加纪录
    if(this.mOperate.equals("VERIFY"))
    {
//1-应收总表和暂交费表数据填充实收总表                       
     mLJAPayBL.setPayNo(mLJSPayBL.getGetNoticeNo());         //交费收据号码
     mLJAPayBL.setIncomeNo(mLJSPayBL.getOtherNo());          //应收/实收编号
     mLJAPayBL.setIncomeType(mLJSPayBL.getOtherNoType());    //应收/实收编号类型
     mLJAPayBL.setAppntNo(mLJSPayBL.getAppntNo());           //  投保人客户号码
     mLJAPayBL.setSumActuPayMoney(mLJSPayBL.getSumDuePayMoney()); // 总实交金额
     mLJAPayBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate());   // 到帐日期
     mLJAPayBL.setPayDate(mLJSPayBL.getPayDate());           //交费日期    
     mLJAPayBL.setConfDate(mLJTempFeeBL.getConfDate());      //确认日期
     mLJAPayBL.setApproveCode(mLJSPayBL.getApproveCode());   //复核人编码
     mLJAPayBL.setApproveDate(mLJSPayBL.getApproveDate());   //  复核日期
     mLJAPayBL.setSerialNo(serNo);                           //流水号
     mLJAPayBL.setOperator(mLJSPayBL.getOperator());         // 操作员
     mLJAPayBL.setMakeDate(CurrentDate);                     //入机时间      
     mLJAPayBL.setMakeTime(CurrentTime);                     //入机时间
     mLJAPayBL.setModifyDate(CurrentDate);                   //最后一次修改日期
     mLJAPayBL.setModifyTime(CurrentTime);                   //最后一次修改时间   
       
//2-暂交费表核销标志置为1    
     mLJTempFeeBL.setConfFlag("1");//核销标志置为1
     tReturn=true;      
     
//3-暂交费分类表，核销标志置为1
     iMax=mLJTempFeeClassSet.size() ;
     LJTempFeeClassBL tLJTempFeeClassBL;
     for (i=1;i<=iMax;i++)
     {
      tLJTempFeeClassBL = new LJTempFeeClassBL() ;
      tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).getSchema());
      tLJTempFeeClassBL.setConfFlag("1");//核销标志置为1
      mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
      tReturn=true;      
     }    
//4-应收个人表填充实收个人表    

    LJAPayPersonBL tLJAPayPersonBL;
    iMax=mLJSPayPersonSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLJSPayPersonBL = new LJSPayPersonBL();	
      tLJAPayPersonBL = new LJAPayPersonBL();
      tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(i).getSchema());
      tLJAPayPersonBL.setPolNo(tLJSPayPersonBL.getPolNo());           //保单号码    
      tLJAPayPersonBL.setPayCount(tLJSPayPersonBL.getPayCount());     //第几次交费
      tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonBL.getGrpPolNo());     //集体保单号码
      tLJAPayPersonBL.setContNo(tLJSPayPersonBL.getContNo());         //总单/合同号码
      tLJAPayPersonBL.setAppntNo(tLJSPayPersonBL.getAppntNo());       //投保人客户号码
      tLJAPayPersonBL.setPayNo(mLJTempFeeBL.getTempFeeNo());          //交费收据号码
      tLJAPayPersonBL.setPayAimClass(tLJSPayPersonBL.getPayAimClass());//交费目的分类
      tLJAPayPersonBL.setDutyCode(tLJSPayPersonBL.getDutyCode());      //责任编码
      tLJAPayPersonBL.setPayPlanCode(tLJSPayPersonBL.getPayPlanCode());//交费计划编码
      tLJAPayPersonBL.setSumDuePayMoney(tLJSPayPersonBL.getSumDuePayMoney());//总应交金额
      tLJAPayPersonBL.setSumActuPayMoney(tLJSPayPersonBL.getSumActuPayMoney());//总实交金额
      tLJAPayPersonBL.setPayIntv(tLJSPayPersonBL.getPayIntv());        //交费间隔
      tLJAPayPersonBL.setPayDate(tLJSPayPersonBL.getPayDate());        //交费日期
      tLJAPayPersonBL.setPayType(tLJSPayPersonBL.getPayType());        //交费类型
      tLJAPayPersonBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate()); //到帐日期
      tLJAPayPersonBL.setConfDate(mLJTempFeeBL.getConfDate());         //确认日期
      tLJAPayPersonBL.setLastPayToDate(tLJSPayPersonBL.getLastPayToDate());  //原交至日期
      tLJAPayPersonBL.setCurPayToDate(tLJSPayPersonBL.getCurPayToDate());    //现交至日期
      tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonBL.getInInsuAccState());//转入保险帐户状态
      tLJAPayPersonBL.setApproveCode(tLJSPayPersonBL.getApproveCode());      //复核人编码
      tLJAPayPersonBL.setApproveDate(tLJSPayPersonBL.getApproveDate());      //复核日期
      tLJAPayPersonBL.setSerialNo(serNo);                              //流水号
      tLJAPayPersonBL.setOperator(tLJSPayPersonBL.getOperator());      //操作员
      tLJAPayPersonBL.setMakeDate(CurrentDate);                        //入机日期
      tLJAPayPersonBL.setMakeTime(CurrentTime);                        //入机时间
      tLJAPayPersonBL.setGetNoticeNo(tLJSPayPersonBL.getGetNoticeNo());//通知书号码
      tLJAPayPersonBL.setModifyDate(CurrentTime);                      //最后一次修改日期
      tLJAPayPersonBL.setModifyTime(CurrentTime);                      //最后一次修改时间
     
      mLJAPayPersonSet.add(tLJAPayPersonBL);      
      tReturn=true;      
    }
//5-更新保单表字段，取第一个应收个人交费纪录
    tLJSPayPersonBL = new LJSPayPersonBL();	
    tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(1).getSchema());
    mLCPolBL.setPaytoDate(tLJSPayPersonBL.getCurPayToDate());                //交至日期
    mLCPolBL.setSumPrem(mLCPolBL.getSumPrem()+mLJSPayBL.getSumDuePayMoney());//总累计保费
    //求总余额:如果应收总表应收款=0，表明有余额，且这次的余额值存放在责任编码为
    //"yet"的应收个人交费纪录中（见个人催收流程图）,取出放在个人保单表的余额字段中 
    //否则，个人保单表余额纪录置为0
    if(mLJSPayBL.getSumDuePayMoney()==0)
    {
      boolean yetFlag=false;
      for(int num=1;num<=mLJSPayPersonSet.size();num++)
      {
      	tLJSPayPersonBL=new LJSPayPersonBL();
      	tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(num));
      	if(tLJSPayPersonBL.getDutyCode().equals("yet"))
      	{
      	 mLCPolBL.setLeavingMoney(mLCPolBL.getLeavingMoney()-tLJSPayPersonBL.getSumActuPayMoney());	
      	 yetFlag=true;
      	 break;
      	}      	 
      }
      //如果没有这条纪录，但是总应收款=0，说明余额和应收款抵消，故设为0
      if(!yetFlag){mLCPolBL.setLeavingMoney(0);}	                	
    }
    else {mLCPolBL.setLeavingMoney(0);}
    mLCPolBL.setModifyDate(CurrentDate);//最后一次修改日期
    mLCPolBL.setModifyTime(CurrentTime);//最后一次修改时间
    tReturn=true;                
//6-更新保费项表字段
    for(int num=1;num<=mLCPremSet.size();num++)
    {
      tLJSPayPersonBL=new LJSPayPersonBL();
      tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(num));
      tLCPremBL=new LCPremBL();
      tLCPremBL.setSchema(mLCPremSet.get(num));
      tLCPremBL.setPayTimes(tLCPremBL.getPayTimes()+1); //已交费次数      
      tLCPremBL.setPrem(tLJSPayPersonBL.getSumActuPayMoney());//实际保费
      tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()+tLJSPayPersonBL.getSumActuPayMoney());//累计保费     
      tLCPremBL.setPaytoDate(tLJSPayPersonBL.getCurPayToDate());//交至日期
      tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期      
      tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间 
      mLCPremNewSet.add(tLCPremBL);
//6-2更新保险责任表
      LCDutyBL tLCDutyBL ;
      int n; 
      for(n=1;n<=mLCDutySet.size();n++);
      {
       tLCDutyBL=new LCDutyBL();
       tLCDutyBL.setSchema(mLCDutySet.get(n));              
       if(tLCPremBL.getPolNo().equals(tLCDutyBL.getPolNo())&&tLCPremBL.getDutyCode().equals(tLCDutyBL.getDutyCode()))
        {
         tLCDutyBL.setPrem(tLCPremBL.getPrem());//实际保费
         tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem()+tLCPremBL.getPrem());//累计保费
         tLCDutyBL.setPaytoDate(tLCPremBL.getPaytoDate());//交至日期
         tLCDutyBL.setModifyDate(CurrentDate);//最后一次修改日期
         tLCDutyBL.setModifyTime(CurrentTime);//最后一次修改时间

         mLCDutySet.add(tLCDutyBL);         
         break;         
        }//end if      	
      } //end for            
    } //end for
    tReturn=true;    
//更新完毕
 }
  return tReturn;
}

 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
   //应收总表
    mLCGrpPolBL.setSchema((LCGrpPolSchema)mInputData.getObjectByObjectName("LCGrpPolSchema",0));    
    if(mLCGrpPolBL ==null )
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="NormPayGrpChooseOperBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
  
  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();  	
    try
    {  	 
    mInputData.add(mLJAPayBL);	          //实收总表
    mInputData.add(mLJAPayGrpBL);         //实收集体表
    mInputData.add(mLCGrpPolBL );         //集体保单表
    mInputData.add(mLJTempFeeBL);         //暂交费表
    mInputData.add(mLJTempFeeClassNewSet);//暂交费分类表
    mInputData.add(mLJAPayPersonSet);     //实收个人表
    mInputData.add(mLJSPayPersonSet);     //应收个人交费表
    mInputData.add(mLCPolBL);             //个人保单表
    mInputData.add(mLCPremNewSet);        //保费项表 
    mInputData.add(mLCDutySet);           //保险责任表       
System.out.println("prepareOutputData:");      
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="NormPayGrpChooseOperBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
                
    return true;
  } 
}
 
  