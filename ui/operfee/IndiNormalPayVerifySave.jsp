 <%
//程序名称：IndiNormalPayVerifySave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//             杨红      2005-07-14       
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%> 
  <%@page import="com.sinosoft.lis.finfee.*"%>  
  <%@page import="com.sinosoft.lis.pubfun.*"%>    
  
<%@page contentType="text/html;charset=GBK" %>

<%
// 输出参数
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    //String PolNo  = request.getParameter("PolNo");
    String ContNo  = request.getParameter("ContNo"); 
    String Num[]      = request.getParameterValues("PremGridNo");  
	String PolNo[]    =request.getParameterValues("PremGrid1");
    String DutyCode[]      = request.getParameterValues("PremGrid2");
    String PayPlanCode[]    = request.getParameterValues("PremGrid3");
//    String PayPlanType[]  = request.getParameterValues("PremGrid3");
//    String PayTimes[]      = request.getParameterValues("PremGrid4");
//    String StandPrem[]     = request.getParameterValues("PremGrid5");
   // String DuePrem[] = request.getParameterValues("PremGrid6");--应交金额
    String ActuPrem[]=request.getParameterValues("PremGrid7");  // --实交金额

    VData tVData = new VData();
  //  LJTempFeeSet tLJTempFeeSet=new LJTempFeeSet();  ；;  ；
    LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
    tLJTempFeeSchema.setOtherNo(ContNo);  //这里的otherno存的是合同号，（先前的是险种号）
  //  tLJTempFeeSchema.setOtherNoType("2");
    tLJTempFeeSchema.setConfFlag("0");
    tLJTempFeeSchema.setTempFeeType("3");  //不定期交费的类型为3-收交费
   // tLJTempFeeSet=  tLJTempFeeSchema.query();  
    LCPremSet tLCPremSet = new LCPremSet();
    LCPremSchema tLCPremSchema = null;
    
    int Count = Num.length; 		
    for (int i = 0; i < Count; i++)
    {
      tLCPremSchema = new LCPremSchema();
      tLCPremSchema.setDutyCode(DutyCode[i]);
      tLCPremSchema.setPayPlanCode(PayPlanCode[i]);
    //  tLCPremSchema.setStandPrem(StandPrem[i]);
      tLCPremSchema.setPolNo(PolNo[i]);   //yangh于2005-06-30添加 
      //杨红于20050714说明：把实交金额暂时放在tLCPremSchema变量中
      tLCPremSchema.setPrem(ActuPrem[i]);
      tLCPremSet.add(tLCPremSchema);      
    }      
    
    tVData.add(tLCPremSet);
    tVData.add(tLJTempFeeSchema);//杨红于2005-07-07说明不定期暂交费也可能多次交费！
    tVData.add(tGI);
    IndiNormalPayVerifyUI tIndiNormalPayVerifyUI = new IndiNormalPayVerifyUI();
    tIndiNormalPayVerifyUI.submitData(tVData,"VERIFY");
    if(tIndiNormalPayVerifyUI.mErrors.needDealError())
     {
       Content = " 个人正常交费失败，原因是: " + tIndiNormalPayVerifyUI.mErrors.getError(0).errorMessage;
       FlagStr = "Fail";
     }
    else
     {
      Content = "个人正常交费成功";
      FlagStr = "Succ";            
     }     
  }//判断页面有效区
%>
<HTML>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</HTML> 