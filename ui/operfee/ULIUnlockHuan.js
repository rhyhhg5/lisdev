var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}
function afterSubmit(FlagStr, content) 
{
  if (FlagStr == "Fail" ) 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  query();
}

//打印按钮对应操作
function save()
{
  if(LCPolGrid.mulLineCount == 0)
  {
    alert("没有需要缓交状态恢复的保单!");
    return false;
  }
  fm.action = "ULIUnlockHuanSave.jsp";
  fm.submit(); //提交
}

function easyQueryClick() 
{
  var strSql = "select a.contno ,b.appntname,a.PolApplyDate,a.paytodate, "
             + "(case when a.standbyflag1 = '1' then '保费缓交' else codename('stateflag',stateflag) end) contstate, "
             + "prem, (select name from laagent where agentcode = a.agentcode), "
             + "case when a.standbyflag1 = '1' then '1' else '0' end "
             + "from lcpol a,lcappnt b where a.mainpolno = a.polno and a.grpcontno = '00000000000000000000' "
             + "and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') "
             + "and a.stateflag != '0' and a.contno=b.contno " 
             + getWherePart('a.ContNo', 'ContNo','=')
             + getWherePart('b.AppntNo', 'AppntNo','=')
             + "order by a.contno with ur ";

  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(strSql, LCPolGrid);
  return true;
}

//Click事件，当点击“查询”图片时触发该函数
function query()
{ 
  var tContNo = trim(fm.ContNo.value);
  var tAppntNo = trim(fm.AppntNo.value);
  if(tContNo == "" && tAppntNo == "")
  {
    alert("客户号和保单号不能全为空！");
    return false;
  }
  if(!easyQueryClick())
  {
    return false;
  }
  if(LCPolGrid.mulLineCount == 0)
  {
    alert("没有查询到数据！");
    return false;
  }
  LCPolGrid.radioBoxSel(1);
  for(i = 0; i < LCPolGrid.mulLineCount; i++)
  {
    if(LCPolGrid.getRowColDataByName(i,"Huan")=="1")
    {
       LCPolGrid.radioBoxSel(i+1);
       break;
	}
  }
  selectOne();
}

function selectOne()
{
  fm.ContNo1.value = LCPolGrid.getRowColDataByName(LCPolGrid.getSelNo()-1,"ContNo");
}

//回车查询
function queryByKeyDown()
{
  if(event.keyCode == "13")
  {
	query();
  }
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}