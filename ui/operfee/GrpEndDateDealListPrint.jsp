<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
    
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  boolean errorFlag = false;
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "团险满期通知清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("/");
  System.out.println("filepath................"+filePath);
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
    
  String tSQL = request.getParameter("Sql");
  System.out.println("打印查询:"+tSQL);
  //设置表头
  String[][] tTitle = {{"管理机构", "营销部门", "业务员", "投保单位", "单位性质", "保单号", "期交保费", "投保日期", "责任到期日", "交至日期", "交费合计", "退费合计", "理赔金合计", "被保人总数"}};
  //表头的显示属性
  int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
    
  //数据的显示属性
  int []displayData = {0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) 
    errorFlag = true;
    if(createexcellist.setData(tSQL,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>
	