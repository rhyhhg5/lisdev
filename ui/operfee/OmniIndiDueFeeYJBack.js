//yangyalin

var showInfo;
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();

//查询报单信息
function queryCont()
{
  if(!checkContQuery())
  {
    return false;
  }
  
  var strSQL = "select distinct a.contNo,a.appntName,a.polApplyDate, a.payToDate, "
  				 + "	case a.AppFlag when '0' then '未签单' when '1' then '已签单' end, "
  				 + "	a.prem ,getUniteCode(a.AgentCode), b.name "
		       + "from LCCont a, LAAgent b "
           + "where 1=1 "
           + "    and a.agentCode = b.agentCode "
           + "    and a.contType = '1' "
		       + getWherePart( 'a.contNo ','ContNo', "like") 
		       + getWherePart( 'a.AppntNo ','AppntNo', "like") 
		       + "    and a.AppFlag='1' "
		       + "    and exists (select 1 from lcpol where contno=a.contno and riskcode in (select riskcode from lmriskapp where risktype4='4')) "
		       + "    and not exists "
		       + "      (select 1 from LCContState "
		       + "      where contNo = a.contNo and polNo in('000000', '00000000000000000000') "
		       + "        and state = '1'"
		       + "        and (endDate is null or endDate > current Date)) "
		       + "order by contNo ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, ContGrid); 
  
  if(ContGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }
}

//校验查询条件是否正确
function checkContQuery()
{
  if(fm.ContNo.value == "" && fm.AppntNo.value == "")
  {
    alert("客户号和保单号不能同时为空");
    return false;
  }
  
  return true;
}

//查询续期实收记录信息
function queryLJAPay()
{
  var row = ContGrid.getSelNo() - 1;  //被选中的行数
  var contNo = ContGrid.getRowColDataByName(row, "contNo");
  fm.tContNo.value = contNo;
  
  var sql   = "select  "
            + "(select insuaccname from lmrisktoacc where insuaccno=a.insuaccno) 账户类型, "
            + "duebaladate 应结算日期,"
            + "rundate 实际结算日期, "
            + "year(DueBalaDate - 1 days) 结算年度, "
            + "month(DueBalaDate - 1 days) 结算月度,"
            + "InsuAccBalaAfter 结算后账户余额,"
            + "nvl((select abs(sum(Money)) from LCInsureAccTrace where OtherType = '6' and MoneyType = 'MF' and OtherNo = a.SequenceNo and ContNo=a.ContNo),0) 管理费, "
            + "nvl((select abs(sum(Money)) from LCInsureAccTrace where OtherType = '6' and MoneyType = 'RP' and OtherNo = a.SequenceNo and ContNo=a.ContNo),0) 风险保费, "
            + "nvl((select sum(Money) from LCInsureAccTrace where OtherType = '6' and MoneyType = 'LX' and OtherNo = a.SequenceNo and ContNo=a.ContNo),0) 结息金, "
            + "sequenceno 月结序号 "
            + "from lcinsureaccbalance a  "
            + "where contno= '" + contNo + "'  "
            + "order by duebaladate desc  ";
    turnPage3.pageDivName = "divPage3";
	turnPage3.queryModal(sql, LJAPayGrid); 
  
  if(LJAPayGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }  
  showCodeName();
}

//查询投保人账户余额
function queryAppAcc(contNo)
{
  var sql = "  select accBala "
            + "from LCAppAcc "
            + "where customerNo = "
            + "   (select appntNo from LCCont where contNo = '" + contNo + "') ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.AccBala.value = rs[0][0];
  }
}

//实收保费转出提交
function cancelRecord()
{
  fm.SeqNo.value = LJAPayGrid.getRowColDataByName(LJAPayGrid.getSelNo() - 1, "SeqNo");
  if(!checkSubmit())
  {
    return false;
  }
 // if(!checkIsBack())
//  {
 //   return false;
 // }
  if(!checkContState())
  {
    return false;
  }
  if(!checkLastBack())
  {
    return false;
  }
  if(!checkShiShou())
  {
    return false;
  }
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
}
//本次万能月结之后若有万能续期实收,则不能进行万能月结回退
function checkShiShou()
{
    var selrow = ContGrid.getSelNo() - 1;  //被选中的行数
 	var selcontNo = ContGrid.getRowColDataByName(selrow, "contNo");
 	var SeqNo = fm.SeqNo.value;
	var sqlYJ="select sum(money) from lcinsureacctrace where "
			+"((makedate>(select max(makedate) from lcinsureacctrace where otherno='"+SeqNo+"' and contno='"+selcontNo+"')) "
			+" or (makedate=(select max(makedate) from lcinsureacctrace where otherno='"+SeqNo+"' and contno='"+selcontNo+"') and " 
			+" maketime>=(select max(maketime) from lcinsureacctrace where otherno='"+SeqNo+"' and contno='"+selcontNo+"'))) "
			+" and  othertype='2' and contno='"+selcontNo+"' group by otherno with ur";
	var yueJIE = easyExecSql(sqlYJ);
	if(!yueJIE){
		return true;
	}
	for(i=0;i<yueJIE.length;i++)
	{
	if(yueJIE[i][0]!=0&&yueJIE[i][0]!=null&&yueJIE[i][0]!=""&&yueJIE[i][0]!="null")
	{
	  alert("此单月结之后有万能续期实收，不能进行万能月结回退。");
	  return false;
	}
	}
	return true;
}




//只能从最后一次万能月结依次向前回退
function checkLastBack()
{
     var selcontNo = fm.tContNo.value;
     var sqlNum=" select 1 from lcinsureaccbalance a where contno='"+selcontNo+"' and sequenceno='"+fm.SeqNo.value+"' and exists (select 1 from lcinsureaccbalance where duebaladate>a.duebaladate and contno='"+selcontNo+"')";
     if(easyQueryVer3(sqlNum))
	{
       alert("只能从最后一次万能月结依次向前回退,此次月结不是最后一次。");
       return false;
     }
     
     return true;
}



//保单失效或者终止状态不能操作万能月结回退
function checkContState()
{
   var selrow = ContGrid.getSelNo() - 1;  //被选中的行数
   var selcontNo = ContGrid.getRowColDataByName(selrow, "contNo");
   var sqlContState="select 1 from lccont where contno='"+selcontNo+"' and stateflag in ('2','3')";
	if(easyQueryVer3(sqlContState))
	{
	  alert("此单已失效或者终止，不能操作万能月结回退。");
	  return false;
	}
	return true;
}




//少儿险实收保费转出只能转出一次
//090616添加转出之前如果有保全则增加confirm提示
function checkIsBack()
{
	var selrow = ContGrid.getSelNo() - 1;  //被选中的行数
 	var selcontNo = ContGrid.getRowColDataByName(selrow, "contNo");
	var backsql = "select 1 from ljspayb a where dealstate='6' and otherno='"+selcontNo+"' "
	            + "and (select count(1) from ljspaypersonb where getnoticeno = a.getnoticeno and riskcode = '320106') >= 1 with ur";
	if(easyQueryVer3(backsql))
	{
	alert("此单已经做过实收保费转出,请勿重复操作");
	return false;
	}
	var payselrow = LJAPayGrid.getSelNo() - 1;  //实收被选中的行数
	var getnoticeno = LJAPayGrid.getRowColDataByName(payselrow, "getNoticeNo");
	var bqsql = "select distinct edorname from lmedoritem where edorcode in (select distinct edortype From lpedoritem a where contno ='"+selcontNo+"' " 
                   +  "and makedate>(select makedate from ljspayb where getnoticeno ='"+getnoticeno+"' ))";
    var bqinfo = easyExecSql(bqsql);
	if(bqinfo!=null)
	{
		var bqcontent="";
		for(i=0;i<bqinfo.length;i++)
		{
		bqcontent+=bqinfo[i][0]+",";
		}
	  if(confirm("该保单在该次收费后做过保全项目有:"+bqcontent+"实收保费转出有可能覆盖该操作,是否依然要继续?"))
	  {
	  return true;	
	  }
	  else
	  {
	  return false;	
	  }
	}  
    return true;
}
  
  
//检验提交数据的完整性
function checkSubmit()
{
  if(LJAPayGrid.mulLineCount == 0)
  {
    alert("没有需要回退的实收记录");
    return false;
  }
  if(LJAPayGrid.getSelNo() < 1)
  {
    alert("请选择实收记录进行回退");
    return false;
  }
  return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	  queryCont(); 
	  initLJAPayGrid();
	//  fm.GetNoticeNo.value = "";
	  fm.tContNo.value = "";
  }
}