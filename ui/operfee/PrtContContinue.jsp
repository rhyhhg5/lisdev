<%
//程序名称：PrtContContinue.jsp
//程序功能：个险保单继续率统计,查询保单继续率汇总,下载清单。
//创建日期：2011-4-18 
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
	//System.out.println(curDate);
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html> 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="PrtContContinue.js"></SCRIPT>
</head>
<body  onload="initForm();initElementtype();" >
	<form method=post name=fm action="./PrtContContinueSave.jsp" >
		<Table  class= common>
	    	<tr>
	    		<td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName elementtype=nacessary>
				</td>
				<td class= title>保单类型</td>
				<td class=input>
					<Input class= "codeno" name=ContType verify="保单类型|NOTNULL" CodeData="0|^0|全部^1|个险^2|银保^3|互动" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);" ><Input class=codename name=ContTypeName readonly elementtype=nacessary >
				</td>
				<td class= title>险种类型</td>
				<td class=input>
					<Input class= "codeno" name=RiskType verify="险种类型|NOTNULL" CodeData="0|^1|传统险^2|万能险" ondblclick="return showCodeListEx('RiskType',[this,RiskTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('RiskType',[this,RiskTypeName],[0,1]);" ><Input class=codename name=RiskTypeName readonly elementtype=nacessary>
				</td>
			</tr>
	    	<tr>
	    		<td class= title>销售渠道</td>
				<td class=input>
					<Input class= "codeno" name=SaleChnl verify="销售渠道|NOTNULL" CodeData="0|^00|全部^01|个险直销^04|银行代理^10|个险中介^13|银代直销^14|互动直销^15|互动中介" ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1]);" ><Input class=codename name=SaleChnlName readonly elementtype=nacessary>
				</td>
				<td class= title>继续率类型</td>
				<td class=input>
					<Input class= "codeno" name=ContinueType verify="继续率类型|NOTNULL" CodeData="0|^1|13个月^2|25个月" ondblclick="return showCodeListEx('ContinueType',[this,ContinueTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('ContinueType',[this,ContinueTypeName],[0,1]);" ><Input class=codename name=ContinueTypeName readonly elementtype=nacessary>
				</td>
	    	    <td class= title>保单服务状态</td>
				<td class=input>
					<Input class= "codeno" name=Orphans verify="保单服务状态|NOTNULL" CodeData="0|^0|全部^1|在职单^2|孤儿单" ondblclick="return showCodeListEx('Orphans',[this,OrphansName],[0,1]);" onkeyup="return showCodeListKeyEx('Orphans',[this,OrphansName],[0,1]);" value = "0"><Input class=codename name=OrphansName elementtype=nacessary readonly  >
				</td>
	    	</tr>
	    	<tr>
				<td class= title>生效开始日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=StartDate elementtype=nacessary verify="生效开始日期|NOTNULL">
				</td>
				<td class= title>生效结束日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=EndDate elementtype=nacessary verify="生效结束日期|NOTNULL">
				</td>
			</tr>
		</Table>
		<Table>
			<tr>
				<td><input value="下载报表" class = cssButton TYPE=button onclick="download();"></td>
			</tr>
		</Table>
		<div id="divInfo"></div>
    	<input type=hidden id="arrset" name="arrset">
    	<input type=hidden id="strsql" name="strsql">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script language="JavaScript">

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";

//表单初始化
function initForm()
{
	initInputBox();
}

function initInputBox()
{
try
	{
		fm.all('ManageCom').value = manageCom;
		fm.all('ContType').value = "0";
		fm.all('RiskType').value = "1";
		fm.all('SaleChnl').value = "00";
		fm.all('ContinueType').value = "1";
		
		var sql = "select current date - 15 month,current date - 12 month from dual ";
		var rs = easyExecSql(sql);
		fm.all('StartDate').value = rs[0][0];
		fm.all('EndDate').value = rs[0][1];
		
		showAllCodeName();
	}
	catch(ex)
	{
		alert("initInputBox函数中发生异常:初始化界面错误!");
	}      
}
</script>
