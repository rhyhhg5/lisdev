<%
//程序名称：PAchieveRateNew.jsp
//程序功能：个险续期达成率统计（新）
//创建日期：2016-3-4 
//创建人  ：LC
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
	//System.out.println(curDate);
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/CurrentTime.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html> 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="PAchieveRateNew.js"></SCRIPT>
</head>
<body  onload="initForm();initElementtype();" >
	<form method=post name=fm action="./PAchieveRateNewSave.jsp" >
		<Table  class= common>
	    	<tr>
	    		<td class= title>机构类型</td>
				<td class=input>
					<Input class= "codeno" name=ManageCom verify="机构类型|NOTNULL" CodeData="0|^0|二级机构^1|三级机构" ondblclick="return showCodeListEx('ManageCom',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ManageCom',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename name=ManageComName readonly elementtype=nacessary >
				</td>
				<td class= title>保单类型</td>
				<td class=input>
					<Input class= "codeno" name=SaleChnlType verify="保单类型|NOTNULL" CodeData="0|^0|全部^1|个险^2|银保^3|互动" ondblclick="return showCodeListEx('SaleChnlType',[this,SaleChnlTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnlType',[this,SaleChnlTypeName],[0,1]);" value = "0"><Input class=codename name=SaleChnlTypeName elementtype=nacessary readonly  >
				</td>
				<td class= title>险种类型</td>
				<td class=input>
					<Input class= "codeno" name=RiskType verify="险种类型|NOTNULL" CodeData="0|^1|传统险^2|万能险^3|全部" ondblclick="return showCodeListEx('RiskType',[this,RiskTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('RiskType',[this,RiskTypeName],[0,1],null,null,null,1);" value = "1"><Input class=codename name=RiskTypeName elementtype=nacessary readonly >
				</td>
			</tr>
	    	<tr>
	    	    <td class= title>销售渠道</td>
				<td class=input>
					<Input class= "codeno" name=SaleChnl verify="销售渠道|NOTNULL" CodeData="0|^00|全部^01|个险直销^04|银行代理^10|个险中介^13|银代直销^14|互动直销^15|互动中介" ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" value = "00"><Input class=codename name=SaleChnlName elementtype=nacessary readonly  >
				</td>
				<td class= title>保单服务状态</td>
				<td class=input>
					<Input class= "codeno" name=Orphans verify="保单服务状态|NOTNULL" CodeData="0|^0|全部^1|在职单^2|孤儿单" ondblclick="return showCodeListEx('Orphans',[this,OrphansName],[0,1]);" onkeyup="return showCodeListKeyEx('Orphans',[this,OrphansName],[0,1]);" value = "0"><Input class=codename name=OrphansName elementtype=nacessary readonly  >
				</td>
				<td class= title>缴次</td>
				<td class=input>
					<Input class= "codeno" name=PayCount verify="缴次|NOTNULL" CodeData="0|^0|全部^2|两次^3|三次^4|四次及以上" ondblclick="return showCodeListEx('PayCount',[this,PayCountName],[0,1]);" onkeyup="return showCodeListKeyEx('PayCount',[this,PayCountName],[0,1]);" value = "0"><Input class=codename name=PayCountName elementtype=nacessary readonly  >
				</td>
	    	</tr>
	    	<tr>
				<td class= title>应缴开始日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=StartDate elementtype=nacessary verify="应缴开始日期|NOTNULL">
				</td>
				<td class= title>应缴截止日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=EndDate elementtype=nacessary verify="应缴开始日期|NOTNULL">
				</td>
			</tr>
			<tr>
				<td class= title>核销开始日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=ActuStartDate>
				</td>
				<td class= title>核销截止日期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=ActuEndDate>
				</td>
			</tr>		
		</Table>
		<Table class= common>
		   <tr>
			<td>
				<input value="下载报表" class = cssButton TYPE=button onclick="easyPrint();">
			</td>
			</tr>
			</br>
			<tr>
				<td class= title >
				   <font color="red">*实收保费不含一般复效保费</font>
				</td>
		    </tr>
		    <tr>
				<td class= title >
				<font color="red">*缓缴保费指超过应缴日60天未收保费金额</font>
				</td>
			</tr>
		</Table>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


<script language="JavaScript">

var ManageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";

//表单初始化
function initForm()
{
	initInputBox();
}
function initInputBox()
{
  try
	{
	  fm.all('ManageCom').value = "0";
	  var sql = "select current date - 3 month,current date from dual ";
      var rs = easyExecSql(sql);
      fm.all('StartDate').value = rs[0][0];
      fm.all('EndDate').value = rs[0][1];

	  showAllCodeName();
	}
	catch(ex)
	{
		alert("PAchieveRateInit.jsp-->initInputBox函数中发生异常:初始化界面错误!");
	}      
}
</script>
