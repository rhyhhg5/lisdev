//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var turnPage5 = new turnPageClass();
var expression ="" ;
var expression1 ="" ;
var mSql ="";

function PersonSingle()
{
    if(beforeSubmit())
    {  
    	if (!checkValidateDate()) return false;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      	  	
      fm.submit();
    }	
}

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,batchNo)
{
	try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
   
    //content = "满期给付抽档操作完成，不能抽档保单请下载清单";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
   //resetForm(batchNo);
}



//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
     
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function commonPayMulti()
{
  if(!checkValidateDate()) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
	fm.payMode.value="Q";
  fm.submit();	
}

function cashPayMulti()
{
	if(!checkValidateDate()) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
	fm.payMode.value="1";
  fm.submit();	
}


function CheckDate()
{
  if(!isDate(fm.all('StartDate').value)||!isDate(fm.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fm.all('StartDate').value,fm.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

// 查询按钮
/*********************************************************************
 *  查询合同号
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	//initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	
	//得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return fasle;
	}
	else
		{
			expression1 = " and a.ManageCom like '%"+fm.all("ManageCom").value +"%'";
		}
	managecom = fm.ManageCom.value;
	
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}	 
	var strSQL =" select a.grpcontno,a.grpname,a.peoples2,(select amnt from lcgrppol where grppolno = b.grppolno),a.cinvalidate,getUniteCode(a.agentcode)"
						 +" ,(select case when edorvalue ='2' then'团体统一给付' when edorvalue ='3' then'个人给付' end from LPEdorEspecialData where edorno =a.grpcontno and detailtype='QUERYTYPE'  ) "
						 +" ,(select edorvalue from LPEdorEspecialData where edorno =a.grpcontno and detailtype='QUERYTYPE'  ) "
						 +" From LCGrpCont a,LCGRPpol b , LMRiskApp c, LMRisk d where 1 =1 "
						 +" and a.grpcontno = b.grpcontno "
						 +" and b.riskcode = c.riskcode 	"
						 +"	and  c.riskcode = d.riskcode and D.getflag ='Y'	"
						 +" and RiskType3 !='7' and a.stateflag in('1','3')" 
						 //+" and exists (select 1 from lccont e where  a.grpcontno = e.grpcontno and not exists(select 1 from ljsgetdraw where e.contno = contno))    "
						 +" and exists (select 1 from ljsgetdraw where a.grpcontno = grpcontno) "
					 //  +" and exists (select 1 from LPEdorEspecialData where a.grpcontno = edorno and Edortype ='TJ') "
						 +" and a.cinvalidate <='"+fm.all("EndDate").value+"'"
						 + getWherePart( 'a.GrpContNo', 'GrpContNo' )    				 
						 +"	with ur ";	
//	fm.QuerySql.value = strSQL ;
	fm.strsql.value = strSQL ;
	fm.tGrpContNo.value = fm.GrpContNo.value;
	mSql = strSQL;
	turnPage4.queryModal(strSQL, LCGrpGrid);
	//initPolGrid();
	contCurrentTime=CurrentTime; 
	contSubTime=SubTime; 
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	if(LCGrpGrid.mulLineCount == 0)
	{
	    alert("没有符合条件的保单信息!");
	}
	BQQueryClick(fm.GrpContNo.value);
	queryImportData(fm.GrpContNo.value);
}

/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getpoldetail()
{
    var arrReturn = new Array();
	  var tSel = IndiContGrid.getSelNo();	
	  if( tSel == 0 || tSel == null )
	  {
		   alert( "请先选择一条记录，再点击返回按钮。" );
	  }	
	  else
	  {
		   getPolInfo();
    }  	                                          
                         	               	
}
/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolInfo()
{
	//modify by fuxin 2008-5-5 11:21:17 少儿险险种和普通险种分开显示。
	var Condition ='';
	if(fm.queryType.value=='2')
	{
		Condition = " and a.riskcode not in ('320106','120706') " ;
	}
	if(fm.queryType.value=='3')
	{
		Condition = " and a.riskcode  in ('320106','120706') " ;
	}
	
	  var tRow = IndiContGrid.getSelNo() - 1;	        
		var tContNo=IndiContGrid.getRowColData(tRow,1);  
		
		var strSQL = "select a.RiskSeqNo,a.InsuredName, (select riskname  from lmrisk where riskcode=a.riskcode) ,a.RiskCode, (select codeName from LDCode where codeType = 'payintv' and code = char(a.PayIntv)), "
			+ "	a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate ,a.enddate "  
			+ " from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
			+ " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
			+ "  and a.contno='" + tContNo + "' "
			+ "  and a.appflag='1' "
			+ Condition
			+ " group by a.polNo,a.RiskSeqNo,a.InsuredName,a.RiskCode, 	a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate ,a.enddate,payintv  "
			+ " order by a.polNo "
			+ " with ur "			
			;
    	turnPage3.queryModal(strSQL, PolGrid); 
}


/*********************************************************************
 *  重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为resetForm()
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm(batchNo)
{
  try
  {   
//  afterContQuery();
	  afterJisPayQuery(batchNo);
	  
  }
  catch(re)
  {
  	alert("在LJSPayPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 



/*********************************************************************
 *  校验日期范围
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkValidateDate()
{
	//校验录入的起始日期和终止日期必须在今天之前，否则不予提交
	var startDate=fm.StartDate.value;
	var endDate=fm.EndDate.value;
	if(startDate==''||endDate=='')
	{
		alert("请录入查询日期范围！");
		return false;
	}
	if(compareDate(startDate,endDate)==1)
	{
		alert("起始日不能晚于截止日!");
		return false;
	}
	return true;
}

	

//催收完成后查询催收纪录
function afterJisPayQuery(batchNo)
{
	// 初始化表格
	//initLjsGetGrid();
	//initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}		  
	if(fm.all("QueryType").value =='2')
	{
	  var strSQL ="	select b.serialno,otherno,grpcontno,grpname,sumgetmoney,curgettodate,getUniteCode(b.agentcode),codename('paymode',paymode) "
							 +"	,(select case when dealstate ='0' then '待给付' when dealstate ='1' then '已给付' end from ljsget where b.getnoticeno=getnoticeno) "
							 +",'未打印',b.getnoticeno "
							 +" From ljsgetdraw a,ljsget b  where 1=1 "
							 +"	and a.getnoticeno = b.getnoticeno	"
							 +" and b.serialno ='"+batchNo+"'"
							 +"	group by b.serialno,otherno,grpcontno,grpname,sumgetmoney,curgettodate,b.agentcode,b.getnoticeno,paymode,dealstate,'打印状态' "
							 ;
  }
  else
  {
  	var strSQL ="	select b.serialno,otherno,contno,grpname,sumgetmoney,curgettodate,getUniteCode(b.agentcode),codename('paymode',paymode) "
			 +"	,(select case when dealstate ='0' then '待给付' when dealstate ='1' then '已给付' end from ljsget where b.getnoticeno=getnoticeno) "
			 +" ,'未打印',b.getnoticeno "
			 +" From ljsgetdraw a,ljsget b  where 1=1 "
			 +"	and a.getnoticeno = b.getnoticeno	"
			 +" and b.serialno ='"+batchNo+"'"
			 //+"	group by b.serialno,otherno,grpcontno,grpname,sumgetmoney,curgettodate,b.agentcode,paymode,dealstate,'打印状态' "
			 ;
  }

	
	turnPage2.queryModal(strSQL, IndiContGrid); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
}


function getCheckdetail()
{
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length < 1)
	{
		return false;	
	}
	fm.all("tGrpContNo").value=LjsGetGrid.getRowColDataByName(tChked,'GrpContNo');
}

function PrintList()
{
		if(LJAGetGrid.mulLineCount==0)
		{
			alert("没有待给付记录，请先查询！");
			return false ;
		}
	if (LJAGetGrid.getSelNo()==0)
	{
		alert("请选择需要打印的记录！");
		return false ;
	}
		
		var tRow = LJAGetGrid.getSelNo() - 1;	        
		var otherNo = LJAGetGrid.getRowColData(tRow,1); 
		window.open("../operfee/GrpExpirBenefitQuerySave.jsp?otherNo="+otherNo);
}

	function a(tType)
	{
		if(tType==2)
		{
			document.all("x").style.display = "none";
			document.all("y").style.display = "";
		}
		if(tType==3)
		{
			document.all("x").style.display =""
			document.all("y").style.display ="none"
		}
	}
	

function getType()
{
	var sql = " select edorvalue from LPEdorEspecialData where edorno = '"+fm.all("GrpContNo").value+"' and Edortype ='TJ' and detailtype ='QUERYTYPE'";
	var result = easyExecSql(sql);
	if(result == null || result == "")
	{
		return false ;
	}
	else
	{              
		a(result[0][0]);
		fm.QueryType.value = result[0][0];    
	}
}

//磁盘导入 
function importInsured()
{
	if(fm.all("tGrpContNo").value == null || fm.all("tGrpContNo").value == "")
	{
		alert("请先查询保单信息，再做导入！");	
		return false ;
	}
	var sql= "select WorkNo from LGWork where ContNo = '" + fm.all("tGrpContNo").value+ "' " 
	       + "and InnerSource = 'LPJ' and AcceptWayNo = '8' order by MakeDate desc with ur ";
	var rs = easyExecSql(sql);
	if(rs)
	{
	  afterImportInsured(rs[0][0]);
	}
	else
	{
	  fm.action ="CreateGrpWorkNo.jsp";
	  fm.submit();
	}
}

function afterImportInsured(edorno)
{
  fm.all('EdorNo').value = edorno; 
  var url = "../bq/BqDiskImportMain.jsp?EdorNo=" + edorno + 
	          "&EdorType=LP" +
	          "&GrpContNo=" + fm.all("tGrpContNo").value;
  var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
  window.open(url, "理赔金帐户导入", param);
}

//保存
function save()
{
  var sql= "select WorkNo from LGWork where ContNo = '" + fm.all("tGrpContNo").value+ "' " 
	       + "and InnerSource = 'LPJ' and AcceptWayNo = '8'  order by MakeDate desc with ur ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.all('EdorNo').value = rs[0][0]; 
  }
  if (LCInsuredGrid.mulLineCount == 0)
  {
    alert("没有有效的导入数据！");
    return false;
  }
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "GEdorTypeLPSave.jsp"
  fm.submit();
}

function afterSave(FlagStr, content)
{
  try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
 // easyQueryClick();
  queryImportData(fm.all('tGrpContNo').value);
}
function queryImportData(aGrpContNo)
{
  var sql= "select WorkNo from LGWork where ContNo = '" + aGrpContNo+ "' " 
	       + "and InnerSource = 'LPJ' and AcceptWayNo = '8' order by MakeDate desc with ur ";
  var rs = easyExecSql(sql);
  if(rs)
  {
	  fm.all('EdorNo').value = rs[0][0];
  }	
  var sql = "select GrpContNo, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + aGrpContNo + "' " +
	             "and State = '1' " +
	             "and Edorno = '" + fm.all('EdorNo').value + "' "
	             "order by Int(SerialNo) ";
  turnPage3.pageDivName = "divPage1";
  turnPage3.queryModal(sql, LCInsuredGrid); 
}


function BQQueryClick(GrpContNo)
{
	 				strSQL = "  select distinct a.EdorAcceptNo, a.otherno, '团单客户号', a.EdorAppName, "
		         + "    (select sum(getMoney) "
		         + "    from LPGrpEdorMain c "
		         + "    where c.edorAcceptNo =  a.edorAcceptNo), "
		         + "    (select userName "
		         + "    from LDUser "
		         + "    where LDUser.userCode =b.acceptorno), "
		         + "    (select userName "
		         + "    from LDUser d "
		         + "    where d.userCode = b.Operator), "
		         + "    (select CodeName from LDCode where CodeType = 'taskstatusno' and Code = b.statusNo), "
		         + "    case edorState"
    		     + "        when '0' then '保全确认' "
    		     + "        when '2' then '申请确认' "
    		     + "        when '9' then '待交费' "
    		     + "        else '正在申请' "
    		     + "    end "
    		     + "from LPEdorApp a, LGWork b "
    		     + "where a.edorAcceptNo = b.workNo "
    		     + "    and otherNoType = '2' and a.EdorState ='0'  and innersource ='LPJOver'"
                   //+ getWherePart('b.contno','GrpContNo')
             + " and b.contno ='"+GrpContNo+"'"      
             + " order by edorAcceptNo desc ";
			   turnPage2.queryModal(strSQL,PolGrid);
}

//显示被保人清单
function showInsuredList()
{
	if(fm.GrpContNo.value == null || fm.GrpContNo.value =="")
	{
		alert("请录入保单号查询后，再打印变更清单！");
		return false;
	}
	var tSel = PolGrid.getSelNo();
	
	if(tSel==0)
	{
		alert("变更列表没有信息或没有选中需要打印的记录！");
		return false ;
	}
	
	var sql = "select EdorType from LPGrpEdorItem " +
	          "where EdorNo = '" + PolGrid. getRowColData(tSel-1,1) + "' ";
	var arrResult = easyExecSql(sql, 1, 0);
	if (!arrResult)
	{
		alert("没有找到保全信息！");
		return false;
	}
	var hasLP = false;

	for (var i = 0; i < arrResult.length; i++)
	{

		if(arrResult[i][0] == "LP")
		{
		  hasLP = true;
		}
	}
	
	if(hasLP == true)
	{
		window.open("../bq/GInsuredListLPMain.jsp?edorType=LP&edorAcceptNo=" + fm.EdorNo.value);
	}

}


function GPBqQueryClick()
{
	
	var tSel = PolGrid.getSelNo();
    fm.all('EdorNo').value = PolGrid. getRowColData(tSel-1,1);
   // fm.all('GrpContNo').value = PolGrid. getRowColData(tSel-1,2);
}

function printNotice()
{
			if(LJAGetGrid.mulLineCount==0)
		{
			alert("没有待给付记录，请先查询！");
			return false ;
		}
	
	if (LJAGetGrid.getSelNo()==0)
	{
		alert("请选择需要打印的记录！");
		return false ;
	}
	var tRow = LCGrpGrid.getSelNo() - 1;	        
	var grpContNo = LCGrpGrid.getRowColData(tRow,1); 
	
	var xRow = LJAGetGrid.getSelNo() - 1;	        
	var taskNo = LJAGetGrid.getRowColData(xRow,1); 
	
	open("./GrpExpirBenefitInsForPrt.jsp?grpContNo="+grpContNo+"&taskNo="+taskNo);
	return false ;
}

function getDetail()
{
		var tRow = LJAGetGrid.getSelNo() - 1;	        
		var taskNo = LJAGetGrid.getRowColData(tRow,1); 
		
		var strSQL =" select b.getnoticeno,(select insuredname from lcpol where b.contno = contno and b.polno=polno) "
             +" ,(select codename('sex',insuredsex) from lcpol where b.contno = contno and b.polno=polno) "
             +" ,(select insuredbirthday from lcpol where b.contno = contno and b.polno=polno) "
             +" ,(select occupationtype from lcpol where b.contno = contno and b.polno=polno ) "
             +" ,(select codename('idtype',idtype) from lcinsured where b.contno = contno ) " 
             +" ,(select idno from lcinsured where b.contno = contno )  "
             +" ,(select contplancode from lcpol where b.contno = contno and b.polno = polno ) "
             +" ,(select bankname from ldbank where b.bankcode = bankcode),b.accname,b.bankaccno "
						 +" ,(case when dealstate ='0' then '待给付' when dealstate ='1' then '已付费' when dealstate ='2' then '已撤销' end ),b.GrpContNo "	
             +" From ljaget a,ljsgetdraw b,ljsget c where a.actugetno = b.getnoticeno and a.actugetno = c.getnoticeno  and a.sumgetmoney >0  "
             +" and a.otherno='"+taskNo+"' " 
             +" and a.othernotype ='21' group by b.getnoticeno,contno,polno,b.accname,b.bankaccno,b.bankcode,grpcontno,actugetno,dealstate   "
             +" with ur	"
             ;

	fm.strsql.value = strSQL ;
	fm.tGrpContNo.value = fm.GrpContNo.value;
	mSql = strSQL;
	turnPage1.queryModal(strSQL, LjsGetGrid);
}

function getLJAGetDetail()
{
	var tRow = LCGrpGrid.getSelNo()-1;	
	var grpContNo = LCGrpGrid.getRowColDataByName(tRow,'GrpContNo');
			var strSQL =" select a.otherno,codename('paymode',a.paymode),a.managecom,sum(b.getmoney) "
                 +" ,(case when dealstate ='0' then '待给付' when dealstate ='1' then '已付费' when dealstate ='2' then '已撤销' end ) "
                 +" from ljaget a ,ljsgetdraw b ,ljsget c where 1=1 "
                 +" and  a.actugetno = b.getnoticeno  and a.actugetno = c.getnoticeno and grpcontno ='"+grpContNo+"' and a.othernotype ='21' and a.sumgetmoney > 0"
                 +" group by a.otherno,a.paymode,a.managecom,dealstate fetch first 3000 rows only "
								 ;
	
	turnPage5.queryModal(strSQL, LJAGetGrid);
}

//给付任务撤销
function getTaskCansel()
{
	if (LCGrpGrid.mulLineCount == 0)
	{
		alert("没有保单数据！");
		return false;
	}
	if (LJAGetGrid.getSelNo()==0)
	{
		alert("请选择需要撤销的记录！");
		return false ;
	} 
	

	var tRow = LJAGetGrid.getSelNo()-1;	
	
	
	var xRow = LCGrpGrid.getSelNo()-1;	
	var grpContNo = LCGrpGrid.getRowColDataByName(xRow,'GrpContNo');
	
	var dealType =LCGrpGrid.getRowColData(xRow,8);

	var batchNo = LJAGetGrid.getRowColDataByName(tRow,'batchNo');

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
	fm.action = "GrpBenefitCansel.jsp?grpcontno="+grpContNo+"&dealType="+dealType+"&batchNo="+batchNo;
	fm.submit();
}