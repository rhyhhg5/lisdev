<%
//程序名称：IndiDueFeeBackInit.jsp
//程序功能：
//创建日期：2006-09-22
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	  // 保单查询条件  
  }
  catch(ex)
  {
    alert("在OmniIndiDueFeeBackinit.jsp-->InitInpBox函数中发生异常:初始化界面错误!" + ex.message);
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initContGrid();
    initLJAPayGrid();
  }
  catch(re)
  {
    alert("OmniIndiDueFeeBackinit.jsp-->InitForm函数中发生异常:初始化界面错误!" + re.message);
  }
}

// 保单信息列表的初始化
function initContGrid()
{                               
  var iArray = new Array();
    
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";         		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="contNo";
    
    iArray[2]=new Array();
    iArray[2][0]="投保人";         		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="appntNo";
    
    iArray[3]=new Array();
    iArray[3][0]="投保日期";         		//列名
    iArray[3][1]="70px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="applyDate";
    
    iArray[4]=new Array();
    iArray[4][0]="交至日期";         		//列名
    iArray[4][1]="70px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="payToDate";
    
    iArray[5]=new Array();
    iArray[5][0]="保单状态";         		//列名
    iArray[5][1]="50px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="polSate";
    
    iArray[6]=new Array();
    iArray[6][0]="期交保费";         		//列名
    iArray[6][1]="50px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="prem"; 
    
    iArray[7]=new Array();
    iArray[7][0]="业务员";         		//列名
    iArray[7][1]="70px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="agentCode";
    
    iArray[8]=new Array();
    iArray[8][0]="业务员";         		//列名
    iArray[8][1]="70px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="agentName";
    
    ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
    //这些属性必须在loadMulLine前
    ContGrid.mulLineCount = 0;   
    ContGrid.displayTitle = 1;
    ContGrid.locked = 1;
    ContGrid.canSel = 1;
    ContGrid.hiddenPlus = 1;
    ContGrid.hiddenSubtraction = 1;
    ContGrid.loadMulLine(iArray);  
    ContGrid.selBoxEventFuncName ="queryLJAPay";      
  }
  catch(ex)
  {
    alert(ex);
  }
}

function initLJAPayGrid()
{                               
  var iArray = new Array();
    
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            		//列最大值
    iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="缴费记录号";          		//列名
    iArray[1][1]="90px";      	      		//列宽
    iArray[1][2]=20;            			//列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[1][21]="getNoticeNo";
    
    iArray[2]=new Array();
    iArray[2][0]="应缴时间";   		//列名
    iArray[2][1]="90px";            		//列宽
    iArray[2][2]=100;            		//列最大值
    iArray[2][3]=0;              		//是否允许输入,1表示允许，
    iArray[2][21]="lastPayToDate";
    
    iArray[3]=new Array();
    iArray[3][0]="应缴保费";		//列名
    iArray[3][1]="100px";            		//列宽
    iArray[3][2]=60;            		//列最大值
    iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="sumDuePayMoney"; 
    
    iArray[4]=new Array();
    iArray[4][0]="实缴保费";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            		//列最大值
    iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="sumActuPayMoney";
    
    iArray[5]=new Array();
    iArray[5][0]="交至日期";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=200;            	        //列最大值
    iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="payToDate";
    
    iArray[6]=new Array();
    iArray[6][0]="抽档时间";         		//列名
    iArray[6][1]="100px";            		//列宽
    iArray[6][2]=200;            	        //列最大值
    iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="dufeeDate";
    
    iArray[7]=new Array();
    iArray[7][0]="到帐时间";         		//列名
    iArray[7][1]="90px";            		//列宽
    iArray[7][2]=200;            	        //列最大值
    iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="enterAccDate";
    
    iArray[8]=new Array();
    iArray[8][0]="处理状态编码";         		//列名
    iArray[8][1]="90px";            		//列宽
    iArray[8][2]=200;            	        //列最大值
    iArray[8][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="dealState";
    
    iArray[9]=new Array();
    iArray[9][0]="处理状态";         		//列名
    iArray[9][1]="90px";            		//列宽
    iArray[9][2]=200;            	        //列最大值
    iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    iArray[9][21]="dealStateName";
    
    LJAPayGrid = new MulLineEnter( "fm" , "LJAPayGrid"); 
    //这些属性必须在loadMulLine前
    LJAPayGrid.mulLineCount =0;   
    LJAPayGrid.displayTitle = 1;
    LJAPayGrid.hiddenPlus = 1;
    LJAPayGrid.hiddenSubtraction = 1;     
    LJAPayGrid.locked = 1;
    LJAPayGrid.canSel = 1;
    LJAPayGrid.loadMulLine(iArray);  

  }
  catch(ex)
  {
    alert(ex);
  }
}

</script>