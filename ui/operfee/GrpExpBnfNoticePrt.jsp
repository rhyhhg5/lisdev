<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： GrpExpBnfNoticePrt.jsp
//程序功能： 团险给付单个打印
//创建日期：2008-11-11
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
  System.out.println("start");
  boolean operFlag=true;
  String Content = "打印失败！";
  String GrpContNo[] = request.getParameterValues("LjsGetGrid1");
  String tChk[] = request.getParameterValues("InpLjsGetGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  
  String tOutXmlFile  = application.getRealPath("")+"\\";
  System.out.println("tOutXmlFileis :"+tOutXmlFile); 
  LCGrpContDB tLCGrpContDB = new LCGrpContDB();
  int grpContCount = GrpContNo.length;  
  for(int i = 0; i < grpContCount; i++)
  {
    if(tChk[i].equals("1"))
    {
      System.out.println("存储保单：" + GrpContNo[i]);
      tLCGrpContDB.setGrpContNo(GrpContNo[i]);
    }				
  }	
  CErrors mErrors = new CErrors();
  GlobalInput tG = new GlobalInput();      
  tG = (GlobalInput)session.getValue("GI");
  
  LDSysVarSchema tLDSysVarSechma =new LDSysVarSchema();
  tLDSysVarSechma.setSysVarValue(tOutXmlFile);
	
  VData tVData = new VData();
  tVData.addElement(tLDSysVarSechma);
  tVData.addElement(tG);
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
  if(tLCGrpContDB.getInfo())
  {
    tLCGrpContSchema = tLCGrpContDB.getSchema();
  }
  tVData.addElement(tLCGrpContSchema);
  GrpExpirBenefitPrintBL tGrpExpirBenefitPrintBL = new GrpExpirBenefitPrintBL();
  if(!tGrpExpirBenefitPrintBL.submitData(tVData,"INSERT"))
  {
    operFlag = false;
  }
  String code = "bq002";
  String PrtSeqSql = "select distinct prtseq from LOPRTManager where code = '"+code+"' and OtherNo='"+tLCGrpContSchema.getGrpContNo()+"'";
  String PrtSeq = new ExeSQL().getOneValue(PrtSeqSql);
%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
		parent.fraInterface.printPDFNotice("0"+"<%=code%>","<%=PrtSeq%>");
	}
	else
	{
		alert("<%=Content%>");
	}
</script>
</html>