 <html> 
<%
//程序名称：IndDueFeeInput.jsp
//程序功能：集体保费催收，实现数据从保费项表到应收集体表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="IndiDueFeeBatchInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="IndiDueFeeBatchInputInit.jsp"%>
</head>
<%
	
        GlobalInput tGI = new GlobalInput();
	    tGI = (GlobalInput)session.getValue("GI");
	   
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);               	               	
      
%>
<SCRIPT>

var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;

var sql = "select char(date('" + CurrentTime + "') + 15 days) from dual ";
var rs = easyExecSql(sql);
if(rs)
{
  CurrentTime = rs[0][0];
}

var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT> 
<body onload="initForm();">

<form name=fm action="./IndiDueFeeMultiQuery.jsp" target=fraSubmit method=post>
<!-- 显示或隐藏信息 -->  
    <INPUT VALUE="查询催收保单" class = cssbutton TYPE=button onclick="easyQueryClick();"/>    
    <INPUT name="bFlag" TYPE=hidden value="other" />   
    <INPUT VALUE="生成催收记录" TYPE=button class = cssbutton onclick="GrpMulti()">   
	  <INPUT VALUE="打印通知书" class = cssbutton TYPE=hidden onclick="printNotice();"> 
	  <INPUT VALUE="打印PDF通知书" class = cssbutton style="width:100px" TYPE=hidden onclick="printInsManage();">
	  <INPUT VALUE="批打PDF通知书" class = cssbutton style="width:100px" TYPE=hidden onclick="printInsManageBat();">
	  <INPUT VALUE="打印PDF通知书" class = cssbutton style="width:100px" TYPE=button onclick="newprintInsManage();">
	  <INPUT VALUE="批打PDF通知书" class = cssbutton style="width:100px" TYPE=button onclick="newprintInsManageBat();">
	  <input type=hidden name=strsql value=""/>
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpmulti);">
      </td>
      <td class= titleImg>
        集体批处理催收
      </td>
     </tr>
    </table>
    <Div  id= "divGrpmulti" style= "display: ''">
      <Table  class= common>
        <tr>
      <TD  class= title> 管理机构</TD><TD  class= input>
      <Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
       <TD  class= title>
          应收日期范围：
          </TD>
		  <TD  class= title>
          开始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>
          结束日期:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD>          
		</tr>
		<TR>
			<TD class= title>
				选择类型:
			</TD>
			<TD class= input>
				<input class="codeNo" name="queryType" value="1" CodeData="0|^1|请选择类型^2|普通单^3|少儿单" readOnly
    	          ondblclick="return showCodeListEx('queryType',[this,queryTypeName],[0,1]);" 
    	          onkeyup="return showCodeListEx('queryType',[this,queryTypeName],[0,1]);"><Input class="codeName" name="queryTypeName" readonly>
			</TD>
			<TD  class= title>销售渠道</TD>
            <TD  class= input>
                 <Input class="codeNo" name="SaleChnl" CodeData="0|^0|全部^1|个险^2|银保" verify="销售渠道|&code:SaleChnl"  ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1]);"><Input class="codeName" name="SaleChnlName"  elementtype="nacessary" readonly>
            </TD>   
  
		</TR>    
      </Table>    
    </Div>     					                                                                            

<!-- 显示或隐藏信息 --> 
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    			 催收记录：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanJisPayGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>     
  	</div>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td> <INPUT VALUE="保单明细查询" class = cssbutton TYPE=button onclick="returnParent();"> </td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanIndiContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
    </center>  	
  	</div>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
    		<td class= titleImg>
    		险种信息
    		</td>
    	</tr>
    </table> 	
  <Div  id= "divGrpDue1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanPolGrid" ></span> 
  	</TD>
      </TR>
    </Table>	
    <center>      				
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
    </center>  
  </Div>	 
  <INPUT type= "hidden" name= "QuerySql" value= "">
  <INPUT type= "hidden" name= "GetNoticeNo" >
  <div id="ErrorsInfo"></div>  
  <div id="test"></div>                                                                             
</form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    <iframe name="printfrm" src="" width=0 height=0></iframe>
  	<form method=post id=printform target="printfrm" action="">
      	<input type=hidden name=filename value=""/>
  	</form>        	
</body>
</html>
