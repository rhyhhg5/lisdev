var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function getQuerySQL()
{
  var tStartDate = fm.all("StartDate").value;
  var tEndDate = fm.all("EndDate").value;
  var tManageCom = fm.all("ManageCom").value;
  var tGetNoticeNo = fm.all("GetNoticeNo").value;
  var tContNo = fm.all("ContNo").value;
  
  var sql1 = "";
  var sql2 = "";
  if(tGetNoticeNo!=null && tGetNoticeNo!="")
  {
    sql1 = "and InnerSource = '"+tGetNoticeNo+"' ";
  }
  if(tContNo!=null && tContNo!="")
  {
    sql2 = "and ContNo = '"+tContNo+"' ";
  }
  
  var PayTaskState = fm.all("PayTaskState").value;
  var subSql = ""
  if(fm.all("PayTaskState").value !="")
  {
	    if(PayTaskState == "0")
	    {
	       subSql += " and exists (select 1 from ljaget c ,ljsgetdraw d where c.actugetno = d.getnoticeno and d.contno=a.contno and c.confdate is null) ";
	    }
	    else if(PayTaskState == "1" )
	    {
	       subSql += " and exists (select 1 from ljaget c ,ljsgetdraw d where c.actugetno = d.getnoticeno and d.contno=a.contno and c.confdate is not null ) ";
	    }
	    else if(PayTaskState == "2")
	    {
	       subSql += " and not exists (select 1 from ljaget c ,ljsgetdraw d where c.actugetno = d.getnoticeno and d.contno=a.contno) ";
	    }
	    else
	    {
	      subSql += " and a.dealstate = '" + PayTaskState + "' ";
	    }
  }
  
  var strSQL = "select  "
             + "(select (select name from LDCom where ComCode = ManageCom) from LCCont where ContNo = a.ContNo) 机构,max(makedate) 修改时间, "
             + "ContNo 保单号,(select appntname from lcappnt where ContNo = a.ContNo ) 投保人, "
             + "(select name from lcinsured where ContNo = a.ContNo fetch first 1 row only  ) 被保人,  "
             + "(select edorvalue from LPEdorEspecialData where edorno = min(a.workno) and EdorType = 'XX' and detailtype = 'BeforeMoney') 修改前的金额, "
             + "(select edorvalue from LPEdorEspecialData where edorno = max(a.workno) and EdorType = 'XX' and detailtype = 'AfterMoney') 申请改变金额, "
             + "varchar(round( "
             + "((select db2inst1.nvl(double(edorvalue),0) from LPEdorEspecialData where edorno = max(a.workno) and EdorType = 'XX' and detailtype = 'AfterMoney') "
             + "-(select db2inst1.nvl(double(edorvalue),0) from LPEdorEspecialData where edorno = min(a.workno) and EdorType = 'XX' and detailtype = 'BeforeMoney')) "
             + ",2)) 修改前和修改后的差额, "
             + "(select substr(remark,locate('变更后的公式为:',remark)+23,7) from LGWork where WorkNo = max(a.workno)) 调整后公式, "
             + "(select substr(remark,locate('修改原因:',remark)+14,locate(']。',remark)-locate('修改原因:',remark)-14) from LGWork where WorkNo = max(a.workno)) 调整原因, "
             + "(select InnerSource from LGWork where WorkNo = max(a.workno)) 给付记录号, "
             + "(select Operator from LGWork where WorkNo = max(a.workno)) 操作用户, "
             + "(select (select name from LDCom where ComCode = ManageCom) from lpedorapp where edoracceptno = max(a.workno)) 操作机构, "
             + "(case when exists (select 1 from ljaget c ,ljsgetdraw d where c.actugetno = d.getnoticeno and d.contno=a.contno and c.confdate is not null ) then '给付完成' "
             + "when exists (select 1 from ljaget c ,ljsgetdraw d where c.actugetno = d.getnoticeno and d.contno=a.contno and c.confdate is null) then '待给付' else '待给付确认' end) 给付任务状态  "
             + "from LGWork a where acceptwayno='8' and TypeNo = '0300025'  "
             + "and exists (select 1 from LCCont where ContNo = a.ContNo and CInvalidate between '"+tStartDate+"' and '" + tEndDate+ "') "
             + "and exists (select 1 from LCCont where ContNo = a.ContNo and ManageCom like '"+tManageCom+"%') "
             + sql1
             + sql2 
             + subSql
             + "group by ContNo order by ContNo with ur ";
             
  return strSQL;

}

function queryClick()
{
  //得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	
	var tStartDate = fm.all("StartDate").value;
	var tEndDate = fm.all("EndDate").value;
	
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("满期日起期不能为空！");
		return false;
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("满期日止期不能为空！");
		return false;
	}
	
	var tTodayDate=fm.Today.value;
	
	//应收时间起止期三个月校验
	var t1 = new Date(tTodayDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>30)
    {
	//	alert("满期日期止期不能大于当前日期后30天!");
	//	return false;
    }
    
  var sql = getQuerySQL();
  turnPage1.queryModal(sql, LjsGetGrid); 
  if(LjsGetGrid.mulLineCount == 0)
  {
   alert("没有符合条件的给付信息");
   return false;
  }
}

function downloadClick()
{
  //得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	
	var tStartDate = fm.all("StartDate").value;
	var tEndDate = fm.all("EndDate").value;
	
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("满期日起期不能为空！");
		return false;
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("满期日止期不能为空！");
		return false;
	}
	
	var tTodayDate=fm.Today.value;
	
	//应收时间起止期三个月校验
	var t1 = new Date(tTodayDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>30)
    {
	//	alert("满期日期止期不能大于当前日期后30天!");
	//	return false;
    }
    
  var sql = getQuerySQL();
  fm.all('Sql').value = sql;
  
  fm.action="./ExpirBenefitCalListSubmit.jsp";
  fm.submit();
}

function selectOne()
{
    document.all("divLjsGetDraw").style.display = "";
    getLjsGetDrawdetail();
    var tSelNo = LjsGetGrid.getSelNo();
    
    fm.ContNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"ContNo");
    
    showAllCodeName();
}

function getLjsGetDrawdetail()
{
	var tSelNo = LjsGetGrid.getSelNo();
	var tGetNoticeNo = LjsGetGrid.getRowColDataByName(tSelNo-1,"GetNoticeNo"); //申请人类型

	var SQL = "SELECT CONTNO,insuredno,'',RISKCODE,GETMONEY,(select enddate from lcpol where polno = LJSGETDRAW.polno),POLNO, " 
	        + "case DutyCode when '000000' then '初始忠诚奖' when '000001' then '外加忠诚奖' end "
	        + "FROM LJSGETDRAW WHERE GETNOTICENO = '" + tGetNoticeNo + "' ";
	turnPage.queryModal(SQL, LjsGetDrawGrid);
	for(var i = 0; i < LjsGetDrawGrid.mulLineCount; i++)
	{
		var sql = "select name from lcbnf where polno ='"+LjsGetDrawGrid.getRowColData(i,7)+"'";
		var rs = easyExecSql(sql);
		var bnf = "";
		if(rs!=null)
		{
			for(var j =0;j<rs.length;j++)
			{
				bnf+=rs[j]+'、';
			}
		}
		LjsGetDrawGrid.setRowColData(i,3,bnf);
	}
}



//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}
