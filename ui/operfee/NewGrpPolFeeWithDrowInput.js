//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var tGrpContNo ; //合同号
var mActuGetNo ;//退费通知书号


//提交，保存按钮对应操作
function submitForm()
{
   if(!check())	 return false;
   var i = 0;
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,ActuGetNo )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  { 
  	mActuGetNo = ActuGetNo ;  	
  	queryAfterDif();
  	queryDifRecord();
  	printDif();
   // var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");	  
  }
}

function check()
{
	var tSelNo = GrpContGrid.getSelNo();
	if (tSelNo==0 || tSelNo==null)
	{
		alert("请选择一条记录");
		return false;
	}
	
	if (isValLjsPay()) 
	{
		alert("请先作废应收，再办理领取");
		return false;
	}
	
	//余额
	if(fm.Dif.value==0)
	{
		alert("退费额为零，不能退费");
		return false;
	}

	//领取金额
	if (fm.PayDif.value == '' || !isNumeric(fm.PayDif.value))
	{ 
		 alert("本次领取金额不正确，请输入合法的数据");
		 return false;
	}
	
	 var tDif = parseFloat(fm.Dif.value);
	 var tPayDif = parseFloat(fm.PayDif.value);
	 if (tPayDif>tDif || tPayDif<0)
	 {
		 alert("本次领取金额输入超出范围！");
		 return false;
	 }
   // 领取人身份证号 
   if (!fm.DrawerID.value=='' &&  !isNumeric(fm.DrawerID.value))
	 {
		 alert("请输入合法的领取人身份证号！");
		 return false;
	 }

	 //退费方式转换
	if (fm.HidPayMode.value=="1" && fm.PayMode.value=="4")
	{
		alert("自行缴费方式不能转换为银行转帐方式！");
		return false;
	}

	//退费方式
	if (fm.PayMode.value=="" )
	{
		alert("请选择退费方式！");
		return false;
	}	
	
	//转帐日期
	if (fm.PayDate.value=='' || !isDate(fm.PayDate.value))
	{
		  alert("请输入合法的转帐日期");
		  return false;
	}
	return true;
}

//判断是否有有效续期应收记录
function isValLjsPay(){

	var strSQL = "select count(GetNoticeNo) from LJSPay a, LJSPayPerson b"
           + " where a.otherno  '" +  fm.all('HidGrpContNo').value + "'"
		   + " and a.otherno = b.GrpContNo"	 
		;
		  
    var arrRetun = easyExecSql(strSQL);	
	if (arrRetun == null || arrRetun ==0) return false;
	
	return true;  
}
//查询按钮
function queryClick(){
  if(fm.all('AppntNo').value == '' && fm.all('GrpContNo').value == '')
  {
  	alert("请输入客户号 或  合同单号 ");
  	return false;
  }
	var strSQL = "select distinct a.GrpContNo,a.GrpName,a.CValiDate,min(b.PayToDate),'已签单',a.Prem,getAgentName(a.AgentCode) from LCGrpCont a,LCCont b"
           + " where 1=1"
		   + getWherePart( 'a.GrpContNo ','GrpContNo' ) 
		   + getWherePart( 'a.AppntNo ','AppntNo' ) 
		   + " and a.GrpContNo=b.GrpContNo and a.OutPayFlag='1'"
		   + " and a.AppFlag='1' and b.AppFlag='1'"
		   + " group by a.GrpContNo,a.GrpName,a.CValiDate,a.AppFlag,a.Prem,a.AgentCode"
		;
	       
	turnPage.queryModal(strSQL,GrpContGrid);
	
}
//查询缴费资料
function getPaydetail(){

    var tSelNo = GrpContGrid.getSelNo();

	if (tSelNo==0 || tSelNo==null)
	{
		alert("请选择一条记录");
		return false;
	}else
  {
		 var tPayToDate = GrpContGrid.getRowColData(tSelNo-1,4); 
		 tGrpContNo = GrpContGrid.getRowColData(tSelNo-1,1); 
		 fm.all('HidGrpContNo').value = tGrpContNo; 
		 var arrReturn = new Array();
         var strSQL = "select distinct  a.Prem,a.Dif,a.BankCode,a.BankAccNo,a.AccName,a.PayMode,min(b.PaytoDate) from LCGrpCont a,LCCont b"
           + " where  a.GrpContNo='" + tGrpContNo + "' and a.AppFlag='1'"
		   + " and b.GrpContNo=a.GrpContNo and b.AppFlag='1'"
		   + " group by a.Prem,a.Dif,a.BankCode,a.BankAccNo,a.AccName,a.PayMode"
		 ;

		 arrReturn = easyExecSql(strSQL);
		 if ( arrReturn == null ) {
			  return false;
		 }else
		 {
         showPayInfo(arrReturn[0],tPayToDate);
		 }
		 //领取人
		 var arrResult = easyExecSql("select a.LinkMan1 from LCGrpAddress a where a.AddressNo=(select AddressNo from LCGrpAppnt  where GrpContNo = '" + tGrpContNo + "') and a.CustomerNo=(select CustomerNo from LCGrpAppnt  where GrpContNo = '" + tGrpContNo + "')", 1, 0);
     if (arrResult == null) {
        return false;

     } else {
          try { fm.all('Drawer').value = arrResult[0][0]; } catch(ex) { };
     }
     
     //领取纪录
     queryDifRecord();
		
	} 
}

//从余额领取记录中获取退费通知书号
function getActuGetNo(){
	 
	 mActuGetNo = GetDifGrid.getRowColData(GetDifGrid.getSelNo()-1,5); 

}
//显示缴费信息
 function showPayInfo(cArr,pmPayToDate)
  {  	  
  	try { fm.all('PayToDate').value = pmPayToDate; } catch(ex) { };
  	try { fm.all('Prem').value = cArr[0]; } catch(ex) { };
  	try { fm.all('Dif').value = cArr[1]; } catch(ex) { };
	  try { fm.all('PayDif').value = cArr[1]; } catch(ex) { };
	  try { fm.all('BankCode').value = cArr[2]; } catch(ex) { };
  	try { fm.all('BankAccNo').value = cArr[3]; } catch(ex) { };
	  try { fm.all('AccName').value = cArr[4]; } catch(ex) { };
	  try { fm.all('HidPayMode').value = cArr[5]; } catch(ex) { };
	  try { fm.all('PayMode').value = cArr[5]; } catch(ex) { };
  		
 }
 
 //查询余额领取记录
function queryDifRecord(){
	
	var strSQL = "select a.ConfDate,a.SumGetMoney,a.Drawer,a.Operator,a.ActuGetNo, case when a.ConfDate is null then '未领取' else '结束' end from LJAGet a"
           + " where 1=1"		   
		       + " and a.OtherNoType='8' and a.OtherNo='" + tGrpContNo + "'"		   
		;

	turnPage1.queryModal(strSQL,GetDifGrid);
	
}


//查询领取后余额
function queryAfterDif()
{
	 var arrReturn = new Array();
         var strSQL = "select a.Dif  from LCGrpCont a"
           + " where  a.GrpContNo='" + tGrpContNo + "' and a.AppFlag='1'"		   
		 ;

		 arrReturn = easyExecSql(strSQL);
		 if ( arrReturn == null ) {
			  return false;
		 }else
		 {
		 	  try { fm.all('Dif').value = arrReturn[0][0]; } catch(ex) { };
		 }
}

//打印人保健康保费余额领取单
function printDif(){	 
		window.open("../f1print/GrpDifGetPrint.jsp?ActuGetNo="+ mActuGetNo);	
}
//补打人保健康保费余额领取单
function printClick(){	 
	var tSelNo = GetDifGrid.getSelNo();
	if (tSelNo == null || tSelNo == 0 )
	{
		   alert("请选择一条余额领取纪录");
		   return false;
	}
	
		window.open("../f1print/GrpDifGetPrint.jsp?ActuGetNo="+ mActuGetNo);	
}



