<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：NewGrpPolFeeWithDrowInput.jsp
//程序功能：个人保费催收，实现数据从保费项表到应收个人表和应收总表的流转
//创建日期：2002-07-24
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="NewGrpPolFeeWithDrowInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="NewGrpPolFeeWithDrowInit.jsp"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurrentDate= PubFun.getCurrentDate();   
	String tCurrentYear=StrTool.getVisaYear(CurrentDate);
	String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
	String tCurrentDate=StrTool.getVisaDay(CurrentDate);        
%>
<script>
    var CurrentYear=<%=tCurrentYear%>;  
	var CurrentMonth=<%=tCurrentMonth%>;  
	var CurrentDate=<%=tCurrentDate%>;
	var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
</script>
</head>
<body onload="initForm();" >
	<form name=fm action=./NewGrpPolFeeWithDrow.jsp target=fraSubmit method=post>
		<table>
			<tr>
				<td>
					<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divPersonSingle);">
				</td>
				<td class= titleImg>团体新单溢交保费退费</td>
			</tr>
		</table>
		<Div id="divPersonSingle" style= "display: ''">
			<Table  class= common width="100%">
				<TR  class= common>
				    <TD  class= title>客户号</TD>
					<TD  class= input>
						<Input class= common name=AppntNo >
					</TD>
					<TD  class= title>合同单号</TD>
					<TD  class= input>
						<Input class= common name=GrpContNo >
					</TD>
					<td class=title>
						<input class="cssButton" value="查  询" type=button onclick="queryClick()">
					</td>
					<TD>  </td>
				</TR>
			</table>
		</Div>
		<!-- 显示或隐藏信息 --> 
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单列表：
    		</td>
    	</tr>
     </table>
  	 <Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
     <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
   </center> 
   </div>
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divtPayShow);">
    		</td>
    		<td class= titleImg>
    			 缴费状况:
    		</td>
    	</tr>
     </table>
	<div id="divtPayShow" style="display: ">
		<Table  class= common width="100%">
			<TR  class= common>
				<TD  class= title>交至日期</TD>
				<TD  class= input>
					<Input class="common" name=PayToDate readonly=true >
				</TD> 
				<TD  class= title>当期应交保费</TD>
				<TD  class= input>
					<Input class="common" name=Prem readonly=true >
				</TD>
				<TD  class= title>保费余额</TD>
				<TD  class= input>
					<Input class="common" name=Dif readonly=true >
				</TD>
			  </TR>
		  </TABLE>
		  <br>
		  <TABLE class= common width="100%">
			 <TR  class= common>
				<TD  class= title>本次领取金额</TD>
				<TD  class= input>
					<Input class="common" name=PayDif  >
				</TD> 
				<TD  class= title>领取人身份证号</TD>
				<TD  class= input>
					<Input class="common" name=DrawerID maxlength="20" >
				</TD> 
				<TD  class= title>领取人</TD>
				<TD  class= input>
					<Input class="common" name=Drawer readonly>
				</TD> 
				
			</TR>
		 </TABLE>
		 <TABLE CLASS="common">
			<TR  class= common>
				<TD  class= title>退费方式</TD>
				 <TD  class= input>
				   <Input class="codeNo" name="PayMode" CodeData="0|^1|现金^4|银行转帐" verify="交付费方式|&code:PayMode"  ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1]);" onkeyup="return showCodeListEx('PayMode',[this,PayModeName],[0,1]);"><Input class="codeName" name="PayModeName"  elementtype="nacessary" readonly>
				 </TD>					
				<TD  class= title>转帐银行</TD>
				<TD  class= input>
					<Input class="common1" name=BankCode readonly >
				</TD>
				<TD  class= title>帐户所有人</TD>
				<TD  class= input>
					<Input class="common1" name=AccName readonly>
				</TD>
				<TD  class= title>帐号</TD>
				<TD  class= input>
					<Input class="common1" name=BankAccNo readonly >
				</TD>
			 </TR>	
			 <TR  class= common>
				<TD  class= title>转帐日期</TD>
				<TD  class= input>
					<Input class="coolDatePicker" dateFormat="short" name=PayDate  >
				</TD> 
			</tr>
		</Table>
		<br>
		<INPUT class="cssButton" VALUE="确  认" TYPE=button onclick="submitForm()">		
	</div>	
	<br>
	<!-- 显示或隐藏信息 --> 
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDifGet);">
    		</td>
    		<td class= titleImg>
    			 余额领取纪录：
    		</td>
    	</tr>
     </table>
  	 <Div  id= "divDifGet" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGetDifGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
     <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
    </center>  
    <INPUT class="cssButton" VALUE="打  印" TYPE=button onclick="printClick();">				
	</div>
	<input type=hidden id="ActuGetNo" name="HidPayMode">
	<input type=hidden id="ContNo" name="HidGrpContNo">
</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>