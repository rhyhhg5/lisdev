<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： IndiPayForBatPrtIns.jsp
//程序功能：
//创建日期：2005-12-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.lang.String"%>
<%
    boolean operFlag = true;
	CErrors tError = null;

	int    tCount=0;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	String tPrintServerPath = "";

	GlobalInput tG = (GlobalInput)session.getValue("GI");
	tG=(GlobalInput)session.getValue("GI");
	//tG.ClientIP = request.getRemoteAddr(); 
	tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
	tG.ServerIP =tG.GetServerIP();
	
//	String tPayNo = request.getParameter("PayNo");
//	String tPayDate = request.getParameter("PayDate");		
//	String tDueMoney = request.getParameter("DueMoney");
//	String tDif = request.getParameter("Dif");

  	transact = "PRINT";
    String tOutXmlFile  = application.getRealPath("")+"\\";
	LDSysVarSchema	tLDSysVarSechma =new LDSysVarSchema();
	tLDSysVarSechma.setSysVarValue(tOutXmlFile);
	LJAPaySet tLJAPaySet = new LJAPaySet();

	try
  	{
		String tPayNo[] = request.getParameterValues("GrpContGrid1");	 
	    String tChk[] = request.getParameterValues("InpGrpContGridChk"); //参数格式=” Inp+MulLine对象名+Chk”  
	
		tCount = tPayNo.length;
		System.out.println("----tCount:"+tCount+"-----");
		for(int j = 0; j < tCount; j++)
		{
			if(tChk[j].equals("1"))  
			{
				LJAPaySchema tLJAPaySchema = new LJAPaySchema();
				tLJAPaySchema.setPayNo(tPayNo[j]);
				tLJAPaySet.add(tLJAPaySchema);
				System.out.println("---------tPayNo: "+tPayNo[j]);
			}
  		}
  		
  	
		// 准备传输数据 VData
  		VData tVData = new VData();
		tVData.add(tLJAPaySet);
		tVData.add(tLDSysVarSechma);
  		tVData.add(tG);
  		GrpPayCompBatchPrintBL tGrpPayCompBatchPrintBL = new GrpPayCompBatchPrintBL();
    	if(tGrpPayCompBatchPrintBL.submitData(tVData,transact))
    	{
			if (transact.equals("PRINT"))
			{
				tCount = tGrpPayCompBatchPrintBL.getCount();
        		String tFileName[] = new String[tCount];
        		VData tResult = new VData();
        		tResult = tGrpPayCompBatchPrintBL.getResult();
        		tFileName=(String[])tResult.getObject(0);

        		String mFileNames = "";
        		for (int i = 0;i<=(tCount-1);i++)
        		{
          			System.out.println("~~~~~~~~~~~~~~~~~~~"+i);
          			System.out.println(tFileName[i]);
          			mFileNames = mFileNames + tFileName[i]+":";
        		}
          		System.out.println("===================="+mFileNames+"=======================");
				System.out.println("=========tFileName.length==========="+tFileName.length);

       			String strRealPath = application.getRealPath("/").replace('\\','/');
				String sql = "Select sysvarvalue from LDSysvar where sysvar = 'PrintServerInterface'";
       			ExeSQL mExeSQL = new ExeSQL();
       			tPrintServerPath =  mExeSQL.getOneValue(sql);
//	    		tPrintServerPath = "http://10.252.1.151:82/PICCPrintIntf";	//正式机用		       			
%>
				<html> 	
					<script language="javascript">
        	    		var printform = parent.fraInterface.document.getElementById("printform");
        	    		printform.elements["filename"].value = "<%=mFileNames%>";
        	    		printform.action = "<%=tPrintServerPath%>";
        	    		printform.submit();
        			</script>
        		</html>
<%         			
			}
		}
	}
	catch(Exception ex)
	{
    	Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}

%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
		alert("打印成功");
	}
	else
	{
		alert("<%=Content%>");
	}
</script>
</html>