 <%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<title>催收信息查询</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>   
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <script src="./GrpDueFeeList.js"></script> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpDueFeeListInit.jsp"%>


</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm action=./GrpDueFeeListResult.jsp target=fraSubmit method=post>
   <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>         
 
      <TD  class= title> 险种编码</TD><TD  class= input>
      <Input class= "codeno"  name=RiskCode  ondblclick="return showCodeList('RiskCode',[this,RiskName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RiskCode',[this,RiskName],[0,1],null,null,null,1);" ><Input class=codename  name=RiskName></TD>
                  
          <TD  class= title>
          缴费方式:
          </TD>
          <TD  class= input>
            <Input class=common name=Payintv >
          </TD>             
       </TR>                     
      	<TR  class= common>
          <TD  class= title>
          催收开始日期:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>       	
          <TD  class= title>
          团体保单号
          </TD>
          <TD  class= input>
            <Input class=common name=GrpPolNo >
          </TD>       	
       </TR>         
   </Table>  
      <INPUT VALUE="查询" Class=cssbutton TYPE=button onclick="submitForm();"> 
      <INPUT VALUE="返回" Class=cssbutton TYPE=button onclick="returnParent();">   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTempFee1);">
    		</TD>
    		<TD class= titleImg>
    			 催收信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divGrpDue1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanGrpDueQueryGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
 </Div>					

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
</html>