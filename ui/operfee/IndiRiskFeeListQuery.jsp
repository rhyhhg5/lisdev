<A HREF=""></A> <html> 
<%
//程序名称：IndiDueFeeListQuery.jsp
//程序功能：个险应收清单查询、打印
//创建日期：2006-11-06 
//创建人  ：
//更新记录：  更新人 huxl    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/CurrentTime.jsp"%>
<html>
<head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="IndiRiskFeeListQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="IndiRiskFeeListQueryInit.jsp"%>
 <script>
   var tsql_Branch=" 1 and branchtype=#3# and branchtype2=#01# and  (EndFlag <> #Y# or EndFlag is null) "  ;
</script>
</head>
<body onload="initForm();">
<form name=fm action=./IndiDueFeeListQuery2.jsp target=fraSubmit method=post>
  
	<table class= common >
    <TR  class= common>		
      <TD  class= title> 保单号 </TD>
      <TD  class= input> <Input class= common name=ProposalGrpContNo >  </TD>
      <TD  class= title>  客户号 </TD>
      <TD  class= input>  <Input class= common name=AppNo ></TD>
	  <TD class= title>是否缴费</td>
			  <TD  class= input>
				  <Input class="codeNo" name="DealState" CodeData="0|^0|未缴费^1|已缴费^2|全部" readOnly " value ="0" ondblclick="return showCodeListEx('',[this,DealStateName], [0,1]);" onkeyup="return showCodeListKey('',[this,DealStateName], [0,1]);" ><Input class="codeName" name="DealStateName" elementtype="nacessary" value="未缴费" readonly >
			  </TD> 	  
		  
	 </TR>

	 <TR class= common>  
   		<td class= title>管理机构</td>
	    <td class=input>
			<Input class= "codeno"  name=ManageCom   ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName >
		</td>
	   <TD  class= title>
      应交开始日期
      </TD>
      <TD  class= input>
        <Input name=SingleStartDate class='coolDatePicker' dateFormat='short' verify="应交开始日期|notnull&Date" elementtype="nacessary" />
      </TD>
      <TD  class= title>
      应交终止日期
      </TD>
      <TD  class= input>
        <Input name=SingleEndDate  class='coolDatePicker' dateFormat='short' verify="应交终止日期|notnull&Date" elementtype="nacessary" />
      </TD>

   </TR>

	   <TR class= common>  
		               
      <TD  class= title>业务员代码</TD>
      <TD  class= input>
      	<!-- <Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCodet" ondblclick="return showCodeList('agentcodetbq1',[this,AgentCodeName,null,null],[0,1,3,4],null,fm.ManageCom.value,'ManageCom',1);" onkeyup="return showCodeListKey('agentcodetbq',[this,AgentCodeName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true > -->
       <Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCodet" ondblclick="return showCodeList('agentcodetbq1',[this,AgentCodeName,null,null],[0,1,3,4],null,null,null,1);" onkeyup="return showCodeListKey('agentcodetbq',[this,AgentCodeName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true > 
      </TD>
      
       <TD  class= title> 险种代码 </TD>
          <TD  class= input> <Input class="codeno" name=RiskCode ondblclick="return showCodeList('BankRiskCode',[this,RiskCodeName],[0,1],null);" onkeyup="return showCodeListKey('BankRiskCode',[this,RiskCodeName],[0,1],null);"><Input class=codename  name=RiskCodeName>
       </TD>
    </TR>
	</table>

<INPUT VALUE="查询个案应收纪录" class = cssbutton TYPE=button onclick="easyQuery();">
<td class= titleImg><INPUT VALUE="打印" class = cssbutton style="width:60px" TYPE=button onclick="easyPrint();">  </td>
    	
<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 应收记录：
    	</tr>
</table>
<Div  id= "divCont" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
      	  		<span id="spanContGrid" >
      	  		</span>
      	  		</td>
  			</tr>
    	</table>
		<center>    	
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 			
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
		</center>  	
  	</Div>

<input type=hidden id="sql" name="sql" value="">
	     
</form>



  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  		<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>  
</body>
</html>
