  //               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var showInfo;
var strSql="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{	
	initContPreserveGrid();
	//判断关键定是否为空
	var strAgentCom=trim(document.fm.all("ManageCom").value);

	var StartDate = trim(document.fm.all("StartDate").value);
	var EndDate = trim(document.fm.all("EndDate").value);
	var strSQL1 ="";
	//初始化防止出错
	if(strAgentCom==null||strAgentCom=='')
	{
		window.alert("请选择查询机构");
		return false;
	}
			  //留存保单明细查询sql
				  strSql="select  showManageName(b.managecom) as managecom, "
					  	+" a.contno,"
				  		+" (select riskname from lmriskapp where riskcode=a.riskcode)," 
				  		+" b.appntname, b.insuredname, b.cvalidate, b.prem, " 
				  		+"(select phone from lcaddress where customerno=b.appntno  "
				  		+" and addressno=(select addressno from lcappnt where contno=b.contno and appntno=b.appntno)), "
				  		+" (select postaladdress from lcaddress where customerno=b.appntno " 
				  		+" and addressno=(select addressno from lcappnt where contno=b.contno and appntno=b.appntno)), "
				  		+" nvl((select name from lacom where agentcom=b.agentcom),''),"
//				  		+" (select case when a.RemainState='1' or a.RemainState='2' then '留存' when a.RemainState='3' then '退保' else '' end from dual) as remainstate"
				  		+" (select '留存' from dual) as remainstate "
				  		+" from LCContRemain a,LCCont b " 
				  		+" where a.contno=b.contno " 
				  		+" and a.applydate between '"+StartDate+"' and '"+EndDate+"' "
				  		+" and b.managecom like '"+strAgentCom+"%' "
				  		+" union all"
				  		+" select  showManageName(b.managecom) as managecom, "
				  		+" a.contno,"
				  		+" (select riskname from lmriskapp where riskcode=a.riskcode)," 
				  		+" b.appntname, b.insuredname, b.cvalidate, b.prem, mobile(b.contno)," 
				  		+" (select postaladdress from lcaddress where customerno=b.appntno "
				  		+" and addressno=(select addressno from lcappnt where contno=b.contno and appntno=b.appntno)),"
				  		+" nvl((select name from lacom where agentcom=b.agentcom),''),"
//				  		+" (select case when a.RemainState='1' or a.RemainState='2' then '留存' when a.RemainState='3' then '退保' else '' end from dual) as remainstate"
				  		+" (select '退保' from dual) as remainstate "
				  		+" from LCContRemain a,LbCont b "
				  		+" where a.contno=b.contno "
				  		+" and a.applydate between '"+StartDate+"' and '"+EndDate+"'"
				  		+" and b.managecom like '"+strAgentCom+"%' "
				  		+" order by managecom,remainstate"
				  		+" with ur";
			


	 //查询SQL，返回结果字符串
	 turnPage.strQueryResult=easyQueryVer3(strSql, 1, 0, 1);
	 fm.StatisticsStartDate.value=StartDate;
	 fm.StatisticsEndDate.value=EndDate;
	 if(!turnPage.strQueryResult)
	 {
	 	window.alert("没有满足条件的记录!");
	 	return false;
	 }
	 else
	 {
		 initContPreserveGrid();
	 	turnPage.queryModal(strSql,ContPreserveGrid);
	 }
}



function afterSubmit(FlagStr, content)
{
  try{ showInfo.close(); } catch (e) {};
  window.focus();     
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
}

function easyPrint()
{	
	if( ContPreserveGrid.mulLineCount == 0)
	{
		window.alert("没有需要打印的信息");
		return false;
	}
	fm.all('strsql').value=strSql;
	fm.submit();
}
