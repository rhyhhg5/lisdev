<%
//程序名称：GrpexpirBenefitNoticeInputInit.jsp
//程序功能：
//创建日期：2008-11-10 9:47:13
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  { 
	// 保单查询条件
    fm.all('StartDate').value = '2008-01-01';
    fm.all('EndDate').value = CurrentTime;
    var sql = "select char(date('" + CurrentTime + "') + 1 month) from dual ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      fm.all('EndDate').value = rs[0][0];
    }
	fm.all('ManageCom').value = managecom;
	showAllCodeName();
  }
  catch(ex)
  {
    alert("GrpexpirBenefitNoticeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm1()
{
  try
  {
    initInpBox();
    initIndiContGrid();
    initLCInsuredGrid();
	initLjsGetGrid();
  }
  catch(re)
  {
    alert("GrpexpirBenefitNoticeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}                    

function initForm()
{
  try
  {
	if(fm.all('TempGrpContNo').value!=null && fm.all('TempGrpContNo').value!="")
	{
	  queryImportData(fm.all('TempGrpContNo').value);
	}
  }
  catch(re)
  {
    alert("IndiDueFeeBatchInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initIndiContGrid()
  {
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="被保人姓名";         		//列名
      iArray[1][1]="70px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="客户号";         		//列名
      iArray[2][1]="70px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许	   

	    iArray[3]=new Array();
      iArray[3][0]="性别";         		//列名
      iArray[3][1]="30px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[4]=new Array();
      iArray[4][0]="证件类型";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="证件号码";         		//列名
      iArray[5][1]="150px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[6]=new Array();
      iArray[6][0]="生日";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][19]= 0 ;

	    iArray[7]=new Array();
      iArray[7][0]="银行";         		//列名
      iArray[7][1]="70px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
     	iArray[8]=new Array();
      iArray[8][0]="户名";         		//列名
      iArray[8][1]="70px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[9]=new Array();
      iArray[9][0]="账号";         		//列名
      iArray[9][1]="150px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      IndiContGrid = new MulLineEnter( "fm" , "IndiContGrid" ); 
      //这些属性必须在loadMulLine前
      IndiContGrid.mulLineCount = 0;   
      IndiContGrid.displayTitle = 1;
      IndiContGrid.locked = 1;
      IndiContGrid.canSel = 0;
      IndiContGrid.hiddenPlus = 1;
      IndiContGrid.hiddenSubtraction = 1;
      IndiContGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 信息列表的初始化
function initLCInsuredGrid() 
{
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号";    				//列名1
    iArray[1][1]="150px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=3;                         //是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="客户号";         			//列名2
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="姓名";         			//列名8
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="性别";         			//列名5
    iArray[4][1]="40px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

    iArray[5]=new Array();
    iArray[5][0]="出生日期";         		//列名6
    iArray[5][1]="120px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="证件类型";         		//列名6
    iArray[6][1]="60px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="证件号码";         		//列名6
    iArray[7][1]="200px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    

    LCInsuredGrid = new MulLineEnter( "fm" , "LCInsuredGrid" ); 
    //这些属性必须在loadMulLine前
    LCInsuredGrid.mulLineCount = 0;
    LCInsuredGrid.pageLineNum=5;    
    LCInsuredGrid.displayTitle = 1;
    LCInsuredGrid.canSel = 0;
    LCInsuredGrid.hiddenPlus = 1; 
    LCInsuredGrid.hiddenSubtraction = 1;
    //LCInsuredGrid.selBoxEventFuncName = "reportDetailClick";
    LCInsuredGrid.detailInfo = "单击显示详细信息";
    LCInsuredGrid.loadMulLine(iArray);  
  }
  catch(ex) 
  {
    alert(ex);
  }
}

// 催收记录的初始化
function initLjsGetGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      	
      iArray[0]=new Array();	
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许	
      	
      iArray[1]=new Array();
      iArray[1][0]="保单号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=10;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="公司名称";         		//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="被保人数";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保单给付金额合计";         		//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[5]=new Array();
      iArray[5][0]="应给付日期";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            		//列最大值
      iArray[5][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="保单业务员";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="给付方式";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="回销状态";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=200;            	        //列最大值
      iArray[8][3]=0; 
      iArray[8][21]="BackState";                      	//是否允许输入,1表示允许，0表示不允许
      
    // iArray[8]=new Array();
    // iArray[8][0]="保单业务员";         		//列名
    // iArray[8][1]="80px";            		//列宽
    // iArray[8][2]=200;            	        //列最大值
    // iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    // 
    // iArray[9]=new Array();
    // iArray[9][0]="被保人姓名";         		//列名
    // iArray[9][1]="80px";            		//列宽
    // iArray[9][2]=200;            	        //列最大值
    // iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    // 
    // iArray[10]=new Array();
    // iArray[10][0]="给付方式";         		//列名
    // iArray[10][1]="50px";            		//列宽
    // iArray[10][2]=100;            	        //列最大值
    // iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    // 
    // iArray[11]=new Array();
    // iArray[11][0]="给付次数";         		//列名
    // iArray[11][1]="50px";            		//列宽
    // iArray[11][2]=200;            	        //列最大值
    // iArray[11][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    // 
    // iArray[12]=new Array();
    // iArray[12][0]="给付任务状态";         		//列名
    // iArray[12][1]="80px";            		//列宽
    // iArray[12][2]=200;            	        //列最大值
    // iArray[12][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
    // 
    // iArray[13]=new Array();
    // iArray[13][0]="占座1";         		//列名
    // iArray[13][1]="80px";            		//列宽
    // iArray[13][3]=200;            	        //列最大值
    // iArray[13][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
    // 
    // iArray[14]=new Array();
    // iArray[14][0]="占座2";         		//列名
    // iArray[14][1]="80px";            		//列宽
    // iArray[14][2]=200;            	        //列最大值
    // iArray[14][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
    // 
    // iArray[15]=new Array();
    // iArray[15][0]="占座3";         		//列名
    // iArray[15][1]="80px";            		//列宽
    // iArray[15][2]=200;            	        //列最大值
    // iArray[15][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
    // 
    // iArray[16]=new Array();
    // iArray[16][0]="给付通知书号码";         		//列名
    // iArray[16][1]="80px";            		//列宽
    // iArray[16][2]=200;            	        //列最大值
    // iArray[16][3]=3;                   	//是否允许输入,1表示允许，0表示不允许

	    LjsGetGrid = new MulLineEnter( "fm" , "LjsGetGrid" ); 
      //这些属性必须在loadMulLine前
      LjsGetGrid.mulLineCount =0;   
      LjsGetGrid.displayTitle = 1;
      LjsGetGrid.hiddenPlus = 1;
      LjsGetGrid.hiddenSubtraction = 1;
      //LjsGetGrid.locked = 1;
      LjsGetGrid.canChk = 1;
      LjsGetGrid.canSel =1;
      LjsGetGrid.loadMulLine(iArray);  
      LjsGetGrid.selBoxEventFuncName ="getIndiContGrid";

	  }
      catch(ex)
      {
        alert("IndiDueFeeBatchInputInit.jsp-->initLjsGetGrid函数中发生异常:初始化界面错误!");
      }
}


</script>