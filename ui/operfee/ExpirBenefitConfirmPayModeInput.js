var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  initDiv();
  easyQueryClick();
  initPayMode();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(ex)
  {
  	alert("在FailListInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{
  var tStartDate = fm.all("StartDate").value;
  var tEndDate = fm.all("EndDate").value;
  var strSQL = "select distinct a.SerialNo,a.otherno,d.contno, "
             + "(select appntname from lccont where contno = d.contno), "
             + "(select appntidno from lccont where contno = d.contno), "
             + "(select name from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + "(select idno from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + "a.sumgetmoney,d.lastgettodate,getUniteCode(d.agentcode),d.insuredno, "
             + "(case when exists (select 1 from ljaget where actugetno = a.getnoticeno) then codename('paymode',a.paymode) else '' end), "
//             + "nvl((select count(distinct c.getnoticeno) from ljsgetdraw b ,ljsget c "
//             + "where b.getnoticeno = c.getnoticeno and c.dealstate ='1' and b.contno = d.contno and b.insuredno =d.insuredno ),0), "
             + "(select name from lacom where agentcom = d.agentcom), "
             + "(case when exists (select 1 from ljaget where actugetno = a.getnoticeno) then '待给付' else '待给付确认' end), " 
             + "d.appntno,a.paymode,d.handler,d.GetNoticeNo from ljsget a ,ljsgetdraw d "
             + "where a.getnoticeno = d.getnoticeno and d.riskcode = '330501' "
             + "and a.othernotype='20' and a.dealstate = '0' and feefinatype != 'YEI' "
             + "and not exists (select 1 from ljaget where actugetno = a.getnoticeno and confdate is not null) "
             + getWherePart( 'd.ManageCom ','ManageCom','like') 
             + getWherePart( 'a.OtherNo ','EdorAcceptNo')
             + getWherePart( 'd.ContNo ','ContNo') 
             + " and d.lastgettodate between '" + tStartDate + "' and '" + tEndDate + "' ";
             
  turnPage1.queryModal(strSQL, LjsGetGrid); 
}
function queryClick()
{
  //得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	
	var tStartDate = fm.all("StartDate").value;
	var tEndDate = fm.all("EndDate").value;
	
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("满期日起期不能为空！");
		return false;
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("满期日止期不能为空！");
		return false;
	}
	
	var tTodayDate=fm.Today.value;
	
	//应收时间起止期三个月校验
	var t1 = new Date(tTodayDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>30)
    {
	//	alert("满期日期止期不能大于当前日期后30天!");
	//	return false;
    }
    
  easyQueryClick();
  initDiv();
  if(LjsGetGrid.mulLineCount == 0)
  {
   alert("没有符合条件的给付信息");
   return false;
  }
}

function selectOne()
{
    document.all("divLjsGetDraw").style.display = "";
    document.all("divPayMode").style.display = "";
    document.all("divDrawer").style.display = "";
    
    getLjsGetDrawdetail();
    var tSelNo = LjsGetGrid.getSelNo();
    fm.GetNoticeNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"GetNoticeNo");
    fm.SerialNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"SerialNo");
    fm.TempContNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"ContNo");
    fm.InsuredNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"InsuredNo");
    fm.AppntNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"AppntNo");
 //   fm.PayMode.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"PayMode");
    fm.GetState.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"GetState"); //给付任务状态
    
    fm.ContNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"ContNo");
    
    initPayMode();
    initDrawerInfo();
    showAllCodeName();
}

function getLjsGetDrawdetail()
{
	var tSelNo = LjsGetGrid.getSelNo();
	var tGetNoticeNo = LjsGetGrid.getRowColDataByName(tSelNo-1,"GetNoticeNo"); //申请人类型

	var SQL = "SELECT CONTNO,insuredno,'',RISKCODE,GETMONEY,(select enddate from lcpol where polno = LJSGETDRAW.polno),POLNO, " 
	        + "case DutyCode when '000000' then '初始忠诚奖' when '000001' then '外加忠诚奖' end "
	        + "FROM LJSGETDRAW WHERE GETNOTICENO = '" + tGetNoticeNo + "' ";
	turnPage.queryModal(SQL, LjsGetDrawGrid);
	for(var i = 0; i < LjsGetDrawGrid.mulLineCount; i++)
	{
		var sql = "select name from lcbnf where polno ='"+LjsGetDrawGrid.getRowColData(i,7)+"'";
		var rs = easyExecSql(sql);
		var bnf = "";
		if(rs!=null)
		{
			for(var j =0;j<rs.length;j++)
			{
				bnf+=rs[j]+'、';
			}
		}
		LjsGetDrawGrid.setRowColData(i,3,bnf);
	}
	fm.RiskCode.value = LjsGetDrawGrid.getRowColDataByName(0,"RiskCode");
}

//校验查询条件是否正确
function checkDealData()
{
  if(LjsGetGrid.mulLineCount == 0)
  {
    alert("没有符合条件的给付信息");
    return false;
  }
  if(LjsGetGrid.getSelNo() < 1)
  {
    alert("请选择需要确认的给付任务");
    return false;
  }
  
  if(trim(fm.Handler.value) == "")
  {
    alert("申请人类型不能为空");
    return false;
  }
  if(trim(fm.DrawerCode.value) == "")
  {
    alert("给付对象不能为空");
    return false;
  }
  var tGetState = fm.GetState.value;
  if(tGetState=="已给付确认")
  {
    alert("该给付任务已经给付确认，不能重复操作！");
    return false;
  }
  
  if(fm.RiskCode.value == "330501" && !check330501())
  {
    return false;
  }
  
  if(trim(fm.PayModeShow.value)=="4")
  {
    if(trim(fm.BankCodeShow.value) == "")
    {
       alert("转账银行不能为空！")
       return false;
    }
    if(trim(fm.AccNameShow.value) == "")
    {
       alert("帐户名不能为空！")
       return false;
    }
    if(trim(fm.BankAccNoShow.value) == "")
    {
       alert("转账帐号不能为空！")
       return false;
    }
  }
  if(trim(fm.PayModeShow.value) == "1")
  {
    return confirm("目前的给付方式是现金，是否进行给付确认？");
  }
  return true;
}

//处理选中的数据
function dealData()
{
  if(!checkDealData())
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
  fm.action = "./ExpirBenefitConfirmSubmit.jsp";
  //document.all.DealButton.disabled=true;
  fm.submit();	
}

function afterCodeSelect(cCodeName, Field)
{
  try	
  {
    if(cCodeName == "PayMode")	
    {
      showPayMode();
    }
    if(cCodeName == "DrawerCode")	
    {
      showDrawerInfo();
    }
  }
  catch(ex){}
}

function initDrawerInfo()
{
  var sql ;
  var contNo = fm.TempContNo.value;
  var insuredNo = fm.InsuredNo.value;
  var appntNo = fm.AppntNo.value;
  
  sql = "select EdorValue from LPEdorEspecialData where DetailType = 'DrawerCode' and EdorNo = '" + fm.GetNoticeNo.value + "' ";
  var rs1 = easyExecSql(sql);
  if(rs1)
  {
    fm.DrawerCode.value = rs1[0][0];
    if(fm.DrawerCode.value=="0")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AccName, PayMode " 
          + "from LJSGet a where GetNoticeNo = '" + fm.GetNoticeNo.value + "' ";
    }
    else if(fm.DrawerCode.value=="1")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AccName, PayMode " 
          + "from LJSGet a where GetNoticeNo = '" + fm.GetNoticeNo.value + "' ";
    }
              
    var rs = easyExecSql(sql);
    if(rs)
    {
      fm.BankCodeShow.value = rs[0][0];
      fm.BankNameShow.value = rs[0][1];
      fm.BankAccNoShow.value = rs[0][2];
      fm.AccNameShow.value = rs[0][3];
      fm.BankCodeShow1.value = rs[0][0];
      fm.BankNameShow1.value = rs[0][1];
      fm.BankAccNoShow1.value = rs[0][2];
      fm.AccNameShow1.value = rs[0][3];
      
      fm.PayModeShow.value = rs[0][4];
      if(fm.PayModeShow.value == "1")
      {
        //显示现金层
        document.all("divCash").style.display = "";
        document.all("divBank").style.display = "none";
        document.all("divOther").style.display = "none";
      }
      else if(fm.PayModeShow.value == "4")
      {
        //显示转账层
        document.all("divCash").style.display = "none";
        document.all("divBank").style.display = "";
        document.all("divOther").style.display = "none";
      }
      else
      {
        //显示其它
        document.all("divCash").style.display = "none";
        document.all("divBank").style.display = "none";
        document.all("divOther").style.display = "";
      }				
    }
  }
  else
  {
    if(fm.RiskCode.value == "330501")
    { 
      fm.DrawerCode.value = "1";
    }
    else
    {
      fm.DrawerCode.value = "0";
    }
    if(fm.DrawerCode.value=="0")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AccName, PayMode " 
          + "from LCCont a where ContNo = '" + contNo + "' ";
    }
    else if(fm.DrawerCode.value=="1")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AccName, 'PayMode' " 
          + "from LCInsured a where ContNo = '" + contNo + "' and InsuredNo = '" + insuredNo + "' ";
    }
              
    var rs = easyExecSql(sql);
    if(rs)
    {
      fm.BankCodeShow.value = rs[0][0];
      fm.BankNameShow.value = rs[0][1];
      fm.BankAccNoShow.value = rs[0][2];
      fm.AccNameShow.value = rs[0][3];
      fm.BankCodeShow1.value = rs[0][0];
      fm.BankNameShow1.value = rs[0][1];
      fm.BankAccNoShow1.value = rs[0][2];
      fm.AccNameShow1.value = rs[0][3];
      
      if(rs[0][0] == "")
      {
        //显示现金层
        document.all("divCash").style.display = "";
        document.all("divBank").style.display = "none";
        document.all("divOther").style.display = "none";
        fm.PayModeShow.value = "1";
      }
      else
      {
        //显示转账层
        document.all("divCash").style.display = "none";
        document.all("divBank").style.display = "";
        document.all("divOther").style.display = "none";
        fm.PayModeShow.value = "4";
      }			
    }
  }
  showAllCodeName();
}

function showDrawerInfo()
{
  initPayMode();
  
  var sql ;
  var contNo = fm.TempContNo.value;
  var insuredNo = fm.InsuredNo.value;
  var appntNo = fm.AppntNo.value;
  
  sql = "select EdorValue from LPEdorEspecialData where DetailType = 'DrawerCode' and EdorNo = '" + fm.GetNoticeNo.value + "' ";
  var rs1 = easyExecSql(sql);
  
  if(rs1)
  {
    var drawerCode1 = rs1[0][0];
    if(fm.DrawerCode.value==drawerCode1)
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AccName, PayMode " 
          + "from LJSGet a where GetNoticeNo = '" + fm.GetNoticeNo.value + "' ";
    }
    else if(fm.DrawerCode.value=="0")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AccName, PayMode " 
          + "from LCCont a where ContNo = '" + contNo + "' ";
    }
    else if(fm.DrawerCode.value=="1")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AccName, 'PayMode' " 
          + "from LCInsured a where ContNo = '" + contNo + "' and InsuredNo = '" + insuredNo + "' ";
    }
              
    var rs = easyExecSql(sql);
    if(rs)
    {
      fm.BankCodeShow.value = rs[0][0];
      fm.BankNameShow.value = rs[0][1];
      fm.BankAccNoShow.value = rs[0][2];
      fm.AccNameShow.value = rs[0][3];
      fm.BankCodeShow1.value = rs[0][0];
      fm.BankNameShow1.value = rs[0][1];
      fm.BankAccNoShow1.value = rs[0][2];
      fm.AccNameShow1.value = rs[0][3];
      
      fm.PayModeShow.value = rs[0][4];
      if(rs[0][4]=='PayMode')
      {
        if(rs[0][0] == "" )
        {
          //显示现金层
          document.all("divCash").style.display = "";
          document.all("divBank").style.display = "none";
          document.all("divOther").style.display = "none";
          fm.PayModeShow.value = "1";
        }
        else 
        {
        //显示转账层
          document.all("divCash").style.display = "none";
          document.all("divBank").style.display = "";
          document.all("divOther").style.display = "none";
          fm.PayModeShow.value = "4";
        }			
      }
      else if(rs[0][4]=='1')
      {
        //显示现金层
          document.all("divCash").style.display = "";
          document.all("divBank").style.display = "none";
          document.all("divOther").style.display = "none";
          fm.PayModeShow.value = "1";
      }
      else if(rs[0][4]=='4')
      {
        //显示转账层
          document.all("divCash").style.display = "none";
          document.all("divBank").style.display = "";
          document.all("divOther").style.display = "none";
          fm.PayModeShow.value = "4";
      }
      else
      {
        //显示转账层
          document.all("divCash").style.display = "none";
          document.all("divBank").style.display = "none";
          document.all("divOther").style.display = "";
          fm.PayModeShow.value = "9";
      }
    }
  }
  else
  {
    if(fm.DrawerCode.value=="0")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AccName, PayMode " 
          + "from LCCont a where ContNo = '" + contNo + "' ";
    }
    else if(fm.DrawerCode.value=="1")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AccName, 'PayMode' " 
          + "from LCInsured a where ContNo = '" + contNo + "' and InsuredNo = '" + insuredNo + "' ";
    }
              
    var rs = easyExecSql(sql);
    if(rs)
    {
      fm.BankCodeShow.value = rs[0][0];
      fm.BankNameShow.value = rs[0][1];
      fm.BankAccNoShow.value = rs[0][2];
      fm.AccNameShow.value = rs[0][3];
      fm.BankCodeShow1.value = rs[0][0];
      fm.BankNameShow1.value = rs[0][1];
      fm.BankAccNoShow1.value = rs[0][2];
      fm.AccNameShow1.value = rs[0][3];
      
      if(rs[0][0] == "")
      {
        //显示现金层
        document.all("divCash").style.display = "";
        document.all("divBank").style.display = "none";
        document.all("divOther").style.display = "none";
        fm.PayModeShow.value = "1";
      }
      else
      {
        //显示转账层
        document.all("divCash").style.display = "none";
        document.all("divBank").style.display = "";
        document.all("divOther").style.display = "none";
        fm.PayModeShow.value = "4";
      }			
    }
  }
  
  showAllCodeName();
}

function showPayMode()
{
  var sql ;
  var contNo = fm.TempContNo.value;
  var insuredNo = fm.InsuredNo.value;
  var appntNo = fm.AppntNo.value;
  
  if(fm.PayMode.value == "1")
  {
      fm.BankCode.value = "";
      fm.BankName.value = "";
      fm.BankAccNo.value = "";
      fm.AccName.value = "";
  }
  else
  {
    if(fm.DrawerCode.value=="0")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,AppntName, PayMode " 
          + "from LCCont a where ContNo = '" + contNo + "' ";
    }
    else if(fm.DrawerCode.value=="1")
    {
      sql = "select BankCode,(select BankName from LDBank where BankCode = a.BankCode) ,BankAccNo ,Name, 'PayMode' " 
          + "from LCInsured a where ContNo = '" + contNo + "' and InsuredNo = '" + insuredNo + "' ";
    }
              
    var rs = easyExecSql(sql);
    if(rs)
    {
        fm.BankCode.value = rs[0][0];
        fm.BankName.value = rs[0][1];
        fm.BankAccNo.value = rs[0][2];
        fm.AccName.value = rs[0][3];
    }
  }
  showAllCodeName();
}



function check330501()
{
  if(fm.PayModeShow.value == "1")
  {
     alert("常无忧B给付方式只能为银行转账或其它！")
     return false;
  }
  if(fm.DrawerCode.value == "0")
  {
    alert("常无忧B只能给付给被保人！");
    return false;
  }
//  var sql = "select 1 from LOPRTManager where OtherNo='" + fm.TempContNo.value + "' and Code = 'MX003' and printtimes > 0 ";
//  var rs = easyExecSql(sql);
//  if(!rs)
//  {
//    alert("请先打印批单，再给付确认！");
//    return false;
//  }
  return true;
}

function printData()
{
  
  if(LjsGetGrid.mulLineCount == 0)
  {
    alert("没有符合条件的给付信息");
    return false;
  }
  if(LjsGetGrid.getSelNo() < 1)
  {
    alert("请选择需要打印批单的给付任务");
    return false;
  }
  if(fm.RiskCode.value != "330501")
  {
    alert("打印批单功能只针对常无忧B保单！");
    return false;
  }
  
  if(fm.RiskCode.value == "330501")
  {
    if(fm.DrawerCode.value == "0")
    {
      alert("常无忧B只能给付给被保人！");
      return false;
    }
    if(fm.PayModeShow.value == "1")
    {
      alert("常无忧B给付方式只能为银行转账或其它!");
      return false;
    }
  }
  
  var tGetNoticeNo = fm.GetNoticeNo.value;
  var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="../uw/PDFPrintSave.jsp?Code=MX003&StandbyFlag2="+tGetNoticeNo;
  fm.submit();
}

function changeConfirm()
{
  if(LjsGetGrid.mulLineCount == 0)
  {
    alert("没有符合条件的给付信息");
    return false;
  }
  if(LjsGetGrid.getSelNo() < 1)
  {
    alert("请选择需要变更给付方式的给付任务");
    return false;
  }
  if(fm.RiskCode.value == "330501")
  {
    if(fm.DrawerCode.value == "0")
    {
      alert("常无忧B只能给付给被保人！");
      return false;
    }
    if(fm.PayMode.value == "1")
    {
      alert("常无忧B给付方式只能选择银行转账或其它！");
      return false;
    }
  }
  if(trim(fm.PayMode.value)=="")
  {
    alert("给付方式不能为空！")
    return false;
  }
  if(trim(fm.PayMode.value)=="4")
  {
    if(trim(fm.BankCode.value) == "")
    {
       alert("转账银行不能为空！")
       return false;
    }
    if(trim(fm.AccName.value) == "")
    {
       alert("帐户名不能为空！")
       return false;
    }
    if(trim(fm.BankAccNo.value) == "")
    {
       alert("转账帐号不能为空！")
       return false;
    }
    if(trim(fm.BankAccNo.value) != trim(fm.BankAccNoConf.value))
    {
       alert("两次录入银行帐号不同，请检查后重新录入！")
       fm.BankAccNoConf.value = "";
       return false;
    }
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.action = "./ExpirBenefitConfirmPayModeSubmit.jsp";
  fm.submit();	
}

function initPayMode()
{
  fm.BankCode.value = "";
  fm.BankName.value = "";
  fm.BankAccNo.value = "";
  fm.BankAccNoConf.value = "";
  fm.AccName.value = "";
  fm.PayMode.value = "";
  fm.PayModeName.value = "";
}
//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

