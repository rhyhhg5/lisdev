<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ExpirBenefitCancelInput.jsp
//程序功能：常无忧B给付任务撤销页面
//创建日期：2010-07-07 16:06
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	
  <SCRIPT src="ExpirBenefitCancelInput.js"></SCRIPT>
  <%@include file="./ExpirBenefitCancelInit.jsp"%>
</head>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate();   
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
  String AheadDays="30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  String SubDate=tD.getString(AfterDate);  
  String tSubYear=StrTool.getVisaYear(SubDate);
  String tSubMonth=StrTool.getVisaMonth(SubDate);
  String tSubDate=StrTool.getVisaDay(SubDate);               	               	
%>

<SCRIPT>
  var CurrentYear=<%=tCurrentYear%>;  
  var CurrentMonth=<%=tCurrentMonth%>;  
  var CurrentDate=<%=tCurrentDate%>;
  var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
  var SubYear=<%=tSubYear%>;  
  var SubMonth=<%=tSubMonth%>;  
  var SubDate=<%=tSubDate%>;
  var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
  var managecom = <%=tGI.ManageCom%>;
  var operator = '<%=tGI.Operator%>';
  var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
  var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
</SCRIPT> 

<body  onload="initForm();" >
<form action="ExpirBenefitCancelSubmit.jsp" method=post name="fm" target="fraSubmit">
  <table>
    <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti);">
      </td>
      <td class= titleImg>
        给付保单查询：
      </td>
     </tr>
   </table>
   <Div  id= "divAlivePayMulti" style= "display: ''">
   <table  class= common align=center>
      <tr  class= common>
        <td class= title>满期日起期</td>
      	<td class= input><Input class="coolDatePicker" dateFormat="short" name=StartDate ></td>
      	<td class= title>满期日止期</td>
      	<td class= input><Input class="coolDatePicker" dateFormat="short" name=EndDate ></td>
        <td class= title>管理机构</td>
      	<td class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><Input class=codename  name=ManageComName readonly></td>
      </tr>
      <tr  class= common>
        <td class= title>代理网点</td>
      	<td class= input><Input class= "codeno"  name=AgentCom  ondblclick="return showCodeList('agentcom8',[this,AgentName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('agentcom8',[this,AgentName],[0,1],null,null,null,1);" readonly><Input class=codename  name=AgentName readonly></td>
      	<td  class= title>给付任务号</td>
        <td  class= input><input class= common name="EdorAcceptNo" ></td>
        <td class= title>保单号</td>
      	<td class= input><Input class= common name=ContNo></td>	
      </tr>
      <tr  class= common>
        <td><Input class = cssbutton VALUE="查 询" TYPE=button onclick="queryClick();"> </td>
      </tr>
    </table>
    </Div>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    			 给付任务清单：
    		</td>
    	</tr>
    </table>
  <div  id= "divJisPay" style= "display: ''">
   	<table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1><span id="spanLjsGetGrid"></span></td>
  	  </tr>
  	  <tr>
  		<td>  					
  		  <center>    	
      			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
     			<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      			<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
    	  </center>
    	</td>
      </tr>
    </table>
  </div> 
  
  <br>
  <div align=left id="divLjsGetDraw" style="display: '' ">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divLjsGetDrawGrid);">
    		</td>
    		<td class= titleImg>
    			 给付明细：
    		</td>
    	</tr>
    </table>
    <div align=left id="divLjsGetDrawGrid" style="display: '' ">
      <table>
        <tr  class= common>
          <td text-align: left colSpan=1>
  		    <span id="spanLjsGetDrawGrid" ></span> 
  	      </td>
  	    </tr>
      </table>
    </div>
  </div>
  <br>

  <input class=cssButton  VALUE="批量撤销"  TYPE=button onclick="dealData()">
  <font color="#FF0000">可以通过复选框选择一条或多条给付记录进行撤销，如果不选择，默认撤销列表中全部给付记录。</font>
  </div>
  
    <input type=hidden id="GetNoticeNo" name="GetNoticeNo">
    <input type=hidden id="PayModeShow" name="PayModeShow">
    <input type=hidden id="SerialNo" name="SerialNo">
    <input type=hidden id="TempContNo" name="TempContNo">
	<input type=hidden id="InsuredNo" name="InsuredNo">
	<input type=hidden id="AppntNo" name="AppntNo">
	<input type=hidden id="GetState" name="GetState">
	<input type=hidden id="RateFlagValue" name="RateFlagValue">
	<input type=hidden id="RiskCode" name="RiskCode">
	<input type=hidden id="sql" name="sql">
	<INPUT type= "hidden" name= "Today" id = "Today" value= "">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
