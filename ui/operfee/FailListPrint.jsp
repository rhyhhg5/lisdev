<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：打印转帐失败清单
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%
  boolean operFlag = true;
  String flagStr = "";
  String content = "";
  String sql = request.getParameter("sql");
  System.out.print("\n"+sql+"\n");
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  VData tVData =new VData();
  tVData.add(0,sql); 
  tVData.add(1,tG);    
  PrintPayFailListUI tPrintPayFailListUI = new PrintPayFailListUI();
  //得到输出的XML数据
  XmlExport txmlExport = tPrintPayFailListUI.getXmlExport(tVData,"");   
  if(txmlExport == null)
  {
    operFlag = false;
    content = tPrintPayFailListUI.mErrors.getFirstError().toString();                 
  }
	//此代码基本不需修改
	if (operFlag==true)
	{
		//得到模版路径
	  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();  
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);  
    session.putValue("PrintVts", dataStream);
	  session.putValue("PrintStream", txmlExport.getInputStream());
		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
	}
	else
	{
    	flagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=content%>");
	top.close();
</script>
</html>
<%
  	}
%>