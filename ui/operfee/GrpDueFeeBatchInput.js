//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.all('multDue').disabled=false;  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }

   resetForm();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {      
	  afterContQuery ();
	  afterJisPayQuery();
	  
  }
  catch(re)
  {
  	alert("在GrpDueFeeBatchInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function CheckGrpPolNo()
{

   
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function GrpMulti()
{
  var showStr="本程序运行时间可能较长，请点击‘确定’按钮，关闭本窗口。本次作业运行情况，请到本窗口的‘当前线程查询’查询。作业已完成的，请到‘催收记录查询’菜单中查询。";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
  fm.all('multDue').disabled=true; 
  fm.submit();	
}



function CheckDate()
{
  if(!isDate(fm.all('StartDate').value)||!isDate(fm.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fm.all('StartDate').value,fm.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		//alert(fm.all("StartDate").value);
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}
	var strSQL = "select b.GrpContNo,b.PrtNo,b.GrpName,'',b.CValiDate,b.SumPrem,b.Dif,(select sum(prem) from lccont where b.grpcontno = grpcontno  and PayIntv > 0),(SELECT min(paytodate) from LCCont a where b.grpcontno=a.grpcontno) paytodate, " 	  
	         + "(select codename from ldcode where codetype='paymode' and code=b.PayMode),ShowManageName(b.ManageCom),getUniteCode(b.AgentCode),'' from LCGrpCont b where  "
			 + " (b.state is null or b.state <> '03050002') and b.AppFlag='1' and (b.StateFlag is null or b.StateFlag = '1')  and b.GrpContNo not in (select otherno from ljspay where othernotype='1')  and b.GrpContNo in(select GrpContNo from LCGrpPol c "
			 + " where (PaytoDate>='"+CurrentTime+"' and PaytoDate<='"+SubTime+"' and PaytoDate<PayEndDate )"
			 +" and managecom like '"+managecom+"%%'"
			 + " and PayIntv>0 and AppFlag='1' and (StateFlag is null or StateFlag = '1') "
       +" and GrpPolNo not in (select GrpPolNo from LJSPayGrp)"
       //add by xp 剔除一年期险种年缴情况的抽档
       +" and not exists (select 1 from lcgrppol d where c.grppolno=d.grppolno and payintv=12 and exists (select 1 from lmriskapp where riskcode=d.riskcode  and riskperiod in ('M','S')))"
       +" and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y'))"
			 +" and managecom like '"+ fm.ManageCom.value+"%%' ";
	if(loadFlag == "WMD")
	{
	  strSQL += " and exists (select 1 from LCCont "
	          + "where polType = '1' and appFlag = '1' and (StateFlag is null or StateFlag = '1') and grpContNo = b.grpContNo) ";  //无名单
	}
	else
	{
	  strSQL += " and not exists (select 1 from LCCont "
	          + "where polType = '1' and appFlag = '1' and (StateFlag is null or StateFlag = '1') and grpContNo = b.grpContNo) ";  //有名单
	}
  	strSQL += " order by paytodate ";	
	turnPage.queryModal(strSQL, GrpContGrid); 
	initGrpPolGrid();  
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	if(GrpContGrid.mulLineCount == 0)
	{
	  alert("没有符合条件的可催收保单信息");
	}
}

function getpoldetail()//被选中后显示的集体险种应该是符合条件的。++
{
    var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}	
	else
	{
		getPolInfo();
		//getSpecInfo();
    }  	                                          
                         	               	
}

//被选中后显示的集体险种应该是符合条件的。++
function getPolInfo()
{
	  var tRow = GrpContGrid.getSelNo() - 1;	        
		var tGrpContNo=GrpContGrid.getRowColData(tRow,1);  
	//	fm.all('ProposalGrpContNo').value = tGrpContNo;
		var strSQL = "select contPlanCode, "
		            //+ "   (select peoples2 from LCContPlan where grpContNo=a.grpContNo and contPlanCode = a.contPlanCode), "
		            + " count(1), "
		            + "   (select riskname  from lmrisk where   riskcode=a.riskcode), "
		            + "   a.riskCode, "
		            + "   (select codeName from LDCode where code=char(a.payIntv) and codeType='payintv'), "
		            + "   sum(prem), min(cValiDate), min(payToDate), min(endDate) "
		            + "from LCPol a "
		            + "where grpContNo = '" + tGrpContNo + "' and (a.StateFlag is null or a.StateFlag = '1') "
		            + " and payintv != 0 "
		            + "group by grpContNo, contPlanCode, riskCode, riskCode, payIntv "
		            + "order by contPlanCode, riskCode ";
		
		
		//var tGetNoticeNo = GrpContGrid.getRowColData(tRow,13);
		//if (tGetNoticeNo =='' || tGetNoticeNo ==null)
		//{
		//	var strSQL = "select a.contplancode,"
		//	+ " (select count(InsuredNo) from lcinsured where contplancode=a.contplancode  and grpcontno=a.grpcontno) CC ,"
		//	+ " (select riskname  from lmrisk where   riskcode=b.riskcode) "
		//	+ " ,b.riskcode,b.PayIntv,b.BB,b.CValiDate,b.PaytoDate,b.EndDate  "
		//	+ " from lccontplan a,"
		//	+ " (select grpcontno,riskcode,grppolno,sum(Prem) BB ,PayIntv,PaytoDate,EndDate,ManageCom,PayEndDate,CValiDate from lcpol"
		//	+ " where grpcontno='" + tGrpContNo
		//	+ "' and PaytoDate>='"+contCurrentTime+"' and PaytoDate<='"+contSubTime+"' and PaytoDate<PayEndDate "
		//	+ "  and GrpPolNo not in (select GrpPolNo from LJSPayGrp)"
		//	+ " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')"
		//	+ " group by grpcontno,riskcode,grppolno,PayIntv,PaytoDate,EndDate,PayEndDate,ManageCom,CValiDate ) b"
		//	+ " where a.grpcontno='"+ tGrpContNo + "'"
		//	+ " and a.grpcontno=b.grpcontno"
		//	+ " and a.contplancode<>'11'"		
		//	+ "  group by a.contplancode,a.grpcontno,b.riskcode,b.GrpPolNo,b.ManageCom,b.BB,b.grpcontno,b.PayIntv,b.PaytoDate,b.EndDate,b.PayEndDate,b.CValiDate "
		//	+ " order by a.contplancode,a.grpcontno,b.riskcode with ur"
		//	
		//}else
	  //  {
	  //  	var strSQL = "select a.contplancode,"
		//	+ " (select count(InsuredNo) from lcinsured where contplancode=a.contplancode  and grpcontno=a.grpcontno) CC ,"
		//	+ " (select riskname  from lmrisk where   riskcode=b.riskcode) "
		//	+ " ,b.riskcode,b.PayIntv,b.BB,b.CValiDate,b.PaytoDate,b.EndDate  "
		//	+ " from lccontplan a,"
		//	+ " (select grpcontno,riskcode,grppolno,sum(Prem) BB ,PayIntv,PaytoDate,EndDate,ManageCom,PayEndDate,CValiDate from lcpol"
		//	+ " where grpcontno='" + tGrpContNo
		//	+ "' and PaytoDate>='"+contCurrentTime+"' and PaytoDate<='"+contSubTime+"' and PaytoDate<PayEndDate "
		//	+ "  and GrpPolNo in (select GrpPolNo from LJSPayGrp)"
		//	+ " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')"
		//	+ " group by grpcontno,riskcode,grppolno,PayIntv,PaytoDate,EndDate,PayEndDate,ManageCom,CValiDate ) b"
		//	+ " where a.grpcontno='"+ tGrpContNo + "'"
		//	+ " and a.grpcontno=b.grpcontno"			
		//	+ " and a.contplancode<>'11'"		
		//	+ "  group by a.contplancode,a.grpcontno,b.riskcode,b.GrpPolNo,b.ManageCom,b.BB,b.grpcontno,b.PayIntv,b.PaytoDate,b.EndDate,b.PayEndDate,b.CValiDate "
		//	+ " order by a.contplancode,a.grpcontno,b.riskcode with ur"
	  //  	
    //      /*  var strSQL = "select a.contplancode ,count(c.InsuredNo) ,(select riskname  from lmrisk where riskcode=b.riskcode) , b.riskcode , "
		//	+ "  b.PayIntv ,sum(b.Prem), b.CValiDate,b.PaytoDate, b.EndDate"  
		//	+ "  from lccontplan a,lcpol b,lcinsured c where 1=1"
		//	+ "  and b.insuredno=c.insuredno "
		//	+ "  and b.contno=c.contno "
		//	+ "  and a.contplancode=c.contplancode "
		//	+ "	 and a.grpcontno=c.grpcontno"
		//	+ "  and a.grpcontno=b.grpcontno"
		//	+ "  and b.grpcontno=c.grpcontno"
		//	+ "  and b.grpcontno='" + tGrpContNo + "' "		
		//	+ "  and b.PayIntv>0  "
		//	+ "  and b.PaytoDate>='"+contCurrentTime+"' and b.PaytoDate<='"+contSubTime+"' and b.PaytoDate<b.PayEndDate "
		//	+ "  and b.managecom like '"+managecom+"%%'"
		//	+ "  and b.GrpPolNo  in (select GrpPolNo from LJSPayGrp)"
		//	+ "  and b.RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')"
		//	+ "  group by a.contplancode,b.riskcode,b.PayIntv,b.CValiDate,b.PaytoDate,b.EndDate " 
		//	;*/
		//}
		
    turnPage2.queryModal(strSQL, GrpPolGrid); 
}

//被选中后显示的集体险种应该是符合条件的。++
function getSpecInfo()
{
	 var tRow = GrpContGrid.getSelNo() - 1;	        
	 var tGrpContNo=GrpContGrid.getRowColData(tRow,1);  
	 fm.all('ProposalGrpContNo').value = tGrpContNo;
	 fm.PrtNo.value=GrpContGrid.getRowColData(tRow, 2);
	 var strSQL = "select RiskCode,GrpPolNo from LCGrpPol where GrpContNo='"+ tGrpContNo+"'"	           
		 + " and PaytoDate>='"+contCurrentTime+"' and PaytoDate<='"+contSubTime+"' and PaytoDate<PayEndDate "
		 +" and managecom like '"+managecom+"%%'"
		 + " and PayIntv>0 and appflag='1' and (StateFlag is null or StateFlag = '1') "
		 + " and GrpPolNo not in (select GrpPolNo from LJSPayGrp)"
		 + " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')"
	 turnPage3.queryModal(strSQL, SpecGrid); 
   
}

//催收完成后查询保单
function afterContQuery()
{
	// 初始化表格
	initGrpContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}   

	 var strSQL = "select a.GrpContNo,a.PrtNo,a.GrpName,'',a.CValiDate,a.SumPrem,a.Dif,b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom), getUniteCode(a.AgentCode),b.GetNoticeNo from LCGrpCont  a, ljspay b,ljspayperson c where  "
	 + " a.AppFlag='1' and (a.StateFlag is null or a.StateFlag = '1')  and b.OtherNo=a.GrpContNo and  b.othernotype='1' and c.GetNoticeNo=b.GetNoticeNo and c.GrpContNo=b.otherno and  b.MakeDate='" + tempCurrentTime
	 + "'  and a.GrpContNo in(select GrpContNo from LCGrpPol "
	 + " where (PaytoDate>='"+CurrentTime+"' and PaytoDate<='"+SubTime+"' and PaytoDate<PayEndDate )"
	 +" and managecom like '"+managecom+"%'"
	 + " and PayIntv>0 and AppFlag='1' and (StateFlag is null or StateFlag = '1') "
	 +" and GrpPolNo in (select GrpPolNo from LJSPayGrp)"
	 +" and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y'))"
	 +" and a.managecom like '"+managecom+"%' "
	 + " group by a.GrpContNo,a.PrtNo,a.GrpName,a.CValiDate,a.SumPrem,a.Dif,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode";

  	;
	turnPage.queryModal(strSQL, GrpContGrid); 
	initGrpPolGrid();  
	initSpecGrid();
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	fm.all('sqlPDF').value = strSQL;
}


//催收完成后查询催收纪录
function afterJisPayQuery()
{
	// 初始化表格
	initGrpJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}	  
	 
	 var strSQL = "select distinct b.SerialNo,b.MakeDate,'',b.Operator,b.MakeDate from LCGrpCont  a, ljspay b where  "
	 + " a.AppFlag='1' and (a.StateFlag is null or a.StateFlag = '1')  and b.OtherNo=a.GrpContNo and  b.othernotype='1' and  b.MakeDate='" + tempCurrentTime+ "'  and a.GrpContNo in(select GrpContNo from LCGrpPol "
	 + " where (PaytoDate>='"+CurrentTime+"' and PaytoDate<='"+SubTime+"' and PaytoDate<PayEndDate )"
	 +" and managecom like '"+managecom+"%%'"
	 + " and PayIntv>0 and AppFlag='1' and (StateFlag is null or StateFlag = '1') "
	 +" and GrpPolNo in (select GrpPolNo from LJSPayGrp)"
	 +" and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y'))"
	 + getWherePart( 'a.ManageCom', 'ManageCom','like')
	;
  		
	turnPage4.queryModal(strSQL, GrpJisPayGrid); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
}


//查看团体保单明细
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击【团体保单明细】按钮。" );
	else
	{
		var cGrpContNo = GrpContGrid.getRowColData( tSel - 1, 1 );
		try
		{
			//window.open("./GrpPolQuery.jsp");
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ cGrpContNo+"&ContType=1");
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}

//催收完成后打印通知书
function printNotice()
{
	var tRow = GrpContGrid.getSelNo() ;	
	if (tRow == 0 || tRow == null)
	{
		alert("请选择一个保单");
		return false;
	}

	 var tGetNoticeNo=GrpContGrid.getRowColData(tRow - 1,13);
	 if (tGetNoticeNo =='')
	 {
		 alert("发生错误，原应是应收记录号为空");
		 return false;
	 }
	 	 window.open("../operfee/GrpPrintSel.jsp?GetNoticeNo="+ tGetNoticeNo);

}
//查询当前线程
function queryProject()
{
    if(divProject.style.display == '')
    {
      divProject.style.display = 'none';
      return;
    }
	divProject.style.display = '';
	var tempCurrDate = CurrentTime;
	var tempOperator=Operator;
	var strSql = "select a.SerialNo,a.Operator,a.MakeDate,a.MakeTime,case when a.DealState='1' then '正在处理中' when a.DealState='2' then '处理错误' when  a.DealState='3' then '处理已完成' end"
	 + " from LCUrgeVerifyLog a"
	 +" where a.MakeDate='"+tempCurrDate+"' and RiskFlag='1' and OperateType='2' and Operator='"+Operator+"'"
	 + " order by a.ModifyDate,a.ModifyTime  desc  fetch first row only with ur"
	 ;
  var arrReturn = new Array();
	arrReturn = easyExecSql(strSql);
	if (!(arrReturn == null ) ) 
	{
	    fm.all("Projecta").value = "批次号：" + arrReturn[0][0]+"；操作员："+ arrReturn[0][1]+"；操作时间："+arrReturn[0][2]+" " + arrReturn[0][3] +";操作状态："
	    + arrReturn[0][4];
	} 	
}
//打印PDF前往打印管理表插入一条数据
function printInsManage()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}

	var tGetNoticeNo=GrpContGrid.getRowColData(tChked[0],13);  
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '91' and standbyflag2='"+tGetNoticeNo+"'");
	
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		fm.all('GetNoticeNo').value = tGetNoticeNo;
		fm.action="./GrpDueFeeInsForPrt.jsp";
		fm.submit();
	}
	else
	{
		//window.open("../uw/PrintPDFSave.jsp?Code=091&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq );	
		fm.action = "../uw/PrintPDFSave.jsp?Code=091&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
		fm.submit();
	}
}
function printPDF()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	var tGetNoticeNo=GrpContGrid.getRowColData(tChked[0],13);  
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '91' and standbyflag2='"+tGetNoticeNo+"'");
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		alert("提取数据失败-->PrtSeq为空");
		return false;
	}
	//window.open("../uw/PrintPDFSave.jsp?Code=091&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq );	
	fm.action = "../uw/PrintPDFSave.jsp?Code=091&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
	fm.submit();
}
function printInsManageBat()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return False;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}

	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.action="./GrpDueFeeInsAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.action="./GrpDueFeeInsBatPrt.jsp";
		fm.submit();
	}
}
//打印PDF前往打印管理表插入一条数据
function newprintInsManage()
{
	if(!checkPrint())
	{
	  return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}

	var tGetNoticeNo=GrpContGrid.getRowColData(tChked[0],13);  
	fm.action = "../uw/PDFPrintSave.jsp?Code=91&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+tGetNoticeNo;
	fm.submit();
	
}
function newprintInsManageBat()
{
	if(!checkPrint())
	{
	  return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}

	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.action="./GrpDueFeeInsAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.action="./GrpDueFeeInsForBatPrt.jsp";
		fm.submit();
	}
}

function checkPrint()
{
  if (GrpJisPayGrid.mulLineCount == 0)
  {
    alert("没有查询到可打印的催收记录，请先催收！");
    return false;
  }
  if (GrpContGrid.mulLineCount == 0)
  {
    alert("打印列表没有数据");
    return false;
  }
  return true;
}

//PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	//showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}