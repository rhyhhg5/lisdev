<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpPayForPrtIns.jsp
//程序功能：
//创建日期：2005-12-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
    boolean operFlag = true;
	String FlagStr = "";
	String Content = "";
	String GrpContNo="";	
	String GetNoticeNo=request.getParameter("GetNoticeNo");
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	LJSPayBDB  tLJSPayBDB=new LJSPayBDB();
	tLJSPayBDB.setGetNoticeNo(GetNoticeNo);
	if(tLJSPayBDB.getInfo())
	{
		GrpContNo=tLJSPayBDB.getOtherNo();
	}
	else
	{
		operFlag=false;
		Content="没有数据";
	}

	tLCGrpContSchema.setGrpContNo(GrpContNo);
	System.out.println("tLCGrpContSchema.getGrpContNo():" + tLCGrpContSchema.getGrpContNo());
	
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	
	VData tVData = new VData();
	tVData.addElement(tG);
	tVData.addElement(tLJSPayBDB);
	tVData.addElement(tLCGrpContSchema);
	
	GrpDueFeePrintBL tGrpDueFeePrintBL = new GrpDueFeePrintBL();
    if(!tGrpDueFeePrintBL.submitData(tVData,"INSERT"))
    {
		operFlag = false;
     	Content = tGrpDueFeePrintBL.mErrors.getErrContent();                
    }
%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
		parent.fraInterface.printPDF();
	}
	else
	{
		alert("<%=Content%>");
	}
</script>
</html>