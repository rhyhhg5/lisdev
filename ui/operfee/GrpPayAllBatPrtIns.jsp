<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： GrpPayAllBatPrtIns.jsp
//程序功能：
//创建日期：2005-12-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>

<%
	System.out.println("start");
	CError cError = new CError();
	boolean operFlag=true;
	int tCount = 0;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	String PayNo="";
	String tPrintServerPath = "";	
	String strsql = request.getParameter("pdfSql");
	String tOutXmlFile  = application.getRealPath("")+"\\";
	LDSysVarSchema	tLDSysVarSechma =new LDSysVarSchema();
	tLDSysVarSechma.setSysVarValue(tOutXmlFile);
	LJAPaySet tLJAPaySet = new LJAPaySet();
	System.out.println("-------------------"+strsql);

	String arrPtrList[][];
	SSRS tSSRS = new SSRS();
	ExeSQL tPrtList= new ExeSQL();
    tSSRS = tPrtList.execSQL(strsql);
    arrPtrList = tSSRS.getAllData();

	int GridCount = tSSRS.getMaxRow();
	for(int j = 0; j < GridCount; j++)
	{
		if(!arrPtrList[j][0].equals(""))
		{
			LJAPaySchema tLJAPaySchema = new LJAPaySchema();
			tLJAPaySchema.setPayNo(arrPtrList[j][0]);
			tLJAPaySet.add(tLJAPaySchema);
		}
		else
		{
			Content="第"+j+"行没有数据";
		}
	}
	CErrors mErrors = new CErrors();			
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");

	VData tVData = new VData();
	tVData.addElement(tLDSysVarSechma);
	tVData.addElement(tG);
	tVData.addElement(tLJAPaySet);
	
	GrpPayCompBatchPrintBL tGrpPayCompBatchPrintBL = new GrpPayCompBatchPrintBL();
	if(tGrpPayCompBatchPrintBL.submitData(tVData,"PRINT"))
	{
		tCount = tGrpPayCompBatchPrintBL.getCount();
	    String tFileName[] = new String[GridCount];
	    VData tResult = new VData();
	    tResult = tGrpPayCompBatchPrintBL.getResult();
	    tFileName=(String[])tResult.getObject(0);
	
	    String mFileNames = "";
	    for (int i = 0;i<=(tCount-1);i++)
	    {
	    	System.out.println("~~~~~~~~~~~~~~~~~~~"+i);
	    	System.out.println(tFileName[i]);
	    	mFileNames = mFileNames + tFileName[i]+":";
	    }
	    System.out.println("===================="+mFileNames+"=======================");
		System.out.println("=========tFileName.length==========="+tFileName.length);
	
	    String strRealPath = application.getRealPath("/").replace('\\','/');
		String sql = "Select sysvarvalue from LDSysvar where sysvar = 'PrintServerInterface'";
	    ExeSQL mExeSQL = new ExeSQL();
	    tPrintServerPath =  mExeSQL.getOneValue(sql);
//	    tPrintServerPath = "http://10.252.1.151:82/PICCPrintIntf";	//正式机用		
	%>
		<html> 	
			<script language="javascript">
	    		var printform = parent.fraInterface.document.getElementById("printform");
	    		printform.elements["filename"].value = "<%=mFileNames%>";
	    		printform.action = "<%=tPrintServerPath%>";
	    		printform.submit();
	    	</script>
	    </html>
	<%         			
	}
%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
		//parent.fraInterface.afterInsForBatPrt();
		alert("打印成功");
	}
	else
	{
		alert("<%=Content%>");
	}
</script>
</html>