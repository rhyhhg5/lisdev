var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  easyQueryClick();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(ex)
  {
  	alert("在FailListInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{
  var tStartDate = fm.all("StartDate").value;
  var tEndDate = fm.all("EndDate").value;
  var strSQL = "select distinct a.SerialNo,a.otherno,d.contno, "
             + "(select appntname from lccont where contno = d.contno), "
             + "(select appntidno from lccont where contno = d.contno), "
             + "(select name from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + "(select idno from lcinsured where contno = d.contno and insuredno = d.insuredno), "
             + "a.sumgetmoney,d.lastgettodate,getUniteCode(d.agentcode),d.insuredno, "
             + "(case when exists (select 1 from ljaget where actugetno = a.getnoticeno) then codename('paymode',a.paymode) else '' end), "
//             + "nvl((select count(distinct c.getnoticeno) from ljsgetdraw b ,ljsget c "
//             + "where b.getnoticeno = c.getnoticeno and c.dealstate ='1' and b.contno = d.contno and b.insuredno =d.insuredno ),0), "
             + "(select name from lacom where agentcom = d.agentcom), "
             + "(case when exists (select 1 from ljaget where actugetno = a.getnoticeno) then '待给付' else '待给付确认' end), " 
             + "d.appntno,a.paymode,d.handler,d.GetNoticeNo from ljsget a ,ljsgetdraw d "
             + "where a.getnoticeno = d.getnoticeno "
             + "and d.riskcode = '330501' "
             + "and a.othernotype='20' and a.dealstate = '0' and feefinatype != 'YEI' "
             + "and not exists (select 1 from ljaget where actugetno = a.getnoticeno ) "
             + getWherePart( 'd.ManageCom ','ManageCom','like') 
             + getWherePart( 'a.OtherNo ','EdorAcceptNo')
             + getWherePart( 'd.ContNo ','ContNo') 
             + " and d.lastgettodate between '" + tStartDate + "' and '" + tEndDate + "' ";
             
  turnPage1.queryModal(strSQL, LjsGetGrid); 
}
function queryClick()
{
  //得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return false;
	}
	
	var tStartDate = fm.all("StartDate").value;
	var tEndDate = fm.all("EndDate").value;
	
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("满期日起期不能为空！");
		return false;
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("满期日止期不能为空！");
		return false;
	}
	
	var tTodayDate=fm.Today.value;
	
	//应收时间起止期三个月校验
	var t1 = new Date(tTodayDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>30)
    {
	//	alert("满期日期止期不能大于当前日期后30天!");
	//	return false;
    }
    
  easyQueryClick();
  if(LjsGetGrid.mulLineCount == 0)
  {
   alert("没有符合条件的给付信息");
   return false;
  }
}

function selectOne()
{
    document.all("divLjsGetDraw").style.display = "";
    getLjsGetDrawdetail();
    var tSelNo = LjsGetGrid.getSelNo();
    fm.GetNoticeNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"GetNoticeNo");
    fm.SerialNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"SerialNo");
    fm.TempContNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"ContNo");
    fm.InsuredNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"InsuredNo");
    fm.AppntNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"AppntNo");
 //   fm.PayMode.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"PayMode");
    fm.GetState.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"GetState"); //给付任务状态
    
    fm.ContNo.value = LjsGetGrid.getRowColDataByName(tSelNo-1,"ContNo");
    
    showAllCodeName();
}

function getLjsGetDrawdetail()
{
	var tSelNo = LjsGetGrid.getSelNo();
	var tGetNoticeNo = LjsGetGrid.getRowColDataByName(tSelNo-1,"GetNoticeNo"); //申请人类型

	var SQL = "SELECT CONTNO,insuredno,'',RISKCODE,GETMONEY,(select enddate from lcpol where polno = LJSGETDRAW.polno),POLNO, " 
	        + "case DutyCode when '000000' then '初始忠诚奖' when '000001' then '外加忠诚奖' end "
	        + "FROM LJSGETDRAW WHERE GETNOTICENO = '" + tGetNoticeNo + "' ";
	turnPage.queryModal(SQL, LjsGetDrawGrid);
	for(var i = 0; i < LjsGetDrawGrid.mulLineCount; i++)
	{
		var sql = "select name from lcbnf where polno ='"+LjsGetDrawGrid.getRowColData(i,7)+"'";
		var rs = easyExecSql(sql);
		var bnf = "";
		if(rs!=null)
		{
			for(var j =0;j<rs.length;j++)
			{
				bnf+=rs[j]+'、';
			}
		}
		LjsGetDrawGrid.setRowColData(i,3,bnf);
	}
	fm.RiskCode.value = LjsGetDrawGrid.getRowColDataByName(0,"RiskCode");
}


function calData()
{
  if(LjsGetGrid.mulLineCount == 0)
  {
    alert("没有符合条件的给付信息");
    return false;
  }
  if(LjsGetGrid.getSelNo() < 1)
  {
    alert("请选择需要给付试算的给付任务");
    return false;
  }
  if(LjsGetGrid.getRowColDataByName(LjsGetGrid.getSelNo()-1,"ContNo")!=fm.SSContNo.value)
  {
 	alert("试算明细中的保单号和给付任务清单中的保单号不一致!请点击[试算]按钮显示试算结果后,再点击[试算确认]!");
    return false;
  }
  if (fm.RateFlag.checked == true)
  {
    fm.RateFlagValue.value = "Y";
  }
  else if (fm.RateFlag.checked == false)
  {
    fm.RateFlagValue.value = "N";
  }
  if(fm.RateFlagValue.value!=fm.SSRateFlagValue.value||fm.SSFormula.value!=fm.Formula.value)
  {
  	alert("试算时和试算确认时选择的公式或者利率计算方式不同,请重新确认!");
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.action = "./ExpirBenefitCalSubmit.jsp";
  fm.submit();	
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

function TestcalData()
{
  if(LjsGetGrid.mulLineCount == 0)
  {
    alert("没有符合条件的给付信息");
    return false;
  }
  if(LjsGetGrid.getSelNo() < 1)
  {
    alert("请选择需要给付试算的给付任务");
    return false;
  }
  if (fm.RateFlag.checked == true)
  {
    fm.RateFlagValue.value = "Y";
    fm.SSRateFlagValue.value = "Y";
  }
  else if (fm.RateFlag.checked == false)
  {
    fm.RateFlagValue.value = "N";
    fm.SSRateFlagValue.value = "N";
  }
    fm.SSFormula.value = fm.Formula.value;
    fm.SSContNo.value = LjsGetDrawGrid.getRowColData(0,1);
    
  var sqlprem = " select prem from lccont where ContNo = '" + fm.SSContNo.value + "' ";
  var rs = easyExecSql(sqlprem);
  if(rs == null || rs[0][0] == null || rs[0][0] == "")
  {
    alert("没有查询到保费信息");
    return false;
  }
  fm.SSPrem.value = rs[0][0];
  
  var sqlget = " select actuget from lcget where ContNo = '" + fm.SSContNo.value + "' and getdutycode='260203' ";
  var rs1 = easyExecSql(sqlget);
  if(rs1 == null || rs1[0][0] == null || rs1[0][0] == "")
  {
    alert("没有查询到保额信息");
    return false;
  }
  fm.SSAmnt.value = rs1[0][0];
  
  var sqlinsuyear = " select insuyear from lcpol where riskcode='330501' and ContNo = '" + fm.SSContNo.value + "' ";
  var rs2 = easyExecSql(sqlinsuyear);
  if(rs2 == null || rs2[0][0] == null || rs2[0][0] == "")
  {
    alert("没有查询到保单的保障年期信息!");
    return false;
  }
  else if(rs2[0][0]=="5"||rs2[0][0]=="10"){
  	alert("5年期的常无忧B保单不能在此页面进行操作!");
    return false;
  }
    
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.action = "./ExpirBenefitTestCalSubmit.jsp";
  fm.submit();	
}
//[试算]按钮提交后操作,服务器数据返回后执行的操作
function afterTestSubmit( FlagStr, content,initMoney,addMoney )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	document.getElementById("divAliveGetMulti").style.display='block';
	fm.SSInitMoney.value=initMoney;
	fm.SSAddMoney.value=addMoney;
	var allmoney = formatNum((initMoney*1.00+addMoney*1.00+fm.SSAmnt.value*1.00),2);
	fm.SSAllMoney.value=allmoney;
  }
}

function formatNum(Num1,Num2){
     if(isNaN(Num1)||isNaN(Num2)){
           return(0);
     }else{
           Num1=Num1.toString();
           Num2=parseInt(Num2);
           if(Num1.indexOf('.')==-1){
                 return(Num1);
           }else{
                 var b=Num1.substring(0,Num1.indexOf('.')+Num2+1);
                 var c=Num1.substring(Num1.indexOf('.')+Num2+1,Num1.indexOf('.')+Num2+2);
                 if(c==""){
                       return(b);
                 }else{
                       if(parseInt(c)<5){
                             return(b);
                       }else{
                             return((Math.round(parseFloat(b)*Math.pow(10,Num2))+Math.round(parseFloat(Math.pow(0.1,Num2).toString().substring(0,Math.pow(0.1,Num2).toString().indexOf('.')+Num2+1))*Math.pow(10,Num2)))/Math.pow(10,Num2));
                       }
                 }
           }
     }
}
