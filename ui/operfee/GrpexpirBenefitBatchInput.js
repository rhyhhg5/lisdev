//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var expression ="" ;
var expression1 ="" ;

function PersonSingle()
{
    if(beforeSubmit())
    {  
    	if (!checkValidateDate()) return false;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      	  	
      fm.submit();
    }	
}

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,batchNo)
{
	try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
   
    //content = "满期给付抽档操作完成，不能抽档保单请下载清单";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
   resetForm(batchNo);
}



//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
     
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function commonPayMulti()
{
  if(!checkValidateDate()) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
	fm.payMode.value="Q";
  fm.submit();	
}

function cashPayMulti()
{
	if(!checkValidateDate()) return false;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
	fm.payMode.value="1";
  fm.submit();	
}


function CheckDate()
{
  if(!isDate(fm.all('StartDate').value)||!isDate(fm.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fm.all('StartDate').value,fm.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

// 查询按钮
/*********************************************************************
 *  查询合同号
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	
	
	//得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return fasle;
	}
	else
		{
			expression1 = " and a.ManageCom like '%"+fm.all("ManageCom").value +"%'";
		}
	managecom = fm.ManageCom.value;
	
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}	 
	if(fm.all("PayTaskState").value == null || fm.all("PayTaskState").value == "")
	{
		expression = " and exists (select 1 from LPEdorEspecialData where a.grpcontno = edorno and Edortype ='TJ') "
	}
	else
	{
		expression = " and exists (select 1 from LPEdorEspecialData where a.grpcontno = edorno and Edortype ='TJ' and detailtype='QUERYTYPE' and edorvalue ='"+fm.all("PayTaskState").value+"') "
	}

	var strSQL =" select a.grpcontno,a.grpname,a.peoples2,(select amnt from lcgrppol where grppolno = b.grppolno),a.cinvalidate,getUniteCode(a.agentcode)"
						 +",(select case when edorvalue ='2' then'团体统一给付' when edorvalue ='3' then'个人给付' end from LPEdorEspecialData where edorno =a.grpcontno and detailtype='QUERYTYPE'  ) "
						 +",'','抽档标记' "
						 +" ,(select count(1) from lpedoritem where edoracceptno in(select edorno from lpedorapp a,lpgrpedoritem b where a.edoracceptno = b.edoracceptno and a.edorstate !='0' and grpcontno =a.grpcontno))"
						 +" ,(select count(1) from llcase where customerno in(select insuredno from lccont where a.grpcontno = grpcontno) and rgtstate not in ('11','12','14')) "
						 +" From LCGrpCont a,LCGRPpol b , LMRiskApp c, LMRisk d where 1 =1 "
						 +" and a.grpcontno = b.grpcontno "
						 +" and b.riskcode = c.riskcode 	"
						 +"	and  c.riskcode = d.riskcode and D.getflag ='Y'	"
						 +" and RiskType3 !='7' and a.stateflag in('1','3')" 
						 +" and exists (select 1 from lccont e where  a.grpcontno = e.grpcontno and not exists(select 1 from ljsgetdraw where e.contno = contno))    "
						 +" and a.cinvalidate <='"+fm.all("EndDate").value+"'"
						 //+" and exists (select 1 from LPEdorEspecialData where a.grpcontno = edorno and Edortype ='TJ' and detailtype='QUERYTYPE' and edorvalue ='"+fm.all("PayTaskState").value+"') "
						// +" and exists (select 1 from LPEdorEspecialData where a.grpcontno = edorno and Edortype ='TJ' and detailtype='QUERYTYPE' and edorvalue ='"+fm.all("PayTaskState").value+"') "
						 +expression
						 +expression1
						 + getWherePart( 'a.GrpContNo', 'GrpContNo' )    
						 + getWherePart( 'a.appntno', 'AppntNo' )  						 
						 +"	with ur ";	
	
	fm.QuerySql.value = strSQL ;
	turnPage1.queryModal(strSQL, LjsGetGrid);
	//initPolGrid();  
	contCurrentTime=CurrentTime; 
	contSubTime=SubTime; 
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	if(LjsGetGrid.mulLineCount == 0)
	{
	    alert("没有符合条件的可催收保单信息");
	}
	//getType();
}



/*********************************************************************
 *  被选中后显示的集体险种应该是符合条件的
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolInfo()
{
	//modify by fuxin 2008-5-5 11:21:17 少儿险险种和普通险种分开显示。
	var Condition ='';
	if(fm.queryType.value=='2')
	{
		Condition = " and a.riskcode not in ('320106','120706') " ;
	}
	if(fm.queryType.value=='3')
	{
		Condition = " and a.riskcode  in ('320106','120706') " ;
	}
	
	  var tRow = IndiContGrid.getSelNo() - 1;	        
		var tContNo=IndiContGrid.getRowColData(tRow,1);  
		
		var strSQL = "select a.RiskSeqNo,a.InsuredName, (select riskname  from lmrisk where riskcode=a.riskcode) ,a.RiskCode, (select codeName from LDCode where codeType = 'payintv' and code = char(a.PayIntv)), "
			+ "	a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate ,a.enddate "  
			+ " from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
			+ " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
			+ "  and a.contno='" + tContNo + "' "
			+ "  and a.appflag='1' "
			+ Condition
			+ " group by a.polNo,a.RiskSeqNo,a.InsuredName,a.RiskCode, 	a.prem, a.CValiDate , a.PaytoDate, a.PayEndDate ,a.enddate,payintv  "
			+ " order by a.polNo "
			+ " with ur "			
			;
    	turnPage3.queryModal(strSQL, PolGrid); 
}


/*********************************************************************
 *  重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为resetForm()
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm(batchNo)
{
  try
  {   
//  afterContQuery();
	  afterJisPayQuery(batchNo);
	  
  }
  catch(re)
  {
  	alert("在LJSPayPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 



/*********************************************************************
 *  校验日期范围
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkValidateDate()
{
	//校验录入的起始日期和终止日期必须在今天之前，否则不予提交
	var startDate=fm.StartDate.value;
	var endDate=fm.EndDate.value;
	if(startDate==''||endDate=='')
	{
		alert("请录入查询日期范围！");
		return false;
	}
	if(compareDate(startDate,endDate)==1)
	{
		alert("起始日不能晚于截止日!");
		return false;
	}
	return true;
}

	

//催收完成后查询催收纪录
function afterJisPayQuery(batchNo)
{
	// 初始化表格
	//initLjsGetGrid();
	initIndiContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}		  
	if(fm.all("QueryType").value =='2')
	{
	  var strSQL ="	select b.serialno,otherno,grpcontno,grpname,sumgetmoney,curgettodate,getUniteCode(b.agentcode),codename('paymode',paymode) "
							 +"	,(select case when dealstate ='0' then '待给付' when dealstate ='1' then '已给付' end from ljsget where b.getnoticeno=getnoticeno) "
							 +",'未打印',b.getnoticeno "
							 +" From ljsgetdraw a,ljsget b  where 1=1 "
							 +"	and a.getnoticeno = b.getnoticeno	"
							 +" and b.serialno ='"+batchNo+"'"
							 +"	group by b.serialno,otherno,grpcontno,grpname,sumgetmoney,curgettodate,b.agentcode,b.getnoticeno,paymode,dealstate,'打印状态' "
							 ;
  }
  else
  {
  	var strSQL ="	select b.serialno,otherno,contno,grpname,sumgetmoney,curgettodate,getUniteCode(b.agentcode),codename('paymode',paymode) "
			 +"	,(select case when dealstate ='0' then '待给付' when dealstate ='1' then '已给付' end from ljsget where b.getnoticeno=getnoticeno) "
			 +" ,'未打印',b.getnoticeno "
			 +" From ljsgetdraw a,ljsget b  where 1=1 "
			 +"	and a.getnoticeno = b.getnoticeno	"
			 +" and b.serialno ='"+batchNo+"'"
			 //+"	group by b.serialno,otherno,grpcontno,grpname,sumgetmoney,curgettodate,b.agentcode,paymode,dealstate,'打印状态' "
			 ;
  }

	
	turnPage2.queryModal(strSQL, IndiContGrid); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
}


function getCheckdetail()
{
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length < 1)
	{
		return false;	
	}
	fm.all("GrpContNo").value=LjsGetGrid.getRowColDataByName(tChked,'GrpContNo');
	getType();
}

function doBenefit()
{
		if(!checkValidateDate()) return false;
		if(LjsGetGrid.mulLineCount==0)
		{
			alert("没有待给付记录，请先查询！");
			return false ;
		}
		if(LjsGetGrid.mulLineCount>1)
		{
			alert("单份抽档只能选择一条记录！");
			return false ;
		}
		if (fm.all("PayMode").value == 0 && fm.all("GpayMode").value == 0 )
		{
			alert("请选择给付方式！");
			return false ;
		}
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
	  fm.submit();	
}

	function a(tType)
	{
		if(tType==2)
		{
			document.all("x").style.display = "none";
			document.all("y").style.display = "";
		}
		if(tType==3)
		{
			document.all("x").style.display =""
			document.all("y").style.display ="none"
		}
	}
	
function doBatchBenefit()
{
		if(!checkValidateDate()) return false;
		if(LjsGetGrid.mulLineCount==0)
		{
			alert("没有待给付记录，请先查询！");
			return false ;
		}
		if(LjsGetGrid.mulLineCount==1)
		{
			alert("批量抽档不能选择一条记录！");
			return false ;
		}
		if (fm.all("PayMode").value == 0 && fm.all("GpayMode").value == 0 )
		{
			alert("请选择给付方式！");
			return false ;
		}
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    	
	  fm.submit();	
}

function getType()
{
	var sql = " select edorvalue from LPEdorEspecialData where edorno = '"+fm.all("GrpContNo").value+"' and Edortype ='TJ' and detailtype ='QUERYTYPE'";
	var result = easyExecSql(sql);
	if(result == null || result == "")
	{
		return false ;
	}
	else
	{              
		a(result[0][0]);
		fm.QueryType.value = result[0][0];    
	}
}