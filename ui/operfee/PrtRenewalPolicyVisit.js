var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mSql="";

//简单查询
function easyQuery()
{
	var tManageCom = fm.all('ManageCom').value;			//管理机构代码
	var tManageComName = fm.all('ManageComName').value;	//管理机构名称
	var tPolNo = fm.all('PolNo').value;					//保单号
	var tGetNoticeNo = fm.all('GetNoticeNo').value;		//应收记录号
	var tSaleChnl = fm.all('SaleChnl').value;			//保单类型
	var tDealState = fm.all('DealState').value;			//目前状态
	var tStartDate = fm.all('StartDate').value;			//应收时间起期
	var tEndDate = fm.all('EndDate').value;				//应收时间止期

	//管理机构校验
	if(tManageCom == "" || tManageCom == null)
	{
		alert("请选择管理机构！");
		return false;
	}
	//应收时间起期校验
	if(tStartDate == "" || tStartDate == null)
	{
		alert("请输入应收时间起期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("应收时间起期",tStartDate))
		{
			fm.all('StartDate').focus();
			return false;
		}
	}
	//应收时间止期校验
	if(tEndDate == "" || tEndDate == null)
	{
		alert("请输入应收时间止期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("应收时间止期",tEndDate)){
			fm.all('EndDate').focus();
			return false;
		}
	}
	//应收时间起止期三个月校验
	var t1 = new Date(tStartDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>92 )
    {
	  alert("应收时间起止日期不能超过3个月！")
	  return false;
    }
	//初始化Grid
	initRableRecGrid();
    //已抽档
    var tSql1 = "select "
	         +  "(select Name from ldcom where comcode=a.ManageCom),"
	         +  "a.ManageCom,char(a.makedate),"
	         +  "(select Name from labranchgroup where AgentGroup=(select AgentGroup from lccont where ContNo=a.ContNo union select AgentGroup from lbcont where ContNo=a.ContNo)),"
	         +  "a.ContNo,"
	         +  "a.GetNoticeNo,"
	         +	"(select codename from ldcode where codetype='stateflag' " 
	         +	"and code=(select stateflag from lcpol where polno=a.polno union all select stateflag from lbpol where polno=a.polno) ) as 保单状态,"  
	         +  "(select d.AppntName from lccont d where d.ContNo=a.ContNo union select d.AppntName from lbcont d where d.ContNo=a.ContNo),"
	         +  "(select c.Mobile from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Mobile from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.Phone from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Phone from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.zipcode from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.zipcode from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.PostalAddress from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.PostalAddress from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select d.insuredname from lcpol d where d.polno=a.polno union select d.insuredname from lbpol d where d.polno=a.polno),"
	         +  "(select c.Phone from lcaddress c, lcinsured d where c.customerno = d.insuredno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Phone from lcaddress c, lbinsured d where c.customerno = d.insuredno and c.addressno = d.addressno and d.contno=a.contno fetch first 1 rows only),"
	         +  "(select c.PostalAddress from lcaddress c, lcinsured d where c.customerno = d.insuredno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.PostalAddress from lcaddress c, lbinsured d where c.customerno = d.insuredno and c.addressno = d.addressno and d.contno=a.contno fetch first 1 rows only),"
			 +  "(select d.CValiDate from lcpol d where d.PolNo=a.PolNo union select d.CValiDate from lbpol d where d.PolNo=a.PolNo),"//生效日
			 +  "(((year(lastpaytodate)-year((select d.CValiDate from lcpol d where d.PolNo=a.PolNo union select d.CValiDate from lbpol d where d.PolNo=a.PolNo)))*12 "
             +  "+ (month(lastpaytodate)-month((select d.CValiDate from lcpol d where d.PolNo=a.PolNo union select d.CValiDate from lbpol d where d.PolNo=a.PolNo))))/payintv +1 ), " //保单缴次
             +  "a.RiskCode,"
             +  "(select sum(sumduepaymoney) from ljspaypersonb where polno = a.polno and lastpaytodate = a.lastpaytodate group by getnoticeno order by getnoticeno fetch first 1 row only ), " //险种应缴保费
             +  "a.lastpaytodate,"
             +  "nvl((select sum(sumactupaymoney) from ljapayperson where polno = a.polno and lastpaytodate = a.lastpaytodate and getnoticeno = a.getnoticeno group by getnoticeno order by getnoticeno fetch first 1 row only ),0), " //险种实收金额
             +  "(select codename('paymode',d.PayMode) from lccont d where d.ContNo=a.ContNo "
	         +  "union select codename('paymode',d.PayMode) from lbcont d where d.ContNo=a.ContNo),"
	         +	"(select bankname from ldbank where bankcode=a.bankcode) as 开户行,"
	         +	"a.bankaccno as 开户行帐号,"
	         +  "char((select paydate from ljspayb where getnoticeno=a.getnoticeno)),"
	         +  "char((select max(enteraccdate) from ljtempfee where tempfeeno=a.getnoticeno)),"
	         +  "(select codename('dealstate',DealState) from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only), "
	         +	" (case a.dealstate when '2' then (select codename from ldcode where codetype ='feecancelreason' and code = a.Cancelreason) end)," 
             +  "(select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo)," //业务员
	         +  "(select b.Mobile from laagent b where b.AgentCode=(select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo)),"
	         +  "(select b.Phone from laagent b where b.AgentCode=(select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo)),"
	         +  "(select b.Name from laagent b where b.AgentCode=(select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo)),"
	         +  "(select b.Name from laagent b where b.AgentCode=(select AgentCode from lccont where ContNo=a.ContNo union select AgentCode from lbcont where ContNo=a.ContNo)),"
//	         +  "(select AgentCom from lccont where ContNo=a.ContNo union select AgentCom from lbcont where ContNo=a.ContNo),"
//	         +  "(select Name from lacom where AgentCom=(select AgentCom from lccont where ContNo=a.ContNo union select AgentCom from lbcont where ContNo=a.ContNo)),"
	         +	"'','','','',''"
	         +  "from ljspaypersonb a "
	         +  "where "
	         +  "a.riskcode in (select riskcode from lmriskapp where riskperiod = 'L') and "
	         +  "a.sumactupaymoney > 0 and "
	         +  "not exists (select 1 from ljspaypersonb where lastpaytodate = a.lastpaytodate "
	         +  "and polno = a.polno and getnoticeno > a.getnoticeno) and "
	         +  "a.lastpaytodate between '" + tStartDate + "' and '" + tEndDate + "' and "
	         +  "a.managecom like '" + tManageCom + "%' " 
//	         +  "and a.riskcode not in (select riskcode from lmriskapp where RiskType4='4') " 
//	         +  "and a.riskcode not in (select code from ldcode1 where codetype = 'mainsubriskrela' and code1 in (select riskcode from lmriskapp where risktype4 = '4')) ";
	         
    //未抽档
	var tSql2 = "select "
	         +  "(select Name from ldcom where comcode=a.ManageCom),"
	         +  "a.ManageCom,"
	         +	"'',"
			 +  "(select Name from labranchgroup where AgentGroup=a.AgentGroup),"
			 +  "a.ContNo,"
			 +  "'',"
			 +	"(select codename from ldcode where codetype='stateflag' " 
	         +	"and code=a.stateflag) as 保单状态,"
			 +  "a.AppntName,"
	         +  "(select c.Mobile from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Mobile from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.Phone from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Phone from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.zipcode from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.zipcode from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +  "(select c.PostalAddress from lcaddress c, lcappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.PostalAddress from lcaddress c, lbappnt d where c.customerno = d.appntno and c.addressno = d.addressno and d.contno=a.contno),"
	         +	"a.insuredname as 被保险人姓名,"
	         +  "(select c.Phone from lcaddress c, lcinsured d where c.customerno = d.insuredno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.Phone from lcaddress c, lbinsured d where c.customerno = d.insuredno and c.addressno = d.addressno and d.contno=a.contno fetch first 1 rows only),"
	         +  "(select c.PostalAddress from lcaddress c, lcinsured d where c.customerno = d.insuredno and c.addressno = d.addressno and d.contno=a.contno "
	         +  " union select c.PostalAddress from lcaddress c, lbinsured d where c.customerno = d.insuredno and c.addressno = d.addressno and d.contno=a.contno fetch first 1 rows only),"
			 +  "a.CValiDate,"
			 +  "(((year(paytodate)-year(cvalidate))*12 + (month(paytodate)-month(cvalidate)))/payintv +1 ), " //保单缴次
			 +	"a.riskcode,"
			 +  "a.Prem,"
			 +  "a.PaytoDate,"
			 +	"0,"
			 +  "(select codename('paymode',a.PayMode) from dual),"
			 +  "'',"
			 +  "'',"
			 +	"'',"
			 +	"'',"
			 +	"'未抽档',"
			 +	"'',"
			 +  "a.AgentCode,"
			 +  "(select b.Mobile from laagent b where b.AgentCode=a.AgentCode),"
	         +  "(select b.Phone from laagent b where b.AgentCode=a.AgentCode),"
	         +  "(select b.Name from laagent b where b.AgentCode=a.AgentCode),"
	         +  "(select b.Name from laagent b where b.AgentCode=a.AgentCode),"
	         +	"'','','','',''"
	         +  "from lcpol a "
			 +  "where "
			 +  "not exists (select 1 from ljspaypersonb where polno=a.polno and lastpaytodate=a.paytodate) and "
			 +  "a.conttype='1' and "
			 +  "a.stateflag='1' and "
	         +  "a.riskcode in (select riskcode from lmriskapp where riskperiod = 'L') and "
	         +  "a.prem <> 0 and "
			 +  "a.paytodate between '" + tStartDate + "' and '" + tEndDate + "' and "
			 +  "a.paytodate<a.payenddate and "
	         +  "a.managecom like '" + tManageCom + "%' "
//	         +  "and a.riskcode not in (select riskcode from lmriskapp where RiskType4='4') " 
//	         +  "and a.riskcode not in (select code from ldcode1 where codetype = 'mainsubriskrela' and code1 in (select riskcode from lmriskapp where risktype4 = '4')) "
	         ;
	         
    if(tSaleChnl == "1")
    {
    	tSql1 += "and not exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) ";
    	tSql1 += "and not exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')) ";
    	tSql2 += "and a.salechnl not in ('04','13') ";
    }
    if(tSaleChnl == "2")
    {
    	tSql1 += "and (exists(select 1 from lccont where contno = a.contno and salechnl in ('04','13')) or exists(select 1 from lbcont where contno = a.contno and salechnl in ('04','13')))";
    	tSql2 += "and a.salechnl in ('04','13') ";
    }
    if(tPolNo != "" && tPolNo !=null)
    {
    	tSql1 += "and a.contno='" + tPolNo + "' ";
    	tSql2 += "and a.contno='" + tPolNo + "' ";
    }
    if(tGetNoticeNo != "" && tGetNoticeNo != null)
    {
    	tSql1 += "and a.getnoticeno = '" + tGetNoticeNo + "' ";
    	tSql2 += "and 1 = 2 ";
    }
    
    var groupSql = " group by a.ManageCom, a.ContNo, a.RiskCode, a.PolNo, a.GetNoticeNo, a.lastpaytodate, a.payintv,a.makedate,a.bankaccno,a.bankcode,a.Cancelreason,a.dealstate  ";

    switch(tDealState)
    {
    	case "0":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='0' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "1":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='1' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "2":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='2' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "3":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='3' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "4":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='4' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "5":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='5' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "6":
			tSql1 += "and (select DealState from ljspayb where getnoticeno = a.getnoticeno fetch first 1 row only)='6' ";
			tSql1 += groupSql;
    		mSql = tSql1;
    		break;
    	case "7":
			mSql = tSql2;
    		break;
    	default:
    	    tSql1 += groupSql;
    		mSql = tSql1 + " union all " +tSql2;
    		break;
    }
	turnPage1.queryModal(mSql, RableRecGrid);  
	if( RableRecGrid.mulLineCount == 0)
	{
		alert("没有查询到数据");
		return false;
	}
	showCodeName(); 
}
function easyPrint()
{	
	if( RableRecGrid.mulLineCount == 0)
	{
		alert("没有需要打印的信息！");
		return false;
	}
	//传递页面信息
	fm.all('strsql').value=mSql;
	fm.submit();
}
//日期格式校验
function checkDateFormat(tName,strValue) {
	if (!isDate(strValue))
	{
		alert("输入的["+tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}