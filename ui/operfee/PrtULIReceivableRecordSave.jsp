<%@page contentType="text/html;charset=GBK" %>
<%@page pageEncoding="GBK"%>
<%request.setCharacterEncoding("GBK");%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/Download.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "万能续期应收记录统计_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    //分割页面信息
//    String[] tArrString = request.getParameter("arrset").split(",");
//    System.out.println("Debug:tArrString-"+tArrString.length);
	//初始化数组下标
//    int tRow=-1;
	//取得页面信息总行数
//    int tArrRowLen = tArrString.length / 28;
	//取得页面信息总单元数
//    int tArrColLen = tArrString.length - 1;
//    String[][] tArrSet = new String[tArrRowLen][28];
//    System.out.println("Debug:tArrLen-"+tArrRowLen);
//    for(int tCol = 0;tCol < tArrColLen;tCol++)
//    {
//        System.out.println("Debug:tCol-"+tCol);
//        if(tCol%28 == 0)  tRow++;
//		tArrSet[tRow][tCol%28] = tArrString[tCol];
//    }
    
    String tSQL = request.getParameter("strsql");
	System.out.println("打印查询:"+tSQL);
    //设置表头
    String[][] tTitle = {{"管理机构","管理机构代码","营销部门","保单号","险种代码","应收记录号","投保人","移动电话","联系电话","投保人联系地址","保单生效日","应缴基础保费","应缴额外保费","实收基础保费","实收额外金额","应收时间","收费方式","代理人编码","代理人手机","代理人固话","代理人姓名","代理机构编码","代理机构名称","缴费截止日期","实收日期","核销日期","销售渠道","保单类型","状态","保单缴次","险种缴费年限","原代理人代码","原代理人","险种状态","保单服务状态"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,0,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
    if(createexcellist.setData(tSQL,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>