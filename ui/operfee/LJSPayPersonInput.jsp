<html> 
<%
//程序名称：LJSPayPersonInput.jsp
//程序功能：应收个人交费表的输入
//创建日期：2002-07-12 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LJSPayPersonInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LJSPayPersonInit.jsp"%>
</head>
<body  onload="initForm();" >

<form name=fmQuery action=./LJSPayPersonQueryResult.jsp target=fraSubmit method=post>

	<td class=common width="35%">保单号码</td>
	<td height="30" width="65%">
	<input class=common name=PolNo value="" maxlength=20 >
	</td>
	</td>	
					                                                                            
</form>
      <INPUT VALUE="查询信息" TYPE=button onclick="fmQuery.submit()"> 	
      <INPUT VALUE="保存信息" TYPE=button onclick="save()">
      
<form action="./LJSPayPersonSave.jsp" method=post name=fm target="fraInterface">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <Div  id= "divLJSPayPerson" style= "display: ''">
      <table  class= common >             
        <TR  class= common>      
          <TD  class= title>
            投保人客户号码
          </TD>
          <TD  class= input>
            <Input class= common name=AppntNo readonly>
          </TD>          
          <TD  class= title>
            投保人姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name readonly>
          </TD>
        </TR>          
        <TR class= common>                                        
          <TD  class= title>
            总应交金额
          </TD>         
          <TD  class= input>
            <Input class= common name=SumDuePayMoney readonly>
          </TD>           
          <TD  class= title>
            业务员
          </TD>
         <TD  class= input>
            <Input class= common width:5px name=Operateor >
          </TD>          
        </TR>                
      </table>
    </Div>     
      <table  class= common>  
       <TR  class= common>
          <TD  class= title>
            责任编码
          </TD>
          <TD  class= title>
            交费计划编码
          </TD>          
          <TD  class= title>
            总实交金额
          </TD>          
          <TD  class= title>
            交费日期
          </TD>
          <TD  class= title>
            交费次数
          </TD>
       </TR>
     </table>

   <TD>
     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJSPayPerson1);">
   </TD>      
    <Div  id= "divLJSPayPerson1" style= "display: ''">             
     <table  class= common>                
          <TD  class= input>
            <Input class= common  name=DutyCode1 readonly >
          </TD>
          <TD  class= input>
            <Input class= common  name=PayPlanCode1 readonly>
          </TD>                
          <TD  class= input>
            <Input class= common width="5px" name=SumActuPayMoney1 >
          </TD>  
          <TD  class= input>
            <Input class= common width="5%" name=PayDate1 >
          </TD>
          <TD  class= input>
            <Input class= common width="10%" name=PayCount1 readonly >
          </TD>                                       
      </table>      
    </Div>
    <TR></TR>
  <span id="spanLCPremCode"  style="display:''; position:absolute; slategray"></span>
        <TD>
          <input type=hidden name=PolNo >
          <input type=hidden name=Transact >
          <input type=hidden name=tableNum >
        </TD> 
  </form>
</body>
</html>
