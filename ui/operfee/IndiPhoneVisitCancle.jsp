<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2002-07-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="java.lang.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.finfee.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  
  IndiVisitLJSCancelUI tIndiVisitLJSCancelUI = new IndiVisitLJSCancelUI();
  try
  {
  	//session
  	GlobalInput tGI =(GlobalInput)session.getValue("GI");
  
  	//应收号
  	String tGetNoticeNo=request.getParameter("SubmitNoticeNo");	
  	String tDetailWorkNo=request.getParameter("DetailWorkNo");	
  	
  	LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
  	tLGPhoneHastenSchema.setGetNoticeNo(tGetNoticeNo);
  	tLGPhoneHastenSchema.setWorkNo(tDetailWorkNo);
  
  	TransferData tTransferData= new TransferData();
  	tTransferData.setNameAndValue("CancelMode","0");
  
  	VData tVData = new VData();
  	tVData.addElement(tLGPhoneHastenSchema);
  	tVData.addElement(tGI);
  
  	tIndiVisitLJSCancelUI.submitData(tVData,"INSERT");
  }
  catch(Exception ex)
  {
  	Content = "失败，原因是:" + ex.toString();
  	FlagStr = "Fail";
  }
  
  if (FlagStr=="")
  {
  	tError = tIndiVisitLJSCancelUI.mErrors;
  	if (tError == null || !tError.needDealError())
  	{
  		Content = " 处理成功";
  		FlagStr = "Succ";
  	}
  	else
  	{
  		Content =" 失败，原因是:" + tError.getFirstError();
  		FlagStr = "Fail";
  	}
  }
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>