<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：ULIHuanPrint.jsp
//程序功能：万能缓交状态打印
//创建日期：2009-7-23
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!—页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <SCRIPT src="ULIHuanPrint.js"></SCRIPT> 
  <%@include file="ULIHuanPrintInit.jsp"%> 
  
  <title>万能缓交状态打印</title>
</head>
<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form method=post name=fm target="fraSubmit">
    <table class= common align=center>
      <tr>
	    <td class=titleImg colspan=3>请输入查询条件</td>
	  </tr>
	  <tr  class= common>
        <td  class= title>管理机构</td>
        <td  class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename readonly name=ManageComName></td>
        <td class= title>起始时间</td>
        <td  class= input><input class=coolDatePicker3 name="StartDate" verify="起始时间|date&notnull" style="width:130"></td>
        <td class= title>终止时间</td>
        <td  class= input><input class=coolDatePicker3 name="EndDate" verify="终止时间|date&notnull" style="width:130"></td>
      </tr>   
      <tr class= common>
        <td class= title>客户号</td>
        <td class= input><input class=common name="AppntNo"></td>
        <td class= title>保单号</td>
        <td class= input><input class=common name="ContNo"></td>
        <td class= title>打印状态</td>
      	<td class= input><Input class= "codeno"  name=PrintState  CodeData="0|^0|未打印^1|已打印^2|全部" verify="打印状态|notnull" ondblclick="return showCodeListEx('PrintState',[this,PrintStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PrintState',[this,PrintStateName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename  name=PrintStateName readonly></td>
      </tr>
      <tr class= common>
        <td class=button><input class=cssButton value="查  询" type=button onclick="query();"></td>
      </tr>
    </table>
    <table>
      <tr>
        <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol);"></td>
	    <td class=titleImg>保单列表</td>
	  </tr>
    </table>
  <!-- 信息（列表） -->
  <div id="divLCPol" style="display:''">
	  <table class=common>
        <tr class=common>
	      <td text-align:left colSpan=1><span id="spanLCPolGrid"></span></td>
	    </tr>
    </table>
  </div>

  <div id="divPage2" align=center style="display: 'none' ">
	  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
	  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </div>
  <br>
  <table>
    <tr class=common>
      <td class=button><input class=cssButton value="打  印" type=button onclick="print();"></td>
    </tr>
  </table>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden name=ContNo1>
    <input type=hidden name=AppntNo1>
  </form>
<!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
