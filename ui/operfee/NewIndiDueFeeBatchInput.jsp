 <html> 
<%
//程序名称：IndiDueFeeInput.jsp
//程序功能：个人保费催收，实现数据从保费项表到应收个人表和应收总表的流转
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="NewIndiDueFeeInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body >

<form name=fmMulti action=./NewIndiDueFeeMultiQuery.jsp target=fraSubmit method=post>
<!-- 显示或隐藏信息 -->      	

      
     <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divPersonmulti);">
      </td>
      <td class= titleImg>
        请录入个单首期批量催收日期范围
      </td>
     </tr>
    </table>
    <Div  id= "divPersonmulti" style= "display: ''">
    <table class= common>
       <TR class= common>
         <TD  class= title>
            起始日期 
          </TD>
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="StartDate" >           
          </TD>
        <TD  class= title>
          终止日期 
          </TD>        
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="EndDate" >                                 
          </TD>
        </TR>  
  
        <TR class= common>  
        	
				<TD  class= title> 银行编码</TD><TD  class= input>
				<Input class= "codeno"  name=bankcode  ondblclick="return showCodeList('bank',[this,bankname],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bank',[this,bankname],[0,1],null,null,null,1);" ><Input class=codename  name=bankname></TD>
         </TR>   
          <TR  class= common>
        <TD>
          <INPUT VALUE="个单首期批量催收" TYPE=button class=common onclick="PersonMulti()">
          <!--<INPUT VALUE="特别约定催收" TYPE=button class=common onclick="SpecPersonMulti()">-->           
        </TD>     
        </TR> 
      </Table>    
    </Div>   					                                                                            
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
