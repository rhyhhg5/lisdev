<%
//程序名称：IndiDueFeeQuery.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
  <%@page import="java.util.*"%>
  <%@page import="java.lang.*"%>  
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.xb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>  
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>  

<%
 
  //保存保单号  
  String ContNo = request.getParameter("ContNoSelected"); 
  String StartDate=request.getParameter("StartDate");
  String EndDate=request.getParameter("EndDate");//传入后台逻辑处理层做校验
  String queryType = request.getParameter("queryType");//传入后台逻辑处理层做校验
  
  System.out.println("\n\n" + ContNo);
  System.out.println(StartDate);
  System.out.println(EndDate);
  
// 输出参数
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  System.out.println(tGI.Operator);
  System.out.println(tGI.ComCode);
  System.out.println(tGI.ManageCom);
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    LCContSchema  tLCContSchema = new LCContSchema();  // 个人保单表
    tLCContSchema.setContNo(ContNo);
    //杨红于2005-07-16 添加页面输入的  起始时间   和终止时间  ，目的是传到后台作校验动作。
    TransferData tempTransferData=new TransferData();
    tempTransferData.setNameAndValue("StartDate",StartDate);
    tempTransferData.setNameAndValue("EndDate",EndDate);
    tempTransferData.setNameAndValue("queryType",queryType);
    
    VData tVData = new VData();
    tVData.add(tLCContSchema);
    tVData.add(tGI);
    tVData.add(tempTransferData);
    
    PRnewDueFeeUI tPRnewDueFeeUI = new PRnewDueFeeUI();
    tPRnewDueFeeUI.submitData(tVData,"INSERT");
    tError = tPRnewDueFeeUI.mErrors;
    if(tError == null || !tError.needDealError())
    {
	     Content="处理成功！";
	     FlagStr="Succ";
    }
    else
    {
	     Content="处理失败！原因："+tError.getErrContent();
	     FlagStr="Fail";
    }  
  }
  Content = PubFun.changForHTML(Content);

%>                                      
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>

