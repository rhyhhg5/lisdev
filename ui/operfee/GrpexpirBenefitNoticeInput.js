//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var expression ="" ;

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,batchNo)
{
  try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function CheckDate()
{
  if(!isDate(fm.all('StartDate').value)||!isDate(fm.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fm.all('StartDate').value,fm.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

// 查询按钮
/*********************************************************************
 *  查询合同号
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	initLjsGetGrid();
	initIndiContGrid();
	initLCInsuredGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	
	//得到所选机构
	if(fm.ManageCom.value == "")
	{
	  alert("请录入机构。");
	  return fasle;
	}
	managecom = fm.ManageCom.value;
	
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}	
	var strSQL ="select a.grpcontno,a.grpname,a.peoples2,'',b.payenddate,getUniteCode(a.agentcode), "
	           +"(select case EdorValue when '2' then '团险统一给付' else '个人给付' end from LPEdorEspecialData "
	           +"where Edorno = a.grpcontno and EdorType = 'TJ' and DetailType = 'QUERYTYPE'), "
	           +"(select case count(1) when 0 then '未回销' else '已回销' end from LPEdorEspecialData "
	           +"where Edorno = a.grpcontno and EdorType = 'TJ' and DetailType = 'QUERYTYPE') "
	           +" From LCGrpCont a,LCGRPpol b, LMRiskApp c where 1 =1 "
			   +" and a.grpcontno = b.grpcontno "
			   +" and b.riskcode = c.riskcode 	"
			   +"	and  GetChgFlag='Y' 	"
			   +" and RiskType3 !='7' " 
			   +" and a.stateflag in('1','3') "
			   +" and b.payenddate between '"+ CurrentTime +"' and '" + SubTime + "' "
			   +" and a.ManageCom like '%"+fm.all("ManageCom").value +"%' "
			   + getWherePart( 'a.GrpContNo', 'GrpContNo' )   
			   + getWherePart( 'a.appntno', 'AppntNo' )  
			   +"	with ur ";	
	turnPage3.queryModal(strSQL, LjsGetGrid);
	if(LjsGetGrid.mulLineCount == 0)
	{
	    alert("没有符合条件的可给付保单信息");
	}
}


/*********************************************************************
 *  校验日期范围
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkValidateDate()
{
	//校验录入的起始日期和终止日期必须在今天之前，否则不予提交
	var startDate=fm.StartDate.value;
	var endDate=fm.EndDate.value;
	if(startDate==''||endDate=='')
	{
		alert("请录入查询日期范围！");
		return false;
	}
	if(compareDate(startDate,endDate)==1)
	{
		alert("起始日不能晚于截止日!");
		return false;
	}
	return true;
}

function getIndiContGrid(parm1, parm2)
{
  var tempGrpContNo = fm.all('TempGrpContNo').value;
  if(tempGrpContNo!="" && tempGrpContNo!=null && tempGrpContNo!="null")
  {
    fm.action="DeleteWorkInfo.jsp";
    fm.submit(); 
  }
  var tGrpContNo = fm.all(parm1).all('LjsGetGrid1').value;
  fm.all('GrpContNo').value = tGrpContNo;
  fm.all('TempGrpContNo').value = tGrpContNo;
  queryContData(tGrpContNo);
  queryImportData(tGrpContNo);
}

function printOneNotice()
{
	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印");
		return false;	
	}
		
	var PrtSeq = easyExecSql("select min(prtseq) from LOPRTManager where code = 'bq002' and otherno ='"+LjsGetGrid.getRowColData(tChked[0],1)+"' ");
    //PDF打印用
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		fm.action="GrpExpBnfNoticePrt.jsp";
	    fm.submit();
	}
	else
    {
		fm.action = "../uw/PrintPDFSave.jsp?Code=0bq002&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+PrtSeq+"&PrtSeq="+PrtSeq;
		fm.submit();
	}
}

function printAllNotice()
{
//	alert("该功能暂时不能使用");
//	return false;

	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{	
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.all('CheckAll').value="Y";
	}
	else
	{
		fm.all('CheckAll').value="N";
	}
//	alert("here......."+fm.all('CheckAll').value+".........checkall");
	
	fm.action="./ExpBenefitAllNoticePrt.jsp";
	fm.submit();
	
}

//新的接口
function printPDFNotice(aCode,aPrtSeq)
{ 
	fm.action="../uw/PrintPDFSave.jsp?Code="+aCode+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+aPrtSeq+"&PrtSeq="+aPrtSeq;
	fm.submit();
}

//磁盘导入 
function importInsured()
{
    var tSel = LjsGetGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
	{
	    alert( "请先选择团体保单号，再做导入！" );
	    return false ;
	}	
	if(fm.all("TempGrpContNo").value == null || fm.all("TempGrpContNo").value == "")
	{
		alert("请先选择团体保单号，再做导入！");	
		return false ;
	}
	
	var sql = " select edorvalue from LPEdorEspecialData where edorno = '"+fm.all("TempGrpContNo").value+"' and Edortype ='TJ' and detailtype ='QUERYTYPE'";
	var result = easyExecSql(sql);
	if(result == null || result == "")
	{
		alert("未录入回执，不可以变更账户信息！");
		return false ;
	}
	
	var sql= "select WorkNo from LGWork where ContNo = '" + fm.all("TempGrpContNo").value+ "' " 
	       + "and InnerSource = 'LPJ' and AcceptWayNo = '8' order by MakeDate desc with ur ";
	var rs = easyExecSql(sql);
	if(rs)
	{
	  afterImportInsured(rs[0][0]);
	}
	else
	{
	  fm.action ="CreateGrpWorkNo.jsp";
	  fm.submit();
	}
}

function afterImportInsured(edorno)
{
  fm.all('EdorNo').value = edorno; 
  var url = "../bq/BqDiskImportMain.jsp?EdorNo=" + edorno + 
	          "&EdorType=LP" +
	          "&GrpContNo=" + fm.all("TempGrpContNo").value;
  var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
  window.open(url, "理赔金帐户导入", param);
}

//查询导入的数据
function queryImportData(aGrpContNo)
{
  var sql= "select WorkNo from LGWork where ContNo = '" + aGrpContNo+ "' " 
	       + "and InnerSource = 'LPJ' and AcceptWayNo = '8' order by MakeDate desc with ur ";
  var rs = easyExecSql(sql);
  if(rs)
  {
	  fm.all('EdorNo').value = rs[0][0];
  }	
  var sql = "select GrpContNo, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + aGrpContNo + "' " +
	             "and State = '1' " +
	             "and Edorno = '" + fm.all('EdorNo').value + "' "
	             "order by Int(SerialNo) ";
  turnPage2.pageDivName = "divPage1";
  turnPage2.queryModal(sql, LCInsuredGrid); 
}

//保存
function save()
{
  var sql= "select WorkNo from LGWork where ContNo = '" + fm.all("TempGrpContNo").value+ "' " 
	       + "and InnerSource = 'LPJ' and AcceptWayNo = '8'  order by MakeDate desc with ur ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.all('EdorNo').value = rs[0][0]; 
  }
  if (LCInsuredGrid.mulLineCount == 0)
  {
    alert("没有有效的导入数据！");
    return false;
  }
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "GEdorTypeLPSave.jsp"
  fm.submit();
}

function afterSave(FlagStr, content)
{
  try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  queryContData(fm.all('TempGrpContNo').value);
  queryImportData(fm.all('TempGrpContNo').value);
}

function queryContData(aGrpContNo)
{
  var sql = "select a.Name, a.InsuredNo,a.Sex, a.IDType, a.IDNo, a.Birthday,a.BankCode,a.AccName,a.BankAccNo " 
          + "from LCInsured a, LCCont b " 
          + "where a.ContNo = b.ContNo " 
	      + "and b.AppFlag = '1' " 
	      + "and a.GrpContNo = '" + aGrpContNo + "' " 
	      + "order by InsuredNo ";
  turnPage1.queryModal(sql, IndiContGrid);
}

function saveType()
{
  var tSel = LjsGetGrid.getSelNo();	
  if(tSel == 0 || tSel == null)
  {
    alert( "请先选择团体保单号，再修改给付方式！" );
    return false ;
  }
  var BackState = LjsGetGrid.getRowColDataByName(LjsGetGrid.getSelNo()-1,"BackState")
  if(BackState!=null && BackState=="已回销")
  {
    alert("该保单已经回销，不能再次录入给付方式！");
	return false ;
  }
  if(fm.all("queryType").value =="" || fm.all("queryType").value=="1" || fm.all("queryType").value=="0")
  {
	alert("请选择给付方式");
	return false ;	
  }
  if(fm.all("queryType").value == "2")
  {
    if(fm.all("GPayMode").value == "" || fm.all("GPayMode").value == "0")
    {
      alert("请选择付费方式");
	  return false ;
    }
  }
  else if(fm.all("queryType").value == "3")
  {
    if(fm.all("PayMode").value == "" || fm.all("PayMode").value == "0")
    {
      alert("请选择付费方式");
	  return false ;
    }
  }
  fm.action = "GrpExpirTypeSave.jsp"
  fm.submit();
}

function afterSaveType(FlagStr, content)
{
  try{showInfo.close();}catch(e){}
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  easyQueryClick();
}

function printList()
{
	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印");
		return false;	
	}
	window.open("../operfee/GrpExpirBenefitList.jsp?GrpContNo="+fm.TempGrpContNo.value);	
}

function afterCodeSelect( cCodeName, Field )
{
  if( cCodeName == "queryType")
  {
    switch(fm.all('queryType').value)
    {
      case "0":
      fm.PayMode.style.display = "none";
      fm.PayModeName.style.display = "none";
      fm.GPayMode.style.display = "none";
      fm.GPayModeName.style.display = "none";
      window.focus();              
      break;
      case "1":
      fm.PayMode.style.display = "none";
      fm.PayModeName.style.display = "none";
      fm.GPayMode.style.display = "none";
      fm.GPayModeName.style.display = "none";
      window.focus();              //选择类型
      break;
      case "2":
      fm.PayMode.style.display = "none";
      fm.PayModeName.style.display = "none";
      fm.GPayMode.style.display = "";
      fm.GPayModeName.style.display = "";
      window.focus();              //团险统一给付
      break;
      case "3":
      fm.PayMode.style.display = "";
      fm.PayModeName.style.display = "";
      fm.GPayMode.style.display = "none";
      fm.GPayModeName.style.display = "none";
      window.focus();              //个人给付
      break;
    }
  }
}

function printOneNoticeNew()
{
	if (LjsGetGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LjsGetGrid.mulLineCount; i++)
	{
		if(LjsGetGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印");
		return false;	
	}
		
	//var PrtSeq = easyExecSql("select min(prtseq) from LOPRTManager where code = 'bq002' and otherno ='"+LjsGetGrid.getRowColData(tChked[0],1)+"' ");
    //PDF打印用
	//if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	//{
	//	fm.action="GrpExpBnfNoticePrt.jsp";
	//    fm.submit();
	//}
	//else
  //  {
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=bq002&OtherNo="+LjsGetGrid.getRowColData(tChked[0],1);
	fm.submit();
	//}
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}
