  //               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();
var showInfo;
var strSql="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{	
	initContPreserveGrid();
	//判断关键定是否为空
	var strAgentCom=trim(document.fm.all("ManageCom").value);
	var riskCode=trim(document.fm.all("riskCode").value);//险种号
	var ContNo = trim(document.fm.all("ContNo").value);//保单号
	var strSQL1 ="";
	//初始化防止出错
	if(strAgentCom==null||strAgentCom=='')
	{
		window.alert("请选择查询机构");
	}
	if(riskCode==null||riskCode=='')
	{
		window.alert("请选择查询险种");
	}else if(riskCode=='0'){
		 strSQL1 = " and riskcode in ('333701','332601','333001') "; 
	}else{
		strSQL1 = "  and riskcode = '"+riskCode+"' ";
	}
	if(ContNo!=null&&ContNo!=""){
		//判断保单是否留存
		var checkSQL = "select 1 from lccontremain where contno='"+ContNo+"'";
		
		var result =easyExecSql(checkSQL);
		
		if (result != null&&result[0][0]=="1"){
			alert("该保单已留存");
			fm.checkAllContPreserveGrid.style.display="none";
			return false;
		} 
		
		
		strSQL1 += " and lcc.contno='"+ContNo+"' ";
	}

			  //查询strSql语句如下
			  	
				  strSql=" select (select name from ldcom where comcode=lcc.managecom), "+
					" lcc.contno, "+
					" (select riskname from lmriskapp where riskcode =lcp.riskcode), "+
					" lcc.appntname,lcc.insuredname,lcc.cvalidate,lcc.prem, "+
					" (select Phone from lcaddress a,lcappnt b where a.customerno =b.appntno "+
					" and a.addressno=b.addressno and contno=lcc.contno), "+
					" (select PostalAddress from lcaddress a,lcappnt b where a.customerno =b.appntno "+
					"  and a.addressno=b.addressno and contno=lcc.contno), "+
					" (select name from lacom where agentcom = lcc.agentcom) "+
					" from lccont lcc inner join lcpol lcp on lcc.prtno = lcp.prtno "+
					" where lcc.conttype = '1'"+
					" and lcc.salechnl in ('04','13') "+
					" and lcc.appflag='1' and lcc.stateflag='1' "+
					" and (lcc.cvalidate + 1 years) <= current date "+
					" and (lcc.cvalidate + 2 years) >=current date " +
					" and not exists (select 1 from lccontremain where contno=lcc.contno) "+
					" and lcc.managecom like '"+strAgentCom+"%' "
					+strSQL1+" with ur  ";
			


	 //查询SQL，返回结果字符串
	 turnPage.strQueryResult=easyQueryVer3(strSql, 1, 0, 1); 
	 
	 if(!turnPage.strQueryResult)
	 {
	 	window.alert("没有满足条件的记录!");
	 	return false;
	 }
	 else
	 {
		 initContPreserveGrid();
	 	turnPage.queryModal(strSql,ContPreserveGrid);
	 	fm.checkAllContPreserveGrid.style.display="none";

	 }
}


function doRemain(){

	var chkNum = 0;
	
	for (i = 0; i < ContPreserveGrid.mulLineCount; i++)
	{
		if (ContPreserveGrid.getChkNo(i)) 
		{
		  chkNum = chkNum + 1;
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一个保单再留存！");
		return false;
	}
	  else
	  {
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

			fm.action="./ContPreserveSubmit.jsp";
			fm.submit();
   }  	    
}
function afterSubmit(FlagStr, content)
{
  try{ showInfo.close(); } catch (e) {};
  window.focus();     
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
}

function easyPrint()
{	
	if( ContPreserveGrid.mulLineCount == 0)
	{
		window.alert("没有需要打印的信息");
		return false;
	}
	fm.all('strsql').value=strSql;
	fm.submit();
}
