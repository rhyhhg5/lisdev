
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PlInFileSave.jsp
//程序功能：职场管理录入文件上传
//创建日期：2012/2/20
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.wpleace.PlFeeInBL"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%

//输出参数         
 String FlagStr = "";
 String Content = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getAttribute("GI");
 //输入参数
 //××××Schema t××××Schema = new ××××Schema();
 String FileName = "";
 String FileNameFinal="";
 String FileNameType="";
 String FileType="";
 String SavePath="";
 String Path_Fn = "";
	
	//得到文件的保存路径
 	String ImportPath = "/wplease/file/";		
 	String path =application.getRealPath("").replace('\\','/');
 	path=path.trim()+ImportPath.trim();
 	System.out.println("path==========="+path);
 	String FullName = new String(request.getParameter("ImportFile").getBytes("iso8859-1"),"GBK"); 
 	String CodeId = request.getParameter("CodeId");

 	
	System.out.println("ImportPath: "+ImportPath);
	System.out.println("Path: "+path);
	System.out.println("FullName: "+FullName);
	System.out.println("CodeId: "+CodeId);

DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
fu.setSizeMax(10000000);
// maximum size that will be stored in memory?
// 设置最多只允许在内存中存储的数据,单位:字节
fu.setSizeThreshold(4096000);
// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
fu.setRepositoryPath(path+"temp");
//开始读取上传信息
List fileItems = null;
try{
 fileItems = fu.parseRequest(request);
}
catch(Exception ex)
{
    System.out.println("报错了！！！");
	ex.printStackTrace();
}

// 依次处理每个上传的文件 
Iterator iter = fileItems.iterator();
while (iter.hasNext()) {
    FileItem item = (FileItem) iter.next();
    //忽略其他不是文件域的所有表单信息
    if (!item.isFormField()) {
    String name = item.getName(); 
    long size = item.getSize();
    if((name==null||name.equals("")) && size==0){
      continue;
      }
    SavePath= path;
    FileNameType = FullName.substring(FullName.lastIndexOf("\\") + 1);
    FileName = FileNameType.substring(0,FileNameType.lastIndexOf("."));
    FileType =FileNameType.substring(FileNameType.lastIndexOf("."));
    System.out.println("FileType======="+FileType);
    FileNameFinal=CodeId.trim()+FileType.trim();
    System.out.println("-----------Path:"+ SavePath);
    System.out.println("-----------filename:"+ FileNameFinal);
    //保存上传的文件到指定的目录
    Path_Fn = SavePath + FileNameFinal;
    System.out.println("-----------Path_Fn:"+Path_Fn);
    try {
       item.write(new File(Path_Fn));
       Content = " 保存成功! ";
       FlagStr = "Success";
    } catch(Exception e) {
      System.out.println("upload file error ...");
      Content = " 保存失败!";
      FlagStr = "Fail";
    }
  }
} 
if (!FlagStr.equals("Fail"))
  {
  	//从磁盘导入被保人清单
  	PlFeeInBL tPlFeeInBL=new PlFeeInBL(CodeId,tG);
  	if (!tPlFeeInBL.doUp(path,FileNameFinal))
  	{
  	    FlagStr = "Fail";
  		Content = tPlFeeInBL.mErrors.getFirstError();
  		System.out.println(tPlFeeInBL.mErrors.getFirstError());
  	}
  	else
  	{
  	    FlagStr = "Succ";
  		Content = "费用导入成功！";
  	}
  }
 //添加各种预处理
%>                      
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmitFee("<%=FlagStr%>","<%=Content%>","<%=FileNameFinal%>");
</script>
</html>
