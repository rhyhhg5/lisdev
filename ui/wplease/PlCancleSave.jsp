<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PlCancleSave.jsp
//程序功能：
//创建日期：2011-06-22 15:12:33
//创建人  ：huodonglei
//更新记录：  更新人    更新日期     更新原因/内容
// 鞠成富 2012-09-07 修改
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.wpleace.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page contentType="text/html;charset=GBK"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LIPlaceRentInfoBSchema mLIPlaceRentInfoBSchema = new LIPlaceRentInfoBSchema();
  LIPlaceRentInfoBSet mLIPlaceRentInfoBSet = new LIPlaceRentInfoBSet();
  
  LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchema = new LIPlaceRentFeeBSchema();
  LIPlaceRentFeeBSet mLIPlaceRentFeeBSet = new LIPlaceRentFeeBSet();
  
  LIPlaceChangeTraceSchema mLIPlaceChangeTraceSchema = new LIPlaceChangeTraceSchema();
  LIPlaceChangeTraceSet mLIPlaceChangeTraceSet = new LIPlaceChangeTraceSet();
  
 /*  LIPlaceEndInfoSchema mLIPlaceEndInfoSchema = new LIPlaceEndInfoSchema();
  LIPlaceEndInfoSet mLIPlaceEndInfoSet = new LIPlaceEndInfoSet(); */
  
  PlCancleUI mPlCancleUI  = new PlCancleUI();

  //输出参数
  CErrors tError = new CErrors();
  String tOperate="CANCLE";
  String FlagStr = "true";
  String Content = "保存成功";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tPlaceno = request.getParameter("Placeno");

  
  //变更轨迹信息
  String tStopDate = request.getParameter("StopDate");                    //职场停用时间
  String tAWeiYueFee = request.getParameter("AWeiYueFee");          //实际违约金
  String tAWeiYueDate = request.getParameter("AWeiYueDate");                      //实际违约金支付日期
  String tBaoZhFee = request.getParameter("BaoZhFee");               //保证金
  String tBaoZhDate = request.getParameter("BaoZhDate");          //保证金收回日期
  String tAYingFee = request.getParameter("AYingFee");              //应收已付金额
  System.out.println("tAYingFee==="+tAYingFee);
  String tYingDate = request.getParameter("YingDate");    //应收已付收回日期
  System.out.println("==基本信息获取完毕！==");
  //对从界面取到的信息二次处理
 /*  String Appno=tApprovalType.trim()+tApprovalYear.trim()+tApprovalCode.trim()+tApprovalSeria.trim();  //对批次号进行二次处理
  System.out.println(Appno); */
  SimpleDateFormat dDateFormat=new SimpleDateFormat("yyyy-MM-dd");    //获取当前日期 
  String date=dDateFormat.format(new java.util.Date());
  System.out.println(date);
  SimpleDateFormat tDateFormat=new SimpleDateFormat("HH:mm:ss");      //获取当前时间
  String time=tDateFormat.format(new java.util.Date());
  System.out.println(time); 
   //变更职场信息表状态
  mLIPlaceChangeTraceSchema.setPlaceno(tPlaceno);
  mLIPlaceChangeTraceSchema.setChangetype("04");//职场撤租
  mLIPlaceChangeTraceSchema.setChangestate("01");//待审批
  mLIPlaceChangeTraceSchema.setEnddate(tStopDate);
  mLIPlaceChangeTraceSchema.setShouldgetfee(tAYingFee);
  mLIPlaceChangeTraceSchema.setShouldgetdate(tYingDate);
  mLIPlaceChangeTraceSchema.setApenaltyfee(tAWeiYueFee);
  mLIPlaceChangeTraceSchema.setApenaltyfeegetdate(tAWeiYueDate);
  mLIPlaceChangeTraceSchema.setMarginfee(tBaoZhFee);
  mLIPlaceChangeTraceSchema.setMarginfeegetdate(tBaoZhDate);
  mLIPlaceChangeTraceSchema.setOperator(tG.Operator);
  mLIPlaceChangeTraceSchema.setMakedate(date);
  mLIPlaceChangeTraceSchema.setMaketime(time);
  mLIPlaceChangeTraceSchema.setModifyDate(date);
  mLIPlaceChangeTraceSchema.setModifyTime(time);
  mLIPlaceChangeTraceSet.add(mLIPlaceChangeTraceSchema);
  
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.add(mLIPlaceRentInfoBSet);
  tVData.add(mLIPlaceRentFeeBSet);
  tVData.add(mLIPlaceChangeTraceSet);
  System.out.println("vdate======="+tVData.size());
  System.out.println("add over");
  try
  {
	  if(!mPlCancleUI.submitData(tVData,tOperate)){
	    if (mPlCancleUI.mErrors.needDealError())
          {
              tError.copyAllErrors(mPlCancleUI.mErrors);
              FlagStr = "true";
              Content = "保存失败，原因是："+tError.getFirstError();
          }
          else
          {
              FlagStr = "true";
              Content = "保存失败，没有获取到详细报错信息！";
          }
	  }
  }
  catch(Exception ex)
  {
    ex.printStackTrace();
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //添加各种预处理

  %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>