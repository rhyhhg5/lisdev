var showInfo;
var ImportPath;
var turnPage = new turnPageClass();
function submitForm() {
	if (beforeSubmit()) {
	if (sumFee()) {
		if(!checkNum(fm.Penaltyfee.value) || !checkNum(fm.Marginfee.value)) 
		{
		    alert("保证金金额或合同约定违约金格式错误，请重新输入！");
		    fm.Penaltyfee.value="";
		    fm.Marginfee.value="";
		    return false;
		}
		if (verifyInput()) {
				var r = confirm("请仔细核对您录入的信息，如果信息录入错误将直接影响职场租金的及时下拨！");
				if (r == true) {
					var tOperate = "ADD";
					var i = 0;
					var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
					var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
							+ showStr;
					showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
					fm.submit();
				} else {
					return false;
				}

			}
		}
	}
}

function beforeSubmit() {
	placeno = fm.Placeno.value;
	if(!checkPlaceNO(placeno)){
		return false;
	};
	var address = fm.Address.value;
//	alert("address--"+address); 
	if(!testAddress(address)){
		return false;
	};
	fm.SumRenFee.value = turnPage.arrDataCacheSet[0][0];
    sumArea();
	var StartDate = document.getElementsByName("Begindate")[0].value;
	var endData = document.getElementsByName("Enddate")[0].value;
	if (!checkdate(StartDate, endData)) {
		return false;
	}
	if(fm.Otherarea.value != "" && fm.Otherarea.value != 0)
	{
		if(fm.Explanation.value=="")
		{
			alert("其他面积不为空且大于零时需填写说明！");
			return false;
		}
	}		
	if (fm.all("HidFileName").value == null
			|| fm.all("HidFileName").value == "") {
		alert("请先上传批文");
		return false;
	}
	return true;

}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	
	initForm();
}
function afterSubmit1(FlagStr, content, filename) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ content;
		showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }else{
		var HidFileName = document.getElementsByName("HidFileName")[0];
		var HidFilePath = document.getElementsByName("HidFilePath")[0];
		HidFileName.value = filename;
		HidFilePath.value = ImportPath;
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="+ content;
		showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		// showDiv(operateButton,"true");
		// showDiv(inputButton,"false");
		// 执行下一步操作
	}
}
/*
 * function DateDiff(sDate1, sDate2){ var aDate, oDate1, oDate2, iDays; aDate =
 * sDate1.split("-"); oDate1 = new Date(aDate[1] + '-' + aDate[2] + '-' +
 * aDate[0]); aDate = sDate2.split("-"); oDate2 = new Date(aDate[1] + '-' +
 * aDate[2] + '-' + aDate[0]); iDays = parseInt(Math.abs(oDate1 - oDate2) / 1000 /
 * 60 / 60 /24); //把相差的毫秒数转换为天数 return iDays; }
 */

/*
 * function sumArea() { var
 * ARiskArea=document.getElementsByName("ARiskArea")[0].value*1; var
 * GRiskArea=document.getElementsByName("GRiskArea")[0].value*1; var
 * BRiskArea=document.getElementsByName("BRiskArea")[0].value*1; var
 * UOfficeArea=document.getElementsByName("UOfficeArea")[0].value*1; var
 * HelporArea=document.getElementsByName("HelporArea")[0].value*1; var
 * OtherArea=document.getElementsByName("OtherArea")[0].value*1; var
 * sumArea=ARiskArea+GRiskArea+BRiskArea+UOfficeArea+HelporArea+OtherArea; var
 * PlaseArea=document.getElementsByName("PlaseArea")[0];
 * PlaseArea.value=sumArea; }
 */
