var showInfo;
var ImportPath;
var turnPage = new turnPageClass();
function submitForm() {
	if (beforeSubmit()) {
	if (sumFee()) {
		if (verifyInput()) {
				var r = confirm("请仔细核对您录入的信息，如果信息录入错误将直接影响职场租金的及时下拨！");
				if (r == true) {
					var tOperate = "Next";
					var i = 0;
					var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
					var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
							+ showStr;
					showInfo = window
							.showModelessDialog(urlStr, window,
									"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
					fm.submit();
				} else {
					return false;
				}

			}
		}
	}
}

function monthsBetween(bdate,edate){
	var b=bdate.split('-');
	var e=edate.split('-');
	return (e[0]-b[0])*12+(e[1]-b[1])*1;
}

function beforeSubmit() {
	var address = fm.Address.value;
	if(!testAddress(address)){
		return false;
	};
    sumArea();  //计算总面积
	var Begindate = document.getElementsByName("Begindate")[0].value;
	var Enddate = document.getElementsByName("Enddate")[0].value;
	var BackMarginfeeDate = document.getElementsByName("SMarginfeeDate")[0].value;
	if (!checkdate(Begindate, Enddate)) {
		return false;
	}
	if((Begindate!=null && Begindate!="") && (BackMarginfeeDate!=null && BackMarginfeeDate!="")){
		if(BackMarginfeeDate>Begindate){
			var months = monthsBetween(Begindate,BackMarginfeeDate);
			var bDate = Begindate.split("-");
			var margindate = BackMarginfeeDate.split("-");
			if(months>1){
				alert("原职场合同的【保证金收回时间】不能晚于新合同租期起期后1个月");
				return false;
			}else if(months==1 && (margindate[2]-bDate[2])*1>0){
				alert("原职场合同的【保证金收回时间】不能晚于新合同租期起期后1个月");
				return false;
			}
		}
	}
	if(fm.Otherarea.value != "" && fm.Otherarea.value !=0)
	{
		if(fm.Explanation.value=="")
		{
			alert("其他面积不为空且大于零时需填写说明！");
			return false;
		}
	}		
	if (fm.all("HidFileName").value == null
			|| fm.all("HidFileName").value == "") {
		alert("请先上传批文");
		return false;
	}
	if(!checkPlaceNO(fm.Placeno.value)){
	  document.getElementsByName("ApprovalType")[0].value="";
  	  document.getElementsByName("Managecom")[0].value="";
	  document.getElementsByName("ApprovalYear")[0].value="";
	  document.getElementsByName("ApprovalCode")[0].value="";
	  document.getElementsByName("ApprovalSeria")[0].value="";
	  return false;
	}
	return true;

}

function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	
	initForm();
}
function afterSubmit1(FlagStr, content, filename) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {

		var HidFileName = document.getElementsByName("HidFileName")[0];
		var HidFilePath = document.getElementsByName("HidFilePath")[0];
		HidFileName.value = filename;
		HidFilePath.value = ImportPath;
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		// showDiv(operateButton,"true");
		// showDiv(inputButton,"false");
		// 执行下一步操作
	}
}

function placeIn(){
    var codeno=document.getElementById("codeno");
    if(codeno.value==null || codeno.value==""){
      alert("请先输入续租职场编码！");
      document.getElementById("codeno").focus();
      return false;
    }
    //var strSQL = "select 1 from LIPlaceRentinfo where Placeno='"+codeno.value+"' and not exists(select 1 from LIPlaceChangeTrace where (placeno='"+codeno.value+"' or Placenorefer='"+codeno.value+"') and Changestate in('01','02'))";			 
    var strSQL = "select 1 from LIPlaceRentinfo where Placeno='" + fm.codeno.value
	+"' and not exists(select 1 from LIPlaceChangeTrace where Placenorefer='" + fm.codeno.value+ "' and Changestate in('01','02') and Changetype in('02','03'))"
	+" and not exists(select 1 from LIPlaceChangeTrace where Placeno='" + fm.codeno.value+ "' and Changestate in('01','02') and Changetype in('04','05','06'))";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
  	  alert("续租职场编码不存在或该职场正处于审批状态，请重新输入！");
  	  codeno.value="";
  	  document.getElementById("codeno").focus();
      return false;
	}
	
	//判断该职场是否已经续租过
	var strSQL = "select 1 from LIPlaceRentinfo where placeno = '" + fm.codeno.value 
		+"' and exists (select 1 from LIPlaceChangeTrace where Placenorefer = '" + fm.codeno.value+ "' and Changestate not in('DL') and Changetype in ('02','03') )";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	if(turnPage.strQueryResult){
		alert("职场编码已被续租或换租，请重新输入！");
		document.getElementById("codeno").focus();
  	 	codeno.value="";
      	return false;
	}
		
	//获取源职场信息
	var strSQL1 = "select (select Comname from ldcom where comcode = a.managecom fetch first 1 rows only),"
                 +"(sigarea + grparea + bankarea + healtharea + servicearea + jointarea+manageroffice + departpersno + planfinance + publicarea +otherarea),"
                 +"begindate,enddate,address,marginfee,a.managecom,comlevel,(select codename from ldcode where codetype='companylevel' and code=a.comlevel),"
                 +"address,lessor,houseno,province,city,isrentregister,(case isrentregister when '1' then '是' else '否' end),islessorassociate,(case islessorassociate when '1' then '是' else '否' end),"
                 +"islessorright,(case islessorright when '1' then '是' else '否' end),ishouserent,(case ishouserent when '1' then '是' else '否' end),"
                 +"isplaceoffice,(case isplaceoffice when '1' then '是' else '否' end),ishousegroup,(case ishousegroup when '1' then '是' else '否' end),"
                 +"ishousefarm,(case ishousefarm when '1' then '是' else '否' end),"
                 +"sigarea,grparea,bankarea,healtharea,servicearea,jointarea,manageroffice,departpersno,planfinance,publicarea,otherarea,explanation "
                 +"from LIPlaceRentInfo a where Placeno='"+codeno.value+"'";			 
	turnPage.strQueryResult  = easyQueryVer3(strSQL1, 1, 1, 1);  
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	//原职场信息
	fm.SComName.value = turnPage.arrDataCacheSet[0][0];
	fm.SumArea.value = turnPage.arrDataCacheSet[0][1];
	fm.SStartDate.value = turnPage.arrDataCacheSet[0][2];
	fm.SEndDate.value = turnPage.arrDataCacheSet[0][3];
	fm.SAddress.value = turnPage.arrDataCacheSet[0][4];
	fm.SMarginfee.value=turnPage.arrDataCacheSet[0][5];
	
	//续租职场信息
	fm.Managecom.value = turnPage.arrDataCacheSet[0][6];
	fm.Comlevel.value = turnPage.arrDataCacheSet[0][7];
	fm.Comlevelname.value = turnPage.arrDataCacheSet[0][8];
	fm.Address.value = turnPage.arrDataCacheSet[0][9];
	fm.Lessor.value = turnPage.arrDataCacheSet[0][10];
	fm.Houseno.value = turnPage.arrDataCacheSet[0][11];
	fm.Province.value = turnPage.arrDataCacheSet[0][12];
	fm.City.value = turnPage.arrDataCacheSet[0][13];
	fm.Isrentregister.value = turnPage.arrDataCacheSet[0][14];
	fm.IsRentRegisterName.value = turnPage.arrDataCacheSet[0][15];
	fm.Islessorassociate.value = turnPage.arrDataCacheSet[0][16];
	fm.IslessorassociateName.value = turnPage.arrDataCacheSet[0][17];
	fm.Islessorright.value = turnPage.arrDataCacheSet[0][18];
	fm.IslessorrightName.value = turnPage.arrDataCacheSet[0][19];
	fm.Ishouserent.value = turnPage.arrDataCacheSet[0][20];
	fm.IshouserentName.value = turnPage.arrDataCacheSet[0][21];
	fm.Isplaceoffice.value = turnPage.arrDataCacheSet[0][22];
	fm.IsplaceofficeName.value = turnPage.arrDataCacheSet[0][23];
	fm.Ishousegroup.value = turnPage.arrDataCacheSet[0][24];
	fm.IshousegroupName.value = turnPage.arrDataCacheSet[0][25];
	fm.Ishousefarm.value = turnPage.arrDataCacheSet[0][26];
	fm.IshousefarmName.value = turnPage.arrDataCacheSet[0][27];

	//续租职场面积
	fm.Sigarea.value = turnPage.arrDataCacheSet[0][28];
	fm.Grparea.value = turnPage.arrDataCacheSet[0][29];
	fm.Bankarea.value = turnPage.arrDataCacheSet[0][30];
	fm.Healtharea.value = turnPage.arrDataCacheSet[0][31];
	fm.Servicearea.value = turnPage.arrDataCacheSet[0][32];
	fm.Jointarea.value = turnPage.arrDataCacheSet[0][33];
	fm.Manageroffice.value = turnPage.arrDataCacheSet[0][34];
	fm.Departpersno.value = turnPage.arrDataCacheSet[0][35];
	fm.Planfinance.value = turnPage.arrDataCacheSet[0][36];
	fm.Publicarea.value = turnPage.arrDataCacheSet[0][37];
	fm.Otherarea.value = turnPage.arrDataCacheSet[0][38];
	fm.Explanation.value = turnPage.arrDataCacheSet[0][39];
	
    var placeIn1=document.getElementById("divPlaceIn1");
    var placeIn2=document.getElementById("divPlaceIn2");
    var divSourceInfo=document.getElementById("divSourceInfo");
    placeIn1.style.display='';
    placeIn2.style.display='';
    divSourceInfo.style.display='';
}