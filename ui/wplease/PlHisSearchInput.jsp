<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：PlHisSearchInput.jsp
//程序功能：
//创建日期：2011-6-10
//创建人  ：huodonglei
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="PlHisSearchInput.js"></SCRIPT> 
	<%@include file="PlHisSearchInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
</head>

<body onload="initElementtype();initForm();">    
  <form action="" method=post name=fm target="fraSubmit" >
    <div  id= "divCessGetData" style= "display: ''" >
	    <table class= common border=0 width=100%>
	      	<tr  class= common>
	      	<td class= title>
                   公司代码
            </td>
            <td class=input>
                 <Input NAME=CompanyCode  CLASS=codeno ondblclick="return showCodeList('comcode',[this,CompanyCodeName],[0,1],null,'1 and length(trim(comcode))=#8#',1,1);" onkeyup="return showCodeListKey('comcode',[this,CompanyCodeName],[0,1],null,'1 and length(trim(comcode))=#8#',1,1);" verify="公司代码|code:comcode&notnull"  readonly><input class=codename name=CompanyCodeName readOnly  elementtype=nacessary>
            </td>
	            <td  class= title>
	                  统计起期
	            </td>
	            <td  class= input> 
	                <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> 
	            </td> 
	            <td class= title>
   		 	         统计止期
   			    </td>
   			    <td class= input >
          	    <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> 
   			    </td>
	        </tr>      
	    </table>
	    <br>
	    <input class=cssButton type="button"  value="查询" onclick="">
	    <br><br>
	    <div id='divBusynessGrid' style="display:''">
		    <table class="common">		
					<tr class="common">
						<td text-align: left colSpan=1><span id="spanBusynessGrid"></span></td>
					</tr>
			</table>
	    </div>  
	    <br>
		<INPUT class=cssButton   VALUE="打印"  TYPE=button  onClick="" >
		<INPUT class=cssButton   VALUE="职场明细查询"  TYPE=button  onClick="" >
		<INPUT class=cssButton   VALUE="批文查询"  TYPE=button  onClick="" >
		</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 