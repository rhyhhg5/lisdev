<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PlInSave.jsp
//程序功能：
//创建日期：2011-06-22 15:12:33
//创建人  ：huodonglei
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.wpleace.*"%>
  <%@page import="com.sinosoft.lis.wpleace.PlChangeUI"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
  <%@page import="java.io.*"%>
  <%@page import="java.text.SimpleDateFormat" %>
  <%@page import="org.apache.commons.fileupload.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LIPliceRentInfoSchema mLIPliceRentInfoSchema = new LIPliceRentInfoSchema();
  LIPliceRentInfoSet mLIPliceRentInfoSchemaSet = new LIPliceRentInfoSet();
  LIPlaceRentFeeSchema mLIPlaceRentFeeSchema;
  LIPlaceRentFeeSet mLIPlaceRentFeeSet=new LIPlaceRentFeeSet();
  LIPlaceChangeInfoSchema mLIPlaceChangeInfoSchema=new LIPlaceChangeInfoSchema();
  LIPlaceChangeInfoSet mLIPlaceChangeInfoSet=new LIPlaceChangeInfoSet();
  PlChangeUI mPlChangeUI  = new PlChangeUI();

  //输出参数
  CErrors tError = new CErrors();
  String tOperate="NEXT";
  String tRela  = "";
  String FlagStr = "true";
  String Content = "保存成功";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  System.out.println("开始获取数据==");
  //换租职场编码
   String tCodeNo = request.getParameter("codeno");
  //基本信息
  String tCodeId = request.getParameter("CodeId");
  String tCompanyCode = request.getParameter("CompanyCode");
  String tApprovalType = request.getParameter("ApprovalType");
  String tApprovalYear = request.getParameter("ApprovalYear");  
  String tApprovalCode = request.getParameter("ApprovalCode");  
  String tApprovalSeria = request.getParameter("ApprovalSeria");
  String tCompanyLevel = request.getParameter("CompanyLevel");  
  String tComName = request.getParameter("ComName");  
  String tStartDate = request.getParameter("StartDate");  
  String tEndDate = request.getParameter("EndDate");  
  String tAddress = request.getParameter("Address"); 
  System.out.println("基本信息获取完毕！");
  //职场面积
  String tARiskArea = request.getParameter("ARiskArea");
  String tGRiskArea = request.getParameter("GRiskArea");
  String tBRiskArea = request.getParameter("BRiskArea");
  String HealthArea = request.getParameter("HealthArea");
  String tServiceArea = request.getParameter("ServiceArea");
  String tHelporArea = request.getParameter("HelporArea");
  String tUOfficeArea = request.getParameter("UOfficeArea");
  String tOtherArea = request.getParameter("OtherArea");
  System.out.println("职场面积信息获取完毕！");
  //合同约定职场费用
  String tMarginFee = request.getParameter("MarginFee");
  String tMarginPayDate = request.getParameter("MarginPayDate");
  String tPenaltyfee = request.getParameter("Penaltyfee");
  System.out.println("合同约定职场费用信息获取完毕！");
  //资本化日期
  String tCapitalDate = request.getParameter("CapitalDate");
  //备注
  String tRemark = request.getParameter("Remark");
  String tHidFileName = request.getParameter("HidFileName");    //文件名
  String tHidFilePath = request.getParameter("HidFilePath");    //文件路径
  //取得租期内租金费用Muline信息
  int lineCount1 = 0;
  //String tGridNo1[] = request.getParameterValues("RelateGridNo"); 
  String tPayDate1[] = request.getParameterValues("RelateGrid1");
  String tPayAmount1[] = request.getParameterValues("RelateGrid2");
  String tPayStartDate1[] = request.getParameterValues("RelateGrid3");
  String tPayEndDate1[] = request.getParameterValues("RelateGrid4");
  lineCount1 = tPayDate1.length; //行数
  System.out.println(tPayDate1[0]);
  System.out.println(tPayAmount1[0]);
  System.out.println(tPayStartDate1[0]);
  System.out.println(tPayEndDate1[0]);
  System.out.println("租金费用length= "+String.valueOf(lineCount1));
  //取得租期内物业费用Muline信息
  int lineCount2 = 0;
  //String tGridNo2[] = request.getParameterValues("PropertyGridNo");
  String tPayDate2[] = request.getParameterValues("PropertyGrid1");
  String tPayAmount2[] = request.getParameterValues("PropertyGrid2");
  String tPayStartDate2[] = request.getParameterValues("PropertyGrid3");
  String tPayEndDate2[] = request.getParameterValues("PropertyGrid4");
  lineCount2 = tPayDate2.length; //行数
  System.out.println("物业费length= "+String.valueOf(lineCount2));
  //取得租期内装修费Muline信息
  int lineCount3 = 0;
  //String tGridNo3[] = request.getParameterValues("DecorationGridNo");
  String tPayDate3[] = request.getParameterValues("DecorationGrid1");
  String tPayAmount3[] = request.getParameterValues("DecorationGrid2");
  lineCount3 = tPayDate3.length; //行数
  System.out.println("装修费length= "+String.valueOf(lineCount3));
  
  //退租信息
  String tPlaceEndDate=request.getParameter("HidPlaceEndDate");
  String tPenaltyFee2=request.getParameter("HidPenaltyFee2");
  String tReBackFee=request.getParameter("HidReBackFee");
  String tReBackDate=request.getParameter("HidReBackDate");
  
  //对从界面取到的信息二次处理
  String Appno=tApprovalType.trim()+tApprovalYear.trim()+tApprovalCode.trim()+tApprovalSeria.trim();  //对批次号进行二次处理
  System.out.println(Appno);
  SimpleDateFormat dDateFormat=new SimpleDateFormat("yyyy-MM-dd");    //获取当前日期 
  String date=dDateFormat.format(new java.util.Date());
  System.out.println(date);
  SimpleDateFormat tDateFormat=new SimpleDateFormat("HH:mm:ss");      //获取当前时间
  String time=tDateFormat.format(new java.util.Date());
  System.out.println(time);
  
  //往职场租赁合同中添加信息
  mLIPliceRentInfoSchema.setPlaceno(tCodeId);
  mLIPliceRentInfoSchema.setManagecom(tCompanyCode);
  mLIPliceRentInfoSchema.setAppno(Appno);
  mLIPliceRentInfoSchema.setState("02");
  mLIPliceRentInfoSchema.setComlevel(tCompanyLevel);
  mLIPliceRentInfoSchema.setComname(tComName);
  mLIPliceRentInfoSchema.setBegindate(tStartDate);
  mLIPliceRentInfoSchema.setEnddate(tEndDate);
  mLIPliceRentInfoSchema.setActualenddate(tEndDate);
  mLIPliceRentInfoSchema.setAddress(tAddress);
  mLIPliceRentInfoSchema.setSigarea(tARiskArea);
  mLIPliceRentInfoSchema.setGrparea(tGRiskArea);
  mLIPliceRentInfoSchema.setBankarea(tBRiskArea);
  mLIPliceRentInfoSchema.setHealtharea(HealthArea);
  mLIPliceRentInfoSchema.setServicearea(tServiceArea);
  mLIPliceRentInfoSchema.setHelparea(tHelporArea);
  mLIPliceRentInfoSchema.setJointarea(tUOfficeArea);
  mLIPliceRentInfoSchema.setOtherarea(tOtherArea);
  mLIPliceRentInfoSchema.setMarginfee(tMarginFee);
  mLIPliceRentInfoSchema.setMarginpaydate(tMarginPayDate);
  mLIPliceRentInfoSchema.setMarginbackdate("");
  mLIPliceRentInfoSchema.setPenaltyfee(tPenaltyfee);
  mLIPliceRentInfoSchema.setCapdate(tCapitalDate);
  mLIPliceRentInfoSchema.setRemake(tRemark);       //备注
  mLIPliceRentInfoSchema.setFilename(tHidFileName);
  mLIPliceRentInfoSchema.setFilepath(tHidFilePath);
  mLIPliceRentInfoSchema.setDealstate("0");
  mLIPliceRentInfoSchema.setAppstate("01");
  mLIPliceRentInfoSchema.setNexttype("01");
  mLIPliceRentInfoSchema.setOperator(tG.Operator);
  mLIPliceRentInfoSchema.setMakedate(date);
  mLIPliceRentInfoSchema.setMaketime(time);
  mLIPliceRentInfoSchema.setModifydate(date);
  mLIPliceRentInfoSchema.setModifytime(time);
  mLIPliceRentInfoSchema.setStandbystring1("02");
  mLIPliceRentInfoSchema.setStandbystring2(tCodeNo);
  mLIPliceRentInfoSchemaSet.add(mLIPliceRentInfoSchema); 
  
  //往费用表中添加租金信息
  for(int i=0;i<lineCount1;i++)
  {
	  mLIPlaceRentFeeSchema=new LIPlaceRentFeeSchema();
	  mLIPlaceRentFeeSchema.setPlaceno(tCodeId);
	  mLIPlaceRentFeeSchema.setFeetype("01");
	  mLIPlaceRentFeeSchema.setSerialno(Integer.toString(i+1));
	  mLIPlaceRentFeeSchema.setPayDate(tPayDate1[i]);
	  //mLIPlaceRentFeeSchema.setPayDate()
	  mLIPlaceRentFeeSchema.setMoney(tPayAmount1[i]);
	  mLIPlaceRentFeeSchema.setPaybegindate(tPayStartDate1[i]);
      mLIPlaceRentFeeSchema.setPayenddate(tPayEndDate1[i]);
      mLIPlaceRentFeeSchema.setOperator(tG.Operator);
      mLIPlaceRentFeeSchema.setMakedate(date);
      mLIPlaceRentFeeSchema.setMaketime(time);
      mLIPlaceRentFeeSchema.setModifyDate(date);
      mLIPlaceRentFeeSchema.setModifyTime(time);
      mLIPlaceRentFeeSet.add(mLIPlaceRentFeeSchema);   
  }
   //往费用表中添加物业费信息
  for(int i=0;i<lineCount2;i++)
  {
	  mLIPlaceRentFeeSchema=new LIPlaceRentFeeSchema();
	  mLIPlaceRentFeeSchema.setPlaceno(tCodeId);
	  mLIPlaceRentFeeSchema.setFeetype("02");
	  mLIPlaceRentFeeSchema.setSerialno(Integer.toString(i+1));
	  mLIPlaceRentFeeSchema.setPayDate(tPayDate2[i]);
	  mLIPlaceRentFeeSchema.setMoney(tPayAmount2[i]);
	  mLIPlaceRentFeeSchema.setPaybegindate(tPayStartDate2[i]);
      mLIPlaceRentFeeSchema.setPayenddate(tPayEndDate2[i]);   
      mLIPlaceRentFeeSchema.setOperator(tG.Operator);
      mLIPlaceRentFeeSchema.setMakedate(date);
      mLIPlaceRentFeeSchema.setMaketime(time);
      mLIPlaceRentFeeSchema.setModifyDate(date);
      mLIPlaceRentFeeSchema.setModifyTime(time); 
      mLIPlaceRentFeeSet.add(mLIPlaceRentFeeSchema);  
  }
   //往费用表中添加装修费信息
  for(int i=0;i<lineCount3;i++)
  {
	  mLIPlaceRentFeeSchema=new LIPlaceRentFeeSchema();
	  mLIPlaceRentFeeSchema.setPlaceno(tCodeId);
	  mLIPlaceRentFeeSchema.setFeetype("03");
	  mLIPlaceRentFeeSchema.setSerialno(Integer.toString(i+1));
	  mLIPlaceRentFeeSchema.setPayDate(tPayDate3[i]);
	  mLIPlaceRentFeeSchema.setMoney(tPayAmount3[i]);
	  mLIPlaceRentFeeSchema.setOperator(tG.Operator);
      mLIPlaceRentFeeSchema.setMakedate(date);
      mLIPlaceRentFeeSchema.setMaketime(time);
      mLIPlaceRentFeeSchema.setModifyDate(date);
      mLIPlaceRentFeeSchema.setModifyTime(time);
      mLIPlaceRentFeeSet.add(mLIPlaceRentFeeSchema);    
  }
 
 //往换租租表中插入信息 
  mLIPlaceChangeInfoSchema.setPreplacno(tCodeNo);
  mLIPlaceChangeInfoSchema.setPlaceno(tCodeId);
  mLIPlaceChangeInfoSchema.setGetmoney(tReBackFee);
  mLIPlaceChangeInfoSchema.setGetdate(tReBackDate);
  mLIPlaceChangeInfoSchema.setPenaltyfee(tPenaltyFee2);
  mLIPlaceChangeInfoSchema.setEnddate(tPlaceEndDate);
  mLIPlaceChangeInfoSchema.setAppstate("0");
  mLIPlaceChangeInfoSchema.setOperator(tG.Operator);
  mLIPlaceChangeInfoSchema.setMakedate(date);
  mLIPlaceChangeInfoSchema.setMaketime(time);
  mLIPlaceChangeInfoSchema.setModifydate(date);
  mLIPlaceChangeInfoSchema.setModifytime(time);
  mLIPlaceChangeInfoSet.add(mLIPlaceChangeInfoSchema);
  
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.add(mLIPliceRentInfoSchemaSet);
  tVData.add(mLIPlaceRentFeeSet);
  tVData.add(tCodeNo);
  tVData.add(mLIPlaceChangeInfoSet);
  System.out.println("vdate======="+tVData.size());
  System.out.println("add over");
  try
  {
	  mPlChangeUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //添加各种预处理

  %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>