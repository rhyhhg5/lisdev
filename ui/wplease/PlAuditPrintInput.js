var showInfo;
var ImportPath;
var turnPage = new turnPageClass();

function print()
{
    if(!checkDate()){ return false;}
	fm.target = "f1print";
	fm.action="./PlAuditPrintSave.jsp";
	fm.fmtransact.value="PRINT";
	submitForm();
	showInfo.close();	
}

//提交，保存按钮对应操作
function submitForm()
{
	  var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.submit(); //提交
}
function checkDate()
{
    var beginDate=document.getElementsByName("BeginDate")[0].value;
    var endDate=document.getElementsByName("EndDate")[0].value;
    if(beginDate =='' || beginDate == null)
    {
       alert("统计起期不能为空，请重新输入！");
       return false;
    }
    if(endDate=='' || endDate==null)
    {
       alert("统计止期不能为空，请重新输入！");
       return false;
    }
    if(beginDate>endDate){
        alert("统计起期大于统计止期，请重新输入！");
        return false;
    }

    return true;
}