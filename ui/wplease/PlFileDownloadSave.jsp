<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.net.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
		GlobalInput globalInput = new GlobalInput(); 	 	 
		globalInput = (GlobalInput) session.getValue("GI");
		String Content = "";
		
		String contextPath = request.getContextPath();
		System.out.println(contextPath);
		String serverPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
		System.out.println(serverPath);
		
		String fileName = request.getParameter("fileName");	
		String filePath = request.getParameter("filePath");		
        System.out.println("fileName："+fileName);
        System.out.println("filePath："+filePath);

		System.out.println("file path : " + fileName + " file name : " + filePath);
		serverPath = application.getRealPath("").replace('\\','/')+'/';
		String Path = serverPath + filePath + "/" + fileName;
		System.out.println("file path : " + Path);

		BufferedOutputStream output = null;
		BufferedInputStream input = null;  
		try
		{
			//filePath = new String(filePath.getBytes("ISO-8859-1"),"gbk");
			
			File file = new File(Path);

			response.reset();
			response.setContentType("application/octet-stream"); 
			response.setHeader("Content-Disposition","attachment; filename=" + URLEncoder.encode(fileName,"GBK"));
			response.setContentLength((int) file.length());

			byte[] buffer = new byte[4096];  
			//写缓冲区
			output = new BufferedOutputStream(response.getOutputStream());
			input = new BufferedInputStream(new FileInputStream(file));

			int len = 0;
			while((len = input.read(buffer)) >0)
			{
				output.write(buffer,0,len);
			}
			Content = "下载成功！";
			out.clear();
			out = pageContext.pushBody();

			input.close();
			output.close(); 
		}
		catch(FileNotFoundException fe)
		{
		
			Content = "没有要下载的文件，请确认文件是否存在！";
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			System.out.println("下载失败！");
			
			Content = "下载失败";
		}
		finally {
			if (input != null) input.close();
			if (output != null) output.close();
		}  

%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%= Content %>");
</script>
</html>

