var FYMoneyturnPage = new turnPageClass();
function calculateFee(SEndDate,codeno){
    var stopDate=fm.StopDate.value;
    if(stopDate==null || stopDate==""){
      alert("停用时间不能为空！");
      return false;
    }
    if (fm.StopDate.value > SEndDate) {
		fm.SYingFee.value = 0.00;
	}else{
	    getshouldFee(SEndDate,codeno,'01');
	    getshouldFee(SEndDate,codeno,'02');
	    getshouldFee(SEndDate,codeno,'03');
	    fm.SYingFee.value = (1*fm.shouldRenFee.value+1*fm.shouldProFee.value).toFixed(2);
	    fm.AYingFee.value = (1*fm.shouldRenFee.value+1*fm.shouldProFee.value).toFixed(2);
	}

 
}

function getshouldFee(SEndDate,codeno,type) {

	var FYMoney;
	var FYMoneyZ;
	var FYSQL = "select b.money,b.Paybegindate,b.Payenddate from LIPlaceRentFee b "
			+ "where b.Placeno='"+ codeno+ "' and b.feetype='"+type+"' "
			+ "and paydate<='"+fm.StopDate.value+"'"
			+ "and YEAR('"+fm.StopDate.value+"') * 100 + MONTH('"+fm.StopDate.value+"') between YEAR(paybegindate) * 100 + MONTH(paybegindate) and YEAR(PAYENDDATE) * 100 + MONTH(PAYENDDATE)"
	FYMoneyturnPage.strQueryResult = easyQueryVer3(FYSQL, 1, 1, 1);

	if (!FYMoneyturnPage.strQueryResult) {
		FYMoney = 0;
	}else{
	    // 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
		FYMoneyturnPage.arrDataCacheSet = clearArrayElements(FYMoneyturnPage.arrDataCacheSet);
		// 查询成功则拆分字符串，返回二维数组
		FYMoneyturnPage.arrDataCacheSet = decodeEasyQueryResult(FYMoneyturnPage.strQueryResult);
		
		var money = FYMoneyturnPage.arrDataCacheSet[0][0];
		var Paybegindate = FYMoneyturnPage.arrDataCacheSet[0][1];
		var Payenddate = FYMoneyturnPage.arrDataCacheSet[0][2];
		var months = DateDiff(Paybegindate, Payenddate);
		var tomonths = DateDiff(Paybegindate, fm.StopDate.value);
		FYMoney=(money / months) * (months - tomonths);
	
	}

    var FYSQLZ = "select nvl(sum(b.money),0) from LIPlaceRentFee b "
			+ "where b.Placeno='"+ codeno+ "' and b.feetype='"+type+"' "
			+ "and paydate<='"+fm.StopDate.value+"'"
			+ "and YEAR('"+fm.StopDate.value+"') * 100 + MONTH('"+fm.StopDate.value+"') <YEAR(paybegindate) * 100 + MONTH(paybegindate)"
	
	FYMoneyturnPage.strQueryResult = easyQueryVer3(FYSQLZ, 1, 1, 1);
		
	if (!FYMoneyturnPage.strQueryResult) {
		FYMoneyZ=0;
	}else{
	    // 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
		FYMoneyturnPage.arrDataCacheSet = clearArrayElements(FYMoneyturnPage.arrDataCacheSet);
		// 查询成功则拆分字符串，返回二维数组
		FYMoneyturnPage.arrDataCacheSet = decodeEasyQueryResult(FYMoneyturnPage.strQueryResult);
		
		FYMoneyZ = FYMoneyturnPage.arrDataCacheSet[0][0];
	}
	if(type=='01'){
	    fm.shouldRenFee.value = FYMoney*1+FYMoneyZ*1;   // 租金应收已收款金额
	}else if(type=='02'){
	    fm.shouldProFee.value =FYMoney*1+FYMoneyZ*1;    // 物业费应收已收款金额
	}else{
	    fm.shouldDecFee.value = FYMoney*1+FYMoneyZ*1;   // 装修费应收已收款金额
	}
	
}


function DateDiff(sDate1, sDate2){  
        var aDate, bDate,months;
        aDate = sDate1.split("-");
        bDate = sDate2.split("-");
        months=(bDate[0]-aDate[0])*12+(bDate[1]-aDate[1])+1;
        return months;
}