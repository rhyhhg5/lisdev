
var showInfo;
var ImportPath;
var turnPage = new turnPageClass();
var rantSqlturnPage = new turnPageClass();
var profeeSqlturnPage = new turnPageClass();
var decorationSqlturnPage = new turnPageClass();
// 查询职场信息并返回到前台页面
function showPlaceInfo(state) {
	var codeno = fm.codeno.value;
	var infoSql="select lri.Placeno,lri.Managecom,case length(lri.Appno) when 13 then substr(lri.Appno, 1, 3) else substr(lri.Appno, 1, 2) end, case length(lri.Appno) when 13 then substr(lri.Appno, 4, 4) else substr(lri.Appno, 3, 4) end,case length(lri.Appno) when 13 then substr(lri.Appno, 8, 4) else substr(lri.Appno, 7, 4) end,case length(lri.Appno) when 13 then substr(lri.Appno, 12, 2) else substr(lri.Appno, 11, 2) end ," +
				"lri.Comlevel,case lri.Comlevel when '1' then '省级分公司' when '2' then '地市级分公司' when '3' then '四级机构-区' when '4' then '四级机构-县' end,lri.Comname,lri.Begindate,lri.Enddate,lri.Address,lri.Lessor,lri.Houseno,lri.Province,lri.City,lri.Isrentregister,case lri.Isrentregister when '1' then '是' when '0' then '否' end," +
				"lri.Islessorassociate,case lri.Islessorassociate when '1' then '是' when '0' then '否' end,lri.Islessorright,case lri.Islessorright when '1' then '是' when '0' then '否' end,lri.Ishouserent,case lri.Ishouserent when '1' then '是' when '0' then '否' end,lri.Isplaceoffice,case lri.Isplaceoffice when '1' then '是' when '0' then '否' end,lri.Ishousegroup,case lri.Ishousegroup when '1' then '是' when '0' then '否' end," +
				"lri.Ishousefarm,case lri.Ishousefarm when '1' then '是' when '0' then '否' end,lri.Sigarea,lri.Grparea,lri.Bankarea,lri.Healtharea,lri.Servicearea,lri.Jointarea,lri.Manageroffice,lri.Departpersno,lri.Planfinance,lri.Publicarea,lri.Otherarea,lri.Explanation,lri.Marginfee,lri.Marginpaydate,lri.Penaltyfee,lri.Remake,lri.Filename,lri.Filepath from LIPlaceRentInfo lri where lri.placeno='"+codeno +"'";
	turnPage.strQueryResult = easyQueryVer3(infoSql, 1, 1, 1);
		// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
		// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	
	//基本信息
	fm.Placeno.value=turnPage.arrDataCacheSet[0][0];
	fm.Managecom.value=turnPage.arrDataCacheSet[0][1];
	fm.ApprovalType.value=turnPage.arrDataCacheSet[0][2];
	fm.ApprovalYear.value=turnPage.arrDataCacheSet[0][3];
	fm.ApprovalCode.value=turnPage.arrDataCacheSet[0][4];
	fm.ApprovalSeria.value=turnPage.arrDataCacheSet[0][5];
	fm.Comlevel.value=turnPage.arrDataCacheSet[0][6];
	fm.Comlevelname.value=turnPage.arrDataCacheSet[0][7];
	fm.Comname.value=turnPage.arrDataCacheSet[0][8];
	fm.Begindate.value=turnPage.arrDataCacheSet[0][9];
	fm.Enddate.value=turnPage.arrDataCacheSet[0][10];
	fm.Address.value=turnPage.arrDataCacheSet[0][11];
	fm.Lessor.value=turnPage.arrDataCacheSet[0][12];
	fm.Houseno.value=turnPage.arrDataCacheSet[0][13];
	fm.Province.value=turnPage.arrDataCacheSet[0][14];
	fm.City.value=turnPage.arrDataCacheSet[0][15];
	fm.Isrentregister.value=turnPage.arrDataCacheSet[0][16];
	fm.IsRentRegisterName.value=turnPage.arrDataCacheSet[0][17];
	fm.Islessorassociate.value=turnPage.arrDataCacheSet[0][18];
	fm.IslessorassociateName.value=turnPage.arrDataCacheSet[0][19];
	fm.Islessorright.value=turnPage.arrDataCacheSet[0][20];
	fm.IslessorrightName.value=turnPage.arrDataCacheSet[0][21];
	fm.Ishouserent.value=turnPage.arrDataCacheSet[0][22];
	fm.IshouserentName.value=turnPage.arrDataCacheSet[0][23];
	fm.Isplaceoffice.value=turnPage.arrDataCacheSet[0][24];
	fm.IsplaceofficeName.value=turnPage.arrDataCacheSet[0][25];
	fm.Ishousegroup.value=turnPage.arrDataCacheSet[0][26];
	fm.IshousegroupName.value=turnPage.arrDataCacheSet[0][27];
	fm.Ishousefarm.value=turnPage.arrDataCacheSet[0][28];
	fm.IshousefarmName.value=turnPage.arrDataCacheSet[0][29];
	//职场面积
	fm.Sigarea.value=turnPage.arrDataCacheSet[0][30];
	fm.Grparea.value=turnPage.arrDataCacheSet[0][31];
	fm.Bankarea.value=turnPage.arrDataCacheSet[0][32];
	fm.Healtharea.value=turnPage.arrDataCacheSet[0][33];
	fm.Servicearea.value=turnPage.arrDataCacheSet[0][34];
	fm.Jointarea.value=turnPage.arrDataCacheSet[0][35];
	fm.Manageroffice.value=turnPage.arrDataCacheSet[0][36];
	fm.Departpersno.value=turnPage.arrDataCacheSet[0][37];
	fm.Planfinance.value=turnPage.arrDataCacheSet[0][38];
	fm.Publicarea.value=turnPage.arrDataCacheSet[0][39];	
	fm.Otherarea.value=turnPage.arrDataCacheSet[0][40];
	fm.Explanation.value=turnPage.arrDataCacheSet[0][41];
	
	fm.ZSumarea.value=turnPage.arrDataCacheSet[0][30]*1+turnPage.arrDataCacheSet[0][31]*1+turnPage.arrDataCacheSet[0][32]*1+turnPage.arrDataCacheSet[0][33]*1+turnPage.arrDataCacheSet[0][34]*1+turnPage.arrDataCacheSet[0][35]*1+turnPage.arrDataCacheSet[0][36]*1+turnPage.arrDataCacheSet[0][37]*1+turnPage.arrDataCacheSet[0][38]*1+turnPage.arrDataCacheSet[0][39]*1+turnPage.arrDataCacheSet[0][40]*1;
	//合同违约金及保证金
	fm.Marginfee.value=turnPage.arrDataCacheSet[0][42];
	fm.Marginpaydate.value=turnPage.arrDataCacheSet[0][43];
	fm.Penaltyfee.value=turnPage.arrDataCacheSet[0][44];
	//备注信息
	fm.Remark.value=turnPage.arrDataCacheSet[0][45];
	//批文下载文件名及路径
	fm.filename.value=turnPage.arrDataCacheSet[0][46];
	fm.filepath.value=turnPage.arrDataCacheSet[0][47];
	//审批不通过时的备注
	//计算租金，物业费，装修费
	showMoneyInfo(state);
}
function showMoneyInfo(state) 
{
	var rantSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '01'";
	var profeeSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '02'";
	var decorationSql = "select paydate,money,substr(char(paybegindate),1,7),substr(char(payenddate),1,7) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '03'";
	var sumrantSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '01'";
	var sumprofeeSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '02'";
	var sumdecorationSql = "select nvl(sum(money),0) from LIPlaceRentFee where placeno = '" + fm.codeno.value + "' and feetype = '03'";

	
	rantSqlturnPage.queryModal(rantSql, RelateGrid);
	profeeSqlturnPage.queryModal(profeeSql, PropertyGrid);
	decorationSqlturnPage.queryModal(decorationSql, DecorationGrid);
	
	
	//获取总费用
	turnPage.strQueryResult = easyQueryVer3(sumrantSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumRenFee.value = turnPage.arrDataCacheSet[0][0];
	
	turnPage.strQueryResult = easyQueryVer3(sumprofeeSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumProFee.value = turnPage.arrDataCacheSet[0][0];
	
	turnPage.strQueryResult = easyQueryVer3(sumdecorationSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	fm.SumDecFee.value = turnPage.arrDataCacheSet[0][0];
	//alert("state=============="+state);
	// 应收已付租金
	if (state == "03" || state == "04") {
		setMoney(state);
		if(state == "03"){
		    calculateFee(fm.Enddate.value,fm.Placenorefer.value);
		}else{
		    calculateFee(fm.Enddate.value,fm.Placeno.value);
		}
		
	}
}
function setMoney(state) {

	if (state == "03") {
		var tSql = "select lci.Enddate,lci.Shouldgetfee,lci.Shouldgetdate,lci.Apenaltyfee,lci.Apenaltyfeegetdate,lci.Marginfee,lci.Marginfeegetdate,lri.Penaltyfee from LIPlaceChangeInfo lci,LIPlaceRentInfo lri where lci.preplacno = '" + fm.codeno.value + "' and lci.preplacno=lri.placeno";
	} else if(state=="04"){
		var tSql = "select le.Enddate,le.Shouldgetfee,le.Shouldgetdate,le.Apenaltyfee,le.Apenaltyfeegetdate,le.Marginfee,le.Marginfeegetdate,lr.Penaltyfee from LIPlaceEndInfo le,LIPlaceRentInfo lr where le.placeno = '" + fm.codeno.value + "' and le.placeno=lr.placeno";
	}
	turnPage.strQueryResult = easyQueryVer3(tSql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	fm.StopDate.value = turnPage.arrDataCacheSet[0][0];   //停用时间
	fm.AYingFee.value = turnPage.arrDataCacheSet[0][1];//应收已付款金额
	fm.YingDate.value = turnPage.arrDataCacheSet[0][2];//应收已付款收回日期
	fm.AWeiYueFee.value = turnPage.arrDataCacheSet[0][3];  //实际违约金
	fm.AWeiYueDate.value = turnPage.arrDataCacheSet[0][4];//违约金支付日期
	fm.BaoZhFee.value = turnPage.arrDataCacheSet[0][5];//保证金
	fm.BaoZhDate.value = turnPage.arrDataCacheSet[0][6];//保证金退还日期
	fm.SWeiYueFee.value= turnPage.arrDataCacheSet[0][7]//合同违约金
}
function placeIn() {
	var placeIn1 = document.getElementById("divPlaceIn1");
	var placeIn2 = document.getElementById("divPlaceIn2");
	placeIn1.style.display = "none";
	placeIn2.style.display = "none";
	var state="";
	var strSQL = "select 1 from LIPlaceRentInfo where Placeno='"+fm.codeno.value+"' ";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("职场编码不存在，请重新输入！");
		fm.codeno.value = "";
		return false;
	}
	var changeSQL = "select Preplacno from LIPlaceChangeInfo where prePlacno='"+fm.codeno.value+"'";
	turnPage.strQueryResult = easyQueryVer3(changeSQL, 1, 1, 1);
	if(turnPage.strQueryResult) {
		// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
		// 查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		fm.Placenorefer.value=turnPage.arrDataCacheSet[0][0];
		//alert("fm.Placenorefer.value====="+fm.Placenorefer.value);
		state="03";//换租
	}
	var endSQL = "select 1 from LIPlaceEndInfo where Placeno='"+fm.codeno.value+"'";
	turnPage.strQueryResult = easyQueryVer3(endSQL, 1, 1, 1);
	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	if(turnPage.strQueryResult) {
		state="04";//撤租
	}
	
	
	placeIn1.style.display = "";
	if (state == "03" || state == "04") {
		placeIn2.style.display = "";
	}
	showPlaceInfo(state);	
}

//批文下载按钮
function downloadmode() {
	//alert("../"+fm.filepath.value+fm.filename.value);
	window.location.href = ".."+fm.filepath.value+ fm.filename.value;
}

