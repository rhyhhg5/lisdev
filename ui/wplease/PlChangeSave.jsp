<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PlChangeSave.jsp
//程序功能：
//创建日期：2012-06-11 15:12:33
//创建人  ：hdl
//更新记录：  更新人    更新日期     更新原因/内容
// 鞠成富 2012-09-03 修改
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.wpleace.*"%>
  <%@page import="java.text.SimpleDateFormat" %>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LIPlaceRentInfoBSchema mLIPlaceRentInfoBSchema = new LIPlaceRentInfoBSchema();
  LIPlaceRentInfoBSet mLIPlaceRentInfoBSet = new LIPlaceRentInfoBSet();
  
  LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchema = new LIPlaceRentFeeBSchema();
  LIPlaceRentFeeBSet mLIPlaceRentFeeBSet=new LIPlaceRentFeeBSet();
  
  /* LIPlaceChangeInfoSchema mLIPlaceChangeInfoSchema=new LIPlaceChangeInfoSchema();
  LIPlaceChangeInfoSet mLIPlaceChangeInfoSet=new LIPlaceChangeInfoSet(); */
  
  LIPlaceChangeTraceSchema mLIPlaceChangeTraceSchema=new LIPlaceChangeTraceSchema();
  LIPlaceChangeTraceSet mLIPlaceChangeTraceSet= new LIPlaceChangeTraceSet();
  
  PlChangeUI mPlChangeUI  = new PlChangeUI();

  //输出参数
  CErrors tError = new CErrors();
  String tOperate="CHANGE";
  String FlagStr = "true";
  String Content = "保存成功";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  System.out.println("开始获取数据==");
  //换租职场编码
  String tCodeNo = request.getParameter("codeno");
  //基本信息
 String tCodeId = request.getParameter("CodeId");
 String tPlaceno = request.getParameter("Placeno");
 String tManagecom = request.getParameter("Managecom");
 String tApprovalType = request.getParameter("ApprovalType");
 String tApprovalYear = request.getParameter("ApprovalYear");  
 String tApprovalCode = request.getParameter("ApprovalCode");  
 String tApprovalSeria = request.getParameter("ApprovalSeria");
 String tComlevel = request.getParameter("Comlevel");  
 String tComname = request.getParameter("Comname");  
 String tBegindate = request.getParameter("Begindate");  
 String tEnddate = request.getParameter("Enddate");  
 String tAddress = request.getParameter("Address"); 
 String tLessor = request.getParameter("Lessor"); 
 String tHouseno = request.getParameter("Houseno"); 
 String tProvince = request.getParameter("Province"); 
 String tCity = request.getParameter("City"); 
 String tIsrentregister = request.getParameter("Isrentregister"); 
 String tIslessorassociate = request.getParameter("Islessorassociate"); 
 String tIslessorright = request.getParameter("Islessorright"); 
 String tIshouserent = request.getParameter("Ishouserent"); 
 String tIsplaceoffice = request.getParameter("Isplaceoffice"); 
 String tIshousegroup = request.getParameter("Ishousegroup"); 
 String tIshousefarm = request.getParameter("Ishousefarm"); 
 System.out.println("基本信息获取完毕！");
  //职场面积
  String tSigarea = request.getParameter("Sigarea");//个险
  String tGrparea = request.getParameter("Grparea");//团险
  String tBankarea = request.getParameter("Bankarea");//银行
  String tHealtharea = request.getParameter("Healtharea");//健康管理
  String tServiceArea = request.getParameter("Servicearea");//运营/客服
   String tJointarea = request.getParameter("Jointarea");//联合办公
  String tManageroffice = request.getParameter("Manageroffice");//经理室
  String tDepartpersno = request.getParameter("Departpersno");//人事行政部
  String tPlanfinance = request.getParameter("Planfinance");//财务部
  String tPublicarea = request.getParameter("Publicarea");//公共区域
  String tOtherarea = request.getParameter("Otherarea");//其他面积
  String tExplanation = request.getParameter("Explanation"); //说明
  System.out.println("职场面积信息获取完毕！");
  //合同约定职场费用
  String tMarginfee = request.getParameter("Marginfee");
  String tMarginpaydate = request.getParameter("Marginpaydate");
  String tPenaltyfee = request.getParameter("Penaltyfee");
  System.out.println("合同约定职场费用信息获取完毕！");
  //资本化日期
  //String tCapitalDate = request.getParameter("CapitalDate");
  //备注
  String tRemark = request.getParameter("Remark");
  String tHidFileName = request.getParameter("HidFileName");    //文件名
  String tHidFilePath = request.getParameter("HidFilePath");    //文件路径
  //取得租期内租金费用Muline信息
  int lineCount1 = 0;
  //String tGridNo1[] = request.getParameterValues("RelateGridNo"); 
  String tPayDate1[] = request.getParameterValues("RelateGrid1");
  String tPayAmount1[] = request.getParameterValues("RelateGrid2");
  String tPayStartDate1[] = request.getParameterValues("RelateGrid3");
  String tPayEndDate1[] = request.getParameterValues("RelateGrid4");
  lineCount1 = tPayDate1.length; //行数
  System.out.println(tPayDate1[0]);
  System.out.println(tPayAmount1[0]);
  System.out.println(tPayStartDate1[0]);
  System.out.println(tPayEndDate1[0]);
  System.out.println("租金费用length= "+String.valueOf(lineCount1));
  
  //取得租期内物业费用Muline信息
  int lineCount2 = 0;
  //String tGridNo2[] = request.getParameterValues("PropertyGridNo");
  String tPayDate2[] = request.getParameterValues("PropertyGrid1");
  String tPayAmount2[] = request.getParameterValues("PropertyGrid2");
  String tPayStartDate2[] = request.getParameterValues("PropertyGrid3");
  String tPayEndDate2[] = request.getParameterValues("PropertyGrid4");
  if(tPayDate2==null){
     lineCount2=0;
  }else{
     lineCount2 = tPayDate2.length; //行数
  }
  System.out.println("物业费length= "+String.valueOf(lineCount2));
  
  //取得租期内装修费Muline信息
  int lineCount3 = 0;
  //String tGridNo3[] = request.getParameterValues("DecorationGridNo");
  String tPayDate3[] = request.getParameterValues("DecorationGrid1");
  String tPayAmount3[] = request.getParameterValues("DecorationGrid2");
  String tPayStartDate3[] = request.getParameterValues("DecorationGrid3");
  String tPayEndDate3[] = request.getParameterValues("DecorationGrid4");
  if(tPayDate3==null){
     lineCount3=0;
  }else{
     lineCount3 = tPayDate3.length; //行数
  }
  System.out.println("装修费length= "+String.valueOf(lineCount3));
  
  //换租信息
  String tStopDate=request.getParameter("StopDate");      //合同终止日期
  String tAWeiYueFee=request.getParameter("AWeiYueFee");  //违约金
  String tAWeiYueDate=request.getParameter("AWeiYueDate"); //违约金支付日期
  String tBaoZhFee=request.getParameter("BaoZhFee");      //保证金
  String tBaoZhDate=request.getParameter("BaoZhDate");    //保证金收回日期
  String tAYingFee=request.getParameter("AYingFee");      //应收已付
  String tYingDate=request.getParameter("YingDate");      //应收已付收回日期
  
  //对从界面取到的信息二次处理
  String Appno=tApprovalType.trim()+tApprovalYear.trim()+tApprovalCode.trim()+tApprovalSeria.trim();  //对批次号进行二次处理
  System.out.println(Appno);
  SimpleDateFormat dDateFormat=new SimpleDateFormat("yyyy-MM-dd");    //获取当前日期 
  String date=dDateFormat.format(new java.util.Date());
  System.out.println(date);
  SimpleDateFormat tDateFormat=new SimpleDateFormat("HH:mm:ss");      //获取当前时间
  String time=tDateFormat.format(new java.util.Date());
  System.out.println(time); 
  
  //往职场租赁合同中添加信息
  mLIPlaceRentInfoBSchema.setPlaceno(tPlaceno);
  mLIPlaceRentInfoBSchema.setDealtype("03");//操作类型-职场换租
  mLIPlaceRentInfoBSchema.setDealstate("01");
  mLIPlaceRentInfoBSchema.setPlacenorefer(tCodeNo);
  mLIPlaceRentInfoBSchema.setManagecom(tManagecom);
  mLIPlaceRentInfoBSchema.setAppno(Appno);
  mLIPlaceRentInfoBSchema.setState("02");
  mLIPlaceRentInfoBSchema.setIslock("01");   // 01 非锁定  02 锁定
  mLIPlaceRentInfoBSchema.setComlevel(tComlevel);
  mLIPlaceRentInfoBSchema.setComname(tComname);
  mLIPlaceRentInfoBSchema.setBegindate(tBegindate);
  mLIPlaceRentInfoBSchema.setEnddate(tEnddate); 
  mLIPlaceRentInfoBSchema.setActualenddate(tEnddate);
  mLIPlaceRentInfoBSchema.setAddress(tAddress);
  mLIPlaceRentInfoBSchema.setLessor(tLessor);
  mLIPlaceRentInfoBSchema.setHouseno(tHouseno);
  mLIPlaceRentInfoBSchema.setProvince(tProvince);
  mLIPlaceRentInfoBSchema.setCity(tCity);
  mLIPlaceRentInfoBSchema.setIsrentregister(tIsrentregister);
  mLIPlaceRentInfoBSchema.setIslessorassociate(tIslessorassociate);
  mLIPlaceRentInfoBSchema.setIslessorright(tIslessorright);
  mLIPlaceRentInfoBSchema.setIshouserent(tIshouserent);
  mLIPlaceRentInfoBSchema.setIsplaceoffice(tIsplaceoffice);
  mLIPlaceRentInfoBSchema.setIshousegroup(tIshousegroup);
  mLIPlaceRentInfoBSchema.setIshousefarm(tIshousefarm);
  //职场面积
  mLIPlaceRentInfoBSchema.setSigarea(tSigarea);
  mLIPlaceRentInfoBSchema.setGrparea(tGrparea);
  mLIPlaceRentInfoBSchema.setBankarea(tBankarea);
  mLIPlaceRentInfoBSchema.setHealtharea(tHealtharea);
  mLIPlaceRentInfoBSchema.setServicearea(tServiceArea);
  mLIPlaceRentInfoBSchema.setJointarea(tJointarea);
  mLIPlaceRentInfoBSchema.setManageroffice(tManageroffice);
  mLIPlaceRentInfoBSchema.setDepartpersno(tDepartpersno);
  mLIPlaceRentInfoBSchema.setPlanfinance(tPlanfinance);
  mLIPlaceRentInfoBSchema.setPublicarea(tPublicarea);
  mLIPlaceRentInfoBSchema.setOtherarea(tOtherarea);
  mLIPlaceRentInfoBSchema.setExplanation(tExplanation);
	//合同违约金和保证金
  mLIPlaceRentInfoBSchema.setMarginfee(tMarginfee);
  mLIPlaceRentInfoBSchema.setMarginpaydate(tMarginpaydate);
  mLIPlaceRentInfoBSchema.setMarginbackdate("");
  mLIPlaceRentInfoBSchema.setPenaltyfee(tPenaltyfee);
  //
  mLIPlaceRentInfoBSchema.setRemake(tRemark);       //备注
  mLIPlaceRentInfoBSchema.setFilename(tHidFileName);
  mLIPlaceRentInfoBSchema.setFilepath(tHidFilePath);
  mLIPlaceRentInfoBSchema.setOperator(tG.Operator);
  mLIPlaceRentInfoBSchema.setMakedate(date);
  mLIPlaceRentInfoBSchema.setMaketime(time);
  mLIPlaceRentInfoBSchema.setModifydate(date);
  mLIPlaceRentInfoBSchema.setModifytime(time);
  mLIPlaceRentInfoBSet.add(mLIPlaceRentInfoBSchema);
  
  
  
  //往费用表中添加租金信息
  for(int i=0;i<lineCount1;i++)
  {
	  mLIPlaceRentFeeBSchema=new LIPlaceRentFeeBSchema();
	  mLIPlaceRentFeeBSchema.setPlaceno(tPlaceno);
	  mLIPlaceRentFeeBSchema.setFeetype("01");
	  mLIPlaceRentFeeBSchema.setOrder(Integer.toString(i+1));
	  mLIPlaceRentFeeBSchema.setPayDate(tPayDate1[i]);
	  mLIPlaceRentFeeBSchema.setMoney(tPayAmount1[i]);
	  mLIPlaceRentFeeBSchema.setPaybegindate(tPayStartDate1[i]+"-01");
      mLIPlaceRentFeeBSchema.setPayenddate(tPayEndDate1[i]+"-01");
      mLIPlaceRentFeeBSchema.setPlacenorefer(tCodeNo);
      mLIPlaceRentFeeBSchema.setDealtype("03"); //换租职场
      mLIPlaceRentFeeBSchema.setDealstate("01"); //审批状态
      mLIPlaceRentFeeBSchema.setOperator(tG.Operator);
      mLIPlaceRentFeeBSchema.setMakedate(date);
      mLIPlaceRentFeeBSchema.setMaketime(time);
      mLIPlaceRentFeeBSchema.setModifyDate(date);
      mLIPlaceRentFeeBSchema.setModifyTime(time);
      mLIPlaceRentFeeBSet.add(mLIPlaceRentFeeBSchema);   
  }
   //往费用表中添加物业费信息
  for(int i=0;i<lineCount2;i++)
  {
	  mLIPlaceRentFeeBSchema=new LIPlaceRentFeeBSchema();
	  mLIPlaceRentFeeBSchema.setPlaceno(tPlaceno);
	  mLIPlaceRentFeeBSchema.setFeetype("02");
	  mLIPlaceRentFeeBSchema.setOrder(Integer.toString(i+1));
	  mLIPlaceRentFeeBSchema.setPayDate(tPayDate2[i]);
	  mLIPlaceRentFeeBSchema.setMoney(tPayAmount2[i]);
	  mLIPlaceRentFeeBSchema.setPaybegindate(tPayStartDate2[i]+"-01");
      mLIPlaceRentFeeBSchema.setPayenddate(tPayEndDate2[i]+"-01");  
      mLIPlaceRentFeeBSchema.setPlacenorefer(tCodeNo);
      mLIPlaceRentFeeBSchema.setDealtype("03"); //换租职场
      mLIPlaceRentFeeBSchema.setDealstate("01"); //审批状态
      mLIPlaceRentFeeBSchema.setOperator(tG.Operator);
      mLIPlaceRentFeeBSchema.setMakedate(date);
      mLIPlaceRentFeeBSchema.setMaketime(time);
      mLIPlaceRentFeeBSchema.setModifyDate(date);
      mLIPlaceRentFeeBSchema.setModifyTime(time); 
      mLIPlaceRentFeeBSet.add(mLIPlaceRentFeeBSchema);  
  }
   //往费用表中添加装修费信息
  for(int i=0;i<lineCount3;i++)
  {
	  mLIPlaceRentFeeBSchema=new LIPlaceRentFeeBSchema();
	  mLIPlaceRentFeeBSchema.setPlaceno(tPlaceno);
	  mLIPlaceRentFeeBSchema.setFeetype("03");
	  mLIPlaceRentFeeBSchema.setOrder(Integer.toString(i+1));
	  mLIPlaceRentFeeBSchema.setPayDate(tPayDate3[i]);
	  mLIPlaceRentFeeBSchema.setMoney(tPayAmount3[i]);
	  mLIPlaceRentFeeBSchema.setPaybegindate(tPayStartDate3[i]+"-01");
      mLIPlaceRentFeeBSchema.setPayenddate(tPayEndDate3[i]+"-01");
      mLIPlaceRentFeeBSchema.setPlacenorefer(tCodeNo);
      mLIPlaceRentFeeBSchema.setDealtype("03"); //换租职场
      mLIPlaceRentFeeBSchema.setDealstate("01"); //审批状态 
	  mLIPlaceRentFeeBSchema.setOperator(tG.Operator);
      mLIPlaceRentFeeBSchema.setMakedate(date);
      mLIPlaceRentFeeBSchema.setMaketime(time);
      mLIPlaceRentFeeBSchema.setModifyDate(date);
      mLIPlaceRentFeeBSchema.setModifyTime(time);
      mLIPlaceRentFeeBSet.add(mLIPlaceRentFeeBSchema);    
  }
  
  
  //职场变更轨迹表
  mLIPlaceChangeTraceSchema.setPlaceno(tPlaceno);
  mLIPlaceChangeTraceSchema.setPlacenorefer(tCodeNo);//关联职场编码即原来的职场编码
  mLIPlaceChangeTraceSchema.setChangetype("03");//职场换租
  mLIPlaceChangeTraceSchema.setChangestate("01");//待审批
  mLIPlaceChangeTraceSchema.setEnddate(tStopDate);
  mLIPlaceChangeTraceSchema.setShouldgetfee(tAYingFee);
  mLIPlaceChangeTraceSchema.setShouldgetdate(tYingDate);
  mLIPlaceChangeTraceSchema.setApenaltyfee(tAWeiYueFee);
  mLIPlaceChangeTraceSchema.setApenaltyfeegetdate(tAWeiYueDate);
  mLIPlaceChangeTraceSchema.setMarginfee(tBaoZhFee);
  mLIPlaceChangeTraceSchema.setMarginfeegetdate(tBaoZhDate);
  mLIPlaceChangeTraceSchema.setOperator(tG.Operator);
  mLIPlaceChangeTraceSchema.setMakedate(date);
  mLIPlaceChangeTraceSchema.setMaketime(time);
  mLIPlaceChangeTraceSchema.setModifyDate(date);
  mLIPlaceChangeTraceSchema.setModifyTime(time);
  mLIPlaceChangeTraceSet.add(mLIPlaceChangeTraceSchema);
  
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.add(mLIPlaceRentInfoBSet);
  tVData.add(mLIPlaceRentFeeBSet);
  tVData.add(mLIPlaceChangeTraceSet);
  tVData.add(tCodeNo);
  //tVData.add(mLIPlaceChangeInfoSet);
  System.out.println("vdate======="+tVData.size());
  System.out.println("add over");
  try
  {
	  if(!mPlChangeUI.submitData(tVData,tOperate)){
	      if (mPlChangeUI.mErrors.needDealError())
                {
                    tError.copyAllErrors(mPlChangeUI.mErrors);
                    FlagStr = "Fail";
                    Content = "保存失败！原因是："+tError.getFirstError();
                }
                else
                {
                    FlagStr = "Fail";
                    Content = "保存失败！但没有获取到详细报错信息";
                }
	  }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //添加各种预处理

  %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>