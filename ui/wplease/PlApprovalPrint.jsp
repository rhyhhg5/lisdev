<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
//name :PlaceInInput.jsp
//function :
//Creator :huodonglei
//date :2011-6-8

%>

	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "PlApprovalPrint.js"></SCRIPT> 
<%@include file="PlApprovalPrintInit.jsp"%>
</head>
<body onload="initElementtype();initForm()" >
    <form action="PlApprovalPrintSave.jsp" method=post name=fm target="fraSubmit">
    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divBasicInfo);"></td>
    	<td class= titleImg>基本信息</td></tr>
    </table>
    <Div id= "divBasicInfo" style= "display: ''">
		<Table class= common>
   			<TR class= common>
<!--   				<TD class= title>-->
<!--   			 		编码-->
<!--   				</TD>-->
<!--   				<TD class= input>-->
<!--   					<Input class= common name= CodeId id="CodeId" > -->
<!--   				</TD>-->
   				<TD class= title>
   			 		公司级别
   				</TD>
   				<TD class= input>
   					<Input class=codeno  NAME="Comlevel"   ondblClick="return showCodeList('companylevel',[this,Comlevelname],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('companylevel',[this,Comlevelname],[0,1],null,null,null,1);"  verify="公司级别|NOTNULL" readOnly><input class=codename name=Comlevelname readOnly >
   				</TD>
   				<TD class= title>
    	             公司代码
    	        </TD>
    	        <TD class= input >
   					<Input class= "codeno"  name=CompanyCode readonly="readonly" ondblclick="return showCodeList('comcode',[this,CompanyCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,CompanyCodeName],[0,1],null,null,null,1);" ><Input class=codename readonly="readonly" name=CompanyCodeName>
   				</TD>
   				<TD class= title>
    	             在用状态
    	        </TD>
    	        <TD class= input >
   					<Input class= "codeno"  name=UseState readonly="readonly" CodeData="0|^1|全部|^2|在用"  ondblclick="return showCodeListEx('',[this,stateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('',[this,stateName],[0,1],null,null,null,1);" ><Input class=codename readonly="readonly" name=stateName>
   				</TD>
<!--	        	<td class = title>-->
<!--	        		距到期日-->
<!--	        	</td>-->
<!--	        	<td class = input>-->
<!--	        		<Input name=ToDate class=common>-->
<!--	        	</td>  	 -->
	        </Tr>
    </table>
    <table>
			<tr>
			<td ><INPUT VALUE="清单打印" class=cssButton TYPE=button onclick="print()"></td>
			</tr>
	</table>
	<div style="display:'none'">
    <table class=common>
			<tr class=common>
				<td class=titleImg >基本信息列表</td>
			</tr>
			<tr class=common>
				<td text-align: left colSpan=1>
					<span id="spanMessageGrid"></span>
				</td>
			</tr>
		</table>
		<Div id= "divPage" align=center style= "display: '' ">
			<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
	</div>
    </form>
	 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>