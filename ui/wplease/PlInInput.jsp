<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
//name :PlaceInInput.jsp
//function :
//Creator :huodonglei
//date :2011-6-8
%>

	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "InputMain.js"></SCRIPT> 
<SCRIPT src = "PlInInput.js"></SCRIPT> 
<SCRIPT src = "PlUpdateFee.js"></SCRIPT> 
<%@include file="PlInInit.jsp"%>
</head>
<body onload="initElementtype();initForm()" >
  <form action="PlInSave.jsp" method=post name=fm target="fraSubmit">
    <jsp:include page="InputMain.jsp" flush="true"/>
  </form>
  <form action="PlInFeeSave.jsp" method=post name=feeimp target="fraSubmit" ENCTYPE="multipart/form-data">
    <jsp:include page="PlUpdateFee.jsp" flush="true"/>
  </form>
  <form action="PlInFileSave.jsp" method=post name=imp target="fraSubmit" ENCTYPE="multipart/form-data">
    <Table class= common>
  	    <TR class= common>
  	 	    <TD class= title>
			    文件名
			</TD>     
			<TD>
			  <Input type="file" name=FileName class=common>
			  <INPUT VALUE="上载租赁合同及批文" class=cssButton TYPE=button onclick="ApprovalImp();">
			</TD>
			<TD>
			  <INPUT VALUE="保存" class=cssButton TYPE=button onclick="submitForm()">
			</TD>
		</TR>
	</Table>
	<span style="color:red">文件请不要超过4M</span>
     <br></br>
    </form>
	 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>