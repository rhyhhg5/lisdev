function ApprovalImp() {
	if (imp.all('FileName').value == "") {
		alert("请选择需要上传的文件");
		return;
	}
	var i = 0;
	var ImportFile = imp.all('FileName').value.toLowerCase();
	var Placeno = document.getElementsByName("Placeno")[0].value;
	if (Placeno == null || Placeno == "") {
		alert("编码信息尚未生成，请先录入其他信息！");
		return false;
	}
	getImportPath();
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	imp.action = "./PlInFileSave.jsp?ImportPath=" + ImportPath + "&ImportFile="+ ImportFile + "&CodeId=" + Placeno;
	imp.submit(); // 提交

}

function checkPlaceNO(mPlaceNO){
        
    var checkPlace = "select 1 from LIPlaceRentinfo where Placeno='" +mPlaceNO+"'";
    turnPage.strQueryResult  = easyQueryVer3(checkPlace, 1, 1, 1);  
    //判断是否查询成功
    if (turnPage.strQueryResult) {
  	  alert("新合同职场编码已存在，请重新输入！");
      return false;
	}
	var checkTrace = "select 1 from LIPlaceChangeTrace where Placeno='" +mPlaceNO+"' and changestate <> 'DL' ";
    turnPage.strQueryResult  = easyQueryVer3(checkTrace, 1, 1, 1);  
    //判断是否查询成功
    if (turnPage.strQueryResult) {
  	  alert("新合同职场编码已存在，并处于审批流程中，请重新输入！");
      return false;
	}
	return true;
}
function getImportPath() {
	// 书写SQL语句
	var strSQL = "select SysvarValue from ldsysvar where sysvar ='ApprovalPath'";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有找到上传路径");
		return false;
	}
	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	ImportPath = turnPage.arrDataCacheSet[0][0];
}
function setCode() {
	var Managecom = document.getElementsByName("Managecom")[0].value;
	var ApprovalType = document.getElementsByName("ApprovalType")[0].value;
	var ApprovalYear = document.getElementsByName("ApprovalYear")[0].value;
	var ApprovalCode = document.getElementsByName("ApprovalCode")[0].value;
	var ApprovalSeria = document.getElementsByName("ApprovalSeria")[0].value;
	var Placeno = document.getElementsByName("PlacenoN")[0];
	if (Managecom == null || Managecom == "") {
		//alert("请输入公司代码");
		return false;
	}
	if (ApprovalType == null || ApprovalType == "") {
		//alert("请输入批文号");
		return false;
	}
	if (ApprovalYear == null || ApprovalYear == "") {
		//alert("请输入批文号");
		return false;
	}
	if (ApprovalCode == null || ApprovalCode == "") {
		//alert("请输入批文号");
		return false;
	}
	if (ApprovalSeria == null || ApprovalSeria == "") {
		//alert("请输入批文号");
		return false;
	}
	var id = Managecom.trim() + ApprovalType.trim() + ApprovalYear.trim()
			+ ApprovalCode.trim() + ApprovalSeria.trim();
	var strSQL = "select 1 from LIPlaceRentinfo where Placeno='"+id+"' union select 1 from LIPlaceRentinfob where Placeno='"+id+"'";			 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	//判断是否查询成功
	if (turnPage.strQueryResult) {
	  alert("该职场编码已存在，请重新输入！");
	  document.getElementsByName("ApprovalType")[0].value="";
	  document.getElementsByName("Managecom")[0].value="";
	  document.getElementsByName("ApprovalYear")[0].value="";
	  document.getElementsByName("ApprovalCode")[0].value="";
	  document.getElementsByName("ApprovalSeria")[0].value="";
	  return false;
	}
	Placeno.value=id;
	
}

//计算总面积
function sumArea(){
    var tSigarea = document.getElementsByName("Sigarea")[0].value*1;
    var tGrparea = document.getElementsByName("Grparea")[0].value*1;
    var tBankarea = document.getElementsByName("Bankarea")[0].value*1;
    var tHealtharea = document.getElementsByName("Healtharea")[0].value*1;
    var tServicearea = document.getElementsByName("Servicearea")[0].value*1;
    var tJointarea = document.getElementsByName("Jointarea")[0].value*1;
    var tManageroffice = document.getElementsByName("Manageroffice")[0].value*1;
    var tDepartpersno = document.getElementsByName("Departpersno")[0].value*1;
    var tPlanfinance = document.getElementsByName("Planfinance")[0].value*1;
    var tPublicarea = document.getElementsByName("Publicarea")[0].value*1;
    var tOtherarea = document.getElementsByName("Otherarea")[0].value*1;
    var tZSumarea = document.getElementsByName("ZSumarea")[0];

    tZSumarea.value=tSigarea+tGrparea+tBankarea+tHealtharea+tServicearea+tJointarea+tManageroffice+tDepartpersno+tPlanfinance+tPublicarea+tOtherarea
    return true;
}

function checkNum(aaa){
    if (aaa == null || aaa == "") {
		return true;
	}
	var regDate = /^\d+(\.\d+)?$/;
	if (!regDate.test(aaa)) {
		return false;
	}
	return true;
}

function checkdate(SDate, EData) {
	if (SDate == null || SDate == "") {
		alert("请输入租期起期");
		return false;
	}
	if (EData == null || EData == "") {
		alert("请输入租期止期");
		return false;
	}
	var aDate = EData.split("-");
	var oDate1 = new Date(aDate[1] + '-' + aDate[2] + '-' + aDate[0]); // 结束日期
	aDate = SDate.split("-");
	var oDate2 = new Date(aDate[1] + '-' + aDate[2] + '-' + aDate[0]); // 开始日期
	if (oDate1 <= oDate2) {
		alert("'租期止期'必须大于'租期起期'!");
		return false;
	} else {
		return true;
	}

}

function sumFee() {
	var i1 = 0; // 租金
	var i2 = 0; // 物业费
	var i3 = 0; // 装修费
	var SumRenFee = 0;
	var SumProFee = 0;
	var SumDecFee = 0;
	var rowNum1 = RelateGrid.mulLineCount;
	var rowNum2 = PropertyGrid.mulLineCount;
	var rowNum3 = DecorationGrid.mulLineCount;
	var colNum1 = RelateGrid.colCount;
	var colNum2 = PropertyGrid.colCount;
	var colNum3 = DecorationGrid.colCount;
	
	//
	var monthCount1=0;
	var monthCount2=0;
	
	if (rowNum1 == 0) {
		alert("租期内租金费用表格中不能为空!");
		return false;
	}
/*	if (rowNum2 == 0) {
		alert("租期内物业费用不能为空!");
		return false;
	}
	if (rowNum3 == 0) {
		alert("租期内装修费用不能为空!");
		return false;
	}*/
	
	//租期内租金费用
	for ( var x = 0; x < rowNum1; x = x + 1) {
		for ( var y = 1; y < colNum1; y = y + 1)
		{
			if (RelateGrid.getRowColData(x, y) == null
					|| RelateGrid.getRowColData(x, y) == "") {
				alert("租期内租金费用表格中不能有空项");
				return false;
			}
			if (y == 1) 
			{
				if (!IsDate(RelateGrid.getRowColData(x, y))) {
					alert("租期内租金费用表格中'支付日期'有误！");
					return false;
				}
			}
			if (y != 2&&y != 1) 
			{
				if (!checkDate(RelateGrid.getRowColData(x, y))) {
					alert("支付租期摊销格式应该为：yyyy-mm!");
					return false;
				}
			}
		}
	}
	//租期内租金费用 日期校验
	for ( var x = 0; x < rowNum1; x = x + 1) {
		if(!testduobledate(x,RelateGrid,rowNum1)){
			return false;
		}
		var begindate=RelateGrid.getRowColData(x, 3);
		var enddate=RelateGrid.getRowColData(x, 4);
		monthCount1=monthCount1+diffMonths(begindate,enddate)*1;
	}
	//租期内物业费费用
	for ( var x = 0; x < rowNum2; x = x + 1) {
		for ( var y = 1; y < colNum2; y = y + 1) {
			if (PropertyGrid.getRowColData(x, y) == null
					|| PropertyGrid.getRowColData(x, y) == "") {
				alert("租期内物业费用表格中不能有空项");
				return false;
			}
			if (y == 1) {
				if (!IsDate(PropertyGrid.getRowColData(x, y))) {
					alert("租期内物业费表格中日期有误！");
					return false;
				}
			}
			if (y != 2&&y != 1) 
			{
				if (!checkDate(PropertyGrid.getRowColData(x, y))) {
					alert("支付租期摊销格式应该为：yyyy-mm!");
					return false;
				}
			}
		}
	}
	
	//租期内物业费用 日期校验
	for ( var x = 0; x < rowNum2; x = x + 1) {
		if(!testduobledate(x,PropertyGrid,rowNum2)){
			return false;
		}
		var begindate=PropertyGrid.getRowColData(x, 3);
		var enddate=PropertyGrid.getRowColData(x, 4);
		monthCount2=monthCount2+diffMonths(begindate,enddate)*1;
	}
	//租期内装修费用
	for ( var x = 0; x < rowNum3; x++) {
		for ( var y = 1; y < colNum3; y++) {
			if (DecorationGrid.getRowColData(x, y) == null
					|| DecorationGrid.getRowColData(x, y) == "") {
				alert("租期内装修费用表格中不能有空项");
				return false;
			}
			if (y == 1) {
				if (!IsDate(DecorationGrid.getRowColData(x, y))) {
					alert("租期内装修费表格中日期有误！");
					return false;
				}
			}
			if (y != 2&&y != 1) 
			{
				if (!checkDate(DecorationGrid.getRowColData(x, y))) {
					alert("支付租期摊销格式应该为：yyyy-mm!");
					return false;
				}
			}
		}
	}
	
	//校验摊销期间是否覆盖整个租期
	if (rowNum1 !=0) {
		if(!testAllDate(monthCount1)){
		   alert("租金摊销期间没有覆盖整个租期,备注不能为空！");
		   return false;
	    }
	}
	if (rowNum2 != 0) {
		if(!testAllDate(monthCount2)){
		   alert("物业摊销期间没有覆盖整个租期,备注不能为空！");
		   return false;
	    }
	}

	
	
	while (rowNum1 > i1) {
		var m = RelateGrid.getRowColData(i1, 2);
		if (m != null && m != "") {
			SumRenFee += RelateGrid.getRowColData(i1, 2) * 1;
		}
		i1 = i1 + 1;
	}
	while (rowNum2 > i2) {
		var m1 = PropertyGrid.getRowColData(i2, 2);
		if (m != null && m != "") {
			SumProFee += PropertyGrid.getRowColData(i2, 2) * 1;
		}
		i2 = i2 + 1;
	}
	while (rowNum3 > i3) {
		var m2 = DecorationGrid.getRowColData(i3, 2);
		if (m != null && m != "") {
			SumDecFee += DecorationGrid.getRowColData(i3, 2) * 1;
		}
		i3 = i3 + 1;
	}
	document.getElementsByName("SumRenFee")[0].value = SumRenFee.toFixed(2);
	document.getElementsByName("SumProFee")[0].value = SumProFee.toFixed(2);
	document.getElementsByName("SumDecFee")[0].value = SumDecFee.toFixed(2);
	return true;
}

function IsDate(oTextbox) {
	var regDate = /^(\d{4})-(\d{1,2})-(\d{1,2})$/;
	if (!regDate.test(oTextbox)) {
		return false;
	}
	var arr = regDate.exec(oTextbox);
	return IsMonthAndDateCorrect(arr[1], arr[2], arr[3]);

}

// 判断年、月、日的取值范围是否正确
function IsMonthAndDateCorrect(nYear, nMonth, nDay) {
	// 月份是否在1-12的范围内，注意如果该字符串不是C#语言的，而是JavaScript的，月份范围为0-11
	if (nMonth > 12 || nMonth <= 0) {
		return false;
	}

	// 日是否在1-31的范围内，不是则取值不正确
	if (nDay > 31 || nDay <= 0) {
		return false;
	}

	// 根据月份判断每月最多日数
	var bTrue = false;
	if (nMonth == 12 || nMonth == 1 || nMonth == 3 || nMonth == 5
			|| nMonth == 7 || nMonth == 8 || nMonth == 10) {
		bTrue = true;
	}
	if (nMonth == 6 || nMonth == 9 || nMonth == 4 || nMonth == 11) {
		bTrue = (nDay <= 30);
	}
	if (bTrue) {
		return true;
	}
	// 2月的情况
	// 如果小于等于28天一定正确
	if (nDay <= 28) {
		return true;
	}
	// 闰年小于等于29天正确
	if (IsLeapYear(nYear))
		return (nDay <= 29);
	// 不是闰年，又不小于等于28，返回false
	return false;
}

// 是否为闰年，规则：四年一闰，百年不闰，四百年再闰
function IsLeapYear(nYear) {
	// 如果不是4的倍数，一定不是闰年
	if (nYear % 4 != 0)
		return false;
	// 是4的倍数，但不是100的倍数，一定是闰年
	if (nYear % 100 != 0)
		return true;

	// 是4和100的倍数，如果又是400的倍数才是闰年
	return (nYear % 400 == 0);
}
function checkDate(aaa){
	var regDate = /^(\d{4})-(\d{1,2})$/;
	if (!regDate.test(aaa)) {
		return false;
	}
	var arr = regDate.exec(aaa);
	return IsMonthAndDateCorrect(arr[1], arr[2], 1);
}
function comparedate(begindate,enddate,flag){
    
	var bdate = begindate.split('-');
	var edate = enddate.split('-');
	var b_Year= bdate[0]*1;
	var e_Year= edate[0]*1;
	var b_Month= bdate[1]*1;
	var e_Month= edate[1]*1;
	if(b_Year>e_Year){
		return false;
	}
	if(b_Year==e_Year){
		if(flag==1){
	       if(b_Month>e_Month){
	    	   return false;
	    	   }
		}else{
		   if(b_Month>=e_Month){return false;}
	    }
	}
	return true;
}
function diffMonths(bdate,edate){
	var b=bdate.split('-');
	var e=edate.split('-');
	return (e[0]-b[0])*12+(e[1]-b[1])+1;
}
function testduobledate(x,mulname,mulcount){
	//获取租期起期和止期
	var StartDate = document.getElementsByName("Begindate")[0].value;
	var endData = document.getElementsByName("Enddate")[0].value;
	
	//获取本次摊销起期和止期
	var xbegindate=mulname.getRowColData(x, 3);
	var xenddate=mulname.getRowColData(x, 4);
	
	//摊销起期应该小于摊销止期
	if(!comparedate(xbegindate,xenddate,1)){
		alert("摊销起期大于摊销止期,摊销起期:"+xbegindate+";摊销止期："+xenddate);
		return false;
	}
	//摊销起期应该小于租期起期
	if(!comparedate(StartDate,xbegindate,1)){
		alert("摊销起期小于职场租期起期，请核实！摊销起期："+xbegindate+";租期起期："+StartDate);
		return false;
	}
	//
	if((x+1)!=mulcount){
		var nbegindate=mulname.getRowColData(x+1, 3);
		if(!comparedate(xenddate,nbegindate,2)){
			alert("摊销区间重叠，摊销起期："+nbegindate+";摊销止期："+xenddate);
			return false;
		}
	}else{
		if(!comparedate(xenddate,endData,1)){
			alert("摊销止期大于职场租期止期，摊销止期:"+xenddate+";租期止期："+endData);
			return false;
	}
	}
/*    if(x!=0){
    	var penddate=mulname.getRowColData(x-1, 4);
    	alert("penddate"+penddate);
    	alert("xbegindate"+xbegindate);
		if(!testdate(penddate,xbegindate)){
			alert("摊销区间重叠，请核实!");
			return false;
		}
    }*/
	
		return true;
}
function testAllDate(m1){
	//获取租期起期和止期
	var StartDate = document.getElementsByName("Begindate")[0].value;
	var endData = document.getElementsByName("Enddate")[0].value;
	var sdate=StartDate.split('-');
	var edate=endData.split('-');
	//获取备注
	var remark=document.getElementsByName("Remark")[0].value;
	//计算租期月份
	var yeartomonths=(edate[0]-sdate[0])*12;
	var monthtomonths=(edate[1]-sdate[1])*1;
	var daytomonths=0
	if(edate[2]>sdate[2]){
		daytomonths=daytomonths+1;
	}
	var months=yeartomonths+monthtomonths+daytomonths;
	if(months>m1){
		if(remark==null || remark==""){
		  return false;
		}
	}
	return true;
}
function testAddress(address){
	var checksql = "select length('"+ address +"') from dual with ur";
	turnPage.strQueryResult = easyQueryVer3(checksql, 1, 1, 1);
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	var length = turnPage.arrDataCacheSet[0][0];
//	alert("address长度---"+length);
	if(turnPage.arrDataCacheSet[0][0]>300){
		alert("职场地址长度超过100字，请重新输入");
		return false;
	}
	return true;
}