<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PlDecAddSave.jsp
//程序功能：
//创建日期：2012-06-12 15:12:33
//创建人  ：hdl
//更新记录：  更新人    更新日期     更新原因/内容
//      鞠成富	2012-09-12  修改
%>
<!--用户校验类-->nosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.wpleace.*"%>
  <%@page import="java.text.SimpleDateFormat" %>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchema;
  LIPlaceRentFeeBSet mLIPlaceRentFeeBSet=new LIPlaceRentFeeBSet();
  LIPlaceRentFeeSet mLIPlaceRentFeeSet= new LIPlaceRentFeeSet();
  LIPlaceChangeTraceSchema mLIPlaceChangeTraceSchema = new LIPlaceChangeTraceSchema();
  LIPlaceChangeTraceSet mLIPlaceChangeTraceSet = new LIPlaceChangeTraceSet();
  PlDecAddUI mPlDecAddUI  = new PlDecAddUI();

  //输出参数
  CErrors tError = new CErrors();
  String tOperate="ADD";
  String tRela  = "";
  String FlagStr = "true";
  String Content = "保存成功";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  System.out.println("开始获取数据==");
  //基本信息  职场编码
  String tCodeId = request.getParameter("codeno");
  System.out.println("基本信息获取完毕！");
  
  //取得租期内装修费Muline信息
  int lineCount3 = 0;
  //String tGridNo3[] = request.getParameterValues("DecorationGridNo");
  String tPayDate3[] = request.getParameterValues("DecorationGrid1");
  String tPayAmount3[] = request.getParameterValues("DecorationGrid2");
  String tPayStartDate3[] = request.getParameterValues("DecorationGrid3");
  String tPayEndDate3[] = request.getParameterValues("DecorationGrid4");
  if(tPayDate3==null){
     lineCount3=0;
  }else{
     lineCount3 = tPayDate3.length; //行数
  }
  System.out.println("装修费length= "+String.valueOf(lineCount3));
  
  SimpleDateFormat dDateFormat=new SimpleDateFormat("yyyy-MM-dd");    //获取当前日期 
  String date=dDateFormat.format(new java.util.Date());
  System.out.println(date);
  SimpleDateFormat tDateFormat=new SimpleDateFormat("HH:mm:ss");      //获取当前时间
  String time=tDateFormat.format(new java.util.Date());
  System.out.println(time);
  
  String sql="select paybegindate,payenddate from LIPlaceRentFee where placeno='"+tCodeId+"' and feetype='03' ";
  System.out.println("sql:"+sql);
  ExeSQL exesql = new ExeSQL();
  SSRS tssrs = exesql.execSQL(sql);
  int length=tssrs.MaxRow;
   
   //往费用表中添加装修费信息
  for(int i=0;i<lineCount3;i++)
  {
	  mLIPlaceRentFeeBSchema=new LIPlaceRentFeeBSchema();
	  mLIPlaceRentFeeBSchema.setPlaceno(tCodeId);
	  mLIPlaceRentFeeBSchema.setFeetype("03");
	  mLIPlaceRentFeeBSchema.setOrder(Integer.toString(i+1+length));
	  mLIPlaceRentFeeBSchema.setPayDate(tPayDate3[i]);
	  mLIPlaceRentFeeBSchema.setMoney(tPayAmount3[i]);
	  mLIPlaceRentFeeBSchema.setPaybegindate(tPayStartDate3[i]+"-01");
      mLIPlaceRentFeeBSchema.setPayenddate(tPayEndDate3[i]+"-01"); 
      mLIPlaceRentFeeBSchema.setDealtype("06");//操作类型
      mLIPlaceRentFeeBSchema.setDealstate("01");//操作状态
	  mLIPlaceRentFeeBSchema.setOperator(tG.Operator);
      mLIPlaceRentFeeBSchema.setMakedate(date);
      mLIPlaceRentFeeBSchema.setMaketime(time);
      mLIPlaceRentFeeBSchema.setModifyDate(date);
      mLIPlaceRentFeeBSchema.setModifyTime(time);
      mLIPlaceRentFeeBSet.add(mLIPlaceRentFeeBSchema);
  }
  //职场变更轨迹表
  mLIPlaceChangeTraceSchema.setPlaceno(tCodeId);
  mLIPlaceChangeTraceSchema.setChangetype("06");    //职场录入
  mLIPlaceChangeTraceSchema.setChangestate("01");   //待审批
  mLIPlaceChangeTraceSchema.setOperator(tG.Operator);
  mLIPlaceChangeTraceSchema.setMakedate(date);
  mLIPlaceChangeTraceSchema.setMaketime(time);
  mLIPlaceChangeTraceSchema.setModifyDate(date);
  mLIPlaceChangeTraceSchema.setModifyTime(time);
  mLIPlaceChangeTraceSet.add(mLIPlaceChangeTraceSchema);

  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tG);
  tVData.add(mLIPlaceRentFeeBSet);
  tVData.add(mLIPlaceChangeTraceSet);
  tVData.add(mLIPlaceRentFeeSet);
  System.out.println("vdate======="+tVData.size());
  System.out.println("add over");
  try
  {
	  if(!mPlDecAddUI.submitData(tVData,tOperate,tCodeId)){
	     if (mPlDecAddUI.mErrors.needDealError())
           {
               tError.copyAllErrors(mPlDecAddUI.mErrors);
               FlagStr = "Fail";
               Content=tError.getFirstError();
           }
           else
           {
               Content="保存失败，没有得到报错详细信息！";
               FlagStr = "Fail";
           }
	  }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //添加各种预处理

  %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>