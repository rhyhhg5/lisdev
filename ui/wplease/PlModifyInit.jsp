
<%
	//name :PlModifyInit.jsp
	//function : 
	//Creator :huodonglei
	//date :2011-06-09
	//鞠成富 2012-09-10 修改
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="InputMainInit.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String Comcode = tG.ManageCom;
	String CurrentDate = PubFun.getCurrentDate();
	String tCurrentYear = StrTool.getVisaYear(CurrentDate);
	String tCurrentMonth = StrTool.getVisaMonth(CurrentDate);
	String tCurrentDate = StrTool.getVisaDay(CurrentDate);
%>
<script language="JavaScript">
	function initForm() 
	{
		try 
		{
			fm.CurrentDate.value='<%=CurrentDate%>';
			initRelateGrid();
			initPropertyGrid();
			initDecorationGrid();
			initModifyGrid();
			modifyInit('<%=Comcode%>');
		} 
		catch (re) 
		{
			alert("PlModifyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	function initModifyGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;

			iArray[1] = new Array();
			iArray[1][0] = "职场编码";
			iArray[1][1] = "50px";
			iArray[1][2] = 100;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "批文号";
			iArray[2][1] = "30px";
			iArray[2][2] = 100;
			iArray[2][3] = 0;
			
			iArray[3] = new Array();
			iArray[3][0] = "租期起期"; //列名
			iArray[3][1] = "25px"; //列宽
			iArray[3][2] = 200; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[4] = new Array();
			iArray[4][0] = "租期止期"; //列名
			iArray[4][1] = "25px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[5] = new Array();
			iArray[5][0] = "操作员"; //列名
			iArray[5][1] = "15px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[6] = new Array();
			iArray[6][0] = "操作日期"; //列名
			iArray[6][1] = "25px"; //列宽
			iArray[6][2] = 200; //列最大值
			iArray[6][3] = 0; //是否允许输入,1表示允许，0表示不允许
		

			ModifyGrid = new MulLineEnter("fm", "ModifyGrid");
			ModifyGrid.mulLineCount = 0;
			ModifyGrid.displayTitle = 1;
			ModifyGrid.canSel = 1;
			ModifyGrid.hiddenPlus = 1;
			ModifyGrid.hiddenSubtraction = 1;
			ModifyGrid.loadMulLine(iArray);
			ModifyGrid.detailInfo = "单击显示详细信息"; 
		} catch (ex) {
			alert("ModifyGrid初始化时出错:" + ex);
		}
	}

</script>