
<%
	//name :PlApprovalInit.jsp
	//function : 
	//Creator :
	//date :2013-12-09
	//* 更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String Comcode = tG.ManageCom;
	String CurrentDate = PubFun.getCurrentDate();
	String tCurrentYear = StrTool.getVisaYear(CurrentDate);
	String tCurrentMonth = StrTool.getVisaMonth(CurrentDate);
	String tCurrentDate = StrTool.getVisaDay(CurrentDate);
%>
<script language="JavaScript">
	function initForm() 
	{
		try 
		{
			initApproveGrid();
			appInit('<%=Comcode%>');
		} 
		catch (re) 
		{
			alert("PlManageAuditInit.jspInitForm函数中发生异常:初始化界面错误!");
		}
	}
	function initApproveGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;

			iArray[1] = new Array();
			iArray[1][0] = "职场编码";
			iArray[1][1] = "60px";
			iArray[1][2] = 100;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "机构名称";
			iArray[2][1] = "100px";
			iArray[2][2] = 200;
			iArray[2][3] = 0;

			iArray[3] = new Array();
			iArray[3][0] = "职场地址"; //列名
			iArray[3][1] = "100px"; //列宽
			iArray[3][2] = 200; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[4] = new Array();
			iArray[4][0] = "流水号"; //列名
			iArray[4][1] = "0px"; //列宽
			iArray[4][2] = 100; //列最大值
			iArray[4][3] = 3; //是否允许输入,1表示允许，0表示不允许

			ApproveGrid = new MulLineEnter("fm", "ApproveGrid");
			ApproveGrid.mulLineCount = 0;
			ApproveGrid.displayTitle = 1;
//			ApproveGrid.canSel = 1;
			ApproveGrid.hiddenPlus = 1;
			ApproveGrid.hiddenSubtraction = 1;
			ApproveGrid.loadMulLine(iArray);
			ApproveGrid.detailInfo = "单击显示详细信息"; 
		} catch (ex) {
			alert("initApproveGrid初始化时出错:" + ex);
		}
	}
</script>