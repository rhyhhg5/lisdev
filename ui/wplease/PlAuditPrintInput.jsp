<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//程序名称：PlAuditPrintInput.jsp
	//程序功能：
	//创建日期：2012-09-21
	//创建人  ：鞠成富
	//更新记录：  更新人    更新日期     更新原因/内容
%>

	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "PlAuditPrintInput.js"></SCRIPT> 
<%@include file="PlAuditPrintInit.jsp"%>
</head>
<body onload="initElementtype();initForm()" >
    <form action="PlAuditPrintSave.jsp" method=post name=fm target="fraSubmit">
    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divBasicInfo);"></td>
    	<td class= titleImg>基本信息</td></tr>
    </table>
    <Div id= "divBasicInfo" style= "display: ''">
		<Table class= common>
	        <tr>
	            <td class= title>统计起期</td>
	            <td class= input><Input name=BeginDate id=BeginDate  class='coolDatePicker' dateFormat='short' > </td>
	            <td class= title>统计止期</td>
	            <td class= input><Input name=EndDate id=EndDate class='coolDatePicker' dateFormat='short' > </td>
	            <TD  class= title>
            管理机构
          </TD>
          <td  class= input><Input class= "codeno" readonly="readonly" name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename readonly="readonly" name=ManageComName></td>        
	        </tr>
	</table>
	<br>
	<INPUT VALUE="清单打印" class=cssButton TYPE=button onclick="print()">
	<input type="hidden" id="fmtransact" name="fmtransact" />
    </form>
	 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>