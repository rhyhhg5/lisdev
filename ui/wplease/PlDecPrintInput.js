var showInfo;
var ImportPath;
var turnPage = new turnPageClass();

function print()
{
    var SDate=document.getElementsByName("BeginDate")[0].value;
    var EData=document.getElementsByName("EndDate")[0].value;
    if(!checkDate(SDate,EData)){ return false;}
	fm.target = "f1print";
	fm.action="./PlDecPrintSave.jsp";
	fm.fmtransact.value="PRINT";
	submitForm();
	showInfo.close();	
}

function IsDate(aaa){
	var regDate = /^(\d{4})-(\d{1,2})$/;
	if (!regDate.test(aaa)) {
		return false;
	}
	var arr = regDate.exec(aaa);
	return IsMonthAndDateCorrect(arr[1], arr[2], 1);
}

//提交，保存按钮对应操作
function submitForm()
{
	  var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.submit(); //提交
}

function checkDate(SDate,EData)
{
	if (SDate == null || SDate == "") {
		alert("请输入统计起期");
		return false;
	}
	if (EData == null || EData == "") {
		alert("请输入统计止期");
		return false;
	}
	if(!IsDate(SDate)){
        alert("统计起期 日期格式错误！");
        fm.BeginDate.value="";
        return false;
    }
    if(!IsDate(EData)){
        alert("统计止期 日期格式错误！");
        fm.EndDate.value="";
        return false;
    }
	var aDate = EData.split("-");
	var oDate1 = new Date(aDate[1] + '-' + '01' + '-' + aDate[0]); // 结束日期
	aDate = SDate.split("-");
	var oDate2 = new Date(aDate[1] + '-' + '01' + '-' + aDate[0]); // 开始日期
	if (oDate1 < oDate2) {
		alert("'租期止期'不能小于'租期起期'!");
		return false;
	} else {
		return true;
	}
}
// 判断年、月、日的取值范围是否正确
function IsMonthAndDateCorrect(nYear, nMonth, nDay) {
	// 月份是否在1-12的范围内，注意如果该字符串不是C#语言的，而是JavaScript的，月份范围为0-11
	if (nMonth > 12 || nMonth <= 0) {
		return false;
	}

	// 日是否在1-31的范围内，不是则取值不正确
	if (nDay > 31 || nDay <= 0) {
		return false;
	}

	// 根据月份判断每月最多日数
	var bTrue = false;
	if (nMonth == 12 || nMonth == 1 || nMonth == 3 || nMonth == 5
			|| nMonth == 7 || nMonth == 8 || nMonth == 10) {
		bTrue = true;
	}
	if (nMonth == 6 || nMonth == 9 || nMonth == 4 || nMonth == 11) {
		bTrue = (nDay <= 30);
	}
	if (bTrue) {
		return true;
	}
	// 2月的情况
	// 如果小于等于28天一定正确
	if (nDay <= 28) {
		return true;
	}
	// 闰年小于等于29天正确
	if (IsLeapYear(nYear))
		return (nDay <= 29);
	// 不是闰年，又不小于等于28，返回false
	return false;
}

// 是否为闰年，规则：四年一闰，百年不闰，四百年再闰
function IsLeapYear(nYear) {
	// 如果不是4的倍数，一定不是闰年
	if (nYear % 4 != 0)
		return false;
	// 是4的倍数，但不是100的倍数，一定是闰年
	if (nYear % 100 != 0)
		return true;

	// 是4和100的倍数，如果又是400的倍数才是闰年
	return (nYear % 400 == 0);
}
