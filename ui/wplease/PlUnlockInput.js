var turnPage = new turnPageClass();
function submitForm(){
		if (verifyInput()) {
				var r = confirm("请仔细核对您要解锁的信息！");
				if (r == true) {
					var tOperate = "ADD";
					var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
					var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ showStr;
					showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
					fm.submit();
				} else {
					return false;
				}

			}
}
function placeIn(){
	 var divInfos=document.getElementById("divInfos");
	    divInfos.style.display='none';
	    
	    var codeno=document.getElementById("codeno");
	    if(codeno.value==null || codeno.value==""){
	      alert("请先输入解锁职场编码！");
	      return false;
	    }		 
	    var strSQL = "select 1 from LIPlaceRentinfo where Placeno='"+codeno.value+"' and Islock='02'" ;
	    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	    //判断是否查询成功
	    if (!turnPage.strQueryResult) {
	  	  alert("解锁职场编码不存在或该职场正处于解锁状态，请重新输入！");
	  	  codeno.value="";
	      return false;
		}
		//获取职场信息
		var strSQL1 = "select Comname,(sigarea + grparea + bankarea + healtharea + servicearea +manageroffice + departpersno + planfinance + jointarea + publicarea +otherarea),"
	                 +"begindate,enddate,address "
	                 +"from LIPlaceRentInfo a where Placeno='"+ codeno.value+"'";			 
		turnPage.strQueryResult  = easyQueryVer3(strSQL1, 1, 1, 1); 
		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
		// 查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		
		//职场信息
		fm.SComName.value = turnPage.arrDataCacheSet[0][0];
		fm.SumArea.value = turnPage.arrDataCacheSet[0][1];
		fm.SStartDate.value = turnPage.arrDataCacheSet[0][2];
		fm.SEndDate.value = turnPage.arrDataCacheSet[0][3];
		fm.SAddress.value = turnPage.arrDataCacheSet[0][4];
	    var divInfos=document.getElementById("divInfos");
	    divInfos.style.display='';
}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	
	initForm();
}