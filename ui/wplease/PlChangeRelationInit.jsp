
<%
	//name :PlApprovalInit.jsp
	//function : 
	//Creator :
	//date :2013-12-09
	//* 更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String Comcode = tG.ManageCom;
	String CurrentDate = PubFun.getCurrentDate();
	String tCurrentYear = StrTool.getVisaYear(CurrentDate);
	String tCurrentMonth = StrTool.getVisaMonth(CurrentDate);
	String tCurrentDate = StrTool.getVisaDay(CurrentDate);
%>
<script language="JavaScript">
	function initForm() 
	{
		try 
		{
			initApproveGrid();
			appInit('<%=Comcode%>');
		} 
		catch (re) 
		{
			alert("PlChangeRelationInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	function initApproveGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "变动类型";
			iArray[1][1] = "40px";
			iArray[1][2] = 50;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "职场编码";
			iArray[2][1] = "105px";
			iArray[2][2] = 100;
			iArray[2][3] = 0;
			
			iArray[3] = new Array();
			iArray[3][0] = "职场地址";
			iArray[3][1] = "105px";
			iArray[3][2] = 100;
			iArray[3][3] = 0;
			
			iArray[4] = new Array();
			iArray[4][0] = "租赁期间";
			iArray[4][1] = "105px";
			iArray[4][2] = 100;
			iArray[4][3] = 0;

			iArray[5] = new Array();
			iArray[5][0] = "面积(平方米)"; //列名
			iArray[5][1] = "55px"; //列宽
			iArray[5][2] = 100; //列最大值
			iArray[5][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[6] = new Array();
			iArray[6][0] = "单位租金(元/平方米/天)"; //列名
			iArray[6][1] = "60px"; //列宽
			iArray[6][2] = 100; //列最大值
			iArray[6][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[7] = new Array();
			iArray[7][0] = "单位物业费(元/平方米/天)"; //列名
			iArray[7][1] = "60px"; //列宽
			iArray[7][2] = 100; //列最大值
			iArray[7][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[8] = new Array();
			iArray[8][0] = "装修总投入(元)"; //列名
			iArray[8][1] = "60px"; //列宽
			iArray[8][2] = 100; //列最大值
			iArray[8][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[9] = new Array();
			iArray[9][0] = "使用状态"; //列名
			iArray[9][1] = "40px"; //列宽
			iArray[9][2] = 50; //列最大值
			iArray[9][3] = 0; //是否允许输入,1表示允许，0表示不允许	

			ApproveGrid = new MulLineEnter("fm", "ApproveGrid");
			ApproveGrid.mulLineCount = 0;
			ApproveGrid.displayTitle = 1;
//			ApproveGrid.canSel = 1;
			ApproveGrid.locked = 1;
			ApproveGrid.hiddenPlus = 1;
			ApproveGrid.hiddenSubtraction = 1;
			ApproveGrid.loadMulLine(iArray);
			ApproveGrid.detailInfo = "单击显示详细信息"; 
			
		} catch (ex) {
			alert("ApproveGrid初始化时出错:" + ex);
		}
	}
</script>