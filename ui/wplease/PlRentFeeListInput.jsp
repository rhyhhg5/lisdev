<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：PlHisSearchInput.jsp
//程序功能：
//创建日期：2011-6-10
//创建人  ：huodonglei
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="PlRentFeeListInput.js"></SCRIPT> 
	<%@include file="PlRentFeeListInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
</head>

<body onload="initElementtype();initForm();">    
  <form action="" method=post name=fm target="fraSubmit" >
    <div  id= "divCessGetData" style= "display: ''" >
	    <table class= common border=0 width=100%>
	      	<tr  class= common>
	      	    <td class=title>
	      	        准租批文号
	      	    </td>
	      	    <td class=input>
	      	        <Input name=EndDate  class=common  verify="准租批文号|notnull" elementtype=nacessary> 
	      	    </td>
	      	    <td  class= title>
	      	         公司代码
	      	    </TD>
	            <td  class= input> 
	          	    <Input NAME=CompanyCode s CLASS=codeno ondblclick="return showCodeList('comcode',[this,CompanyCodeName],[0,1],null,'1 and length(trim(comcode))=#8#',1,1);" onkeyup="return showCodeListKey('comcode',[this,CompanyCodeName],[0,1],null,'1 and length(trim(comcode))=#8#',1,1);" verify="公司代码|code:comcode&notnull"  readonly><input class=codename name=CompanyCodeName readOnly  elementtype=nacessary>
	            </td> 
	            <td class= title>
	                职场类型
	            </td>
	            <td class=input>
	               <Input class=codeno readOnly NAME=PlaceType VALUE="" CodeData="0|^1|再用|^2|停用" ondblClick="showCodeListEx('',[this,PlaceTypeName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('',[this,PlaceTypeName],[0,1],null,null,null,1);"  verify="职场类型|NOTNULL"><input class=codename name=PlaceTypeName readOnly  elementtype=nacessary>
	            </td>
	            </tr>
	            <tr class=common>
	            <td  class= title>
	                  统计起期
	            </td>
	            <td  class= input> 
	                <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> 
	            </td> 
	            <td class= title>
   		 	         统计止期
   			    </td>
   			    <td class= input >
          	    <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> 
   			    </td>
   			    <td class= title>
   		 	         公司级别
   			    </td>
   			    <td class= input >
          	         <Input class=codeno  NAME=CompanyLevel   ondblClick="return showCodeList('companylevel',[this,CompanyLevelName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('companylevel',[this,CompanyLevelName],[0,1],null,null,null,1);"  verify="公司级别|NOTNULL" readOnly><input class=codename name=CompanyLevelName readOnly  elementtype=nacessary>
   			    </td>
	        </tr>      
	    </table>
	    <br>
	    <input class=cssButton type="button"  value="查询" onclick="">
	    <span style="color:red">根据不同的统计类型，显示不同的报表</span>
	    <br><br>
	    职场呈现表
	    <div id='divPlaceGrid' style="display:''">
		    <table class="common">		
					<tr class="common">
						<td text-align: left colSpan=1><span id="spanPlaceGrid"></span></td>
					</tr>
			</table>
	    </div>  
	    <br>
	    新设职场详单
	    <div id='divNewPlaceGrid' style="display:''">
		    <table class="common">		
					<tr class="common">
						<td text-align: left colSpan=1><span id="spanNewPlaceGrid"></span></td>
					</tr>
			</table>
	    </div> 
	    <br>
	    撤销职场详单
	    <div id='divRemovePlaceGrid' style="display:''">
		    <table class="common">		
					<tr class="common">
						<td text-align: left colSpan=1><span id="spanRemovePlaceGrid"></span></td>
					</tr>
			</table>
	    </div> 
	    <br>
	    变更职场详单（包括续租，换租）
	    <div id='divChangePlaceGrid' style="display:''">
		    <table class="common">		
					<tr class="common">
						<td text-align: left colSpan=1><span id="spanChangePlaceGrid"></span></td>
					</tr>
			</table>
	    </div> 
	    <br>
		<INPUT class=cssButton   VALUE="打印"  TYPE=button  onClick="" >
		</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
