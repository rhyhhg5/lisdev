
<jsp:directive.page
	import="com.sinosoft.lis.certify.SysOperatorNoticeBL" />
<%@page import="java.sql.Date"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：PlSolvencyPrintSave.jsp
	//程序功能：
	//创建日期：2012-09-21
	//创建人  ：鞠成富
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	System.out.println("start");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	//输出参数
	String FlagStr = "";
	String Content = "";
	
	boolean operFlag = true;
	
	String tBeginDate = request.getParameter("BeginDate");      //统计起期
	String tEndDate = request.getParameter("EndDate");        //统计止期
	String tManageCom = request.getParameter("ManageCom");        //页面选择机构
	String strOperation = request.getParameter("fmtransact");   //获取操作类型
	
	//
	Calendar cal=Calendar.getInstance();    
	String year=Integer.toString(cal.get(Calendar.YEAR)) ;;    

	System.out.println("统计起期：" + tBeginDate);
	System.out.println("统计止期：" + tEndDate);
	System.out.println(tG.Operator);
	System.out.println(tManageCom);

	VData tVData = new VData();
	VData mResult = new VData();
	
	tVData.addElement(tBeginDate);
	tVData.addElement(tEndDate);
	tVData.addElement(year);
	tVData.addElement(tG);
	tVData.addElement(tManageCom);

	XmlExport txmlExport = new XmlExport();

	PlSolvencyPrintUI tPlSolvencyPrintUI = new PlSolvencyPrintUI();
	if (!tPlSolvencyPrintUI.submitData(tVData, strOperation)) {
		FlagStr = "False";
		Content=tPlSolvencyPrintUI.mErrors.getFirstError();
	}
	mResult = tPlSolvencyPrintUI.getResult();

	txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);
	if (txmlExport == null) {
		System.out.println("null");
		return;
	}
	
	ExeSQL tExeSQL = new ExeSQL();

	//获取临时文件名
	String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
	String strFilePath = tExeSQL.getOneValue(strSql);
	String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName() + ".vts";
	//获取存放临时文件的路径
	String strRealPath = application.getRealPath("/").replace('\\', '/');
	String strVFPathName = strRealPath + "/" + strVFFileName;

	CombineVts tcombineVts = null;

	if (operFlag == true) {
		//合并VTS文件
		String strTemplatePath = application
		.getRealPath("f1print/picctemplate/")
		+ "/";
		tcombineVts = new CombineVts(txmlExport.getInputStream(),
		strTemplatePath);

		ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
		tcombineVts.output(dataStream);

		//把dataStream存储到磁盘文件
		System.out.println("存储文件到" + strVFPathName);
		AccessVtsFile.saveToFile(dataStream, strVFPathName);
		System.out.println("==> Write VTS file to disk ");

		System.out.println("===strVFFileName : " + strVFFileName);
		//本来打算采用get方式来传递文件路径
		//缺点是文件路径被暴露
		System.out.println("---passing params by get method");
		response
		.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="
				+ strVFPathName);
	}
%>
