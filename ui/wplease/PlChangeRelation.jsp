<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%
	//name :PlManageState.jsp
	//function :
	//Creator :
	//date :2012-12-9
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<%@include file="./PlChangeRelationInit.jsp"%>
<SCRIPT src="./PlChangeRelation.js"></SCRIPT>

</head>
<body onload="initElementtype();initForm()">
	<form action="./PlChangeRelationSave.jsp" method=post name=fm target="fraSubmit">
		<table class=common>
			<tr class=common>
				<td class=title>查询职场编码</td>
				<td><Input class=common name="placeno" id="placeno"
					verify="职场编码|NOTNULL" elementtype=nacessary></td>
			</tr>
			<tr class=common >
				<td colspan="2"><INPUT VALUE="确认" class=cssButton TYPE=button onclick="placeIn()"></td>
			</tr>
			
		</table>
		<hr>
		<span style="color:green;font-weight: bold;">【注】计量单位->：(1)单位租金、单位物业费：元/平方米/天；(2)装修总投入：元；(3)面积：平方米。</span>
		<hr>
		<div id="divPlaceList" align="left" style="display: ''" >
			<table class=common>
				<tr class=common>
					<td>
						<span id="spanApproveGrid"></span>
					</td>
			</tr>
			</table>
		</div>

		<Div id= "divPage" align=center style= "display: 'none' ">
			<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
		
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>