//程序名称：EasyScanErrorList.js
//创建日期：2008-6-4
//创建人  ：ZhangJianbao
//更新记录：更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

var tDefaultStartDate;

function afterSubmit(flag, content)
{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	queryContInfo();
}

//查询外包批次返回成功，但保单状态为发送成功的保单
function queryContInfo()
{
    if(!verifyInput2())
    {
        return false;
    }
    
	var sql = "select a.BatchNo, b.BussNo, b.MissionID, b.ManageCom, CodeName('bpobatchstate', a.State), CodeName('bpomissionstate', b.State) "
	    + "from BPOBatchInfo a, BPOMissionState b "
	    + "where a.BatchNo = b.BatchNo and a.State not in ('01', '02') and b.State = '01' "
	    + "   and ManageCom like '" + tManageCom + "%' "
	    + "   and ManageCom like '" + fm.ManageCom.value + "%' "
	    + "   and not exists(select 1 from LCCont where PrtNo = b.BussNo and ContType = '1')"
	    + "   and not exists(select 1 from LBCont where PrtNo = b.BussNo and ContType = '1')"
	    + getWherePart("b.MakeDate", "StartDate", ">=")
	    + getWherePart("b.MakeDate", "EndDate", "<=")
	    + getWherePart("b.BussNo", "PrtNo")
	    + getWherePart("a.BatchNo", "BPOBatchNo")
	    + "order by b.MakeDate, b.MakeTime ";
	
	turnPage.strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
    turnPage.queryModal(sql,ContGrid);
    fm.BatchNo.value = "";
    fm.BussNo.value = "";
	
    return true;
}

//初始化查询条件
function initInputBox()
{
    fm.ManageCom.value = tManageCom;
    fm.EndDate.value = tCurrentDate;
    
    var sql = "select date('" + tCurrentDate + "') - 3 month from dual ";
    tDefaultStartDate = easyExecSql(sql);
    fm.StartDate.value = tDefaultStartDate;
}

//进入修改页面
function modifyState()
{
    var sel = ContGrid.getSelNo();
    if(sel == 0 || sel == null)
    {
        alert( "请先选择一条记录。" );
        return false;
    }
    fm.BatchNo.value = ContGrid.getRowColDataByName(sel - 1, "BatchNo");
    fm.BussNo.value = ContGrid.getRowColDataByName(sel - 1, "BussNo");
    if(fm.BatchNo.value == null || fm.BatchNo.value == "")
    {
        alert("批次信息为空！");
        return false;
    }
    if(fm.BussNo.value == null || fm.BussNo.value == "")
    {
        alert("印刷号数据为空！");
        return false;
    }
    //alert(fm.BatchNo.value + "****" + fm.BussNo.value);
    fm.action = "./EasyScanErrorListSave.jsp";
    fm.submit();
}