<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.brieftb.ContInputAgentcomChkBL"%>
<%
//输出参数
CErrors tError = null;
String tRela  = "";                
String FlagStr="";
String Content = "";
String tAction = "";
String tOperate = "";
String wFlag = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
    VData tVData = new VData();
    //工作流操作型别，根据此值检索活动ID，取出服务类执行具体业务逻辑
    wFlag = request.getParameter("WorkFlowFlag");
    TransferData mTransferData = new TransferData();
    mTransferData.setNameAndValue("ProposalGrpContNo", request.getParameter("ProposalGrpContNo"));
    mTransferData.setNameAndValue("PrtNo", request.getParameter("PrtNo"));
    mTransferData.setNameAndValue("SaleChnl", request.getParameter("SaleChnl"));
    mTransferData.setNameAndValue("AgentCode",request.getParameter("AgentCode"));    
    mTransferData.setNameAndValue("AgentGroup",request.getParameter("AgentGroup"));
    mTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
    mTransferData.setNameAndValue("GrpName", request.getParameter("GrpName"));
    mTransferData.setNameAndValue("CValiDate", request.getParameter("CValiDate"));
    mTransferData.setNameAndValue("Operator",tG.Operator);
    mTransferData.setNameAndValue("MakeDate",PubFun.getCurrentDate());
    mTransferData.setNameAndValue("MissionID",request.getParameter("MissionID"));  
    mTransferData.setNameAndValue("SubMissionID",request.getParameter("SubMissionID"));
    mTransferData.setNameAndValue("MissionProp20",request.getParameter("MissionProp20"));
    
    System.out.println(request.getParameter("PrtNo"));
    tVData.add(mTransferData);
    tVData.add(tG);
    System.out.println("wFlag="+wFlag);
    System.out.println("-------------------start workflow---------------------");
    GrpTbWorkFlowUI tGrpTbWorkFlowUI = new GrpTbWorkFlowUI();
  	//江苏团险校验
    boolean tJSFlag = false;
    {
  	  String tSaleChnl = request.getParameter("SaleChnl");
  	  String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
  			  	  + "and code = '"+tSaleChnl+"'";
  	  SSRS tSSRS = new ExeSQL().execSQL(tSql);
  	  String tSubManageCom = request.getParameter("ManageCom").substring(0,4);
  	  if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(tSubManageCom)){
  		  ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
      	  TransferData tJSTransferData = new TransferData();
      	  tJSTransferData.setNameAndValue("ContNo",request.getParameter("GrpContNo"));
      	  VData tJSVData = new VData();
      	  tJSVData.add(tJSTransferData);
      	  if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
      		  Content = tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
      		  FlagStr = "Fail";
      		  tJSFlag = true;
      	  }
  	  }
    }
    if(!tJSFlag){
	  	if( !tGrpTbWorkFlowUI.submitData( tVData, wFlag ) ) {
	      	Content = " 处理失败，原因是: " + tGrpTbWorkFlowUI.mErrors.getError(0).errorMessage;
	      	FlagStr = "Fail";
	  	}else {
	      	Content = " 处理成功！";
	      	FlagStr = "Succ";
	  	}
    }
 	System.out.println("-------------------end workflow---------------------");
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>