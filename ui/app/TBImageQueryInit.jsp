<%
//程序名称：ProposalQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	String ManageCom = request.getParameter("ManageCom");
	String PrtNo=request.getParameter("PrtNo");
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('PrtNo').value = "<%=PrtNo%>";
	fm.all('ManageCom').value= "<%=ManageCom%>";
  }
  catch(ex)
  {
    alert("在TBImageQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}


function initForm()
{
  try
  {
    initInpBox();
    initEsDocPagesGrid();
    initQuery();
  }
  catch(re)
  {
    alert("TBImageQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initEsDocPagesGrid()
{                               
    var iArray = new Array();
      try
      {
		iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列宽
        iArray[0][2]=100;            //列最大值
        iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="单证号码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=50;         //最大长度
        iArray[1][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="单证类型编码";         //列名
        iArray[2][1]="60px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="单证类型";         //列名
        iArray[3][1]="90px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="管理机构";         //列名
        iArray[4][1]="60px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="操作员";         //列名
        iArray[5][1]="40px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
    
        iArray[6]=new Array();
        iArray[6][0]="扫描日期";         //列名
        iArray[6][1]="60px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

		iArray[7]=new Array();
        iArray[7][0]="扫描时间";         //列名
        iArray[7][1]="60px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="修改日期";         //列名
        iArray[8][1]="60px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=3;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="修改时间";         //列名
        iArray[9][1]="60px";         //列宽
        iArray[9][2]=100;            //列最大值
        iArray[9][3]=3;              //是否允许输入,1表示允许，0表示不允许
        
        iArray[10]=new Array();
        iArray[10][0]="docid";         //列名
        iArray[10][1]="0px";         //列宽
        iArray[10][2]=100;            //列最大值
        iArray[10][3]=3;              //是否允许输入,1表示允许，0表示不允许
     
                       
        EsDocPagesGrid = new MulLineEnter( "fm" , "EsDocPagesGrid" ); 

        //这些属性必须在loadMulLine前
        EsDocPagesGrid.mulLineCount = 0;   
        EsDocPagesGrid.displayTitle = 1;
        EsDocPagesGrid.locked = 1;
        EsDocPagesGrid.canSel = 1;
        EsDocPagesGrid.hiddenPlus = 1;
        EsDocPagesGrid.hiddenSubtraction = 1;
        EsDocPagesGrid.loadMulLine(iArray);
        EsDocPagesGrid.selBoxEventFuncName = "ChangePic";
      }
      catch(ex)
      {
        alert("初始化EsDocPagesGrid时出错："+ ex);
      }
}
</script>