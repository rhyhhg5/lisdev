<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  String printChannelType = request.getParameter("printChannelType");
%>
<script>
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var printChannelType = "<%=printChannelType%>";//外包打印和分公司打印的标记 0：外包打印；1分公司打印
</script>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="YBKPrint.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="YBKPrintInit.jsp"%>
  <title>打印个人保单 </title>
</head>
<body onload="initForm();" >
	<form action="./YBKPrintSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 个人保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr><td class= titleImg align= center>请输入保单查询条件：</td></tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>保险合同号</TD>
				<TD class= input> <Input class= common name=ContNo verify="保险合同号|int"> </TD>
				<TD class= title>印刷号码</TD>
				<TD class= input> <Input class= common name=PrtNo verify="印刷号码"> </TD>
			</TR>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<INPUT VALUE="打印保单" class="cssButton" TYPE=button onclick="printPol();" name='printButton' >
		<INPUT VALUE="发送保单" class="cssButton" TYPE=button onclick="sendPol();" name='sendButton' >
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<Div id= "divLCPol1" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanContGrid" ></span>
					</td>
				</tr>
			</table>
		</div>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>



