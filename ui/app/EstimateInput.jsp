<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
		String tGrpContNo = request.getParameter("GrpContNo");
		String tLookFlag = request.getParameter("LookFlag");
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="EstimateInput.js"></script>
		<%@include file="EstimateInputInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>";
  		var tGrpContNo = "<%=tGrpContNo%>";
  		var tLookFlag = "<%=tLookFlag%>";
  		</script>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./EstimateInputSave.jsp" method=post name=fm target="fraSubmit">
		<br />
			<table>
				<tr>
					<td>
								
					</td>
					<td class=titleImg>
						保单信息
					</td>
				</tr>
			</table>
            <Div id="divGrpContInfo" style="display:''" align=center>
			<table class=common align=''>
				<TR class=common>
					<TD class=title>
						保单号
					</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=GrpContNo>
					</TD>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=ProjectName>
					</TD>
					<TD class=title>
						项目编码
					</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=ProjectNo>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						投保机构名称
					</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=GrpName>
					</TD>
					<TD class=title>
						生效日期
					</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=CvaliDate>
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
				</TR>
			</table>
			</Div>
			<br />
			<table>
				<tr>
					<td>
					</td>
					<td class=titleImg>
						已录入的预估信息
					</td>
				</tr>
			</table>
			<div id="divEstimate" style="display:''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanEstimateGrid"></span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">
				<table>
					<tr>
						<td class=button>
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</Div>
			<hr>
			<div id="divEstimateInfo" style="display:''">
			<table>
				<tr>
					<td>
					</td>
					<td class=titleImg>
						预估信息录入
					</td>
				</tr>
			</table>
			<Div id="divEstimateInfo1" style="display:''" align=center>
			<table class=common align=''>
				<TR class=common>
					<TD class=title>
						预估赔付率
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=EstimateRate elementtype=nacessary>
					</TD>
					<TD class=title>
						预估赔付率（含结余返还/保费回补）
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=EstimateBalanceRate>
					</TD>
					<TD class=title>
						预估结余返还金额
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=ContEstimatePrem><font color = red  size = 2>（单位：元）</font>
					</TD>
				</TR>
				<tr>
					<td class=title>
						预计保费回补金额
					</td>
					<td class = input>
						<input type="text" class="common" name=BackEstimatePrem elementtype=nacessary>
					</td>
					<td class=title>
						录入工号
					</td>
					<td class = input>
						<input type="text" class="readonly" name=Operator readonly="readonly" onmouseover="this.title=this.value">
					</td>
				</tr>
			</table>
			<table class=common align='center'>
				<TR class=common>
					<td class=title>
						录入原因
					</td>
					<TD class=input colspan="5">
						<textarea class="common" name=Reason cols="100%" rows="3" elementtype=nacessary></textarea>
					</TD>

				</TR>
				<TR class=common>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
				</TR>
			</table>
			</Div>
			</div>
			<br />
			<table>
				<input type=hidden id="fmtransact" name="fmtransact">
				<td>
					<input type="button" class=cssButton value=" 新 增 " name=insert onclick="submitForm()">
					<input type="button" class=cssButton value=" 修 改 " name=update onclick="updateForm()"> 
					<input type="button" class=cssButton value=" 删 除 " name=delete onclick="deleteForm()">
					<input type="button" class=cssButton value=" 退 出 " name=tuichu onclick="cancelForm()"> 
				</td>
			</table>
			<div id="divShowInfo" style="display:''">
			<table>
				<br/>
				<tr class=common>
					<td>
					<font color = red  size = 2>
						录入说明：<br/>
						1、预估赔付率：必录项，小数形式保存，小数点后四位，如99.99%录入为0.9999。<br>
						2、预估赔付率（含结余返还）：小数形式保存，小数点后四位，如99.99%录入为0.9999。<br/>
						3、预估结余返还金额：整数，单位元。<br/>
						4、录入原因：必录项，最多1000字。<br/>
						5、预计保费回补金额：整数，单位：元。<br>
						6、预计保费回补金额与预估结余返还金额只录其一。<br/>
					</td>
				</tr>
			</table>
			</div>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>

		</form>
	</body>
</html>
