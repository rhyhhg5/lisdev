var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;
  var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  if(dateDiff(tStartDate,tEndDate,"M")>3)
  {
	alert("统计期最多为三个月！");
	return false;
  }
  	var tMngCom = fm.ManageCom.value;
	var tStrSQL = "";
	if(dateDiff(tStartDate,tEndDate,"M")<1){
      alert("统计止期比统计统计起期早");
      return false;
    }
     var tMngCom = fm.ManageCom.value;
     var tRiskcode = fm.RiskCode.value;
     var length=tMngCom.split("").length;
     var managecom="";
     if(length==8){
         managecom="length('"+tMngCom+"')";
     }else{
        managecom="length('"+tMngCom+"')*2";
     }	
    
//  var i = 0;
//  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//  //showSubmitFrame(mDebug);
//  //fm.fmtransact.value = "PRINT";
//  fm.target = "f1print";
//  fm.all('op').value = '';
//  fm.submit();
//  showInfo.close();

    var tSQL =" Select a,                                                                                        "
             +"        q,                                                                                        "             
             +"        b,                                                                                        "             
             +"        To_Zero(C1) + To_Zero(C2) + To_Zero(C3),                                                  "             
             +"        To_Zero(D1) + To_Zero(D2) + To_Zero(D3),                                                  "             
             +"        Value(e, 0),                                                                              "             
             +"        Value(f, 0),                                                                              "             
             +"        Value(g, 0),                                                                              "             
             +"        Value(h, 0),                                                                              "             
             +"        Value(m, 0),                                                                              "             
             +"        Value(n, 0),                                                                              "             
             +"        To_Zero(O1) + To_Zero(O2) + To_Zero(O3),                                                  "             
             +"        To_Zero(P1) + To_Zero(P2) + To_Zero(P3)                                                   "             
             +" From (Select z.Comcode a,                                                                        "             
             +"              z.Showname q,                                                                       "             
             +"              (Select Riskshortname                                                               "             
             +"               From Lmrisk                                                                        "             
             +"               Where Riskcode = '"+ tRiskcode +"') b,                                             "             
             +"              (Select Count(Distinct Insuredno)                                                   "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode)                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +"         ) C1,                                            "               
             +"              (Select Count(Distinct Insuredno)                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode)                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) C2, "                                                                  
             +"              (Select Count(Distinct Insuredno)                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lbcont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode)                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) C3,   "                                                                
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode)                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) D1,    "                                                               
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode)                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) D2,        "                                                           
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lbcont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode)                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) D3,     "                                                              
             +"              (Select Count(a.Contno)                                                             "             
             +"               From Lcuwmaster a,                                                                 "             
             +"                    Lccont b,                                                                     "             
             +"                    Lcpol c                                                                       "             
             +"               Where c.Renewcount <= 0                                                            "             
             +"               And a.Contno = b.Contno                                                            "             
             +"               And a.Polno = c.Polno                                                              "             
             +"               And b.Conttype = '1'                                                               "             
             +"               And a.Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'                     "             
             +"               And c.Riskcode = '"+ tRiskcode +"'                                                 "             
             +"               And a.Passflag = 'a'                                                               "             
             +"               And Substr(b.Managecom, 1, "+managecom+") = z.Comcode                              "             
             + getWherePart("b.Salechnl","saleChnl")                                                                           
             + getWherePart("b.Agentcom","agentCom") +" ) e,    "                                                              
             +"              (Select Sum(c.Prem)                                                                 "             
             +"               From Lcuwmaster a,                                                                 "             
             +"                    Lccont b,                                                                     "             
             +"                    Lcpol c                                                                       "             
             +"               Where c.Prtno = b.Prtno                                                            "             
             +"               And a.Contno = b.Contno                                                            "             
             +"               And a.Polno = c.Polno                                                              "             
             +"               And b.Conttype = '1'                                                               "             
             +"               And a.Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'                     "             
             +"               And c.Riskcode = '"+ tRiskcode +"'                                                 "             
             +"               And a.Passflag = 'a'                                                               "             
             +"               And Substr(b.Managecom, 1, "+managecom+") = z.Comcode                              "             
             + getWherePart("b.Salechnl","saleChnl")                                                                           
             + getWherePart("b.Agentcom","agentCom") +" ) f,     "                                                             
             +"              (Select Count(a.Contno)                                                             "             
             +"               From Lcuwmaster a,                                                                 "             
             +"                    Lccont b,                                                                     "             
             +"                    Lcpol c                                                                       "             
             +"               Where c.Renewcount <= 0                                                            "             
             +"               And a.Contno = b.Contno                                                            "             
             +"               And a.Polno = c.Polno                                                              "             
             +"               And b.Conttype = '1'                                                               "             
             +"               And a.Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'                     "             
             +"               And c.Riskcode = '"+ tRiskcode +"'                                                 "             
             +"               And a.Passflag = '1'                                                               "             
             +"               And Substr(b.Managecom, 1, "+managecom+") = z.Comcode                              "             
             + getWherePart("b.Salechnl","saleChnl")                                                                           
             + getWherePart("b.Agentcom","agentCom") +" ) g,       "                                                           
             +"              (Select Sum(c.Prem)                                                                 "             
             +"               From Lcuwmaster a,                                                                 "             
             +"                    Lccont b,                                                                     "             
             +"                    Lcpol c                                                                       "             
             +"               Where c.Prtno = b.Prtno                                                            "             
             +"               And a.Contno = b.Contno                                                            "             
             +"               And a.Polno = c.Polno                                                              "             
             +"               And b.Conttype = '1'                                                               "             
             +"               And a.Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'                     "             
             +"               And c.Riskcode = '"+ tRiskcode +"'                                                 "             
             +"               And a.Passflag = '1'                                                               "             
             +"               And Substr(b.Managecom, 1, "+managecom+") = z.Comcode                              "             
             + getWherePart("b.Salechnl","saleChnl")                                                                           
             + getWherePart("b.Agentcom","agentCom") +" ) h,      "                                                            
             +"              (Select Count(Distinct Insuredno)                                                   "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Uwdate Between '"+ tStartDate +"' and '"+ tEndDate +"'          "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Uwflag In ('4', '9')                                              "             
             +"                            And Appflag <> '1'                                                    "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode)                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) m,         "                                                           
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Uwdate Between '"+ tStartDate +"' and '"+ tEndDate +"'          "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Uwflag In ('4', '9')                                              "             
             +"                            And Appflag <> '1'                                                    "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"') n,                                               "             
             +"              (Select Count(Distinct Insuredno)                                                   "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+") "                                                                        
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"') O1,                                              "             
             +"              (Select Count(Distinct Insuredno)                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"') O2,                                              "             
             +"              (Select Count(Distinct Insuredno)                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lbcont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"') O3,                                              "             
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Riskcode = '"+ tRiskcode +"') P1,                                              "             
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lbcont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Riskcode = '"+ tRiskcode +"') P2,                                              "             
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, "+managecom+") = z.Comcode                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Riskcode = '"+ tRiskcode +"') P3                                               "             
             +"       From Ldcom z                                                                               "             
             +"       Where Length(Trim(z.Comcode)) = "+managecom+"                                              "             
             +"       And Sign = '1'                                                                             "             
             +"       And Substr(z.Comcode, 1, Length('"+tMngCom+"')) = '"+tMngCom+"') As x                      "             
             +" Union                                                                                            "             
             +" Select a,                                                                                        "             
             +"        q,                                                                                        "             
             +"        b,                                                                                        "             
             +"        To_Zero(C1) + To_Zero(C2) + To_Zero(C3),                                                  "             
             +"        To_Zero(D1) + To_Zero(D2) + To_Zero(D3),                                                  "             
             +"        Value(e, 0),                                                                              "             
             +"        Value(f, 0),                                                                              "             
             +"        Value(g, 0),                                                                              "             
             +"        Value(h, 0),                                                                              "             
             +"        Value(m, 0),                                                                              "             
             +"        Value(n, 0),                                                                              "             
             +"        To_Zero(O1) + To_Zero(O2) + To_Zero(O3),                                                  "             
             +"        To_Zero(P1) + To_Zero(P2) + To_Zero(P3)                                                   "             
             +" From (Select '总计' a,                                                                           "             
             +"              '总计' q,                                                                           "             
             +"              (Select Riskshortname                                                               "             
             +"               From Lmrisk                                                                        "             
             +"               Where Riskcode = '"+ tRiskcode +"') b,                                             "             
             +"              (Select Count(Distinct(Insuredno || Riskcode))                                      "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode)          "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) C1, "                                                                  
             +"              (Select Count(Distinct(Insuredno || Riskcode))                                      "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode)          "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) C2,  "                                                                 
             +"              (Select Count(Distinct(Insuredno || Riskcode))                                      "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lbcont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode)          "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) C3,    "                                                               
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode)          "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) D1,   "                                                                
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode)          "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) D2,   "                                                                
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lbcont                                                           "             
             +"                            Where Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode)          "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) D3,   "                                                                
             +"              (Select Count(a.Contno)                                                             "             
             +"               From Lcuwmaster a,                                                                 "             
             +"                    Lccont b,                                                                     "             
             +"                    Lcpol c                                                                       "             
             +"               Where c.Renewcount <= 0                                                            "             
             +"               And a.Contno = b.Contno                                                            "             
             +"               And a.Polno = c.Polno                                                              "             
             +"               And b.Conttype = '1'                                                               "             
             +"               And a.Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'                     "             
             +"               And c.Riskcode = '"+ tRiskcode +"'                                                 "             
             +"               And a.Passflag = 'a'                                                               "             
             +"               And Substr(b.Managecom, 1, Length('"+tMngCom+"')) = z.Comcode) e,                  "             
             +"              (Select Sum(c.Prem)                                                                 "             
             +"               From Lcuwmaster a,                                                                 "             
             +"                    Lccont b,                                                                     "             
             +"                    Lcpol c                                                                       "             
             +"               Where c.Prtno = b.Prtno                                                            "             
             +"               And a.Contno = b.Contno                                                            "             
             +"               And a.Polno = c.Polno                                                              "             
             +"               And b.Conttype = '1'                                                               "             
             +"               And a.Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'                     "             
             +"               And c.Riskcode = '"+ tRiskcode +"'                                                 "             
             +"               And a.Passflag = 'a'                                                               "             
             +"               And Substr(b.Managecom, 1, Length('"+tMngCom+"')) = z.Comcode) f,                  "             
             +"              (Select Count(a.Contno)                                                             "             
             +"               From Lcuwmaster a,                                                                 "             
             +"                    Lccont b,                                                                     "             
             +"                    Lcpol c                                                                       "             
             +"               Where c.Renewcount <= 0                                                            "             
             +"               And a.Contno = b.Contno                                                            "             
             +"               And a.Polno = c.Polno                                                              "             
             +"               And b.Conttype = '1'                                                               "             
             +"               And a.Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'                     "             
             +"               And c.Riskcode = '"+ tRiskcode +"'                                                 "             
             +"               And a.Passflag = '1'                                                               "             
             +"               And Substr(b.Managecom, 1, Length('"+tMngCom+"')) = z.Comcode) g,                  "             
             +"              (Select Sum(c.Prem)                                                                 "             
             +"               From Lcuwmaster a,                                                                 "             
             +"                    Lccont b,                                                                     "             
             +"                    Lcpol c                                                                       "             
             +"               Where c.Prtno = b.Prtno                                                            "             
             +"               And a.Contno = b.Contno                                                            "             
             +"               And a.Polno = c.Polno                                                              "             
             +"               And b.Conttype = '1'                                                               "             
             +"               And a.Makedate Between '"+ tStartDate +"' and '"+ tEndDate +"'                     "             
             +"               And c.Riskcode = '"+ tRiskcode +"'                                                 "             
             +"               And a.Passflag = '1'                                                               "             
             +"               And Substr(b.Managecom, 1, Length('"+tMngCom+"')) = z.Comcode) h,                  "             
             +"              (Select Count(Distinct(Insuredno || Riskcode))                                      "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Uwdate Between '"+ tStartDate +"' and '"+ tEndDate +"'          "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Uwflag In ('4', '9')                                              "             
             +"                            And Appflag <> '1'                                                    "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode)          "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"'                                                   "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom") +" ) m,  "                                                                  
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lcpol                                                                         "             
             +"               Where Prtno In (Select Prtno                                                       "             
             +"                               From Lccont                                                        "             
             +"                               Where Uwdate Between '"+ tStartDate +"' and '"+ tEndDate +"'       "             
             +"                               And Conttype = '1'                                                 "             
             +"                               And Uwflag In ('4', '9')                                           "             
             +"                               And Appflag <> '1'                                                 "             
             +"                               And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode        "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")                                            "                             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"') n,                                               "             
             +"              (Select Count(Distinct(Insuredno || Riskcode))                                      "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode           "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"') O1,                                              "             
             +"              (Select Count(Distinct(Insuredno || Riskcode))                                      "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode           "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"') O2,                                              "             
             +"              (Select Count(Distinct(Insuredno || Riskcode))                                      "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lbcont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode           "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Riskcode = '"+ tRiskcode +"') O3,                                              "             
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lcpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode           "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Riskcode = '"+ tRiskcode +"') P1,                                              "             
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lbcont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode           "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Riskcode = '"+ tRiskcode +"') P2,                                              "             
             +"              (Select Sum(Prem)                                                                   "             
             +"               From Lbpol                                                                         "             
             +"               Where Renewcount <= 0                                                              "             
             +"               And Appflag = '1'                                                                  "             
             +"               And Conttype = '1'                                                                 "             
             +"               And Prtno In (Select Prtno                                                         "             
             +"                            From Lccont                                                           "             
             +"                            Where Signdate Between '"+ tStartDate +"' and '"+ tEndDate +"'        "             
             +"                            And Conttype = '1'                                                    "             
             +"                            And Appflag = '1'                                                     "             
             +"                            And Substr(Managecom, 1, Length('"+tMngCom+"')) = z.Comcode           "             
             + getWherePart("Salechnl","saleChnl")                                                                             
             + getWherePart("Agentcom","agentCom")+")"                                                                         
             +"               And Riskcode = '"+ tRiskcode +"') P3                                               "             
             +"       From Ldcom z                                                                               "             
             +"       Where Length(Trim(z.Comcode)) = Length('"+tMngCom+"')                                      "             
             +"       And Sign = '1'                                                                             "             
             +"       And Substr(z.Comcode, 1, Length('"+tMngCom+"')) = '"+tMngCom+"') As x                      "             
             +" Order By a ";  
             
            fm.querySql.value = tSQL;
            fm.action="ParticularStatisticSubDown.jsp";
            fm.submit();
             
                                                                                                     
   


}
function getRisk(Obj)
{
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('I','A','C','D')"
           + " order by RiskCode";
  ;
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "")
  {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++)
    {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
  }
  Obj.CodeData=tCodeData;
}


//下拉框选择后执行
function afterCodeSelect(cName, Filed)
{	
  if(cName=='lcsalechnl')
  {
  	if(fm.saleChnl.value == '04')
  	{
  		fm.agentCom.value = '';
    	fm.all("tdAgentComName").style.display = '';
    	fm.all("tdAgentCom").style.display = '';
  	}
  	else
  	{
  		fm.agentCom.value = '';
    	fm.all("tdAgentComName").style.display = 'none';
    	fm.all("tdAgentCom").style.display = 'none';
  	}
	}
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    var tTmpUrl = "../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01";
    showInfo=window.open(tTmpUrl);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.agentCom.value = arrQueryResult[0][0];
    }
}