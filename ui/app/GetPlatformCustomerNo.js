//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function easyQueryClick()
{
	
	if (fm.CustomerNo.value == "" && fm.CustomerNo.value == null) {
		alert("请输入客户号");
		return false;
	}
	// 初始化表格
	initPolGrid();
	// 书写SQL语句
	var strSql = "select customerno,name,idno,PlatformCustomerNo  "
				+ " from ldperson  "
				+ " where 1 = 1 "
				+ getWherePart( 'customerno','CustomerNo' )	
				+ " with ur";	
	turnPage.pageLineNum = 50;
	turnPage.queryModal(strSql,PolGrid,0,0,1);
	fm.querySql.value = strSql;
}

var mSwitch = parent.VD.gVSwitch;


//获取
function GetPlatformCustomerNo() {
  var i = 0;
  var checkFlag = 0;
  
  if (fm.all('CustomerNo').value != '' && fm.all('CustomerNo').value != null) {
  	var cCustomerNo = fm.CustomerNo.value;
    fm.action = "./GetPlatformCustomerNoSave.jsp?CustomerNo=" + cCustomerNo;   
    fm.submit();
  }
  else {
    alert("请输客户号，并先进行查询！"); 
  }
}

function afterSubmit(FlagStr,Content)
{
	if (FlagStr == "Succ" )
	{
		alert(Content);
		easyQueryClick();
	}
}