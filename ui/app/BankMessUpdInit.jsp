<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ScanContInit.jsp
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
PubFun tpubFun = new PubFun();
String strCurDay = tpubFun.getCurrentDate();
    GlobalInput globalInput = (GlobalInput)session.getValue("GI");
    String strManageCom = globalInput.ComCode;
%>
<SCRIPT>
    var strCurDay= "<%=strCurDay%>";
    var type = "<%=tFlag%>";
    </SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

 
}
                            

function initForm()
{
  try
  {
    
    initInpBox();
   
    initGrpGrid();
    divPage.style.display="";
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initGrpGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";                    //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";                  //列宽
      iArray[0][2]=10;                      //列最大值
      iArray[0][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";               //列名
      iArray[1][1]="120px";                 //列宽
      iArray[1][2]=170;                     //列最大值
      iArray[1][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="保单号";              //列名
      iArray[2][1]="120px";                 //列宽
      iArray[2][2]=100;                     //列最大值
      iArray[2][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="缴费方式";              //列名
      iArray[3][1]="80px";                  //列宽
      iArray[3][2]=200;                     //列最大值
      iArray[3][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="银行编码";               //列名
      iArray[4][1]="100px";                 //列宽
      iArray[4][2]=100;                     //列最大值
      iArray[4][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="户名";                //列名
      iArray[5][1]="100px";                   //列宽
      iArray[5][2]=200;                     //列最大值
      iArray[5][3]=0;                       //是否允许输入,1表示允许，0表示不允许 
      
      iArray[6]=new Array();
      iArray[6][0]="账号";                //列名
      iArray[6][1]="100px";                   //列宽
      iArray[6][2]=200;                     //列最大值
      iArray[6][3]=0; 
      
       iArray[7]=new Array();
      iArray[7][0]="保单类型";                //列名
      iArray[7][1]="50px";                   //列宽
      iArray[7][2]=200;                     //列最大值
      iArray[7][3]=0;                       //是否允许输入,1表示允许，0表示不允许 
      

      GrpGrid = new MulLineEnter( "fm" , "GrpGrid" ); 
      //这些属性必须在loadMulLine前
      GrpGrid.mulLineCount = 0;   
      GrpGrid.displayTitle = 1;
      GrpGrid.locked = 1;
      GrpGrid.canSel = 1;
      GrpGrid.canChk = 0;
      GrpGrid.hiddenSubtraction = 1;
      GrpGrid.hiddenPlus = 1;
      GrpGrid.selBoxEventFuncName="selonclick";
      GrpGrid.loadMulLine(iArray);  
      
      
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex.message);        
      }
}


</script>