<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = "";
	try
	{
		tContNo = request.getParameter( "ContNo" );
		//默认情况下为团体投保单
		if( tContNo == null || tContNo.equals( "" ))
			tContNo = "";
	}
	
	catch( Exception e1 )
	{
		tContNo = "";
			System.out.println("---contno:"+tContNo);

	}
	String tGrpPolNo = "";
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("---contno:"+tContNo);
%>
<script>
	
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="GrpApproveModify.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpApproveModifyInit.jsp"%>
  <title>团体投保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    
    <table  class= common align=center>
    </table>
          <!--INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"-->           
          <!--INPUT VALUE="问题件查询" class= common TYPE=button onclick="QuestQuery();"-->
    	  <!--INPUT VALUE="修改完成确认" class= common TYPE=button onclick="ModifyMakeSure();"-->					
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询团体主险投保单条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <!--<TD  class= title>
            团体保单号码
          </TD>
          <TD  class= input>-->
            <Input class= common name=GrpProposalNo type="hidden" >
      
          <TD  class= title>
            印刷号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>
          <TD  class= title>
            销售渠道
          </TD>
          <TD  class= input>
            <Input class=codeNo name=SaleChnl ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName readonly=true >
          </TD>
        </TR>
        <TR  class= common>
          
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class=codeNo name=ManageCom ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >
          </TD>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet',[this,AgentCodeName],[0,1]);" onkeyup="return showCodeListKey('AgentCodet',[this,AgentCodeName],[0,1]);"><input class=codename name=AgentCodeName readonly=true >
          </TD>
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class=codeNo name=AgentGroup ondblclick="return showCodeList('AgentGroup',[this,AgentGroupName],[0,1]);" onkeyup="return showCodeListKey('AgentGroup',[this,AgentGroupName],[0,1]);"><input class=codename name=AgentGroupName readonly=true >
          </TD>  
        </TR>
        
    </table>
          <INPUT VALUE="查  询" class=cssButton  TYPE=button onclick="easyQueryClick();">  
          
    <table>    	
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton  TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<P>
  	<INPUT VALUE="修改投保单明细" class=cssButton TYPE=button onclick="returnParent();">    
  	</P>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
