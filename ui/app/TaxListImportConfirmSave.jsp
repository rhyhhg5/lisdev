<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
System.out.println("TaxListImportConfirmSave.jsp Begin ...");

GlobalInput tG = (GlobalInput)session.getValue("GI");

String FlagStr = "Fail";
String Content = "";

String tBatchNo = request.getParameter("ConfirmBatchNo");
String tOperateType = request.getParameter("fmOperatorFlag");
String tChk[] = request.getParameterValues("InpImportBatchGridChk");
String tBatchNos[] = request.getParameterValues("ImportBatchGrid1");
String tNames[] = request.getParameterValues("ImportBatchGrid2");
String tIdTypes[] = request.getParameterValues("ImportBatchGrid5");
String tIdNos[] = request.getParameterValues("ImportBatchGrid6");

System.out.println("tBatchNo:"+tBatchNo+";tOperateType:"+tOperateType);

LSInsuredListSet tLSInsuredListSet = new LSInsuredListSet();

if("ImportDelete".equals(tOperateType)){
	int count = tChk.length;
	int n = 0;
	for(int i=0; i<count; i++){
		LSInsuredListSchema tLSInsuredListSchema = new LSInsuredListSchema();
		if(tChk[i].equals("1")){
			String theBatchNo = tBatchNos[i];
			String theIdType = tIdTypes[i];
			String theIdNo = tIdNos[i];
			String theName = StrTool.GBKToUnicode(tNames[i]);
			String theSQL = "select insuredid from lsinsuredlist where batchno='"+theBatchNo+"' and idtype='"+theIdType+"' and idno='"+theIdNo+"' and name='"+theName+"'";
			String theInsuredId = new ExeSQL().getOneValue(theSQL);
			tLSInsuredListSchema.setBatchNo(theBatchNo);
			tLSInsuredListSchema.setInsuredId(theInsuredId);
			
			tLSInsuredListSet.add(tLSInsuredListSchema);
		}
	}
}

//向后台传数据需要满足两个条件。1、批次号不为空；2、有执行动作（删除或确认）。
if(tBatchNo!=null && tOperateType!=null){
	try{
		VData tVData = new VData();
		
		TransferData tTransferDate = new TransferData();
		tTransferDate.setNameAndValue("BatchNo",tBatchNo);
		
		tVData.add(tLSInsuredListSet);
		tVData.add(tTransferDate);
		tVData.add(tG);
		
		TaxConfirmListUI tTaxConfirmListUI = new TaxConfirmListUI();
		
		if(!tTaxConfirmListUI.submitData(tVData,tOperateType)){
			Content = "数据处理失败，原因是："+tTaxConfirmListUI.mErrors.getLastError();
			FlagStr = "Fail";
		}else{
			Content = "数据处理成功！";
			FlagStr = "Succ";
		}
	}catch(Exception e){
		Content = "出现异常，操作失败！";
		FlagStr = "Fail";
		e.printStackTrace();
	}
}

System.out.println("TAxListImportConfirmSave.jsp End ...");
%>

<html>
	<script language="javascript">
		parent.fraInterface.afterConfirmImport("<%=FlagStr%>", "<%=Content%>");
	</script>
</html>