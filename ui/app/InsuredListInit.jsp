<%@page import="com.sinosoft.utility.*"%>
<%
    GlobalInput globalInput = new GlobalInput();
    globalInput = (GlobalInput)session.getValue("GI");
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        fm.all('manageCom').value = "<%=globalInput.ManageCom%>";
        if(fm.all('manageCom').value != null)
        {
            var arrResult = easyExecSql("select Name from LDCom where ComCode = '" 
                + fm.all('manageCom').value + "'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('manageComName').value=arrResult[0][0];
            }
        }
        //初始化生效日期
        var arr = easyExecSql("select Current Date from dual");
        if(arr){
            fm.all('endSignDate').value = arr[0][0];
        }
    }
    catch(ex)
    {
        alert("InsuredListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initInsuredListGrid();
    }
    catch(re)
    {
        alert("InsuredListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var InsuredListGrid;

// 保单信息列表的初始化
function initInsuredListGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="团体保单号";
        iArray[1][1]="80px";
        iArray[1][2]=50;
        iArray[1][3]=0;
        iArray[1][21]="grpContNo";

        iArray[2]=new Array();
        iArray[2][0]="印刷号";
        iArray[2][1]="80px";
        iArray[2][2]=80;
        iArray[2][3]=0;
        iArray[2][21]="prtNo";

        iArray[3]=new Array();
        iArray[3][0]="机构编码";
        iArray[3][1]="60px";
        iArray[3][2]=60;
        iArray[3][3]=0;
        iArray[3][21]="manageCom";

        iArray[4]=new Array();
        iArray[4][0]="业务员编码";
        iArray[4][1]="60px";
        iArray[4][2]=60;
        iArray[4][3]=0;
        iArray[4][21]="agentCode";

        iArray[5]=new Array();
        iArray[5][0]="代理机构";
        iArray[5][1]="60px";
        iArray[5][2]=60;
        iArray[5][3]=0;
        iArray[5][21]="agentCom";

        iArray[6]=new Array();
        iArray[6][0]="销售渠道";
        iArray[6][1]="60px";
        iArray[6][2]=60;
        iArray[6][3]=0;
        iArray[6][21]="saleChnl";

        iArray[7]=new Array();
        iArray[7][0]="承保日期";
        iArray[7][1]="60px";
        iArray[7][2]=60;
        iArray[7][3]=0;
        iArray[7][21]="signDate";

        iArray[8]=new Array();
        iArray[8][0]="被保险人数";
        iArray[8][1]="60px";
        iArray[8][2]=60;
        iArray[8][3]=0;
        iArray[8][21]="insuredCount";

        InsuredListGrid = new MulLineEnter("fm", "InsuredListGrid");
        //这些属性必须在loadMulLine前
        InsuredListGrid.mulLineCount = 0;
        InsuredListGrid.displayTitle = 1;
        InsuredListGrid.hiddenPlus = 1;
        InsuredListGrid.hiddenSubtraction = 1;
        InsuredListGrid.canSel = 1;
        InsuredListGrid.canChk = 0;
        InsuredListGrid.loadMulLine(iArray);
        //InsuredListGrid.selBoxEventFuncName = "InsuredListDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>