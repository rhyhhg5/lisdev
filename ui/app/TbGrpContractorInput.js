//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
parent.fraMain.rows = "0,0,0,0,*";
var turnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
function FillAddress(){
	var province = fm.Province.value;
	var city = fm.City.value;
	var county = fm.County.value;
	var detailAddress = fm.DetailAddress.value;
	if (fm.CityID.value == "000000") {
		fm.ConstructAddress.value =province + detailAddress;	
	}else if(fm.CountyID.value == "000000") {
		fm.ConstructAddress.value=province + city + detailAddress;
	}else{
		fm.ConstructAddress.value=province + city + county + detailAddress;
	}
}
//提交，保存按钮对应操作
function submitForm()
{	
	if(!CheckAddress()){
		return false;
	}
	var sql1="select 1 from LCContPlanDutyParam where grpcontno='"+GrpContNo+"' and calfactor='ProjectAddress' and CalFactorValue is not null and CalFactorValue<>'' with ur";
	var arr = easyExecSql(sql1);
	if(arr){
		alert("保存结果只能进行修改操作！");
		return false;
	}
	var address=fm.ConstructAddress.value;
	if(address==""||address==null)
		{ alert("施工地址未录入！");
		 return false;
		}
	var projectName=ConractorGrid.getRowColData(0,4)
	if(projectName==""||projectName==null)
		{ alert("项目名称未录入！");
		 return false;
		}
	var premCalType=ConractorGrid.getRowColData(2,4)
	if(premCalType==""||premCalType==null)
		{ alert("保费计算方式未录入！");
		 return false;
		}
	if(premCalType=="1"){
		var  conPrice=ConractorGrid.getRowColData(3,4)
		if(conPrice==""||conPrice==null)
		{ alert("保费计算方式为：按承包合同价计算，施工承包合同价必须录入！");
		 return false;
		}
	}
	if(premCalType=="2"){
		var  conArea=ConractorGrid.getRowColData(4,4)
		if(conArea==""||conArea==null)
		{ alert("保费计算方式为：按建筑面积计算，施工建筑总面积必须录入！");
		 return false;
		}
	}	
  fm.fmtransact.value = "INSERT||MAIN" ;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	window.focus();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  initForm();
 
  
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在TbGrpConractorInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if(!CheckAddress()){
		return false;
	}
	var sql1="select 1 from LCContPlanDutyParam where grpcontno='"+GrpContNo+"' and calfactor='ProjectAddress' and CalFactorValue is not null and CalFactorValue<>'' with ur";
	var arr = easyExecSql(sql1);
	if(arr==null){
		alert("请先做建工险要素保存操作，再进行修改!");
		return false;
	}
	var address=fm.ConstructAddress.value;
	if(address==""||address==null)
		{ alert("施工地址未录入！");
		 return false;
		}
	var projectName=ConractorGrid.getRowColData(0,4)
	if(projectName==""||projectName==null)
		{ alert("项目名称未录入！");
		 return false;
		}
	var premCalType=ConractorGrid.getRowColData(2,4)
	if(premCalType==""||premCalType==null)
		{ alert("保费计算方式未录入！");
		 return false;
		}
	if(premCalType=="1"){
		var  conPrice=ConractorGrid.getRowColData(3,4)
		if(conPrice==""||conPrice==null)
		{ alert("保费计算方式为：按承包合同价计算，施工承包合同价必须录入！");
		 return false;
		}
	}
	if(premCalType=="2"){
		var  conArea=ConractorGrid.getRowColData(4,4)
		if(conArea==""||conArea==null)
		{ alert("保费计算方式为：按建筑面积计算，施工建筑总面积必须录入！");
		 return false;
		}
	}
	  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“修改”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}
//保存时校验联系人地址
function CheckAddress(){
	//校验判断联系人地址是否为空
	if (fm.Province.value=="" || fm.City.value=="" || fm.County.value=="" || fm.DetailAddress.value=="") {
		alert("团体基本资料中的省，市，县和详细地址不能为空，请核实！");
		return false;
	}
	
	//校验省和市是否与匹配
	var provinceID = fm.ProvinceID.value;
	var cityID = fm.CityID.value;
	var countyID = fm.CountyID.value;
	var strSql = "select code from ldcode1 where codetype='province1' and code = '" + provinceID + "'";
	var strSql2 = "select code1 from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
	var strSql3 = "select code from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
	var strSql4 = "select code1 from ldcode1 where codetype='county1' and code = '" + countyID  + "'";
	var arrResult = easyExecSql(strSql);
	var arrResult2 = easyExecSql(strSql2);
	var arrResult3 = easyExecSql(strSql3);
	var arrResult4 = easyExecSql(strSql4);
	
	if (provinceID=="710000" || provinceID=="810000" || provinceID=="820000"||provinceID=="990000") {
		if(cityID != "000000"){
 			alert("省和市不匹配，请核实！");
 			return false;
		}
		if(countyID != "000000"){
			alert("市和县不匹配，请核实！");
 			return false;
		}

	}else if ((provinceID=="620000"&&cityID=="620200") || (provinceID=="440000"&&(cityID=="442000"||cityID=="441900")) ) {
		if(countyID != "000000"){
			alert("市和县不匹配，请核实！");
 			return false;
		}
	}else{
 		if (arrResult[0][0] != arrResult2[0][0]) {
 			alert("省和市不匹配，请核实！");
 			return false;
 		}
 		if (arrResult3[0][0] != arrResult4[0][0]) {
 			alert("市和县不匹配，请核实！");
 			return false;
 		}
	}
	return true;
	
}
//返现省市县数据
function displayConstruct() {
	 fm.all('ProvinceID').value = "";
	 fm.all('Province').value = "";
	 fm.all('CityID').value = "";
	 fm.all('City').value = "";
	 fm.all('CountyID').value = "";
	 fm.all('County').value = "";
	 fm.all('DetailAddress').value = "";
	 fm.all('ConstructAddress').value = "";
	var tSql1="select bak2,(select CodeName from LDCode1 where codetype = 'province1' and Code1 = '0' and code=lcg.bak2),bak3,(select CodeName from LDCode1 where codetype = 'city1' and Code1 = lcg.bak2 and code=lcg.bak3 ),bak4,(select CodeName from LDCode1 where codetype = 'county1' and Code1 = lcg.bak3 and code=lcg.bak4),bak5 from lcgrpcontsub lcg where prtno='"+fm.PrtNo.value+"' with ur";
	 arrResult = new easyExecSql(tSql1, 1, 0);
	  try {
	    fm.all('ProvinceID').value = arrResult[0][0];
	  } catch(ex) { }
	  ;
	  try {
	    fm.all('Province').value = arrResult[0][1];
	  } catch(ex) { }
	  ;
	  try {
	    fm.all('CityID').value = arrResult[0][2];
	  } catch(ex) { }
	  ;
	  try {
	    fm.all('City').value = arrResult[0][3];
	  } catch(ex) { }
	  ;
	  try {
		    fm.all('CountyID').value = arrResult[0][4];
		  } catch(ex) { }
		  ;
		  try {
		    fm.all('County').value = arrResult[0][5];
		  } catch(ex) { }
		  ;
		  try {
			    fm.all('DetailAddress').value = arrResult[0][6];
			  } catch(ex) { }
			  ;
			  var tSql2="select CalFactorValue from LCContPlanDutyParam where grpcontno='"+GrpContNo+"'and calfactor='ProjectAddress' and CalFactorType='0' with ur";
				 arrResult = new easyExecSql(tSql2, 1, 0);
				 try {
					    fm.all('ConstructAddress').value = arrResult[0][0];
					  } catch(ex) { }
					  ;
}
//初始化信息
function initDutyFactor(){
		var strSql = "select distinct CalFactor,FactorName,FactorNoti,(select distinct CalFactorValue from LCContPlanDutyParam where GrpContNo='"+GrpContNo+"' and ContPlanCode='11' and CalFactor=lmriskdutyfactor.CalFactor ),CalFactorType from lmriskdutyfactor where "
		+" RiskCode in (select RiskCode from lmriskapp where risktype8='4') and riskcode in(select riskcode from lcgrppol where grpcontno='"+GrpContNo+"') and chooseflag='1'  and calfactor<>'ProjectAddress' order by CalFactor";
		turnPage.queryModal(strSql,ConractorGrid);
}