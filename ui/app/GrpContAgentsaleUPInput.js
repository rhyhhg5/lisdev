var arrDataSet
var showInfo;
var turnPage = new turnPageClass();
var Grididex;
var tAgentCom;
function easyQueryClick()
{
	
    // 初始化表格
	initSetGrid();
    if(!verifyInput2())
    {
	    return false;
    }
    // 书写SQL语句
    var strSQL = "";
	strSQL = "select GrpContNo,PrtNo,SaleChnl,AgentCom,AgentSaleCode "
        + " from lcgrpcont "
        + "where 1=1 "
        + getWherePart('GrpContNo','ContNo')
        + getWherePart('PrtNo','PrtNo')
        + getWherePart('ManageCom','ManageCom');
	turnPage.queryModal(strSQL, SetGrid);
	if(SetGrid.mulLineCount==0)
	{
	    alert("没有查询到任何满足条件的数据！");
	    return false;
	 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}


//更新代理业务员编码
function Gotoupdate()
{
    if(fm.all("AgentSaleCode").value == null || fm.all("AgentSaleCode").value == "")
    {
        alert("请录入代理销售员编码");
        return;
    }
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}


//代理销售业务员的查询
function queryAgentSaleCode()
{
	var rowNum = SetGrid.getSelNo();
	if(rowNum>0){
		tAgentCom=SetGrid.getRowColData(rowNum-1,4);
	}else if(rowNum==0){
		alert("请选择一条数据");
		return;
		}
	
    if(fm.all('AgentSaleCode').value == "")
    {
        var strURL = "../sys/AgentSaleCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&AgentCom=" + tAgentCom
        var newWindow = window.open(strURL, "AgentSaleCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
}

function afterQuery5(arrResult)
{
	  if(arrResult!=null) {
	    fm.AgentSaleCode.value = arrResult[0][0];
	    fm.AgentSaleName.value = arrResult[0][1];
	  }
}


