//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询事件
function easyQueryClick()
{
    if(!checkDate()) return false;

    var strSQL = "";
    if(fm.contType.value == "1")
    {
        var filialePrint = "";
        if(fm.cardFlag.value == "0")
        {
            filialePrint = "and exists (select 1 from LDCode1 where CodeType = 'printchannel' and Code = substr(a.ManageCom, 1, 4) and Code1 = ContType and CodeName = '1') ";
        }
        strSQL = "select a.ManageCom, c.BranchAttr, c.Name, "
            + "(select CodeName from LDCode where CodeType = 'lcsalechnl' and Code = a.SaleChnl), "
            + "getUniteCode(a.AgentCode), (select d.Name from LAAgent d where a.AgentCode = d.AgentCode), "
            + "a.PrtNo, a.ContNo, a.AppntName, a.Prem, "
            + "a.CValidate, a.SignDate, b.MakeDate, b.MakeTime, "
            + "(case when a.cardflag='0' then '标准个险平台' when a.cardflag in ('1','2','3','5','6') then '个人简易平台' "
            + "when a.cardflag='8' then '万能险平台' when a.cardflag='b' then '电商平台' when a.cardflag='a' then '信保通' "
            + "when a.cardflag='9' then '银保通' when a.cardflag='c' then '网银' when a.cardflag='d' then 'ATM' " 
            + "when a.cardflag='e' then '手机银行' when a.cardflag='f' then '智慧柜员机' else '其他' end)," +
            		"(case when b.PrintType ='1' then'电子保单' else '' end)  "
            + "from LCCont a, LCContPrint b, LABranchGroup c "
            + "where a.ContType = '1' and b.OtherNoType = '1' and a.ContNo = b.OtherNo and c.AgentGroup = a.AgentGroup "
            + "and b.ManageCom like '" + fm.manageCom.value + "%' "
            + filialePrint
            + "and b.MakeTime between '" + fm.startTime.value + ":00:00' and '" + fm.endTime.value + ":00:00' "
            + "and b.FilePath like '%.xml' "
            + getWherePart('a.PrtNo', 'prtNo')
            + getWherePart('a.SaleChnl', 'saleChnl')
            + getWherePart('a.CardFlag', 'cardFlag')
            + getWherePart('b.MakeDate', 'dailyDate')
            + "order by c.BranchAttr, a.AgentCode";
    }
    else if(fm.contType.value == "2")
    {
        strSQL = "select a.ManageCom, BranchAttr, c.Name, "
            + "(select CodeName from LDCode where CodeType = 'salechnl' and Code = a.SaleChnl), "
            + "a.AgentCode, (select d.Name from LAAgent d where a.AgentCode = d.AgentCode), "
            + "a.PrtNo, a.GrpContNo, a.GrpName, a.Prem, "
            + "a.CValidate, a.SignDate, b.MakeDate, b.MakeTime, "
            + "(case when a.cardflag is null then '标准团单' when a.cardflag='0' then '简易团单' "
            + "when a.cardflag='2' then '卡折业务' when a.cardflag='3' then '网销' "
            + "when a.cardflag='4' then '团体工伤险' else '其他' end),'' "
            + "from LCGrpCont a, LCContPrint b, LABranchGroup c "
            + "where b.OtherNoType = '2' and a.GrpContNo = b.OtherNo and c.AgentGroup = a.AgentGroup "
            + "and b.ManageCom like '" + fm.manageCom.value + "%' "
            + "and exists (select 1 from LDCode1 where CodeType = 'printchannel' and Code = substr(a.ManageCom, 1, 4) and Code1 = '2' and CodeName = '1') "
            + "and b.MakeTime between '" + fm.startTime.value + ":00:00' and '" + fm.endTime.value + ":00:00' "
            + "and b.FilePath like '%.xml' "
            + getWherePart('a.PrtNo', 'prtNo')
            + getWherePart('a.SaleChnl', 'saleChnl')
            + getWherePart('b.MakeDate', 'dailyDate')
            + "order by c.BranchAttr, a.AgentCode";
    }
    else
    {
        alert("请选择保单类型");
        return false;
    }

    turnPage.queryModal(strSQL, ContPrintDailyGrid);

    fm.querySql.value = strSQL;
}

//校验日期
function checkDate()
{
	if(fm.dailyDate.value == null || fm.dailyDate.value == "")
	{
		alert("查询日期必选!");
		return false;
	}
    if(fm.startTime.value == null || fm.startTime.value == "")
    {
        alert("查询起始时间必选！");
        return false;
    }
    if(fm.endTime.value == null || fm.endTime.value == "")
    {
        alert("查询终止时间必选！");
        return false;
    }
    return true;
}

//下载事件
function download()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && ContPrintDailyGrid.mulLineCount > 0)
    {
        fm.action = "ContPrintDailyDownload.jsp";
        fm.target = "_blank";
        fm.submit();
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
