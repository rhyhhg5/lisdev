//TempFeeWithdrawInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var tFees;
//提交，保存按钮对应操作
function submitForm()
{ 
  var i = 0;
  var checkFlag = 0;
  for (i=0; i<FeeGrid.mulLineCount; i++) {
    if (FeeGrid.getChkNo(i)) { 
      var tempFeeNo = FeeGrid.getRowColData(i,2);
      var strSqlCheck="select 1 from ljtempfee where tempfeeno='"+tempFeeNo+"' and tempfeetype='1' with ur";
      var arrResult = easyExecSql(strSqlCheck);
      if (arrResult!='null'&&arrResult!=''&&arrResult!=null) {
         if(confirm("暂收号"+tempFeeNo+"收费类型为‘团体保单新单收费’，请确认是否需要做暂收退费操作，谢谢"))
         {
		  }
	     else{
            break;
		  }
         }
         checkFlag = 1;
    }
  }
   if(fm.qPayMode.value==''||fm.qPayMode.value==null){
   checkFlag = 0;
   }
    if (checkFlag) { 
   // if(confirm('如果已经打印暂交费退费实付凭证，请选择<确定>，否则选择<取消>，然后点击<打印暂交费退费实付凭证>按钮进行打印！'))
   //  {
		   var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
       showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
       fm.submit(); //提交
	//	}
	//  else{
	//	window.open("./TempFeeWithdrawQuery.jsp")
	//	}

  }
  else {
    alert("您没有选择暂交费信息或者没有选择暂交费付费方式！"); 
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    //var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

var queryBug = 0;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//暂交费号码查询按钮
function tempFeeNoQuery(afterQuery) {
 var tempFeeNo = fm.TempFeeNo.value;
  var prtNo = fm.PrtNo.value;
  //只显示出该暂交费号码的未退费的险种
  //已核销、退费（ConfFlag）的不显示出来
  if (prtNo == "") { 
    var strSql = "select '', TempFeeNo, TempFeeType, RiskCode, APPntName, OtherNo, PayMoney, PayDate, EnterAccDate, PayMoney from LJTempFee where ConfMakedate is not null "
               + " and Confdate is null and TempFeeType in ('1','5','11','16') "
               + getWherePart('TempFeeNo')
               + getWherePart('RiskCode')
               + getWherePart('PayMoney')
               + getWherePart('PayDate')
               + getWherePart('EnterAccDate')
               + getWherePart('ManageCom','ManageCom','like')
               + getWherePart('AgentGroup')
               + getWherePart('getUniteCode(AgentCode)','AgentCode')
               + getWherePart('Operator');
  }
  else {
    var strSql = "select '', TempFeeNo, TempFeeType, RiskCode, APPntName, OtherNo, PayMoney, PayDate, EnterAccDate,PayMoney from LJTempFee where ConfMakedate is not null "
               + " and Confdate is null and TempFeeType in ('1','5','11','16') "
               + getWherePart('TempFeeNo')
               + getWherePart('RiskCode')
               + getWherePart('PayMoney')
               + getWherePart('PayDate')
               + getWherePart('EnterAccDate')
               + getWherePart('ManageCom','ManageCom','like')
               + getWherePart('AgentGroup')
               + getWherePart('getUniteCode(AgentCode)','AgentCode')
               + getWherePart('Operator')
               + " and ((OtherNo='" + prtNo + "')"
               + " or (OtherNo in (select ProposalNo from lcpol where PrtNo='" + prtNo + "')))";               
  }
  //alert(strSql);
             
  turnPage.queryModal(strSql, FeeGrid);

}
