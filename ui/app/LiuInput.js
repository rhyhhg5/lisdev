var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();

//***************************************************
//* 单击“查询”进行的操作
//***************************************************
function easyQueryClick()
{  
  if( verifyInput() == false )   
  	return false;  
	var tSQL = "";
	
	tSQL = "select SerialNo,Name,Sex,Birthday,Idtype,Idno,Makedate" 
    + " FROM "
    + " NewPeopleTaskTable " 
    + " where  1=1 "
	+ getWherePart('Name', 'Name','like')
	+ getWherePart('Sex', 'Sex')
	+ getWherePart('Idtype', 'IDType')
	+ getWherePart('Idno', 'IDNo','like')
	+ getWherePart('Birthday', 'Birthday')
	+ getWherePart('Makedate', 'MakeDate')
	+ " ORDER BY SerialNo" ;
	
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  
		alert("没有符合条件的信息！"); 
		turnPage.queryModal(tSQL, EvaluateGrid); 
		return false;
	}
    else 
    	turnPage.queryModal(tSQL, EvaluateGrid);
}


function gotoAdd()
{
	//下面增加相应的代码

	showInfo=window.open("./LiuAdd.jsp?transact=insert");
}
function gotoUpdate()
{
  var checkFlag = 0;
  for (i=0; i<EvaluateGrid.mulLineCount; i++)
  {
    if (EvaluateGrid.getSelNo(i))
    {
      checkFlag = EvaluateGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	var	tSerialNo = EvaluateGrid.getRowColData(checkFlag - 1, 1);
  
  	showInfo=window.open("./LiuAdd.jsp?transact=update&SerialNo="+tSerialNo);
  }else{
  	alert("请选择需要修改的信息！");
  	return false;
  }
  return true;
}

//删除操作
function deleteClick()
{
  var checkFlag = 0;
  for (i=0; i<EvaluateGrid.mulLineCount; i++)
  {
    if (EvaluateGrid.getSelNo(i))
    {
      checkFlag = EvaluateGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	 var tSerialNo = EvaluateGrid.getRowColData(checkFlag - 1, 1);
  	 if(tSerialNo== null || tSerialNo == ""){
  	 	alert("删除时，获取人员编码失败！");
  		return false;
  	 }
  	 if (confirm("您确实想删除该记录吗?"))
	  {
	  var i = 0;
	  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.fmtransact.value = "DELETE||MAIN";
	  fm.action = "./LiuQueryInputSave.jsp?SerialNo="+tSerialNo;
	  fm.submit(); //提交
//	  fm.action="";
//	  initForm();

	  }
	  else
	  {
	    alert("您取消了删除操作！");
	  }
  }else{
  	alert("请选择需要删除的信息！");
  	return false;
  }
}


//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content,aSerialNo)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  easyQueryClick();
} 
