<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
  //程序名称：
  //程序功能：
  //创建日期：2005-12-12 17:07
  //创建人  ：yangming
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LCPreviewInput.js"></SCRIPT>
<%@include file="LCPreviewInputInit.jsp"%>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body onload="initForm();initElementtype();">
<form action="./LCPreviewSave.jsp" method=post name=fm target="fraSubmit">
<table>
  <tr>
    <td>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivQuery);">
    </td>
    <td class=titleImg>查询条件:</td>
  </tr>
</table>
<DIV id=DivQuery STYLE="display:''">
  <TABLE class=common>
    <TR CLASS=common>
      <TD CLASS=title>印刷号</TD>
      <TD CLASS=input COLSPAN=1>
        <Input class="common" NAME=PrtNo>
      </TD>
      <TD CLASS=title>投保人</TD>
      <TD CLASS=input COLSPAN=1>
        <Input class="common" NAME=AppntName>
      </TD>
      <TD CLASS=title STYLE="display:'none'">被保人</TD>
      <TD CLASS=input COLSPAN=1 STYLE="display:'none'">
        <Input class="common" NAME=InsuredName>
      </TD>

      <TD CLASS=title>管理机构</TD>
      <TD CLASS=input COLSPAN=1>
        <Input class="common" NAME=ManageCom>
      </TD>
          </TR>
    <tr>
      <TD CLASS=title>生效日期</TD>
      <TD CLASS=input COLSPAN=1>
        <Input class="coolDatePicker" NAME=CValiDate>
      </TD>
      <TD CLASS=title>核保师代码</TD>
      <TD CLASS=input COLSPAN=1>
        <Input class="common" NAME=UWOperator>
      </TD>
    </tr>
  </TABLE>
</DIV>
<table>
  <tr>
    <td>
      <input type=button class=cssButton value="查 询" onclick="QueryPreviewContInfo()">
    </td>
  </tr>
</table>
<Div id="divLCImpart1" style="display: ''">
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanPreviewContGrid"> </span>
      </td>
    </tr>
  </table>
   <Div  id= "divPage" align=center style= "display: '' ">
      <INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">    
    </div>
</div>

 <P>
    <input type=button class=cssButton value="预打保单" onclick="PrintPol()">
 </P>
<input type="hidden" name=MissionID>
<input type="hidden" name=SubMissionID>
<input type="hidden" name=ActivityID>
<input type="hidden" name=ContNo>
<input type="hidden" name=fmtransact>
</form>
  <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
