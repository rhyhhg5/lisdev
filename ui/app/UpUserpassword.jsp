<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; 	//记录管理机构
    var comcode = "<%=tGI.ComCode%>";		//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="UpUserpassword.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="UpUserpasswordInit.jsp" %>
<title>用户密码修改</title>
</head>
<body onload="initForm();">
	<form action="./UpUserpasswordSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 保单信息部分 -->
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>
		<table class="common" align=center>
			<tr class="common">
				<td class="title">管理机构</td>
				<td class="input"><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true ></td>
				<td class="title">用户编码</td>
				<td class="input"><input class="common" name="UserCode" /></td>
			</tr>
			<tr class="common">
				<td class="title">用户姓名</td>
				<td class="input"><input class="common" name="UserName" /></td>				
			</tr>
		</table>
		<input class="cssButton" type="button" value=查询用户 onclick="easyQueryClick();" />
		<input class="cssButton" type="button" value=重置密码 onclick="updatepol();" />
		<input class="cssButton" type="button" value=上级修改 onclick="updatelevel();" />
		<INPUT  type="hidden" class=Common name=querySql >
		
		<table>
			<tr>
				<td class=common>
					<IMG src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divLCPol1);">
				</td>
				<td class=titleImg>用户信息</td>
			</tr>
		</table>
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" ></span>
					</td>
				</tr>
			</table>
			<table align=center>
			<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
		 </table>
		</div>
		
			<input type=hidden id="fmtransact" name="fmtransact">

	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>