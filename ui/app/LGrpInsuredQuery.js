var turnPage = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
  	//parent.fraInterface.initForm();
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showDiv(operateButton, "true");
		showDiv(inputButton, "false");
    //执行下一步操作
	}
}
// 查询按钮
function easyQueryClick() {
	if (!verifyInput2()) {
		return false;
	}
	//此处书写SQL语句			     			    
	var strSql = "select Seqno,Name,Sex,(case Sex when '1' then '\u5973' else '\u7537' end),Birthday,IDType,(select codename from ldcode where codetype='idtype' and code=IDType),IDNo,IDStartDate,IDEndDate,OthIDType,(select codename from ldcode where codetype='idtype' and code=OthIDType),OthIDNo,ContPlanCode,OccupationCode,OccupationType,(select  CodeName from ldcode where codetype = 'occupationtype' and code=OccupationType),Retire,(case Retire when '1' then '\u5728\u804c' else '\u9000\u4f11' end)," + "EmployeeName,Relation,(case Relation when '00' then '\u672c\u4eba' when '01' then '\u59bb\u5b50' when '02' then '\u4e08\u592b' when '03' then '\u5b50\u5973' else '\u5176\u5b83' end ),BankCode,BankAccNo,AccName" + " from LCGrpSubInsuredImport where prtno = '" + PrtNo + "' " + getWherePart("Name", "Name") + getWherePart("Sex", "Sex") + getWherePart("Birthday", "Birthday") + getWherePart("IDType", "IDType") + getWherePart("IDNo", "IDNo");
	turnPage.queryModal(strSql, LCInuredListGrid);
}
function getQueryResult() {
	var arrSelected = null;
	tRow = LCInuredListGrid.getSelNo();
	if (tRow == 0 || tRow == null) {
		return arrSelected;
	}
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = LCInuredListGrid.getRowData(tRow - 1);
	return arrSelected;
}
function returnParent() {
	var arrReturn = new Array();
	var tSel = LCInuredListGrid.getSelNo();
	if (tSel == 0 || tSel == null) {
		top.close();
	} else {
		try {	
			//alert(tSel);
			arrReturn = getQueryResult();
			top.opener.afterQuery(arrReturn);
		}
		catch (ex) {
			alert("\u6ca1\u6709\u53d1\u73b0\u7236\u7a97\u53e3\u7684afterQuery\u63a5\u53e3\u3002" + ex);
		}
		top.close();
	}
}

