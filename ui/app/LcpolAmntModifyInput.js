
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function EasyQuery() {
	var grpContNo = fm.GrpContNo.value;
	var insuredNo = fm.InsuredNo.value;
	var riskCode = fm.RiskCode.value;
	if (grpContNo == "" || insuredNo == "" || riskCode == "") {
		alert("保单号码、被保人客户号码和险种编码，都不能为空");
		return false;
	}
	var tSql = "select GrpContNo,insuredNo,riskCode,amnt,polNo,ContNo from lcpol where grpContNo ='" + grpContNo + "' and insuredNo='" + insuredNo + "' and riskCode='" + riskCode + "'";
	turnPage.pageLineNum = 10;
	turnPage.queryModal(tSql, LcpolAmntModifyGrid);
	var arr = easyExecSql(tSql);
	if (!arr) {
		alert("未查询到数据！");
	}
}

//用于MulLine单选内容得回选
function query()
{
	  var tSelNo = LcpolAmntModifyGrid.getSelNo();
	  var tAmnt =LcpolAmntModifyGrid.getRowColData(tSelNo-1,4);//muline取出第4列作为查询条件
	  fm.all('Amnt').value = tAmnt;
}

//修改保额
function updateClick() {
	var amnt = fm.all("Amnt").value;
	var mSelNo = LcpolAmntModifyGrid.getSelNo();
	if (mSelNo == 0) {
		alert("请选择需要修改的数据！");
		return false;
	}
	/**判断保额polNo空值问题*/
	if ((amnt == null || amnt == "")) {
		alert("请填写修改后的保额！");
		return false;
	}
	/**判断保额有值*/
	if (amnt != null && amnt != "") {
		if (!isNumeric(amnt)) {
			alert("保额必须为数字！");
			return false;
		}
		if (amnt < 0) {
			alert("保额必须大于0！");
			return false;
		}
	}
	if (beforeSubmit()) {
		fm.fmtransact.value = "UPDATE";
		var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit();
	}
}
function beforeSubmit() {
	return true;
}
/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content) {
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		window.focus();
		//window.location.reload();
	}
}