<%
//程序名称：DiskApplyInit2.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initForm() {
  try {
    initImportGrid();
  } catch(re) {
    alert("DiskApplyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initImportGrid() {
  var iArray = new Array();

  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="批次号";         		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="合同ID序号";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="被保险人序号";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="险种编码";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="错误信息";         		//列名
    iArray[5][1]="200px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="被保人姓名";         		//列名
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许


    ImportGrid = new MulLineEnter( "fm" , "ImportGrid" );
    //这些属性必须在loadMulLine前
    ImportGrid.mulLineCount = 0;
    ImportGrid.displayTitle = 1;
    ImportGrid.locked = 1;
    ImportGrid.canSel = 0;
    ImportGrid.hiddenPlus = 1;
    ImportGrid.hiddenSubtraction = 1;
    ImportGrid.loadMulLine(iArray);

    //这些操作必须在loadMulLine后面
    //GrpPolGrid.setRowColData(1,1,"asdf");
  } catch(ex) {
    alert(ex);
  }
}

</script>
