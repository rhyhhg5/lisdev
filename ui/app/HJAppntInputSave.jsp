<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输入参数
	LCInsuredListSchema mLCInsuredListSchema = new LCInsuredListSchema();
	HJAppntUI mHJAppntUI = new HJAppntUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mGrpContNo = request.getParameter("GrpContNo");
	String mInsuredId = request.getParameter("InsuredId");

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	String strInput = "";

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmAction");
	//System.out.println(transact);
	if ("UPDATE".equals(transact)){
          LCInsuredListDB mLCInsuredListDB= new LCInsuredListDB();
          mLCInsuredListDB.setGrpContNo(mGrpContNo);
          mLCInsuredListDB.setInsuredID(mInsuredId);
          if(mLCInsuredListDB.getInfo()){
          mLCInsuredListSchema=mLCInsuredListDB.getSchema();
          
          mLCInsuredListSchema.setAppntName(request.getParameter("AppntName"));
          mLCInsuredListSchema.setAppntSex(request.getParameter("AppntSex"));
          mLCInsuredListSchema.setAppntBirthday(request.getParameter("AppntBirthday"));
          mLCInsuredListSchema.setAppntIdType(request.getParameter("AppntIDType"));
          mLCInsuredListSchema.setAppntIdNo(request.getParameter("AppntIDNo"));
          
           // 准备向后台传输数据 VData
            VData tVData = new VData();
            FlagStr = "";
            tVData.add(tG);
            tVData.addElement(mLCInsuredListSchema);
            //System.out.println("VData的值"+tLCProjectInfoSchema);
            try {
                mHJAppntUI.submitData(tVData, transact);
            } catch (Exception ex) {
                Content = "保存失败，原因是:" + ex.toString();
                FlagStr = "Fail";
            }
            if (!FlagStr.equals("Fail")) {
                tError = mHJAppntUI.mErrors;
                if (!tError.needDealError()) {
                    Content = " 保存成功! ";
                    FlagStr = "Succ";
                } else {
                    Content = " 保存失败，原因是:" + tError.getFirstError();
                    FlagStr = "Fail";
                }
            }
          }else{
          Content = " 保存失败，原因是:被保人信息查询失败！";
          FlagStr = "Fail";
          }
          
		  
	} 
	
	
%>
<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>