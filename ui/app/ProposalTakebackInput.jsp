<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
    var branch = "";//更加销售渠道查询业务员时用到
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
 <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="ProposalTakebackInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ProposalTakebackInit.jsp"%>
<title>重打个单</title>
</head>
<body onload="initForm();" >
	<form action="./ProposalTakebackSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr>
				<td class= titleImg align= center>请输入保单查询条件：</td>
			</tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>管理机构</TD>
				<TD  class= input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true > </TD>
				<TD class= title>印 刷 号</TD>
				<TD class= input><Input class= common name=PrtNo></TD>
				<TD class= title>合 同 号</TD>
				<TD class= input><Input class= common name=ContNo></TD>
			</TR>
			<TR class= common>
		    	<TD class= title>保单类型</TD>
				<TD class= input><Input class=codeno name=ContType value="1" CodeData="0|^1|个单^2|团单" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" ><input class=codename name=ContTypeName value="个单" readonly=true ></TD>
				<td class="title">渠 道</td>
                <td class="input"><input class="codeNo" name="SaleChnlCode" ondblclick="return showCodeList('salechnlall',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('salechnlall',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" /></td>
			    <TD class= title>业务员代码</TD>
				<TD class= input><Input class=codeNo name=AgentCode ondblclick="beforeQueryAgent();return showCodeList('agentcode',[this,AgentCodeName],[0,1],null,'1 and ManageCom like #' + fm.all('ManageCom').value + '%#' + branch,'1',1);" 
				    onkeyup="beforeQueryAgent();return showCodeList('agentcode',[this,AgentCodeName],[0,1],null,'1 and ManageCom like #' + fm.all('ManageCom').value + '%#' + branch,'1',1);"><input class=codename name=AgentCodeName readonly=true ></TD>
            </tr>
			<TR class= common>
				<td class="title">网 点</td>
                <td class="input"><input id="AgentComBank" class="code" name="AgentComBank" style="display:none;" ondblclick="return queryAgentComBank();" /></td>
				<TD class= title>投 保 人</TD>
				<TD class= input><Input class= common name=AppntName verify="投保人|len<=20"></TD>
				<TD class= title>被 保 人</TD>
				<TD class= input><Input class= common name=InsuredName verify="被保人|len<=20"></TD>
			</TR>
			<TR class= common>
                <TD class= title>回销类型</TD>
				<TD class= input><Input class=codeno name=GetpolState  CodeData="0|^0|未回销^1|已回销" ondblclick="return showCodeListEx('GetpolState',[this,GetpolStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('GetpolState',[this,GetpolStateName],[0,1],null,null,null,1);"><input class=codename name=GetpolStateName readonly=true ></TD>
				<TD class= title>申请日期</TD>
				<TD class= input><Input class= coolDatePicker name=PolApplyDate></TD>
				<TD class= title>生效日期</TD>
				<TD class= input><Input class= coolDatePicker name=CValiDate></TD>
			</TR>
			<TR class= common>
				<TD class= title>签单日期起</TD>
				<TD class= input><Input class= coolDatePicker name=startSignDate></TD>
				<TD class= title>签单日期止</TD>
				<TD class= input><Input class= coolDatePicker name=endSignDate></TD>
			</TR>
			<tr class=common>
				<td class=title>回销日期起</td>
				<td class=input><input class=coolDatePicker name=startGetPolDate></td>
				<td class=title>回销日期止</td>
				<td class=input><input class=coolDatePicker name=endGetPolDate></td>
			</tr>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<INPUT class=cssbutton VALUE="清单下载" TYPE=button onclick="downloadClick();"> 
		<INPUT  type="hidden" class=Common name=querySql >
	</form>
	<form action="./ProposalTakebackSave.jsp" method=post name=fmSave target="fraSubmit">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<Div id= "divLCPol1" style= "display: ''">
			<table class= common>
				<TR class= common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" ></span>
					</td>
				</tr>
			</table>

			<table align=center>
			<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
		 </table>
		</div>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmOperator" name="fmOperator">
		<input type=hidden id="mCertifyCode" name="mCertifyCode">
		<input type=hidden id="mManageCom" name="mManageCom">
		<input type=hidden id="mAgentCode" name="mAgentCode">
		<input type=hidden id="mContType" name="mContType">
		
					<table>
				<tr>
					<TD  class= titleImg> 保单回执回销信息</TD>
				</tr>
			</table>
			<table class= common align=center>
			<TR class= common>
				<TD class= title>合同号码</TD>
				<TD class= input><Input class= readonly readOnly=true name=mContNo></TD>
				<TD class= title>打印时间</TD>
				<TD class= input><Input class= readonly readOnly=true name=mPrintDate></TD>
				</TR>
			<TR class= common>
				<TD class= title>签收日期</TD>
				<TD class= input><Input class= coolDatePicker name=mGetpolDate></TD>
				<TD class= title>签收人员</TD>
				<TD class= input><Input class= common name=mGetpolMan></TD>
			</TR>
			<TR class= common>
				<TD class= title>递交人员</TD>
				<TD class= input><Input class= common name=mSendPolMan></TD>
				<TD class= title>操作人员</TD>
				<TD class= input><Input class= readonly readOnly=true name=mGetPolOperator></TD>
			</TR>
		</table>
			<table>
				<tr>
					<td>
						<INPUT VALUE="保  存" name=savebtn class="cssButton" TYPE=button onclick="savepol();">
					</td>
					<td><INPUT VALUE="修  改" name=updatebtn class="cssButton" TYPE=button onclick="updatepol();"></td>
				</tr>
			</table>
			
	</form>
	<form action="./ProposalTakebackReceiveSave.jsp" method=post name=fmReceive target="fraSubmit">
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden name="mReceiveID" value="">
		<input type=hidden name="mContNo" value="">
		<input type=hidden name="mPrtNo" value="">
		<input type=hidden name="mContType" value="">
		<input type=hidden name="BackReasonCode" value="">
		<input type=hidden name="BackReason" value="">
		<input type=hidden name="BackReasonDetail" value="">
	</form>
<span id="spanCode"style="display: none; position:absolute; slategray"></span>
</body>
</html>
