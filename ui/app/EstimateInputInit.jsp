<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    

<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
  
%>
<script language="JavaScript">

function initForm()
{
  try
  {
    initInpBox();
    initEstimateGrid();
    initGrpContInfo();
    showAllCodeName();
    initButton();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

function initInpBox()
{ 
  fm.all('GrpContNo').value = "";
  fm.all('ProjectNo').value = "";
  fm.all('ProjectName').value = "";
  fm.all('GrpName').value = "";
  fm.all('CvaliDate').value = "";
  fm.all('EstimateRate').value = "";
  fm.all('EstimateBalanceRate').value = "";
  fm.all('ContEstimatePrem').value = "";
  fm.all('BackEstimatePrem').value = "";
  fm.all('Reason').value = "";
  fm.all('Operator').value ="<%=tGI.Operator%>";
}

var EstimateGrid
function initEstimateGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="预估赔付率";         	  //列名
    iArray[1][1]="80px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="预估赔付率（含结余返还/保费回补）";         	  //列名
    iArray[2][1]="100px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[3]=new Array();
    iArray[3][0]="预估结余返还金额";         	
    iArray[3][1]="80px";            	
    iArray[3][2]=200;            		 
    iArray[3][3]=0;              		 
    
    iArray[4]=new Array();
    iArray[4][0]="预计保费回补金额";         	
    iArray[4][1]="80px";            	
    iArray[4][2]=200;            		 
    iArray[4][3]=0;  
    
    iArray[5]=new Array();
    iArray[5][0]="录入原因";         	  //列名
    iArray[5][1]="300px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="录入工号";         	  //列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;
    
    iArray[7]=new Array();
    iArray[7][0]="录入日期";         	  //列名
    iArray[7][1]="80px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    EstimateGrid = new MulLineEnter("fm", "EstimateGrid"); 
    //设置Grid属性
    EstimateGrid.mulLineCount = 0;
    EstimateGrid.displayTitle = 1;
    EstimateGrid.locked = 1;
    EstimateGrid.canSel = 1;
    EstimateGrid.canChk = 0;
    EstimateGrid.hiddenSubtraction = 1;
    EstimateGrid.hiddenPlus = 1;
    EstimateGrid.selBoxEventFuncName = "getQueryResult";
   
    EstimateGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
