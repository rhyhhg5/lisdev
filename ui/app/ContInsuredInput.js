//               该文件中包含客户端需要处理的函数和事件
var mOperate = "";
var showInfo1;
var mDebug="0";
var cflag = "0";  //问题件操作位置
var turnPage = new turnPageClass();
var arrResult;
var Resource;
var approvefalg;
//存放添加动作执行的次数
var addAction = 0;
//暂交费总金额
var sumTempFee = 0.0;
//暂交费信息中交费金额累计
var tempFee = 0.0;
//暂交费分类信息中交费金额累计
var tempClassFee = 0.0;
//单击确定后，该变量置为真，单击添加一笔时，检验该值是否为真，为真继续，然后再将该变量置假
var confirmFlag=false;
//
var arrCardRisk;
//window.onfocus=focuswrap;
var mSwitch = parent.VD.gVSwitch;
//工作流flag
var mWFlag = 0;
//使得从该窗口弹出的窗口能够聚焦
function focuswrap() {
  myonfocus(showInfo1);
}

//提交，保存按钮对应操作
function submitForm() {
	ChangeDecodeStr();
	}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
	UnChangeDecodeStr();
  try {
    showInfo.close();
  } catch(e) {}
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  if (FlagStr=="Succ" && mWFlag == 0 && LoadFlag!=24) {
    if(confirm("是否继续录入其他客户？")) {
      emptyInsured();
      if (fm.ContType.value==2) {
        fm.ContNo.value="";
        fm.ProposalContNo.value="";
      }

      if(fm.InsuredSequencename.value=="第一被保险人资料") {
        //emptyFormElements();
        param="122";
        fm.pagename.value="122";
        fm.SequenceNo.value="2";
        fm.InsuredSequencename.value="第二被保险人资料";
        if (scantype== "scan") {
          setFocus();
        }
        noneedhome();
        return false;
      }
      if(fm.InsuredSequencename.value=="第二被保险人资料") {
        //emptyFormElements();
        param="123";
        fm.pagename.value="123";
        fm.SequenceNo.value="3";
        fm.InsuredSequencename.value="第三被保险人资料";
        if (scantype== "scan") {
          setFocus();
        }
        noneedhome();
        return false;
      }
      if(fm.InsuredSequencename.value=="第三被保险人资料") {
        //emptyFormElements();
        param="123";
        fm.pagename.value="123";
        fm.SequenceNo.value="3";
        fm.InsuredSequencename.value="第三被保险人资料";
        if (scantype== "scan") {
          setFocus();
        }
        return false;
      }
      initArray();
    }
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterQuery( FlagStr, content ) {
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
}

//取消按钮对应操作
function cancelForm() {
}

//提交前的校验、计算
function beforeSubmit() {
  //添加操作

}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
  if(cDebug=="1") {
    parent.fraMain.rows = "0,0,50,82,*";
  } else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

//问题件录入
function QuestInput() {

 cContNo = fm.ContNo.value;  //保单号码
//  if(LoadFlag=="2"||LoadFlag=="4"||LoadFlag=="13") {
//    if(mSwitch.getVar( "ProposalGrpContNo" )=="") {
//      alert("尚无集体合同投保单号，请先保存!");
//    } else {
//      window.open("./GrpQuestInputMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&ProposalNo="+cGrpProposalContNo+"&Flag="+cflag,"window2");
      //window.open("./GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+cflag+"&ProposalNo="+cGrpProposalContNo,"window2");
//    }
//  } else {
    if(cContNo == "") {
      alert("尚无该投保单，请先保存!");
    } else {
      window.open("../uw/QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+ LoadFlag+"&AppFlag=2","window1");
    }
//  }
  var cGrpProposalContNo = fm.GrpContNo.value;  //团体保单号码
	//if(cGrpProposalContNo==""||cGrpProposalContNo==null)
	//{
  //		alert("请先选择一个团体主险投保单!");
  //		return ;
  //  }
  //
	//window.open("../uw/GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+LoadFlag,"window2");

}
//问题件查询
function QuestQuery() {
  cContNo = fm.all("ContNo").value;  //保单号码
  if(LoadFlag=="2"||LoadFlag=="4"||LoadFlag=="13") {
    if(mSwitch.getVar( "ProposalGrpContNo" )==""||mSwitch.getVar( "ProposalGrpContNo" )==null) {
      alert("请先选择一个团体主险投保单!");
      return ;
    } else {
      window.open("./GrpQuestQueryMain.jsp?GrpContNo="+mSwitch.getVar( "ProposalGrpContNo" )+"&Flag="+LoadFlag);
    }
  } else {
    if(cContNo == "") {
      alert("尚无该投保单，请先保存!");
    } else {
      window.open("../uw/QuestQueryMain.jsp?ContNo="+cContNo+"&Flag="+LoadFlag,"window1");
    }
  }
}
//Click事件，当点击增加图片时触发该函数
function addClick() {
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick() {
	ChangeDecodeStr();
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick() {
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick() {
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow) {
}

/*********************************************************************
 *  进入险种信息录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoRiskInfo() {
  if(fm.InsuredNo.value==""||fm.ContNo.value=="") {
    alert("请先添加，选择被保人");
    return false;
  }
  if(Resource==1|Resource==2|Resource==3){
  	alert("保单明细不能进入险种明细！");
  	return false;
  }
  //mSwitch =parent.VD.gVSwitch;
  delInsuredVar();
  addInsuredVar();

  try {
    mSwitch.addVar('SelPonNo','',fm.SelPolNo.value);
  } catch(ex) {}
  ; //选择险种单进入险种界面带出已保存的信息

  if ((/*(LoadFlag=='5'||*/LoadFlag=='6'||LoadFlag=='16')&&(mSwitch.getVar( "PolNo" ) == null || mSwitch.getVar( "PolNo" ) == "")) {
    alert("请先选择被保险人险种信息！");
    return;
  }
  try {
    mSwitch.addVar('SelPolNo','',fm.SelPolNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ContNo','',fm.ContNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.updateVar('ContNo','',fm.ContNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('mainRiskPolNo');
  } catch(ex) {}
  ;
  parent.fraInterface.window.location = "./ProposalInput.jsp?LoadFlag=" + LoadFlag+"&prtNo="+prtNo + "&ContType="+ContType+"&ScanFlag="+ScanFlag+"&scantype=" + scantype+ "&MissionID=" + MissionID+ "&SubMissionID=" + SubMissionID+"&BQFlag="+BQFlag+"&EdorType="+EdorType+"&checktype="+checktype+"&SaleChnl="+SaleChnl+"&SaleChnlDetail="+SaleChnlDetail+"&oldContNo="+oldContNo+"&ContNo="+ContNo+"&InsuredNo="+fm.InsuredNo.value;

}

/*********************************************************************
 *  删除缓存中被保险人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delInsuredVar() {
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('InsuredNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('PrtNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('GrpContNo');
  } catch(ex) {}
  ;
  //   try{mSwitch.deleteVar('AppntNo');}catch(ex){};
  //   try{mSwitch.deleteVar('ManageCom');}catch(ex){};
  try {
    mSwitch.deleteVar('ExecuteCom');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('FamilyType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('RelationToMainInsure');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('RelationToAppnt');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('AddressNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('SequenceNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Name');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Sex');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Birthday');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('IDType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('IDNo');
  } catch(ex) {}
  ;
  try {
	    mSwitch.deleteVar('InsuIDStartDate');
	  } catch(ex) {}
	  ;
  try {
		 mSwitch.deleteVar('InsuIDEndDate');
	   } catch(ex) {}
	  ;	  
	  
  try {
    mSwitch.deleteVar('RgtAddress');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Marriage');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('MarriageDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Health');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Stature');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Avoirdupois');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Degree');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('CreditGrade');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('BankCode');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('BankAccNo');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('AccName');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('JoinCompanyDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('StartWorkDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Position');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Salary');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('OccupationType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('OccupationCode');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('WorkType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('PluralityType');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('SmokeFlag');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ContPlanCode');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('Operator');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('MakeDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('MakeTime');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ModifyDate');
  } catch(ex) {}
  ;
  try {
    mSwitch.deleteVar('ModifyTime');
  } catch(ex) {}
  ;
}

/*********************************************************************
 *  将被保险人信息加入到缓存中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addInsuredVar() {
  try {
    mSwitch.addVar('ContNo','',fm.ContNo.value);
  } catch(ex) {}
  ;
  //alert("ContNo:"+fm.ContNo.value);
  try {
    mSwitch.addVar('InsuredNo','',fm.InsuredNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('PrtNo','',fm.PrtNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);
  } catch(ex) {}
  ;
  //   try{mSwitch.addVar('AppntNo','',fm.AppntNo.value);}catch(ex){};
  //   try{mSwitch.addVar('ManageCom','',fm.ManageCom.value);}catch(ex){};
  try {
    mSwitch.addVar('ExecuteCom','',fm.ExecuteCom.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('FamilyType','',fm.FamilyType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('RelationToMainInsure','',fm.RelationToMainInsure.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('RelationToAppnt','',fm.RelationToAppnt.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('AddressNo','',fm.AddressNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('SequenceNo','',fm.SequenceNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Name','',fm.Name.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Sex','',fm.Sex.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Birthday','',fm.Birthday.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('IDType','',fm.IDType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('IDNo','',fm.IDNo.value);
  } catch(ex) {}
  ;
  try {
	    mSwitch.addVar('InsuIDStartDate','',fm.InsuIDStartDate.value);
	  } catch(ex) {}
	  ;
  try {
		    mSwitch.addVar('InsuIDEndDate','',fm.InsuIDEndDate.value);
		  } catch(ex) {}
		  ;
  try {
    mSwitch.addVar('RgtAddress','',fm.RgtAddress.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Marriage','',fm.Marriage.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('MarriageDate','',fm.MarriageDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Health','',fm.Health.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Stature','',fm.Stature.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Avoirdupois','',fm.Avoirdupois.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Degree','',fm.Degree.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('CreditGrade','',fm.CreditGrade.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('BankCode','',fm.BankCode.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('AccName','',fm.AccName.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('JoinCompanyDate','',fm.JoinCompanyDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('StartWorkDate','',fm.StartWorkDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Position','',fm.Position.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Salary','',fm.Salary.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('OccupationType','',fm.OccupationType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('OccupationCode','',fm.OccupationCode.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('WorkType','',fm.WorkType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('PluralityType','',fm.PluralityType.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('SmokeFlag','',fm.SmokeFlag.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ContPlanCode','',fm.ContPlanCode.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('Operator','',fm.Operator.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('MakeDate','',fm.MakeDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('MakeTime','',fm.MakeTime.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ModifyDate','',fm.ModifyDate.value);
  } catch(ex) {}
  ;
  try {
    mSwitch.addVar('ModifyTime','',fm.ModifyTime.value);
  } catch(ex) {}
  ;
}

/*********************************************************************
 *  添加被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addRecord() {
  if(LoadFlag==1||LoadFlag==3||LoadFlag==5||LoadFlag==6) {
    var tPrtNo=fm.all("PrtNo").value;
    var sqlstr="select SequenceNo from LCInsured where PrtNo='"+tPrtNo+"'";
    arrResult=easyExecSql(sqlstr,1,0);
    if(arrResult!=null) {
      for(var sequencenocout=0; sequencenocout<arrResult.length;sequencenocout++ ) {
        if(fm.SequenceNo.value==arrResult[sequencenocout][0]) {
          alert("已经存在该客户内部号码");
          fm.SequenceNo.focus();
          return false;
        }
      }
    }
  }


  if(LoadFlag==1) {
    if(fm.SequenceNo.value==""||fm.SequenceNo.value>3) {
      alert("只能添加三个被保险人！");
      fm.SequenceNo.focus();
      return false;
    }
    if(fm.SequenceNo.value=="") {
      fm.SequenceNo.focus();
      alert("内部客户号码为空！");
      return false;
    }
    if(fm.Marriage.value=="") {
      fm.Marriage.focus();
      alert("请填写婚姻状况！");
      return false;
    }
    if(fm.RelationToMainInsured.value=="") {
      fm.RelationToMainInsured.focus();
      alert("请填写与第一被保险人关系！");
      return false;
    }
    if(fm.RelationToAppnt.value==""&&fm.SequenceNo.value=="1") {
      fm.RelationToAppnt.focus();
      alert("请填写与投保人关系！！");
      return false;
    }
    if ( fm.SamePersonFlag.checked==true&&fm.RelationToAppnt.value!="00")
    {
    	fm.RelationToAppnt.focus();
    	alert("投保人为被保险人本人时，与投保人关系矛盾，请检查！");
      return false;
    }
    if ( fm.SequenceNo.value=="1"&&fm.RelationToMainInsured.value!="00")
    {
    	fm.RelationToMainInsured.focus();
    	alert("客户内部号码与第一被保险人关系矛盾，请检查！");
      return false;
    }

  }
  //zxs 20190402
	var nameResult = checkValidateName(fm.IDType.value,fm.Name.value);
	if(nameResult!=""){
		alert("被保人"+nameResult);
		return false;
	}
	var relationResult = checkInsuredRelation(fm.SequenceNo.value,fm.RelationToAppnt.value,fm.RelationToMainInsured.value);
	if(relationResult!=""){
		alert(relationResult);
		return false;
	}
  if(!checkNative()){
      return false;
  }
//增加校验：增加保险人资料中证件信息校验   ranguo 20170821
  var tIDType=fm.IDType.value;
  var tIDNo=fm.IDNo.value;
  var tBirthday=fm.Birthday.value;
  var tSex=fm.Sex.value;
  var checkResult=checkIdNo(tIDType,tIDNo,tBirthday,tSex);
  if(checkResult!=null && checkResult!=""){
	  alert(checkResult);
	  return false;
  }
  if(!checksex())
    return false;
  if (fm.all('PolTypeFlag').value==0) {
    if( verifyInput2() == false )
      return false;
  }
  //zxs
  if(!checkInsuredPassIDNO()){
	  return false;  
  }  
  if(fm.all('IDType').value=="0") {
    var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
    if (strChkIdNo != "") {
      alert(strChkIdNo);
      return false;
    }
  }
  if(fm.all('IDType').value=="5") {
	    var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
	    if (strChkIdNo != "") {
	      alert(strChkIdNo);
	      return false;
	    }
	  }
  //if(!checkself()) return false;
  if(!checkrelation())
    return false;
  if(ImpartGrid.checkValue2(ImpartGrid.name,ImpartGrid)== false)
    return false;
  if(ImpartDetailGrid.checkValue2(ImpartDetailGrid.name,ImpartDetailGrid)== false)
    return false;

  //InputMuline1();  //保障状况告知;
  if (InputMuline1()==false)
  {
     return false;
  }
  //by gzh  校验被保人告知19条，当选“是”时，保额不能为空。
  if(!ImpartCheck119()){
  		return false;
  }
  
  ImpartGrid.delBlankLine();
  ImpartDetailGrid.delBlankLine();

//  if (fm.InsuredNo.value==''&&fm.AddressNo.value!='') {
//    alert("被保险人客户号为空，不能有地址号码");
//    return false;
//  }
  if(LoadFlag==9) {
    if(!checkifnoname())
      return false;
    if(!checkenough())
      return false;
  }
	//#1893 校验
   if(!checkAllInsu(2)){
      return false;
   }
   
  //getImpartInfo();
  fm.all('ContType').value=ContType;
  fm.all( 'BQFlag' ).value = BQFlag;
  fm.all('fmAction').value="INSERT||CONTINSURED";
  ChangeDecodeStr();
  fm.action="./ContInsuredSave.jsp?oldContNo="+oldContNo;
  fm.submit();
}

/*********************************************************************
 *  修改被选中的被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function modifyRecord() {
//增加校验：修改保险人资料中证件信息校验   ranguo 20170821
  var tIDType=fm.IDType.value;
  var tIDNo=fm.IDNo.value;
  var tBirthday=fm.Birthday.value;
  var tSex=fm.Sex.value;
  var checkResult=checkIdNo(tIDType,tIDNo,tBirthday,tSex);
  if(checkResult!=null && checkResult!=""){
    alert(checkResult);
    return false;
  }	
  //zxs 20190402
	var nameResult = checkValidateName(fm.IDType.value,fm.Name.value);
	if(nameResult!=""){
		alert("被保人"+nameResult);
		return false;
	}
	var relationResult = checkInsuredRelation(fm.SequenceNo.value,fm.RelationToAppnt.value,fm.RelationToMainInsured.value);
	if(relationResult!=""){
		alert(relationResult);
		return false;
	}
  if (fm.all('PolTypeFlag').value==0 && LoadFlag!=24) {
    if( verifyInput2() == false )
      return false;
  }
  //zxs
  if(!checkInsuredPassIDNO()){
	  return false;  
  }
  // if(!checkself()) return false;
  if(!checksex())
    return false;
  if (fm.Name.value=='') {
    alert("请选中需要修改的客户！")
    return false;
  }
  //alert("SelNo:"+InsuredGrid.getSelNo());
  if (InsuredGrid.mulLineCount==0) {
    alert("该被保险人还没有保存，无法进行修改！");
    return false;
  }
//  if (fm.InsuredNo.value==''&&fm.AddressNo.value!='') {
//    alert("被保险人客户号为空，不能有地址号码");
//    return false;
//  }
  var strSql = "select ContType from lccont where contno ='"+fm.ProposalContNo.value+"'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
  	if(arr[0][0]==1)
  	{
  		if(fm.SequenceNo.value == null || fm.SequenceNo.value == "")
  		{
  			alert("请选择是第几个被保险人!");
  			return false;
  		}
  		var insuredCount = InsuredGrid.mulLineCount;
  		var checkCount = fm.SequenceNo.value;
  		//认为如果mulline中的客户数大于等于SequenceNo 才可以修改，否则应该是保存
  		if(insuredCount<checkCount)
  		{
  			alert("请先保存客户信息才能修改！");
  			return false;
  		}
  	}
  }
  if(ImpartGrid.checkValue2(ImpartGrid.name,ImpartGrid)== false)
    return false;
  if(ImpartDetailGrid.checkValue2(ImpartDetailGrid.name,ImpartDetailGrid)== false)
    return false;

	initArray();
  //InputMuline1();  //保障状况告知;
  if (InputMuline1()==false)
  {
     return false;
  }
  //by gzh  校验被保人告知19条，当选“是”时，保额不能为空。
  if(!ImpartCheck119()){
  		return false;
  }
//增加身份证的校验
  if(BirthdaySexAppntIDNo()==false){
  	return false;
  }
  
  if(!checkNative()){
      return false;
  }
  
  	//#1893 校验
   if(!checkAllInsu(2)){
      return false;
   }

  ImpartGrid.delBlankLine();
  ImpartDetailGrid.delBlankLine();
  //getImpartInfo();
  fm.all('ContType').value=ContType;
  fm.all('fmAction').value="UPDATE||CONTINSURED";
  if(LoadFlag=="24"){
		var strSQL = "select 1 from lobgrpcont where PrtNo='"+fm.PrtNo.value+"'";
		var arr = easyExecSql(strSQL);
	  	if(!arr){
		  	if(checkCanModify()){
		  		fm.action="./ContInsuredSave.jsp?LoadFlag=2";
		  		ChangeDecodeStr();
		  		fm.submit();
				}
			}else{
				ChangeDecodeStr();
				fm.submit();
			}
	}else{
		ChangeDecodeStr();
	  fm.submit();
	}
}
/*********************************************************************
 *  删除被选中的被保险人
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteRecord() {
  if (fm.InsuredNo.value=='') {
    alert("请选中需要删除的客户！")
    return false;
  }
  if (InsuredGrid.mulLineCount==0) {
    alert("该被保险人还没有保存，无法进行删除！");
    return false;
  }
//  if (fm.InsuredNo.value==''&&fm.AddressNo.value!='') {
//    alert("被保险人客户号为空，不能有地址号码");
//    return false;
//  }
  if(!confirm("是否要删除此被保险人？删除请点击“确定”，否则点“取消”。"))
  {
  	return;
  }
  //getImpartInfo();
  fm.all('ContType').value=ContType;
  fm.all('fmAction').value="DELETE||CONTINSURED";
  fm.submit();
}
/*********************************************************************
 *  返回上一页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function returnparent() {
  var backstr=fm.all("ContNo").value;

  mSwitch.addVar("PolNo", "", backstr);
  mSwitch.updateVar("PolNo", "", backstr);
  var cVideoFlag = fm.VideoFlag.value;
  var cXSFlag = fm.XSFlag.value;
  try {
    mSwitch.deleteVar('ContNo');
  } catch(ex) {}
  ;
  if(LoadFlag=="1"||LoadFlag=="3") {
    //alert(fm.all("PrtNo").value);
    location.href="../app/ContInput.jsp?LoadFlag="+ LoadFlag + "&prtNo=" + fm.all("PrtNo").value+"&scantype="+scantype+"&ScanFlag="+ScanFlag+"&MissionID=" + MissionID+ "&SubMissionID=" + SubMissionID+"&ContNo="+ContNo+"&ActivityID="+mSwitch.getVar("ActivityID");
  }
  if(LoadFlag=="5"||LoadFlag=="25") {
    //alert(fm.all("PrtNo").value);
    location.href="../app/ContInput.jsp?LoadFlag="+ LoadFlag + "&prtNo=" + fm.all("PrtNo").value+"&scantype="+scantype+"&ContNo="+ContNo+"&ActivityID="+mSwitch.getVar("ActivityID")+"&VideoFlag="+cVideoFlag+"&XSFlag="+cXSFlag;
  }

  if(LoadFlag=="2") {
    location.href="../app/ContGrpInsuredInput.jsp?LoadFlag="+ LoadFlag + "&polNo=" + fm.all("GrpContNo").value+"&scantype="+scantype+"&ScanFlag="+ScanFlag+"&ContNo="+ContNo+"&ActivityID="+mSwitch.getVar("ActivityID");
  }
  else if (LoadFlag=="6") {
    location.href="ContInput.jsp?LoadFlag="+ LoadFlag + "&ContNo=" + backstr+"&prtNo="+fm.all("PrtNo").value+"&scantype="+scantype+"&ContNo="+ContNo+"&ActivityID="+mSwitch.getVar("ActivityID");
    return;
  } else if (LoadFlag=="7") {
    location.href="../bq/GEdorTypeNI.jsp?BQFlag="+BQFlag+"&ActivityID="+mSwitch.getVar("ActivityID");
    return;
  }
  //无名单
  else if (LoadFlag=="9") {
    location.href="../app/ContPolInput.jsp?ScanFlag="+ScanFlag+"&LoadFlag="+ LoadFlag + "&polNo=" + fm.all("GrpContNo").value+"&scantype="+scantype+"&oldContNo="+oldContNo+"&ContNo="+ContNo+"&ActivityID="+mSwitch.getVar("ActivityID");
    return;
  } else if(LoadFlag=="4"||LoadFlag=="16"||LoadFlag=="13"||LoadFlag=="14"||LoadFlag=="23") {
    if(Auditing=="1") {
      top.close();
    } else {
      mSwitch.deleteVar("PolNo");
      parent.fraInterface.window.location = "../app/ContGrpInsuredInput.jsp?LoadFlag="+LoadFlag+"&scantype="+scantype+"&ContNo="+ContNo+"&ActivityID="+mSwitch.getVar("ActivityID");
    }
  } else if (LoadFlag=="99") {
    location.href="ContPolInput.jsp?LoadFlag="+ LoadFlag+"&scantype="+scantype+"&ContNo="+ContNo+"&ActivityID="+mSwitch.getVar("ActivityID");
    return;
  }
  else if (LoadFlag=="24") {
    location.href="../briefapp/BriefLCInuredListInput.jsp?GrpContNo="+mSwitch.getVar("GrpContNo")+"&LoadFlag=2&ActivityID="+mSwitch.getVar("ActivityID");
    return;
  }
  /*    else
      {
          location.href="ContInput.jsp?LoadFlag="+ LoadFlag;
  	}  针对	各种情况的不同暂不支持else方式
  */
}
/*********************************************************************
 *  进入险种计划界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function grpRiskPlanInfo() {
  var newWindow = window.open("../app/GroupRiskPlan.jsp");
}
/*********************************************************************
 *  代码选择后触发时间
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) {

  if(cCodeName=="AddressNo") {
    getdetailaddress()
  }

  try {
    if(cCodeName == "Relation") {
      if(fm.all('RelationToAppnt').value=="00") {
        fm.all('SamePersonFlag').checked=true;
        displayissameperson();
        isSamePerson();
      }
    }
    //如果是无名单
    if( cCodeName == "PolTypeFlag") {
      DivGrpNoname.style.display='';
      if (Field.value!='0') {
        fm.all('InsuredPeoples').readOnly=false;
        fm.all('InsuredAppAge').readOnly=false;
      } else {
        fm.all('InsuredPeoples').readOnly=true;
        fm.all('InsuredAppAge').readOnly=true;
      }
      if(Field.value=='1') {
        FillNoName();
      }
    }
    if( cCodeName == "ImpartCode") {
    }
    if( cCodeName == "SequenceNo") {
      if(Field.value=="1"&&fm.SamePersonFlag.checked==false) {
        emptyInsured();
        param="121";
        fm.pagename.value="121";
        fm.InsuredSequencename.value="第一被保险人资料";
        fm.RelationToMainInsured.value="00";
      }
      if(Field.value=="2"&&fm.SamePersonFlag.checked==false) {
        if(InsuredGrid.mulLineCount==0) {
          alert("请先添加第一被保人");
          fm.SequenceNo.value="1";
          return false;
        }
        emptyInsured();
        noneedhome();
        param="122";
        fm.pagename.value="122";
        fm.InsuredSequencename.value="第二被保险人资料";
      }
      if(Field.value=="3"&&fm.SamePersonFlag.checked==false) {
        if(InsuredGrid.mulLineCount==0) {
          alert("请先添加第一被保人");
          Field.value="1";
          return false;
        }
        if(InsuredGrid.mulLineCount==1) {
          alert("请先添加第二被保人");
          Field.value="1";
          return false;
        }
        emptyInsured();
        noneedhome();
        param="123";
        fm.pagename.value="123";
        fm.InsuredSequencename.value="第三被保险人资料";
      }
      if (scantype== "scan") {
        setFocus();
      }
      initArray();
    }
    if( cCodeName == "CheckPostalAddress") {
    	var customerno=easyExecSql("select appntno,addressno from lcappnt where prtno='"+fm.all( 'PrtNo' ).value+"'");
        var PostalAddressSQL="select PostalAddress,PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity from lcaddress where customerno ='"+customerno[0][0]+"' and addressno='"+customerno[0][1]+"'";
    var sqlstr=easyExecSql(PostalAddressSQL);
    if(sqlstr){
			//alert();
    	if(fm.CheckPostalAddress.value=="1") {
    		PostalAddressID.style.display='';
    		insured_PostalAddressID.style.display='none';
    		fm.all('PostalAddress').value=sqlstr[0][0];
    		 var rs1=easyExecSql("select codename from ldcode1 where codetype='province1' and code='"+sqlstr[0][1]+"' and code1='0'");
    		 var rs2=easyExecSql("select codename from ldcode1 where codetype='city1' and code='"+sqlstr[0][2]+"' and code1='"+sqlstr[0][1]+"'");
    		 var rs3=easyExecSql("select codename from ldcode1 where codetype='county1' and code='"+sqlstr[0][3]+"' and code1='"+sqlstr[0][2]+"'");
    		 if(rs1 != null){
    			 fm.all('PostalProvince').value=rs1;
    			 fm.all('PostalCity').value=rs2;
    			 fm.all('PostalCounty').value=rs3;
				 fm.all('Province').value=sqlstr[0][1];
				 fm.all('City').value=sqlstr[0][2];
				 fm.all('County').value=sqlstr[0][3];
			}else{
				var rs4=easyExecSql("select code from ldcode1 where codetype='province1' and codename='"+sqlstr[0][1]+"' ");
				var rs5=easyExecSql("select code from ldcode1 where codetype='city1' and codename='"+sqlstr[0][2]+"' ");
				var rs6=easyExecSql("select code from ldcode1 where codetype='county1' and codename='"+sqlstr[0][3]+"'");
				fm.all('PostalProvince').value=sqlstr[0][1];
				fm.all('PostalCity').value=sqlstr[0][2];
				fm.all('PostalCounty').value=sqlstr[0][3];
				fm.all('Province').value=rs4;
				fm.all('City').value=rs5;
				fm.all('County').value=rs6;
    		 }
    		fm.all('PostalStreet').value=sqlstr[0][4];
    		fm.all('PostalCommunity').value=sqlstr[0][5];
    		fm.all('ZipCode').value="";
		}else if(fm.CheckPostalAddress.value=="2"){
    		PostalAddressID.style.display='none';
    		insured_PostalAddressID.style.display='';
    		fm.all('PostalAddress').value="";
    		fm.all('ZipCode').value="";    		
    		}
  		}else{
	    	fm.all('PostalAddress').value="";
	    	fm.all('ZipCode').value="";    
    	}  
    }
    if(cCodeName=="OccupationCode") {
      //alert();
      var t = Field.value;
      var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+t+"'";
      //alert(strSql);
      var arr = easyExecSql(strSql);
      if( arr ) {
        fm.OccupationType.value = arr[0][0];
      }
    }
    if(cCodeName=="AccountNo") {
    	if(Field.value=="1") {
		    if(mSwitch.getVar("AppntBankAccNo"))
		      fm.all('BankAccNo').value=mSwitch.getVar("AppntBankAccNo");
		    if(mSwitch.getVar("AppntBankCode"))
		      fm.all('BankCode').value=mSwitch.getVar("AppntBankCode");
		    if(mSwitch.getVar("AppntAccName"))
		      fm.all('AccName').value=mSwitch.getVar("AppntAccName");
		   }
	  }

	if(cCodeName=="NativePlace"){
	   if(Field.value == "OS"){
	      fm.all("NativePlace1").style.display = "";
	      fm.all("NativeCity1").style.display = "";
	      fm.all("NativeCityTitle").innerHTML="国家";
	   }else if(Field.value == "HK"){
	      fm.all("NativePlace1").style.display = "";
	      fm.all("NativeCity1").style.display = "";
	      fm.all("NativeCityTitle").innerHTML="地区";
	   }else{
	      fm.all("NativePlace1").style.display = "none";
	      fm.all("NativeCity1").style.display = "none";
	   }
	   fm.NativeCity.value = ""; 
	   fm.NativeCityName.value = "";
	}
	//zxs
	if(cCodeName=="IDType1"){
		   if(Field.value == "a"||Field.value == "b"){
			   	fm.all("PassNumer1").style.display = "";
				fm.all("PassIDNo1").style.display = "";
		   }else{
			   fm.all("PassNumer1").style.display = "none";
			   fm.all("PassIDNo1").style.display = "none";
		   }
		  // fm.NativeCity.value = ""; 
		   //fm.NativeCityName.value = "";
		}
  } catch(ex) {}

}
/*********************************************************************
 *  显示家庭单下被保险人的信息
 *  返回值：  无
 *********************************************************************
 */
function getInsuredInfo() {
  var ContNo=fm.all("ContNo").value;
  //alert();
  if(ContNo!=null&&ContNo!="") {
    var strSQL ="select InsuredNo,Name,Sex,Birthday,RelationToMainInsured,SequenceNo from LCInsured  where ContNo='"+ContNo+"' "
     						;
    	turnPage.queryModal(strSQL,InsuredGrid);					
        
    if(InsuredGrid.mulLineCount > 0)
    {
        InsuredGrid.radioBoxSel(1);
        getInsuredDetail();
    }

  }
}

/*********************************************************************
 *  获得个单合同的被保人信息
 *  返回值：  无
 *********************************************************************
 */
function getProposalInsuredInfo() {
  var tContNo=fm.all("ContNo").value;
  Resource=fm.Resource.value;
  if(Resource==2){
  var StrSQL="select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate,'','',Authorization,OriginalIDNo  from LCInsured where ContNo='"+tContNo+"' "                                              
 						+" union "
						+"select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate,'','',Authorization,OriginalIDNo  from LobInsured where ContNo='"+tContNo+"' "                                               						
 	;
 	}else if(Resource==1){
  var StrSQL="select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate,'','',Authorization,OriginalIDNo  from LbInsured where ContNo='"+tContNo+"' "                                              
 	;
 	}else if(Resource==3){
  var StrSQL="select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate,'','',Authorization,OriginalIDNo  from LcInsured where ContNo='"+tContNo+"' "                                              
 	;
 	}else{
  var StrSQL="select GrpContNo,ContNo,InsuredNo,PrtNo,AppntNo,ManageCom,ExecuteCom,FamilyID,RelationToMainInsured,RelationToAppnt,AddressNo,SequenceNo "             
						+",Name,Sex,Birthday,IDType,IDNo,NativePlace,Nationality,RgtAddress,Marriage,MarriageDate,Health,Stature,Avoirdupois,Degree,CreditGrade, "             
						+"BankCode,BankAccNo,AccName,JoinCompanyDate,StartWorkDate,Position,Salary,OccupationType,OccupationCode,WorkType,PluralityType "                      
						+",SmokeFlag,ContPlanCode,Operator,InsuredStat,MakeDate,MakeTime,ModifyDate,ModifyTime,UWFlag,UWCode,UWDate,UWTime,BMI,InsuredPeoples,ContPlanCount, " 
						+"DiskImportNo,OthIDType,OthIDNo,EnglishName,GrpInsuredPhone,IDStartDate,IDEndDate ,'','',Authorization,OriginalIDNo from LcInsured where ContNo='"+tContNo+"' "                                              
 	; 	
 	}				
  arrResult=easyExecSql(StrSQL,1,0);

	
  if(arrResult==null) {
    //alert("未得到被投保人信息");
    return;
  } else {
    DisplayInsured();//该合同下的被投保人信息
    var tCustomerNo = arrResult[0][2];		// 得到投保人客户号
    var tAddressNo = arrResult[0][10]; 		// 得到投保人地址号
    arrResult=easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo='"+tCustomerNo+"'",1,0);
    if(arrResult==null) {
      //alert("未得到用户信息");
      //return;
    } else {
      //displayAppnt();       //显示投保人详细内容
      emptyUndefined();
      fm.AddressNo.value=tAddressNo;
      getdetailaddress();//显示投保人地址详细内容
    }
  }
  getInsuredPolInfo();
  getImpartInfo();
  getImpartDetailInfo();
}

/*********************************************************************
 *  MulLine的RadioBox点击事件，获得被保人详细信息，填入被保人信息表
 *  返回值：  无
 *********************************************************************
 */
function getInsuredDetail(parm1,parm2) {
    var oneRowResult = null;
    if(parm1 == null || parm1 == "" || parm1 == "undefined")
    {
        var tSelNo = InsuredGrid.getSelNo() - 1;

        if("-1" == tSelNo)
        {
            alert("未有选择的被保人信息！");
            return false;
        }
        
        oneRowResult = InsuredGrid.getRowData(tSelNo);
    }
    
    if(oneRowResult != null)
    {
        fm.SequenceNo.value = oneRowResult[5];
    }
    else
    {
        fm.SequenceNo.value=fm.all(parm1).all('InsuredGrid6').value;
    }
    
  if(fm.SequenceNo.value=="1") {
    fm.pagename.value="121";
    fm.InsuredSequencename.value="第一被保险人资料";
  }
  if(fm.SequenceNo.value=="2") {
    fm.pagename.value="122";
    fm.InsuredSequencename.value="第二被保险人资料";
  }
  if(fm.SequenceNo.value=="3") {
    fm.pagename.value="123";
    fm.InsuredSequencename.value="第三被保险人资料";
  }
  if (scantype== "scan") {
    setFocus();
  }
  
	var InsuredNo = null;
	if(oneRowResult != null)
    {
        InsuredNo = oneRowResult[0];
    }
    else
    {
        InsuredNo = fm.all(parm1).all('InsuredGrid1').value;
    }
    
  var ContNo = fm.ContNo.value;
  //被保人详细信息
  var strSQL ="select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo='"+InsuredNo+"'";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null) {
    displayAppnt();
  }
  strSQL ="select * from LCInsured where ContNo = '"+ContNo+"' and InsuredNo='"+InsuredNo+"'";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null) {
    DisplayInsured();
  }
  var tAddressNo = arrResult[0][10]; 		// 得到被保人地址号
  fm.AddressNo.value=tAddressNo;
  getdetailaddress();

  getInsuredPolInfo();

  getImpartInfo();

  getImpartDetailInfo();

  initArray();
  
//  if(LoadFlag=="5"){
//    var strSQL1="select 1 from lcapprove where prtno='"+prtNo+"' and customerno='"+InsuredNo+"' ";
//    arrResult=easyExecSql(strSQL1);
//    if(arrResult==null){
//       fm.Name.value="";
//       fm.Birthday.value="";
//       fm.Sex.value="";
//       fm.SexName.value="";
//       fm.IDType.value="";
//       fm.IDTypeName.value="";
//       fm.IDNo.value="";
//       fm.RelationToAppnt.value="";
//    }
//  }

}
/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayCustomer() {
  try {
    fm.all('Nationality').value= arrResult[0][8];
  } catch(ex) {}
  ;

}
/*********************************************************************
 *  把查询返回的客户地址数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayAddress() {
  try {
    fm.all('PostalAddress').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= arrResult[0][14];
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= arrResult[0][16];
  } catch(ex) {}
  ;
  try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){};
  try {
    fm.all('GrpPhone').value= arrResult[0][12];
  } catch(ex) {}
  ;
  try {
    fm.all('CompanyAddress').value= arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][11];
  } catch(ex) {}
  ;
}
/*********************************************************************
 *  显示被保人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DisplayInsured() {
  try {
    fm.all('GrpContNo').value=arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('ContNo').value=arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('InsuredNo').value=arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('PrtNo').value=arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('AppntNo').value=arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('ManageCom').value=arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('ExecuteCom').value=arrResult[0][6];
  } catch(ex) {}
  ;
  try {
    fm.all('FamilyID').value=arrResult[0][7];
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToMainInsured').value=arrResult[0][8];
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToAppnt').value=arrResult[0][9];
  } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value=arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('SequenceNo').value=arrResult[0][11];
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value=arrResult[0][12];
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value=arrResult[0][13];fm.all('SexName').value=easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'sex' and Code='"+arrResult[0][13]+"'");
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value=arrResult[0][14];
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value=arrResult[0][15];
  } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value=arrResult[0][16];
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value=arrResult[0][17];
    var arr = easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'nativeplace' and Code='"+arrResult[0][17]+"'");
    if(arr)
    fm.all('NativePlaceName').value=arr;
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value=arrResult[0][18];
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value=arrResult[0][19];
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value=arrResult[0][20];fm.all('MarriageName').value=easyExecSql("select  CodeName from ldcode where 1 = 1 and codetype = 'marriage' and Code='"+arrResult[0][20]+"'");
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value=arrResult[0][21];
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value=arrResult[0][22];
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value=arrResult[0][23];
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value=arrResult[0][24];
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value=arrResult[0][25];
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value=arrResult[0][26];
  } catch(ex) {}
  ;
  try {
    fm.all('BankCode').value=arrResult[0][27];
  } catch(ex) {}
  ;
  try {
    fm.all('BankAccNo').value=arrResult[0][28];
  } catch(ex) {}
  ;
  try {
    fm.all('AccName').value=arrResult[0][29];
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value=arrResult[0][30];
  } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value=arrResult[0][31];
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value=arrResult[0][32];
  } catch(ex) {}
  ;
  try {
  if(arrResult[0][33]=='-1'){
	try{fm.all('Salary').value=""; }catch(ex){};               
	}else{
	try{fm.all('Salary').value= arrResult[0][33]; }catch(ex){};
	}
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value=arrResult[0][34];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value=arrResult[0][35];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][35]);
  } catch(ex) { }
  ;
  try {
    fm.all('WorkType').value=arrResult[0][36];
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value=arrResult[0][37];
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value=arrResult[0][38];
  } catch(ex) {}
  ;
  try {
    fm.all('ContPlanCode').value=arrResult[0][39];
  } catch(ex) {}
  ;
  try {
    fm.all('Operator').value=arrResult[0][40];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value=arrResult[0][41];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value=arrResult[0][42];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value=arrResult[0][43];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value=arrResult[0][44];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value=arrResult[0][44];
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDNo').value=arrResult[0][55];
  } catch(ex) {}
  ;
  try {
    fm.all('EnglishName').value=arrResult[0][56];
  } catch(ex) {}
  ;
  try {
	    fm.all('InsuIDStartDate').value=arrResult[0][58];
	  } catch(ex) {}
	  ;
  try {
		    fm.all('InsuIDEndDate').value=arrResult[0][59];
		  } catch(ex) {}
		  ;
  try {
    fm.all('NativeCity').value=arrResult[0][60];
    setCodeName("NativeCityName", "nativecity", arrResult[0][60]);
  } catch(ex) {}
  ;	
	//modify by zxs   
  try {
	    fm.all('InsuredAuth').value=arrResult[0][62];
	  } catch(ex) {}
	  ;	
  var s =  easyExecSql("select 1 from lwmission where missionprop1 = '"+fm.prtn2.value+"' and (activityid='0000001098' or activityid='0000001099')");
		if(s =='1'){
				fm.all('InsuredAuth').value = '1';
			}
		//modify by zxs    2019-01-16
   try {
	    fm.all('InsuredPassIDNo').value=arrResult[0][63];
	} catch(ex) {}
     ;	
  showAllCodeName();
  controlNativeCity("");
}
function displayissameperson() {
  try {
    fm.all('InsuredNo').value= mSwitch.getVar( "AppntNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value= mSwitch.getVar( "AppntName" );
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value= mSwitch.getVar( "AppntSex" );
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value= mSwitch.getVar( "AppntBirthday" );
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value= mSwitch.getVar( "AppntIDType" );
  } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value= mSwitch.getVar( "AppntIDNo" );
  } catch(ex) {}
  ;
  try {
	    fm.all('InsuIDStartDate').value= mSwitch.getVar( "AppIDStartDate" );
	  } catch(ex) {}
	  ;
	  try {
	    fm.all('InsuIDEndDate').value= mSwitch.getVar( "AppIDEndDate" );
	  } catch(ex) {}
	  ;
  try {
    fm.all('Password').value= mSwitch.getVar( "AppntPassword" );
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value= mSwitch.getVar( "AppntNativePlace" );
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value= mSwitch.getVar( "AppntNationality" );
  } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value= mSwitch.getVar( "AddressNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value= mSwitch.getVar( "AppntRgtAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value= mSwitch.getVar( "AppntMarriage" );
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value= mSwitch.getVar( "AppntMarriageDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value= mSwitch.getVar( "AppntHealth" );
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value= mSwitch.getVar( "AppntStature" );
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value= mSwitch.getVar( "AppntAvoirdupois" );
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value= mSwitch.getVar( "AppntDegree" );
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value= mSwitch.getVar( "AppntDegreeCreditGrade" );
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDType').value= mSwitch.getVar( "AppntOthIDType" );
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDNo').value= mSwitch.getVar( "AppntOthIDNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('ICNo').value= mSwitch.getVar( "AppntICNo" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpNo').value= mSwitch.getVar( "AppntGrpNo" );
  } catch(ex) {}
  ;
  try {
    fm.all( 'JoinCompanyDate' ).value = mSwitch.getVar( "JoinCompanyDate" );
    if(fm.all( 'JoinCompanyDate' ).value=="false") {
      fm.all( 'JoinCompanyDate' ).value="";
    }
  } catch(ex) { }
  ;
  try {
    fm.all('StartWorkDate').value= mSwitch.getVar( "AppntStartWorkDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= mSwitch.getVar( "AppntPosition" );
  } catch(ex) {}
  ;
  try {
    fm.all('Salary').value= mSwitch.getVar( "AppntSalary" );
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= mSwitch.getVar( "AppntOccupationType" );
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value= mSwitch.getVar( "AppntOccupationCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('WorkType').value= mSwitch.getVar( "AppntWorkType" );
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value= mSwitch.getVar( "AppntPluralityType" );
  } catch(ex) {}
  ;
  try {
    fm.all('DeathDate').value= mSwitch.getVar( "AppntDeathDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value= mSwitch.getVar( "AppntSmokeFlag" );
  } catch(ex) {}
  ;
  try {
    fm.all('BlacklistFlag').value= mSwitch.getVar( "AppntBlacklistFlag" );
  } catch(ex) {}
  ;
  try {
    fm.all('Proterty').value= mSwitch.getVar( "AppntProterty" );
  } catch(ex) {}
  ;
  try {
    fm.all('Remark').value= mSwitch.getVar( "AppntRemark" );
  } catch(ex) {}
  ;
  try {
    fm.all('State').value= mSwitch.getVar( "AppntState" );
  } catch(ex) {}
  ;
  try {
    fm.all('Operator').value= mSwitch.getVar( "AppntOperator" );
  } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value= mSwitch.getVar( "AppntMakeDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value= mSwitch.getVar( "AppntMakeTime" );
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value= mSwitch.getVar( "AppntModifyDate" );
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value= mSwitch.getVar( "AppntModifyTime" );
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value= mSwitch.getVar( "AppntPostalAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value= mSwitch.getVar( "AppntPostalAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= mSwitch.getVar( "AppntZipCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= mSwitch.getVar( "AppntPhone" );
  } catch(ex) {}
  ;
  try {
    fm.all('Fax').value= mSwitch.getVar( "AppntFax" );
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= mSwitch.getVar( "AppntMobile" );
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= mSwitch.getVar( "AppntEMail" );
  } catch(ex) {}
  ;
  try {
  fm.all('GrpName').value= mSwitch.getVar( "AppntGrpName" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpPhone').value= mSwitch.getVar( "AppntGrpPhone" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value= mSwitch.getVar( "CompanyAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= mSwitch.getVar( "AppntGrpZipCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('GrpFax').value= mSwitch.getVar( "AppntGrpFax" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= mSwitch.getVar( "AppntHomeAddress" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomePhone').value= mSwitch.getVar( "AppntHomePhone" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= mSwitch.getVar( "AppntHomeZipCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeFax').value= mSwitch.getVar( "AppntHomeFax" );
  } catch(ex) {}
  ;
  try {fm.all('PostalProvince').value= mSwitch.getVar( "appnt_PostalProvince" );} catch(ex) {} ;
  try {fm.all('PostalCity').value= mSwitch.getVar( "appnt_PostalCity" );} catch(ex) {};
  try {fm.all('PostalCounty').value= mSwitch.getVar( "appnt_PostalCounty" );} catch(ex) {};
  try {fm.all('Province').value= mSwitch.getVar( "Province" );} catch(ex) {} ;
  try {fm.all('City').value= mSwitch.getVar( "City" );} catch(ex) {};
  try {fm.all('County').value= mSwitch.getVar( "County" );} catch(ex) {};
  
  try {
    fm.all('PostalStreet').value= mSwitch.getVar( "appnt_PostalStreet" );
  } catch(ex) {}
  ;
  try {
    fm.all('PostalCommunity').value= mSwitch.getVar( "appnt_PostalCommunity" );
  } catch(ex) {}
  ; 
  try {
    fm.all('HomeCode').value= mSwitch.getVar( "AppntHomeCode" );
  } catch(ex) {}
  ;
  try {
    fm.all('HomeNumber').value= mSwitch.getVar( "AppntHomeNumber" );
  } catch(ex) {}
  ; 
  try {
    fm.all('NativeCity').value= mSwitch.getVar( "AppntNativeCity" );
    setCodeName("NativeCityName", "nativecity", fm.NativeCity.value);
  } catch(ex) {}
  ;
  //modify by zxs 
   try {
	  //	alert(mSwitch.getVar( "AppntAuth" ));
	    fm.all('InsuredAuth').value= mSwitch.getVar( "AppntAuth" );
	    
	  } catch(ex) {}
	  ;
	  try {
		  //	alert(mSwitch.getVar( "AppntAuth" ));
		    fm.all('InsuredPassIDNo').value= mSwitch.getVar( "AppntPassIDNo" );
		    
		  } catch(ex) {}
		  ;
  showAllCodeName();
  controlNativeCity("");
}
/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartInfo() {
  initImpartGrid();
  var InsuredNo=fm.all("InsuredNo").value;
  var ContNo=fm.all("ContNo").value;
  //告知信息初始化
  if(InsuredNo!=null&&InsuredNo!="") {
    var strSQL ="select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LCCustomerImpart where CustomerNo='"+InsuredNo+"' and ProposalContNo='"+fm.ProposalContNo.value+"' and CustomerNoType='I'";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      clearImpart();
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = ImpartGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
  clearImpart();    //清空
  getImpartbox();   //保障状况告知※健康告知
}
/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartDetailInfo() {
  initImpartDetailGrid();
  var InsuredNo=fm.all("InsuredNo").value;
  var ContNo=fm.all("ContNo").value;
  //告知信息初始化
  if(InsuredNo!=null&&InsuredNo!="") {
    var strSQL ="select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where CustomerNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and CustomerNoType='I'";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = ImpartDetailGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
}
/*********************************************************************
 *  获得被保人险种信息，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getInsuredPolInfo() {
  initPolGrid();
  var InsuredNo=fm.all("InsuredNo").value;
  var ContNo=fm.all("ContNo").value;
  //险种信息初始化
  if(InsuredNo!=null&&InsuredNo!="") {
    var strSQL ="select PolNo,RiskCode,Prem,Amnt,SupplementaryPrem from LCPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
    					 +" union "
    					 +"select PolNo,RiskCode,Prem,Amnt,SupplementaryPrem from LobPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
    					 +" union "
    					 +"select PolNo,RiskCode,Prem,Amnt,SupplementaryPrem from LbPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
    					 ;
    if(ContType==1){
    	strSQL ="select PolNo,RiskCode,Prem,Amnt,SupplementaryPrem from LCPol where InsuredNo='"+InsuredNo+"' and ContNo='"+ContNo+"'"
    					 ;
    	}
    //alert(strSQL);
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
}
/*********************************************************************
 *  MulLine的RadioBox点击事件，获得被保人险种详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolDetail(parm1,parm2) {
  var PolNo=fm.all(parm1).all('PolGrid1').value
            try {
              mSwitch.deleteVar('PolNo')
            } catch(ex) {}
            ;
  try {
    mSwitch.addVar('PolNo','',PolNo);
  } catch(ex) {}
  ;
  fm.SelPolNo.value=PolNo;
}
/*********************************************************************
 *  根据家庭单类型，隐藏界面控件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function choosetype() {
  if(fm.FamilyType.value=="1")
    divTempFeeInput.style.display="";
  if(fm.FamilyType.value=="0")
    divTempFeeInput.style.display="none";
}
/*********************************************************************
 *  校验被保险人与主被保险人关系
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function checkself() {
  if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value=="") {
    fm.RelationToMainInsured.value="00";
    return true;
  } else if(fm.FamilyType.value=="0"&&fm.RelationToMainInsured.value!="00") {
    alert("个人单中'与第一被保险人关系'只能是'本人'");
    fm.RelationToMainInsured.value="00";
    return false;
  } else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value==""&&InsuredGrid.mulLineCount==0) {
    fm.RelationToMainInsured.value="00";
    return true;
  } else if(fm.FamilyType.value=="1"&&fm.RelationToMainInsured.value!="00"&&InsuredGrid.mulLineCount==0) {
    alert("家庭单中第一位被保险人的'与第一被保险人关系'只能是'本人'");
    fm.RelationToMainInsured.value="00";
    return false;
  } else
    return true;
}
/*********************************************************************
 *  校验保险人
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function checkrelation() {
  if(LoadFlag==2||LoadFlag==7) {
    if (fm.all('ContNo').value != "") {
      alert("团单的个单不能有多被保险人");
      return false;
    } else
      return true;
  } else {
    if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="0") {
      var strSQL="select * from LCInsured where contno='"+fm.all('ContNo').value +"'";
      turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
      if(turnPage.strQueryResult) {
        alert("个单不能有多被保险人");
        return false;
      } else
        return true;
    }
    //else if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="1"&&InsuredGrid.mulLineCount>0&&fm.RelationToMainInsured.value=="00")
    //{
    //    alert("家庭单只能有一个第一被保险人");
    //    return false;
    //}
    else if (fm.all('ContNo').value != ""&&fm.FamilyType.value=="1"&&fm.RelationToAppnt.value=="00") {
      var strSql="select * from LCInsured where contno='"+fm.all('ContNo').value +"' and RelationToAppnt='00' ";
      turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);
      if(turnPage.strQueryResult) {
        alert("投保人已经是该保单下的被保险人");
        return false;
      } else
        return true;
    } else
      return true;
  }
  //select count(*) from ldinsured

}
/*********************************************************************
 *  投保人与被保人相同选择框事件
 *  参数  ：  无
 *  返回值：  true or false
 *********************************************************************
 */
function isSamePerson() {
  //对应未选同一人，又打钩的情况
  if ( fm.SamePersonFlag.checked==true&&fm.RelationToAppnt.value!="00") {
    fm.all('DivLCInsured').style.display = "none";
    divLCInsuredPerson.style.display = "none";
    divSalary.style.display = "none";
    fm.SamePersonFlag.checked = true;
    fm.RelationToAppnt.value="00"
    displayissameperson();
  }
  //对应是同一人，又打钩的情况
  else if (fm.SamePersonFlag.checked == true) {
    fm.all('DivLCInsured').style.display = "none";
    divLCInsuredPerson.style.display = "none";
    divSalary.style.display = "none";
    displayissameperson();
  }
  //对应不选同一人的情况
  else if (fm.SamePersonFlag.checked == false) {
    fm.all('DivLCInsured').style.display = "";
    emptyInsured();
//    try {
//      fm.all('Name').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Sex').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Birthday').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('IDType').value= "0";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('IDNo').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Password').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('NativePlace').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Nationality').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('RgtAddress').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Marriage').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('MarriageDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Health').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Stature').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Avoirdupois').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Degree').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('CreditGrade').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OthIDType').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OthIDNo').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('ICNo').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpNo').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('JoinCompanyDate').value= "";
//    } catch(ex) {}
//    try {
//      fm.all('StartWorkDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Position').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Salary').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OccupationType').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OccupationCode').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('OccupationName').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('WorkType').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('PluralityType').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('DeathDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('SmokeFlag').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('BlacklistFlag').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Proterty').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Remark').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('State').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Operator').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('MakeDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('MakeTime').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('ModifyDate').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('ModifyTime').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('PostalAddress').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('PostalAddress').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('ZipCode').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Phone').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Mobile').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('EMail').value="";
//    } catch(ex) {}
//    ;
//    try {
//   fm.all('GrpName').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpPhone').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpAddress').value="";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpZipCode').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('HomeAddress').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('HomeZipCode').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('HomePhone').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('HomeFax').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('GrpFax').value= "";
//    } catch(ex) {}
//    ;
//    try {
//      fm.all('Fax').value= "";
//    } catch(ex) {}
//    ;

  }
}
/*********************************************************************
 *  投保人客户号查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryInsuredNo() {
  if (fm.all("InsuredNo").value == "") {
    showAppnt1();
  } else {
    arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + fm.all("InsuredNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到投保人信息");
      displayAppnt(new Array());
      emptyUndefined();
    } else {
      displayAppnt(arrResult[0]);
      getaddresscodedata();
    }
  }
}
/*********************************************************************
 *  投保人查询按扭事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt1() {
  if( mOperate == 0 ) {
    mOperate = 2;
    showInfo = window.open( "../sys/LDPersonQueryNew.html" );
  }
}
/*********************************************************************
 *  显示投保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt() {
  try {
    fm.all('InsuredNo').value= arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('Name').value= arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('IDTypeName').value= getNameFromLDCode("IDType",arrResult[0][4]);
  } catch(ex) { }
  ;
  try {
    fm.all('IDNo').value= arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('Password').value= arrResult[0][6];
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value= arrResult[0][7];
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value= arrResult[0][8];
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value= arrResult[0][9];
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value= arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value= arrResult[0][11];
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value= arrResult[0][12];
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value= arrResult[0][13];
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value= arrResult[0][14];
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value= arrResult[0][15];
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value= arrResult[0][16];
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDType').value= arrResult[0][17];
  } catch(ex) {}
  ;
  try {
    fm.all('OthIDNo').value= arrResult[0][18];
  } catch(ex) {}
  ;
  try {
    fm.all('ICNo').value= arrResult[0][19];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpNo').value= arrResult[0][20];
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value= arrResult[0][21];
  } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value= arrResult[0][22];
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= arrResult[0][23];
  } catch(ex) {}
  ;
  try {
	if(arrResult[0][24]=='-1'){
	try{fm.all('Salary').value=""; }catch(ex){};               
	}else{
	try{fm.all('Salary').value= arrResult[0][24]; }catch(ex){};
	}
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= arrResult[0][25];
  } catch(ex) {}
  ;
  //alert( arrResult[0][25]);
  try {
    fm.all('OccupationCode').value= arrResult[0][26];
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][26]);
  } catch(ex) { }
  ;
  try {
    fm.all('WorkType').value= arrResult[0][27];
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value= arrResult[0][28];
  } catch(ex) {}
  ;
  try {
    fm.all('DeathDate').value= arrResult[0][29];
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value= arrResult[0][30];
  } catch(ex) {}
  ;
  try {
    fm.all('BlacklistFlag').value= arrResult[0][31];
  } catch(ex) {}
  ;
  try {
    fm.all('Proterty').value= arrResult[0][32];
  } catch(ex) {}
  ;
  try {
    fm.all('Remark').value= arrResult[0][33];
  } catch(ex) {}
  ;
  try {
    fm.all('State').value= arrResult[0][34];
  } catch(ex) {}
  ;
  try {
    fm.all('Operator').value= arrResult[0][35];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeDate').value= arrResult[0][36];
  } catch(ex) {}
  ;
  try {
    fm.all('MakeTime').value= arrResult[0][37];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyDate').value= arrResult[0][38];
  } catch(ex) {}
  ;
  try {
    fm.all('ModifyTime').value= arrResult[0][39];
  } catch(ex) {}
  ;
  try {
 fm.all('GrpName').value= arrResult[0][41];
  } catch(ex) {}
  ;
  //地址显示部分的变动
  try {
    fm.all('AddressNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value=  "";
  } catch(ex) {}
  ;
 //try{fm.all('GrpName').value= arrResult[0][46];}catch(ex){};
  try {
    fm.all('GrpPhone').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomePhone').value=  "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativeCity').value= arrResult[0][47];
  } catch(ex) {}
  ;
  showAllCodeName();
  controlNativeCity("");
}
/*********************************************************************
 *  查询返回后触发
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {

  //alert("here:" + arrQueryResult + "\n" + mOperate);
  if( arrQueryResult != null ) {
    arrResult = arrQueryResult;

    if( mOperate == 1 ) {		// 查询投保单
      fm.all( 'ContNo' ).value = arrQueryResult[0][0];

      arrResult = easyExecSql("select ProposalContNo,PrtNo,ManageCom,SaleChnl,AgentCode,AgentGroup,AgentCode1,AgentCom,AgentType,Remark from LCCont where LCCont = '" + arrQueryResult[0][0] + "'", 1, 0);

      if (arrResult == null) {
        alert("未查到投保单信息");
      } else {
        displayLCContPol(arrResult[0]);
      }
    }

    if( mOperate == 2 ) {		// 投保人信息
      //arrResult = easyExecSql("select a.*,b.AddressNo,b.PostalAddress,b.ZipCode,b.HomePhone,b.Mobile,b.EMail,a.GrpNo,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode from LDPerson a,LCAddress b where 1=1 and a.CustomerNo=b.CustomerNo and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      arrResult = easyExecSql("select a.*,(select GrpName from LDGrp where CustomerNo=(select GrpNo from LDPerson where CustomerNo=a.CustomerNo) ) from LDPerson a where 1=1  and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
      if (arrResult == null) {
        alert("未查到投保人信息");
      } else {
        displayAppnt(arrResult[0]);
        getaddresscodedata();
      }
    }
  }
  mOperate = 0;		// 恢复初态
}
/*********************************************************************
 *  查询职业类别
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getdetailwork() {
  var strSql = "select OccupationType from LDOccupation where OccupationCode='" + fm.OccupationCode.value+"'";
  var arrResult = easyExecSql(strSql);
  //alert(arrResult[0][0]);
  if (arrResult != null) {
    //alert(fm.OccupationType.value);
    fm.OccupationType.value = arrResult[0][0];
  }
}
/*获得个人单信息，写入页面控件
function getProposalInsuredInfo(){
  var ContNo = fm.ContNo.value;
  //被保人详细信息
  var strSQL ="select * from ldperson where CustomerNo in (select InsuredNo from LCInsured where ContNo='"+ContNo+"')";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null){
  	DisplayCustomer();
  }

  strSQL ="select * from LCInsured where ContNo = '"+ContNo+"'";
  arrResult=easyExecSql(strSQL);
  if(arrResult!=null){
    DisplayInsured();
  }else{
    return;
  }

  var tAddressNo = arrResult[0][10]; 		// 得到被保人地址号
  var InsuredNo=arrResult[0][2];
  var strSQL="select * from LCAddress where AddressNo='"+tAddressNo+"' and CustomerNo='"+InsuredNo+"'";
  arrResult=easyExecSql(strSQL);
    if(arrResult!=null){
  	DisplayAddress();
    }

    getInsuredPolInfo();

}*/


/*********************************************************************
 *  把合同所有信息录入结束确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function inputConfirm(wFlag) {
	
	if(!checkSalechnl_CrsBuss()){
		return false;
	}
	
	
	if(!checkBonusGetMode()){
		return false;
	}
	
	if(!checkAllInsu(1)){
		return false;
	}
	
	
    if(!checkMixCom()){
        return false;
    }
     
    if(!checkSaleChnlInfo()){
    	return false;
    }
    
    if(!checkSaleChnl())
    {
        return false;
    }
    //险种校验
    if(!checkRiskInfo())
    {
        return false;
    }
    
    //校验集团交叉业务销售渠道
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    //银保渠道与险种校验
//    if(!checkSaleChnlRisk())
//    {
//        return false;
//    }
    //校验小于18周岁的被保人是否选择了第19项告知
    if(!checkGZ19()){
    	return false;
    }
    //当19项告知选择“是”时，19项告知保额内容不能为空，且必须是数字
	if(!checkGZ19Prem()){
		return false;
	}         
    
    //保单保险期间的校验
    if(!checkInsuYear())
    {
    	return false;
    }
    
  //zxs 20190424
	if(wFlag==2){
		if (!checkNameRule()) {
			return false;
		}
	}
	
	//zxs 20190521
	if(!checkAppDate()){
		return false;
	}
    
    // 豁免险保额校验 张成轩
    var s = "select 1 from lcpol where prtno = '"+fm.PrtNo.value+"' and riskcode='232901'";
	var result = easyExecSql(s);
	if (result && result[0][0] == "1") {
	}else{
		if(!checkExemption())
	    {
	    	return false;
	    }
	}
    //校验缴费方式
    if(!valiMedical()){
    	return false;
    }
    
    //校验主附险之间的关系是否正确
	if(!checkMainSubRiskRela()){
		return false;
	}
	
	//校验121501的主附险各种问题
	if(!check121501()){
		return false;
	}
	
	//校验险种332301的保险期间等
    if(!check332301()){
        return false;
    } 
    
    if(!checkSYAll()){
        return false;
    }    
    
    //校验税优产品告知信息
    if(!checktaxCustomerImpart()){
        return false;
    }
    //校验税务登记证号和社会信用代码长度
    if(!checktaxnoAndcreditcode()){
        return false;
    }
	
    if (wFlag ==1 ) //录入完毕确认
    {
        var tStr= "	select * from lwmission where 1=1 and lwmission.missionprop1 = '"+fm.ContNo.value+"'";
        turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
        if (turnPage.strQueryResult) {
            alert("该合同已经做过保存！");
            return;
        }
        fm.AppntNo.value = AppntNo;
        fm.AppntName.value = AppntName;
        fm.WorkFlowFlag.value = "7999999999";
    }
    else if (wFlag ==2)//复核完毕确认
    {
    	//对个人业务纸质出单进行卡控
    	if(!checkPaper()){
    		return false;
    	}
    	//增加身份证号校验
    	//添加身份号证校验 20170912
    	var rs1 = easyExecSql("select idtype,idno, AppntBirthday,AppntSex from lcappnt where contno='"+fm.ContNo.value+"'");
    	var error1 = checkIdNo(rs1[0][0],rs1[0][1],rs1[0][2],rs1[0][3]);
    	if(error1 != ""){
    		alert("投保人"+error1);
    		return false;
    	}
    	var rs2 = easyExecSql("select IDType,IDNo,Birthday,Sex from lcinsured where contno='"+fm.ContNo.value+"' and appntno='"+fm.AppntNo.value+"'");
    	if(rs2 !=null && rs2 !=""){
    		for(var i=0;i<rs2.length;i++){
    			var error2 = checkIdNo(rs2[i][0],rs2[i][1],rs2[i][2],rs2[i][3]);
    			if(error2 != ""){
    				alert("被保人"+error2);
    				return false;
    			}
    		}
    	}
    	var rs3 = easyExecSql("select IDType,IDNo,Birthday,Sex from lcbnf where contno='"+fm.ContNo.value+"'");
    	if(rs3 !=null && rs3 !=""){
    		for(var i=0;i<rs3.length;i++){
    			var error3 = checkIdNo(rs3[i][0],rs3[i][1],rs3[i][2],rs3[i][3]);
    			if(error3 != ""){
    				alert("受益人"+error3);
    				return false;
    			}
    		}
    	}
    	
    	//增加PAD保单核保回退再复核的话投保备注必须录入“有”的校验。
    	var tPrtNo = fm.PrtNo.value;
    	var tContNo = fm.ContNo.value;
    	var tExiSpec = easyExecSql("select ExiSpec from lccont where contno ='"+tContNo+"'");
    	var backUWData = easyExecSql("select 1 from LWMission a, LCCont b,laagent la  where b.PrtNo = a.MissionProp2 and a.ActivityID = '0000001001' and a.ProcessID = '0000000003' and ActivityStatus ='4' and b.agentcode=la.agentcode  and a.MissionProp2='"+tPrtNo+"'  and a.MissionProp8 like '86%' ");
    	var SYData = easyExecSql("select a.TaxOptimal from lmriskapp a,lcpol b where a.riskcode = b.riskcode and b.prtno='"+tPrtNo+"'");
    	tPrtNo = tPrtNo.toUpperCase();
    	if(SYData[0][0] !="Y" && tPrtNo.substring(0,2)=="PD" && backUWData && tExiSpec == "N"){
    		alert("该单曾进入人工核保，再次复核需在投保备注录入“有”！");
    		return false;
    	}
    	
        //添加必录项校验，查询和随动定制不需要校验必录项
//        if(LoadFlag == "5" && !verifyInput2())
//        {
//            return false;
//        }
    	//目前仅当险种为122601、122901时，可以录入折扣因子，其他产品录入折扣因子报错提示不通过。
    	if(!checkSYAbout()) {
    		return false;
    	}

    	/*
    	 * #3443: 增加税优产品期缴保费投保上限
    	 * 需求相关：期缴保费上限不得高于“风险保险费+10万元”，对所有税优产品有效
    	 * Created by JC 2017-07-20
    	 */
    	if(!checkPremUpperLimit()) {
    		return false;
    	}
    	if (!checkRisk122002()) {
			return false;
		}
    	/**
		 * #3918:关于优化税优业务数据质量、完善核心系统校验功能的需求 需求相关：投保税优系列产品，工作单位必录。 Created by CLH
		 * 2018-07-09
		 */
		if (!checkGrpName1()) {
			return false;
		}

		/**
		 * #3918:关于优化税优业务数据质量、完善核心系统校验功能的需求 需求相关：对于投保形式为团体的税优业务，校验投保人信息中的“工作单位”与
		 * 团体信息中的“单位名称”是否一致，如不一致，则提示 Created by CLH 2018-07-06
		 */
		if (!checkGrpName()) {
			return false;
		}
    	
    	
//    	add by lxs
	    if(!checkdiscountfactor1()){
	      return false;
	    }
    	
	    //zqt 20170623
	    if(!checkPremMult()){
	    	return false;
		}
	    
        if(!checkTrans()){
          return false;
        }

        //校验是否有未回销的体检件
        if(!checkCard()){
          return false;
        }
        
        //校验第一被保人与主被保人关系
        if(!checkRelation()){
          return false;
        }
        
        if(!chenkBnfBlack()){
          return false;
        }
        
        //校验广东长期险是否调阅录音录像
        if(!checkVideo()){
          return false;
        }
 
    	//增加身份证的校验
//        if(BirthdaySexAppntIDNo()==false){
//        	return false;
//        }
        //增加Polapplydate不能为空的校验
        var arrResult1 = easyExecSql("select polapplydate from LCCont where prtNo='"+fm.PrtNo.value+"' " );
        if(arrResult1 == null){
        	alert("投保日期为空,不能通过复核!");
        	return false;
        }
        if(fm.all('ProposalContNo').value == "")
        {
            alert("未查询出合同信息,不容许您进行 [复核完毕] 确认！");
            return;
        }
        var qurSql="select bussno from es_issuedoc where bussno like '"+fm.PrtNo.value+"%' and ((status='1' and stateflag='1') or (status='0' and stateflag='2')  )";
        var arrqurSql=easyExecSql(qurSql);
        if(arrqurSql){
              //#1868 关于扫描修改待审批状态及预收保费状态时的限制规则
              //alert("保单目前为扫描修改待审批状态，不能新单复核通过，请先完成扫描修改。");
              alert("保单有扫描修改申请尚未处理，须修改完毕后再次提交。");
              return;
        }
        qurSql="select 1 from LJSPay where OtherNo = '"+fm.PrtNo.value+"' and OtherNoType in ('9','16') and BankOnTheWayFlag = '1' ";
        arrqurSql=easyExecSql(qurSql);
        if(arrqurSql){
              alert("保单目前为银行在途时，不能新单复核通过，请财务确认回盘后再次操作。");
              return;
        }

        // 复核完毕时，对先收费的保单，对保费到帐情况进行校验。
        if(!checkEnterAccOfTempFee())
        {
            return false;
        }
        // ---------------------------
        
        //黑名单校验 20120503  gzh
        if(!checkBlackName()){
        	return false;
        }
        
		// #3966 关于建立承保黑名单及配置承保黑名单校验规则的需求    by yuchunjian 20190415
		if (!checkLDHBlackList()) {
			return false;
		}
        
        if(!checkAppnt(wFlag)){
        	return false;
        }
        
        if(!checkInsured()){
		    return false;    
        }
     //校验投保人账户信息
       if(!checkBank()){
        	return false;
        }
      //校验是否复录相关信息  
//       var approveAppSQL=" select 1 from lcappnt a "
//                         +" where prtno='"+fm.PrtNo.value+"'"
//                         +" and not exists (select 1 from lcapprove where prtno=a.prtno and a.appntno=customerno)";
//       var approveInsSQL=" select 1 from lcinsured a "
//                         +" where prtno='"+fm.PrtNo.value+"'"               
//                         +" and not exists (select 1 from lcapprove where prtno=a.prtno and a.insuredno=customerno )  ";
//       var arr1 = easyExecSql(approveAppSQL);
//       if(arr1){
//           alert("请对投保单投保人相关信息进行复录操作");
//           return false;
//       }
//       var arr = easyExecSql(approveInsSQL);
//       if(arr){
//           alert("请对未复录完成的被保人相关信息进行复录操作");
//           return false;
//       } 
        
       
        fm.WorkFlowFlag.value = "0000001001";					//复核完毕
        fm.MissionID.value = MissionID;
        fm.SubMissionID.value = SubMissionID;
        approvefalg="2";
        
        //阳光宝贝只有五家机构（北京、上海、江苏、辽宁、深圳）可以销售
        //var arrResult = easyExecSql("select ManageCom from LCPol where prtNo='"+fm.PrtNo.value+"' " 
        //                          + " and riskcode in('320106','120706') fetch first 1 rows only");
        //if(arrResult){
        //	if(arrResult[0][0].substr(0,4) != "8611"&&arrResult[0][0].substr(0,4) != "8621"&&arrResult[0][0].substr(0,4) != "8631"&&arrResult[0][0].substr(0,4) != "8632"&&arrResult[0][0].substr(0,4) != "8695"){
        //		alert("该管理机构"+arrResult[0][0]+"没有销售阳光宝贝的权限!!");
        //		return false;
        //	}
        //}
    }
    else if (wFlag ==3)
    {
        if(fm.all('ProposalContNo').value == "") {
            alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
            return;
        }
        fm.WorkFlowFlag.value = "0000001002";
        fm.MissionID.value = tMissionID;
        fm.SubMissionID.value = tSubMissionID;
    }
    else
        return;

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    var tAction = fm.action;
    fm.action = "./InputConfirm.jsp";
    fm.submit(); //提交
    fm.action=tAction;
}

/**
 * 检查：期缴保费上限不得高于“风险保险费+10万元”，对所有税优产品有效
 */
function checkPremUpperLimit() {
	var sql = "select 1 from lmriskapp where taxoptimal='Y' and riskcode = (select riskcode from  lcpol where prtno = '"+fm.PrtNo.value+"') ";
	var arrResult = easyExecSql(sql);
	//判断是否为税优保单
	if(arrResult) {
		//1、先获取计算风险保费的要素
		var getSomeFactorsSql = "select PolNo,PayIntv,InsuredAppAge,Prem from lcpol where prtno = '"+fm.PrtNo.value+"'";
		var Result = easyExecSql(getSomeFactorsSql);
		var PolNo = Result[0][0];
		var PayIntv = Result[0][1];
		var InsuredAppAge = Result[0][2];
		var Prem = Result[0][3];
		//2、再获取计算风险保费的算费公式
		var getCalSql = "select CalSql from lmcalmode where riskcode = (select riskcode from  lcpol where prtno = '"+fm.PrtNo.value+"') and remark = '风险保费扣费'";
		var CalSqlResult = easyExecSql(getCalSql);
		var CalSql = CalSqlResult[0][0];
		//3、拼出计算风险保费的算费SQL(九种税优险种的风险保费计算情况都考虑在内了...)
		//这里所有的税优总折扣因子都默认为：1
		var mtotaldiscountfactor =easyExecSql("select Totaldiscountfactor from lccontsub s where prtno='"+fm.PrtNo.value+"' ");
		//期缴保费上限不得高于“风险保险费*折扣因子+10万元”
		if(mtotaldiscountfactor !=""&&mtotaldiscountfactor !=null){
			if(CalSql.indexOf("?Totaldiscountfactor?") >= 0) {
				CalSql = CalSql.replace("?Totaldiscountfactor?",mtotaldiscountfactor);
			}
		}else{
			if(CalSql.indexOf("?Totaldiscountfactor?") >= 0) {
				CalSql = CalSql.replace("?Totaldiscountfactor?","1");
			}
		}
		/**
		 * 目前数据库中税优产品一共就9种，每种的风险保费算法都不同，但其中只有AppAge这个参数会出现两次以上，因此需要将该字符串
		 * 在总算法sql的字符串中全部替换掉，已知最多出现两次，因此只需循环两次
		 * 
		 * 但这样写有个缺点就是如果以后sql出现变动，AppAge出现的次数增加或者减少，那么还需更改代码的循环次数，这样写非常死板
		 * 并且如果以后循环的次数多了也会降低性能，因此这里可以换成另一种写法：JavaScript中没有replaceAll()方法,但是在js
		 * 中replace方法存在利用正则表达式的方式进行字符串替换，这样就可以避免多次循环。
		 * 
		 * 但是这里我就要吐槽一下，为啥我换成正则表达式就报错，各种不识别，难道我们亲爱的老Lis框架不屑于兼容正则表达式吗？无语中...
		 * 
		 * Created by JC 2017-07-20
		 * I am thinking in Java, not the JavaScript in LIS...
		 */
		for(var i = 0; i < 2; i++) {
			if(CalSql.indexOf("?AppAge?") >= 0) {
				CalSql = CalSql.replace("?AppAge?",InsuredAppAge);
			}
		}
		if(CalSql.indexOf("?PayIntv?") >= 0) {
			CalSql = CalSql.replace("?PayIntv?",PayIntv);
		}
		if(CalSql.indexOf("?PolNo?") >= 0) {
			CalSql = CalSql.replace("?PolNo?",PolNo);
		}
		//算出风险保费
		var riskPrem = easyExecSql(CalSql);
		//得到int类型的风险保费
		var a = parseInt(riskPrem[0][0]);
		//得到int类型的期交保费
		var b = parseInt(Prem);
		//期缴保费上限不得高于“风险保险费+1万元”
		var sqlprem = "select code from ldcode where codetype='limitedprem' ";
		var sqlpremresult=easyExecSql(sqlprem);
		var c = parseInt(sqlpremresult[0][0] + "0000");
		if(PayIntv=="12"){
			if(b > (a + c)) {
				alert("年化期缴保费上限不得高于“年风险保险费+"+sqlpremresult[0][0]+"万元”!");
				return false;
			}
		}else if(PayIntv=="1"){
			if((b*12) > ((a)*12 + c)) {
				alert("年化期缴保费上限不得高于“年风险保险费+"+sqlpremresult[0][0]+"万元”!");
				return false;
			}
		}
	}
	return true;
}

function checkRisk122002() {
	var tPolSQL = "select mult,insuredno,insuredname from lcpol where prtno = '" + fm.PrtNo.value
			+ "' and riskcode = '122002' with ur";
	var arr1 = easyExecSql(tPolSQL);
	if (arr1) {
		for (var i = 0; i < arr1.length; i++) {
			// 险种为122002时，档次必录！
			//实际发现毫无意义，lcpol里档次为非空字段。
			if (!arr1[i][0]) {
				alert("险种122002必须录入档次信息！");
				return false;
			}
			// 险种为122002时，此条健康告知必录！
			var CustomerimpartSql = "select impartparammodle from lccustomerimpart where prtno = '"
					+ fm.PrtNo.value
					+ "' and customerno = '"
					+ arr1[i][1]
					+ "' and impartver = '001' and impartcode='170' with ur";
			var arrCustomer = easyExecSql(CustomerimpartSql);
			if (arrCustomer == "N,N,N,N,N" || arrCustomer == "N,N,N,N") {
				alert("当险种为122002时，被保人"+arr1[i][2]+"的医疗费用支付方式为必录！");
				return false;
			}
			var impartparammodle = arrCustomer[0][0].split(",");
			if (impartparammodle[1] == "Y" && arr1[i][0] != "2") {
				alert("被保人"+arr1[i][2]+"的医疗费用支付方式为社会医疗保险时，险种122002档次必须为2！");
				return false;
			}
			if (impartparammodle[1] != "Y" && arr1[i][0] != "1") {
				alert("被保人"+arr1[i][2]+"的医疗费用支付方式不为社会医疗保险时，险种122002档次必须为1！");
				return false;
			}
		}
	}
	return true;
}

/*********************************************************************
 *  查询被保险人详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getdetailaddress() {
  var strSQL="select b.AddressNo,b.PostalAddress,b.ZipCode,b.Phone,b.Mobile,b.EMail,b.CompanyPhone,b.CompanyAddress,b.CompanyZipCode,b.HomeAddress,b.HomeZipCode,b.HomePhone,b.GrpName,b.PostalProvince,b.PostalCity,b.PostalCounty,b.PostalStreet,b.PostalCommunity,b.HomeCode,b.HomeNumber from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.InsuredNo.value+"'";
  arrResult=easyExecSql(strSQL);
  try {
    fm.all('AddressNo').value= arrResult[0][0];
  } catch(ex) {}
  ;
  try {
    fm.all('PostalAddress').value= arrResult[0][1];
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= arrResult[0][2];
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= arrResult[0][3];
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= arrResult[0][4];
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= arrResult[0][5];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpPhone').value= arrResult[0][6];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value= arrResult[0][7];
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= arrResult[0][8];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= arrResult[0][9];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= arrResult[0][10];
  } catch(ex) {}
  ;
  try {
    fm.all('HomePhone').value= arrResult[0][11];
  } catch(ex) {}
  ;
  try {

 fm.all('GrpName').value= arrResult[0][12];
  } catch(ex) {}
  ; //已经在 LDPerson 里里面查询出来了
  var rs1 = easyExecSql("select codename from ldcode1 where codetype='province1' and code ='"+arrResult[0][13]+"' and code1='0'");
  var rs2 = easyExecSql("select codename from ldcode1 where codetype='city1' and code ='"+arrResult[0][14]+"' and code1='"+arrResult[0][13]+"'");
  var rs3 = easyExecSql("select codename from ldcode1 where codetype='county1' and code ='"+arrResult[0][15]+"' and code1='"+arrResult[0][14]+"'");
  if(rs1 !=null ){
	  try {fm.all('Province').value= arrResult[0][13];} catch(ex) {};
	  try {fm.all('City').value= arrResult[0][14];} catch(ex) {};
	  try {fm.all('County').value= arrResult[0][15];} catch(ex) {};
	  try {fm.all('PostalProvince').value= rs1;} catch(ex) {};
	  try {fm.all('PostalCity').value= rs2;} catch(ex) {};
	  try {fm.all('PostalCounty').value= rs3;} catch(ex) {};
  }else{
	  var rs4 = easyExecSql("select code from ldcode1 where codetype='province1' and codename ='"+arrResult[0][13]+"'");
	  var rs5 = easyExecSql("select code from ldcode1 where codetype='city1' and codename ='"+arrResult[0][14]+"'");
	  var rs6 = easyExecSql("select code from ldcode1 where codetype='county1' and codename ='"+arrResult[0][15]+"'");
	  try {fm.all('PostalProvince').value= arrResult[0][13];} catch(ex) {};
	  try {fm.all('PostalCity').value= arrResult[0][14];} catch(ex) {};
	  try {fm.all('PostalCounty').value= arrResult[0][15];} catch(ex) {};
	  try {fm.all('Province').value= rs4;} catch(ex) {};
	  try {fm.all('City').value= rs5;} catch(ex) {};
	  try {fm.all('County').value= rs6;} catch(ex) {};
  }
  
  try {
    fm.all('PostalStreet').value= arrResult[0][16];
  } catch(ex) {}
  ;
  try {
    fm.all('PostalCommunity').value= arrResult[0][17];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeCode').value= arrResult[0][18];
  } catch(ex) {}
  ;
  try {
    fm.all('HomeNumber').value= arrResult[0][19];
  } catch(ex) {}
  ;  
}

/*********************************************************************
 *  查询保险计划
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getContPlanCode(tProposalGrpContNo) {
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select ContPlanCode,ContPlanName from LCContPlan where ContPlanCode<>'00' and ContPlanCode<>'11' and ProposalGrpContNo='"+tProposalGrpContNo+"'";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
    divContPlan.style.display="";
  } else {
    //alert("保险计划没查到");
    divContPlan.style.display="none";
  }
  //alert ("tcodedata : " + tCodeData);
  return tCodeData;
}

/*********************************************************************
 *  查询处理机构
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getExecuteCom(tProposalGrpContNo) {
  //alert("1");
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select ExecuteCom,Name from LCGeneral a,LDCom b where a.GrpContNo='"+tProposalGrpContNo+"' and a.ExecuteCom=b.ComCode";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
    divExecuteCom.style.display="";
  } else {
    divExecuteCom.style.display="none";
  }
  //alert ("tcodedata : " + tCodeData);

  return tCodeData;
}

function emptyInsured() {

  try {
    fm.all('InsuredNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('ExecuteCom').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('FamilyID').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToMainInsured').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('RelationToAppnt').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('AddressNo').value= "";
  } catch(ex) {}
  ;
//  try
//  {
//  	fm.all('SequenceNo').value= "";
//  }
//  catch(ex)
//  {}
//  ;
  try {
    fm.all('Name').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Sex').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('SexName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Birthday').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('IDType').value= "0";
  } catch(ex) {}
  ;
  try {
    fm.all('IDTypeName').value= "身份证";
  } catch(ex) {}
  ;
  try {
    fm.all('IDNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlace').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Nationality').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativePlaceName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('RgtAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Marriage').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('MarriageDate').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Health').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Stature').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Avoirdupois').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Degree').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('CreditGrade').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('BankCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('BankAccNo').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('AccName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('JoinCompanyDate').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('StartWorkDate').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Position').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Salary').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationType').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('OccupationName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('WorkType').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('PluralityType').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('SmokeFlag').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('ContPlanCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpName').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeZipCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomePhone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('HomeFax').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpFax').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Fax').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('CheckPostalAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativeCity').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('NativeCityName').value= "";
  } catch(ex) {}
  ;
  controlNativeCity("");
  emptyAddress();
  ImpartGrid.clearData();
  ImpartGrid.addOne();
  ImpartDetailGrid.clearData();
  //ImpartDetailGrid.addOne();
  clearImpart();
  initArray();
}

/*********************************************************************
 *  清空客户地址数据
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function emptyAddress() {
  try {
    fm.all('PostalAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('ZipCode').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Phone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('Mobile').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('EMail').value= "";
  } catch(ex) {}
  ;
  //try{fm.all('GrpName').value= arrResult[0][2]; }catch(ex){};
  try{
  	fm.all('GrpName').value="";
  }catch(ex){}
  ;
  try {
    fm.all('GrpPhone').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpAddress').value= "";
  } catch(ex) {}
  ;
  try {
    fm.all('GrpZipCode').value= "";
  } catch(ex) {}
  ;
}
/*********************************************************************
 *  根据身份证号取得出生日期和性别
 *  参数  ：  身份证号
 *  返回值：  无
 *********************************************************************
 */

function getBirthdaySexByIDNo(iIdNo) {
  if(fm.all('IDType').value=="0") {
    fm.all('Birthday').value=getBirthdatByIdNo(iIdNo);
    fm.all('Sex').value=getSexByIDNo(iIdNo);
  }
  if(fm.all('IDType').value=="5") {
	    fm.all('Birthday').value=getBirthdatByIdNo(iIdNo);
	    fm.all('Sex').value=getSexByIDNo(iIdNo);
	  }
  //zxs  
	if (fm.all('IDType').value == "a"||fm.all('IDType').value == "b") {
		fm.all('Birthday').value = getBirthdatByIdNo(iIdNo);
		fm.all('Sex').value = getSexByIDNo(iIdNo);
	}
}
/*********************************************************************
 *  合同信息录入完毕确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpInputConfirm(wFlag) {
  mWFlag = 1;
  if (wFlag ==1 ) //录入完毕确认
  {
    var tStr= "	select * from lwmission where 1=1 "
              +" and lwmission.processid = '0000000004'"
              +" and lwmission.activityid = '0000002001'"
              +" and lwmission.missionprop1 = '"+fm.ProposalGrpContNo.value+"'";
    turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
    if (turnPage.strQueryResult) {
      alert("该团单合同已经做过保存！");
      return;
    }
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("团单合同信息未保存,不容许您进行 [录入完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "6999999999";			//录入完毕
  } else if (wFlag ==2)//复核完毕确认
  {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出团单合同信息,不容许您进行 [复核完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000002002";					//复核完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else if (wFlag ==3) {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001002";					//复核修改完毕
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else if(wFlag == 4) {
    if(fm.all('ProposalGrpContNo').value == "") {
      alert("未查询出合同信息,不容许您进行 [修改完毕] 确认！");
      return;
    }
    fm.WorkFlowFlag.value = "0000001021";					//问题修改
    fm.MissionID.value = MissionID;
    fm.SubMissionID.value = SubMissionID;
  } else
    return;

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./GrpInputConfirm.jsp";
  fm.submit(); //提交
}
function getaddresscodedata() {
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  //strsql = "select AddressNo,PostalAddress from LCAddress where CustomerNo ='"+fm.InsuredNo.value+"'";
  strsql = "select max(int(AddressNo)) from LCAddress where CustomerNo ='"+fm.InsuredNo.value+"'";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      //tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData =turnPage.arrDataCacheSet[i][0];
    }
  }
  //alert ("tcodedata : " + tCodeData);
  //return tCodeData;
  //fm.all("AddressNo").CodeData=tCodeData;
  fm.all("AddressNo").value=tCodeData;
  afterCodeSelect("AddressNo","");
}

function getImpartCode(parm1,parm2) {
  //alert("hehe:"+fm.all(parm1).all('ImpartGrid1').value);
  var impartVer=fm.all(parm1).all('ImpartGrid1').value;
  window.open("../app/ImpartCodeSel.jsp?ImpartVer="+impartVer);
}
function checkidtype() {
  if(fm.IDType.value=="") {
    alert("请先选择证件类型！");
    fm.IDNo.value="";
  }
}
function getallinfo() {
  if(fm.Name.value!=""&&fm.IDType.value!=""&&fm.IDNo.value!="") {
    strSQL = "select a.CustomerNo, a.Name, a.Sex, a.Birthday, a.IDType, a.IDNo from LDPerson a where 1=1 "
             +"  and Name='"+fm.Name.value
             +"' and IDType='"+fm.IDType.value
             +"' and IDNo='"+fm.IDNo.value
             +"' order by a.CustomerNo";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (turnPage.strQueryResult != "") {
      mOperate = 2;
      //alert(mOperate);
      //window.open("../sys/LDPersonQueryAll.html?Name="+fm.Name.value+"&IDType="+fm.IDType.value+"&IDNo="+fm.IDNo.value,"newwindow","height=10,width=1090,top=180,left=180, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no,status=no");
    } else
      return;
  }
}
function DelRiskInfo() {
  if(fm.InsuredNo.value=="") {
    alert("请先选择被保人");
    return false;
  }
  var tSel =PolGrid.getSelNo();
  if( tSel == 0 || tSel == null ) {
    alert("该客户没有险种或者您忘记选择了？");
    return false;
  }
  if(!confirm("是否要删除该险种？删除请点击“确定”，否则点“取消”。"))
  {
  	return;
  }
  var tRow = PolGrid.getSelNo() - 1;
  var tpolno=PolGrid.getRowColData(tRow,1)
             fm.all('fmAction').value="DELETE||INSUREDRISK";
  fm.action="./DelIsuredRisk.jsp?polno="+tpolno;
  fm.submit(); //提交

}
function InsuredChk() {
  var tSel =InsuredGrid.getSelNo();
  if( tSel == 0 || tSel == null ) {
    alert("请先选择被保险人！");
    return false;
  }
  var tRow = InsuredGrid.getSelNo() - 1;
  var tInsuredNo=InsuredGrid.getRowColData(tRow,1);
  var tInsuredName=InsuredGrid.getRowColData(tRow,2);
  var tInsuredSex=InsuredGrid.getRowColData(tRow,3);
  var tBirthday=InsuredGrid.getRowColData(tRow,4);
  var sqlstr="select *from ldperson where Name='"+tInsuredName+"' and Sex='"+tInsuredSex+"' and Birthday='"+tBirthday+"' and CustomerNo<>'"+tInsuredNo+"'";
  arrResult = easyExecSql(sqlstr,1,0);
  if(arrResult==null) {
    alert("没有与该被保人相似的客户,无需校验");
    return false;
  }

  window.open("../uw/InsuredChkMain.jsp?ProposalNo1="+fm.ContNo.value+"&InsuredNo="+tInsuredNo+"&Flag=I","window1");
}
function FillPostalAddress() {
	var customerno=easyExecSql("select appntno,addressno from lcappnt where prtno='"+fm.all( 'PrtNo' ).value+"'");
    var PostalAddressSQL="select PostalAddress,PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity from lcaddress where customerno ='"+customerno[0][0]+"' and addressno='"+customerno[0][1]+"'";
  var sqlstr=easyExecSql(PostalAddressSQL);
    if(sqlstr){
			//alert();
    	if(fm.CheckPostalAddress.value=="1") {
    		PostalAddressID.style.display='';
    		insured_PostalAddressID.style.display='none';
    		fm.all('PostalAddress').value=sqlstr[0][0];
			var rs1=easyExecSql("select codename from ldcode1 where codetype='province1' and code='"+sqlstr[0][1]+"' and code1='0'");
			var rs2=easyExecSql("select codename from ldcode1 where codetype='city1' and code='"+sqlstr[0][2]+"' and code1='"+sqlstr[0][1]+"'");
			var rs3=easyExecSql("select codename from ldcode1 where codetype='county1' and code='"+sqlstr[0][3]+"' and code1='"+sqlstr[0][2]+"'");
			if(rs1 != null){
				 fm.all('PostalProvince').value=rs1;
				 fm.all('PostalCity').value=rs2;
				 fm.all('PostalCounty').value=rs3;
				 fm.all('Province').value=sqlstr[0][1];
				 fm.all('City').value=sqlstr[0][2];
				 fm.all('County').value=sqlstr[0][3];
			}else{
				var rs4=easyExecSql("select code from ldcode1 where codetype='province1' and codename='"+sqlstr[0][1]+"' ");
				var rs5=easyExecSql("select code from ldcode1 where codetype='city1' and codename='"+sqlstr[0][2]+"' ");
				var rs6=easyExecSql("select code from ldcode1 where codetype='county1' and codename='"+sqlstr[0][3]+"'");
				fm.all('PostalProvince').value=sqlstr[0][1];
				fm.all('PostalCity').value=sqlstr[0][2];
				fm.all('PostalCounty').value=sqlstr[0][3];
				 fm.all('Province').value=rs4;
				 fm.all('City').value=rs5;
				 fm.all('County').value=rs6;
			}
			fm.all('PostalStreet').value=sqlstr[0][4];
			fm.all('PostalCommunity').value=sqlstr[0][5];
    		fm.all('ZipCode').value="";
		}else if(fm.CheckPostalAddress.value=="2"){
    		PostalAddressID.style.display='none';
    		insured_PostalAddressID.style.display='';
			fm.all('PostalAddress').value="";
			fm.all('ZipCode').value="";    		
		}
	}else{
	fm.all('PostalAddress').value="";
	fm.all('ZipCode').value="";    
	} 
}
function checksex() {
  var malearray= new Array("02","04","06","08","09","14","16","19","22");
  var femalearray= new Array("01","05","07","10","11","15","17","20","23");
  if(fm.Sex.value=="0") {
    for(var relationmcount=0;relationmcount<femalearray.length-1;relationmcount++) {
      if(fm.RelationToAppnt.value==femalearray[relationmcount]) {
        alert("被保人性别与'与投保人关系'中的关系身份不符");
        return false;
      }
      if(fm.RelationToMainInsured.value==femalearray[relationmcount]) {
        alert("被保人性别与'与第一被保人关系'中的关系身份不符");
        return false;
      }
    }
  }
  if(fm.Sex.value=="1") {
    for(var relationfcount=0;relationfcount<malearray.length;relationfcount++) {
      if(fm.RelationToAppnt.value==malearray[relationfcount]) {
        alert("被保人性别与'与投保人关系'中的关系身份不符");
        return false;
      }
      if(fm.RelationToMainInsured.value==malearray[relationfcount]) {
        alert("被保人性别与'与第一被保人关系'中的关系身份不符");
        return false;
      }
    }
  }
  //alert(fm.OccupationCode.value);
  if(fm.OccupationCode.value!=""){
  var tsqlstr=" select OccupationType from LDOccupation where  OccupationCode ='"+fm.OccupationCode.value+"'";
   var OccupationType = easyExecSql(tsqlstr,1,0);
  if(OccupationType==null) {
    alert("职业代码"+fm.OccupationCode.value+"没有定义职业类别!");
    return false;
  }
  if(OccupationType!=fm.OccupationType.value){
    alert("职业代码"+fm.OccupationCode.value+"的职业类别与页面录入不符,应为"+OccupationType+"类职业!");
    return false;  
  }
 } 
  return true;
}
function getdetailaccount() {
  if(fm.AccountNo.value=="1") {
    if(mSwitch.getVar("AppntBankAccNo"))
      fm.all('BankAccNo').value=mSwitch.getVar("AppntBankAccNo");
    if(mSwitch.getVar("AppntBankCode"))
      fm.all('BankCode').value=mSwitch.getVar("AppntBankCode");
      fm.all('bankName').value = easyExecSql("select codename from ldcode where codetype='bank' and code='"+mSwitch.getVar("AppntBankCode")+"'");
    if(mSwitch.getVar("AppntAccName"))
      fm.all('AccName').value=mSwitch.getVar("AppntAccName");
  }
  if(fm.AccountNo.value=="2") {
    fm.all('BankAccNo').value="";
    fm.all('BankCode').value="";
    fm.all('AccName').value="";
  }

}
function AutoMoveForNext() {
  if(fm.AutoMovePerson.value=="定制第二被保险人") {
    //emptyFormElements();
    param="122";
    fm.pagename.value="122";
    fm.AutoMovePerson.value="定制第三被保险人";
    return false;
  }
  if(fm.AutoMovePerson.value=="定制第三被保险人") {
    //emptyFormElements();
    param="123";
    fm.pagename.value="123";
    fm.AutoMovePerson.value="定制第一被保险人";
    return false;
  }
  if(fm.AutoMovePerson.value=="定制第一被保险人") {
    //emptyFormElements();
    param="121";
    fm.pagename.value="121";
    fm.AutoMovePerson.value="定制第二被保险人";
    return false;
  }
}
function noneedhome() {
  var insuredno="";
  if(InsuredGrid.mulLineCount>=1) {
    for(var personcount=0;personcount< InsuredGrid.mulLineCount;personcount++) {
      if(InsuredGrid.getRowColData(personcount,5)=="00") {
        insuredno=InsuredGrid.getRowColData(personcount,1);
        break;
      }
    }
    var strhomea="select HomeAddress,HomeZipCode,HomePhone from lcaddress where customerno='"+insuredno+"' and addressno=(select addressno from lcinsured where contno='"+fm.ContNo.value+"' and insuredno='"+insuredno+"')";
    arrResult=easyExecSql(strhomea,1,0);
    try {
      fm.all('HomeAddress').value= arrResult[0][0];
    } catch(ex) {}
    ;
    try {
      fm.all('HomeZipCode').value= arrResult[0][1];
    } catch(ex) {}
    ;
    try {
      fm.all('HomePhone').value= arrResult[0][2];
    } catch(ex) {}
    ;
  }
}
function getdetail() {
  var strSql = "select BankCode,AccName from LCAccount where BankAccNo='" + fm.BankAccNo.value+"'";
  arrResult = easyExecSql(strSql);
  if (arrResult != null) {
    fm.BankCode.value = arrResult[0][0];
    fm.AccName.value = arrResult[0][1];
  }

}
//用于无名单时填充被保险人信息
function FillNoName() {
  fm.IDType.value='4';
  DivLCInsured.style.display='none';
  DivGrpNoname.style.display='';
}

//用于无名单校验
function checkifnoname() {
  var strSql = "select ContPlanCode from LCInsured where ContNo='" + oldContNo+"' and Name='无名单'";
  arrResult = easyExecSql(strSql);
  if(fm.ContPlanCode.value!=""&&fm.ContPlanCode.value!=arrResult[0][0]) {
    alert("所选计划与原无名单所选不一致！");
    return false;
  } else {
    return true;
  }
}
//用于无名单校验
function checkenough() {
  var strSql1 = " select * from lcpol where appflag='4' and MasterPolNo in (select  proposalNo from lcpol where Contno='" + oldContNo+"')";
  var arrResult1 = easyExecSql(strSql1);
  var strSql2 = " select * from lccont where appflag<>'1' and GrpContNo = (select  GrpContNo from lcCont where Contno='" + oldContNo+"')";
  var arrResult2 = easyExecSql(strSql2);
  if(arrResult1!=null||arrResult2!=null) {
    alert("尚有补名单未签单,请先签单再补其他名单!");
    return false;
  } else {
    return true;
  }
}
//险种复合
function showComplexRiskInfo()
{
	window.open("./ComplexRiskMain.jsp?ContNo="+ContNo+"&prtNo="+fm.PrtNo.value,"", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}
//校验是否重新生成新的合同
function checkCanModify(){
	var strSql = "select 1 from LCInsured where name='"+fm.Name.value+"' and EnglishName='"+fm.EnglishName.value+"' and OthIDNo='"+fm.OthIDNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		return false;
	}else{
		return true;
	}
}
function afterSubmit1( FlagStr, content )
{
	UnChangeDecodeStr();
	showInfo.close();
	window.focus();
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "新单复核成功！";
		cMissionID = fm.MissionID.value;
		strSQL = "select 1 from lwmission where 1=1 "
				 + " and missionid = '"+ cMissionID +"'"
				 + " and activityid = '0000001100' "
				 + " and processid = '0000000003'"
				 ;	 

	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  	//判断是否查询成功
  	if (turnPage.strQueryResult) {
			content = content + "但需提交人工核保！";
  	}
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		top.opener.queryCont();
	}
	mAction = ""; 
	top.close();
}




//外包反馈错误信息 2007-9-28 11:47
function findIssue()
{
	window.open("./BPOIssueInput.jsp?prtNo="+prtNo,"window1");
}

//销售渠道和业务员的校验
function checkSaleChnl()
{
    //业务员和销售渠道的校验
    var agentCodeSql = "select 1 from LAAgent a, LDCode1 b, LCCont c "
        + "where c.PrtNo = '" + fm.PrtNo.value + "' "
        + "and a.AgentState < '06' and b.CodeType = 'salechnl' "
        + "and a.AgentCode = c.AgentCode and b.Code = c.SaleChnl "
        + "and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName " 
        + "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '10' = '" + fm.SaleChnl.value + "' and a.BranchType2 = '04'  "
		+ "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '03' = '" + fm.SaleChnl.value + "' and a.BranchType2 = '04'  "
		+ "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '02' = '" + fm.SaleChnl.value + "' and a.BranchType = '2' and a.BranchType2 = '02'  "
        + "with ur";
    var arr = easyExecSql(agentCodeSql);
    if(!arr){
        alert("业务员和销售渠道不相符！");
        return false;
    }
    return true;
}

/**
 * 校验所填入缴费凭证号的，到帐情况
 */
function checkEnterAccOfTempFee()
{
    var tContNo = fm.ContNo.value;
    var tResult = easyExecSql("select PayMode, TempFeeNo from LCCont lcc where lcc.ContNo = '" + tContNo + "'");
    if(!tResult)
    {
        alert('未查询出该保单对应缴费相关信息。');
        return false;
    }
    
    var tPayMode = tResult[0][0];
    var tTempFeeNo = tResult[0][1];
    
    // 只对缴费方式为：1-现金；11-银行代收，进行校验。
    if(tPayMode != "1" && tPayMode != "11" && tPayMode != "12")
        return true;

    // 暂缴号为空或不填，不进行校验。
    if(tTempFeeNo == null || tTempFeeNo == "")
        return true;
        
    var tStrSql = "select nvl(sum(PayMoney), 0) "
       + " from LJTempFeeClass ljtfc "
       + " where ljtfc.EnterAccDate is not null and ljtfc.ConfMakeDate is not null "
       + " and ConfDate is null "
       + " and ljtfc.TempFeeNo = '" + tTempFeeNo + "' ";
       
    tResult = easyExecSql(tStrSql);
    if(!tResult)
    {
        alert('未查询出该暂缴号对应保费相关信息。');
        return false;
    }
    var tEnterAccMoney = tResult[0][0];
    
    tResult = easyExecSql("select nvl(sum(Prem), 0) from LCPol lcp where lcp.ContNo = '" + tContNo + "' ");
    if(!tResult)
    {
        alert('未查询出该保单对应保费相关信息。');
        return false;
    }
    var tContPrem = tResult[0][0];
    
    var tComment = "该单为先收费保单:\n"
        + "缴费方式为：" + tPayMode + "\n"
        + "暂缴收据号为：" + tTempFeeNo + "\n"
        + "目前到帐保费为：" + tEnterAccMoney + "\n"
        + "该单系统保费为：" + tContPrem;
    return confirm(tComment);
}

//险种的校验
function checkRiskInfo()
{
    //该分公司险种是否停售校验
    var riskEnd = "select 1 from LCPol a, LMRiskApp b, LDCode c where a.PrtNo = '" 
        + fm.PrtNo.value + "' and b.RiskType4 = '4' and c.CodeType = 'UniversalSale' "
        + "and a.RiskCode = b.RiskCode and c.Code = substr(a.ManageCom, 1, 4) ";
    var result = easyExecSql(riskEnd);
    if(result)
    {
        alert("该分公司万能险已停售！");
        return false;
    }
    
    //保险期间单位为空的校验
    var riskInsu = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and (InsuYearFlag is null or InsuYearFlag = '')";
    result = easyExecSql(riskInsu);
    if(result)
    {
        alert("保险期间单位为空，请修改！");
        return false;
    }
    
    //个人防癌套餐的校验
    var risk230501And331001 = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and RiskCode in ('230501', '331001')";
    result = easyExecSql(risk230501And331001);
    if(result)
    {
        var riskInfoSql = "select 1 from LCPol where PrtNo = '" + fm.PrtNo.value + "' "
            + "and RiskCode in ('230501', '331001') "
            + "and not (InsuYear in (10, 15, 20) and InsuYearFlag = 'Y' "
            + "and PayEndYear = InsuYear and PayEndYearFlag = InsuYearFlag "
            + "and PayIntv = 12)";
        result = easyExecSql(riskInfoSql);
        if(result)
        {
            alert("请检查缴费频次、保险期间和缴费期间！\n(1).保险期间必须为10年、15年、20年。"
                + "\n(2).缴费年期必须与保险期间一致。\n(3).缴费频次必须为年缴。");
            return false;
        }
    }
    return true;
}


/**
 * 校验销售渠道与产品关联
 */
function checkSaleChnlRisk()
{   
    var tPrtNo = fm.PrtNo.value;
    var saleChannel = null;
    
    var arr = easyExecSql("select salechnl from lccont where prtno = '" + tPrtNo + "' ");
    if(arr)
    {
        saleChannel = arr[0][0];
    }
    
    if(saleChannel != null && saleChannel == "13")
    {
        var tStrSql = " select 1 "
            + " from LCPol lcp "
            + " left join LDCode1 ldc1 on lcp.SaleChnl = ldc1.Code and lcp.RiskCode = ldc1.code1 and ldc1.codetype = 'checksalechnlrisk' "
            + " where 1 = 1 "
            + " and ldc1.CodeType is null "
            + " and lcp.PrtNo = '" + tPrtNo + "' "
            ;
        var result = easyExecSql(tStrSql);
        
        if(result)
        {
            alert("该单为银代直销保单，但存在非银代直销产品，请核实。");
            return false;
        }
    }
    
    return true;
}


function BirthdaySexAppntIDNo()
{
	if(fm.all('IDType').value=="0")
	{
		if(fm.all('Birthday').value!=getBirthdatByIdNo(fm.all('IDNo').value) || fm.all('Sex').value!=getSexByIDNo(fm.all('IDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
	
	if(fm.all('IDType').value=="5")
	{
		if(fm.all('Birthday').value!=getBirthdatByIdNo(fm.all('IDNo').value) || fm.all('Sex').value!=getSexByIDNo(fm.all('IDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
}
//by gzh 20110402 录入完毕及复核完毕时，校验被保人告知第19项是否正确填写（即存在小于18周岁的未成年人，但还没选择被保人告知第19项）
function checkGZ19(){
	var strSql = "select insuredno,insuredappage from lcpol where prtno = '"+prtNo+"' and insuredappage<18";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		for(var i=0;i<arr.length;i++){
			var CustomerimpartSql = "select * from lccustomerimpart where prtno = '"+prtNo+"' and customerno = '"+arr[i][0]+"' and impartver = '001' and impartcode='250'";
			var arrCustomer = easyExecSql(CustomerimpartSql);
			if(arrCustomer==null){
				alert("被保人（"+arr[i][0]+"）年龄为"+arr[i][1]+"岁，请填写被保人告知第19项！");
				return false;
			}
		}
	}
	return true;
}

//by gzh 20110614 录入完毕及复核完毕时，校验被保人告知第19项是否正确填写（选择“是”时，保额必须填写）
function checkGZ19Prem(){
	var CustomerimpartSql = "select impartparammodle from lccustomerimpart where prtno = '"+prtNo+"' and impartver = '001' and impartcode='250'";
	var arrCustomer = easyExecSql(CustomerimpartSql);
	if(arrCustomer !=null){
		for(var i=0;i<arrCustomer.length;i++){
			var impartparammodle = arrCustomer[i][0].split(",");
			if(impartparammodle[0] == "Y" && (!isNumeric(impartparammodle[1]))){
				alert("被保人告知第19项,当选择“是”时，保额不能为空且必须是数字！");
				return false;
			}
		}
	}
	return true;
}

//校验保单的保险期间是否为负数     by zhangyang  2011-06-09
function checkInsuYear()
{
	var strSQL = "select Years from lcpol where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		for(var i = 0; i < arrResult.length; i++)
		{
			var tYears = arrResult[i][0];
			if(tYears != null && tYears!= '' && tYears < 0)
			{
				alert("保单的保险期间有误，请确认保单的保险期间录入值是否正确！");
				return false;
			}
		}
	}
	return true;
}

//校验黑名单
function checkBlackName(){
	//黑名单校验投保人
	var sql1 =  "select appntname,idtype,idno from lcappnt where prtno='"+fm.PrtNo.value+"' ";
	var arrResult = easyExecSql(sql1);
	
	if(arrResult){
		var sqlblack  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
						"  and name='"+arrResult[0][0]+"' and idnotype='"+arrResult[0][1]+"' and idno='"+arrResult[0][2]+"' and type='0' "+
						"  union  "+
						"  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
						"  and name='"+arrResult[0][0]+"' and idno='"+arrResult[0][2]+"' and type='0' "+
						"  union   "+
						"  select 1 from lcblacklist where (idno is null or  idno='' or  idno='无')  and name='"+arrResult[0][0]+"' and type='0' ";
		var arrResult1 = easyExecSql(sqlblack);
		if(arrResult1){
			if(!confirm("该保单投保人："+arrResult[0][0]+"，存在于黑名单中，确认要复核通过吗？")){
				return false;
			}
		}
	}
	//黑名单校验被保人
	var sql2 =  "select name, idtype, idno from lcinsured where prtno='"+fm.PrtNo.value+"'  ";
	var arrRes = easyExecSql(sql2);
	var tInsuNames = "";
	if(arrRes){
		for(var i = 0; i < arrRes.length; i++){
			var sqlblack2  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrRes[i][0]+"' and idnotype='"+arrRes[i][1]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrRes[i][0]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrRes[i][0]+"' and type='0'";
			
			var arrRes1 = easyExecSql(sqlblack2);
			if(arrRes1){
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ " , " + arrRes[i][0];
				}else{
					tInsuNames = tInsuNames + arrRes[i][0];
				}
			}
		}
	}
	if(tInsuNames != ""){
		if (!confirm("该保单被保人姓名："+tInsuNames+",存在于黑名单中，确认要复核通过吗？")){
			return false;
		}
	}
	//黑名单校验受益人
	var sql3 =  "select distinct name, idtype, idno from lcbnf where contno=(select contno from lccont where prtno='"+fm.PrtNo.value+"')  ";
	var arrR = easyExecSql(sql3);
	var tBnfNames = "";
	if(arrR){
		for(var i = 0; i < arrR.length; i++){
			var sqlblack3  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrR[i][0]+"' and idnotype='"+arrR[i][1]+"' and idno='"+arrR[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrR[i][0]+"' and idno='"+arrR[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrR[i][0]+"' and type='0' ";
			var arrR1 = easyExecSql(sqlblack3);
			if(arrR1){
				if(tBnfNames!=""){
					tBnfNames = tBnfNames+ " , "+arrR[i][0];
				}else{
					tBnfNames = tBnfNames + arrR[i][0];
				}
			}
		}
	}
	if(tBnfNames != ""){
		if (!confirm("该保单受益人："+tBnfNames+",存在于黑名单中，确认要复核通过吗？")){
			return false;
		}
	}
	return true;
}
//#3966 关于建立承保黑名单及配置承保黑名单校验规则的需求    by yuchunjian 20190415
function checkLDHBlackList() {
	// 黑名单校验投保人
	var sql1 = "select appntname,idtype,idno from lcappnt where prtno='"
			+ fm.PrtNo.value + "' ";
	var arrResult = easyExecSql(sql1);

	if (arrResult) {
		var sqlblack = "select 1 from ldhblacklist where idtype is not null and idno is not null  "
				+ "  and name='"
				+ arrResult[0][0]
				+ "' and idtype='"
				+ arrResult[0][1]
				+ "' and idno='"
				+ arrResult[0][2]
				+ "' and type='0' "
				+ "  union  "
				+ "  select 1 from ldhblacklist where (idtype is null or idtype='') and idno is not null   "
				+ "  and name='"
				+ arrResult[0][0]
				+ "' and idno='"
				+ arrResult[0][2]
				+ "' and type='0' "
				+ "  union   "
				+ "  select 1 from ldhblacklist where (idno is null or  idno='' or  idno='无')  and name='"
				+ arrResult[0][0] + "' and type='0' ";
		var arrResult1 = easyExecSql(sqlblack);
		if (arrResult1) {
			if (!confirm("该保单投保人：" + arrResult[0][0] + "，存在于承保黑名单中，请确认是否复核通过！")) {
				return false;
			}
		}
	}
	// 黑名单校验被保人
	var sql2 = "select name, idtype, idno from lcinsured where prtno='"
			+ fm.PrtNo.value + "'  ";
	var arrRes = easyExecSql(sql2);
	var tInsuNames = "";
	if (arrRes) {
		for (var i = 0; i < arrRes.length; i++) {
			var sqlblack2 = "  select 1 from ldhblacklist where idtype is not null and idno is not null  "
					+ "  and name='"
					+ arrRes[i][0]
					+ "' and idtype='"
					+ arrRes[i][1]
					+ "' and idno='"
					+ arrRes[i][2]
					+ "' and type='0' "
					+ "  union  "
					+ "  select 1 from ldhblacklist where (idtype is null or idtype='') and idno is not null   "
					+ "  and name='"
					+ arrRes[i][0]
					+ "' and idno='"
					+ arrRes[i][2]
					+ "' and type='0' "
					+ "  union   "
					+ "  select 1 from ldhblacklist where  (idno is null or  idno='' or  idno='无')  and name='"
					+ arrRes[i][0] + "' and type='0' ";
			var arrRes1 = easyExecSql(sqlblack2);
			if (arrRes1) {
				if (tInsuNames != "") {
					tInsuNames = tInsuNames + " , " + arrRes[i][0];
				} else {
					tInsuNames = tInsuNames + arrRes[i][0];
				}
			}
		}
	}
	if (tInsuNames != "") {
		if (!confirm("该保单被保人：" + tInsuNames + ",存在于承保黑名单中，请确认是否复核通过！")) {
			return false;
		}
	}
	// 黑名单校验受益人
	var sql3 = "select distinct name, idtype, idno from lcbnf where contno=(select contno from lccont where prtno='"
			+ fm.PrtNo.value + "')  ";
	var arrR = easyExecSql(sql3);
	var tBnfNames = "";
	if (arrR) {
		for (var i = 0; i < arrR.length; i++) {
			var sqlblack3 = "  select 1 from ldhblacklist where idtype is not null and idno is not null  "
					+ "  and name='"
					+ arrR[i][0]
					+ "' and idtype='"
					+ arrR[i][1]
					+ "' and idno='"
					+ arrR[i][2]
					+ "' and type='0' "
					+ "  union  "
					+ "  select 1 from ldhblacklist where (idtype is null or idtype='') and idno is not null   "
					+ "  and name='"
					+ arrR[i][0]
					+ "' and idno='"
					+ arrR[i][2]
					+ "' and type='0' "
					+ "  union   "
					+ "  select 1 from ldhblacklist where  (idno is null or  idno='' or  idno='无')  and name='"
					+ arrR[i][0] + "' and type='0' ";
			var arrR1 = easyExecSql(sqlblack3);
			if (arrR1) {
				if (tBnfNames != "") {
					tBnfNames = tBnfNames + " , " + arrR[i][0];
				} else {
					tBnfNames = tBnfNames + arrR[i][0];
				}
			}
		}
	}
	if (tBnfNames != "") {
		if (!confirm("该保单受益人：" + tBnfNames + ",存在于承保黑名单中，请确认是否复核通过！")) {
			return false;
		}
	}
	return true;
}

function checkAppnt(wFlag){
	var tSql = "select cc.prtno,cad.Customerno,cc.Managecom,cad.Mobile,cad.Phone,cad.homephone,ca.idno,ca.idtype,ca.appntbirthday,ca.appntsex, "
	           +" cad.PostalProvince,cad.PostalCity,cad.PostalCounty,cad.PostalStreet,cad.PostalCommunity,cad.homecode,cad.homenumber,ca.appntname,ca.nativeplace,ca.nativecity," 
	           +" cad.email"
	           +" from lccont cc inner join lcappnt ca on ca.appntno=cc.appntno and ca.contno=cc.contno " +
				"inner join lcaddress cad on cad.customerno=ca.appntno and cad.addressno=ca.addressno where cc.prtno='" + fm.PrtNo.value + "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		// 印刷号
		var prtno = appntResult[0][0];
		// 投保人客户号
		var customerno = appntResult[0][1];
		// 管理机构
		var managecom = appntResult[0][2];
		// 移动电话
		var mobile = appntResult[0][3];
		// 联系电话
		var phone = appntResult[0][4];
		// 家庭电话
		var homephone = appntResult[0][5];
		// 证件号
		var id = appntResult[0][6];
		// 证件类型
		var idtype = appntResult[0][7];
		// 出生日期
		var birthday = appntResult[0][8];
		// 性别
		var sex = appntResult[0][9];
		//联系地址 固定电话
		var homecode = trim(appntResult[0][15]);
        var homenumber = trim(appntResult[0][16]);
        var PostalProvince = trim(appntResult[0][10]);
        var PostalCity = trim(appntResult[0][11]);
        var PostalCounty = trim(appntResult[0][12]);
        var PostalStreet = trim(appntResult[0][13]);
        var PostalCommunity = trim(appntResult[0][14]);
        // 姓名
		var name = appntResult[0][17];
		var nativeplace = appntResult[0][18];
		var nativecity = appntResult[0][19];
		var email = appntResult[0][20];
		
		if(!isNull(email)){
			   var checkemail =CheckEmail(email);
			   if(checkemail !=""){
				   alert("投保人"+checkemail);
				   return false;
			   }
		   }
		if(!isNull(homephone)){
	     	   var checkhomephone =CheckFixPhone(homephone);
	     	   if(checkhomephone !=""){
	     		   alert("投保人"+checkhomephone);
	     		   return false;
	     	   }
	        }
		// 共有校验
		if(!isNull(mobile)) {
			if(!isInteger(mobile) || mobile.length != 11){
				alert("投保人移动电话需为11位数字，请核查！");
		   		return false;
			}	
		}
		
		if(!checkMobile(1,mobile,'')){
		   return false;
		}

        if(!chenkIdNo(1,idtype,id,'')){
	       return false;
	    }
	
    	if(!chenkHomePhone(1,homecode,homenumber,'')){
	       return false;
	    }

	    if(!checkAddress(1,PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity,'','')){
	       return false;
      	}
		
		if(idtype == "0"){
			
			if(birthday != getBirthdatByIdNo(id)){
				alert("投保人身份证号与出生日期不符，请核查！");
				return false;
			}
			
			if(sex != getSexByIDNo(id)){
				alert("投保人身份证号与性别不符，请核查！");
				return false;
			}
		}
		
		if(!checkCity(1,name,nativeplace,nativecity)){
	        return false;
	    }
		
		// 机构特殊校验  --需要在数据库中配置 codetype = 字段名 + check

		// 暂时统一对投保人进行校验，若需要单独校验，可以调整为按字段进行校验
		// 若配置为空则会对全部机构进行校验
		if(manageCheck(managecom.substring(0, 4),"appnt")){
			
			if(isNull(id)){
				alert("投保人证件号码不能为空，请核查！");
				return false;
			}
			
			if(isNull(mobile) && isNull(phone) && isNull(homephone)){
				alert("投保人联系电话、固定电话、移动电话不能同时为空，请核查！");
		   		return false;
			}

		}
		
		if(manageCheck(managecom.substring(0, 4),"phone")){

			if(!isNull(phone)){
				if(phone.length < 7){
					alert("投保人联系电话不能少于7位，请核查！");
			   		return false;
				}
			}
			
			// 复核时，对于3个以上重复号码，强制下发问题件
			if(wFlag == 2 && (!isNull(mobile) || !isNull(phone))){
				// 由于重复号码校验速度很慢，因此先对问题件进行判断
				
				// 空代表没有下发问题件 1代表已下发问题件未回销 2代表已回销
				// order by doc 为避免多次下发问题件联系电话问题件 只要有没有回销的 不让通过
				var issueSql = "Select (Case When (Select Docid From Es_Doc_Main Where Doccode = Ci.Prtseq And Subtype = 'TB22') Is Null Then 1 Else 2 End) doc "
						+ "From Lcissuepol Ci Where Errfieldname = '联系电话' And Questionobj Like '投保人%' And Contno = (Select Contno From Lccont Where Prtno = '" + prtno + "') " 
						+ "order by doc";
				var issueResult = easyExecSql(issueSql);
				
				// 页面提示信息
				var alterInf = "";
				if(issueResult == null){
					alterInf = "请下发投保人联系电话问题件";
				} else if(issueResult[0][0] == "1"){
					alterInf = "已下发的联系电话问题件尚未回销，请确认";
				}
				
				if(issueResult == null || issueResult[0][0] == "1"){
					if(!isNull(phone)){
						var phoneSql = "select count(distinct customerno) from lcaddress where phone='" + phone + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
						var result = easyExecSql(phoneSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人联系电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
				
					if(!isNull(mobile)){
						var mobilSql = "select count(distinct customerno) from lcaddress where mobile='" + mobile + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
						var result = easyExecSql(mobilSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人移动电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
					
					if(!isNull(homephone)){
						var mobilSql = "select count(distinct customerno) from lcaddress where homephone='" + homephone + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
						var result = easyExecSql(mobilSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人固定电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
					
				}
			}
		}
		
		if(manageCheck(managecom.substring(0, 4),"agent")){
				// 业务员手机号码校验
				if(!isNull(phone)){
					var mobilSql = "select groupagentcode from laagent where phone='" + phone + "' and idno <> '"+id+"' with ur";
					var result = easyExecSql(mobilSql);
					if(result){
						alert("该投保人联系电话与本方业务员" + result[0][0] + "联系号码相同，请核查！");
						return false;
					}
					var mobilSql = "select groupagentcode from laagent where mobile='" + phone + "' and idno <> '"+id+"' with ur";
					var result = easyExecSql(mobilSql);
					if(result){
						alert("该投保人联系电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
						return false;
					}
					var mobilSql = "select agentcode from laagenttemp where phone='" + phone + "' and name <> '"+name+"' with ur";
					var result = easyExecSql(mobilSql);
					if(result){
						alert("该投保人联系电话与代理销售业务员" + result[0][0] + "联系号码相同，请核查！");
						return false;
					}
				}
				
				if(!isNull(mobile)){
					var phoneSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
					var result = easyExecSql(phoneSql);
					if(result){
						alert("该投保人移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
						return false;
					}
					var phoneSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
					var result = easyExecSql(phoneSql);
					if(result){
						alert("该投保人移动电话与本方业务员" + result[0][0] + "移动号码相同，请核查！");
						return false;
					}
					var mobilSql = "select agentcode from laagenttemp where mobile='" + mobile + "' and name <> '"+name+"' with ur";
					var result = easyExecSql(mobilSql);
					if(result){
						alert("该投保人移动电话与代理销售业务员" + result[0][0] + "手机号码相同，请核查！");
						return false;
					}
				}
				
				if(!isNull(homephone)){
					var phoneSql = "select groupagentcode from laagent where phone='" + homephone + "' and idno <> '"+id+"' with ur";
					var result = easyExecSql(phoneSql);
					if(result){
						alert("该投保人固定电话与本方业务员" + result[0][0] + "移动号码相同，请核查！");
						return false;
					}
					var phoneSql = "select groupagentcode from laagent where mobile='" + homephone + "' and idno <> '"+id+"' with ur";
					var result = easyExecSql(phoneSql);
					if(result){
						alert("该投保人固定电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
						return false;
					}
					var mobilSql = "select agentcode from laagenttemp where phone='" + homephone + "' and name <> '"+name+"' with ur";
					var result = easyExecSql(mobilSql);
					if(result){
						alert("该投保人固定电话与代理销售业务员" + result[0][0] + "固定电话相同，请核查！");
						return false;
					}
				}
			}
	}
	return true;
}

// 判断机构是否需要进行校验
// managecom 机构
// checkflag 校验项
function manageCheck(managecom, checkflag){
	var checkSql = "select (case (select count(1) from ldcode where codetype = '" + checkflag + "check') " +  
			"when 0 then 1 else (select distinct 1 from ldcode where codetype = '" + checkflag + "check' and code = '" + managecom + "') end ) " + 
			"from dual";
	var result = easyExecSql(checkSql);
	if(result){
		if(result[0][0] == "1"){
			// 该机构需要进行校验
			return true;
		}
	}
	// 该机构不需要进行校验
	return false;
}

// 判断是否为空
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}
//校验投保人银行账户信息
function checkBank()
{
    var strSql= easyExecSql("select PayMode,ExPayMode,BankCode, Accname,BankAccNo from LCCont where prtno = '" + fm.PrtNo.value + "'");
 	if(strSql) 
 	{          
		if(strSql[0][0]== "4"||strSql[0][1]=="4")
		{
			if(strSql[0][2] == null ||strSql[0][2] == '')
			{   
				alert("银行代码为空，若修改以后请点击修改按钮！");
				return false;
			}      
			if(strSql[0][3] == null ||strSql[0][3] == '')
			{
				alert("户名为空，若修改以后请点击修改按钮！");
				return false;
			} 
			if(strSql[0][4] == null ||strSql[0][4] == '')
			{
				alert("账号为空，若修改以后请点击修改按钮！");
				return false;
			 }     
		}
	}else{
			alert("未查到该保单信息，请核对数据！")
			return false;	
	}
    return true;
}

// 豁免险保额校验
function checkExemption(){
	var sql = "select 1 from lcpol where prtno = '" + fm.PrtNo.value 
					+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
	var result= easyExecSql(sql);
	if(result && result[0][0] == "1"){
		sql = "select 1 from lcpol where appntno=insuredno and prtno = '" + fm.PrtNo.value 
					+ "' and not exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
		result= easyExecSql(sql);
		if(result && result[0][0] == "1"){
			alert("非豁免险险种的被保人不能为投保人，请核查！");
			return false;
		}
		sql = "select 1 from lccont where payintv=0 and prtno = '" + fm.PrtNo.value + "' ";
		result= easyExecSql(sql);
		if(result && result[0][0] == "1"){
			alert("保单中存在豁免险险种，缴费频次不能为趸缴！");
			return false;
		}	
		var sql1 = "select 1 from lcpol a where prtno = '" + fm.PrtNo.value + "' and exists (select 1 from lmriskapp where riskcode=a.riskcode and kindcode='S') ";
        var sql2 = "select 1 from ldcode1 b where codetype='checkexemptionrisk' and exists ("+sql1+" and a.riskcode=b.code1 )  ";
        var sql3 = "select 1 from ldcode1 c where codetype='checkappendrisk' and exists ("+sql2+" and c.code1=b.code ) ";
		var sql = "select sum(prem) from lcpol lc where prtno = '" + fm.PrtNo.value + "' and ( exists ("+sql2+" and lc.riskcode= b.code) or exists ("+sql3+" and lc.riskcode=c.code) ) ";
		result= easyExecSql(sql);
		if(result && !isNull(result[0][0]) && result[0][0] != "null"){
			var sumPrem = result[0][0];
			sql = "select amnt,appntno,insuredno from lcpol where prtno = '" + fm.PrtNo.value 
					+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
			result= easyExecSql(sql);
			if(sumPrem != result[0][0]){
				alert("豁免险保额异常，请到豁免险险种页面，点击修改按钮，重新计算豁免险保额！");
				return false;
			}
			if(result[0][1] != result[0][2]){
				alert("豁免险险种的被保人必须为投保人，请核查！");
				return false;
			}
		} else {
			alert("保单下尚未添加除豁免险外其他险种，或其他险种总保费为零，请核查！");
			return false;
		}
	}
	return true;
}
function valiMedical(){
	var strSQL = "Select cc.paymode,cc.expaymode,ca.idtype"
			+" From Lccont Cc"
			+" Inner Join Lcappnt Ca"
			+"   On Cc.Contno = Ca.Contno"
			+" Inner Join Lcaddress Cad"
			+"    On Ca.Appntno = Cad.Customerno"
			+"   And Ca.Addressno = Cad.Addressno "
			+" where  cc.prtno = '"+fm.PrtNo.value+"'"
			+" with ur" ;
	var riskResult = easyExecSql(strSQL);
	if(riskResult){
		var payMode = riskResult[0][0];
		var exPayMode = riskResult[0][1];
		var idtype = riskResult[0][2];
		/**
		 * 校验首续期的缴费方式
		 */
		if(payMode == '8' && exPayMode != '8'){
			alert("首期的缴费方式是8-医保个人账户的缴费方式，续期的缴费方式必须是8-医保个人账户！"); 
			return false;
		}
		if(payMode != '8' && exPayMode == '8'){
			alert("首期的缴费方式不是8-医保个人账户的缴费方式，续期的缴费方式不能是8-医保个人账户！"); 
			return false;
		}
		/**
		 * 选择8-医保个人账户转账的缴费方式，必须录入投保人的身份证号
		 */
		if(payMode == '8' && idtype != '0'){
			alert("缴费方式选择8-医保个人账户的缴费方式，证件类型必须是身份证！"); 
			return false;
		}
		
		if(payMode =="8"){
           var sql= "select 1 from LDmedicalcom ld "
	              + "where cansendflag='1' and exists(select 1 from lccont where prtno='"+fm.PrtNo.value+"' and bankcode=ld.medicalcomcode and managecom=ld.comcode) ";
	       
	       var rs = easyExecSql(sql);
           if(rs == null || rs[0][0] == "" || rs[0][0] == "null")
           {
                alert("没有医保编码对应的医保！");
                return false;
           }   
        }
	}
	
	/*
	var riskSQL= " select code from ldcode1 where codetype='medicaloutrisk' and code1 like '"+fm.all('ManageCom').value+"%' ";
	
	var strSQL = "select 1 from lcpol where prtno = '"+fm.PrtNo.value+"' " +
				"and riskcode not in (select code from ldcode1 where CodeType = 'checkappendrisk' " +
				"and code1 in ("+riskSQL+")) " +
				"and riskcode not in ("+riskSQL+") with ur";
				*/
	/*
	 *  #3849 modify by zxs 2018-05-09
	 *   福建分公司（机构代码8635）极其下属机构放开险种为
	 *  232401,123501,240901的缴费方式或续期方式为8医疗个人保险不能通过校验的权限的放开
	 *  #4343 modify by wy 2019-04-18
	 *   福建分公司（机构代码8635）极其下属机构放开险种为
	 *  233001,124501的缴费方式或续期方式为8医疗个人保险不能通过校验的权限的放开
	 */
	var riskSQL= " select code from ldcode1 where codetype='medicaloutrisk' and code1 like '"+fm.all('ManageCom').value+"%' ";
	var strSQL;
	//
	if(fm.all('ManageCom').value.indexOf('8635')==0){
	 strSQL = "select 1 from lcpol where prtno = '"+fm.PrtNo.value+"' " +
		"and riskcode not in (select code from ldcode1 where CodeType = 'checkappendrisk' " +
		"and code1 in ("+riskSQL+")) " +
		"and riskcode not in ("+riskSQL+") and riskcode not in ('232401','123501','240901','335201','231701','333801'," +
		"'231702','333802','240902','232402','335202','233001','124501') with ur";
	}else{
		strSQL = "select 1 from lcpol where prtno = '"+fm.PrtNo.value+"' " +
		"and riskcode not in (select code from ldcode1 where CodeType = 'checkappendrisk' " +
		"and code1 in ("+riskSQL+")) " +
		"and riskcode not in ("+riskSQL+") with ur";	
	}

	var riskResult = easyExecSql(strSQL);
	if(riskResult){
	var payModeStr = "select paymode from lccont where prtno ='" + fm.PrtNo.value+"'";
	var payModeResult = easyExecSql(payModeStr);
	if(payModeResult){
		if(payModeResult[0][0] == '8'){
			alert("你录入了不支持医保个人账户转账的险种，请查看！");
			return false;
		}
		}
	}
	return true;
}

function checkMainSubRiskRela(){
	var getPolSQL = "select riskcode,mainpolno from lcpol where prtno = '"+fm.PrtNo.value+"' and mainpolno != polno ";
	var tPolResult = easyExecSql(getPolSQL);
	if(tPolResult){
		for(var i=0;i<tPolResult.length;i++){
			var getMainRiskSQL = "select riskcode from lcpol where polno = '"+tPolResult[i][1]+"' ";
			var tMainRiskResult = easyExecSql(getMainRiskSQL);
			if(tMainRiskResult){
				var getRelaSQL = "select 1 from ldcode1 where codetype = 'mainsubriskrela' and code = '"+tPolResult[i][0]+"' and code1 = '"+tMainRiskResult[0][0]+"' ";
				var tRelaResult = easyExecSql(getRelaSQL);
				if(!tRelaResult){
					alert("附加险"+tPolResult[i][0]+",与对应的主险"+tMainRiskResult[0][0]+",没有主附险关系！");
					return false;
				}
			}else{
				alert("获取险种"+tPolResult[i][0]+"的主险信息失败！");
				return false;
			}
		}
	}
	return true;
}

function check121501(){
	var getPolSQL = "select riskcode,mainpolno,mult from lcpol where prtno = '"+fm.PrtNo.value+"' and mainpolno != polno and riskcode = '121501' ";
	var tPolResult = easyExecSql(getPolSQL);
	if(tPolResult){
		for(var i=0;i<tPolResult.length;i++){
			var getMainRiskSQL = "select prem,mult,payintv,riskcode,insuyear,insuyearflag from lcpol where polno = '"+tPolResult[i][1]+"' ";
			var tMainRiskResult = easyExecSql(getMainRiskSQL);
			if(tMainRiskResult){
				if(tMainRiskResult[i][2] == "0"){
					alert("121501不能附加在主险缴费频次为趸缴的产品上!");
					return false;
				}
				/*
				if(parseFloat(tMainRiskResult[i][0]) < 1500){
					alert("121501选择的主险期交保费不能小于1500!");
					return false;
				}
				if((tPolResult[i][2] == "3" || tPolResult[i][2] == "4" || tPolResult[i][2] == "5") && parseFloat(tMainRiskResult[i][0]) < 3000){
					alert("121501选择3-5档时，主险期交保费须大于等于3000!");
					return false;
				}
				*/
				
				var tSumPremSQL = "select sum(lcp.prem) from lcpol lcp where lcp.polno = '"+tPolResult[i][1]+"' or (lcp.mainpolno = '"+tPolResult[i][1]+"' and lcp.riskcode != '121501' and exists (select 1 from ldcode1 where codetype = 'checkappendrisk' and code1 = '"+tMainRiskResult[i][3]+"' and code = lcp.riskcode)) ";
				var tSumPremResult = easyExecSql(tSumPremSQL);
				if(parseFloat(tSumPremResult[0][0]) < 1500){
					alert("121501选择的主险及绑定附加险期交保费和不能小于1500!");
					return false;
				}
				if((tPolResult[i][2] == "3" || tPolResult[i][2] == "4" || tPolResult[i][2] == "5") && parseFloat(tSumPremResult[0][0]) < 3000){
					alert("121501选择3-5档时，主险及绑定附加险期交保费和须大于等于3000!");
					return false;
				}
				
				var tRiskAppSQL = "select RiskPeriod,RiskType4 from lmriskapp where riskcode = '"+tMainRiskResult[i][3]+"' ";
				var tRiskAppResult = easyExecSql(tRiskAppSQL);
				if(tRiskAppResult){
					if(tRiskAppResult[0][0] != "L"){
						alert("121501附加的主险必须为长期险!");
						return false;
					}
					if(tRiskAppResult[0][1] == "4"){
						alert("121501不能附加在主险为万能型的产品上!");
						return false;
					}
				}else{
					alert("获取险种信息失败!");
					return false;
				}
			}
		}
	}
	return true;
}

function check332301(){
   var sql = "select distinct lcp.insuredno,lcp.riskcode from lcpol lcp,ldcode ld where lcp.riskcode = ld.code and ld.codetype='checkmainlawssame' and prtno = '"+fm.PrtNo.value+"' " ;
   var Result = easyExecSql(sql);
   var Result1;
   if(Result){
      var length=Result.length;
      for(var i=0;i<length;i++){
         var riskcode=Result[i][1];
         var insuredno=Result[i][0];
         //保险期间
         var  insuyear = "select 1 from lcpol a ,lcpol b where a.riskcode='"+riskcode+"' and a.mainpolno=b.polno "
                       + " and a.insuyear=b.insuyear and a.insuyearflag=b.insuyearflag and a.prtno = '"+fm.PrtNo.value+"' and a.insuredno='"+insuredno+"'" ;
         Result1 = easyExecSql(insuyear);
         if(!Result1){
            alert("险种"+riskcode+"与主险的保险期间不一致，请检查");
            return false;
         } 
         //缴费期间
         var payendyear =  "select 1 from lcpol a ,lcpol b where a.riskcode='"+riskcode+"' and a.mainpolno=b.polno "
                       + " and a.PayEndYear=b.PayEndYear and a.PayEndYearFlag=b.PayEndYearFlag and a.prtno = '"+fm.PrtNo.value+"' and a.insuredno='"+insuredno+"'" ;        
         Result1 = easyExecSql(payendyear);
         if(!Result1){
            alert("险种"+riskcode+"与主险的缴费期间不一致，请检查");
            return false;
         } 
         //缴费频次
         var payintv =  "select 1 from lcpol a ,lcpol b where a.riskcode='"+riskcode+"' and a.mainpolno=b.polno "
                     + " and a.PayIntv=b.PayIntv  and a.prtno = '"+fm.PrtNo.value+"' and a.insuredno='"+insuredno+"'" ;           
         Result1 = easyExecSql(payintv);
         if(!Result1){
            alert("险种"+riskcode+"与主险的缴费频次不一致，请检查");
            return false;
         }
      }     
   }
   return true;
}

function approveClick(){

   var tSelNo = InsuredGrid.getSelNo() - 1;

   if("-1" == tSelNo)
   {
            alert("未有选择的被保人信息！");
            return false;
   }
   var oneRowResult = InsuredGrid.getRowData(tSelNo);
        

   var arr = easyExecSql(" select Name,Sex,Birthday,IDType,IDNo,"
                       + " RelationToAppnt "
                       + " from lcinsured where prtno='"+fm.PrtNo.value+"' and insuredno='"+oneRowResult[0]+"' ");
            
   if(arr[0][0]!=fm.Name.value){
      alert("录入的被保人姓名与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][1]!=fm.Sex.value){
      alert("录入的被保人性别与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][2]!=fm.Birthday.value){
      alert("录入的被保人出生日期与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][3]!=fm.IDType.value){
      alert("录入的被保人证件类型与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][4]!=fm.IDNo.value){
      alert("录入的被保人证件号码与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][5]!=fm.RelationToAppnt.value){
      alert("录入的被保人与投保人关系与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }

   var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ; 
   showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   var tAction = fm.action;
   fm.action="./ApproveContSave.jsp?LoadFlag=" + LoadFlag + "&CustomerType=01&customerNo="+oneRowResult[0] ;
   fm.submit();
   fm.action=tAction;
   
}

function afterSubmit1( FlagStr, content )
{

	showInfo.close();
	window.focus();
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
	}

}

function customerClick(){
     var tSelNo = InsuredGrid.getSelNo() - 1;

     if("-1" == tSelNo)
     {
        alert("未选择需要检测的被保人！");
        return false;
     }
     var oneRowResult = InsuredGrid.getRowData(tSelNo);
     
     var sFeatures = "status:no;help:0;close:0;dialogWidth:500px;dialogHeight:500px;resizable=1";
	 showInfo = window.open( "./SuspectedCustomerInfo.html?cusType=1&cusNo="+oneRowResult[0]+"&prtNo="+prtNo,"",sFeatures);
}

function checkAppInsr(relation,name,birthday){
    if(relation=="03"){
       var appday = easyExecSql("select AppntBirthday + 18 year from lcappnt where prtno='"+fm.PrtNo.value+"'");
       var fdate = new Date(Date.parse(appday[0][0].replace(/-/g,"/")));
       var cdate = new Date(Date.parse(birthday.replace(/-/g,"/")));
      
       if(Date.parse(cdate)-Date.parse(fdate) < 0){
           if(!confirm("被保人"+name+"与投保人年龄差距小于18岁，是否继续？")){
               return false;
           }
       }
    }
    return true;
}

function chenkIdNo(type,idtype,idno,name){
        var str = "";
        if(type=="1"){
          str="投保人";
        }else{
          str="被保人"+name;
        }
        if(idtype == "" || idtype == null){
             alert("证件类型不能为空！");
             return false;
        }else{
             var arr = easyExecSql("select code1,codealias,othersign from ldcode1 where codetype='checkidtype' and code='"+idtype+"'");
             if(arr!=null){
                var othersign = arr[0][2];
                var first = arr[0][0];
                var two = arr[0][1];
                if("double"==othersign){
                   if(idno.length!=first&&idno.length!=two){
                       alert("录入的"+str+"证件号码有误，请检查！");
                       return false;
                   }
                }
                if("min"==othersign){
                   if(idno.length<first){
                      alert("录入的"+str+"证件号码有误，请检查！");
                      return false;
                   }
                }
                if("between"==othersign){
                   if(idno.length<first||idno.length>two){
                      alert("录入的"+str+"证件号码有误，请检查！");
                      return false;
                   }
                }
                
             }else{
                alert("选择的"+str+"证件类型有误，请检查！");
                return false;
             }
        }
        return true;
}

function chenkHomePhone(type,homecode,homenumber,flag){
     var str = "";
     if(type=="1"){
        str="投保人";
     }else{
        str="被保人";
     }
    if(homecode == "" || homecode == null){
         if(homenumber != "" && homenumber != null){
            alert(str+"固定电话区号未进行录入！");
            return false;
         }
    }else{
        if(homecode.length!=3 && homecode.length!=4 || !isInteger(homecode)){
            alert(str+"固定电话区号录入有误，请检查！");
            return false;
        }
        if(homenumber == "" || homenumber == null){
            alert(str+"固定电话号码未进行录入！");
            return false;
        }
        if(homenumber.length!=7&&homenumber.length!=8&&homenumber.length!=10||!isInteger(homenumber)){
            alert(str+"固定电话号码录入有误，请检查！");
            return false;
        }else{
           if(homenumber.length==10){
               if(homenumber.substring(0, 3)!=400&&homenumber.substring(0, 3)!=800){
                  alert(str+"固定电话号码录入有误，请检查！");
                  return false;
               }
           }
        
        }
    
    }
    return true;
}

function checkAddress(type,PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity,flag,name){
   var str = "";
   if(type=="1"){
      str="投保人";
   }else{
      str="被保人" + name;
   }
		if(PostalProvince == "" || PostalProvince == null || PostalCity == "" || PostalCity == null
		   || PostalCounty == "" || PostalCounty == null || PostalStreet == "" || PostalStreet == null
		   || PostalCommunity == "" || PostalCommunity == null){
		     alert(str+"联系地址未填写!");
		     return false;
		}
		
		//校验省市县信息是否存在
		var provinceFlag =  easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+PostalProvince+"' ");
		if(!provinceFlag){
			   alert(str + "省（自治区直辖市）信息录入不正确，请录入省（自治区直辖市）编码！")
			   return false;
		}
		var cityFlag =  easyExecSql("select 1 from ldcode1 where codetype='city1' and code='"+PostalCity+"' ");
		if(!cityFlag){
			   alert(str + "市信息录入不正确，请录入市编码！")
			   return false;
		}
		var county1Flag =  easyExecSql("select 1 from ldcode1 where codetype='county1' and code='"+PostalCounty+"' ");
		if(!county1Flag){
			   alert(str + "县（区）信息录入不正确，请录入县（区）编码！")
			   return false;
		}
		//校验联系地址级联关系
		var checkCityError = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+PostalProvince+"' "
				   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+PostalCity+"' )");
		if(checkCityError == null || checkCityError == ""){
			   alert(str +"省、市地址级联关系不正确，请检查！");
			   return false;
		}
		var checkCityError2 = easyExecSql("select 1 from ldcode1 where codetype='city1' and code='"+PostalCity+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+PostalCounty+"')");
		if(!checkCityError2){
			   alert(str +"市、县地址级联关系不正确，请检查！");
			   return false;
		}
	   //当省级单位下没有市县级单位或市级单位下没有县级单位的时候改变拼接完整地址的方式
		if(flag=="yes"){
			if(fm.PostalCity.value == '空' && fm.PostalCounty.value == '空'){
				fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalStreet.value+fm.PostalCommunity.value;
			}else if(fm.PostalCity.value != '空' && fm.PostalCounty.value == '空') {
				fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalCity.value+fm.PostalStreet.value+fm.PostalCommunity.value;
			}else{
				fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalCity.value+fm.PostalCounty.value+fm.PostalStreet.value+fm.PostalCommunity.value;
			}	
		}
	   return true;
}

function checkMobile(type,mobile,name){
   var str = "";
   if(type=="1"){
      str="投保人";
   }else{
      str="被保人"+name;
   }
   if(mobile != null && mobile !="") {
	    if(!isInteger(mobile) || mobile.length != 11){
		    alert(str+"移动电话需为11位数字，请核查！");
 	      	return false;
	    } 
	}
   if(mobile != "" && mobile != null){
      if(mobile.length!=11){
          alert(str+"移动电话不符合规则，请核实！");
          return false;
      }else{
         if(mobile.substring(0,2)!=13&&mobile.substring(0,2)!=16&&mobile.substring(0,2)!=14&&mobile.substring(0,2)!=15&&mobile.substring(0,2)!=18&&mobile.substring(0,2)!=17&&mobile.substring(0,2)!=19){
            alert(str+"移动电话不符合规则，请核实！");
            return false;
         }
      }
   }else if(type=="1"){
      if(!confirm("投保人移动电话为空，是否继续？")){
         return false;
	  }
   }
   return true;
}

function checkAllInsu(tFlag){
   var mobile = fm.Mobile.value;
   var idtype = fm.IDType.value;
   var idno = fm.IDNo.value;
   var realtion = fm.RelationToAppnt.value;
   var birthday = fm.Birthday.value;
    if(!chenkIdNo(2,idtype,idno,'')){
	  return false;
	}
	
	if(!checkAppInsr(realtion,'',birthday)){
	  return false;
	} 
	
	if(!checkMobile(2,mobile,'')){
	  return false;
	}
	if(tFlag == "1"){
		var insuredAddressData = easyExecSql("select ad.PostalProvince,ad.PostalCity,ad.PostalCounty,ad.PostalStreet,ad.PostalCommunity,ap.name,ad.homephone,ad.email " +
				" from lcaddress ad,lcinsured ap where ad.customerno=ap.insuredno and ad.addressno=ap.addressno and ap.contno='"+fm.ContNo.value+"'");
		for(var i=0;i<insuredAddressData.length;i++){
			var PostalProvince = insuredAddressData[i][0];
			var PostalCity = insuredAddressData[i][1];
			var PostalCounty = insuredAddressData[i][2];
			var PostalStreet = insuredAddressData[i][3];
			var PostalCommunity = insuredAddressData[i][4];
			var name = insuredAddressData[i][5];
			var homephone = insuredAddressData[i][6];
			var email = insuredAddressData[i][7];
			if(!checkAddress(2,PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity,'',name)){
				return false;
			}
			if(!isNull(email)){
				   var checkemail =CheckEmail(email);
				   if(checkemail !=""){
					   alert("被保人"+checkemail);
					   return false;
				   }
			   }
			if(!isNull(homephone)){
				   var checkhomephone =CheckFixPhone(homephone);
				   if(checkhomephone !=""){
					   alert("被保人"+checkhomephone);
					   return false;
				   }
			}
		}
	}else{
		var PostalProvince = fm.Province.value;
		var PostalCity = fm.City.value;
		var PostalCounty = fm.County.value;
		var PostalStreet = fm.PostalStreet.value;
		var PostalCommunity = fm.PostalCommunity.value;
		var name = fm.Name.value;
		var homephone = fm.HomePhone.value;
		if(!checkAddress(2,PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity,'yes',name)){
			return false;
		}
		if(!isNull(homephone)){
			   var checkhomephone =CheckFixPhone(homephone);
			   if(checkhomephone !=""){
				   alert(checkhomephone);
				   return false;
			   }
		}
	}
   return true;
}

function checkInsured(){
	var tSql = "select ca.name,ca.idno,ca.idtype,ca.birthday,ca.sex,cad.mobile,ca.RelationToAppnt,cad.phone,cad.homephone,ca.nativeplace,ca.nativecity  from lcinsured ca "
	         + "inner join lcaddress cad on cad.customerno=ca.insuredno and cad.addressno=ca.addressno "
	         + " where ca.prtno='" + fm.PrtNo.value + "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		for(var index = 0; index < appntResult.length; index++){
			// 被保人姓名
			var name = appntResult[index][0];
			// 证件号
			var id = appntResult[index][1];
			// 证件类型
			var idtype = appntResult[index][2];
			// 出生日期
			var birthday = appntResult[index][3];
			// 性别
			var sex = appntResult[index][4];
			//移动电话
			var mobile = appntResult[index][5];
			//与投保人关系
			var relation = appntResult[index][6];
			// 联系电话
		    var phone = appntResult[index][7];
		    // 家庭电话
		    var homephone = appntResult[index][8];
		    // 国籍
		    var nativeplace = appntResult[index][9];
		    // 国家
		    var nativecity = appntResult[index][10];
		    
			if(idtype == "0"){
		
				if(birthday != getBirthdatByIdNo(id)){
					alert("被保人" + name + "身份证号与出生日期不符，请核查！");
					return false;
				}
			
				if(sex != getSexByIDNo(id)){
					alert("被保人" + name + "身份证号与性别不符，请核查！");
					return false;
				}
			}
			if(!chenkIdNo(2,idtype,id,name)){
	          return false;
	        }
	        
	        if(!checkCity(2,name,nativeplace,nativecity)){
	            return false;
	        }
	        
	        // 共有校验
		    if(!isNull(mobile)) {
			    if(!isInteger(mobile) || mobile.length != 11){
				    alert("被保人" + name + "移动电话需为11位数字，请核查！");
		   	      	return false;
			    } 
			    if(!checkMobile(2,mobile,name)){
			        return false;
			    }
		    }
		    
		    if(!checkAppInsr(relation,name,birthday)){
		       return false;
		    }
		    
		    if(!isNull(phone)){
				var mobilSql = "select groupagentcode from laagent where phone='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人联系电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select groupagentcode from laagent where mobile='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人联系电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + phone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人联系电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
			
			if(!isNull(mobile)){
				var phoneSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人移动电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where mobile='" + mobile + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人移动电话与代理销售业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
			}
			if(!isNull(homephone)){
				var phoneSql = "select groupagentcode from laagent where phone='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人固定电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where mobile='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人固定电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + homephone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人固定电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
			
			
		}
	}

	return true;
}
function checkSaleChnlInfo(){
	var tSQL = "select managecom,agentcom,agentcode,salechnl from lccont where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单数据失败！");
		return false;
	}
	var tManageCom = arr[0][0];
	var tAgentCom = arr[0][1];
	var tAgentCode = arr[0][2];
	var tSaleChnl = arr[0][3];
	
	var tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
	var arrCode = easyExecSql(tSQLCode);
	if(!arrCode){
		alert("业务员与管理机构不匹配！");
		return false;
	}
	var agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
	                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
                     + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
                     + "union all "
		             + "select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
		             + "and '10' = '" + tSaleChnl + "' and a.BranchType2 = '04'  "
		             + "union all "
		             + "select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
		             + "and '03' = '" + tSaleChnl + "' and a.BranchType2 = '04'  "
		             + "union all "
		             + "select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
		             + "and '02' = '" + tSaleChnl + "' and a.BranchType = '2' and a.BranchType2 = '02'  ";
    var arrAgentCode = easyExecSql(agentCodeSql);
    if(!arrAgentCode){
    	alert("业务员和销售渠道不匹配！");
		return false;
    }
	if(tSaleChnl == "03" || tSaleChnl == "04" || tSaleChnl == "10" || tSaleChnl == "15" || tSaleChnl == "20"){
	    if(tAgentCom == "" || tAgentCom == null || tAgentCom == "null"){
	        alert("请录入中介机构！");
			return false;
	    }
		var tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
		var arrCom = easyExecSql(tSQLCom);
		if(!arrCom){
			alert("中介机构与管理机构不匹配！");
			return false;
		}
		var tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
		var arrCom = easyExecSql(tSQLComCode);
		if(!arrCom){
			alert("业务员与中介机构不匹配！");
			return false;
		}
	}
	return true;
}

function checkMixCom(){
    var tSQLMix = "select prtno,Crs_SaleChnl,Crs_BussType,GrpAgentIDNo,GrpAgentCom,GrpAgentCode,GrpAgentName from lccont where prtno='" + fm.PrtNo.value + "'";
	var arrMix = easyExecSql(tSQLMix);
	if(!arrMix){
		alert("获取保单数据失败！");
		return false;
	}
	var sale= arrMix[0][1];
	var buss= arrMix[0][2];
	var id= arrMix[0][3];
	var com= arrMix[0][4];
	var code= arrMix[0][5];
	var name= arrMix[0][6];
	if(sale==""&&buss==""&&id==""&&com==""&&code==""&&name==""){
	    return true;
	}else{
	   if(sale!=""&&buss!=""&&id!=""&&com!=""&&code!=""&&name!=""){
	       if(code.length!=10){
	          alert("对方业务员工号不为集团统一工号！");
		      return false;
	       }else if((sale=="01"&&code.substring(0,1)!=1)||(sale=="02"&&code.substring(0,1)!=3)){
	          alert("对方业务员工号不为集团统一工号！");
		      return false;
	       }
	   }else{
	      alert("交叉销售信息填写有误，请对其进行修改！");
		  return false;
	   }
	}
    return true;
}

function controlNativeCity(displayFlag)
{
  if(fm.NativePlace.value=="OS"){
    fm.all("NativePlace1").style.display = displayFlag;
	fm.all("NativeCity1").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="国家";
  }else if(fm.NativePlace.value=="HK"){
    fm.all("NativePlace1").style.display = displayFlag;
	fm.all("NativeCity1").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="地区";
  }else{
    fm.all("NativePlace1").style.display = "none";
	fm.all("NativeCity1").style.display = "none";
  }
//zxs
	if(fm.IDType.value == "a"||fm.IDType.value == "b"){
			fm.all("PassNumer1").style.display = "";
		    fm.all("PassIDNo1").style.display = "";
	}else{
		    fm.all("PassNumer1").style.display = "none";
			fm.all("PassIDNo1").style.display = "none";
		 }
}

function checkNative(){
   if(fm.NativePlace.value=="OS"||fm.NativePlace.value=="HK"){
      if(fm.NativeCity.value==""||fm.NativeCity.value==null){
         alert("被保人所属国家/地区未录入！");
         return false;
      }
   }
   return true;
}

function checkCity(type,name,place,city){
   var str = "";
   if(type=="1"){
      str="投保人";
   }else{
      str="被保人:"+name;
   }
   if((place=="OS"||place=="HK")&&(city==""||city==null)){
      alert(str+"所属国家/地区不能为空！");
      return false;
   }

   return true;
}

function chenkBnfBlack(){
      var insuredSql= " select insuredno ,insuredname,paymode ,sum(paymoney) "
             + " from "
             + " (select insuredno ,insuredname,paymode ,"
             + "(Case When Payintv = '0' Then Prem + Nvl(Supplementaryprem, 0) "
             + " When Payyears <> 0 Then Prem * Double(12 / Payintv) * Payyears + Nvl(Supplementaryprem, 0) "
             + " When Payyears = 0 Then "
             + " Case When Payintv='12' Then Prem + Nvl(Supplementaryprem, 0) "
             + " Else  Prem * (to_month(payenddate)-to_month(cvalidate))/payintv + Nvl(Supplementaryprem, 0) End "
             + " End )paymoney "
             + " from lcpol where prtno='" + fm.PrtNo.value + "'"
             + " ) temp group by insuredno ,insuredname,paymode "
             +" having ((paymode='1' and sum(paymoney)>=20000) or (paymode<>'1' and sum(paymoney)>=200000)) ";
      var insuredResult = easyExecSql(insuredSql); 
      var bnfSql="select a.name from lcbnf a,lcpol b where BnfType='1' and (trim(a.NativePlace) is null or trim(a.NativePlace)=''"
                +" or trim(a.OccupationCode) is null or trim(a.OccupationCode)='' "
                +" or trim(a.Phone) is null or trim(a.Phone)='' "
                +" or trim(a.PostalAddress) is null or trim(a.PostalAddress)='' "
                +" or trim(a.IDStartDate) is null or trim(a.IDStartDate)='' "
                +" or trim(a.Sex) is null or trim(a.Sex)='' "
                +" or trim(a.BnfType) is null or trim(a.BnfType)='' "
                +" or trim(a.BnfGrade) is null or trim(a.BnfGrade)='' "
                +" or trim(a.BnfLot) is null or trim(a.BnfLot)='' "
                +" or trim(a.Name) is null or trim(a.Name)='' "
                +" or trim(a.IDType) is null or trim(a.IDType)='' "
                +" or trim(a.IDNo) is null or trim(a.IDNo)='' "
                +" or trim(a.IDEndDate) is null or trim(a.IDEndDate)='' ) "
                +" and (a.RelationToInsured not in ('01','02','03','04','05' ) "
                +" or trim(a.RelationToInsured) is null or trim(a.RelationToInsured)='' )"
                +" and a.contno=b.contno and a.polno=b.polno and b.prtno='" + fm.PrtNo.value + "' ";

      if(insuredResult){
		for(var index = 0; index < insuredResult.length; index++){
		    bnfSql+=" and a.insuredno='"+insuredResult[index][0]+"' ";
		    var bnfResult = easyExecSql(bnfSql); 
		    if(bnfResult){
		         alert("达“反洗钱”标准且非法定继承人以外的身故受益人："+bnfResult[0][0]+" 信息不全!请对其进行【受益人信息补录】！");
		         return false;
		    }		  
		}
	  }
          
      return true;
}

function checkCard()
{
	//体检件回销校验
	var checkSql = "select MissionProp1 from LWMission where processid ='0000000003' and MissionProp2='"
	              + fm.ContNo.value + "' and activityid in ('0000001106','0000001111')";
	var result = easyExecSql(checkSql);
	if(result)
	{
		alert("体检件未回销，不能新单复核通过！");
		return false;
	}
	return true;
}

function getAudioAndVideo(){
  window.open("http://10.214.4.143:9001/prpcard/video/"+trim(fm.PrtNo.value)+".rar","window1");
  fm.VideoFlag.value="1"; 
}

function checkVideo(){
   var videoSql="select 1 from lcpol lcp,lmriskapp lm,lccont lcc where lcc.contno=lcp.contno and lcc.managecom like '8644%' "
               +"and lcc.contno='"+ fm.ContNo.value + "' and lcp.riskcode=lm.riskcode and Year(lcc.PolApplyDate - lcc.AppntBirthday)>=60 and lm.RiskPeriod='L' ";
   var result = easyExecSql(videoSql);
   if(result&&fm.VideoFlag.value!='1'){
      alert("请进行录音录像调阅！");
      return false;
   }
   videoSql="select 1 from lccont where managecom like '8691%' and contno='"+ fm.ContNo.value + "'";
   result = easyExecSql(videoSql);
   if(result&&fm.XSFlag.value!='Y'){
      alert("未进行销售过程全程记录，不能复核通过！");
      return false;
   }
   return true;
}

//为代码赋汉字
function setCodeName(fieldName, codeType, code)
{
  var sql = "select CodeName('" + codeType + "', '" + code + "') from dual";
  var rs = easyExecSql(sql)
  if(rs)
  {
    fm.all(fieldName).value = rs[0][0];
  }
}

function checkRelation(){
  var sql = "select 1 from lcinsured where prtno='"+fm.PrtNo.value+"' and SequenceNo='1' and RelationToMainInsured<>'00' ";
  var rs = easyExecSql(sql)
  if(rs)
  {
    alert("第一被保险人与第一被保险人关系只能为本人！");
	return false;
  }
  return true;
}

function checkSYAll(){

	   var sql = "select * "
	            + " from lcpol lc,lmriskapp lm "
	            + " where lc.prtno='" + fm.PrtNo.value + "' "
	            + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' ";
	   var rs = easyExecSql(sql);
	   var mobile;
	   if(rs){
	      sql = "select lc.taxflag,lc.taxno,lc.taxpayertype,lc.BatchNo,lc.transflag,lc.creditcode,lc.GTaxNo,lc.GOrgancomCode "
	            + " from lccontsub lc "
	            + " where lc.prtno='" + fm.PrtNo.value + "' ";
	      rs = easyExecSql(sql);
	      if(rs){
	         var taxflag=rs[0][0];
	         var taxno=rs[0][1];
	         var payertype=rs[0][2];
	         var batchno=rs[0][3];
	         var transflag=rs[0][4];
	         var creditcode=rs[0][5];
	         var GTaxNo=rs[0][6];
	         var GOrgancomCode=rs[0][7];
	         if(taxflag==""||taxflag==null){
	            alert("承保为税优产品，请填写税优投保标识！");
	            return false;
	         }
	         
	         if(transflag==""||transflag==null){
	            alert("承保为税优产品，请填写投保单来源！");
	            return false;
	         }
	         
	         if(payertype==""||payertype==null){
	            alert("承保为税优产品，请填写个税征收方式！");
	            return false;
	         }
	         
//	         if(taxflag=="1"){
//	             alert("承保为税优产品，仅限团体投保！");
//	             return false;
//	          }
	         
	         if(taxflag=="1"&&((taxno==null||taxno=="")&&(creditcode==null||creditcode=="")&&(GTaxNo==null||GTaxNo=="")&&(GOrgancomCode==null||GOrgancomCode==""))){
	             alert("税优投保标识为个人，个人税务登记证、个人社会信用代码、单位税务登记证代码 、单位社会信用代码 信息需录入一项！");
	             return false;
	          }
	         if(taxflag=="2"&&(batchno==null||batchno=="")){
	            alert("税优投保标识为团体，需录入团体客户信息！");
	            return false;
	         }   
	      }else{
	    	     alert("承保为税优产品，请确认是否录入了税优相关字段！");
	    	     return false;
	      }
	      
	      sql = "select 1 "
	          + " from lcpol lc,lmriskapp lm,LCCustomerImpartParams li "
	          + " where lc.prtno='" + fm.PrtNo.value + "' "
	          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
	          + " and lc.contno=li.contno and lc.insuredno=li.customerno"
	          + " and li.impartcode='170' and li.impartver='001' and li.impartparam='Y' ";
	      rs = easyExecSql(sql);
	      if(!rs){
	          alert("被保险人医疗费用支付方式未勾选！");
	          return false;
	      }
	      
//	      sql = "select 1 "
//	          + " from lcpol lc,lmriskapp lm,LCCustomerImpartParams li "
//	          + " where lc.prtno='" + fm.PrtNo.value + "' "
//	          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
//	          + " and lc.contno=li.contno and lc.insuredno=li.customerno"
//	          + " and li.impartcode='170' and li.impartver='001' and li.impartparam='Y' "
//	          + " and exists (select 1 from ldcode1 where codetype='SYRiskNotImpartCode' and code=lc.riskcode and code1=li.impartparamno )";
//	      rs = easyExecSql(sql);
//	      if(rs){
//	          alert("被保险人医疗费用支付方式与险种不匹配！");
//	          return false;
//	      }
	      
	      sql = "select li.customerno,count(distinct li.impartcode) "
	          + " from lcpol lc,lmriskapp lm,LCCustomerImpart li "
	          + " where lc.prtno='" + fm.PrtNo.value + "' "
	          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
	          + " and lc.contno=li.contno and lc.insuredno=li.customerno"
	          + " and li.impartcode in ('010','170','180','190','110','260','270') and li.impartver='001' "
	          + " group by li.customerno having count(distinct li.impartcode)<7 ";
	      rs = easyExecSql(sql);      
	      if(rs){
	          alert("承保为税优产品，被保人告知第：1、2、3、4、12、20、21 项 为必录项！");
	          return false;
	      }
	      
	      sql =  "select 1 "
	          + " from lcpol lc,lmriskapp lm  "
	          + " where lc.prtno='" + fm.PrtNo.value + "' "
	          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
	          + " and not exists (select 1 from LCCustomerImpart where impartcode ='240' and impartver='001' and customerno=lc.insuredno and contno=lc.contno )"
	          + " and lc.insuredsex='1' and lc.insuredappage>=16  ";
	      rs = easyExecSql(sql);
	      if(rs){
	          alert("承保为税优产品，被保人为16周岁及以上女性，告知第18项为必录项！");
	          return false;
	      }
	      
	      sql =  "select 1 "
	          + " from lcpol lc,lmriskapp lm  "
	          + " where lc.prtno='" + fm.PrtNo.value + "' "
	          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
	          + " and not exists (select 1 from ldcode where codetype='sypremnult' and code=lc.riskcode) "
	          + " and exists (select 1 from LCCustomerImpartParams where impartcode ='260' and impartver='001' and customerno=lc.insuredno and contno=lc.contno and impartparam='Y')"
	          + " and lc.mult<>2  ";
	      rs = easyExecSql(sql);
	      if(rs){
	          alert("承保为税优产品，被保人投保保额不符合要求！");
	          return false;
	      }
	      
	      sql = "select 1 "
	            + " from lcappnt lca,lcaddress lcd "
	            + " where lca.prtno='" + fm.PrtNo.value + "' "
	            + " and lca.appntno=lcd.customerno and lca.addressno=lcd.addressno And (Length(Trim(lcd.Mobile)) <> 11 Or lcd.Mobile Is Null Or lcd.Mobile = '' Or  lcd.Mobile not like '1%') "
	            + " union all "
	            + " select 1 "
	            + " from lcinsured lca,lcaddress lcd "
	            + " where lca.prtno='" + fm.PrtNo.value + "' "
	            + " and lca.insuredno=lcd.customerno and lca.addressno=lcd.addressno And (Length(Trim(lcd.Mobile)) <> 11 Or lcd.Mobile Is Null Or lcd.Mobile = '' Or  lcd.Mobile not like '1%') ";
	     rs = easyExecSql(sql);
	     if(rs){
	         alert("承保为税优产品，投、被保人移动电话为必录项，请核查是否录入，录入是否正确！");
	         return false;
	     }
	   }else {
		   var sql2 = "select lc.taxflag,lc.transflag  from lccontsub lc "
	            + " where lc.prtno='" + fm.PrtNo.value + "' ";
	     var rs2 = easyExecSql(sql2);
	      if(rs2){
	    	  if((rs2[0][0]!="")||(rs2[0][1]!="")){
			   alert("未承保税优产品,请检查是否录入了税优专属字段");
			   return false;
	    	  }
		   }
	   }
	   
	   return true;  
	    
}

function checkTrans(){

   var sql = "select 1 "
            + " from lcpol lcp,lccontsub lcc,lmriskapp lm "
            + " where lcp.prtno='" + fm.PrtNo.value + "' "
            + " and lcp.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
            + " and lcp.prtno=lcc.prtno and lcc.transflag<>'1' "
            + " and exists (select 1 from lstransinfo where prtno=lcc.prtno)";
   var rs = easyExecSql(sql);
   if(rs){
      alert("该单不为税优转入保单，请对税优转入保单信息进行删除！");
      return false;
   }
   
   sql = "select lcp.agentcode,lcp.managecom "
            + " from lcpol lcp,lccontsub lcc,lmriskapp lm "
            + " where lcp.prtno='" + fm.PrtNo.value + "' "
            + " and lcp.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
            + " and lcp.prtno=lcc.prtno and lcc.transflag='1' ";
   rs = easyExecSql(sql);
   if(rs){
      sql = "select case when ls.agentcode=lc.agentcode then '1' else '0' end,case when 1=(select count(1) from ldfinbank where bankaccno=ls.accno and managecom=lc.managecom) then '1' else '0' end "
          + " from lstransinfo ls,lccont lc where lc.prtno='" + fm.PrtNo.value + "' and lc.prtno=ls.prtno ";
      rs = easyExecSql(sql);
      if(!rs){
         alert("该单为税优转入保单，请进行税优转入保单信息录入！");
         return false;
      }else{
         var flag1=rs[0][0];
         var flag2=rs[0][1];
         if(flag1=='0'){
            alert("税优转入保单录入联系人与投保单业务员不为同一人！");
            return false;
         }
         if(flag2=='0'){
            alert("税优转入保单录入归集帐号不为投保单管理机构归集帐号！");
            return false;
         }     
      }
      
   }
   
   sql = "select CheckFlag,CheckInfo,lcc.transflag "
      + " from lccontsub lcc,lcpol lcp,lmriskapp lm where lcc.prtno='" + fm.PrtNo.value + "' "
      + " and lcc.prtno=lcp.prtno and lcp.riskcode=lm.riskcode and lm.TaxOptimal='Y' ";
   rs = easyExecSql(sql);
   if(rs){
       var check=rs[0][0];
       var checkinfo=rs[0][1];
       var transflag=rs[0][2];
       if(rs[0][0]=='99'&&transflag!='1'){
          alert("该单为税优产品保单，该单税优验证失败，失败原因为："+checkinfo);
          return false;
       }
       if(rs[0][0]!='00'&&rs[0][0]!='99'){
          alert("该单为税优产品保单，需进行税优唯一性验证！");
          return false;
       }
   }
       
   return true;
}

function checkGrpName1() {
	var riskcodeSql = "select distinct 1 from lcpol where prtno = '"
			+ fm.PrtNo.value
			+ "' "
			+ "and riskcode in (select riskcode from lmriskapp where taxoptimal = 'Y') with ur";
	var result = easyExecSql(riskcodeSql);
	if (result && result[0][0] == "1") {
		var GrpName = fm.AppntGrpName.value;
		if (GrpName == null || GrpName == "") {
			alert("投保税优系列产品，工作单位必录!");
			return false;
		}
	}
	return true;
}

function checkGrpName() {
	var TaxFlag = fm.TaxFlag.value;
	var GrpName = fm.AppntGrpName.value;
	var sss = "select Grpno from lccontsub where prtno = '"+fm.PrtNo.value+"' with ur";
	var resultww = easyExecSql(sss);
	var GrpNo = resultww[0][0];
	if (TaxFlag == "2") {
		var tSql = "select GrpName from lsgrp where grpno = '" + GrpNo
				+ "' with ur";
		var result = easyExecSql(tSql);
		if (!result) {
			alert("请输入正确的团体编号！");
			return false;
		}
		var tGrpName = result[0][0];
		if (GrpName != tGrpName) {
			alert("该客户所在团体单位名称为" + tGrpName + "，与投保人工作单位不一致，请修改！");
			return false;
		}
	}
	return true;
}

function checkdiscountfactor1() {
	var sql = "select InsuredAppAge,amnt  from lcpol lc,lmriskapp lm  where lc.prtno='"
			+ fm.PrtNo.value
			+ "' "
			+ " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' ";
	var arrResult = easyExecSql(sql);
	if (arrResult) {
		var sql1 = "select Totaldiscountfactor  from lccontsub  where prtno='"
				+ fm.PrtNo.value + "' ";
		var zkResult = easyExecSql(sql1);
		if (zkResult) {
			if ((zkResult[0][0]<1&&zkResult[0][0]!="") && (arrResult[0][1] == 40000)) {
				alert("该被保人已患既往症,不能享受税优折扣，请修改折扣因子！");
				return false;
			}
		}
	}
	return true;
}

function checkPremMult(){
	var sql = "select distinct riskcode from lcpol where prtno='"+ fm.PrtNo.value + "' ";
	var arr = easyExecSql(sql);
	if(!arr){
		alert("未录入险种信息，请核实！");
		return false;
	}
	var sql1 = "select premmult from lccontsub where prtno='"+ fm.PrtNo.value + "' ";
	var arr1 = easyExecSql(sql1);
	if(!arr1){
		alert("信息录入不全，请核实！");
		return false;
	}
	var tPremMult=arr1[0][0];
	for(var i=0;i<arr.length;i++){
		var tsql = "select 1 from ldcode where codetype='sypremnult' and code='"+ arr[i][0] + "' ";		
		var arrtsql = easyExecSql(tsql);
		if(arrtsql&&(tPremMult==null||tPremMult=="")){
			alert("对于险种"+arr[i][0]+"被保险人风险保险费档次字段必录！");
			return false;
		}		
		if(!arrtsql&&tPremMult!=""){
			alert("险种"+arr[i][0]+"不可录入被保险人风险保险费档次字段！");
			return false;
		}
		if(arrtsql){
			//险种风险保费和页面录入被保险人风险保险费档次对比
			var ttsql = "select 1 from lcpol where prtno='"+ fm.PrtNo.value + "' "
					  + "and riskcode ='"+ arr[i][0] + "' "
					  + "and mult='"+ tPremMult +"' ";
			var arrttsql = easyExecSql(ttsql); 
			if(!arrttsql){
				alert("风险保费档次与保障计划不符！");
				return false;
			}
			//告知与页面录入被保险人风险保险费档次对比
			var ttsql1 = "select 1 from LCCustomerImpartParams where prtno='"+ fm.PrtNo.value + "' "
				       + "and impartcode='170' and impartparam='N' and impartparamno='5' "
			var arrttsql1 = easyExecSql(ttsql1); 
			if(arrttsql1&&tPremMult=="2"){
				alert("被保险人医疗费用支付方式与风险保险费档次不匹配！");
				return false;
			}
			var ttsql2 =  "select 1 "
		          	   + " from lcpol lc "
		          	   + " where lc.prtno='" + fm.PrtNo.value + "' "
		          	   + " and exists (select 1 from ldcode where codetype='sypremnult' and code=lc.riskcode) "
		          	   + " and exists (select 1 from LCCustomerImpartParams where impartcode ='260' and impartver='001' and customerno=lc.insuredno and contno=lc.contno and impartparam='Y')"
		          	   + " and lc.amnt='250000' ";
			var arrttsql2 = easyExecSql(ttsql2); 
			if(arrttsql2){
				alert("承保为税优产品，被保人投保保额不符合要求！");
				return false;
			}
		}
	}
	return true;  
}
//校验标准个险(被保人页面) 集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   by zhangyang 2011-03-21
function checkCrsBussSaleChnl()
{
	var strSQL = "select 1 from lccont lcc where lcc.ContType = '1' "  
			   + " and lcc.Crs_SaleChnl is not null " 
			   + " and lcc.Crs_BussType = '01' " 
			   + " and not (lcc.SaleChnl = '10' and exists (select 1 from LACom lac where lac.AgentCom = lcc.AgentCom and lac.BranchType = '1' and lac.AcType in ('06', '07'))) " 
			   + " and not (lcc.SaleChnl = '15' and exists (select 1 from LACom lac where lac.AgentCom = lcc.AgentCom and lac.BranchType = '5' and lac.AcType in ('06', '07'))) " 
			   + " and not (lcc.SaleChnl = '03' and exists (select 1 from LACom lac where lac.AgentCom = lcc.AgentCom and lac.BranchType = '2' and lac.AcType in ('06', '07'))) " 
			   + " and lcc.PrtNo = '" + fm.PrtNo.value + "' ";
	var arrResult = easyExecSql(strSQL);
    if (arrResult != null){
        alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为中介渠道，并且机构必须为对应的中介机构！");
        return false;
    }
	return true;
}

function chagePostalAddress(){
	if(fm.PostalCity.value == '空' ){
		   fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalStreet.value+fm.PostalCommunity.value;
	   }else if(fm.PostalCity.value != '空' && fm.PostalCounty.value == '空') {
		   fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalCity.value+fm.PostalStreet.value+fm.PostalCommunity.value;
	   }else{
		   fm.PostalAddress.value=fm.PostalProvince.value+fm.PostalCity.value+fm.PostalCounty.value+fm.PostalStreet.value+fm.PostalCommunity.value;
	   }
}

function checkBonusGetMode(){
	var rs4 =easyExecSql( "select a.risktype4,b.bonusgetmode from lmriskapp a, (select riskcode,BonusGetMode from lcpol where prtno = '"+fm.PrtNo.value+"') b where a.riskcode =b.riskcode");
	if(rs4 != "" && rs4 != null){
		for(var i=0;i<rs4.length;i++){
			if(rs4[i][0] == "2" && rs4[i][1] != "1" && rs4[i][1] != "2"){
				alert("分红险必须录入红利领取方式：1或2！");
				return false;
			}
		}
	}
	var rs5 = easyExecSql("select insuredno from lcpol where prtno='"+fm.PrtNo.value+"' with ur");
	if(rs5 != "" && rs5 != null){
		for(var k=0; k<rs5.length;k++){
			var rs6= easyExecSql("select 1 from lcpol where insuredno = '"+rs5[k][0]+"' and  prtno='"+fm.PrtNo.value+"' and riskcode='340601' with ur");
			if(rs6 == 1){
				var rs7 = easyExecSql("select bonusgetmode from lcpol where insuredno = '"+rs5[k][0]+"' and  prtno='"+fm.PrtNo.value+"' and riskcode='730503' with ur");
				if(rs7 != null && rs7 !="" && rs7 != 2){
					alert("730503与340601同时投保时，红利领取方式只能选择“累积生息”。");
					return false;
				}
			}
		}
	}
	return true;
}
//销售渠道与交叉销售校验	
function checkSalechnl_CrsBuss(){
	var sql = "select salechnl from lccont where prtno = '"+fm.PrtNo.value+"'";
	var sql2 = "select Crs_BussType from lccont where prtno = '"+fm.PrtNo.value+"'";
	var salechnl = easyExecSql(sql);
	var Crs_BussType = easyExecSql(sql2);
	
	//社保渠道不能有交叉销售信息
	if ((salechnl=="16" || salechnl=="18" || salechnl=="20") && (Crs_BussType != "")) {
			alert("社保渠道不能有交叉销售信息！");
			return false;
	}else if ((salechnl == "14"||salechnl == "01" || salechnl == "02" || salechnl == "13"||salechnl == "17"||salechnl == "21") && Crs_BussType != "") {
		if (Crs_BussType != "02" && Crs_BussType != "03" && Crs_BussType != "13" && Crs_BussType != "14") {
			alert("销售渠道为直销渠道时，只可选择联合展业、渠道共享、互动部、农网共建！");
			return false;
		}
	}else if (salechnl == "15" && Crs_BussType != "") {
		if (Crs_BussType != "01") {
			alert("销售渠道为互动中介时，只可选择相互代理！");
			return false;
		}
	} else if((salechnl=="03"||salechnl=="10"||salechnl=="22")&& (Crs_BussType != "")){
			alert("除互动中介外其他中介渠道不能勾选交叉销售!");
			return false;
	}else if (salechnl != "14" && salechnl != "15" && salechnl != "16" && salechnl != "18" && salechnl != "20" && Crs_BussType != "") {
		if ( Crs_BussType != "02") {
			//alert("非社保渠道、互动中介和互动直销时，只可选择相互代理或者联合展业！");
			  alert("非互动中介渠道，交叉销售业务类型只可选择联合展业！");
			return false;
		}
	}
	
	return true;
}

/**
 * 增加以下税优相关校验：
 * 1.当投保标识为“个人”时，团体投保人数折扣因子必须等于1。
 * 2.当被保险人告知医疗费用支付方式中不包含“⑤补充医疗保险”时，补充医疗保险折扣因子必须等于1。
 * 3.目前仅当险种为122601、122901时，可以录入折扣因子，其他产品录入折扣因子报错提示不通过。
 * @returns {Boolean}
 */
function checkSYAbout() {
	//目前仅当险种为122601、122901时，可以录入折扣因子，其他产品录入折扣因子报错提示不通过。
	var risk122601And3122901 = "select 1 from LCPol p where PrtNo = '"+fm.PrtNo.value+"' and RiskCode in ('122601', '122901') ";
    result = easyExecSql(risk122601And3122901);
    var discountData =easyExecSql("select Agediscountfactor,Supdiscountfactor,Grpdiscountfactor,Totaldiscountfactor,DiscountMode,TaxFlag from lccontsub s where prtno='"+fm.PrtNo.value+"' ");
    if(!result){
    	if(discountData){
    		if(discountData[0][0]!=""||discountData[0][1]!=""||discountData[0][2]!=""||discountData[0][3]!=""||discountData[0][4]!=""){
    			alert("当前录入的险种不为122601或122901，不可以录入折扣方式!");
    			return false;
    		}
    	}
    }else{
    	if(discountData){
    		if(discountData[0][3]==""||discountData[0][4]==""){
    			alert("当前录入的险种为122601或122901，必须录入折扣方式!");
    			return false;
    		}
    		if (discountData[0][4]=="1") {
    			var Grpdiscountfactor = discountData[0][2];
    			//1、当投保标识为“个人”时，团体投保人数折扣因子必须等于1。
    			if((discountData[0][5] == "1") && (Grpdiscountfactor != null) && (Grpdiscountfactor != "")) {
    				if(Grpdiscountfactor != '1') {
    					alert("税优标识为个人时，团体投保人数折扣因子必须等于1！");
    					return false;
    				}
    			}
    			//2、当被保险人告知医疗费用支付方式中不包含“⑤补充医疗保险”时，补充医疗保险折扣因子必须等于1。
    			if(!checkGZ2Prem()) {
    				alert("当被保险人告知医疗费用支付方式中不包含“⑤补充医疗保险”时，补充医疗保险折扣因子必须等于1！");
    				return false;
    			}
    		}else if(discountData[0][4]=="2") {
				var rss0 =easyExecSql("select 1 from ldcode1 where codetype='discountmode' and  code='2' and code1=left('"+fm.all('ManageCom').value+"',4) ");
				var rss1 =easyExecSql("select grpno from lccontsub where prtno='"+fm.PrtNo.value+"'");
				var rss2;
				if(rss1){
					rss2 =easyExecSql("select 1 from ldcode1 where codetype='discountmode' and  code='2' and code1='"+rss1[0][0]+"' ");
				}
				if((rss0==""||rss0==null) && (rss2==""||rss2==null)){
					alert("当前机构或当前集团编号下不允许录入约定折扣方式！")
					return false;
				}
				if(!dealAppointDiscount(discountData[0][3])){
					return false;
				}
			}
    	}
    }
	return true;
}

function dealAppointDiscount(num){
	if(num !=""){
		var reg = /(^[-+]?[1-9]\d*(\.\d{1,2})?$)|(^[-+]?[0]{1}(\.\d{1,2})?$)/;
		if (!reg.test(num)) {
            alert("约定折扣必须为合法数字(正数，最多两位小数)！");
            return false;
        } 
		if(num < 0.7){
			alert("约定折扣不能小于0.7！");
			return false;
		}
	}else{
		alert("请录入约定折扣！");
		return false;
	}
	return true;
}

/**
 * 当被保险人告知医疗费用支付方式中不包含“⑤补充医疗保险”时，补充医疗保险折扣因子必须等于1
 * @returns {Boolean}
 */
function checkGZ2Prem() {
	var CustomerimpartSql = "select impartparammodle from lccustomerimpart where prtno = '"+fm.PrtNo.value+"' and impartver = '001' and impartcode='170' and impartcontent = '被保险人的医疗费用支付方式   □公费医疗  □社会医疗保险  □商业医疗保险  □自费 □补充医疗保险' ";
	var arrCustomer = easyExecSql(CustomerimpartSql);
	var getFactorSql = " select Agediscountfactor,Supdiscountfactor,Grpdiscountfactor,Totaldiscountfactor from lccontsub where prtno = '"+fm.PrtNo.value+"' ";
	var factorResult = easyExecSql(getFactorSql);
	var Supdiscountfactor = factorResult[0][1];
	if(arrCustomer != null || arrCustomer != ""){
		var arry = arrCustomer[0][0].split(",");
		if(Supdiscountfactor != "" && Supdiscountfactor != null) {
			if(arry[0] == "Y" || arry[1] == "Y"){
				if(arry[4] == "N" && Supdiscountfactor != "1") {
					return false;
				}
			}
		}
	}
	return true;
}


function checktaxCustomerImpart(){
	var sql = "select salechnl from lccont where prtno = '"+fm.PrtNo.value+"'";
	var salechnl = easyExecSql(sql);
	if (salechnl=="01") {
		sql =  "select 1 "
	        + " from lcpol lc,lmriskapp lm  "
	        + " where lc.prtno='" + fm.PrtNo.value + "' "
	        + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
	        + " and  exists (select 1 from LCCustomerImpart where impartcode in('110','260','270') and impartver='001' and customerno=lc.insuredno and ProposalContNo=lc.contno and impartparammodle='Y' )";
		rs = easyExecSql(sql);
	    if(rs){
	        alert("健康状况存在异常，不允许通过本销售渠道投保。");
	        return false;
	    }
	
     }
	return true;  
}

function checktaxnoAndcreditcode(){
	sql = "select lc.taxno,lc.creditcode, "
	    + "lc.GTaxNo,lc.GOrgancomCode,lc.taxflag from lccontsub lc "
	    + " where lc.prtno='" + fm.PrtNo.value + "' ";
	rs = easyExecSql(sql);
	 if(rs){
         var taxno=rs[0][0];
         var creditcode=rs[0][1];
         var GTaxNo=rs[0][2];
         var GOrgancomCode=rs[0][3];
         var taxflag=rs[0][4];
         if(taxflag!=""&&taxflag!=null){
        	 if(taxno!=""&&taxno!=null&&taxno.length!=15&&taxno.length!=18&&taxno.length!=20){
                 alert("社会信用代码或税务登记号长度不符合要求！");
                 return false;
               }
      	     if(GTaxNo!=""&&GTaxNo!=null&&GTaxNo.length!=15&&GTaxNo.length!=18&&GTaxNo.length!=20){
                 alert("社会信用代码或税务登记号长度不符合要求！");
                 return false;
               }
      	     if(creditcode!=""&&creditcode!=null&&creditcode.length!=18){
                 alert("社会信用代码或税务登记号长度不符合要求！");
                 return false;
               }
      	     if(GOrgancomCode!=""&&GOrgancomCode!=null&&GOrgancomCode.length!=18){
                 alert("社会信用代码或税务登记号长度不符合要求！");
                 return false;
               } 
          } 
	 }
	 return true; 
}
//对个人业务纸质出单进行卡控
function checkPaper(){
	var prtno=fm.PrtNo.value;
	if(prtno.substring(0,2)=="PD"){
		return true;
	}
	var checkTaxFlag="select 1 from lccontsub where prtno='"+prtno+"' and (taxflag is null or taxflag='')";
	var checkPayMode="select paymode from lccont where prtno='"+prtno+"' and conttype='1' and cardflag='0' ";
	var TaxFlagResult=easyExecSql(checkTaxFlag);
	var PayModeResult=easyExecSql(checkPayMode);
	if((TaxFlagResult != null && TaxFlagResult !="") && PayModeResult!="8" ){
		var checkSql="select 1 from lccont lcc " +
				" where lcc.prtno='"+prtno+"' " +
						"and lcc.conttype='1' " +
						"and lcc.cardflag='0' " +
						"and lcc.managecom not in (select code from ldcode where codetype='cardctrlcom' )" +
						"and lcc.salechnl in (select code from ldcode ld where ld.codetype='cardctrlsalechnl')";
		var checkSqlResult=easyExecSql(checkSql);
		if(checkSqlResult !=null && checkSqlResult !=""){
			alert("请通过电子出单平台出单!");
			return false;
		}
	}
	return true;
}

//modify by zxs 2019-01-08
function checkInsuredPassIDNO(){
	 //zxs
	if(fm.IDType.value=='a'||fm.IDType.value=='b'){
		if(fm.NativePlace.value!="HK"){
			alert("被保人的证件类型和国籍不相符！");
			return false;	
		}
		if(fm.InsuredPassIDNo.value.replace(/(^\s*)|(\s*$)/g, '') == ''){
			alert("被保人的原通行证号码不能为空！");
			return false;
		}	
		if(fm.IDType.value=='a'){
			if(fm.NativeCity.value!='1'&&fm.NativeCity.value!='2'){
				alert("被保人的证件类型和地区不相符！");
				return false;
			}
		}
		if(fm.IDType.value=='b'){
			if(fm.NativeCity.value!='3'){
				alert("被保人的证件类型和地区不相符！");
				return false;	
			}
		}
		if(fm.InsuIDStartDate.value.trim().replace(/(^s*)|(s*$)/g, "").length ==0||fm.InsuIDEndDate.value.trim().replace(/(^s*)|(s*$)/g, "").length ==0){
			alert("证件起至日期不能为空！");
			return false;
		}else{
			  var startDate = new Date(fm.InsuIDStartDate.value.replace(/-/g,'/')); 
			  var endDate =new Date(fm.InsuIDEndDate.value.replace(/-/g,'/'));
			if(endDate.getFullYear()-startDate.getFullYear()!=5){
				alert("证件有效期必须为五年,请检查！");
				return false;
			}
		}
		if(fm.IDNo.value.indexOf('810000')==0){
			if(fm.NativeCity.value!='1'){
				alert("投保人证件号码和地区不相符！");
				return false;
			}
		}
		if(fm.IDNo.value.indexOf('820000')==0){
			if(fm.NativeCity.value!='2'){
				alert("投保人证件号码和地区不相符！");
				return false;
			}
		}
		if(fm.IDNo.value.indexOf('830000')==0){
			if(fm.NativeCity.value!='3'){
				alert("投保人证件号码和地区不相符！");
				return false;
			}
		}
	}
	return true;
}
//校验保单登记的全部信息
function checkNameRule(){
	var PrtNo = fm.PrtNo.value;
	var rs1 = easyExecSql("select idtype,appntname,idno from lcappnt where prtno='"
			+ PrtNo + "'");
	if(rs1[0][0]=='1'){
		if(rs1[0][2].length<6){
			alert("投保人护照号码位数错误！")
			return false;
		}
	}
	if(rs1[0][0]=='6'){
		if(rs1[0][2].length<7){
			alert("投保人出生证号码位数错误！")
			return false;
		}
	}
	var nameResult = checkValidateName(rs1[0][0], rs1[0][1]);
	if(nameResult!=""){
		alert("投保人"+nameResult);
		return false;
	}
	var rs2 = easyExecSql("select idtype,name,SequenceNo,RelationToAppnt,RelationToMainInsured,idno from lcinsured where prtno='"
			+ PrtNo + "'");
	 for(var i=0; i<rs2.length;i++ ) {
		var nameResult2 = checkValidateName(rs2[i][0], rs2[i][1]);
		if(nameResult2!=""){
			alert("被保人"+nameResult2);
			return false;
		}
		var relationResult = checkInsuredRelation(rs2[i][2], rs2[i][3],rs2[i][4]);
		if(relationResult!=""){
			alert(relationResult);
			return false;
		}
		if(rs2[i][0]=='1'){
			if(rs2[i][5].length<6){
				alert("被保人护照号码位数错误！")
				return false;
			}
		}
		if(rs2[i][0]=='6'){
			if(rs2[i][5].length<7){
				alert("被保人出生证号码位数错误！")
				return false;
			}
		}
	 }
	var rs3 = easyExecSql("select idtype,name,relationtoinsured,idno from lcbnf where contno=(select contno from lccont where prtno ='"+PrtNo+"')");
	if(rs3!=null){
		 for(var j=0; j<rs3.length;j++ ) {
				var nameResult3 = checkValidateName(rs3[j][0], rs3[j][1]); 
				if(nameResult3!=""){
					alert("受益人"+nameResult3);
					return false;
				}
				if(rs3[j][2]=='30'){
					alert("请明确受益人与被保人关系！");
					return false;	
				}
				if(rs3[j][0]=='1'){
					if(rs3[j][3].length<6){
						alert("受益人护照号码位数错误！")
						return false;
					}
				}
				if(rs3[j][0]=='6'){
					if(rs3[j][3].length<7){
						alert("受益人出生证号码位数错误！")
						return false;
					}
				}
			 }
	}
	return true;
}
//zxs 20190521
function checkAppDate(){
	var prtno = fm.PrtNo.value;
	var daysql = "select code from ldcode where  codetype = 'checkpolapplydate' ";
	var dayresult = easyExecSql(daysql);
	var sql = "select case when polApplyDate between current date -"+dayresult+" day and  current date  then 1 else 0 end from lccont where prtno = '"+prtno+"' ";
	var result = easyExecSql(sql);
	if(result!=null&&result!=""&&result=='0'){
		alert("投保申请日期录入有误，不得晚于当前日期，且不得早于当前日期-"+dayresult+"天。");
		return false;
	}

	var sql1 = "select case when receiveDate is null then 1 when receiveDate between current date -"+dayresult+" day and  current date  then 1 else 0 end from lccont where prtno = '"+prtno+"' ";
	var result1 = easyExecSql(sql1);
	if(result1=='0'){
			alert("投保收单日期录入有误，不得晚于当前日期，且不得早于当前日期-"+dayresult+"天。");
			return false;
	  }
		var sql2 = "select case when firstTrialDate is null then 1 when firstTrialDate between current date -"+dayresult+" day and  current date  then 1 else 0 end from lccont where prtno = '"+prtno+"' ";
		var result2 = easyExecSql(sql2);
		if(result2=='0'){
			alert("投保初审日期录入有误，不得晚于当前日期，且不得早于当前日期-"+dayresult+"天。");
			return false;
		}
	return true;
}