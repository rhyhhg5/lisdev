<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输入参数
	System.out.println("start ClaimNumModifySave.jsp...");
	ClaimNumModifyUI tClaimNumModifyUI = new ClaimNumModifyUI();
	
	String mClaimNum = request.getParameter("MClaimNum");
	String mGrpcontno = request.getParameter("Grpcontno");
	String tRadio[] = request.getParameterValues("ClaimNumGridNo");
	String tGrpContNo[] = request.getParameterValues("ClaimNumGrid1");
	String tRiskCode[] = request.getParameterValues("ClaimNumGrid2");
	String tOldClaimNum[] = request.getParameterValues("ClaimNumGrid3");
	
	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
    System.out.println(tRadio[1]);
	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();

	for (int i = 0; i < tRadio.length; i++) {
		if ("1".equals(tRadio[i])) {
			transferData.setNameAndValue("GrpContNo", tGrpContNo[i]);
			transferData.setNameAndValue("RiskCode", tRiskCode[i]);
			transferData.setNameAndValue("ClaimNum", tOldClaimNum[i]);
			transferData.setNameAndValue("MClaimNum", mClaimNum);
			break;
		}
	}
	if (!"Fail".equals(FlagStr)) {
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			tClaimNumModifyUI.submitData(tVData, "");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = tClaimNumModifyUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>