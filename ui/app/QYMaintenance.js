
function showContInfo()
{
	if(prtno != "" && conttype != "" && conttype == "1")
	{
		var strSQL = "select lcc.ManageCom,lcc.Prtno,lcc.ContNo,lcc.ContType,lcc.CValiDate," 
			+ "lcc.SignDate,lcc.SaleChnl,"
			+ "(select CodeName from ldcode where codetype = 'lcsalechnl' and Code = lcc.SaleChnl ) SaleChnlName,"
			+ "lcc.AgentCom,"
			+ "(select Name from lacom where agentcom = lcc.AgentCom) AgentComName," 
			+ "lcc.AgentCode," 
			+ "(select name from laagent where agentcode = lcc.AgentCode) AgentName," 
			+ "lcc.AgentGroup,"
			+ "(select Name from ldcom where comcode = lcc.ManageCom) ManageComName " 
			+ "from lccont lcc where 1 = 1 "
			+ " and lcc.PrtNo = '" + prtno + "' "
			+ " and lcc.ContType = '" + conttype + "' ";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
			alert("没有查到保单信息！");
			return false;
		}
		fm.ManageCom.value = arrResult[0][0];
		fm.ManageComName.value = arrResult[0][13];
		fm.PrtNo.value = arrResult[0][1];
		fm.ContNo.value = arrResult[0][2];
		fm.ContType.value = arrResult[0][3];
		fm.CValiDate.value = arrResult[0][4];
		fm.SignDate.value = arrResult[0][5];
		fm.SaleChnl.value = arrResult[0][6];
		fm.SaleChnlName.value = arrResult[0][7];
		fm.AgentCom.value = arrResult[0][8];
		fm.AgentComName.value = arrResult[0][9];
		fm.AgentCode.value = arrResult[0][10];
		fm.AgentName.value = arrResult[0][11];
		fm.AgentGroup.value = arrResult[0][12];
		
	}else if(prtno != "" && conttype != "" && conttype == "2")
	{
		var strSQL = "select lcc.ManageCom,lcc.Prtno,lcc.GrpContNo,'2' ContType,lcc.CValiDate," 
			+ "lcc.SignDate,lcc.SaleChnl,"
			+ "(select CodeName from ldcode where codetype = 'salechnl' and Code = lcc.SaleChnl ) SaleChnlName,"
			+ "lcc.AgentCom,"
			+ "(select Name from lacom where agentcom = lcc.AgentCom) AgentComName," 
			+ "lcc.AgentCode," 
			+ "(select name from laagent where agentcode = lcc.AgentCode) AgentName," 
			+ "lcc.AgentGroup,"
			+ "(select Name from ldcom where comcode = lcc.ManageCom) ManageComName " 
			+ "from lcgrpcont lcc where 1 = 1 "
			+ " and lcc.PrtNo = '" + prtno + "' ";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
			alert("没有查到保单信息！");
			return false;
		}
		fm.ManageCom.value = arrResult[0][0];
		fm.ManageComName.value = arrResult[0][13];
		fm.PrtNo.value = arrResult[0][1];
		fm.ContNo.value = arrResult[0][2];
		fm.ContType.value = arrResult[0][3];
		fm.CValiDate.value = arrResult[0][4];
		fm.SignDate.value = arrResult[0][5];
		fm.SaleChnl.value = arrResult[0][6];
		fm.SaleChnlName.value = arrResult[0][7];
		fm.AgentCom.value = arrResult[0][8];
		fm.AgentComName.value = arrResult[0][9];
		fm.AgentCode.value = arrResult[0][10];
		fm.AgentName.value = arrResult[0][11];
		fm.AgentGroup.value = arrResult[0][12];
	}
	fm.Action.value = action;
	fm.FinancialAction.value = "None";
}

//进行财务充负
function negativeRecoil()
{
	if(!checkMessages())
	{
		return false;
	}
	
	fm.FinancialAction.value = "doNegative";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./QYMaintenanceSave.jsp";
	fm.submit();
}

//进行财务充正
function positiveRecoil()
{
	if(!checkMessages())
	{
		return false;
	}
	
	fm.FinancialAction.value = "doPositive";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./QYMaintenanceSave.jsp";
	fm.submit();
}

//修改业务数据
function update()
{
	if(!checkMessages())
	{
		return false;
	}
	
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./QYMaintenanceSave.jsp";
	fm.submit();
}
//只修改业务数据（包含实收表） by bxf 20181207
function onlyupdate()
{
	if(!checkMessages())
	{
		return false;
	}
	fm.OnlyDoUpdate.value = "do";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./QYMaintenanceSave.jsp";
	fm.submit();
}


//校验录入的数据
function checkMessages()
{
	if(action == "SaleChnl" || action == "Both")
	{
		var tSaleChnl = fm.mSaleChnl.value;
		if("" == tSaleChnl || null == tSaleChnl || "null" == tSaleChnl)
		{
			alert("销售渠道不能为空！");
			return false;
		}
	}
	
	if(action == "AgentCode" || action == "Both")
	{
		var SaleChnl = fm.SaleChnl.value;
		var tSaleChnl = fm.mSaleChnl.value;
		var tAgentCom = fm.mAgentCom.value;
		var tAgentCode = fm.mAgentCode.value;
		var tAgentGroup = fm.mAgentGroup.value;
		if(SaleChnl == "03" || tSaleChnl == "03" )
		{
			if("" == tAgentCom || null == tAgentCom || "null" == tAgentCom)
			{
				alert("中介机构代码不能为空！");
				return false;
			}
		}
		if("" == tAgentCode || null == tAgentCode || "null" == tAgentCode)
		{
			alert("业务员代码不能为空！");
			return false;
		}
		if("" == tAgentGroup || null == tAgentGroup || "null" == tAgentGroup)
		{
			alert("业务员组别不能为空！");
			return false;
		}
	}
	return true;
}

function afterSubmit( FlagStr, content )
{
	showInfo.close();
	
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		showContInfo();
	}
}

