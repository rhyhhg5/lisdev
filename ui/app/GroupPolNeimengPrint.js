var showInfo;
var mDebug="0";
var manageCom;
var turnPage = new turnPageClass();
var arrDataSet;
window.onfocus=myonfocus;
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();

	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = GrpContGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
function easyQueryClick(){
	// 初始化表格
	initGrpContGrid();
	//加入管理机构查询限制
	if(!verifyInput2())
	return false;
	var strManageComWhere = "";
	if ( fm.ManageCom.value != '' && (fm.ManageCom.value).substring(0, 4)!= "8615" ) {
		alert("登陆管理机构不为内蒙本级或内蒙分支公司，或查询所选管理机构不为内蒙本级或内蒙分支公司！");
		return false;
	}
	if( fm.ManageCom.value != '' ) {
		strManageComWhere += " AND ManageCom LIKE '" + fm.ManageCom.value + "%%' ";
	}
	if( fm.BranchGroup.value != '' ) {
		strManageComWhere += " AND AgentGroup IN ( SELECT AgentGroup FROM LABranchGroup WHERE BranchAttr LIKE '" + fm.BranchGroup.value + "%%') ";
	}

	// 书写SQL语句
	var strSQL = "";
	strSQL = "SELECT GrpContNo,PrtNo,Prem,GrpName,CValiDate,PrintCount,'打印','1','打印','1' FROM LCGrpCont A"
		+ " WHERE AppFlag in ('1','9') and ( PrintCount < 1 OR PrintCount IS NULL ) and (prtno like '18%%' or prtno like '20%%' or prtno like '91%%' or prtno like '99%%' or prtno like 'PR%' or prtno like 'SX%' or prtno like '05%' or prtno like '1E%' or prtno like '106%') "
		+ " and exists (select 1 from LDCode1 where CodeType = 'printchannel' and Code = substr(ManageCom, 1, 4) and Code1 = '2' and CodeName = '" + printChannelType + "') "
		+ " and not exists (select 1 from lcgrppol where grpcontno = A.grpcontno and riskcode = '162001') "
		+ " and ContPrintType <> '5' " //add by zjd 汇交件不在这里打印
		//+ " and riskcode in (select riskcode from LMRiskApp where NotPrintPol = '0')"
        //+ " and ContNo='" + contNo + "' "
		+ getWherePart( 'PrtNo' )
		+ getWherePart( 'GrpContNo' )
		+ getWherePart( 'getUniteCode(AgentCode)','AgentCode' )
		+ getWherePart( 'SaleChnl' )
        //+ getWherePart( 'RiskCode' )
		+ strManageComWhere
        //+ " AND NOT EXISTS ( SELECT GrpPolNo FROM LCGrpPol WHERE A.PrtNo = PrtNo AND AppFlag = '0' ) "
		+ " order by ManageCom,AgentGroup,AgentCode";
		//fm.all('BranchGroup').value = strSQL;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //fm.PrtNo.value=strSQL;
    //alert(strSQL);
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("未查询到满足条件的数据！");
		return false;
	}
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	turnPage.pageLineNum = 10 ;
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = GrpContGrid;
	//保存SQL语句
	turnPage.strQuerySql     = strSQL ;
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}


//提交，保存按钮对应操作
function printGroupPol()
{
	var i = 0;
	var flag = 0;
	flag = 0;
	//判定是否有选择打印数据
	for( i = 0; i < GrpContGrid.mulLineCount; i++ )
	{
		if( GrpContGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}
	//如果没有打印数据，提示用户选择
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	fm.all("printButton").disabled=true;
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	window.focus();
	showInfo.close();
	//无论打印结果如何，都重新激活打印按钮
	fm.all("printButton").disabled=false;
	if (FlagStr == "Fail" )
	{
		//如果失败，则返回错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		//如果提交成功，则执行查询操作
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
}

function queryBranch()
{
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
	if(arrResult!=null)
	{
		fm.BranchGroup.value = arrResult[0][3];
	}
}
