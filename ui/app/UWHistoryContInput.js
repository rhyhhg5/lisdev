//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var approvefalg;
var arrResult;
var mDebug = "0";
var mOperate = 0;
var mAction = "";
var flag="";
var mSwitch = parent.VD.gVSwitch;
var mShowCustomerDetail = "GROUPPOL";
var turnPage = new turnPageClass();
this.window.onfocus=myonfocus;

var contState = "C";

function initContState(){
	var sql = "select 1 from lccont where proposalcontno='" 
			+ ContNo 
			+ "' union all "
			+ "select 2 from lbcont where proposalcontno='" 
			+ ContNo 
			+ "' and edorno not like 'xb%'";
	var rs = easyExecSql(sql);
	if(rs){
		if(rs[0][0] == "2") {
			// 退保保单
			contState="B";
		} else if(rs[0][0] == "1") {
			contState="C";
		}
	}
}

//逻辑校验
function LogicVerify(){
	var i = 0;                                                                                                                                                                                                                                                                                                                                              
 	var showStr="正在校验数据，请您稍候并且不要修改屏幕上的值或链接其他页面";                                                                                                                                                                                                                                                                                
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;                                                                                                                                                                                                                                                                                
 	 	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");                                                                                                                                                                                                  	
 	    fm.action= "./ContLogicVerify.jsp";   
      fm.submit(); //提交                 
			return;

	}

function payModeHelp(){
	window.open("./PayMode.jsp");
}


/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	showDiv( operateButton, "false" ); 
	showDiv( inputButton, "true" ); 
}           

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick()
{
        if(this.ScanFlag == "1"){
	  alert( "有扫描件录入不允许查询!" );
          return false;	  
        }	
	if( mOperate == 0 )
	{
		mOperate = 1;
		cContNo = fm.all( 'ContNo' ).value;
		showInfo = window.open("./ContQueryMain.jsp?ContNo=" + cContNo);
	}
} 

/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
	if(fm.all('PolApplyDate').value==""){
		alert("请录入投保申请日期!");
		return false;
	}
    
    // 使个单生效日期为保单投保日期
    fm.all('CValiDate').value=fm.all('PolApplyDate').value
    // --------------------
    
	if(!chkDayAndMonth()) return false;
	var tGrpProposalNo = "";
	tGrpProposalNo = fm.all( 'ProposalContNo' ).value;
	if(BirthdaySexAppntIDNo()==false){return false;}
	if( tGrpProposalNo == null || tGrpProposalNo == "" )
		alert( "请先做投保单保存操作，再进行修改!" );
	else
	{
   if (verifyInput2()== false) {        	
	  return false;
                }
// by gzh 20101118
	//选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}

    // 校验集团交叉业务要素
    //if(!checkCrsBussParams())
    //{
        //return false;
    //} 

	  ImpartGrid.delBlankLine();    		
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "UPDATE";
			fm.all( 'fmAction' ).value = mAction;
			ChangeDecodeStr();
			fm.submit(); //提交
		}
	}
}           

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick()
{
	var tGrpProposalNo = "";
	var tProposalContNo = "";
	if(LoadFlag==1)
	{
		tGrpProposalNo = fm.all( 'GrpContNo' ).value;
	if( tGrpProposalNo == null || tGrpProposalNo == "" )
		alert( "请先做投保单保存操作，再进行删除!" );
	else
	{
		 if (!confirm("确认要删除该保单吗？"))
	{
		return;
	}
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	 }
  }
}           

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

/*********************************************************************
 *  当点击“进入个人信息”按钮时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoInsured()
{
	//下面增加相应的代码
	var tAppntNo = fm.AppntNo.value;
	var tAppntName = fm.AppntName.value;
	if( fm.ContNo.value == "" )
	{
		alert("您必须先录入合同信息才能进入被保险人信息部分。");
		return false
	}
    
	//把集体信息放入内存
	mSwitch = parent.VD.gVSwitch;  //桢容错
	putCont();
	try { goToPic(2) } catch(e) {}
	
	try {
	  parent.fraInterface.window.location = "./UWHistoryContInsuredInput.jsp?LoadFlag=" + LoadFlag + "&prtNo="+prtNo + "&type=" + type+ "&MissionID=" + tMissionID+ "&SubMissionID=" + tSubMissionID+ "&AppntNo=" + tAppntNo+ "&AppntName=" + tAppntName+"&checktype=1"+"&scantype="+scantype+"&ScanFlag="+ScanFlag+"&SaleChnl="+fm.SaleChnl.value+"&SaleChnlDetail="+fm.SaleChnlDetail.value+"&ContNo="+ContNo ;
	}
	catch (e) {
		parent.fraInterface.window.location = "./UWHistoryContInsuredInput.jsp?LoadFlag=1&prtNo="+prtNo + "&type=" + type+ "&MissionID=" +tMissionID+ "&SubMissionID=" + tSubMissionID+ "&AppntNo=" + tAppntNo+ "&AppntName=" + tAppntName +"&ContNo="+ContNo;
	}
}           


/*********************************************************************
 *  把合同所有信息录入结束确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function inputConfirm(wFlag)
{
    //销售渠道为中介时中介机构的校验
    if(!checkAgentCom())
    {
        return false;
    }
    //销售渠道和业务员的校验
    if(!checkSaleChnl())
    {
        return false;
    }
    //银行信息的校验
    if(!checkBankInfo())
    {
        return false;
    }
    //添加必录项校验
    if (verifyInput2()== false)
    {
        return false;
    }
    //险种校验
    if(!checkRiskInfo())
    {
        return false;
    }
    //银保渠道与险种校验
    if(!checkSaleChnlRisk())
    {
        return false;
    }
    
    // 校验集团交叉业务要素
    if(!checkCrsBussParams())
    {
        return false;
    } 
    
    //校验集团交叉业务销售渠道
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    //校验被保人职业类别与职业代码
    if(!checkOccTypeOccCode())
    {
    	return false;
    }   
    //校验小于18周岁的被保人是否选择了第19项告知
    if(!checkGZ19()){
    	return false;
    }
    //当19项告知选择“是”时，19项告知保额内容不能为空，且必须是数字
	if(!checkGZ19Prem()){
		return false;
	} 
    
    //保单保险期间的校验
    if(!checkInsuYear())
    {
    	return false;
    }
    
    //校验代理机构及代理机构业务员
    if(!checkAgentComAndSaleCdoe()){
    	return false;
    }
    
    if(!checkAgentCode()){
    	return false;
    }
    
    if(!checkAppnt(wFlag)){
    	return false;
    }
    //校验投保人银行账户
     if(!checkBank()){
    	return false;
    }
    
    if (wFlag ==1 ) //录入完毕确认
    {
        var tStr= "	select * from lwmission where 1=1 and lwmission.missionprop1 = '"+fm.ContNo.value+"'";
        turnPage.strQueryResult = easyQueryVer3(tStr, 1, 0, 1);
        if (turnPage.strQueryResult) {
            alert("该合同已经做过保存！");
            return;
        }
        if(fm.all('ProposalContNo').value == "")
        {
            alert("合同信息未保存,不容许您进行 [录入完毕] 确认！");
            return;
        }
        if(ScanFlag=="1")
        {
            fm.WorkFlowFlag.value = "0000001099";
        }
        if(ScanFlag=="0")
        {
            fm.WorkFlowFlag.value = "0000001098";
        }
    
        fm.MissionID.value = tMissionID;
        fm.SubMissionID.value = tSubMissionID;			//录入完毕
    }
    else if (wFlag ==2)//复核完毕确认
    {
        var qurSql="select bussno from es_issuedoc where bussno like '"+fm.PrtNo.value+"%' and status='1' and stateflag='1'";
        var arrqurSql=easyExecSql(qurSql);
        if(arrqurSql){
            if (!confirm("该单还有扫描修改申请待审批，确认要继续吗？"))
            {
                return;
            }
        }

        if(fm.all('ProposalContNo').value == "")
        {
            alert("未查询出合同信息,不容许您进行 [复核完毕] 确认！");
            return;
        }
        if(fm.all('AppntIDType').value == 0){
            if(fm.all('AppntIDNo').value == ""){
                alert("投保人证件类型为身份证,但是证件号码为空,请首先将证件类型修改为其它!");
                return false;
            }
        }
        
        if(fm.all('AppntIDType').value == 5){
            if(fm.all('AppntIDNo').value == ""){
                alert("投保人证件类型为户口本,但是证件号码为空,请首先将证件类型修改为其它!");
                return false;
            }
        }
        //增加身份证的校验
        if(BirthdaySexAppntIDNo()==false){
        	return false;
        }

        // 复核时，加入对整单保费的校验。
        //if(!checkContPrem())
        //{
        //    return false;
        //}
        // ---------------------------

        // 复核完毕时，对先收费的保单，对保费到帐情况进行校验。
        if(!checkEnterAccOfTempFee())
        {
            return false;
        }
        // ---------------------------

        // 复核完毕时，校验万能类险种。
        if(!checkULIRisk())
        {
            return false;
        }

        //if(!checkULIInsured())
        //{
        //    return false;
        //}
        // ---------------------------

        var strsql1=" select idtype,idno,name,RelationToMainInsured,RelationToAppnt from lcinsured where prtno='"+fm.PrtNo.value+"'";
        var arr=easyExecSql(strsql1);
        // alert(arr.length);
        for(var i=0;i<arr.length;i++){
            //	alert(arr[i][0]);
            //	alert(arr[i][1]);
            //	alert(arr[i][2]);
            if(arr[i][0]==0&arr[i][1]==""){
                alert("被保险人"+arr[i][2]+"证件类型为身份证,但是证件号码为空,请首先将证件类型修改为其它!");
                return false;
            }
            if(arr[i][3]==""){
                alert("被保险人"+arr[i][2]+"与主被保人的关系为空，请进行修改！");
                return false;
            }
            if(arr[i][4]==""&arr[i][3]==00){
                alert("被保险人"+arr[i][2]+"与投保人的关系为空，请进行修改！");
                return false;
            }
        }
        fm.WorkFlowFlag.value = "0000001001";					//复核完毕
        fm.MissionID.value = tMissionID;
        fm.SubMissionID.value = tSubMissionID;
        approvefalg="2";
        
        //增加polapplydate不能为空的校验
        var arrResult1 = easyExecSql("select polapplydate from LCCont where prtNo='"+fm.PrtNo.value+"' " );
        if(arrResult1 == null){
        	alert("投保日期为空,不能通过复核!");
        	return false;
        }
        if(BirthdaySexAppntIDNo()==false){return false;}
        //黑名单校验 20120503  gzh
        if(!checkBlackName()){
        	return false;
        }
    }
    else if (wFlag ==3)
    {
        if(fm.all('ProposalContNo').value == "")
        {
            alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
            return;
        }

        fm.WorkFlowFlag.value = "0000001002";					//复核修改完毕
        fm.MissionID.value = tMissionID;
        fm.SubMissionID.value = tSubMissionID;
    }
    else if(wFlag == 4)
    {
        if(fm.all('ProposalContNo').value == "")
        {
            alert("未查询出合同信息,不容许您进行 [修改完毕] 确认！");
            return;
        }
        fm.WorkFlowFlag.value = "0000001021";					//问题修改
        fm.MissionID.value = tMissionID;
        fm.SubMissionID.value = tSubMissionID;
    }   
    else
        return;

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    var tempAction=fm.action;
    fm.action = "./InputConfirm.jsp";
    fm.submit(); //提交
    fm.action=tempAction;
    //top.window.close();
}                                                                             
            
/*********************************************************************
 *  把合同信息放入内存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function putCont()
{
	delContVar();
	addIntoCont();
}

/*********************************************************************
 *  把合同信息放入加到变量中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addIntoCont()
{
	//try { mSwitch.addVar( "intoPolFlag", "", "GROUPPOL" ); } catch(ex) { };
	// body信息
	try { mSwitch.addVar( "BODY", "", window.document.body.innerHTML ); } catch(ex) { };
	// 集体信息
	//由"./AutoCreatLDGrpInit.jsp"自动生成
    try{mSwitch.addVar('ContNo','',fm.ContNo.value);}catch(ex){};
    try{mSwitch.addVar('ProposalContNo','',fm.ProposalContNo.value);}catch(ex){};
    try{mSwitch.addVar('PrtNo','',fm.PrtNo.value);}catch(ex){};
    try{mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);}catch(ex){};
    try{mSwitch.addVar('ContType','',fm.ContType.value);}catch(ex){};
    try{mSwitch.addVar('FamilyType','',fm.FamilyType.value);}catch(ex){};
    try{mSwitch.addVar('PolType','',fm.PolType.value);}catch(ex){};
    try{mSwitch.addVar('CardFlag','',fm.CardFlag.value);}catch(ex){};
    try{mSwitch.addVar('ManageCom','',fm.ManageCom.value);}catch(ex){};
    try{mSwitch.addVar('AgentCom','',fm.AgentCom.value);}catch(ex){};
    try{mSwitch.addVar('AgentCode','',fm.AgentCode.value);}catch(ex){};
    try{mSwitch.addVar('AgentGroup','',fm.AgentGroup.value);}catch(ex){};
    try{mSwitch.addVar('AgentCode1','',fm.AgentCode1.value);}catch(ex){};
    try{mSwitch.addVar('AgentType','',fm.AgentType.value);}catch(ex){};
    try{mSwitch.addVar('SaleChnl','',fm.SaleChnl.value);}catch(ex){};
    try{mSwitch.addVar('Handler','',fm.Handler.value);}catch(ex){};
    try{mSwitch.addVar('Password','',fm.Password.value);}catch(ex){};
    try{mSwitch.addVar('AppntNo','',fm.AppntNo.value);}catch(ex){};
    try{mSwitch.addVar('AppntName','',fm.AppntName.value);}catch(ex){};
    try{mSwitch.addVar('AppntSex','',fm.AppntSex.value);}catch(ex){};
    try{mSwitch.addVar('AppntBirthday','',fm.AppntBirthday.value);}catch(ex){};
    try{mSwitch.addVar('AppntIDType','',fm.AppntIDType.value);}catch(ex){};
    try{mSwitch.addVar('AppntIDNo','',fm.AppntIDNo.value);}catch(ex){};
    try{mSwitch.addVar('InsuredNo','',fm.InsuredNo.value);}catch(ex){};
    try{mSwitch.addVar('AppIDStartDate','',fm.AppIDStartDate.value);}catch(ex){};
    try{mSwitch.addVar('AppIDEndDate','',fm.AppIDEndDate.value);}catch(ex){};
    try{mSwitch.addVar('InsuredName','',fm.InsuredName.value);}catch(ex){};
    try{mSwitch.addVar('InsuredSex','',fm.InsuredSex.value);}catch(ex){};
    try{mSwitch.addVar('InsuredBirthday','',fm.InsuredBirthday.value);}catch(ex){};
    try{mSwitch.addVar('InsuredIDType','',fm.InsuredIDType.value);}catch(ex){};
    try{mSwitch.addVar('InsuredIDNo','',fm.InsuredIDNo.value);}catch(ex){};
    try{mSwitch.addVar('PayIntv','',fm.PayIntv.value);}catch(ex){};
    try{mSwitch.addVar('PayMode','',fm.PayMode.value);}catch(ex){};
    try{mSwitch.addVar('PayLocation','',fm.PayLocation.value);}catch(ex){};
    try{mSwitch.addVar('DisputedFlag','',fm.DisputedFlag.value);}catch(ex){};
    try{mSwitch.addVar('OutPayFlag','',fm.OutPayFlag.value);}catch(ex){};
    try{mSwitch.addVar('GetPolMode','',fm.GetPolMode.value);}catch(ex){};
    try{mSwitch.addVar('SignCom','',fm.SignCom.value);}catch(ex){};
    try{mSwitch.addVar('SignDate','',fm.SignDate.value);}catch(ex){};
    try{mSwitch.addVar('SignTime','',fm.SignTime.value);}catch(ex){};
    try{mSwitch.addVar('ConsignNo','',fm.ConsignNo.value);}catch(ex){};
    try{mSwitch.addVar('BankCode','',fm.BankCode.value);}catch(ex){};
    try{mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);}catch(ex){};
    try{mSwitch.addVar('AccName','',fm.AccName.value);}catch(ex){};
    try{mSwitch.addVar('PrintCount','',fm.PrintCount.value);}catch(ex){};
    try{mSwitch.addVar('LostTimes','',fm.LostTimes.value);}catch(ex){};
    try{mSwitch.addVar('Lang','',fm.Lang.value);}catch(ex){};
    try{mSwitch.addVar('Currency','',fm.Currency.value);}catch(ex){};
    try{mSwitch.addVar('Remark','',fm.Remark.value);}catch(ex){};
    try{mSwitch.addVar('Peoples','',fm.Peoples.value);}catch(ex){};
    try{mSwitch.addVar('Mult','',fm.Mult.value);}catch(ex){};
    try{mSwitch.addVar('Prem','',fm.Prem.value);}catch(ex){};
    try{mSwitch.addVar('Amnt','',fm.Amnt.value);}catch(ex){};
    try{mSwitch.addVar('SumPrem','',fm.SumPrem.value);}catch(ex){};
    try{mSwitch.addVar('Dif','',fm.Dif.value);}catch(ex){};
    try{mSwitch.addVar('PaytoDate','',fm.PaytoDate.value);}catch(ex){};
    try{mSwitch.addVar('FirstPayDate','',fm.FirstPayDate.value);}catch(ex){};
    try{mSwitch.addVar('CValiDate','',fm.CValiDate.value);}catch(ex){};
    try{mSwitch.addVar('InputOperator','',fm.InputOperator.value);}catch(ex){};
    try{mSwitch.addVar('InputDate','',fm.InputDate.value);}catch(ex){};
    try{mSwitch.addVar('InputTime','',fm.InputTime.value);}catch(ex){};
    try{mSwitch.addVar('ApproveFlag','',fm.ApproveFlag.value);}catch(ex){};
    try{mSwitch.addVar('ApproveCode','',fm.ApproveCode.value);}catch(ex){};
    try{mSwitch.addVar('ApproveDate','',fm.ApproveDate.value);}catch(ex){};
    try{mSwitch.addVar('ApproveTime','',fm.ApproveTime.value);}catch(ex){};
    try{mSwitch.addVar('UWFlag','',fm.UWFlag.value);}catch(ex){};
    try{mSwitch.addVar('UWOperator','',fm.UWOperator.value);}catch(ex){};
    try{mSwitch.addVar('UWDate','',fm.UWDate.value);}catch(ex){};
    try{mSwitch.addVar('UWTime','',fm.UWTime.value);}catch(ex){};
    try{mSwitch.addVar('AppFlag','',fm.AppFlag.value);}catch(ex){};
    try{mSwitch.addVar('PolApplyDate','',fm.PolApplyDate.value);}catch(ex){};
    try{mSwitch.addVar('GetPolDate','',fm.GetPolDate.value);}catch(ex){};
    try{mSwitch.addVar('GetPolTime','',fm.GetPolTime.value);}catch(ex){};
    try{mSwitch.addVar('CustomGetPolDate','',fm.CustomGetPolDate.value);}catch(ex){};
    try{mSwitch.addVar('State','',fm.State.value);}catch(ex){};
    try{mSwitch.addVar('Operator','',fm.Operator.value);}catch(ex){};
    try{mSwitch.addVar('MakeDate','',fm.MakeDate.value);}catch(ex){};
    try{mSwitch.addVar('MakeTime','',fm.MakeTime.value);}catch(ex){};
    try{mSwitch.addVar('ModifyDate','',fm.ModifyDate.value);}catch(ex){};
    try{mSwitch.addVar('ModifyTime','',fm.ModifyTime.value);}catch(ex){};
   
   //新的数据处理
try { mSwitch.addVar('AppntNo','',fm.AppntNo.value); } catch(ex) { };				
try { mSwitch.addVar('AppntName','',fm.AppntName.value); } catch(ex) { };			
try { mSwitch.addVar('AppntSex','',fm.AppntSex.value); } catch(ex) { };				
try { mSwitch.addVar('AppntBirthday','',fm.AppntBirthday.value); } catch(ex) { };		
try { mSwitch.addVar('AppntIDType','',fm.AppntIDType.value); } catch(ex) { };			
try { mSwitch.addVar('AppntIDNo','',fm.AppntIDNo.value); } catch(ex) { };
try { mSwitch.addVar('AppntPassword','',fm.AppntPassword.value); } catch(ex) { };		
try { mSwitch.addVar('AppIDStartDate','',fm.AppIDEndDate.value); } catch(ex) { };	
try { mSwitch.addVar('AppIDEndDate','',fm.AppIDEndDate.value); } catch(ex) { };		
try { mSwitch.addVar('AppntNativePlace','',fm.AppntNativePlace.value); } catch(ex) { };		
try { mSwitch.addVar('AppntNationality','',fm.AppntNationality.value); } catch(ex) { };
try { mSwitch.addVar('AddressNo','',fm.AddressNo.value); } catch(ex) { };		
try { mSwitch.addVar('AppntRgtAddress','',fm.AppntRgtAddress.value); } catch(ex) { };		
try { mSwitch.addVar('AppntMarriage','',fm.AppntMarriage.value); } catch(ex) { };		
try { mSwitch.addVar('AppntMarriageDate','',fm.AppntMarriageDate.value); } catch(ex) { };	
try { mSwitch.addVar('AppntHealth','',fm.AppntHealth.value); } catch(ex) { };			
try { mSwitch.addVar('AppntStature','',fm.AppntStature.value); } catch(ex) { };			
try { mSwitch.addVar('AppntAvoirdupois','',fm.AppntAvoirdupois.value); } catch(ex) { };		
try { mSwitch.addVar('AppntDegree','',fm.AppntDegree.value); } catch(ex) { };			
try { mSwitch.addVar('AppntCreditGrade','',fm.AppntCreditGrade.value); } catch(ex) { };		
try { mSwitch.addVar('AppntOthIDType','',fm.AppntOthIDType.value); } catch(ex) { };		
try { mSwitch.addVar('AppntOthIDNo','',fm.AppntOthIDNo.value); } catch(ex) { };			
try { mSwitch.addVar('AppntICNo','',fm.AppntICNo.value); } catch(ex) { };			
try { mSwitch.addVar('AppntGrpNo','',fm.AppntGrpNo.value); } catch(ex) { };			
try { mSwitch.addVar('AppntJoinCompanyDate','',fm.AppntJoinCompanyDate.value); } catch(ex) { };	
try { mSwitch.addVar('AppntStartWorkDate','',fm.AppntStartWorkDate.value); } catch(ex) { };	
try { mSwitch.addVar('AppntPosition','',fm.AppntPosition.value); } catch(ex) { };		
try { mSwitch.addVar('AppntSalary','',fm.AppntSalary.value); } catch(ex) { };			
try { mSwitch.addVar('AppntOccupationType','',fm.AppntOccupationType.value); } catch(ex) { };	
try { mSwitch.addVar('AppntOccupationCode','',fm.AppntOccupationCode.value); } catch(ex) { };  	
try { mSwitch.addVar('AppntWorkType','',fm.AppntWorkType.value); } catch(ex) { };		
try { mSwitch.addVar('AppntPluralityType','',fm.AppntPluralityType.value); } catch(ex) { };	
try { mSwitch.addVar('AppntDeathDate','',fm.AppntDeathDate.value); } catch(ex) { };		
try { mSwitch.addVar('AppntSmokeFlag','',fm.AppntSmokeFlag.value); } catch(ex) { };		
try { mSwitch.addVar('AppntBlacklistFlag','',fm.AppntBlacklistFlag.value); } catch(ex) { };	
try { mSwitch.addVar('AppntProterty','',fm.AppntProterty.value); } catch(ex) { };		
try { mSwitch.addVar('AppntRemark','',fm.AppntRemark.value); } catch(ex) { };			
try { mSwitch.addVar('AppntState','',fm.AppntState.value); } catch(ex) { };			
try { mSwitch.addVar('AppntOperator','',fm.AppntOperator.value); } catch(ex) { };		
try { mSwitch.addVar('AppntMakeDate','',fm.AppntMakeDate.value); } catch(ex) { };		
try { mSwitch.addVar('AppntMakeTime','',fm.AppntMakeTime.value); } catch(ex) { };		
try { mSwitch.addVar('AppntModifyDate','',fm.AppntModifyDate.value); } catch(ex) { };		
try { mSwitch.addVar('AppntModifyTime','',fm.AppntModifyTime.value); } catch(ex) { };
try { mSwitch.addVar('AppntHomeAddress','',fm.AppntHomeAddress.value); } catch(ex) { };
try { mSwitch.addVar('AppntHomeZipCode','',fm.AppntHomeZipCode.value); } catch(ex) { };
try { mSwitch.addVar('AppntHomePhone','',fm.AppntHomePhone.value); } catch(ex) { };
try { mSwitch.addVar('AppntHomeFax','',fm.AppntHomeFax.value); } catch(ex) { };		
try { mSwitch.addVar('AppntGrpName','',fm.AppntGrpName.value); } catch(ex) { };
try { mSwitch.addVar('AppntGrpPhone','',fm.AppntGrpPhone.value); } catch(ex) { };
try { mSwitch.addVar('CompanyAddress','',fm.CompanyAddress.value); } catch(ex) { };
try { mSwitch.addVar('AppntGrpZipCode','',fm.AppntGrpZipCode.value); } catch(ex) { };
try { mSwitch.addVar('AppntGrpFax','',fm.AppntGrpFax.value); } catch(ex) { };
try { mSwitch.addVar('AppntPostalAddress','',fm.AppntPostalAddress.value); } catch(ex) { };	
try { mSwitch.addVar('AppntZipCode','',fm.AppntZipCode.value); } catch(ex) { };
try { mSwitch.addVar('AppntPhone','',fm.AppntPhone.value); } catch(ex) { };
try { mSwitch.addVar('AppntMobile','',fm.AppntMobile.value); } catch(ex) { };
try { mSwitch.addVar('AppntFax','',fm.AppntFax.value); } catch(ex) { };
try { mSwitch.addVar('AppntEMail','',fm.AppntEMail.value); } catch(ex) { };
try { mSwitch.addVar('AppntBankAccNo','',fm.all('AppntBankAccNo').value); } catch(ex) { };    
try { mSwitch.addVar('AppntBankCode','',fm.all('AppntBankCode').value); } catch(ex) { };    
try { mSwitch.addVar('AppntAccName','',fm.all('AppntAccName').value); } catch(ex) { };   
try { mSwitch.addVar('ActivityID' ,'',ActivityID); } catch(ex) {};
try { mSwitch.addVar('MissionID' ,'',tMissionID); } catch(ex) {} ;
try { mSwitch.addVar('SubMissionID' ,'',tSubMissionID); } catch(ex) {} ;
try { mSwitch.addVar('LoadFlag' ,'',LoadFlag); } catch(ex) {} ;
}  
   
/*********************************************************************
 *  把集体信息从变量中删除
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delContVar()
{  
	try { mSwitch.deleteVar( "intoPolFlag" ); } catch(ex) { };
	// body信息
	try { mSwitch.deleteVar( "BODY" ); } catch(ex) { };
	// 集体信息
	//由"./AutoCreatLDGrpInit.jsp"自动生成
   try { mSwitch.deleteVar('ContNo'); } catch(ex) { };
   try { mSwitch.deleteVar('ProposalContNo'); } catch(ex) { };
   try { mSwitch.deleteVar('PrtNo'); } catch(ex) { };
   try { mSwitch.deleteVar('GrpContNo'); } catch(ex) { };
   try { mSwitch.deleteVar('ContType'); } catch(ex) { };
   try { mSwitch.deleteVar('FamilyType'); } catch(ex) { };
   try { mSwitch.deleteVar('PolType'); } catch(ex) { };
   try { mSwitch.deleteVar('CardFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('ManageCom'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentCom'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentCode'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentGroup'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentCode1'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentType'); } catch(ex) { };
   try { mSwitch.deleteVar('SaleChnl'); } catch(ex) { };
   try { mSwitch.deleteVar('Handler'); } catch(ex) { };
   try { mSwitch.deleteVar('Password'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntNo'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntName'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntSex'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntBirthday'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntIDType'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntIDNo'); } catch(ex) { };
   try { mSwitch.deleteVar('AppIDStartDate'); } catch(ex) { };
   try { mSwitch.deleteVar('AppIDEndDate'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredNo'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredNam'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredSex'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredBirthday'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredIDType'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredIDNo'); } catch(ex) { };
   try { mSwitch.deleteVar('PayIntv'); } catch(ex) { };
   try { mSwitch.deleteVar('PayMode'); } catch(ex) { };
   try { mSwitch.deleteVar('PayLocation'); } catch(ex) { };
   try { mSwitch.deleteVar('DisputedFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('OutPayFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('GetPolMode'); } catch(ex) { };
   try { mSwitch.deleteVar('SignCom'); } catch(ex) { };
   try { mSwitch.deleteVar('SignDate'); } catch(ex) { };
   try { mSwitch.deleteVar('SignTime'); } catch(ex) { };
   try { mSwitch.deleteVar('ConsignNo'); } catch(ex) { };
   try { mSwitch.deleteVar('BankCode'); } catch(ex) { };
   try { mSwitch.deleteVar('BankAccNo'); } catch(ex) { };
   try { mSwitch.deleteVar('AccName'); } catch(ex) { };
   try { mSwitch.deleteVar('PrintCount'); } catch(ex) { };
   try { mSwitch.deleteVar('LostTimes'); } catch(ex) { };
   try { mSwitch.deleteVar('Lang'); } catch(ex) { };
   try { mSwitch.deleteVar('Currency'); } catch(ex) { };
   try { mSwitch.deleteVar('Remark'); } catch(ex) { };
   try { mSwitch.deleteVar('Peoples'); } catch(ex) { };
   try { mSwitch.deleteVar('Mult'); } catch(ex) { };
   try { mSwitch.deleteVar('Prem'); } catch(ex) { };
   try { mSwitch.deleteVar('Amnt'); } catch(ex) { };
   try { mSwitch.deleteVar('SumPrem'); } catch(ex) { };
   try { mSwitch.deleteVar('Dif'); } catch(ex) { };
   try { mSwitch.deleteVar('PaytoDate'); } catch(ex) { };
   try { mSwitch.deleteVar('FirstPayDate'); } catch(ex) { };
   try { mSwitch.deleteVar('CValiDate'); } catch(ex) { };
   try { mSwitch.deleteVar('InputOperator'); } catch(ex) { };
   try { mSwitch.deleteVar('InputDate'); } catch(ex) { };
   try { mSwitch.deleteVar('InputTime'); } catch(ex) { };
   try { mSwitch.deleteVar('ApproveFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('ApproveCode'); } catch(ex) { };
   try { mSwitch.deleteVar('ApproveDate'); } catch(ex) { };
   try { mSwitch.deleteVar('ApproveTime'); } catch(ex) { };
   try { mSwitch.deleteVar('UWFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('UWOperator'); } catch(ex) { };
   try { mSwitch.deleteVar('UWDate'); } catch(ex) { };
   try { mSwitch.deleteVar('UWTime'); } catch(ex) { };
   try { mSwitch.deleteVar('AppFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('PolApplyDate'); } catch(ex) { };
   try { mSwitch.deleteVar('GetPolDate'); } catch(ex) { };
   try { mSwitch.deleteVar('GetPolTime'); } catch(ex) { };
   try { mSwitch.deleteVar('CustomGetPolDate'); } catch(ex) { };
   try { mSwitch.deleteVar('State'); } catch(ex) { };
   try { mSwitch.deleteVar('Operator'); } catch(ex) { };
   try { mSwitch.deleteVar('MakeDate'); } catch(ex) { };
   try { mSwitch.deleteVar('MakeTime'); } catch(ex) { };
   try { mSwitch.deleteVar('ModifyDate'); } catch(ex) { };
   try { mSwitch.deleteVar('ModifyTime'); } catch(ex) { };

    //新的删除数据处理
try { mSwitch.deleteVar  ('AppntNo'); } catch(ex) { };				
try { mSwitch.deleteVar  ('AppntName'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntSex'); } catch(ex) { };				
try { mSwitch.deleteVar  ('AppntBirthday'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntIDType'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntIDNo'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntPassword'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntNativePlace'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntNationality'); } catch(ex) { };	
try {mSwitch.deleteVar('AddressNo');}catch(ex){};   	
try { mSwitch.deleteVar  ('AppntRgtAddress'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntMarriage'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntMarriageDate'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntHealth'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntStature'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntAvoirdupois'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntDegree'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntCreditGrade'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntOthIDType'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntOthIDNo'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntICNo'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntGrpNo'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntJoinCompanyDate'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntStartWorkDate'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntPosition') } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntSalary'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntOccupationType'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntOccupationCode'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntWorkType'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntPluralityType');} catch(ex) { };	
try { mSwitch.deleteVar  ('AppntDeathDate'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntSmokeFlag'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntBlacklistFlag'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntProterty'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntRemark'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntState'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntOperator'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntMakeDate'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntMakeTime'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntModifyDate'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntModifyTime'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntHomeAddress'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntHomeZipCode'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntHomePhone'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntHomeFax'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntPostalAddress'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntZipCode'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntPhone'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntFax'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntMobile'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntEMail'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntGrpName'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntGrpPhone'); } catch(ex) { };
try { mSwitch.deleteVar  ('CompanyAddress'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntGrpZipCode'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntGrpFax'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntBankAccNo'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntBankCode'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntAccName'); } catch(ex) { };
try { mSwitch.deleteVar  ('ActivityID' ); } catch(ex) {};
try { mSwitch.deleteVar  ('MissionID' ); } catch(ex) {} ;
try { mSwitch.deleteVar  ('SubMissionID'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('LoadFlag'); } catch(ex) {} ;
}


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {
  //alert("here:" + arrQueryResult + "\n" + mOperate);
	if( arrQueryResult != null ) {
		arrResult = arrQueryResult;

		if( mOperate == 1 )	{		// 查询投保单	
			fm.all( 'ContNo' ).value = arrQueryResult[0][0];

			arrResult = easyExecSql("select ProposalContNo,PrtNo,ManageCom,SaleChnl,AgentCode,AgentGroup,AgentCode1,AgentCom,AgentType,Remark,ReceiveDate,FirstTrialOperator,FirstTrialDate from LCCont where LCCont = '" + arrQueryResult[0][0] + "'", 1, 0);

			if (arrResult == null) {
			  alert("未查到投保单信息");
			} else {
			   displayLCContPol(arrResult[0]);
			}
		}
		if( mOperate == 2 )	{		// 投保人信息
			arrResult = easyExecSql("select a.* from LDPerson a where 1=1  and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
			if (arrResult == null) {
			  alert("未查到投保人信息");
			} else {
			   displayAppnt(arrResult[0]);
			   getaddresscodedata();
			  showAllCodeName(); 
			}
		}
	}
	mOperate = 0;		// 恢复初态
}
/*********************************************************************
 *  投保单信息初始化函数。以loadFlag标志作为分支
 *  参数  ：  投保单印刷号
 *  返回值：  无
 *********************************************************************
 */
function detailInit(PrtNo){
try{
  if(PrtNo==null)
    return;
  arrResult=easyExecSql("select GRPCONTNO ,CONTNO ,PROPOSALCONTNO ,PRTNO ,CONTTYPE ,FAMILYTYPE ,FAMILYID ,POLTYPE ,CARDFLAG ,MANAGECOM ,EXECUTECOM ,AGENTCOM ,AGENTCODE ,AGENTGROUP ,AGENTCODE1 ,AGENTTYPE ,SALECHNL ,HANDLER ,PASSWORD ,APPNTNO ,APPNTNAME ,APPNTSEX ,APPNTBIRTHDAY,APPNTIDTYPE ,APPNTIDNO ,INSUREDNO ,INSUREDNAME ,INSUREDSEX ,INSUREDBIRTHDAY,INSUREDIDTYPE ,INSUREDIDNO ,PAYINTV,PAYMODE ,PAYLOCATION ,DISPUTEDFLAG ,OUTPAYFLAG ,GETPOLMODE ,SIGNCOM ,SIGNDATE,SIGNTIME ,CONSIGNNO ,BANKCODE ,BANKACCNO ,ACCNAME ,PRINTCOUNT,LOSTTIMES,LANG ,CURRENCY ,REMARK ,PEOPLES,MULT,PREM ,AMNT ,SUMPREM ,DIF ,PAYTODATE ,FIRSTPAYDATE ,CVALIDATE ,INPUTOPERATOR ,INPUTDATE ,INPUTTIME ,APPROVEFLAG ,APPROVECODE ,APPROVEDATE ,APPROVETIME ,UWFLAG ,UWOPERATOR ,UWDATE ,UWTIME ,APPFLAG ,POLAPPLYDATE ,GETPOLDATE ,GETPOLTIME ,CUSTOMGETPOLDATE ,STATE ,OPERATOR ,MAKEDATE ,MAKETIME ,MODIFYDATE ,MODIFYTIME ,FIRSTTRIALOPERATOR ,FIRSTTRIALDATE ,FIRSTTRIALTIME ,RECEIVEOPERATOR ,RECEIVEDATE ,RECEIVETIME ,TEMPFEENO ,PROPOSALTYPE ,SALECHNLDETAIL ,CONTPRINTLOFLAG ,CONTPREMFEENO ,CUSTOMERRECEIPTNO ,CINVALIDATE ,COPYS ,DEGREETYPE ,HANDLERDATE ,HANDLERPRINT ,STATEFLAG ,PREMSCOPE ,INTLFLAG ,UWCONFIRMNO ,PAYERTYPE ,GRPAGENTCOM ,GRPAGENTCODE ,GRPAGENTNAME ,ACCTYPE ,CRS_SALECHNL ,CRS_BUSSTYPE ,GRPAGENTIDNO ,DUEFEEMSGFLAG ,PAYMETHOD ,EXISPEC ,EXPAYMODE ,AGENTSALECODE from L" +　contState + "Cont where PrtNo='"+PrtNo+"'",1,0);
  if(arrResult==null){
    alert(未得到投保单信息);
    return;
  }else{
    displayLCCont();       //显示投保单详细内容
    showOneCodeName("PayIntv",fm.PayIntvName.name);
    showOneCodeName("PayMode",fm.PayModeName.name);
  }
  arrResult = easyExecSql("select a.* from LDPerson a where 1=1  and a.CustomerNo = '" + arrResult[0][19] + "'", 1, 0);
  if (arrResult == null) {
    alert("未查到投保人信息");
  } else {
     displayAppnt(arrResult[0]);
  }  
  arrResult=easyExecSql("select GRPCONTNO ,CONTNO ,PRTNO ,APPNTNO ,APPNTGRADE ,APPNTNAME ,APPNTSEX ,APPNTBIRTHDAY ,APPNTTYPE ,ADDRESSNO ,IDTYPE ,IDNO ,NATIVEPLACE ,NATIONALITY ,RGTADDRESS ,MARRIAGE ,MARRIAGEDATE ,HEALTH ,STATURE ,AVOIRDUPOIS ,DEGREE ,CREDITGRADE ,BANKCODE ,BANKACCNO ,ACCNAME ,JOINCOMPANYDATE ,STARTWORKDATE ,POSITION ,SALARY ,OCCUPATIONTYPE ,OCCUPATIONCODE ,WORKTYPE ,PLURALITYTYPE ,SMOKEFLAG ,OPERATOR ,MANAGECOM ,MAKEDATE ,MAKETIME ,MODIFYDATE ,MODIFYTIME ,BMI ,OTHIDTYPE ,OTHIDNO ,ENGLISHNAME ,IDSTARTDATE ,IDENDDATE ,EXISPEC from L" +　contState + "Appnt where PrtNo='"+PrtNo+"'",1,0);
  if(arrResult==null){
    alert("未得到投保人信息");
    return;
  }else{
    displayContAppnt();       //显示投保人的详细内容
    //displayAccount();			//显示头投保人帐户信息
  }
  var tContNo = arrResult[0][1];  
  var tCustomerNo = arrResult[0][3];		// 得到投保人客户号
  var tAddressNo = arrResult[0][9]; 		// 得到投保人地址号
  getdetailaddress();
  fm.AddressNo.value=tAddressNo;
  getdetailaddress();//显示投保人地址详细内容
  var strSQL1="select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LCCustomerImpart where CustomerNo='"+tCustomerNo+"' and ContNo='"+tContNo+"' and CustomerNoType='A'";
  turnPage.strQueryResult = easyQueryVer3(strSQL1, 1, 0, 1);
  //判断是否查询成功,成功则显示告知信息
  if (turnPage.strQueryResult) {
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = ImpartGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL1; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
  else{
    initImpartGrid();
  }
  var arr = easyExecSql("select GrpName from LCAddress where CustomerNo='"+tCustomerNo+"' and AddressNo='"+tAddressNo+"'");
  	if(arr)
  	{
  		fm.AppntGrpName.value = arr[0][0];
  	}
}catch(ex){}
 
}

/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAccount()
{
//	if(fm.all('PayMode').value=="4" 
//	||fm.all('PayMode').value=="6" 
//    || fm.all('PayMode').value=="1"
//        || fm.all('PayMode').value=="11" 
//        || fm.all('PayMode').value=="12")
//	{
	try{fm.all('AppntBankAccNo').value= arrResult[0][24]; }catch(ex){}; 
	try{fm.all('AppntBankCode').value= arrResult[0][23]; }catch(ex){}; 
	try{fm.all('AppntAccName').value= arrResult[0][25]; }catch(ex){};
	
// }
//else{
	      //fm.all('AppntBankCode').style.display="none";
        //fm.all('AppntBankCodeName').style.display="none";
        //fm.all('AppntAccName').style.display="none";
        //fm.all('AppntBankAccNo').style.display="none";
//	}
	
}
/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayCustomer()
{
	try{fm.all('AppntNationality').value= arrResult[0][8]; }catch(ex){}; 
}
/*********************************************************************
 *  把查询返回的客户地址数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAddress()
{
try{fm.all('AddressNo').value= arrResult[0][0];}catch(ex){}; 
try{fm.all('AppntPostalAddress').value= arrResult[0][1];}catch(ex){}; 
try{fm.all('AppntZipCode').value= arrResult[0][2];}catch(ex){}; 
try{fm.all('AppntPhone').value= arrResult[0][3];}catch(ex){}; 
try{fm.all('AppntMobile').value= arrResult[0][4];}catch(ex){}; 
try{fm.all('AppntEMail').value= arrResult[0][5];}catch(ex){}; 
//try{fm.all('GrpName').value= arrResult[0][6];}catch(ex){}; 
try{fm.all('AppntGrpPhone').value= arrResult[0][6];}catch(ex){}; 
try{fm.all('CompanyAddress').value= arrResult[0][7];}catch(ex){}; 
try{fm.all('AppntGrpZipCode').value= arrResult[0][8];}catch(ex){}; 
/*	
        try{fm.all('AppntPostalAddress').value= arrResult[0][2]; }catch(ex){}; 
	try{fm.all('AppntZipCode').value= arrResult[0][3]; }catch(ex){}; 
	try{fm.all('AppntPhone').value= arrResult[0][4]; }catch(ex){}; 
	try{fm.all('AppntMobile').value= arrResult[0][14]; }catch(ex){}; 
	try{fm.all('AppntEMail').value= arrResult[0][16]; }catch(ex){}; 
	//try{fm.all('AppntGrpName').value= arrResult[0][2]; }catch(ex){}; 
	try{fm.all('AppntGrpPhone').value= arrResult[0][12]; }catch(ex){}; 
	try{fm.all('CompanyAddress').value= arrResult[0][10]; }catch(ex){}; 
	try{fm.all('AppntGrpZipCode').value= arrResult[0][11]; }catch(ex){}; 
*/
}

/*********************************************************************
 *  把查询返回的合同中被保人数据显示到页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayContAppnt()
{

try{fm.all('AppntGrpContNo').value= arrResult[0][0];}catch(ex){};             
try{fm.all('AppntContNo').value= arrResult[0][1];}catch(ex){};                 
try{fm.all('AppntPrtNo').value= arrResult[0][2];}catch(ex){};                  
try{fm.all('AppntNo').value= arrResult[0][3];}catch(ex){};                
try{fm.all('AppntGrade').value= arrResult[0][4];}catch(ex){};             
try{fm.all('AppntName').value= arrResult[0][5];}catch(ex){};              
try{fm.all('AppntSex').value= arrResult[0][6];}catch(ex){};               
try{fm.all('AppntBirthday').value= arrResult[0][7];}catch(ex){};          
try{fm.all('AppntType').value= arrResult[0][8];}catch(ex){};              
try{fm.all('AppntAddressNo').value= arrResult[0][9]; }catch(ex){};             
try{fm.all('AppntIDType').value= arrResult[0][10]; }catch(ex){};               
try{fm.all('AppntIDNo').value= arrResult[0][11]; }catch(ex){};                 
try{fm.all('AppntNativePlace').value= arrResult[0][12];}catch(ex){};           
try{fm.all('AppntNationality').value= arrResult[0][13];}catch(ex){};           
try{fm.all('AppntRgtAddress').value= arrResult[0][14];}catch(ex){};            
try{fm.all('AppntMarriage').value= arrResult[0][15];}catch(ex){};             
try{fm.all('AppntMarriageDate').value= arrResult[0][16];}catch(ex){};          
try{fm.all('AppntHealth').value= arrResult[0][17];}catch(ex){};                
try{fm.all('AppntStature').value= arrResult[0][18];}catch(ex){};               
try{fm.all('AppntAvoirdupois').value= arrResult[0][19];}catch(ex){};           
try{fm.all('AppntDegree').value= arrResult[0][20];}catch(ex){};                          
 if(fm.all('PayMode').value!="")
 {
 		//alert(fm.all('PayMode').value);
	//if(fm.all('PayMode').value=="4"
	//||fm.all('PayMode').value=="6"
    //|| fm.all('PayMode').value=="1"
    //    || fm.all('PayMode').value == "11"
    //    || fm.all('PayMode').value == "12")
	//{
			//alert();
				try{fm.all('AppntBankCode').value= arrResult[0][22];}catch(ex){}; 
	      try{fm.all('AppntAccName').value= arrResult[0][24]; }catch(ex){};  
	      try{fm.all('AppntBankAccNo').value= arrResult[0][23];}catch(ex){};
	      var tsql="select BankName from LDBank where (BankUniteFlag is null or BankUniteFlag<>'1') and bankcode='"+fm.all('AppntBankCode').value+"'";
	     // alert(tsql);
	      var bankname=easyExecSql(tsql);
          bankname = bankname == "null" ? "" : bankname;
				//alert(bankname);
				try{fm.all('AppntBankCodeName').value= bankname;}catch(ex){}; 

  //}
  //else
  //{
	      //fm.all('AppntBankCode').style.display="none";
        //fm.all('AppntBankCodeName').style.display="none";
        //fm.all('AppntAccName').style.display="none";
        //fm.all('AppntBankAccNo').style.display="none";
	//}
}                     
try{fm.all('AppntJoinCompanyDate').value= arrResult[0][25];}catch(ex){};       
try{fm.all('AppntStartWorkDate').value= arrResult[0][26];}catch(ex){};         
try{fm.all('AppntPosition').value= arrResult[0][27];}catch(ex){};              
if(arrResult[0][28]=='-1'){
	try{fm.all('AppntSalary').value=""; }catch(ex){};               
}else{
	try{fm.all('AppntSalary').value= arrResult[0][28]; }catch(ex){};
}              
try{fm.all('AppntOccupationType').value= arrResult[0][29];}catch(ex){};     
try{fm.all('AppntOccupationCode').value= arrResult[0][30];}catch(ex){};
try {fm.all('AppntOccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][30]);} catch(ex) { };      
try{fm.all('AppntWorkType').value= arrResult[0][31]; }catch(ex){};             
try{fm.all('AppntPluralityType').value= arrResult[0][32];}catch(ex){};         
try{fm.all('AppntSmokeFlag').value= arrResult[0][33];}catch(ex){};             
try{fm.all('AppntOperator').value= arrResult[0][34];}catch(ex){};              
try{fm.all('AppntManageCom').value= arrResult[0][35];}catch(ex){};             
try{fm.all('AppntMakeDate').value= arrResult[0][36];}catch(ex){};              
try{fm.all('AppntMakeTime').value= arrResult[0][37]; }catch(ex){};             
try{fm.all('AppntModifyDate').value= arrResult[0][38];}catch(ex){};            
try{fm.all('AppntModifyTime').value= arrResult[0][39];}catch(ex){};  

try{fm.all('AppIDStartDate').value= arrResult[0][44];}catch(ex){};
try{fm.all('AppIDEndDate').value= arrResult[0][45];}catch(ex){};
//try{fm.all('ExiSpec').value= arrResult[0][46];}catch(ex){};  // 投保人不需要这个字段，投保备注栏应该是保单层的数据，这个字段估计是创建错了.. 
}
/*********************************************************************
 *  把查询返回的投保人数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt()
{
try{fm.all('AppntNo').value= arrResult[0][0]; }catch(ex){};              
try{fm.all('AppntName').value= arrResult[0][1]; }catch(ex){};            
try{fm.all('AppntSex').value= arrResult[0][2]; }catch(ex){};             
try{fm.all('AppntBirthday').value= arrResult[0][3]; }catch(ex){};        
try{fm.all('AppntIDType').value= arrResult[0][4]; }catch(ex){};          
try{fm.all('AppntIDNo').value= arrResult[0][5]; }catch(ex){};            
try{fm.all('AppntPassword').value= arrResult[0][6]; }catch(ex){};        
try{fm.all('AppntNativePlace').value= arrResult[0][7]; }catch(ex){};     
try{fm.all('AppntNationality').value= arrResult[0][8]; }catch(ex){};     
try{fm.all('AppntRgtAddress').value= arrResult[0][9]; }catch(ex){};      
try{fm.all('AppntMarriage').value= arrResult[0][10];}catch(ex){};        
try{fm.all('AppntMarriageDate').value= arrResult[0][11];}catch(ex){};    
try{fm.all('AppntHealth').value= arrResult[0][12];}catch(ex){};          
try{fm.all('AppntStature').value= arrResult[0][13];}catch(ex){};         
try{fm.all('AppntAvoirdupois').value= arrResult[0][14];}catch(ex){};     
try{fm.all('AppntDegree').value= arrResult[0][15];}catch(ex){};          
try{fm.all('AppntCreditGrade').value= arrResult[0][16];}catch(ex){};     
try{fm.all('AppntOthIDType').value= arrResult[0][17];}catch(ex){};       
try{fm.all('AppntOthIDNo').value= arrResult[0][18];}catch(ex){};         
try{fm.all('AppntICNo').value= arrResult[0][19];}catch(ex){};   
         
try{fm.all('AppntGrpNo').value= arrResult[0][20];}catch(ex){};           
try{fm.all('AppntJoinCompanyDate').value= arrResult[0][21];}catch(ex){}; 
try{fm.all('AppntStartWorkDate').value= arrResult[0][22];}catch(ex){};   
try{fm.all('AppntPosition').value= arrResult[0][23];}catch(ex){};        
try{fm.all('AppntSalary').value= arrResult[0][24];}catch(ex){};        
try{fm.all('AppntOccupationType').value= arrResult[0][25];}catch(ex){};  
try{fm.all('AppntOccupationCode').value= arrResult[0][26];}catch(ex){};
try {fm.all('AppntOccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][26]);} catch(ex) { };    
try{fm.all('AppntWorkType').value= arrResult[0][27];}catch(ex){};        
try{fm.all('AppntPluralityType').value= arrResult[0][28];}catch(ex){};   
try{fm.all('AppntDeathDate').value= arrResult[0][29];}catch(ex){};       
try{fm.all('AppntSmokeFlag').value= arrResult[0][30];}catch(ex){};       
try{fm.all('AppntBlacklistFlag').value= arrResult[0][31];}catch(ex){};   
try{fm.all('AppntProterty').value= arrResult[0][32];}catch(ex){};        
try{fm.all('AppntRemark').value= arrResult[0][33];}catch(ex){};          
try{fm.all('AppntState').value= arrResult[0][34];}catch(ex){};
try{fm.all('VIPValue').value= arrResult[0][35];}catch(ex){};           
try{fm.all('AppntOperator').value= arrResult[0][36];}catch(ex){};        
try{fm.all('AppntMakeDate').value= arrResult[0][37];}catch(ex){};        
try{fm.all('AppntMakeTime').value= arrResult[0][38];}catch(ex){};      
try{fm.all('AppntModifyDate').value= arrResult[0][39];}catch(ex){};      
try{fm.all('AppntModifyTime').value= arrResult[0][40];}catch(ex){};      
try{fm.all('AppntGrpName').value= arrResult[0][41];}catch(ex){}; 

//try{fm.all('AppntPostalAddress').value= "";}catch(ex){}; 
//try{fm.all('AppntPostalAddress').value= "";}catch(ex){}; 
//try{fm.all('AppntZipCode').value= "";}catch(ex){}; 
//try{fm.all('AppntPhone').value= "";}catch(ex){}; 
//try{fm.all('AppntFax').value= "";}catch(ex){}; 
//try{fm.all('AppntMobile').value= "";}catch(ex){}; 
//try{fm.all('AppntEMail').value= "";}catch(ex){}; 
////try{fm.all('AppntGrpName').value= "";}catch(ex){}; 
//try{fm.all('AppntHomeAddress').value= "";}catch(ex){}; 
//try{fm.all('AppntHomeZipCode').value= "";}catch(ex){}; 
//try{fm.all('AppntHomePhone').value= "";}catch(ex){}; 
//try{fm.all('AppntHomeFax').value= "";}catch(ex){}; 
//try{fm.all('AppntGrpPhone').value= "";}catch(ex){}; 
//try{fm.all('CompanyAddress').value= "";}catch(ex){}; 
//try{fm.all('AppntGrpZipCode').value= "";}catch(ex){};  
//try{fm.all('AppntGrpFax').value= "";}catch(ex){};  

}
/**
 *投保单详细内容显示
 */
function displayLCCont() {
    try { fm.all('GrpContNo').value = arrResult[0][0]; } catch(ex) { };                    
    try { fm.all('ContNo').value = arrResult[0][1]; } catch(ex) { };                       
    try { fm.all('ProposalContNo').value = arrResult[0][2]; } catch(ex) { };               
    try { fm.all('PrtNo').value = arrResult[0][3]; } catch(ex) { };                        
    try { fm.all('ContType').value = arrResult[0][4]; } catch(ex) { };                     
    try { fm.all('FamilyType').value = arrResult[0][5]; } catch(ex) { };                
    try { fm.all('FamilyID').value = arrResult[0][6]; } catch(ex) { };                     
    try { fm.all('PolType ').value = arrResult[0][7]; } catch(ex) { };                     
    try { fm.all('CardFlag').value = arrResult[0][8]; } catch(ex) { };                     
    try { fm.all('ManageCom').value = arrResult[0][9]; } catch(ex) { };                    
    try { fm.all('ExecuteCom ').value = arrResult[0][10]; } catch(ex) { };                 
    try { fm.all('AgentCom').value = arrResult[0][11]; } catch(ex) { };                    
    try { fm.all('AgentCom1').value = arrResult[0][11]; } catch(ex) { };   
    try { fm.all('AgentCode').value = arrResult[0][12]; } catch(ex) { };
    try { fm.all('AgentGroup').value = arrResult[0][13];} catch(ex) { };                 
    try { fm.all('AgentCode1 ').value = arrResult[0][14]; } catch(ex) { };                 
    try { fm.all('AgentType').value = arrResult[0][15]; } catch(ex) { };                   
    try { fm.all('SaleChnl').value = arrResult[0][16]; } catch(ex) { };                    
    try { fm.all('Handler').value = arrResult[0][17]; } catch(ex) { };                    
    try { fm.all('Password').value = arrResult[0][18]; } catch(ex) { };                    
    try { fm.all('AppntNo').value = arrResult[0][19]; } catch(ex) { };                    
    try { fm.all('AppntName').value = arrResult[0][20]; } catch(ex) { };                   
    try { fm.all('AppntSex').value = arrResult[0][21]; } catch(ex) { };                    
    try { fm.all('AppntBirthday').value = arrResult[0][22]; } catch(ex) { };              
    try { fm.all('AppntIDType').value = arrResult[0][23]; } catch(ex) { };                 
    try { fm.all('AppntIDNo').value = arrResult[0][24]; } catch(ex) { };                   
    try { fm.all('InsuredNo').value = arrResult[0][25]; } catch(ex) { };                   
    try { fm.all('InsuredName').value = arrResult[0][26]; } catch(ex) { };                 
    try { fm.all('InsuredSex').value = arrResult[0][27]; } catch(ex) { };                 
    try { fm.all('InsuredBirthday').value = arrResult[0][28]; } catch(ex) { };             
    try { fm.all('InsuredIDType ').value = arrResult[0][29]; } catch(ex) { };              
    try { fm.all('InsuredIDNo').value = arrResult[0][30]; } catch(ex) { };                 
    try { fm.all('PayIntv').value = arrResult[0][31]; } catch(ex) { };    
    try { fm.all('PayIntvName').value = arrResult[0][31]; } catch(ex) { };                
    try { fm.all('PayMode').value = arrResult[0][32]; } catch(ex) { }; 
    try { fm.all('PayModeName').value = arrResult[0][32]; } catch(ex) { };                   
    try { fm.all('PayLocation').value = arrResult[0][33]; } catch(ex) { };                 
    try { fm.all('DisputedFlag').value = arrResult[0][34]; } catch(ex) { };                
    try { fm.all('OutPayFlag').value = arrResult[0][35]; } catch(ex) { };                 
    try { fm.all('GetPolMode').value = arrResult[0][36]; } catch(ex) { };                 
    try { fm.all('SignCom').value = arrResult[0][37]; } catch(ex) { };                    
    try { fm.all('SignDate').value = arrResult[0][38]; } catch(ex) { };                    
    try { fm.all('SignTime').value = arrResult[0][39]; } catch(ex) { };                    
    try { fm.all('ConsignNo').value = arrResult[0][40]; } catch(ex) { };                   
    try { fm.all('BankCode').value = arrResult[0][41]; } catch(ex) { };                    
    try { fm.all('BankAccNo').value = arrResult[0][42]; } catch(ex) { };                   
    try { fm.all('AccName').value = arrResult[0][43]; } catch(ex) { };                    
    try { fm.all('PrintCount ').value = arrResult[0][44]; } catch(ex) { };                 
    try { fm.all('LostTimes').value = arrResult[0][45]; } catch(ex) { };                   
    try { fm.all('Lang ').value = arrResult[0][46]; } catch(ex) { };                       
    try { fm.all('Currency').value = arrResult[0][47]; } catch(ex) { };                    
    try { fm.all('Remark').value = arrResult[0][48]; } catch(ex) { };                      
    try { fm.all('Peoples').value = arrResult[0][49]; } catch(ex) { };                    
    try { fm.all('Mult').value = arrResult[0][50]; } catch(ex) { };                       
    try { fm.all('Prem').value = arrResult[0][51]; } catch(ex) { };                       
    try { fm.all('Amnt').value = arrResult[0][52]; } catch(ex) { };                       
    try { fm.all('SumPrem').value = arrResult[0][53]; } catch(ex) { };                    
    try { fm.all('Dif').value = arrResult[0][54]; } catch(ex) { };                         
    try { fm.all('PaytoDate').value = arrResult[0][55]; } catch(ex) { };                   
    try { fm.all('FirstPayDate').value = arrResult[0][56]; } catch(ex) { };                
    try { fm.all('CValiDate').value = arrResult[0][57]; } catch(ex) { };                   
    try { fm.all('InputOperator ').value = arrResult[0][58]; } catch(ex) { };              
    try { fm.all('InputDate').value = arrResult[0][59]; } catch(ex) { };                   
    try { fm.all('InputTime').value = arrResult[0][60]; } catch(ex) { };                   
    try { fm.all('ApproveFlag').value = arrResult[0][61]; } catch(ex) { };                 
    try { fm.all('ApproveCode').value = arrResult[0][62]; } catch(ex) { };                 
    try { fm.all('ApproveDate').value = arrResult[0][63]; } catch(ex) { };                 
    try { fm.all('ApproveTime').value = arrResult[0][64]; } catch(ex) { };                 
    try { fm.all('UWFlag').value = arrResult[0][65]; } catch(ex) { };                      
    try { fm.all('UWOperator ').value = arrResult[0][66]; } catch(ex) { };                 
    try { fm.all('UWDate').value = arrResult[0][67]; } catch(ex) { };                      
    try { fm.all('UWTime').value = arrResult[0][68]; } catch(ex) { };                      
    try { fm.all('AppFlag').value = arrResult[0][69]; } catch(ex) { };                    
    try { fm.all('PolApplyDate').value = arrResult[0][70];} catch(ex) { };                
    try { fm.all('GetPolDate').value = arrResult[0][71]; } catch(ex) { };                 
    try { fm.all('GetPolTime').value = arrResult[0][72]; } catch(ex) { };                 
    try { fm.all('CustomGetPolDate').value = arrResult[0][73]; } catch(ex) { };           
    try { fm.all('State').value = arrResult[0][74]; } catch(ex) { };                       
    try { fm.all('Operator').value = arrResult[0][75]; } catch(ex) { };                    
    try { fm.all('MakeDate').value = arrResult[0][76]; } catch(ex) { };                    
    try { fm.all('MakeTime').value = arrResult[0][77]; } catch(ex) { };                    
    try { fm.all('ModifyDate').value = arrResult[0][78]; } catch(ex) { };                 
    try { fm.all('ModifyTime').value = arrResult[0][79]; } catch(ex) { };  
    try { fm.all('FirstTrialOperator').value = arrResult[0][80]; }catch(ex) { };
    try { fm.all('FirstTrialDate').value = arrResult[0][81]; } catch(ex) { }; 
    try { fm.all('FirstTrialTime').value = arrResult[0][82]; } catch(ex) { };
    try { fm.all('ReceiveOperator').value = arrResult[0][83]; } catch(ex) { };
    try { fm.all('ReceiveDate').value = arrResult[0][84]; } catch(ex) { }; 
    try { fm.all('ReceiveTime').value = arrResult[0][85]; } catch(ex) { }; 
    try { fm.all('SaleChnlDetail').value = arrResult[0][88]; } catch(ex) { }; 
    try { fm.all('TempFeeNo').value = arrResult[0][86]; } catch(ex) { };
    try { fm.all('GrpAgentCom').value = arrResult[0][102]; } catch(ex) { }; 
    try { fm.all('GrpAgentCode').value = arrResult[0][103]; } catch(ex) { }; 
    try { fm.all('GrpAgentName').value = arrResult[0][104]; } catch(ex) { };  
    try { fm.all('Crs_SaleChnl').value = arrResult[0][106]; } catch(ex) { };
    try { fm.all('Crs_BussType').value = arrResult[0][107]; } catch(ex) { };
    try { fm.all('GrpAgentIDNo').value = arrResult[0][108]; } catch(ex) { };
    try { fm.all('DueFeeMsgFlag').value = arrResult[0][109]; } catch(ex) { };
    //try { fm.all('PayMethod').value = arrResult[0][110]; } catch(ex) { };
    try { fm.all('ExiSpec').value = arrResult[0][111]; } catch(ex) { };
    try { fm.all('ExPayMode').value = arrResult[0][112]; } catch(ex) { };
    try { fm.all('AgentSaleCode').value = arrResult[0][113];} catch(ex) { };
    
    if(arrResult[0][102] != "" && arrResult[0][103] != "" &&arrResult[0][104]!=""&&arrResult[0][106]!=""&&arrResult[0][107]!=""&&arrResult[0][108]!="")
    {   
        fm.MixComFlag.checked = true;
        if(fm.MixComFlag.checked == true)
        {
            fm.all('GrpAgentComID').style.display = "";
	        fm.all('GrpAgentTitleID').style.display = "";
	        fm.all('GrpAgentTitleIDNo').style.display = "";
        }
        var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
		var arrResult1 = easyExecSql(strSql);
		if (arrResult1 != null) {
		     fm.GrpAgentComName.value = arrResult1[0][0];
		}
		else{  
		     fm.GrpAgentComName.value = "";
	    }
    }
    
    if(arrResult[0][16] == "10"){
    	fm.all("AgentSaleCodeID").style.display = "";
    	var strSql2 = "select Name from laagenttemp where '1337927697000'='1337927697000' and  AgentCode='" + fm.AgentSaleCode.value+"' with ur";
		var arrResult2 = easyExecSql(strSql2);
		if (arrResult2 != null) {
		     fm.AgentSaleName.value = arrResult2[0][0];
		}
    } else {
    	fm.all("AgentSaleCodeID").style.display = "none";
    	fm.all('AgentSaleCode').value = "";
    }
    
}                                                                                                                                                        
//使得从该窗口弹出的窗口能够聚焦                                             
function myonfocus()                                                         
{                                                                            
	if(showInfo!=null)                                                           
	{                                                                            
	  try                                                                        
	  {                                                                          
	    showInfo.focus();                                                        
      }
	  catch(ex)                                                                  
	  {                                                                          
	    showInfo=null;                                                           
	  }                                                                          
	}                                                                            
}                                                                            
                                                                             
                                                                             
                                                                             
//投保人客户号查询按扭事件                                                   
function queryAppntNo() { 
  if (fm.all("AppntNo").value == "" && loadFlag == "1") {                    
    showAppnt1();                                                            
    } else if (loadFlag != "1" && loadFlag != "2") {                         
     alert("只能在投保单录入时进行操作！");                                 
    } else {                                                    
   arrResult = easyExecSql("select a.* from LDPerson a where 1=1  and a.CustomerNo = '" + fm.all("AppntNo").value + "'", 1, 0);
    if (arrResult == null) {                                                 
      alert("未查到投保人信息");                                             
      displayAppnt(new Array());
      emptyUndefined();    
    } else {                                                                 
      displayAppnt(arrResult[0]);
      getaddresscodedata();                                            
    }                                                                  
  }                                                                          
}                                                                            
                                                                             
function showAppnt1()                                                        
{                                                                            
	if( mOperate == 0 )                                                          
	{                                                                            
		mOperate = 2;                                                        
		showInfo = window.open( "../sys/LDPersonQueryNew.html" );               
	}                                                                            
}                                                                            
                                                                             
function queryAgent()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！"); 
        return ;
    }

    if(fm.all('AgentCode').value == "")
    {  
        //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=1","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        
        var tSaleChnl = (fm.SaleChnl != null && fm.SaleChnl != "undefined") ? fm.SaleChnl.value : "";
        var tAgentCom = (fm.AgentCom != null && fm.AgentCom != "undefined") ? fm.AgentCom.value : "";
        var tBranchType = 1;
        // 个险中的职团开拓表示由团险业务员卖个单。
        // 因此，当渠道为“职团开拓”（07）时，应查询出团险的业务员。
        if(tSaleChnl == "07" || tSaleChnl == "11" || tSaleChnl == "12")
            tBranchType = 2;
        //------------------------------------
        var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&SaleChnl=" + tSaleChnl
            + "&AgentCom=" + tAgentCom
            + "&branchtype=" + tBranchType;
        var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('AgentCode').value != "")
    {
        var cAgentCode = fm.AgentCode.value;  //保单号码	
        var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentGroup.value = arrResult[0][2];
            alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value="";
            alert("代码为:[" + fm.all('AgentCode').value + "]的业务员不存在，请确认!");
        }
    }
}
//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryGrpAgent()
{
    if(fm.all('GrpAgentCode').value == "" && fm.all('GrpAgentIDNo').value == "" )
    {  
        var tGrpAgentCom = (fm.GrpAgentCom != null && fm.GrpAgentCom != "undefined") ? fm.GrpAgentCom.value : "";
        var tGrpAgentName = (fm.GrpAgentName != null && fm.GrpAgentName != "undefined") ? fm.GrpAgentName.value : "";
        var tGrpAgentIDNo = (fm.GrpAgentIDNo != null && fm.GrpAgentIDNo != "undefined") ? fm.GrpAgentIDNo.value : "";
        var strURL = "../sys/GrpAgentCommonQueryMain.jsp?GrpAgentCom=" + tGrpAgentCom +
                     "&GrpAgentName=" + tGrpAgentName + "&GrpAgentIDNo=" + tGrpAgentIDNo;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCode').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Sales_Cod='" + fm.all('GrpAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.GrpAgentCode.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }else if(fm.all('GrpAgentIDNo').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Id_No='" + fm.all('GrpAgentIDNo').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("身份证号码为:[" +  fm.GrpAgentIDNo.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}

//问题件录入
function QuestInput()
{
	cContNo = fm.ContNo.value;  //保单号码
	if(cContNo == "")
	{
		alert("尚无该投保单，请先保存!");
	}
	else
	{	
        var tStrUrl = "../uw/QuestInputMain.jsp?AppFlag=1"
            + "&ContNo=" + cContNo
            + "&Flag=" + LoadFlag
            + "&MissionID=" + tMissionID
            + "&SubMissionID=" + tSubMissionID
            ;
		window.open(tStrUrl, "window1");
	}
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery1(arrQueryResult)
{     
      //fm.all("Donextbutton1").style.display="";
      //fm.all("Donextbutton2").style.display="none";
      //fm.all("Donextbutton3").style.display="";
      //fm.all("butBack").style.display="none";
      //详细信息初始化
      detailInit(arrQueryResult); 
    
}
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];//fm.all('AgentCodeName').value = easyExecSql("select name from laagent where agentcode='"+arrSelected[0][1]+"'");
  	fm.AgentGroup.value = arrResult[0][1];//fm.all('AgentGroupName').value = easyExecSql("select name from labranchgroup where  AgentGroup='"+arrResult[0][1]+"'");
  }
}
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery3(arrResult)
{  
  if(arrResult!=null)
    {  	
  	fm.GrpAgentCode.value = arrResult[0][0];
  	fm.GrpAgentName.value = arrResult[0][1];
    fm.GrpAgentIDNo.value = arrResult[0][2];
  }
}

function queryAgent2()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==10 )	 {
	var cAgentCode = fm.AgentCode.value;  //保单号码	
	var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("代码为:["+fm.all('AgentCode').value+"]的业务员不存在，请确认!");
     }
	}	
}                                                                             
function getdetail()
{
var strSql = "select BankCode,AccName from LCAccount where BankAccNo='" + fm.AppntBankAccNo.value+"'";
var arrResult = easyExecSql(strSql);
if (arrResult != null) {
      fm.AppntBankCode.value = arrResult[0][0];
      fm.AppntAccName.value = arrResult[0][1];
    }
	
}	                                                                             
function getdetailwork()
{
var strSql = "select OccupationType from LDOccupation where OccupationCode='" + fm.AppntOccupationCode.value+"'";
var arrResult = easyExecSql(strSql);
if (arrResult != null) {
      fm.AppntOccupationType.value = arrResult[0][0];

    }
else{  
     fm.AppntOccupationType.value = '';
}	
}                                                                             
function getdetailaddress(){
 	var strSQL="select b.* from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.AppntNo.value+"'";
        arrResult=easyExecSql(strSQL);   
   try{fm.all('AppntNo').value= arrResult[0][0];}catch(ex){};         
   try{fm.all('AppntAddressNo').value= arrResult[0][1];}catch(ex){};          
   try{fm.all('AppntPostalAddress').value= arrResult[0][2];}catch(ex){};     
   try{fm.all('AppntZipCode').value= arrResult[0][3];}catch(ex){};            
   try{fm.all('AppntPhone').value= arrResult[0][4];}catch(ex){};              
   try{fm.all('AppntFax').value= arrResult[0][5];}catch(ex){};                
   try{fm.all('AppntHomeAddress').value= arrResult[0][6];}catch(ex){};        
   try{fm.all('AppntHomeZipCode').value= arrResult[0][7];}catch(ex){};        
   try{fm.all('AppntHomePhone').value= arrResult[0][8];}catch(ex){};          
   try{fm.all('AppntHomeFax').value= arrResult[0][9];}catch(ex){};            
   try{fm.all('CompanyAddress').value= arrResult[0][10];}catch(ex){};     
   try{fm.all('AppntGrpZipCode').value= arrResult[0][11];}catch(ex){};     
   try{fm.all('AppntGrpPhone').value= arrResult[0][12];}catch(ex){};       
   try{fm.all('AppntGrpFax').value= arrResult[0][13];}catch(ex){};         
   try{fm.all('AppntMobile').value= arrResult[0][14];}catch(ex){};             
   try{fm.all('AppntMobileChs').value= arrResult[0][15];}catch(ex){};          
   try{fm.all('AppntEMail').value= arrResult[0][16];}catch(ex){};              
   try{fm.all('AppntBP').value= arrResult[0][17];}catch(ex){};                 
   try{fm.all('AppntMobile2').value= arrResult[0][18];}catch(ex){};            
   try{fm.all('AppntMobileChs2').value= arrResult[0][19];}catch(ex){};         
   try{fm.all('AppntEMail2').value= arrResult[0][20];}catch(ex){};             
   try{fm.all('AppntBP2').value= arrResult[0][21];}catch(ex){}; 
   try{fm.all('AppntGrpName').value= arrResult[0][27];}catch(ex){}; 
}
function getaddresscodedata()
{
    var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;
    var strsql = "";
    var tCodeData = "0|";
    //strsql = "select AddressNo,PostalAddress from LCAddress where CustomerNo ='"+fm.AppntNo.value+"'";
    strsql = "select max(int(AddressNo)) from LCAddress where CustomerNo ='"+fm.AppntNo.value+"'";
    //alert("strsql :" + strsql);
    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
    if (turnPage.strQueryResult != "")
    {
    	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	m = turnPage.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		//tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    		tCodeData = turnPage.arrDataCacheSet[i][0];
    	}
    }
    //alert ("tcodedata : " + tCodeData);
    //return tCodeData;
    //fm.all("AddressNo").CodeData=tCodeData;
    fm.all("AddressNo").value=tCodeData;
    afterCodeSelect("GetAddressNo","" );
} 
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="GetAddressNo"){
 	var strSQL="select b.* from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.AppntNo.value+"'";
        arrResult=easyExecSql(strSQL);                         
   try{fm.all('AppntNo').value= arrResult[0][0];}catch(ex){};         
   try{fm.all('AppntAddressNo').value= arrResult[0][1];}catch(ex){};          
   try{fm.all('AppntPostalAddress').value= arrResult[0][2];}catch(ex){};     
   try{fm.all('AppntZipCode').value= arrResult[0][3];}catch(ex){};            
   try{fm.all('AppntPhone').value= arrResult[0][4];}catch(ex){};              
   try{fm.all('AppntFax').value= arrResult[0][5];}catch(ex){};                
   try{fm.all('AppntHomeAddress').value= arrResult[0][6];}catch(ex){};        
   try{fm.all('AppntHomeZipCode').value= arrResult[0][7];}catch(ex){};        
   try{fm.all('AppntHomePhone').value= arrResult[0][8];}catch(ex){};          
   try{fm.all('AppntHomeFax').value= arrResult[0][9];}catch(ex){};            
   try{fm.all('CompanyAddress').value= arrResult[0][10];}catch(ex){};     
   try{fm.all('AppntGrpZipCode').value= arrResult[0][11];}catch(ex){};     
   try{fm.all('AppntGrpPhone').value= arrResult[0][12];}catch(ex){};       
   try{fm.all('AppntGrpFax').value= arrResult[0][13];}catch(ex){};        
   try{fm.all('AppntMobile').value= arrResult[0][14];}catch(ex){};             
   try{fm.all('AppntMobileChs').value= arrResult[0][15];}catch(ex){};          
   try{fm.all('AppntEMail').value= arrResult[0][16];}catch(ex){};              
   try{fm.all('AppntBP').value= arrResult[0][17];}catch(ex){};                 
   try{fm.all('AppntMobile2').value= arrResult[0][18];}catch(ex){};            
   try{fm.all('AppntMobileChs2').value= arrResult[0][19];}catch(ex){};         
   try{fm.all('AppntEMail2').value= arrResult[0][20];}catch(ex){};             
   try{fm.all('AppntBP2').value= arrResult[0][21];}catch(ex){};   
   try{fm.all('AppntGrpName').value= arrResult[0][27];}catch(ex){};          
 }
if( cCodeName == "CheckPostalAddress")
{ 
	 if(fm.CheckPostalAddress.value=="1") 
	 {
	 	fm.all('AppntPostalAddress').value=fm.all('CompanyAddress').value;
                fm.all('AppntZipCode').value=fm.all('AppntGrpZipCode').value;
//                fm.all('AppntPhone').value= fm.all('AppntGrpPhone').value;
                fm.all('AppntFax').value= fm.all('AppntGrpFax').value;

	 }      
	 else if(fm.CheckPostalAddress.value=="2") 
	 {      
	 	fm.all('AppntPostalAddress').value=fm.all('AppntHomeAddress').value;
                fm.all('AppntZipCode').value=fm.all('AppntHomeZipCode').value;
//                fm.all('AppntPhone').value= fm.all('AppntHomePhone').value;
                fm.all('AppntFax').value= fm.all('AppntHomeFax').value;
	 }
	 else if(fm.CheckPostalAddress.value=="3") 
	 {
	 	fm.all('AppntPostalAddress').value="";
                fm.all('AppntZipCode').value="";
//                fm.all('AppntPhone').value= "";
                fm.all('AppntFax').value= "";
	 } 
	 
}	 	
	if(cCodeName=="OccupationCode")
  {
    	//alert();
    	var t = Field.value;
    	var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+t+"'";
    	//alert(strSql);
    	var arr = easyExecSql(strSql);
    	if( arr )
    	{
    		fm.AppntOccupationType.value = arr[0][0];
    	}
   } 
    if(cCodeName=="PayMode")
    {
        //if(fm.PayMode.value=="1" || fm.PayMode.value=="4"||fm.PayMode.value=="6") 
	    //{   
            fm.all('AppntBankCode').style.display="";
            fm.all('AppntBankCodeName').style.display="";
            fm.all('AppntAccName').style.display="";
            //fm.all('AppntAccName').readOnly=false;
            fm.all('AppntBankAccNo').style.display="";
        //}
     
        // 加入对缴费凭证号的控制。
        // 可录入原则：缴费方式为（1：现金；11：银行代扣）
        //initPayMode();
    }
    else if(cCodeName=="LCSaleChnl")
	{
	  if(Field.value == "04" )
	  {
	    fm.all("AgentComTitleID").style.display = "";
	    fm.all("AgentComInputID").style.display = "";
	  }
	  else
	  {
	    fm.all("AgentComTitleID").style.display = "none";
	    fm.all("AgentComInputID").style.display = "none";
	  }
	  
	  if(Field.value == "03" || Field.value == "11" || Field.value == "12"){
	    fm.all("AgentComTitleID1").style.display = "";
	    fm.all("AgentComInputID1").style.display = "";
	    var tBranchType="2";
	    var tBranchType2="";
	    if (Field.value == "11"){
	    	tBranchType2="04";	    	
	    }else if (Field.value == "12"){
	    	tBranchType2="04";
	    }else if (Field.value == "03"){
            tBranchType2="02";
        }
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
	  }else{
	  	//加入个险中介的判断
	  	if(Field.value == "10"){
	  		fm.all("AgentComTitleID1").style.display = "";
		    fm.all("AgentComInputID1").style.display = "";
		    var tBranchType="#1";
		    var tBranchType2="02#,#04";
		    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
		    fm.all("AgentSaleCodeID").style.display = "";
		  	//------------------------------------------------------------
		}else{
		    fm.all("AgentComTitleID1").style.display = "none";
		    fm.all("AgentComInputID1").style.display = "none";
		    fm.all("AgentSaleCodeID").style.display = "none";
		}
	  }
	  
	  
	  
	  fm.AgentCom.value = "";
	  fm.AgentCode.value = "";
	  fm.AgentCom1.value = "";
	  fm.GrpAgentCom.value = "";
	  fm.GrpAgentCode.value = "";
	  fm.GrpAgentName.value = "";
	}
}                                                                
                                                                             
/*********************************************************************
 *  初始化工作流MissionID
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function initMissionID()
{
	if(tMissionID == "null" || tSubMissionID == "null")
	{
	  tMissionID = mSwitch.getVar('MissionID');
	  tSubMissionID = mSwitch.getVar('SubMissionID');
	}
	else
	{
	  mSwitch.deleteVar("MissionID");
	  mSwitch.deleteVar("SubMissionID");
	  mSwitch.addVar("MissionID", "", tMissionID);
	  mSwitch.addVar("SubMissionID", "", tSubMissionID);
	  mSwitch.updateVar("MissionID", "", tMissionID);
	  mSwitch.updateVar("SubMissionID", "", tSubMissionID);
	}
}   
function FillPostalAddress()
{
	 if(fm.CheckPostalAddress.value=="1") 
	 {
	 	fm.all('AppntPostalAddress').value=fm.all('CompanyAddress').value;
                fm.all('AppntZipCode').value=fm.all('AppntGrpZipCode').value;
                fm.all('AppntPhone').value= fm.all('AppntGrpPhone').value;
                fm.all('AppntFax').value= fm.all('AppntGrpFax').value;

	 }      
	 else if(fm.CheckPostalAddress.value=="2") 
	 {      
	 	fm.all('AppntPostalAddress').value=fm.all('AppntHomeAddress').value;
                fm.all('AppntZipCode').value=fm.all('AppntHomeZipCode').value;
                fm.all('AppntPhone').value= fm.all('AppntHomePhone').value;
                fm.all('AppntFax').value= fm.all('AppntHomeFax').value;
	 }
	 else if(fm.CheckPostalAddress.value=="3") 
	 {
	 	fm.all('AppntPostalAddress').value="";
                fm.all('AppntZipCode').value="";
                fm.all('AppntPhone').value= "";
                fm.all('AppntFax').value= "";
	 }
}                                                                                                                                                   
function AppntChk()
{
	var Sex=fm.AppntSex.value;
	var i=Sex.indexOf("-");
	Sex=Sex.substring(0,i);
        var sqlstr="select *from ldperson where Name='"+fm.AppntName.value+"' and Sex='"+fm.AppntSex.value+"' and Birthday='"+fm.AppntBirthday.value+"' and CustomerNo<>'"+fm.AppntNo.value+"'";
        //alert(sqlstr);        
        arrResult = easyExecSql(sqlstr,1,0);
        if(arrResult==null)
        {
	  alert("没有与该投保人相似的客户,无需校验");
	  return false;
        }	
	window.open("../uw/AppntChkMain.jsp?ProposalNo1="+fm.ContNo.value+"&Flag=A","window1");
}                                                                             
 
 
/*********************************************************************
 *  根据身份证号取得出生日期和性别
 *  参数  ：  身份证号
 *  返回值：  无
 *********************************************************************
 */

function getBirthdaySexByIDNo(iIdNo)
{
	if(fm.all('AppntIDType').value=="0")
	{
		fm.all('AppntBirthday').value=getBirthdatByIdNo(iIdNo);
		fm.all('AppntSex').value=getSexByIDNo(iIdNo);
	}	
	if(fm.all('AppntIDType').value=="5")
	{
		fm.all('AppntBirthday').value=getBirthdatByIdNo(iIdNo);
		fm.all('AppntSex').value=getSexByIDNo(iIdNo);
	}	
}  

/*********************************************************************
 *  验证身份证号、生日＆性别
 *  参数  ：  
 *  返回值：  无
 *********************************************************************
 */
function BirthdaySexAppntIDNo()
{
	if(fm.all('AppntIDType').value=="0")
	{
		if(fm.all('AppntBirthday').value!=getBirthdatByIdNo(fm.all('AppntIDNo').value) || fm.all('AppntSex').value!=getSexByIDNo(fm.all('AppntIDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
	if(fm.all('AppntIDType').value=="5")
	{
		if(fm.all('AppntBirthday').value!=getBirthdatByIdNo(fm.all('AppntIDNo').value) || fm.all('AppntSex').value!=getSexByIDNo(fm.all('AppntIDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
}


function checkidtype()
{
	if(fm.AppntIDType.value=="")
	{
		alert("请选择证件的类型");
		fm.AppntIDNo.value="";
        }
}                                                                    
function getCompanyCode()
{
    var strsql = "";
    var tCodeData = "";
    strsql = "select CustomerNo,GrpName from LDgrp ";
    fm.all("AppntGrpNo").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}                                                                            
//复合险种信息
function showComplexRiskInfo()
{
	window.open("./UWHistoryComplexRiskMain.jsp?ContNo="+ContNo+"&prtNo="+fm.PrtNo.value,"", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}




//外包反馈错误信息 2007-9-27 20:21
function findIssue()
{
	window.open("./BPOIssueInput.jsp?prtNo="+prtNo,"window1");
}

function checkContPrem()
{
	// 在点击复核完毕时，进行整单录入保费与系统计算保费进行校验。
    var tContNo = fm.ContNo.value;
    var strSql = "select (select PremScope from LCCont where ContNo = '" + tContNo + "') PremScope, Sum(prem) Prem "
        + " from LCPol where ContNo ='" + tContNo + "' "
        + " with ur ";
    var tResult = easyExecSql(strSql);
    if(!tResult)
    {
        alert('未查询出合同保费相关信息。');
        return false;
    }
    var tPremScope = tResult[0][0];
    var tPrem = tResult[0][1];
    if(tPremScope != tPrem)
    {
        var tResultComment = "整单录入的保费，与系统计算出的保费不符。"
            + "整单录入的保费为：" + tPremScope
            + "；系统计算出的保费为：" + tPrem;
        return confirm(tResultComment);
    }
    return true;
    // ----------------------------------------------
}

//销售渠道和业务员的校验
function checkSaleChnl()
{
    //业务员离职校验
    var agentStateSql = "select 1 from LAAgent where AgentCode = '" 
        + fm.AgentCode.value + "' and AgentState < '06'";
    var arr = easyExecSql(agentStateSql);
    if(!arr){
        alert("该业务员已离职！");
        return;
    }
    //业务员和销售渠道的校验
    var agentCodeSql = "select 1 from LAAgent a, LDCode1 b where a.AgentCode = '" 
        + fm.AgentCode.value
        + "' and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
        + "and b.CodeType = 'salechnl' and b.Code = '" 
        + fm.SaleChnl.value + "'"
        + "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '10' = '" + fm.SaleChnl.value + "' and a.BranchType2 = '04'  ";


    var arr = easyExecSql(agentCodeSql);
    if(!arr){
        alert("业务员和销售渠道不相符！");
        return;
    }
    return true;
}

/**
 * 加入根据PayMode对缴费凭证号的控制。
 * 可录入原则：缴费方式为（1：现金；11：银行代扣；12：银行代扣-导入）
 */
function initPayMode()
{
    var tPayMode = fm.all.PayMode.value;
    switch(tPayMode)
    {
        case "1":
            displayTempFeeNoInput(true);
            break;
        case "3":
            displayTempFeeNoInput(false);
            displayAccInfoInput(false);
            break;
        case "4":
            displayTempFeeNoInput(false);
            break;
        case "6":
            displayTempFeeNoInput(false);
            break;    
        case "11":
            displayTempFeeNoInput(true);
            displayAccInfoInput(true);
            break;
        case "12":
            displayTempFeeNoInput(true);
            displayAccInfoInput(true);
            break;
    }
    // ------------------------------------------
}

/**
 * 展现银行帐户信息。
 */
function displayAccInfoInput(isDisplay)
{
    displayInput("AppntBankCode", isDisplay, true);
    displayInput("AppntBankCodeName", isDisplay, true);
    displayInput("AppntAccName", isDisplay, true);
    displayInput("AppntBankAccNo", isDisplay, true);
}

/**
 * 展现暂缴收据号信息。
 */
function displayTempFeeNoInput(isDisplay)
{
    displayInput("TempFeeNo", isDisplay, true);
}

/**
 * 校验所填入缴费凭证号的，到帐情况
 */
function checkEnterAccOfTempFee()
{
    var tContNo = fm.ContNo.value;
    var tResult = easyExecSql("select PayMode, TempFeeNo from LCCont lcc where lcc.ContNo = '" + tContNo + "'");
    if(!tResult)
    {
        alert('未查询出该保单对应缴费相关信息。');
        return false;
    }
    
    var tPayMode = tResult[0][0];
    var tTempFeeNo = tResult[0][1];
    
    // 只对缴费方式为：1-现金；11-银行代收，进行校验。
    if(tPayMode != "1" && tPayMode != "11" && tPayMode != "12")
        return true;

    // 暂缴号为空或不填，不进行校验。
    if(tTempFeeNo == null || tTempFeeNo == "")
        return true;
        
    var tStrSql = "select nvl(sum(PayMoney), 0) "
	   + " from LJTempFeeClass ljtfc "
	   + " where ljtfc.EnterAccDate is not null and ljtfc.ConfMakeDate is not null "
       + " and ConfDate is null "
	   + " and ljtfc.TempFeeNo = '" + tTempFeeNo + "' ";
       
    tResult = easyExecSql(tStrSql);
    if(!tResult)
    {
        alert('未查询出该暂缴号对应保费相关信息。');
        return false;
    }
    var tEnterAccMoney = tResult[0][0];
    
    tResult = easyExecSql("select nvl(sum(Prem), 0) from LCPol lcp where lcp.ContNo = '" + tContNo + "' ");
    if(!tResult)
    {
        alert('未查询出该保单对应保费相关信息。');
        return false;
    }
    var tContPrem = tResult[0][0];
    
    var tComment = "该单为先收费保单:\n"
        + "缴费方式为：\t" + tPayMode + "\n"
        + "暂缴收据号为：\t" + tTempFeeNo + "\n"
        + "目前到帐保费为：" + tEnterAccMoney + "\n"
        + "该单系统保费为：" + tContPrem;
    return confirm(tComment);
}

function controlAgentComDisplay(displayFlag)
{
  if(fm.SaleChnl.value == "03" || fm.SaleChnl.value == "04")
  {
    fm.all("AgentComTitleID").style.display = displayFlag;
	  fm.all("AgentComInputID").style.display = displayFlag;
  }else if (fm.SaleChnl.value == "11" || fm.SaleChnl.value == "12") {
  	fm.all("AgentComTitleID1").style.display = displayFlag;
  	fm.all("AgentComInputID1").style.display = displayFlag;
  }else if (fm.SaleChnl.value == "10"){
  	fm.all("AgentComTitleID1").style.display = displayFlag;
  	fm.all("AgentComInputID1").style.display = displayFlag;
  }
  initAgentCom();
}

//销售渠道为中介时中介机构的校验
function checkAgentCom()
{
	var saleChannel = fm.SaleChnl.value;
	var agentCom = fm.AgentCom.value;
	
	if(saleChannel != null && (saleChannel == "03" || saleChannel == "10"))
	{
	    if(agentCom == null || agentCom == "")
	    {
	        alert("中介机构为空，请填写！");
	        return false;
	    }
	    else
	    {
            var result = easyExecSql("select 1 from LAComToAgent where AgentCom = '" + agentCom + "' and AgentCode = '" + fm.AgentCode.value + "'");
            if(!result)
            {
                return confirm("中介机构和业务员不匹配！");
            }
	    }
        var result = easyExecSql("select 1 from LCCont where PrtNo = '" + fm.PrtNo.value 
            + "' and (SaleChnl = '03' or SaleChnl = '10') and (AgentCom is null or AgentCom = '') with ur");
        if(result)
        {
            alert("请先保存保单信息！");
            return false;
        }
	}
	return true;
}

//校验银行信息
function checkBankInfo()
{
    //查询和随动定制不需要校验银行信息
    if(LoadFlag == "6" || LoadFlag == "99")
    {
        return true;
    }
    if(fm.all('PayMode').value=="4"||fm.all('PayMode').value=="6"||fm.all('ExPayMode').value=="4")
    {
        if(fm.all('AppntBankCode').value==null||fm.all('AppntBankCode').value=="")
        {
            alert("请录入开户银行！");
            fm.all('AppntBankCode').focus;
            return false;
        }
        if(fm.all('AppntAccName').value==null||fm.all('AppntAccName').value=="")
        {
            alert("请录入户名！");
            fm.all('AppntAccName').focus;
            return false;
        }
        if(fm.all('AppntBankAccNo').value==null||fm.all('AppntBankAccNo').value=="")
        {
            alert("请录入帐号！");
            fm.all('AppntBankAccNo').focus;
            return false;
        }
        var result = easyExecSql("select 1 from LCCont where PrtNo = '" + fm.PrtNo.value 
            + "' and PayMode in( '4' ,'6')and (BankCode is null or AccName is null or BankAccNo is null)");
        if(result)
        {
            alert("请先保存缴费信息！");
            return false;
        }
    }
    return true;
}

/**
 * 对万能类险种进行特殊处理。
 */
function initULIRiskInput()
{
    if(hasULIRisk(ContNo))
    {
        fm.all("CValiDateInput").style.display = "none";
    }
}

/**
 * 校验万能类险种缴费年期。
 */
function checkULIRisk()
{
    if(!hasULIRisk(ContNo))
    {
        return true;
    }
    
    var tStrSql = ""
        + " select lcp.InsuredAppAge,lcp.PayEndYearFlag,lcp.PayEndYear, lcp.InSuYearFlag, lcp.InSuYear from LCPol lcp "
        + " where lcp.ContNo = '" + ContNo + "' "
        + " and lcp.PayIntv != 0 and lcp.PolNo = lcp.MainPolNo "
        ;
    var tResult = easyExecSql(tStrSql);
    var tFlag = true;
    if(tResult)
    {
        for(i = 0, n = tResult.length; i < n; i++)
        {
        	var tPayYears;// 缴费期间
        	var tInsuYears;// 保障期间
        	
            var tInsuredAppAge = tResult[i][0];
            
            // 处理缴费期间
            var tPayEndYearFlag = tResult[i][1];
            var tPayEndYear = tResult[i][2];
            
            if(tPayEndYearFlag == 'A')
            {
            	tPayYears = tPayEndYear - tInsuredAppAge;
            }
            else if(tPayEndYearFlag == 'Y')
            {
            	tPayYears = tPayEndYear;
            }
            else
            {
            	continue;
            }
            // --------------------
            //处理保障期间
            var tInSuYearFlag = tResult[i][3];
            var tInSuYear = tResult[i][4];
            
             if(tInSuYearFlag == 'A')
            {
            	tInsuYears = tInSuYear - tInsuredAppAge;
            }
            else if(tInSuYearFlag == 'Y')
            {
            	tInsuYears = tInSuYear;
            }
            else
            {
            	continue;
            }
            
            
            // 缴费年期要小于被保年期
            if(tPayYears >tInsuYears)
            {
                alert("万能类险种，缴费年期要小于被保年期。");
                tFlag = false;
                break;
            }
        }
    }
    return tFlag;
}

/**
 * 校验万能类险种，只能存在一个被保险人。
 */
function checkULIInsured()
{
    if(!hasULIRisk(ContNo))
    {
        return true;
    }
    
    var tStrSql = ""
        + " select Count(1) "
        + " from LCInsured lci "
        + " where lci.ContNo = '" + ContNo + "' "
        ;
    var tResult = easyExecSql(tStrSql);
    var tFlag = true;
    if(tResult)
    {
        if(tResult[0][0] > 1)
        {
            alert("万能类险种，不能存在多被保人。");
            tFlag = false;
        }
    }
    return tFlag;
}

/**
 * 是否为万能类险种。
 */
function hasULIRisk(cContNo)
{
    var tStrSql = ""
        + " select 1 from L" +　contState + "Pol lcp "
        + " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
        + " where lmra.RiskType4 = '4' " 
        + " and lcp.ContNo = '" + cContNo + "' "
        ;
    var tResult = easyExecSql(tStrSql);
    if(tResult)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * 问题件回销
 */
function questBack()
{
    var tContNo = ContNo;
    var tStrSql = "select 1 from lcissuepol where proposalcontno = '" + tContNo + "' ";
    var tResult = easyExecSql(tStrSql);
    if(!tResult)
    {
        alert("未录入问题件!");
        return false;
    }

    var tLoadFlag = LoadFlag;
    var tFlag = "1";

    if(tContNo != "")
    {
        var tUrl = "../uw/UWManuQuestQMain.jsp"
            + "?ContNo=" + tContNo
            + "&Flag=" + tFlag
            + "&LoadFlag=" + tLoadFlag
            ;
        window.open(tUrl, "window2");
    }
    else
    {
        alert("请先选择保单!");
    }
}

//险种的校验
function checkRiskInfo()
{
    //该分公司险种是否停售校验
    var riskEnd = "select 1 from LCPol a, LMRiskApp b, LDCode c where a.PrtNo = '" 
        + fm.PrtNo.value + "' and b.RiskType4 = '4' and c.CodeType = 'UniversalSale' "
        + "and a.RiskCode = b.RiskCode and c.Code = substr(a.ManageCom, 1, 4) ";
    var result = easyExecSql(riskEnd);
    if(result)
    {
        alert("该分公司万能险已停售！");
        return false;
    }
    
    //保险期间单位为空的校验
    var riskInsu = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and (InsuYearFlag is null or InsuYearFlag = '')";
    result = easyExecSql(riskInsu);
    if(result)
    {
        alert("保险期间单位为空，请修改！");
        return false;
    }
    
    //个人防癌套餐的校验
    var risk230501And331001 = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and RiskCode in ('230501', '331001')";
    result = easyExecSql(risk230501And331001);
    if(result)
    {
        var riskInfoSql = "select 1 from LCPol where PrtNo = '" + fm.PrtNo.value + "' "
            + "and RiskCode in ('230501', '331001') "
            + "and not (InsuYear in (10, 15, 20) and InsuYearFlag = 'Y' "
            + "and PayEndYear = InsuYear and PayEndYearFlag = InsuYearFlag "
            + "and PayIntv = 12)";
        result = easyExecSql(riskInfoSql);
        if(result)
        {
            alert("请检查缴费频次、保险期间和缴费期间！\n(1).保险期间必须为10年、15年、20年。"
                + "\n(2).缴费年期必须与保险期间一致。\n(3).缴费频次必须为年缴。");
            return false;
        }
    }
    return true;
}

/**
 * 校验销售渠道与产品关联
 */
function checkSaleChnlRisk()
{   
    var tPrtNo = fm.PrtNo.value;
    var saleChannel = null;
    
    var arr = easyExecSql("select salechnl from lccont where prtno = '" + tPrtNo + "' ");
    if(arr)
    {
        saleChannel = arr[0][0];
    }
    
    if(saleChannel != null && saleChannel == "13")
    {
        var tStrSql = " select 1 "
            + " from LCPol lcp "
            + " left join LDCode1 ldc1 on lcp.SaleChnl = ldc1.Code and lcp.RiskCode = ldc1.code1 and ldc1.codetype = 'checksalechnlrisk' "
            + " where 1 = 1 "
            + " and ldc1.CodeType is null "
            + " and lcp.PrtNo = '" + tPrtNo + "' "
            ;
        var result = easyExecSql(tStrSql);
        
        if(result)
        {
            alert("该单为银代直销保单，但存在非银代直销产品，请核实。");
            return false;
        }
    }
    
    return true;
}

/**
 * 集团交叉要素校验
 */
function checkCrsBussParams()
{
    var tCrs_SaleChnl = trim(fm.Crs_SaleChnl.value);
    var tCrs_BussType = trim(fm.Crs_BussType.value);
   
    var tGrpAgentCom = trim(fm.GrpAgentCom.value);
    var tGrpAgentCode = trim(fm.GrpAgentCode.value);
    var tGrpAgentName = trim(fm.GrpAgentName.value);
    var tGrpAgentIDNo = trim(fm.GrpAgentIDNo.value);
    
    if(tCrs_SaleChnl != null && tCrs_SaleChnl != "")
    {
        if(tCrs_BussType == null || tCrs_BussType == "")
        {
            alert("选择集团交叉渠道时，集团交叉业务类型不能为空。");
            return false;
        }
        if(tGrpAgentCom == null || tGrpAgentCom == "")
        {
            alert("选择集团交叉渠道时，对方业务员机构不能为空。");
            return false;
        }
        if(tGrpAgentCode == null || tGrpAgentCode == "")
        {
            alert("选择集团交叉渠道时，对方业务员代码不能为空。");
            return false;
        }
        if(tGrpAgentName == null || tGrpAgentName == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名不能为空。");
            return false;
        }
        if(tGrpAgentIDNo == null || tGrpAgentIDNo == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名身份证不能为空。");
            return false;
        }
    }
    else
    {
        if(tCrs_BussType != null && tCrs_BussType != "")
        {
            alert("未选择集团交叉渠道时，集团交叉业务类型不能填写。");
            return false;
        }
        if(tGrpAgentCom != null && tGrpAgentCom != "")
        {
            alert("未选择集团交叉渠道时，对方业务员机构不能为填写。");
            return false;
        }
        if(tGrpAgentCode != null && tGrpAgentCode != "")
        {
            alert("未选择集团交叉渠道时，对方业务员代码不能为填写。");
            return false;
        }
        if(tGrpAgentName != null && tGrpAgentName != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名不能为填写。");
            return false;
        }
        if(tGrpAgentIDNo != null && tGrpAgentIDNo != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名身份证不能为填写。");
            return false;
        }
    }
    
    return true;
}

//显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value = "";
        fm.all('Crs_SaleChnlName').value = "";
        fm.all('Crs_BussType').value = "";
        fm.all('Crs_BussTypeName').value = "";
        fm.all('GrpAgentCom').value = "";
        fm.all('GrpAgentComName').value = "";
        fm.all('GrpAgentCode').value = "";
        fm.all('GrpAgentName').value = "";
        fm.all('GrpAgentIDNo').value = "";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}

//根据对方机构代码带出对方机构名称
function GetGrpAgentName()
{
	var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     fm.GrpAgentComName.value = "";
	}
}  
//根据机构代码显示机构名称
function getAgentName()
{
    var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     alert("对方机构代码有错误,请修改");
	     fm.GrpAgentCom.value = "";
	     return false;
	}
}

//校验被保人职业类别与职业代码
function checkOccTypeOccCode()
{
	var strSql = "select distinct OccupationType,OccupationCode from lcinsured "
				+ " where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSql);
	var i = 0;
	
	if(arrResult != null) {
		for(i = 0; i < arrResult.length; i++)
		{
			var OccupationType = arrResult[i][0];
			var OccupationCode = arrResult[i][1];
			
			if(OccupationType != null && OccupationType != "")
			{
				if(!CheckOccupationType(OccupationType))
				{
					return false;
				}
			}
			if(OccupationCode != null && OccupationCode != "")
			{
				if(!CheckOccupationCode(OccupationType,OccupationCode))
				{
					return false;
				}
			}
		}
	}
	return true;
}

//校验职业代码
function CheckOccupationCode(Type,Code)
{
	
	var strSql = "select 1 from ldoccupation where OccupationCode = '" + Code +"' and OccupationType = '" + Type + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别和职业代码与系统描述不一致，请查看！");
		return false;
	} 
	return true;
}

//校验职业类别
function CheckOccupationType(Type)
{
	var strSql = "select 1 from dual where '" + Type + "' in (select distinct occupationtype from ldoccupation)";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6");
		return false;
	} 
	return true;
}

//执行查询交叉销售对方机构代码 date 20101117 by gzh
function queryGrpAgentCom()
{
	if(fm.all('Crs_SaleChnl').value == "" || fm.all('Crs_SaleChnl').value == null)
	{
		alert("请先选择交叉销售渠道！！");
		return false;
	}
    if(fm.all('GrpAgentCom').value == "")
    {  
        var tCrs_SaleChnl =  fm.Crs_SaleChnl.value;
        var tCrs_SaleChnlName = fm.Crs_SaleChnlName.value;
        var strURL = "../sys/GrpAgentComQueryMain.jsp?Crs_SaleChnl="+tCrs_SaleChnl+"&Crs_SaleChnlName="+tCrs_SaleChnlName;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentComQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCom').value != "")
    {	
        var strGrpSql = "select grpagentcom,Under_Orgname  from lomixcom  where  grpagentcom ='"+fm.GrpAgentCom.value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery4(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("机构代码为:[" +  fm.GrpAgentCom.value + "]的机构不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery4(arrResult)
{  
  if(arrResult!=null)
    { 	
  	fm.GrpAgentCom.value = arrResult[0][0];
  	fm.GrpAgentComName.value = arrResult[0][1];
  }
}


//选择交叉销售后对交叉销售各不为空项的校验
    // by gzh 20101118
function MixComCheck()
{    
    if(fm.MixComFlag.checked == true)
    {
    	if(fm.Crs_SaleChnl.value == "" || fm.Crs_SaleChnl.value == null)
    	{
    		alert("选择交叉销售时，交叉销售渠道不能为空，请核查！");
    		fm.all('Crs_SaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.Crs_BussType.value == "" || fm.Crs_BussType.value == null)
    	{
    		alert("选择交叉销售时，交叉销售业务类型不能为空，请核查！");
    		fm.all('Crs_BussType').focus();
    		return false;
    	}
    	if(fm.GrpAgentCom.value == "" || fm.GrpAgentCom.value == null)
    	{
    		alert("选择交叉销售时，对方机构代码不能为空，请核查！");
    		fm.all('GrpAgentCom').focus();
    		return false;
    	}
    	/**
    	if(fm.GrpAgentComName.value == "" || fm.GrpAgentComName.value == null)
    	{
    		alert("选择交叉销售时，对方机构名称不能为空，请核查！");
    		fm.all('GrpAgentComName').focus();
    		return false;
    	}
    	*/
    	if(fm.GrpAgentCode.value == "" || fm.GrpAgentCode.value == null)
    	{
    		alert("选择交叉销售时，对方业务员代码不能为空，请核查！");
    		fm.all('GrpAgentCode').focus();
    		return false;
    	}
    	if(fm.GrpAgentName.value == "" || fm.GrpAgentName.value == null)
    	{
    		alert("选择交叉销售时，对方业务员姓名不能为空，请核查！");
    		fm.all('GrpAgentName').focus();
    		return false;
    	}
    	if(fm.GrpAgentIDNo.value == "" || fm.GrpAgentIDNo.value == null)
    	{
    		alert("选择交叉销售时，对方业务员身份证号码不能为空，请核查！");
    		fm.all('GrpAgentIDNo').focus();
    		return false;
    	}
      	return true;
    }else{
    	return true;
    }
}    
    // --------------------

//校验标准个险集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   by zhangyang 2011-03-21
function checkCrsBussSaleChnl()
{
	if(fm.Crs_SaleChnl.value != null && fm.Crs_SaleChnl.value != "")
	{
		var strSQL = "select 1 from lccont lcc where lcc.ContType = '1' "  
			+ " and lcc.Crs_SaleChnl is not null " 
			+ " and lcc.Crs_BussType = '01' " 
			+ " and not (lcc.SaleChnl = '10' and exists (select 1 from LACom lac where lac.AgentCom = lcc.AgentCom and lac.BranchType = '1' and lac.AcType in ('06', '07'))) " 
			+ " and lcc.PrtNo = '" + fm.PrtNo.value + "' ";
		var arrResult = easyExecSql(strSQL);
        if (arrResult != null)
        {
        	alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为个险中介并且机构必须为对应的中介机构！");
        	return false;
        }
	}
	return true;
}
//------------------------------

//初始化中介机构的显示
function initAgentCom()
{
	if(fm.SaleChnl.value != null && fm.SaleChnl.value != "")
	{
		if(fm.SaleChnl.value == "10")
		{
			var tBranchType="#1";
			var tBranchType2="02#,#04";
			fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";	
		}else if(fm.SaleChnl.value == "03")
		{
			var tBranchType="2";
			var tBranchType2="02";
			fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;	
		}
	}
}

//by gzh 20110402 录入完毕及复核完毕时，校验被保人告知第19项是否正确填写（即存在小于18周岁的未成年人，但还没选择被保人告知第19项）
function checkGZ19(){
	var strSql = "select insuredno,insuredappage from lcpol where prtno = '"+prtNo+"' and insuredappage<18";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		for(var i=0;i<arr.length;i++){
			var CustomerimpartSql = "select * from lccustomerimpart where prtno = '"+prtNo+"' and customerno = '"+arr[i][0]+"' and impartver = '001' and impartcode='250'";
			var arrCustomer = easyExecSql(CustomerimpartSql);
			if(arrCustomer==null){
				alert("被保人（"+arr[i][0]+"）年龄为"+arr[i][1]+"岁，请填写被保人告知第19项！");
				return false;
			}
		}
	}
	return true;
}

//by gzh 20110614 录入完毕及复核完毕时，校验被保人告知第19项是否正确填写（选择“是”时，保额必须填写）
function checkGZ19Prem(){
	var CustomerimpartSql = "select impartparammodle from lccustomerimpart where prtno = '"+prtNo+"' and impartver = '001' and impartcode='250'";
	var arrCustomer = easyExecSql(CustomerimpartSql);
	if(arrCustomer !=null){
		for(var i=0;i<arrCustomer.length;i++){
			var impartparammodle = arrCustomer[i][0].split(",");
			if(impartparammodle[0] == "Y" && (!isNumeric(impartparammodle[1]))){
				alert("被保人告知第19项,当选择“是”时，保额不能为空且必须是数字！");
				return false;
			}
		}
	}
	return true;
}

//校验保单的保险期间是否为负数     by zhangyang  2011-06-09
function checkInsuYear()
{
	var strSQL = "select Years from lcpol where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		for(var i = 0; i < arrResult.length; i++)
		{
			var tYears = arrResult[i][0];
			if(tYears != null && tYears!= '' && tYears < 0)
			{
				alert("保单的保险期间有误，请确认保单的保险期间录入值是否正确！");
				return false;
			}
		}
	}
	return true;
}

function Crs_BussTypeHelp(){
	window.open("../sys/JtjxYwlyInfo.jsp");
}

//校验黑名单
function checkBlackName(){
	var strSQL = "select appntname from lccont where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		for(var i = 0; i < arrResult.length; i++)
		{
			var tAppntName = arrResult[i][0];
			var strSQL1 = "select 1 from LCBlackList where type = '0' and name = '"+tAppntName+"'";
			var arrResult1 = easyExecSql(strSQL1);
			if(arrResult1 != null)
			{
				if (!confirm("该保单投保人姓名："+arrResult[i][0]+",存在于黑名单中，确认要复核通过吗？"))
				{
					return false;
				}
			}
		}
	}
	var strSQL2 = "select name from lcinsured where prtno = '" + fm.PrtNo.value + "'";
	var arrResult2 = easyExecSql(strSQL2);
	var tInsuNames = "";
	if(arrResult2 != null){
		for(var i = 0; i < arrResult2.length; i++)
		{
			var tName = arrResult2[i][0];
			var strSQL3 = "select 1 from LCBlackList where type = '0' and name = '"+tName+"'";
			var arrResult3 = easyExecSql(strSQL3);
			if(arrResult3 != null)
			{
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ ","+tName;
				}else{
					tInsuNames = tInsuNames + tName;
				}
			}
		}	
	}
	if(tInsuNames != ""){
		if (!confirm("该保单被保人姓名："+tInsuNames+",存在于黑名单中，确认要复核通过吗？"))
		{
			return false;
		}
	}
	return true;
}

//by zcx 20120524 增加对代理销售业务员的查询
function queryAgentSaleCode()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        fm.all('ManageCom').focus();
        return ;
    }
    
    if(fm.all('AgentCom1').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom1').focus();
        return ;
    }
    var tAgentCom = fm.all('AgentCom').value;
    var tAgentCom1 = fm.all('AgentCom1').value;
    if(fm.all('AgentSaleCode').value == "")
    {
        var strURL = "../sys/AgentSaleCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&AgentCom=" + tAgentCom
        var newWindow = window.open(strURL, "AgentSaleCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('AgentSaleCode').value != "")
    {
        var cAgentCode = fm.AgentSaleCode.value;  //保单号码
        var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom1').value + "'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentSaleName.value = arrResult[0][1];
            //alert("查询结果:  代理销售业务员代码:["+arrResult[0][0]+"] ，代理销售业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value = "";
            alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
        }
    }
}

function queryAgentSaleCode2() {

  if(fm.all('AgentSaleCode').value != ""){
  	if(fm.all('AgentCom').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom1').focus();
        return ;
    }
    var cAgentCode = fm.AgentSaleCode.value;  //保单号码
    var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom1').value + "'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentSaleName.value = arrResult[0][1];
      //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    } else {
      fm.AgentGroup.value="";
      alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
    }
  }
}

function afterQuery5(arrResult){
  if(arrResult!=null) {
    fm.AgentSaleCode.value = arrResult[0][0];
    fm.AgentSaleName.value = arrResult[0][1];
  }
}

//北分中介时，中介机构及代理销售业务员必须填写
function checkAgentComAndSaleCdoe(){
	var strSql = "select AgentCom,AgentSaleCode from lccont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%' and salechnl = '10'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCom = arrResult[0][0];
	    var tAgentSaleCode = arrResult[0][1];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCom == null || tAgentCom ==''){
	   		alert("中介公司代码必须录入，请核查！");
	   		return false;    	
	   	}
	   	if(tAgentSaleCode == null || tAgentSaleCode ==''){
	   		alert("代理销售业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	
	   	var tSqlCode = "select ValidStart,ValidEnd from LAQualification where agentcode = '"+tAgentSaleCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tValidStart = arrCodeResult[0][0];
	   		var tValidEnd = arrCodeResult[0][1];
	   		if(tValidStart == null || tValidStart == ''){
	   			alert("代理销售业务员资格证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tValidEnd == null || tValidEnd == ''){
	   			alert("代理销售业务员资格证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
	   			alert("代理销售业务员资格证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("代理销售业务员资格证信息不完整，请核查！");
	   		return false;
	   	}
	   	
	   	var tSqlCom = "select LicenseStartDate,LicenseEndDate,EndFlag from lacom where agentcom = '"+tAgentCom+"'";
	   	var arrComResult = easyExecSql(tSqlCom);
	   	if(arrComResult){
	   		var tLicenseStartDate = arrComResult[0][0];
	   		var tLicenseEndDate = arrComResult[0][1];
	   		var tEndFlag = arrComResult[0][2];
	   		if(tLicenseStartDate == null || tLicenseStartDate == ''){
	   			alert("中介公司许可证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tLicenseEndDate == null || tLicenseEndDate == ''){
	   			alert("中介公司许可证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == null || tEndFlag == ''){
	   			alert("中介机构合作终止状态为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == 'Y'){
	   			alert("中介机构合作终止状态为失效，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tLicenseEndDate)=='1'){
	   			alert("中介公司许可证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("中介公司许可证信息不完整，请核查！");
	   		return false;
	   	}
    }
    return true;
}
function initAgentSaleCode(){
	var tSqlCode = "select AgentSaleCode from LCCont where prtno='" + fm.PrtNo.value + "'";
	var arrCodeResult = easyExecSql(tSqlCode);
	if(arrCodeResult){
		if(arrCodeResult[0][0] != null && arrCodeResult[0][0] != ""){
			var tSQL = "select agentcode,name from laagenttemp where agentcode = '"+arrCodeResult[0][0]+"'";
			var arrResult = easyExecSql(tSQL);
			if(arrResult){
				fm.AgentSaleCode.value = arrResult[0][0];
    			fm.AgentSaleName.value = arrResult[0][1];
			}
		}
	}
}
//北分业务员必须填写
function checkAgentCode(){
	var strSql = "select AgentCode from lccont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCode = arrResult[0][0];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCode == null || tAgentCode ==''){
	   		alert("业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	var tSqlCode = "select branchtype,branchtype2 from laagent where agentcode = '"+tAgentCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tSqlValid = "";
	   		if(arrCodeResult[0][0] == "1" || arrCodeResult[0][0] == "2" || arrCodeResult[0][0] == "4"){
	   			tSqlValid = "select ValidStart,ValidEnd from LAQUALIFICATION where agentcode = '"+tAgentCode+"' and state='0'";
	   		} else if (arrCodeResult[0][0] == "3"){
	   			tSqlValid = "select Quafstartdate,QuafEndDate from laagent where agentcode = '"+tAgentCode+"'";
	   		} else {
	   			alert("业务员资销售渠道异常，请核查！");
	   			return false;
	   		}
	   		if((arrCodeResult[0][0] == "1")&&(arrCodeResult[0][1]='06')){
				return true;
			}else{
			var arrValid = easyExecSql(tSqlValid);
			if (arrValid) {
				var tValidStart = arrValid[0][0];
				var tValidEnd = arrValid[0][1];
				if (tValidStart == null || tValidStart == '') {
					alert("业务员资格证有效起期为空，请核查！");
					return false;
				}
				if (tValidEnd == null || tValidEnd == '') {
					alert("业务员资格证有效止期为空，请核查！");
					return false;
				}
				if (compareDate(tCurrentDate, tValidEnd) == '1') {
					alert("业务员资格证已失效，请核查！");
					return false;
				}
			} else {
				alert("业务员不存在有效资格证信息，请核查！");
				return false;
			}
			}
	   	}else{
	   		alert("业务员不存在，请核查！");
	   		return false;
	   	}
    }
    return true;
}

function checkAppnt(wFlag){
	var tSql = "select cc.prtno,cad.Customerno,cc.Managecom,cad.Mobile,cad.Phone,cad.homephone,ca.idno from lccont cc inner join lcappnt ca on ca.appntno=cc.appntno and ca.contno=cc.contno " +
				"inner join lcaddress cad on cad.customerno=ca.appntno and cad.addressno=ca.addressno where cc.prtno='" + fm.PrtNo.value + "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		// 印刷号
		var prtno = appntResult[0][0];
		// 投保人客户号
		var customerno = appntResult[0][1];
		// 管理机构
		var managecom = appntResult[0][2];
		// 移动电话
		var mobile = appntResult[0][3];
		// 联系电话
		var phone = appntResult[0][4];
		// 家庭电话
		var homephone = appntResult[0][5];
		// 证件号
		var id = appntResult[0][6];
		
		// 共有校验
		if(!isNull(mobile)) {
			if(!isInteger(mobile) || mobile.length != 11){
				alert("投保人移动电话需为11位数字，请核查！");
		   		return false;
			}
		}
		
		// 机构特殊校验  --需要在数据库中配置 codetype = 字段名 + check

		// 暂时统一对投保人进行校验，若需要单独校验，可以调整为按字段进行校验
		// 若配置为空则会对全部机构进行校验
		if(manageCheck(managecom.substring(0, 4),"appnt")){
			
			if(isNull(id)){
				alert("投保人证件号码不能为空，请核查！");
				return false;
			}
			
			if(isNull(mobile) && isNull(phone) && isNull(homephone)){
				alert("投保人联系电话、住宅电话、移动电话不能同时为空，请核查！");
		   		return false;
			}
		}
		
		// 由于个分公司都提出了电话的校验 因此新建phonecheck
		if(manageCheck(managecom.substring(0, 4),"phone")){
						
			if(!isNull(phone)){
				if(phone.length < 7){
					alert("投保人联系电话不能少于7位，请核查！");
			   		return false;
				}
			}
			
			// 复核时，对于3个以上重复号码，强制下发问题件
			if(wFlag == 2 && (!isNull(mobile) || !isNull(phone))){
				// 由于重复号码校验速度很慢，因此先对问题件进行判断
				
				// 空代表没有下发问题件 1代表已下发问题件未回销 2代表已回销
				// order by doc 为避免多次下发问题件联系电话问题件 只要有没有回销的 不让通过
				var issueSql = "Select (Case When (Select Docid From Es_Doc_Main Where Doccode = Ci.Prtseq And Subtype = 'TB22') Is Null Then 1 Else 2 End) doc "
						+ "From Lcissuepol Ci Where Errfieldname = '联系电话' And Questionobj Like '投保人%' And Contno = (Select Contno From Lccont Where Prtno = '" + prtno + "') " 
						+ "order by doc";
				var issueResult = easyExecSql(issueSql);
				
				// 页面提示信息
				var alterInf = "";
				if(issueResult == null){
					alterInf = "请下发投保人联系电话问题件";
				} else if(issueResult[0][0] == "1"){
					alterInf = "已下发的联系电话问题件尚未回销，请确认";
				}
				
				if(issueResult == null || issueResult[0][0] == "1"){
					if(!isNull(phone)){
						var phoneSql = "select count(distinct customerno) from lcaddress where phone='" + phone + "' and Customerno<>'" + customerno + "' ";
						var result = easyExecSql(phoneSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人联系电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
				
					if(!isNull(mobile)){
						var mobilSql = "select count(distinct customerno) from lcaddress where mobile='" + mobile + "' and Customerno<>'" + customerno + "' ";
						var result = easyExecSql(mobilSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人移动电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
				}
			}
			
			if(manageCheck(managecom.substring(0, 4),"agent")){
				// 业务员手机号码校验
				if(!isNull(phone)){
					var mobilSql = "select agentcode from laagent where phone='" + phone + "' with ur";
					var result = easyExecSql(mobilSql);
					if(result){
						alert("该投保人联系电话与本方业务员" + result[0][0] + "联系号码相同，请核查！");
						return false;
					}
				}
				
				if(!isNull(mobile)){
					var phoneSql = "select agentcode from laagent where mobile='" + mobile + "' with ur";
					var result = easyExecSql(phoneSql);
					if(result){
						alert("该投保人移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
						return false;
					}
				}
			}
		}
	}
	return true;
}

// 判断机构是否需要进行校验
// managecom 机构
// checkflag 校验项
function manageCheck(managecom, checkflag){
	var checkSql = "select (case (select count(1) from ldcode where codetype = '" + checkflag + "check') " +  
			"when 0 then 1 else (select distinct 1 from ldcode where codetype = '" + checkflag + "check' and code = '" + managecom + "') end ) " + 
			"from dual";
	var result = easyExecSql(checkSql);
	if(result){
		if(result[0][0] == "1"){
			// 该机构需要进行校验
			return true;
		}
	}
	// 该机构不需要进行校验
	return false;
}

// 判断是否为空
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}
//校验投保人银行账户信息
function checkBank()
{
    var strSql= easyExecSql("select PayMode,ExPayMode,BankCode, Accname,BankAccNo from LCCont where PrtNo = '" + fm.PrtNo.value 
            + "' and (BankCode is null or  Accname is null or BankAccNo is null )");
   if(strSql) {          
    if(strSql[0][0]== "4"||strSql[0][1]=="4")
    {
     	if(strSql[0][2] == null ||strSql[0][2] == '')
        {   
         	alert("银行代码为空，若修改以后请点击修改按钮！");
            return false;
        }      
        if(strSql[0][3] == null ||strSql[0][3] == '')
        {
            alert("户名为空，若修改以后请点击修改按钮！");
            return false;
        } 
        if(strSql[0][4] == null ||strSql[0][4] == '')
        {
            alert("账号为空，若修改以后请点击修改按钮！");
            return false;
        }     
             
      }
   }
    return true;
}

