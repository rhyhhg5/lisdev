var showInfo;
var turnPage = new turnPageClass();

function Print(type)
{
	if(!verifyInput()){
		return false;
	}
        
	if(!checkData()){
		return false;
	}
	fm.downloadtype.value = type;
    fm.submit();
}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

function checkData(){
	if(fm.ManageCom.value == ""){
		alert("管理机构不能为空");
		return false;
	}
	var signdate = fm.SignStartDate.value != "" && fm.SignEndDate.value != "";
	var cvalidate = fm.CVStartDate.value != "" && fm.CVEndDate.value != "";
	var customgetpoldate = fm.CusStartDate.value != "" && fm.CusEndDate.value != "";
	
	if(signdate || cvalidate || customgetpoldate){
		if(signdate){
			if(dateDiff(fm.SignStartDate.value,fm.SignEndDate.value,"M") <= 3){
				return true;
			}
		}
		if(cvalidate){
			if(dateDiff(fm.CVStartDate.value,fm.CVEndDate.value,"M") <= 3){
				return true;
			}
		}
		if(customgetpoldate){
			if(dateDiff(fm.CusStartDate.value,fm.CusEndDate.value,"M") <= 3){
				return true;
			}
		}
		alert("至少需要有一组日期查询时间范围小于三个月");
		return false;
	}
	
	alert("签单日期、生效日期、回执回销日期至少录入一组");
	return false;
}