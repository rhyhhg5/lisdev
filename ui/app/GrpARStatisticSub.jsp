<%
//程序名称：ARStatisticSub.jsp
//程序功能：F1报表生成
//创建日期：2005-09-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%
		System.out.println("start");
  	CError cError = new CError();
  	boolean operFlag=true;
  	
		String tRela  = "";
		String FlagStr = "";
		String Content = "";
		String strOperation = "";
		String OperatorManagecom = "";
  	String StartDate = request.getParameter("StartDate");
  	String EndDate = request.getParameter("EndDate");
  	String transact = request.getParameter("fmtransact");
  	String QYType_ch = StrTool.unicodeToGBK(request.getParameter("QYType_ch"));
  	String QYType = StrTool.unicodeToGBK(request.getParameter("QYType"));
		String ManageCom = request.getParameter("ManageCom");
		
System.out.println("ManageCom1: ...."+ ManageCom);
		
  	GlobalInput tG = new GlobalInput(); 
 		tG=(GlobalInput)session.getValue("GI");	
 		OperatorManagecom=tG.ManageCom;
 		
  	TransferData StartEnd = new TransferData();
   	StartEnd.setNameAndValue("StartDate",StartDate);
  	StartEnd.setNameAndValue("EndDate",EndDate);
  	StartEnd.setNameAndValue("OperatorManagecom",OperatorManagecom);
  	StartEnd.setNameAndValue("QYType",QYType);
    StartEnd.setNameAndValue("QYType_ch",QYType_ch);
    StartEnd.setNameAndValue("Managecom",ManageCom); 	
System.out.println("ManageCom2: ...."+ ManageCom);
    System.out.println("StartDate"+StartDate);
    System.out.println("EndDate"+EndDate);
    System.out.println("OperatorManagecom"+OperatorManagecom);
    System.out.println("QYType"+QYType);
    System.out.println("QYType_ch"+StrTool.unicodeToGBK(QYType_ch));
    //System.out.println("Managecom"+Managecom);
		VData tVData = new VData();
		VData mResult = new VData();
		CErrors mErrors = new CErrors();
    tVData.addElement(tG);
    tVData.addElement(StartEnd);
 
    GrpARStatisticUI tGrpARStatisticUI = new GrpARStatisticUI();
	  XmlExport txmlExport = new XmlExport();    
    if(!tGrpARStatisticUI.submitData(tVData,"PRINT"))
    {
       	operFlag=false;
       	Content=tGrpARStatisticUI.mErrors.getFirstError().toString();                 
    }
    else
    {  
    	System.out.println("--------成功----------");  
			mResult = tGrpARStatisticUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	
ExeSQL tExeSQL = new ExeSQL();
//获取临时文件名
String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
String strFilePath = tExeSQL.getOneValue(strSql);
String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
//获取存放临时文件的路径
//strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
//String strRealPath = tExeSQL.getOneValue(strSql);
String strRealPath = application.getRealPath("/").replace('\\','/');
String strVFPathName = strRealPath + "//" +strVFFileName;

CombineVts tcombineVts = null;	
if (operFlag==true)
{
	//合并VTS文件
	String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
	tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);

	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
	tcombineVts.output(dataStream);

	//把dataStream存储到磁盘文件
	//System.out.println("存储文件到"+strVFPathName);
	AccessVtsFile.saveToFile(dataStream,strVFPathName);
	System.out.println("==> Write VTS file to disk ");

	System.out.println("===strVFFileName : "+strVFFileName);
//本来打算采用get方式来传递文件路径
	response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=03&RealPath="+strVFPathName);
}
	
	else
	{
    	FlagStr = "Fail";

%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
	
	//window.opener.afterSubmit("<%=FlagStr%>","<%=Content%>");	
	
</script>
</html>
<%
  	}
%>