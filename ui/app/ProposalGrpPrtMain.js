//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;
var manageCom = '';
var AppntName ='';

//提交，保存按钮对应操作,提交申请
function printPol()
{
	
	var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
     		 if (GrpPolGrid.getSelNo(i)) { 
     			 checkFlag = GrpPolGrid.getSelNo();
     			 break;
  		 	 }
  }
  if(checkFlag){
  	if(fm.RePrtReasonCode.value==""||fm.Remark.value==""){
		alert("请选择重打原因代码，并录入详细重打原因");
		return;
		}
 
  fm.tContNo.value = GrpPolGrid.getRowColData(checkFlag - 1, 1); 
  fm.tPrtNo.value=GrpPolGrid.getRowColData(checkFlag - 1, 2); 
  fm.tManageCom.value=GrpPolGrid.getRowColData(checkFlag - 1, 7); 
  fm.fmtransact.value="PRINT";	
  //保单打印类型校验--汇交件打印类型校验
  var strSql="select ContPrintType from lcgrpcont where grpcontno='"+fm.tContNo.value+"'";
  var arrResult = easyExecSql(strSql);
  if (arrResult!=null && arrResult[0][0]!="" && arrResult[0][0]=="5") {
	  if(fm.ContPrintType.value!="5"){
		  alert("该保单是汇交件，保单打印类型应该是5-汇交件！");
		  return;  
	  }
	  
	}
  var strSql1="select cardflag from lcgrpcont where grpcontno='"+fm.tContNo.value+"' with ur";
  var arrResult1 = easyExecSql(strSql1);
  if(arrResult1!=null && arrResult1[0][0]!="" && arrResult1[0][0]=="cd"){
	  if(fm.ContPrintType.value!="2"){
		  alert("该保单是一卡通保单，保单打印类型应该是2-委托管理型！");
		  return;
	  }
  }
	
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//showSubmitFrame(mDebug);
	fm.fmtransact.value="PRINT";
	fm.submit();
	}  	
	else{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	initHiddenBox();
		if(!verifyInput2())
	return false;
	
	var strManageComWhere = " AND ManageCom LIKE '" + manageCom + "%25' ";
	if( fm.ManageCom.value != '' )
	{
		strManageComWhere += " AND ManageCom LIKE '" + fm.ManageCom.value + "%25' ";
	}
	var strAppntNameWhere = " AND AppntName LIKE '%25" + AppntName + "%25' ";
	var strgrpAppntNameWhere = " AND grpName LIKE '%25" + AppntName + "%25' ";
	if( fm.AppntName.value != '' )
	{
		strAppntNameWhere += " AND AppntName LIKE '%25" + fm.AppntName.value + "%25' ";
		strgrpAppntNameWhere+=" AND grpName LIKE '%25" + fm.AppntName.value + "%25' ";
	}

	// 书写SQL语句
	var strSQL = "";
	strSQL = "select GrpContNo, PrtNo, CValiDate,grpName,Prem,getUniteCode(agentcode),managecom,signdate,"
	       + "(select makedate from lccontprint where prtno=LCgrpCont.prtno fetch first 1 rows only),"
	       + "(select printcount from lccontprint where prtno=LCgrpCont.prtno fetch first 1 rows only),'2'  "
	       + "from LCgrpCont where AppFlag in ('1','9') AND PrintCount >= 1 and (cardflag<>'0' or cardflag is null ) "
				 + getWherePart( 'grpContNo','ContNo' )
				 + getWherePart( 'PrtNo' )
				 + getWherePart( 'ManageCom','ManageCom','like' )
				 + getWherePart( 'PolApplyDate' )
				 + getWherePart( 'CValiDate' )
				 + getWherePart( 'SignDate' )
				 + getWherePart( 'getUniteCode(agentcode)','AgentCode' )
				 + strgrpAppntNameWhere
				 + " order by ManageCom,AgentCode with ur";

	turnPage.strQueryResult  = easyQueryVer3(strSQL);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有查询到要处理的业务数据！");
		return false;
	}
	
	//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = GrpPolGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

}

//判断是否生成新数据
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }

}
function GridFuncInit( cCodeName, Field )
{
	initHiddenBox();

}
function initHiddenBox()
{
    fm.all('Remark').value = '';
    fm.all('fmtransact').value = '';
    fm.all('RePrtReasonCode').value = '';
    fm.all('RePrtReason').value = '';
    fm.all('tContNo').value = '';
    fm.all('tPrtNo').value = '';
    fm.all('tManageCom').value = '';
}
