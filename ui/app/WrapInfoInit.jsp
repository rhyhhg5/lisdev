<%@page import="com.sinosoft.utility.*"%>
<script language="JavaScript">
  
// 输入框的初始化（单记录部分）
function initInputBox()
{
    try
    {
        
    }
    catch(ex)
    {
        alert("WrapInfoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInputBox();
        initWrapInfoGrid();
        initRiskInfoGrid();
        initDutyInfoGrid();
    }
    catch(re)
    {
        alert("WrapInfoInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var WrapInfoGrid;

// 保单信息列表的初始化
function initWrapInfoGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="套餐编码";
        iArray[1][1]="50px";
        iArray[1][2]=50;
        iArray[1][3]=0;
        iArray[1][21]="wrapCode";

        iArray[2]=new Array();
        iArray[2][0]="套餐名称";
        iArray[2][1]="100px";
        iArray[2][2]=80;
        iArray[2][3]=0;
        iArray[2][21]="wrapName";

        iArray[3]=new Array();
        iArray[3][0]="团险/个险";
        iArray[3][1]="50px";
        iArray[3][2]=60;
        iArray[3][3]=0;
        iArray[3][21]="riskWrapType";

        iArray[4]=new Array();
        iArray[4][0]="套餐类型";
        iArray[4][1]="50px";
        iArray[4][2]=60;
        iArray[4][3]=0;
        iArray[4][21]="wrapType";

        WrapInfoGrid = new MulLineEnter("fm", "WrapInfoGrid");
        //这些属性必须在loadMulLine前
        WrapInfoGrid.mulLineCount = 0;
        WrapInfoGrid.displayTitle = 1;
        WrapInfoGrid.hiddenPlus = 1;
        WrapInfoGrid.hiddenSubtraction = 1;
        WrapInfoGrid.canSel = 1;
        WrapInfoGrid.canChk = 0;
        WrapInfoGrid.loadMulLine(iArray);
        WrapInfoGrid.selBoxEventFuncName = "wrapInfoDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

var RiskInfoGrid;

// 保单信息列表的初始化
function initRiskInfoGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="套餐编码";
        iArray[1][1]="50px";
        iArray[1][2]=50;
        iArray[1][3]=0;
        iArray[1][21]="wrapCode";

        iArray[2]=new Array();
        iArray[2][0]="险种编码";
        iArray[2][1]="50px";
        iArray[2][2]=60;
        iArray[2][3]=0;
        iArray[2][21]="riskCode";

        iArray[3]=new Array();
        iArray[3][0]="险种名称";
        iArray[3][1]="100px";
        iArray[3][2]=60;
        iArray[3][3]=0;
        iArray[3][21]="riskName";

        iArray[4]=new Array();
        iArray[4][0]="主险编码";
        iArray[4][1]="50px";
        iArray[4][2]=60;
        iArray[4][3]=0;
        iArray[4][21]="mainRiskCode";

        iArray[5]=new Array();
        iArray[5][0]="险种保费";
        iArray[5][1]="50px";
        iArray[5][2]=60;
        iArray[5][3]=0;
        iArray[5][21]="prem";

        RiskInfoGrid = new MulLineEnter("fm", "RiskInfoGrid");
        //这些属性必须在loadMulLine前
        RiskInfoGrid.mulLineCount = 0;
        RiskInfoGrid.displayTitle = 1;
        RiskInfoGrid.hiddenPlus = 1;
        RiskInfoGrid.hiddenSubtraction = 1;
        RiskInfoGrid.canSel = 1;
        RiskInfoGrid.canChk = 0;
        RiskInfoGrid.loadMulLine(iArray);
        RiskInfoGrid.selBoxEventFuncName = "riskInfoDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}

var DutyInfoGrid;

// 保单信息列表的初始化
function initDutyInfoGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";	//列宽
        iArray[0][2]=10;	//列最大值
        iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="套餐编码";
        iArray[1][1]="50px";
        iArray[1][2]=50;
        iArray[1][3]=0;
        iArray[1][21]="wrapCode";

        iArray[2]=new Array();
        iArray[2][0]="险种编码";
        iArray[2][1]="50px";
        iArray[2][2]=60;
        iArray[2][3]=0;
        iArray[2][21]="riskCode";

        iArray[3]=new Array();
        iArray[3][0]="责任编码";
        iArray[3][1]="50px";
        iArray[3][2]=60;
        iArray[3][3]=0;
        iArray[3][21]="dutyCode";

        iArray[4]=new Array();
        iArray[4][0]="责任名称";
        iArray[4][1]="100px";
        iArray[4][2]=60;
        iArray[4][3]=0;
        iArray[4][21]="dutyName";

        iArray[5]=new Array();
        iArray[5][0]="要素名";
        iArray[5][1]="50px";
        iArray[5][2]=60;
        iArray[5][3]=0;
        iArray[5][21]="CalFactorName";

        iArray[6]=new Array();
        iArray[6][0]="要素值";
        iArray[6][1]="50px";
        iArray[6][2]=60;
        iArray[6][3]=0;
        iArray[6][21]="CalFactorValue";

        DutyInfoGrid = new MulLineEnter("fm", "DutyInfoGrid");
        //这些属性必须在loadMulLine前
        DutyInfoGrid.mulLineCount = 0;
        DutyInfoGrid.displayTitle = 1;
        DutyInfoGrid.hiddenPlus = 1;
        DutyInfoGrid.hiddenSubtraction = 1;
        DutyInfoGrid.canSel = 0;
        DutyInfoGrid.canChk = 0;
        DutyInfoGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>