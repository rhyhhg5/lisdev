//程序名称：ContDelete
//程序功能：个单整单删除
//创建日期：2004-12-06 11:10:36
//创建人  ：Zhangrong
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保
window.onfocus=myonfocus;
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	window.focus();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content);
  }
  else
  { 
    alert("操作成功");
  }  
  query();
  fm.all("ContNo").value = ""; 
  fm.all("PrtNo").vlaue = ""; 
  fm.all('DeleteReason').value = "";    
  
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//查询单
function query()
{
	// 初始化表格                                                                                                                                                           
	initGrid();	
	//alert(fm.QAgentCode.value);
	/*if(!verifyInput2())
	return false;*/
	/*var tagentcode="";
	if(fm.QAgentCode.value!=""){
		var magentcode=easyExecSql(" select getAgentCode('"+fm.QAgentCode.value+"') from dual");
		tagentcode=" and a.AgentCode='"+magentcode+"'";
	} */
	// 书写SQL语句
	var str = "";	
	var strsql = "";
    //
	strsql = "select a.ProposalContNo,a.ContNo,a.PrtNo,a.AppntName,a.ManageCom,a.SaleChnl,a.AgentCom from LCCont a where 1=1 and ContType='1'"
				 + getWherePart( 'a.ProposalContNo','QProposalNo' )
				 + getWherePart( 'a.ContNo','QContNo' )
				 + getWherePart( 'a.ManageCom','QManageCom','like')
				 //+ tagentcode
				 + getWherePart( 'getUniteCode(a.agentcode)','QAgentCode' )
				 + getWherePart( 'a.PrtNo','QPrtNo')
                 
                 // 加入渠道、网点条件
                 + getWherePart("a.SaleChnl","SaleChnlCode")
                 + getWherePart("a.AgentCom","AgentComBank")
                 // ----------------------------------------
                 
				 +" and a.ManageCom like '"+manageCom+"%%'" 
				 +" and a.prtno like 'PD%' ";
                 
            	// 如果需要，只显示简易件。
//                if(bIsBrief == 1)
//                {
//                    strsql += " and a.cardflag in ('1', '2', '3', '6') ";
//                }
                // -------------------------------
                 
				 if (fm.all("QState").value == "已签单")
		strsql = strsql + " and a.AppFlag = '1' "
	else if (fm.all("QState").value != null && fm.all("QState").value != "")
		strsql = strsql + " and (a.AppFlag <> '1' or a.AppFlag is null)";
		
				strsql = strsql+"union select 'x','',missionprop1,'','','','' from lwmission lwm "
                    + " where not exists (select 1 from LCCont lcc where lcc.ContType='1' and lcc.PrtNo = lwm.Missionprop1)  "
                 + " and activityid in ('0000001099','0000001098', '0000009001') and missionprop3 like '"+manageCom+"%%' and missionprop3 like 'PD'"                
				 + getWherePart( 'missionprop1','QPrtNo' )	;
                 
                // 如果需要，只显示简易件。
//                if(bIsBrief == 1)
//                {	
//                    strsql += " and ProcessId = '0000000007' ";
//                }
                // -------------------------------
                
                // 加入渠道、网点条件
                 if(fm.SaleChnlCode.value != "" || fm.AgentComBank.value != "")
                 {
                    strsql += " and 1 = 2 ";
                 }
                 // ----------------------------------------
	
	//strsql = strsql + " order by  a.ContNo ";
	
	
  //查询SQL，返回结果字符串
  turnPage.pageLineNum = 50;
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的投保单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组	
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = Grid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  fm.all("ContNo").value = "";
  return true;
}


//
function ContSelect(parm1,parm2)
{
	var cPrtNo = "";
	var cPolNo = "";
	var cProposalNo = "";
	if(fm.all(parm1).all('InpGridSel').value == '1' )
	{
		//当前行第1列的值设为：选中
   		cPrtNo = fm.all(parm1).all('Grid3').value;	//流水号
   		cContNo = fm.all(parm1).all('Grid2').value;	//合同号
   		cProposalNo = fm.all(parm1).all('Grid1').value;	//投保单号
   		//alert(cContNo);
   	}
   	//if(cContNo == null || cContNo == "" )
   	//{
   	//	alert("请选择一条有效的团体单!");
   	//}
   	//else
   	//{
		fm.all("ContNo").value = cContNo;
		fm.all("PrtNo").value = cPrtNo;
   	//}
}

function deleteCont()
{
    var tSel = Grid.getSelNo();
    if( tSel == 0 || tSel == null || fm.PrtNo.value == "")
    {
        alert( "请先选择一条记录。" );
        return false;
    }
        
	if(fm.DeleteReason.value == "")
	{
		alert("没有录入删除原因，不能新单删除!")
		return;
	}
	
	var strsql="select 1 from es_doc_main where"
						+" doccode like '"+fm.PrtNo.value
						+"1%%' or doccode like '"
						+fm.PrtNo.value
						+"2%%' or doccode like '"
						+fm.PrtNo.value+"9%%'";
	var arr = easyExecSql(strsql);
  
	if(arr)
	{
		if(!confirm("该保单有已经扫描上传的体检、问题件、契调或核保通知书！是否确认删除"))
		{
			return;
		}
	}
	
	var sql = "select 1 from Es_Doc_Main esd "
	        + "where State is not null "
	        + "   and State = '03' "
	        + "   and DocCode = '" + fm.PrtNo.value + "' "
	        + "   and exists(select 1 from bpomissionstate where bussno = esd.doccode and state<>'05' ) ";
	var rs = easyExecSql(sql);
	if(rs)
	{
	  alert("扫描件已发送至外包录入前置机，不能删除");
	  return false;
	}
	
    // 在删除前，必须进行先财务锁定。
	sql = "select 1 from LJSPay "
	    + "where OtherNoType = '9' "
	    + "   and OtherNo = '" + fm.PrtNo.value + "' "
	    + "   and (BankOnTheWayFlag = '1' or (CanSendBank = '0' or CanSendBank is null or CanSendBank = '2')) ";
	rs = easyExecSql(sql);
	if(rs)
	{
	  alert("收费已发盘或尚未进行缴费锁定，不能删除。");
	  return false;
	}
    // ---------------------------------
	
	var strsql = "select 1 from ljtempfee t where otherno = '" + fm.PrtNo.value + "' "
            + " and EnterAccDate is not null ";
			// + "and ConfDate is not null and tempfeeno not in (select tempfeeno from ljagettempfee)";
            // 张成轩 2012-5-23 需求有调整 在做完暂收费退后也不能进行新单删除
			//+ "and ConfFlag = '0' ";
	var arr = easyExecSql(strsql);
	if(arr){
		alert("保单已交费,不能删除!");
		return ;
	}

	//if (fm.all("ContNo").value == null || fm.all("ContNo").value == "")
	//{
	//	alert("请先选择一条保单！");
	//	return;
	//}
	//var strsql="select 1 from ljtempfee where otherno='"+fm.all("PrtNo").vlaue+"'";
	//var arr = easyExecSql(strsql);
	//if(arr){
	//	alert("财务已交费！不能进行新单删除！");
	//	return;
	//}
    
	if (!confirm("确认要删除该保单吗？"))
	{
		return;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.action = "./PadContDeleteChk.jsp";
	fm.submit();
}
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.QAgentCode.value = "";
  	fm.QAgentCodeName.value = "";   
 }
}

/**
 * 下拉框，选定后触发事件。
 */
function afterCodeSelect(tCodeName, tObj)
{
    if (tCodeName == "lcsalechnl")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    showInfo=window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
    }
}

/**
 * EXCEL下载
 */
function queryDown()
{
	if(Grid.mulLineCount == 0)
  {
     alert("没有需要下载的数据");
     return false;
  }
  fm.sql.value = turnPage.strQuerySql;
  fm.action = "PadContDeleteDown.jsp";
  fm.submit();
}
 