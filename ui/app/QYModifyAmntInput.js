var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;

window.onfocus = myonfocus;

// 查询按钮
function easyQueryClick()
{
	initPolGrid();
	if(!verifyInput2())
		return false;
	
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == "null"){
		alert("管理机构必须填写！");
		return false;
	}
	if((fm.PrtNo.value == null || fm.PrtNo.value == "" || fm.PrtNo.value == "null") && (fm.ContNo.value == null || fm.ContNo.value == "" || fm.ContNo.value == "null")){
		alert("印刷号或合同号必须填写其一！");
		return false;
	}
	if(fm.ContType.value == null || fm.ContType.value == "" || fm.ContType.value == "null"){
		alert("保单类型必须填写！");
		return false;
	}
	/*
	if(fm.ContPlanCode.value == null || fm.ContPlanCode.value == "" || fm.ContPlanCode.value == "null"){
		alert("保障计划编码必须填写！");
		return false;
	}
	if(fm.ContPlanCode.value == null || fm.ContPlanCode.value == "" || fm.ContPlanCode.value == "null"){
		alert("保障计划编码必须填写！");
		return false;
	}
	if(fm.RiskCode.value == null || fm.RiskCode.value == "" || fm.RiskCode.value == "null"){
		alert("险种编码必须填写！");
		return false;
	}
	if(fm.DutyCode.value == null || fm.DutyCode.value == "" || fm.DutyCode.value == "null"){
		alert("责任编码必须填写！");
		return false;
	}
	*/
	
	// 书写SQL语句
    var strSQL = "";
    if(fm.ContType.value == 1)
    {
    	strSQL = "select distinct lcc.Prtno,lcc.Contno,lcc.CValiDate,lcc.AppntName,lcp.contplancode,lcp.riskcode,lcd.dutycode,lcd.amnt," 
    	+ "lcc.AgentCode,lcc.ManageCom,lcc.ContType "
    	+ "from lccont lcc "
    	+ "inner join lcpol lcp on lcc.contno = lcp.contno "
    	+ "inner join lcduty lcd on lcd.polno = lcp.polno "
    	+ "where 1 = 1 "
    	+ "and lcc.appflag = '1' and lcc.ContType = '1' "
        + getWherePart('lcc.ManageCom','ManageCom','like')
        + getWherePart('lcc.PrtNo','PrtNo')
        + getWherePart('lcc.ContNo','ContNo')
        + getWherePart('lcp.riskcode','RiskCode')
        + getWherePart('lcd.dutycode','DutyCode');
    }
    else if(fm.ContType.value == 2)
    {
    	strSQL = "select distinct lgc.Prtno,lgc.GrpContno,lgc.CValiDate,lgc.GrpName,lcp.contplancode,lcp.riskcode,lcd.dutycode,lcd.amnt," 
    	+ "lgc.AgentCode,lgc.ManageCom,'2' "
    	+ "from lcgrpcont lgc "
    	+ "inner join lcpol lcp on lgc.grpcontno = lcp.grpcontno "
    	+ "inner join lcduty lcd on lcd.polno = lcp.polno "
    	+ "where 1 = 1 "
    	+ "and lgc.appflag = '1' "
        + getWherePart('lgc.ManageCom','ManageCom','like')
        + getWherePart('lgc.PrtNo','PrtNo')
        + getWherePart('lgc.GrpContNo','ContNo')
        + getWherePart('lcp.contplancode','ContPlanCode')
        + getWherePart('lcp.riskcode','RiskCode')
        + getWherePart('lcd.dutycode','DutyCode');
    }
    turnPage.pageLineNum = 50;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    //fm.querySql.value = strSQL;
}

//维护销售渠道
function modifyAmnt()
{
	var i = 0;
	var checkFlag = 0;
	
	for(i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getSelNo(i))
		{
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	
	if(checkFlag == 0)
	{
		alert("请先选择一条保单信息！");
		return false;
	}
	if(fm.ModifyAmnt.value == null || fm.ModifyAmnt.value == "" || fm.ModifyAmnt.value == "null"){
		alert("修改后责任保额不能为空！");
		fm.ModifyAmnt.focus();
		return false;
	}
	if(!isNumeric(fm.ModifyAmnt.value)){
		alert("修改后责任保额必须为数字！");
		fm.ModifyAmnt.focus();
		return false;
	}
	
	var tContNo=PolGrid.getRowColData(checkFlag-1,2);
	var tContPlanCode=PolGrid.getRowColData(checkFlag-1,5);
	var tRiskCode=PolGrid.getRowColData(checkFlag-1,6);
	var tDutyCode=PolGrid.getRowColData(checkFlag-1,7);
	var tOldAmnt=PolGrid.getRowColData(checkFlag-1,8);
	var tContType=PolGrid.getRowColData(checkFlag-1,11);
	
	var tWhere = "";
	var tSQLBQ = "";
	var tLPSQL = "";
	if("1" == tContType){
		tWhere = " where contno = '"+tContNo+"' and riskcode = '"+tRiskCode+"' ";
		tSQLBQ = "select 1 from lpedoritem where contno = '"+tContNo+"' ";
		tLPSQL = "select 1 from LLClaimDetail where contno = '"+tContNo+"' union select 1 from LLClaimPolicy where contno = '"+tContNo+"' ";
	}else{
		tWhere = " where grpcontno = '"+tContNo+"' and contplancode = '"+tContPlanCode+"' and riskcode = '"+tRiskCode+"' ";
		tSQLBQ = "select 1 from lpgrpedoritem where grpcontno = '"+tContNo+"' ";
		tLPSQL = "select 1 from LLClaimDetail where grpcontno = '"+tContNo+"' union select 1 from LLClaimPolicy where grpcontno = '"+tContNo+"' ";
	}
	
	var tBQResult = easyExecSql(tSQLBQ);
	if(tBQResult){
		if(!confirm("该单做过保全项目，是否继续？")){
			return false;
		}
	}
	
	
	var tLPResult = easyExecSql(tLPSQL);
	if(tLPResult){
		if(!confirm("该单做过理赔项目，是否继续？")){
			return false;
		}
	}
	
	var tSQL = "select distinct amnt,riskamnt from lcpol "+tWhere;
	var tResult = easyExecSql(tSQL);
	if(tResult){
		for(var i=0;i<tResult.length;i++){
			if(tResult[i][0] != tResult[i][1]){
				if(!confirm("该险种的责任累加保额与险种保额不一致，是否继续？")){
					return false;
				}
			}
		}
	}else{
		alert("获取险种保额失败！");
		return false;
	}
	
	if(!confirm("修改前保额："+tOldAmnt+",修改后保额："+fm.ModifyAmnt.value+"，是否继续？")){
		return false;
	}
	
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.submit(); //提交
}
function afterSubmit(FlagStr,content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
	
}