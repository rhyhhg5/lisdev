<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>


<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%

	boolean errorFlag = false;
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");
	//生成文件名
	Calendar cal = new GregorianCalendar();
	String min = String.valueOf(cal.get(Calendar.MINUTE));
	String sec = String.valueOf(cal.get(Calendar.SECOND));
	String downLoadFileName = "保单明细_" + tG.Operator + "_" + min + sec + ".xls";
	String filePath = application.getRealPath("temp");
	System.out.println("filepath................" + filePath);
	String tOutXmlPath = filePath + File.separator + downLoadFileName;
	System.out.println("OutXmlPath:" + tOutXmlPath);
	String querySql = request.getParameter("querySql");
    querySql = querySql.replaceAll("%25","%");
    String[][] tTitle = {
				{	"省级机构代码","省级机构名称","管理机构代码","管理机构名称","渠道名称","保单号","险种代码","险种名称","保额","保费","保单生效日","保单签单日","投保人客户号","投保人姓名","投保人性别","年龄","投保人证件类型","投保人证件号","联系电话","联系地址","缴费频次","ICD编码","ICD名称" }};
    // 表头的显示属性
    int[] displayTitle = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13,14,15,16,17,18,19,20,21,22,23};
    // 数据的显示属性
    int[] displayData = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13,14,15,16,17,18,19,20,21,22,23 };
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"明细数据"};
    
    createexcellist.addSheetGBK(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
      createexcellist.setRowColOffset(row,0);//设置偏移
    if(createexcellist.setData(querySql,displayData)==-1)
      errorFlag = true;
	if (!errorFlag)
		//写文件到磁盘
		try {
			createexcellist.write(tOutXmlPath);
		} catch (Exception e) {
			errorFlag = true;
			System.out.println(e);
		}
	//返回客户端
	if (!errorFlag)
		downLoadFile(response, filePath, downLoadFileName);
	out.clear();
	out = pageContext.pushBody();
%>