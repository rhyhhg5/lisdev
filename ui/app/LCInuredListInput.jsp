<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LCInuredListInput.js"></SCRIPT>
  <script>
  	var GrpContNo ="<%=request.getParameter("GrpContNo")%>";
  	var operator ="<%=tGI.Operator%>";
  </script>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LCInuredListInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LCInuredListSave.jsp" method=post name=fmSave target="fraSubmit" >
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInuredList);">
    		</td>
    		 <td class= titleImg>
        		 被保人清单表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLCInuredList" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title style="display:'none'">
      集体合同号码
    </TD>
    <TD  class= input style="display:'none'" >
      <Input class= 'common'   name=GrpContNo verify="集体合同号码|int&len=10">
    </TD>
    <TD  class= title>
      被保人序号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InsuredID verify="被保人序号|int">
    </TD>
    <TD  class= title style="display:'none'">
      状态
    </TD>
    <TD  class= input style="display:'none'">
      <Input class= 'common' name=State verify="状态|int">
    </TD>
    <TD  class= title>
      被保人客户号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InusredNo verify="被保人客户号|int&len=9">
    </TD>


   <TD  class= title>
      在职/退休
    </TD>
    <TD  class= input>
    	<Input class= 'codeno' name=Retire verify="在职/退休|int" CodeData= "0|^1|在职^2|退休" ondblClick= "showCodeListEx('',[this,RetireName],[0,1],null,null,null,1);" onkeyup= "showCodeListKeyEx('',[this,RetireName],[0,1],null,null,null,1);"><input class = codename name=RetireName readonly=true>
    </TD>

    <TD  class= title>
      员工姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EmployeeName verify="员工姓名|len<=20">
    </TD>
              </TR>
      <TR  class= common>
    <TD  class= title>
      被保人姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InsuredName verify="被保人姓名|len<=20">
    </TD>
    <TD  class= title>
     与员工关系</TD>
    <TD  class= input>
    <Input class=codeNo name=Relation  ondblclick="return showCodeList('Relation', [this,RelationName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,RelationName],[0,1]);"><input class=codename name=RelationName readonly=true> </TD>

 <TD  class= title>
         性别
    </TD>
   <TD  class= input>
  <Input class=codeNo name=Sex  verify="投保人性别|code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true elementtype=nacessary>    
  </TD>

    <TD  class= title>
      出生日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=Birthday verify="出生日期|date">
    </TD>
              </TR>
      <TR  class= common>
    <TD  class= title>
      证件类型
    </TD>
    <TD  class= input>
      <Input class=codeNo name="IDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1]);"><input class=codename name=AppntIDTypeName readonly=true elementtype=nacessary>    
    </TD>
   <TD  class= title>
     证件号码
   </TD>
   <TD  class= input colspan=1>
     <Input class= common3 name="IDNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" >
   </TD> 
    </TD>

    <TD  class= title>
      保险计划
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContPlanCode >
    </TD>

  	<TD class=title> 职业类别
      </TD>
			 <TD class=input>
			<Input class="codeno" name=OccupationType  verify="职业类别|code:OccupationType" MAXLENGTH=0 ondblclick="return showCodeList('OccupationType',[this,OccupationTypeName],[0,1]);" onkeyup="return showCodeListKey('OccupationType',[this,OccupationTypeName],[0,1]);"><input class=codename name=OccupationTypeName readonly=true> 
	              </TR>
      <TR  class= common>
    <TD  class= title>
      理赔金转帐银行
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BankCode >
    </TD>
    <TD  class= title>
      帐号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BankAccNo >
    </TD>
     
    <TD  class= title>
      户名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccName >
    </TD>
    <TD  class= title>
      学校
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SchoolName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      班级
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ClassName >
    </TD>
    <TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
     
    <TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
    <TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
  </TR>
</table>
</Div>
<Div  id= "divHidden" style= "display: ''" style="float: right">
 <TR  class= common>
    <TD  class= title>
      合同号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContNo >
    </TD>
    <TD  class= title>
      批次号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BatchNo >
    </TD>
       <TD  class= title>
      操作员
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD> 
  </div>  
<hr>
<div>
    <font color="#ff0000">提示：</font>若在被保人清单上载过程中，出现“执行插入语句失败”的图像，请先检查名单：</br>
    &nbsp;&nbsp;1. 检查被保人证件号码是否过长，尤其是“军官证”的情况。总长度不大于30（其中总长度=汉字*4+字母个数）。</br>
    &nbsp;&nbsp;2. 请将名单最后的空白行，全部选中，双击右键，点击“删除行”，保存后即可。</br>
    &nbsp;&nbsp;3. 当选中“被保险人导入错误信息”中的数据，对其信息进行修改后，请再次点击“计算保费”按钮进行算费操作。</br>
</div>
<Div id="divLCInuredList1" style="display:'none'">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLCInuredListGrid">
  				</span> 
		    </td>
			</tr>
		</table>
</div>
    <input type=hidden id="" name="fmtransact">
    <input type=hidden id="" name="fmAction">
    <input type="hidden" name="querySql">
</form>
<form action="./LCInuredListSave.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
<div id="divDiskInput" style="display:''">
    <table class = common >
    	<TR>
      <TD  colspan=2>
        
      </TD>
	  	</TR>
    <TR  >
      <TD  width='8%' style="font:9pt">
        文件名：
      </TD>
      
      <TD  width='80%'>
        <input type="file"　width="100%" name=FileName class= common>
        <input VALUE="上载被保险人清单" class="cssbutton" TYPE=button onclick = "InsuredListUpload();" >
        <input value="无名单录入" class="cssbutton" type= button onclick = "NoNameCont();">
        <input VALUE="计算保费" class="cssbutton" name="CalPremButton" TYPE=button onclick = "CalInsuredPrem();" >       
        <!-- <input VALUE="保费重算" class="cssbutton" name="ReCalPremButton" TYPE=button onclick = "ReCalInsuredPrem();" > -->        	
        <input VALUE="汇交件投保人管理" class="cssbutton" name="HJAppntButton" TYPE=button onclick = "HJAppnt();" >
        <input VALUE="保费解锁" class="cssbutton" name="UnlockButton" TYPE=button onclick = "Unlock();" >
        <input type="hidden" width="100%" name="insuredimport" value ="1">
      </TD>
    </TR>
    
	    <input type=hidden name=ImportFile>
	</table>
	<div id="divInsuredInfo" style="display: ''">
	
	<table class=common>
		<tr class=common >
			<TD  class= title>
		     被保人总数
		  </TD>
		  <TD  class= input>
		    <Input class= 'common' name=SumInsured >
		  </TD>
		  <TD  class= title>
		     待导入人数
		  </TD>
		  <TD  class= input>
		    <Input class= 'common' name=HoldInsured >
		  </TD>
		  <TD  class= title>
		     导入成功数
		  </TD>
		  <TD  class= input>
		    <Input class= 'common' name=SuccInsured >
		  </TD>
		  <TD  class= title>
		     导入失败数
		  </TD>
		  <TD  class= input>
		    <Input class= 'common' name=FailInsured >
		  </TD>
		</tr>
	</table>
    </div>
    </br>
    <div>
    <font color="#ff0000">提示：</font>1. 算费过程中可以点击“保费解锁”功能查看算费进度。</br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. 若“完成人数”已超过5分钟未增加，可进行解锁操作，解锁后点击“计算保费”按钮继续算费操作。</br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. 若算费过程未中断，再次点击“计算保费”可能会导致保单数据插入错误或被保险人数据重复。</br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;若发生此种情况需要使用“团体保单-新单删除-被保险人删除”功能将保单下所有被保人数据删除后再继续操作。</br>
    
</div>
    <table>
        <tr>
            <td class=common>
                <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContPlanPeoples);">
            </td>
         
            <td class= titleImg>各保障计划导入的被保险人数：</td>
        </tr>
    </table>
    <Div id= "divContPlanPeoples" style= "display: ''">
        <Table  class= common>
            <TR  class= common>
                <TD text-align: left colSpan=1>
                    <span id="spanContPlanPeoplesGrid" ></span>
                </TD>
            </TR>
        </Table>
        <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();">
        <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();">
        <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();">
        <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();">
    </Div>
    </br>
    <table>
        <tr>
            <td class=common>
                <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDiskErr);">
            </td>
            <td class= titleImg>被保险人导入错误信息：</td>
            <td class=common>
                <input VALUE="错误信息下载" class="cssbutton" name="DownloadListButton" TYPE=button onclick = "downloadList();" >
            </td>
        </tr>
    </table>
    <Div  id= "divDiskErr" style= "display: ''">
        <Table  class= common>
            <TR  class= common>
                <TD text-align: left colSpan=1>
                    <span id="spanDiskErrQueryGrid" ></span>
                </TD>
            </TR>
        </Table>
        <p align="center">
            <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
            <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
            <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
            <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
        </p>
    </Div>
</div>
    <input type=hidden id="" name="fmtransact">
    <input type=hidden id="" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
