<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpFeeSave.jsp
//程序功能：伤残等级给付比例保存
//创建日期：2015-03-19 15:12:33
//创建人 ：CrtHtml程序创建
//更新记录： 更新人  更新日期   更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.HashMap"%>
<%
//接收信息，并作校验处理。
//输入参数
LCContPlanDutyDefGradeSet tLCContPlanDutyDefGradeSet=new LCContPlanDutyDefGradeSet();
DefGradeRateUI tDefGradeRateUI = new DefGradeRateUI();

//输出参数
CErrors tError = null;
String tRearStr = "";
String tRela = "";
String FlagStr = "Fail";
String Content = "";
String mSql = "";

//全局变量
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

System.out.println("begin ...");

String tOperate=request.getParameter("mOperate");	//操作模式
String tGrpContNo = request.getParameter("mGrpContNo");	//集体合同号码
String ProposalGrpContNo = request.getParameter("ProposalGrpContNo");	//集体投保单号码
String ContPlanCode = request.getParameter("DefContPlan");	//保障级别
String DefDutycode = request.getParameter("DefContDuty");	//分类说明
System.out.println("grpcontno:"+tGrpContNo+"contplancode:"+ContPlanCode+"dutycode:"+DefDutycode);
String tPrtno=new ExeSQL().getOneValue("select prtno from lcgrpcont where grpcontno='"+tGrpContNo+"'");
String FeeCount[] = request.getParameterValues("DisableRateGridNo");
String tRiskCode[] = request.getParameterValues("DisableRateGrid1");	//险种编码
String tDefGrade[] = request.getParameterValues("DisableRateGrid3");	//伤残等级
String tRate[] = request.getParameterValues("DisableRateGrid5");	//给付比例
if( FeeCount !=null)
{
	int Count=FeeCount.length;
	for (int i = 0; i < Count; i++)  {
		LCContPlanDutyDefGradeSchema tLCContPlanDutyDefGradeSchema = new LCContPlanDutyDefGradeSchema();
		tLCContPlanDutyDefGradeSchema.setGrpContNo(tGrpContNo);
		tLCContPlanDutyDefGradeSchema.setPrtNo(tPrtno);
		tLCContPlanDutyDefGradeSchema.setContPlanCode(ContPlanCode);
		tLCContPlanDutyDefGradeSchema.setDutyCode(DefDutycode);
		tLCContPlanDutyDefGradeSchema.setDeformityGrade(tDefGrade[i]);
		if(tRate[i]!=null && !"".equals(tRate[i])){
			tLCContPlanDutyDefGradeSchema.setGetRate(tRate[i]);	
		}else{
			tLCContPlanDutyDefGradeSchema.setGetRate("0");
		}
		tLCContPlanDutyDefGradeSchema.setOperator(tG.Operator);
		tLCContPlanDutyDefGradeSchema.setMakeDate(PubFun.getCurrentDate());
		tLCContPlanDutyDefGradeSchema.setMakeTime(PubFun.getCurrentTime());
		tLCContPlanDutyDefGradeSchema.setModifyDate(PubFun.getCurrentDate());
		tLCContPlanDutyDefGradeSchema.setModifyTime(PubFun.getCurrentTime());
		tLCContPlanDutyDefGradeSet.add(tLCContPlanDutyDefGradeSchema);
	}
}
System.out.println("end ...");

// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tG);
tVData.addElement(tLCContPlanDutyDefGradeSet);
tVData.addElement(ProposalGrpContNo);
tVData.addElement(tGrpContNo);
tVData.addElement(ContPlanCode);
tVData.addElement(DefDutycode);
try{
	System.out.println("this will save the data!!!");
	tDefGradeRateUI.submitData(tVData,tOperate);
}
catch(Exception ex){
	ex.printStackTrace();
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}

if (!FlagStr.equals("Fail")){
	tError = tDefGradeRateUI.mErrors;
	if (!tError.needDealError()){
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	else{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>