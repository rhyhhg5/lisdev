<DIV id=DivLCAppntIndButton STYLE="display:''">
<!-- 投保人信息部分 -->
<table>
<tr>
<td>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
</td>
<td class= titleImg>
投保人信息(客户号)：<Input class= common  name=AppntCustomerNo >
<INPUT id="butBack" VALUE="查询" TYPE=button class= cssButton onclick="queryAppntNo();">
首次投保客户无需填写客户号）
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCAppntInd STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      姓名 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntName VALUE="" CLASS=common MAXLENGTH=20 verify="投保人姓名|notnull" >
    </TD>
    <TD CLASS=title>
      性别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntSex VALUE="" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('Sex', [this]);" onkeyup="return showCodeListKey('Sex', [this]);" verify="投保人性别|notnull&code:Sex" >
    </TD>
    <TD CLASS=title>
      出生日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntBirthday VALUE="" CLASS=common verify="投保人出生日期|notnull&date" >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      证件类型 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntIDType0 VALUE="0" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('IDType', [this]);" onkeyup="return showCodeListKey('IDType', [this]);" verify="投保人证件类型|code:IDType" >
    </TD>
    <TD CLASS=title>
      证件号码</TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntIDNo0 VALUE="" CLASS=common MAXLENGTH=20 >
    </TD>
    <TD CLASS=title>
      　</TD>
    <TD CLASS=input COLSPAN=1>
      　</TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      通讯地址
    </TD>
    <TD CLASS=input COLSPAN=3>
      <Input NAME=AppntPostalAddress VALUE="" CLASS=common3 MAXLENGTH=80 size="57" >
    </TD>
    <TD CLASS=title>
      通讯地址邮政编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntZipCode VALUE="" CLASS=common MAXLENGTH=6 verify="投保人邮政编码|zipcode" >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      固定电话</TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntPhone VALUE="" CLASS=common MAXLENGTH=18 >
    </TD>
    <TD CLASS=title>
      手机
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntMobile VALUE="" CLASS=common MAXLENGTH=18 >
    </TD>
    <TD CLASS=title>
      工作单位
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntGrpName VALUE="" CLASS=common MAXLENGTH=60 >
    </TD>
  </TR>
<TR CLASS=common>
	    <TD CLASS=title  >
	      授权转帐开户行 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=AppntBankCode VALUE="" CLASS=common MAXLENGTH=20 verify="开户行|code:bank" ondblclick="return showCodeList('bank',[this]);" onkeyup="return showCodeListKey('bank',[this]);" >
	    </TD>
	    <TD CLASS=title >
	      户名 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=AppntAccName VALUE="" MAXLENGTH=1 verify="户名|len<=20" >
	    </TD>
	    <TD CLASS=title >
	      帐号</TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=AppntBankAccNo VALUE="" CLASS=common verify="银行帐号|len<=40" >
	    </TD>
	  </TR>


  </TABLE>
</DIV>
