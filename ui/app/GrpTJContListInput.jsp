<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="GrpTJContListInput.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="GrpTJContListInputInit.jsp"%>
<title>天津理赔的团单被保险人承保信息报表</title>
</head>
<body onload="initForm();" >
	<form action="" method=post name=fm target="fraSubmit">
		<table class=common border=0 width=100%>
			<tr>
				<td class= titleImg align= center>按保单查询：</td>
			</tr>
		</table>
		
		<table class=common align=center>
			<tr class=common>
				<td class=title>管理机构</td>
				<td class=input><Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode" readonly=true ><input class=codename name=ManageComName readonly=true ></td>
				<td class=title>保单号</td>
				<td class=input><Input class=common name=ContNo></td>
				<td class=title>印刷号</td>
				<td class=input><Input class=common name=PrtNo></td>
			</tr>
			<tr class=common>
				<td class=title>包含险种1</td>
				<td class=input><input class="codeNo" name="RiskCode1" ondblclick="return showCodeList('riskcode', [this,RiskCode1Name], [0,1]);" onkeyup="return showCodeListKey('riskgrp', [this,RiskCode1Name],[0,1]);"><input class="codename" name="RiskCode1Name" style="width:80" readonly></td>
				<td class=title>包含险种2</td>
				<td class=input><input class="codeNo" name="RiskCode2" ondblclick="return showCodeList('riskcode', [this,RiskCode2Name], [0,1]);" onkeyup="return showCodeListKey('riskgrp', [this,RiskCode2Name],[0,1]);"><input class="codename" name="RiskCode2Name" style="width:80" readonly></td>
				<td class=title>生效日期起</td>
				<td class=input><Input class=coolDatePicker name=startCValiDate></td>
				<td class=title>生效日期止</td>
				<td class=input><Input class=coolDatePicker name=endCValiDate></td>
			</tr>
			<tr class=common>
				<td class=title>签单日期起</td>
				<td class=input><Input class=coolDatePicker name=startSignDate></td>
				<td class=title>签单日期止</td>
				<td class=input><Input class=coolDatePicker name=endSignDate></td>
			</tr>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" id="QueryButton" TYPE=button onclick="easyQueryClick();">
		<INPUT VALUE="下载被保险人清单" class="cssButton" id="DownLoadButton" TYPE=button onclick="downLoadLists();">
		<font color="red">备注：下载功能只能下载页面当前展示的保单信息</font>
		<br /><br />
		
		<Div  id= "divLCPol1" style= "display: ''" align = center>
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
	  					<span id="spanGrpPolGrid" >
	  					</span>
	  				</td>
	  			</tr>
	    	</table>
			<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage.firstPage();"> 
			<INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
			<INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage.nextPage();"> 
			<INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage.lastPage();">
  		</Div>
  		<input name="querySql" type="hidden" />
  		<input type="hidden" name=op value="" />
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>