<%
//程序名称：TbGrpConractorInit.jsp
//程序功能：
//创建日期：2006-5-17 15:19
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">                                      
function initForm()
{
  try
  {
	queryGrpInfo();
    
  	//alert(LoadFlag);
  	if(LoadFlag=='16'||LoadFlag=='23')
  	{
  	 ButtonDiv.style.display="none";
  	}
    initCoInsuranceParamGrid();
    queryCIParams();
    //initDutyFactor();
  }
  catch(re)
  {
    alert("CoInsuranceParamInput.jsp-->InitForm函数中发生异常:初始化界面错误!" + re.message);
  }
}


function initCoInsuranceParamGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=10;
        iArray[0][3]=0;
        
        iArray[1]=new Array();
        iArray[1][0]="共保机构代码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=2;
        iArray[1][4]="CoInsuranceCom";
        iArray[1][5]="1|2";
        iArray[1][6]="0|1";
        iArray[1][9]="共保机构代码|NotNull";
        iArray[1][15]="1"
        iArray[1][16]="1 and ManageCom like #" + fm.ManageCom.value +"%#";
        
        iArray[2]=new Array();
        iArray[2][0]="共保机构名称";
        iArray[2][1]="150px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="负担比例";
        iArray[3][1]="60px";
        iArray[3][2]=100;
        iArray[3][3]=1;
        iArray[3][9]="负担比例|NotNull|Num";
        
        
        CoInsuranceParamGrid = new MulLineEnter("fm", "CoInsuranceParamGrid");
        
        CoInsuranceParamGrid.mulLineCount = 0;   
        CoInsuranceParamGrid.displayTitle = 1;
        CoInsuranceParamGrid.locked = 0;
        CoInsuranceParamGrid.canSel = 0;
        CoInsuranceParamGrid.canChk = 0;
        CoInsuranceParamGrid.hiddenPlus = 0;
        CoInsuranceParamGrid.hiddenSubtraction = 0;
        CoInsuranceParamGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert(ex);
    }
}
</script>

