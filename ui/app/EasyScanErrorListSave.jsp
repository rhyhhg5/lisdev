<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：EasyScanErrorList.jsp
//程序功能：外包异常保单修改状态页面
//创建日期：2008-6-4
//创建人  ：ZhangJianbao
//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
  System.out.println("input in to scan delete page!");
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  //获得单证主表信息
  String batchNo = request.getParameter("BatchNo");
  String prtNo = request.getParameter("BussNo");
  System.out.println("BatchNo : " + batchNo + "  BussNo : " + prtNo);

  EasyScanDeleteBL tEasyScanDeleteBL = new EasyScanDeleteBL();
  try
  {
    TransferData mTransferData = new TransferData();
    mTransferData.setNameAndValue("prtNo", prtNo);
    mTransferData.setNameAndValue("batchNo", batchNo);
    
    VData vData =  new VData();
    vData.add(tG);
    vData.add(mTransferData);

    tEasyScanDeleteBL.submitData(vData, SysConst.UPDATE);
  }
  catch(Exception ex)
  {
    Content = "修改失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("after submit!");
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "")
  {
    tError = tEasyScanDeleteBL.mErrors;
    if (!tError.needDealError())
    {
      Content = " 修改成功! ";
      FlagStr = "Success";
    }
    else
    {
      Content = " 修改失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  </script>
</html>
