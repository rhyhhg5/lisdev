<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2009-08-10
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);

  String querySql = request.getParameter("querySql");
  
  querySql = querySql.replaceAll("%25","%");
  
	//设置表头
	String[][] tTitle = {{"管理机构编码","管理机构名称","保单的类型","个单号","印刷号","销售渠道","代理人编码","代理人姓名","团队外部编码","团队名称","银行网点名称","投保人姓名","投保人生日","投保人性别 ","投保人证件类型","投保人证件号码","投保人国籍","投保人婚姻状况","投保人职业类别","投保人职业代码","投保人工作单位","投保人岗位职务","投保人年收入","投保人联系地址","投保人邮编","投保人联系电话","投保人手机","保单生效日期","合同终止日期","投保日期","录入日期","复核日期","核保通过日期","签单日期","客户签收日期","回执回销日期","缴费方式","开户银行编码","开户银行名称","银行账号","银行户名","保费","保额（仅供参考）","被保险人姓名","被保险人生日","被保险人性别","被保险人证件类型","被保险人证件号码","被保险人工作单位","被保险人岗位职务","被保险人年收入","被保险人联系地址","被保险人邮编","被保险人联系电话","被保险人手机","被保险人婚姻","被保险人职业类别","被保险人职业代码","被保险人国籍","被保人总数","总保费（含首年追加保费）","保单状态","是否扫描","扫描时间"}};
	//表头的显示属性
	int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64};
    
  //数据的显示属性
  int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64};
  
  //生成文件
  CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
  createexcellist.createExcelFile();
  String[] sheetName ={"list"};
  createexcellist.addSheet(sheetName);
  int row = createexcellist.setData(tTitle,displayTitle);
  
  if(row ==-1) errorFlag = true;
  createexcellist.setRowColOffset(row+1,0);//设置偏移
  if(createexcellist.setData(querySql,displayData)==-1)
  {
  	errorFlag = true;
  	System.out.println(errorFlag);
  }
  if(!errorFlag)
  //写文件到磁盘
  try{
     createexcellist.write(tOutXmlPath);
  }catch(Exception e)
  {
  	errorFlag = true;
  	System.out.println(e);
  }
  //返回客户端
  if(!errorFlag)
  	downLoadFile(response,filePath,downLoadFileName);
  out.clear();
	out = pageContext.pushBody();
%>

