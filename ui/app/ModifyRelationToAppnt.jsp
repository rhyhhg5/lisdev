<%@ page contentType="text/html;charset=GBK"%>
<%@ include file="../common/jsp/UsrCheck.jsp"%>

<%
	//程序名称：
	//程序功能：
	//创建日期：2009-11-12
	//创建人  ：GUZG
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<script src="../common/javascript/CommonTools.js"></script>
		    
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

		<script src="ModifyRelationToAppnt.js"></script>
		<%@include file="ModifyRelationToAppntInit.jsp"%>
	</head>

	<body onload="initForm();">
		<form action="" method="post" name="fm" target="fraSubmit">
			<table>
				<tr>
					<td class="titleImg">
						请输入查询条件：
					</td>
				</tr>
			</table>

			<div id="divCondCode" style="display:''">
				<table class="common">
					<tr class="common">
						<td class="title8">
							保单号
						</td>
						<td class="input8">
							<input class="common" name="ContNo" verify="保单号|notnull" elementtype="nacessary" />
						</td>
						<td class="title8">&nbsp;</td>
                        <td class="input8">&nbsp;</td>
                        <td class="input8">&nbsp;</td>
                        <td class="input8">&nbsp;</td>
					</tr>
				</table>
			</div>

			<table>
				<tr>
					<td class="common">
						<input class="cssButton" type="button" value=" 查  询 " onclick="queryRelationToAppnt();" />
					</td>
					<td class="common">
						<input class="cssButton" type="button" id="btnImport" value="修  改" onclick="submitData();" />
					</td>
				</tr>
			</table>
			
			<div id="divRelation" style="display:''">
				<table class="common">
					<tr class="common">
						<td class="title8">
							修改前与投保人的关系:
						</td>
						<td class="input8">
							<input class="common" name="RalationToAppnt" readonly/>
						</td>
						<td CLASS="title">
						    修改后与投保人的关系:
			    		</td>
						<td CLASS="input" COLSPAN="1">
							<input name="RelationToAppntNew"  CLASS=codeNo ondblclick="return showCodeList('relation',[this,RelationToAppntNewName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RelationToAppntNew',[this,RelationToAppntNewName],[0,1],null,null,null,1);" verify="修改于投保人的关系|code:relation&notnull"><input class=codename name=RelationToAppntNewName readonly=true elementtype=nacessary>  
			    		</td>  
                        <Input type='hidden' name=ManageCom readonly >
                        <td class="input8">&nbsp;</td>
					</tr>
				</table>
			</div>
			
			<br />
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
