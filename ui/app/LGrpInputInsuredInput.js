
function initContInfo() {
	var contQuerySql = "select (select name from ldcom where comcode=gc.managecom),gc.grpname from lcgrpcont gc where prtno='" + PrtNo + "'";
	var arrResult = easyExecSql(contQuerySql);
	if(arrResult) {
		fm.PrtNo.value = PrtNo;
		fm.ManageCom.value = arrResult[0][0];
		fm.GrpName.value = arrResult[0][1];
	} else {
		alert("初始化保单信息异常！");
		return false;
	}
}

function goBack() {
	top.close();
}

function InsuredListUpload() {
	
	var pathQuerySql = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
	var arrResult = easyExecSql(pathQuerySql);
	
	var importPath = "";
	if(arrResult){
		importPath = arrResult[0][0];
	} else {
		alert("获取上传路径异常，文件上传失败！");
		return false;
	}
	
	var tprtno = fm.all('FileName').value;

	if (tprtno.indexOf("\\") > 0 ) {
		tprtno =tprtno.substring(tprtno.lastIndexOf("\\") + 1);
	}
    
	if (tprtno.indexOf("/") > 0 ) {
		tprtno =tprtno.substring(tprtno.lastIndexOf("/") + 1);
	}
	
	if ( tprtno.indexOf(".") > 0) {
  		tprtno = tprtno.substring(0, tprtno.indexOf("."));
	}
	
	var batchno = tprtno;
    
	if (tprtno.indexOf("_") > 0) {
		tprtno = tprtno.substring(0, tprtno.indexOf("_"));
	}
	
	if (PrtNo!=tprtno ) {
    	alert("文件名与印刷号不一致,请检查文件名!");
    	return false;
  	} else {
  		var checkSql = "select 1 from LCGrpSubInsuredImport where BatchNo='" + batchno + "' fetch first 1 rows only with ur";
  		var arrResult = easyExecSql(checkSql);
		if(arrResult){
			alert("导入被保人失败：该文件名已使用，请确实该文件是否已经上传！");
			return false;
		} else {
			fm.BatchNo.value = batchno;
			fm.InputInsured.value = "0";
			fm.InputErrorInsured.value = "0";
		}
  	}

    var showStr="正在上载数据……";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./LGrpInputInsuredSave.jsp?ImportPath=" + importPath + "&GrpContNo=" + GrpContNo + "&PrtNo=" + PrtNo;
    fm.submit(); //提交
}

function afterSubmit(flagStr, content) {

	showInfo.close();
	
	var urlStr = "";
	if (flagStr == "Fail") {             
		urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	} else { 
    	urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
   		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

function refreshInsured(){
	if(fm.BatchNo.value != null && fm.BatchNo.value != ""){
		var querySQL = "Select count(1) From Lcgrpsubinsuredimport Where Prtno = '" +  PrtNo + "' and BatchNo='" + fm.BatchNo.value + "' with ur";
		var arrResult = easyExecSql(querySQL);
		if(arrResult){
			fm.InputInsured.value = arrResult[0][0];
		}
	}
}