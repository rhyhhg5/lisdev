
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%!
	String handleFunction(HttpSession session, HttpServletRequest request) {
	String tPrtNo = request.getParameter("PrtNo");
	String tAction = request.getParameter("Action");
	String MissionID = request.getParameter("MissionID");
	String ActivityID = request.getParameter("ActivityID");
	String strOperation = "DELETE";
	TransferData tTransferData = new TransferData();
	 tTransferData.setNameAndValue("PrtNo", tPrtNo);	
	 tTransferData.setNameAndValue("Action", tAction);	

	GlobalInput globalInput = new GlobalInput();

	if( (GlobalInput)session.getValue("GI") == null )
	{
		return "网页超时或者是没有操作员信息，请重新登录";
	}
	else
	{
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
	}
	
  	GrpSignLockBL tGrpSignLockBL =new GrpSignLockBL();
  
	VData vData = new VData();
	vData.add(tPrtNo);
	vData.add(tAction);
	vData.add(tTransferData);
	vData.add(ActivityID);
	vData.add(globalInput);

	try {
		if( !tGrpSignLockBL.submitData(vData, strOperation) )
		{
	   		if ( tGrpSignLockBL.mErrors.needDealError() )
	   		{
	   			return tGrpSignLockBL.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		return "保存失败，但是没有详细的原因";
			}
		}

	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		return ex.getMessage();
	}
	return "";
}
%>
<%
String FlagStr = "";
String Content = "";

try {
	Content = handleFunction(session, request);

	if( Content.equals("") ) {
		FlagStr = "Succ";
		Content = "操作成功";
	} else {
		FlagStr = "Fail";
		Content = "操作失败";
	}
} catch (Exception ex) {
	ex.printStackTrace();
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

