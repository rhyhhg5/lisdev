//程序名称：
//程序功能：
//创建日期：2007-09-25
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


/**
 * 提交表单
 */
function submitForm()
{
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}


/**
 * 提交后操作,服务器数据返回后执行的操作
 */
function afterSubmit(FlagStr, content)
{
    window.focus();
    showInfo.close();
    
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
    showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    if (FlagStr == "Succ")
    {
        alert("Succ");
    }
}afterMissionTraceDelSubmit

function queryMissionTrace()
{
    if(!verifyInput2())
    {
        return false;
    }
    
    var tCondMissionID = "";
    var tTmpPrtNo = fm.PrtNo.value;
    if(tTmpPrtNo != "" && tTmpPrtNo != null)
    {
        tCondMissionID = ""
            + " and (lwm.MissionProp1 = '" + tTmpPrtNo + "' "
            + " or lwm.MissionProp2 = '" + tTmpPrtNo + "' "
            + " or lwm.MissionProp5 = '" + tTmpPrtNo + "') "
            ;
    }

    var tStrSql = ""
        + " select lwm.MissionId, lwm.SubMissionId, lwm.ActivityId, lwm.ActivityStatus, "
        + " varchar(lwm.MakeDate) || ' ' || varchar(lwm.MakeTime), "
        + " lwm.MissionProp1, lwm.MissionProp2, lwm.MissionProp3, "
        + " lwm.MissionProp4, lwm.MissionProp5, lwm.MissionProp6, "
        + " 'LBMission' "
        + " from LBMission lwm "
        + " where 1 = 1 "
        + tCondMissionID
        + getWherePart("lwm.MissionId", "MissionId")
        + getWherePart("lwm.ActivityId", "ActivityId")
        + getWherePart("lwm.MakeDate", "StartMakeDate", ">=")
        + getWherePart("lwm.MakeDate", "EndMakeDate", "<=")
        ;
    turnPage.queryModal(tStrSql, MissionGrid);
}

function delMissionTrace()
{
    var tSelNo = MissionGrid.getSelNo() - 1;

    if("-1" == tSelNo)
    {
        alert("请先选择一条保单信息！");
        return false;
    }
    
    if (confirm("您确实想删除该记录吗?"))
    {
        fm.btnDelMission.disabled = true;

        var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
        showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

        fm.action = "MissionQueryDeleteSave.jsp";
        fm.submit();
    }
    
}

function afterMissionTraceDelSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
        //initForm();
    }
    queryMissionTrace();
    fm.btnDelMission.disabled = false;
}

