<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    

<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
  
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initEstimateGrid();
	divPage.style.display="";    
	if(tManageCom.length <= 4){
		fm.all('ManageCom').value = tManageCom;
		var tSQL = "select name from ldcom where comcode = '"+tManageCom+"' ";
		var strQueryResult = easyExecSql(tSQL);
		fm.all('ManageComName').value = strQueryResult[0][0];
	}             
}

var EstimateGrid
function initEstimateGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="管理机构";         	  //列名
    iArray[1][1]="60px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="保单号";         	  //列名
    iArray[2][1]="60px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[3]=new Array();
    iArray[3][0]="项目名称";         	
    iArray[3][1]="120px";            	
    iArray[3][2]=200;            		 
    iArray[3][3]=0;              		 
    
    iArray[4]=new Array();
    iArray[4][0]="项目编码";         	  //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="投保机构名称";      //列名
    iArray[5][1]="120px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="预估赔付率（含结余返还）";      //列名
    iArray[6][1]="100px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="生效日期";      //列名
    iArray[7][1]="60px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="承保日期";      //列名
    iArray[8][1]="60px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    EstimateGrid = new MulLineEnter("fm", "EstimateGrid"); 
    //设置Grid属性
    EstimateGrid.mulLineCount = 0;
    EstimateGrid.displayTitle = 1;
    EstimateGrid.locked = 1;
    EstimateGrid.canSel = 1;
    EstimateGrid.canChk = 0;
    EstimateGrid.hiddenSubtraction = 1;
    EstimateGrid.hiddenPlus = 1;
   
    EstimateGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
