var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		} catch (ex) {
			showInfo = null;
		}
	}
}

function updateinfo(){
   if(!beforeSub()){
   return false;
   }
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmAction.value = "UPDATE";
    fm.action = "./HJAppntInputSave.jsp";
    fm.submit(); //提交
}
function beforeSub(){
     var selno=AppntGrid.getSelNo();
     if(selno==0){
      alert("请先选择一条被保人信息!");
      return;
      }
	//从mulline中取值
	if(fm.AppntName.value == null || fm.AppntName.value == "" || fm.AppntName.value == "null"){
		alert("投保人姓名不可以为空！");
		fm.AppntName.focus();
		return false;
	}
	
    if(fm.AppntSex.value == null || fm.AppntSex.value == "" || fm.AppntSex.value == "null"){
        alert("投保人性别不可以为空！");
        fm.AppntSex.focus();
        return false;
    }
    
    if(fm.AppntBirthday.value == null || fm.AppntBirthday.value == "" || fm.AppntBirthday.value == "null"){
        alert("投保人出生日期不可以为空！");
        fm.AppntBirthday.focus();
        return false;
    }
    
    if(fm.AppntIDType.value == null || fm.AppntIDType.value == "" || fm.AppntIDType.value == "null"){
        alert("投保人证件类型不可以为空！");
        fm.AppntIDType.focus();
        return false;
    }
    
    if(fm.AppntIDNo.value == null || fm.AppntIDNo.value == "" || fm.AppntIDNo.value == "null"){
        alert("投保人证件号码不可以为空！");
        fm.AppntIDNo.focus();
        return false;
    }
    
    if(fm.AppntIDType.value=="0"){
    if(checkIdCard(fm.AppntIDNo.value)){
    return false;
    }
    }
	
	
	return true;
}

function returnParentone() {
	top.close();
}

function queryInfo(){
	//if (verifyInput() == false) {
	//	return false;
	//}
	
	try
	{
		// 查询前需要调用grid的清空函数清空数据
		AppntGrid.clearData();
		var tSql = "select AppntName,(select codename from ldcode where codetype='sex' and code=AppntSex ) AppntSex,AppntBirthday,(select codename from ldcode where codetype='idtype' and code=AppntIdType ) AppntIdType,AppntIdNo,InsuredName,Sex,Birthday,IDType,IDNo,InsuredID,AppntSex,AppntIdType  from LCInsuredList where GrpContNo='"+tGrpcontno+"' ";
		tSql += getWherePart('AppntName','Name');
		tSql += getWherePart('AppntSex','Sex');
		tSql += getWherePart('AppntBirthday','Birthday');
        tSql += getWherePart('AppntIdType','IDType');
        tSql += getWherePart('AppntIdNo','IDNo');
        tSql += " union ";
        tSql += "select AppntName,(select codename from ldcode where codetype='sex' and code=AppntSex ) AppntSex,AppntBirthday,(select codename from ldcode where codetype='idtype' and code=AppntIdType ) AppntIdType,AppntIdNo,InsuredName,Sex,Birthday,IDType,IDNo,InsuredID,AppntSex,AppntIdType  from LBInsuredList where GrpContNo='"+tGrpcontno+"' ";
		tSql += getWherePart('AppntName','Name');
		tSql += getWherePart('AppntSex','Sex');
		tSql += getWherePart('AppntBirthday','Birthday');
        tSql += getWherePart('AppntIdType','IDType');
        tSql += getWherePart('AppntIdNo','IDNo');
		turnPage1.queryModal(tSql, AppntGrid);
	}
	catch (ex)
	{
		alert("查询出错！"+ex.message);
	}

}


function reportDetailClick(){
    var selno=AppntGrid.getSelNo();
     if(selno==0){
      alert("请先选择一条被保人信息!");
      return;
      }
    fm.InsuredId.value=AppntGrid.getRowColData(selno-1,11);
    fm.AppntName.value=AppntGrid.getRowColData(selno-1,1);
    fm.AppntSex.value=AppntGrid.getRowColData(selno-1,12);
    fm.AppntSexName.value=AppntGrid.getRowColData(selno-1,2);
    fm.AppntBirthday.value=AppntGrid.getRowColData(selno-1,3);
    fm.AppntIDType.value=AppntGrid.getRowColData(selno-1,13);
    fm.AppntIDTypeName.value=AppntGrid.getRowColData(selno-1,4);
    fm.AppntIDNo.value=AppntGrid.getRowColData(selno-1,5);
}




function afterSubmit(FlagStr,content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        queryInfo();
        fm.InsuredId.value="";
    fm.AppntName.value="";
    fm.AppntSex.value="";
    fm.AppntSexName.value="";
    fm.AppntBirthday.value="";
    fm.AppntIDType.value="";
    fm.AppntIDTypeName.value="";
    fm.AppntIDNo.value="";
	}
	
}
