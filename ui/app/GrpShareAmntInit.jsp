<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpShareAmntInit.jsp
//程序功能：
//创建日期：2011-12-22
//创建人  ：gzh
//更新记录：  更新人 更新日期    更新原因/内容
%>

<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
%> 
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox(){
  try{
	// 保单查询条件
	if(LoadFlag!="" && LoadFlag=="16"){
		divGrpPayPlanSave.style.display="none";
		document.getElementById("submitButtonDetail").style.display="none";
		GrpShareAmntPoint.style.display="none";
	}else{
		GrpShareAmntPoint.style.display="";
	}
  }
  catch(ex)
  {
    alert("GrpShareAmntInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!"+ex.message);
  }
}

// 下拉框的初始化
function initSelBox(){
  try{
  }
  catch(ex)
  {
    alert("GrpShareAmntInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm(){
  try{
	initInpBox();
    initGrpShareAmntGrid();
    initGrpShareAmntRiskGrid();
    
    initGrpSharenAmntRisk();
  }
  catch(re)
  {
    alert("GrpShareAmntInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

function initGrpShareAmntGrid(){
	var iArray = new Array();
	try{
	iArray[0]=new Array();
	iArray[0][0]="序号";
	iArray[0][1]="30px";
	iArray[0][2]=100;
	iArray[0][3]=0;
	//alert("not here");
    iArray[1]= new Array();
	iArray[1][0]="共用计划编码";
	iArray[1][1]="200px";
	iArray[1][2]=100;
	iArray[1][3]=0;
	
	iArray[2]=new Array();
	iArray[2][0]="共用计划名称";
	iArray[2][1]="250px";
	iArray[2][2]=100;
	iArray[2][3]=1;
	
	iArray[3]=new Array();
	iArray[3][0]="共用保险金额";
	iArray[3][1]="250px";
	iArray[3][2]=100;
	iArray[3][3]=1;
	
  	GrpShareAmntGrid = new MulLineEnter( "fm" , "GrpShareAmntGrid" );
     //这些属性必须在loadMulLine前
     GrpShareAmntGrid.mulLineCount = 1;
     GrpShareAmntGrid.displayTitle = 1;
     GrpShareAmntGrid.hiddenPlus =0;
     GrpShareAmntGrid.hiddenSubtraction = 0;
     GrpShareAmntGrid.canSel=1;

     //GrpShareAmntGrid.addEventFuncName = "addtestClick";
     //GrpShareAmntGrid.delEventFuncName = "testdelClick"; //该属性不好用
     GrpShareAmntGrid.selBoxEventFuncName = "ShowGrpShareAmntDetail"; 
     GrpShareAmntGrid.loadMulLine(iArray);
	}
	catch(ex) {
      alert(ex);
    }
}



function initGrpShareAmntRiskGrid(){
	
	var iArray = new Array();
	try{
	iArray[0]=new Array();
	iArray[0][0]="序号";
	iArray[0][1]="30px";
	iArray[0][2]=100;
	iArray[0][3]=0;
	
	iArray[1]= new Array();
	iArray[1][0]="险种代码";
	iArray[1][1]="120px";
	iArray[1][2]=100;
	iArray[1][3]=0;
	
	iArray[2]=new Array();
	iArray[2][0]="险种名称";
	iArray[2][1]="200px";
	iArray[2][2]=100;
	iArray[2][3]=0;
	
	
	  GrpShareAmntRiskGrid = new MulLineEnter( "fm" , "GrpShareAmntRiskGrid" );
      //这些属性必须在loadMulLine前
      GrpShareAmntRiskGrid.mulLineCount = 1;
      GrpShareAmntRiskGrid.displayTitle = 1;
      GrpShareAmntRiskGrid.hiddenPlus = 1;
      GrpShareAmntRiskGrid.hiddenSubtraction = 1;
      GrpShareAmntRiskGrid.canChk=1;
      GrpShareAmntRiskGrid.loadMulLine(iArray);
	}
	catch(ex) {
      alert(ex);
    }
}
</script>