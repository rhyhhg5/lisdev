<script language="JavaScript">
var loadFlag=<%=tLoadFlag%>;
//初始化输入框
function initInpBox() { 

}

// 下拉框的初始化
function initSelBox(){

}

//表单初始化
function initForm(){
  try{
  if(scantype=="scan")
  {
    setFocus();
  } 
  	fm.SaleChnl.value = "01";
	//modify by zxs
	fm.AppntAuth.value='1';
  	fm.PolApplyDate.value = "";
    fm.querybutton.disabled=true;   
    initImpartGrid();
    initMissionID();
    initULIRiskInput();
    if(this.ScanFlag == "0")
    {
     fm.all('PrtNo').value = prtNo;
     fm.all('ManageCom').value = ManageCom;
     fm.PrtNo.readOnly=true;   
     detailInit(prtNo); 
    }  
    if(this.ScanFlag == "1")
    {
     fm.all('PrtNo').value = prtNo;
     fm.all('ManageCom').value = ManageCom;
     fm.PrtNo.readOnly=true;
     detailInit(prtNo); 
     showAllCodeName();
     initshowCrs_BussTypeName();
    }
    initExtend();
    if(this.loadFlag == "1"){  
    
      AppntPostalAddressID.style.display="none";
      fm.all("Donextbutton28").style.display="none";
      detailInit(mSwitch.getVar( "PolNo" ));      
      fm.all('Remark').value=trim(fm.all('Remark').value);
      showAllCodeName();
      initshowCrs_BussTypeName();
      fm.all("RiskInfoButton1").style.display="none";
      if(this.DiskInputFlag == "1"){
			 fm.all('lrwb').style.display="none";
		 	} 
		 	controlAgentComDisplay("");
		 	getMedicalName();
      return;
    }
    
    //复合修改
    if(this.loadFlag=="3"){
			divButton.style.display="none";
			divInputContButton.style.display = "none";
			divApproveContButton.style.display = "none";
			divApproveModifyContButton.style.display = "";
			fm.all("RiskInfoButton1").style.display="none";
		      fm.all("Donextbutton27").style.display="none";
    }
    //新单复合  
    if (this.loadFlag == "5"){
    	fm.all("AppntAuth").verify="授权使用客户信息|code:Auth";
    	fm.all("AppntAuth").elementtype='';
    	fm.all("AppntAuthName").elementtype='';
    	
    	divButton.style.display="none";
		divInputContButton.style.display = "none";
		divApproveModifyContButton.style.display = "none";
      	divApproveContButton.style.display = "";
      	if(fm.all('ManageCom').value.length>=4){
      	   if(fm.all('ManageCom').value.substring(0,4)=='8644'){
      	      fm.all("AudioAndVideo").style.display="";
      	   }
      	   if(fm.all('ManageCom').value.substring(0,4)=='8691'){
      	      fm.all("DLXSInput1").style.display="";
      	      fm.all("DLXSInput2").style.display="";
      	   }
      	}
      	fm.PolApplyDate.readOnly=false;
      	fm.all("RiskInfoButton1").style.display="none";
      	fm.VideoFlag.value=tVideoFlag;
      	if(tXSFlag=="Y"){
      	  fm.XSFlag.value=tXSFlag;
      	  fm.XSFlagName.value="是";
      	}else if(tXSFlag=="N"){
      	  fm.XSFlag.value=tXSFlag;
      	  fm.XSFlagName.value="否";
      	}
      	
    }
  if (this.loadFlag == "25"){//人核保单明细进来的
	  
	  fm.all("AppntAuth").verify="授权使用客户信息|code:Auth";
  	  fm.all("AppntAuth").elementtype='';
  	  fm.all("AppntAuthName").elementtype='';
    		divButton.style.display="none";
				divInputContButton.style.display = "none";
				divApproveModifyContButton.style.display = "none";
      	divchangplan.style.display = "";
      	fm.PolApplyDate.readOnly=false;
      	fm.all("RiskInfoButton1").style.display="none";
//         fm.all("Donextbutton27").style.display="none";
    }

    if (this.loadFlag == "6"){
      divButton.style.display="none";
      fm.all("Donextbutton1").style.display="none";
      //fm.all("Donextbutton2").style.display="none";
      //fm.all("Donextbutton3").style.display="";
      //fm.all("Donextbutton5").style.display="none";
      //fm.all("butBack").style.display="none";           
      detailInit(prtNo); 
	
    }
    
  if(this.loadFlag == "99"){
       divInputContButton.style.display="none";
       autoMoveButton.style.display="";  
       fm.all("RiskInfoButton1").style.display="none";  
       fm.all("Donextbutton27").style.display="none";
 }   
  
    var tPolNo = top.opener.parent.VD.gVSwitch.getVar( "PolNo" );
      //详细信息初始化
    detailInit(tPolNo);
    controlAgentComDisplay("");
  
showAllCodeName();
initshowCrs_BussTypeName();
//显示医保机构编码名称
getMedicalName();
  }catch(ex){ }
}
// 告知信息列表的初始化


function initshowCrs_BussTypeName(){
	var tsql = "select codename('crs_busstype',Crs_BussType) from lccont where 1=1"
			 + " and PrtNo = '" + fm.PrtNo.value + "' ";
	var tarrResult = easyExecSql(tsql);
	var Crs_BussType="";
	if (tarrResult != null){
		fm.Crs_BussTypeName.value = tarrResult[0][0];
	}
}



function initImpartGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="告知版别";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="ImpartVer";
      iArray[1][9]="告知版别|len<=5";
      iArray[1][18]=300;

      iArray[2]=new Array();
      iArray[2][0]="告知编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ImpartCode";
      iArray[2][5]="2|3";
      iArray[2][6]="0|1";
      iArray[2][9]="告知编码|len<=4";
      iArray[2][15]="ImpartVer";
      iArray[2][17]="1";
      iArray[2][18]=700;

      iArray[3]=new Array();
      iArray[3][0]="告知内容";         		//列名
      iArray[3][1]="250px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="填写内容";         		//列名
      iArray[4][1]="150px";            		//列宽
      iArray[4][2]=150;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][9]="填写内容|len<=200";

//      iArray[5]=new Array();
//      iArray[5][0]="告知客户类型";         		//列名
//      iArray[5][1]="90px";            		//列宽
//      iArray[5][2]=90;            			//列最大值
//      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
//      iArray[5][4]="CustomerType";
//      iArray[5][9]="告知客户类型|len<=18";
//      
//      iArray[6]=new Array();
//      iArray[6][0]="告知客户号码";         		//列名
//      iArray[6][1]="90px";            		//列宽
//      iArray[6][2]=90;            			//列最大值
//      iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
//      iArray[6][4]="CustomerNo";
//      iArray[6][9]="告知客户号码";
//      iArray[6][15]="Cont";

      ImpartGrid = new MulLineEnter( "fm" , "ImpartGrid" ); 
      //这些属性必须在loadMulLine前
      ImpartGrid.mulLineCount = 1;   
      ImpartGrid.displayTitle = 1;
      //ImpartGrid.tableWidth   ="500px";
      ImpartGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ImpartGrid.setRowColData(1,1,"asdf");
    }
    catch(ex) {
      alert(ex);
    }
}

</script>