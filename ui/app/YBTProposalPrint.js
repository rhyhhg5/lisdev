//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  // showSubmitFrame(mDebug);
  initContGrid();

  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
    var tRow = ContGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = ContGrid.getRowData(tRow);
    
    var tContNo = tRowDatas[0];
    var tPrtNo = tRowDatas[1];
    var tCode = tRowDatas[8];

    if(tCode == null || tCode == "")
    {
        fm.action = "./YBTProposalPrintSave.jsp";
    }
    else
    {
    	fm.action = "../uw/PDFPrintSave.jsp?Code=" + tCode + "&OtherNo=" + tContNo;
    }

	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	fm.all("printButton").disabled=true;
	
	fm.submit();
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//无论打印结果如何，都重新激活打印按钮
  window.focus();
  try
  {
	  showInfo.close();
	}
	catch(ex)
	{}
	
	fm.all("printButton").disabled=false;
	if (FlagStr == "Fail" )
	{
		//如果打印失败，展现错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
	 
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在YBTProposal.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initContGrid();
    if(!verifyInput2())
        return false;
	
    if(fm.PrtNo.value == "" && fm.ContNo.value == "" && fm.startUWDate.value == "")
    {
        alert("印刷号、保单号和签单起始日期至少录入一个！");
        return false;
    }
    if(fm.PrtNo.value != "" || fm.ContNo.value != "")
    {
        fm.startUWDate.value = "";
        fm.endUWDate.value = "";
    }
    
	var strManageComWhere = " ";
	if( fm.BranchGroup.value != '' )
	{
		strManageComWhere += " AND EXISTS" + " ( SELECT AgentGroup FROM LABranchGroup WHERE AgentGroup=A.AgentGroup and BranchAttr LIKE '" + fm.BranchGroup.value + "%%') ";
	}
	
//	var tPrintCountCondition = " and (PrintCount < 1 or PrintCount is null ) and LostTimes > 0 ";
	// Modify by ZC At PM 19:51 Apr-11-2016 Monday
	var tPrintCountCondition = " and (PrintCount < 1 or PrintCount is null ) ";
	var tPrintState = "";

	//去掉了几个查询的筛选条件，有待以后考察是否需要保留
	var strSQL = "";
	strSQL = "select lcc.ContNo,lcc.PrtNo,lcc.Prem,lcc.AppntName,lcc.CValiDate,lcc.PrintCount, "
	    + "case when lcc.PrintCount > 0 then '已打印' else '未打印' end, CodeName('lcsalechnl', lcc.SaleChnl), "
        + " (select case when lcp.RiskCode = '330501' then 'J006' when lcp.RiskCode = '550806' then 'XB002' else '' end from LCPol lcp where lcp.ContNo = lcc.ContNo and lcp.RiskCode in ('330501','550806'))"
        + "from LCCont lcc where lcc.ContType = '1' and lcc.GrpContNo = '00000000000000000000' "
		+ "and lcc.CardFlag in ('9','a','c','d','e','f') "
		+ "and lcc.AppFlag in ( '1','9') "
		+ "and lcc.UWFlag not in ('a','1','8') "
		+ tPrintCountCondition
		//+ " and not exists (select 1 from LCPol a, LMRiskApp b where a.ContNo = LCCont.ContNo and a.RiskCode = b.RiskCode and b.RiskType4 = '4') "
		+ getWherePart('lcc.ContNo', 'ContNo')
		+ getWherePart('lcc.PrtNo', 'PrtNo')
		+ getWherePart('getUniteCode(lcc.AgentCode)', 'AgentCode')
		+ getWherePart('lcc.SaleChnl', 'SaleChnl')
		+ getWherePart('lcc.ManageCom', 'ManageCom', 'like')
        + getWherePart('lcc.UWDate','startUWDate','>=')
        + getWherePart('lcc.UWDate','endUWDate','<=')
		+ strManageComWhere
        //+ " and exists (select 1 from LDCode1 where CodeType = 'printchannel' and Code = substr(ManageCom, 1, 4) and Code1 = ContType and CodeName = '" + printChannelType + "') "
		+ "and lcc.ManageCom like '" + comcode + "%%' "
		+ "order by lcc.ManageCom,lcc.AgentGroup,lcc.AgentCode "
		+ "with ur ";

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}

	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	turnPage.pageLineNum = 30 ;
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = ContGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL ;
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function queryBranch()
{
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
	if(arrResult!=null)
	{
		fm.BranchGroup.value = arrResult[0][3];
	}
}

//下载客户卡
function downloadCustomerCard(suffix)
{
  var count = 0;
  for(var i = 0; i < ContGrid.mulLineCount; i++)
  {
    if(ContGrid.getChkNo(i))
    {
      count++;
    }
  }
  if(count == 0)
  {
    alert("请选择保单");
    return false;
  }
  
  var fmAction = fm.action;
  fm.action = "../intlapp/DownloadCustCardSave.jsp?Suffix=" + suffix;
  fm.submit();
}

function printPolpdfNew()
{
    var tRow = ContGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }

    var tRowDatas = ContGrid.getRowData(tRow);
    
    var tContNo = tRowDatas[0];
    var tPrtNo = tRowDatas[1];
    var tCode = tRowDatas[8];
    
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    //fm.fmtransact.value = "PRINT";
    fm.target = "fraSubmit";
    fm.action = "../uw/PDFPrintSave.jsp?Code=" + tCode + "&OtherNo=" + tContNo;
    fm.submit();
    
    return true;
}

function afterSubmit2(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("打印失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    //fm.btnPrintPdf.disabled = false;
}