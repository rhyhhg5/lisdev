<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：
//程序功能：
//创建日期：2009-08-13
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="FirstTrialWorkload1Day.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();">    
<form action="" method=post name=fm target="fraSubmit">

    <table class="common">
        <tr class="common">
            <td class="title">管理机构</td>
            <td class="input">
                <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
            </td>
            <td class="title">单证下发日期起</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="StartDate" verify="扫描日期起|date&notnull" elementtype="nacessary" />
            </td>
            <td class="title">单证下发日期止</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="EndDate" verify="扫描日期止|date&notnull" elementtype="nacessary" />
            </td>
        </tr>            
    </table>
    
    <br />
    
    <input value="下  载" class="cssButton" type="button" onclick="submitForm();" />
    
    <hr />
    
    <div>
        <font color="red">
            <ul>报表说明：
                <li>本报表用于个人保单统计各种下发单证的数据统计。</li>
                <li>日期查询口径：以单证下发时间为统计口径。</li>
                <li>由于系统中初审人员填写不统一，且初审时间不填，因此，原统计字段“初审人员”、“初审人员名称”暂不进行统计。</li>
            </ul>
            <ul>名词解释：
                <li>填单问题件数：下发后的每一个问题件通知书视为一件。</li>
            </ul>
            <ul>统计条件：
                <li>管理机构、扫描日期段。</li>
                <li>扫描日期段间隔不超过三个月。</li>
            </ul>
        </font>
    </div>
    
    <input name="querySql" type="hidden" />
    <input type="hidden" name=op value="" />

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 