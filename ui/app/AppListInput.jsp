<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="java.util.*"%>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>

<script>
  var managecom = <%=tGI.ManageCom%>;
</script> 
	<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<%@include file="./AppListInit.jsp"%>
		<SCRIPT src="./AppListInput.js"></SCRIPT>

		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

	</head>
	<body onload="initForm();initElementtype();">
		<form action="./AppListSave.jsp" method=post name=fm
			target="PRINT">
			<table>
				<tr>
					<td class="titleImg">
						可疑客户资料清单
					</td>
				</tr>
			</table>
			<br>
			<TABLE class="common">
				<tr class=common>
					<td class=title>
						管理机构
					</td>
					<td class=input colspan='3'>
						<Input class=codeNo name=ManageCom verify="管理机构|notnull"
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true elementtype="nacessary">
					</td>
				</tr>
				<tr class="common">
					<td class="title">
						签单日期起期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="SignStartDate" verify="签单日期起期|date">
					<td class="title">
						签单日期止期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="SignEndDate" verify="签单日期止期|date">
					</td>
				</tr>
				<tr class="common">
					<td class="title">
						生效日期起期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="CVStartDate" verify="生效日期起期|date">
					<td class="title">
						生效日期止期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="CVEndDate" verify="生效日期止期|date">
					</td>
				</tr>
				<tr class="common">
					<td class="title">
						回执回销日期起期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="CusStartDate" verify="回执回销日期起期|date">
					<td class="title">
						回执回销日期止期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="CusEndDate" verify="回执回销日期止期|date">
					</td>
				</tr>
			</TABLE><br>
			
			<input value="电话清单下载" type=button onclick="Print(1)" class="cssButton"
				type="button">
			<input value="地址清单下载" type=button onclick="Print(2)" class="cssButton"
				type="button">
			<input type="hidden" value="" name="downloadtype">
			<br><br>
			<font color="red">签单日期、生效日期、回执回销日期至少录入一组。</font><br>
			<font color="red">至少需要有一组日期，查询时间范围小于三个月。</font>
			
			<input type="hidden" name="fmtransact" value="">
		</form>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</body>
</html>
