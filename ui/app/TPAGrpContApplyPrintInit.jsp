<%
//程序名称：
//程序功能：
//创建日期：2009-2-26
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;

String mRePrintFlag = request.getParameter("RePrintFlag");
%>

<script language="JavaScript">

var mRePrintFlag = "<%=mRePrintFlag%>";

function initForm()
{
    try
    {
        //initTPAGrpContPrintGrid();
        
        fm.all('ManageCom').value = <%=strManageCom%>;

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initTPAGrpContPrintGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="证号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="结算单号";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="集体投保单号";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=3;
        
        iArray[4]=new Array();
        iArray[4][0]="团体合同号";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=3;
        
        iArray[5]=new Array();
        iArray[5][0]="中介机构";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="业务员代码";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="业务员姓名";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="保费";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="缴费方式";
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=0;
        
        iArray[10]=new Array();
        iArray[10][0]="是否打印";
        iArray[10][1]="80px";
        iArray[10][2]=100;
        iArray[10][3]=0;
        
        iArray[11]=new Array();
        iArray[11][0]="通知书类型";
        iArray[11][1]="80px";
        iArray[11][2]=100;
        iArray[11][3]=3;


        TPAGrpContPrintGrid = new MulLineEnter("fm", "TPAGrpContPrintGrid");

        TPAGrpContPrintGrid.mulLineCount = 0;
        TPAGrpContPrintGrid.displayTitle = 1;
        TPAGrpContPrintGrid.canSel = 1;
        TPAGrpContPrintGrid.hiddenSubtraction = 1;
        TPAGrpContPrintGrid.hiddenPlus = 1;
        TPAGrpContPrintGrid.canChk = 0;
        TPAGrpContPrintGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化TPAGrpContPrintGrid时出错：" + ex);
    }
}
</script>

