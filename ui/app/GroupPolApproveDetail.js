//程序名称：GroupPolApproveDetail.js
//程序功能：集体新单复核
//创建日期：2002-08-12 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var k = 0;
var cflag = "5";  //问题件操作位置 1.新单复核
var mSwitch = parent.VD.gVSwitch;
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function aftersubmit(FlagStr,content)
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    alert(content);
  }
  else
  { 

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
    alert(content);
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           


/*********************************************************************
 *  查询团体下的附属于各团单的个人保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if(cProposalNo==null||cProposalNo=="")
  {
  	showInfo.close();
  	alert("请选择个人保单,后查看其信息!");
  	
  }
  else{
  mSwitch.deleteVar( "PolNo" );
  mSwitch.addVar( "PolNo", "", cProposalNo );
  window.open("./ProposalMain.jsp?LoadFlag=4");
  //window.open("../app/ProposalDisplay.jsp?PolNo="+cProposalNo,"window1");
  showInfo.close();}
}
                    

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fmQuery.submit(); //提交
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//Click事件，当点击“责任信息”按钮时触发该函数
function showDuty()
{
  //下面增加相应的代码
  showModalDialog("./ProposalDuty.jsp",window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=13cm");
  
}           

//Click事件，当点击“暂交费信息”按钮时触发该函数
function showFee()
{
  //下面增加相应的代码
  showModalDialog("./ProposalFee.jsp",window,"status:no;help:0;close:0;dialogWidth=16cm;dialogHeight=8cm");
  
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


/*********************************************************************
 *  查询团体下的团体主附团体单(一个主险单,零个或零个以上附加险团体单)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function querygrp()
{
	// 初始化表格                                                                                                                                                           
	//initInpBox();
    initPolBox();
	initPolGrid();
	initGrpGrid();
	showDiv(divPerson,"false");
	
	// 书写SQL语句
	var str = "";
	var PrtNo = fmQuery.all('PrtNoHide').value;
	var mOperate = fmQuery.all('Operator').value;
	//alert("grp:"+GrpProposalNo);	
	
	var strsql = "";
	strsql = "select LCGrpPol.GrpProposalNo,LCGrpPol.prtNo,LCGrpPol.GrpName,LCGrpPol.RiskCode,LCGrpPol.RiskVersion,LCGrpPol.ManageCom from LCGrpPol where 1=1 "
				 + " and LCGrpPol.PrtNo = '"+PrtNo+"'"
				 + "and LCGrpPol.AppFlag='0' "
				 + "and LCGrpPol.ApproveFlag = '0' " //为null 或 0
				 + "and LCGrpPol.ContNo='00000000000000000000' "
				 + "order by LCGrpPol.makedate,lcgrppol.maketime";
	//alert(strsql);
    //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件集体单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

/*********************************************************************
 *  查询个人单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPol()
{
	// 初始化表格  
	showDiv(divPerson,"false");                                                                                                                                                        
	initPolBox();
	initPolGrid();                                                                                                                                                         
	// 书写SQL语句
	
	var GrpProposalNo = fmQuery.all('GrpProposalNo').value;
	var mOperate = fmQuery.all('Operator').value;
	//alert("grp:"+GrpProposalNo);
	
	var strsql = "";
	strsql = "select LCPol.ProposalNo,LCPol.AppntName,LCPol.RiskCode,LCPol.RiskVersion,LCPol.InsuredName,LCPol.ManageCom from LCPol where 1=1 "
				 + "and LCPol.AppFlag='0' "
				 + "and LCPol.ApproveFlag = '0' "  //为null 或 0
				 + " and LCPol.GrpPolNo = '"+GrpProposalNo+"'"	
				 + " order by lcpol.makedate,lcpol.maketime";			 
    //alert(strsql);
   //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);  
   //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("此单下个人单均已复核通过！");
    return "";
  }
  
  showDiv(divPerson,"true");
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


/*********************************************************************
 *  查询集体下个人单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getGrpPolGridCho()
{
	//setproposalno();
	fmQuery.PolTypeHide.value = '2';
	fmQuery.submit();
}



/*********************************************************************
 *  选择要复核的保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolGridCho()
{
	//setproposalno();
	fmQuery.PolTypeHide.value = '1';
	fmQuery.submit();
}







/*********************************************************************
 *  选中团单下个单问题件的录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestInput()
{
	cGrpProposalNo = fmQuery.GrpMainProposalNo.value;  //团体主险保单号码
	cProposalNo = fm.ProposalNo.value;
	
	if(cProposalNo == "")
	{
		alert("请选择一条个人单记录！");
	}
	else
	{
		window.open("./GrpQuestInputMain.jsp?GrpProposalNo="+cProposalNo+"&ProposalNo="+cProposalNo+"&Flag="+cflag,"window1");
	}
}

/*********************************************************************
 *  选中团单问题件的录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpQuestInput()
{
	//cGrpProposalNo = fmQuery.GrpMainProposalNo.value;  //团体主险保单号码	
	//cProposalNo = fm.ProposalNo.value;
	var cGrpProposalNo = fmQuery.GrpProposalNo.value;  //团体保单号码	
	if(cGrpProposalNo==""||cGrpProposalNo==null)
	{
  		alert("请先选择一个团体主险投保单!");
  		return ;
    }
    if(cGrpProposalNo != fmQuery.GrpMainProposalNo.value)
	{
  		alert("请先选择一个团体主险投保单!");
  		return ;
    }
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./GrpQuestInputMain.jsp?GrpProposalNo="+cGrpProposalNo+"&ProposalNo="+cGrpProposalNo+"&Flag="+cflag,"window2");
	
}

/*********************************************************************
 *  选中团单下个单问题件的查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestQuery()
{
	
	cProposalNo = fm.ProposalNo.value;//个单投保单号
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("../uw/QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	
	
}
/*********************************************************************
 *  选中团单问题件的查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function GrpQuestQuery()
{
	var cGrpProposalNo = fmQuery.GrpProposalNo.value;  //团单投保单号码
	if(cGrpProposalNo==""||cGrpProposalNo==null)
	{
  		alert("请先选择一个团体主险投保单!");
  		return ;
    }
	if(cGrpProposalNo!=fmQuery.GrpMainProposalNo.value)
	{
  		alert("请先选择一个团体主险投保单!");
  		return ;
    }	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("../uw/QuestQueryMain.jsp?ProposalNo1="+cGrpProposalNo+"&Flag="+cflag);
	
	
}

/*********************************************************************
 *  复核通过该团单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function gmanuchk()
{
	
	cGrpProposalNo = fmQuery.GrpProposalNo.value;  //团单投保单号码
	cflag="5";
	
	if( cGrpProposalNo == null || cGrpProposalNo == "" )
		alert("请选择集体主险投保单后，再进行复核操作");
	else
	{
		var i = 0;
		//var showStr="正在复核集体投保单，请您稍候并且不要修改屏幕上的值或链接其他页面gmanuchk()"+cGrpProposalNo;
		//var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		  window.open("./GroupPolApproveSave.jsp?GrpProposalNo1="+cGrpProposalNo+"&Flag1=5","windows1");
	    //window.close();
	    //fm.submit(); //提交
	}

}

/*********************************************************************
 *  点击取消按钮,重新初始化当前页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelchk()
{
	fm.all('UWState').value = "";
	fm.all('UWIdea').value = "";
	fm.all('GUWState').value = "";
	fm.all('GUWIdea').value = "";	
}

/*********************************************************************
 *  点击返回按钮,关闭当前页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function goback()
{
  top.opener.easyQueryClick();
  top.close();	
}

/*********************************************************************
 *  点击扫描件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function ScanQuery2()
{
	var arrReturn = new Array();
	var tSel = GrpGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var prtNo = GrpGrid.getRowColData(tSel - 1,2);				

		
		if (prtNo == "")
		    return;

		 window.open("../sys/ProposalEasyScan.jsp?prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");								
	}	     
}


/*function temp()
{
	alert("此功能尚缺！");
}*/
