//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var mCodeName;

//提交，保存按钮对应操作
function submitForm()
{
	if(!verifyInput2())
	return false;
	var tStartDate = fm.StartDate.value;
	var tEndDate = fm.EndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>3)
	{
		alert("统计期最多为三个月！");
		return false;
	}
	getQuerySQL();
//	fm.filename.value = '扫描工作量统计表_';
    fm.action = "ScanStatisticDownload.jsp";
//    fm.target = "_blank";
    
    fm.submit();
}

/**
 * 获取查询SQL
// */
function getQuerySQL(){
	var comcode = fm.ManageCom.value;
	var StartDate = fm.StartDate.value;
	var EndDate = fm.EndDate.value;
	var QYType_ch = fm.QYType_ch.value;
	var ScanOperator = fm.ScanOperator.value;
	var QYType = fm.QYType.value;
	QYType1 = "1=1";
			if(QYType =="1")
				 {
						QYType1=" SubType='TB01' and makedate between '"+StartDate+"' and '"+EndDate+"'";
					}
//			
			if(QYType =="2")
				 {
						QYType1=" SubType='TB04' and makedate between '"+StartDate+"' and '"+EndDate+"'";
					}
			if(QYType == "3")
			 {
					QYType1=" SubType='TB02' and makedate between '"+StartDate+"' and '"+EndDate+"'";
				}

		if(QYType == "4")
			 {
					QYType1=" SubType='TB22' and makedate between '"+StartDate+"' and '"+EndDate+"'";
				}
		
		if(QYType == "5")
			 {
					QYType1=" SubType = 'TB15' and makedate between '"+StartDate+"' and '"+EndDate+"'";
				}
		if(QYType == "6")
			 {
					QYType1=" SubType = 'TB16' and makedate between '"+StartDate+"' and '"+EndDate+"'";
				}		
		if(QYType == "7")
			 {
					QYType1=" SubType = 'TB21' and makedate between '"+StartDate+"' and '"+EndDate+"'";
				}	
		if(QYType == "8")
			 {
					QYType1=" SubType like 'TB%' and makedate between '"+StartDate+"' and '"+EndDate+"'";
				}
		
//    if (comcode == '86')) {
//		var querySQL = "select substr(managecom,1,length('" + comcode + "')*2),ScanOperator,'"+QYType_ch+"',count(DocID),sum(NumPages) " +
//				" from ES_DOC_MAIN where  "
//				+QYType
//				" and substr(managecom,1,length('" + comcode + "')*2) in (select substr(ComCode,1,4) from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode) " 
////				+ getWherePart('scanoperator', 'ScanOperator')
//	//			+" and scanoperator= " +ScanOperator 
//				" group by ScanOperator,SubType with ur";
//    }
	var querySQL = "";
	var totalSQL = "";
	if(comcode == '86'){
		querySQL = "select substr(managecom,1,length('" + comcode + "')*2),ScanOperator,'"+QYType_ch+"',count(DocID),sum(NumPages) " 
		+ " from ES_DOC_MAIN where  "
		+ QYType1
		+ " and substr(managecom,1,length('" + comcode + "')*2) in (select substr(ComCode,1,4) from  LDCom where sign='1' and length(trim(Comcode))=8 order by ComCode) " 
		+ getWherePart('scanoperator', 'ScanOperator')
////			+" and scanoperator= " +ScanOperator 
		+" group by ScanOperator,SubType,managecom ";
		
		totalSQL += "select '总计',ScanOperator,'" + QYType_ch +
                               "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                               QYType1 + "" 
                               + getWherePart('scanoperator', 'ScanOperator')
                               + " group by ScanOperator,SubType";
	}else if(comcode.length == 4){
		querySQL = "select substr(managecom,1,length('" + comcode + "')*2), ScanOperator,'" +
			        QYType_ch +
			        "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
			        QYType1 + " and substr(managecom,1,length('" + comcode + "')*2) in (select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +comcode + "' order by ComCode)" 
			        + getWherePart('scanoperator', 'ScanOperator')
			        +" group by ScanOperator,SubType,managecom"
			        
		totalSQL += "select '总计',ScanOperator,'" + QYType_ch +
                    "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
                    QYType1
                    + " and substr(managecom,1,length('" + comcode + "')*2) in (select ComCode from  LDCom where sign='1' and length(trim(Comcode))=8 and substr(comcode,1,4)='" +comcode + "' order by ComCode)" 
                    + getWherePart('scanoperator', 'ScanOperator')
                    + " group by ScanOperator,SubType";
	}else if(comcode.length == 8){
		querySQL = "select '" + comcode + "', ScanOperator,'" +
			        QYType_ch +
			        "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
			        QYType1 + " and substr(managecom,1,length('" +
			        comcode + "'))='" + comcode + "' " 
			        + getWherePart('scanoperator', 'ScanOperator')
			        +" group by ScanOperator,SubType";
		
		totalSQL += " select '总计',ScanOperator,'" + QYType_ch +
			        "',count(DocID),sum(NumPages) from ES_DOC_MAIN where " +
			        QYType1 + " and substr(managecom,1,length('" +
			        comcode + "'))='" + comcode + "' " 
			        + getWherePart('scanoperator', 'ScanOperator')
			        + " group by ScanOperator,SubType";
	}
    fm.querySql.value = querySQL;
    fm.totalSql.value = totalSQL;
}
