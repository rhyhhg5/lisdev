var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var i = 0;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>12)
	{
		alert("统计期最多为12个月！");
		return false;
	}
    var tManageCom = fm.ManageCom.value;  
    var tSaleChnl = fm.SaleChnl.value;
    if (tSaleChnl==""){
        fm.op.vale ="";      
  	}else{
        fm.op.vale ="and a.salechnl = '"+tSaleChnl+"'";    		
  	}  
    var strSQL = "select a.managecom,a.prtno,a.ContNo,a.riskcode,(select riskname from lmrisk where riskcode=a.riskcode),a.getstartdate,a.enddate,a.prem, "
             + "a.Amnt,a.mult,a.signdate,(select codename from ldcode where codetype='lcsalechnl' and code=a.salechnl),getUniteCode(a.agentcode), "
             + "(select name from laagent where agentcode =a.agentcode),b.branchattr,b.name from lcpol a,labranchgroup b "
             + "where a.conttype='1' and a.renewcount=0 "
             + "and a.signdate between '" + tStartDate + "' and '" + tEndDate + "' "
             + fm.op.vale + " and a.managecom like '" + tManageCom + "%' and a.agentgroup = b.agentgroup order by a.managecom,b.branchattr,a.agentcode with ur"
    
    fm.AnalysisSql.value = strSQL; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    
	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}  
	fm.submit();
}