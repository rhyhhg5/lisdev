//程序名称：
//程序功能：
//创建日期：2007-09-25
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


/**
 * 提交表单
 */
function submitForm()
{
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}


/**
 * 提交后操作,服务器数据返回后执行的操作
 */
function afterSubmit(FlagStr, content)
{
    window.focus();
    showInfo.close();
    
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
    showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    if (FlagStr == "Succ")
    {
        alert("Succ");
    }
}afterMissionTraceDelSubmit

function queryMissionTrace()
{

    if(!verifyInput2())
    {
        return false;
    }
    var tCondMissionID = "";
    var tPrtNo = fm.PrtNo.value;
    
	if (fm.Contno.value == "" && fm.PrtNo.value == "") {// 至少有一个是不为空的
		alert("至少需要印刷号或合同号中的一个！");
		return false;
	}

    if(tPrtNo != "" && tPrtNo != null)
    {
        tCondMissionID = ""
            + " and (lj.tempfeeno = '" + tPrtNo + "' "
            + " or lj.tempfeeno = '" + tPrtNo +"801" + "') "
            ;
    }

    var tStrSql = ""
        + " select lj.tempfeeno,lj.Otherno, "
        + " (select prtno from lccont where contno = lj.otherno), "
        + " codename('paymode',(select paymode from ljtempfeeclass where tempfeeno = lj.tempfeeno)), "
        + " (select paymoney from ljtempfeeclass where tempfeeno = lj.tempfeeno),lj.paydate,lj.managecom,lj.appntname,lj.makedate,lj.maketime "
        + " from ljtempfee lj "
        + " where 1 = 1 "
        + tCondMissionID
        + getWherePart("lj.Otherno", "Contno")
        + " fetch first 1 rows only "
        ;
    turnPage.queryModal(tStrSql, MissionGrid);
	fm.FinancialAction.value = "None";
	var arrResult = easyExecSql(tStrSql);
	if(arrResult == null)
	{
		alert("没有查到保单暂收信息！");
		return false;
	}
}

//暂收数据删除
function delMissionTrace()
{
    var tSelNo = MissionGrid.getSelNo() - 1;

    if("-1" == tSelNo)
    {
        alert("请先选择一条保单信息！");
        return false;
    }
    
    if (confirm("您确实想删除该记录吗?"))
    {
        fm.btnDelMission.disabled = true;
    	fm.FinancialAction.value = "doDelete";
        var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
        showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.action = "./DeleLJTempSave.jsp";
        fm.submit();
    }
    
}

//进行财务充负
function negativeRecoil()
{
    var tSelNo = MissionGrid.getSelNo() - 1;

    if("-1" == tSelNo)
    {
        alert("请先选择一条保单信息！");
        return false;
    }
	
	fm.FinancialAction.value = "doNegative";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./DeleLJTempSave.jsp";
	fm.submit();
}

//进行财务充正
function positiveRecoil()
{
    var tSelNo = MissionGrid.getSelNo() - 1;

    if("-1" == tSelNo)
    {
        alert("请先选择一条保单信息！");
        return false;
    }
	fm.FinancialAction.value = "doPositive";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./DeleLJTempSave.jsp";
	fm.submit();
}

function afterMissionTraceDelSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
        //initForm();
    }
    queryMissionTrace();
    fm.btnDelMission.disabled = false;
}
