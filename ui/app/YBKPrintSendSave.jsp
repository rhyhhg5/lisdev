<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalPrintSave.jsp
//程序功能：
//创建日期：2002-11-26
//创建人  ：Kevin
//修改人  ：朱向峰
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
CErrors tError = null;
String FlagStr = "";
String Content = "";
String tOperate = "";
//获得mutline中的数据信息
int nIndex = 0;
String tLCContGrids[] = request.getParameterValues("ContGridNo");
System.out.println("tLCContGrids[] = " + tLCContGrids[0]);
String tContNo[] = request.getParameterValues("ContGrid1");
String tPrtNo[] = request.getParameterValues("ContGrid2");
System.out.println("tPrtNo[] = " + tPrtNo[0]);

//获得session中的人员喜讯你
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//String sql = "";
//sql = "select riskcode from lcpol where contno = '" + tContNo[0]
//	+"' with ur";

//操作对象及容器
LCContSchema tLCContSchema = new LCContSchema();
YBKPrintSendUI YBKPrintSendUI = new YBKPrintSendUI();
System.out.println("合同号为: " + tContNo[0]);

//判断该合同的险种是否为医保险种,若为医保险种则继续,否则结束
String sql = "select riskcode from lcpol where contno='" + tContNo[0] + "'";
SSRS riskCodeSSRS = new ExeSQL().execSQL(sql);
String risk = riskCodeSSRS.GetText(1, 1);
System.out.println("================" + risk);

if(risk.equals("123202") || risk.equals("220602") || risk.equals("123201") || risk.equals("220601")){
	VData tVData = new VData();
	tVData.add(tG);
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("ContNo",tContNo[0]);
	tVData.add(tTransferData);
	
	if(!YBKPrintSendUI.submitData(tVData)){
		FlagStr = "Fail";
		tError = YBKPrintSendUI.mErrors;
		Content = tError.getFirstError();
	}else {
		FlagStr = "Fail";
		Content = "发送成功!!!";
	}
}else{
	System.out.println("该险种为非医保险种!!!");
	FlagStr = "......";
	Content = "该险种为非医保险种!!!";
	
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>