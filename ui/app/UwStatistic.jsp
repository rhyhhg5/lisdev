<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：UwStatistic.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="UwStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./UwStatisticSub.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
					<TD  class= title> 管理机构</TD>
					<TD  class= input><Input class= "codeno"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
          <TD  class= title>统计日期</TD>
          <TD  class= input> <Input name=MakeDate class='common' value="<%=PubFun.getCurrentDate()%>" dateFormat='short' readonly =ture> </TD> 
        </TR>
    </table>

    <input type="hidden" name=op value=""><input name="querySql" type="hidden" />
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		<hr>
	
	    <div>
        <font color="red">
            <ul>查询字段说明：
                <li>统计日期：核保完成时间（默认为统计当日，且不可选）</li>                
            </ul>
            <ul>统计结果说明：
                <li>核保审核完毕件数：统计日期中，统计机构核保审核完毕件数</li>
                <li>尚未审核完毕件数：统计机构尚未核保完毕件数</li>
            </ul>
        </font>
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>