//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var spanObj;
var mDebug = "100";
var mOperate = 0;
var mAction = "";
var arrResult = new Array();
var mShowCustomerDetail = "PROPOSAL";
var mCurOperateFlag = ""	// "1--录入，"2"--查询
var mGrpFlag = ""; 	//个人集体标志,"0"表示个人,"1"表示集体.

window.onfocus = myonfocus;

/*********************************************************************
 *  选择险种后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) {
	try	{
		if( cCodeName == "RiskInd" || cCodeName == "RiskGrp" || cCodeName == "RiskCode") {
			getRiskInput(Field.value, loadFlag);//loadFlag在页面出始化的时候声明
		}
		//将扫描件图片翻到第一页
		try { goToPic(0);	}	catch(ex2) {} 		
	}
	catch( ex ) {
	}
}

/*********************************************************************
 *  根据LoadFlag设置一些Flag参数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function convertFlag( cFlag )
{
  //alert("cFlag:" + cFlag);
	if( cFlag == "1" )		// 个人投保单直接录入
	{
		mCurOperateFlag = "1";
		mGrpFlag = "0";
	}
	if( cFlag == "2" )		// 集体下个人投保单录入
	{
		mCurOperateFlag = "1";
		mGrpFlag = "1";
	}
	if( cFlag == "3" )		// 个人投保单明细查询
	{
		mCurOperateFlag = "2";
		mGrpFlag = "0";
	}
	if( cFlag == "4" )		// 集体下个人投保单明细查询
	{
		mCurOperateFlag = "2";
		mGrpFlag = "1";
	}
	if( cFlag == "5" )		// 个人投保单复核查询
	{
		mCurOperateFlag = "2";
		mGrpFlag = "3";
	}
}

/*********************************************************************
 *  根据不同的险种,读取不同的代码
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getRiskInput( cRiskCode, cFlag ) {
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "";
	var tPolNo = "";

	convertFlag( cFlag );

	if( mGrpFlag == "0" )		// 个人投保单
		urlStr = "../riskinput/Risk" + cRiskCode + ".jsp";  
	if( mGrpFlag == "1" )		// 集体下个人投保单
		urlStr = "../riskgrp/Risk" + cRiskCode + ".jsp";  
	if( mGrpFlag == "3" )		// 个人投保单复核
		//urlStr = "../riskinput/RiskAll.jsp"; 
		urlStr = "../riskinput/Risk" + cRiskCode + ".jsp";

	//读取险种的界面描述:模态对话框（传递要打开的新的页面，当前窗口句柄，新窗口的设置）
	showInfo = window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;dialogTop:-800;dialogLeft:-800");
    //读取险种的界面描述:非模态对话框（传递要打开的新的页面，""，新窗口的设置）     
    //showInfo = window.open(urlStr,"","status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;dialogTop:-800;dialogLeft:-800");
	
  showDiv(inputButton, "true"); 
  
	//出始化公共信息
	emptyForm();

	fm.all("RiskCode").value = cRiskCode;
	//fm.all("PrtNo").value = prtNo;
	  
	//setFocus(prtNo);
	//fm.all("ManageCom").focus();
	fm.all("PrtNo").focus();
	fm.all("PrtNo").readOnly = false;
	fm.all("PrtNo").className = "common";

	try	{
		showInfo.close();
	}	catch(ex){}

	if( mCurOperateFlag == "1" ) {             // 录入
		if( mGrpFlag == "1" )	{                  // 集体下个人投保单			
			getGrpPolInfo();                       // 带入部分集体信息
		}
		
		if( isSubRisk( cRiskCode ) == true ) {   // 附险
			tPolNo = getMainRiskNo( cRiskCode );   //弹出录入附险的窗口,得到主险保单号码

			try	{                                  //出始化附险信息
				if (!initPrivateRiskInfo( tPolNo )) {
				  //if (loadFlag == "2") top.fraInterface.window.location = "";
				  //else 
				  //{
				    var strRefresh = "./ProposalInput.jsp";
				    //?LoadFlag=" + loadFlag + "&prtNo=" + prtNo;
				    top.fraInterface.window.location = strRefresh;
				  //}
				}
			}	catch(ex1) {
				alert( "初始化险种出错" + ex1 );
			}
		} // end of 附险if
		return false;
		
		showInfo = null;
	} // end of 录入if
	
	mCurOperateFlag = "";
	mGrpFlag = "";
}

/*********************************************************************
 *  判断该险种是否是附险,在这里确定既可以做主险,又可以做附险的代码
 *  参数  ：  险种代码
 *  返回值：  无
 *********************************************************************
 */
function isSubRisk(cRiskCode) {
  var arrQueryResult = easyExecSql("select SubRiskFlag from LMRiskApp where RiskCode='" + cRiskCode + "'", 1, 0);

	if(arrQueryResult[0] == "S")    //需要转成大写
		return true;
	if(arrQueryResult[0] == "M")
		return false;

	if (arrQueryResult[0].toUpperCase() == "A")
		if (confirm("该险种既可以是主险,又可以是附险!选择确定进入主险录入,选择取消进入附险录入"))
			return false;
		else
			return true;

	return false;
}

/*********************************************************************
 *  弹出录入附险的窗口,得到主险保单号码
 *  参数  ：  险种代码
 *  返回值：  无
 *********************************************************************
 */
function getMainRiskNo(cRiskCode)
{
	var urlStr = "./MainRiskNoInput.jsp";
	var tPolNo="";
  
  if (typeof(top.mainPolNo)!="undefined" && top.mainPolNo!="") tPolNo = top.mainPolNo;
  else tPolNo = window.showModalDialog(urlStr,tPolNo,"status:no;help:0;close:0;dialogWidth:310px;dialogHeight:100px;center:yes;");
	
	return tPolNo;
}

/*********************************************************************
 *  初始化附险信息
 *  参数  ：  投保单号
 *  返回值：  无
 *********************************************************************
 */
function initPrivateRiskInfo(cPolNo) {
	if(cPolNo=="") {
		alert("没有主险保单号,不能进行附加险录入!");
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
		return false
	}
	
	var arrLCPol = new Array(); 
	var arrQueryResult = null;
	// 主保单信息部分
	var sql = "select * from lcpol where polno='" + cPolNo + "' "
			+ "and riskcode in "
			+ "( select riskcode from LMRiskApp where SubRiskFlag = 'M' )"; 
	
	arrQueryResult = easyExecSql( sql , 1, 0);
		
	if (arrQueryResult == null)	{
		mCurOperateFlag="";        //清空当前操作类型,使得不能进行当前操作.
		alert("读取主险信息失败,不能进行附加险录入!");
		return false
	}
	
	arrLCPol = arrQueryResult[0]; 	
	displayPol( arrLCPol );	
	
	fm.all("MainPolNo").value = cPolNo;
	var tAR;
			
  	//投保人信息
  	if (arrLCPol[28]=="2") {     //集体投保人信息
  	  arrQueryResult = null;
  	  arrQueryResult = easyExecSql("select * from lcappntgrp where polno='"+cPolNo+"'"+" and grpno='"+arrLCPol[26]+"'", 1, 0);	  		
  	  tAR = arrQueryResult[0];
  	  displayPolAppntGrp(tAR);
  	} else {                     //个人投保人信息
  	  arrQueryResult = null;
  	  arrQueryResult = easyExecSql("select * from lcappntind where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[26]+"'", 1, 0);
  	  tAR = arrQueryResult[0];
  	  displayPolAppnt(tAR);
  	}

	// 被保人信息部分
	if (arrLCPol[18] == arrLCPol[26]) {
	  fm.all("SamePersonFlag").checked = true;
		parent.fraInterface.isSamePersonQuery();
    parent.fraInterface.fm.all("CustomerNo").value = arrLCPol[18];
	}
	//else {
		arrQueryResult = null;
		arrQueryResult = easyExecSql("select * from lcinsured where polno='"+cPolNo+"'"+" and customerno='"+arrLCPol[18]+"'", 1, 0);
		tAR = arrQueryResult[0];
		displayPolInsured(tAR);	
	//}
	
	return true;
}

/*********************************************************************
 *  校验投保单的输入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function verifyProposal() {
	if(verifyInput() == false) return false;
	
	BnfGrid.delBlankLine();
	if(BnfGrid.checkValue("BnfGrid") == false) return false;
	
	try {
	  //以年龄和性别校验身份证号
	  if (fm.all("AppntIDType").value == "0") 
	    chkIdNo(fm.all("AppntIDNo").value, fm.all("AppntBirthday").value, fm.all("AppntSex").value);
	  if (fm.all("IDType").value == "0") 
	    chkIdNo(fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);
	    
	  //校验职业和职业代码
	  var arrCode = new Array();
	  arrCode = verifyCode("职业（工种）", fm.all("AppntWorkType").value, "code:OccupationCode", 1); 
	  if (arrCode != true && fm.all("AppntOccupationCode").value != arrCode[0]) alert("投保人职业和职业代码不匹配！");
	  arrCode = verifyCode("职业（工种）", fm.all("WorkType").value, "code:OccupationCode", 1); 
	  if (arrCode != true && fm.all("OccupationCode").value != arrCode[0]) alert("被保人职业和职业代码不匹配！");
	    
	  //校验受益比例
	  var i;
	  var sumBnf = new Array();
	  for (i=0; i<BnfGrid.mulLineCount; i++) {
	    if (typeof(sumBnf[parseInt(BnfGrid.getRowColData(i, 7))]) == "undefined") 
	      sumBnf[parseInt(BnfGrid.getRowColData(i, 7))] = 0;
	    sumBnf[parseInt(BnfGrid.getRowColData(i, 7))] = sumBnf[parseInt(BnfGrid.getRowColData(i, 7))] + parseFloat(BnfGrid.getRowColData(i, 6));
	  }
	  for (i=0; i<sumBnf.length; i++) {
  	  if (typeof(sumBnf[i])!="undefined" && sumBnf[i]>1) {
  	    alert("受益顺序 " + i + " 的受益比例和为：" + sumBnf[i] + " 。大于100%，不能提交！");
  	    return false; 
  	  }
  	  else if (typeof(sumBnf[i])!="undefined" && sumBnf[i]<1) 
  	    alert("注意：受益顺序 " + i + " 的受益比例和为：" + sumBnf[i] + " 。小于100%");
	  }
	  
	}
	catch(e) {}
	
	return true;
}

/*********************************************************************
 *  保存个人投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  

	showSubmitFrame(mDebug);
	// 校验录入数据
	if( verifyProposal() == false ) return;
	
	if (trim(fm.all('ProposalNo').value) != "") {
	  alert("该投保单号已经存在，不允许再次新增，请重新进入录入界面！");
	  return false;
	}
	
	//if( mAction == "" )	{
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		if (loadFlag=="1") 
		{
			mAction = "INSERTPERSON";
		}
		else
		{
			mAction = "INSERTGROUP";
		}
	
		fm.all( 'fmAction' ).value = mAction;
		fm.submit(); //提交
	//}
}


/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		try { top.mainPolNo = fm.all("ProposalNo").value } catch(e) {}
	}
	mAction = ""; 
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
	try	{
		//initForm();
		var tRiskCode = fm.RiskCode.value;
		var prtNo = fm.PrtNo.value;
		
		emptyForm();
		
		fm.RiskCode.value = tRiskCode;
		fm.PrtNo.value = prtNo;
		
		if (loadFlag == "2") {
		  getGrpPolInfo();  
		}
	}
	catch(re)	{
		alert("在ProposalInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
} 

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
}
 
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	//if( cDebug == "1" )
		//parent.fraMain.rows = "0,0,50,82,*";
	//else 
		//parent.fraMain.rows = "0,0,80,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	//showDiv( operateButton, "false" ); 
	//showDiv( inputButton, "true" ); 
}           

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick() {   
	if( mOperate == 0 )	{
		mOperate = 1;
		
		cGrpPolNo = fm.all( 'GrpPolNo' ).value;
		cContNo = fm.all( 'ContNo' ).value;
		window.open("./ProposalQueryMain.jsp?GrpPolNo=" + cGrpPolNo + "&ContNo=" + cContNo);		
	}
}           

/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
	var tProposalNo = "";
	tProposalNo = fm.all( 'ProposalNo' ).value;
	
	if( tProposalNo == null || tProposalNo == "" )
		alert( "请先做投保单查询操作，再进行修改!" );
	else {
		// 校验录入数据
		if (fm.all('DivLCInsured').style.display == "none") {
      for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {    
    	  if (fm.elements[elementsNum].verify != null && fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
    	    fm.elements[elementsNum].verify = "";
    	  }
    	} 
    }
    
		if( verifyProposal() == false ) return;

		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )	{
			showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			if (loadFlag=="1") {
				mAction = "UPDATEPERSON";
			}
			else {
				mAction = "UPDATEGROUP";
			}
			
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
		
		try {
		  if (typeof(top.opener.modifyClick) == "object") top.opener.initQuery();
		  //if (top.opener != null) top.close();
		}
		catch(e) {
		}
	}
}           

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick() { 
	var tProposalNo = fm.all( 'ProposalNo' ).value;
	
	if( tProposalNo == null || tProposalNo == "" )
		alert( "请先做投保单查询操作，再进行删除!" );
	else {
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )	{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}           

/*********************************************************************
 *  Click事件，当点击“选择责任”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function chooseDuty()
{
	cRiskCode = fm.RiskCode.value;
	cRiskVersion = fm.RiskVersion.value
	
	if( cRiskCode == "" || cRiskVersion == "" )
	{
		alert( "您必须先录入险种和险种版本才能修改该投保单的责任项。" );
		return false
	}

	showInfo = window.open("./ChooseDutyInput.jsp?RiskCode="+cRiskCode+"&RiskVersion="+cRiskVersion);
	return true
}           

/*********************************************************************
 *  Click事件，当点击“查询责任信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showDuty()
{
	//下面增加相应的代码
	cPolNo = fm.ProposalNo.value;
	if( cPolNo == "" )
	{
		alert( "您必须先保存投保单才能查看该投保单的责任项。" );
		return false
	}
	
	var showStr = "正在查询数据，请您稍候......";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	showModalDialog( "./ProposalDuty.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
	showInfo.close();
}           

/*********************************************************************
 *  Click事件，当点击“关联暂交费信息”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showFee()
{
	cPolNo = fm.ProposalNo.value;
	if( cPolNo == "" )
	{
		alert( "您必须先保存投保单才能进入暂交费信息部分。" );
		return false
	}
	
	showInfo = window.open( "./ProposalFee.jsp?PolNo=" + cPolNo + "&polType=PROPOSAL" );
}           

/*********************************************************************
 *  Click事件，当双击“投保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt()
{
	if( mOperate == 0 )
	{
		mOperate = 2;
		showInfo = window.open( "../sys/LDPersonMain.html" );
	}
}           

/*********************************************************************
 *  Click事件，当双击“被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsured()
{
	if( mOperate == 0 )
	{
		mOperate = 3;
		showInfo = window.open( "../sys/LDPersonMain.html" );
	}
}           

/*********************************************************************
 *  Click事件，当双击“连带被保人客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubInsured( span, arrPara )
{
	if( mOperate == 0 )
	{
		mOperate = 4;
		spanObj = span;
		showInfo = window.open( "../sys/LDPersonMain.html" );
	}
}           

/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPol(cArr) 
{
	try 
	{
	    fm.all('ManageCom').value = cArr[12];
	    fm.all('SaleChnl').value = cArr[15];
	    fm.all('AgentCom').value = cArr[13];
	    fm.all('AgentCode').value = cArr[87];
	    fm.all('AgentGroup').value = cArr[88];
	    fm.all('Handler').value = cArr[82];
	    fm.all('AgentCode1').value = cArr[89];
	
	    fm.all('ContNo').value = cArr[1];

	    fm.all('Amnt').value = cArr[43];
	    fm.all('CValiDate').value = cArr[29];
	    fm.all('HealthCheckFlag').value = cArr[72];
	    fm.all('OutPayFlag').value = cArr[97];
	    fm.all('PayLocation').value = cArr[59];
	    fm.all('BankCode').value = cArr[102];
	    fm.all('BankAccNo').value = cArr[103];
	    fm.all('LiveGetMode').value = cArr[98];
	    fm.all('BonusGetMode').value = cArr[100];
	    fm.all('AutoPayFlag').value = cArr[65];
	    fm.all('InterestDifFlag').value = cArr[66];

	    fm.all('InsuYear').value = cArr[111];
	    fm.all('InsuYearFlag').value = cArr[110];
	    
	} catch(ex) {
	  alert("displayPol err:" + ex + "\ndata is:" + cArr);
	}
}

/*********************************************************************
 *  把保单中的投保人信息显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolAppnt(cArr) 
{
	// 从LCAppntInd表取数据
	try { fm.all('AppntCustomerNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppntRelationToInsured').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppntPassword').value = cArr[5]; } catch(ex) { };
	try { fm.all('AppntName').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntSex').value = cArr[7]; } catch(ex) { };
	try { fm.all('AppntBirthday').value = cArr[8]; } catch(ex) { };
	try { fm.all('AppntNativePlace').value = cArr[9]; } catch(ex) { };
	try { fm.all('AppntNationality').value = cArr[10]; } catch(ex) { };
	try { fm.all('AppntMarriage').value = cArr[11]; } catch(ex) { };
	try { fm.all('AppntMarriageDate').value = cArr[12]; } catch(ex) { };
	try { fm.all('AppntOccupationType').value = cArr[13]; } catch(ex) { };
	try { fm.all('AppntStartWorkDate').value = cArr[14]; } catch(ex) { };
	try { fm.all('AppntSalary').value = cArr[15]; } catch(ex) { };
	try { fm.all('AppntHealth').value = cArr[16]; } catch(ex) { };
	try { fm.all('AppntStature').value = cArr[17]; } catch(ex) { };
	try { fm.all('AppntAvoirdupois').value = cArr[18]; } catch(ex) { };
	try { fm.all('AppntCreditGrade').value = cArr[19]; } catch(ex) { };
	try { fm.all('AppntIDType').value = cArr[20]; } catch(ex) { };
	try { fm.all('AppntProterty').value = cArr[21]; } catch(ex) { };
	try { fm.all('AppntIDNo').value = cArr[22]; } catch(ex) { };
	try { fm.all('AppntOthIDType').value = cArr[23]; } catch(ex) { };
	try { fm.all('AppntOthIDNo').value = cArr[24]; } catch(ex) { };
	try { fm.all('AppntICNo').value = cArr[25]; } catch(ex) { };
	try { fm.all('AppntHomeAddressCode').value = cArr[26]; } catch(ex) { };
	try { fm.all('AppntHomeAddress').value = cArr[27]; } catch(ex) { };
	try { fm.all('AppntPostalAddress').value = cArr[28]; } catch(ex) { };
	try { fm.all('AppntZipCode').value = cArr[29]; } catch(ex) { };
	try { fm.all('AppntPhone').value = cArr[30]; } catch(ex) { };
	try { fm.all('AppntBP').value = cArr[31]; } catch(ex) { };
	try { fm.all('AppntMobile').value = cArr[32]; } catch(ex) { };
	try { fm.all('AppntEMail').value = cArr[33]; } catch(ex) { };
	try { fm.all('AppntJoinCompanyDate').value = cArr[34]; } catch(ex) { };
	try { fm.all('AppntPosition').value = cArr[35]; } catch(ex) { };
	try { fm.all('AppntGrpNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('AppntGrpName').value = cArr[37]; } catch(ex) { };
	try { fm.all('AppntGrpPhone').value = cArr[38]; } catch(ex) { };
	try { fm.all('AppntGrpAddressCode').value = cArr[39]; } catch(ex) { };
	try { fm.all('AppntGrpAddress').value = cArr[40]; } catch(ex) { };
	try { fm.all('AppntDeathDate').value = cArr[41]; } catch(ex) { };
	try { fm.all('AppntRemark').value = cArr[42]; } catch(ex) { };
	try { fm.all('AppntState').value = cArr[43]; } catch(ex) { };
	try { fm.all('AppntWorkType').value = cArr[46]; } catch(ex) { };
	try { fm.all('AppntPluralityType').value = cArr[47]; } catch(ex) { };
	try { fm.all('AppntOccupationCode').value = cArr[48]; } catch(ex) { };
	try { fm.all('AppntDegree').value = cArr[49]; } catch(ex) { };
	try { fm.all('AppntGrpZipCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('AppntSmokeFlag').value = cArr[51]; } catch(ex) { };
	try { fm.all('AppntRgtAddress').value = cArr[52]; } catch(ex) { };

}

/*********************************************************************
 *  把保单中的投保人数据显示到投保人部分
 *  参数  ：  集体客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolAppntGrp( cArr )
{
	// 从LCAppntGrp表取数据
	try { fm.all('AppntPolNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('AppntGrpNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppntRelationToInsured').value = cArr[2]; } catch(ex) { };
	try { fm.all('AppntAppntGrade').value = cArr[3]; } catch(ex) { };
	try { fm.all('AppntPassword').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppntGrpName').value = cArr[5]; } catch(ex) { };
	try { fm.all('AppntGrpAddressCode').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntGrpAddress').value = cArr[7]; } catch(ex) { };
	try { fm.all('AppntGrpZipCode').value = cArr[8]; } catch(ex) { };
	try { fm.all('AppntBusinessType').value = cArr[9]; } catch(ex) { };
	try { fm.all('AppntGrpNature').value = cArr[10]; } catch(ex) { };
	try { fm.all('AppntPeoples').value = cArr[11]; } catch(ex) { };
	try { fm.all('AppntRgtMoney').value = cArr[12]; } catch(ex) { };
	try { fm.all('AppntAsset').value = cArr[13]; } catch(ex) { };
	try { fm.all('AppntNetProfitRate').value = cArr[14]; } catch(ex) { };
	try { fm.all('AppntMainBussiness').value = cArr[15]; } catch(ex) { };
	try { fm.all('AppntCorporation').value = cArr[16]; } catch(ex) { };
	try { fm.all('AppntComAera').value = cArr[17]; } catch(ex) { };
	try { fm.all('AppntLinkMan1').value = cArr[18]; } catch(ex) { };
	try { fm.all('AppntDepartment1').value = cArr[19]; } catch(ex) { };
	try { fm.all('AppntHeadShip1').value = cArr[20]; } catch(ex) { };
	try { fm.all('AppntPhone1').value = cArr[21]; } catch(ex) { };
	try { fm.all('AppntE_Mail1').value = cArr[22]; } catch(ex) { };
	try { fm.all('AppntFax1').value = cArr[23]; } catch(ex) { };
	try { fm.all('AppntLinkMan2').value = cArr[24]; } catch(ex) { };
	try { fm.all('AppntDepartment2').value = cArr[25]; } catch(ex) { };
	try { fm.all('AppntHeadShip2').value = cArr[26]; } catch(ex) { };
	try { fm.all('AppntPhone2').value = cArr[27]; } catch(ex) { };
	try { fm.all('AppntE_Mail2').value = cArr[28]; } catch(ex) { };
	try { fm.all('AppntFax2').value = cArr[29]; } catch(ex) { };
	try { fm.all('AppntFax').value = cArr[30]; } catch(ex) { };
	try { fm.all('AppntPhone').value = cArr[31]; } catch(ex) { };
	try { fm.all('AppntGetFlag').value = cArr[32]; } catch(ex) { };
	try { fm.all('AppntSatrap').value = cArr[33]; } catch(ex) { };
	try { fm.all('AppntEMail').value = cArr[34]; } catch(ex) { };
	try { fm.all('AppntFoundDate').value = cArr[35]; } catch(ex) { };
	try { fm.all('AppntBankAccNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('AppntBankCode').value = cArr[37]; } catch(ex) { };
	try { fm.all('AppntGrpGroupNo').value = cArr[38]; } catch(ex) { };
	try { fm.all('AppntState').value = cArr[39]; } catch(ex) { };
	try { fm.all('AppntRemark').value = cArr[40]; } catch(ex) { };
	try { fm.all('AppntBlacklistFlag').value = cArr[41]; } catch(ex) { };
	try { fm.all('AppntOperator').value = cArr[42]; } catch(ex) { };
	try { fm.all('AppntMakeDate').value = cArr[43]; } catch(ex) { };
	try { fm.all('AppntMakeTime').value = cArr[44]; } catch(ex) { };
	try { fm.all('AppntModifyDate').value = cArr[45]; } catch(ex) { };
	try { fm.all('AppntModifyTime').value = cArr[46]; } catch(ex) { };
	try { fm.all('AppntFIELDNUM').value = cArr[47]; } catch(ex) { };
	try { fm.all('AppntPK').value = cArr[48]; } catch(ex) { };
	try { fm.all('AppntfDate').value = cArr[49]; } catch(ex) { };
	try { fm.all('AppntmErrors').value = cArr[50]; } catch(ex) { };
}

/*********************************************************************
 *  把保单中的被保人数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayPolInsured(cArr) 
{
	// 从LCInsured表取数据
	try { fm.all('CustomerNo').value = cArr[1]; } catch(ex) { };
	try { fm.all('SequenceNo').value = cArr[2]; } catch(ex) { };
	try { fm.all('InsuredGrade').value = cArr[3]; } catch(ex) { };
	try { fm.all('RelationToInsured').value = cArr[4]; } catch(ex) { };
	try { fm.all('Password').value = cArr[5]; } catch(ex) { };
	try { fm.all('Name').value = cArr[6]; } catch(ex) { };
	try { fm.all('Sex').value = cArr[7]; } catch(ex) { };
	try { fm.all('Birthday').value = cArr[8]; } catch(ex) { };
	try { fm.all('NativePlace').value = cArr[9]; } catch(ex) { };
	try { fm.all('Nationality').value = cArr[10]; } catch(ex) { };
	try { fm.all('Marriage').value = cArr[11]; } catch(ex) { };
	try { fm.all('MarriageDate').value = cArr[12]; } catch(ex) { };
	try { fm.all('OccupationType').value = cArr[13]; } catch(ex) { };
	try { fm.all('StartWorkDate').value = cArr[14]; } catch(ex) { };
	try { fm.all('Salary').value = cArr[15]; } catch(ex) { };
	try { fm.all('Health').value = cArr[16]; } catch(ex) { };
	try { fm.all('Stature').value = cArr[17]; } catch(ex) { };
	try { fm.all('Avoirdupois').value = cArr[18]; } catch(ex) { };
	try { fm.all('CreditGrade').value = cArr[19]; } catch(ex) { };
	try { fm.all('IDType').value = cArr[20]; } catch(ex) { };
	try { fm.all('Proterty').value = cArr[21]; } catch(ex) { };
	try { fm.all('IDNo').value = cArr[22]; } catch(ex) { };
	try { fm.all('OthIDType').value = cArr[23]; } catch(ex) { };
	try { fm.all('OthIDNo').value = cArr[24]; } catch(ex) { };
	try { fm.all('ICNo').value = cArr[25]; } catch(ex) { };
	try { fm.all('HomeAddressCode').value = cArr[26]; } catch(ex) { };
	try { fm.all('HomeAddress').value = cArr[27]; } catch(ex) { };
	try { fm.all('PostalAddress').value = cArr[28]; } catch(ex) { };
	try { fm.all('ZipCode').value = cArr[29]; } catch(ex) { };
	try { fm.all('Phone').value = cArr[30]; } catch(ex) { };
	try { fm.all('BP').value = cArr[31]; } catch(ex) { };
	try { fm.all('Mobile').value = cArr[32]; } catch(ex) { };
	try { fm.all('EMail').value = cArr[33]; } catch(ex) { };
	try { fm.all('JoinCompanyDate').value = cArr[34]; } catch(ex) { };
	try { fm.all('Position').value = cArr[35]; } catch(ex) { };
	try { fm.all('GrpNo').value = cArr[36]; } catch(ex) { };
	try { fm.all('GrpName').value = cArr[37]; } catch(ex) { };
	try { fm.all('GrpPhone').value = cArr[38]; } catch(ex) { };
	try { fm.all('GrpAddressCode').value = cArr[39]; } catch(ex) { };
	try { fm.all('GrpAddress').value = cArr[40]; } catch(ex) { };
	try { fm.all('DeathDate').value = cArr[41]; } catch(ex) { };
	try { fm.all('Remark').value = cArr[42]; } catch(ex) { };
	try { fm.all('State').value = cArr[43]; } catch(ex) { };
	try { fm.all('WorkType').value = cArr[46]; } catch(ex) { };
	try { fm.all('PluralityType').value = cArr[47]; } catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[48]; } catch(ex) { };
	try { fm.all('Degree').value = cArr[49]; } catch(ex) { };
	try { fm.all('GrpZipCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('SmokeFlag').value = cArr[51]; } catch(ex) { };
	try { fm.all('RgtAddress').value = cArr[52]; } catch(ex) { };

}

/*********************************************************************
 *  把查询返回的客户数据显示到连带被保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displaySubInsured()
{
	fm.all( spanObj ).all( 'SubInsuredGrid1' ).value = arrResult[0][0];
	fm.all( spanObj ).all( 'SubInsuredGrid2' ).value = arrResult[0][1];
	fm.all( spanObj ).all( 'SubInsuredGrid3' ).value = arrResult[0][2];
	fm.all( spanObj ).all( 'SubInsuredGrid4' ).value = arrResult[0][3];
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {
	if( arrQueryResult != null ) {
		arrResult = arrQueryResult;

		if( mOperate == 1 )	{           // 查询保单明细
			var tPolNo = arrResult[0][0];
			
			// 查询保单明细
			queryPolDetail( tPolNo );
		}
		
		if( mOperate == 2 ) {		// 投保人信息	  
			arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
			if (arrResult == null) {
			  alert("未查到投保人信息");
			} else {
			   displayAppnt(arrResult[0]);
			}

	  }
		if( mOperate == 3 )	{		// 主被保人信息
			arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
			if (arrResult == null) {
			  alert("未查到主被保人信息");
			} else {
			   displayInsured(arrResult[0]);
			}

	  }
		if( mOperate == 4 )	{		// 连带被保人信息
			displaySubInsured(arrResult[0]);
	  }
	}
	mOperate = 0;		// 恢复初态
	
	emptyUndefined(); 
}

/*********************************************************************
 *  根据查询返回的信息查询投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPolDetail( cPolNo )
{

	emptyForm();
	//var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	//var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  

	//showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	//parent.fraSubmit.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;
	parent.fraTitle.window.location = "./ProposalQueryDetail.jsp?PolNo=" + cPolNo;
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//*************************************************************
//被保人客户号查询按扭事件
function queryInsuredNo() {
  if (fm.all("CustomerNo").value == "") {
    showInsured1();
  } else if (loadFlag != "1") {
    alert("只能在投保单录入时进行操作！");
  }  else {
    arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + fm.all("CustomerNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到主被保人信息");
      displayInsured(new Array());
      emptyUndefined(); 
    } else {
      displayInsured(arrResult[0]);
    }
  }
}

//*************************************************************
//投保人客户号查询按扭事件
function queryAppntNo() {
  if (fm.all("AppntCustomerNo").value == "" && loadFlag == "1") {
    showAppnt1();
  } else if (loadFlag != "1") {
    alert("只能在投保单录入时进行操作！");
  } else {
    arrResult = easyExecSql("select * from LDPerson where CustomerNo = '" + fm.all("AppntCustomerNo").value + "'", 1, 0);
    if (arrResult == null) {
      alert("未查到投保人信息");
      displayAppnt(new Array());
      emptyUndefined(); 
    } else {
      displayAppnt(arrResult[0]);
    }
  }
}

//*************************************************************
//投保人与被保人相同选择框事件
function isSamePerson() {
  if (fm.AppntRelationToInsured.value != "00" && fm.SamePersonFlag.checked == true) {
    fm.all('DivLCInsured').style.display = ""
    fm.SamePersonFlag.checked = false;
    alert("投保人与被保人关系不是本人，不能进行该操作！");
  }
  else {
    showPage(this,DivLCInsured); 
  }

  for (var elementsNum=0; elementsNum<fm.elements.length; elementsNum++) {    
	  if (fm.elements[elementsNum].name.indexOf("Appnt") != -1) {
	    try {
	      insuredName = fm.elements[elementsNum].name.substring(fm.elements[elementsNum].name.indexOf("t") + 1);
	      if (fm.all('DivLCInsured').style.display == "none") {
	        fm.all(insuredName).value = fm.elements[elementsNum].value;
	      }
	      else {
	        fm.all(insuredName).value = "";
	      }
	    }
	    catch (ex) {}
	  }
	} 
	
}  

/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  个人客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt(cArr) 
{
	// 从LDPerson表取数据
	try { fm.all('AppntCustomerNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('AppntPassword').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppntName').value = cArr[2]; } catch(ex) { };
	try { fm.all('AppntSex').value = cArr[3]; } catch(ex) { };
	try { fm.all('AppntBirthday').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppntNativePlace').value = cArr[5]; } catch(ex) { };
	try { fm.all('AppntNationality').value = cArr[6]; } catch(ex) { };
	try { fm.all('AppntMarriage').value = cArr[7]; } catch(ex) { };
	try { fm.all('AppntMarriageDate').value = cArr[8]; } catch(ex) { };
	try { fm.all('AppntOccupationType').value = cArr[9]; } catch(ex) { };
	try { fm.all('AppntStartWorkDate').value = cArr[10]; } catch(ex) { };
	try { fm.all('AppntSalary').value = cArr[11]; } catch(ex) { };
	try { fm.all('AppntHealth').value = cArr[12]; } catch(ex) { };
	try { fm.all('AppntStature').value = cArr[13]; } catch(ex) { };
	try { fm.all('AppntAvoirdupois').value = cArr[14]; } catch(ex) { };
	try { fm.all('AppntCreditGrade').value = cArr[15]; } catch(ex) { };
	try { fm.all('AppntIDType').value = cArr[16]; } catch(ex) { };
	try { fm.all('AppntProterty').value = cArr[17]; } catch(ex) { };
	try { fm.all('AppntIDNo').value = cArr[18]; } catch(ex) { };
	try { fm.all('AppntOthIDType').value = cArr[19]; } catch(ex) { };
	try { fm.all('AppntOthIDNo').value = cArr[20]; } catch(ex) { };
	try { fm.all('AppntICNo').value = cArr[21]; } catch(ex) { };
	try { fm.all('AppntHomeAddressCode').value = cArr[22]; } catch(ex) { };
	try { fm.all('AppntHomeAddress').value = cArr[23]; } catch(ex) { };
	try { fm.all('AppntPostalAddress').value = cArr[24]; } catch(ex) { };
	try { fm.all('AppntZipCode').value = cArr[25]; } catch(ex) { };
	try { fm.all('AppntPhone').value = cArr[26]; } catch(ex) { };
	try { fm.all('AppntBP').value = cArr[27]; } catch(ex) { };
	try { fm.all('AppntMobile').value = cArr[28]; } catch(ex) { };
	try { fm.all('AppntEMail').value = cArr[29]; } catch(ex) { };
	try { fm.all('AppntBankCode').value = cArr[30]; } catch(ex) { };
	try { fm.all('AppntBankAccNo').value = cArr[31]; } catch(ex) { };
	try { fm.all('AppntJoinCompanyDate').value = cArr[32]; } catch(ex) { };
	try { fm.all('AppntPosition').value = cArr[33]; } catch(ex) { };
	try { fm.all('AppntGrpNo').value = cArr[34]; } catch(ex) { };
	try { fm.all('AppntGrpName').value = cArr[35]; } catch(ex) { };
	try { fm.all('AppntGrpPhone').value = cArr[36]; } catch(ex) { };
	try { fm.all('AppntGrpAddressCode').value = cArr[37]; } catch(ex) { };
	try { fm.all('AppntGrpAddress').value = cArr[38]; } catch(ex) { };
	try { fm.all('AppntDeathDate').value = cArr[39]; } catch(ex) { };
	try { fm.all('AppntRemark').value = cArr[40]; } catch(ex) { };
	try { fm.all('AppntState').value = cArr[41]; } catch(ex) { };
	try { fm.all('AppntWorkType').value = cArr[48]; } catch(ex) { };
	try { fm.all('AppntPluralityType').value = cArr[49]; } catch(ex) { };
	try { fm.all('AppntOccupationCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('AppntDegree').value = cArr[51]; } catch(ex) { };
	try { fm.all('AppntGrpZipCode').value = cArr[52]; } catch(ex) { };
	try { fm.all('AppntSmokeFlag').value = cArr[53]; } catch(ex) { };
	try { fm.all('AppntRgtAddress').value = cArr[54]; } catch(ex) { };

}

/*********************************************************************
 *  把数组中的数据显示到投保人部分
 *  参数  ：  集体客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayAppntGrp( cArr )
{
	// 从LDGrp表取数据
	try { fm.all('AppGrpNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('Password').value = cArr[1]; } catch(ex) { };
	try { fm.all('AppGrpName').value = cArr[2]; } catch(ex) { };
	try { fm.all('GrpAddressCode').value = cArr[3]; } catch(ex) { };
	try { fm.all('AppGrpAddress').value = cArr[4]; } catch(ex) { };
	try { fm.all('AppGrpZipCode').value = cArr[5]; } catch(ex) { };
	try { fm.all('BusinessType').value = cArr[6]; } catch(ex) { };
	try { fm.all('GrpNature').value = cArr[7]; } catch(ex) { };
	try { fm.all('Peoples').value = cArr[8]; } catch(ex) { };
	try { fm.all('RgtMoney').value = cArr[9]; } catch(ex) { };
	try { fm.all('Asset').value = cArr[10]; } catch(ex) { };
	try { fm.all('NetProfitRate').value = cArr[11]; } catch(ex) { };
	try { fm.all('MainBussiness').value = cArr[12]; } catch(ex) { };
	try { fm.all('Corporation').value = cArr[13]; } catch(ex) { };
	try { fm.all('ComAera').value = cArr[14]; } catch(ex) { };
	try { fm.all('LinkMan1').value = cArr[15]; } catch(ex) { };
	try { fm.all('Department1').value = cArr[16]; } catch(ex) { };
	try { fm.all('HeadShip1').value = cArr[17]; } catch(ex) { };
	try { fm.all('Phone1').value = cArr[18]; } catch(ex) { };
	try { fm.all('E_Mail1').value = cArr[19]; } catch(ex) { };
	try { fm.all('Fax1').value = cArr[20]; } catch(ex) { };
	try { fm.all('LinkMan2').value = cArr[21]; } catch(ex) { };
	try { fm.all('Department2').value = cArr[22]; } catch(ex) { };
	try { fm.all('HeadShip2').value = cArr[23]; } catch(ex) { };
	try { fm.all('Phone2').value = cArr[24]; } catch(ex) { };
	try { fm.all('E_Mail2').value = cArr[25]; } catch(ex) { };
	try { fm.all('Fax2').value = cArr[26]; } catch(ex) { };
	try { fm.all('Fax').value = cArr[27]; } catch(ex) { };
	try { fm.all('Phone').value = cArr[28]; } catch(ex) { };
	try { fm.all('GetFlag').value = cArr[29]; } catch(ex) { };
	try { fm.all('Satrap').value = cArr[30]; } catch(ex) { };
	try { fm.all('EMail').value = cArr[31]; } catch(ex) { };
	try { fm.all('FoundDate').value = cArr[32]; } catch(ex) { };
	try { fm.all('BankAccNo').value = cArr[33]; } catch(ex) { };
	try { fm.all('BankCode').value = cArr[34]; } catch(ex) { };
	try { fm.all('GrpGroupNo').value = cArr[35]; } catch(ex) { };
	try { fm.all('State').value = cArr[36]; } catch(ex) { };
	try { fm.all('Remark').value = cArr[37]; } catch(ex) { };
}

/*********************************************************************
 *  把查询返回的客户数据显示到被保人部分
 *  参数  ：  客户的信息
 *  返回值：  无
 *********************************************************************
 */
function displayInsured(cArr) 
{
	// 从LDPerson表取数据
	try { fm.all('CustomerNo').value = cArr[0]; } catch(ex) { };
	try { fm.all('Password').value = cArr[1]; } catch(ex) { };
	try { fm.all('Name').value = cArr[2]; } catch(ex) { };
	try { fm.all('Sex').value = cArr[3]; } catch(ex) { };
	try { fm.all('Birthday').value = cArr[4]; } catch(ex) { };
	try { fm.all('NativePlace').value = cArr[5]; } catch(ex) { };
	try { fm.all('Nationality').value = cArr[6]; } catch(ex) { };
	try { fm.all('Marriage').value = cArr[7]; } catch(ex) { };
	try { fm.all('MarriageDate').value = cArr[8]; } catch(ex) { };
	try { fm.all('OccupationType').value = cArr[9]; } catch(ex) { };
	try { fm.all('StartWorkDate').value = cArr[10]; } catch(ex) { };
	try { fm.all('Salary').value = cArr[11]; } catch(ex) { };
	try { fm.all('Health').value = cArr[12]; } catch(ex) { };
	try { fm.all('Stature').value = cArr[13]; } catch(ex) { };
	try { fm.all('Avoirdupois').value = cArr[14]; } catch(ex) { };
	try { fm.all('CreditGrade').value = cArr[15]; } catch(ex) { };
	try { fm.all('IDType').value = cArr[16]; } catch(ex) { };
	try { fm.all('Proterty').value = cArr[17]; } catch(ex) { };
	try { fm.all('IDNo').value = cArr[18]; } catch(ex) { };
	try { fm.all('OthIDType').value = cArr[19]; } catch(ex) { };
	try { fm.all('OthIDNo').value = cArr[20]; } catch(ex) { };
	try { fm.all('ICNo').value = cArr[21]; } catch(ex) { };
	try { fm.all('HomeAddressCode').value = cArr[22]; } catch(ex) { };
	try { fm.all('HomeAddress').value = cArr[23]; } catch(ex) { };
	try { fm.all('PostalAddress').value = cArr[24]; } catch(ex) { };
	try { fm.all('ZipCode').value = cArr[25]; } catch(ex) { };
	try { fm.all('Phone').value = cArr[26]; } catch(ex) { };
	try { fm.all('BP').value = cArr[27]; } catch(ex) { };
	try { fm.all('Mobile').value = cArr[28]; } catch(ex) { };
	try { fm.all('EMail').value = cArr[29]; } catch(ex) { };
	try { fm.all('BankCode').value = cArr[30]; } catch(ex) { };
	try { fm.all('BankAccNo').value = cArr[31]; } catch(ex) { };
	try { fm.all('JoinCompanyDate').value = cArr[32]; } catch(ex) { };
	try { fm.all('Position').value = cArr[33]; } catch(ex) { };
	try { fm.all('GrpNo').value = cArr[34]; } catch(ex) { };
	try { fm.all('GrpName').value = cArr[35]; } catch(ex) { };
	try { fm.all('GrpPhone').value = cArr[36]; } catch(ex) { };
	try { fm.all('GrpAddressCode').value = cArr[37]; } catch(ex) { };
	try { fm.all('GrpAddress').value = cArr[38]; } catch(ex) { };
	try { fm.all('DeathDate').value = cArr[39]; } catch(ex) { };
	try { fm.all('Remark').value = cArr[40]; } catch(ex) { };
	try { fm.all('State').value = cArr[41]; } catch(ex) { };
	try { fm.all('WorkType').value = cArr[48]; } catch(ex) { };
	try { fm.all('PluralityType').value = cArr[49]; } catch(ex) { };
	try { fm.all('OccupationCode').value = cArr[50]; } catch(ex) { };
	try { fm.all('Degree').value = cArr[51]; } catch(ex) { };
	try { fm.all('GrpZipCode').value = cArr[52]; } catch(ex) { };
	try { fm.all('SmokeFlag').value = cArr[53]; } catch(ex) { };
	try { fm.all('RgtAddress').value = cArr[54]; } catch(ex) { };

}

//*********************************************************************
function showAppnt1()
{
	if( mOperate == 0 )
	{
		mOperate = 2;
		showInfo = window.open( "../sys/LDPersonQuery.html" );
	}
}           

//*********************************************************************
function showInsured1()
{
	if( mOperate == 0 )
	{
		mOperate = 3;
		showInfo = window.open( "../sys/LDPersonQuery.html" );
	}
}  

function isSamePersonQuery() {
  fm.SamePersonFlag.checked = true;
  //divSamePerson.style.display = "none";
  DivLCInsured.style.display = "none";
}
