<%
//程序名称：ProposalQuery.jsp
//程序功能：复核不通过修改
//创建日期：2002-11-23 17:06:57
//创建人  ：胡博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
  //个人下个人
	String tPrtNo = request.getParameter("PrtNo");
	String tManageCom= request.getParameter("ManageCom");
%>
<script>
	var PrtNo = "<%=tPrtNo%>";      //印刷号
	var ManageCom = "<%=tManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="TBImageQuery.js"></SCRIPT>
  <%@include file="TBImageQueryInit.jsp"%>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <title>保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraTitle">
    <table class= common align=center>
      	<tr class= common>
          <td  class= title> 印刷号码 </td>
          <td  class= input ><Input type="text" class="readonly" name=PrtNo></td>
          <td  class= title> 管理机构 </td>
          <td  class= input><Input type="text" class="readonly" name=ManageCom ></td>
          <td></td> 
          <td></td>   
        </tr>
    </table>
		<table>
		</table>
	  <Div  id= "divEsDocPages1Title" style= "display: ''">
		 <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEsDocPages1);">
    		</td>
    		<td class= titleImg>
        		 扫描件基本信息
       	</td>   		 
    	</tr>
    </table>
  </div>
    <Div  id= "divEsDocPages1" style= "display: ''" align=center>
    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanEsDocPagesGrid">
	  				</span> 
			    </td>
				</tr>
		</table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>