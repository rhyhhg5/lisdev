<%--
    团体大项目复核 2006-3-14 16:43
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%
  //输出参数
  CErrors tError = null;             
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String tActivityID = "0000002002";
  String tMissionID = request.getParameter("MissionID");
  String tSubMissionID = request.getParameter("SubMissionID");
  String tGrpContNo = request.getParameter("GrpContNo");
  GrpTbWorkFlowUI tGrpTbWorkFlowUI = new GrpTbWorkFlowUI();
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  try
  {
  	transact = request.getParameter("fmAction");
  	TransferData tTransferData = new TransferData();
  	tTransferData.setNameAndValue("MissionID",tMissionID);
  	tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
  	tTransferData.setNameAndValue("GrpContNo",tGrpContNo);
  	// 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tTransferData);
  	tVData.add(tG);
    tGrpTbWorkFlowUI.submitData(tVData,tActivityID);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tGrpTbWorkFlowUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>