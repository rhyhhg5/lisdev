<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：InsuredChk.jsp
	//程序功能：被保人查重
	//创建日期：2002-11-23 17:06:57
	//创建人  ：WHN
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
	<%
		GlobalInput tGI = new GlobalInput();
		tGI = (GlobalInput) session.getValue("GI");
		System.out.println("operator:" + tGI.Operator);
	%>
	<script>
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var prtNo = "<%=request.getParameter("prtNo")%>";
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="ComplexBnfInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
		<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
		<SCRIPT src="ProposalAutoMove.js"></SCRIPT>
		<%@include file="ComplexBnfInputInit.jsp"%>
		<title>被保人</title>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraTitle" action="ComplexAddBnfSave.jsp">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCBnfinfo);">
					</td>
					<td class=titleImg>
						被保人受益人信息
					</td>
				</tr>
			</table>
			<Div id="divLCBnf1" style="display: ''">
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanPolGrid"> </span>
						</td>
					</tr>
				</table>
			</div>

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCBnfinfo);">
					</td>
					<td class=titleImg>
						受益人信息补录
					</td>
				</tr>
			</table>
			<div id="divLCBnfinfo" style="display: ''">
				<table class=common>
					<TR class=common>
						<TD class=title>
							受益人姓名
						</TD>
						<TD class=input>
							<Input class=common name=Name elementtype=nacessary readonly>
						</TD>
						<TD class=title>
							受益人性别
						</TD>
						<TD class=input>
							<Input class=codeNo name=Sex verify="受益人性别|notnull&code:Sex"
								ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);"
								onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input class=codename name=SexName readonly=true elementtype=nacessary>
						</TD>
                        <TD class=title>
							国籍
						</TD>
						<TD class=input>
							<input class=codeNo name="NativePlace"
								verify="受益人国籍|notnull&code:NativePlace"
								ondblclick="return showCodeList('NativePlace',[this,NativePlaceName],[0,1]);"
								onkeyup="return showCodeListKey('NativePlace',[this,NativePlaceName],[0,1]);"><input class=codename name=NativePlaceName readonly=true elementtype=nacessary>
						</TD>
					</TR>
					<TR class=common>
					<TR class=common>
						<TD class=title COLSPAN="1">
							证件生效日期
						</TD>
						<TD class=input COLSPAN="1">
							<Input class=coolDatePicker name=IDStartDate
								verify="证件生效日期|notnull&date">
						</TD>
						<TD class=title COLSPAN="1">
							证件失效日期
						</TD>
						<TD class=input COLSPAN="1">
							<Input class=coolDatePicker name=IDEndDate
								verify="证件失效日期|notnull&date">
						</TD>						
						<TD class=title>
							联系电话
						</TD>
						<TD class=input>
							<Input class=common name=Phone  verify="联系电话|notnull&len<=18">
						</TD>
						<TD class=title>
							联系地址
						</TD>
						<TD class=input>
							<Input class='common' name=PostalAddress verify="联系地址|notnull&len<=100">
						</TD>
					</TR>
					<TR>
					<TD class=title>
							职业代码
						</TD>
						<TD class=input>
							<Input class="code" name="OccupationCode"
								elementtype=nacessary verify="职业代码|notnull"
								ondblclick="return showCodeList('OccupationCode',[this,OccupationName],[0,1],null,null,null,null,300);"
								onkeyup="return showCodeListKey('OccupationCode',[this,OccupationName],[0,1],null,null,null,null,300);"">
						</TD>
						<TD class=title>
							职业
						</TD>
						<TD class=input>
							<Input class="common" name="OccupationName"
								elementtype=nacessary TABINDEX="-1" readonly>
						</TD>
					</TR>
				</table>
			</div>
			<input type=button class=cssButton value="补 录" onclick="submitForm('1')">
			<input type=button class=cssButton value="删 除" onclick="submitForm('2')">
			<input type=button class=cssButton value="返 回" onclick="top.close()">
		    <input type=hidden name="PolNo" >
		    <input type=hidden name="BnfType" >
		    <input type=hidden name="ContNo" >
		    <input type=hidden name="BnfNo" >
		    <input type=hidden name="fmtransact" >
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
