var turnPage = new turnPageClass();

function submitForm()
{
   if( verifyInput2() == false ) return false;
   fm.fmtransact.value="INSERT";
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   fm.submit(); //提交
   queryClick();
   initDiv();
}

function afterSubmit(FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}
function updateClick()
{
   if (SpecialGrid.getSelNo() == 0)
   {
    alert("请先选择一条要修改的特殊产品信息！");
    return false;
   }
   if(fm.SpecRiskCode.value != fm.SpecRiskCode1.value){
   	alert("特殊产品编码不可修改！");
   	return false;
   }
   if(fm.RiskShowName.value == ""){
   	alert("显示名称不能为空！");
   	return false;
   }
   if (confirm("您确实想修改该记录吗?"))
   {
     var i = 0;
     var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
     fm.fmtransact.value = "UPDATE";
     fm.submit(); 
     queryClick();
	 initDiv();
    }
    else
    {
       alert("您取消了修改操作！");
    }
}

function queryClick()
{
	strSQL = "select code,(select riskname from lmrisk where riskcode = code),codename from ldcode where  codetype = 'printriskcode1' "	
			 + getWherePart( 'Code','SpecRiskCode' )
			 + getWherePart( 'CodeName','RiskShowName' )
			 +" order by code";
//prompt('',strSQL);
   turnPage.queryModal(strSQL,SpecialGrid,1,1);
}

function deleteClick()
{
 if (SpecialGrid.getSelNo() == 0)
  {
   alert("请先选择一条要删除的特殊产品信息！");
   return false;
  }
  if(fm.SpecRiskCode.value != fm.SpecRiskCode1.value){
   	alert("特殊产品编码不可修改！");
  	return false;
  }
  if (confirm("您确实想删除该特殊产品信息记录吗?"))
  {
     var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
     fm.fmtransact.value = "DELETE";
     fm.submit(); //提交
     initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}
function SpecialProductsShow()
{
    fm.SpecRiskCode.value = SpecialGrid.getRowColData(SpecialGrid.getSelNo() - 1, 1);
    fm.SpecRiskCode1.value = SpecialGrid.getRowColData(SpecialGrid.getSelNo() - 1, 1);
    fm.RiskCodeName.value = SpecialGrid.getRowColData(SpecialGrid.getSelNo() - 1, 2);
    fm.RiskShowName.value = SpecialGrid.getRowColData(SpecialGrid.getSelNo() - 1, 3);
}