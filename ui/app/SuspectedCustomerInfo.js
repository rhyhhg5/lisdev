//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{
	
	// 书写SQL语句
	var strSQL = "";
	var prtNo=fm.prtNo.value;
	var cusType=fm.cusType.value;
	var cusNo = fm.cusNo.value;
	if(cusType==0){
	
	strSQL = " select cif.uid,cif.customerno,cif.name,(select codename from ldcode where codetype='sex' and code=cif.sex) ,cif.birthday,(select codename from ldcode where codetype='idtype' and code=cif.idtype),cif.idno,cif.tel_no,cif.bankname,cif.accno  "
           + " from cust_info cif " 
           + " inner join lcappnt lca on lca.appntname=cif.name and lca.idtype=cif.idtype and lca.idno=cif.idno"
           + " where lca.prtno = '"+prtNo+"'"
           + " union "
           + " select cif.uid,cif.customerno,cif.name,(select codename from ldcode where codetype='sex' and code=cif.sex) ,cif.birthday,(select codename from ldcode where codetype='idtype' and code=cif.idtype),cif.idno,cif.tel_no,cif.bankname,cif.accno  "
           + " from cust_info cif "
           + " inner join lccont lcc on lcc.appntname=cif.name and lcc.bankcode=cif.bankcode and lcc.bankaccno=cif.accno"
           + " where lcc.prtno = '"+prtNo+"'"
           + " union "
           + " select cif.uid,cif.customerno,cif.name,(select codename from ldcode where codetype='sex' and code=cif.sex) ,cif.birthday,(select codename from ldcode where codetype='idtype' and code=cif.idtype),cif.idno,cif.tel_no,cif.bankname,cif.accno  "
           + " from cust_info cif "
           + " inner join (select a.appntname customername, nvl(b.mobile, b.phone) tel_no "
           + "             from lcappnt a, lcaddress b "
           + "             where a.appntno = b.customerno "
           + "             and   a.addressno = b.addressno " 
           + "             and   a.prtno= '"+prtNo+"' ) s on cif.name = s.customername "
           + "                                            and cif.tel_no = s.tel_no"
           + " with ur ";
	
	}else{
	
	strSQL = " select cif.uid,cif.customerno,cif.name,(select codename from ldcode where codetype='sex' and code=cif.sex) ,cif.birthday,(select codename from ldcode where codetype='idtype' and code=cif.idtype),cif.idno,cif.tel_no,cif.bankname,cif.accno  "
           + " from cust_info cif " 
           + " inner join lcinsured lca on lca.name=cif.name and lca.idtype=cif.idtype and lca.idno=cif.idno"
           + " where lca.prtno = '"+prtNo+"' and insuredno='"+cusNo+"'"
           + " union "
           + " select cif.uid,cif.customerno,cif.name,(select codename from ldcode where codetype='sex' and code=cif.sex) ,cif.birthday,(select codename from ldcode where codetype='idtype' and code=cif.idtype),cif.idno,cif.tel_no,cif.bankname,cif.accno  "
           + " from cust_info cif "
           + " inner join lccont lcc on lcc.appntname=cif.name and lcc.bankcode=cif.bankcode and lcc.bankaccno=cif.accno"
           + " inner join lcinsured lca on lca.prtno=lcc.prtno "
           + " where lca.prtno = '"+prtNo+"' "
           + " and lca.insuredno='"+cusNo+"' "
           + " and lca.relationtoappnt = '00' "
           + " union "
           + " select cif.uid,cif.customerno,cif.name,(select codename from ldcode where codetype='sex' and code=cif.sex) ,cif.birthday,(select codename from ldcode where codetype='idtype' and code=cif.idtype),cif.idno,cif.tel_no,cif.bankname,cif.accno  "
           + " from cust_info cif "
           + " inner join (select a.name customername, nvl(b.mobile, b.phone) tel_no "
           + "             from lcinsured a, lcaddress b "
           + "             where a.appntno = b.customerno "
           + "             and   a.addressno = b.addressno " 
           + "             and   a.prtno= '"+prtNo+"' "
           + "             and   a.insuredno='"+cusNo+"' ) s on cif.name = s.customername "
           + "                                             and cif.tel_no = s.tel_no"
           + " with ur ";	
		
	}
	
      
	turnPage.queryModal(strSQL, PersonGrid);        
}

// 数据返回父窗口
function returnParent()
{	
	top.close();			
}



