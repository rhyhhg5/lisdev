<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PubAccInputSave.jsp
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
  LCGrpFeeSet tLCGrpFeeSet = new LCGrpFeeSet();
  LCContPlanDutyParamSet tLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
			
  GrpPubAccUI tGrpPubAccUI   = new GrpPubAccUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String tGrpContNo = request.getParameter("GrpContNo");
  String tGrpPolNo = request.getParameter("GrpPolNo");
  //实际是存储在职/退休 但是应数据被保险人状态，因此用词字段存储账户类型
  String tPublicAccType = request.getParameter("PublicAccType");
  //如果是公共账户的话存储公共账户名称
	String tInsuredName = request.getParameter("InsuredName");
	//账户金额
	String tPublicAcc = request.getParameter("Prem");
	//险种代码
	String tRiskCode = request.getParameter("RiskCode");
	//处理要素信息
	String[] tCalFactor = request.getParameterValues("PublicAccGrid1");
	String[] tCalFactorValue = request.getParameterValues("PublicAccGrid4");
	String[] tCalFactorType = request.getParameterValues("PublicAccGrid5");
	String tInsuAccNo = request.getParameter("InsuAccNo");

	LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
	tLCInsuredListSchema.setGrpContNo(tGrpContNo);
	tLCInsuredListSchema.setInsuredName(tInsuredName);
	tLCInsuredListSchema.setEmployeeName(tInsuredName);
	tLCInsuredListSchema.setRelation("00");
	tLCInsuredListSchema.setRiskCode(tRiskCode);
	tLCInsuredListSchema.setPublicAcc(tPublicAcc);
	tLCInsuredListSchema.setPublicAccType(tPublicAccType);
	tLCInsuredListSet.add(tLCInsuredListSchema);
	//add by zjd 赔付顺序 2015-01-12
	String claimnum=request.getParameter("ClaimNum");
	if(tCalFactor!=null)
	{
		int n = tCalFactor.length;
		for(int m = 0 ; m<n ; m++)
		{
			LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
			tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
			tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
			tLCContPlanDutyParamSchema.setGrpPolNo(tGrpPolNo);
			
			tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
			tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
			tLCContPlanDutyParamSchema.setContPlanCode("11");
			tLCContPlanDutyParamSchema.setDutyCode("000000");
			tLCContPlanDutyParamSchema.setCalFactor(tCalFactor[m]);
			tLCContPlanDutyParamSchema.setCalFactorType(tCalFactorType[m]);
			tLCContPlanDutyParamSchema.setCalFactorValue(tCalFactorValue[m]);
			tLCContPlanDutyParamSchema.setPlanType("0");
			tLCContPlanDutyParamSchema.setPayPlanCode("000000");
			tLCContPlanDutyParamSchema.setGetDutyCode("000000");
			tLCContPlanDutyParamSchema.setInsuAccNo(tInsuAccNo);
			tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);
		}
	}
	
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	TransferData tTransferData = new TransferData();
  	tTransferData.setNameAndValue("GrpContNo",tGrpContNo);
  	tTransferData.setNameAndValue("RiskCode",tRiskCode);
  	tTransferData.setNameAndValue("GrpPolNo",tGrpPolNo);
  	tTransferData.setNameAndValue("InsuAccNo",tInsuAccNo);
  	tVData.add(tTransferData);
		tVData.add(tLCInsuredListSet);
		tVData.add(tLCContPlanDutyParamSet);
  	tVData.add(tG);
    tGrpPubAccUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tGrpPubAccUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
