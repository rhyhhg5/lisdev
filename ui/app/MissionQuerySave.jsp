<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.tb.*"%>

<%
System.out.println("MissionQuerySave.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

CErrors tError = null;
String tAction = "";
String tOperate = "";

GlobalInput tG = (GlobalInput)session.getValue("GI");

String tMissionID = null;
String tSubMissionID = null;
String tActivityID = null;

String[] tMissionIDAll = request.getParameterValues("MissionGrid1");
String[] tSubMissionIDAll = request.getParameterValues("MissionGrid2");
String[] tActivityIDAll = request.getParameterValues("MissionGrid3");

String[] tRadio = request.getParameterValues("InpMissionGridSel");
for (int i = 0; i < tRadio.length; i++)
{
    if(tRadio[i].equals("1"))
    {
        tMissionID = tMissionIDAll[i];
        tSubMissionID = tSubMissionIDAll[i];
        tActivityID = tActivityIDAll[i];
    }
}


TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("MissionID", tMissionID);
tTransferData.setNameAndValue("SubMissionID", tSubMissionID);
tTransferData.setNameAndValue("ActivityID", tActivityID);

VData tVData = new VData();
tVData.add(tG);
tVData.add(tTransferData);

MissionTraceUI tMissionTraceUI = new MissionTraceUI();

if (!tMissionTraceUI.submitData(tVData, ""))
{
    Content = " 删除工作流失败! 原因是: " + tMissionTraceUI.mErrors.getLastError();
    FlagStr = "Fail";
}
else
{
    Content = " 删除工作流成功！";
    FlagStr = "Succ";
}

System.out.println("MissionQuerySave.jsp End ...");
%>

<html>
<script language="javascript">
    parent.fraInterface.afterMissionTraceDelSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
