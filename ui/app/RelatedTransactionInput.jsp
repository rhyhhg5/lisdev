<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<script>
	var mType = "<%=request.getParameter("type")%>";
	var RelateNo = "<%=request.getParameter("RelateNo")%>";
	var RelatedType = "<%=request.getParameter("RelatedType")%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="../common/javascript/CommonTools.js"></script>
<SCRIPT src="RelatedTransactionInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="RelatedTransactionInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="./RelatedTransactionSave.jsp" method=post name=fm
		target="fraSubmit">
		<table id="divrelatedinfo" style="display: ''">
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;"
					OnClick="showPage(this,divRelatedTransaction);"></td>
				<td class=titleImg>关联方信息：</td>
			</tr>
		</table>

		<Div id="divRelatedTransaction" style="display: ''">
			<table class=common style="display: ''">
				<TR class=common>
					<TD class=title8>关联方编号：</TD>
					<TD class=input8><Input name=RelateNo class="common" readonly>
					</TD>
					<TD class=title8>关联方名称：</TD>
					<TD class=input8><Input name=Name class="common"
						verify="关联方名称|notnull" id="Name"></TD>
					<TD class=title8>关联方类别：</TD>
					<TD class=input8><Input class=codeNo name=RelatedType id="RelatedType"
						verify="关联方类别|code:relatedtype&notnull"
						ondblclick="return showCodeList('RelatedType',[this,RelatedTypeName],[0,1]);"
						onkeyup="return showCodeListKey('RelatedType',[this,RelatedTypeName],[0,1]);"><input
						class=codename name=RelatedTypeName id="RelatedTypeName" readonly=true></TD>
				</TR>
			</table>
			<table class=common id="grp" style="display: 'none'">
				<TR>
					<TD class=title8>注册资本：</TD>
					<TD class=input8><Input class=common8 name=RgtMoney></TD>
					<TD class=title8>公司董事长姓名：</TD>
					<TD class=input8><Input class=common8 name=ChairmanName>
					</TD>
					<TD class=title8>公司总经理姓名：</TD>
					<TD class=input8><Input class=common8 name=GeneralManagerName>
					</TD>
				</TR>
				<TR>
					<TD class=title8>经营范围：</TD>
					<TD class=title8 colspan="5"><textarea name="BusinessScope"
							cols="90" rows="5" class="common"></textarea></TD>
				</TR>

			</table>
			<table class=common id="person" style="display: 'none'">

				<TR>
					<TD class=title8>性别：</TD>
					<TD class=input8><Input class=codeNo name=Sex
						verify="性别|code:Sex"
						ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);"
						onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input
						class=codename name=SexName readonly=true></TD>
					<TD class=title8>职务：</TD>
					<TD class=input8><Input class=common8 name=Position></TD>
					<TD class=title8>证件号码：</TD>
					<TD class=input8><Input class=common8 name=IDNo></TD>

				</TR>
				<TR class=common>
					<TD class=title8>其他法人或其他组织：</TD>
					<TD class=input8><Input name=OtherOrganization class="common">
					</TD>
				</TR>
			</table>
			<br> <INPUT VALUE="保  存" class=cssButton TYPE=button
				onclick="AddInput();" id="AddSave" style="display: none"> <INPUT
				VALUE="修  改" class=cssButton TYPE=button onclick="UpdateInput();"
				id="UpdateSave" style="display: none"> <INPUT VALUE="返  回"
				class=cssButton TYPE=button onclick="goBack();" id="GoBack">
			<input type=hidden name=transact>
		</Div>
		</form>
		<form action="./ListRelatedTransactionSave.jsp" method=post name=fm2 target="fraSubmit" ENCTYPE="multipart/form-data" >
			<table id="divuploadRelatedTransaction" style="display: 'none'">
				<TR>
					<td class=common><IMG src="../common/images/butExpand.gif"
						style="cursor: hand;"
						OnClick="showPage(this,divuploadRelatedTransaction2);"></td>
					<td class=titleImg>上载关联方清单：</td>
				</TR>
			</table>
			<table id="divuploadRelatedTransaction2" style="display: 'none'">
				<TR >
					<TD width='8%' style="font: 9pt">文件名：</TD>
					<TD width='80%'>
						<input type="file" 　width="100%" name=FileName class=common >
						<input VALUE="上载关联方清单" class="cssbutton" type=button onclick="relatedListUpload();">
					</TD>
				</TR>
			</table>
			<INPUT VALUE="返  回"
				class=cssButton TYPE=button id="goback" onclick="goBack();" style="display: 'none'">
	</form>

	<span id="spanCode" style="display: none; position: absolute;"></span>

</body>
</html>
