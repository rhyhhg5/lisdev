<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="GrpRiskManage.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="GrpRiskManageInit.jsp"%>
		<%@page contentType="text/html;charset=GBK"%>


	</head>
	<body onload="initForm();">
		<form action="./GrpRiskManageSave.jsp" method=post name=fm
			target="fraSubmit">
			<Div id="firstSetting" style="display: 'none'">
				<table class=common>
					<TR class=common>
						<TD class=title8>
							险种代码
						</TD>
						<TD class=input8>
							<Input class=codeNo name="RiskGrp" verify="险种代码|code:RiskGrp"
								ondblclick="return showCodeList('RiskGrp',[this,RiskGrpName],[0,1]);"
								onkeyup="return showCodeListKey('RiskGrp',[this,RiskGrpName],[0,1]);"><input class=codename name=RiskGrpName readonly=true
								elementtype=nacessary>
						</TD>
						<TD class=title>
							起始日期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" name="Startdatesave"
								verify="NodeNo|notnull" elementtype="nacessary">
						</TD>
					</TR>
					<TR class=common>
						<TD class=title8>
							是否启用
						</TD>
						<TD class=input8>
							<Input class=codeNo name="Statesave" verify="是否启用|code:Statesave"
								CodeData="0|1^0|启用^1|停用"
								ondblclick="return showCodeListEx('Statesave',[this,StatesaveName],[0,1]);"><input class=codename name=StatesaveName readonly=true
								elementtype=nacessary>
						</TD>
						<TD class=title>
							终止日期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" name="Enddatesave">
						</TD>
					</TR>
					<TR class=common>
					<td class=title>
						产品算法
					</td>
					<TD class=input colspan="5">
						<textarea class="common" name="AuditOpinion" cols="80%" rows="3"> 
        </textarea>
					</TD>

				</TR>
					<tr align=left>
						<td class=button>
							<INPUT class=cssButton VALUE="保  存" TYPE=button
								onclick="return save();">
						</td>
					</tr>
				</table>
				<hr>
			</Div>
			<!-- 查询结果部分 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLGGroupMember1);">
					</td>
					<td class=titleImg>
						查询
					</td>
				</tr>
			</table>

			<Div id="RiskcodeSetting" style="display: ''">
				<table class=common>
					<TR class=common>
						<TD class=title>
							险种编码
						</TD>
						<TD class=input>
							<Input class=codeNo name="RiskcodeQuery"
								verify="险种代码|code:RiskGrp"
								ondblclick="return showCodeList('RiskGrp',[this,RiskCodeName,RiskType],[0,1,2]);"
								onkeyup="return showCodeListKeyEx('RiskGrp',[this,RiskCodeName,RiskType],[0,1,2]);"><input class=codename name=RiskCodeName readonly=true>
						</TD>
						<td class=button>
							<INPUT class=cssButton VALUE="查  询" TYPE=button
								onclick="return easyQueryClick();">
						</td>
					</TR>
				</table>
			</Div>

			<!-- 信息（列表） -->
			<Div id="divLGGroupMember1" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanLGGroupMemberGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<br>

			<Div id="divRuleSetting" style="display: 'none'">
				<hr>
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divRuleSetting);">
						</td>
						<td class=titleImg>
							保存个人内容
						</td>
					</tr>
				</table>


				<table class=common>
					<TR class=common>
						<TD class=title>
							险种编码
						</TD>
						<TD class=input>
							<Input class=codeNo name="Riskcode" readonly><input class=codename name=RiskName readonly=true
								elementtype=nacessary>
						</TD>
						<TD class=title>
							起始日期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" name="Startdate"
								verify="NodeNo|notnull" elementtype="nacessary">
						</TD>
					</TR>
					<TR class=common>
						<TD class=title>
							是否启用
						</TD>
						<TD class=input>
							<Input class=codeNo name="State" verify="是否启用|code:State"
								CodeData="0|1^0|启用^1|停用"
								ondblclick="return showCodeListEx('State',[this,StateName],[0,1]);"><input class=codename name=StateName elementtype=nacessary>
						</TD>
						<TD class=title>
							终止日期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" name="Enddate">
						</TD>
					</TR>
					<TR class=common>
					<td class=title>
						产品算法
					</td>
					<TD class=input colspan="5">
						<textarea class="common" name="AuditOpinion" cols="80%" rows="3"> 
        </textarea>
					</TD>

				</TR>
					<tr align=left>
						<td class=button>
							<INPUT class=cssButton VALUE="删  除" TYPE=button
								onclick="return deleteClick();">
							<INPUT class=cssButton VALUE="修  改" TYPE=button
								onclick="return updateClick();">
						</td>
					</tr>
				</table>
			</Div>



			<input type=hidden id="fmtransact" name="fmtransact"
				value="INSERT||MAIN">
			<input type=hidden name=RiskType>
			<input type=hidden id="fmAction" name="fmAction">
			<input type=hidden id="ruleNo" name="ruleNo" value="">
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
