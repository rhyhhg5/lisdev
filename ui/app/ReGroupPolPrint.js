//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var manageCom;

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}

	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();

	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = GrpPolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	var strManageComWhere = " AND ManageCom LIKE '" + manageCom + "%' ";

	if( fm.ManageCom.value != '' ) {
		strManageComWhere += " AND ManageCom LIKE '" + fm.ManageCom.value + "%' ";
	}

	// 书写SQL语句
	var strSQL = "";
	strSQL = "SELECT GrpContNo,PrtNo,Prem,GrpName,CValiDate FROM LCGrpCont where 1=1 "
		+ "and AppFlag in ('1','9') and PrintCount = 1 and (cardflag<>'0' or cardflag is null )"
//		+ "and ContNo='" + contNo + "' "
//		+ "and riskcode in " + "( select riskcode from LMRiskApp where SubRiskFlag = 'M' )"
		+ getWherePart( 'GrpContNo' )
		+ getWherePart( 'PrtNo' )
		+ getWherePart( 'AgentCode' )
		+ getWherePart( 'AgentGroup' )
//		+ getWherePart( 'RiskCode' )
//		+ getWherePart( 'RiskVersion' )
//		+ getWherePart( 'GrpName','GrpName','like' )
		+ strManageComWhere;
	if( fm.GrpName.value != '' ) {
		strSQL +=" AND GrpName LIKE '%" + fm.GrpName.value + "%' ";
	}
	strSQL +="order by ManageCom,AgentCode,GrpContNo";
//	alert(strSQL);
	//fm.all('PrtNo').value=strSQL;
	execEasyQuery( strSQL );
}


function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpPolGrid();
		//HZM 到此修改
		GrpPolGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpPolGrid.loadMulLine(GrpPolGrid.arraySave);
		//HZM 到此修改
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpPolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);

		GrpPolGrid.delBlankLine();
	} // end of if
}


//提交，保存按钮对应操作
function printGroupPol()
{
	var i = 0;
	var flag = 0;
	flag = 0;

	for( i = 0; i < GrpPolGrid.mulLineCount; i++ )
	{
		if( GrpPolGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}

	if( flag == 0 ) {
		alert("请先选择一条记录，再打印保单");
		return false;
	}

	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	showSubmitFrame(mDebug);

	fm.submit();
	if(fmSave.fmtransact.value == "")
	{
		fmSave.fmtransact.value = "PRINT";
		fmSave.submit();
	}
	else
	{
		fmSave.submit();
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}

function dataConfirm(rr)
{
	if(rr.checked == true)
	{
		if( confirm("是否生成新数据？生成新数据点击“确定”，否则点“取消”。"))
		{
			rr.checked = true;
			fmSave.fmtransact.value = "CONFIRM";
		}
		else
		{
			rr.checked = false;
			fmSave.fmtransact.value = "PRINT";
		}
	}
	else
	{
		rr.checked = false;
		fmSave.fmtransact.value = "PRINT";
	}
}