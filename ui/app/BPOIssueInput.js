//程序名称：BPOIssueInput.js
//程序功能：外包反馈错误信息
//创建日期：2007-9-27 18:45
//创建人  ：xiaoxin
//更新记录：更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  外包反馈错误信息的EasyQuery
 *  描述:外包反馈错误信息.显示条件:有外包反馈错误信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	if(!verifyInput2())
	return false;
	// 书写SQL语句
	//var strSQL = "select '" + prtNo + "', '被保人', l.name, i.IssueField, c.codeName, i.OriginalInfo, i.Remark"
	//             + " from BPOIssue i, BPOLCInsured l, LDCode c"
	//             + " where i.contid in (select ContID from BPOLCPol where PrtNo = '" + prtNo + "')"
	//             + " and i.contid = l.contid and i.IssueObject = '02' and c.codetype = 'issuetype' and code = i.IssueType union all "
	//             + " select '" + prtNo + "', '投保人', l.name, i.IssueField, c.codeName, i.OriginalInfo, i.Remark"
	//             + " from BPOIssue i, BPOLCAppnt l, LDCode c"
	//             + " where i.contid in (select ContID from BPOLCPol where PrtNo = '" + prtNo + "')"
	//             + " and i.contid = l.contid and i.IssueObject = '01' and c.codetype = 'issuetype' and code = i.IssueType";
  var strSQL = "select distinct b.PrtNo, '投保人', "
            + "   (select Name from BPOLCAppnt where BPOBatchNo = a.BPOBatchNo and ContID = a.ContID), "
            + "   CodeName('bpodatafield', a.IssueField), CodeName('bpoissuetype', a.IssueType), "
            + "   a.OriginalInfo, a.Remark "
            + "from BPOIssue a, BPOLCPol b "
            + "where a.BPOBatchNo = b.BPOBatchNo "
            + "   and a.ContID = b.ContID "
            + "   and b.PrtNo = '" + prtNo + "' "
            + "   and a.IssueObject = '01' "
            + "union all "
            + "select distinct b.PrtNo, '被保人', "
            + "   (select Name from BPOLCInsured where BPOBatchNo = a.BPOBatchNo and ContID = a.ContID and InsuredID = a.IssueObjectID), "
            + "   CodeName('bpodatafield', a.IssueField), CodeName('bpoissuetype', a.IssueType), "
            + "   a.OriginalInfo, a.Remark "
            + "from BPOIssue a, BPOLCPol b "
            + "where a.BPOBatchNo = b.BPOBatchNo "
            + "   and a.ContID = b.ContID "
            + "   and b.PrtNo = '" + prtNo + "' "
            + "   and a.IssueObject = '02' ";
	
	turnPage.queryModal(strSQL, IssueGrid);
//	
//	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
//  
//  //判断是否查询成功
//  if (!turnPage.strQueryResult) {
//    alert("没有外包反馈错误信息！");
//    return "";
//  }
//  
//  //查询成功则拆分字符串，返回二维数组
//  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
//  
//  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
//  turnPage.pageDisplayGrid = IssueGrid;    
//          
//  //保存SQL语句
//  turnPage.strQuerySql     = strSQL; 
//  
//  //设置查询起始位置
//  turnPage.pageIndex       = 0;  
//  
//  //在查询结果数组中取出符合页面显示大小设置的数组
//  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
//  
//  //调用MULTILINE对象显示查询结果
//  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  return true;	 

}
