<%
//程序名称：
//程序功能：
//创建日期：2015-11-30
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initGrpContGrid();
        initGrpSubContGrid();
        
        fm.all("ManageCom").value = <%=strManageCom%>;
        var arrResult = easyExecSql("select current date - 3 Month,current date from dual where 1 = 1 ");
        if (arrResult != null) {
            fm.all('StartDate').value = arrResult[0][0];
    	    fm.all('EndDate').value = arrResult[0][1];
        }

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initGrpContGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="导入批次号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="团体编号";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="投保单位名称";
        iArray[3][1]="250px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="税务登记证号码";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="社会信用代码";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="批次申请日期";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=0;


        GrpContGrid = new MulLineEnter("fm", "GrpContGrid"); 

        GrpContGrid.mulLineCount = 0;   
        GrpContGrid.displayTitle = 1;
        GrpContGrid.canSel = 1;
        GrpContGrid.hiddenSubtraction = 1;
        GrpContGrid.hiddenPlus = 1;
        GrpContGrid.canChk = 0;
        GrpContGrid.selBoxEventFuncName = "initGrpSubConf";
        GrpContGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化GrpContGrid时出错：" + ex);
    }
}

function initGrpSubContGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="投保单号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="缴费通知书号";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="投保人";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="缴费方式";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="保费";
        iArray[5][1]="60px";
        iArray[5][2]=100;
        iArray[5][3]=0;


        GrpSubContGrid = new MulLineEnter("fm", "GrpSubContGrid"); 

        GrpSubContGrid.mulLineCount = 0;   
        GrpSubContGrid.displayTitle = 1;
        GrpSubContGrid.canSel = 0;
        GrpSubContGrid.hiddenSubtraction = 1;
        GrpSubContGrid.hiddenPlus = 1;
        GrpSubContGrid.canChk = 0;
        GrpSubContGrid.selBoxEventFuncName = "";
        GrpSubContGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化GrpSubContGrid时出错：" + ex);
    }
}
</script>

