<DIV id=DivLCInsuredButton STYLE="display:''">
<!-- 被保人信息部分 -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCInsured);">
</td>
<td class= titleImg>
<Input class= mulinetitle value="第一被保险人资料"  name=InsuredSequencename readonly >（客户号：<Input class= common name=InsuredNo >
<INPUT id="butBack" VALUE="查  询" class=cssButton TYPE=button onclick="queryInsuredNo();"> 首次投保客户无需填写客户号）
<Div  id= "divSamePerson" style="display:'none'">
<font color=red>
如投保人为被保险人本人，可免填本栏，请选择
<INPUT TYPE="checkbox" NAME="SamePersonFlag" onclick="isSamePerson();">
</font>
</div>
</td>
</tr>
</table>
</DIV> 
<DIV id=DivRelation STYLE="display:''">
    <table  class= common>  
        <TR  class= common>  
           </TD> 
	        <TD  class= title>
                客户内部号码</TD>             
            <TD  class= input>
                <Input class="code" name="SequenceNo"  readonly= true elementtype=nacessary   CodeData="0|^1|第一被保险人 ^2|第二被保险人 ^3|第三被保险人" ondblclick="return showCodeListEx('SequenceNo', [this]);" onkeyup="return showCodeListKeyEx('SequenceNo', [this]);"></TD>              
          <TD  class= title>
            与投保人关系
            </TD>
          <TD  class= input>           
          <Input class="code" name="RelationToAppnt" readonly= true elementtype=nacessary  ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);" >
           </TD> 
	        <TD  class= title>
                与第一被保险人关系</TD>             
            <TD  class= input>
                <Input class="code" name="RelationToMainInsured" readonly= true   elementtype=nacessary  ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);"></TD>                       
        </TR> 
    </table> 
</DIV>       
<DIV id=DivLCInsured STYLE="display:''">  
    <table  class= common>  
        <TR  class= common>        
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name elementtype=nacessary verify="被保险人姓名|notnull" onblur="getallinfo();">
          </TD>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
          <input class="coolDatePicker" elementtype=nacessary dateFormat="short" name="Birthday" verify="被保险人出生日期|notnull&date" >
          </TD> 
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class=codeno name=Sex verify="被保险人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1],null,null,null,1);"><input class=codename name=SexName readonly=true elementtype=nacessary>
          </TD> 
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class=codeno name=Marriage  ondblclick="return showCodeList('Marriage',[this,MarriageName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Marriage',[this,MarriageName],[0,1],null,null,null,1);"  verify="婚姻状况|notnull&code:Marriage"><input class=codename name=MarriageName readonly=true elementtype=nacessary>
          </TD>                     
        </TR>       
        <TR  class= common>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class=codeno name=IDType  value="0" verify="证件类型|code:IDType1" ondblclick="return showCodeList('IDType1',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType1',[this,IDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=IDTypeName readonly=true elementtype=nacessary verify="被保险人证件类型|code:IDType">
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name="IDNo"  onblur="checkidtype();getBirthdaySexByIDNo(this.value);getallinfo();" verify="被保险人证件号码|len<=20"  >
          </TD>
          <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <Input class=codeno name=NativePlace  verify="被保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,NativePlaceName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('NativePlace',[this,NativePlaceName],[0,1],null,null,null,1);" ><input class=codename name=NativePlaceName readonly=true >
          
          </TD>
          <TD  class= title id="NativePlace1" style="display: none">
          <div id="NativeCityTitle">国家</div>
          </TD>
          <TD  class= input id="NativeCity1" style="display: none">
          <input class=codeNo name="NativeCity" ondblclick="return showCodeList('NativeCity',[this,NativeCityName],[0,1],null,fm.NativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,NativeCityName],[0,1],null,fm.NativePlace.value,'ComCode',1);"><input class=codename name=NativeCityName readonly=true >    
          </TD>   
          <TD  class= title id="EnglishNameTitle" STYLE="display : 'none'">
            拼音
          </TD>
          <TD  class= input id="EnglishNameInput" STYLE="display : 'none'">
            <Input class=common name=EnglishName  >
          </TD>                            
        </TR>    
         <TR class=common >
  	  		 <TD  class= title COLSPAN="1" >
  	         	 	证件生效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=InsuIDStartDate  verify="证件生效日期|date">
  	        </TD>
  	        	 <TD  class= title COLSPAN="1" >
  	         		 证件失效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=InsuIDEndDate  verify="证件失效日期|date">
  	        </TD>
			<TD  class= title>
            		授权使用客户信息
          </TD>
          <TD  class= input>
            <Input class=codeNo name="InsuredAuth" readonly=true verify="授权使用客户信息|code:Auth" ondblclick="return false;return showCodeList('Auth',[this,InsuredAuthName],[0,1]);" onkeyup="return false;return showCodeListKey('Auth',[this,InsuredAuthName],[0,1]);"><input class=codename name=InsuredAuthName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title id="PassNumer1" style="display: none">
          <div id="PassNumerTitle">原通行证号码</div>
          </TD>
          <TD  class= input id="PassIDNo1" style="display: none">
           <Input class= common3 name="InsuredPassIDNo" onblur=""  verify="被保人原通行证号码|len<=20" >  
          </TD> 
  	        </TR>                                 
        <TR  class= common>
           <TD  class= title>
                职业代码
            </TD>
            <TD  class= input>
                <Input class="code" name="OccupationCode"  elementtype=nacessary verify="被保险人职业代码|notnull" ondblclick="return showCodeList('OccupationCode',[this,OccupationName],[0,1],null,null,null,1,300);" onkeyup="return showCodeListKey('OccupationCode',[this,OccupationName],[0,1],null,null,null,1,300);" onfocus="getdetailwork();">
            </TD>
            	<!--TD class=title > 职业类别
            		</TD-->
					   <!--TD class=input -->

					<!--/TD-->
            <TD  class= title>
                职业
            </TD>
            <TD  class= input>
                <Input class="common" name="OccupationName"  readonly >
            </TD>
            <TD  class= title>
                职业类别
            </TD>
            <TD  class= input>
						<Input class="code" name="OccupationType"   verify="被保险人职业类别|code:OccupationType" MAXLENGTH=0 ondblclick="return showCodeList('OccupationType',[this],[0]);" onkeyup="return showCodeListKey('OccupationType',[this],[0]);">
						</TD>  

			<TD class=title>
			  职位
			</TD>
			<TD class=input>
			  <Input name="Position" class=common>
			</TD>	      
          <TD  class= title id="PassPortTitle" STYLE="display : 'none'">
            护照号码
          </TD>
          <TD  class= input id="PassPortInput" STYLE="display : 'none'">
            <Input class= common name="OthIDNo"   >
          </TD>
        </TR>
    </Table>        
	<DIV id=DivAddress STYLE="display:''">   
	    <table  class= common>         
	        <TR  class= common>        
	          <TD  class= title>
	            工作单位
	          </TD>
	          <TD  class= input colspan=3>
	            <Input class= common3 name="GrpName"  verify="投保人工作单位|len<=60">
	          </TD>
	            <TD  class= title>
	                办公电话
	            </TD>
	            <TD  class= input>
	                <Input class= common name="GrpPhone" verify="被保险人办公电话|NUM&len<=30" >
	            </TD>
	            			<TD class=title>
			  年收入（万元）
			</TD>
			<TD class=input>
			  <Input name="Salary" class=common>
			</TD>                   
	         </TR>  
	        <!--
	        <TR  class= common>  
	            <TD  class= title>
	                地址代码
	            </TD>                  
	            <TD  class= input>
	                <Input class="code" name="AddressNo"  ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this],[0],'', '', '', true);" onkeyup="return showCodeListKeyEx('GetAddressNo',[this],[0],'', '', '', true);" onfocus="getdetailaddress();">
	            </TD>
		    </TR>                      
	        -->
          <Input class=common name="AddressNo" type="hidden">
	        <TR  class= common>                          
	          <TD  class= title>
	            单位地址
	          </TD>
	          <TD  class= input  colspan=3>
	            <Input class= common3 name="GrpAddress" verify="投保人单位地址|len<=80" >      
	          </TD>          
	          <TD  class= title>
	            邮编
	          </TD>
	          <TD  class= input>
	            <Input class= common name="GrpZipCode"  verify="被保人单位邮政编码|zipcode" >
	          </TD>
	         </TR>  
			<TR class=common>
				<TD class=title>
				  家庭地址
				</TD>
				<TD class=input colspan=3>
				  <Input name="HomeAddress" class=common3 verify="投保人家庭地址|len<=80">
				</TD>
				<TD class=title>
				  家庭邮编
				</TD>
				<TD class=input>
				  <Input name="HomeZipCode" class=common verify="投保人家庭邮政编码|zipcode">
				</TD>					
			</TR>
	        <TR  class= common>
		    <TD class=title>
		      固定电话
		    </TD>
		    <TD class=input>
		      <input class= common11 name=HomeCode  type="hidden"><input class= common10 name=HomeNumber  type="hidden">
		      <Input name="HomePhone" class=common verify="被保人家庭电话|NUM&len<=30">
		    </TD>                              
	            <TD  class= title>
	                移动电话
	            </TD>
	            <TD  class= input>
	                <Input class= common name="Mobile" verify="被保险人移动电话|NUM&len<=30" >
	            </TD>
	            <TD  class= title>
	                E-mail
	            </TD>
	            <TD  class= input>
	                <Input class= common name="EMail" verify="被保险人电子邮箱|Email&len<=60" >
	            </TD> 
	         </TR> 		
	        <TR  class= common>     
	          <TD  class= title>
	            联系地址选择
	          </TD>
	          <TD  class= input >
	            <Input class="code" name="CheckPostalAddress"  readonly= true  value='' CodeData="0|^1|同投保人(带出投保人联系地址）^2|其它"  ondblclick="return showCodeListEx('CheckPostalAddress',[this]);" onkeyup="return showCodeListKeyEx('CheckPostalAddress',[this]);" onfocus="FillPostalAddress();">     
	            <!-- <input  class= common10 name=PostalProvince type="hidden" >
		        <input  class= common10 name=PostalCity type="hidden" >
		        <input  class= common10 name=PostalCounty type="hidden" >
		        <input  class= common10 name=PostalStreet type="hidden" >
		        <input  class= common10 name=PostalCommunity type="hidden" > -->               
	          </TD>   
	          <TD  class= title colspan=2>
	            <font color=red>(在填写投保人地址后实现速填)</font>
	          </TD> 
	        </TR> 		
	        <TR  class= common id =PostalAddressID style= "display: ''">
	        <TD  class= title>
	                联系地址
	            </TD>
	            <TD  class= input  colspan=5 >
	                <Input class= common3 name="PostalAddress"  verify="被保险人联系地址|len<=80"  readonly=true >
	            </TD> 
	       </TR>
	       <TR  class= common id =insured_PostalAddressID colspan=9  readonly=true style= "display: 'none'">
	            <TD  class= title>
		            联系地址
		        </TD>
				<TD  class= input colspan=9>
					<Input class="codeno" name=Province verify="省（自治区直辖市）|notnull" ondblclick="return showCodeList('Province1',[this,PostalProvince],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province1',[this,PostalProvince],[0,1],null,'0','Code1',1);"  readonly=true ><input class=codename name=PostalProvince readonly=true elementtype=nacessary>省（自治区直辖市）
					<Input class="codeno" name=City verify="市|notnull" ondblclick="return showCodeList('City1',[this,PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onkeyup="return showCodeListKey('City1',[this,PostalCity],[0,1],null,fm.Province.value,'Code1',1);"  readonly=true ><input class=codename name=PostalCity readonly=true elementtype=nacessary>市
					<Input class="codeno" name=County verify="县（区）|notnull" ondblclick="return showCodeList('County1',[this,PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onkeyup="return showCodeListKey('County1',[this,PostalCounty],[0,1],null,fm.City.value,'Code1',1);"  readonly=true ><input class=codename name=PostalCounty readonly=true elementtype=nacessary>县（区）
					<input  class= common10 name=PostalStreet verify="乡镇（街道）|notnull" > 乡镇（街道）
			    	<input  class= common10 name=PostalCommunity verify="村（社区）|notnull" > 村（社区）
			    </TD>
	        </TR> 	
	        <TR  class= common>
	            <TD  class= title>
	                邮政编码
	            </TD>
	            <TD  class= input>
	                <Input class= common name="ZipCode"  MAXLENGTH="6" verify="被保险人邮政编码|zipcode" >
	            </TD>
	                        <TD  class= title>
                联系电话
            </TD>
            <TD  class= input>
                <input class= common name="Phone"   >
            </TD>	
	        </TR>		                                   
	    </Table> 
	</DIV>         
</DIV>
<DIV id=DivGrpNoname STYLE="display:'none'">        
    <table  class= common>        
        <TR class= common>
                 
            <TD CLASS=title>
                被保人人数
            </TD>
            <TD CLASS=input >
                <Input NAME=InsuredPeoples VALUE="" readonly=true CLASS=common verify="被保人人数|int" >
            </TD>
            <TD CLASS=title>
                被保人年龄
            </TD>
            <TD CLASS=input >
                <Input NAME=InsuredAppAge VALUE="" readonly=true CLASS=common verify="被保人年龄|int" >
            </TD>
            <TD CLASS=title>
            </TD>
            <TD CLASS=input >
            </TD>
	    </TR>  
      </table>
</Div>
<TABLE class= common>
    <TR class= common>
        <TD  class= title>
            <DIV id="divContPlan" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            保障计划
                    </TD>
                    <TD  class= input>
                        <Input class="code" name="ContPlanCode" verify="保障计划|code:ContPlanCode" ondblclick="showCodeListEx('ContPlanCode',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ContPlanCode',[this],[0],'', '', '', true);">
                    </TD>
                    </TR>
	            </TABLE>
            </DIV>
        </TD> 
        <TD  class= title>
            <DIV id="divExecuteCom" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            处理机构
                        </TD>
                        <TD  class= input>
                            <Input class="code" name="ExecuteCom" ondblclick="showCodeListEx('ExecuteCom',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ExecuteCom',[this],[0],'', '', '', true);">
                        </TD>
		            </TR>
	            </TABLE>
            </DIV>
        </TD>
        <TD  class= title>
        	<DIV id="divPolTypeFlag" style="display:''" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD CLASS=title>
					          保单类型标记
					        </TD>
					        <TD CLASS=input >
					          <Input class=codeno name=PolTypeFlag  verify="保单类型标记|code:PolTypeFlag" value="0" ondblclick="return showCodeList('PolTypeFlag',[this,PolTypeFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('PolTypeFlag',[this,PolTypeFlagName],[0,1],null,null,null,1);"><input class=codename name=PolTypeFlagName readonly=true >	
					        </TD> 
		            </TR>
	            </TABLE>
            </DIV>
        </TD>		
    </TR>
</TABLE>
<DIV id=DivClaim STYLE="display:'none'"> 
<DIV id=DivClaimHead STYLE="display:''">   
<table>
		<tr>
			<td>
			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivClaim);">
			</td>
			<td class="titleImg">理赔信息
			</td>
		</tr>
</table>
</Div>	
    <table  class= common>         
    <TR  class= common>  
            <TD  class= title>
                理赔金帐户
            </TD>                  
            <TD  class= input>
                <Input class=codeno name=AccountNo  CodeData="0|^1|缴费帐户^2|其它" verify="理赔金帐户|code:AccountNo" ondblclick="return showCodeListEx('AccountNo',[this,AccountNoName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AccountNo',[this,AccountNoName],[0,1],null,null,null,1);" ><input class=codename name=AccountNoName readonly=true >	
            </TD>
    </TR> 
          <TR CLASS=common>
	    <TD CLASS=title  >
	      开户银行 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input class=codeno name=BankCode MAXLENGTH=20 verify="开户行|code:bank" ondblclick="return showCodeList('bank',[this,bankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bank',[this,bankName],[0,1],null,null,null,1);"><input class=codename name=bankName readonly=true >	
      </TD>
	    <TD CLASS=title >
	      户名 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20 verify="户名|len<=20" >
	    </TD>
            <TD CLASS=title width="109" >
	      账号</TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=BankAccNo class="code" VALUE="" CLASS=common ondblclick="return showCodeList('accint',[this],null,null,fm.AppntNo.value,'CustomerNo');" onkeyup="return showCodeListKey('accint',[this],null,null,fm.AppntNo.value,'CustomerNo');" verify="银行帐号|len<=40" onfocus="getdetail();">
	    </TD>
	  </TR>   
    </Table> 	          
</DIV> 
<DIV id="divLCInsuredPerson" style="display:'none'"> 
  <TABLE class= common>       
         
        <TR  class= common>
            <TD  class= title>
                联系电话
            </TD>
            <TD  class= input>
                <input class= common name="Phone1"  verify="联系电话|NUM&len<=30" >
            </TD>
        </TR>
	    <TR class= common>
            <TD  class= title>
                职业类别
            </TD>
            <TD  class= input>
              <!--  <Input class="code" name="OccupationType"  verify="被保险人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
           --> </TD>  
	    </TR>             
        <TR class= common>
          <TD  class= title>
            户口所在地
          </TD>
          <TD  class= input>
            <Input class= common name="RgtAddress" >
            <!--<Input class= common name="RgtAddress" verify="被保险人户口所在地|len<=80" >-->
          </TD>
          <TD  class= title>
            民族
          </TD>
          <TD  class= input>
          <input class="code" name="Nationality"  ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">
         <!-- <input class="code" name="Nationality" verify="被保险人民族|code:Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">-->
          </TD>
          <TD  class= title>
            学历
          </TD>
          <TD  class= input>
            <Input class="code" name="Degree"  ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">
            <!--<Input class="code" name="Degree" verify="被保险人学历|code:Degree" ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">-->
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            职业（工种）
          </TD>
          <TD  class= input>
            <Input class= common name="WorkType"  >
            <!--<Input class= common name="WorkType" verify="被保险人职业（工种）|len<=10" >-->
          </TD>
          <TD  class= title>
            兼职（工种）
          </TD>
          <TD  class= input>
            <Input class= common name="PluralityType"  >
            <!--<Input class= common name="PluralityType" verify="被保险人兼职（工种）|len<=10" >-->
          </TD>                  
          <TD  class= title>
            是否吸烟
          </TD>
          <TD  class= input>
            <Input class="code" name="SmokeFlag"  ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
            <!--<Input class="code" name="SmokeFlag" verify="被保险人是否吸烟|code:YesNo" ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">-->
          </TD>
       </TR>
      </table>   
</DIV>
<DIV id=divSalary style="display:'none'">
	<table class=common>
		<TR class=common>
			<TD class=title>
			  入司时间
			</TD>
			<TD class=input>
			  <Input name="JoinCompanyDate" class="coolDatePicker" dateFormat="short">
			</TD>	
		</TR>
	</table>
</DIV>
<DIV id=divaddressbu style="display:'none'">
	<table class=common>
		<TR class=common>
			<TD class=title>
			  家庭传真
			</TD>
			<TD class=input>
			  <Input name="HomeFax" >
			</TD>
			<TD class=title>
			  单位传真
			</TD>
			<TD class=input>
			  <Input name="GrpFax" class=common>
			</TD>
			<TD class=title>
			  联系传真
			</TD>
			<TD class=input>
			  <Input name="Fax" class=common>
			</TD>		
		</TR>		
	</table>
</DIV>