
<%
	//程序名称：ReProposalPrintInput.jsp
	//程序功能：
	//创建日期：2002-11-25
	//创建人  ：Kevin
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page contentType="text/html;charset=GBK"%>
<%
	String tChk[] = request.getParameterValues("PolGridNo");
	String tusercode[] = request.getParameterValues("PolGrid1");
	String tOperator[] = request.getParameterValues("PolGrid5");
	String strOperation = request.getParameter("fmtransact");
	String Content = "";

	GlobalInput globalInput = new GlobalInput();

	if ((GlobalInput) session.getValue("GI") == null) {
		Content = "网页超时或者是没有操作员信息，请重新登录";
	} else {
		globalInput.setSchema((GlobalInput) session.getValue("GI"));
	}

	LDUserSchema tLDuserSchema = new LDUserSchema();
	for (int i = 0; i < tChk.length; i++) {
		if (tChk[i].equals("1")) {
			tLDuserSchema.setUserCode(tusercode[i].trim());
			tLDuserSchema.setOperator(tOperator[i].trim());

			System.out.println("用户编码啊" + tusercode[i].trim());
			System.out.println("上级编码啊 " + tOperator[i].trim());

		}
	}

	UpUserpasswordBL tUpUserpasswordBL = new UpUserpasswordBL();

	VData vData = new VData();
	vData.addElement(tLDuserSchema);
	vData.add(globalInput);

	try {
		if (!tUpUserpasswordBL.submitData(vData, strOperation)) {
			if (tUpUserpasswordBL.mErrors.needDealError()) {
		Content = tUpUserpasswordBL.mErrors.getFirstError();
			} else {
		Content = "保存失败，但是没有详细的原因";
			}
		}
	} catch (Exception ex) {
		ex.printStackTrace();
		Content = ex.getMessage();
	}

	String FlagStr = "";
	if (Content.equals("")) {
		FlagStr = "Succ";
		Content = "保存成功";
	} else {
		FlagStr = "Fail";
	}
%>
<html>
	<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
