//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	if((fm.AppntName.value!=null&&fm.AppntName.value!=""&&fm.AppntName.value!="null")||
			(fm.InsuredName.value!=null&&fm.InsuredName.value!=""&&fm.InsuredName.value!="null")||
			(fm.AgentCode.value!=null&&fm.AgentCode.value!=""&&fm.AgentCode.value!="null")||
			(fm.PolApplyDate.value!=null&&fm.PolApplyDate.value!=""&&fm.PolApplyDate.value!="null")||
			(fm.CValiDate.value!=null&&fm.CValiDate.value!=""&&fm.CValiDate.value!="null"))
{	
		var tagentcode="";
		if(fm.AgentCode.value!=""){
			var magentcode=easyExecSql(" select getAgentCode('"+fm.AgentCode.value+"') from dual");
			tagentcode=" and a.AgentCode='"+magentcode+"'";
		}  
		// 书写SQL语句
	  	var strSql = "select a.ContNo,a.PrtNo,a.PolApplyDate,char(a.CValiDate),a.AppntName,getUniteCode(a.AgentCode),a.ManageCom,b.ArchiveNo "
                    + ",CodeName('lcsalechnl', SaleChnl), AgentCom "
                    + " from LCCont a "
                    + " left join ES_DOC_Main b ON a.PrtNo=b.Doccode and b.SubType in ('TB01', 'TB05', 'TB04', 'TB24','TB06','TB10','TB27','TB28','TB29') " 
                    + " left join LCRnewStateLog lcr on lcr.newcontno = a.contno "
                    + " where a.conttype='1' "
    				+ "   and (b.SubType is null or b.SubType in ('TB01', 'TB05', 'TB04', 'TB24','TB06','TB10','TB27','TB28','TB29')) "
    				//+ " and manageCom like '"+manageCom+"%' "
    				+ getWherePart( 'a.PrtNo','PrtNo' )	
    				+ getWherePart( 'a.ManageCom','ManageCom' ,'like',null,"%")
				    + getWherePart( 'b.ArchiveNo','ArchiveNo','like')
    				+ getWherePart( 'a.AppntName' ,'AppntName' )
    				+ getWherePart( 'a.InsuredName','InsuredName' )
    				+ tagentcode
    				+ getWherePart( 'a.PolApplyDate','PolApplyDate' ) 
    				+ getWherePart( 'a.CValiDate','CValiDate' )
    				// 加入渠道、网点条件
                    + getWherePart("a.SaleChnl","SaleChnlCode")
                    + getWherePart("a.AgentCom","AgentComBank")
                    // ----------------------------------------
                    + " and lcr.newcontno is null "
    				;	

}    else
{  
	   var tagentcode="";
	   if(fm.AgentCode.value!=""){
		var magentcode=easyExecSql(" select getAgentCode('"+fm.AgentCode.value+"') from dual");
		tagentcode=" and AgentCode='"+magentcode+"'";
	   }  
		var strSql = "select a.ContNo,a.PrtNo,a.PolApplyDate,char(a.CValiDate),a.AppntName,getUniteCode(a.AgentCode),a.ManageCom,b.ArchiveNo "
                    + ",CodeName('lcsalechnl', SaleChnl), AgentCom "//08-1-11 add
                    + " from LCCont a " 
                    + " left join ES_DOC_Main b ON a.PrtNo=b.Doccode and b.SubType in ('TB01', 'TB05', 'TB04', 'TB24','TB06','TB10','TB27','TB28','TB29') " 
                    + " left join LCRnewStateLog lcr on lcr.newcontno = a.contno "
                    + " where a.conttype='1'"
    				+ "   and (b.SubType is null or b.SubType in ('TB01', 'TB05', 'TB04', 'TB24','TB06','TB10','TB27','TB28','TB29')) "
    				//+ " and manageCom like '"+manageCom+"%' "
    				+ getWherePart( 'a.PrtNo','PrtNo' )	
    				+ getWherePart( 'a.ManageCom','ManageCom' , 'like',null,"%")
				    + getWherePart( 'b.ArchiveNo','ArchiveNo','like')
				    // 加入渠道、网点条件
                    + getWherePart("a.SaleChnl","SaleChnlCode")
                    + getWherePart("a.AgentCom","AgentComBank")
                    // ----------------------------------------
    				+ getWherePart( 'AppntName' ,'AppntName' )
    				+ getWherePart( 'InsuredName','InsuredName' )
    				+ tagentcode
    				+ getWherePart( 'PolApplyDate','PolApplyDate' ) 
    				+ getWherePart( 'CValiDate','CValiDate' )
                    + " and lcr.newcontno is null "
                    + " and a.PrtNo=COALESCE(a.PrtNo,a.PrtNo) "
    				+" union select 'x',missionprop1,a.makedate,'','','',missionprop3,b.ArchiveNo,'','' from lwmission a left join ES_DOC_Main b ON a.missionprop1=b.Doccode "
                    + " where not exists (select 1 from LCCont lcc where conttype='1' and lcc.prtno = a.missionprop1) "
                    + " and activityid in ('0000001099','0000001098', '0000007999', '0000009001') and missionprop3 like '"+fm.ManageCom.value+"%' "
    				+ " and (b.SubType is null or b.SubType in ('TB01', 'TB05', 'TB04','TB06','TB10','TB27','TB28','TB29')) "
    				+ " and a.missionprop1=COALESCE(a.missionprop1,a.missionprop1) "
				  //+ getWherePart( 'missionprop3','ManageCom' , 'like',null,"%")
				  //
					+ getWherePart( 'missionprop1','PrtNo','like' )
				    + getWherePart( 'b.ArchiveNo','ArchiveNo','like')							
    			  //+ getWherePart( 'l.ManageCom','ManageCom' , 'like')
    				+" with ur"
    				;
    			}
    			  //fm.aa.value=strSql;
  //判断保单状态
//  if(fm.PolState.value!=null&&fm.PolState.value!=""&&fm.PolState.value!="null"){
//  	if(fm.PolState.value=="0000001001"||fm.PolState.value=="0000001150"){
//  		strSql = "select l.ContNo,l.PrtNo,l.AppntName,l.Operator,char(l.MakeDate)||'  '||l.MakeTime,l.ApproveCode,char(l.ApproveDate)||'  '||l.ApproveTime,l.UWOperator,char(l.UWDate)||'  '||l.UWTime,l.UWFlag,l.State from LCCont l,lwmission w where 1=1 "
//    				+" and conttype='1'"
//    				+" and w.activityid='"+fm.PolState.value+"'"
//    				+" and l.PrtNo=w.MissionProp2 "
//    				+ " and l.manageCom like '"+manageCom+"%%'"
//    				+ getWherePart( 'l.PrtNo','PrtNo' )	
//    				+ getWherePart( 'l.ManageCom','ManageCom' , 'like')
//    				+getWherePart( 'l.AppntName' ,'AppntName' )
//    				+ getWherePart( 'l.InsuredName','InsuredName' )
//    				+ getWherePart( 'l.AgentCode','AgentCode' ) 
//    				+ getWherePart( 'l.PolApplyDate','PolApplyDate' ) 
//    				+ getWherePart( 'l.CValiDate','CValiDate' )  
//  	}
//  	if(fm.PolState.value=="0000001097"||fm.PolState.value=="0000001098"||fm.PolState.value=="0000001099"||fm.PolState.value=="0000001100"){
//  		strSql = "select l.ContNo,l.PrtNo,l.AppntName,l.Operator,char(l.MakeDate)||'  '||l.MakeTime,l.ApproveCode,char(l.ApproveDate)||'  '||l.ApproveTime,l.UWOperator,char(l.UWDate)||'  '||l.UWTime,l.UWFlag,l.State from LCCont l,lwmission w where 1=1 "
//    				+" and conttype='1'"
//    				+" and w.activityid='"+fm.PolState.value+"'"
//    				+" and l.PrtNo=w.MissionProp1 "
//    				+ " and l.manageCom like '"+manageCom+"%%'"
//    				+ getWherePart( 'l.PrtNo','PrtNo' )	
//    				+ getWherePart( 'l.ManageCom','ManageCom' , 'like')
//    				+getWherePart( 'l.AppntName' ,'AppntName' )
//    				+ getWherePart( 'l.InsuredName','InsuredName' )
//    				+ getWherePart( 'l.AgentCode','AgentCode' ) 
//    				+ getWherePart( 'l.PolApplyDate','PolApplyDate' ) 
//    				+ getWherePart( 'l.CValiDate','CValiDate' )
//  	}
//  	if(fm.PolState.value=="0000000000"){
//  		strSql = "select l.ContNo,l.PrtNo,l.AppntName,l.Operator,char(l.MakeDate)||'  '||l.MakeTime,l.ApproveCode,char(l.ApproveDate)||'  '||l.ApproveTime,l.UWOperator,char(l.UWDate)||'  '||l.UWTime,l.UWFlag,l.State from LCCont l where 1=1 "
//    				+" and conttype='1'"
//    				+ " and l.manageCom like '"+manageCom+"%%'"
//    				+" and l.appflag='1' "
//    				+ getWherePart( 'l.PrtNo','PrtNo' )	
//    				+ getWherePart( 'l.ManageCom','ManageCom' , 'like')
//    				+getWherePart( 'l.AppntName' ,'AppntName' )
//    				+ getWherePart( 'l.InsuredName','InsuredName' )
//    				+ getWherePart( 'l.AgentCode','AgentCode' )
//    				+ getWherePart( 'l.PolApplyDate','PolApplyDate' ) 
//    				+ getWherePart( 'l.CValiDate','CValiDate' ) 
//  	}
//  }
	//turnPage.pageDivName = "divPage";
	turnPage.pageLineNum = 50;
	turnPage.queryModal(strSql,PolGrid,0,0,1);
	fm.querySql.value = strSql;

/*	
	turnPage.strQueryResult  = easyQueryVer3(strSql);
//  alert(strQueryResult);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合查询条件的记录");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult)
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //tArr = chooseArray(arrDataSet,[0,1,3,4]) 
  //调用MULTILINE对象显示查询结果
  */
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  divEsDocPages1Title.style.display = "none";
	divEsDocPages1.style.display = "none";
}

var mSwitch = parent.VD.gVSwitch;


function queryDetailClick() {
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	var cProposalContNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	var cprtno = PolGrid.getRowColData(checkFlag - 1, 2); 
//  	mSwitch.deleteVar( "fm.value('PrtNo')" );
  	
//  	mSwitch.addVar( "fm.value('PrtNo')", "", cPrtNo);
  	
    urlStr = "./ProposalEasyScan.jsp?LoadFlag=32&ContNo="+cProposalContNo+"&prtNo="+cprtno ,"status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";   
  
    window.open(urlStr);
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}


function queryMARiskClick()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cMainPolNo = PolGrid.getRowColData(tSel - 1,2);	
	    var cPrtNo = PolGrid.getRowColData(tSel - 1,3);			

		
		if (cMainPolNo == "")
		    return;
		    
		  window.open("./MainOddRiskQueryMain.jsp?MainPolNo=" + cMainPolNo + "&PrtNo=" + cPrtNo);										
	}
}

//扫描件查询
function ScanQuery() {
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else {
	    var prtNo = PolGrid.getRowColData(tSel - 1,2);				
		
		if (prtNo == "") return;
		    fm.PrtNo2.value = prtNo;

		divEsDocPages1Title.style.display = "";
		divEsDocPages1.style.display = "";
		initEsDocPagesGrid(); 
		easyqueryClick1();
	}	     
}

function easyqueryClick1()
{

	var strSQL = "select a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
					+ " a.maketime,a.modifydate,a.modifytime,a.docid "
					+ " from es_doc_main a ,es_doc_def b where 1=1 "
					+ " and a.subtype = b.subtype "
				  + " and a.doccode like '"+fm.PrtNo2.value+"%%'"
				  + " and a.busstype='TB'"
				  + " and b.busstype='TB'";			
					
					
	  turnPage.queryModal(strSQL,EsDocPagesGrid);
	 
}


function ChkState()
{
	fm.action =  "../uw/PolStatusChk.jsp";
	fm.submit(); //提交
}
function afterSubmit(FlagStr,Content)
{
  var i = 0;
  var tstatue="";
  for (i=0; i<PolStatuGrid.mulLineCount; i++) {
  	var j=i+1;
    if (PolStatuGrid.getRowColData(i,1)!=null||PolStatuGrid.getRowColData(i,1)!=""||PolStatuGrid.getRowColData(i,1)!="null") { 
     tstatue+=j+"、"+PolStatuGrid.getRowColData(i,1)+" ";
    }
  }
  fm.ProState.value=tstatue;
}

function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }
 if(cCodeName=="agentgroup1"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
 
}
function queryStateClick() {
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	var cProposalContNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	var cprtno = PolGrid.getRowColData(checkFlag - 1, 2); 	
  	
    urlStr = "./ProposalQueryStateMain.jsp?ContNo="+cProposalContNo+"&prtNo="+cprtno ,"status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";   
  
    window.open(urlStr);
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

function ChangePic()
{
	tRow = EsDocPagesGrid.getSelNo();
		
	if( tRow == 0 || tRow == null  )
	{
		alert("请选择一条记录！");
		return;
	}
	var cDocID = EsDocPagesGrid.getRowColData(tRow-1,10);	
 	var cDocCode = EsDocPagesGrid.getRowColData(tRow-1,1); 	
  var	cBussTpye = "TB" ;
  var cSubTpye = EsDocPagesGrid.getRowColData(tRow-1,2); 
    
    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
}

/**
 * 下拉框，选定后触发事件。
 */
function afterCodeSelect(tCodeName, tObj)
{
    if (tCodeName == "lcsalechnl")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    showInfo=window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
}
/**
 * 查询银行结果返回
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
    }
}
function downloadClick()
{
	  if(PolGrid.mulLineCount==0)
    {
        alert("列表中没有数据可下载");
    }
    var oldAction = fm.action;
    fm.action = "ProposalQueryDownLoad.jsp";
    fm.submit();
    fm.action = oldAction;
}

//调用查看外包错误信息界面
function viewBPOIssue()
{
  var tSel = PolGrid.getSelNo();

  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择一条记录。" );
  }
  else
  {
    var prtNo = PolGrid.getRowColData(tSel - 1,2);
    if (prtNo == "") return;
    fm.PrtNo2.value = prtNo;
    window.open("./BPOIssueInput.jsp?prtNo=" + fm.PrtNo2.value,"BPOIssue");
  }
}

//查询业务员代码
function queryAgentCode()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        return ;
    }
    if(fm.all('SaleChnlCode').value == "")
    {
        //if(confirm("没有选择销售渠道！\n是否选择销售渠道？")) return false;
    }
    
    var tSaleChnl = (fm.SaleChnlCode != null && fm.SaleChnlCode != "undefined") ? fm.SaleChnlCode.value : "";
    var tAgentCom = (fm.AgentCom != null && fm.AgentCom != "undefined") ? fm.AgentCom.value : "";
    var tBranchType = 1;
        if(tSaleChnl == "07" || tSaleChnl == "11" || tSaleChnl == "12")
    tBranchType = 2;
    var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
        //+ "&SaleChnl=" + tSaleChnl
        //+ "&AgentCom=" + tAgentCom
        //+ "&branchtype=" + tBranchType
        ;
    var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    return true;
}

//业务员查询后的操作
function afterQuery2(arrResult)
{
    if(arrResult!=null)
    {
        fm.AgentCode.value = arrResult[0][0];
        fm.AgentCodeName.value = arrResult[0][5];
        fm.AgentGroup.value = arrResult[0][1];
    }
}