<%@page contentType="text/html;charset=GBK"%>
<jsp:directive.page import="com.sinosoft.lis.db.LCInsuredDB"/>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：GroupPolInput.jsp
	//程序功能：
	//创建日期：2002-08-15 11:48:43
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//输出参数
	CErrors tError = null;
	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String LoadFlag = request.getParameter("LoadFlag");
	String tCustomerType = request.getParameter("CustomerType");
	GlobalInput tG = new GlobalInput();
	LCInsuredDB tOLDLCInsuredDB=new LCInsuredDB();
	LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
	tG = (GlobalInput) session.getValue("GI");

	if (tG == null) {
		System.out.println("页面失效,请重新登陆");
		FlagStr = "Fail";
		Content = "页面失效,请重新登陆";
	} else //页面有效
	{
		//输入参数
		VData ttVData = new VData();
		LCApproveSchema tLCApproveSchema = new LCApproveSchema();

		if (LoadFlag.equals("5") && tCustomerType.equals("00")) {
			tLCApproveSchema.setPrtNo(request.getParameter("PrtNo"));
			tLCApproveSchema.setContType("1");
			tLCApproveSchema.setCustomerType(tCustomerType);
		} else {
			tLCApproveSchema.setPrtNo(request.getParameter("PrtNo"));
			tLCApproveSchema.setCustomerType(tCustomerType);
			tLCApproveSchema.setContType("1");
			tLCApproveSchema.setCustomerNo(request.getParameter("customerNo"));
		}
		System.out.println("end setSchema:");
		ttVData.add(tG);
		ttVData.add(tLCApproveSchema); 
	    ttVData.add(tLCInsuredSchema);	
	    ttVData.add(tOLDLCInsuredDB);

		if (LoadFlag.equals("5")) {
			ApproveContUI tApproveContUI = new ApproveContUI();
			tApproveContUI.submitData(ttVData, "Approve");
		}
		Content = " 保存成功! ";
		FlagStr = "Succ";

		// 显示
		// 保单信息
		System.out.println("Content:" + Content);
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit1("<%=FlagStr%>","<%=Content%>");
</script>
</html>
