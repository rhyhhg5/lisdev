//程序名称：ContDelete
//程序功能：个单整单删除
//创建日期：2008-1-15 11:10:36
//创建人  ：liuli
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var strsql = 0;

           
//查询单   待录入、待收费、待签单、已签单
function query()
{
	// 初始化表格                                                                                                                                                           
	initGrid();	
	if(!verifyInput2())
	return false;
	// 书写SQL语句
   //alert(fm.prtNo.value);
   var strprtno="";
   if(fm.prtNo.value != null && fm.prtNo.value != ""){
   	    strprtno = " and lcc.prtNo = '"+fm.prtNo.value+"'";//投保书号
   }
   //alert(fm.ContNo.value);
   var strContNo="";
   if(fm.ContNo.value != null && fm.ContNo.value != ""){
        strContNo = " and lcc.ContNo = '"+fm.ContNo.value+"'"; // 保单号	
   }
   //alert(fm.ManageCom.value);
   var strManageCom="";
   if(fm.ManageCom.value != null && fm.ManageCom.value != ""){
        strManageCom = " and lcc.ManageCom like '"+fm.ManageCom.value+"%'"; //机构号
   }
   //alert(fm.SaleChnlCode.value);
   var strSaleChnlCode="";
   if(fm.SaleChnlCode.value != null && fm.SaleChnlCode.value != ""){
        strSaleChnlCode = " and lcc.SaleChnl = '"+fm.SaleChnlCode.value+"'"; // 渠道	
   }
   //alert(fm.AgentCom.value);
   var strAgentCom="";
   if(fm.AgentCom.value != null && fm.AgentCom.value != ""){
        strAgentCom = " and lcc.AgentCom = '"+fm.AgentCom.value+"'"; // 网点	
   }
   //alert(fm.AppntNo.value);
   var strAppntNo="";
   if(fm.AppntNo.value != null && fm.AppntNo.value != ""){
        strAppntNo = " and lcc.AppntNo = '"+fm.AppntNo.value+"'"; //投保人	
   }
   //alert(fm.cardflag.value);
   var strcardflag="";
   if(fm.cardflag.value != null && fm.cardflag.value != ""){
        strcardflag = " and lcc.cardflag = '"+fm.cardflag.value+"'"; // 保单类型
   }else {
        strcardflag = " and lcc.cardflag in ('1','2','6')";	
   }
   var strinputdate="";
   if(fm.inputdate.value != null && fm.inputdate.value != ""){
        strinputdate = " and lcc.inputdate = '"+fm.inputdate.value+"'"; //录入日期	
   }   
   //alert(fm.StateCode.value);
   if(fm.StateCode.value=="1"||fm.StateCode.value=="0"){
       //待录入
        var strsql1;
        strsql1 = " select lcc.PrtNo,lcc.ContNo as xx,lcc.ManageCom"
                 +",(select codename from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl)"
                 +",(select appntname from lcappnt where contno=lcc.contno and appntno=lcc.appntno)"
                 +",(select postalAddress from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno and b.appntno=lcc.appntno)"
                 +",(select zipcode from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno  and b.appntno=lcc.appntno)"
                 +",(select mobile from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno  and b.appntno=lcc.appntno)"
                 +",lcc.cvalidate,lcc.inputdate,'待录入',lcc.prem,lcc.agentcom as yy"
                 +",(select name from lacom where agentcom=lcc.agentcom)"
                 +",lcc.agentcode as zz,(select name from laagent where agentcode =lcc.agentcode)"
                 +" from lccont lcc where lcc.conttype='1' and lcc.grpcontno='00000000000000000000' and lcc.appflag='1'"
                 +" and exists (select 1 from lwmission lwm where lwm.missionprop1 = lcc.PrtNo and lwm.activityid in('0000007001','0000007003'))"
                 +strprtno
                 +strContNo
                 +strManageCom
                 +strSaleChnlCode
                 +strAgentCom
                 +strAppntNo
                 +strcardflag
                 +strinputdate
                 ; 
          strsql = strsql1;
   }
   if(fm.StateCode.value=="2"||fm.StateCode.value=="0"){
   	  //待收费
        var strsql2;
        strsql2 = " select lcc.PrtNo,lcc.ContNo as xx,lcc.ManageCom"
                 +",(select codename from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl)"
                 +",(select appntname from lcappnt where contno=lcc.contno and appntno=lcc.appntno)"
                 +",(select postalAddress from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno and b.appntno=lcc.appntno)"
                 +",(select zipcode from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno  and b.appntno=lcc.appntno)"
                 +",(select mobile from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno  and b.appntno=lcc.appntno)"
                 +",lcc.cvalidate,lcc.inputdate,'待收费',lcc.prem,lcc.agentcom as yy"
                 +",(select name from lacom where agentcom=lcc.agentcom)"
                 +",lcc.agentcode as zz,(select name from laagent where agentcode =lcc.agentcode)"
                 +" from lccont lcc where lcc.conttype='1' and lcc.grpcontno='00000000000000000000'"
                 +" and exists(select 1 from lwmission lwm where lwm.missionprop2 = lcc.PrtNo and lwm.activityid = '0000007002')"
                 +" and exists(select 1 from ljtempfee ljt where ljt.otherno = lcc.prtno and otherno = '4' and ljt.enteraccdate is null)"
                 +strprtno
                 +strContNo
                 +strManageCom
                 +strSaleChnlCode
                 +strAgentCom
                 +strAppntNo
                 +strcardflag
                 +strinputdate
                 ; 
        if(fm.StateCode.value=="0"){
           strsql = strsql2 +" union all "+strsql; 
         }else {
         	strsql = strsql2;
         }   	   
   }
   if(fm.StateCode.value=="3"||fm.StateCode.value=="0"){
   	   //待签单
   	    var strsql3;
        strsql3 = " select lcc.PrtNo,lcc.ContNo as xx,lcc.ManageCom"
                 +",(select codename from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl)"
                 +",(select appntname from lcappnt where contno=lcc.contno and appntno=lcc.appntno)"
                 +",(select postalAddress from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno and b.appntno=lcc.appntno)"
                 +",(select zipcode from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno  and b.appntno=lcc.appntno)"
                 +",(select mobile from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno  and b.appntno=lcc.appntno)"
                 +",lcc.cvalidate,lcc.inputdate,'待签单',lcc.prem,lcc.agentcom as yy"
                 +",(select name from lacom where agentcom=lcc.agentcom)"
                 +",lcc.agentcode as zz,(select name from laagent where agentcode =lcc.agentcode)"
                 +" from lccont lcc where lcc.conttype='1' and lcc.grpcontno='00000000000000000000'"
                 +" and exists (select 1 from lwmission lwm where lwm.missionprop2 = lcc.PrtNo and lwm.activityid = '0000007002')"
                 +" and exists (select 1 from ljtempfee ljt where ljt.otherno = lcc.prtno and otherno = '4' and ljt.enteraccdate is not null and confdate is null) "
                 +strprtno
                 +strContNo
                 +strManageCom
                 +strSaleChnlCode
                 +strAgentCom
                 +strAppntNo
                 +strcardflag
                 +strinputdate
                 ; 
         if(fm.StateCode.value=="0"){
           strsql = strsql3 +" union all "+strsql; 
         }else {
         	strsql = strsql3;
         } 
   }
    if(fm.StateCode.value=="4"||fm.StateCode.value=="0"){
	    //己签单
	    var strsql4;
        strsql4 = " select lcc.PrtNo,lcc.ContNo as xx,lcc.ManageCom"
                 +",(select codename from ldcode where codetype='lcsalechnl' and code=lcc.SaleChnl)"
                 +",(select appntname from lcappnt where contno=lcc.contno and appntno=lcc.appntno)"
                 +",(select postalAddress from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno and b.appntno=lcc.appntno)"
                 +",(select zipcode from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno  and b.appntno=lcc.appntno)"
                 +",(select mobile from lcaddress a,lcappnt b where a.customerno=b.appntno and a.addressno=b.addressno and b.contno =lcc.contno  and b.appntno=lcc.appntno)"
                 +",lcc.cvalidate,lcc.inputdate,'己签单',lcc.prem,lcc.agentcom as yy"
                 +",(select name from lacom where agentcom=lcc.agentcom)"
                 +",lcc.agentcode as zz,(select name from laagent where agentcode =lcc.agentcode)"
                 +" from lccont lcc where lcc.conttype='1' and lcc.grpcontno='00000000000000000000' and lcc.appflag='1'"
                 +strprtno
                 +strContNo
                 +strManageCom
                 +strSaleChnlCode
                 +strAgentCom
                 +strAppntNo
                 +strcardflag
                 +strinputdate
                 ; 
         if(fm.StateCode.value=="0"){
           strsql = strsql4 +" union all "+strsql; 
         }else {
         	strsql = strsql4;
         } 
   }
  strsql = strsql + " order by yy,zz,xx with ur";
  turnPage2.pageDivName = "divPage2";		
  turnPage2.queryModal(strsql,Grid); 
  
  if(Grid.mulLineCount == 0)
  {
      alert("没有查询到数据");
  }
}

function afterCodeSelect(tCodeName, tObj)
{
    if (tCodeName == "lcsalechnl")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}
/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    showInfo=window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
    }
}

function queryDown(){
  if(Grid.mulLineCount == 0)
  {
    alert("没有需要下载的数据");
    return false;
  }
  fm.sql.value = turnPage2.strQuerySql;
  fm.action = "ContBankQueryDown.jsp";
  fm.submit();
}

