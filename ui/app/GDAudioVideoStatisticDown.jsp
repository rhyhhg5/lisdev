<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2009-08-10
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "清单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);

  String querySql = request.getParameter("querySql");
  
  querySql = querySql.replaceAll("%25","%");
  
	//设置表头
	String[][] tTitle = {{"机构名称","保单合同号","销售渠道","出单平台","银行网点名称","产品名称","交费方式","交费金额","投保人姓名","投保人年龄","投保日期","保单生效日期","保单终止日期","业务员姓名","是否录音录像","是否扫描","保单状态"}};
	//表头的显示属性
	int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17};
    
  //数据的显示属性
  int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17};
  
  //生成文件
  CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
  createexcellist.createExcelFile();
  String[] sheetName ={"list"};
  createexcellist.addSheet(sheetName);
  int row = createexcellist.setData(tTitle,displayTitle);
  
  if(row ==-1) errorFlag = true;
  createexcellist.setRowColOffset(row,0);//设置偏移
  if(createexcellist.setData(querySql,displayData)==-1)
  {
  	errorFlag = true;
  	System.out.println(errorFlag);
  }
  if(!errorFlag)
  //写文件到磁盘
  try{
     createexcellist.write(tOutXmlPath);
  }catch(Exception e)
  {
  	errorFlag = true;
  	System.out.println(e);
  }
  //返回客户端
  if(!errorFlag)
  	downLoadFile(response,filePath,downLoadFileName);
  out.clear();
	out = pageContext.pushBody();
%>
