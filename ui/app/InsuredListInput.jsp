<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Verifyinput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="InsuredListInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="InsuredListInit.jsp"%>
    <title>被保险人清单下载</title>
</head>
<body onload="initForm();" >
    <form action="./InsuredListSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 个人保单信息部分 -->
        <table class= common border=0 width=100%>
            <tr><td class= titleImg align= center>请输入保单查询条件：</td></tr>
        </table>
        <table class= common align=center>
            <TR class= common>
                <TD class= title>管理机构</TD>
                <TD class= input><input class="codeNo" name=manageCom ondblclick="return showCodeList('comcode',[this,manageComName],[0,1],null,null,null,1);" readonly ><input class=codename name=manageComName readonly ></TD>
                <TD class= title>保单号</TD>
                <TD class= input><input class="common" name=grpContNo ></TD>
                <TD class= title>印刷号</TD>
                <TD class= input><input class="common" name=prtNo ></TD>
            </TR>
            <TR class= common>
                <TD class= title>销售渠道</TD>
                <TD class= input><input class="codeNo" name=saleChnl ondblclick="return showCodeList('salechnl',[this,saleChnlName],[0,1]);" readonly ><input class=codename name=saleChnlName readonly ></TD>
                <TD class= title>业务员</TD>
                <TD class= input><input class="common" name=agentCode ></TD>
                <TD class= title>代理机构</TD>
                <TD class= input><input class="common" name=agentCom ></TD>
            </TR>
            <TR class= common>
                <TD class= title>承保起始日期</TD>
                <TD class= input><input class="coolDatePicker" name=startSignDate ></TD>
                <TD class= title>承保终止日期</TD>
                <TD class= input><input class="coolDatePicker" name=endSignDate ></TD>
            </TR>
        </table>
        <input value="查询保单信息" class="cssButton" type=button onclick="easyQueryClick();">
        <br>
        <table>
            <tr>
                <td class=common>
                    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInsuredList);">
                </td>
                <td class= titleImg>保单列表</td>
            </tr>
        </table>
        <div id= "divInsuredList" style= "display: ''">
            <table class= common>
                <tr class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanInsuredListGrid" ></span>
                    </td>
                </tr>
            </table>
            <div id= "divPage" align=center style= "display: 'none' ">
                <input value="首 页" class="cssButton" type=button onclick="turnPage.firstPage();">
                <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
                <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
                <input value="尾 页" class="cssButton" type=button onclick="turnPage.lastPage();">
            </div>
        </div>
    </form>
    <form action="./InsuredListDownload.jsp" method=post name=fmSave target="fraSubmit">
        <table class= common border=0 width=100%>
            <tr><td class= titleImg align= center>下载选中保单的被保险人清单：</td></tr>
        </table>
        <input value="下载" class="cssButton" type=button onclick="downloadList();">
        <input type="hidden" class="common" name="prtNo" >
        <input type="hidden" class="common" name="querySql" >
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
