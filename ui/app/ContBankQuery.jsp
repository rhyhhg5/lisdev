<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ContDelete.jsp
//程序功能：个单整单删除
//创建日期：2004-12-06 11:10:36
//创建人  ：Zhangrong
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	//String tPolNo = "00000000000000000000";
	//String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	   
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>

<head >
<meta http-equiv="Content-Type" content="text/html charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ContBankQuery.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ContBankQueryInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单查询条件 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	   </tr>
    </table>
    <table  class= common align=center>
        <TR  class= common>
         <TD  class= title>
            投保单号
          </TD>
          <TD  class= input>
            <Input class=common name=prtNo>
         </TD>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class=common name=ContNo >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          	<input class="codeNo"  name=ManageCom value="<%=tGI.ManageCom%>" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype="nacessary">
          </TD> 
		</TR>
        <TR class= common>
         <TD  class= title>
            渠道
          </TD>
          <TD  class= input8>
            <input class="codeNo" name="SaleChnlCode" ondblclick="return showCodeList('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" />
          </TD>
          <TD class="title8">网点</TD>
            <TD class="input8">
                 <input id="AgentComBank" class="code" name="AgentCom" style="display:none;" ondblclick="return queryAgentComBank();" />
           </TD>
           <TD class="title8">投保人</TD>
           <TD>
           	 <Input class=common name=AppntNo>
           </TD>
        </TR>
        <TR>
        	<TD class="title8">
        		状态
        	</TD>
        	<TD class="input8">
        		<Input class=codeno name=StateCode  value="0" verify="状态|notnull" CodeData="0|5^0|全选^1|待录入^2|待收费^3|待签单^4|己签单" ondblClick="showCodeListEx('sex',[this,StateName],[0,1]);" onkeyup="showCodeListKeyEx('StateCode',[this,StateName],[0,1]);"><Input class=codename value="全选" name= StateName elementtype="nacessary" >
        	</TD>
        	<TD class="title8">保单类型</TD>
        	<TD class="input8">
        		<Input class=codeno name=cardflag  CodeData="0|3^1|境外救援^2|意外险平台^6|银行保险" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);"><input class=codename name=ContTypeName>
            </TD>
        	<TD class="title8">录入日期
        	</TD>
        	<TD>
        		<Input class= 'coolDatePicker'  dateFormat="Short" name=inputdate value="" verify="入机时间|DATE">
            </TD>
        </TR>
    </table>
       <INPUT VALUE="查  询" Class="cssButton" TYPE=button onclick="query();">
    <!-- 查询未过单（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单查询结果
    		</td>
    	</tr>
    </table>
    
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      	<div id="divPage2" align=center style="display: 'none' ">
      		<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
	        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	        <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();"> 	
    	</div>
	</Div>
	<INPUT VALUE="查询结果下载" Class="cssButton" TYPE=button onclick="queryDown();"> 
	<INPUT type=hidden name=sql>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
