<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Verifyinput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="RiskdutyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="RiskdutyInit.jsp"%>
    <title>险种责任查询</title>
</head>
<body onload="initForm();" >
    <form action="./RiskInfoSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 个人保单信息部分 -->
        <table class= common border=0 width=100%>
            <tr><td class= titleImg align= center>请输险种编码：</td></tr>
        </table>
        <table class= common >
            <TR class= common >
                <TD class= title >险种编码</TD>
                <TD class= input ><input class="codeNo" name=riskCode ondblclick="return showCodeList('RiskCode',[this,riskName],[0,1],null,null,null,1);" ><input class=codename name=riskName readonly ></TD>
            </TR>
        </table>
        <input type="hidden" class="common" name="querySql" >
        <input value="查询险种责任信息" class="cssButton" type=button onclick="easyQueryClick();">
        <input value="下载清单" class="cssButton" type=button onclick="downloadList();">
        <br>
        <table>
            <tr>
                <td class=common>
                    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskInfo);">
                </td>
                <td class= titleImg>险种责任列表</td>
            </tr>
        </table>
        <div id= "divRiskInfo" style= "display: ''">
            <table class= common>
                <tr class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanRiskdutyGrid" ></span>
                    </td>
                </tr>
            </table>
            <div id= "divPage" align=center style= "display: 'none' ">
                <input value="首 页" class="cssButton" type=button onclick="turnPage.firstPage();">
                <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
                <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
                <input value="尾 页" class="cssButton" type=button onclick="turnPage.lastPage();">
            </div>
        </div>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
