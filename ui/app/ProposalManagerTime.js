//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSql = "select DOC_CODE, INPUT_DATE, Operator, InputStartDate, InputStartTime from ES_DOC_MAIN where "
    				 + " DOC_FLAGE='1' "
    				 + " and InputState='0' "
    				 + getWherePart('DOC_CODE')
    				 + getWherePart('INPUT_DATE')
    				 + getWherePart('Operator');
//alert(strSQL);
	turnPage.queryModal(strSql, PolGrid);
}

var mSwitch = parent.VD.gVSwitch;
function queryDetailClick() {
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	var cPolNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	mSwitch.deleteVar( "PolNo" );
  	mSwitch.addVar( "PolNo", "", cPolNo );
  	
    urlStr = "./ProposalMain.jsp?LoadFlag=6";
    window.open(urlStr);
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}
