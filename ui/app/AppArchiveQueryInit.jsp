<%
//程序名称：LGWorkBoxQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-23 09:59:56
//创建人  ：ctrHTML
//更新人  ：  
%>
 <%@page import="com.sinosoft.lis.pubfun.*" %>
<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
  String superNo=tGI.ManageCom;//上级机构编号
  //String tGroupNo = superNo+ "_" + PubFun1.CreateMaxNo("TASKGROUP" + superNo, 6);//生成小组编号
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="larryQueryInput.js"></SCRIPT> 
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">                        
function initForm() {
  try {
    initLGWorkBoxGrid();
    fm.all("ManageCom").value='<%=superNo%>';
  }
  catch(re) {
    alert("LGWorkBoxQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initLGWorkBoxGrid() {                               
  var iArray = new Array();
    
  try {
  	iArray[0]=new Array();
		iArray[0][0]="序号";            	//列名
		iArray[0][1]="40px";            	//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
   	iArray[1]=new Array();
		iArray[1][0]="归档号";         	//列名
		iArray[1][1]="100px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
		iArray[2][0]="印刷号";       	  //列名
		iArray[2][1]="100px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[3]=new Array();
		iArray[3][0]="保单号";         	
		iArray[3][1]="80px";            	
		iArray[3][2]=200;            		 
		iArray[3][3]=0;         
		iArray[3][21]="billName";
		
		iArray[4]=new Array();           
		iArray[4][0]="投保人";         	//列名
		iArray[4][1]="100px";            	//列宽
		iArray[4][2]=200;                   //列最大值
		iArray[4][3]=0;              		//是否允许输入,1表示允许，0表示不允许
		iArray[4][21]="appent";
		
		iArray[5]=new Array();
		iArray[5][0]="保单类型";         	
		iArray[5][1]="50px";            	
		iArray[5][2]=200;            		 
		iArray[5][3]=0; 
		
		iArray[6]=new Array();           
		iArray[6][0]="保单状态";         	//列名
		iArray[6][1]="50px";            	//列宽
		iArray[6][2]=200;                   //列最大值
		iArray[6][3]=0;              		//是否允许输入,1表示允许，0表示不允许
		iArray[6][21]="billState";
		
		iArray[7]=new Array();
		iArray[7][0]="扫描日期";         	
		iArray[7][1]="50px";            	
		iArray[7][2]=200;            		 
		iArray[7][3]=0;
		
		iArray[8]=new Array();
		iArray[8][0]="扫描件页数";         	
		iArray[8][1]="0px";            	
		iArray[8][2]=200;            		 
		iArray[8][3]=3;
		      
		iArray[9]=new Array();
    iArray[9][0]="销售渠道";                 //列名
    iArray[9][1]="40px";               //列名
    iArray[9][3]=0;                //列名
    iArray[9][21]="saleChnl";

    iArray[10]=new Array();
    iArray[10][0]="网点";                 //列名
    iArray[10][1]="80px";               //列名
    iArray[10][3]=0;                //列名
    iArray[10][21]="agentCom";
		
		LarryBoxGrid1 = new MulLineEnter("fm", "LarryBoxGrid1"); 
		//设置Grid属性
		LarryBoxGrid1.mulLineCount = 10;
		LarryBoxGrid1.displayTitle = 1;
		LarryBoxGrid1.locked = 1;
		LarryBoxGrid1.canSel = 0;
		LarryBoxGrid1.canChk = 0;
		LarryBoxGrid1.hiddenSubtraction = 1;
		LarryBoxGrid1.hiddenPlus = 1;
		LarryBoxGrid1.loadMulLine(iArray);
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
