<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BPOCont.jsp
//程序功能：外包保单修改
//创建日期：2007-10-19 16:52
//创建人  ：YangYalin
//更新记录：更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="BPOCont.js"></SCRIPT>
  <SCRIPT src="ContInsuredLCImpart.js"></SCRIPT>
  <SCRIPT src="ContInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="AutoMove.js"></SCRIPT> 	
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  <%@include file="BPOContInit.jsp"%>
  <title>扫描录入</title>
</head>
<body  onload="initForm();setFocus()" >
  <form action="" method=post name=fm target="fraSubmit">
    <br></br>
    <table>
      <tr>
        <td class=titleImg>导入错误日志:</td>
      </tr>
      <tr>
        <TD colspan="7" class= input>
        	<textarea class="readonly" readonly style="border: none; overflow: hidden; background-color: #F7F7F7;" name="BPOImportLog" cols="90%" rows="2"></textarea> 
        </TD>
      </tr>
      <tr>
        <INPUT VALUE="查看外包错误" class=cssButton TYPE=button onclick="viewBPOIssue();">
      </tr>
      
    </table>
    
    <!--%@include file="BPOIssueList.jsp"%-->
    
    <%@include file="ContPage.jsp"%>
    
    <DIV id=DivLCAppnt STYLE="display:''">
      <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
          </td>
          <td class= titleImg><input class="readonly" name="AppntInfo" value = "投保人信息" readOnly>
          </td>
        </tr>
      </table>
    </DIV>

    <DIV id=DivLCAppntInd STYLE="display:''">
      <table  class= common>
        <TR  class= common>        
          <TD  class= title>姓名</TD>
          <TD  class= input>
            <Input class= common name=AppntName elementtype=nacessary verify="投保人姓名|notnull" >
          </TD>
          <TD  class= title>出生日期</TD>
          <TD  class= input>
            <input class="coolDatePicker" name="AppntBirthday" dateFormat="short" elementtype=nacessary verify="投保人出生日期|notnull&date" >
          </TD>                          
          <TD  class= title>性别</TD>
          <TD  class= input>
            <Input class=codeNo name=AppntSex  verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true elementtype=nacessary>    
          </TD>
          <TD id="AppntMarriageTitle" class= title>婚姻</TD>
          <TD id="AppntMarriageInput" class= input>
            <Input class=codeNo name="AppntMarriage"  verify="投保人婚姻状况|notnull&code:Marriage" ondblclick="return showCodeList('Marriage',[this,AppntMarriageName],[0,1]);" onkeyup="return showCodeListKey('Marriage',[this,AppntMarriageName],[0,1]);"><input class=codename name=AppntMarriageName readonly=true elementtype=nacessary>    
          </TD>           
        </TR>  
        <TR  class= common>
          <TD  class= title>证件类型</TD>
          <TD  class= input>
            <Input class=codeNo name="AppntIDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1]);"><input class=codename name=AppntIDTypeName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title>证件号码</TD>
          <TD  class= input colspan=1>
            <Input class= common name="AppntIDNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" >
          </TD> 
          <TD id="AppntNativePlaceTitle" class= title>国籍</TD>
          <TD id="AppntNativePlaceInput" class= input>
          <input class=codeNo name="AppntNativePlace" verify="投保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,AppntNativePlaceName],[0,1]);" onkeyup="return showCodeListKey('NativePlace',[this,AppntNativePlaceName],[0,1]);"><input class=codename name=AppntNativePlaceName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title id="NativePlace" style="display: none">
          <div id="NativeCityTitle">国家</div>
          </TD>
          <TD  class= input id="NativeCity" style="display: none">
          <input class=codeNo name="AppntNativeCity" ondblclick="return showCodeList('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);"><input class=codename name=AppntNativeCityName readonly=true >    
          </TD>       
          </TR>
          <TR class=common >
  	  		 <TD  class= title COLSPAN="1" >
  	          证件生效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=AppIDStartDate  verify="证件生效日期|date">
  	        </TD>
  	        	 <TD  class= title COLSPAN="1" >
  	          证件失效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=AppIDEndDate  verify="证件失效日期|date">
  	        </TD>
  	        </TR>                                                 
        <TR  class= common>     
          <TD  class= title>工作单位</TD>
          <TD  class= input  >
            <Input class= common name="AppntGrpName" verify="投保人工作单位|len<=60" >
          </TD>
          <TD  class= title>岗位职务</TD>
    			<TD  class= input>
      		  <Input class= 'common' name=AppntPosition >
    			</TD>   
          <TD  class= title>职业代码</TD>
          <TD  class= input>
            <Input class="code" name="AppntOccupationCode" elementtype=nacessary verify="投保人职业代码|notnull" ondblclick="return showCodeList('OccupationCode',[this,AppntOccupationName],[0,1],null,null,null,null,300);" onkeyup="return showCodeListKey('OccupationCode',[this,AppntOccupationName],[0,1],null,null,null,null,300);"><Input class="common" name="AppntOccupationName" type="hidden" readonly>
          </TD>        
          <TD  class= title>职业类别</TD>
          <TD  class= input>
            <Input class="code" name="AppntOccupationType" verify="投保人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>                  
        </TR>       
        <TR  class= common>  
          <TD id="AppntSalaryTitle" class= title>年收入(万元)</TD>
      		<TD id="AppntSalaryInput" class= input>
        	  <Input class= 'common' name=AppntSalary >
      		</TD>   
        </TR>
        <TR  class= common>
          <TD  class= title>联系地址</TD>
          <TD  class= input colspan=5 >
            <Input class= common3 name="AppntPostalAddress" type="hidden" verify="投保人联系地址|len<=80" >
            <input class= common10 name=appnt_PostalProvince > 省（自治区直辖市）
		      	<input class= common10 name=appnt_PostalCity > 市
		      	<input class= common10 name=appnt_PostalCounty > 县（区）
		      	<input class= common10 name=appnt_PostalStreet > 乡镇（街道）
		      	<input class= common10 name=appnt_PostalCommunity > 村（社区）
          </TD>
          <TD  class= title>邮编</TD>
          <TD  class= input>
            <Input class= common name="AppntZipCode" verify="投保人邮政编码|zipcode" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>联系电话</TD>
          <TD  class= input colspan=3 >
            <input class= common name="AppntPhone" maxlength=18 > （联系电话为数字或数字和‘-’的组合）
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>移动电话</TD>
          <TD  class= input colspan=3 >
            <Input class= common name="AppntMobile" maxlength=15 verify="投保人移动电话|NUM&len<=30" > （移动电话为数字或数字和‘-’的组合）
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>E-mail</TD>
          <TD  class= input>
            <Input class= common name="AppntEMail" verify="投保人电子邮箱|Email&len<=60"  >
          </TD> 
          <TD  class= title>固定电话</TD>
          <TD  class= input colspan=1 >
            <input class= common11 name=AppntHomeCode> - <input class= common10 name=AppntHomeNumber>
            <input class= common name="AppntHomePhone"  type="hidden"  >
          </TD>                   
        </TR>  
      </table>   
    </DIV>
    
    
    <DIV id=divFeeButton STYLE="display:''">
      <table>
        <tr>
          <td class= titleImg>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFee);">
            <input class="readonly" name="PayInfo" value = "缴费信息" readOnly>
          </td>
        </tr>
      </table>
    </DIV>        
    <Div  id= "divFee" style= "display: ''">  
      <table  class= common>
        <TR CLASS=common>
          <TD CLASS=title >缴费方式 </TD>
    	    <TD CLASS=input COLSPAN=1 >
    	      <Input NAME=PayMode VALUE=""  CLASS=codeNo CodeData="0|^4|银行转账 ^1|自缴^3|其它^11|银行代收^12|银行代收-导入" MAXLENGTH=20  ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode',[this,PayModeName],[0,1],null,null,null,1);" verify="缴费方式|notnull&code:PayMode"><input class=codename name=PayModeName readonly=true elementtype=nacessary> 
    	    </TD>
          <TD CLASS=title >整单保费 </TD>
    	    <TD CLASS=input COLSPAN=1 >
    	      <Input name=PremScope class=common >
    	    </TD>
    	    <TD CLASS=title >续期缴费方式 </TD>
    	    <TD CLASS=input COLSPAN=1 >
    	      
    	    <Input class=codeNo name="ExPayMode"  ondblclick="return showCodeList('ExPayMode',[this,ExPayModeName],[0,1]);" onkeyup="return showCodeListKey('ExPayMode',[this,ExPayModeName],[0,1]);"><input class=codename name=ExPayModeName >
    	    </TD>
    	  </TR>    
        <TR CLASS=common>
    	    <TD CLASS=title  >开户银行</TD>
    	    <TD CLASS=input COLSPAN=1  >
    	      <Input NAME=BPOBankCode VALUE="" CLASS=codeNo MAXLENGTH=20  ondblclick="return showCodeList('bank',[this,AppntBankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('bank',[this,AppntBankCodeName],[0,1],null,null,null,1);"  ><input class=codename name=AppntBankCodeName readonly=true>    
    	    </TD>
    	    <TD CLASS=title >户名 </TD>
    	    <TD CLASS=input COLSPAN=1  >
    	      <Input NAME=BPOAccName VALUE="" CLASS=common MAXLENGTH=20 verify="户名|len<=20" >
    	    </TD>
          <TD CLASS=title width="109" >账号</TD>
    	    <TD CLASS=input COLSPAN=1  >
    	      <Input NAME=BPOBankAccNo class="common" VALUE="" verify="银行帐号|len<=40"">
    	    </TD>
    	  </TR>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>是否需要续期缴费提醒</td>
            <td>
                <input name="DueFeeMsgFlag" class="codeNo" ondblclick="return showCodeList('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);" verify="是否需要续期缴费提醒|notnull&code:duefeemsgflag"><input class="codename" name="DueFeeMsgFlagName" readonly=true elementtype=nacessary />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      </table>   
    </DIV>  
    <INPUT VALUE="保  存"  class =  cssButton TYPE=button onclick="saveManageInfo();"> 
    
    
    <!---
    <table class=common>
      <TR  class= common> 
        <TD  class= common>特别约定 </TD>
      </TR>
      <TR  class= common>
        <TD  class= common>
          <textarea name="Remark" cols="110" rows="2" class="common"  verify="特别约定|len<=1000"></textarea>
        </TD>
      </TR>
    </table>
    -->
    
    <br></br>
    <!--被保人信息-->
    <DIV id=DivLCInsured STYLE="display:''">
      <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInsured);">
          </td>
          <td class= titleImg>
            <input class="readonly" name="InsuredInfo" value = "被保人信息" readOnly>
          </td>
        </tr>
      </table>
    </DIV>
    
  	<Div  id= "divInsured" style= "display: ''" align=center>
  	  
    	<Div  id= "divContGrid" style= "display: ''" align=center>
        <table  class= common>
         	<tr  class= common>
        	  <td text-align: left colSpan=1>
    					<span id="spanInsuredGrid" >
    					</span> 
    			  </td>
    			</tr>
      	</table>
      </Div>
    	<Div id= "divPage2" align=center style= "display: 'none' ">
        <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPage2.lastPage();"> 
      </Div>
    
      <table  class= common>
        <TR  class= common style="display:none">        
          <TD  class= title>合同ID</TD>
          <TD  class= input>
            <Input class= common name=BPOInsuredContID elementtype=nacessary verify="合同ID|notnull&INT" >
          </TD>
          <TD  class= title style="display:none">被保人ID</TD>
          <TD  class= input colspan="5" style="display:none">
            <input class="common" elementtype=nacessary name="BPOInsuredID" verify="被保人ID|notnull&INT" >
          </TD>    
        </TR> 
        <TR  class= common style="display:''">        
          <TD  class= title>与投保人关系</TD>
          <TD  class= input>
            <Input class=codeNo name=RelationToAppnt  verify="投保人性别|notnull&code:relation" ondblclick="return showCodeList('Relation',[this,RelationToAppntName],[0,1]);" onkeyup="return showCodeListKey('Relation',[this,RelationToAppntName],[0,1]);"><input class=codename name=RelationToAppntName readonly=true elementtype=nacessary>
          </TD>
          <TD  class= title>与主被保人关系</TD>
          <TD  class= input colspan="5">
            <Input class=codeNo name=RelationToMainInsured  verify="投保人性别|notnull&code:relation" ondblclick="return showCodeList('Relation',[this,RelationToMainInsuredName],[0,1]);" onkeyup="return showCodeListKey('Relation',[this,RelationToMainInsuredName],[0,1]);"><input class=codename name=RelationToMainInsuredName readonly=true elementtype=nacessary>
          </TD>  
        </TR> 
        <TR  class= common>        
          <TD  class= title>姓名</TD>
          <TD  class= input>
            <Input class= common name=BPOInsuredName elementtype=nacessary verify="投保人姓名|notnull" >
          </TD>
          <TD  class= title>出生日期</TD>
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" elementtype=nacessary name="BPOInsuredBirthday" verify="投保人出生日期|notnull&date" >
          </TD>                          
          <TD  class= title>性别</TD>
          <TD  class= input>
            <Input class=codeNo name=BPOInsuredSex  verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,BPOInsuredSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,BPOInsuredSexName],[0,1]);"><input class=codename name=BPOInsuredSexName readonly=true elementtype=nacessary>    
          </TD>
          <TD id="BPOInsuredMarriageTitle" class= title>婚姻</TD>
          <TD id="BPOInsuredMarriageInput" class= input>
            <Input class=codeNo name="BPOInsuredMarriage"  verify="投保人婚姻状况|notnull&code:Marriage" ondblclick="return showCodeList('Marriage',[this,BPOInsuredMarriageName],[0,1]);" onkeyup="return showCodeListKey('Marriage',[this,BPOInsuredMarriageName],[0,1]);"><input class=codename name=BPOInsuredMarriageName readonly=true elementtype=nacessary>    
          </TD>           
        </TR>  
        <TR  class= common>
          <TD  class= title>证件类型</TD>
          <TD  class= input>
            <Input class=codeNo name="BPOInsuredIDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,BPOInsuredIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,BPOInsuredIDTypeName],[0,1]);"><input class=codename name=BPOInsuredIDTypeName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title>证件号码</TD>
          <TD  class= input colspan=1>
            <Input class= common name="BPOInsuredIDNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" >
          </TD> 
          <TD id="BPOInsuredNativePlaceTitle" class= title>国籍</TD>
          <TD id="BPOInsuredNativePlaceInput" class= input>
          <input class=codeNo name="BPOInsuredNativePlace" verify="投保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,BPOInsuredNativePlaceName],[0,1]);" onkeyup="return showCodeListKey('NativePlace',[this,BPOInsuredNativePlaceName],[0,1]);" onkeyup="GetDisplay(this.value)"><input class=codename name=BPOInsuredNativePlaceName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title id="NativePlaces" style="display: none">
          <div id="NativeCityTitles">国家</div>
          </TD>
          <TD  class= input id="NativeCitys" style="display: none">
          <input class=codeNo name="BPOInsuredNativeCity" ondblclick="return showCodeList('NativeCity',[this,BPOInsuredNativeCityName],[0,1],null,fm.BPOInsuredNativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,BPOInsuredNativeCityName],[0,1],null,fm.BPOInsuredNativePlace.value,'ComCode',1);"><input class=codename name=BPOInsuredNativeCityName readonly=true >    
          </TD>      
          </TR>
          <TR class=common >
  	  		 <TD  class= title COLSPAN="1" >
  	         	 	证件生效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=InsuIDStartDate  verify="证件生效日期|date">
  	        </TD>
  	        	 <TD  class= title COLSPAN="1" >
  	         		 证件失效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=InsuIDEndDate  verify="证件失效日期|date">
  	        </TD>
  	        </TR>                                                          
        <TR  class= common>     
          <TD  class= title>工作单位</TD>
          <TD  class= input  >
            <Input class= common name="BPOInsuredGrpName" verify="投保人工作单位|len<=60" >
          </TD>
          <TD  class= title>岗位职务</TD>
    			<TD  class= input>
      		  <Input class= 'common' name=BPOInsuredPosition >
    			</TD>   
          <TD  class= title>职业代码</TD>
          <TD  class= input>
            <Input class="code" name="BPOInsuredOccupationCode" verify="投保人职业代码|notnull" ondblclick="return showCodeList('OccupationCode',[this,BPOInsuredOccupationName],[0,1],null,null,null,null,300);" onkeyup="return showCodeListKey('OccupationCode',[this,BPOInsuredOccupationName],[0,1],null,null,null,null,300);"><Input class="common" name="BPOInsuredOccupationName" elementtype=nacessary type="hidden" readonly>
          </TD>        
          <TD  class= title>职业类别</TD>
          <TD  class= input>
            <Input class="code" name="BPOInsuredOccupationType" verify="投保人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>                  
        </TR>       
        <TR  class= common>  
          <TD id="BPOInsuredSalaryTitle" class= title>年收入(万元)</TD>
      		<TD id="BPOInsuredSalaryInput" class= input>
        	  <Input class= 'common' name=BPOInsuredSalary >
      		</TD>   
        </TR>
        <TR  class= common>
          <TD  class= title>联系地址</TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="BPOInsuredPostalAddress" verify="投保人联系地址|len<=80" >
          </TD>
          <TD  class= title>邮编</TD>
          <TD  class= input>
            <Input class= common name="BPOInsuredZipCode" verify="投保人邮政编码|zipcode" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>联系电话</TD>
          <TD  class= input colspan=3 >
            <input class= common name="BPOInsuredPhone" maxlength=18 >  （联系电话为数字或数字和‘-’的组合）
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>移动电话</TD>
          <TD  class= input colspan=3 >
            <Input class= common name="BPOInsuredMobile" maxlength=15 verify="投保人移动电话|NUM&len<=30" > （移动电话为数字或数字和‘-’的组合）
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>E-mail</TD>
          <TD  class= input>
            <Input class= common name="BPOInsuredEMail" verify="投保人电子邮箱|Email&len<=60"  >
          </TD>     
          <TD  class= title>固定电话</TD>
          <TD  class= input>
            <Input class= common name="BPOInsuredHomePhone"  >
          </TD>             
        </TR>  
      </table>  
	  <br>
	  
		<!-- 抵达国家 -->
		<div id="divNation" style="display: 'none'">
			<table class=common>
				<tr>
					<td COLSPAN="1" class="titleImg">
						<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divNationGrid);">抵达国家
					</td>
				</tr>
			</table>
		</div> 
		<div id="divNationGrid" style="display: 'none'">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanNationGrid" >
						</span>
					</td>
				</tr>
			</table>
		</div>
	  <br>
    
    <!--套餐信息-->
    <DIV id=divWrap STYLE="display:'none'">
      <table class= common>
        <tr>
          <td COLSPAN="1" class="titleImg">
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divWrapGrid);">
            <input class="readonly" name="SetInfo" value = "套餐信息" readOnly>
          </td>
        </tr>
      </table>
      <div  id= "divWrapGrid" style= "display: 'none'">
        <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
              <span id="spanWrapGrid" >
              </span>
            </td>
          </tr>
        </table>
      </div>
      <Div id= "divPage2" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
    	</Div>
    
    	<div id= "queryWrapDetailID" style= "display: ''">
    		<table class= common>
        	<tr>
          	<td>
            	<input type =button name="queryWrapDetailButton" class=cssButton value="责任查询" onclick="queryWrapDetail();">
            </td>
            </tr>
      	</table>
			</div>
    </DIV>
    
    <!--套餐责任信息-->
    <DIV id=divRiskWrapID STYLE="display:'none'">
      <table class= common>
        <tr>
          <td COLSPAN="1" class="titleImg">
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskWrap);">
            <input class="readonly" name="DutyInfo" value = "责任信息" readOnly>
          </td>
        </tr>
      </table>

      <div  id= "divRiskWrap" style= "display: ''">
        <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
              <span id="spanRiskWrapGrid" >
              </span>
            </td>
          </tr>
        </table>
      </div>
      <Div id= "divPage3" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage();"> 
    	</Div>
    </DIV>
	  <br>
          
      <!--险种信息-->
      <!--受益人信息-->
      <DIV id="DivLCPol" style="display:''">
        <table class= common>
          <tr class= common>
            <td class= titleImg>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);"> <input class="readonly" name="PolInfo" value = "险种信息" readOnly>
            </td>
            <td >
            </td>
          </tr>
        </table>
      </DIV>
      <Div  id= "divPolGrid" style= "display: ''" align=center>
        <table  class= common>
         	<tr  class= common>
        	  <td text-align: left colSpan=1>
    					<span id="spanPolGrid" >
    					</span> 
    			  </td>
    			</tr>
      	</table>
      </Div>
    	<Div id= "divPage3" align=center style= "display: 'none' ">
        <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPage3.firstPage();"> 
        <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage3.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage3.nextPage();"> 
        <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPage3.lastPage();"> 
      </Div>
      
      <!--受益人信息-->
      <DIV id=DivLCBnf STYLE="display:''">
        <table class= common>
          <tr class= common>
            <td class= titleImg>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBnfGrid);"> <input class="readonly" name="BnfInfo" value = "受益人信息" readOnly >
            </td>
            <td >
            </td>
          </tr>
        </table>
      </DIV>
      <Div  id= "divBnfGrid" style= "display: ''" align=center>
        <table  class= common>
         	<tr  class= common>
        	  <td text-align: left colSpan=1>
    					<span id="spanBnfGrid" >
    					</span> 
    			  </td>
    			</tr>
      	</table>
      </Div>
    	<Div id= "divPage4" align=center style= "display: 'none' ">
        <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="turnPage4.firstPage();"> 
        <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage4.previousPage();"> 					
        <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage4.nextPage();"> 
        <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="turnPage4.lastPage();"> 
      </Div>
      
      <br></br>
      <%@include file="ContInsuredLCImpart.jsp"%>
      
    </DIV>
     
    <INPUT VALUE="保  存"  class=cssButton TYPE=button onclick="saveInsuredInfo();"> 
    <INPUT VALUE="生成保单" class=cssButton TYPE=button name=import onclick="importCont();">
    <!-- <input type="button" name="autoMoveInput" value="随动定制确定" onclick="submitAutoMove('11');" class=cssButton> -->
    
    <Input type="hidden" name="BPOBatchNo">
    <Input type="hidden" name="MissionID">
    <Input type="hidden" name="PrtNoOld" value="<%=request.getParameter("PrtNo")%>">
    <Input type="hidden" name="ContID">
    <Input type="hidden" name="AppntID">
    <Input type="hidden" name="RiskWrapColFix">
    <Input type="hidden" name="RiskWrapCol">
    <Input type="hidden" name="SubType">
    
    <!--为了支持复用ContInput.js，特增加此div，否则会抛异常-->
    <Div  id= "divButtonBPO" style= "display: none">
      <%@include file="../common/jsp/OperateButton.jsp"%>
      <%@include file="../common/jsp/InputButton.jsp"%>
    </DIV>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
