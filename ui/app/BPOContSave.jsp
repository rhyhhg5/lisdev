<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：外包录入错误数据修改保存页面
//创建日期：2007-10-23 15:24
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	//输出参数            
	String FlagStr="";
	String Content = "";
	
	String tBPOBatchNo = request.getParameter("BPOBatchNo");
	String tContID = request.getParameter("ContID");
	String tAppntID = request.getParameter("AppntID");
	String tPrtNo = request.getParameter("PrtNoOld");
	
  BPOLCAppntSchema tBPOLCAppntSchema = null;
      
  //修改投保人合同信息
  BPOLCAppntDB tBPOLCAppntDB = new BPOLCAppntDB();
  tBPOLCAppntDB.setBPOBatchNo(tBPOBatchNo);
  tBPOLCAppntDB.setAppntID(tAppntID);
  if(tBPOLCAppntDB.getInfo())
  {
    tBPOLCAppntSchema = tBPOLCAppntDB.getSchema();
    tBPOLCAppntSchema.setName(request.getParameter("AppntName"));
    tBPOLCAppntSchema.setBirthday(request.getParameter("AppntBirthday"));
    tBPOLCAppntSchema.setSex(request.getParameter("AppntSex"));
    tBPOLCAppntSchema.setMarriage(request.getParameter("AppntMarriage"));
    tBPOLCAppntSchema.setIDType(request.getParameter("AppntIDType"));
    tBPOLCAppntSchema.setIDNo(request.getParameter("AppntIDNo"));
    tBPOLCAppntSchema.setNativePlace(request.getParameter("AppntNativePlace"));
    tBPOLCAppntSchema.setGrpName(request.getParameter("AppntGrpName"));
    tBPOLCAppntSchema.setPosition(request.getParameter("AppntPosition"));
    tBPOLCAppntSchema.setOccupationCode(request.getParameter("AppntOccupationCode"));
    tBPOLCAppntSchema.setOccupationType(request.getParameter("AppntOccupationType"));
    tBPOLCAppntSchema.setSalary(request.getParameter("AppntSalary"));
    tBPOLCAppntSchema.setPostalAddress(request.getParameter("AppntPostalAddress"));
    tBPOLCAppntSchema.setZipCode(request.getParameter("AppntZipCode"));
    tBPOLCAppntSchema.setPhone(request.getParameter("AppntPhone"));
    tBPOLCAppntSchema.setMobile(request.getParameter("AppntMobile"));
    tBPOLCAppntSchema.setEMail(request.getParameter("AppntEMail"));
    tBPOLCAppntSchema.setRemark(request.getParameter("Remark"));
    tBPOLCAppntSchema.setPremScope(request.getParameter("PremScope"));
    tBPOLCAppntSchema.setIDStartDate(request.getParameter("AppIDStartDate"));
    tBPOLCAppntSchema.setIDEndDate(request.getParameter("AppIDEndDate"));
    tBPOLCAppntSchema.setExiSpec(request.getParameter("ExiSpec"));
    tBPOLCAppntSchema.setHomePhone(request.getParameter("AppntHomePhone"));
    tBPOLCAppntSchema.setNativeCity(request.getParameter("AppntNativeCity"));
    //缴费信息
    tBPOLCAppntSchema.setBankCode(request.getParameter("BPOBankCode"));
    tBPOLCAppntSchema.setAccName(request.getParameter("BPOAccName"));
    tBPOLCAppntSchema.setBankAccNo(request.getParameter("BPOBankAccNo"));
    
    // 续期提醒
    tBPOLCAppntSchema.setDueFeeMsgFlag(request.getParameter("DueFeeMsgFlag"));
  }
  
  //修改保单管理系信息
  BPOLCPolDB tBPOLCPolDB = new BPOLCPolDB();
  tBPOLCPolDB.setBPOBatchNo(tBPOBatchNo);
  tBPOLCPolDB.setPrtNo(tPrtNo);
  BPOLCPolSet tBPOLCPolSet = tBPOLCPolDB.query();
  for(int i = 1; i <= tBPOLCPolSet.size(); i++)
  {
    tBPOLCPolSet.get(i).setAppntID(tAppntID);  //外包给的xml文件可能没有AppntID
    tBPOLCPolSet.get(i).setManageCom(request.getParameter("ManageCom"));
    tBPOLCPolSet.get(i).setReceiveDate(request.getParameter("ReceiveDate"));
    tBPOLCPolSet.get(i).setPolApplyDate(request.getParameter("PolApplyDate"));
    tBPOLCPolSet.get(i).setFirstTrialOperator(request.getParameter("FirstTrialOperator"));
    tBPOLCPolSet.get(i).setSaleChnl(request.getParameter("SaleChnl"));
    tBPOLCPolSet.get(i).setAgentCom(request.getParameter("AgentCom"));
    tBPOLCPolSet.get(i).setAgentCode(request.getParameter("GroupAgentCode"));
    tBPOLCPolSet.get(i).setCrs_SaleChnl(request.getParameter("Crs_SaleChnl"));
    tBPOLCPolSet.get(i).setCrs_BussType(request.getParameter("Crs_BussType"));
    tBPOLCPolSet.get(i).setGrpAgentCom(request.getParameter("GrpAgentCom"));
    tBPOLCPolSet.get(i).setGrpAgentCode(request.getParameter("GrpAgentCode"));
    tBPOLCPolSet.get(i).setGrpAgentName(request.getParameter("GrpAgentName"));
    tBPOLCPolSet.get(i).setGrpAgentIDNo(request.getParameter("GrpAgentIDNo"));
    String tAgentName = "";
    if(request.getParameter("AgentCode") != null)
    {
        String tSql = "select Name from LAAgent where GroupAgentCode = '" + request.getParameter("GroupAgentCode") + "'";
        tAgentName = new ExeSQL().getOneValue(tSql);
    }
    tBPOLCPolSet.get(i).setAgentName(tAgentName);
    tBPOLCPolSet.get(i).setCValiDate(request.getParameter("CValiDate"));
    tBPOLCPolSet.get(i).setManageCom(request.getParameter("ManageCom"));
    
    tBPOLCPolSet.get(i).setPayMode(request.getParameter("PayMode"));
    tBPOLCPolSet.get(i).setExPayMode(request.getParameter("ExPayMode"));
  }
  
  
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	VData tVData = new VData();
	tVData.add(tBPOLCAppntSchema);   
	tVData.add(tBPOLCPolSet);                                                 
	tVData.add(tG);
	
	BPOContUI tBPOContUI = new BPOContUI();                               
	if(!tBPOContUI.submitData( tVData, SysConst.UPDATE))                       
	{                                                                               
		Content = " 保存失败，原因是: " + tBPOContUI.mErrors.getErrContent();
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
  
  Content = PubFun.changForHTML(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmitBPO("<%=FlagStr%>","<%=Content%>");
</script>
</html>

