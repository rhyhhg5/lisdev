//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function initQuery() {
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
/*	var strSql = "select DOC_CODE, NUM_PAGES, Operator, InputStartDate, InputStartTime, ManageCom from ES_DOC_MAIN where " + ++queryBug + "=" + queryBug
    				 + " and ( InputState='0' or InputState is null )"              //状态为0，未完成录入
    				 + " and ( Operator is null or Operator='" + operator + "' )"
    				 //+ " and ( SUBSTR(DOC_CODE, 3, 2)='11' or SUBSTR(DOC_CODE, 3, 2)='15' )"
    				 + " and ( SUBSTR(DOC_CODE, 3, 2) in (select trim(code1) from ldcode1 where codetype='scaninput' and code='proposal'))"					//控制该页面能录入哪些扫描件
    				 + " and ( SUBSTR(DOC_CODE, 3, 2) in (select trim(code) from ldcode1 where codetype='scaninputctrl' and code1='" + manageCom + "'))"	//控制当前登陆的机构能看到的扫描件种类
    				 + " and managecom in (select trim(comcode) from ldcode1 where codetype='scaninputctrl' and code1='" + manageCom + "'))"				//控制当前登陆的机构能看到哪些机构的扫描件
    				 ;
*/
// add by guoxiang for 外包需求 add Input_Date,Input_Time                                                    
	var strSql = "select DOC_CODE, NUM_PAGES, Operator, InputStartDate, InputStartTime, ManageCom,Input_Date,Input_Time from ES_DOC_MAIN a, ldcode1 b ,ldcode1 c " 
					+ " where "+queryBug + "=" + queryBug
					+ " and ( InputState='0' or InputState is null ) "
					+ " and ( Operator is null or Operator='" + operator + "' ) "
					+ " and ( substr(doc_code,3,2)=trim(b.code1) and b.codetype='scaninput' and b.code='proposal' )"
					+ " and ( substr(doc_code,3,2)=trim(c.code) and c.codetype='scaninputctrl' and trim(c.code1)='" + manageCom + "' and trim(managecom) like trim(c.comcode)||'%%')"
					+ " ORDER BY  a.Input_Date ASC, a.Input_Time ASC ";


    				 
  //alert(strSql);
	//turnPage.queryModal(strSql, PolGrid);
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    PolGrid.clearData();  
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
}

var prtNo = "";
function scanApplyClick() {
  var i = 0;
  var checkFlag = 0;
  var state = "0";
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	prtNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	
    var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;
    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    var strReturn = window.showModalDialog(urlStr, "", sFeatures);
    
    //打开扫描件录入界面
    //sFeatures = "";
    sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    if (strReturn == "1") window.open("./ProposalInputMain.jsp?LoadFlag=1&prtNo="+prtNo, "", sFeatures);        
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
  
  initQuery();
}

function afterInput() {
  //录入完成，询问是否完成该保单
  var completeFlag = window.confirm("该印刷号对应的保单录入是否全部完成？\n选择是将不再查询出该印刷号！");
  
  if (completeFlag) {
    var type = prtNo.substring(2,4);
    
    //增加扫描件类型的判断，at 2003-11-17 by Minim
    var strSql = "select code from ldcode1 where codetype='scaninput' and code1='" + type + "'";
    var arrScanType = easyExecSql(strSql);
    
    for (i=0; i<arrScanType.length; i++) {
      if ("proposal"==arrScanType[i][0]) {
        var strSql = "select PrtNo from LCPol where PrtNo='" + prtNo + "'";
        break;
      }
      if ("group"==arrScanType[i][0]) {
        var strSql = "select PrtNo from LCGrpPol where PrtNo='" + prtNo + "'";
        break;
      }
    }
    
    //if (type=="11" || type=="15") {
    //  var strSql = "select PrtNo from LCPol where PrtNo='" + prtNo + "'";
    //}
    //else if (type == "12") {
    //  var strSql = "select PrtNo from LCGrpPol where PrtNo='" + prtNo + "'";
    //}
    
    var isComplete = easyExecSql(strSql);
    //alert(isComplete);
    
    if (isComplete) {    
      var state = "1";
      var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;
      var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
      window.showModalDialog(urlStr, "", sFeatures);
    }
    else {
      alert("该投保单（印刷号：" + prtNo + "）还没有进行录入，不能关闭扫描件！");
    }
  }
  
  initQuery();
  
  return true;
}

