<%@page import="com.sinosoft.lis.db.LCGrpContDB"%>
<%@page import="com.sinosoft.lis.db.LCContDB"%>
<%@page import="com.sinosoft.lis.db.LDCode1DB"%>
<%
//程序名称：ProposalCopyInput.jsp
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="java.util.*"%>
	<%@page import="java.io.*"%>	
<%
	System.out.println("-------------");
	
	String FlagStr="True";
	String Content="";
	String Result="";	
	String tAction="modify";
	LDCode1Set tLDCode1Set=new LDCode1Set();
	LDCode1DB tLDCode1DB=new LDCode1DB();
	LDCode1Schema tLDCode1Schema=new LDCode1Schema();
	tLDCode1Set=tLDCode1DB.executeQuery("select * from ldcode1 where codetype='crs'");
	System.out.println(tLDCode1Set.size());
	LCContDB tLcContDB=new LCContDB();
	LCContSet tLcContSet=new LCContSet();
	LCContSchema tLcContSchema=new LCContSchema();
	LCContSchema mLcContSchema=new LCContSchema();
	LCGrpContDB tLcGrpContDB=new LCGrpContDB();
	LCGrpContSet tlcGrpContSet=new LCGrpContSet();
	LCGrpContSchema tlcGrpContSchema=new LCGrpContSchema();
	
	String tPrtNo="";
	String tSaleChnl="";
	String tContNo="";
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");
	VData tVData = new VData();
	CrsnanceBL tCrsnanceBL = new CrsnanceBL();
	String tContType = request.getParameter("conttype");
	if(tLDCode1Set.size()==0){
		FlagStr="Fail";
		Content="未查询到数据,原因是："+tLDCode1DB.mErrors.getError(0).errorMessage;
		
	}else{
		
		for(int i=1;i<=tLDCode1Set.size();i++){			
			tVData.clear();
			tLDCode1Schema=tLDCode1Set.get(i);
			//获取合同号
			tContNo=tLDCode1Schema.getCode1();
			//个单
			if(tContType=="1"){
				tLcContDB.setContNo(tContNo);
				tLcContSet=tLcContDB.query();
				if(tLcContSet.size()==0){				
					
					Content = " 保存失败，原因是: 个单中不存在该单号" ;
					FlagStr = "Fail";
					return;
				}
				mLcContSchema=tLcContSet.get(1);			
				
				tPrtNo = mLcContSchema.getPrtNo();		
				tSaleChnl = mLcContSchema.getSaleChnl();
			}else if(tContType=="2"){
				tLcGrpContDB.setGrpContNo(tContNo);
				tlcGrpContSet=tLcGrpContDB.query();
				if(tlcGrpContSet.size()==0){			
					
					Content = " 保存失败，原因是: 团单中不存在该单号" ;
					FlagStr = "Fail";
					return;
				}
				tlcGrpContSchema=tlcGrpContSet.get(1);
				tPrtNo = tlcGrpContSchema.getPrtNo();		
				tSaleChnl = tlcGrpContSchema.getSaleChnl();
				
			}
			
			String tCrsSaleChnl = request.getParameter("mCrsSaleChnl");
			System.out.println(tCrsSaleChnl);
			String tCrsBussType = request.getParameter("mCrsBussType");
			System.out.println(tCrsBussType);
			String tGrpAgentCode = tLDCode1Schema.getComCode();
			String tGrpAgentName = tLDCode1Schema.getOtherSign();
			String tGrpAgentIdNo = tLDCode1Schema.getCodeAlias();
			String tGrpAgentCom = tLDCode1Schema.getCodeName();
			TransferData tTransferData = new TransferData(); 
			tTransferData.setNameAndValue("tAction", "modify");			
			tTransferData.setNameAndValue("tContType", tContType);
			tTransferData.setNameAndValue("tPrtNo", tPrtNo);
			tTransferData.setNameAndValue("tContNo", tContNo);
			tTransferData.setNameAndValue("tSaleChnl", tSaleChnl);
			tTransferData.setNameAndValue("tCrsSaleChnl", tCrsSaleChnl);
			tTransferData.setNameAndValue("tCrsBussType", tCrsBussType);
			tTransferData.setNameAndValue("tGrpAgentCode", tGrpAgentCode);
			tTransferData.setNameAndValue("tGrpAgentName", tGrpAgentName);
			tTransferData.setNameAndValue("tGrpAgentIdNo", tGrpAgentIdNo);	
			tTransferData.setNameAndValue("tGrpAgentCom", tGrpAgentCom);
			tVData.addElement(tTransferData);
			tVData.add(tGlobalInput);
			if(!tCrsnanceBL.submitData(tVData,tAction))
			{
				Content = " 保存失败，原因是: " + tCrsnanceBL.mErrors.getError(0).errorMessage;
				FlagStr = "Fail";
			}
			else
			{
				Content = " 保存成功! ";
				FlagStr = "Succ";
				tAction="insert";
				if(!tCrsnanceBL.submitData(tVData,tAction))
				{
					Content = " 保存失败，原因是: " + tCrsnanceBL.mErrors.getError(0).errorMessage;
					FlagStr = "Fail";					
					return;
				}
				
			}	
		
		}
		for(int i=1;i<=tLDCode1Set.size();i++){
			tLDCode1Schema=tLDCode1Set.get(i);
			//获取合同号
			tContNo=tLDCode1Schema.getCode1();
			tLDCode1DB.setCode1(tContNo);
			tLDCode1DB.setCodeType("crs");
			tLDCode1DB.setCode("CS");
			tLDCode1DB.delete();
			
		}
		
		
		/*tvData.add(tLcContSet);
		tvData.add(tG);
		String tOperate="UPDATE";
		CrsnanceBL tCrsnanceBL = new CrsnanceBL();
		System.out.println("before submit");
		if(!tCrsnanceBL.submitData(tvData,tOperate))
		{
			Content = " 保存失败，原因是: " + tCrsnanceBL.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		else
		{
			Content = " 保存成功! ";
			FlagStr = "Succ";
		}*/
		
		
	}
	
	
	
	

%>                      
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>


