<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：TempFeeWithdrawInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="TempFeeWithdrawInput.js"></SCRIPT>
  <%@include file="TempFeeWithdrawInit.jsp"%>
  
  <title>暂交费信息 </title>
</head>

<body  onload="initForm();" >
  <form action="./TempFeeWithdrawSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 暂交费信息部分 fraSubmit-->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
  		</tr>
  	</table>
  	
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        暂交费收据号码
      </TD>
      <TD  class= input>
        <Input class= common name=TempFeeNo >
      </TD>
      <TD  class= title>
        险种编码
      </TD>
      <TD  class= input>
      	<Input class=codeNo name=RiskCode ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('RiskCode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true >
        
      </TD>
      <TD  class= title>
        交费金额
      </TD>
      <TD  class= input>
        <Input class= common name=PayMoney >
      </TD>
    </TR>
    <TR  class= common>
      <TD  class= title>
        交费日期
      </TD>
      <TD  class= input>
        <Input class= common name=PayDate >
      </TD>
      <TD  class= title>
        到帐日期
      </TD>
      <TD  class= input>
        <Input class= common name=EnterAccDate >
      </TD>
      <TD  class= title>
        管理机构
      </TD>
      <TD  class= input>
      	<Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
        
      </TD>
    </TR>
    <TR  class= common>
      <TD  class= title>
        代理人编码
      </TD>
      <TD  class= input>
      	<Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCode',[this,AgentCodeName],[0,1], null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeName],[0,1], null, fm.all('ManageCom').value, 'ManageCom');"><input class=codename name=AgentCodeName readonly=true >
        
      </TD>
      <TD  class= title>
        代理人组别
      </TD>
      <TD  class= input>
        <Input class= common name=AgentGroup >
      </TD>
      <TD  class= title>
        操作员
      </TD>
      <TD  class= input>
        <Input class= common name=Operator >
      </TD>
    </TR>
    <TR  class= common>
      <TD  class= title>
        印刷号
      </TD>
      <TD  class= input>
        <Input class=common name=PrtNo >
      </TD>

    </TR>
    </table>
    <table align=right>
      <TR>
        <TD>
          <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="tempFeeNoQuery();">
        </TD>
        <TD>
          <INPUT VALUE="打印暂交费退费实付凭证" class= cssButton TYPE=button onclick="window.open('./TempFeeWithdrawQuery.jsp');">
        </TD>
      </TR>
    </table>
    <br><br>
    
    <!-- 暂交费信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJTempFee1);">
    		</td>
    		<td class= titleImg>
    			 暂交费信息 
    		</td>
    	</tr>
    </table>
	<Div  id= "divLJTempFee1" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanFeeGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
	
  <Div id= "divPage" align=center style= "display: 'none' ">
  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
 <table   class= common>
    <tr class= common>
      <TD  class= title>暂交费付费方式</TD>
          <TD  class= input><Input class=codeNo name=qPayMode  ondblclick="return showCodeList('paymode3',[this,qPayModeName],[0,1]);" onkeyup="return showCodeListKey('PayMode3',[this,qPayModeName],[0,1]);"><input class=codename name=qPayModeName readonly=true ></TD> 
      <td align=right>
        <INPUT VALUE="退费确认" class= cssButton TYPE=button onclick="submitForm();">
      </td>
    </tr>
  </table>        										
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    
  <form action="./TempFeeWithdrawPrintMain.jsp" method=post name=fm2 target="_blank">
    <Input type=hidden name=PrtData >
  </form>
  
</body>
</html>
