<%
//程序名称：GrpPayPlanInput.jsp
//程序功能：
//创建日期：2004-04-10
//创建人  ：mqhu
//更新记录：  更新人    更新日期    更新原因/内容 
%>


<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<html>
<%
 GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
String ProposalGrpContNo = request.getParameter("ProposalGrpContNo");
String PrtNo = request.getParameter("PrtNo");
String Flag = request.getParameter("Flag");
%>
<head>
<script>
var ProposalGrpContNo ="<%=ProposalGrpContNo%>";
var Flag = "<%=Flag%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT> 
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="GrpPayPlanInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="GrpPayPlanInit.jsp"%>
<title>约定缴费计划</title>
</head>
<body onload="initForm();initElementtype();">
	<form method=post name=fm target="fraSubmit" action="GrpPayPlanSave.jsp">		
		<Div  id= "divGrpPayPlan" style= "display: ''">	
			<table>
	    	  	<tr>
		        	<td class=common>
					    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
		    		</td>
		    		<td class= titleImg>
		    			 缴费计划
		    		</td>
	    		</tr>
	    	</table>
	    	<table  class= common>
	        	<tr  class= common>
	    	  		<td text-align: left colSpan=1>
						<span id="spanGrpPayPlanGrid" ></span> 
					</td>
				</tr>
			</table>
			<Div  id= "divGrpPayPlanSave" style= "display: ''" align= left> 
				<input class=cssButton type=button value="保  存" onclick="this.disabled=true; setEnable('submitButton');Submit();" id="submitButton" >
				<input class=cssButton type=button value="全部删除" onclick="this.disabled=true; setEnable('delButton');DelGrpPayPlan();" id="delButton" >  	
			</Div>
			<br>
		 	<table>
		    	<tr>
		        	<td class=common>
					    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
		    		</td>
		    		<td class= titleImg>
		    			 缴费计划明细
		    		</td>
		    	</tr>
		     </table>
    	     <table  class= common>
	        	<tr  class= common>
	    	  		<td text-align: left colSpan=1>
						<span id="spanGrpPayPlanDetailGrid"></span> 
					</td>
				</tr>
		     </table>
		     <Div  id= "divGrpPayPlanSave" style= "display: ''" align= left> 
				<input class=cssButton type=button value="保  存" onclick="this.disabled=true; setEnable('submitButtonDetail');SubmitDetail();" id="submitButtonDetail" > 	
			</Div>
			<input type = hidden name = ProposalGrpContNo>
			<input type = hidden name = fmtransact>
			<br>
			<table>
				<tr>
					<td>
						<font color = red  size = 2>
						注：<br>
						1、必须录入首期缴费计划。<br>
						2、[缴费计划]必须一次性录入并保存。<br>
						3、[缴费计划]如需在保存后进行修改，需全部删除[缴费计划]，再重新录入。<br>
						4、约定缴费时间格式：YYYY-MM-DD，如2011-01-01。<br>
						5、缴费计划的计划编码无需填写，保存缴费计划后，会自动显示。<br>
						6、缴费计划需按顺序填写，即第一条数据为第一期缴费计划，第二条为第二期缴费计划，以此类推。<br>
						</font>
					</td>
				</tr>
			</table>
       </Div>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>