var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick() {
	
	if (!verifyInput2()) {
		return false;
	}
	
	if(!check()){
		return false;
	}

  // 书写SQL语句
	var strSQL1 = "";
	var strSQL2 = "";
	initRelatedPartyGrid();
	strSQL1 = "select b.grpcontno,c.linkman1,d.idno,a.managecom,a.cvalidate,b.riskcode,b.prem " 
			+ "from lcgrpcont a,lcgrppol b,lcgrpaddress c,lcgrpappnt d " 
			+ "where a.grpcontno=b.grpcontno " 
			+ "and a.grpcontno=d.grpcontno " 
			+ "and a.appntno=c.customerno " 
			+ "and a.addressno=c.addressno " 
			+ "and a.appflag='1' "
			+ "and a.stateflag='1'"
			+ "and d.relatedtags='1' "
			+ getWherePart("d.idno","IDNo")
			+ getWherePart("a.managecom","ManageCom","like")
			+ getWherePart("a.signdate","StartDate",">=")
			+ getWherePart("a.signdate","EndDate","<=")
			+ getWherePart("a.salechnl","SaleChnl")
			+ getWherePart("b.prem","PremX",">=")
			+ getWherePart("b.prem","PremS","<=")
			;
	strSQL2 = "select b.grpcontno,d.name,'',a.managecom,a.cvalidate,b.riskcode,b.prem " 
		+ "from lcgrpcont a,lcgrppol b,lcgrpaddress c,lcgrpappnt d " 
		+ "where a.grpcontno=b.grpcontno " 
		+ "and a.grpcontno=d.grpcontno " 
		+ "and a.appntno=c.customerno " 
		+ "and a.addressno=c.addressno " 
		+ "and a.appflag='1' "
		+ "and a.stateflag='1'"
		+ "and d.relatedtags='2' "
		+ getWherePart("a.managecom","ManageCom","like")
		+ getWherePart("a.signdate","StartDate",">=")
		+ getWherePart("a.signdate","EndDate","<=")
		+ getWherePart("a.salechnl","SaleChnl")
		+ getWherePart("b.prem","PremX",">=")
		+ getWherePart("b.prem","PremS","<=")
		;
	
	strSql3="select a.contno,c.appntname,c.idno,a.managecom,a.cvalidate,b.riskcode,b.prem "
		+ " from lccont a,lcpol b,lcappnt c "
		+ " where a.contno=b.contno "
		+ " and a.contno=c.contno "
		+ " and a.appntno=c.appntno "
		+ " and a.conttype='1' " 
		+ " and a.appflag='1' "
		+ " and a.stateflag='1' "
		+ " and c.relatedtags='1' " 
		+ getWherePart("c.idno","IDNo")
		+ getWherePart("a.managecom","ManageCom","like")
		+ getWherePart("a.signdate","StartDate",">=")
		+ getWherePart("a.signdate","EndDate","<=")
		+ getWherePart("a.salechnl","SaleChnl")
		+ getWherePart("b.prem","PremX",">=")
		+ getWherePart("b.prem","PremS","<=");
	if(fm.all("RelatedType").value=="1"){
		var strSQL="";
		if(fm.all("Name").value!=null && fm.all("Name").value!=""){
			strSQL1 += "and c.linkman1 like '%'||'"+fm.all("Name").value+"'||'%' ";
			strSql3 += " and a.appntname like '%'||'"+fm.all("Name").value+"'||'%' with ur"
		}
		strSQL=strSQL1 + " union " + strSql3;
		fm.all("querySql").value=strSQL;
		turnPage.queryModal(strSQL, RelatedPartyGrid);
	}else if(fm.all("RelatedType").value=="2"){
		if(fm.all("Name").value!=null && fm.all("Name").value!=""){
			strSQL2 += "and d.name like '%'||'"+fm.all("Name").value+"'||'%' with ur";
		}
		fm.all("querySql").value=strSQL2;
		turnPage.queryModal(strSQL2, RelatedPartyGrid);
	}else{
		var strSQL = "";
		if(fm.all("Name").value!=null && fm.all("Name").value!=""){
			strSQL =  strSQL1 + "and c.linkman1 like '%'||'"+fm.all("Name").value+"'||'%' ";
			strSQL = strSQL + " union " + strSql3 + "and c.appntname like '%'||'"+fm.all("Name").value+"'||'%' " + " union ";
			strSQL = strSQL + strSQL2 + "and d.name like '%'||'"+fm.all("Name").value+"'||'%' with ur";
		}else{
			strSQL = strSQL1 +" union " + strSql3 + " union " + strSQL2 + "with ur ";
		}
		fm.all("querySql").value=strSQL;
		turnPage.queryModal(strSQL, RelatedPartyGrid);
	}
	
	if(RelatedPartyGrid.mulLineCount<1){
		alert("没有查询到相应数据！");
	}
}

function check(){
	if(fm.all("Name").value!=null && fm.all("Name").value!=""){
		if(fm.all("RelatedType").value==null || fm.all("RelatedType").value==""){
			alert("交易对象不为空时，关联方类别也不能为空！");
			return false;
		}
	}
//	var tStartDate = fm.all('StartDate').value;
//	var tEndDate = fm.all('EndDate').value;
//	if(dateDiff(tStartDate,tEndDate,"M") > 3){
//		alert("查询时间间隔最长为三个月！");
//		return false;
//	}
	return true;
}

function DoNewDownload(){
	if( verifyInput() == false ) return false;
	 // 书写SQL语句
	var querySql = fm.all("querySql").value;
	if(querySql != null && querySql != "" && RelatedPartyGrid.mulLineCount > 0){
		var formAction = fm.action;
		fm.action = "RelatedListReport.jsp";
		fm.submit();
		fm.target = "fraSubmit";
	    fm.action = formAction;
	}else{
		alert("请先执行查询操作，再下载！");
	    return false;
	}
}

