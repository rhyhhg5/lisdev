<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	String tGroupNo = request.getParameter("GroupNo");
	String tBatchNo = null;
	
	if(tGroupNo!=null && !"".equals(tGroupNo)){
		try{
			//生成批次号
			String tNoLimit = PubFun.getCurrentDate2()+PubFun.getCurrentTime2();
			tBatchNo = "TAX" + tNoLimit + PubFun1.CreateMaxNo("T"+tNoLimit,2);
			System.out.println("tBatchNo : " + tBatchNo);
			
			if(tBatchNo!=null && !"".equals(tBatchNo)){
				LSBatchInfoSchema tLSBatchInfoSchema = new LSBatchInfoSchema();
				tLSBatchInfoSchema.setBatchNo(tBatchNo);
				tLSBatchInfoSchema.setGrpNo(tGroupNo);
				
				VData tVData = new VData();
				tVData.add(tLSBatchInfoSchema);
				tVData.add(tG);
				
				TaxApplyUI tTaxApplyUI = new TaxApplyUI();
				if(!tTaxApplyUI.submitData(tVData,null)){
					Content = "申请失败，原因是：" + tTaxApplyUI.mErrors.getFirstError();
					FlagStr = "Succ";
				}else{
					Content = "申请成功！";
					FlagStr = "Succ";
				}
			}else{
				Content = "申请批次号失败";
				FlagStr = "Fail";
			}
		}catch(Exception e){
			Content = "申请失败！";
			FlagStr = "Fail";
			e.printStackTrace();
		}
	}
%>

<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=tBatchNo%>");
	</script>
</html>