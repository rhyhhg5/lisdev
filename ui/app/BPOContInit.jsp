<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：
//程序功能：外包保单修改清单js文件
//创建日期：2007-10-19 16:52
//创建人  ：YangYalin
//更新记录：更新人    更新日期     更新原因/内容
%>

<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	String tCurrentDate = PubFun.getCurrentDate();
	
	String tBPOBatchNo = request.getParameter("BPOBatchNo");
	String tMissionID = request.getParameter("MissionID");
	String tPrtNo = request.getParameter("PrtNo");
%>                            

<script language="JavaScript">
  
  var tManageCom = "<%=tGI.ComCode%>";
  var tCurrentDate = "<%=tCurrentDate%>";
  
  var tBPOBatchNo = "<%=tBPOBatchNo%>";
  var tMissionID = "<%=tMissionID%>";
  var tPrtNo = "<%=tPrtNo%>";

	var intlContInsuredInput = 1;
	
	var tIntlFlag = "";  //国际业务标志：0：菲国际业务；1：国际业务
	var tCardFlag = "";  //卡单标志：6：银行保险；7：国际业务；8：简易万能险
	var tWrapType = "";  //套餐类型，详见LDWrap描述

function initForm()
{
  try
  {
    initHiddenInputBox();
    
    //initIssueGrid();
    
    initInsuredGrid();
    
    //抵达国家
    initNationGrid();
    
    initWrapGrid();
    
    initRiskWrapGrid();
    
    initPolGrid();
    initBnfGrid();
    
    initImpartGrid();
    initImpartDetailGrid();
    
    initExtend();
    
    queryImportLog();
    queryContManageInfo();
    controlAgentComDisplay("");
    queryAppntInfo();
    queryFeeInfo();
    queryInsuredListInfo();
    
    initElementtype();
    
    initDiv();  //如果境外救援也走本平台，则需要放开
    showAllCodeName();
    
  }
  catch(ex)
  {
    alert("初始化界面错误，" + ex.message);
  }
}

// 被保人信息列表的初始化
function initInsuredGrid()
{
  var iArray = new Array();
      
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            		//列宽
    iArray[0][2]=20;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="合同ID";         		//列名
    iArray[1][1]="20px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="ContID";

    iArray[2]=new Array();
    iArray[2][0]="被保人ID";         		//列名
    iArray[2][1]="25px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="InsuredID";

    iArray[3]=new Array();
    iArray[3][0]="姓名";         		//列名
    iArray[3][1]="60px";            		//列宽
    iArray[3][2]=80;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="Name"; 

    iArray[4]=new Array();
    iArray[4][0]="生日";         		//列名
    iArray[4][1]="40px";            		//列宽
    iArray[4][2]=80;            			//列最大值
    iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="Birthday"; 

    iArray[5]=new Array();
    iArray[5][0]="性别";         		//列名
    iArray[5][1]="20px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[5][21]="SexName";

    iArray[6]=new Array();
    iArray[6][0]="证件类型代码";         		//列名
    iArray[6][1]="60px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[6][21]="IDType";

    iArray[7]=new Array();
    iArray[7][0]="证件类型";         		//列名
    iArray[7][1]="30px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[7][21]="IDTypeName";

    iArray[8]=new Array();
    iArray[8][0]="证件号码";         		//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[8][21]="IDNo";
    
    InsuredGrid = new MulLineEnter( "fm" , "InsuredGrid" ); 
    //这些属性必须在loadMulLine前
    InsuredGrid.mulLineCount = 0;   
    InsuredGrid.displayTitle = 1;
    InsuredGrid.locked = 1;
    InsuredGrid.canSel = 1;
    InsuredGrid.canChk = 0;
    InsuredGrid.hiddenPlus=1;   
  	InsuredGrid.hiddenSubtraction=1;
  	InsuredGrid.selBoxEventFuncName = "getOneInsuredInfo";
    InsuredGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("初始化" + ex.message);
  }
}

//险种信息列表的初始化
function initPolGrid()
{
  var iArray = new Array();
      
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            		//列宽
    iArray[0][2]=20;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="主险ID";         		//列名
    iArray[1][1]="40px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="MainPolID";

    iArray[2]=new Array();
    iArray[2][0]="合同ID";         		//列名
    iArray[2][1]="0";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="ContID";

    iArray[3]=new Array();
    iArray[3][0]="被保人ID";         		//列名
    iArray[3][1]="50px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="InsuredID";

    iArray[4]=new Array();
    iArray[4][0]="险种ID";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=80;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="PolID"; 

    iArray[5]=new Array();
    iArray[5][0]="险种名称";         		//列名
    iArray[5][1]="200px";            		//列宽
    iArray[5][2]=80;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="RiskName"; 

    iArray[6]=new Array();
    iArray[6][0]="险种编码";         		//列名
    iArray[6][1]="50px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[6][4]="RiskCode";
    iArray[6][5]="6|5";
    iArray[6][6]="0|1";
    iArray[6][21]="RiskCode";

    iArray[7]=new Array();
    iArray[7][0]="档次/保额";         		//列名
    iArray[7][1]="60px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[7][21]="Mult";

    iArray[8]=new Array();
    iArray[8][0]="保险期间";         		//列名
    iArray[8][1]="50px";            		//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[8][21]="InsuYear";

    iArray[9]=new Array();
    iArray[9][0]="保险期间标志";         		//列名
    iArray[9][1]="70px";            		//列宽
    iArray[9][2]=100;            			//列最大值
    iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[9][21]="InsuYearFlag";

    iArray[10]=new Array();
    iArray[10][0]="缴费年期";         		//列名
    iArray[10][1]="50px";            		//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[10][21]="PayEndYear";

    iArray[11]=new Array();
    iArray[11][0]="缴费年期标志";         		//列名
    iArray[11][1]="70px";            		//列宽
    iArray[11][2]=100;            			//列最大值
    iArray[11][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[11][21]="PayEndYearFlag";

    iArray[12]=new Array();
    iArray[12][0]="缴费频次";         		//列名
    iArray[12][1]="50px";            		//列宽
    iArray[12][2]=100;            			//列最大值
    iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[12][4]="payintv";
    iArray[12][5]="12|13";
    iArray[12][6]="0|1";
    iArray[12][21]="PayIntv";

    iArray[13]=new Array();
    iArray[13][0]="缴费频次名";         		//列名
    iArray[13][1]="50px";            		//列宽
    iArray[13][2]=100;            			//列最大值
    iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[13][21]="PayIntvName";

    iArray[14]=new Array();
    iArray[14][0]="期交保费";         		//列名
    iArray[14][1]="50px";            		//列宽
    iArray[14][2]=100;            			//列最大值
    iArray[14][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[14][21]="Prem";
    
    iArray[15]=new Array();
    iArray[15][0]="追加保费";               //列名
    iArray[15][1]="50px";                   //列宽
    iArray[15][2]=100;                      //列最大值
    iArray[15][3]=1;                        //是否允许输入,1表示允许，0表示不允许 
    iArray[14][21]="SupplementaryPrem";
    
    PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
    //这些属性必须在loadMulLine前
    PolGrid.mulLineCount = 0;   
    PolGrid.displayTitle = 1;
    PolGrid.locked = 0;
    PolGrid.canSel = 0;
    PolGrid.canChk = 0;
    PolGrid.hiddenPlus=0;   
  	PolGrid.hiddenSubtraction=0;
    PolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("初始化" + ex.message);
  }
}


//受益人信息列表的初始化
function initBnfGrid()
{
  var iArray = new Array();
      
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            		//列宽
    iArray[0][2]=20;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="合同ID";         		//列名
    iArray[1][1]="0";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="ContID";

    iArray[2]=new Array();
    iArray[2][0]="被保人ID";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="InsuredID";

    iArray[3]=new Array();
    iArray[3][0]="受益人类别";         		//列名
    iArray[3][1]="50px";            		//列宽
    iArray[3][2]=80;            			//列最大值
    iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="bnftype";
    iArray[3][5]="3|4";
    iArray[3][6]="0|1";
    iArray[3][21]="BnfType"; 
    
    iArray[4]=new Array();
    iArray[4][0]="类别名";         		//列名
    iArray[4][1]="50px";            		//列宽
    iArray[4][2]=80;            			//列最大值
    iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="BnfTypeName"; 
    
    iArray[5]=new Array();
    iArray[5][0]="姓名";         		//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=80;            			//列最大值
    iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="Name"; 
    
    iArray[6]=new Array();
    iArray[6][0]="性别";         		//列名
    iArray[6][1]="20px";            		//列宽
    iArray[6][2]=80;            			//列最大值
    iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][4]="sex";
    iArray[6][5]="6|7";
    iArray[6][21]="Sex";
    
    iArray[7]=new Array();
    iArray[7][0]="性别名";         		//列名
    iArray[7][1]="20px";            		//列宽
    iArray[7][2]=80;            			//列最大值
    iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="SexName";
    
    iArray[8]=new Array();
    iArray[8][0]="生日";         		//列名
    iArray[8][1]="40px";            		//列宽
    iArray[8][2]=80;            			//列最大值
    iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="Birthday";

    iArray[9]=new Array();
    iArray[9][0]="证件类型";         		//列名
    iArray[9][1]="40px";            		//列宽
    iArray[9][2]=100;            			//列最大值
    iArray[9][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[9][4]="idtype";
    iArray[9][5]="9|10";
    iArray[9][21]="IDType";
    
    iArray[10]=new Array();
    iArray[10][0]="证件类型名";         		//列名
    iArray[10][1]="40px";            		//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[10][21]="IDTypeName";

    iArray[11]=new Array();
    iArray[11][0]="证件号码";         		//列名
    iArray[11][1]="120px";            		//列宽
    iArray[11][2]=100;            			//列最大值
    iArray[11][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[11][21]="IDNo";

    iArray[12]=new Array();
    iArray[12][0]="与被保人关系";         		//列名
    iArray[12][1]="60px";            		//列宽
    iArray[12][2]=80;            			//列最大值
    iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[12][4]="relation";
    iArray[12][5]="12|13";
    iArray[12][6]="0|1";
    iArray[12][21]="RelationToInsured";

    iArray[13]=new Array();
    iArray[13][0]="关系名称";         		//列名
    iArray[13][1]="50px";            		//列宽
    iArray[13][2]=100;            			//列最大值
    iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[13][21]="RelationToInsuredName";

    iArray[14]=new Array();
    iArray[14][0]="受益比例";         		//列名
    iArray[14][1]="50px";            		//列宽
    iArray[14][2]=100;            			//列最大值
    iArray[14][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[14][21]="BnfLot";

    iArray[15]=new Array();
    iArray[15][0]="受益顺位";         		//列名
    iArray[15][1]="50px";            		//列宽
    iArray[15][2]=100;            			//列最大值
    iArray[15][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[15][4]="occupationtype";
    iArray[15][5]="15|16";
    iArray[15][21]="BnfGrade";

    iArray[16]=new Array();
    iArray[16][0]="受益顺位名";         		//列名
    iArray[16][1]="50px";            		//列宽
    iArray[16][2]=100;            			//列最大值
    iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[16][21]="BnfGradeName";

    iArray[17]=new Array();
    iArray[17][0]="序号";         		//列名
    iArray[17][1]="50px";            		//列宽
    iArray[17][2]=100;            			//列最大值
    iArray[17][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[17][21]="SequenceNo";
    
    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" ); 
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 0;   
    BnfGrid.displayTitle = 1;
    BnfGrid.locked = 0;
    BnfGrid.canSel = 0;
    BnfGrid.canChk = 0;
    BnfGrid.hiddenPlus=0;   
  	BnfGrid.hiddenSubtraction=0;
    BnfGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("初始化" + ex.message);
  }
}

//抵达国家
function initNationGrid()
{
  var iArray = new Array();

  iArray[0]=new Array();
  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  iArray[0][1]="30px";         			//列宽
  iArray[0][2]=10;          			//列最大值
  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[1]=new Array();
  iArray[1][0]="国家代码";    	//列名
  iArray[1][1]="100px";            		//列宽
  iArray[1][2]=100;            			//列最大值
  iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
  iArray[1][4]="ldnation";
  iArray[1][5]="1|2";
  iArray[1][6]="0|1";
  iArray[1][9]="国家代码|len<=20";
  iArray[1][15]="chinesename";
  iArray[1][17]="2";
  iArray[1][18]=250;

  iArray[2]=new Array();
  iArray[2][0]="国家名称";    	//列名
  iArray[2][1]="100px";            		//列宽
  iArray[2][2]=100;            			//列最大值
  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


  NationGrid = new MulLineEnter( "fm" , "NationGrid" );
  //这些属性必须在loadMulLine前
  NationGrid.mulLineCount = 0;
  NationGrid.displayTitle = 1;
  NationGrid.locked = 0;
  NationGrid.canSel = 0;
  NationGrid.canChk = 0;
  NationGrid.hiddenPlus = 0;
  NationGrid.hiddenSubtraction = 0;
  NationGrid.loadMulLine(iArray);
}

//被保人险种信息列表初始化
function initWrapGrid(){
    var iArray = new Array();
      
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		        //列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="套餐编码";         		//列名
      iArray[1][1]="30px";            		        //列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  	  iArray[1][21]="RiskWrapCode";
      
      iArray[2]=new Array();
      iArray[2][0]=" 套餐名称";         		//列名
      iArray[2][1]="80px";            		        //列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
  	  iArray[2][21]="WrapName";           			//是否允许输入,1表示允许，0表示不允许           
      
      WrapGrid = new MulLineEnter( "fm" , "WrapGrid" ); 
      WrapGrid.mulLineCount = 0;   
      WrapGrid.displayTitle = 1;
      WrapGrid.canChk =1;
      WrapGrid. hiddenPlus=1;
      WrapGrid. hiddenSubtraction=1;
      WrapGrid.loadMulLine(iArray);  
    }
    catch(ex) {
      alert(ex);
    }
}

// 保障计划列表的初始化
function initRiskWrapGrid(riskWrapCodes)
{
  riskWrapCodes = ((riskWrapCodes == null || riskWrapCodes == "")? "''" : riskWrapCodes);
  
  try{
    var strsql = "select distinct 'Factor',ChooseFlag,CalFactor,CalFactorName,function,factorType "
                +"from LDRiskDutyWrap a,LDWrap b "
                +"where a.RiskWrapCode=b.RiskWrapCode "
                + "   and b.WrapType = '" + tWrapType + "' "
                + "   and b.RiskWrapCode in (" + riskWrapCodes + ") ";

  	var arr = easyExecSql(strsql);
  	if(!arr){
  		arr = new Array();
  		arr[0] = new Array();
  	}
  	
  	var displayFlag = ("8" == tCardFlag ? "3" : "0");
  	
  	var iArray = new Array();
  	iArray[0]=new Array();
  	iArray[0][0]="序号";
  	iArray[0][1]="30px";
  	iArray[0][2]=10;
  	iArray[0][3]=0;
  	
  	iArray[1]=new Array();
  	iArray[1][0]="套餐编码";
  	iArray[1][1]="50px";
  	iArray[1][2]=10;
  	iArray[1][3]=displayFlag;
  	iArray[1][21]="RiskWrapCode";
  	
  	iArray[2]=new Array();
  	iArray[2][0]="套餐名称";
  	iArray[2][1]="150px";
  	iArray[2][2]=10;
  	iArray[2][3]=displayFlag;
  	iArray[2][21]="WrapName";
  	
  	iArray[3]=new Array();
  	iArray[3][0]="险种编码";
  	iArray[3][1]="50px";
  	iArray[3][2]=10;
  	iArray[3][3]= ("6" == tWrapType ? 3 : 0);
  	iArray[3][21]="RiskCode";
  	
  	iArray[4]=new Array();
  	iArray[4][0]="险种名称";
  	iArray[4][1]="250px";
  	iArray[4][2]=10;
  	iArray[4][3]= ("6" == tWrapType ? 3 : 0);
  	iArray[4][21]="RiskName";
  	
  	iArray[5]=new Array();
  	iArray[5][0]="责任编码";
  	iArray[5][1]="50px";
  	iArray[5][2]=10;
  	iArray[5][3]= ("6" == tWrapType ? 3 : displayFlag);
  	iArray[5][21]="DutyCode";
  	
  	iArray[6]=new Array();
  	iArray[6][0]="责任名称";
  	iArray[6][1]="150px";
  	iArray[6][2]=10;
  	iArray[6][3]= ("6" == tWrapType ? 3 : displayFlag);;
  	iArray[6][21]="DutyName";
  	
  	iArray[7]=new Array();
  	iArray[7][0]="折扣";
  	iArray[7][1]="50";
  	iArray[7][2]=10;
  	iArray[7][3]= ("6" == tWrapType ? 3 : displayFlag);;
  	iArray[7][21]="FloatRate";
  	
  	fm.RiskWrapColFix.value = 7;  //固定列数
  	
  	if("9" == tWrapType || "8" == tWrapType)
  	{
    	strSqlMain = "select distinct a.RiskWrapCode,a.WrapName, "
    	            + "   b.RiskCode, (select RiskName from LMRisk where RiskCode = b.RiskCode), "
    	            + "   b.DutyCode, (select DutyName from LMDuty where DutyCode = b.DutyCode), "
    	            + "   (select replace(varchar(decimal(round(Prem/StandPrem, 2), 10, 2)), 'null', '') from LCPol where PrtNo = '" + fm.PrtNo.value + "'   and InsuredNo = '" + fm.InsuredNo.value + "'   and RiskCode = b.RiskCode) "
    }
    else
    {
      //除了国际业务和万能险，其他的套餐险种、责任和折扣都不需要
      strSqlMain = "select a.RiskWrapCode,a.WrapName, '000000' RiskCode, '000000', '000000' DutyCode, '000000', '000000' ";
    }
  	
  	var i = fm.RiskWrapColFix.value;
  	for(var m=0 ; m<arr.length ; m++)
  	{
  		for(var n=0 ; n<arr[m].length ; n++)
  		{
  			if(arr[m][n]=="Factor")
  			{
  				var display = 0;
  				if(arr[m][n+1] == "6")
  				{
  					display = 1;
  				}
  				if(arr[m][n+1] == "7")
  				{
  					display = 3;
  				}
  			}
  			else
  			{
  				break
  			}
  			var ChooseFlag = arr[m][n+1];
  			var CalFactor = arr[m][n+2];
  			var CalFactorName = arr[m][n+3];
  			var tFunction = arr[m][n+4];
  			var FactorType = arr[m][n+5];
  
  			i++;
  			iArray[i]=new Array();
  			iArray[i][0]="标志位";
  			iArray[i][1]="30px";
  			iArray[i][2]=10;
  			iArray[i][3]=3;
  			iArray[i][21]=ChooseFlag;
  			
  			i++;
  			iArray[i]=new Array();
  			iArray[i][0]="要素";
  			iArray[i][1]="30px";
  			iArray[i][2]=10;
  			iArray[i][3]=3;
  			iArray[i][21]=CalFactor;
  			
  			i++;
  			iArray[i]=new Array();
  			iArray[i][0]=CalFactorName
  			iArray[i][1]="60px";
  			iArray[i][2]=10;
  			iArray[i][3]=display;//控制是否显示此要素
  			iArray[i][21]=CalFactorName;//控制是否显示此要素
  			
  		  if(FactorType == "2")
  		  {
  		    //险种要素
  		  	strSqlMain += ",'Factor','"+CalFactor+"',''";
  		  }
  		  else if(FactorType == "1")
  		  {
  		  	strSqlMain += ",'Factor','"+CalFactor+"',(select ";
  				
  		    if("9" == tWrapType || "8" == tWrapType)
  		    {
    		    strSqlMain +=  tFunction + "(d." + CalFactor + ") from LCPol c, LCDuty d "
    		                   + "where c.PolNo = d.PolNo "
    		                   + "    and d.DutyCode = b.DutyCode "
    		                   + "    and c.prtno = '" + fm.PrtNo.value + "' "
    		                   + "    and c.InsuredNo = '" + fm.InsuredNo.value + "' "
    		                   + ") ";
  		    }
  		    else if("6" == tWrapType)
  		    {
    		    strSqlMain +=  "CalFactorValue from BPOLCRiskDutyWrap b where CalFactor='"+CalFactor+"' and prtno='" + fm.PrtNo.value
    		    						+ "' and (Riskcode = '000000' or Riskcode in (select Riskcode from LDRiskWrap where RiskWrapCode=b.RiskWrapCode))"
    		    						+ " and b.RiskWrapCode = a.riskWrapCode and BPOBatchNo = '" + fm.BPOBatchNo.value + "')";
  		    }
  		    else
  		    {
    		    strSqlMain +=  tFunction + "(d." + CalFactor + ") from LCPol c,LCRiskDutyWrap b,LCDuty d "
    		    						+ "where c.ContNo=d.ContNo and b.DutyCode=d.DutyCode and b.CalFactor='"+CalFactor
    		    						+ "' and c.prtno='"+fm.PrtNo.value+"' and b.RiskWrapCode=a.RiskWrapCode and c.ContNo=b.ContNo "
    		    						+ "and c.Riskcode=b.RiskCode and c.Riskcode in (select Riskcode from LDRiskWrap where RiskWrapCode=a.RiskWrapCode))";
  		    }
  		  }
  		}
  	}
  	
  	if("9" == tWrapType || "8" == tWrapType)
    {
  		strSqlMain += " from LDWrap a, LDRiskDutyWrap b "
                		+ "where a.RiskWrapCode = b.RiskWrapCode "
                		+ "   and a.WrapType ='" + tWrapType + "' "
                		+ "   and a.WrapProp='Y' ";
    }
    else
    {
      strSqlMain += " from LDWrap a where a.WrapType = '" + tWrapType + "' and a.WrapProp='Y' ";
    }
              		
		fm.RiskWrapCol.value=i;  //最大列数
		
		RiskWrapGrid = new MulLineEnter( "fm" , "RiskWrapGrid" );
		//这些属性必须在loadMulLine前
		RiskWrapGrid.mulLineCount = 0;
		RiskWrapGrid.displayTitle = 1;
		RiskWrapGrid.locked = 0;
		RiskWrapGrid.canSel = 0;
		RiskWrapGrid.canChk = 1;
		RiskWrapGrid.hiddenPlus = 1;
		RiskWrapGrid.hiddenSubtraction = 1;
		RiskWrapGrid.loadMulLine(iArray);
	}
	catch (ex)
	{
		alert(ex.message);
	}
}

</script>