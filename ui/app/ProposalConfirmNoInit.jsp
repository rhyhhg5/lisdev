<%
//程序名称：ProposalConfirmNoInit.jsp
//程序功能：投保审核号录入界面初始化
//创建日期：2007-11-15
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!--引入必要的java类  -->
<%@page import="com.sinosoft.lis.pubfun.*"%>	
<% 
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var operator = "<%=tGI.Operator%>"; //记录当前登录人
  var tCurrentDate = "<%=tCurrentDate%>";
  var tCurrentTime = "<%=tCurrentTime%>";
 
//输入框的初始化 
function initInpBox()
{
  try
  {
	fm.all('PrtNo').value = "";
    fm.all('ConfirmNo').value = "";
    fm.all('ConfirmOperator').value = operator;
    var strSQL = "select Username from LDUser where Usercode = '" + operator  + "'";
	var Result = new Array();
    Result = easyExecSql(strSQL);
    fm.all('ConfirmName').value = Result[0][0];
    fm.all('ConfirmDate').value = tCurrentDate;
    fm.all('ConfirmTime').value = tCurrentTime;
    fm.all('Remark').value = "";
  }
  catch(ex)
  {
    alert("在ProposalConfirmNoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initInpBox();
    initLCUWConfirmInfoGrid(); 
    initElementtype();
  }
  catch(re) 
  {
    alert("在ProposalConfirmNoInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//工单列表Mulline的初始化
var LCUWConfirmInfoGrid;
function initLCUWConfirmInfoGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=3;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="印刷号码";      //列名PrtNo
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[2]=new Array();
    iArray[2][0]="核保审核号";      //列名ConfirmNo
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[3]=new Array();
    iArray[3][0]="审核人";      //列名ConfirmOperator
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[4]=new Array();
    iArray[4][0]="审核人姓名";      //列名ConfirmName
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[5]=new Array();
    iArray[5][0]="审核日期";      //列名ConfirmDate
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[6]=new Array();
    iArray[6][0]="审核时间";      //列名ConfirmTime
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[7]=new Array();
    iArray[7][0]="备注";      //列名Remark
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=3;             //是否允许输入,1表示允许，0表示不允许
      
    LCUWConfirmInfoGrid = new MulLineEnter("fm", "LCUWConfirmInfoGrid"); 
	  //设置Grid属性
    LCUWConfirmInfoGrid.mulLineCount = 10;
    LCUWConfirmInfoGrid.displayTitle = 1;
    LCUWConfirmInfoGrid.locked = 1;
    LCUWConfirmInfoGrid.canSel = 1;	
    LCUWConfirmInfoGrid.canChk = 0;
    LCUWConfirmInfoGrid.hiddenSubtraction = 1;
    LCUWConfirmInfoGrid.hiddenPlus = 1;
    LCUWConfirmInfoGrid.selBoxEventFuncName = "selectOne" ;
    LCUWConfirmInfoGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在ProposalConfirmNoInit.jsp-->initLCUWConfirmInfoGrid函数中发生异常:初始化界面错误!");
  }
}
</script>
