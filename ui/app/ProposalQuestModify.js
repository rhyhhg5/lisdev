//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var k = 0;

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

// 查询按钮
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function initQuery() {
	// 初始化表格
	initPolGrid();
	// 书写SQL语句
//	var strSql = "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName from LCPol where " + ++queryBug + "=" + queryBug
//    				 + " and AppFlag='0' "                 //个人保单
//    				 + " and GrpPolNo='" + grpPolNo + "'"  //集体单号，必须为20个0
//    				 + " and ContNo='" + contNo + "'"      //内容号，必须为20个0
//	           + " and ProposalNo in ( select ProposalNo from LCIssuePol where "
//    				 + " ( BackObjType = '1' and BackObj ='" + operator + "'"
//    				 + " ) or ( BackObjType = '3' and BackObj ='" + manageCom + "'"
//    				 + " ) )";

//modify by yt
//  var strSql = "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName from LCPol where " +queryBug + "=" + queryBug
//    				 //+ " and Operator ='" + operator + "' "
//    				 + " and AppFlag='0' "                 //个人保单
//    				 + " and GrpPolNo='" + grpPolNo + "'"  //集体单号，必须为20个0
//    				 + " and ContNo='" + contNo + "'"      //内容号，必须为20个0    				 
//    				 + " and uwflag <> '0' "
//    				 + " and approveflag = '2'"
//    				 //+ " and proposalno in (select proposalno from lcuwmaster where printflag in ('1','2','3'))"
//    				 + " and proposalno not in (select proposalno from lcissuepol where backobjtype = '4' and replyresult is null)"
//    				 + " and proposalno not in (select proposalno from lcissuepol where backobjtype in ('2','3') and replyresult is null and needprint = 'Y')";    				 
//    				 //+ " and proposalno in (select proposalno from lcissuepol where backobjtype = '1' and replyresult is null";    				 
//	           		 //+ " and ProposalNo in ( select ProposalNo from LCIssuePol where "
//    				 //+ " ( BackObjType = '2' and ReplyResult is not null "
//    				 //+ " ) or ( BackObjType = '3' and ManageCom like '" + manageCom + "%%' and ReplyResult is not null"
//    				 //+ " ) )";
/*
	var strSql = "select * from ("
		+ "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName from LCPol"
		+ " where " +queryBug + "=" + queryBug
		+ " and uwflag <> '0'  and approveflag = '2'"
    	+ " and AppFlag='0' "                 //个人保单
    	+ " and GrpPolNo='" + grpPolNo + "'"  //集体单号，必须为20个0
    	+ " and ContNo='" + contNo + "'"      //内容号，必须为20个0    				 
		+ " ) a "
		+ " where "
		+ " not exists (select proposalno from lcissuepol where replyresult is null and proposalno=a.proposalno "
		+ " and (backobjtype = '4' or(needprint = 'Y' and (backobjtype ='2' or backobjtype ='3'))))";
*/
	var strSql = "select lwmission.missionprop1,lwmission.missionprop2,lwmission.missionprop3 from lwmission where "+k+"="+k
						+ " and activityid = '0000001021'"
						;
  //alert(strSql);
	turnPage.queryModal(strSql, PolGrid);
}

var mSwitch = parent.VD.gVSwitch;

function modifyClick() {
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	var cPolNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	mSwitch.deleteVar( "PolNo" );
  	mSwitch.addVar( "PolNo", "", cPolNo );
  	
    //urlStr = "./ProposalMain.jsp?LoadFlag=3";
    var prtNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
    window.open("./ProposalEasyScan.jsp?LoadFlag=3&prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

//问题件查询
function QuestQuery()
{
	  var i = 0;
	  var checkFlag = 0;
	  var cProposalNo = "";
	  var cflag = "3";
	  
	  for (i=0; i<PolGrid.mulLineCount; i++) {
	    if (PolGrid.getSelNo(i)) { 
	      checkFlag = PolGrid.getSelNo();
	      break;
	    }
	  }
	  
	  if (checkFlag) { 
	  	cProposalNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
	  }
	  else {
	    alert("请先选择一条保单信息！"); 
	  }
	
	window.open("../uw/QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	
}


