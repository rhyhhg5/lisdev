<%
//程序名称：
//程序功能：
//创建日期：2008-8-14
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strPrtNo = tGI.ComCode;
String strContno = tGI.ComCode;
String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">
function initInpBox()
{
    try
    {
        fm.all("PrtNo").value = "";
        fm.all("Contno").value = "";
        fm.all("ManageCom").value = "";
    }
    catch(ex)
    {
        alert("初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInpBox();
        initMissionGrid();

        fm.all('ManageCom').value = <%=strManageCom%>;

        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


var MissionGrid;
function initMissionGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="暂收号";
        iArray[1][1]="150px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        iArray[1][21]="Tempfeeno";
        
        iArray[2]=new Array();
        iArray[2][0]="保单号";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        iArray[2][21]="Otherno";
        
        iArray[3]=new Array();
        iArray[3][0]="印刷号";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        iArray[3][21]="PrtNo";
        
        iArray[4]=new Array();
        iArray[4][0]="缴费方式";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        iArray[4][21]="PayMode";
        
        iArray[5]=new Array();
        iArray[5][0]="交费金额";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        iArray[5][21]="PayMoney";
        
        iArray[6]=new Array();
        iArray[6][0]="交费日期";
        iArray[6][1]="150px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        iArray[6][21]="PayDate";
        
        iArray[7]=new Array();
        iArray[7][0]="管理机构";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        iArray[7][21]="ManageCom";
        
        iArray[8]=new Array();
        iArray[8][0]="投保人";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        iArray[8][21]="APPntName";
        
        iArray[9]=new Array();
        iArray[9][0]="入机日期";
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=0;
        iArray[9][21]="MakeDate";
        
        iArray[10]=new Array();
        iArray[10][0]="入机时间";
        iArray[10][1]="80px";
        iArray[10][2]=100;
        iArray[10][3]=0;
        iArray[10][21]="MakeTime";
        
        

        MissionGrid = new MulLineEnter("fm", "MissionGrid"); 

        MissionGrid.mulLineCount = 0;   
        MissionGrid.displayTitle = 1;
        MissionGrid.canSel = 1;
        MissionGrid.hiddenSubtraction = 1;
        MissionGrid.hiddenPlus = 1;
        MissionGrid.canChk = 0;
        MissionGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ResultGrid时出错：" + ex);
    }
}
</script>

