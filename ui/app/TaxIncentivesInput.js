var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function AddInput() {
   window.location = "./TaxIncentivesGrpInput.jsp?type=1";
}
function easyQueryClick() {

  // 书写SQL语句
	var strSQL = "";
	initGrpGrid();
	strSQL = "select grpno,grpname,organcomcode,taxregistration from lsgrp "
		   + "where 1=1 "
		   + getWherePart("GrpNo","GrpNo")
		   + getWherePart("GrpName","GrpName")
		   + getWherePart("TaxRegistration","TaxNo")
		   + getWherePart("OrgancomCode","OrgancomCode")
		   + " order by grpno";
	turnPage.queryModal(strSQL, GrpGrid);
	if(GrpGrid.mulLineCount<1){
		alert("没有查询到相应数据！");
	}
}
function UpdateInput() {
	var checkFlag = 0;
	for(i=0; i<GrpGrid.mulLineCount; i++){
		if(GrpGrid.getSelNo(i)){
			checkFlag = GrpGrid.getSelNo();
			break;
		}
	}
	if(!checkFlag){
		alert("请选择一条客户信息进行修改！");
		return false;
	}else{
		var tGrpNo = GrpGrid.getRowColData(checkFlag-1 , 1);
		var tGrpName = GrpGrid.getRowColData(checkFlag-1 , 2);
		var tTaxNo = GrpGrid.getRowColData(checkFlag-1 , 4);
		var tOrgancomCode = GrpGrid.getRowColData(checkFlag-1 , 3);
		window.location = "./TaxIncentivesGrpInput.jsp?type=2&GrpNo="+tGrpNo;
	}
}

