var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
  return false;
  var tSQL="";  
  var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  if(dateDiff(tStartDate,tEndDate,"M")>12)
  {
    alert("统计期最多为一年！");
    return false;
  }
  if(dateDiff(tStartDate,tEndDate,"M")<1){
      alert("统计止期比统计统计起期早");
      return false;
  }
	
//  tSQL = " select lcc.contno ,codename('lcsalechnl',lcc.salechnl),"
//       + " (select lm.riskname from lcpol lcp,lmriskapp lm where lcc.contno=lcp.contno "
//       + " and lcp.riskcode=lm.riskcode  and lm.RiskPeriod='L' and lm.SubRiskFlag='M' fetch first 1 rows only ),"
//       + " case lcc.payintv when '0' then '趸交' else '期交' end,"
//       + " lcc.prem ,"
//       + " lcc.appntname,Year(lcc.polapplydate-lcc.AppntBirthday),"
//       + " lcc.cvalidate,lcc.cinvalidate,'是',codename('stateflag',lcc.stateflag) "
//       + " from  lccont lcc "
//       + " where lcc.managecom like '8644%'"
//       + " and lcc.appflag='1'"
//       + " and lcc.conttype='1'"
//       + " and Year(lcc.PolApplyDate - lcc.AppntBirthday)>=60"
//       + " and lcc.cardflag not in ('9','a','b') "
//       + " and exists ("
//       + " select 1 from lcpol lcp,lmriskapp lm where lcc.contno=lcp.contno"
//       + " and lcp.riskcode=lm.riskcode  and lm.RiskPeriod='L' "
//       + " )"
//       + " and lcc.cvalidate between '"+tStartDate+"' and '"+tEndDate+"'"
//       + " union all "
//       + " select lcc.contno ,codename('lcsalechnl',lcc.salechnl),"
//       + " (select lm.riskname from lbpol lcp,lmriskapp lm where lcc.contno=lcp.contno "
//       + " and lcp.riskcode=lm.riskcode  and lm.RiskPeriod='L' and lm.SubRiskFlag='M' fetch first 1 rows only ),"
//       + " case lcc.payintv when '0' then '趸交' else '期交' end,"
//       + " lcc.prem ,"
//       + " lcc.appntname,Year(lcc.polapplydate-lcc.AppntBirthday),"
//       + " lcc.cvalidate,lcc.cinvalidate,'是',(select edorname from lmedoritem where appobj='I' and edorcode=lpe.edortype) "
//       + " from  lbcont lcc, lpedoritem lpe"
//       + " where lcc.managecom like '8644%'"
//       + " and lcc.appflag='1'"
//       + " and lcc.conttype='1'"
//       + " and Year(lcc.PolApplyDate - lcc.AppntBirthday)>=60"
//       + " and lcc.cardflag not in ('9','a','b') "
//       + " and exists ("
//       + " select 1 from lbpol lcp,lmriskapp lm where lcc.contno=lcp.contno"
//       + " and lcp.riskcode=lm.riskcode  and lm.RiskPeriod='L' "
//       + " )"
//       + " and lcc.cvalidate between '"+tStartDate+"' and '"+tEndDate+"'"
//       + " and lcc.edorno=lpe.edorno "
//       + " and lpe.edortype in ('CT','WT','XT')  "
//       ;
  
  tSQL = "select distinct temp.managecom 机构名称,temp.contno 保单合同号,temp.salechnl 销售渠道,temp.platform 出单平台,"
	   + " temp.agentcom 银行网点名称,temp.riskname 险种名称,temp.payintv 交费方式,temp.prem 交费金额,"
	   + " temp.appntname 投保人姓名,temp.age 投保人年龄,temp.polapplydate 投保日期,temp.cvalidate 保单生效日起,"
	   + " temp.cinvalidate 保单终止日期,temp.agentname 业务员姓名,temp.isaudio 是否录音录像,"
	   + " temp.isscan 是否扫描,temp.state 保单状态"
	   + " from"
	   + " ("
	   + " select" 
	   + " (select name from ldcom where comcode=lcc.managecom) managecom"
	   + " ,lcc.contno contno"
	   + " ,codename('lcsalechnl',lcc.salechnl) salechnl"
	   + " ,(case lcc.cardflag when '9' then '银保通出单' when 'c' then '网银出单' when 'd' then '自助终端出单' "
	   + " else '核心业务系统出单' end) platform"
	   + " ,(select name from lacom where agentcom=lcc.agentcom) agentcom"
	   + " ,(select lm.riskname from lcpol lcp,lmriskapp lm where lcc.contno=lcp.contno "
	   + " and lcp.riskcode=lm.riskcode  and lm.RiskPeriod='L' and lm.SubRiskFlag='M' fetch first 1 rows only ) riskname"
	   + " ,(case lcc.payintv when '0' then '趸交' else '期交' end) payintv"
	   + " ,lcc.prem prem,lcc.appntname appntname,Year(lcc.polapplydate-lcc.AppntBirthday) age"
	   + " ,lcc.polapplydate polapplydate,lcc.cvalidate cvalidate,lcc.cinvalidate cinvalidate"
	   + " ,(select name from laagent where agentcode=lcc.agentcode) agentname,'是' isaudio"
	   + " ,(case (select distinct 1 from es_doc_main em where em.doccode=lcc.prtno) when '1' then '是' else '否' end) isscan"
	   + " ,codename('stateflag',lcc.stateflag) state"
	   + " from  lccont lcc "
	   + " where lcc.managecom like '8644%'"
	   + " and lcc.appflag='1'"
	   + " and lcc.conttype='1'"
	   + " and Year(lcc.PolApplyDate - lcc.AppntBirthday)>=60"
	   + " and lcc.cardflag not in ('9','a','b','c','d') "
	   + " and lcc.salechnl <> '04'"
	   + " and exists ("
	   + " select 1 from lcpol lcp,lmriskapp lm where lcc.contno=lcp.contno"
	   + " and lcp.riskcode=lm.riskcode  and lm.RiskPeriod='L' "
	   + " )"
	   + " and lcc.cvalidate between '"+tStartDate+"' and '"+tEndDate+"'"
	   + " union all"
	   + " select "
	   + " (select name from ldcom where comcode=lcc.managecom) managecom"
	   + " ,lcc.contno contno"
	   + " ,codename('lcsalechnl',lcc.salechnl) salechnl"
	   + " ,(case lcc.cardflag when '9' then '银保通出单' when 'c' then '网银出单' when 'd' then '自助终端出单' "
	   + " else '核心业务系统出单' end) platform"
	   + " ,(select name from lacom where agentcom=lcc.agentcom) agentcom"
	   + " ,(select lm.riskname from lcpol lcp,lmriskapp lm where lcc.contno=lcp.contno "
	   + " and lcp.riskcode=lm.riskcode and lm.SubRiskFlag='M' fetch first 1 rows only ) riskname"
	   + " ,(case lcc.payintv when '0' then '趸交' else '期交' end) payintv"
	   + " ,lcc.prem prem,lcc.appntname appntname,Year(lcc.polapplydate-lcc.AppntBirthday) age"
	   + " ,lcc.polapplydate polapplydate,lcc.cvalidate cvalidate,lcc.cinvalidate cinvalidate"
	   + " ,(select name from laagent where agentcode=lcc.agentcode) agentname,'' isaudio"
	   + " ,(case (select distinct 1 from es_doc_main em where em.doccode=lcc.prtno) when '1' then '是' else '否' end) isscan"
	   + " ,codename('stateflag',lcc.stateflag) state"
	   + " from  lccont lcc "
	   + " where lcc.managecom like '8644%'"
	   + " and lcc.appflag='1'"
	   + " and lcc.conttype='1'"
	   + " and lcc.cardflag not in ('9','a','b','c','d')  "
	   + " and lcc.salechnl = '04'"
	   + " and lcc.cvalidate between '"+tStartDate+"' and '"+tEndDate+"'"
	   + " union all"
	   + " select "
	   + " (select name from ldcom where comcode=lcc.managecom) managecom"
	   + " ,lcc.contno contno"
	   + " ,codename('lcsalechnl',lcc.salechnl) salechnl"
	   + " ,(case lcc.cardflag when '9' then '银保通出单' when 'c' then '网银出单' when 'd' then '自助终端出单' "
	   + " else '核心业务系统出单' end) platform"
	   + " ,(select name from lacom where agentcom=lcc.agentcom) agentcom"
	   + " ,(select lm.riskname from lcpol lcp,lmriskapp lm where lcc.contno=lcp.contno "
	   + " and lcp.riskcode=lm.riskcode and lm.SubRiskFlag='M' fetch first 1 rows only ) riskname"
	   + " ,(case lcc.payintv when '0' then '趸交' else '期交' end) payintv"
	   + " ,lcc.prem prem,lcc.appntname appntname,Year(lcc.polapplydate-lcc.AppntBirthday) age"
	   + " ,lcc.polapplydate polapplydate,lcc.cvalidate cvalidate,lcc.cinvalidate cinvalidate"
	   + " ,(select name from laagent where agentcode=lcc.agentcode) agentname,'' isaudio"
	   + " ,(case (select distinct 1 from es_doc_main em where em.doccode=lcc.prtno) when '1' then '是' else '否' end) isscan"
	   + " ,codename('stateflag',lcc.stateflag) state"
	   + " from  lccont lcc "
	   + " where lcc.managecom like '8644%'"
	   + " and lcc.appflag='1'"
	   + " and lcc.conttype='1'"
	   + " and lcc.cardflag in ('9','c','d')  "
	   + " and lcc.cvalidate between '"+tStartDate+"' and '"+tEndDate+"'"
	   + " union all"
	   + " select (select name from ldcom where comcode=lcc.managecom)"
	   + " ,lcc.contno "
	   + " ,codename('lcsalechnl',lcc.salechnl)"
	   + " ,(case lcc.cardflag when '9' then '银保通出单' when 'c' then '网银出单' when 'd' then '自助终端出单' "
	   + " else '核心业务系统出单' end)"
	   + " ,(select name from lacom where agentcom=lcc.agentcom)"
	   + " ,(select lm.riskname from lbpol lcp,lmriskapp lm where lcc.contno=lcp.contno "
	   + " and lcp.riskcode=lm.riskcode  and lm.RiskPeriod='L' and lm.SubRiskFlag='M' fetch first 1 rows only )"
	   + " ,(case lcc.payintv when '0' then '趸交' else '期交' end)"
	   + " ,lcc.prem,lcc.appntname,Year(lcc.polapplydate-lcc.AppntBirthday)"
	   + " ,lcc.polapplydate,lcc.cvalidate,lcc.cinvalidate"
	   + " ,(select name from laagent where agentcode=lcc.agentcode),'是'"
	   + " ,(case (select distinct 1 from es_doc_main em where em.doccode=lcc.prtno) when '1' then '是' else '否' end)"
	   + " ,(select edorname from lmedoritem where appobj='I' and edorcode=lpe.edortype) "
	   + " from  lbcont lcc, lpedoritem lpe"
	   + " where lcc.managecom like '8644%'"
	   + " and lcc.appflag='1'"
	   + " and lcc.conttype='1'"
	   + " and Year(lcc.PolApplyDate - lcc.AppntBirthday)>=60"
	   + " and lcc.cardflag not in ('9','a','b','c','d') "
	   + " and lcc.salechnl <> '04'"
	   + " and exists ("
	   + " select 1 from lbpol lcp,lmriskapp lm where lcc.contno=lcp.contno"
	   + " and lcp.riskcode=lm.riskcode  and lm.RiskPeriod='L' "
	   + " )"
	   + " and lcc.cvalidate between '"+tStartDate+"' and '"+tEndDate+"'"
	   + " and lcc.edorno=lpe.edorno "
	   + " and lpe.edortype in ('CT','WT','XT')"
	   + " union all "
	   + " select (select name from ldcom where comcode=lcc.managecom)"
	   + " ,lcc.contno "
	   + " ,codename('lcsalechnl',lcc.salechnl)"
	   + " ,(case lcc.cardflag when '9' then '银保通出单' when 'c' then '网银出单' when 'd' then '自助终端出单' "
	   + " else '核心业务系统出单' end)"
	   + " ,(select name from lacom where agentcom=lcc.agentcom)"
	   + " ,(select lm.riskname from lbpol lcp,lmriskapp lm where lcc.contno=lcp.contno "
	   + " and lcp.riskcode=lm.riskcode and lm.SubRiskFlag='M' fetch first 1 rows only )"
	   + " ,(case lcc.payintv when '0' then '趸交' else '期交' end)"
	   + " ,lcc.prem,lcc.appntname,Year(lcc.polapplydate-lcc.AppntBirthday)"
	   + " ,lcc.polapplydate,lcc.cvalidate,lcc.cinvalidate"
	   + " ,(select name from laagent where agentcode=lcc.agentcode),''"
	   + " ,(case (select distinct 1 from es_doc_main em where em.doccode=lcc.prtno) when '1' then '是' else '否' end)"
	   + " ,(select edorname from lmedoritem where appobj='I' and edorcode=lpe.edortype) "
	   + " from  lbcont lcc, lpedoritem lpe"
	   + " where lcc.managecom like '8644%'"
	   + " and lcc.appflag='1'"
	   + " and lcc.conttype='1'"
	   + " and lcc.cardflag not in ('9','a','b','c','d') "
	   + " and lcc.salechnl = '04'"
	   + " and lcc.cvalidate between '"+tStartDate+"' and '"+tEndDate+"'"
	   + " and lcc.edorno=lpe.edorno "
	   + " and lpe.edortype in ('CT','WT','XT')"
	   + " union all "
	   + " select (select name from ldcom where comcode=lcc.managecom)"
	   + " ,lcc.contno "
	   + " ,codename('lcsalechnl',lcc.salechnl)"
	   + " ,(case lcc.cardflag when '9' then '银保通出单' when 'c' then '网银出单' when 'd' then '自助终端出单' "
	   + " else '核心业务系统出单' end)"
	   + " ,(select name from lacom where agentcom=lcc.agentcom)"
	   + " ,(select lm.riskname from lbpol lcp,lmriskapp lm where lcc.contno=lcp.contno "
	   + " and lcp.riskcode=lm.riskcode and lm.SubRiskFlag='M' fetch first 1 rows only )"
	   + " ,(case lcc.payintv when '0' then '趸交' else '期交' end)"
	   + " ,lcc.prem,lcc.appntname,Year(lcc.polapplydate-lcc.AppntBirthday)"
	   + " ,lcc.polapplydate,lcc.cvalidate,lcc.cinvalidate"
	   + " ,(select name from laagent where agentcode=lcc.agentcode),''"
	   + " ,(case (select distinct 1 from es_doc_main em where em.doccode=lcc.prtno) when '1' then '是' else '否' end)"
	   + " ,(select edorname from lmedoritem where appobj='I' and edorcode=lpe.edortype) "
	   + " from  lbcont lcc, lpedoritem lpe"
	   + " where lcc.managecom like '8644%'"
	   + " and lcc.appflag='1'"
	   + " and lcc.conttype='1'"
	   + " and lcc.cardflag in ('9','c','d') "
	   + " and lcc.cvalidate between '"+tStartDate+"' and '"+tEndDate+"'"
	   + " and lcc.edorno=lpe.edorno "
	   + " and lpe.edortype in ('CT','WT','XT')"
	   + " ) temp"
	   ;
	
  fm.querySql.value = tSQL;
  fm.action="GDAudioVideoStatisticDown.jsp";
  fm.submit();


}



	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
