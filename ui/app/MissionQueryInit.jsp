<%
//程序名称：
//程序功能：
//创建日期：2008-8-14
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">
function initInpBox()
{
    try
    {
        fm.all("ManageCom").value = "";
    }
    catch(ex)
    {
        alert("初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInpBox();
        initMissionGrid();

        fm.all('ManageCom').value = <%=strManageCom%>;

        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


var MissionGrid;
function initMissionGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="任务ID";
        iArray[1][1]="150px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        iArray[1][21]="MissionID";
        
        iArray[2]=new Array();
        iArray[2][0]="子任务ID";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        iArray[2][21]="SubMissionID";
        
        iArray[3]=new Array();
        iArray[3][0]="当前活动ID";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        iArray[3][21]="ActivityID";
        
        iArray[4]=new Array();
        iArray[4][0]="当前活动状态";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        iArray[4][21]="ActivityStatus";
        
        iArray[5]=new Array();
        iArray[5][0]="创建时间";
        iArray[5][1]="150px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        iArray[5][21]="MakeDate";
        
        iArray[6]=new Array();
        iArray[6][0]="MissionProp1";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        iArray[6][21]="MissionProp1";
        
        iArray[7]=new Array();
        iArray[7][0]="MissionProp2";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        iArray[7][21]="MissionProp2";
        
        iArray[8]=new Array();
        iArray[8][0]="MissionProp3";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        iArray[8][21]="MissionProp3";
        
        iArray[9]=new Array();
        iArray[9][0]="MissionProp4";
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=0;
        iArray[9][21]="MissionProp4";
        
        iArray[10]=new Array();
        iArray[10][0]="MissionProp5";
        iArray[10][1]="80px";
        iArray[10][2]=100;
        iArray[10][3]=0;
        iArray[10][21]="MissionProp5";
        
        iArray[11]=new Array();
        iArray[11][0]="MissionProp6";
        iArray[11][1]="80px";
        iArray[11][2]=100;
        iArray[11][3]=0;
        iArray[11][21]="MissionProp6";


        MissionGrid = new MulLineEnter("fm", "MissionGrid"); 

        MissionGrid.mulLineCount = 0;   
        MissionGrid.displayTitle = 1;
        MissionGrid.canSel = 1;
        MissionGrid.hiddenSubtraction = 1;
        MissionGrid.hiddenPlus = 1;
        MissionGrid.canChk = 0;
        MissionGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ResultGrid时出错：" + ex);
    }
}
</script>

