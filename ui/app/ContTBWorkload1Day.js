var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;

	if(dateDiff(tStartDate, tEndDate, "M") > 3)
	{
		alert("统计期最多为三个月！");
		return false;
	}

    var tStrSQL = ""
        + " select "
        + " tmpContInfo.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = tmpContInfo.ManageCom) ManageName,  "
        + " count(distinct tmpContInfo.DocCode) ScanCount, "
        + " count(distinct tmpContInfo.PrtNo) InputCount, "
        + " sum(case when (tmpContInfo.PrtNo is not null and ((tmpContInfo.ApproveFlag != '9' or tmpContInfo.ApproveFlag is null))) then 1 else 0 end) ApproveCount, "
        + " sum(case when (tmpContInfo.PrtNo is not null and ((tmpContInfo.ApproveFlag = '9' and tmpContInfo.UWFlag in ('0', '5', 'z')))) then 1 else 0 end) UWCount, "
        + " sum(case when (tmpContInfo.PrtNo is not null and ((tmpContInfo.ApproveFlag = '9' and tmpContInfo.UWFlag in ('a', '1', '8')))) then 1 else 0 end) CancleCount, "
        + " sum(case when (tmpContInfo.PrtNo is not null and tmpContInfo.AppFlag not in ('1') and ((tmpContInfo.ApproveFlag = '9' and tmpContInfo.UWFlag in ('4', '9')))) then 1 else 0 end) UWPassCount, "
        + " sum(case when (tmpContInfo.AppFlag in ('1')) then 1 else 0 end) SignCount "
        + " from "
        + " ( "
        + " select "
        + " distinct edm.ManageCom, edm.DocCode, lcc.PrtNo, lcc.ContNo, lcc.ApproveFlag, lcc.UWFlag, AppFlag, "
        + " '' "
        + " from Es_Doc_Main edm "
        + " left join LCCont lcc on lcc.PrtNo = edm.DocCode and lcc.ContType = '1' "
        + " where 1 = 1 "
        + " and length(trim(edm.ManageCom)) = 8 "
        + " and edm.SubType = 'TB01' "
        + " and edm.BussType = 'TB' "
        + getWherePart("edm.ManageCom", "ManageCom", "like")
        + getWherePart("edm.MakeDate", "StartDate", ">=")
        + getWherePart("edm.MakeDate", "EndDate", "<=")
        + " union "
        + " select "
        + " distinct edm.ManageCom, edm.DocCode, lcc.PrtNo, lcc.ContNo, lcc.ApproveFlag, lcc.UWFlag, AppFlag, "
        + " '' "
        + " from Es_Doc_Main edm "
        + " left join LBCont lcc on lcc.PrtNo = edm.DocCode and lcc.ContType = '1' "
        + " where 1 = 1 "
        + " and length(trim(edm.ManageCom)) = 8 "
        + " and edm.SubType = 'TB01' "
        + " and edm.BussType = 'TB' "
        + getWherePart("edm.ManageCom", "ManageCom", "like")
        + getWherePart("edm.MakeDate", "StartDate", ">=")
        + getWherePart("edm.MakeDate", "EndDate", "<=")
        + " ) as tmpContInfo "
        + " where 1 = 1 "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.NewContNo = tmpContInfo.ContNo or lcrnsl.ContNo = tmpContInfo.ContNo) "
        + " group by tmpContInfo.ManageCom "
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "ContTBWorkload1DaySave.jsp";
    fm.submit();
    fm.action = oldAction;

}