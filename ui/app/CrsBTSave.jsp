<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：CrsBTSave.jsp
	//程序功能：
	//创建日期：2016-12-26
	//创建人  ：于坤
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
	String tCValidateStart = request.getParameter("CValidateStart");
	String tCValidateEnd = request.getParameter("CValidateEnd");
	
	//输入参数
	CrsBTUI tCrsBTUI = new CrsBTUI();
	
	//输出参数
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");	
	String strManageCom = tG.ComCode;
	
	if(tCValidateStart == ""){
		FlagStr="Fail";
		Content="请选择起始日期!";
	}else if(tCValidateEnd == ""){
		FlagStr="Fail";
		Content="请选择截止日期!";
	}else if(java.sql.Date.valueOf(tCValidateStart).after(java.sql.Date.valueOf(tCValidateEnd))){
		//起始日期大于结束日期 
		FlagStr="Fail";
		Content="起始日期大于结束日期,请重新选择!";
	}else{
		tCrsBTUI.setTime(tCValidateStart,tCValidateEnd);
		tCrsBTUI.run();
		FlagStr="true";
		Content="处理成功!";
	}
	
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

