/**
 * 投保书图像查询
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function initQuery(){
	var strSQL = "select a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
				+ " a.maketime,a.modifydate,a.modifytime,a.docid "
				+ " from es_doc_main a ,es_doc_def b where 1=1 "
				+ " and a.subtype = b.subtype "
				+ " and a.doccode like '"+fm.PrtNo.value+"%'"
				+ " and a.busstype='TB'"
				+ " and b.busstype='TB'";
	turnPage.queryModal(strSQL,EsDocPagesGrid,0,0,1);
}

function ChangePic()
{
	tRow = EsDocPagesGrid.getSelNo();
		
	if( tRow == 0 || tRow == null  )
	{
		alert("请选择一条记录！");
		return;
	}
	var cDocID = EsDocPagesGrid.getRowColData(tRow-1,10);	
 	var cDocCode = EsDocPagesGrid.getRowColData(tRow-1,1); 	
 	var	cBussTpye = "TB" ;
 	var cSubTpye = EsDocPagesGrid.getRowColData(tRow-1,2); 
    
    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
}