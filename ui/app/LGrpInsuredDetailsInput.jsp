<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="InsuredDetails.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="InsuredDetailsInit.jsp"%>
   <%@page contentType="text/html;charset=GBK" %>
  
  
</head>
<body  onload="initForm();" >
 <form action="./InsuredDetailsSave.jsp" method=post name=fm target="fraSubmit">
 <Div  id= "firstSetting" style= "display: ''">
 <table  class=common >
		<tr align=right>
			<td class=button >
				<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();">
				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="return easyQueryClick();">
			</td>
			</tr>
	</table>
		<table  class= common >
  			<TR  class= common>
  			<TD  class= title8>
    			被保人姓名
    			</TD>
    			<TD  class= input8>
    			  	<Input class=common name="Name" verify="被保人姓名|notnull" elementtype="nacessary" >
    			</TD>
    			<TD  class= title8>
			        被保人性别
			   	 </TD>
			   	<TD  class= input>
			  	<Input class=codeNo name=Sex  verify="投保人性别|code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true elementtype=nacessary>    
			  	</TD>             
    			<TD  class= title>
                   出生日期
                 </TD>
                 <TD  class= input>
                 <Input class= "coolDatePicker" name="Birthday" verify="出生日期|date|notnull" elementtype="nacessary">
                 </TD>
  			</TR>
  			<TR  class= common>
				<TD  class= title>
      证件类型
    </TD>
    <TD  class= input>
      <Input class="codeNo" name="IDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1]);"><input class="codename" name="AppntIDTypeName" readonly="true" elementtype="nacessary">    
    </TD>
   <TD  class= title8>
     证件号码
   </TD>
   <TD  class= input8>
     <Input class= common name="IDNo"  verify="投保人证件号码|len<=20|notnull"  elementtype="nacessary">
   </TD> 
   <TD  class= title>
     证件有效期起
   </TD>
   <TD  class= input colspan=1>
     <Input class= common3 name="IDStartDate" verify="Idstartdate" >
   </TD> 	
  			</TR>
  			<TR  class= common>
  			<TD  class= title>
     证件有效期止
   </TD>
   <TD  class= input colspan=1>
     <Input class= "common3" name="IDEndDate" verify="Idstartdate" >
   </TD> 
				<TD  class= title>
     其他证件类型
    </TD>
    <TD  class= input>
      <Input class="codeNo" name="OthIDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,OthIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,OthIDTypeName],[0,1]);"><input class="codename" name="OthIDTypeName" readonly="true" >    
    </TD>
   <TD  class= title>
     其他证件号码
   </TD>
   <TD  class= input colspan=1>
     <Input class= "common3" name="OthIDNo" verify="投保人证件号码|len<=20" >
   </TD> 
  			</TR>
  		<TR  class= common>	
  		<TD  class= title8>
     保障计划
   </TD>
   <TD  class= input8 >
     <Input class= "common" name="ContPlanCode" verify="ContPlanCode|notnull" elementtype="nacessary">
   </TD> 	
   	<TD  class= title>
     职业代码
   </TD>
   <TD  class= input colspan=1>
     <Input class= "common3" name="OccupationCode" verify="OccupationCode" >
   </TD> 
  			
  			<TD class=title> 职业类别
      </TD>
			 <TD class=input>
			<Input class="codeno" name="OccupationType"  verify="职业类别|code:OccupationType" MAXLENGTH=0 ondblclick="return showCodeList('OccupationType',[this,OccupationTypeName],[0,1]);" onkeyup="return showCodeListKey('OccupationType',[this,OccupationTypeName],[0,1]);"><input class="codename" name="OccupationTypeName" readonly="true"> 
  			 </TD>
  			</TR>
  			<TR  class= common>	
  		<TD  class= title>
      在职/退休
    </TD>
    <TD  class= input>
    	<Input class= "codeno" name="Retire" verify="在职/退休|int" CodeData= "0|^1|在职^2|退休" ondblClick= "showCodeListEx('',[this,RetireName],[0,1],null,null,null,1);" onkeyup= "showCodeListKeyEx('',[this,RetireName],[0,1],null,null,null,1);"><input class = "codename" name="RetireName" readonly="true">
    </TD>
   	<TD  class= title>
      员工姓名
    </TD>
    <TD  class= input>
      <Input class= "common" name="EmployeeName" verify="员工姓名|len<=20|notnull" elementtype="nacessary">
    </TD>
  			
  			<TD  class= title>
     与员工关系</TD>
    <TD  class= input>
    <Input class="codeNo" name="Relation"  ondblclick="return showCodeList('Relation', [this,RelationName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,RelationName],[0,1]);"><input class=codename name="RelationName" readonly="true" elementtype="acessary" elementtype="nacessary"> </TD>

  			</TR>
  			 <TR  class= common>
    <TD  class= title>
      理赔金转帐银行
    </TD>
    <TD  class= input>
      <Input class= "common" name="BankCode" >
    </TD>
    <TD  class= title>
      帐号
    </TD>
    <TD  class= input>
      <Input class= "common" name="BankAccNo" >
    </TD>
     
    <TD  class= title>
      户名
    </TD>
    <TD  class= input>
      <Input class= "common" name="AccName" >
    </TD>
  </TR>
  			
  			
		</table>
		
    </Div>
	 <INPUT class=cssButton id="pisdbutton1" VALUE="导入被保人清单" TYPE=button onclick="getin();"> 
	 <Input class=common name="SeqNo" style=display:none>
	<input type=hidden id="fmtransact" name="fmtransact" value="INSERT||MAIN">
	<input type=hidden name=RiskType>
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="ruleNo" name="ruleNo" value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>