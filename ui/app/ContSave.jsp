<%@page contentType="text/html;charset=GBK" %>
<jsp:directive.page import="com.sinosoft.lis.db.LCInsuredDB"/>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolInput.jsp
//程序功能：
//创建日期：2002-08-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr="";
	String Content = "";
	String tAction = "";
	String tOperate = "";
	String LoadFlag =  request.getParameter("LoadFlag");
	
	//输入参数
	VData tVData = new VData();
	LCContSchema tLCContSchema   = new LCContSchema();
	LCAppntSchema tLCAppntSchema = new LCAppntSchema();
	LCAddressSchema tLCAddressSchema = new LCAddressSchema();
        LCAccountSchema tLCAccountSchema = new LCAccountSchema();
        LCCustomerImpartSet tLCCustomerImpartSet = new LCCustomerImpartSet();
    //复录使用    
    VData ttVData = new VData();
    LCApproveSchema tLCApproveSchema = new LCApproveSchema(); 
    LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
	LCInsuredDB tOLDLCInsuredDB=new LCInsuredDB();
	  
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");


	tAction = request.getParameter( "fmAction" );
        System.out.println("动作是:"+tAction);
	if( tAction.equals( "DELETE" ))
	{
	    tLCContSchema.setContNo(request.getParameter("ContNo"));
            tLCContSchema.setManageCom(request.getParameter("ManageCom"));	    
	}
	else
      {        
  	LDSysTraceSchema tLDSysTraceSchema = new LDSysTraceSchema();
        tLDSysTraceSchema.setPolNo(request.getParameter("PrtNo"));
        tLDSysTraceSchema.setCreatePos("承保录单");
        tLDSysTraceSchema.setPolState("1002");
        LDSysTraceSet inLDSysTraceSet = new LDSysTraceSet();
        inLDSysTraceSet.add(tLDSysTraceSchema);
        VData VData3 = new VData();
        VData3.add(tG);
        VData3.add(inLDSysTraceSet);
        
        LockTableUI LockTableUI1 = new LockTableUI();
        if (!LockTableUI1.submitData(VData3, "INSERT")) 
        {
          VData rVData = LockTableUI1.getResult();
          System.out.println("LockTable Failed! " + (String)rVData.get(0));
        }
        else 
        {
          System.out.println("LockTable Succed!");
        }
      
        //合同信息
        if( tAction.equals( "UPDATE" ))
        {
        tLCContSchema.setGrpContNo(request.getParameter("GrpContNo")); 
        System.out.println("集体合同号是:"+request.getParameter("GrpContNo"));
         System.out.println("集体合同号是:"+request.getParameter("ContNo"));
                 System.out.println("集体合同号是:"+request.getParameter("MakeDate"));
        tLCContSchema.setContNo(request.getParameter("ContNo")); 
	tLCContSchema.setProposalContNo(request.getParameter("ProposalContNo")); 
	}
        tLCContSchema.setPrtNo(request.getParameter("PrtNo"));
        tLCContSchema.setManageCom(request.getParameter("ManageCom"));
        tLCContSchema.setSaleChnl(request.getParameter("SaleChnl"));
        tLCContSchema.setSaleChnlDetail(request.getParameter("SaleChnlDetail"));
        tLCContSchema.setAgentCode(request.getParameter("AgentCode"));
        tLCContSchema.setAgentCode1(request.getParameter("AgentCode1"));
        tLCContSchema.setAgentGroup(request.getParameter("AgentGroup"));
        tLCContSchema.setRemark(request.getParameter("Remark"));
        tLCContSchema.setAgentCom(request.getParameter("AgentCom"));
        tLCContSchema.setAgentType(request.getParameter("AgentType"));
        tLCContSchema.setPolType("0");
        tLCContSchema.setContType("1");
        tLCContSchema.setPayIntv(request.getParameter("PayIntv"));     
        tLCContSchema.setPayMode(request.getParameter("PayMode"));  
        System.out.println("在Save中，测试缴费方式是否传入 ："+request.getParameter("PayMode"));        
        tLCContSchema.setBankCode(request.getParameter("AppntBankCode"));
        tLCContSchema.setAccName(request.getParameter("AppntAccName"));
        tLCContSchema.setBankAccNo(request.getParameter("AppntBankAccNo")); 
        tLCContSchema.setPolApplyDate(request.getParameter("PolApplyDate")); 
        tLCContSchema.setReceiveDate(request.getParameter("ReceiveDate"));
        tLCContSchema.setFirstTrialOperator(request.getParameter("FirstTrialOperator"));
        tLCContSchema.setFirstTrialDate(request.getParameter("FirstTrialDate"));         
        tLCContSchema.setCValiDate(request.getParameter("CValiDate"));            
        //tLCContSchema.setTempFeeNo(request.getParameter("TempFeeNo"));
        tLCContSchema.setGrpAgentCom(request.getParameter("GrpAgentCom"));
        tLCContSchema.setGrpAgentCode(request.getParameter("GrpAgentCode"));
        tLCContSchema.setGrpAgentName(request.getParameter("GrpAgentName"));
        tLCContSchema.setGrpAgentIDNo(request.getParameter("GrpAgentIDNo"));
        
        tLCContSchema.setCrs_SaleChnl(request.getParameter("Crs_SaleChnl"));
        tLCContSchema.setCrs_BussType(request.getParameter("Crs_BussType"));
        tLCContSchema.setExPayMode(request.getParameter("ExPayMode"));
        tLCContSchema.setExiSpec(request.getParameter("ExiSpec"));
        tLCContSchema.setAgentSaleCode(request.getParameter("AgentSaleCode"));
        
        // 续保提醒标志
        tLCContSchema.setDueFeeMsgFlag(request.getParameter("DueFeeMsgFlag"));
        // --------------------
        
        
        // 如果为先收费流程保单，PayLocation置“0”，正常单为null
        //String tFirstPay = null;
        //String tPayMode = request.getParameter("PayMode");
        //String tTempFeeNo = request.getParameter("TempFeeNo") == null ? "" : request.getParameter("TempFeeNo");
        //if(("1".equals(tPayMode) || "11".equals(tPayMode) || "12".equals(tPayMode)) && !"".equals(tTempFeeNo))
        //{
        //    tFirstPay = "0";
        //}
        //tLCContSchema.setPayLocation(tFirstPay);
        // ------------------------

	    //投保人信息
		  //modify by zxs 2019-01-16
		  if(request.getParameter("AppntIDType").equals("a")||request.getParameter("AppntIDType").equals("b")){
			  tLCAppntSchema.setOriginalIDNo(request.getParameter("AppntPassIDNo"));//原通行证号码  
		  }
	    tLCAppntSchema.setAuthorization(request.getParameter("AppntAuth"));//
	tLCAppntSchema.setAppntNo(request.getParameter("AppntNo"));       
        tLCAppntSchema.setAppntName(request.getParameter("AppntName"));              
        tLCAppntSchema.setAppntSex(request.getParameter("AppntSex"));               
        tLCAppntSchema.setAppntBirthday(request.getParameter("AppntBirthday"));          
        tLCAppntSchema.setIDType(request.getParameter("AppntIDType"));            
        tLCAppntSchema.setIDNo(request.getParameter("AppntIDNo"));              
        tLCAppntSchema.setAppntGrade("1");               
        tLCAppntSchema.setAppntType("1");             
        tLCAppntSchema.setRgtAddress(request.getParameter("AppntRgtAddress"));        
        tLCAppntSchema.setMarriage(request.getParameter("AppntMarriage"));          
        tLCAppntSchema.setDegree(request.getParameter("AppntDegree"));                    
        tLCAppntSchema.setOccupationType(request.getParameter("AppntOccupationType"));    
        tLCAppntSchema.setOccupationCode(request.getParameter("AppntOccupationCode"));    
        tLCAppntSchema.setWorkType(request.getParameter("AppntWorkType"));          
        tLCAppntSchema.setPluralityType(request.getParameter("AppntPluralityType"));
        tLCAppntSchema.setSmokeFlag(request.getParameter("AppntSmokeFlag"));
        tLCAppntSchema.setNativePlace(request.getParameter("AppntNativePlace"));
        tLCAppntSchema.setNationality(request.getParameter("AppntNationality"));         
        tLCAppntSchema.setBankCode(request.getParameter("AppntBankCode"));
        tLCAppntSchema.setAccName(request.getParameter("AppntAccName"));
        tLCAppntSchema.setBankAccNo(request.getParameter("AppntBankAccNo"));
        tLCAppntSchema.setMakeDate(request.getParameter("AppntMakeDate"));
        tLCAppntSchema.setMakeTime(request.getParameter("AppntMakeTime"));
        tLCAppntSchema.setSalary(request.getParameter("AppntSalary"));
        tLCAppntSchema.setPosition(request.getParameter("AppntPosition"));
        tLCAppntSchema.setIDStartDate(request.getParameter("AppIDStartDate"));
        tLCAppntSchema.setIDEndDate(request.getParameter("AppIDEndDate"));
        tLCAppntSchema.setExiSpec(request.getParameter("ExiSpec"));
        tLCAppntSchema.setNativeCity(request.getParameter("AppntNativeCity"));
        tLCAccountSchema.setBankCode(request.getParameter("AppntBankCode"));
        tLCAccountSchema.setAccName(request.getParameter("AppntAccName"));
        tLCAccountSchema.setBankAccNo(request.getParameter("AppntBankAccNo"));
        tLCAccountSchema.setAccKind("1");
        
        tLCAddressSchema.setCustomerNo(request.getParameter("AppntNo"));
        //tLCAddressSchema.setAddressNo(request.getParameter("AddressNo"));                    
        tLCAddressSchema.setPostalAddress(request.getParameter("AppntPostalAddress"));
        tLCAddressSchema.setZipCode(request.getParameter("AppntZipCode"));
        tLCAddressSchema.setPhone(request.getParameter("AppntPhone"));
        tLCAddressSchema.setFax(request.getParameter("AppntFax"));        
        tLCAddressSchema.setMobile(request.getParameter("AppntMobile"));
        tLCAddressSchema.setEMail(request.getParameter("AppntEMail"));
        tLCAddressSchema.setHomeAddress(request.getParameter("AppntHomeAddress"));
        tLCAddressSchema.setHomePhone(request.getParameter("AppntHomePhone"));
        tLCAddressSchema.setHomeFax(request.getParameter("AppntHomeFax"));
        tLCAddressSchema.setHomeZipCode(request.getParameter("AppntHomeZipCode"));        
        tLCAddressSchema.setGrpName(request.getParameter("AppntGrpName"));
        tLCAddressSchema.setCompanyPhone(request.getParameter("AppntGrpPhone"));
        tLCAddressSchema.setCompanyAddress(request.getParameter("CompanyAddress"));
        tLCAddressSchema.setCompanyZipCode(request.getParameter("AppntGrpZipCode"));
        tLCAddressSchema.setCompanyFax(request.getParameter("AppntGrpFax"));
        tLCAddressSchema.setPostalCity(request.getParameter("City"));
        tLCAddressSchema.setPostalCommunity(request.getParameter("appnt_PostalCommunity"));
        tLCAddressSchema.setPostalCounty(request.getParameter("County"));
        tLCAddressSchema.setPostalProvince(request.getParameter("Province"));
        tLCAddressSchema.setPostalStreet(request.getParameter("appnt_PostalStreet"));
        tLCAddressSchema.setHomeCode(request.getParameter("AppntHomeCode"));
        tLCAddressSchema.setHomeNumber(request.getParameter("AppntHomeNumber"));
        
        String tImpartNum[] = request.getParameterValues("ImpartGridNo");
	String tImpartVer[] = request.getParameterValues("ImpartGrid1");            //告知版别
	String tImpartCode[] = request.getParameterValues("ImpartGrid2");           //告知编码
	String tImpartContent[] = request.getParameterValues("ImpartGrid3");        //告知内容
	String tImpartParamModle[] = request.getParameterValues("ImpartGrid4");        //填写内容
		//String tImpartCustomerNoType[] = request.getParameterValues("ImpartGrid5"); //告知客户类型
		//String tImpartCustomerNo[] = request.getParameterValues("ImpartGrid6");     //告知客户号码
			
			int ImpartCount = 0;
			if (tImpartNum != null) ImpartCount = tImpartNum.length;
	        
			for (int i = 0; i < ImpartCount; i++)	{
	    
				LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
				
                                tLCCustomerImpartSchema.setProposalContNo(request.getParameter("ContNo"));
                                tLCCustomerImpartSchema.setContNo(request.getParameter("ContNo"));
				//tLCCustomerImpartSchema.setCustomerNo(tLDPersonSchema.getCustomerNo());
				tLCCustomerImpartSchema.setCustomerNoType("A");
				tLCCustomerImpartSchema.setImpartCode(tImpartCode[i]);
				tLCCustomerImpartSchema.setImpartContent(tImpartContent[i]);
				tLCCustomerImpartSchema.setImpartParamModle(tImpartParamModle[i]);
				tLCCustomerImpartSchema.setImpartVer(tImpartVer[i]) ;
				tLCCustomerImpartSet.add(tLCCustomerImpartSchema);
			}
			
             System.out.println("end set schema 告知信息..."+ImpartCount);
	                                                                                
	} // end of else                  
	
	String tAssistSaleChnl = request.getParameter("AssistSaleChnl");
  	String tAssistAgentCode = request.getParameter("AssistAgentCode");
	LCExtendSchema tLCExtendSchema = new LCExtendSchema();
	tLCExtendSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCExtendSchema.setAssistSalechnl(tAssistSaleChnl);
	tLCExtendSchema.setAssistAgentCode(tAssistAgentCode);
	tVData.add(tLCExtendSchema);
	
	LCContSubSchema tLCContSubSchema = new LCContSubSchema();
	tLCContSubSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCContSubSchema.setTaxFlag(request.getParameter("TaxFlag"));
	tLCContSubSchema.setBatchNo(request.getParameter("BatchNo"));
	tLCContSubSchema.setGrpNo(request.getParameter("GrpNo"));
	tLCContSubSchema.setTaxNo(request.getParameter("TaxNo"));
	tLCContSubSchema.setTaxPayerType(request.getParameter("TaxPayerType"));
	tLCContSubSchema.setInsuredId(request.getParameter("InsuredId"));
	tLCContSubSchema.setTransFlag(request.getParameter("TransFlag"));
	tLCContSubSchema.setCreditCode(request.getParameter("CreditCode"));
	tLCContSubSchema.setPrintType(request.getParameter("printtype"));
	tLCContSubSchema.setUuid(request.getParameter("uuid"));
	System.out.println("end setSchema:"+request.getParameter("printtype"));   
	System.out.println("end setSchema:"+request.getParameter("uuid"));  
		if("1".equals(request.getParameter("TaxFlag"))){
	tLCContSubSchema.setGOrgancomCode(request.getParameter("GOrgancomCode"));
	tLCContSubSchema.setGTaxNo(request.getParameter("GTaxNo"));}
    tLCContSubSchema.setAgediscountfactor(request.getParameter("Agediscountfactor"));	
    tLCContSubSchema.setSupdiscountfactor(request.getParameter("Supdiscountfactor"));
    tLCContSubSchema.setGrpdiscountfactor(request.getParameter("Grpdiscountfactor"));
    tLCContSubSchema.setTotaldiscountfactor(request.getParameter("Totaldiscountfactor"));
    tLCContSubSchema.setDiscountMode(request.getParameter("DiscountMode"));
	tLCContSubSchema.setPremMult(request.getParameter("PremMult"));
	tVData.add(tLCContSubSchema);
	
	if(LoadFlag.equals("5")){
        tLCApproveSchema.setPrtNo(request.getParameter("PrtNo"));
        tLCApproveSchema.setCustomerType("00");       
        tLCApproveSchema.setContType("1");
        ttVData.add(tG);
	    ttVData.add(tLCApproveSchema); 
	    ttVData.add(tLCInsuredSchema);	
	    ttVData.add(tOLDLCInsuredDB);
	}
	                                               
   System.out.println("end setSchema:");                                             
	// 准备传输数据 VData                                                           
	tVData.add( tLCContSchema );  
	tVData.add( tLCAddressSchema );   
	tVData.add( tLCAppntSchema );   
	tVData.add( tLCAccountSchema );    
	tVData.add(tLCCustomerImpartSet);                                                 
	tVData.add( tG );
	                                                              
	                                                                                
	//传递非SCHEMA信息                                                              
  TransferData tTransferData = new TransferData();                                  
  String SavePolType="";                                                            
  String BQFlag=request.getParameter("BQFlag");                                     
  if(BQFlag==null) SavePolType="0";                                                 
  else if(BQFlag.equals("")) SavePolType="0";                                       
    else  SavePolType=BQFlag;                                                       
                                                                                    
                                                                                    
  tTransferData.setNameAndValue("SavePolType",SavePolType); //保全保存标记，默认为0，标识非保全
  tTransferData.setNameAndValue("GrpNo",request.getParameter("AppntGrpNo"));
  tTransferData.setNameAndValue("GrpName",request.getParameter("AppntGrpName"));
    //modify by zxs 
  tTransferData.setNameAndValue("Authorization", request.getParameter("AppntAuth"));
    //modify by zxs 2019-01-16 原通行证号码
 if(request.getParameter("AppntIDType").equals("a")||request.getParameter("AppntIDType").equals("b")){
   tTransferData.setNameAndValue("AppntPassIDNo", request.getParameter("AppntPassIDNo")); 
 	}else{
 	   tTransferData.setNameAndValue("AppntPassIDNo", "");
 	}
  System.out.println("SavePolType，BQ is 2，other is 0 : " + request.getParameter("BQFlag"));
  tVData.addElement(tTransferData);                                                 
    
                                                                            
	if( tAction.equals( "INSERT" )) tOperate = "INSERT||CONT";                  
	if( tAction.equals( "UPDATE" )) tOperate = "UPDATE||CONT";                  
	if( tAction.equals( "DELETE" )) tOperate = "DELETE||CONT";                  
                                                                                    
	ContUI tContUI = new ContUI();         
	System.out.println("before submit");                             
	if( tContUI.submitData( tVData, tOperate ) == false )                       
	{                                                                               
		Content = " 保存失败，原因是: " + tContUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";
		if(LoadFlag.equals("5")&&tOperate.equals("UPDATE||CONT")){		   
		   ApproveContUI tApproveContUI=new ApproveContUI();
		   tApproveContUI.submitData(ttVData, tOperate);
		   ttVData.clear();
		}		
		tVData.clear();
		tVData = tContUI.getResult();

		// 显示
		// 保单信息
		LCContSchema mLCContSchema = new LCContSchema(); 
                LCAddressSchema mLCAddressSchema = new LCAddressSchema();		
		mLCContSchema.setSchema(( LCContSchema )tVData.getObjectByObjectName( "LCContSchema", 0 ));
		mLCAddressSchema.setSchema(( LCAddressSchema )tVData.getObjectByObjectName( "LCAddressSchema", 0 ));		
		%>
    	<script language="javascript">
    	 	parent.fraInterface.fm.all("ContNo").value = "<%=mLCContSchema.getContNo()%>";
    	 	parent.fraInterface.fm.all("ProposalContNo").value = "<%=mLCContSchema.getProposalContNo()%>";   
    	 	parent.fraInterface.fm.all("AppntNo").value = "<%=mLCContSchema.getAppntNo()%>";  
    	 	parent.fraInterface.fm.all("GrpContNo").value = "<%=mLCContSchema.getGrpContNo()%>";    	 	
    	        parent.fraInterface.fm.all("AppntMakeDate").value = "<%=mLCContSchema.getMakeDate()%>";
    	        parent.fraInterface.fm.all("AppntMakeTime").value = "<%=mLCContSchema.getMakeTime()%>";
    	        parent.fraInterface.fm.all("AddressNo").value = "<%=mLCAddressSchema.getAddressNo()%>";    	        
    	</script>
		<%		
	}
System.out.println("Content:"+Content);	
 
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

