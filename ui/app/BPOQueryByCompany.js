//程序功能：
//创建日期：2008-3-20
//创建人  ：ZhangJianBao
//更新记录：更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

//查询分公司外包件数
function queryBPONo()
{
  if(!verifyInput2())
  {
    return false;
  }

  var subTypeSql = "";
  var subType = fm.subType.value;
  if(subType != null && subType != "")
  {
    if(subType == "TB01" || subType == "TB05")
    {
      subTypeSql = " and exists (select SubType from ES_Doc_Main where DocCode = BussNo and SubType = '" + subType + "') ";
    }
    else
    {
      alert("扫描件类型选择有误！");
      return;
    }
  }

  var sql = "select a.ManageCom, b.Name, count(1) from BPOMissionState a, LDCom b "
          + "where a.ManageCom = b.ComCode and a.ManageCom like '" 
          + fm.manageCom.value + "%' " + subTypeSql
          + getWherePart("a.State","state"," >= ")
          + getWherePart("a.MakeDate","startDate",">=")
          + getWherePart("a.MakeDate","endDate","<=")
          + " group by a.ManageCom, b.Name";
  
  turnPage.pageDivName = "divPage";
  turnPage.queryModal(sql, BPONoGrid);
  return true;
}

//初始化查询条件
function initInputBox()
{
  fm.manageCom.value = tManageCom;
  fm.endDate.value = tCurrentDate;
  var sql = "select date('" + tCurrentDate + "') - 3 month from dual ";
  fm.startDate.value = easyExecSql(sql);
}

/**
* EXCEL下载
*/
function queryDown()
{
  if(BPONoGrid.mulLineCount == 0)
  {
    alert("没有需要下载的数据");
    return false;
  }
  fm.sql.value = turnPage.strQuerySql;
  //fm.target='_blank';
  fm.action = "BPOQueryByCompanyDown.jsp";
  fm.submit();
}
