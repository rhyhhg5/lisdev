//程序名称：ScanContInput.js
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{
  if(!verifyInput2())
    return false;
  var ApplyDate = fm.InputDate.value;
  if(strCurDay!==ApplyDate)
  {
    alert("申请日期不是当天！");
    return false;
  }
  if( verifyInput2() == false )
    return false;
  cPrtNo = fm.PrtNo.value;
  cManageCom = fm.ManageCom.value;
  if(cPrtNo == "")
  {
    alert("请录入印刷号！");
    return;
  }else{
  	var srtPrtno = CheckPrtno(cPrtNo);
 	if (srtPrtno != "") {
 		alert(srtPrtno);
	  return false;
 	}
  }
  if(cManageCom == "")
  {
    alert("请录入管理机构！");
    return;
  }
  //if(isNumeric(cPrtNo)==false)
  //{
  //  alert("印刷号应为数字");
  //  return false;
  //}
  if(type=='2')//对于集体保单的申请
  {
    if (GrpbeforeSubmit() == false)
    {
      alert("已存在该印刷号，请选择其他值!");
      return false;
    }

  }
  else //对于个人保单的申请
  {
    if (beforeSubmit() == false)
    {
      alert("已存在该印刷号，请选择其他值!");
      return false;
    }
  }
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  easyQueryClick();
}

/*********************************************************************
 *  执行新契约扫描的EasyQuery
 *  描述:查询显示对象是扫描件.显示条件:扫描件已上载成功
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
  // 初始化表格

  
  if(!verifyInput2())
    return false;
  // 书写SQL语句
  var strSQL = "";
  if(type=='1')
  {
  initGrpGrid1();
    strSQL = "select lwmission.missionprop1,lwmission.missionprop3,lwmission.missionprop2,lwmission.missionprop4,lwmission.missionid,lwmission.submissionid from lwmission where 1=1 "
             + " and activityid = '0000001098' "
             + " and processid = '0000000003'"
             + " and missionprop3 like '"+manageCom+"%%' "
             + getWherePart('missionprop1','PrtNo')
             + getWherePart('missionprop2','InputDate')
             + getWherePart('missionprop3','ManageCom','like')
             + getWherePart('missionprop4','Operator')
             + " order by lwmission.missionprop1"
             ;
    turnPage.queryModal(strSQL,GrpGrid1);
      return true;
  }
  else if (type=='2')
  {
  	initGrpGrid();
		strSQL = "select * "
						+" from( "				     
				    +" select lwmission.missionprop1,lwmission.missionprop3,lwmission.missionprop2, "
				    +" lwmission.missionprop4,lwmission.missionid,lwmission.submissionid, "
						+" (select case when count(1)<>0 then '有问题件' else '没有问题件' end from LCGrpIssuePol where grpcontno in (select grpcontno from lcgrpcont where PrtNo =lwmission.missionprop1) and state<>'5' ) "
				    +" from lwmission where 1=1 "
            +" and activityid = '0000002098' "
            +" and processid = '0000000004' "
            +" and missionprop5 = '0' "
            + getWherePart('missionprop1','PrtNo')
            + getWherePart('missionprop2','InputDate')
            + getWherePart('missionprop3','ManageCom','like')
            + getWherePart('missionprop4','Operator')
            + " order by lwmission.missionprop1  ) as X"
             ;
     turnPage.queryModal(strSQL,GrpGrid); 
       return true; 
    }
  //turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //
  ////判断是否查询成功
  //if (!turnPage.strQueryResult)
  //{
  //  alert("没有已申请的投保单，请录入印刷号开始录入！");
  //  return "";
  //}
  //
  ////查询成功则拆分字符串，返回二维数组
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //
  ////设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  //turnPage.pageDisplayGrid = GrpGrid;
  //
  ////保存SQL语句
  //turnPage.strQuerySql     = strSQL;
  //
  ////设置查询起始位置
  //turnPage.pageIndex       = 0;
  //
  ////在查询结果数组中取出符合页面显示大小设置的数组
  //var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //
  ////调用MULTILINE对象显示查询结果
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

}
function GoToInput()
{
	if(type=='2')
 {
  var i = 0;
  var checkFlag = 0;
  var state = "0";

  for (i=0; i<GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getSelNo(i))
    {
      checkFlag = GrpGrid.getSelNo();
      break;
    }
  }

  if (checkFlag)
  {
    var	prtNo = GrpGrid.getRowColData(checkFlag - 1, 1);
    var ManageCom = GrpGrid.getRowColData(checkFlag - 1, 2);
    var MissionID = GrpGrid.getRowColData(checkFlag - 1, 5);
    var SubMissionID = GrpGrid.getRowColData(checkFlag - 1, 6);
    var PolApplyDate = GrpGrid.getRowColData(checkFlag - 1, 3);

    //var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;
    //var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    //var strReturn = window.showModalDialog(urlStr, "", sFeatures);
    var strReturn="1";
    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    //校验印刷号是否符合规则
    if (prtNo != "") {
    	var srtPrtno = CheckPrtno(prtNo);
     	if (srtPrtno != "") {
     		alert(srtPrtno);
    	  return false;
     	}
	}
    var strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and PolState=1002 ";
	  var arrResult = easyExecSql(strSql);
	  if (arrResult!=null && arrResult[0][1]!=operator) {
	    alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
	    return;
	  }
    
    var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 

    if (strReturn == "1"){
        //var type = "2";
		window.open("./ContPolInputNoScanMain.jsp?ScanFlag=0&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PolApplyDate="+PolApplyDate+"&ActivityID=0000002098"+"&type="+type, "", sFeatures);
		}
  }
  else
  {
    alert("请先选择一条保单信息！");
  }
	}else if(type=='1'){
	  var i = 0;
  var checkFlag = 0;
  var state = "0";

  for (i=0; i<GrpGrid1.mulLineCount; i++)
  {
    if (GrpGrid1.getSelNo(i))
    {
      checkFlag = GrpGrid1.getSelNo();
      break;
    }
  }

  if (checkFlag)
  {
    var	prtNo = GrpGrid1.getRowColData(checkFlag - 1, 1);
    var ManageCom = GrpGrid1.getRowColData(checkFlag - 1, 2);
    var MissionID = GrpGrid1.getRowColData(checkFlag - 1, 5);
    var SubMissionID = GrpGrid1.getRowColData(checkFlag - 1, 6);
    var PolApplyDate = GrpGrid1.getRowColData(checkFlag - 1, 3);

    //var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;
    //var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    //var strReturn = window.showModalDialog(urlStr, "", sFeatures);
    //校验印刷号是否符合规则
    if (prtNo != "") {
    	var srtPrtno = CheckPrtno(prtNo);
     	if (srtPrtno != "") {
     		alert(srtPrtno);
    	  return false;
     	}
	}
    var strReturn="1";
    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    var strSql = "select * from ldsystrace where PolNo='" + prtNo + "' and PolState=1002 ";
	  var arrResult = easyExecSql(strSql);
	  if (arrResult!=null && arrResult[0][1]!=operator) {
	    alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
	    return;
	  }
    
    var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 

    if (strReturn == "1"){
        window.open("./ContInputNoScanMain.jsp?ScanFlag=0&LoadFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PolApplyDate="+PolApplyDate+"&ActivityID=0000001098", "", sFeatures);
				
				}
  }
  else
  {
    alert("请先选择一条保单信息！");
  }	
	}
}
function beforeSubmit()
{
  var strSQL = "";
  strSQL = "select missionprop1 from lwmission where 1=1 "
           + " and activityid = '0000001098' "
           + " and processid = '0000000003'"
           + " and missionprop1='"+fm.PrtNo.value+"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    return false;
  }

  strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
           "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    return false;
  }
}
function GrpbeforeSubmit()
{
  var strSQL = "";
  strSQL = "select missionprop1 from lwmission where 1=1 "
           + " and activityid = '0000002098' "
           + " and processid = '0000000004'"
           + " and missionprop1='"+fm.PrtNo.value+"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (turnPage.strQueryResult)
  {

    return false;
  }

  strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
           "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    return false;
  }
}
