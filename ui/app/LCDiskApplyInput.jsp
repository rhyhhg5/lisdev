<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="LCDiskApplyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <%@include file="LCDiskApplyInit.jsp"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>

</head>
<body  onload="initForm();" >
  <form action="./LCDiskApplySave.jsp" method=post 
  	name=fmlog target="fraSubmit" ENCTYPE="multipart/form-data">
<table class = common>
    <TR  class= common>
      <TD class = title >
        选择导入的文件：
      </TD>
      <TD class = common width=30%>
        <Input  type="file" name=FileName size=30>
      </TD>
    </TR>
    
    <TR class = common >
      <td class = common colspan=2>
        <INPUT class=cssButton VALUE="导入" TYPE=button onclick = "submitForm();" > 
        <INPUT class=cssButton VALUE="重置" TYPE=button onclick="resetForm();">         
        <INPUT class=cssButton VALUE="生成错误清单" TYPE=button onclick = "creatExcel();" > 	
      </td>
    </TR>
    
    <input type=hidden name=ImportFile>
    
</table>

  </form>
  
  <form name=fmquery action=./DiskErrList.jsp target=fraSubmit method=post>
      <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>导入状态查询－请输入查询条件：
		</td>
	</tr>
      </table>
	
      <table  class= common align=center>                     
      	<TR  class= common>
	          <TD  class= title>
	          批次号:
	          </TD>	          
	          <TD  class= input>
	            	<Input class=common  name=BatchNo verify="批次号|NUM">
	          </TD>
	          <TD  class= title>
	          印刷号:
	          </TD>	          
	          <TD  class= input>
	            	<Input class=common  name=prtNo verify="印刷号码|int" >
	          </TD>    	               	
	          <TD  class= title></TD>	          
	          <TD  class= input></TD>            	
        </TR>         
      </Table>  
   
      <INPUT VALUE="查  询" class=cssButton TYPE=button onclick="easyQueryClick();"> 
      
      <Table>
  
   	<Div  id= "divDiskErr" style= "display: ''">
   	 <Table  class= common>
       	   <TR  class= common>
        	<TD text-align: left colSpan=1>
            		<span id="spanDiskErrQueryGrid" ></span> 
  		</TD>
      	   </TR>
         </Table>	   				
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
       </Div>
   	
  </form>
  
  <form  method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>导入确认－请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            批次号
          </TD>
          <TD  class= input>
            <Input class= common name=BatchNo verify="批次号|NUM">
          </TD>
          <TD  class= title>
            印刷号
          </TD>
          <TD  class= input>
            <Input class= common name=tPrtNo verify="印刷号码|int">
          </TD>
          <TD  class= title>
            投保人
          </TD>
          <TD  class= input>
            <Input class="common"  dateFormat="short" name=AppntNo verify="投保人|len<=20">
          </TD>
          <TD  class= title>
            被保人
          </TD>
          <TD  class= input>
            <Input class="common"  name=InsuredNo verify="被保人|len<=20" >
          </TD>
          
        </TR>
      	<TR  class= common>
      		<TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
           <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
          </TD>
      		<TD  class= title>
            导入日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker"  dateFormat="short" name=ApplyDate verify="导入日期|date">
          </TD>
          
          <TD  class= title>
            业务员
          </TD>
          <TD  class= input>
            <Input class="common" name=Operator verify="业务员|int&len<=20" >
          </TD>
          <TD  class= title></TD>
          <TD  class= input></TD>
        </TR>        
    </table>
          <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick2();"> 
    <table>
    	<tr>
        	<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrp1);">
    		</td>
    		<td class= titleImg>
    			投保单信息
    		</td>
    	</tr>
    	<tr>
          <INPUT  type= "hidden" class= Common name= MissionID  value="">
          <INPUT  type= "hidden" class= Common name= SubMissionID  value="">
    	</tr>
    </table>
  	<Div  id= "divLCGrp1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanGrpGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage2.lastPage();">					
  	</div>
  	<p>
      	<INPUT VALUE="投保书修改" class = cssButton TYPE=button onclick="GoToInput();">
      	<input type =button class=cssButton value="复核险种信息" onclick="showComplexRiskInfo();"> </TD>
      	<INPUT VALUE="选择导入确认" class = cssButton TYPE=button onclick="finish();">
      	<INPUT VALUE="批次导入确认" class = cssButton TYPE=button onclick="batchFinish();"> 
      	<INPUT VALUE="选择删除" class = cssButton TYPE=button onclick="deleteCont();">
      	<!--INPUT VALUE="批次删除" class = cssButton TYPE=button onclick="batchDeleteCont();"-->
  	</p>  
        <!--
        <Input class= common type=hidden name=Operator >  	     
        -->
        <Input class= common type=hidden name=ContNo >
        <Input class= common type=hidden name=PrtNo >
  </form>  
    
  <span id="spanCode"  style="display: none; position:absolute; slategray">
  </span>  
</body>
</html>
