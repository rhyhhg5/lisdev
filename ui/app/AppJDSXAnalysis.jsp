<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：AppJDSXAnalysis.jsp
//程序功能：F1报表生成
//创建日期：2007-09-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="AppJDSXAnalysis.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./AppJDSXAnalysisSave.jsp" method=post name=fm target="fraSubmit">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input><Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>
          <TD  class= title>销售渠道</TD>
	        <TD  class= input><Input class=codeNo name=SaleChnl ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary></TD>          
        </TR>
       <TR  class= common>
		  <TD  class= title>保单申请日期起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>保单申请日期止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD>        	
      </TR>
      <TR class= common>
      	<TD class=title>保单签发起期</TD>
      	<TD class=input>
      		<input name=SignStartDate class=coolDatePicker dateFormat=short verify="保单签发起期|Date">
      	</TD>
      	<TD class=title>保单签发止期</TD>
      	<TD class=input>
      		<input name=SignEndDate class=coolDatePicker dateFormat=short verify="保单签发止期|Date">
      	</TD>
      </TR>
      <TR class= common>
      	<TD class=title>保单类型</TD>
      	<TD class=input>
      		<input name=TheContType class=codeNo CodeData="0|^0|标准件^9|银保通^99|简易件" 
      		ondblclick="return showCodeListEx('TheContType',[this,TheContTypeName],[0,1],null,null,null,1);" 
      		onkeyup="return showCodeListKeyEx('TheContType',[this,TheContTypeName],[0,1],null,null,null,1);"
      		><input name=TheContTypeName class=codeName readonly=true>
      	</TD>
      	<TD class=title></TD>
      	<TD class=input></TD>
      </TR>
    </table>
	  <input name= "AnalysisSql" type=hidden>
    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
			<hr>
	
	    <div>
        <font color="red">
            <ul>查询字段说明：
                <li>统计起止期：保单申请日期的起止期。</li>                
            </ul>
            <ul>统计结果说明：
                <li>填单日期：投保人填单时间（即保单申请日期）</li>
                <li>交单日期：收单日期</li>
                <li>保单类型：简易件中a-信保通平台出单,b-电子商务平台出单,1-境外救援,2-意外险平台,3-高原疾病,5-电话销售,6-银行保险</li>
                <li>交单时效：交单日期与填单日期的差值</li>
            </ul>
        </font>
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 