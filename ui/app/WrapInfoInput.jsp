<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Verifyinput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="WrapInfoInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="WrapInfoInit.jsp"%>
    <title>套餐信息查询</title>
</head>
<body onload="initForm();" >
    <form action="./WrapInfoSave.jsp" method=post name=fm target="fraSubmit">
        <!-- 个人保单信息部分 -->
        <table class= common border=0 width=100%>
            <tr><td class= titleImg align= center>请输入套餐查询条件：</td></tr>
        </table>
        <table class= common align=center>
            <TR class= common>
                <TD class= title>套餐编码</TD>
                <TD class= input><input class="codeNo" name=riskWrapCode ondblclick="return showCodeList('RiskWrapCode',[this,riskWrapName],[0,1],null,null,null,1);" ><input class=codename name=riskWrapName readonly ></TD>
                <TD class= title>团险/个险</TD>
                <TD class= input><input class="codeNo" name=riskWrapType CodeData="0|^G|团险|^I|个险" ondblclick="return showCodeListEx('RiskWrapType',[this,riskWrapTypeName],[0,1]);" ><input class=codename name=riskWrapTypeName readonly ></TD>
                <TD class= title>套餐类型</TD>
                <TD class= input><input class="codeNo" name=wrapType CodeData="0|^2|意外|^5|电销^6|银邮^8|银保万能|^9|国际业务" ondblclick="return showCodeListEx('WrapType',[this,wrapTypeName],[0,1]);" ><input class=codename name=wrapTypeName readonly=true ></TD>
            </TR>
            <TR class= common>
                <TD class= title>套餐有效标志</TD>
                <TD class= input><input class="codeNo" name=canSale CodeData="0|^0|有效|^1|停售" ondblclick="return showCodeListEx('CanSale',[this,CanSaleName],[0,1]);" ><input class=codename name=CanSaleName readonly=true ></TD>
            </TR>
        </table>
        <input type="hidden" class="common" name="querySql" >
        <input value="查询险种信息" class="cssButton" type=button onclick="easyQueryClick();">
        <input value="下载清单" class="cssButton" type=button onclick="downloadList();">
        <br>
        <table>
            <tr>
                <td class=common>
                    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divWrapInfo);">
                </td>
                <td class= titleImg>套餐列表</td>
            </tr>
        </table>
        <div id= "divWrapInfo" style= "display: ''">
            <table class= common>
                <tr class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanWrapInfoGrid" ></span>
                    </td>
                </tr>
            </table>
            <div id= "divPage" align=center style= "display: 'none' ">
                <input value="首　页" class="cssButton" type=button onclick="turnPage.firstPage();">
                <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
                <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
                <input value="尾　页" class="cssButton" type=button onclick="turnPage.lastPage();">
            </div>
        </div>
        <br>
        <table>
            <tr>
                <td class=common>
                    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRisk);">
                </td>
                <td class= titleImg>险种列表</td>
            </tr>
        </table>
        <div id= "divRisk" style= "display: ''">
            <table class= common>
                <tr class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanRiskInfoGrid" ></span>
                    </td>
                </tr>
            </table>
            <div id= "divPage1" align=center style= "display: 'none' ">
                <input value="首　页" class="cssButton" type=button onclick="turnPage1.firstPage();">
                <input value="上一页" class="cssButton" type=button onclick="turnPage1.previousPage();">
                <input value="下一页" class="cssButton" type=button onclick="turnPage1.nextPage();">
                <input value="尾　页" class="cssButton" type=button onclick="turnPage1.lastPage();">
            </div>
        </div>
        <br>
        <table>
            <tr>
                <td class=common>
                    <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDuty);">
                </td>
                <td class= titleImg>责任列表</td>
            </tr>
        </table>
        <div id= "divDuty" style= "display: ''">
            <table class= common>
                <tr class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanDutyInfoGrid" ></span>
                    </td>
                </tr>
            </table>
            <div id= "divPage2" align=center style= "display: 'none' ">
                <input value="首　页" class="cssButton" type=button onclick="turnPage2.firstPage();">
                <input value="上一页" class="cssButton" type=button onclick="turnPage2.previousPage();">
                <input value="下一页" class="cssButton" type=button onclick="turnPage2.nextPage();">
                <input value="尾　页" class="cssButton" type=button onclick="turnPage2.lastPage();">
            </div>
        </div>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
