<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AppStatisticSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
boolean errorFlag = false;

//获得session中的人员信息




GlobalInput tG = (GlobalInput)session.getValue("GI");

//生成文件名
Calendar cal = new GregorianCalendar();
String min=String.valueOf(cal.get(Calendar.MINUTE));
String sec=String.valueOf(cal.get(Calendar.SECOND));
String type= request.getParameter("QYType");
String downLoadFileName = "";
if(type.equals("1")){
	downLoadFileName = "个单契约投保统计报表_"+tG.Operator+"_"+ min + sec + ".xls";
}else{
	downLoadFileName = "个单契约退保统计报表_"+tG.Operator+"_"+ min + sec + ".xls";	
}
String mStartDate=request.getParameter("StartDate");
String mEndDate=request.getParameter("EndDate");
String filePath = application.getRealPath("temp");
String tOutXmlPath = filePath +File.separator+ downLoadFileName;
System.out.println("OutXmlPath:" + tOutXmlPath);

String querySql = request.getParameter("querySql");

querySql = querySql.replaceAll("%25","%");

	//设置表头
	String[][] tTitle = new String[2][];
    tTitle[0] = new String[] { "制表人：" , tG.Operator, "",
			"统计时间：" +mStartDate+"至"+mEndDate,"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","" };
    tTitle[1] = new String[] {"保险合同号","投保书印刷号","状态","投保人","被保险人","险种代码","档次","保额","保费","申请日期","生效日期"," 收单日期","初审日期","初审人员","扫描日期","扫描人员","录入日期","录入人员","复核日期","复核人员","自核通过标志","填单问题","体检件数","契约调查","变更承保","核保人员","人工核保决定","人工核保通过日期","收费方式","收费日期","签单日期","合同接收日期","回执回销日期","管理机构","业务员代码","交费频次"};
	//表头的显示属性
	int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36};
  
//数据的显示属性
int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36};

CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
createexcellist.createExcelFile();
String[] sheetName ={"list"};
createexcellist.addSheet(sheetName);
int row = createexcellist.setData(tTitle,displayTitle);

if(row ==-1) errorFlag = true;
createexcellist.setRowColOffset(row+1,0);//设置偏移
if(createexcellist.setData(querySql,displayData)==-1)
{
	errorFlag = true;
	System.out.println(errorFlag);
}
if(!errorFlag)
//写文件到磁盘
try{
   createexcellist.write(tOutXmlPath);
}catch(Exception e)
{
	errorFlag = true;
	System.out.println(e);
}
//返回客户端
if(!errorFlag)
	downLoadFile(response,filePath,downLoadFileName);
out.clear();
	out = pageContext.pushBody();
%>
