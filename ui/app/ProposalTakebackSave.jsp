<%
//程序名称：ReProposalPrintInput.jsp
//程序功能：
//创建日期：2002-11-25
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%!
	String handleFunction(HttpSession session, HttpServletRequest request) {
	int nIndex = 0;
	String tContNo = request.getParameter("mContNo");
	String tGetpolDate = request.getParameter("mGetpolDate");
	String tGetpolMan = request.getParameter("mGetpolMan");
	String tSendPolMan=request.getParameter("mSendPolMan");
	String tGetPolOperator=request.getParameter("mGetPolOperator");
	String tManageCom = request.getParameter("mManageCom");
	String tAgentCode=request.getParameter("mAgentCode");
	String tContType=request.getParameter("mContType");
	String strOperation = request.getParameter("fmtransact");

	GlobalInput globalInput = new GlobalInput();

	if( (GlobalInput)session.getValue("GI") == null )
	{
		return "网页超时或者是没有操作员信息，请重新登录";
	}
	else
	{
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
	}

	if( tContNo == null ) {
		return "没有输入需要的打印参数";
	}
  LCContGetPolSchema tLCContGetPolSchema=new LCContGetPolSchema();
  LCContGetPolUI tLCContGetPolUI =new LCContGetPolUI();
  tLCContGetPolSchema.setContNo(tContNo);
  tLCContGetPolSchema.setGetpolDate(tGetpolDate);
  tLCContGetPolSchema.setGetpolMan(tGetpolMan);
  tLCContGetPolSchema.setSendPolMan(tSendPolMan);
  tLCContGetPolSchema.setGetPolOperator(tGetPolOperator);
  tLCContGetPolSchema.setManageCom(tManageCom);
  tLCContGetPolSchema.setAgentCode(tAgentCode);
  tLCContGetPolSchema.setContType(tContType);

	VData vData = new VData();

	vData.addElement(tLCContGetPolSchema);
	vData.add(globalInput);

	try {
		if( !tLCContGetPolUI.submitData(vData, strOperation) )
		{
	   		if ( tLCContGetPolUI.mErrors.needDealError() )
	   		{
	   			return tLCContGetPolUI.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		return "保存失败，但是没有详细的原因";
			}
		}

	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		return ex.getMessage();
	}
	return "";
}
%>
<%
String FlagStr = "";
String Content = "";

try {
	Content = handleFunction(session, request);

	if( Content.equals("") ) {
		String strTemplatePath = application.getRealPath("xerox/printdata") + "/";
		FlagStr = "Succ";
		Content = "保存成功";
	} else {
		FlagStr = "Fail";
	}
} catch (Exception ex) {
	ex.printStackTrace();
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

