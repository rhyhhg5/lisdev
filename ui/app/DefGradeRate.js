var turnPage = new turnPageClass();
var tdutysql="";
var showInfo;
function saveDefGradeRate()
{
    //校验规则至少录入一个给付比例
	var tcount=DisableRateGrid.mulLineCount;
	var tflag=false;
	for(var i=0;i< tcount ;i++){
		var trate=DisableRateGrid.getRowColData(i,5);
		var tgrade=DisableRateGrid.getRowColData(i,4);
		if(trate!="" ){
			if(!isNaN(trate) && trate >=0 && trate<=1){
				tflag=true;
			}else{
				alert("责任"+fm.DefContDuty.value+"的"+tgrade+"的给付比例不是0到1的有效数字！");
				return;
			}
		}
			
	}
	if(tflag==true){
		fm.mOperate.value="DELETE&INSERT";
		fm.mGrpContNo.value = Grpcontno;
		var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit();
	}else{
		alert("请至少录入一条给付比例信息！");
		return;
	}
	
}

function deleteDefGradeRate()
{
	if(fm.DefContPlan.value=="" || fm.DefContPlan.value==null){
		alert("请选择保障计划！");
		return;
	}
	if(fm.DefContDuty.value=="" || fm.DefContDuty.value==null){
		alert("请选择责任编码！");
		return;
	}
		fm.mOperate.value="DELETE";
		fm.mGrpContNo.value = Grpcontno;
		var showStr = "正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit();
}

function afterCodeSelect( cCodeName, Field ) {
	  if(cCodeName=="DefContPlan") {
		  tdutysql=" 1 and grpcontno=#"+Grpcontno+"# and contplancode=#"+fm.DefContPlan.value+"# and riskcode=#460301#";
	  }
	  if(cCodeName=="DefContDuty"){
		  var tdutycode=fm.DefContDuty.value;
		  initDisableRateGridData(tdutycode);
	  }
}

function initDisableRateGridData(mdutycode){
	var tinitdutysql="";
	var ratesql="select 1 from LCContPlanDutyDefGrade ld where ld.grpcontno='"+Grpcontno+"' and ld.ContPlanCode='"+fm.DefContPlan.value+"' and ld.DutyCode='"+fm.DefContDuty.value+"' ";
	var rateresult = easyExecSql(ratesql);
	if(!rateresult && tLookFlag=="0"){
	    tinitdutysql=" select '','"+mdutycode+"',code,codealias,comcode from ldcode where codetype='"+mdutycode+"Rate' and codename='"+mdutycode+"' order by int(code) ";
	}else{
	    tinitdutysql=" select '','"+mdutycode+"',code,codealias,(select GetRate from LCContPlanDutyDefGrade ld where ld.grpcontno='"+Grpcontno+"' and ld.ContPlanCode='"+fm.DefContPlan.value+"' and ld.DutyCode='"+fm.DefContDuty.value+"' and ld.DeformityGrade=ldcode.code )from ldcode where codetype='"+mdutycode+"Rate' and codename='"+mdutycode+"' order by int(code) ";
	}
	turnPage.queryModal(tinitdutysql,DisableRateGrid);
}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
    initDisableRateGridData(fm.DefContDuty.value); 
  }
}

function goback(){
	top.close();
}
function initButton(){
	if(tLookFlag != "0"){
		fm.all("SaveIt").style.display = "none";
		fm.all("DeleteIt").style.display = "none";
	}
}