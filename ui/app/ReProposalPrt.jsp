<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="ReProposalPrt.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ReProposalPrtInit.jsp"%>
<title>重打个单</title>
</head>
<body onload="initForm();" >
	<form action="./ReProposalPrtSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr>
				<td class= titleImg align= center>请输入保单查询条件：</td>
			</tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>保险合同号</TD>
				<TD class= input><Input class= common name=ContNo verify="保险合同号|int"></TD>
				<TD class= title>管理机构</TD>
					<TD class= input><Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>
				<!--TD class= title>险种编码</TD>
				<TD class= input><Input class="code" name=RiskCode ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);"></TD-->
				<TD class= title>业务员组别</TD>
				<TD class= input><Input class=codeNo name=AgentGroup verify="业务员组别|code:AgentCode" ondblclick="return showCodeList('AgentGroup',[this,AgentGroupName],[0,1]);" onkeyup="return showCodeListKey('AgentGroup',[this,AgentGroupName],[0,1]);"><input class=codename name=AgentGroupName readonly=true ></TD>
			</TR>
			<TR class= common>
				<TD class= title>业务员代码</TD>
				<TD class= input><Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this,AgentCodeName],[0,1]);" onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeName],[0,1]);"><input class=codename name=AgentCodeName readonly=true ></TD>
				<TD class= title>投保人</TD>
				<TD class= input><Input class= common name=AppntName verify="投保人|len<=20"></TD>
				<TD class= title>被保人</TD>
				<TD class= input><Input class= common name=InsuredName verify="被保人|len<=20"></TD>
			</TR>
			<TR class= common id="PrintStateTRID" style="display: ''">
				<TD class= title>保单类型</TD>
				<TD class= input colspan="5">
				  <Input class=codeNo name=CardFlag value="" CodeData="0|^0|其他保单^8|万能险" ondblclick="return showCodeListEx('ReCardFlag',[this,CardFlagName],[0,1]);" onkeyup="return showCodeListKey('ReCardFlag',[this,CardFlagName],[0,1]);"><input class=codename name=CardFlagName value="" readonly=true> 不选择即查询所有类型保单</TD>
			</TR>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" TYPE=button onclick="easyQueryClick();">
	</form>
	<form action="./ReProposalPrtSave.jsp" method=post name=fmSave target="fraSubmit">
		<INPUT VALUE="提交申请" class="cssButton" TYPE=button onclick="printPol();">
		<input VALUE="yes" name = "" type=hidden onclick="dataConfirm(this);">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<Div id= "divLCPol1" style= "display: ''">
			<table class= common>
				<TR class= common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" ></span>
					</td>
				</tr>
			</table>
			<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
		</div>
		<input type=hidden id="fmtransact" name="fmtransact">
	</form>
<span id="spanCode"style="display: none; position:absolute; slategray"></span>
</body>
</html>
