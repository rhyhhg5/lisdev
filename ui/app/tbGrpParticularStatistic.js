var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;
  var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  var tRiskCode = fm.RiskCode.value;
  var tManageCom = fm.ManageCom.value;
  var tsalechnl = fm.saleChnl.value;
  var tagentcom = fm.agentCom.value;
  var type = fm.QYType.value;
  var tSQL = "";
  if(dateDiff(tStartDate,tEndDate,"M")>12)
  {
	alert("统计期最多为十二个月！");
	return false;
  }
  if(dateDiff(tStartDate,tEndDate,"M")<1){
      alert("统计止期比统计统计起期早");
      return false;
  }
  var length=tManageCom.split("").length;
  var tManageCom1="";
  var tManageCom2="";
  if(length==8){
      tManageCom1="length('"+tManageCom+"')";
	  tManageCom2="length('"+tManageCom+"')";
  }else{
      tManageCom1="length('"+tManageCom+"')*2";
	  tManageCom2="length('"+tManageCom+"')";
  }	
  
  if(tsalechnl==""||tsalechnl==null){
     tsalechnl="1";
  }
  
  if(tagentcom==""||tagentcom==null){
     tagentcom="1"; 
  }
    
//  var i = 0;
//  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//  //showSubmitFrame(mDebug); 
//	//fm.fmtransact.value = "PRINT";
//	fm.target = "f1print";
//	fm.all('op').value = '';
//	fm.submit();
//	showInfo.close();

	if(type=="1"){
	   tSQL =" select A,S,B, "
          +" to_zero(C1)+to_zero(C2)+to_zero(C3),"
          +" to_zero(D1)+to_zero(D2),"
          +" to_zero(E1)+to_zero(E2)+to_zero(E3),"
          +" to_zero(F1)+to_zero(F2)+to_zero(F3),'' "
          +" from "
          +" ( "
          +" 	select "
          +" 	Z.ComCode A,Z.showname S, "
          +" 	(select RiskShortName from lmrisk where riskcode='"+tRiskCode+"' ) B, "
          +" 	"
          +" 	(select count(distinct prtno) from lcgrppol where riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE )) C1, "
          +" 	(select count(distinct prtno) from lbgrppol where riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE )) C2, "
          +" 	(select count(distinct prtno) from lbgrppol where riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE )) C3, "
          +" "
          +"  (select count(distinct b.APPNTNo) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom1+")=Z.COMCODE and a.riskcode='"+tRiskCode+"' ) D1, "
          +"  (select count(distinct b.APPNTNo) from lBgrppol a,lBgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom1+")=Z.COMCODE and a.riskcode='"+tRiskCode+"' ) D2,      "      	
          +"  	"
          +" 	(select count(distinct (insuredno)) from lcpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E1, "
          +" 	(select count(distinct (insuredno)) from lBpol where  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E2, "
          +"		(select count(distinct (insuredno)) from lBpol where  conttype='2' and grpcontno in (select grpcontno from lBgrpcont where edorno not like 'xb%' and  makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E3,   "         	          	
          +"    "
          +" 	(select sum(prem) from lcpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F1, "
          +" 	(select sum(prem) from lBpol where conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F2, "
          +" 	(select sum(prem) from lBpol where conttype='2' and grpcontno in (select grpcontno from lBgrpcont where edorno not like 'xb%' and  makedate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F3 "
          +"  "
          +" 	from LDCom Z where length(trim(Z.comcode))="+tManageCom1+" and sign='1' and substr(Z.comcode,1,"+tManageCom2+")='"+tManageCom+"' "
          +" ) as X    "
          +"union "
          +"          select A,S,B, "
          +" to_zero(C1)+to_zero(C2)+to_zero(C3), "
          +" to_zero(D1)+to_zero(D2), "
          +" to_zero(E1)+to_zero(E2)+to_zero(E3), "
          +" to_zero(F1)+to_zero(F2)+to_zero(F3),'' "
          +" "
          +" from  "
          +" (  " 
          +" 	select "
          +" 	'总计' A,'总计' S, "
          +" 	(select RiskShortName from lmrisk where riskcode='"+tRiskCode+"' ) B, "
          +" 	"
          +" 	(select count(distinct (prtno||riskcode)) from lcgrppol where riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcGRPcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE )) C1,"
          +" 	(select count(distinct (prtno||riskcode)) from lbgrppol where riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcGRPcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE )) C2, "
          +" 	(select count(distinct (prtno||riskcode)) from lbgrppol where riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lbGRPcont where edorno not like 'xb%' and makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE )) C3, "
          +"  "
          +"  (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.prtno=b.prtno and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom2+")=Z.COMCODE and a.riskcode='"+tRiskCode+"' ) D1,"
          +"  (select count(distinct (b.APPNTNo||a.riskcode)) from lBgrppol a,lBgrpcont b where a.edorno not like 'xb%' and a.prtno=b.prtno and b.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom2+")=Z.COMCODE and a.riskcode='"+tRiskCode+"' ) D2,    "        	
          +"  	"
          +" 	(select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E1, "
          +" 	(select count(distinct (insuredno||riskcode)) from lBpol where  conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E2, "
          +"	(select count(distinct (insuredno||riskcode)) from lBpol where  conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lBgrpcont where  edorno not like 'xb%' and makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E3,   "        	          	
          +" "
          +" 	(select sum(prem) from lcpol where conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F1,"
          +" 	(select sum(prem) from lBpol where conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F2,"
          +" 	(select sum(prem) from lBpol where conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lBgrpcont where edorno not like 'xb%' and  makedate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F3"
          +" "
          +" 	from LDCom Z where length(trim(Z.comcode))="+tManageCom2+" and sign='1' and substr(Z.comcode,1,"+tManageCom2+")='"+tManageCom+"' "
          +" ) as X order by A ";    
	}else if(type=="2"){
	    tSQL="select A,S,B,VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) from (  select Z.ComCode A,Z.showname S, 	(select RiskShortName from lmrisk where riskcode='"+tRiskCode+"' ) B, 	(select count(distinct a.grpcontno) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom1+")=Z.COMCODE  and a.passflag='a' and c.riskcode='"+tRiskCode+"' and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else b.salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else b.agentcom end) = "+tagentcom+") G, 	(select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom1+")=Z.COMCODE  and a.passflag='a' and c.riskcode='"+tRiskCode+"' and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else b.salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else b.agentcom end) = "+tagentcom+") H, 	(select count(distinct prtno) from lcgrppol where riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else agentcom end) = "+tagentcom+" )) L, 	(select count(distinct insuredno) from lcpol where  conttype='2' and  riskcode='"+tRiskCode+"'  and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE ) and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else agentcom end) = "+tagentcom+" ) M, 	(select sum(prem) from lcpol where  conttype='2' and  riskcode='"+tRiskCode+"'  and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE ) and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else agentcom end) = "+tagentcom+") N from LDCom Z where length(trim(Z.comcode))="+tManageCom1+" and sign='1' and substr(Z.comcode,1,"+tManageCom2+")='"+tManageCom+"'  ) as X  union select A,S,B, VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) from(  select '总计' A,'总计' S, 	(select RiskShortName from lmrisk where riskcode='"+tRiskCode+"' ) B, 	(select count(distinct (a.grpcontno||riskcode)) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom2+")=Z.COMCODE  and a.passflag='a' and c.riskcode='"+tRiskCode+"' and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else b.salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else b.agentcom end) = "+tagentcom+") G, 	(select sum(b.prem) from LCGCUWMaster a,lcgrpcont b,lcgrppol c where a.grpcontno=b.grpcontno and b.prtno=c.prtno and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom2+")=Z.COMCODE  and a.passflag='a' and c.riskcode='"+tRiskCode+"' and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else b.salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else b.agentcom end) = "+tagentcom+") H, 	(select count(distinct (prtno||riskcode)) from lcgrppol where riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE ) and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else agentcom end) = "+tagentcom+") L, 	(select count(distinct (insuredno||riskcode)) from lcpol where  conttype='2' and  riskcode='"+tRiskCode+"'  and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE ) and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else agentcom end) = "+tagentcom+") M, 	(select sum(prem) from lcpol where  conttype='2' and riskcode='"+tRiskCode+"'  and grpcontno in (select grpcontno from lcGRPcont where  uwflag in('9') and appflag<>'1' and UWdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE ) and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case "+tagentcom+" when '1' then "+tagentcom+" else agentcom end) = "+tagentcom+") N from LDCom Z where length(trim(Z.comcode))="+tManageCom2+" and sign='1' and substr(Z.comcode,1,"+tManageCom2+")='"+tManageCom+"'  ) as X order by A ";
	}else{
	    tSQL= " select A,S,B, "
          +" to_zero(C1)+to_zero(C2)+to_zero(C3), "
          +" to_zero(D1)+to_zero(D2), "
          +" to_zero(E1)+to_zero(E2)+to_zero(E3), "   
          +" to_zero(F1)+to_zero(F2)+to_zero(F3) ,'' "
          +" from  "
          +" (  "
          +" 	select "
          +" 	Z.ComCode A,Z.showname S,  "   
          +" 	(select RiskShortName from lmrisk where riskcode='"+tRiskCode+"' ) B,  "
          +" 	"
          +" 	(select count(distinct prtno) from lcgrppol where appflag='1' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE )) C1, "
          +" 	(select count(distinct prtno) from lbgrppol where appflag='1' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE )) C2, "
          +" 	(select count(distinct prtno) from lbgrppol where appflag='1' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE )) C3, "
          +"  "
          +"  (select count(distinct b.APPNTNo) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom1+")=Z.COMCODE and a.riskcode='"+tRiskCode+"' ) D1, "
          +"  (select count(distinct b.APPNTNo) from lBgrppol a,lBgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom1+")=Z.COMCODE and a.riskcode='"+tRiskCode+"' ) D2,   "         	
          +"  	 "
          +" 	(select count(distinct (insuredno)) from lcpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E1, "
          +" 	(select count(distinct (insuredno)) from lBpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E2, "
          +"		(select count(distinct (insuredno)) from lBpol where appflag='1' and  conttype='2' and grpcontno in (select grpcontno from lBgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E3,     "        	          	
          +"   "
          +" 	(select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F1, "
          +" 	(select sum(prem) from lBpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F2, "
          +" 	(select sum(prem) from lBpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lBgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom1+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F3 "
          +"    "
          +" 	from LDCom Z where length(trim(Z.comcode))="+tManageCom1+" and sign='1' and substr(Z.comcode,1,"+tManageCom2+")='"+tManageCom+"' "
          +" ) as X  "
          +" union  "
          +"          select A,S,B, "
          +" to_zero(C1)+to_zero(C2)+to_zero(C3), "
          +" to_zero(D1)+to_zero(D2), "
          +" to_zero(E1)+to_zero(E2)+to_zero(E3),   " 
          +" to_zero(F1)+to_zero(F2)+to_zero(F3),'' "
          +" "
          +" from       "   
          +" (  "
          +" 	select "
          +" 	'总计' A,'总计' S, "
          +" 	(select RiskShortName from lmrisk where riskcode='"+tRiskCode+"' ) B, "
          +" 	 "
          +" 	(select count(distinct (prtno||riskcode)) from lcgrppol where appflag='1' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcGRPcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE )) C1,   "
          +" 	(select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcGRPcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE )) C2,"
          +" 	(select count(distinct (prtno||riskcode)) from lbgrppol where appflag='1' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lbGRPcont where edorno not like 'xb%' and appflag='1' and signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE )) C3,"
          +"   "
          +"  (select count(distinct (b.APPNTNo||a.riskcode)) from lcgrppol a,lcgrpcont b where a.prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom2+")=Z.COMCODE and a.riskcode='"+tRiskCode+"' ) D1, "
          +"  (select count(distinct (b.APPNTNo||a.riskcode)) from lBgrppol a,lBgrpcont b where a.edorno not like 'xb%' and a.appflag='1' and b.appflag='1' and a.prtno=b.prtno and b.signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom2+")=Z.COMCODE and a.riskcode='"+tRiskCode+"' ) D2,     "       	 
          +"  	 "
          +" 	(select count(distinct (insuredno||riskcode)) from lcpol where appflag='1' and  conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E1, "
          +" 	(select count(distinct (insuredno||riskcode)) from lBpol where appflag='1' and  conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E2, "
          +"	(select count(distinct (insuredno||riskcode)) from lBpol where appflag='1' and  conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lBgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"' and PolTypeFlag <>'2') E3,  "          	          	
          +"     "
          +" 	(select sum(prem) from lcpol where appflag='1' and conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F1,  "
          +" 	(select sum(prem) from lBpol where appflag='1' and conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F2, "
          +" 	(select sum(prem) from lBpol where appflag='1' and conttype='2' and riskcode='"+tRiskCode+"' and grpcontno in (select grpcontno from lBgrpcont where edorno not like 'xb%' and appflag='1' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and substr(managecom,1,"+tManageCom2+")=Z.COMCODE) and riskcode ='"+tRiskCode+"') F3 "
          +"   "
          +" 	from LDCom Z where length(trim(Z.comcode))="+tManageCom2+" and sign='1' and substr(Z.comcode,1,"+tManageCom2+")='"+tManageCom+"' "
          +" ) as X order by A  ";
	}
	
	fm.querySql.value=tSQL;
	fm.action="tbGrpParticularStatisticSubDown.jsp";
	fm.submit();
	
}


//下拉框选择后执行
function afterCodeSelect(cName, Filed)
{	
  if(cName=='salechnlall')
  {
  	if(fm.saleChnl.value == '04')
  	{
  		fm.agentCom.value = '';
    	fm.all("tdAgentComName").style.display = '';
    	fm.all("tdAgentCom").style.display = '';
  	}
  	else
  	{
  		fm.agentCom.value = '';
    	fm.all("tdAgentComName").style.display = 'none';
    	fm.all("tdAgentCom").style.display = 'none';
  	}
	}
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    var tTmpUrl = "../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01";
    showInfo=window.open(tTmpUrl);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.agentCom.value = arrQueryResult[0][0];
    }
}