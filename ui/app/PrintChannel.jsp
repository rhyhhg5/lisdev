<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
    tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/Verifyinput.js"></SCRIPT>
<SCRIPT src="PrintChannel.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="PrintChannelInit.jsp"%>
<title>打印个人保单 </title>
</head>
<body onload="initForm();" >
	<form action="./PrintChannelSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 个人保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr><td class= titleImg align= center>请输入保单打印配置条件：</td></tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>公司编码</TD>
				<TD class= input><input class="codeNo" name=manageComQuery ondblclick="return showCodeList('comcode',[this,manageComNameQuery],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,manageComNameQuery],[0,1],null,null,null,1);"><input class=codename name=manageComNameQuery readonly=true > </TD>			
				<TD class= title>保单类型</TD>
				<TD class= input><input class="codeNo" name=contTypeQuery CodeData="0|^1|个人保单|^2|团体保单" ondblclick="return showCodeListEx('contType',[this,contTypeNameQuery],[0,1],null,null,null,1);" onkeyup="return showCodeListEx('contType',[this,contTypeNameQuery],[0,1],null,null,null,1);"><input class=codename name=contTypeNameQuery readonly=true > </TD>			
				<TD class= title>打印渠道</TD>
				<TD class= input><input class="codeNo" name=printChannelQuery ondblclick="return showCodeList('printchanneltype',[this,printChannelNameQuery],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('printchanneltype',[this,printChannelNameQuery],[0,1],null,null,null,1);"><input class=codename name=printChannelNameQuery readonly=true > </TD>			
			</TR>
		</table>
		<input value="查询配置" class="cssButton" type=button onclick="easyQueryClick();">
		<p></p>
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPrintChannel);">
				</td>
				<td class= titleImg>配置信息</td>
			</tr>
		</table>
		<div id= "divPrintChannel" style= "display: ''">
			<table class= common>
				<tr class= common>
					<td text-align: left colSpan=1>
						<span id="spanPrintChannelGrid" ></span>
					</td>
				</tr>
			</table>
			<input value="首 页" class="cssButton" type=button onclick="getFirstPage();">
			<input value="上一页" class="cssButton" type=button onclick="getPreviousPage();">
			<input value="下一页" class="cssButton" type=button onclick="getNextPage();">
			<input value="尾 页" class="cssButton" type=button onclick="getLastPage();">
		</div>
		<p></p>
		<table class= common border=0 width=100%>
			<tr><td class= titleImg align= center>保单打印渠道配置：</td></tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>公司编码</TD>
				<TD class= input><input class="codeNo" name=manageCom ondblclick="return showCodeList('comcode',[this,manageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,manageComName],[0,1],null,null,null,1);"><input class=codename name=manageComName > </TD>			
				<TD class= title>保单类型</TD>
				<TD class= input><input class="codeNo" name=contType CodeData="0|^1|个人保单|^2|团体保单" ondblclick="return showCodeListEx('contType',[this,contTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListEx('contType',[this,contTypeName],[0,1],null,null,null,1);"><input class=codename name=contTypeName readonly=true > </TD>			
				<TD class= title>打印渠道</TD>
				<TD class= input><input class="codeNo" name=printChannel ondblclick="return showCodeList('printchanneltype',[this,printChannelName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('printchanneltype',[this,printChannelName],[0,1],null,null,null,1);"><input class=codename name=printChannelName readonly=true > </TD>			
			</TR>
		</table>
		<input type=hidden name="operate">
		<input value="保  存" class="cssButton" type=button onclick="submitForm();">
		<input value="修  改" class="cssButton" type=button onclick="updateForm();">
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
