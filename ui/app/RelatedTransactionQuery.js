var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function AddInput() {
   window.location = "./RelatedTransactionInput.jsp?type=1";
}
function AddList(){
	window.location = "./RelatedTransactionInput.jsp?type=3";
}
function easyQueryClick() {

  // 书写SQL语句
	var strSQL = "";
	initRelatedPartyGrid();
	strSQL = "select RelateNo,Name,RgtMoney,ChairmanName,GeneralManagerName,Sex,Position,IDNo,OtherOrganization,RelatedType,(select codename from ldcode where codetype='relatedtype' and code=relatedtype),MakeDate from LDRelatedParty "
		   + "where 1=1 "
		   + getWherePart("RelateNo","RelateNo")
//		   + getWherePart("Name","Name","like")
		   + getWherePart("RelatedType","RelatedType")
		   + getWherePart('MakeDate', 'ImportDateEnd', '<=')
		   + getWherePart('MakeDate', 'ImportDateBegin', '>=');
//		   + " order by RelateNo";
	if(fm.Name.value==null || fm.Name.value==""){
		strSQL += "order by RelateNo ";
	}else{
		strSQL += " and Name like '%"+fm.Name.value+"%' order by RelateNo ";
	}
	turnPage.queryModal(strSQL, RelatedPartyGrid);
	if(RelatedPartyGrid.mulLineCount<1){
		alert("没有查询到相应数据！");
	}
}
function UpdateInput() {
	var checkFlag = 0;
	for(i=0; i<RelatedPartyGrid.mulLineCount; i++){
		if(RelatedPartyGrid.getSelNo(i)){
			checkFlag = RelatedPartyGrid.getSelNo();
			break;
		}
	}
	if(!checkFlag){
		alert("请选择一条关联方信息进行修改！");
		return false;
	}else{
		var tRelateNo = RelatedPartyGrid.getRowColData(checkFlag-1 , 1);
		var tName = RelatedPartyGrid.getRowColData(checkFlag-1 , 2);
		var tRelatedType = RelatedPartyGrid.getRowColData(checkFlag-1 , 10);
		window.location = "./RelatedTransactionInput.jsp?type=2&RelateNo="+tRelateNo+"&RelatedType="+tRelatedType;
	}
}
function DeleteInput(){
	var checkFlag = 0;
	for(i=0; i<RelatedPartyGrid.mulLineCount; i++){
		if(RelatedPartyGrid.getSelNo(i)){
			checkFlag = RelatedPartyGrid.getSelNo();
			break;
		}
	}
	if(!checkFlag){
		alert("请选择一条关联方信息进行删除！");
		return false;
	}else{
		var tRelateNo = RelatedPartyGrid.getRowColData(checkFlag-1 , 1);
        if(tRelateNo==null || tRelateNo==""){
        	alert("获取关联方编号失败！");
        	return false;
        }
		if (confirm("您确实想删除该记录吗?"))
	    {	
			fm.all("RelateNobak").value=tRelateNo;
			fm.all("transact").value="DELETE";
	        var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ showStr;
	        showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	        fm.submit(); // 提交
	    }
		
	}
}

function afterSubmit(FlagStr, Content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	easyQueryClick();
}
