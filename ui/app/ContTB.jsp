<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：
//程序功能：
//创建日期：2009-08-06
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="ContTB.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();">    
<form action="" method=post name=fm target="fraSubmit">

    <table class="common">
        <tr class="common">
            <td class="title">管理机构</td>
            <td class="input">
                <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
            </td>
            <td class="title">投保日期起</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="StartMakeDate" verify="投保日期|date&notnull" elementtype="nacessary" />
            </td>
            <td class="title">投保日期止</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="EndMakeDate" verify="投保日期|date&notnull" elementtype="nacessary" />
            </td>
        </tr>
        <tr class="common">
            <td class="title">销售渠道</td>
            <td class="input">
                <input class="codeNo" name="SaleChnl" verify="销售渠道" ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" readonly="readonly" />
            </td>
            <td class="title">&nbsp;</td>
            <td class="input">&nbsp;</td>
            <td class="title">&nbsp;</td>
            <td class="input">&nbsp;</td>
        </tr>               
    </table>
    
    <br />
    
    <input value="下  载" class="cssButton" type="button" onclick="submitForm();" />
    
    <hr />
    
    <div>
        <font color="red">
            <ul>报表说明：
                <li>本报表是对个人投保保费，件数进行统计。</li>
                <li>本报表不包括：犹豫期撤保/退保和投保撤单的情况。</li>
                <li>投保日期查询口径：这里指保单录入日期。</li>
            </ul>
            <ul>名词解释：
                <li>合同号码：没有保单签发之前以13开头的，都是临时合同号码。</li>
                <li>投保件数：一个合同算一件。相同合同号只统计一次。</li>
                <li>投保首年追加保费：主要针对万能险种。</li>
                <li>被保人数：有多少被保人。相同客户号只统计一次。</li>
                <li>投保险种件均保费：（投保保费 + 首年追加）/ 投保件数。</li>
            </ul>
            <ul>统计条件：
                <li>管理机构、投保日期段、销售渠道。</li>
                <li>投保日期段间隔不超过十二个月。</li>
            </ul>
        </font>
    </div>
    
    <input name="querySql" type="hidden" />
    <input type="hidden" name=op value="" />

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 