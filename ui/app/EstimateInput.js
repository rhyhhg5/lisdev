



var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function beforeSub(){
	if(fm.EstimateRate.value == null || fm.EstimateRate.value == "" || fm.EstimateRate.value == "null"){
		alert("预估赔付率不可以为空！");
		fm.EstimateRate.focus();
		return false;
	}
	if(!isNumeric(fm.EstimateRate.value)){
		alert("预估赔付率必须为数字！");
		fm.EstimateRate.focus();
		return false;
	}
	if(fm.EstimateRate.value.indexOf(".") == 0){
		alert("预估赔付率必须为数字！");
		fm.EstimateRate.focus();
		return false;
	}
	var tStrs = new Array();
	var tEstimateRate = parseFloat(fm.EstimateRate.value)+"";  
	tStrs = tEstimateRate.split(".");
	if(tStrs.length>1){
		if(tStrs[1].length>4){
			alert("预估赔付率最多保留四位小数！");
			fm.EstimateRate.focus();
			return false;
		}
	}
	var tSQL = "select BalanceTermFlag from LCGrpContSub where prtno = (select prtno from LCGrpCont where grpcontno = '"+tGrpContNo+"')";
	var strQueryResult = easyExecSql(tSQL);
	if(strQueryResult[0][0] == "Y"){
		if(fm.EstimateBalanceRate.value == null || fm.EstimateBalanceRate.value == "" || fm.EstimateBalanceRate.value == "null"){
			alert("保单关联项目时，选择了“存在结余返还”，预估赔付率（含结余返还/保费回补）不可以为空！");
			fm.EstimateBalanceRate.focus();
			return false;
		}
		//功能#3783需要
	/*	if(fm.ContEstimatePrem.value == null || fm.ContEstimatePrem.value == "" || fm.ContEstimatePrem.value == "null"){
			alert("保单关联项目时，选择了“存在结余返还”，预估结余返还金额不可以为空！");
			fm.ContEstimatePrem.focus();
			return false;
		}*/
	}
	if(fm.EstimateBalanceRate.value != "" && fm.EstimateBalanceRate.value != null && fm.EstimateBalanceRate.value != "null")
   	{
   		if(!isNumeric(fm.EstimateBalanceRate.value)){
			alert("预估赔付率（含结余返还/保费回补）必须为数字！");
			fm.EstimateBalanceRate.focus();
			return false;
		}
		if(fm.EstimateBalanceRate.value.indexOf(".") == 0){
			alert("预估赔付率（含结余返还/保费回补）必须为数字！");
			fm.EstimateBalanceRate.focus();
			return false;
		}
		tStrs = new Array();
		var tEstimateBalanceRate = parseFloat(fm.EstimateBalanceRate.value)+"";  
		tStrs = tEstimateBalanceRate.split(".");
		if(tStrs.length>1){
			if(tStrs[1].length>4){
				alert("预估赔付率（含结余返还/保费回补）最多保留四位小数！");
				fm.EstimateBalanceRate.focus();
				return false;
			}
		}
   	}
   	if(fm.ContEstimatePrem.value != "" && fm.ContEstimatePrem.value != null && fm.ContEstimatePrem.value != "null")
   	{
   		if(!isInteger(fm.ContEstimatePrem.value)){
			alert("预估结余返还金额必须为非负整数！");
			fm.ContEstimatePrem.focus();
			return false;
		}
   	}
   	if(fm.Reason.value == null || fm.Reason.value == "" || fm.Reason.value == "null"){
		alert("录入原因不可以为空！");
		fm.Reason.focus();
		return false;
	}
	return true;
}

function submitForm(){
	//预估结余返还金额和预计保费回补金额 的校验
	if (!test()) {
		return false;
	} 
	
	if(!beforeSub()){
		return false;
	}
	
	var tCurrentDate = getCurrentDate();
	var tSQL = "select 1 from LCEstimate where grpcontno = '"+tGrpContNo+"' and makedate = '"+tCurrentDate+"' ";
	var strQueryResult = easyExecSql(tSQL);
	if(strQueryResult){
		alert("今天已录入社保业务动态评估信息，请进行修改！");
		return false;
	}
	
	if(!checkBJ()){
		return false;
	}
	
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "INSERT||MAIN";
	fm.action="./EstimateInputSave.jsp";
	fm.submit(); //提交
	fm.action= "";
	return true;
}

function test(){
	var a = fm.ContEstimatePrem.value;//预估结余返还金额
	var b = fm.BackEstimatePrem.value;//预计保费回补金额 
	var number = /^[0-9]+[0-9]*]*$/;
	//“预计保费回补金额”与“预估结余返还金额”必须且只能录入其中之一,
	//若“预计保费回补金额”或“预估结余返还金额”录入值为0，则系统认为该项未录入，另外一项必录一个非0的有效值；
//	if ((a == null && b == null)||(a == "" && b == "")||(a == 0 && b == 0)) {
//		alert("“预计保费回补金额”与“预估结余返还金额”不能都为空！(录入0等同空)");
//		return false;
//	}
	if ((a != ""&& a !="0") && (b != ""&& b !="0")) {
		alert("“预计保费回补金额”与“预估结余返还金额”必须且只能录入其中之一!");
		return false;
	}
	if (a == "" || a == null|| a == "0") {
		if (b == "") {
			return true;
		}else{
			if(!number.test(b) || b < 0){
				alert("“预计保费回补金额”录入内容必须是非负整数数字，请核实！");
				return false;
			}
		}
	}
	if (b == "" || b == null || b =="0") {
		if (a == "") {
			return true;
		}else{
			if(!number.test(a)||a < 0){
				alert("“预估结余返还金额”录入内容必须是非负整数数字，请核实！");
				return false;
			}
		}
	}
	return true;
}

function updateForm(){
	//预估结余返还金额和预计保费回补金额 的校验
	if (!test()) {
		return false;
	} 
	
	var tCurrentDate = getCurrentDate();
	var checkFlag = 0;
	for (i = 0; i < EstimateGrid.mulLineCount; i++) {
		if (EstimateGrid.getSelNo(i)) {
			checkFlag = EstimateGrid.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		var aCurrentDate = EstimateGrid.getRowColData(checkFlag - 1, 7);
		if(compareDate(tCurrentDate,aCurrentDate) != 0){
			alert("仅能对今天的数据进行操作，请核查选择记录的录入日期！");
			return false;
		}
	}else{
		alert("请先选择需要修改的记录！");
		return false;
	}
	
	if(!beforeSub()){
		return false;
	}
	
	var tSQL = "select 1 from LCEstimate where grpcontno = '"+tGrpContNo+"' and makedate = '"+tCurrentDate+"' ";
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("今天还未录入社保业务动态评估信息，请先进行新增！");
		return false;
	}
	
	if(!checkBJ()){
		return false;
	}
	
	if (confirm("是否需要修改所选记录？")) {
		var i = 0;
		var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.fmtransact.value = "UPDATE||MAIN";
		fm.action="./EstimateInputSave.jsp";
		fm.submit(); //提交
		fm.action= "";
		return true;
	}
}

function deleteForm(){
	var tCurrentDate = getCurrentDate();
	var checkFlag = 0;
	for (i = 0; i < EstimateGrid.mulLineCount; i++) {
		if (EstimateGrid.getSelNo(i)) {
			checkFlag = EstimateGrid.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		var aCurrentDate = EstimateGrid.getRowColData(checkFlag - 1, 7);
		if(compareDate(tCurrentDate,aCurrentDate) != 0){
			alert("仅能对今天的数据进行操作，请核查选择记录的录入日期！");
			return false;
		}
	}else{
		alert("请先选择需要删除的记录！");
		return false;
	}
	
	var tSQL = "select 1 from LCEstimate where grpcontno = '"+tGrpContNo+"' and makedate = '"+tCurrentDate+"' ";
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("今天还未录入社保业务动态评估信息，不能删除！");
		return false;
	}
	
	if(!checkBJ()){
		return false;
	}
	
	if (confirm("是否需要删除所选记录？")) {
		var i = 0;
		var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.fmtransact.value = "DELETE||MAIN";
		fm.action="./EstimateInputSave.jsp";
		fm.submit(); //提交
		fm.action= "";
		return true;
	}
}

function afterSubmit(FlagStr,content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
	
}
function initGrpContInfo(){
	if(tGrpContNo == null || tGrpContNo == ""){
		alert("初始化保单数据时，获取保单数据失败！");
		return false;
	}
	var tSQL = "select lgc.grpcontno,lgcs.projectname,lgcs.projectno,lgc.grpname, lgc.cvalidate " 
	   		 + "from lcgrpcont lgc " 
		     + "inner join lcgrpcontsub lgcs on lgc.prtno = lgcs.prtno "
		     + "where lgc.appflag = '1' "
		     + "and lgcs.projectname is not null and lgcs.projectname != '' "
		     + "and lgc.grpcontno = '"+tGrpContNo+"' ";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("初始化保单数据失败！");
		return false;
	}
	fm.all('GrpContNo').value = strQueryResult[0][0];
	fm.all('ProjectName').value = strQueryResult[0][1];
	fm.all('ProjectNo').value = strQueryResult[0][2];
    fm.all('GrpName').value = strQueryResult[0][3];
    fm.all('CvaliDate').value = strQueryResult[0][4];
    var strSql = "select distinct EstimateRate,EstimateBalanceRate,ContEstimatePrem,BackEstimatePrem,Reason,Operator,MakeDate from LCEstimate where grpcontno = '"+tGrpContNo+"' order by makedate desc ";
	turnPage1.queryModal(strSql, EstimateGrid);
}

function cancelForm() {
	top.close();
}

function initButton(){
	if(tLookFlag != "0"){
		fm.all("insert").style.display = "none";
		fm.all("update").style.display = "none";
		fm.all("delete").style.display = "none";
		fm.all("divShowInfo").style.display = "none";
		fm.all("divEstimateInfo").style.display = "none";
	}
}
function getQueryResult() {
	var mSelNo = EstimateGrid.getSelNo();
	fm.EstimateRate.value = EstimateGrid.getRowColData(mSelNo - 1, 1);
	fm.EstimateBalanceRate.value = EstimateGrid.getRowColData(mSelNo - 1, 2);
	fm.ContEstimatePrem.value = EstimateGrid.getRowColData(mSelNo - 1, 3);
	fm.BackEstimatePrem.value = EstimateGrid.getRowColData(mSelNo - 1, 4);
	fm.Reason.value = EstimateGrid.getRowColData(mSelNo - 1, 5);
	fm.all('Operator').value= EstimateGrid.getRowColData(mSelNo - 1, 6);
}
function checkBJ(){
	var tSQL = "select 1 from lpgrpedoritem where edortype='BJ' and grpcontno = '"+tGrpContNo+"' ";
	var strQueryResult = easyExecSql(tSQL);
	if(strQueryResult){
		alert("该单已存在结余返还保全项目，不允许进行操作！");
		return false;
	}
	return true;
}