<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称 GrpCustomerQuerySave.jsp
//程序功能 
//创建日期：2002-06-19 11:10:36
//创建人  ：tianjingxia
//更新记录：  更新人    更新日期     更新原因/内容

 %>
<!-- 用户校验类 -->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
	response.setContentType("text/html;charset=GBK"); 
	request.setCharacterEncoding("GBK");
  //输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
  //接收信息
	String tProposalNo[] = request.getParameterValues("GrpCustomerListGridNo");      //标记
	String tCustomerNo[] = request.getParameterValues("GrpCustomerListGrid1");    //客户号
	String tGrpName[] = request.getParameterValues("GrpCustomerListGrid2");       //投保单位名称
	String tChk[] = request.getParameterValues("InpGrpCustomerListGridChk");
	String tRelationFlag[] = request.getParameterValues("GrpCustomerListGrid3");   //是否关联标志 
	String tRelaName[] = request.getParameterValues("GrpCustomerListGrid4");       //关联方名称
	
	boolean flag = false;
	int proposalCount = tProposalNo.length;
	System.out.println("总条数："+proposalCount);
	String tOperator = request.getParameter("fmtransact");
	
	try
	{
		for (int i = 0; i < proposalCount; i++)
		{
			LDGrpSet tLDGrpSet = new LDGrpSet();
			flag = false;
		
			if (tProposalNo[i] != null && tChk[i].equals("1"))         //判断是否选中
			{
				System.out.println("select ProposalNo:" +  tProposalNo[i]);
				LDGrpSchema tLDGrpSchema = new LDGrpSchema ();
				tLDGrpSchema.setCustomerNo(tCustomerNo[i]);
				if("submit".equals(tOperator)){
					tLDGrpSchema.setRelationFlag("1");
					tLDGrpSchema.setRelaName(tRelaName[i]);
				}else if("calcel".equals(tOperator)){
					tLDGrpSchema.setRelationFlag("0");
					tLDGrpSchema.setRelaName("");
				}
				
				tLDGrpSet.add(tLDGrpSchema);
				
				flag = true;
				
			}
			if(flag == true)
			{
			//准备传送数据 VDate
				VData tVData = new VData();
				tVData.add( tG );
				tVData.addElement( tLDGrpSet );
		    //数据传输
				GrpTbRalaUI tGrpTbRelaUI = new GrpTbRalaUI();
				if ( tGrpTbRelaUI.submitData( tVData, tOperator)== false )
				{
					System.out.println("Content>>>>>>0:" + Content );
					Content = "确认关联失败 " ;         //???
					FlagStr = "Fail";
				}else{
					FlagStr = "Succ";
				}
				int n = tGrpTbRelaUI.mErrors.getErrorCount();
				System.out.println("flag==="+FlagStr);
				
				if( n == 0 && FlagStr.equals("Succ"))
				{    
			              
			    	Content = "操作成功！";             //???
				   	FlagStr = "Succ";			   	
				   	
			    }
			    else
			    {
			       
					String strErr = "";
					for (int t = 0; t < n; t++)
					{
						strErr += (t+1) + ": " + tGrpTbRelaUI.mErrors.getError(t).errorMessage + "; ";
					}
					if ( FlagStr.equals("Succ"))
					{
						Content = "---2---失败，原因是: " + strErr;
						FlagStr = "Fail";
					}
					else
					{
					  Content +="可能有如下原因:"+ strErr;
					}
					
					
				} // end of if
				
			
			
			
			}    //end of if
		
		}        //end of for
	
	}catch(Exception e1)
	{
	e1.printStackTrace();
		Content = "---------失败，原因是: " + e1.toString();    //???
		FlagStr = "Fail";
	}
  
%>
<html>
	<script language="javascript">
    
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");


</script>

</html>
