<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LGWorkBoxInput.jsp
//程序功能：功能描述
//创建日期：2005-02-23 09:59:56
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="AppArchiveQuery.js"></SCRIPT> 
  <%@include file="AppArchiveQueryInit.jsp"%>
  <title>信箱信息查询</title>
  <script>
  	var ManageCom = "<%=tGI.ManageCom%>";  //上级机构编号
  </script>
</head>
<body onload="initForm();">
	<form action="larryQuerySave.jsp" name="fm" method="post">
	   <table class="common">    
  	<tr class="common">
  			<td class="title" >管理机构</td>
        <td class="input" ><input class="codeNo" name="ManageCom"  elementtype="nacessary" ondblclick="return showCodeList('station', [this,Manage], [0,1]);"><input class="codename" name="Manage" readonly="true"></td>
        <td  class="title" >投保人客户号</td>
  			<td  class="input" ><input type="text" name="applicant" class="common"></td>
  			<td  class="title">印刷号</td>
  			<td  class="input"><input type="text" name="printNo" class="common"></td>
  	</tr>
    <tr class="common">
        <td class="title">保单类型</td>
        <td class="input">
            <select name="type">
                <option value=""></option>
                <option value="TB01">个单</option>
                <option value="TB02">团单</option>
                <option value="TB04">银保</option>
            </select>
        </td>
  			<td  class="title">归档号区间：</td>
  			<td  class="input"><input type="text" name="startArchiveNo" class="common"></td>
  			<td  class="title"> 到 </td>
  			<td  class="input"><input type="text" name="endArchiveNo" class="common"></td>
    </tr>
    <tr class="common">
		<td class="title">扫描日期起</td>
		<td class="input"><Input class="coolDatePicker" name="scanDate" verify="扫描日期起|date" /></td>
		<td class="title">扫描日期止</td>
		<td class="input"><Input class="coolDatePicker" name="scanEndDate" verify="扫描日期止|date" /></td>
		
		<td class="title">渠道</td>
     <td class="input">
       <input class="codeNo" name="SaleChnlCode" ondblclick="return showCodeList('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" />
     </td>
     </tr>
     <tr class="common">
     <td class="title">网点</td>
     <td class="input">
       <input id="AgentComBank" class="code" name="AgentComBank" style="display:none;" ondblclick="return queryAgentComBank();" />
     </td>
  	</tr>
    <tr class="common">
    <td class="common"><INPUT VALUE="查  询" class= cssButton TYPE=button onclick="easyQuery()"></td>
    <td class="common"><INPUT VALUE="打印清单" class = cssbutton style="width:100px" TYPE=button onclick="printList();"></td>
    <td></td><td></td><td></td><td></td>
    <td class="common"><input value="业务触发" style="width:5px" TYPE="hidden" onclick="maintainArchiveNo();"></td>
    </tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLGWorkBox1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLarryBoxGrid1"></span> 
		    </td>
		</tr>
		</table>
	</div>
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();addMul()"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();addMul()"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();addMul()"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();addMul()">
  </Div>
    <!-- 用于保存需要打印的 SQL 语句 -->
    <input id="QuerySql" type="hidden" name="QuerySql" />
    <!-- 最小归档号 -->
    <input id="minArchiveNo" type="hidden" name="minArchiveNo" />
    <!-- 最大归档号 -->
    <input id="maxArchiveNo" type="hidden" name="maxArchiveNo" />
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
 
