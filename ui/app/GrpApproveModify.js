//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


/*********************************************************************
 *  集体投保单复核的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	//showInfo.close();
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	if( FlagStr == "Fail" )
	{             
		
		showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		// 刷新查询结果
		showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		easyQueryClick();		
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据进入投保单详细信息
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	var i = 0;
	var checkFlag = 0;
	var cGrpProposalNo = "";
	var cGrpPrtNo = "";
	var cflag = "2";
	var cMissionID = "";
	var cSubMissionID = "";
	  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
	    if (GrpPolGrid.getSelNo(i)) { 
	      checkFlag = GrpPolGrid.getSelNo();
	      break;
	    }
	  }
	 
	  if (checkFlag) { 
	  	cGrpProposalNo = GrpPolGrid.getRowColData(checkFlag - 1, 1);
	  	cGrpPrtNo = GrpPolGrid.getRowColData(checkFlag - 1, 2);
	  	cMissionID = GrpPolGrid.getRowColData(checkFlag - 1, 6);
	  	cSubMissionID = GrpPolGrid.getRowColData(checkFlag - 1, 7);
	  }
	  else {
	    alert("请先选择一条保单信息！"); 
	    return false;
	  }
	  if(trim(cGrpPrtNo)==''||trim(cGrpProposalNo)=='')
	  {
	    alert("请先选择一条有效保单信息！"); 
	    return false;
	  }
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	 window.open("./GroupPolApproveInfo.jsp?LoadFlag=13&polNo="+cGrpProposalNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID,"window1");
	
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = GrpPolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
var queryBug = 1;
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	if (contNo==null||contNo=='')
		contNo = "";
	//alert(contNo);
	// 书写SQL语句
	var strSQL = "";
	/*
	 strSQL = "select ProposalGrpContNo,PrtNo,SaleChnl,GrpName,CValiDate from LCGrpCont where 1=1"
	           +" and ApproveFlag=1"
	           + getWherePart( 'PrtNo' )
		   + getWherePart( 'ProposalGrpContNo','GrpProposalNo' )
		   + getWherePart( 'ManageCom' )
		   + getWherePart( 'AgentCode' )
		   + getWherePart( 'AgentGroup' )
		   + getWherePart( 'SaleChnl' );
	           + "order by makedate,maketime";
	           */
	  strSQL = "select lwmission.missionprop1,lwmission.missionprop2,lwmission.missionprop3,lwmission.missionprop7,lwmission.missionprop8,lwmission.missionid,lwmission.submissionid from lwmission where 1=1 "
				 + " and activityid = '0000002002' "
				 + " and processid = '0000000004'"				 
				 + getWherePart('missionprop1','GrpProposalNo')
				 + getWherePart('missionprop2','PrtNo')
				 + getWherePart('missionprop3','SaleChnl')
				 + getWherePart('missionprop5','AgentCode')
				 + getWherePart('missionprop4','ManageCom')
				 + getWherePart('missionprop6','AgentGroup')
				 + " and LWMission.MissionProp4 like '" + comcode + "%%'";  //集中权限管理体现					 
				 + " order by lwmission.missionprop2"
				 ;
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpPolGrid();
		//HZM 到此修改
		GrpPolGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpPolGrid.loadMulLine(GrpPolGrid.arraySave);		
		//HZM 到此修改
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			//GrpPolGrid.addOne();
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
			  GrpPolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//GrpPolGrid.delBlankLine();
	} // end of if
}

/*********************************************************************
 *  团体单的问题件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestQuery()
{
	  var i = 0;
	  var checkFlag = 0;
	  var cGrpProposalNo = "";
	  var cflag = "2";
	  
	  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
	    if (GrpPolGrid.getSelNo(i)) { 
	      checkFlag = GrpPolGrid.getSelNo();
	      break;
	    }
	  }
	 
	  if (checkFlag) { 
	  	cGrpProposalNo = GrpPolGrid.getRowColData(checkFlag - 1, 1);
	  }
	  else {
	    alert("请先选择一条主险保单信息！"); 
	    return false;
	  }
	  if(cGrpProposalNo==null||trim(cGrpProposalNo)=='')
	  {
	    alert("请先选择一条有效保单信息！"); 
	    return false;
	  }
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	 window.open("../uw/QuestQueryMain.jsp?ProposalNo1="+cGrpProposalNo+"&Flag="+cflag);
	
}


/*********************************************************************
 *  复核修改完成后确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ModifyMakeSure(){
	  var i = 0;
	  var checkFlag = 0;
	  var cGrpProposalNo = "";
	  var cGrpPrtNo="";
	  var cflag = "2";
	  
	  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
	    if (GrpPolGrid.getSelNo(i)) { 
	      checkFlag = GrpPolGrid.getSelNo();
	      break;
	    }
	  }
	 
	  if (checkFlag) { 
	  	cGrpProposalNo = GrpPolGrid.getRowColData(checkFlag - 1, 1);
	  	cGrpPrtNo = GrpPolGrid.getRowColData(checkFlag - 1, 2);
	  }
	  else {
	    alert("请先选择一条保单信息！"); 
	    return false;
	  }
	  if(trim(cGrpPrtNo)==''||trim(cGrpProposalNo)=='')
	  {
	    alert("请先选择一条有效保单信息！"); 
	    return false;
	  }
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	 window.open("./GrpApproveModifyMakeSure.jsp?GrpProposalNo="+trim(cGrpProposalNo)+"&GrpPrtNo="+trim(cGrpPrtNo),"window1");
	
	}