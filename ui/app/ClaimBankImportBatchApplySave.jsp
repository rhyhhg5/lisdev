<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2012-03-05
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
System.out.println("ClaimBankImportBatchApplySave.jsp start ...");

CErrors tError = null;
String FlagStr = "Fail";
String Content = "";

String tActivityID = request.getParameter("ActivityID");

GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;

String tBatchNo = null;

try
{
    int length = 8;
    if (strManageCom.length() < length)
    {
        strManageCom = PubFun.RCh(strManageCom, "0", length);
    }
    
    try
    {
        tBatchNo = PubFun1.CreateMaxNo("CBBATCHNO", strManageCom, null);
        System.out.println("BatchNo:" + tBatchNo);
    }
    catch(Exception e)
    {
        tBatchNo = null;
        e.printStackTrace();
    }
    
    if(tBatchNo != null && !tBatchNo.equals(""))
    {
        VData tVData = new VData();
        
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", tBatchNo);
        tTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
        tTransferData.setNameAndValue("ApplyDate", PubFun.getCurrentDate());
        tTransferData.setNameAndValue("Operator", tGI.Operator);
        
        tVData.add(tTransferData);
        tVData.add(tGI);
        
        GrpTbWorkFlowUI tGrpTbWorkFlowUI = new GrpTbWorkFlowUI();
        if(!tGrpTbWorkFlowUI.submitData(tVData, tActivityID))
        {
            Content = " 申请失败，原因是: " + tGrpTbWorkFlowUI.mErrors.getFirstError();
            FlagStr = "Fail";
        }
        else
        {
            Content = " 申请成功！";
            FlagStr = "Succ";
        }
    }
    else
    {
        Content = " 申请流水号失败！";
        FlagStr = "Fail";
    }
}
catch (Exception e)
{
    Content = " 申请失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("ClaimBankImportBatchApplySave.jsp end ...");

%>

<html>
<script language="javascript">
    parent.fraInterface.afterApplyImportBatchSubmit("<%=FlagStr%>", "<%=Content%>", "<%=tBatchNo%>");
</script>
</html>
