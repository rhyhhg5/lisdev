var arrDataSet
var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();
var PolNo;

window.onfocus = myonfocus;

// 查询按钮
function easyQueryClick() {
	
	initPolGrid();
//	if (!verifyInput2())// 检验之后将光标置于错误出，并且颜色变黄
//		return false;
//	}
	// 书写SQL语句
	var strSQL = "";
	if(fm.Riskcode.value!=null&&fm.Riskcode.value!=""){
	strSQL = " select lmr.riskcode,lmr.riskname,lmr.maxinsuredage,lmr.mininsuredage,lmr.startdate,lmr.enddate "
			+ " from lmriskapp lmr where lmr.riskcode="+fm.Riskcode.value;	
	
	turnPage.pageLineNum = 50;
	turnPage.strQueryResult = easyQueryVer3(strSQL);

	if (!turnPage.strQueryResult) {
		alert("没有查询到要处理的业务数据！");
		return false;
	}

	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	// 设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = PolGrid;
	// 保存SQL语句
	turnPage.strQuerySql = strSQL;
	// 设置查询起始位置
	turnPage.pageIndex = 0;
	// 在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	fm.querySql.value = strSQL;
	}
	else{
		alert("请输入需要修改的险种！");
	}
}

// 初始化查询数据
function Polinit() {
	var i = 0;
	var checkFlag = 0;

	for (i = 0; i < PolGrid.mulLineCount; i++) {
		if (PolGrid.getSelNo(i)) {
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		var riskcode = PolGrid.getRowColData(checkFlag - 1, 1);
		fm.all('riskcode1').value = riskcode;
		var abeforeMaxage = PolGrid.getRowColData(checkFlag - 1, 3);
		fm.all('beforeMaxage').value = abeforeMaxage;
		fm.all('afterMaxage').value = abeforeMaxage;
		var abeforeMinage = PolGrid.getRowColData(checkFlag - 1, 4);
		fm.all('beforeMinage').value = abeforeMinage;
		fm.all('afterMinage').value = abeforeMinage;
		var abeforeStartdate = PolGrid.getRowColData(checkFlag - 1, 5);
		fm.all('beforeStartdate').value = abeforeStartdate;
		fm.all('afterStartdate').value = abeforeStartdate;
		var abeforeEnddate = PolGrid.getRowColData(checkFlag - 1, 6);
		fm.all('beforeEnddate').value = abeforeEnddate;
		fm.all('afterEnddate').value = abeforeEnddate;
	} else {
		alert("请先选择一条险种信息！");
	}
}

function modifyage() {
	var mSelNo = PolGrid.getSelNo();// 判断该行的 Radio 单选框被选中,行号是从1开始,如果没有选中行,返回值是0
	if (mSelNo == 0) {
		alert("请选择一条数据！");
		return false;
	}
	
	var qafterMaxage = fm.afterMaxage.value;
	var qafterMinage = fm.afterMinage.value;
	var qafterStartdate = fm.afterStartdate.value;
	var qafterEnddate = fm.afterEnddate.value;
	if ((qafterMaxage == null || qafterMaxage=="")&&(qafterMinage == null || qafterMinage=="")) {
		if ((qafterStartdate == null || qafterStartdate=="")&&(qafterEnddate == null || qafterEnddate=="")){
			if(!confirm("修改后最大、最小年龄和生效、终止日期将为置为空，确定继续操作吗？")){
				return false;
		}
		}
	}
		
	
	fm.fmtransact.value = "UPDATE";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}
