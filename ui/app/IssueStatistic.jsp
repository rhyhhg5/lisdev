<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：IssueStatistic
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="IssueStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form  action="./IssueStatisticSub.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
					<TD  class= title> 管理机构</TD>
					<TD  class= input><Input class= "codeno"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
        <TR  class= common>
					<TD  class= title> 问题件类型</TD>
					<TD  class= input><Input class= "codeno"  name=IssuePolType  ondblclick="return showCodeList('issuepoltype',[this,issuepolName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('issuepoltype',[this,issuepolName],[0,1],null,null,null,1);" ><Input class=codename  name=issuepolName></TD>
                		
    </TR>
    </table>

    <input type="hidden" name=op value="">
    <input type="hidden" name=querySql value="">
		<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="downLoad()">
		
			<hr>
	
	    <div>
        <font color="red">
            <ul>查询字段说明：
                <li>统计起止期：保单签单日期的起始时间</li>  
            </ul>
            <ul>统计结果说明：
                <li>类型：页面选择的扫描类型</li>
                <li>件数：选择问题件类型的件数</li>
                <li>比例：问题件件数占统计期内保单的比例</li>
                <li>回复时效：问题件下发至问题件核销的平均时效</li>
            </ul>
        </font>
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
