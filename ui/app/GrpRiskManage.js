
var turnPage = new turnPageClass();
var showInfo;

//保存
function save() {

	if(fm.RiskGrp.value == null || fm.RiskGrp.value == ""){
		alert("险种编码不能为空！");
		return false;
	} else {
		var riskSql = "select 1 from lmriskapp where riskcode='" + fm.RiskGrp.value + "'";
		var arrResult = easyExecSql(riskSql);
		if(!arrResult){
			alert("险种编码不存在，请确认填写的险种编码是否正确！");
			return false;
		}
		
		riskSql = "select 1 from ldriskmanage where riskcode='" + fm.RiskGrp.value + "' and checktype = 'lgrprisk'";
		arrResult = easyExecSql(riskSql);
		if(arrResult){
			alert("该险种编码已存在！");
			return false;
		}
	}
	
	if(fm.Statesave.value != "0" && fm.Statesave.value != "1" && fm.Statesave.value != "-1"){
		alert("是否启用只能为0-启用或1-停用，请核对！");
		return false;
	}
	
	if(fm.Startdatesave.value == null || fm.Startdatesave.value == ""){
		alert("起始日期不能为空！");
		return false;
	}
	
	fm.fmtransact.value = "INSERT||MAIN";
	return submitForm();
}
//删除
function deleteClick() {

	if(fm.Riskcode.value == null || fm.Riskcode.value == ""){
		alert("请先进行查询，选择相关查询信息后进行删除！");
		return false;
	}
	
	fm.fmtransact.value = "DELETE||MAIN";
	return submitForm();
}
//修改
function updateClick() {

	if(fm.Riskcode.value == null || fm.Riskcode.value == ""){
		alert("请先进行查询，选择相关查询信息后进行修改！");
		return false;
	}
	
	if(fm.State.value != "0" && fm.State.value != "1" && fm.State.value != "-1"){
		alert("是否启用只能为0-启用或1-停用，请核对！");
		return false;
	}
	
	if(fm.Startdate.value == null || fm.Startdate.value == ""){
		alert("起始日期不能为空！");
		return false;
	}
	fm.fmtransact.value = "UPDATE||MAIN";
	return submitForm();
}

/* 提交表单 */
function submitForm() {
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content) {
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		window.focus();
	}
	easyQueryClick();
}

//查询
function easyQueryClick() {
	var strSql = "select Riskcode,(select riskname from lmriskapp where riskcode=ldriskmanage.riskcode),State,Startdate,Enddate,(case State when '0' then '\u542f\u7528' when '1' then '\u505c\u7528' end) Statename" + " from LDRISKMANAGE where checktype = 'lgrprisk' " + getWherePart("LDRISKMANAGE.Riskcode", "RiskcodeQuery", "=");

	turnPage.queryModal(strSql, LGGroupMemberGrid);
	showCodeName();
	
	fm.all("Riskcode").value = "";
	fm.all("RiskName").value = "";
	fm.all("Startdate").value = "";
	fm.all("Enddate").value = "";
	fm.all("State").value = "";
	fm.all("StateName").value = "";
}
function ShowDetail() {
	var tSel = LGGroupMemberGrid.getSelNo();
	fm.all("Riskcode").value = LGGroupMemberGrid.getRowColData(tSel - 1, 1);
	fm.all("RiskName").value = LGGroupMemberGrid.getRowColData(tSel - 1, 2);
	fm.all("Startdate").value = LGGroupMemberGrid.getRowColData(tSel - 1, 4);
	fm.all("Enddate").value = LGGroupMemberGrid.getRowColData(tSel - 1, 5);
	fm.all("State").value = LGGroupMemberGrid.getRowColData(tSel - 1, 3);
	fm.all("StateName").value = LGGroupMemberGrid.getRowColData(tSel - 1, 6);
}

