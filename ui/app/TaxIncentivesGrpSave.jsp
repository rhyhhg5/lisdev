<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<% 
	//输出参数
	CErrors tError = null;
  	String FlagStr = "Fail";
  	String Content = "";
  	GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");
  	String transact = request.getParameter("transact");
  	
  	LSGrpSchema tLSGrpSchema = new LSGrpSchema();
  	if("UPDATE".equals(transact)){
  		tLSGrpSchema.setGrpNo(request.getParameter("GrpNo"));
  	  	tLSGrpSchema.setGrpName(request.getParameter("GrpName"));
  	  	tLSGrpSchema.setTaxRegistration(request.getParameter("TaxRegistration"));
  	  	tLSGrpSchema.setGrpAddress(request.getParameter("GrpAddress"));
  	  	tLSGrpSchema.setGrpZipcode(request.getParameter("GrpZipCode"));
  	  	tLSGrpSchema.setGrpNature(request.getParameter("GrpNature"));
  	  	tLSGrpSchema.setPeoples(request.getParameter("Peoples"));
  	  	tLSGrpSchema.setOrgancomCode(request.getParameter("OrgancomCode"));
  	  	tLSGrpSchema.setBusinessType(request.getParameter("BusinessType"));
  	  	tLSGrpSchema.setLeaglePerson(request.getParameter("LegalPersonName"));
  	  	tLSGrpSchema.setPhone(request.getParameter("Phone"));
  	  	tLSGrpSchema.setHandlerName(request.getParameter("HandlerName"));
  	  	tLSGrpSchema.setDepartment(request.getParameter("Department"));
  	  	tLSGrpSchema.setHandlerPhone(request.getParameter("HandlerPhone"));
  	  	tLSGrpSchema.setBankCode(request.getParameter("BankCode"));
  	  	tLSGrpSchema.setBankAccName(request.getParameter("BankAccName"));
  	  	tLSGrpSchema.setBankAccNo(request.getParameter("BankAccNo"));
  	  	tLSGrpSchema.setOperator(tG.Operator);
  	    tLSGrpSchema.setMakeDate(new ExeSQL().getOneValue("select makedate from lsgrp where grpno='"+request.getParameter("GrpNo")+"'"));
  	    tLSGrpSchema.setMakeTime(new ExeSQL().getOneValue("select maketime from lsgrp where grpno='"+request.getParameter("GrpNo")+"'"));
  	    tLSGrpSchema.setModifyDate(PubFun.getCurrentDate());
  	  	tLSGrpSchema.setModifyTime(PubFun.getCurrentTime());
  	}else if("INSERT".equals(transact)){
  		tLSGrpSchema.setGrpNo("S" + PubFun1.CreateMaxNo("L",8));
  	  	tLSGrpSchema.setGrpName(request.getParameter("GrpName"));
  	  	tLSGrpSchema.setTaxRegistration(request.getParameter("TaxRegistration"));
  	  	tLSGrpSchema.setGrpAddress(request.getParameter("GrpAddress"));
  	  	tLSGrpSchema.setGrpZipcode(request.getParameter("GrpZipCode"));
  	  	tLSGrpSchema.setGrpNature(request.getParameter("GrpNature"));
  	  	tLSGrpSchema.setPeoples(request.getParameter("Peoples"));
  	  	tLSGrpSchema.setOrgancomCode(request.getParameter("OrgancomCode"));
  	  	tLSGrpSchema.setBusinessType(request.getParameter("BusinessType"));
  	  	tLSGrpSchema.setLeaglePerson(request.getParameter("LegalPersonName"));
  	  	tLSGrpSchema.setPhone(request.getParameter("Phone"));
  	  	tLSGrpSchema.setHandlerName(request.getParameter("HandlerName"));
  	  	tLSGrpSchema.setDepartment(request.getParameter("Department"));
  	  	tLSGrpSchema.setHandlerPhone(request.getParameter("HandlerPhone"));
  	  	tLSGrpSchema.setBankCode(request.getParameter("BankCode"));
  	  	tLSGrpSchema.setBankAccName(request.getParameter("BankAccName"));
  	  	tLSGrpSchema.setBankAccNo(request.getParameter("BankAccNo"));
  	  	tLSGrpSchema.setOperator(tG.Operator);
  	  	tLSGrpSchema.setMakeDate(PubFun.getCurrentDate());
  		tLSGrpSchema.setMakeTime(PubFun.getCurrentTime());
  		tLSGrpSchema.setModifyDate(PubFun.getCurrentDate());
  	  	tLSGrpSchema.setModifyTime(PubFun.getCurrentTime());
  	}
  	
  	try{
  		VData tVData = new VData();
  		tVData.add(tG);
  		tVData.add(tLSGrpSchema);
  		TaxIncentivesGrpUI tTaxIncentivesGrpUI = new TaxIncentivesGrpUI();
  		if(!tTaxIncentivesGrpUI.submitData(tVData,transact)){
  			Content = "处理失败，原因是:" + tTaxIncentivesGrpUI.mErrors.getFirstError();
            FlagStr = "Fail";
  		}else{
  			Content = "处理成功";
            FlagStr = "Succ";
  		}
  	}catch(Exception ex){
  		ex.printStackTrace();
  	}
%>
  
<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>