<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：
//程序功能：
//创建日期：2009-01-19
//创建人  ：GUZG
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.tb.*"%> 
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>


<%
String FlagStr = "Fail";
String Content = "";

String tContNo = request.getParameter("ContNo");
String tRelationToAppnt = request.getParameter("RelationToAppntNew");
GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ContNo", tContNo);
    tTransferData.setNameAndValue("RelationToAppnt", tRelationToAppnt);
    
    tVData.add(tTransferData);
    tVData.add(tGI);
    
    ModifyRelationToAppntUI tModifyRelationToAppntUI = new ModifyRelationToAppntUI();
    if(!tModifyRelationToAppntUI.submitData(tVData, null))
    {
        Content = "修改与投保人关系错误，原因是: " + tModifyRelationToAppntUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 修改与投保人关系成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 修改与投保人关系失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

%>

<html>
<script language="javascript">
    parent.fraInterface.afterImportSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
