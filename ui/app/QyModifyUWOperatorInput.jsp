<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; 	//记录管理机构
    var comcode = "<%=tGI.ComCode%>";		//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="QyModifyUWOperatorInput.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="QyModifyUWOperatorInit.jsp" %>
<title>维护保单核保师</title>
</head>
<body onload="initForm();">
	<form action="./QyModifyUWOperatorSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 保单信息部分 -->
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入保单查询条件：</td>
			</tr>
		</table>
		<table class="common" align=center>
			<tr class="common">
				<td class="title">管理机构</td>
				<td class="input"><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true ></td>
				<td class="title">印刷号</td>
				<td class="input"><input class="common" name="PrtNo" /></td>
				<td class="title">保单合同号</td>
				<td class="input"><input class="common" name="ContNo" /></td>
			</tr>
			<tr class="common">
				<td class="title">保单类型</td>
				<td class="input"><Input class=codeno name=ContType value="1" CodeData="0|^1|个单^2|团单" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" ><input class=codename name=ContTypeName value="个单" readonly=true ></td>
				<td></td><td></td><td></td><td></td>
			</tr>
		</table>
		<input class="cssButton" type="button" value="查询保单" onclick="easyQueryClick();" />
		<INPUT  type="hidden" class=Common name=querySql >
		
		<table>
			<tr>
				<td class=common>
					<IMG src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divLCPol1);">
				</td>
				<td class=titleImg>保单信息</td>
			</tr>
		</table>
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" ></span>
					</td>
				</tr>
			</table>

			<table align=center>
			<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
		 </table>
		</div>
		
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmOperator" name="fmOperator">
		<input type=hidden id="mContType" name="mContType">
		<input type=hidden id="mPrtNo" name="mPrtNo">
		<input type=hidden id="mAction" name="mAction">
		
		<table>
			<tr>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>合同号码</TD>
				<TD class= input><Input class= readonly readOnly=true name=mContNo></TD>
				<TD class= title>现保单核保师</TD>
				<TD class= input><Input class= readonly readOnly=true name=mNowUWOperator></TD>
			</TR>
			<TR class= common>
				<TD class= title>要修改的核保师</TD>
				<TD class= input>
					<Input class= 'codeno' name=mModifyUWOperator  verify="要修改的核保师|len<=10&code:uwusercode" ondblclick="return showCodeList('uwusercode',[this,UserCodeName],[0,1]);" onkeyup="return showCodeListKey('uwusercode', [this,UserCodeName],[0,1]);"><input  class="codename" name="UserCodeName" > 
				</TD>
				<TD class= title>操作人员</TD>
				<TD class= input><Input class= readonly readOnly=true name=mOperator></TD>
			</TR>
		</table>
		<INPUT VALUE="修改核保师" name=modifybtn class="cssButton" TYPE=button onclick="modifyUWOperator();">
		<INPUT VALUE="放回核保池" name=returnbtn class="cssButton" TYPE=button onclick="returnUWPol();">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>