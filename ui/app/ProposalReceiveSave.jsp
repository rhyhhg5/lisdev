<%
//程序名称：ReProposalPrintInput.jsp
//程序功能：
//创建日期：2002-11-25
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%!
	String handleFunction(HttpSession session, HttpServletRequest request) {
	int nIndex = 0;
	String tReceiveID = request.getParameter("mReceiveID");
	String tContNo = request.getParameter("mContNo");
	String tPrtNo = request.getParameter("mPrtNo");
	String tContType = request.getParameter("mContType");
	String tBackReasonCode=request.getParameter("BackReasonCode");
	String tBackReason=request.getParameter("BackReason");
	String strOperation = request.getParameter("fmtransact");
	

	GlobalInput globalInput = new GlobalInput();

	if( (GlobalInput)session.getValue("GI") == null )
	{
		return "网页超时或者是没有操作员信息，请重新登录";
	}
	else
	{
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
	}

	if( tReceiveID == null ) {
		return "没有输入需要的打印参数";
	}
  LCContReceiveSchema tLCContReceiveSchema=new LCContReceiveSchema();
  LCContReceiveUI tLCContReceiveUI =new LCContReceiveUI();
  tLCContReceiveSchema.setReceiveID(tReceiveID);
  tLCContReceiveSchema.setContNo(tContNo);
  tLCContReceiveSchema.setPrtNo(tPrtNo);
  tLCContReceiveSchema.setContType(tContType);
  tLCContReceiveSchema.setBackReasonCode(tBackReasonCode);
  tLCContReceiveSchema.setBackReason(tBackReason);

	VData vData = new VData();

	vData.addElement(tLCContReceiveSchema);
	vData.add(globalInput);

	try {
		if( !tLCContReceiveUI.submitData(vData, strOperation) )
		{
	   		if ( tLCContReceiveUI.mErrors.needDealError() )
	   		{
	   			return tLCContReceiveUI.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		return "保存失败，但是没有详细的原因";
			}
		}

	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		return ex.getMessage();
	}
	return "";
}
%>
<%
String FlagStr = "";
String Content = "";

try {
	Content = handleFunction(session, request);

	if( Content.equals("") ) {
		String strTemplatePath = application.getRealPath("xerox/printdata") + "/";
		FlagStr = "Succ";
		Content = "保存成功";
	} else {
		FlagStr = "Fail";
	}
} catch (Exception ex) {
	ex.printStackTrace();
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

