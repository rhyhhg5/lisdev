var turnPage = new turnPageClass();

//查询批次导入的被保人信息
function queryBatchList(){
	var tBatchNo = fm.BatchNo.value;
	var tSQL = "select batchno,name,sex,birthday,idtype,idno "
			 + "from lsinsuredlist "
			 + "where batchno='"+tBatchNo+"' "
			 + "order by insuredid";
	turnPage.queryModal(tSQL,ImportBatchGrid);
	if(!turnPage.strQueryResult){
		alert("该批次尚未导入被保人信息！");
		return false;
	}
	queryBatchInfo();
}

//查询已导入人数
function queryBatchInfo(){
	var tBatchNo = fm.BatchNo.value;
	var tSQL = "select count(1) from lsinsuredlist where batchno='"+tBatchNo+"'";
	var tArr = easyExecSql(tSQL);
	if(tArr){
		fm.ImportPeoples.value = tArr[0][0];
	}else{
		fm.ImportPeoples.value = "0";
	}
}

//依据录入条件查找导入的被保人信息
function queryInsured(){
	initImportBatchGrid();
	var tBatchNo = fm.BatchNo.value;
	var tSQL = "select batchno,name,sex,birthday,idtype,idno "
		 	 + "from lsinsuredlist "
		 	 + "where batchno='"+tBatchNo+"' "
		 	 + getWherePart("Name","Name")
		 	 + getWherePart("IdType","IDType")
		 	 + getWherePart("IdNo","IDNo")
		 	 + "order by insuredid";
	turnPage.queryModal(tSQL,ImportBatchGrid);
	if(ImportBatchGrid.mulLineCount<1){
		alert("批次:"+tBatchNo+"下没有该被保人信息！");
		return false;
	}
}

//关闭本页面
function goBack(){
	top.close();
}