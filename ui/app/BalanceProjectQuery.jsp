<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="BalanceProjectQuery.js"></script>
		<%@include file="BalanceProjectQueryInit.jsp"%>

		<script>
		var ManageCom = "<%=tGI.ManageCom%>";
		var tOperator = "<%=tGI.Operator%>"; 
		</script>
	</head>
	
	<body onload="initForm();">
		<form action="" method=post name=fm target="fraSubmit">
			<div id="divQueryInput" , style="display:hidden">
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divProject1);">
						</td>
						<td class=titleImg>
							查询条件
						</td>
					</tr>
				</table>

				<table class=common align='center'>
					<TR class=common>
						<TD class=title>
							管理机构
						</TD>
						<TD class=input>
							<Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
						</TD>
						<TD class=title>
							项目编号
						</TD>
						<TD class=input>
							<Input class='common' name=ProjectNo verify="项目编号|notnull&len<=100">
						</TD>
						<TD class=title>
							项目名称
						</TD>
						<TD class=input>
							<Input class='common' name=ProjectName verify="项目名称|notnull&len<=100">
						</TD>
					</TR>
				</table>
				<table>
					<td class=button width="10%" align=left>
						<input type="button" class=cssButton value="查  询" id=QUERY onclick="queryClick()">
						<input type="button" class=cssButton value="返  回" id=fanhui onclick="returnClick()">
					</td>
				</table>
				<span id="spanCode"
					style="display: none; position:absolute; slategray"></span>

				<!-- 查询结果部分 -->
				<table>
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;" 	OnClick="showPage(this,divProject1);">
						</td>
						<td class=titleImg>
							项目信息
						</td>
					</tr>
				</table>

			</div>
			<!-- 信息（列表） -->
			<Div id="divProject1" style="display:''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanProjectGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">

				<table>
					<tr>
						<td class=button>
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</Div>
		</form>
	</body>
</html>
