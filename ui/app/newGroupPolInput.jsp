 <%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-15 11:48:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<script>
	var prtNo = "<%=request.getParameter("prtNo")%>";
	
	var type = "<%=request.getParameter("type")%>";
	if (prtNo == "null") prtNo = "";
	
	//保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
	var BQFlag = "<%=request.getParameter("BQFlag")%>";
	if (BQFlag == "null") BQFlag = "0";
	
	//保全调用会传险种过来
	var BQRiskCode = "<%=request.getParameter("riskCode")%>";
</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  
  <%@include file="ContPolInit.jsp"%>
  <SCRIPT src="newContPolInput.js"></SCRIPT>
  <SCRIPT src="ProposalAutoMove.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
<% if (request.getParameter("type") == null) { %>
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<% } %>
   
</head>
<body  onload="initForm();" >
  <form action="./GroupPolSave.jsp" method=post name=fm target="fraSubmit">
   <Div  id= "divButton" style= "display: ''">
 	<INPUT class=cssButton VALUE="查 询"  TYPE=button onclick="queryClick()"> 
 	<INPUT class=cssButton VALUE="修 改"  TYPE=button onclick="updateClick()"> 
 	<INPUT class=cssButton VALUE="删 除"  TYPE=button onclick="deleteClick()"> 
 	<INPUT class=cssButton VALUE="保 存"  TYPE=button onclick=""> 
  </DIV>
    <!-- 合同信息部分 GroupPolSave.jsp-->
     
    <Div  id= "divGroupPol1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
	 
      <!--    <TD  class= title8>
            集体投保单号码
          </TD>-->
            <Input class="readonly" readonly name=GrpProposalNo tpye="hidden">
          <TD  class= title8>
            *印刷号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=PrtNo verify="印刷号码|notnull" >
          </TD>
        </TR>
        <TR  class= common8>
          <TD  class= title8>
            *管理机构
          </TD>
          <TD  class= input8>
            <!--<Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">-->
            <Input class="code8" name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
          </TD>
          <TD  class= title8>
            *销售渠道
          </TD> 
          <TD  class= input8>
            <!--<Input class="readonly" readonly name=SaleChnl verify="销售渠道|code:SaleChnl&notnull" >-->
          	<Input class="code8"  name=SaleChnl verify="销售渠道|code:SaleChnl&notnull" ondblclick="return showCodeList('SaleChnl',[this]);" onkeyup="return showCodeListKey('SaleChnl',[this]);">
          </TD>
          <TD  class= title8>
            代理机构
          </TD>
          <TD  class= input8>
            <Input class="code8" name=AgentCom ondblclick="return showCodeList('AgentCom',[this],null,null, ManageCom, 'ManageCom');" onkeyup="return showCodeListKey('AgentCom',[this],null,null, ManageCom, 'ManageCom');">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title8>
            代理人编码
          </TD>
          <TD  class= input8>
      <Input NAME=AgentCode VALUE="" MAXLENGTH=10 CLASS=code8 ondblclick="return queryAgent();"onkeyup="return queryAgent2();" >
         </TD>
          <TD  class= title8>
            代理人组别
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=AgentGroup verify="代理人组别|notnull&len<=12" >
          </TD>          
          <TD  class= title8>
            联合代理人代码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=AgentCode1 >
          </TD>
        </TR>
      </table>
    </Div>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol2);">
    		</td>
    		<td class= titleImg>
    			 投保单位资料（客户号 <Input class= common8 name=GrpNo > <INPUT id="butGrpNoQuery" class=cssButton VALUE="查询" TYPE=button onclick="showAppnt();"> ）
    		</td>
    	</tr>
    </table>
    
    <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title8>
            *单位名称
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpName onchange=verifyElement(this.verify,this.value) verify="单位名称|notnull&len<=60">
          </TD>
           </TR>
       <TR>
          <TD  class= title8>
            *单位地址编码
          </TD>
          <TD  class= input8>
            <Input class= code8 name=GrpAddressNo onchange=verifyElement(this.verify,this.value) verify="单位地址编码|len<=2">
          </TD>
          <TD  class= title8>
            单位地址
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpAddress onchange=verifyElement(this.verify,this.value) verify="单位地址|len<=80">
          </TD>
          <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpZipCode onchange=verifyElement(this.verify,this.value) verify="邮政编码|zipcode">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            单位性质
          </TD>
          <TD  class= input8>
            <Input class=code8 name=GrpNature verify="单位性质|len<=10" ondblclick="showCodeList('GrpNature',[this]);" onkeyup="showCodeListKey('GrpNature',[this]);">
          </TD>
      
          <TD  class= title8>
            行业类别
          </TD>
          <TD  class= input8>
            <Input class= common8 name=BusinessType verify="行业类别|len<=20">
          </TD>
          <TD  class= title8>
            单位总人数
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Peoples verify="单位总人数|int">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            注册资本金
          </TD>
          <TD  class= input8>
            <Input class= common8 name=RgtMoney verify="注册资本金|num&len<=17">
          </TD>
          <TD  class= title8>
            资产总额
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Asset verify="资产总额|num&len<=17">
          </TD>
          <TD  class= title8>
            净资产收益率
          </TD>
          <TD  class= input8>
            <Input class= common8 name=NetProfitRate verify="净资产收益率|num&len<=17">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            主营业务
          </TD>
          <TD  class= input8>
            <Input class= common8 name=MainBussiness verify="主营业务|len<=60">
          </TD>
          <TD  class= title8>
            单位法人代表
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Corporation verify="单位法人代表|len<=20">
          </TD>
          <TD  class= title8>
            机构分布区域
          </TD>
          <TD  class= input8>
            <Input class= common8 name=ComAera verify="机构分布区域|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            保险联系人一
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan1 verify="保险联系人一姓名|len<=10">
          </TD>
          <TD  class= title8>
            部门
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Department1 verify="保险联系人一部门|len<=30">
          </TD>
          <TD  class= title8>
            职务
          </TD>
          <TD  class= input8>
            <Input class= common8 name=HeadShip1 verify="保险联系人一职务|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone1 verify="保险联系人一联系电话|len<=30">
          </TD>
          <TD  class= title8>
            E-MAIL
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail1 verify="保险联系人一E-MAIL|len<=60">
          </TD>
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax1 verify="保险联系人一传真|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            保险联系人二
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            姓名
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan2 verify="保险联系人二姓名|len<=10">
          </TD>
          <TD  class= title8>
            部门
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Department2 verify="保险联系人二部门|len<=30">
          </TD>
          <TD  class= title8>
            职务
          </TD>
          <TD  class= input8>
            <Input class= common8 name=HeadShip2 verify="保险联系人二职务|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            联系电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone2 verify="保险联系人二联系电话|len<=30">
          </TD>
          <TD  class= title8>
            E-MAIL
          </TD>
          <TD  class= input8>
            <Input class= common8 name=E_Mail2 verify="保险联系人二E-MAIL|len<=60">
          </TD>
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax2 verify="保险联系人二传真|len<=30">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            付款方式
          </TD>
          <TD  class= input8>
            <Input class="code8" name=GetFlag verify="付款方式|code:PayMode" ondblclick="return showCodeList('PayMode',[this]);" onkeyup="return showCodeListKey('PayMode',[this]);">
          </TD>
          <TD  class= title8>
            开户银行
          </TD>
          <TD  class= input8>
            <Input class=code8 name=BankCode verify="开户银行|len<=24" ondblclick="showCodeList('bank',[this]);" onkeyup="showCodeListKey('bank',[this]);">
          </TD>
          <TD  class= title8>
            帐号
          </TD>
          <TD  class= input8>
            <Input class= common8 name=BankAccNo verify="帐号|len<=40">
          </TD>       
        </TR>
        
        <TR  class= common>
          <TD  class= title8>
            币种
          </TD>
          <TD  class= input8>
            <Input class=code8 name=Currency verify="币种|len<=2" ondblclick="showCodeList('currency',[this]);" onkeyup="showCodeListKey('currency',[this]);">
          </TD>
                 
        </TR>     
        <tr class=common>
        <td class= titleImg>
    			
    		</td>
        </TR> 
        
<TR  class= common>
          <TD  class= title8>
            投保申请日期 
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker" dateFormat="short" name=PolApplyDate >
          </TD> 
          <TD  class= title8>
            *保单生效日期 
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker" dateFormat="short" name=CValiDate >
          </TD>  
          </TR>
      </table>
    </Div>
    
    <Div  id= "divGroupPol21" style= "display: 'none'">
    <table>
        <TR  class= common>
          <TD  class= title8>
            电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Phone >
          </TD>
          <TD  class= title8>
            传真
          </TD>
          <TD  class= input8>
            <Input class= common8 name=Fax >
          </TD>
          <TD  class= title8>
            e_mail
          </TD>
          <TD  class= input8>
            <Input class= common8 name=EMail >
          </TD>
        </TR> 
    </table>
    </Div>
    <table class=common>
    <TR  class= common> 
      <TD  class= title8> 特别约定及备注 </TD>
    </TR>
    <TR  class= common>
      <TD  class= title8>
      <textarea name="GrpContSpec" cols="120" rows="3" class="common" >
      </textarea></TD>
    </TR>
    
            <Input type=hidden name=EmployeeRate verify="雇员自付比例|num&len<=5">
            <Input type=hidden name=FamilyRate verify="家属自付比例|num&len<=80">
    
    </table>
           
         
    <br>
      <INPUT class=cssButton VALUE="进入保单险种信息"  TYPE=button onclick="grpRiskInfo()"> 
      <INPUT class=cssButton VALUE="导入被保人清单"  TYPE=button onclick=""> 
       <INPUT class=cssButton VALUE="导出被保人清单"  TYPE=button onclick="">
        
      <input type=hidden id="fmAction" name="fmAction">
      <Input type=hidden name=ContNo  >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
