
function submitDate(){
	if(fm.ModifyDate.value==null||fm.ModifyDate.value==""){
		alert("请输入修改日期!");
		return false;
	}
	
	if(confirm("确定修改日期?")){
	    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.action="../app/EstmakedateSave.jsp";
		fm.submit();
	}
	
}

function afterSubmit( FlagStr, content )
{
	showInfo.close();
	window.focus();
	mAction = ""; 

	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		showDiv(operateButton, "true"); 
		showDiv(inputButton, "false");
		top.close();
	}
}
