<%
//程序名称：
//程序功能：契约保单发送及下载规则设置
//创建日期：2008-3-13 02:24下午
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<%
  GlobalInput globalInput = (GlobalInput)session.getValue("GI");
  String tManageCom = globalInput.ComCode;
%>

<script language="JavaScript">
  
var tManageCom = "<%= tManageCom%>";

function initForm()
{
	try
	{
		initInpBox();
		initBusinessRuleGrid();
	}
	catch(re)
	{
		alert("BusinessRuleConfigInit.jsp-->InitForm函数中发生异常:初始化界面错误，" + re.message);
	}
}

//保单信息列表的初始化
function initBusinessRuleGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="规则编码";
		iArray[1][1]="0px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		iArray[1][21]="RuleType";
    
		iArray[2]=new Array();
		iArray[2][0]="规则类型";
		iArray[2][1]="40px";           
		iArray[2][2]=80;            	
		iArray[2][3]=3;
		iArray[2][21]="RuleName";
		
		iArray[3]=new Array();
		iArray[3][0]="业务类型";
		iArray[3][1]="40px";           
		iArray[3][2]=100;            	
		iArray[3][3]=2;
    iArray[3][4]="TBBussType";
    iArray[3][5]="3|4";
    iArray[3][6]="0|1";
    iArray[3][9]="业务类型|code:TBBussType";
		iArray[3][21]="BussType";
    
		iArray[4]=new Array();
		iArray[4][0]="业务类型名";
		iArray[4][1]="40px";           
		iArray[4][2]=80;            	
		iArray[4][3]=3;
		iArray[4][21]="BussName";
		
		iArray[5]=new Array();
		iArray[5][0]="机构编码";
		iArray[5][1]="80px";           
		iArray[5][2]=300;            	
		iArray[5][3]=2;
    iArray[5][4]="comcode4";
    iArray[5][5]="5|6";
    iArray[5][6]="0|1";
    iArray[5][9]="机构编码|code:comcode4";
		iArray[5][21]="ComCode";
    
		iArray[6]=new Array();
		iArray[6][0]="机构";
		iArray[6][1]="40px";           
		iArray[6][2]=80;            	
		iArray[6][3]=3;
		iArray[6][21]="ComName";
    
		iArray[7]=new Array();
		iArray[7][0]="值";
		iArray[7][1]="40px";           
		iArray[7][2]=80;            	
		iArray[7][3]=3;
		iArray[7][21]="RuleValue";
		
		BusinessRuleGrid = new MulLineEnter( "fm" , "BusinessRuleGrid" );
		//这些属性必须在loadMulLine前
		BusinessRuleGrid.mulLineCount = 0;
		BusinessRuleGrid.displayTitle = 1;
		BusinessRuleGrid.canSel = 0;
		BusinessRuleGrid.canChk = 0;
		BusinessRuleGrid.hiddenPlus=0;
		BusinessRuleGrid.hiddenSubtraction=0;
		BusinessRuleGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>