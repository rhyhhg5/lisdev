
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function queryClick() {
	initProjectGrid();
	var strSql = "select distinct a.* from (select distinct lgcs.managecom,"
			   + "(select name from ldcom where comcode = lgcs.managecom),"
			   + "lgcs.projectno,lgcs.projectname from lcgrpcontsub lgcs " 
			   + "where lgcs.projectno is not null and lgcs.projectno != '' " 
			   + getWherePart("lgcs.ManageCom", "ManageCom", "like") 
			   + getWherePart("lgcs.ProjectNo", "ProjectNo") 
			   + getWherePart("lgcs.ProjectName", "ProjectName", "like") 
			   +" union"
			   +" select distinct lgp.managecom,"
			   +" (select name from ldcom where comcode = lgp.managecom),"
			   +" lgp.projectno,lgp.projectname from LCProjectInfo lgp " 
			   +" where lgp.projectno is not null and lgp.projectno != '' "
			   + getWherePart("lgp.ManageCom", "ManageCom", "like") 
			   + getWherePart("lgp.ProjectNo", "ProjectNo") 
			   + getWherePart("lgp.ProjectName", "ProjectName", "like") 
			   +" ) a "
			   +" order by a.managecom,a.projectno,a.projectname ";
	turnPage1.queryModal(strSql, ProjectGrid);
}
function returnClick() {
	var checkFlag = 0;
	for (i = 0; i < ProjectGrid.mulLineCount; i++) {
		if (ProjectGrid.getSelNo(i)) {
			checkFlag = ProjectGrid.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		var tProjectNo = ProjectGrid.getRowColData(checkFlag - 1, 3);
		if (tProjectNo == null || tProjectNo == "" || tProjectNo == "null") {
			alert("获取项目编码信息失败！");
			return false;
		}
		try
		{
			var tSQL = "select distinct a.* from (select  projectname,projectno from lcgrpcontsub lcg where lcg.projectno = '"+tProjectNo+"' union select projectname,projectno from LCProjectInfo lcp where lcp.projectno = '"+tProjectNo+"' ) a ";
			var arrReturn = easyExecSql(tSQL);
			top.opener.afterQuery(arrReturn);
			top.close();
		}
		catch(ex)
		{
			alert( "要返回的页面函数出错！");
		}
	} else {
		alert("请先选择项目信息！");
		return false;
	}
}