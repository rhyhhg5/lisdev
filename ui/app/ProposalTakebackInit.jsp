<%
//程序名称：ReProposalPrtInit.jsp
//程序功能：
//创建日期：2003-04-3 11:10:36
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ManageCom;
String strOperator =globalInput.Operator;
System.out.println("strOperator is "+strOperator);
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{

    try
    {
        // 初始化
        fm.all('PrtNo').value = '';
        fmSave.all('fmOperator').value = '<%=strOperator%>';
        fm.all('ManageCom').value = '<%=strManageCom%>';
        if(fm.all('ManageCom').value==86){
            fm.all('ManageCom').readOnly=false;
        }
        else{
            fm.all('ManageCom').readOnly=true;
        }
        if(fm.all('ManageCom').value!=null)
        {
            var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
            //显示代码选择中文
            if (arrResult != null) {
                fm.all('ManageComName').value=arrResult[0][0];
            }
        }
        //签单时间段默认为两个月
        var arrResult = easyExecSql("select current date - 2 month, current date from dual");
        if (arrResult != null) {
            fm.all('startSignDate').value=arrResult[0][0];
            fm.all('startGetPolDate').value=arrResult[0][0];
            fm.all('endSignDate').value=arrResult[0][1];
            fm.all('endGetPolDate').value=arrResult[0][1];
        }
        fm.all('AgentCode').value = '';
        fm.all('AgentCodeName').value = '';
        fm.all('PolApplyDate').value = '';
        fm.all('CValiDate').value = '';
        fm.all('InsuredName').value = '';
        fm.all('AppntName').value = '';
        fm.all('GetpolState').value = '0';
        fm.all('GetpolStateName').value = '未回销';
    }
    catch(ex)
    {
        alert("在ReProposalPrtInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm()
{
  try
  {
    initInpBox();
	  initPolGrid();
	  
  }
  catch(re)
  {
    alert("ReProposalPrtInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var PolGrid;          //定义为全局变量，提供给displayMultiline使用

// 保单信息列表的初始化
// 保单信息列表的初始化
function initPolGrid()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="合同号";         		//列名
      iArray[1][1]="82px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="印刷号";         		//列名
      iArray[2][1]="82px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="生效日期";         		//列名
      iArray[3][1]="72px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="投保人";         		//列名
      iArray[4][1]="70px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="保险费";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="业务员编码";         		//列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="业务员姓名";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许       
      
      iArray[8]=new Array();
      iArray[8][0]="管理机构";         		//列名
      iArray[8][1]="65px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      iArray[9]=new Array();
      iArray[9][0]="接收日期";         		//列名
      iArray[9][1]="70px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      iArray[10]=new Array();
      iArray[10][0]="回销日期";         		//列名
      iArray[10][1]="70px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      iArray[11]=new Array();
      iArray[11][0]="打印日期";         		//列名
      iArray[11][1]="70px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许  
         
      iArray[12]=new Array();
      iArray[12][0]="回销状态";         		//列名
      iArray[12][1]="10px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许  
      
      iArray[13]=new Array();
      iArray[13][0]="客户签收日期";         		//列名
      iArray[13][1]="70px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
      
      iArray[14]=new Array();
      iArray[14][0]="签收人员";         		//列名
      iArray[14][1]="5px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
      iArray[15]=new Array();
      iArray[15][0]="递交人员";         		//列名
      iArray[15][1]="5px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许  
        
      iArray[16]=new Array();
      iArray[16][0]="操作人员";         		//列名
      iArray[16][1]="5px";            		//列宽
      iArray[16][2]=100;            			//列最大值
      iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许  
      
      iArray[17]=new Array();
      iArray[17][0]="保单类型";         		//列名
      iArray[17][1]="5px";            		//列宽
      iArray[17][2]=100;            			//列最大值
      iArray[17][3]=3;              			//是否允许输入,1表示允许，0表示不允许  
      
        iArray[18]=new Array();
	    iArray[18][0]="销售渠道";                 //列名
	    iArray[18][1]="40px";               //列名
	    iArray[18][3]=0;                //列名
	
	    iArray[19]=new Array();
	    iArray[19][0]="网点";                 //列名
	    iArray[19][1]="80px";               //列名
	    iArray[19][3]=0;                //列名
	
        iArray[20]=new Array();
	    iArray[20][0]="回销状态";                 //列名
	    iArray[20][1]="60px";               //列名
	    iArray[20][3]=0;                //列名
	    iArray[20][21]="GetPolState";                //列名

      PolGrid = new MulLineEnter( "fmSave" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 10;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canChk = 0;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.selBoxEventFuncName ="Polinit";
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
    }
    catch(ex)
    {
        alert(ex);
    }
}

</script>