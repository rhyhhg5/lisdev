var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	if(!verifyInput2())
	{
		return false;
	}
	
	if(!checkData())
	{
		return false;
	}
	
	// 初始化险种查询条件
	var tRiskCode1 = fm.all('RiskCode1').value;
	var tRiskCode2 = fm.all('RiskCode2').value;
	var strRiskSQL = "";
	
	if(tRiskCode1 != null && tRiskCode1 != "" && tRiskCode2 != null && tRiskCode2 != "")
	{
		strRiskSQL = "select 1 from lcgrppol lgp where lgp.RiskCode in('" + tRiskCode1 + "','" + tRiskCode2 + "')";
	}else 
	if(tRiskCode1 != null && tRiskCode1 != "" && (tRiskCode2 == null || tRiskCode2 == ""))
	{
		strRiskSQL = "select 1 from lcgrppol lgp where lgp.RiskCode in('" + tRiskCode1 + "')";
	}else 
	if((tRiskCode1 == null || tRiskCode1 == "") && tRiskCode2 != null && tRiskCode2 != "")
	{
		strRiskSQL = "select 1 from lcgrppol lgp where lgp.RiskCode in('" + tRiskCode2 + "')";
	}
	
	var strSQL = "select lgc.PrtNo,lgc.GrpName,lgc.GrpContNo,lgc.CValiDate,lgc.CInValiDate from lcgrpcont lgc where lgc.AppFlag = '1' " 
				+ getWherePart('lgc.GrpContNo', 'ContNo')
				+ getWherePart('lgc.PrtNo', 'PrtNo')
				+ getWherePart('lgc.ManageCom', 'ManageCom', 'like')
				+ getWherePart('lgc.CValiDate', 'startCValiDate', '>=')
				+ getWherePart('lgc.CValiDate', 'endCValiDate', '<=')
				+ getWherePart('lgc.SignDate', 'startSignDate', '>=')
				+ getWherePart('lgc.SignDate', 'endSignDate', '<=');
	
	if(strRiskSQL != null && strRiskSQL != "")
	{
		strSQL = strSQL + " and exists (" + strRiskSQL + " and lgp.GrpContNo = lgc.GrpContNo ) ";
	}
	
	turnPage.pageLineNum = 100;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 100);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    //fm.querySql.value = strSQL;
}

//下载团单被保险人险种信息
function downLoadLists()
{
	var i = 0;
	var checkFlag = 0;
	var tGrpContNo = "";
	
	for (i=0; i<PolGrid.mulLineCount; i++) 
	{
		if(i == 0)
		{
			tGrpContNo = tGrpContNo + "'" + PolGrid.getRowColData(i,3) + "'";
		}
		else
		{
			tGrpContNo = tGrpContNo + ",'" + PolGrid.getRowColData(i,3) + "'";
		}
	}
	
	if(tGrpContNo != null && tGrpContNo != "")
	{
		var tStrSQL = "select lcp.PrtNo,lcp.AppntName,lcp.Grpcontno,lcp.CValiDate,lcp.EndDate,"
				+ "lcp.Insuyear,lcp.Insuyearflag,lcp.InsuredName,"
				+ "case when lcp.InsuredSex = '0' then '男' when lcp.InsuredSex = '1' then '女' else '其他' end,"
				+ "lcid.IDNo,lcid.BirthDay,lcp.Contplancode,lcp.riskcode,lcp.prem,"
				+ "round(double(lcpr.riskprem/lccp.peoples2),2),lcid.BankCode,lcid.BankAccNo, lcid.AccName "
				+ "from lcpol lcp " 
				+ "inner join lcinsured lcid on lcid.GrpContNo = lcp.GrpContNo and lcid.Insuredno = lcp.Insuredno "
				+ "inner join lccontplan lccp on lccp.grpcontno = lcp.grpcontno and lccp.ContPlanCode = lcp.ContPlanCode " 
				+ "inner join lccontplanrisk lcpr on lcpr.grpcontno = lcp.grpcontno and lcpr.ContPlanCode = lcp.ContPlanCode and lcpr.riskcode = lcp.riskcode "  
				+ "where 1 = 1 "
				+ "and lcp.GrpContNo in (" + tGrpContNo + ") "
				+ "order by lcp.PrtNo asc ";
					
		fm.querySql.value = tStrSQL;
		var oldAction = fm.action;
		fm.action = "GrpTJContListSave.jsp";
		fm.submit();
		fm.action = oldAction;
	}
	else
	{
		alert("没有保单信息，无法进行下载！");
	}
}

//校验查询条件
function checkData()
{
	var tContNo = fm.all('ContNo').value;
	var tPrtNo = fm.all('PrtNo').value;
	var tStartCValiDate = fm.all('startCValiDate').value;
	var tEndCValiDate = fm.all('endCValiDate').value;
	var tStartSignDate = fm.all('startSignDate').value;
	var tEndSignDate = fm.all('endSignDate').value;
	
	if((tContNo == null || tContNo == "") && (tPrtNo == null || tPrtNo == "")
		&&(tStartCValiDate == null || tStartCValiDate == "")&&(tEndCValiDate == null || tEndCValiDate == "")
		&&(tStartSignDate == null || tStartSignDate == "")&&(tEndSignDate == null || tEndSignDate == ""))
	{
		alert("保单号、印刷号、生效日期起止和签单日期起止四组条件，必须填写其中一组才能进行查询！");
		return false;
	}
	
	if((tStartSignDate != null && tStartSignDate != "") 
		&& (tEndSignDate == null || tEndSignDate == ""))
	{
		alert("请选择签单日期止！");
		return false;
	}
	
	if((tStartSignDate == null || tStartSignDate == "") 
		&& (tEndSignDate != null && tEndSignDate != ""))
	{
		alert("请选择签单日期起！");
		return false;
	}
	
	if((tStartSignDate != null && tStartSignDate != "") 
		&& (tEndSignDate != null && tEndSignDate != ""))
	{
		if(dateDiff(tStartSignDate,tEndSignDate,"M") > 3)
		{
			alert("签单日期统计最多为三个月！");
			return false;
		}
	}
	
	if((tStartCValiDate != null && tStartCValiDate != "") 
		&& (tEndCValiDate == null || tEndCValiDate == ""))
	{
		alert("请选择生效日期止！");
		return false;
	}
	
	if((tStartCValiDate == null || tStartCValiDate == "") 
		&& (tEndCValiDate != null && tEndCValiDate != ""))
	{
		alert("请选择生效日期起！");
		return false;
	}
	
	if((tStartCValiDate != null && tStartCValiDate != "") 
		&& (tEndCValiDate != null && tEndCValiDate != ""))
	{
		if(dateDiff(tStartCValiDate,tEndCValiDate,"M") > 3)
		{
			alert("生效日期统计最多为三个月！");
			return false;
		}
	}
	return true;
}


