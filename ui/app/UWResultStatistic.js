var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;
	var tStartDate = fm.StartDate.value;
	var tEndDate = fm.EndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>3)
	{
		alert("统计期最多为三个月！");
		return false;
	}
	
	var tMngCom = fm.ManageCom.value;
	var tStrSQL = "";
	if(dateDiff(tStartDate,tEndDate,"M")<1){
      alert("统计止期比统计统计起期早");
      return false;
    }
     var tMngCom = fm.ManageCom.value;
     var length=tMngCom.split("").length;
     var managecom="";
     if(length==8){
         managecom="length('"+tMngCom+"')";
     }else{
        managecom="length('"+tMngCom+"')*2";
     }	
    
	
  tStrSQL="  select U , V , A+B    "
         +" , div ( ( C+D )*100 , ( A+B ) ) , div ( ( E+F )*100 , ( A+B ) ) , div ( ( G+H )*100 , ( A+B ) ) , div ( ( I+J )*100 , ( A+B ) ) , div ( ( K+L )*100 , ( A+B ) ) , "
         +" div ( ( M+N )*100 , ( A+B ) ) , div ( ( O+P )*100 , ( A+B ) ) , div ( ( R+S )*100 , ( A+B ) ) "
         +" from "
         +" (  "
         +" SELECT Z.comcode U , Z.showname V ,  "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '4' , '9' , '1' , 'a' , '8' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) A ,   "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '4' , '9' , '1' , 'a' , '8' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) B ,  "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and uwflag='9' and conttype='1' and prtno like '16%' and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) C ,  "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and uwflag='9' and conttype='1' and prtno like '16%' and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) D ,  "
         +" ( select count ( 1 ) from LCSpec a , lcpol b where a.polno=b.polno and a.contno=b.proposalcontno "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and substr ( b.managecom , 1 , "+managecom+" )=Z.COMCODE and b.prtno like '16%' and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" ) E , "
         +" ( select count ( 1 ) from LCSpec a , lbpol b where a.polno=b.polno and a.contno=b.proposalcontno "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and substr ( b.managecom , 1 , "+managecom+" )=Z.COMCODE and b.prtno like '16%' and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" ) F , "
         +" ( select count ( 1 ) from lcprem a , lcpol b where substr ( a.payplancode , 1 , 6 )='000000'  "
         +" and a.polno=b.polno and a.contno=b.contno and a.grpcontno='00000000000000000000' and conttype='1' and substr ( b.managecom , 1 , "+managecom+" )=Z.COMCODE and b.prtno like '16%' "
         +" and b.uwflag='4' and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+") G ,  "
         +" ( select count ( 1 ) from lbprem a , lbpol b where substr ( a.payplancode , 1 , 6 )='000000'  "
         +" and a.polno=b.polno and a.contno=b.contno and a.grpcontno='00000000000000000000' and conttype='1' and substr ( b.managecom , 1 , "+managecom+" )=Z.COMCODE and b.prtno like '16%' "
         +" and b.uwflag='4' and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" ) H , "
         +" ( select count ( 1 ) from LCUWMaster a , lcpol b where a.polno=b.polno and a.contno=b.contno and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and a.SubMultFlag='1' and substr ( b.managecom , 1 , "+managecom+" )=Z.COMCODE and b.prtno like '16%' ) I , "
         +" ( select count ( 1 ) from LCUWMaster a , lbpol b where a.polno=b.polno and a.contno=b.contno and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+"  "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and a.SubMultFlag='1' and substr ( b.managecom , 1 , "+managecom+" )=Z.COMCODE and b.prtno like '16%' ) J , "
         +" ( select count ( 1 ) from LCUWMaster a , lcpol b where a.polno=b.polno and a.contno=b.contno and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and a.SubAmntFlag='1' and substr ( b.managecom , 1 , "+managecom+" )=Z.COMCODE and b.prtno like '16%' ) K , "
         +" ( select count ( 1 ) from LCUWMaster a , lbpol b where a.polno=b.polno and a.contno=b.contno and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and a.SubAmntFlag='1' and substr ( b.managecom , 1 , length('86110000') )=Z.COMCODE and b.prtno like '16%' ) L , "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '8' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) M , "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '8' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) N , "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '1' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) O , "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '1' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) P , "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( 'a' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) R , "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , "+managecom+" )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( 'a' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) S "
         +" from LDCom Z where length ( trim ( Z.comcode ) )="+managecom+" and sign='1' and substr ( Z.comcode , 1 , length('"+tMngCom+"') )='"+tMngCom+"' "
         +" ) as X "
         +" union "
         +" select U , V , A+B "
         +" , div ( ( C+D )*100 , ( A+B ) ) , div ( ( E+F )*100 , ( A+B ) ) , div ( ( G+H )*100 , ( A+B ) ) , div ( ( I+J )*100 , ( A+B ) ) , div ( ( K+L )*100 , ( A+B ) ) ,  "
         +" div ( ( M+N )*100 , ( A+B ) ) , div ( ( O+P )*100 , ( A+B ) ) , div ( ( R+S )*100 , ( A+B ) ) "
         +" from "
         +" (  "
         +" SELECT '总计' U , '总计' V , "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '4' , '9' , '1' , 'a' , '8' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) A , "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '4' , '9' , '1' , 'a' , '8' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) B , "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and uwflag='9' and conttype='1' and prtno like '16%' and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) C ,  "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and uwflag='9' and conttype='1' and prtno like '16%' and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) D ,  "
         +" ( select count ( 1 ) from LCSpec a , lcpol b where a.polno=b.polno and a.contno=b.proposalcontno "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and substr ( b.managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and b.prtno like '16%' and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" ) E ,  " 
         +" ( select count ( 1 ) from LCSpec a , lbpol b where a.polno=b.polno and a.contno=b.proposalcontno "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and substr ( b.managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and b.prtno like '16%' and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" ) F , "
         +" ( select count ( 1 ) from lcprem a , lcpol b where substr ( a.payplancode , 1 , 6 )='000000' "
         +" and a.polno=b.polno and a.contno=b.contno and a.grpcontno='00000000000000000000' and conttype='1' and substr ( b.managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and b.prtno like '16%' "
         +" and b.uwflag='4' and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" ) G , "
         +" ( select count ( 1 ) from lbprem a , lbpol b where substr ( a.payplancode , 1 , 6 )='000000' "
         +" and a.polno=b.polno and a.contno=b.contno and a.grpcontno='00000000000000000000' and conttype='1' and substr ( b.managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and b.prtno like '16%' "
         +" and b.uwflag='4' and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" ) H , "
         +" ( select count ( 1 ) from LCUWMaster a , lcpol b where a.polno=b.polno and a.contno=b.contno and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and a.SubMultFlag='1' and substr ( b.managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and b.prtno like '16%' ) I , "
         +" ( select count ( 1 ) from LCUWMaster a , lbpol b where a.polno=b.polno and a.contno=b.contno and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and a.SubMultFlag='1' and substr ( b.managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and b.prtno like '16%' ) J , "
         +" ( select count ( 1 ) from LCUWMaster a , lcpol b where a.polno=b.polno and a.contno=b.contno and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and a.SubAmntFlag='1' and substr ( b.managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and b.prtno like '16%' ) K , "
         +" ( select count ( 1 ) from LCUWMaster a , lbpol b where a.polno=b.polno and a.contno=b.contno and b.UWdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("b.riskcode","RiskCode")+getWherePart("b.uwcode","UWUserCode")+" "
         +" and b.uwflag='4' and a.grpcontno='00000000000000000000' and b.conttype='1' and a.SubAmntFlag='1' and substr ( b.managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and b.prtno like '16%' ) L , "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '8' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) M , "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '8' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) N , "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '1' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) O , "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( '1' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) P , "
         +" ( select count ( 1 ) from lcpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( 'a' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) R , "
         +" ( select count ( 1 ) from lbpol where substr ( managecom , 1 , length('"+tMngCom+"') )=Z.COMCODE and conttype='1' and prtno like '16%' and uwflag in ( 'a' ) and Uwdate between '"+tStartDate+"' and '"+tEndDate+"' "+  getWherePart("riskcode","RiskCode")+getWherePart("uwcode","UWUserCode")+" ) S "
         +" from LDCom Z where length ( trim ( Z.comcode ) )=length('"+tMngCom+"') and sign='1' and substr ( Z.comcode , 1 , length('"+tMngCom+"') )='"+tMngCom+"'  "
         +" ) as X order by U WITH UR " ;
	
     fm.querySql.value = tStrSQL;
     fm.action="UWResultStatisticSubDown.jsp";
     fm.submit();
//  var i = 0;
//  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//  //showSubmitFrame(mDebug);
//  //fm.fmtransact.value = "PRINT";
//  fm.target = "f1print";
//  fm.all('op').value = '';
//  fm.submit();
//  showInfo.close();     

}
function getRisk(Obj)
{
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select RiskCode, RiskName from LMRiskApp where RiskProp in ('I','A','C','D')"
           + " order by RiskCode";
  ;
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "")
  {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++)
    {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
  }
  Obj.CodeData=tCodeData;
}
/*
function CheckDate()
{
	if(fm.StartDate.value!=""&fm.EndDate.value=="")
	{
				alert("核保开始日期和结束日期必须同时录入！");
			return false;

	}else if(fm.StartDate.value==""&fm.EndDate.value!=""){
		alert("核保开始日期和结束日期必须同时录入！");
			return false;
	}
}
*/


