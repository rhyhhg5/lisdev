<%
//程序名称：LGrpInsuredInit.jsp
//程序功能：大团单被保人录入页面
//创建日期：2013-05-14
//创建人  ：张成轩
%>

<script language="JavaScript">

function initForm()
{
  try
  {
  	initDiskErrQueryGrid();
  	initContInfo();
  }
  catch(re)
  {
    alert("LCInuredListInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initDiskErrQueryGrid()
{
    var iArray = new Array();
      
    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）             
      iArray[0][1]="30px";         			//列宽                                                     
      iArray[0][2]=10;          			//列最大值                                                 
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[1]=new Array();                                                                                       
      iArray[1][0]="团体保单号";    			//列名                                                     
      iArray[1][1]="150px";            			//列宽                                                     
      iArray[1][2]=100;            			//列最大值                                                 
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许5                    
                                                                                                                   
      iArray[2]=new Array();                                                                                       
      iArray[2][0]="批次号";         		//列名                                                     
      iArray[2][1]="150px";            			//列宽                                                     
      iArray[2][2]=100;            			//列最大值                                                 
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许 0                     

      iArray[3]=new Array();                                                                                       
      iArray[3][0]="合同ID";         		//列名                                                     
      iArray[3][1]="100px";            			//列宽                                                     
      iArray[3][2]=100;            			//列最大值                                                 
      iArray[3][3]=3;             			//是否允许输入,1表示允许，0表示不允许1                    
         
      iArray[4]=new Array();                                                                                       
      iArray[4][0]="被保人ID";         		//列名                                                     
      iArray[4][1]="100px";            			//列宽                                                     
      iArray[4][2]=100;            			//列最大值                                                 
      iArray[4][3]=0;             			//是否允许输入,1表示允许，0表示不允许1                    
                                                                                                                          
      iArray[5]=new Array();                                                                                       
      iArray[5][0]="被保人姓名";         		//列名                                                     
      iArray[5][1]="100px";            			//列宽                                                     
      iArray[5][2]=100;            			//列最大值                                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用6     
                                                                                                                   
      iArray[6]=new Array();                                                                                       
      iArray[6][0]="错误信息";         		//列名                                                     
      iArray[6][1]="700px";            			//列宽                                                     
      iArray[6][2]=100;            			//列最大值                                                 
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用3
                                                                                                           
      DiskErrQueryGrid = new MulLineEnter( "fm" , "DiskErrQueryGrid" ); 
      //这些属性必须在loadMulLine前
      DiskErrQueryGrid.mulLineCount = 0;   
      DiskErrQueryGrid.displayTitle = 1;
	  	DiskErrQueryGrid.hiddenPlus = 1;
      DiskErrQueryGrid.hiddenSubtraction = 1;
      DiskErrQueryGrid.canSel = 1;
      DiskErrQueryGrid.selBoxEventFuncName = "showList";
      DiskErrQueryGrid.loadMulLine(iArray);  

      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>