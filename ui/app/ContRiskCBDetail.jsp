<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：
//程序功能：
//创建日期：2009-08-11
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="ContRiskCBDetail.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();">    
<form action="" method=post name=fm target="fraSubmit">

    <table class="common">
        <tr class="common">
            <td class="title">管理机构</td>
            <td class="input">
                <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
            </td>
            <td class="title">签单日期起</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="StartSignDate" verify="签单日期|date&notnull" elementtype="nacessary" />
            </td>
            <td class="title">签单日期止</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="EndSignDate" verify="签单日期|date&notnull" elementtype="nacessary" />
            </td>
        </tr>
        <tr class="common">
            <td class="title">销售渠道</td>
            <td class="input">
                <input class="codeNo" name="SaleChnl" verify="销售渠道" ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" readonly="readonly" />
            </td>
            <td class="title">是否移动行销平台出单</td>
            <td class="input">
            	<Input class=codeno name=ContainsPAD CodeData='0|^0|全部^1|是^2|否'  ondblclick="return showCodeListEx('ContainsPAD',[this,ContainsPADName],[0,1]);" onkeyup="return showCodeListKeyEx('ContainsPAD',[this,ContainsPADName],[0,1]);" value = "0"><Input class=codename name="ContainsPADName" readonly=true value = "全部">
			</td>
            <td class="title">&nbsp;</td>
            <td class="input">&nbsp;</td>
        </tr>
    </table>
    
    <br />
    
    <input value="下  载" class="cssButton" type="button" onclick="submitForm();" />
    
    <hr />
    
    <div>
        <font color="red">
            <ul>报表说明：
                <li>该报表是按照保单签发时间为口径的个人保单险种情况列明报表。</li>
                <li>此报表包括：犹豫期退保，退保的情况</li>
                <li>此报表不包括：续保保单数据。</li>
                <li>签单日期查询口径：这里指保单签单日期。</li>
            </ul>
            <ul>名词解释：
            </ul>
            <ul>统计条件：
                <li>管理机构、签单日期段、销售渠道。</li>
                <li>管理机构、签单日期段必填。</li>
                <li>签单日期段间隔不超过十二个月。</li>
            </ul>
        </font>
    </div>
    
    <input name="querySql" type="hidden" />
    <input type="hidden" name=op value="" />

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 