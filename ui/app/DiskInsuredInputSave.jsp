<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LCInuredListSave.jsp
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LCInsuredListSchema tLCInsuredListSchema   = new LCInsuredListSchema();
  OLCInuredListUI tOLCInuredListUI   = new OLCInuredListUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println(transact);
    tLCInsuredListSchema.setGrpContNo(request.getParameter("GrpContNo"));
    tLCInsuredListSchema.setInsuredID(request.getParameter("InsuredID"));
    tLCInsuredListSchema.setState(request.getParameter("State"));
    tLCInsuredListSchema.setContNo(request.getParameter("ContNo"));
    tLCInsuredListSchema.setBatchNo(request.getParameter("BatchNo"));
    tLCInsuredListSchema.setInsuredNo(request.getParameter("InusredNo"));
    tLCInsuredListSchema.setRetire(request.getParameter("Retire"));
    tLCInsuredListSchema.setEmployeeName(request.getParameter("EmployeeName"));
    tLCInsuredListSchema.setInsuredName(request.getParameter("InsuredName"));
    tLCInsuredListSchema.setRelation(request.getParameter("Relation"));
    tLCInsuredListSchema.setSex(request.getParameter("Sex"));
    tLCInsuredListSchema.setBirthday(request.getParameter("Birthday"));
    tLCInsuredListSchema.setIDType(request.getParameter("IDType"));
    tLCInsuredListSchema.setIDNo(request.getParameter("IDNo"));
    tLCInsuredListSchema.setContPlanCode(request.getParameter("ContPlanCode"));
    tLCInsuredListSchema.setOccupationType(request.getParameter("OccupationType"));
    tLCInsuredListSchema.setBankCode(request.getParameter("BankCode"));
    tLCInsuredListSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLCInsuredListSchema.setAccName(request.getParameter("AccName"));
    tLCInsuredListSchema.setOperator(request.getParameter("Operator"));
    tLCInsuredListSchema.setMakeDate(request.getParameter("MakeDate"));
    tLCInsuredListSchema.setMakeTime(request.getParameter("MakeTime"));
    tLCInsuredListSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLCInsuredListSchema.setModifyTime(request.getParameter("ModifyTime"));
    tLCInsuredListSchema.setSchoolNmae(request.getParameter("SchoolName"));
    tLCInsuredListSchema.setClassName(request.getParameter("ClassName"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  System.out.println(tLCInsuredListSchema.encode());
	tVData.add(tLCInsuredListSchema);
  	tVData.add(tG);
  	TransferData tdata = new TransferData();
	tdata.setNameAndValue("NeedCalPrem","NoCalPrem");
	tVData.addElement(tdata);
    tOLCInuredListUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLCInuredListUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功，请再次点击“计算保费”按钮，对修改后的数据进行算费操作! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
