<%@page import="com.sinosoft.utility.*"%>
<!--用户校验类-->

<script language="JavaScript">
	function initInpBox() {
		try {
	        // 初始化
	        fm.all('typecode').value = '';
	        fm.all('typename').value = '';
	        fm.all('beforeMinValue').value = '';
	        fm.all('beforeMaxValue').value = '';
	        fm.all('afterMinValue').value = '';
	        fm.all('afterMaxValue').value = '';
		} catch (ex) {
			alert("在PADPrintTypeTranInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	function initInpBox0() {
		try {
	        // 初始化
	         fm.all('beforeMinValue').value = '';
	        fm.all('beforeMaxValue').value = '';
	        fm.all('afterMinValue').value = '';
	        fm.all('afterMaxValue').value = '';
	        fm.all('bMaxValue').style.display = "";
			 fm.all('aMaxValue').style.display = "";
			  fm.all('beforeMaxValue').style.display = "";
			 fm.all('afterMaxValue').style.display = "";
		} catch (ex) {
			alert("在PADPrintTypeTranInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}

	function initForm() {
		try {
			initInpBox();
			initPolGrid0();
			
		} catch (re) {
			alert("PADPrintTypeTranInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}

	var PolGrid; //定义为全局变量，提供给displayMultiline使用

	// 保单信息列表的初始化
	function initPolGrid0() {
		var iArray = new Array();

		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1] = "30px"; //列宽
			iArray[0][2] = 10; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "最小值"; //列名
			iArray[1][1] = "100px"; //列宽
			iArray[1][2] = 100; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[2] = new Array();
			iArray[2][0] = "最大值"; //列名
			iArray[2][1] = "100px"; //列宽
			iArray[2][2] = 100; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[3] = new Array();
			iArray[3][0] = "注释"; //列名
			iArray[3][1] = "100px"; //列宽
			iArray[3][2] = 100; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许
			
			iArray[4] = new Array();
			iArray[4][0] = "传输类型"; //列名
			iArray[4][1] = "10px"; //列宽
			iArray[4][2] = 100; //列最大值
			iArray[4][3] = 3; //是否允许输入,1表示允许，0表示不允许

			PolGrid = new MulLineEnter("fm", "PolGrid");

			//这些属性必须在loadMulLine前
			PolGrid.mulLineCount = 0;
			PolGrid.displayTitle = 1;// 显示标题
			PolGrid.locked = 1;
			PolGrid.canSel = 1;// 显示单选框
			PolGrid.hiddenPlus = 1;
			PolGrid.selBoxEventFuncName = "Polinit";// 单击单选框时,响应 js 函数
			PolGrid.hiddenSubtraction = 1;// 不显示 - 号按钮

			PolGrid.loadMulLine(iArray);

		} catch (ex) {
			alert(ex);
		}
	}
	function initPolGrid1() {
		var iArray = new Array();

		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1] = "30px"; //列宽
			iArray[0][2] = 10; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "最小值"; //列名
			iArray[1][1] = "100px"; //列宽
			iArray[1][2] = 100; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[2] = new Array();
			iArray[2][0] = "最大值"; //列名
			iArray[2][1] = "100px"; //列宽
			iArray[2][2] = 100; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[3] = new Array();
			iArray[3][0] = "注释"; //列名
			iArray[3][1] = "100px"; //列宽
			iArray[3][2] = 100; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array();
			iArray[4][0] = "传输类型"; //列名
			iArray[4][1] = "10px"; //列宽
			iArray[4][2] = 100; //列最大值
			iArray[4][3] = 3; //是否允许输入,1表示允许，0表示不允许

			PolGrid = new MulLineEnter("fm", "PolGrid");

			//这些属性必须在loadMulLine前
			PolGrid.mulLineCount = 0;
			PolGrid.displayTitle = 1;// 显示标题
			PolGrid.locked = 1;
			PolGrid.canSel = 1;// 显示单选框
			PolGrid.hiddenPlus = 1;
			PolGrid.selBoxEventFuncName = "Polinit";// 单击单选框时,响应 js 函数
			PolGrid.hiddenSubtraction = 1;// 不显示 - 号按钮

			PolGrid.loadMulLine(iArray);

		} catch (ex) {
			alert(ex);
		}
	}
		function initPolGrid2() {
			var iArray = new Array();

			try {
				iArray[0] = new Array();
				iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
				iArray[0][1] = "30px"; //列宽
				iArray[0][2] = 10; //列最大值
				iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

				iArray[1] = new Array();
				iArray[1][0] = "最小人数"; //列名
				iArray[1][1] = "100px"; //列宽
				iArray[1][2] = 100; //列最大值
				iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许

				iArray[2] = new Array();
				iArray[2][0] = "团单录入最小被保险人数校验"; //列名
				iArray[2][1] = "100px"; //列宽
				iArray[2][2] = 100; //列最大值
				iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许
				
				iArray[3] = new Array();
				iArray[3][0] = "注释"; //列名
				iArray[3][1] = "100px"; //列宽
				iArray[3][2] = 100; //列最大值
				iArray[3][3] = 3; //是否允许输入,1表示允许，0表示不允许

				iArray[4] = new Array();
				iArray[4][0] = "传输类型"; //列名
				iArray[4][1] = "10px"; //列宽
				iArray[4][2] = 100; //列最大值
				iArray[4][3] = 3; //是否允许输入,1表示允许，0表示不允许

				PolGrid = new MulLineEnter("fm", "PolGrid");

				//这些属性必须在loadMulLine前
				PolGrid.mulLineCount = 0;
				PolGrid.displayTitle = 1;// 显示标题
				PolGrid.locked = 1;
				PolGrid.canSel = 1;// 显示单选框
				PolGrid.hiddenPlus = 1;
				PolGrid.selBoxEventFuncName = "Polinit";// 单击单选框时,响应 js 函数
				PolGrid.hiddenSubtraction = 1;// 不显示 - 号按钮

				PolGrid.loadMulLine(iArray);

			} catch (ex) {
				alert(ex);
			}
	}
</script>