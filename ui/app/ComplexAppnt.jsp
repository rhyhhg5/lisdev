<DIV id=DivLCAppntIndButton STYLE="display:''">
<!-- 投保人信息部分 -->
<table>
<tr>
<td>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
</td>
<td class= titleImg>
投保人资料(客户号)：<Input class= common  name=AppntNo >
<INPUT id="butBack" VALUE="查  询" TYPE=button class= cssButton onclick="queryAppntNo();">
首次投保客户无需填写客户号）
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCAppntInd STYLE="display:''">
 <table  class= common>
        <TR  class= common>        
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=AppntName elementtype=nacessary verify="投保人姓名|notnull" >
          </TD>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
          <input class="coolDatePicker" dateFormat="short" elementtype=nacessary name="AppntBirthday" verify="投保人出生日期|notnull&date" >
          </TD>                          
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class=codeNo name=AppntSex  verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title>
            婚姻
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntMarriage"  verify="投保人婚姻状况|notnull&code:Marriage" ondblclick="return showCodeList('Marriage',[this,AppntMarriageName],[0,1]);" onkeyup="return showCodeListKey('Marriage',[this,AppntMarriageName],[0,1]);"><input class=codename name=AppntMarriageName readonly=true elementtype=nacessary>    
          </TD>           
        </TR>  
        <TR  class= common>
          
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntIDType" verify="投保人证件类型|code:IDType1" ondblclick="return showCodeList('IDType1',[this,AppntIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType1',[this,AppntIDTypeName],[0,1]);"><input class=codename name=AppntIDTypeName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input colspan=1>
            <Input class= common3 name="AppntIDNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" >
          </TD> 
          <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <input class=codeNo name="AppntNativePlace" verify="投保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,AppntNativePlaceName],[0,1]);" onkeyup="return showCodeListKey('NativePlace',[this,AppntNativePlaceName],[0,1]);"><input class=codename name=AppntNativePlaceName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title id="NativePlace" style="display: none">
          <div id="NativeCityTitle">国家</div>
          </TD>
          <TD  class= input id="NativeCity" style="display: none">
          <input class=codeNo name="AppntNativeCity" ondblclick="return showCodeList('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,AppntNativeCityName],[0,1],null,fm.AppntNativePlace.value,'ComCode',1);"><input class=codename name=AppntNativeCityName readonly=true >    
          </TD>   
          </TR>
          <TR class=common >
  	  		 <TD  class= title COLSPAN="1" >
  	         	 	证件生效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=AppIDStartDate  verify="证件生效日期|date">
  	        </TD>
  	        	 <TD  class= title COLSPAN="1" >
  	         		 证件失效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=AppIDEndDate  verify="证件失效日期|date">
  	        </TD>
  	       <TD  class= title>
            		授权使用客户信息
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntAuth" readonly=true verify="授权使用客户信息|notnull&code:Auth" ondblclick="return false;return showCodeList('Auth',[this,AppntAuthName],[0,1]);" onkeyup="return false;return showCodeListKey('Auth',[this,AppntAuthName],[0,1]);"><input class=codename name=AppntAuthName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title id="PassNumer" style="display: none">
          <div id="PassNumerTitle">原通行证号码</div>
          </TD>
          <TD  class= input id="PassIDNo" style="display: none">
           <Input class= common3 name="AppntPassIDNo" onblur=""  verify="投保人原通行证号码|len<=20" >  
          </TD> 
  	        </TR>
  	      <TR>
  	      <TD  class= title8>
           个税征收方式
          </TD>
          <TD  class= input8>
      			<input NAME="TaxPayerType" CLASS=codeNo ondblclick="return showCodeList('taxpayertype',[this,TaxPayerTypeName],[0,1],null,'1','1',1);" onkeyup="return showCodeListKey('taxpayertype',[this,TaxPayerTypeName],[0,1],null,'1','1',1);" verify="个税征收方式|code:taxpayertype"><input class=codename name=TaxPayerTypeName readonly=true >
         </TD>
  	      <TD  class= title8>
            个人税务登记证代码
          </TD>
          <TD  class= input8>
      			<Input NAME=TaxNo VALUE="" CLASS=common >
         </TD>
         <TD  class= title8>
            个人社会信用代码
          </TD>
          <TD  class= input8>
      			<Input NAME=CreditCode VALUE="" CLASS=common >
         </TD> 
  	      </TR>   
  	      <TR > 
  	      <TD  class= title8>
            团体编号
          </TD>
          <TD  class= input8>
      		 <input NAME="GrpNo" VALUE MAXLENGTH="0" CLASS="code" ondblclick="return queryGrpTax();" onkeyup="return queryGrpTax1();">
         </TD>         
          <TD  class= title8>
            单位税务登记证代码
          </TD>
          <TD  class= input8>
      			<Input NAME=GTaxNo VALUE=""  readonly CLASS=common >
      			<Input NAME=BatchNo VALUE=""  readonly CLASS=common type=hidden>
      			<Input NAME=InsuredId VALUE=""  readonly CLASS=common type=hidden>
         </TD>
         <TD  class= title8>
            单位社会信用代码
          </TD>
          <TD  class= input8>
      			<Input NAME=GOrgancomCode VALUE=""  readonly CLASS=common >
         </TD>
         </TR>                                                            
        <TR  class= common>
          <TD  class= title>
            职业代码
          </TD>
          <TD  class= input>
            <Input class="code" name="AppntOccupationCode" elementtype=nacessary verify="投保人职业代码|notnull" ondblclick="return showCodeList('OccupationCode',[this,AppntOccupationName],[0,1],null,null,null,null,300);" onkeyup="return showCodeListKey('OccupationCode',[this,AppntOccupationName],[0,1],null,null,null,null,300);"onfocus="getdetailwork();">
          </TD> 
          <TD  class= title>
            职业
          </TD>
          <TD  class= input>
            <Input class="common" name="AppntOccupationName" elementtype=nacessary TABINDEX="-1" readonly >
          </TD>         
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input>
            <Input class="code" name="AppntOccupationType" verify="投保人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>
          <TD  class= title>
      			岗位职务
    			</TD>
    			<TD  class= input>
      		<Input class= 'common' name=AppntPosition >
    			</TD>                         
        </TR>       
        <TR  class= common>
          <!--TD  class= title>
            单位编码
          </TD>
          <TD  class= input  >
            <Input class= code name="AppntGrpNo"  ondblclick="getCompanyCode();return showCodeListEx('getGrpNo',[this,AppntGrpName],[0,1],'', '', '', true,300);" onkeyup="getCompanyCode();return showCodeListKeyEx('getGrpNo',[this,AppntGrpName],[0,1],'', '', '', true,300);">
          </TD-->        
          <TD  class= title>
            工作单位
          </TD>
          <TD  class= input  >
            <Input class= common name="AppntGrpName" verify="投保人工作单位|len<=60" >
          </TD>
          <TD  class= title>
            办公电话(含分机)
          </TD>
          <TD  class= input>
            <Input class= common name="AppntGrpPhone" verify="投保人单位电话|NUM&len<=30" >
          </TD>
           <TD  class= title>
      	年收入(万元)
   	 			</TD>
    		<TD  class= input>
      	<Input class= 'common' name=AppntSalary >
    		</TD>   
          <!--
          <TD  class= title>
            地址代码
          </TD>
          <TD  class= input>
            <Input class="code" name="AddressNo"  ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this],[0],'', '', '', true);" onkeyup="getaddresscodedata();return showCodeListKeyEx('GetAddressNo',[this],[0],'', '', '', true);">
          </TD>           
          -->
          <Input class= common name="AddressNo" type="hidden" >
        </TR>                
        <TR  class= common>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="CompanyAddress" verify="投保人单位地址|len<=80" >
          </TD> 
          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= common name="AppntGrpZipCode" verify="投保人单位邮政编码|zipcode" >
          </TD>                   
        </TR> 
        <TR  class= common>
          <TD  class= title>
            家庭地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="AppntHomeAddress" verify="投保人家庭地址|len<=80" >
          </TD>
          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= common name="AppntHomeZipCode" verify="投保人家庭邮政编码|zipcode" >
          </TD>
        </TR>                 
        <TR  class= common id="AppntPostalAddressID" style= "display: ''">
        	<TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="AppntPostalAddress" verify="投保人联系地址|len<=80"  readonly=true>
          </TD>
        </TR>
        <TR  class= common>        
          <TD  class= title>
            联系地址
          </TD>
		<TD  class= input colspan=9>
			<Input class="codeno" name=Province verify="省（自治区直辖市）|notnull" ondblclick="return showCodeList('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1);" onblur="return chagePostalAddress();" readonly=true ><input class=codename name=appnt_PostalProvince readonly=true elementtype=nacessary >省（自治区直辖市）
			<Input class="codeno" name=City verify="市|notnull" ondblclick="return showCodeList('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onkeyup="return showCodeListKey('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onblur="return chagePostalAddress();" readonly=true ><input class=codename name=appnt_PostalCity readonly=true elementtype=nacessary>市
			<Input class="codeno" name=County verify="县（区）|notnull" ondblclick="return showCodeList('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onkeyup="return showCodeListKey('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onblur="return chagePostalAddress();" readonly=true ><input class=codename name=appnt_PostalCounty readonly=true elementtype=nacessary>县（区）
			<input  class= common10 name=appnt_PostalStreet verify="乡镇（街道）|notnull" onblur="return chagePostalAddress();" > 乡镇（街道）
	    	<input  class= common10 name=appnt_PostalCommunity verify="村（社区）|notnull" onblur="return chagePostalAddress();"> 村（社区）
	    </TD>
          <!-- <TD  class= input colspan=5>
            <Input class=codeNo name="CheckPostalAddress"  type="hidden"  readonly= true value CodeData="0|^1|单位地址^2|家庭地址^3|其它" verify="联系地址选择|code:CheckPostalAddress" ondblclick="return showCodeListEx('CheckPostalAddress',[this,CheckPostalAddressName],[0,1]);" onkeyup="return showCodeListKeyEx('CheckPostalAddress',[this,CheckPostalAddressName],[0,1]);FillPostalAddress()" onfocus="FillPostalAddress();"><input class=codename name=CheckPostalAddressName readonly=true type="hidden">               
            <input  class= common10 name=appnt_PostalProvince verify="投保人联系地址|notnull"> 省（自治区直辖市）
		    <input  class= common10 name=appnt_PostalCity verify="投保人联系地址|notnull"> 市
		    <input  class= common10 name=appnt_PostalCounty verify="投保人联系地址|notnull"> 县（区）
		    <input  class= common10 name=appnt_PostalStreet verify="投保人联系地址|notnull"> 乡镇（街道）
		    <input  class= common10 name=appnt_PostalCommunity verify="投保人联系地址|notnull"> 村（社区） 
          </TD>    -->
        </TR> 
        <TR  class= common>
<%--          <TD  class= title>--%>
<%--            联系地址--%>
<%--          </TD>--%>
<%--          <TD  class= input colspan=3 >--%>
<%--          </TD>--%>
          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= common name="AppntZipCode" verify="投保人邮政编码|zipcode" >
          </TD>
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
          <input class= common name="AppntPhone"  >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            固定电话
          </TD>
          <TD  class= input>
          <input class= common11 name=AppntHomeCode> - <input class= common10 name=AppntHomeNumber>
          <input class= common name="AppntHomePhone"  type="hidden"  >
          </TD>                
          <TD  class= title>
            移动电话
          </TD>
          <TD  class= input>
            <Input class= common name="AppntMobile" verify="投保人移动电话|NUM&len<=30" >
          </TD> 
          <TD  class= title>
            E-mail
          </TD>
          <TD  class= input>
            <Input class= common name="AppntEMail" verify="投保人电子邮箱|Email&len<=60"  >
          </TD>                    
        </TR>  
     	<TR  class= common>                   
          <TD  class= title>
             折扣方式
          </TD>
          <TD  class= input>
			<Input class="codeno" name=DiscountMode   ondblclick="return showCodeList('DiscountMode',[this,DiscountMode_Name],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('DiscountMode',[this,DiscountMode_Name],[0,1],null,'0','Code1',1);"  readonly=true ><input class=codename name=DiscountMode_Name  readonly=true  >
          </TD>                    
          <TD  class= title id=AppointDiscountID style= "display: 'none'">
            约定折扣
          </TD>
          <TD  class= input id=AppointDiscountID2 style= "display: 'none'">
      			<Input NAME=AppointDiscount VALUE=""  type  CLASS=common onblur="dealDiscount();">
          </TD> 
        
        </TR>   
                 <TR  class= common id=discountID style= "display: ''"> 
              <TD  class= title8>
            年龄折扣因子
          </TD>
          <TD  class= input>
      			<Input NAME=Agediscountfactor VALUE=""  readonly CLASS=common >
         </TD>
                   <TD  class= title8>
            补充医疗保险折扣因子
          </TD>
          <TD  class= input>
      			<Input NAME=Supdiscountfactor VALUE=""   CLASS=common >
         </TD>
                            <TD  class= title8>
           团体投保人数折扣因子
          </TD>
          <TD  class= input>
      			<Input NAME=Grpdiscountfactor VALUE=""   CLASS=common >
         </TD>
                            <TD  class= title8>
            税优总折扣因子
          </TD>
          <TD  class= input>
      			<Input NAME=Totaldiscountfactor VALUE=""  readonly CLASS=common >
         </TD>
                 </TR>
           <tr>
            <TD  class= title>
            被保险人风险保险费档次
          	</TD>
          	<TD  class= input>
      		 <input NAME="PremMult" CLASS=codeNo CodeData="0|^1|未参加补充医疗保险^2|已参加补充医疗保险" ondblclick="return showCodeListEx('AppntPremMult',[this,PremMultName],[0,1]);" onkeyup="return showCodeListKeyEx('PremMult',[this,PremMultName],[0,1]);"><input class=codename name=PremMultName readonly=true >
          	</TD>
           </tr>                 
            </table> 
</DIV>
<DIV id=divFeeButton STYLE="display:''">
<table>
<tr>
<td class= titleImg>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFee);">
缴费资料
</td>
</tr>
</table>
</DIV>        
<Div  id= "divFee" style= "display: ''">  
 <table  class= common>
          <TR CLASS=common>
	    <TD CLASS=title  >
	      缴费频次 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
     <Input NAME=PayIntv VALUE="" CLASS=codeNo CodeData="0|^0|趸缴^1|月缴 ^3|季缴 ^6|半年缴 ^12|年缴" MAXLENGTH=20  verify="缴费频次|notnull&code:PayIntv" ondblclick="return showCodeListEx('PayIntv',[this,PayIntvName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayIntv',[this,PayIntvName],[0,1],null,null,null,1);" ><input class=codename name=PayIntvName readonly=true elementtype=nacessary > 	    </TD>	    <TD CLASS=title  >
	      缴费方式 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PayMode VALUE=""  CLASS=codeNo MAXLENGTH=20  ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);" verify="缴费方式|notnull&code:PayModeInd"><input class=codename name=PayModeName readonly=true elementtype=nacessary> 
	  
	  <TD CLASS=title >续期缴费方式 </TD>
    	    <TD CLASS=input COLSPAN=1 >
    	      
    	    <Input class=codeNo name="ExPayMode"  ondblclick="return showCodeList('ExPayMode',[this,ExPayModeName],[0,1]);" onkeyup="return showCodeListKey('ExPayMode',[this,ExPayModeName],[0,1]);"><input class=codename name=ExPayModeName >
    	    </TD>
	  </TR>    
      <TR CLASS=common>
	    <TD CLASS=title  >
	      开户银行 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=AppntBankCode VALUE="" CLASS=codeNo MAXLENGTH=20  ondblclick="return showCodeList('bankcode',[this,AppntBankCodeName],[0,1],null,fm.PayMode.value,fm.ManageCom.value,1);" onkeyup="return showCodeListKey('bankcode',[this,AppntBankCodeName],[0,1],null,fm.PayMode.value,fm.ManageCom.value,1);"  ><input class=codename name=AppntBankCodeName readonly=true>    
	    </TD>
	    <TD CLASS=title >
	      户名 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=AppntAccName VALUE="" CLASS=common>
	    </TD>
      <TD CLASS=title width="109" >
	      账号
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=AppntBankAccNo class="code" VALUE="" CLASS=common ondblclick="return showCodeList('accint',[this],null,null,fm.AppntNo.value,'CustomerNo');" onkeyup="return showCodeListKey('accint',[this],null,null,fm.AppntNo.value,'CustomerNo');" verify="银行帐号|len<=40" onfocus="getdetail();">
	    </TD>
	  </TR>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>是否需要续期缴费提醒</td>
            <td>
                <input name="DueFeeMsgFlag" class="codeNo" ondblclick="return showCodeList('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);" verify="是否需要续期缴费提醒|notnull&code:duefeemsgflag"><input class="codename" name="DueFeeMsgFlagName" readonly=true elementtype=nacessary />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
      
      </table>      
</DIV>    	  
<Div  id= "divLCAppnt1" style= "display: 'none'">
<table  class= common align='center' >
  <TR  class= common>
<!--    <TD  class= title>
      集体合同号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GrpContNo >
    </TD>
    <TD  class= title>
      合同号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContNo >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      印刷号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PrtNo >
    </TD>
    <TD  class= title>
      投保人客户号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntNo >
    </TD>-->
              
  </TR>
        <TR  class= common>
          <TD  class= title>
            户口所在地
          </TD>
          <TD  class= input>
            <Input class= common name="AppntRgtAddress" verify="投保人户口所在地|len<=80" >
          </TD>
          <TD  class= title>
            民族
          </TD>
          <TD  class= input>
          <input class="code" name="AppntNationality" verify="投保人民族|code:Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">
          </TD>
        </TR>   
  <TR  class= common>

    <TD  class= title>
      投保人级别
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntGrade >
    </TD>
        <TR  class= common>
          <TD  class= title>
            学历
          </TD>
          <TD  class= input>
            <Input class="code" name="AppntDegree" verify="投保人学历|code:Degree" ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">
          </TD>        
          <TD  class= title>
            职业（工种）
          </TD>
          <TD  class= input>
            <Input class= common name="AppntWorkType" verify="投保人职业（工种）|len<=10" >
          </TD>
          <TD  class= title>
            兼职（工种）
          </TD>
          <TD  class= input>
            <Input class= common name="AppntPluralityType" verify="投保人兼职（工种）|len<=10" >
          </TD>       
        </TR>       
        <TR  class= common>        
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input>
            <Input class="code" name="AppntOccupationType1" verify="投保人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>
          <TD  class= title>
            是否吸烟
          </TD>
          <TD  class= input>
            <Input class="code" name="AppntSmokeFlag" verify="投保人是否吸烟|code:YesNo" ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
          </TD>

        </TR> 
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
          <input class= common name="AppntPhone1" verify="投保人联系电话|len<=18" >
          </TD>
          <TD  class= title>
            通讯传真
          </TD>
          <TD  class= input>
            <Input class= common name="AppntFax" verify="传真|len<=15" >
          </TD> 
          <TD  class= title>
            单位传真
          </TD>
          <TD  class= input>
            <Input class= common name="AppntGrpFax" verify="单位传真|len<=18" >
          </TD>  
        
        <TR  class= common>

          <TD  class= title>
            家庭传真
          </TD>
          <TD  class= input>
            <Input class= common name="AppntHomeFax" verify="投保人家庭传真|len<=15" >
          </TD>
        </TR>                                  
<!--     <TD  class= title>
      投保人名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      投保人性别
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntSex >
    </TD>
    <TD  class= title>
      投保人出生日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntBirthday >
    </TD>
  </TR>-->
  <TR  class= common>
    <TD  class= title>
      投保人类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntType >
    </TD>
<!--     <TD  class= title>
      客户地址号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AddressNo >
    </TD>
  </TR>
 <TR  class= common>
    <TD  class= title>
      证件类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDType >
    </TD>
    <TD  class= title>
      证件号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDNo >
    </TD>
  </TR>-->
  <TR  class= common>
    <TD  class= title>
      户口所在地
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RgtAddress >
    </TD>
    <TD  class= title>
      婚姻状况
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Marriage >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      结婚日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MarriageDate >
    </TD>
    <TD  class= title>
      健康状况
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Health >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      身高
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Stature >
    </TD>
    <TD  class= title>
      体重
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Avoirdupois >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      学历
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Degree >
    </TD>
    <TD  class= title>
      信用等级
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CreditGrade >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      银行编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BankCode >
    </TD>
    <TD  class= title>
      银行帐号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BankAccNo >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      银行帐户名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccName >
    </TD>
    <TD  class= title>
      入司日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=JoinCompanyDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      参加工作日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StartWorkDate >
    </TD>
    <TD  class= title>
      职位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Position >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      工资
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Salary >
    </TD>
    <TD  class= title>
      职业类别
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OccupationType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      职业代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OccupationCode >
    </TD>
    <TD  class= title>
      职业（工种）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WorkType >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      兼职（工种）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PluralityType >
    </TD>
    <TD  class= title>
      是否吸烟标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SmokeFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      操作员
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
<!--    <TD  class= title>
      管理机构
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ManageCom >
    </TD>-->
  </TR>
  <TR  class= common>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntMakeDate >
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntMakeTime >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntModifyTime >
    </TD>
  </TR>
</table>
    </Div>