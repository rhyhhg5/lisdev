//程序名称：Underwrite.js
//程序功能：个人人工核保
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保
var pflag = "1";  //保单类型 1.个人单

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  //alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
    showInfo.close();
    alert(content);
    //parent.close();
  }
  else
  { 
	var showStr="操作成功";
  	
  	showInfo.close();
  	alert(showStr);
  	//parent.close();
  	
    //执行下一步操作
  }

}

//提交后操作,服务器数据返回后执行的操作
function afterApply( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {                     
    alert(content);
    
    	// 初始化表格
	initPolGrid();
	divLCPol1.style.display= "";
    	divLCPol2.style.display= "none";
    	divMain.style.display = "none";    
  }
  else
  { 
	var showStr="申请成功";
  	
  	alert(showStr);
  }

}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

      

//保单明细信息
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if (cProposalNo != "")
  {
  	//window.open("../app/ProposalDisplay.jsp?PolNo="+cProposalNo,"window1");
  	mSwitch.deleteVar( "PolNo" );
  	mSwitch.addVar( "PolNo", "", cProposalNo );
  	mSwitch.updateVar("PolNo", "", cProposalNo);
		
  	//window.open("../app/ProposalMain.jsp?LoadFlag=3");]
  	var prtNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
  	window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+prtNo);
  	//showInfo.close();
  }
  else
  {  	
  	alert("请先选择保单!");	
  }

}           




//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	k++;
	var strSQL = "";

		strSQL = "select LCPol.ProposalNo,LCPol.PrtNo,LCPol.RiskCode,LCPol.FloatRate,LCPol.AppntName,LCPol.InsuredName from LCPol where "+k+"="+k				 	
				 + " and LCPol.AppFlag='0' "
				 + " and LCPol.grppolno = '00000000000000000000'"
				 + " and LCPol.contno = '00000000000000000000'"
				 + " and LCPol.ProposalNo = LCPol.MainPolNo "
				 + " and LCPol.uwflag not in ('1','2','4','9','a','0') and LCPol.uwflag is not null "
				 + getWherePart( 'LCPol.ProposalNo','QProposalNo' )
				 + getWherePart( 'LCPol.ManageCom','QManageCom' )
				 + getWherePart( 'LCPol.RiskCode','QRiskCode' )
				 + getWherePart( 'LCPol.PrtNo','PrtNo')

  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的个人单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			}// end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}



function checkBackPol(ProposalNo) {
  var strSql = "select * from LCApplyRecallPol where ProposalNo='" + ProposalNo + "' and InputState='0'";
  var arrResult = easyExecSql(strSql);
  //alert(arrResult);
  
  if (arrResult != null) {
    return false;
  }
  return true;
}


//保险计划变更
var mSwitch = parent.VD.gVSwitch;
function showChangePlan()
{
  var cType = "ChangePlan";
  if(PolGrid.getSelNo()==0) 
  {
  	alert("请先选择保单!"); 
  }
  else
  {
		cProposalNo = PolGrid.getRowColData(PolGrid.getSelNo()-1, 1);
		cPrtNo = PolGrid.getRowColData(PolGrid.getSelNo()-1, 2);
		mSwitch.deleteVar( "PolNo" );
		mSwitch.addVar( "PolNo", "", cProposalNo );
		  
		if(cProposalNo!=""&&cPrtNo!="")  
		{
	      window.open("../app/ProposalEasyScan.jsp?LoadFlag=10&Type="+cType+"&prtNo="+cPrtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
		}
		else
		{
			alert("请先选择保单!"); 
		}  	
  }
  
	
	
	
}


function setproposalno()
{
	var count = PolGrid.getSelNo();
	fm.all('ProposalNo').value = PolGrid.getRowColData(count - 1,1,PolGrid);
}


function showNotePad() {
  var ProposalNo = PolAddGrid.getRowColData(PolAddGrid.getSelNo()-1, 1);
  var PrtNo = PolAddGrid.getRowColData(PolAddGrid.getSelNo()-1, 3);
  
  if (ProposalNo!="" && PrtNo!="") {
  	window.open("./UWNotePadMain.jsp?ProposalNo="+ProposalNo+"&PrtNo="+PrtNo+"&OperatePos=1", "window1");
  }
  else {
  	alert("请先选择保单!");
  }
}