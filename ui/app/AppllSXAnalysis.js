var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var i = 0;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
    var tSignStartDate = fm.SignStartDate.value;
    var tSignEndDate = fm.SignEndDate.value;
	if(dateDiff(tStartDate, tEndDate, "M") > 12)
	{
		alert("统计期最多为12个月！");
		return false;
	}
    var tManageCom = fm.ManageCom.value;
    var tSaleChnl = fm.SaleChnl.value;  
    var tTheContType = fm.TheContType.value;
    var strSQL = "select a.ManageCom, a.PrtNo,a.CardFlag || ' - ' || (case a.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end),a.Prem, b.MakeDate, a.MakeDate, getUniteCode(a.agentcode), "
        + "(select Name from LAAgent where AgentCode = a.AgentCode), c.BranchAttr, c.Name "
        + " , case when a.MakeDate is not null then  to_date(a.MakeDate)-to_date(b.MakeDate) else 0 end "
        + "from LCCont a, ES_Doc_Main b, LABranchGroup c "
        + "where a.ContType='1' and a.ManageCom like '" + tManageCom + "%' " 
        + "and b.MakeDate between '" + tStartDate + "' and '" + tEndDate + "' "
        + "and b.DocCode = a.PrtNo and a.AgentGroup = c.AgentGroup "
        + " and b.busstype='TB' and not exists (select 1 from lbcont where prtno=a.prtno ) "
        + " and not exists (select 1 from lcrnewstatelog where prtno=a.prtno) "
        + getWherePart('a.SaleChnl','SaleChnl');
    if(tSignStartDate!="" && tSignStartDate!=null && tSignStartDate!="null"){
    	strSQL += " and a.signdate>='"+tSignStartDate+"' ";
    }
    if(tSignEndDate!="" && tSignEndDate!=null && tSignEndDate!="null"){
    	strSQL += " and a.signdate<='"+tSignEndDate+"' ";
    }
    if(tTheContType!="" && tTheContType!=null && tTheContType!="null"){
    	if(tTheContType=="0" || tTheContType=="9"){
    		strSQL += " and a.cardflag='"+tTheContType+"' ";
    	}
    	if(tTheContType=="99"){
    		strSQL += " and a.cardflag not in ('0','9') ";
    	}
    }
    strSQL += " order by a.ManageCom, c.BranchAttr, a.AgentCode with ur";
               
    fm.AnalysisSql.value = strSQL; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}  
	fm.submit();
}