<%
//程序名称：ReProposalPrintInput.jsp
//程序功能：
//创建日期：2002-11-25
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%!
	String handleFunction(HttpSession session, HttpServletRequest request) {
	String tContNo = request.getParameter("tContNo");
	String tPrtNo = request.getParameter("tPrtNo");
	String tRePrtReasonCode = request.getParameter("RePrtReasonCode");
	String tRemark = request.getParameter("Remark");
	String tManageCom = request.getParameter("tManageCom");
	String strOperation = request.getParameter("fmtransact");

	GlobalInput globalInput = new GlobalInput();

	if( (GlobalInput)session.getValue("GI") == null )
	{
		return "网页超时或者是没有操作员信息，请重新登录";
	}
	else
	{
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
	}

	LCContSet tLCContSet = new LCContSet();
	ReLCContF1PUI tReLCContF1PUI = new ReLCContF1PUI();
		LCContSchema tLCContSchema = new LCContSchema();
		tLCContSchema.setContNo( tContNo);
		tLCContSet.add(tLCContSchema);
		
	LCContRePrtLogSchema tLCContRePrtLogSchema=new LCContRePrtLogSchema();
	tLCContRePrtLogSchema.setContNo(tContNo);
	tLCContRePrtLogSchema.setPrtNo(tPrtNo);
	tLCContRePrtLogSchema.setContType("1");
	tLCContRePrtLogSchema.setRePrtReasonCode(tRePrtReasonCode);
	tLCContRePrtLogSchema.setRemark(tRemark);
	tLCContRePrtLogSchema.setManageCom(tManageCom);
	


	// Prepare data for submiting
	VData vData = new VData();

	vData.addElement(tLCContSet);
	vData.addElement(tLCContRePrtLogSchema);
	vData.add(globalInput);

	try {
		if( !tReLCContF1PUI.submitData(vData, strOperation) )
		{
	   		if ( tReLCContF1PUI.mErrors.needDealError() )
	   		{
	   			return tReLCContF1PUI.mErrors.getFirstError();
		  	}
		  	else
		  	{
		  		return "保存失败，但是没有详细的原因";
			}
		}

	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		return ex.getMessage();
	}
	return "";
}
%>
<%
String FlagStr = "";
String Content = "";

try {
	Content = handleFunction(session, request);

	if( Content.equals("") ) {
		FlagStr = "Succ";
		Content = "提交申请成功";
	} else {
		FlagStr = "Fail";
	}
} catch (Exception ex) {
	ex.printStackTrace();
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

