
function showContInfo()
{
	if(prtno != "" && conttype != "" && conttype == "1")
	{
		var strSQL = "select lcc.ManageCom,lcc.Prtno,lcc.ContNo,lcc.ContType,lcc.salechnl,lcc.crs_salechnl," 
			+ "lcc.crs_busstype,lcc.grpagentcode,lcc.grpagentname,lcc.grpagentidno,lcc.grpagentcom, "
			+ "(select Name from ldcom where comcode = lcc.ManageCom) ManageComName, " 
			+ "(select CodeName from ldcode where codetype = 'salechnl' and Code = lcc.SaleChnl ) SaleChnlName, "
			+"(select CodeName from ldcode where codetype = 'crs_salechnl' and Code = lcc.crs_SaleChnl ) Crs_SaleChnlName, "
			+"(select CodeName from ldcode where codetype = 'crs_busstype' and Code = lcc.crs_busstype ) busstype "
			+ "from lccont lcc where 1 = 1 "
			+ " and lcc.PrtNo = '" + prtno + "' "
			+ " and lcc.ContType = '" + conttype + "' ";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
			alert("没有查到保单信息！！");
			return false;
		}
		fm.ContTypeName.value='个单';
		fm.ManageCom.value = arrResult[0][0];
		fm.ManageComName.value = arrResult[0][11];
		fm.PrtNo.value = arrResult[0][1];
		fm.ContNo.value = arrResult[0][2];
		fm.ContType.value = arrResult[0][3];
		fm.SaleChnl.value = arrResult[0][4];		
		fm.SaleChnlName.value = arrResult[0][12];		
		fm.CrsSaleChnl.value = arrResult[0][5];
		fm.CrsSaleChnlName.value = arrResult[0][13];
		fm.CrsBussType.value = arrResult[0][6];
		fm.CrsBussTypeName.value = arrResult[0][14];		
		fm.GrpAgentCode.value = arrResult[0][7];
		fm.GrpAgentName.value = arrResult[0][8];
		fm.GrpAgentIdNo.value = arrResult[0][9];
		fm.GrpAgentCom.value = arrResult[0][10];
		fm.mSaleChnl.value = arrResult[0][4];		
		fm.mSaleChnlName.value = arrResult[0][12];
		
	}else if(prtno != "" && conttype != "" && conttype == "2")
	{
		var strSQL = "select lcc.ManageCom,lcc.Prtno,lcc.GRPContNo,lcc.salechnl,lcc.crs_salechnl," 
			+ "lcc.crs_busstype,lcc.grpagentcode,lcc.grpagentname,lcc.grpagentidno,lcc.grpagentcom, "
			+ "(select Name from ldcom where comcode = lcc.ManageCom) ManageComName, " 
			+ "(select CodeName from ldcode where codetype = 'salechnl' and Code = lcc.SaleChnl ) SaleChnlName, "
			+"(select CodeName from ldcode where codetype = 'crs_salechnl' and Code = lcc.crs_SaleChnl ) Crs_SaleChnlName, "
			+"(select CodeName from ldcode where codetype = 'crs_busstype' and Code = lcc.crs_busstype ) busstype "
			+ "from lcgrpcont lcc where 1 = 1 "
			+ " and lcc.PrtNo = '" + prtno + "' ";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
			alert("没有查到保单信息！");
			return false;
		}
		fm.ContTypeName.value='团单';
		fm.ManageCom.value = arrResult[0][0];
		fm.ManageComName.value = arrResult[0][10];
		fm.PrtNo.value = arrResult[0][1];
		fm.ContNo.value = arrResult[0][2];
		fm.ContType.value ='2';
		fm.SaleChnl.value = arrResult[0][3];		
		fm.SaleChnlName.value = arrResult[0][11];		
		fm.CrsSaleChnl.value = arrResult[0][4];
		fm.CrsSaleChnlName.value = arrResult[0][12];
		fm.CrsBussType.value = arrResult[0][5];
		fm.CrsBussTypeName.value = arrResult[0][13];		
		fm.GrpAgentCode.value = arrResult[0][6];
		fm.GrpAgentName.value = arrResult[0][7];
		fm.GrpAgentIdNo.value = arrResult[0][8];
		fm.GrpAgentCom.value = arrResult[0][9];
		fm.mSaleChnl.value = arrResult[0][3];		
		fm.mSaleChnlName.value = arrResult[0][11];
	}
	fm.Action.value = action;
	fm.FinancialAction.value = "None";
}

//修改业务数据
function update()
{
	fm.Action.value = 'modify';
	if(!checkMessages())
	{
		return false;
	}
	
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./CrsnanceSave.jsp";
	fm.submit();
}
function report()
{
	fm.Action.value = "insert";
	if(!checkMessages())
	{
		return false;
	}
	
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "./CrsnanceReport.jsp";
	fm.submit();
	
	
}

//校验录入的数据
function checkMessages()
{
	return true;
}

function afterSubmit( FlagStr, content )
{
	showInfo.close();
	
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		showContInfo();
	}
}

