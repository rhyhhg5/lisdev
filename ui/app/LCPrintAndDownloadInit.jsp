<script language="JavaScript">
function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LDClassInfoInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
    //    setOption("t_sex","0=男&1=女&2=不详");
    //    setOption("sex","0=男&1=女&2=不详");
  }
  catch(ex)
  {
    alert(ex.message);
  }
}
function initForm()
{
  try
  {
  	initPreviewContGrid();
    initInpBox();
    initSelBox();
  }
  catch(re)
  {
    alert(re.message);
  }
}
function initPreviewContGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="40px"; 	           		//列宽
    iArray[0][2]=1;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="MissionID";          		//列名
    iArray[1][1]="60px";      	      		//列宽
    iArray[1][2]=20;            			//列最大值
    iArray[1][3]=3;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[2]=new Array();
    iArray[2][0]="SubMissionID";          		//列名
    iArray[2][1]="60px";      	      		//列宽
    iArray[2][2]=20;            			//列最大值
    iArray[2][3]=3;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[3]=new Array();
    iArray[3][0]="ActivityID";          		//列名
    iArray[3][1]="60px";      	      		//列宽
    iArray[3][2]=20;            			//列最大值
    iArray[3][3]=3;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[4]=new Array();
    iArray[4][0]="合同号码";          		//列名
    iArray[4][1]="60px";      	      		//列宽
    iArray[4][2]=20;            			//列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    //iArray[1][19]=1;

    iArray[5]=new Array();
    iArray[5][0]="印刷号码";         			//列名
    iArray[5][1]="100px";            			//列宽
    iArray[5][2]=20;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="投保人";      	   		//列名
    iArray[6][1]="80px";            			//列宽
    iArray[6][2]=20;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="管理机构";      	   		//列名
    iArray[7][1]="80px";            			//列宽
    iArray[7][2]=20;            			//列最大值
    iArray[7][3]=0;
    
    iArray[8]=new Array();
    iArray[8][0]="生效日期";      	   		//列名
    iArray[8][1]="60px";            			//列宽
    iArray[8][2]=20;            			//列最大值
    iArray[8][3]=0;
    
    iArray[9]=new Array();
    iArray[9][0]="核保师代码";      	   		//列名
    iArray[9][1]="60px";            			//列宽
    iArray[9][2]=20;            			//列最大值
    iArray[9][3]=0;
    
    iArray[10]=new Array();
    iArray[10][0]="核保通过日期";      	   		//列名
    iArray[10][1]="80px";            			//列宽
    iArray[10][2]=20;            			//列最大值
    iArray[10][3]=0;
    
    PreviewContGrid = new MulLineEnter( "fm" , "PreviewContGrid" );
    //这些属性必须在loadMulLine前
    PreviewContGrid.mulLineCount = 10;
    PreviewContGrid.displayTitle = 1;
    PreviewContGrid.canChk=0;
    PreviewContGrid.canSel=1;
    PreviewContGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    PreviewContGrid.hiddenSubtraction=1;

    PreviewContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>
