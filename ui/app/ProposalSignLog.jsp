
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ProposalSignLog.jsp
//程序功能：工单管理工单批注页面
//创建日期：2007-12-20 11:18上午
//创建人  ：Yangyalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!—页面编码方式-->
<!—以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <!—以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <SCRIPT src="ProposalSignLog.js"></SCRIPT>
  <%@include file="ProposalSignLogInit.jsp"%>
  
<title>签单日志处理</title>
</head>
<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form action="ProposalSignLogSave.jsp" method=post name=fm target="fraSubmit">
    <table  class=common align=center>
  		<tr align=right>
  			<td class=button >
  				&nbsp;&nbsp;
  			</td>
  			<td class=button width="10%" align=right>
  				<INPUT class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="saveLog();">
  			</td>
  			<td class=button width="10%" align=right>
  				<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="updateLog();">
  			</td>			
  			<td class=button width="10%" align=right>
  				<INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="queryLog();">
  			</td>			
  			<td class=button width="10%" align=right>
  				<INPUT class=cssButton name="deleteButton" VALUE="删  除"  TYPE=button onclick="deleteLog();">
  			</td>
  		</tr>
  	</table>
    <table class= common align=center>
      <tr class= common>
        <td class= title>合同号</td>
        <td class= input>
          <input class=common verify="合同号|notnull" name=ContNo elementtype="nacessary">
        </td>
        <td class= title>保单类型</td>
        <td class= input>
          <Input class=codeNo name="ContType" verify="保单类型|notnull" readonly=true CodeData="0|^1|个单^2|团单" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);" ><input class=codename name=ContTypeName readonly=true  elementtype="nacessary">
        </td>
      </tr>
      <tr class= common>
        <td class= title>错误信息</td>
        <td class= input>
          <input class=common verify="错误信息|notnull" name=ErrInfo elementtype="nacessary">
        </td>
      </tr>
      <tr class= common>
        <td class=button><input class=cssButton  VALUE="重  置"  TYPE=button onclick="initInpBox();"></td>
      </tr>
    </table>
    <table>
      <tr>
        <td class=common>
          <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSignLog);">
        </td>
  	    <td class=titleImg>查询结果</td>
  	  </tr>
    </table>
    <!-- 信息（列表） -->
    <div id="divSignLog" style="display:''">
  	<table class=common>
      <tr class=common>
  	    <td text-align:left colSpan=1>
  	      <span id="spanSignLogGrid">
  	      </span>
  	    </td>
  	  </tr>
  	</table>
    </div>
  
    <div id="divPage2" align=center style="display: 'none' ">
    	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
    	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
    	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
    	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
    </div>
    
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="SeriNoID" name="SeriNo">
  </form>
  <!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
