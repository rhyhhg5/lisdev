//程序名称：ProposalConfirmNo.js
//程序功能： 
//创建日期：2007-11-15
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}

function beforeSubmit()
{
	return true;
}

//提交，保存按钮对应操作
function submitForm()
{
  if(!verifyInput2())
  {
    return false;
  }
  if(!checkPrtNo())
  {
    alert("此印刷号已存在！");
    return false;
  }
  if (beforeSubmit())
  {
    fm.fmtransact.value = "INSERT||MAIN" ;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.action = "ProposalConfirmNoSave.jsp";
    fm.submit(); //提交
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, transact)
{

  easyQueryClick();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  
}

function getAppFlag()
{
  var sql = "select AppFlag from LCcont where PrtNo = '" + fm.PrtNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    return rs[0][0];
  }
  
  return "";
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if(!verifyInput2())
  {
    return false;
  }
  if(checkPrtNo())
  {
    alert("此印刷号不存在！");
    return false;
  }
  
  if(getAppFlag() != 0)
  {
    alert("保单不是投保单，不能执行本操作");
    return false;
  }
	
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {	
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "UPDATE||MAIN";
    fm.action = "ProposalConfirmNoSave.jsp";
    fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}   
function easyQueryClick() 
{ 	
  var strSql = "select PrtNo, ConfirmNo, ConfirmOperator, ConfirmName, ConfirmDate, ConfirmTime, Remark "
	         + "from LCUWConfirmInfo "
	         + "where 1 = 1 "
	         + getWherePart('PrtNo','PrtNo','=')
			 + getWherePart('ConfirmNo','ConfirmNo','=')
		//	 + getWherePart('ConfirmOperator', 'ConfirmOperator','=')
		//	 + getWherePart('ConfirmName', 'ConfirmName','=')
		//	 + getWherePart('ConfirmDate', 'ConfirmDate','=')
		//	 + getWherePart('ConfirmTime', 'ConfirmTime','=')
		//	 + getWherePart('Remark', 'Remark','like')
			 + " order by PrtNo ";
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(strSql, LCUWConfirmInfoGrid);
  return true;
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if(fm.PrtNo.value == null || fm.PrtNo.value.trim() == "")
  {
    alert("印刷号不能为空！");
    return false;
  }
  if(checkPrtNo())
  {
    alert("此印刷号不存在！");
    return false;
  }
  
  if(getAppFlag() != 0)
  {
    alert("保单不是投保单，不能执行本操作");
    return false;
  }
  
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "DELETE||MAIN";
    fm.action = "ProposalConfirmNoSave.jsp";
    fm.submit(); //提交
    initInpBox();
  }
  else
  {
    alert("您取消了删除操作！");
  }
} 

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  if(!easyQueryClick())
  {
  return false;
  }
  if(LCUWConfirmInfoGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }
}

function selectOne() 
{
  var arrReturn = new Array();
  arrReturn = getQueryResult();
  afterQuery(arrReturn);	          		  
}

function getQueryResult()
{
  var arrSelected = null;
  tRow = LCUWConfirmInfoGrid.getSelNo();
  if( tRow == 0 || tRow == null )
  {
    return arrSelected;
  }
  arrSelected = new Array();
  //设置需要返回的数组
  arrSelected[0] = new Array();
  arrSelected[0] = LCUWConfirmInfoGrid.getRowData(tRow-1);
  return arrSelected;
}

function afterQuery( arrQueryResult )
{
  var arrResult = new Array();	
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.PrtNo.value = arrResult[0][0];
    fm.ConfirmNo.value = arrResult[0][1];
    fm.ConfirmOperator.value = arrResult[0][2];
    fm.ConfirmName.value = arrResult[0][3];
    fm.ConfirmDate.value = arrResult[0][4];
    fm.ConfirmTime.value = arrResult[0][5];
    fm.Remark.value = arrResult[0][6]; 
    showAllCodeName();
  }
}

//检查印刷号是否存在
function checkPrtNo()
{
  var strSQL = "select count(*) from LCUWConfirmInfo where Prtno ='" + fm.PrtNo.value  + "'";
  if(easyExecSql(strSQL) == 0)
  {
    return true;
  }
  
  return false;
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
	cDiv.style.display="";
  }
  else
  {
	cDiv.style.display="none";  
  }
}
