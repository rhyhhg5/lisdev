//程序名称：ProposalSignLog.js
//程序功能：万能险帐户结算利率录入
//创建日期：2007-12-20 03:33下午
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;

//输入框的初始化 
function initInpBox()
{
  try
  {
	  fm.all('ContNo').value = "";
    fm.all('ContType').value = "";
    fm.all('ErrInfo').value = "";
  }
  catch(ex)
  {
    alert("在ProposalSignLog.js-->InitInpBox函数中发生异常:初始化界面错误!" + ex.message);
  }
}

//提交，保存按钮对应操作
function saveLog()
{
  if(!verifyInput2())
  {
    return false;
  }
  
  if (confirm("您确实想保存该记录吗?"))
  {	
    fm.fmtransact.value = "INSERT";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.action = "ProposalSignLogSave.jsp";
    fm.submit(); //提交
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    queryLog();
  }
  
}

//Click事件，当点击“修改”图片时触发该函数
function updateLog()
{
  if(!verifyInput2())
  {
    return false;
  }
  
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {	
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "UPDATE";
    fm.action = "ProposalSignLogSave.jsp";
    fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}   

//删除日志
function deleteLog()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 //   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "DELETE";
    fm.action = "ProposalSignLogSave.jsp";
    fm.submit(); //提交
    initInpBox();
  }
  else
  {
    alert("您取消了删除操作！");
  }
} 

//查询日志
function queryLog() 
{
  var sql = "select SeriNo, ContNo, ContType, ErrInfo "
          + "from LCSignLog "
          + "where 1 = 1 "
          + getWherePart("ContNo")
          + getWherePart("ContType")
          + getWherePart("ErrInfo");
  
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(sql, SignLogGrid);
  
  if(SignLogGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }
  
  return true;
}

//选择一条日志时触发的动作
function selectOne() 
{
  var row = SignLogGrid.getSelNo();
  fm.SeriNo.value = SignLogGrid.getRowColDataByName(row - 1, "SeriNo");
  fm.all('ContNo').value = SignLogGrid.getRowColDataByName(row - 1, "ContNo");
  fm.all('ContType').value = SignLogGrid.getRowColDataByName(row - 1, "ContType");
  fm.all('ErrInfo').value = SignLogGrid.getRowColDataByName(row - 1, "ErrInfo");
}