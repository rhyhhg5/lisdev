<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2011-02-10
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="GrpCustomerQuery.js"></script>
    <%@include file="GrpCustomerQueryInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="GrpCustomerQuerySave.jsp" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">请输入查询条件：</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
                    </td>
                    <td class="title8">&nbsp;</td>
                    <td class="input8">&nbsp;</td>
                </tr>
                <tr class="common">
                    <td class="title8">客户号</td>
                    <td class="input8">
                        <input class="common" name="CustomerNo" />
                    </td>
                    <td class="title8">单位名称</td>
                    <td class="input8">
                        <input class="common" name="GrpName" verify="单位名称|notnull" elementtype="nacessary" />
                    </td>
                    <td class="title8">组织机构代码</td>
                    <td class="input8">
                        <input class="common" name="OrganComCode" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title8">是否关联交易方</td>
                    <td class="input8">
                        <input class="codeNo" name="TradeRelaFlag" CodeData="0|^0|否^1|是" ondblclick="return showCodeListEx('TradeRelaFlag',[this,TradeRelaFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('TradeRelaFlag',[this,TradeRelaFlagName],[0,1],null,null,null,1);"/><input class="codename" name="TradeRelaFlagName" readonly="readonly" />
                    </td>
                    <td class="title8">起始日期</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="StartDate" verify="起始日期|date" />
                    </td>
                    <td class="title8">截止日期</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="EndDate" verify="截止日期|date" />
                    </td>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="qryGrpCustomerList();" />
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnInput" value="下载清单" onclick="downloadList();" />
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divGrpCustomerListGrid);" />
                </td>
                <td class="titleImg">客户清单</td>
            </tr>
        </table>
        <div id="divGrpCustomerListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanGrpCustomerListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divGrpCustomerListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnInput" value="确认关联" onclick="submitConfirm();" />
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnInput" value="取消关联" onclick="calCel();" />
                </td>
            </tr>
        </table>
        
        <input type="hidden" class="common" name="ActivityID" value="" />
        <input type=hidden id="fmtransact" name="fmtransact">
        <input type="hidden" class="common" name="querySql" >
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
