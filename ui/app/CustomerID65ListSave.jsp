<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2009-07-29
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  String querySql = request.getParameter("querySql");
  String tCustomerAction = request.getParameter("CustomerAction");
  
  querySql = querySql.replaceAll("%25","%");
  String[][] cTitle=null;
  int []cDisplayTitle = null;
  int []cDisplayData = null;
  String downLoadFileName = null;
  if(tCustomerAction.equals("doAppnt")){
	  //设置表头
	  String[][] tTitle = {{"管理机构","管理机构名称","险种编码","险种名称","保费金额","签单日期","保单号","投保人姓名","投保人身份证号","缴费账户户名","缴费账户号"}};
	  //表头的显示属性
	  int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11};
	  //数据的显示属性
	  int []displayData= {1,2,3,4,5,6,7,8,9,10,11};
	  cTitle = tTitle;
	  cDisplayTitle = displayTitle;
	  cDisplayData = displayData;
	  downLoadFileName = "清单_投保人.xls";
  }else if(tCustomerAction.equals("doInsured")){
	  //设置表头
	  String[][] tTitle = {{"管理机构","管理机构名称","险种编码","险种名称","保费金额","签单日期","保单号","被保人姓名","被保人身份证号"}};
	  //表头的显示属性
	  int []displayTitle = {1,2,3,4,5,6,7,8,9};
	  //数据的显示属性
	  int []displayData = {1,2,3,4,5,6,7,8,9};
	  cTitle = tTitle;
	  cDisplayTitle = displayTitle;
	  cDisplayData = displayData;
	  downLoadFileName = "清单_被保人.xls";
  }else{
	  //设置表头
	 String[][] tTitle = {{"管理机构","管理机构名称","险种编码","险种名称","保费金额","签单日期","保单号","受益人姓名","受益人身份证号"}};
	  //表头的显示属性
	  int []displayTitle = {1,2,3,4,5,6,7,8,9};
	  //数据的显示属性
	  int []displayData = {1,2,3,4,5,6,7,8,9};
	  cTitle = tTitle;
	  cDisplayTitle = displayTitle;
	  cDisplayData = displayData;
	  downLoadFileName = "清单_受益人.xls";
  }
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);

  //生成文件
  CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
  createexcellist.createExcelFile();
  String[] sheetName ={"list"};
  createexcellist.addSheet(sheetName);
  int row = createexcellist.setData(cTitle,cDisplayTitle);
  
  if(row ==-1) errorFlag = true;
  createexcellist.setRowColOffset(row+1,0);//设置偏移
  if(createexcellist.setData(querySql,cDisplayData)==-1)
  {
  	errorFlag = true;
  	System.out.println(errorFlag);
  }
  if(!errorFlag)
  //写文件到磁盘
  try{
     createexcellist.write(tOutXmlPath);
  }catch(Exception e)
  {
  	errorFlag = true;
  	System.out.println(e);
  }
  //返回客户端
  if(!errorFlag)
  	downLoadFile(response,filePath,downLoadFileName);
  out.clear();
	out = pageContext.pushBody();
%>

