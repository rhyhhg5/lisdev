//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;
var manageCom;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  // showSubmitFrame(mDebug);
  initContGrid();

  fm.submit(); //提交
}

//提交，保存按钮对应操作
//function printPol()
//{
//	var i = 0;
//	flag = 0;
//	//判定是否有选择打印数据
//
//	//如果没有打印数据，提示用户选择
//	
//	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//	//disable打印按钮，防止用户重复提交
//	//fm.all("printButton").disabled=true;
//	
//	  var tContNo = "";	  
//	  var selno = ContGrid.getSelNo()-1;
//	  var tContNo="";
//	  if (selno >=0)
//	  {
//	    tContNo = ContGrid.getRowColData( selno, 1);
//	  }
//	  
//	  if(tContNo==""){
//	  	alert("请选择一行数据！");
//	  	return false;
//	  }
//	  var tIntlFlag="2";
//	  fm.action = "../briefapp/OmnipotenceBriefSingleContPrintSave.jsp?mContNo=" + tContNo +"&IntlFlag=" +tIntlFlag;
//	  fm.submit();
//	
//}

//提交，保存按钮对应操作
function printPol()
{
	var i = 0;
	flag = 0;
	//判定是否有选择打印数据

	//如果没有打印数据，提示用户选择
	
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	//fm.all("printButton").disabled=true;
	
	var tCheckCount = 0;
	for (i = 0; i < ContGrid.mulLineCount; i++)
	{
		if (ContGrid.getChkNo(i)) 
		{
		  tCheckCount = tCheckCount + 1;
		}
	}
	
	if(tCheckCount == 0)
	{
	  alert("请选择需要打印的保单");
	  return false;
	}
	
	var showStr="正在打印保单，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
    var tIntlFlag="3";
    fm.action = "../briefapp/OmnipotenceBriefSingleContPrintSave.jsp?IntlFlag=" +tIntlFlag;
	
	//查询保单，如保单是团体电话销售，则按简易险打印
	var prtNo = ContGrid.getRowColData(tCheckCount - 1, 2);
	var sql = "select distinct b.RiskWrapType from LCRiskDutyWrap a, LDRiskWrap b where a.RiskWrapCode = b.RiskWrapCode and a.PrtNo = '" 
	        + prtNo + "' and b.RiskWrapType = 'G' "
	        + " and exists (select 1 from LCCont c where ContType = '1' and CardFlag = '5' and c.PrtNo = '"+ prtNo + "')";
	var arr = easyExecSql(sql);
    if(arr)
    {
        fm.action = "../briefapp/BriefContPhonePrintSave.jsp";
    }
    else
    {
        fm.action = "../briefapp/OmnipotenceBriefSingleContPrintSave.jsp?IntlFlag=" +tIntlFlag;
    }
    fm.submit();
}


function printPol1()
{
	var i = 0;
	flag = 0;
	//判定是否有选择打印数据

	//如果没有打印数据，提示用户选择
	
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	//fm.all("printButton").disabled=true;
	
	  var tContNo = "";	  
	  var selno = ContGrid.getSelNo()-1;
	  var tContNo="";
	  if (selno >=0)
	  {
	    tContNo = ContGrid.getRowColData( selno, 1);
	  }
	  
	  if(tContNo==""){
	  	alert("请选择一行数据！");
	  	return false;
	  }
	  var tIntlFlag="3";
	  fm.action = "../briefapp/OmnipotenceBriefSingleContPrintSave.jsp?mContNo=" + tContNo +"&IntlFlag=" +tIntlFlag;
	  fm.submit();
	
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null || arrDataSet == null )
	//{alert("111");
	return arrSelected;
	//}
	//alert("222");
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//无论打印结果如何，都重新激活打印按钮
    //window.focus();
    try
    {
	    showInfo.close();
	}
	catch(ex)
	{}
	
	fm.all("printButton").disabled=false;
	if (FlagStr == "Fail" )
	{
		//如果打印失败，展现错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initContGrid();
	if(fm.PrtNo.value != "" || fm.ContNo.value != "")
    {
        fm.startSignDate.value = "";
        fm.endSignDate.value = "";
    }
    if(fm.PrtNo.value == "" && fm.ContNo.value == "" && fm.startSignDate.value == "")
    {
        alert("印刷号、保单号和签单起始日期至少录入一个！");
        return false;
    }
	
	var strManageComWhere = " ";
	if( fm.BranchGroup.value != '' )
	{
		strManageComWhere += " AND EXISTS" + " ( SELECT AgentGroup FROM LABranchGroup WHERE AgentGroup=A.AgentGroup and BranchAttr LIKE '" + fm.BranchGroup.value + "%%') ";
	}
	
	var tPrintCountCondition = " ";
	if(fm.PrintState.value == "0")
	{
	  tPrintCountCondition = " and ( PrintCount < 1 OR PrintCount IS NULL ) ";
	}
	if(fm.PrintState.value == "1")
	{
	  tPrintCountCondition = " and ( PrintCount > 0) ";
	}

	//去掉了几个查询的筛选条件，有待以后考察是否需要保留
	var strSQL = "";
	strSQL = "select ContNo,PrtNo,Prem,AppntName,CValiDate,PrintCount, case when PrintCount > 0 then '已打印' else '未打印' end, '打印','1' "
        + "from LCCont where ContType = '1' and cardflag='" + cardFlag + "'"
        //+ " and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+comcode+"','Pj')=1 "
		+ " AND AppFlag in ('1') "
		+ tPrintCountCondition
		+ getWherePart('ContNo')
		+ getWherePart('PrtNo')
		+ getWherePart('getUniteCode(AgentCode)','AgentCode')
		+ getWherePart('SaleChnl')
		+ getWherePart('AgentCom')
		+ getWherePart('SignDate', 'startSignDate', '>=')
		+ getWherePart('SignDate', 'endSignDate', '<=')
		+ getWherePart('ManageCom', 'ManageCom', 'like')
		+ strManageComWhere
		+ " and ManageCom like '" + comcode + "%%'"
		+ " order by ManageCom,AgentGroup,AgentCode "
		+ "with ur ";

	turnPage.pageLineNum = 50;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}

	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	turnPage.pageLineNum = 30 ;
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = ContGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL ;
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	
	fm.querySql.value = strSQL;
}

function queryBranch()
{
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
	if(arrResult!=null)
	{
		fm.BranchGroup.value = arrResult[0][3];
	}
}

//下载客户卡
function downloadCustomerCard()
{
  var i = 0;
	flag = 0;
	//判定是否有选择打印数据

	//如果没有打印数据，提示用户选择
	
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	//var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//disable打印按钮，防止用户重复提交
	//fm.all("printButton").disabled=true;
	
	  var tContNo = "";	  
	  var selno = ContGrid.getSelNo()-1;
	  var tContNo="";
	  if (selno >=0)
	  {
	    tContNo = ContGrid.getRowColData( selno, 1);
	  }
	  
	  if(tContNo==""){
	  	alert("请选择一行数据！");
	  	return false;
	  }
  fm.action = "./ES_DOC_WNMAINQueryDownLoad.jsp?mContNo=" + tContNo;
  fm.submit();
}

//双击事件后的操作
function afterCodeSelect(codeName, field)
{
    if (codeName == "salechnlall" || codeName == "LCSaleChnl")
    {
        if("03" == fm.SaleChnl.value)
        {
            fm.AgentComBank.value = "";
            agentComCode = "agentcombrief";
            agentComName = "ManageCom";
            agentComValue = fm.ManageCom.value;
            fm.AgentComBank.style.display = "";
        }
        else if("04" == fm.SaleChnl.value)
        {
            fm.AgentComBank.value = "";
            agentComCode = "agentcombank";
            agentComName = "ManageCom";
            agentComValue = fm.ManageCom.value;
            fm.AgentComBank.style.display = "";
        }
        else if("11" == fm.SaleChnl.value || "12" == fm.SaleChnl.value)
        {
            fm.AgentComBank.value = "";
            agentComCode = "agentcominput";
            agentComName = "branchtype";
            agentComValue = "2^04^" + fm.ManageCom.value;
            fm.AgentComBank.style.display = "";
        }
        else
        {
            fm.AgentComBank.value = "";
            fm.AgentComBank.style.display = "none";
        }
    }
}

//下载清单
function download()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && ContGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "OmnipotenceProposalPrintDownload.jsp";
        fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}