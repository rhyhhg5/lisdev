var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var showInfo;
var contype;//查询判断条件，如果是1表示个人2表示团体

window.onfocus = myonfocus;

function easyQueryClick(){
	
	initPolGrid();
	fm.AfterUpdateRemark.value = "";
	
	if(fm.ManageCom.value==null || fm.ManageCom.value=="" || fm.ManageCom.value=="null"){
		alert("请录入管理机构");
		return false；
	}
	if((fm.PrtNo.value==null || fm.PrtNo.value=="" || fm.PrtNo.value=="null") && (fm.GrpContNo.value==null || fm.GrpContNo.value=="" || fm.GrpContNo.value=="null")){
		alert("合同号和印刷号不能同时为空");
		return false;
	}
	if(fm.ContType.value==null || fm.ContType.value=="" || fm.ContType.value=="null"){
		alert("个团标识不能为空");
		return false;
	}
	
	contype = fm.ContType.value;//给contype赋值

	if(contype==2){	
		//contype = ‘2’ 团体
		var strSql = "select lgc.managecom,lgc.grpcontno,lgc.prtno,lgc.cvalidate,lgc.remark,"
		+ contype
		+ " from lcgrpcont lgc " 
		+ "where appFlag = '1' "
		+ getWherePart('lgc.managecom','ManageCom','like')
		+ getWherePart('lgc.prtno','PrtNo')
		+ getWherePart('lgc.grpcontno','GrpContNo')
		+ getWherePart('lgc.cvalidate','CValidateStart','>=')
		+ getWherePart('lgc.cvalidate','CValidateEnd','<=')
		+ " with ur";
		
		}
	  else if(contype==1){
			//conttype = '1' 个人
			var strSql = "select lcc.managecom,lcc.contno,lcc.prtno,lcc.cvalidate,lcc.remark," 
				+ contype
				+ " from lccont lcc " 
				+ "where appFlag = '1' and conttype = '1' "
				+ getWherePart('lcc.managecom','ManageCom','like')
				+ getWherePart('lcc.prtno','PrtNo')
				+ getWherePart('lcc.contno','GrpContNo')
				+ getWherePart('lcc.cvalidate','CValidateStart','>=')
				+ getWherePart('lcc.cvalidate','CValidateEnd','<=')
				+ " with ur";
	    }

	
	turnPage1.pageLineNum = 50;
    turnPage1.strQueryResult  = easyQueryVer3(strSql);
	if (!turnPage1.strQueryResult) 
    {
        alert("没有查询到相关数据！");
        return false;
    }

	turnPage1.queryModal(strSql, PolGrid);
	
}

//添加一个标识action参数，如果是save表示保存update表示更新delete表示删除
function updateAppoint(){
	
	var checkFlag = 0;
	var remark  = fm.AfterUpdateRemark.value;
	
	for(var i=0;i<PolGrid.mulLineCount;i++){
		if(PolGrid.getSelNo(i))
		{
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	if(checkFlag == 0){
		alert("请先选择一条保单信息！");
		return false;
	}else{
		if(remark==null || remark=="" || remark=="null"){
			if(confirm("没有输入修改后的特别约定，是否继续？")){
				var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
				var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
				showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
				fm.action="./UpdateRemarkSave.jsp?action=update";
				fm.submit();
			}else{
				return false;
			}
		}else{
			var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
			var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
			showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.action="./UpdateRemarkSave.jsp?action=update";
			fm.submit();
		}
	}
	
}

function afterSubmit(FlagStr,content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
	
}

//单击单选按钮触发的事件
function returnTableField(){
	var i = 0;
	for (i = 0; i < PolGrid.mulLineCount; i++) {
		if (PolGrid.getSelNo(i)) {
			//取出表字段
			var code = PolGrid.getRowColData(
					PolGrid.getSelNo(i) - 1, 5);
			break;
		}
	}
	if(code!=null && code!=""){
		//赋值编码之对应名字
		fm.all("AfterUpdateRemark").value = code;
	}

}
