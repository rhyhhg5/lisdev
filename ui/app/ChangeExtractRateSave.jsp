<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.ulitb.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>

<%	
	LCRiskZTFeeSet  tLCRiskZTFeeSet =  new LCRiskZTFeeSet();
    TransferData tTransferData = new TransferData(); 
	//输出参数
	String FlagStr = "Fail";
	String Content = "";
	//全局变量
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");

	System.out.println("*********begin****************");
	String tGrpPolNo = request.getParameter("GrpPolNo");
	System.out.println("GrpPolNo=" + tGrpPolNo);
	System.out.println("**********End****************");
	
	
	//取得险种退保费率明细
	int lineCount = 0;
	String arrCount[] = request.getParameterValues("ChangeExtractRateGridNo");
	if(arrCount != null)
	{
		String[] strBeginPolYear = request.getParameterValues("ChangeExtractRateGrid1");//提取投保年限
		String[] strEndPolYear = request.getParameterValues("ChangeExtractRateGrid2");//提取投保年限
		String[] strExtractRate = request.getParameterValues("ChangeExtractRateGrid3");//提取投保费率	
		lineCount = arrCount.length;	

		for(int i=0;i<lineCount;i++)
		{
			LCRiskZTFeeSchema  tLCRiskZTFeeSchema = new LCRiskZTFeeSchema();	
			tLCRiskZTFeeSchema.setGrpPolNo(tGrpPolNo);
			tLCRiskZTFeeSchema.setBeginPolYear(strBeginPolYear[i]);
			tLCRiskZTFeeSchema.setEndPolYear(strEndPolYear[i]);			
			tLCRiskZTFeeSchema.setExtractRate(strExtractRate[i]);	
			
			tLCRiskZTFeeSet.add(tLCRiskZTFeeSchema);
			System.out.println("记录"+i+"放入Set！");
		} 
		System.out.println("****************end ...***************");
   
	}
	
    tTransferData.setNameAndValue("grppolno",tGrpPolNo);
//	 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";

	tVData.add(tG);
	tVData.addElement(tLCRiskZTFeeSet);
	tVData.add(tTransferData);
	
	
	LCRiskZTFeeUI tLCRiskZTFeeUI = new LCRiskZTFeeUI();
	if( tLCRiskZTFeeUI.submitData( tVData, "" ) == false )                       
	{                                                                               
		Content = " 保存失败，原因是: " + tLCRiskZTFeeUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	System.out.println("Content=" + Content);
	System.out.println("FlagStr=" + FlagStr);
%>

<html>
<script language="javascript">
	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	
</script>
</html>
