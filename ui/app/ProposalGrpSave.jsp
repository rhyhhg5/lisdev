<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ProposalSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%


//输出参数
CErrors tError = null;
String FlagStr = "Fail";
String Content = "";
String tAction = "";
String tOldAction = "";
String tOperate = "";

	
	LKGrpPolSchema tLKgrppolSchema = new LKGrpPolSchema();
	GlobalInput tG = new GlobalInput();
	tG = ( GlobalInput )session.getValue( "GI" );
	  
    String dutycode = request.getParameter("DutyCode1");
    String riskCode = request.getParameter("RCode");
    String avgAge = request.getParameter("Age");
    String occuType =  request.getParameter("Professional");
    String amnt =  request.getParameter("Amnt");
    String peopleCount =  request.getParameter("People");
    String prem = request.getParameter("InputPrem");
    String prtno = request.getParameter("prtNo");
    tOperate = request.getParameter("operate");
    System.out.println("dutycode="+dutycode+",riskCode="+riskCode+",avgAge="+avgAge+",occuType="+occuType+",amnt="+amnt+",peopleCount="+peopleCount+",prem="+prem+"prtno="+prtno);
    tLKgrppolSchema.setPrtNo(prtno);
    tLKgrppolSchema.setDutyCode(dutycode);
    tLKgrppolSchema.setRiskCode(riskCode);
    tLKgrppolSchema.setAvgAge(avgAge);
    tLKgrppolSchema.setOccupationType(occuType);
    tLKgrppolSchema.setAmnt(amnt);
    tLKgrppolSchema.setPeoples(peopleCount);
    tLKgrppolSchema.setPrem(prem);
    System.out.println("tOperate="+tOperate);
	VData  tVData = new VData();
    // 准备传输数据 VData
    tVData.addElement(tLKgrppolSchema);
    tVData.addElement(tG);
   // tVData.addElement(tTransferData);
   ProposalGrpUI tProposalGrpUI = new ProposalGrpUI();
   if(!tProposalGrpUI.submitData(tVData, tOperate)){
	   Content = "操作失败，原因是: " + tProposalGrpUI.mErrors.getError(0).errorMessage;
       FlagStr = "Fail"; 
   }else{
	   FlagStr = "Succ";
	   Content = "保存成功!";
   }
   System.out.println("Content="+Content+",FlagStr="+FlagStr);
%>
<html>
<script language="javascript">
try {
  parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
}  catch(ex) {
  //alert("after Save but happen err:" + ex);
}
</script>

</html>

