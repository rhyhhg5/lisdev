<%
//程序名称：ProposalCopyInput.jsp
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%@include file="../common/jsp/UsrCheck.jsp"%>

<SCRIPT src="../common/javascript/Common.js">
</SCRIPT>

<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>                            

<script language="JavaScript">

function initInpBox()
{ 
  try
  {                                   
	fmlog.all('FileName').value = '';
	fm.all('PrtNo').value = '';
  }
  catch(ex)
  {
    alert("在LCDiskApplyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {

  }
  catch(ex)
  {
    alert("在LCDiskApplyInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        


function initDiskErrQueryGrid()
{
    var iArray = new Array();
      
    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）             
      iArray[0][1]="30px";         		//列宽                                                     
      iArray[0][2]=10;          		//列最大值                                                 
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[1]=new Array();                                                                                       
      iArray[1][0]="印刷号";    		//列名                                                     
      iArray[1][1]="180px";            		//列宽                                                     
      iArray[1][2]=100;            		//列最大值                                                 
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许5                    
                                                                                                                   
      iArray[2]=new Array();                                                                                       
      iArray[2][0]="合同ID";         		//列名                                                     
      iArray[2][1]="100px";            		//列宽                                                     
      iArray[2][2]=100;            		//列最大值                                                 
      iArray[2][3]=3;              		//是否允许输入,1表示允许，0表示不允许 0                     

      iArray[3]=new Array();                                                                                       
      iArray[3][0]="被保人ID";         		//列名                                                     
      iArray[3][1]="100px";            		//列宽                                                     
      iArray[3][2]=100;            		//列最大值                                                 
      iArray[3][3]=3;             		//是否允许输入,1表示允许，0表示不允许1                    
         
      iArray[4]=new Array();                                                                                       
      iArray[4][0]="被保人";         		//列名                                                     
      iArray[4][1]="100px";            		//列宽                                                     
      iArray[4][2]=100;            		//列最大值                                                 
      iArray[4][3]=3;             		//是否允许输入,1表示允许，0表示不允许1                    
                                                                                                                          
      iArray[5]=new Array();                                                                                       
      iArray[5][0]="具体描述";         		//列名                                                     
      iArray[5][1]="570	px";            		//列宽                                                     
      iArray[5][2]=100;            		//列最大值                                                 
      iArray[5][3]=0;              		//是否允许输入,1表示允许，0表示不允许,2表示代码引用    
     
                                                                                                              
      DiskErrQueryGrid = new MulLineEnter( "fmquery" , "DiskErrQueryGrid" ); 
      //这些属性必须在loadMulLine前
	DiskErrQueryGrid.mulLineCount = 5;   
	DiskErrQueryGrid.displayTitle = 1;
	DiskErrQueryGrid.hiddenPlus = 1;
	DiskErrQueryGrid.hiddenSubtraction = 1;
	DiskErrQueryGrid.loadMulLine(iArray);  

      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 保单信息列表的初始化
function initGrpGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="合同ID";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="印刷号";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="投保人";         		//列名
      iArray[4][1]="120px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();                                                  
			iArray[5][0]="业务员";         		//列名                               
			iArray[5][1]="120px";            		//列宽                             
			iArray[5][2]=200;            			//列最大值                           
			iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
    	iArray[6]=new Array();
    	iArray[6][0]="管理机构";         		//列名
    	iArray[6][1]="120px";            		//列宽
    	iArray[6][2]=100;            			//列最大值
    	iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="导入日期";         		//列名
      iArray[7][1]="120px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[8]=new Array();
      iArray[8][0]="操作员";         		//列名
      iArray[8][1]="120px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[9]=new Array();
      iArray[9][0]="工作流任务号";         		//列名
      iArray[9][1]="0px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[10]=new Array();
      iArray[10][0]="工作流子任务号";         		//列名
      iArray[10][1]="0px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[11]=new Array();
      iArray[11][0]="合同号";         		//列名
      iArray[11][1]="0px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[12]=new Array();
      iArray[12][0]="投保人客户号";         		//列名
      iArray[12][1]="0px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=3	;              			//是否允许输入,1表示允许，0表示不允许 

      
      
      
        
                                    
                                   
                                   
      GrpGrid = new MulLineEnter( "fm" , "GrpGrid" ); 
      //这些属性必须在loadMulLine前
      GrpGrid.mulLineCount = 5;   
      GrpGrid.displayTitle = 1;
      GrpGrid.locked = 1;
      GrpGrid.canSel = 0;
      GrpGrid.canChk = 1;
      GrpGrid.hiddenSubtraction = 1;
      GrpGrid.hiddenPlus = 1;
      GrpGrid.loadMulLine(iArray);  
      
      
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initDiskErrQueryGrid();
    initGrpGrid();
    fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
  }
  catch(re)
  {
    alert("在LCDiskApplyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>