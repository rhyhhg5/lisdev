<script src="../common/javascript/Common.js"></script>

<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	String strManageCom = tGI.ComCode;
	String strBatchNo = request.getParameter("BatchNo");
%>

<script language="JavaScript">
	var mBatchNo = "<%=strBatchNo%>";
	
	function initInpBox(){
    	try{
        	fm.BatchNo.value = mBatchNo;
    	}catch(ex){
        	alert("初始化界面错误!");
    	}
	}
	
	function initForm(){
		try{
			initInpBox();
			initImportBatchGrid();
			
			queryBatchList();
			showAllCodeName();
		}catch(e){
			alert("InitForm函数中发生异常:初始化界面错误!");
		}
	}
	
	function initImportBatchGrid(){
		
		var iArray = new Array();

	    try{
	        iArray[0]=new Array();
	        iArray[0][0]="序号";
	        iArray[0][1]="30px";
	        iArray[0][2]=100;
	        iArray[0][3]=3;
	        
	        iArray[1]=new Array();
	        iArray[1][0]="批次号";
	        iArray[1][1]="120px";
	        iArray[1][2]=100;
	        iArray[1][3]=0;
	        
	        iArray[2]=new Array();
	        iArray[2][0]="客户姓名";
	        iArray[2][1]="80px";
	        iArray[2][2]=80;
	        iArray[2][3]=0;
	        
	        iArray[3]=new Array();
	        iArray[3][0]="性别";
	        iArray[3][1]="40px";
	        iArray[3][2]=40;
	        iArray[3][3]=0;
	        
	        iArray[4]=new Array();
	        iArray[4][0]="出生日期";
	        iArray[4][1]="70px";
	        iArray[4][2]=70;
	        iArray[4][3]=0;
	        
	        iArray[5]=new Array();
	        iArray[5][0]="证件类型";
	        iArray[5][1]="60px";
	        iArray[5][2]=60;
	        iArray[5][3]=0;
	        
	        iArray[6]=new Array();
	        iArray[6][0]="证件号码";
	        iArray[6][1]="120px";
	        iArray[6][2]=100;
	        iArray[6][3]=0;
	        
	        ImportBatchGrid = new MulLineEnter("fm", "ImportBatchGrid"); 

	        ImportBatchGrid.mulLineCount = 0;   
	        ImportBatchGrid.displayTitle = 1;
	        ImportBatchGrid.canSel = 0;
	        ImportBatchGrid.hiddenSubtraction = 1;
	        ImportBatchGrid.hiddenPlus = 1;
	        ImportBatchGrid.canChk = 0;
	        ImportBatchGrid.loadMulLine(iArray);
	    }catch(ex){
	        alert("初始化ImportBatchGrid时出错：" + ex);
	    }
	}
</script>