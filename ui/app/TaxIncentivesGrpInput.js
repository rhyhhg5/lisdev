var showInfo;
var turnPage = new turnPageClass();

// 保存
function AddInput() {
	// Peoples

	if (fm.GrpName.value == null || fm.GrpName.value == ""
			|| fm.GrpName.value == "null") {
		alert("请录入单位名称！");
		return false;
	}
	if (fm.Peoples.value == null || fm.Peoples.value == ""
			|| fm.Peoples.value == "null" || fm.Peoples.value < 1) {
		alert("在职员工总数不能小于1！");
		return false;
	}
	// if(fm.TaxRegistration.value==null || fm.TaxRegistration.value=="" ||
	// fm.TaxRegistration.value=="null"){
	// alert("请录入税务登记证号！");
	// return false;
	// }
	// if(fm.OrgancomCode.value==null || fm.OrgancomCode.value=="" ||
	// fm.OrgancomCode.value=="null"){
	// alert("请录入组织机构代码！");
	// return false;
	// }
	// 规则变动，将之前的必录注释掉，改为录入时二选一，且两者不能同时录入，如下！
	if ((fm.TaxRegistration.value == null || fm.TaxRegistration.value == "" || fm.TaxRegistration.value == "null")
			&& (fm.OrgancomCode.value == null || fm.OrgancomCode.value == "" || fm.OrgancomCode.value == "null")) {
		alert("税务登记证号与社会信用代码两者必录其一！");
		return false;
	}
	if (fm.TaxRegistration.value != null && fm.TaxRegistration.value != ""
			&& fm.OrgancomCode.value != null && fm.OrgancomCode.value != "") {
		alert("税务登记证号与社会信用代码两者只能录入一项！");
		return false;
	}

	// if (!beforeAddSubmit()) {
	// return;
	// }

	if (!checktaxnoAndcreditcode()) {
		return false;
	}
	/**
	 * #3918:关于优化税优业务数据质量、完善核心系统校验功能的需求 需求相关：对于同一个团体单位名称，不允许新建多个团体编号，
	 * 或对应多个纳税登记号、社会信用代码。 Created by CLH 2018-07-09
	 */
	if (!checkBeforeInsert()) {
		return false;
	}

	fm.transact.value = "INSERT";

	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit();

}
// 修改
function UpdateInput() {
	var tTaxRegistration = fm.TaxRegistration.value;
	if (fm.Peoples.value == null || fm.Peoples.value == ""
			|| fm.Peoples.value == "null" || fm.Peoples.value < 1) {
		alert("在职员工总数不能小于1！");
		return false;
	}
	// var tSQL1 = "select grpname from lsgrp where taxregistration='"+
	// tTaxRegistration +"'";
	// var tArr1 = easyExecSql(tSQL1);
	// if(tArr1){
	// alert("修改失败,原因是:已经存在税务登记证号为："+tTaxRegistration+"的客户:"+tArr1[0][0]);
	// return false;
	// }
	// var tOrgancomCode = fm.OrgancomCode.value;
	// var tSQL2 = "select grpname from lsgrp where
	// organcomcode='"+tOrgancomCode+"'";
	// var tArr2 = easyExecSql(tSQL2);
	// if(tArr2){
	// alert("修改失败,原因是:已经存在社会信用代码为："+tOrgancomCode+"的客户:"+tArr2[0][0]);
	// return false;
	// }
	if ((fm.TaxRegistration.value == null || fm.TaxRegistration.value == "" || fm.TaxRegistration.value == "null")
			&& (fm.OrgancomCode.value == null || fm.OrgancomCode.value == "" || fm.OrgancomCode.value == "null")) {
		alert("修改失败,原因是:税务登记证号与社会信用代码两者必录其一！");
		return false;
	}
	if (fm.TaxRegistration.value != null && fm.TaxRegistration.value != ""
			&& fm.OrgancomCode.value != null && fm.OrgancomCode.value != "") {
		alert("修改失败,原因是:税务登记证号与社会信用代码两者只能录入一项！");
		return false;
	}
	if (!checktaxnoAndcreditcode()) {
		return false;
	}
	/**
	 * #3918:关于优化税优业务数据质量、完善核心系统校验功能的需求
	 * 需求相关：修改团体客户信息时，增加不能与已有社会信用代码或税务登记证代码重复校验。 Created by CLH 2018-07-09
	 */
	if (!checkBeforeUpdate()) {
		return false;
	}
	var tGrpNo = fm.GrpNo.value;
	var tSQL = "select distinct 1 from lsinsuredlist "
			+ "where batchno in (select batchno from lsbatchinfo where grpno='"
			+ tGrpNo + "') " + "and prtno is not null";
	var tArr = easyExecSql(tSQL);
	if (tArr) {
		alert("修改失败,原因是:该客户已存在投保保单，不能修改客户信息！");
		return false;
	}

	fm.transact.value = "UPDATE";

	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit();
}

function checkBeforeInsert() {
	var GrpName = fm.GrpName.value;
	// 判断是新团体还是老团体
	var checkGrpNo = "select distinct 1 from lsgrp where grpname = '" + GrpName
			+ "' with ur";
	var GrpNoResult = easyExecSql(checkGrpNo);
	if (GrpNoResult && GrpNoResult[0][0] == "1") {
		alert("已存在拥有相同团体单位名称的团体数据，请修改!");
		return false;
	}
	var TaxRegistration = fm.TaxRegistration.value;
	var OrgancomCode = fm.OrgancomCode.value;
	if (TaxRegistration == null || TaxRegistration == "") {
		var checkOrgancomCodeSql = "select distinct 1 from lsgrp where OrgancomCode = '"
				+ OrgancomCode + "' with ur";
		var OrgancomCodeResult = easyExecSql(checkOrgancomCodeSql);
		if (OrgancomCodeResult && OrgancomCodeResult[0][0] == "1") {
			alert("已存在拥有相同社会信用代码的团体数据，请修改");
			return false;
		}
	} else {
		var checkTaxRegistrationSql = "select distinct 1 from lsgrp where TaxRegistration = '"
				+ TaxRegistration + "' with ur";
		var TaxRegistrationResult = easyExecSql(checkTaxRegistrationSql);
		if (TaxRegistrationResult && TaxRegistrationResult[0][0] == "1") {
			alert("已存在拥有相同税务登记号码的团体数据，请修改");
			return false;
		}
	}
	return true;
}

function checkBeforeUpdate() {
	var GrpName = fm.GrpName.value;
	var GrpNo = fm.GrpNo.value;
	var checkGrpNo = "select distinct 1 from lsgrp where grpname = '" + GrpName
			+ "' and grpno <>'" + GrpNo + "' with ur";
	var GrpNoResult = easyExecSql(checkGrpNo);
	if (GrpNoResult && GrpNoResult[0][0] == "1") {
		alert("已存在拥有相同团体单位名称的团体数据，请修改!");
		return false;
	}
	var TaxRegistration = fm.TaxRegistration.value;
	var OrgancomCode = fm.OrgancomCode.value;
	if (TaxRegistration == null || TaxRegistration == "") {
		var checkOrgancomCodeSql = "select distinct 1 from lsgrp where OrgancomCode = '"
				+ OrgancomCode + "' and grpno <>'" + GrpNo + "' with ur";
		var OrgancomCodeResult = easyExecSql(checkOrgancomCodeSql);
		if (OrgancomCodeResult && OrgancomCodeResult[0][0] == "1") {
			alert("已存在拥有相同社会信用代码的团体数据，请修改");
			return false;
		}
	} else {
		var checkTaxRegistrationSql = "select distinct 1 from lsgrp where TaxRegistration = '"
				+ TaxRegistration + "' and grpno <>'" + GrpNo + "' with ur";
		var TaxRegistrationResult = easyExecSql(checkTaxRegistrationSql);
		if (TaxRegistrationResult && TaxRegistrationResult[0][0] == "1") {
			alert("已存在拥有相同税务登记号码的团体数据，请修改");
			return false;
		}
	}
	return true;
}

function checkBeforeInsertAndUpdate() {
	var GrpName = fm.GrpName.value;
	var GrpNo = fm.GrpNo.value;
	// 判断是新团体还是老团体
	if (GrpNo == null || GrpNo == "") {
		var checkGrpNo = "select distinct 1 from lsgrp where grpname = '"
				+ GrpName + "' with ur";
		var GrpNoResult = easyExecSql(checkGrpNo);
		if (GrpNoResult && GrpNoResult[0][0] == "1") {
			alert("已存在拥有相同团体单位名称的团体数据，请修改!");
			return false;
		}
		var TaxRegistration = fm.TaxRegistration.value;
		var OrgancomCode = fm.OrgancomCode.value;
		if (TaxRegistration == null || TaxRegistration == "") {
			var checkOrgancomCodeSql = "select distinct 1 from lsgrp where OrgancomCode = '"
					+ OrgancomCode + "' with ur";
			var OrgancomCodeResult = easyExecSql(checkOrgancomCodeSql);
			if (OrgancomCodeResult && OrgancomCodeResult[0][0] == "1") {
				alert("已存在拥有相同社会信用代码的团体数据，请修改");
				return false;
			}
		} else {
			var checkTaxRegistrationSql = "select distinct 1 from lsgrp where TaxRegistration = '"
					+ TaxRegistration + "' with ur";
			var TaxRegistrationResult = easyExecSql(checkTaxRegistrationSql);
			if (TaxRegistrationResult && TaxRegistrationResult[0][0] == "1") {
				alert("已存在拥有相同税务登记号码的团体数据，请修改");
				return false;
			}
		}
	} else {
		var checkGrpNo = "select distinct 1 from lsgrp where grpname = '"
				+ GrpName + "' and grpno <>'" + GrpNo + "' with ur";
		var GrpNoResult = easyExecSql(checkGrpNo);
		if (GrpNoResult && GrpNoResult[0][0] == "1") {
			alert("已存在拥有相同团体单位名称的团体数据，请修改!");
			return false;
		}
		var TaxRegistration = fm.TaxRegistration.value;
		var OrgancomCode = fm.OrgancomCode.value;
		if (TaxRegistration == null || TaxRegistration == "") {
			var checkOrgancomCodeSql = "select distinct 1 from lsgrp where OrgancomCode = '"
					+ OrgancomCode + "' and grpno <>'" + GrpNo + "' with ur";
			var OrgancomCodeResult = easyExecSql(checkOrgancomCodeSql);
			if (OrgancomCodeResult && OrgancomCodeResult[0][0] == "1") {
				alert("已存在拥有相同社会信用代码的团体数据，请修改");
				return false;
			}
		} else {
			var checkTaxRegistrationSql = "select distinct 1 from lsgrp where TaxRegistration = '"
					+ TaxRegistration + "' and grpno <>'" + GrpNo + "' with ur";
			var TaxRegistrationResult = easyExecSql(checkTaxRegistrationSql);
			if (TaxRegistrationResult && TaxRegistrationResult[0][0] == "1") {
				alert("已存在拥有相同税务登记号码的团体数据，请修改");
				return false;
			}
		}
	}
	return true;
}

// 提交前的相关校验加在这里
function beforeAddSubmit() {
	var tTaxRegistration = fm.TaxRegistration.value;
	if (tTaxRegistration != null && tTaxRegistration != ""
			&& tTaxRegistration != "null") {
		var tSQL1 = "select grpname from lsgrp where taxregistration='"
				+ tTaxRegistration + "'";
		var tArr1 = easyExecSql(tSQL1);
		if (tArr1) {
			alert("已经存在税务登记证号为：" + tTaxRegistration + "的客户:" + tArr1[0][0]);
			return false;
		}
	}
	var tOrgancomCode = fm.OrgancomCode.value;
	if (tOrgancomCode != null && tOrgancomCode != "" && tOrgancomCode != "null") {
		var tSQL2 = "select grpname from lsgrp where organcomcode='"
				+ tOrgancomCode + "'";
		var tArr2 = easyExecSql(tSQL2);
		if (tArr2) {
			alert("已经存在社会信用代码为：" + tOrgancomCode + "的客户:" + tArr2[0][0]);
			return false;
		}
	}
	var tGrpName = fm.GrpName.value;
	if (tOrgancomCode != null && tOrgancomCode != "" && tOrgancomCode != "null") {
		var tSQL3 = "select taxregistration from lsgrp where grpname='"
				+ tGrpName + "' and taxregistration is not null";
		var tArr3 = easyExecSql(tSQL3);
		if (tArr3) {
			if (!confirm("已存在税务登记证号：" + tArr3[0][0] + "的客户:" + tGrpName
					+ ",是否继续使用社会信用代码:" + tOrgancomCode + "添加客户？")) {
				return false;
			}
		}
	}
	if (tTaxRegistration != null && tTaxRegistration != ""
			&& tTaxRegistration != "null") {
		var tSQL4 = "select organcomcode from lsgrp where grpname='" + tGrpName
				+ "' and organcomcode is not null";
		var tArr4 = easyExecSql(tSQL4);
		if (tArr4) {
			if (!confirm("已存在社会信用代码：" + tArr4[0][0] + "的客户:" + tGrpName
					+ ",是否继续使用税务登记证号:" + tTaxRegistration + "添加客户？")) {
				return false;
			}
		}
	}
	return true;
}

function initThePage() {
	var tSQL = "select grpname,taxregistration,organcomcode,grpaddress,grpzipcode,grpnature,"
			+ "peoples,leagleperson,phone,handlername,department,handlerphone,bankcode,bankaccname,bankaccno,businesstype "
			+ "from lsgrp where grpno='" + pGrpNo + "'";
	var tArr = easyExecSql(tSQL);
	if (tArr) {
		fm.GrpName.value = tArr[0][0];
		fm.TaxRegistration.value = tArr[0][1];
		fm.OrgancomCode.value = tArr[0][2];
		fm.GrpAddress.value = tArr[0][3];
		fm.GrpZipCode.value = tArr[0][4];
		fm.GrpNature.value = tArr[0][5];
		fm.Peoples.value = tArr[0][6];
		fm.LegalPersonName.value = tArr[0][7];
		fm.Phone.value = tArr[0][8];
		fm.HandlerName.value = tArr[0][9];
		fm.Department.value = tArr[0][10];
		fm.HandlerPhone.value = tArr[0][11];
		fm.BankCode.value = tArr[0][12];
		fm.BankAccName.value = tArr[0][13];
		fm.BankAccNo.value = tArr[0][14];
		fm.BusinessType.value = tArr[0][15];
	}
}

function afterSubmit(FlagStr, Content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		afterInsertGrpInfo();
	}
}

function afterInsertGrpInfo() {
	var tGrpName = fm.GrpName.value;
	var tTaxRegistration = fm.TaxRegistration.value;
	var tOrgancomCode = fm.OrgancomCode.value;
	var tSQL = "";
	if (tOrgancomCode == null || tOrgancomCode == "" || tOrgancomCode == "null") {
		tSQL = "select grpno from lsgrp where grpname='" + tGrpName
				+ "' and taxregistration='" + tTaxRegistration + "'";
	} else if (tTaxRegistration == null || tTaxRegistration == ""
			|| tTaxRegistration == "null") {
		tSQL = "select grpno from lsgrp where grpname='" + tGrpName
				+ "' and organcomcode='" + tOrgancomCode + "'";
	}
	var tArr = easyExecSql(tSQL);
	if (tArr) {
		fm.GrpNo.value = tArr[0][0];
	}
}

function goBack() {
	window.location.replace("./TaxIncentivesInput.jsp");
}

function checktaxnoAndcreditcode() {
	var GTaxNo = fm.TaxRegistration.value;
	var GOrgancomCode = fm.OrgancomCode.value;
	if (GTaxNo != "" && GTaxNo != null && GTaxNo.length != 15
			&& GTaxNo.length != 18 && GTaxNo.length != 20) {
		alert("社会信用代码或税务登记号长度不符合要求！");
		return false;
	}
	if (GOrgancomCode != "" && GOrgancomCode != null
			&& GOrgancomCode.length != 18) {
		alert("社会信用代码或税务登记号长度不符合要求！");
		return false;
	}
	return true;
}
