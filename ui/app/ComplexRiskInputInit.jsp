<%
//程序名称：ComplexRiskInputInit.jsp
//程序功能：功能描述
//创建日期：2005-05-30 18:29:02
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
  }
  catch(ex) {
    alert("在ComplexRiskInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    	initInpBox();
		initComplexRiskGrid();
		initBnfGrid();
		showRiskInfo();
		showBnfInfo();
  }
  catch(re) {
    alert("ComplexRiskInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ComplexRiskGrid;
function initComplexRiskGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();                    
		iArray[1][0]="被保险人";         		//列名    
		iArray[1][1]="60px";         		//列名    
		iArray[1][3]=0;         		//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="险种代码";         		//列名    
		iArray[2][1]="30px";         		//列名    
		iArray[2][3]=0;         		//列名        
		
		iArray[3]=new Array();                    
		iArray[3][0]="保额";         		//列名    
		iArray[3][1]="30px";         		//列名    
		iArray[3][3]=0;         		//列名   
		
		iArray[4]=new Array();  
		iArray[4][0]="档次";
		iArray[4][1]="10px";   
		iArray[4][3]=0;         
		
		iArray[5]=new Array();  
		iArray[5][0]="身故受益人标志";
		iArray[5][1]="30px";   
		iArray[5][3]=0;         
		
		iArray[6]=new Array();  
		iArray[6][0]="保险期间";
		iArray[6][1]="20px";   
		iArray[6][3]=0;         
		
		iArray[7]=new Array();  
		iArray[7][0]="缴费年期";
		iArray[7][1]="20px";   
		iArray[7][3]=0;  
		
		iArray[8]=new Array();  
		iArray[8][0]="缴费频次";
		iArray[8][1]="20px";   
		iArray[8][3]=0;  
		
		iArray[9]=new Array();  
		iArray[9][0]="期交保费";
		iArray[9][1]="20px";   
		iArray[9][3]=0;  
        
        iArray[10]=new Array();  
        iArray[10][0]="追加保费";
        iArray[10][1]="20px";   
        iArray[10][3]=0;
		     
    
    ComplexRiskGrid = new MulLineEnter( "fm" , "ComplexRiskGrid" ); 
    //这些属性必须在loadMulLine前           
    
    ComplexRiskGrid.mulLineCount = 0;   
    ComplexRiskGrid.displayTitle = 1;
    ComplexRiskGrid.hiddenPlus = 1;
    ComplexRiskGrid.hiddenSubtraction = 1;
    ComplexRiskGrid.canSel = 0;
    ComplexRiskGrid.canChk = 0;
    //ComplexRiskGrid.selBoxEventFuncName = "showOne";
    ComplexRiskGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ComplexRiskGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

// 受益人信息列表的初始化
function initBnfGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";			//列宽
    iArray[0][2]=10;				//列最大值
    iArray[0][3]=0;					//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="受益人类别"; 		//列名
    iArray[1][1]="60px";			//列宽
    iArray[1][2]=40;				//列最大值
    iArray[1][3]=0;					//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="姓名"; 			//列名
    iArray[2][1]="30px";			//列宽
    iArray[2][2]=30;				//列最大值
    iArray[2][3]=0;					//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="证件类型"; 			//列名
    iArray[3][1]="40px";			//列宽
    iArray[3][2]=40;				//列最大值
    iArray[3][3]=0;					//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="证件号码"; 			//列名
    iArray[4][1]="120px";			//列宽
    iArray[4][2]=80;				//列最大值
    iArray[4][3]=0;					//是否允许输入,1表示允许，0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="与被保人关系"; 		//列名
    iArray[5][1]="60px";			//列宽
    iArray[5][2]=60;				//列最大值
    iArray[5][3]=0;					//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="受益比例"; 			//列名
    iArray[6][1]="40px";			//列宽
    iArray[6][2]=40;				//列最大值
    iArray[6][3]=0;					//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="受益顺序"; 			//列名
    iArray[7][1]="40px";			//列宽
    iArray[7][2]=40;				//列最大值
    iArray[7][3]=0;					//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="险种"; 			//列名
    iArray[8][1]="60px";			//列宽
    iArray[8][2]=100;				//列最大值
    iArray[8][3]=0;					//是否允许输入,1表示允许，0表示不允许

    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" );
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 0;
    BnfGrid.displayTitle = 1;
    BnfGrid.addEventFuncName="setFocus";
    BnfGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在ProposalInit.jsp-->initBnfGrid函数中发生异常:初始化界面错误!");
  }
}


</script>
