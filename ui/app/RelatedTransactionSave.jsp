<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输出参数
	CErrors tError = null;
  	String FlagStr = "";
  	String Content = "";
  	GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");
  	String transact = request.getParameter("transact");
  	
  	LDRelatedPartySchema tLDRelatedPartySchema = new LDRelatedPartySchema();
  	LDRelatedPartyDB tLDRelatedPartyDB = new LDRelatedPartyDB();
  	
  	if("UPDATE".equals(transact)){
  		
  		String tRelateNo = request.getParameter("RelateNo");
  		if(tRelateNo == null || "".equals(tRelateNo)){
  			Content = "保存失败，原因是:获取关联方编号失败！";
  			FlagStr = "Fail";
  		}
  		tLDRelatedPartyDB.setRelateNo(tRelateNo);
  		LDRelatedPartySet tLDRelatedPartySet = tLDRelatedPartyDB.query();
  		if(tLDRelatedPartySet.size()!=1){
  		  	Content = "保存失败，原因是:获取关联方失败！";
  			FlagStr = "Fail";
  		}
  		if(!"Fail".equals(FlagStr)){
  		  		
  			tLDRelatedPartySchema = tLDRelatedPartySet.get(1);
  			tLDRelatedPartySchema.setRgtMoney(request.getParameter("RgtMoney"));
  			tLDRelatedPartySchema.setBusinessScope(request.getParameter("BusinessScope"));
  			tLDRelatedPartySchema.setChairmanName(request.getParameter("ChairmanName"));
  			tLDRelatedPartySchema.setGeneralManagerName(request.getParameter("GeneralManagerName"));
  			tLDRelatedPartySchema.setSex(request.getParameter("Sex"));
  			tLDRelatedPartySchema.setPosition(request.getParameter("Position"));
  			tLDRelatedPartySchema.setOtherOrganization(request.getParameter("OtherOrganization"));
  			tLDRelatedPartySchema.setModifyDate(PubFun.getCurrentDate());
  			tLDRelatedPartySchema.setModifyTime(PubFun.getCurrentTime());
  			
  		}
  		
  	}else if("INSERT".equals(transact)){
  		String tRelateNo = PubFun1.CreateMaxNo("RELATENO",10);
  		if("".equals(tRelateNo) || tRelateNo == null){
  			return ;
  		}
  		tLDRelatedPartySchema.setRelateNo(tRelateNo);
  		tLDRelatedPartySchema.setName(request.getParameter("Name"));
  		tLDRelatedPartySchema.setRgtMoney(request.getParameter("RgtMoney"));
  		tLDRelatedPartySchema.setBusinessScope(request.getParameter("BusinessScope"));
  		tLDRelatedPartySchema.setChairmanName(request.getParameter("ChairmanName"));
  		tLDRelatedPartySchema.setGeneralManagerName(request.getParameter("GeneralManagerName"));
  		tLDRelatedPartySchema.setSex(request.getParameter("Sex"));
  		tLDRelatedPartySchema.setPosition(request.getParameter("Position"));
  		tLDRelatedPartySchema.setIDNo(request.getParameter("IDNo"));
  		tLDRelatedPartySchema.setOtherOrganization(request.getParameter("OtherOrganization"));
  		tLDRelatedPartySchema.setRelatedType(request.getParameter("RelatedType"));
  		tLDRelatedPartySchema.setOperator(tG.Operator);
  		tLDRelatedPartySchema.setMakeDate(PubFun.getCurrentDate());
  		tLDRelatedPartySchema.setMakeTime(PubFun.getCurrentTime());
  		tLDRelatedPartySchema.setModifyDate(PubFun.getCurrentDate());
  		tLDRelatedPartySchema.setModifyTime(PubFun.getCurrentTime());
  	}
  	
  	try{
  		VData tVData = new VData();
  		tVData.add(tG);
  		tVData.add(tLDRelatedPartySchema);
  		RelatedTransactionUI tRelatedTransactionUI = new RelatedTransactionUI();
  		if(!tRelatedTransactionUI.submitData(tVData,transact)){
  			Content = "处理失败，原因是:" + tRelatedTransactionUI.mErrors.getFirstError();
            FlagStr = "Fail";
  		}else{
  			Content = "处理成功";
            FlagStr = "Succ";
  		}
  	}catch(Exception ex){
  		ex.printStackTrace();
  	}
%>

<html>
<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>