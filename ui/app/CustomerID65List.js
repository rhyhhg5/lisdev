var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//投保人报表
function appntSubmitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }  
    var tStartSignDate = fm.StartSignDate.value;
    var tEndSignDate = fm.EndSignDate.value;
	if(dateDiff(tStartSignDate, tEndSignDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}
    var tStrSQL = ""
    	+ " select "
    	+ " lcc.managecom , "
    	+ " (select name from ldcom where comcode = lcc.managecom) , "
    	+ " lcp.riskcode , "
    	+ " (select riskname from lmriskapp where riskcode = lcp.riskcode) , "
    	+ " lcp.prem , "
    	+ " lcc.signdate , "
    	+ " lcc.contno , " 
    	+ " lcc.AppntName , "
    	+ " lcc.AppntIDNo , "
    	+ " lcc.AccName , "
    	+ " lcc.BankAccNo "
    	+ " from lccont lcc, lcpol lcp "
    	+ " where 1=1 "
    	+ " and lcc.contno = lcp.contno "
    	+ " and lcc.managecom like '8661%' "
    	+ " and lcc.InsuredIDNo like '65%'  "
        + getWherePart("lcc.signdate", "StartSignDate", ">=")
        + getWherePart("lcc.signdate", "EndSignDate", "<=")
        ;
    fm.querySql.value = tStrSQL;
	fm.CustomerAction.value = "doAppnt";
    var oldAction = fm.action;
    fm.action = "CustomerID65ListSave.jsp";
    fm.submit();
    fm.action = oldAction;
}
//被保人报表
function insuredSubmitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }  
    var tStartSignDate = fm.StartSignDate.value;
    var tEndSignDate = fm.EndSignDate.value;
	if(dateDiff(tStartSignDate, tEndSignDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}
    var tStrSQL = ""
    	+ " select "
    	+ " lcc.managecom , "
    	+ " (select name from ldcom where comcode = lcc.managecom) , "
    	+ " lcp.riskcode , "
    	+ " (select riskname from lmriskapp where riskcode = lcp.riskcode) , "
    	+ " lcp.prem , "
    	+ " lcc.signdate , "
    	+ " lcc.contno , " 
    	+ " lcc.InsuredName , "
    	+ " lcc.InsuredIDNo  "
    	+ " from lccont lcc, lcpol lcp "
    	+ " where 1=1 "
    	+ " and lcc.contno = lcp.contno "
    	+ " and lcc.managecom like '8661%' "
    	+ " and lcc.InsuredIDNo like '65%'  "
        + getWherePart("lcc.signdate", "StartSignDate", ">=")
        + getWherePart("lcc.signdate", "EndSignDate", "<=")
        ;
    fm.querySql.value = tStrSQL;
	fm.CustomerAction.value = "doInsured";
    var oldAction = fm.action;
    fm.action = "CustomerID65ListSave.jsp";
    fm.submit();
    fm.action = oldAction;
}
//受益人报表
function bnfSubmitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }  
    var tStartSignDate = fm.StartSignDate.value;
    var tEndSignDate = fm.EndSignDate.value;
	if(dateDiff(tStartSignDate, tEndSignDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}
    var tStrSQL = ""
    	+ " select "
    	+ " lcc.managecom , "
    	+ " (select name from ldcom where comcode = lcc.managecom) , "
    	+ " lcp.riskcode , "
    	+ " (select riskname from lmriskapp where riskcode = lcp.riskcode) , "
    	+ " lcp.prem , "
    	+ " lcc.signdate , "
    	+ " lcc.contno , "
    	+ " lcb.Name , "
    	+ " lcb.IDNo  "
    	+ " from lccont lcc, lcpol lcp, LCBnf lcb "
    	+ " where 1=1  "
    	+ " and lcc.contno = lcp.contno "
    	+ " and lcc.contno = lcb.contno "
    	+ " and lcc.managecom like '8661%' "
    	+ " and lcb.IDNo like '65%' "
        + getWherePart("lcc.signdate", "StartSignDate", ">=")
        + getWherePart("lcc.signdate", "EndSignDate", "<=")
        ;
    fm.querySql.value = tStrSQL;
	fm.CustomerAction.value = "doBnf";
    var oldAction = fm.action;
    fm.action = "CustomerID65ListSave.jsp";
    fm.submit();
    fm.action = oldAction;
}