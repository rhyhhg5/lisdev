<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输入参数
	QYModifyAmntUI tQYModifyAmntUI = new QYModifyAmntUI();
	
	String mModifyAmnt = request.getParameter("ModifyAmnt");
	String mAmntType = request.getParameter("AmntType");
	String tRadio[] = request.getParameterValues("InpPolGridSel"); 
	String tPrtNo[] = request.getParameterValues("PolGrid1");
	String tGrpContNo[] = request.getParameterValues("PolGrid2");
	String tContPlanCode[] = request.getParameterValues("PolGrid5");
	String tRiskCode[] = request.getParameterValues("PolGrid6");
	String tDutyCode[] = request.getParameterValues("PolGrid7");
	String tOldAmnt[] = request.getParameterValues("PolGrid8");
	String tContType[] = request.getParameterValues("PolGrid11");

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();
	
	for (int i=0; i< tRadio.length;i++){
		if("1".equals(tRadio[i])){
			transferData.setNameAndValue("PrtNo", tPrtNo[i]);
			transferData.setNameAndValue("GrpContNo", tGrpContNo[i]);
			transferData.setNameAndValue("ContPlanCode", tContPlanCode[i]);
			transferData.setNameAndValue("RiskCode", tRiskCode[i]);
			transferData.setNameAndValue("DutyCode", tDutyCode[i]);
			transferData.setNameAndValue("ContType", tContType[i]);
			transferData.setNameAndValue("OldAmnt", tOldAmnt[i]);
			transferData.setNameAndValue("ModifyAmnt", mModifyAmnt);
			transferData.setNameAndValue("AmntType", mAmntType);
			break;
		}
	}
	if(!"Fail".equals(FlagStr)){
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			tQYModifyAmntUI.submitData(tVData, "");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = tQYModifyAmntUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>