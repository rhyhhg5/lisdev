<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：QuestQuery.jsp
//程序功能：问题件查询
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>

<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>

<head >
<title>问题件查询 </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="QuestQuery.js"></SCRIPT>
  <%@include file="QuestQueryInit.jsp"%>
  
</head>

<body  onload="initForm('<%=tProposalNo%>','<%=tFlag%>');" >
  <form method=post name=fm target="fraSubmit" action="./QuestQueryChk.jsp">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);">
    		</td>
    		<td class= titleImg>
    			 问题件查询
    		</td>
    	</tr>
    </table>
    <table class= common>
    	<TR  class= common>
    	  <TD  class= title>
          印刷号码
        </TD>
        <TD  class= input>
          <Input class=common name=PrtNo>
        </TD>
   <!--     <TD  class= title>
          投保单号码
        </TD>-->
          <Input class=common name=ProposalNo  type="hidden">
        <TD  class= title>
            操作位置
          </TD>
          <TD  class= input>
            <Input class=codeno name=OperatePos CodeData="0|^0|承保^1|核保^5|复核" ondblclick= "showCodeListEx('OperatePos',[this,OperatePosName],[0,1]);" onkeyup="showCodeListKeyEx('OperatePos',[this,OperatePosName],[0,1]);" ><input class=className name= OperatePosName>
          </TD>
      </TR>

    	<TR  class= common>
        <TD  class= title>
          返回对象
        </TD>
        <TD  class= input>
          <Input class=codeno name=BackObj CodeData="0|^1|操作员^2|业务员^3|保户^4|机构" ondblclick= "showCodeListEx('BackObj',[this,BackObjName],[0,1]);" onkeyup="showCodeListKeyEx('BackObj',[this,BackObjName],[0,1]);" ><input class=className name= BackObjName>
        </TD>
        <TD  class= title>
          返回对象代码
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=Operator CLASS=common >
        </TD>
        <TD  class= title>
          是否回复
        </TD>
        <TD  class= input>
          <Input class=codeno name=IfReply ondblclick="return showCodeList('YesNo',[this,IfReplyName],[0,1]);" onkeyup="return showCodeListKey('YesNo',[this,IfReplyName],[0,1]);"><input class=className name= IfReplyName>
        </TD>
      </TR>
      
      <TR>
        <TD CLASS=title>
          问题件代码 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=IssueType CLASS=codeno ondblclick= "showCodeListEx('IssueType',[this,IssueTypeName],[0,1],null,null,null,null,1500);" onkeyup="showCodeListKeyEx('IssueType',[this,IssueTypeName],[0,1],null,null,null,null,1500);"  ><input class=className name= IssueTypeName>
        </TD>
        <TD CLASS=title>
          问题件所在机构 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=IsueManageCom VALUE="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('comcode',[this,IsueManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,IsueManageComName],[0,1]);" ><input class=className name= IsueManageComName>
        </TD>
        <TD CLASS=title>
          需要打印 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=needprint CLASS=codeno  CodeData="0|3^Y|需要打印^N|不需要打印^P|打印已经回复"  ondblclick="return showCodeListEx('UWNeedPrint',[this,needprintName],[0,1]);" onkeyup="return showCodeListKeyEx('UWNeedPrint',[this,needprintName],[0,1]);"><input class=className name= needprintName>
        </TD>
      </TR>  
    </table>

    <input type= "button" name= "Query" value="查询" class=common onClick= "query()">
    <br><br>
    
    <table class=common>
      <TR>
        <TD CLASS=title>
          核保通知书流水号 
        </TD>
        <TD CLASS=input COLSPAN=1>
          <Input NAME=SerialNo VALUE="" CLASS=common >
        </TD>

      </TR>    
    </table>
    <input type= "button" name= "Query" value="根据通知书查询" class=common onClick= "query2()">
    <br><br>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);">
    		</td>
    		<td class= titleImg>
    			 问题件内容：
    		</td>
    	</tr>
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanQuestGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    	
    	<Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div> 	 
         
    </div>
  
  <br> 
  <table width="80%" height="37%" class= common>
    <TR  class= common> 
      <TD width="100%" height="13%"  class= title> 问题件内容 </TD>
    </TR>
    <TR  class= common>
      <TD height="87%"  class= title><textarea name="Content" cols="120" rows="10" class="common" readonly></textarea></TD>
    </TR>
  </table>
  
  <table width="80%" height="37%" class= common>
    <TR  class= common> 
      <TD width="100%" height="13%"  class= title> 问题件回复 </TD>
    </TR>
    <TR  class= common>
      <TD height="87%"  class= title><textarea name="ReplyResult" cols="120" rows="10" class="common" readonly></textarea></TD>
    </TR>
  </table>
  <p> 
    <!--读取信息-->
    <input type= "hidden" name= "Flag" value="">
    <input type= "hidden" name= "ProposalNoHide" value= "">
    <input type= "hidden" name= "Type" value="">
    <input type= "hidden" name= "HideOperatePos" value="">
    <input type= "hidden" name= "HideQuest" value="">
  </p>
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>