//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var QueryResult="";
var QueryCount = 0;
var mulLineCount = 0;
var QueryWhere="";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mupFlag=false;//执行修改的标记
window.onfocus=myonfocus;
//初始化险种信息

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}


function initGrpSharenAmntRisk(){
//查询打印号
  var tSql=" select prtno from lcgrpcont where proposalgrpcontno='"+ProposalGrpContNo+"'";
  var tcontResult=easyExecSql(tSql);
  fm.prtNo.value=tcontResult[0][0];
  fm.ContPlanCode.value=ContPlanCode;
  fm.GrpContNo.value=GrpContNo;
  //查询险种信息
  var tRiskSql="select a.RiskCode,(select b.riskname from lmrisk b where 1=1 and  b.riskcode=a.riskCode) from LCContPlanRisk a where a.grpcontno='"+GrpContNo+"' and a.contplancode='"+ContPlanCode+"' union select a.RiskCode,(select b.riskname from lmrisk b where b.riskcode=a.riskCode) from LbContPlanRisk a where a.grpcontno='"+GrpContNo+"' and a.contplancode='"+ContPlanCode+"' union select a.RiskCode,(select b.riskname from lmrisk b where b.riskcode=a.riskCode) from LobContPlanRisk a where a.grpcontno='"+GrpContNo+"' and a.contplancode='"+ContPlanCode+"' with ur ";
  turnPage.queryModal(tRiskSql,GrpShareAmntRiskGrid);
  
  //查询共用计划
  var tSharePlanSql="select SharePlanCode,SharePlanName,ShareAmnt from LCGrpShareAmnt a where PrtNo='"+fm.prtNo.value+"' and ContPlanCode='"+ContPlanCode+"'" ;
  turnPage1.queryModal(tSharePlanSql,GrpShareAmntGrid);
  
  
  
}

//获取共用计划信息
function ShowGrpShareAmntDetail(parm1,parm2)
{
    try{
    document.getElementById("divShareAmntSave").style.display="";
    if(fm.all(parm1).all('InpGrpShareAmntGridSel').value == '1' && fm.all(parm1).all('GrpShareAmntGrid1').value!="")
    {
       var cSharePlanCode = fm.all(parm1).all('GrpShareAmntGrid1').value;
       fm.ShareAmntcode.value=cSharePlanCode;
       //勾选选择的险种
       var tSqlchk=" select distinct Riskcode,SharePlanCode,AmntType,ShareAmnt,GetRate,OutDutyAmnt from LCGrpRiskShareAmnt where PrtNo='"+fm.prtNo.value+"' and ContPlanCode='"+fm.ContPlanCode.value+"' and SharePlanCode='"+fm.ShareAmntcode.value+"'";
       var tResult=easyExecSql(tSqlchk);
       if(tResult!=null && tResult!=""){
       
       for(var i=0;i<GrpShareAmntRiskGrid.mulLineCount;i++)
       {
         for(var j=0;j<tResult.length;j++){
          if(GrpShareAmntRiskGrid.getRowColData(i,1)==tResult[j][0]){
          GrpShareAmntRiskGrid.checkBoxSel(i+1);
          }
         }
       }
       
    }
     var tSqlShareAmnt=" select lgrs.SharePlanCode,(select lgs.SharePlanName from LCGrpShareAmnt lgs where lgs.PrtNo='"+fm.prtNo.value+"' and lgs.ContPlanCode='"+fm.ContPlanCode.value+"' and lgs.SharePlanCode=lgrs.SharePlanCode ),lgrs.ShareAmnt,lgrs.AmntType,case lgrs.AmntType when '1' then '保额' when '2' then '限额' else ''  end , lgrs.GetRate,lgrs.OutDutyAmnt from LCGrpRiskShareAmnt lgrs where lgrs.PrtNo='"+fm.prtNo.value+"' and lgrs.ContPlanCode='"+ContPlanCode+"' and lgrs.SharePlanCode='"+fm.ShareAmntcode.value+"' with ur";
     var tResultShareAmnt=easyExecSql(tSqlShareAmnt);
     if(tResultShareAmnt){
     fm.SharePlanCode.value=tResultShareAmnt[0][0];
     fm.SharePlanName.value=tResultShareAmnt[0][1];
     fm.AmntType.value=tResultShareAmnt[0][3];
     fm.AmntTypeName.value=tResultShareAmnt[0][4];
     fm.ShareAmnt.value=tResultShareAmnt[0][2];
     //fm.GetRate.value=tResultShareAmnt[0][5];
     //fm.OutDutyAmnt.value=tResultShareAmnt[0][6];
     mupFlag=true;
     }else{
     //后置的
     fm.SharePlanCode.value=fm.all(parm1).all('GrpShareAmntGrid1').value;
     fm.SharePlanName.value=fm.all(parm1).all('GrpShareAmntGrid2').value;
     fm.ShareAmnt.value=fm.all(parm1).all('GrpShareAmntGrid3').value;
     fm.AmntType.value="";
     fm.AmntTypeName.value="";
     //fm.GetRate.value="";
     //fm.OutDutyAmnt.value="";
     GrpShareAmntRiskGrid.checkBoxAllNot();
     }
     
     
     }
     
    }catch(e){
     alert(e.message);
    }
    
     
    
	
}

//新增共用计划
function SubmitDetail()
{
   try{
   
   if(!beforeSubDetail()){
     return false;
    }
    
    //if(mupFlag){
    //fm.all('fmtransact').value = "UPDATEDetail";
    //}else{
    fm.all('fmtransact').value = "INSERTDetail";
   // }
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action="./GrpShareAmntSave.jsp";
    fm.submit();
   }catch(e){
    alert("222"+e.message);
   }
	
}

function beforeSubDetail()
{

    if(fm.SharePlanName.value==""){
       alert("共用计划名称不能为空!");
       return ;
    }
    
    if(fm.AmntType.value==""){
       alert("保险金额类型不能为空!");
       return ;
    }
    
    if(fm.ShareAmnt.value==""){
       alert("共用保险金额不能为空!");
       return ;
    }
    
    if(fm.SharePlanCode.value==""){
       alert("共用计划编码不能为空!");
       return ;
    }
   
    var lineCount = 0;
    lineCount = GrpShareAmntRiskGrid.mulLineCount;
    if(lineCount==0){
        alert("请先保存保障计划的险种!");
        return ;
    }
    
    var tflag=false;
    for(var i=0;i<lineCount;i++){
        if(GrpShareAmntRiskGrid.getChkNo(i)){
        tflag=true;
        var tPaySQL = "select Riskcode from LCGrpRiskShareAmnt where PrtNo='"+fm.prtNo.value+"' and ContPlanCode='"+ContPlanCode+"' and Riskcode ='"+GrpShareAmntRiskGrid.getRowColData(i,1)+"' and SharePlanCode <> '"+fm.SharePlanCode.value+"'";
        var PayArr = easyExecSql(tPaySQL);
        if(PayArr){
        alert("该险种已存在改保障计划下的共用计划中！");
        return ;   
        }
        }
        
    }
    if(!tflag){
     alert("请先选择险种信息!");
     return;
    }
    
    return true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit(flag,content)
{
  showInfo.close();
  window.focus();
  if (flag == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    initForm();
    document.getElementById("divShareAmntSave").style.display="none";
  }
 }   

function DelShareAmntPlan()
{
	if(!confirm("删除共用计划，是否继续？"))
	{
		return false;
	}
	fm.all('fmtransact').value = "DELETE";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action="./GrpShareAmntSave.jsp";
    fm.submit();
}

//保存共用计划内容
function SubmitInfo()
{
    try{
    if(!beforeSub()){
        return false;
    }
    fm.all('fmtransact').value = "INSERT";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action="./GrpShareAmntSave.jsp";
    fm.submit();
    }catch(e){
    alert("cuowu11"+e.message);
    }
	
}

function beforeSub()
{
   try{
    var tnewflag=false;
    var tdelflag=false;
    var lineCount = 0;
    lineCount = GrpShareAmntGrid.mulLineCount;
    if(lineCount==0){
        alert("请填写共用保额计划信息！");
        return false;
    }
    for(var i=0;i<lineCount;i++){
        var tcode=GrpShareAmntGrid.getRowColData(i,1);
        var tname=GrpShareAmntGrid.getRowColData(i,2);
        var tAmnt=GrpShareAmntGrid.getRowColData(i,3);
        //有新增的计划
        if(tcode=="" && tname!="" && tAmnt!=""){
        tnewflag=true;
        }
        
        if(tname==""){
         alert("第'"+(i+1)+"'行，共用计划名称不能为空！");
         return false;
        }
        if(tAmnt!=""){
        if(isNaN(tAmnt)){
           alert("第'"+(i+1)+"'行，共用保费金额必须为数字！");
           return false;
        } 
        }else{
           alert("第'"+(i+1)+"'行，共用保费金额不能为空！");
           return false;
        }
              
    }
    
    if(!tnewflag){
     alert("请先增加新的共用计划名称和共用保险金额!");
     return false;
    }
   }catch(e){
    alert("cuowu"+e.message);
    return false;
   }
   
    return true;
    
}


function goback(){
 top.close();
}


