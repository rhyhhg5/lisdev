<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	CError tError = null;
	String Content = "";
	String FlagStr="";
	String tAction = request.getParameter("Action");
	String tFinancialAction = request.getParameter("FinancialAction");
	String tContType = request.getParameter("ContType");
	String tPrtNo = request.getParameter("PrtNo");
	String tContNo = request.getParameter("ContNo");
	String tSaleChnl = request.getParameter("mSaleChnl");
	String tCrsSaleChnl = request.getParameter("mCrsSaleChnl");	
	String tCrsBussType = request.getParameter("mCrsBussType");	
	String tGrpAgentCode = request.getParameter("mGrpAgentCode");
	String tGrpAgentName = request.getParameter("mGrpAgentName");
	String tGrpAgentIdNo = request.getParameter("mGrpAgentIdNo");
	String tGrpAgentCom = request.getParameter("mGrpAgentCom");
	
	System.out.println("动作为：" + tAction);
	System.out.println("保单类型为：" + tContType);
	System.out.println("保单印刷号：" + tPrtNo);
	System.out.println("保单号：" + tContNo);
	System.out.println("要改的销售渠道：" + tSaleChnl);
	System.out.println("要改的交叉销售渠道：" + tCrsSaleChnl);
	System.out.println("要改的交叉销售业务类型：" + tCrsBussType);
	System.out.println("要改的对方业务员代码：" + tGrpAgentCode);
	System.out.println("要改的对方业务员名字：" + tGrpAgentName);
	System.out.println("要改的对方业务员证件号：" + tGrpAgentIdNo);
	System.out.println("要改的对方机构代码：" + tGrpAgentCom);
	
	
	VData tVData = new VData();
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");
	tVData.add(tGlobalInput);
	
	TransferData tTransferData = new TransferData(); 
	tTransferData.setNameAndValue("tAction", tAction);
	tTransferData.setNameAndValue("tFinancialAction", tFinancialAction);
	tTransferData.setNameAndValue("tContType", tContType);
	tTransferData.setNameAndValue("tPrtNo", tPrtNo);
	tTransferData.setNameAndValue("tContNo", tContNo);
	tTransferData.setNameAndValue("tSaleChnl", tSaleChnl);
	tTransferData.setNameAndValue("tCrsSaleChnl", tCrsSaleChnl);
	tTransferData.setNameAndValue("tCrsBussType", tCrsBussType);
	tTransferData.setNameAndValue("tGrpAgentCode", tGrpAgentCode);
	tTransferData.setNameAndValue("tGrpAgentName", tGrpAgentName);
	tTransferData.setNameAndValue("tGrpAgentIdNo", tGrpAgentIdNo);	
	tTransferData.setNameAndValue("tGrpAgentCom", tGrpAgentCom);
	
	tVData.addElement(tTransferData);
	
	CrsnanceBL tCrsnanceBL = new CrsnanceBL();
	System.out.println("before submit");
	if(!tCrsnanceBL.submitData(tVData,tAction))
	{
		Content = " 保存失败，原因是: " + tCrsnanceBL.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	
	System.out.println("Content:"+Content);

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>