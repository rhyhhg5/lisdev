 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskMySelectSave.jsp
//程序功能：非医药账单明细转交数据保存页面
//创建日期：2013-4-15
//创建人  ：zlx
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
   <%@page import="com.sinosoft.test.*"%>
  
<%
//	输入参数
	RateAndLimitModificationUI rateAndLimitModificationUI = new RateAndLimitModificationUI();
	
	String mGetlimit = request.getParameter("Getlimit");
	String mGetrate = request.getParameter("Getrate");
	
	String tRadio[] = request.getParameterValues("InpLMrateAndLimitGridSel"); 
	String tPrtNo[] = request.getParameterValues("LMrateAndLimitGrid1");
	String tGrpContNo[] = request.getParameterValues("LMrateAndLimitGrid2");
	
	String tContPlanCode[] = request.getParameterValues("LMrateAndLimitGrid5");
	String tRiskCode[] = request.getParameterValues("LMrateAndLimitGrid6");
	String tDutyCode[] = request.getParameterValues("LMrateAndLimitGrid7");
  //String tContType[] = request.getParameterValues("LMrateAndLimitGrid11");

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	String strInput = "";
	TransferData transferData = new TransferData();
	
	for (int i=0; i< tRadio.length;i++){
		if("1".equals(tRadio[i])){
			transferData.setNameAndValue("PrtNo", tPrtNo[i]);
			transferData.setNameAndValue("GrpContNo", tGrpContNo[i]);
			transferData.setNameAndValue("ContPlanCode", tContPlanCode[i]);
			transferData.setNameAndValue("RiskCode", tRiskCode[i]);
			transferData.setNameAndValue("DutyCode", tDutyCode[i]);
			transferData.setNameAndValue("Getlimit", mGetlimit);
			transferData.setNameAndValue("GetRate", mGetrate);
			break;
		}
	}
	if(!"Fail".equals(FlagStr)){
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			rateAndLimitModificationUI.submitData(tVData, "");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = rateAndLimitModificationUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	
	  
%>
<html>
<script language="javascript">	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>
