<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalCopyInput.jsp
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ProposalNo').value = '';
    fm.all('GrpProposalNo').value = '';
    fm.all('CValiDate').value = '';
    fm.all('RiskCode').value = '';
    fm.all('RiskVersion').value = '';
    fm.all('PayLocation').value = '';
    fm.all('PayIntv').value = '';
    fm.all('PayEndYear').value = '';
    fm.all('PayEndYearFlag').value = '';
    fm.all('GetYear').value = '';
    fm.all('GetYearFlag').value = '';
    fm.all('GetStartType').value = '';
    fm.all('InsuYear').value = '';
    fm.all('InsuYearFlag').value = '';
    fm.all('Mult').value = '';
    fm.all('Prem').value = '';
    fm.all('Amnt').value = '';
    fm.all('GrpNo').value = '';
    fm.all('Name').value = '';
    fm.all('LinkMan').value = '';
    fm.all('Phone').value = '';
    fm.all('Address').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Fax').value = '';
    fm.all('EMail').value = '';

  }
  catch(ex)
  {
    alert("在ProposalCopyInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ProposalCopyInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initInsuredGrid();
  }
  catch(re)
  {
    alert("ProposalCopyInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化InsuredGrid
 ************************************************************
 */
function initInsuredGrid()
  {                               
    var iArray = new Array();
      
      try
      {
		iArray[0]=new Array();
		iArray[0][0]="序号";         //列名
		iArray[0][1]="30px";         //列名
		iArray[0][2]=100;         //列名
		iArray[0][3]=0;         //列名
		
		iArray[1]=new Array();
		iArray[1][0]="被保人客户号";         //列名
		iArray[1][1]="160px";         //宽度
		iArray[1][2]=100;         //最大长度
		iArray[1][3]=1;         //是否允许录入，0--不能，1--允许
		
		iArray[2]=new Array();
		iArray[2][0]="姓名";         //列名
		iArray[2][1]="100px";         //宽度
		iArray[2][2]=100;         //最大长度
		iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[3]=new Array();
		iArray[3][0]="性别";         //列名
		iArray[3][1]="80px";         //宽度
		iArray[3][2]=100;         //最大长度
		iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[4]=new Array();
		iArray[4][0]="出生日期";         //列名
		iArray[4][1]="140px";         //宽度
		iArray[4][2]=100;         //最大长度
		iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
		
		iArray[5]=new Array();
		iArray[5][0]="婚姻状况";         //列名
		iArray[5][1]="100px";         //宽度
		iArray[5][2]=100;         //最大长度
		iArray[5][3]=1;         //是否允许录入，0--不能，1--允许
		
		iArray[6]=new Array();
		iArray[6][0]="职业类别";         //列名
		iArray[6][1]="100px";         //宽度
		iArray[6][2]=100;         //最大长度
		iArray[6][3]=1;         //是否允许录入，0--不能，1--允许
		
		iArray[7]=new Array();
		iArray[7][0]="健康状况";         //列名
		iArray[7][1]="100px";         //宽度
		iArray[7][2]=100;         //最大长度
		iArray[7][3]=1;         //是否允许录入，0--不能，1--允许
		
        InsuredGrid = new MulLineEnter( "fm" , "InsuredGrid" ); 

        //这些属性必须在loadMulLine前
        InsuredGrid.mulLineCount = 3;   
        InsuredGrid.displayTitle = 1;
        InsuredGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化InsuredGrid时出错："+ ex);
      }
    }

</script>