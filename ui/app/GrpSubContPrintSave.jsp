<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2009-7-15
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


<%
System.out.println("GrpSubContPrintSave.jsp start ...");

CErrors tError = null;
String FlagStr = "Fail";
String Content = "";
GlobalInput tG = (GlobalInput)session.getValue("GI");

String tActivityID = request.getParameter("ActivityID");
String tPrintType = request.getParameter("PrintType");
String mPrtNo = null;
String chk = "";

try
{

    String[] tChk = request.getParameterValues("InpGrpSubContGridChk");
    String[] tGrpSubContNo = request.getParameterValues("GrpSubContGrid1");
    String[] tInsuredNo = request.getParameterValues("GrpSubContGrid3");
    
    int tGSNum = tGrpSubContNo.length;
    LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
    for(int i = 0; i < tGSNum; i++)
    {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        if(tChk[i].equals("1"))  
        {  
            tLOPRTManagerSchema.setOtherNo(tGrpSubContNo[i] + "_" + tInsuredNo[i]);
            tLOPRTManagerSchema.setStandbyFlag1(tGrpSubContNo[i]);
            tLOPRTManagerSchema.setStandbyFlag2(tInsuredNo[i]);
            tLOPRTManagerSet.add(tLOPRTManagerSchema);
        }
    }
    
    VData tVData = new VData();
    
    String tCode = "";
    if(tPrintType != null && !"".equals(tPrintType)){
    	tCode = tPrintType;
    }else{
    	tCode = "GS001";
    }
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("Code", tCode);
    
    tVData.add(tLOPRTManagerSet);
    tVData.add(tTransferData);
    
//    tG.ClientIP = request.getRemoteAddr(); //操作员的IP地址
	tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
    System.out.println("------操作员的IP地址:"+tG.ClientIP);
  
    tVData.add(tG);
    
    GrpSubContBatchPrtBL tGrpSubContBatchPrtBL = new GrpSubContBatchPrtBL();
    if(!tGrpSubContBatchPrtBL.submitData(tVData, "batch"))
    {
        Content = " 打印失败，原因是: " + tGrpSubContBatchPrtBL.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 打印成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 打印失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("GrpSubContPrintSave.jsp end ...");

%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit2("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
