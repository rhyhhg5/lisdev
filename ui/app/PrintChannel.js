//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPrintChannelGrid();
	
	var strSQL = "select Code, CodeAlias, Code1, case Code1 when '1' then '个单' when '2' then '团单' end, CodeName, CodeName('printchanneltype', CodeName) "
	    + "from LDCode1 where CodeType = 'printchannel' "
	    + getWherePart('Code', 'manageComQuery', 'like')
	    + getWherePart('Code1', 'contTypeQuery')
	    + getWherePart('CodeName', 'printChannelQuery')
	    + " order by Code";

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}

	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	turnPage.pageLineNum = 10 ;
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = PrintChannelGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL ;
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

//显示打印渠道配置详细信息
function printChannelDetail()
{
    var row = PrintChannelGrid.getSelNo() - 1;
    fm.manageCom.value = PrintChannelGrid.getRowColDataByName(row, "manageCom");
    fm.manageComName.value = PrintChannelGrid.getRowColDataByName(row, "manageComName");
    fm.contType.value = PrintChannelGrid.getRowColDataByName(row, "contType");
    fm.contTypeName.value = PrintChannelGrid.getRowColDataByName(row, "contTypeName");
    fm.printChannel.value = PrintChannelGrid.getRowColDataByName(row, "printChannel");
    fm.printChannelName.value = PrintChannelGrid.getRowColDataByName(row, "printChannelName");
}

//保存按钮对应操作
function submitForm()
{
    var sql = "select 1 from LDCode1 where CodeType = 'printchannel' and Code = '" + fm.manageCom.value + "' and Code1 = '" + fm.contType.value + "'";
    var arrResult = easyExecSql(sql);
	if (arrResult)//存在则不可继续描述
	{
	    alert("已存在该公司该保单类型的打印渠道配置！请点击修改！");
	    return;
	}
    fm.operate.value = "INSERT";
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.submit(); //提交
}

//修改按钮对应操作
function updateForm()
{
    var sql = "select 1 from LDCode1 where CodeType = 'printchannel' and Code = '" + fm.manageCom.value + "' and Code1 = '" + fm.contType.value + "'";
    var arrResult = easyExecSql(sql);
	if (!arrResult)//存在则不可继续描述
	{
	    alert("不存在该公司该保单类型的打印渠道配置！请点击保存！");
	    return;
	}
    fm.operate.value = "UPDATE";
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    window.focus();
    try
    {
	  showInfo.close();
	}
    catch(ex)
    {}
    easyQueryClick();
    initInpBox();
}