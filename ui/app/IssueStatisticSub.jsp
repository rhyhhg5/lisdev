<% //程序名称：IssueStatisticSub.jsp 
	//程序功能：F1报表生成 
	//创建日期：2005-09-16 
	//创建人  ：St.GN 
	//更新记录：  更新人    更新日期     更新原因/内容 
%>
<%@page contentType="text/html;charset=GBK" %> 
<%@page import="com.sinosoft.utility.*"%> 
<%@page import="com.sinosoft.report.f1report.*"%> 
<%@page import="com.sinosoft.lis.pubfun.*"%> 
<%@page import="com.sinosoft.lis.schema.*"%> 
<%@page import="com.sinosoft.lis.vschema.*"%> 
<%@page import="com.sinosoft.lis.f1print.*"%> 
<%@page import="java.io.*"%> 
<% System.out.println("start");
CError cError = new CError();
boolean operFlag=true;
String FileName ="IssueStatistic";
String flag = "0";
String FlagStr = "";
String tRela  = "";
String Content = "";
String strOperation = "";
String OperatorManagecom = "";
String StartDate = request.getParameter("StartDate");
String EndDate = request.getParameter("EndDate");
String IssuePolType =   request.getParameter("IssuePolType");
String ManageCom =   request.getParameter("ManageCom");
String ManageCom1="";
String ManageCom2="";
String QYType1="";
String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
OperatorManagecom=tG.ManageCom;
if(sd.compareTo(ed) > 0)
{
  flag = "1";
  FlagStr = "Fail";
  Content = "操作失败，原因是:统计止期比统计统计起期早";
}
if(ManageCom.length() != 8)
{
  ManageCom1="length('"+ManageCom+"')*2";
  ManageCom2="length('"+ManageCom+"')";
}
else
{
  ManageCom1="length('"+ManageCom+"')";
  ManageCom2="length('"+ManageCom+"')";
}

System.out.println(ManageCom1);
System.out.println(ManageCom2);
if(IssuePolType.equals("1"))
{
  QYType1="code in ('85')";
}
System.out.println(QYType1);

if(IssuePolType.equals("2"))
{
  QYType1="code in ('03')";
}
System.out.println(QYType1);


if(IssuePolType.equals("3"))
{
  QYType1="code in ('05')";
}
System.out.println(QYType1);

if(IssuePolType.equals("4"))
{
  QYType1="code in ('06')";
}
System.out.println(QYType1);

if(IssuePolType.equals("5"))
{
  QYType1="code in ('03','05','06','85')";
}
System.out.println(QYType1);
JRptList t_Rpt = new JRptList();
String tOutFileName = "";
if(flag.equals("0"))
{
  t_Rpt.m_NeedProcess = false;
  t_Rpt.m_Need_Preview = false;
  t_Rpt.mNeedExcel = true;

  StartDate = AgentPubFun.formatDate(StartDate, "yyyy-MM-dd");
  EndDate = AgentPubFun.formatDate(EndDate, "yyyy-MM-dd");
  String CurrentDate = PubFun.getCurrentDate();
  CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");

  t_Rpt.AddVar("StartDate", StartDate);
  t_Rpt.AddVar("EndDate", EndDate);
  t_Rpt.AddVar("ManageCom",ManageCom);
  t_Rpt.AddVar("IssuePolType",IssuePolType);
  t_Rpt.AddVar("ManageCom1",ManageCom1);
  t_Rpt.AddVar("ManageCom2",ManageCom2);
  t_Rpt.AddVar("QYType1",QYType1);
  t_Rpt.AddVar("ManageComName", ReportPubFun.getMngName(OperatorManagecom));
  String YYMMDD = "";
  YYMMDD = StartDate.substring(0, StartDate.indexOf("-")) + "年"
           + StartDate.substring(StartDate.indexOf("-") + 1,StartDate.lastIndexOf("-")) + "月"
           + StartDate.substring(StartDate.lastIndexOf("-") + 1) + "日";
  t_Rpt.AddVar("StartDateN", YYMMDD);
  YYMMDD = "";
  YYMMDD = EndDate.substring(0, EndDate.indexOf("-")) + "年"
           + EndDate.substring(EndDate.indexOf("-") + 1,EndDate.lastIndexOf("-")) + "月"
           + EndDate.substring(EndDate.lastIndexOf("-") + 1) + "日";
  t_Rpt.AddVar("EndDateN", YYMMDD);
  YYMMDD = "";
  YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
           + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
           + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
  t_Rpt.AddVar("MakeDate", YYMMDD);

t_Rpt.Prt_RptList(pageContext,FileName);
tOutFileName = t_Rpt.mOutWebReportURL;
String strVFFileName = 
FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
String strRealPath = 
application.getRealPath("/web/Generated").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName; 
System.out.println("strVFPathName : "+ strVFPathName); 
System.out.println("=======Finshed in JSP==========="); 
System.out.println(tOutFileName);

response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+
"&RealPath="+strVFPathName);
}
%>
<form name="fm"  method="post"  action="../web/ShowF1Report.jsp" >
 <input name="FileName" type="hidden" value="<%=tOutFileName%>">
 </form>

 </body>
 </html>
<script language="javascript">
var flag1 = <%=flag%>;
if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
  alert("<%=Content%>");
}
</script >
