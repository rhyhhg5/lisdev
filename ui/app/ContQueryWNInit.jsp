<%
//程序名称：ContQueryWNInit.jsp
//程序功能：契约银代界面初始化
//创建日期：2007-11-9
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!--引入必要的java类  -->
<%@page import="com.sinosoft.lis.pubfun.*"%>	

<% 
	//获取传入的参数
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	String tCurrentDate = PubFun.getCurrentDate();
%>
<script language="JavaScript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var tCurrentDate = "<%=tCurrentDate%>";
  
 //输入框的初始化
function initInpBox()
{
	try
	{
	fm.all('PrtNo').value = "";
    fm.all('ContNo').value = "";
    fm.all('ManageComName').value = "";
    fm.all('SaleChnl').value = "";
    fm.all('SaleChnlName').value = "";
    fm.all('AppntName').value = "";
    fm.all('CValiDate').value = "";
    fm.all('State').value = "";
    fm.all('StateName').value = "";
    
    fm.all('ManageCom').value = manageCom;
    fm.CValiDateEnd.value = tCurrentDate;
    var sql = "select Current Date - 3 month from dual ";
    fm.CValiDateStart.value = easyExecSql(sql);
	}
	catch(ex)
	{
		alert("在ContQueryWNInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

//初始化表单
function initForm()
{ 
//initTaskInfo();  //初始化信息，函数在*.js中定义
try 
  {
    initInpBox();
    initLCContGrid(); 
    initElementtype();
  }
  catch(re) 
  {
    alert("在QYBankInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//工单列表Mulline的初始化
var LCContGrid;
function initLCContGrid()     
{                         
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";    //列宽
      iArray[0][2]=200;      //列最大值
      iArray[0][3]=0;        //是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="印刷号码";      //列名
      iArray[1][1]="60px";        //列宽
      iArray[1][2]=200;           //列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="合同号码";      //列名
      iArray[2][1]="90px";        //列宽
      iArray[2][2]=200;           //列最大值
      iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="管理机构";      //列名
      iArray[3][1]="60px";        //列宽
      iArray[3][2]=200;           //列最大值
      iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
      iArray[4]=new Array();
      iArray[4][0]="销售渠道";      //列名
      iArray[4][1]="60px";        //列宽
      iArray[4][2]=200;           //列最大值
      iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="投保人";      //列名
      iArray[5][1]="60px";        //列宽
      iArray[5][2]=200;           //列最大值
      iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="生效日期";      //列名
      iArray[6][1]="60px";        //列宽
      iArray[6][2]=200;           //列最大值
      iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="状态";      //列名
      iArray[7][1]="40px";        //列宽
      iArray[7][2]=200;           //列最大值
      iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="保费";      //列名
      iArray[8][1]="60px";        //列宽
      iArray[8][2]=200;           //列最大值
      iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="当前帐户余额";      //列名
      iArray[9][1]="80px";        //列宽
      iArray[9][2]=200;           //列最大值
      iArray[9][3]=0;             //是否允许输入,1表示允许，0表示不允许
            
 	  LCContGrid = new MulLineEnter("fm", "LCContGrid"); 
	  //设置Grid属性
      LCContGrid.mulLineCount = 10;
      LCContGrid.displayTitle = 1;
      LCContGrid.locked = 1;
      LCContGrid.canSel = 0;	
      LCContGrid.canChk = 0;
      LCContGrid.hiddenSubtraction = 1;
      LCContGrid.hiddenPlus = 1;
      LCContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert("在ContQueryWNInit.jsp-->initLCContGrid函数中发生异常:初始化界面错误!");
  }
}
</script>
