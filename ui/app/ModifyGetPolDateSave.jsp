<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：
//程序功能：
//创建日期：2009-01-19
//创建人  ：GUZG
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.tb.*"%> 
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>


<%
String FlagStr = "Fail";
String Content = "";

String tContNo = request.getParameter("ContNo");
String tGetPolDate = request.getParameter("GetPolDateNew");
String tSaleState=request.getParameter("SaleState");
String tWageNo=request.getParameter("WageNo");
GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ContNo", tContNo);
    tTransferData.setNameAndValue("GetPolDate", tGetPolDate);
    tTransferData.setNameAndValue("SaleState", tSaleState);
    tTransferData.setNameAndValue("WageNo", tWageNo);
    tVData.add(tTransferData);
    tVData.add(tGI);
    
    ModifyGetPolDateUI tModifyGetPolDateUI = new ModifyGetPolDateUI();
    if(!tModifyGetPolDateUI.submitData(tVData, null))
    {
        Content = "修改回执回销日期失败，原因是: " + tModifyGetPolDateUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 修改回执回销日期成功！";
        FlagStr = "Succ";
    }
}
catch (Exception e)
{
    Content = " 修改回执回销日期失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

%>

<html>
<script language="javascript">
    parent.fraInterface.afterImportSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
