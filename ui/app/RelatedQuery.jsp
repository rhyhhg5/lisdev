<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
	tGI=(GlobalInput)session.getValue("GI");
%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<SCRIPT src="RelatedQuery.js"></SCRIPT>
<%@include file="RelatedQueryInit.jsp"%>
<title>关联交易查询</title>
</head> 
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询关联方交易条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
      	  <TD  class= title>
            交易对象：
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
          <TD  class= title>
            关联方类别：
          </TD>
          <TD  class= input>
            <Input class=codeno name=RelatedType CodeData="0|1^1|个人关联方^2|团体关联方" ondblclick="return showCodeListEx('RelatedType',[this,RelatedTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('RelatedType',[this,RelatedTypeName],[0,1],null,null,null,1);"><input class=codename name=RelatedTypeName readonly=true>
          </TD>
          <TD  class= title>
            证件号码：
          </TD>
          <TD  class= input>
            <Input class= common name=IDNo >
          </TD>
        </TR>
        <TR>
        <TD  class= title>
            承保机构：
        </TD>
        <TD  class= input>
           <Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
        </TD>
        	<TD class=title>
        		承保日期起期：
        	</TD>
        	<TD class=input>
				<Input class="coolDatePicker" dateFormat="short" name=StartDate verify="承保日期起|date&notnull">
			</TD>
			<TD class=title>
				承保日期止期：
			</TD>
			<TD class=input>
				<Input class="coolDatePicker" dateFormat="short" name=EndDate verify="承保日期止|date&notnull">
			</TD>
			</TR>
        <TR>
        <TD  class= title>
            承保渠道：
          </TD>
          <TD  class= input>
            <Input class=codeNo name=SaleChnl ondblclick="return showCodeList('unitesalechnlsgrp',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('unitesalechnlsgrp',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true >
          </TD>
			<TD class=title>
        		交易金额下限：
        	</TD>
        	<TD class=input>
				<Input class=common name="PremX">
			</TD>
			<TD class=title>
				交易金额上限：
			</TD>
			<TD class=input>
				<Input class=common name="PremS">
			</TD>
        </TR>
    </table>
          <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
          <input type=button value="下  载" class=cssButton onclick="DoNewDownload();">
          <input type=hidden name=querySql value="">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRelatedParty);">
    		</td>
    		<td class= titleImg>
    			 关联方交易查询结果：
    		</td>
    	</tr>
    	<tr>
    	</tr>
    </table>
  	<Div  id= "divRelatedParty" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanRelatedPartyGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
  	</Div>
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
