<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BaseStatisticSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
boolean errorFlag = false;

//获得session中的人员信息

GlobalInput tG = (GlobalInput)session.getValue("GI");

//生成文件名
Calendar cal = new GregorianCalendar();
String min=String.valueOf(cal.get(Calendar.MINUTE));
String sec=String.valueOf(cal.get(Calendar.SECOND));
String type= request.getParameter("QYType"); //下载类型
String downLoadFileName = "";
String mStartDate=request.getParameter("StartDate");//扫描起期
String mEndDate=request.getParameter("EndDate");//扫描止期
String filePath = application.getRealPath("temp");

String querySql = request.getParameter("querySql");

querySql = querySql.replaceAll("%25","%");

//设置表头
String[][] tTitle = new String[1][];

if(type.equals("1")){
	downLoadFileName = "新单差错件报表_"+tG.Operator+"_"+ min + sec + ".xls";
// 	tTitle[0] = new String[] { "制表人：" , tG.Operator, "","统计时间：" +mStartDate+"至"+mEndDate,"","","","","","","","","","","","","",""};
    tTitle[0] = new String[] {"省级机构","省级机构名称","管理机构","管理机构名称","印刷号","投保人姓名","销售渠道","业务员编码","业务员姓名","营销机构编码","营销机构名称","扫描日期","扫描员工号","初审人员工号","外包批次号","外包状态","是否进入外包错误修改","发送外包日期"};
    
}else if(type.equals("2")){
	downLoadFileName = "新单删除件报表_"+tG.Operator+"_"+ min + sec + ".xls";	
// 	tTitle[0] = new String[] { "制表人：" , tG.Operator, "","统计时间：" +mStartDate+"至"+mEndDate,"","","","","","","","","","","",""};
    tTitle[0] = new String[] {"省级机构","省级机构名称","管理机构","管理机构名称","保单类型","印刷号","投保人姓名","销售渠道","业务员编码","业务员姓名","营销机构编码","营销机构名称","扫描日期","扫描员工号","初审人员工号","投保件状态"};
 }

String tOutXmlPath = filePath +File.separator+ downLoadFileName;
System.out.println("OutXmlPath:" + downLoadFileName);
//表头的显示属性
int[] displayTitle1 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
int[] displayTitle2 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
//数据的显示属性
int[] displayData1 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
int[] displayData2 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};

CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
createexcellist.createExcelFile();
String[] sheetName ={"list"};
createexcellist.addSheet(sheetName);

if(type.equals("1")) {
	int row = createexcellist.setData(tTitle,displayTitle1);
	if(row ==-1) errorFlag = true;
// 	createexcellist.setRowColOffset(row+1,0);//设置偏移
	if(createexcellist.setData(querySql,displayData1)==-1) {
		errorFlag = true;
		System.out.println(errorFlag);
	}
}else if(type.equals("2")) {
	int row = createexcellist.setData(tTitle,displayTitle2);
	if(row ==-1) errorFlag = true;
// 	createexcellist.setRowColOffset(row+1,0);//设置偏移
	if(createexcellist.setData(querySql,displayData2)==-1) {
		errorFlag = true;
		System.out.println(errorFlag);
	}
}

if(!errorFlag)
//写文件到磁盘
try{
   createexcellist.write(tOutXmlPath);
}catch(Exception e) {
	errorFlag = true;
	System.out.println(e);
}
//返回客户端
if(!errorFlag) downLoadFile(response,filePath,downLoadFileName);
out.clear();
out = pageContext.pushBody();
%>
