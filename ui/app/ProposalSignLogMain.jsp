<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：ProposalSignLogMain.jsp
//程序功能：工单管理工单批注页面
//创建日期：2007-12-20 10:41上午
//创建人  ：Yangyalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
<title>签单日志处理</title>
</head>

<html>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="1">
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	<!—表单提交帧，必须有，在提交时指向本帧-->
	<frame name="fraSubmit"  scrolling="yes" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="*,0" cols="*">
    <!—页面展现帧，src一般指向需要展现的页面文件-->
		<frame id="fraInterfaceView" name="fraInterface" scrolling="auto" src="ProposalSignLog.jsp?LoadFlag=Test">
		<frame id="fraNext" name="fraNext" scrolling="auto" src="">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
