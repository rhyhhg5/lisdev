var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var i = 0;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
    var tSignStartDate = fm.SignStartDate.value;
    var tSignEndDate = fm.SignEndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>12)
	{
		alert("统计期最多为12个月！");
		return false;
	}
    var tManageCom = fm.ManageCom.value;
    var tSaleChnl = fm.SaleChnl.value;  
    var strSQL = " select a.managecom,a.prtno,getUniteCode(a.agentcode),(select name from laagent where agentcode =a.agentcode), "
             + "b.branchattr,b.name,codename('lcsalechnl',a.salechnl),a.approvedate,codename('uwflag',a.uwflag), "
             + "a.UWdate,"
             +" (select min(MakeDate) from loprtmanager where otherno=a.ProposalContno and OtherNoType = '02' and code='85' ),"
             +" (select max(edm.MakeDate) from loprtmanager lop,es_doc_main edm where lop.otherno=a.ProposalContno and lop.OtherNoType = '02' and lop.code='85' and edm.doccode = lop.prtseq ),"
             +" (select min(MakeDate) from loprtmanager where otherno=a.ProposalContno and OtherNoType = '02' and code='03' ),"
             +" (select max(edm.MakeDate) from loprtmanager lop,es_doc_main edm where lop.otherno=a.ProposalContno and lop.OtherNoType = '02' and lop.code='03' and edm.doccode = lop.prtseq ),"
             +" (select min(MakeDate) from loprtmanager where otherno=a.ProposalContno and OtherNoType = '02' and code='04' ),"
             +" (select max(edm.MakeDate) from loprtmanager lop,es_doc_main edm where lop.otherno=a.ProposalContno and lop.OtherNoType = '02' and lop.code='04' and edm.doccode = lop.prtseq ),"
             +" a.UWoperator,case when a.UWdate is not null then  to_date(a.UWdate)-to_date(a.approvedate) else 0 end from lccont a,labranchgroup b where a.agentgroup = b.agentgroup and a.conttype='1' and a.approvedate  between " 
             + "'"+tStartDate+"' and '"+tEndDate+"'"
             + getWherePart('a.SaleChnl','SaleChnl')
             + " and a.managecom like '"+tManageCom+"%' and not exists (select 1 from lcpol where renewcount<>0 and contno=a.contno) "
             + " and not exists (select 1 from lbcont where prtno=a.prtno ) "
             + " and a.cardflag='0' ";
    if(tSignStartDate!="" && tSignStartDate!=null && tSignStartDate!="null"){
    	strSQL += " and a.signdate>='"+tSignStartDate+"' ";
    }
    if(tSignEndDate!="" && tSignEndDate!=null && tSignEndDate!="null"){
    	strSQL += " and a.signdate<='"+tSignEndDate+"' ";
    }
    strSQL += " order by a.managecom,b.branchattr,a.agentcode with ur ";
           
    fm.AnalysisSql.value = strSQL; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}  
	fm.submit();
}