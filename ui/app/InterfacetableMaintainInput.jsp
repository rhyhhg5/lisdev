<html>
<%
//程序名称 :InterfacetableMaintainInput.jsp
//程序功能 :接口表信息维护
//创建人 :
//创建日期 :
//
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。  
 %>
<script>
  var comcode = "<%=tGI1.ComCode%>";
 

</script>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
<SCRIPT src = "InterfacetableMaintainInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="InterfacetableMaintainInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./InterfacetableMaintainSave.jsp" method=post name=fm target="fraSubmit">
<Div id= "divLLReport1" style= "display: ''">
<strong><IMG id="a1" src="../common/images/butExpand.gif"
style="cursor:hand;" OnClick="showPage(this,divOperator);" />接口表信息维护</strong>
  	<table>
    	<tr>
    		 <td class= titleImg>请输入查询条件</td>   		 
    	</tr>
    </table>

 <table class= common border=0 width=100%>
	<table  class= common>
        <TR  class= common>
		<TD class=title>批次号</TD>
		<TD class=input><Input class="common" name=BatchNo elementtype=nacessary></TD>
		
		<TD class=title>流水号</TD>
		<TD class=input><Input class="common" name=SerialNo ></TD>
		
		<TD class=title>成本中心</TD>
		<TD class=input><Input class="common" name=CostCenter></TD>
	</TR> 
		
		<TR  class= common>
			<TD class=title>渠道</TD>
			<TD class=input><Input class="common" name=Chinal></TD>
			
			<TD class=title>记账日期</TD>
			<TD class=input><Input class="coolDatePicker"
			verify="记账日期|notnull&date" dateFormat="short" name=ChargeDate></TD>	
			
			<TD class=title>读取状态</TD>
			<TD class=input>
				<Input class=codeno name=StateF CodeData="0|^1|读取数据^2|读取成功^3|读取失败^4|尚未读取" verify="读取状态|NOTNULL"
				ondblClick="showCodeListEx('StateF',[this,StateFName],[0,1],null,null,null,[1]);"
				onkeyup="showCodeListKeyEx('StateF',[this,StateFName],[0,1],null,null,null,[1]);">
			<Input class="codename" name=StateFName readonly=true elementtype=nacessary></TD>
       
        </TR>  
            
 </table> 
  <INPUT VALUE="查询" class=cssButton TYPE=button onclick="Query();">
  <input value="增加" id=addbutton  class=cssButton type=button onclick="Add();">
  <input value="修改" id=updatebutton  class=cssButton type=button onclick="Update();">
  <input value="删除" id=deletebutton  class=cssButton type=button onclick="Delete();">
    
<hr width=98%>
	<Div  id= "divInterfacetableMaintainGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanInterfacetableMaintainGrid" >
  					</span> 
  			  	</td>
  			</tr>					

 
</table>	
      <input class=cssButton value="首页"    type=button  onclick="turnPage.firstPage();"></input>
      <input class=cssButton value="上一页"  type=button  onclick="turnPage.previousPage();"></input>
      <input class=cssButton value="下一页"  type=button  onclick="turnPage.nextPage();"></input>
      <input class=cssButton value="尾页"    type=button  onclick="turnPage.lastPage();"></input>
     
 </div>
     <br>
     <input name=sql type=hidden class=common>
     <input name=mOperate type=hidden class=common>
     </table>
     </Div>
     </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>