<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "清单_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String querySql = request.getParameter("querySql");
    querySql = querySql.replaceAll("%25","%");
    //设置标题
    String[][] global = {{"机构：",request.getParameter("ManageCom"),"","",
        "制表时间：",PubFun.getCurrentDate()}};
    //表头的显示属性
    int []displayGlobal = {1,2,3,4,5,6};
    //设置表头
    String[][] tTitle = {{"管理机构代码","销售渠道","保险合同号","投保书印刷号","状态","保全结案日期 ","销售机构","销售人员代码 ","销售人员姓名","投保人","投保人性别","投保人生日 ","被保险人","被保人性别","被保人生日","险种代码","险种名称","档次","保额","保费","交费频次","生效日期","核保人员","核保结论","收费方式","签单日期","回执回销日期"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
        21,22,23,24,25,26,27};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
        21,22,23,24,25,26,27};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list", "help"};
    createexcellist.addSheet(sheetName);
    int row;
    //row = createexcellist.setData(global,displayGlobal);
    //if(row ==-1) errorFlag = true;
    row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
    createexcellist.setRowColOffset(row+1,0);//设置偏移
    row = createexcellist.setData(querySql,displayData);
    if(row == -1)
        errorFlag = true;

    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>

