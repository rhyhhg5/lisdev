<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="YBCBDetailInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="">    
<form action="" method=post name=fm target="fraSubmit">

    <table class="common">
        <tr class="common">
            <td class="title">管理机构</td>
            <td class="input">
                <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
            </td>
            <td class="title">签单日期起</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="StartSignDate" verify="投保日期|date&notnull" elementtype="nacessary" />
            </td>
            <td class="title">签单日期止</td>
            <td class="input">
                <input class="coolDatePicker" dateFormat="short" name="EndSignDate" verify="投保日期|date&notnull" elementtype="nacessary" />
            </td>
        </tr>
      </TR>
			<TR  class= common>
				<TD  class= title> 销售渠道 </TD>
				<TD  class= Input><input class=codeno  name=saleChn CodeData= "0|^00|全部^04|银行代理^13|银代直销"  ondblclick="return showCodeListEx('saleChn',[this,saleChnlName1],[0,1]);" onkeyup="return showCodeListEx('saleChn',[this,saleChnlName1],[0,1]);"><input class=codename name=saleChnlName1></TD>
	            <td class=title>产品类型</td>
		        <td class=input><Input name="productType" class=codeno  CodeData="0|^00|全部^01|高现价产品^02|非高现价产品" ondblclick="return showCodeListEx('productType',[this,productTypeName],[0,1]);" onkeyup="return showCodeListEx('productType',[this,productTypeName],[0,1]);" ><input class=codename name=productTypeName ></td>
	            <TD  class= title> 缴费频次</TD>
				<TD  class= Input><input class=codeno  name="payintvCode"  CodeData= "0|^00|全部^01|趸交^02|期交"  ondblclick="return showCodeListEx('payintvCode',[this,payintv_ch],[0,1]);" onkeyup="return showCodeListEx('payintvCode',[this,payintv_ch],[0,1]);"><input class=codename name=payintv_ch></TD>
		</TR> 
		<tr class=common>
		<TD class=title>是否是银保通出单</TD>
		<td class=title><Input class="codeno" name=outFlag
			verify="是否是银保通出单" CodeData="0|^0|否|^1|是"
			ondblclick="return showCodeListEx('outFlag',[this,outFlagName],[0,1]);"
			onkeyup="return showCodeListKeyEx('outFlag',[this,outFlagName],[0,1]);"><input
			class=codename name=outFlagName readonly=true></td>			
	</tr>             
    </table>
    
    <br />
    
    <input value="下  载" class="cssButton" type="button" onclick="submitForm();" />
    
    <hr />
    
    <div>
        <font color="red">
            <ul>报表说明：
                <li>本报表是按照保单签发时间为口径的银保保单明细报表。</li>                
                <li>本报表包括：犹豫期撤保/退保/投保撤单的情况。</li>
                <li>本报表只包含首次承保的保单，不包括续保保单。</li>
                <li>承保日期查询口径：这里指保单的签单日期。</li>
            </ul>
            <ul>名词解释：
                <li>承保时间：这里指保单签发的时间，通常认为是财务到账确认日期。</li>
                <li>保费确认日：采用“生效日期”和“保单签发日期”孰后的原则。</li>
                <li>被保人总数：统计被保人数量。相同客户号仅统计一次。</li>
                <li>总保费：期缴保费 + 追加保费</li>
            </ul>
            <ul>统计条件：
                <li>管理机构、签单日期段、销售渠道、缴费频次、产品类型。</li>
                <li>签单日期段间隔不超过十二个月。</li>
            </ul>
        </font>
    </div>
    
    <input name="querySql" type="hidden" />
    <input type="hidden" name=op value="" />
	
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 