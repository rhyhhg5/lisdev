<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	String tPrtNo = request.getParameter("PrtNo");
%>
<script>
	var ComCode = "<%=tGI.ManageCom%>";
	var tOperator = "<%=tGI.Operator%>";
	var tPrtNo = <%=tPrtNo%>;
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<script src="./InsertLCGrpContLoadInput.js"></script>
		<%@include file="./InsertLCGrpContLoadInit.jsp"%>
	
</head>
<body onload="initForm();">
	<form action="InsertLCGrpContLoadSave.jsp" method=post name=fm target="fraSubmit">
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divQuery);">
				</td>
				<td class=titleImg>一带一路信息</td>
			</tr>
		</table>
<div id="divQuery" style="display:''">
			<table class=common>
				<tr class=common>
					<TD class=title8>国家类别</TD>
					<TD class=input>
						<Input class=codeNo name=CountryCategory
						verify="国家系数|notnull"
						ondblclick="return showCodeList('Country',[this,CountryName],[0,1],null,null,null,1);"
						onkeyup="return showCodeListKey('Country',[this,CountryName],[0,1],null,null,null,1);"><Input
						class=codename name="CountryName" readonly=true></TD>
				</tr>
				<tr class=common>
					<TD class=title8>国家/地区名称</TD>
					<TD class=input>
						<Input class= common name=Country  >
					</TD>
				</tr>
				<tr class=common>
					<td class="title8">被保险人在境外是否参与建筑施工项目？&nbsp&nbsp&nbsp&nbsp</td>
					<td class="title8"><input type="radio" name="flaga"
						class="box" onclick="onclickEngineering(1);">&nbsp&nbsp
						参与/ <input class="common7"> <input type="radio"
						name="flagb" class="box" onclick="onclickEngineering(2);">&nbsp&nbsp
						不参与 <input class="common7"></td>
				</tr>
				<TR class=common style="display: 'none'" id="divEngineering">
					<TD class=title8>工程类别</TD>
					<TD class=input8><Input class=codeNo name=Engineering
						ondblclick="return showCodeList('Engineering',[this,EngineeringName],[0,1],null,null,null,1);"
						onkeyup="return showCodeListKey('Engineering',[this,EngineeringName],[0,1],null,null,null,1);"><Input
						class=codename name="EngineeringName" readonly=true></TD>
				</TR class=common>
				<tr>
	      			<TD class=title8>标准保费（总保费）</TD>
			     	<TD class=input8>
			        	<Input class="readonly" readonly name=CalculPrem>
			      	</TD>
			     </tr>
			     <tr>
				    <TD class=title8>浮动比例</TD>
				 	<TD class=input8>
				        <Input class="readonly" readonly name=TotalFactor>
			      	</TD>
				</tr>
			</table>
		</div>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="EngineeringFlag" name="EngineeringFlag">
		<input type=hidden id="PrtNo" name="PrtNo">
		<input type=hidden id="SerialNo" name="SerialNo">
		<input type=button class=cssButton value=" 保  存 " name=Save onclick="save();" style="display:''">
<!-- 		<input type=button class=cssButton value=" 修  改 " name=Update onclick="update();" style="display:''"> -->
<!-- 		<input type=button class=cssButton value=" 删  除" name=Delete onclick="deletee();" style="display:''"> -->
		<input type=button class=cssButton value=" 退  出 " name=Exit onclick="cancle();" style="display:''">
	</form>
	<span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>