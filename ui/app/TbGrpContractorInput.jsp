<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：HealthArchiveInput.jsp
//程序功能：F1报表生成
//创建日期：2005-07-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="TbGrpContractorInput.js"></SCRIPT>   
		<%@include file="TbGrpContractorInit.jsp"%>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<script>
	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
	<%-- var PrtNo="<%=request.getParameter("PrtNo")%>"; --%>
	var LoadFlag="<%=request.getParameter("LoadFlag")%>";
	//alert(LoadFlag);
</script>
<body onload="initForm();initElementtype();">    
  <form action="./TbGrpContractorSave.jsp" method=post name=fm target="fraSubmit">
  	<DIV id=DivLCContButton STYLE="display:''">
			<table id="table1">
					<tr>
						<td>
						<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroupPol1);">
						</td>
						<td class="titleImg">建工要素定义
						</td>
					</tr>
			</table>
		</DIV>
		<Div  id= "divLCPol2" style= "display: ''">
			<table  id= "AddressTab"  cellspacing="1" border="0"  style="font-size:13px;" width='54%' style= "display:''">
			<TD  class="titleImg1" >
          施工地址录入：
          </TD>
			
          <tr>
        	<TD   bgcolor="#F7F7F7" width=5%>
            省（自治区直辖市）
          </TD>
          <TD   bgcolor="#F7F7F7" width=7%>
      		<Input name=ProvinceID class=fcodeno ondblclick="return showCodeList('Province1',[this,Province],[0,1],null,'0','Code1',1);"  onkeyup="return showCodeListKey('Province1',[this,Province],[0,1],null,'0','Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=Province class= fcodename style="width:75%"   readonly="readonly">
          </TD>
          <TD  bgcolor="#F7F7F7"  width=2.5% >
            市
          </TD>
          <TD   bgcolor="#F7F7F7"   width=5%>
              <Input name=CityID class=fcodeno ondblclick="return showCodeList('City1',[this,City],[0,1],null,fm.ProvinceID.value,'Code1',1);"  onkeyup="return showCodeListKey('City1',[this,City],[0,1],null,fm.ProvinceID.value,'Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=City class= fcodename style="width:75%"    readonly="readonly">
          </TD>
         <tr>
          
  		 <TD   bgcolor="#F7F7F7"  width=5% >
           县        
          </TD>
          <TD  bgcolor="#F7F7F7"  width=5%>
              <Input name=CountyID class=fcodeno ondblclick="return showCodeList('County1',[this,County],[0,1],null,fm.CityID.value,'Code1',1);"  onkeyup="return showCodeListKey('County1',[this,County],[0,1],null,fm.CityID.value,'Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=County class= fcodename style="width:75%"  readonly="readonly">
          </TD>       
         <TD  bgcolor="#F7F7F7"  width=2.5%>
            详细地址
          </TD>
          <TD  bgcolor="#F7F7F7"  width=7.5%>
      		<Input class= common3 style="width:100%" name=DetailAddress  onblur="FillAddress()">
          </TD>
          	<tr>
			<TD  bgcolor="#F7F7F7"  width=3% >
            施工地址：
          </TD>	
          <TD  class= input8 colspan="3">
      		<Input class= common3 style="width:100%" name=ConstructAddress  readonly=true onblur="FillAddress()">
          </TD>
        </TR>
        </table>
        <tr  class= common>
        <table  class= common>
					<td text-align: left colSpan=1>
						<span id="spanConractorGrid" >
						</span> 
					</td>
				</tr>
			</table>
		</div>
		<div id="ButtonDiv" style="display: ''">
			<table class=common>
				<tr class=common>
					<td class=common>
						<INPUT VALUE="保  存" class=cssButton TYPE=button onclick="submitForm();">

						<INPUT VALUE="修  改" class=cssButton TYPE=button onclick="updateClick();">

						<INPUT VALUE="删  除" class=cssButton TYPE=button onclick="deleteClick();">
						
						<INPUT VALUE="返  回" class=cssButton TYPE=button onclick="top.close();">
					</td>
				</tr>
			</table>
		</div>
		
		<INPUT TYPE="hidden" name="PrtNo" VALUE= "<%=request.getParameter("PrtNo")%>">
		<INPUT TYPE="hidden" name="fmtransact" >
		<INPUT TYPE="hidden" name="GrpContNo" VALUE= "<%=request.getParameter("GrpContNo")%>">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 