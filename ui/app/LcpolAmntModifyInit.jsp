<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
                          
function initForm() {
  try {
      initLcpolAmntModifyGrid(); 
  }
  catch(re) {
    alert("初始化界面错误!");
  }
}

var LcpolAmntModifyGrid;
function initLcpolAmntModifyGrid() {                               
  var iArray = new Array();
    
  try {

      iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
      
      iArray[1]=new Array();
      iArray[1][0]="保单号码";         	  //列名
      iArray[1][1]="50px";         		  //列名
    
      iArray[2]=new Array();
	  iArray[2][0]="被保人客户号码";         	  //列名
	  iArray[2][1]="50px";            	  //列宽
	  iArray[2][2]=200;            		  //列最大值
	
      iArray[3]=new Array();
	  iArray[3][0]="险种号码";         	  //列名
	  iArray[3][1]="50px";            	  //列宽
	  iArray[3][2]=200;            		  //列最大值
	
      iArray[4]=new Array();           
	  iArray[4][0]="保额";                 //列名
	  iArray[4][1]="50px";            	  //列宽
	  iArray[4][2]=200;                   //列最大值
	  
	  iArray[5]=new Array();           
	  iArray[5][0]="polno隐藏";            //列名
	  iArray[5][1]="50px";            	  //列宽
	  iArray[5][2]=200;                   //列最大值
	  iArray[5][3]=3;                     //如果0：只读 1：可写 2：双击下拉 3：隐藏
	  
	  iArray[6]=new Array();           
	  iArray[6][0]="分单号码";            //列名
	  iArray[6][1]="50px";            	  //列宽
	  iArray[6][2]=200;                   //列最大值
	  iArray[6][3]=3;                     //如果0：只读 1：可写 2：双击下拉 3：隐藏

      LcpolAmntModifyGrid = new MulLineEnter( "fm" , "LcpolAmntModifyGrid" );
      //这些属性必须在loadMulLine前
      
      LcpolAmntModifyGrid.mulLineCount = 10;
      LcpolAmntModifyGrid.displayTitle = 1;

      LcpolAmntModifyGrid.canSel=1;
      LcpolAmntModifyGrid.canChk=0;
      LcpolAmntModifyGrid.hiddenPlus=1;
      LcpolAmntModifyGrid.hiddenSubtraction=1;

      LcpolAmntModifyGrid.loadMulLine(iArray);
      LcpolAmntModifyGrid.selBoxEventFuncName ="query";//设置js，反选用
      //这些操作必须在loadMulLine后面
	
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
