<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initGrpInfoGrid();
        initBatchDetailGrid();
        fm.all('ManageCom').value = <%=strManageCom%>;
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initGrpInfoGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			
        iArray[0][1]="30px";            		
        iArray[0][2]=10;            			
        iArray[0][3]=0;              			

        iArray[1]=new Array();
        iArray[1][0]="团体编号";         		
        iArray[1][1]="120px";            		
        iArray[1][2]=170;            			
        iArray[1][3]=0;              			

        iArray[2]=new Array();
        iArray[2][0]="客户单位名称";         		
        iArray[2][1]="120px";            		
        iArray[2][2]=100;            			
        iArray[2][3]=0;              			

        iArray[3]=new Array();
        iArray[3][0]="社会信用代码";         		
        iArray[3][1]="80px";            		
        iArray[3][2]=200;            			
        iArray[3][3]=0;              			

        iArray[4]=new Array();
        iArray[4][0]="税务登记证号码";         		
        iArray[4][1]="100px";            		
        iArray[4][2]=100;            			
        iArray[4][3]=0;              			
        
        iArray[5]=new Array();
        iArray[5][0]="法人代表";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="单位地址";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        

        GrpInfoGrid = new MulLineEnter("fm", "GrpInfoGrid"); 

        GrpInfoGrid.mulLineCount = 0;   
        GrpInfoGrid.displayTitle = 1;
        GrpInfoGrid.canSel = 1;
        GrpInfoGrid.hiddenSubtraction = 1;
        GrpInfoGrid.hiddenPlus = 1;
        GrpInfoGrid.canChk = 0;
        GrpInfoGrid.selBoxEventFuncName = "initGrpNo";
        GrpInfoGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化GrpInfoGrid时出错：" + ex);
    }
}

function initBatchDetailGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="团体编号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="批次号";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="申请日期";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="批次状态";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="申请机构";
        iArray[5][1]="60px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="申请人";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=0;        

        BatchDetailGrid = new MulLineEnter("fm", "BatchDetailGrid"); 

        BatchDetailGrid.mulLineCount = 0;   
        BatchDetailGrid.displayTitle = 1;
        BatchDetailGrid.canSel = 1;
        BatchDetailGrid.hiddenSubtraction = 1;
        BatchDetailGrid.hiddenPlus = 1;
        BatchDetailGrid.canChk = 0;
        BatchDetailGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化BatchDetailGrid时出错：" + ex);
    }
}
</script>