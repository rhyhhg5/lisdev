<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="ContractSign.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ContractSignInit.jsp"%>
  <title>合同签单 </title>
</head>
<body  onload="initForm();" >
  <form action="./ContractSignQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 合同信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询合同条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
    <table  class= common align=center>
      	<TR  class= common>
          <!--<TD  class= title>
            合同投保单号
          </TD>
          <TD  class= input>-->
            <Input class= common name=ProposalNo  type="hidden">
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            投保单位客户号
          </TD>
          <TD  class= input>
            <Input class= common name=GrpNo >
          </TD>
          <TD  class= title>
            投保单位名称
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentGroup ondblclick="return showCodeList('AgentGroup',[this]);" onkeyup="return showCodeListKey('AgentGroup',[this]);">
          </TD>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this]);" onkeyup="return showCodeListKey('AgentCode',[this]);">
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="submitForm();"> 
  </form>
  <form action="./ContractSignSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont1);">
    		</td>
    		<td class= titleImg>
    			 合同信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCCont1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button > 
      <INPUT VALUE="上一页" TYPE=button > 					
      <INPUT VALUE="下一页" TYPE=button > 
      <INPUT VALUE="尾页" TYPE=button > 					
  	</div>
  	<p>
      <INPUT VALUE="签发保单" TYPE=button onclick=""> 
  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
