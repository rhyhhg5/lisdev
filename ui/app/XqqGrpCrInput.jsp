<html>
<% 
                /*=============================================
                  Name：XqqGrpCrInput.jsp
                  Function： 集团交叉
                  Date：2014-9-30
                  Author：xieqingqing
                 =============================================*/
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
 <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="XppGrpCrsInputV.js"></SCRIPT> 
  <%@include file="XqqGrpCrsInputInit.jsp"%>
<title>集团交叉信息管理</title>
 <script language="javascript"></script>
</head>
<body onload="initForm();">
  <form action="../app/XqqGrpCrsSave.jsp" name=fm method=post target="fraSubmit">
 
     <table class=common border=0 width=100%>
     <tr>
       <td class=titleImg align=center>请输入查询条件：</td>
     </tr>
     </table>
     <div id="divLLMainAskInput" style="display:''">
      <table class=common align="center">
       <tr class=common>
         <tr class=common>
         <td class=title>保单类型</td><td class=input><Input class=codeno name=bdlx verify="保单类型|NOTNULL" CodeData="0|^0|团单^1|个单"  ondblClick="showCodeListEx('bdlx',[this,bdlxName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKeyEx('bdlx',[this,bdlxName],[0,1],null,null,null,[1]);" readonly=true ><input class=codename name=bdlxName readonly=true elementtype=nacessary></td>
                                                     
         <td class=title>保单号</td><td class=input><input class=common name=bdh verify="保单号|notnull&len=12" elementtype="nacessary"></td></tr>                           
         <tr class=common>
               <table class=common>
      <tr class= common><Input class=cssbutton type=button value="查 询"  onclick="grpCrsQuery()" name="addBlack"></tr>
      <tr class=common><td class= input ><input class=common name=mComCode type=hidden></td></tr>
      </table>
         
     <br>
     <br>
     </div>
     <table>
      <tr>
       <td>
         <img src="../common/images/butExpand.gif" style="cursor:hand;" onClick="showPag(this,divCustomerInfo);"/>
       </td>
       <td class= titleImg>
          集团交叉列表：
       </td>
      </tr>
     </table>
     <div id= "divLLMainAskInput4" align=center style= "display: ''">
     <table class= common>
      <tr class=common>
       <td text-aglin: left colSpan=1>
        <span id="spanQueryGrpCrsGrid"></span>
       </td>
      </tr>
     </table>
      <input class=cssButton value="首页"    type=button  onclick="turnPage.firstPage();"></input>
      <input class=cssButton value="上一页"  type=button  onclick="turnPage.previousPage();"></input>
      <input class=cssButton value="下一页"  type=button  onclick="turnPage.nextPage();"></input>
      <input class=cssButton value="尾页"    type=button  onclick="turnPage.lastPage();"></input>
     </div>
     <HR>
     <div id="divLLMainAskInput5" style="disply:'none'">
     <table class= common>
                   
         <tr>
         <td class=title style= "width: '5%'">集团交叉销售渠道</td>
         <td class=input><Input style= "width: '50%'" class=codeNo readonly=true name=Crs_Salechnl verify="销售渠道|code:Crs_Salechnl&NOTNULL&INT" ondblclick="return showCodeList('Crs_Salechnl',[this,Crs_SalechnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Crs_Salechnl',[this,Crs_SalechnlName],[0,1],null,null,null,1);"><Input class="codename" name=Crs_SalechnlName elementtype=nacessary><font size=1 color="#ff0000"></font></td>
         <td class=title style= "width: '5%'">集团销售业务类型</td>
         <td class=input><Input style= "width: '50%'" class=codeNo readonly=true name=Crs_Busstype verify="业务类型|code:Crs_Busstype&NOTNULL" ondblclick="return showCodeList('Crs_Busstype',[this,Crs_BusstypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Crs_Busstype',[this,Crs_BusstypeName],[0,1],null,null,null,1);"><Input class="codename" name=Crs_BusstypeName elementtype=nacessary><font size=1 color="#ff0000"></font></td>
       </tr>
     
                    
                 
					<TR class= common8>
						<TD class=title8 style= "width: '5%'">集团代理机构</TD>
				    <TD class= input8><input class="common"  name=repRiskCode style= "width: '50%' " TYPE=hidden >
				                      <input class="common"  name=repCodeGrp style= "width: '50%'"></TD>
				        <TD class=title8 style= "width: '5%'">集团代理人</TD>
				        <TD class= input8><input class="common"  name=repCodePep style= "width: '50%'"></TD>
				        <TD class=title8 style= "width: '5%'">集团代理人姓名</TD>
				        <TD class= input8><input class="common"  name=repCodeName style= "width: '50%'"></TD>
				        <TD class=title8 style= "width: '5%'">集团代理人身份证</TD>
				        <TD class= input8><input class="common"  name=repCodeIdno style= "width: '60%'"></TD>
				        
					</TR>
				</table>
				<HR>
      <input value="增加" id=addbutton  class=cssButton type=button onclick="grpCrsAdd();">
      <input value="修改" id=updatebutton  class=cssButton type=button onclick="grpCrsRep();">
      <input value="删除" id=deletebutton  class=cssButton type=button onclick="grpCrsDel();">
     </div>
     <br>
     <input name=sql type=hidden class=common>
     <input name=mOperate type=hidden class=common>
  </form>
  <span id="spanCode" style="display:none;position:absolute;slategray"></span>
</body>
</html>
