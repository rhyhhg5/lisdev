<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<html>
	<!--用户校验类-->
	<%@page import="com.sinosoft.utility.*"%>
	<%@page import="com.sinosoft.lis.schema.*"%>
	<%@page import="com.sinosoft.lis.vschema.*"%>
	<%@page import="com.sinosoft.lis.db.*"%>
	<%@page import="com.sinosoft.lis.vdb.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="com.sinosoft.lis.cbcheck.*"%>
	<%@page import="com.sinosoft.lis.tb.*"%>
	<%
		//接收信息，并作校验处理。
		//输入参数
		CErrors tError = null;
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
		LcpolAmntModifyUI tLcpolAmntModifyUI = new LcpolAmntModifyUI();
		//System.out.println(tG);

		//输出参数
		String FlagStr = "";
		String Content = "";
		String transact = "";
		String tOperate = "";

		//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		transact = request.getParameter("fmtransact");
		System.out.println(transact);

		//动作为添加。
		if (transact.equals("UPDATE")) {
			System.out.println("进入save的UPDATE");
			String tRadio[] = request.getParameterValues("InpLcpolAmntModifyGridSel"); //取单选框那一列 Inp与Sel是固定的，中间为mulline
			String tPolNo[] = request.getParameterValues("LcpolAmntModifyGrid5");//获取mulline第五列
			String tGrpContNo[] = request.getParameterValues("LcpolAmntModifyGrid1");//获取保单号，传到UI
			String tRiskCode[] = request.getParameterValues("LcpolAmntModifyGrid3");
			String tContNo[] = request.getParameterValues("LcpolAmntModifyGrid6");
			String tAmnt = request.getParameter("Amnt");//获取修改后的保额，传到UI
			//		输出参数
			String strManageCom = tG.ComCode;
			String strInput = "";
			TransferData transferData = new TransferData();

			for (int i = 0; i < tRadio.length; i++) {
				if ("1".equals(tRadio[i])) {
					transferData.setNameAndValue("PolNo", tPolNo[i]);
					System.out.println("tPolNo[i]传的值是：" + tPolNo[i]);
					transferData.setNameAndValue("GrpContNo", tGrpContNo[i]);
					transferData.setNameAndValue("RiskCode", tRiskCode[i]);
					transferData.setNameAndValue("ContNo", tContNo[i]);
					break;
				}
			}
			transferData.setNameAndValue("Amnt", tAmnt);
			if (!"Fail".equals(FlagStr)) {
				// 准备向后台传输数据 VData
				VData tVData = new VData();
				FlagStr = "";
				tVData.add(tG);
				tVData.add(transferData);
				try {
					tLcpolAmntModifyUI.submitData(tVData, "");
				} catch (Exception ex) {
					Content = "保存失败，原因是:" + ex.toString();
					FlagStr = "Fail";
				}
				if (!FlagStr.equals("Fail")) {
					tError = tLcpolAmntModifyUI.mErrors;
					if (!tError.needDealError()) {
						Content = " 保存成功! ";
						FlagStr = "Succ";
					} else {
						Content = " 保存失败，原因是:" + tError.getFirstError();
						FlagStr = "Fail";
					}
				}
			}
		}
	%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>