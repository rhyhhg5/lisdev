var showInfo;
var turnPage = new turnPageClass();

// 保存
function AddInput() {
	if (!verifyInput2()) {
		return false;
	}

	if (!beforeAddSubmit()) {
		return;
	}

	fm.transact.value = "INSERT";

	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.submit();

}
// 修改
function UpdateInput() {

	if (!verifyInput2()) {
		return false;
	}
	
	var i = 0;
	var showStr = "正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="+ showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.transact.value = "UPDATE";
	fm.submit(); // 提交
}
// 提交前的相关校验加在这里
function beforeAddSubmit() {
	var relatedtype = fm.RelatedType.value;
	if (relatedtype == 1) {
		var name = fm.Name.value;
		var idno = fm.IDNo.value;
		if (!idno) {
			alert("证件号码不能为空！");
			return false;
		}
		var tSQL1 = "select name from ldrelatedparty where name='" + name
				+ "' and idno='" + idno + "' and relatedtype='1' ";
		var tArr1 = easyExecSql(tSQL1);
		if (tArr1) {
			alert("已经存在名称为" + name + "的关联方！");
			return false;
		}
	} else if (relatedtype == 2) {
		var name = fm.Name.value;
		var tSQL1 = "select name from ldrelatedparty where name='" + name
				+ "' and relatedtype='2' ";
		var tArr1 = easyExecSql(tSQL1);
		if (tArr1) {
			alert("已经存在名称为" + name + "的关联方！");
			return false;
		}
	}

	return true;
}

function initThePage() {
	fm.RelatedType.value = RelatedType;
	if (RelatedType == 1) {
		fm.all("person").style.display = "";
		fm.all("RelatedTypeName").value = "个人关联方";
	} else if (RelatedType == 2) {
		fm.all("grp").style.display = "";
		fm.all("RelatedTypeName").value = "团体关联方";
	}
	var tSQL = "select Name,RgtMoney,BusinessScope,ChairmanName,GeneralManagerName,Sex,(select codename from ldcode where codetype='sex' and code=sex),"
			+ "Position,IDNo,OtherOrganization "
			+ "from LDRelatedParty where RelateNo='" + RelateNo + "'";
	var tArr = easyExecSql(tSQL);
	if (tArr) {
		fm.Name.value = tArr[0][0];
		fm.RgtMoney.value = tArr[0][1];
		fm.BusinessScope.value = tArr[0][2];
		fm.ChairmanName.value = tArr[0][3];
		fm.GeneralManagerName.value = tArr[0][4];
		fm.Sex.value = tArr[0][5];
		fm.SexName.value = tArr[0][6];
		fm.Position.value = tArr[0][7];
		fm.IDNo.value = tArr[0][8];
		fm.OtherOrganization.value = tArr[0][9];
	}
}

function afterSubmit(FlagStr, Content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		afterInsertGrpInfo();
	}
}

function afterInsertGrpInfo() {
	// var tGrpName = fm.GrpName.value;
	// var tTaxRegistration = fm.TaxRegistration.value;
	// var tOrgancomCode = fm.OrgancomCode.value;
	// var tSQL = "";
	// if(tOrgancomCode==null || tOrgancomCode=="" || tOrgancomCode=="null"){
	// tSQL = "select grpno from lsgrp where grpname='"+tGrpName+"' and
	// taxregistration='"+tTaxRegistration+"'";
	// }else if(tTaxRegistration==null || tTaxRegistration=="" ||
	// tTaxRegistration=="null"){
	// tSQL = "select grpno from lsgrp where grpname='"+tGrpName+"' and
	// organcomcode='"+tOrgancomCode+"'";
	// }
	// var tArr = easyExecSql(tSQL);
	// if(tArr){
	// fm.GrpNo.value = tArr[0][0];
	// }
}

function goBack() {
	window.location.replace("./RelatedTransactionQuery.jsp");
}

function afterCodeSelect(cCodeName, Field) {
	if (cCodeName == "RelatedType") {
		if (Field.value == "1") {
			fm.all('grp').style.display = "none";
			fm.all('person').style.display = "";
			fm.all('RgtMoney').value = "";
			fm.all('ChairmanName').value = "";
			fm.all('GeneralManagerName').value = "";
			fm.all('BusinessScope').value = "";

		} else if (Field.value == "2") {
			fm.all('person').style.display = "none";
			fm.all('grp').style.display = "";
			fm.all('Sex').value = "";
			fm.all('SexName').value = "";
			fm.all('Position').value = "";
			fm.all('IDNo').value = "";
			fm.all('OtherOrganization').value = "";
		}
	}
}

function relatedListUpload() {
	var importFile = fm2.all("FileName").value;

	// 获取上载文件的路径
	var strSQL = "";
	strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("获取上传路径异常，文件上传失败！");
		return;
	}

	// 上载的文件名
	if (fm2.all('FileName').value == "") {
		alert("请选择要上载的文件！");
		return false;
	}

	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	var ImportPath = turnPage.arrDataCacheSet[0][0];

	var showStr = "正在上载数据……";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm2.action = "./ListRelatedTransactionSave.jsp?ImportPath=" + ImportPath;
	fm2.submit(); //提交

}
