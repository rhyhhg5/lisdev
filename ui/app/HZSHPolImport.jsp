<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：杭州商业银行代理保单导入
//创建日期：2004-07-19
//创建人  ：LiuQiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="HZSHPolImport.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="HZSHPolImportInit.jsp"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>

</head>
<body  onload="initForm();" >
  <form action="./HZSHPolImportSave.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
<table class = common>
    <TR  class= common>
      <TD class = title width=20%>
        文件地址：
      </TD>
      <TD class = common width=80%>
        <Input  class = common type="file" name=FileName>
      </TD>
    </TR>
    <TR class = common >
      <TD class = common colspan=2>
        <INPUT class = common VALUE="提交" TYPE=button onclick = "submitForm();" > 
        <INPUT class = common VALUE="重置" TYPE=button onclick="resetForm();"> 					
      </td>
  	</TR>
    <input type=hidden name=ImportFile>
</table>
<Div id ="divImport" style="display:none">
 <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
    		</td>
    		<td class= titleImg>
    			 导入错误日志:
    		</td>
    	</tr>
    </table>
  	<Div  id= "divDetail" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanLCGrpImportLogGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
       <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 						
  	</div>
  </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
