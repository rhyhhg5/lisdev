<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDDrugSave.jsp
//程序功能：
//创建日期：2005-11-8 13:53
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>  
<%
  //输出参数
  System.out.println("进入Save页面...");
  CErrors tError = null;
  CErrors mError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  String tNum[] = request.getParameterValues("GrpGridNo");
  String tChk[] = request.getParameterValues("InpGrpGridChk");
  String tGrpContNo[] = request.getParameterValues("GrpGrid1");
  String tPrtNo[] = request.getParameterValues("GrpGrid2");
  transact = request.getParameter("fmtransact");
  boolean error = false;
  int n=0;
 	if(tNum != null ) n=tNum.length;
  tG=(GlobalInput)session.getValue("GI");
  for(int i=0 ; i<n ; i++){
  System.out.println("进入Save页面...");
  	if(tChk[i].equals("1")){
		  GrpSignAfterPrintUI tGrpSignAfterPrintUI = new GrpSignAfterPrintUI();
		  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		  tLCGrpContSchema.setGrpContNo(tGrpContNo[i]);
		  try
		  {
		  	//准备传输数据 VData
		  	
		  	VData tVData = new VData();
				tVData.add(tLCGrpContSchema);
		  	tVData.add(tG);
		  	boolean bl = tGrpSignAfterPrintUI.submitData(tVData,transact);
		  	System.out.println("预打签单是否成功："+bl);
		  	if(bl){
		  		String tSQL = " select 1 from lcgrpcont where prtno = '"+tPrtNo[i]+"' "
	    					+ " and substr(managecom,1,4) in ( select code from ldcode where codetype = 'qysendmessgem' ) ";
	    		String tFlag = new ExeSQL().getOneValue(tSQL);
	    		if("1".equals(tFlag)){
	    			VData tSMSVData = new VData();
	    			TransferData tSMSTransferData = new TransferData();
	    			tSMSTransferData.setNameAndValue("PrtNo",tPrtNo[i] );
	    			tSMSTransferData.setNameAndValue("ContTypeFlag","2" );
	    			tSMSVData.add(tSMSTransferData);
	    			tSMSVData.add( tG );
    				QySendMessgeMBL tQySendMessgeMBL = new QySendMessgeMBL();
    				if(tQySendMessgeMBL.submitData(tSMSVData,"QYMSG")){
    					System.out.println(tPrtNo+"签单后即时发送短信成功！");
    				}else{
    					System.out.println(tPrtNo+"签单后即时发送短信失败！");
    				}
	    		}
		  	}
		  }
		  catch(Exception ex)
		  {
		    Content = Content + "<br>"+tPrtNo[i]+"保存失败，原因是:" + ex.toString();
		    FlagStr = "Fail";
		    error = true;
		  }
		  //如果在Catch中发现异常，则不从错误类中提取错误信息
		    tError = tGrpSignAfterPrintUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = Content + "<br>"+tPrtNo[i]+" 保存成功! ";
		    	FlagStr = "Success";
		    }
		    else                                                                           
		    {
		    	System.out.println(i+"     "+Content);
		    	Content = Content +"<br>"+tPrtNo[i]+" 保存失败，原因是:" + tError.getLastError();
		    	FlagStr = "Fail";
		    	error = true;
		    }
	  }
  }
  if(error){
  	FlagStr = "Fail";
  }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
