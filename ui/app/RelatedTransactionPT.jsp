<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="../common/javascript/CommonTools.js"></script>
<SCRIPT src="RelatedTransactionPT.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="RelatedTransactionPTInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="./RelatedTransactionPTSave.jsp" method=post name=fm
		target="fraSubmit">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;"
					OnClick="showPage(this,divRelatedTransaction);"></td>
				<td class=titleImg>关联交易保单标识补提：</td>
			</tr>
		</table>

		<Div id="divRelatedTransaction">
			<table class=common >
				<TR class=common>
					<TD class=title8>补提日期起：</TD>
					<TD class=input8>
						<Input class=coolDatePicker dateFormat="short" name=StartDate  verify="补提日期起|date&notnull" elementtype=nacessary>
					</TD>
					<TD class=title8>补提日期止：</TD>
					<TD class=input8>
						<Input class=coolDatePicker dateFormat="short" name=EndDate  verify="补提日期止|date&notnull" elementtype=nacessary>
					</TD>
				</TR>
			</table>
			<br> <INPUT VALUE="补  提" class=cssButton TYPE=button onclick="add()">
		</Div>
		</form>

	<span id="spanCode" style="display: none; position: absolute;"></span>

</body>
</html>
