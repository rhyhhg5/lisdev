var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartMakeDate = fm.StartMakeDate.value;
    var tEndMakeDate = fm.EndMakeDate.value;

	if(dateDiff(tStartMakeDate, tEndMakeDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}
	var tContainsPAD="";
	if(fm.ContainsPAD.value=="" || fm.ContainsPAD.value =="0"){
		tContainsPAD="";
	}else if(fm.ContainsPAD.value == "1"){
		tContainsPAD=" and lcc.prtno like 'PD%' ";
	}else if(fm.ContainsPAD.value == "2"){
		tContainsPAD=" and lcc.prtno not like 'PD%' ";
	}

    var tStrSQL = ""
        + " select "
        + " tmpInfo.ManageCom, tmpInfo.ManageComName, tmpInfo.RiskType3, tmpInfo.RiskType4, "
        + " tmpInfo.SaleChnl, tmpInfo.RiskCode, tmpInfo.RiskName, "
        + " tmpInfo.ContTBCount, tmpInfo.ContTBInsuCount, "
        + " tmpInfo.AllPrem, tmpInfo.AllSupplementaryPrem, "
        + " Div((tmpInfo.AllPrem + tmpInfo.AllSupplementaryPrem), tmpInfo.ContTBCount) AvgTBPrem, "
        + " tmpInfo.AllAmnt"
        + " from "
        + " ( "
        + " select "
        + " tmp.ManageCom, tmp.ManageComName, tmp.RiskType3, tmp.RiskType4, "
        + " tmp.SaleChnl, tmp.RiskCode, tmp.RiskName, "
        + " count(distinct tmp.ContNo) ContTBCount, "
        + " count(distinct tmp.InsuredNo) ContTBInsuCount, "
        + " sum(tmp.Prem) AllPrem, "
        + " sum(tmp.SupplementaryPrem) AllSupplementaryPrem, "
        + " sum(tmp.Amnt) AllAmnt "
        + " from "
        + " ( "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " CodeName('risktype3', lmra.RiskType3) RiskType3, "
        + " CodeName('risktype4', lmra.RiskType4) RiskType4, "
        + " CodeName('lcsalechnl', lcc.SaleChnl) SaleChnl, "
        + " lcp.RiskCode, lmra.RiskName, "
        + " lcp.ContNo, lcp.InsuredNo, "
        + " lcp.Prem, nvl(lcp.SupplementaryPrem, 0) SupplementaryPrem, lcp.Amnt, "
        + " '' "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcp.ContNo = lcc.ContNo "
        + " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + getWherePart("lmra.RiskType3", "RiskType3")
        + getWherePart("lmra.RiskType4", "RiskType4")
        + tContainsPAD
        + " ) as tmp "
        + " group by tmp.ManageCom, tmp.ManageComName, tmp.RiskType3, tmp.RiskType4, "
        + " tmp.SaleChnl, tmp.RiskCode, tmp.RiskName "
        + " ) as tmpInfo "
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "ContRiskTBSave.jsp";
    fm.submit();
    fm.action = oldAction;

}