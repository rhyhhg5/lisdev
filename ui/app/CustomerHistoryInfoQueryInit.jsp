<%
//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<script language="JavaScript">

function initForm()
{
    try
    {
        initGrpCustomerGrid();

        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initGrpCustomerGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="团体客户号";
        iArray[1][1]="80px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        iArray[1][21]="MissionID";
        
        iArray[2]=new Array();
        iArray[2][0]="单位名称";
        iArray[2][1]="150px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        iArray[2][21]="SubMissionID";


        GrpCustomerGrid = new MulLineEnter("fm", "GrpCustomerGrid"); 

        GrpCustomerGrid.mulLineCount = 0;   
        GrpCustomerGrid.displayTitle = 1;
        GrpCustomerGrid.canSel = 1;
        GrpCustomerGrid.hiddenSubtraction = 1;
        GrpCustomerGrid.hiddenPlus = 1;
        GrpCustomerGrid.canChk = 0;
        GrpCustomerGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ResultGrid时出错：" + ex);
    }
}
</script>

