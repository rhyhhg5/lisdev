<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：ARStatistic
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="ScanStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form name=fm  target="fraTitle" method=post>
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
					<TD  class= title> 管理机构</TD>
					<TD  class= input><Input class= "codeno"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
        <TR  class= common>

					<TD  class= title>
					类型
					</TD>
					<TD  class= Input>
					<input class=codeno name=QYType verify="|len<=20" CodeData= "0|^个险投保书|1^银保万能投保书|2^团险投保书|3^个单问题件通知书|4^个单体检通知书|5^个单生调通知书|6^个单核保通知书|7^全部|8"  ondblclick="return showCodeListEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);" onkeyup="return showCodeListKeyEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);"><input class=codename name=QYType_ch></TD>    
		         
					<TD  class= title> 扫描人员</TD>
					<TD  class= input><Input class= "codeno"  name=ScanOperator  ondblclick="return showCodeList('scanoperator',[this,ScanOperatorName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('scanoperator',[this,ScanOperatorName],[0,1],null,null,null,1);" ><Input class=codename  name=ScanOperatorName></TD>
            
    </TR>
    </table>

    <input type="hidden" name=op value="">
    <INPUT VALUE="" TYPE=hidden  name=querySql>
    <INPUT VALUE="" TYPE=hidden  name=totalSql>
    <INPUT VALUE="" TYPE=hidden  name=filename>
	<INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
	<hr>
	
	    <div>
        <font color="red">
            <ul>查询字段说明：
                <li>统计起止期：扫描件扫描入系统的起止时间</li>  
                <li>扫描类型：个险投保书（扫描类型为“TB01-家庭投保书(含个险万能)”）</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;银保万能投保书（扫描类型为“TB04-银保万能险投保”）</li>      
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;团险投保书（扫描类型为“TB02-团体投保书”）</li> 
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;个单问题件通知书（扫描类型为“TB22-个单问题件通知书”）</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;个单体检通知书（扫描类型为“TB15-个单体检通知书”）</li>    
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;个单生调通知书（扫描类型为“TB16-个单契调通知书”）</li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;个单核保通知书（扫描类型为“TB21-个单核保通知书”）</li> 
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;全部（扫描类型为“TB”承保业务的扫描件）</li>       
            </ul>
            <ul>统计结果说明：
                <li>类型：页面选择的扫描类型</li>
                <li>扫描件数：扫描件扫描件数</li>
                <li>扫描页数：扫描件扫描页数</li>
            </ul>
        </font>
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
