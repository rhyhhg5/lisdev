
<%
	//程序名称：ARStatisticSub.jsp
	//程序功能：F1报表生成
	//创建日期：2005-09-16
	//创建人  ：St.GN
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%
	System.out.println("start");
	CError cError = new CError();
	boolean operFlag = true;

	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	String OperatorManagecom = "";
	String StartDate = request.getParameter("StartDate");
	String EndDate = request.getParameter("EndDate");
	String transact = request.getParameter("fmtransact");
	String QYType_ch = StrTool.unicodeToGBK(request.getParameter("QYType_ch"));
	String QYType = StrTool.unicodeToGBK(request.getParameter("QYType"));
	String ManageCom = request.getParameter("ManageCom");

	System.out.println("ManageCom1: ...." + ManageCom);
	System.out.println("QYType: ...." + QYType);
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	OperatorManagecom = tG.ManageCom;

	TransferData StartEnd = new TransferData();
	StartEnd.setNameAndValue("StartDate", StartDate);
	StartEnd.setNameAndValue("EndDate", EndDate);
	StartEnd.setNameAndValue("OperatorManagecom", OperatorManagecom);
	StartEnd.setNameAndValue("QYType", QYType);
	StartEnd.setNameAndValue("QYType_ch", QYType_ch);
	StartEnd.setNameAndValue("Managecom", ManageCom);
	System.out.println("ManageCom2: ...." + ManageCom);
	System.out.println("StartDate" + StartDate);
	System.out.println("EndDate" + EndDate);
	System.out.println("OperatorManagecom" + OperatorManagecom);
	System.out.println("QYType" + QYType);
	System.out.println("QYType_ch" + StrTool.unicodeToGBK(QYType_ch));
	//System.out.println("Managecom"+Managecom);
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	tVData.addElement(tG);
	tVData.addElement(StartEnd);

	ARStatisticCSVUI tARStatisticUI = new ARStatisticCSVUI();

	String realpath = application.getRealPath("/").substring(0, application.getRealPath("/").length());//UI地址
	String temp = realpath.substring(realpath.length() - 1, realpath.length());
	if (!temp.equals("/")) {
		realpath = realpath + "/";
	}
	StartEnd.setNameAndValue("realpath", realpath);

	tVData.addElement(tG);
	tVData.addElement(StartEnd);

	String outname = " ";
	String outpathname = " ";
	if (!tARStatisticUI.submitData(tVData, "PRINT")) {
		FlagStr = "Fail";
		Content = tARStatisticUI.mErrors.getFirstError().toString();

	} else {

		Content = "操作成功!";
		FlagStr = "Success";
		outname = tARStatisticUI.getFileName();
		outpathname = realpath + "vtsfile/" + outname;
		try {
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
			outname = java.net.URLEncoder.encode(outname, "UTF-8");
			outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
			outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
			System.out.println("outpathname=" + outpathname);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("--------成功----------");
		System.out.println(outname + "=========" + outpathname);

	}
%>
<html>
	<%@page contentType="text/html;charset=GBK"%>
	<a
		href="../f1print/download.jsp?filename=<%=outname%>&filenamepath=<%=outpathname%>">点击下载</a>
</html>
