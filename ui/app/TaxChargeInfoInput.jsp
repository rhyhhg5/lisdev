<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2015-11-30
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="TaxChargeInfoInput.js"></script>
    <%@include file="TaxChargeInfoInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">查询条件：</td>
            </tr>
        </table>
        
        <table class="common">
				<tr CLASS="common">
					<td CLASS="title">管理机构</td>
					<td CLASS="input" COLSPAN="1">
						<Input class="codeNo"  name=ManageCom  readonly=true  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true  >
		    		</td>
					<td CLASS="title">团体编号</td>
					<td CLASS="input" COLSPAN="1">
						<input name="GrpNo" CLASS="common"  >  
		    		</td>
					<td CLASS="title">单位名称</td>
					<td CLASS="input" COLSPAN="1">
						<input name="GrpName" CLASS="common"  >
		    		</td>
		    		<td CLASS="title">税务登记证号码</td>
					<td CLASS="input" COLSPAN="1">
						<input name="TaxRegistration" CLASS="common"  >
		    		</td>      		
				</tr>
				<tr CLASS="common">
					<td CLASS="title">社会信用代码</td>
					<td CLASS="input" COLSPAN="1">
						<input name="organCode" CLASS="common" >
		    		</td>
					<td CLASS="title">批次号</td>
					<td CLASS="input" COLSPAN="1">
						<input name="BatchNo" CLASS="common"  >  
		    		</td>
					<td CLASS="title">批次申请起期</td>
					<td CLASS="input" COLSPAN="1">
						<input NAME="StartDate" VALUE  class="coolDatePicker"  verify="申请日期|date&&notnull" >
		    		</td>
		    		<td CLASS="title">批次申请止期</td>
					<td CLASS="input" COLSPAN="1">
						<input NAME="EndDate" VALUE  class="coolDatePicker"  verify="申请日期|date&&notnull" >
		    		</td>
				</tr>
			</table>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryGrpCont();" /> 	
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divGrpContGrid);" />
                </td>
                <td class="titleImg">团体批次列表</td>
            </tr>
        </table>
        <div id="divGrpContGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanGrpContGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divGrpContGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>
        
        <br />
        
        <table>
            <tr>
                <td class="titleImg">批次待收费查询条件：</td>
            </tr>
        </table>
        
        <div id="divGrpSubCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">批次号</td>
                    <td class="input8">
                        <input class="common" name="GS_BatchNo" readonly="readonly" />
                    </td>
                    <td class="title8">团体编号</td>
                    <td class="input8">
                        <input class="common" name="GS_GrpNo" readonly="readonly" />
                    </td>
                    <td class="title">单位名称</td>
        			<td class="input" COLSPAN="1">
                        <input class="common" name="GS_GrpName" readonly="readonly" />
                    </td>
                    <td class="title8">&nbsp;</td>
                    <td class="input8">&nbsp;</td>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryGrpSubCont();" />    
                </td>
            </tr>
            
        </table>
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divGrpSubContGrid);" />
                </td>
                <td class="titleImg">待收费信息列表</td>
            </tr>
        </table>
        <div id="divGrpSubContGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanGrpSubContGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divGrpSubContGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                     
            </div>
        </div>
        
        <br />
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnSubContPrint" value="下载批次待收费信息" onclick="downChargeInfo();" />   
                </td>
            </tr>
        </table>
        
        <br />
        
        <hr />
        
        <div>
            <font color="red">
                <ul>
                    <li>只可查询批次中未收费投保单信息</li>
                </ul>
            </font>
        </div>
        <input type="hidden" name="querySql" value="" >
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
