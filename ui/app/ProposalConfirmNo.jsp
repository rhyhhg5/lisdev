<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ProposalConfirmNo.jsp
//程序功能：投保审核号录入界面
//创建日期：2007-11-15
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!—页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <SCRIPT src="ProposalConfirmNo.js"></SCRIPT> 
  <%@include file="ProposalConfirmNoInit.jsp"%> 
 
  
  <title>投保审核号录入</title>
</head>
<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form action="ProposalConfirmNoSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>印刷号码</td>
        <td  class= input><input class= common name=PrtNo  verify="印刷号码|notnull&len=12" elementtype="nacessary"></td>
        <td  class= title>核保审核号</td>
        <td  class= input><input class= common name=ConfirmNo  verify="核保审核号|notnull" elementtype="nacessary"></td>
      </tr>
      <tr class= common id="ConfirmInfoTRID" style="display:''">
        <td  class= title>审核人</td>
        <td  class= input><input class="codeNo"  name=ConfirmOperator ondblclick="return showCodeList('usercode',[this,ConfirmName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('usercode',[this,ConfirmName],[0,1],null,null,null,1);"><input class=codename name=ConfirmName readonly=true></td>
        <td  class= title>审核日期</td>
        <td  class= input><input class=coolDatePicker3 name="ConfirmDate" verify="审核日期|date&notnull"  style="width:130"  elementtype="nacessary"></td>
        <td class= title>审核时间</td>
        <td  class= input><input class= common verify="审核时间|notnull&time" name=ConfirmTime  elementtype="nacessary"></td>
      </tr>
      <tr  class= common id="RemarkTRID" style="display:'none'">         
        <td  class= title>备注</td>
        <td class=input colspan="5"><textarea classs ="common" name="Remark" cols="100%" rows="1"></textarea></td>     
      </tr>
      <tr class= common>
      <td class=button><input class=cssButton  VALUE="重  置"  TYPE=button onclick="initInpBox();"></td>
      <!-- <td class=button><input class=cssButton  VALUE="测试用"  TYPE=button onclick="checkPrtNo();"></td> -->
      </tr>
    </table>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCUWConfirmInfo);"></td>
	  <td class=titleImg>查询结果</td>
	</tr>
  </table>
  <!-- 信息（列表） -->
  <div id="divLCUWConfirmInfo" style="display:''">
	<table class=common>
      <tr class=common>
	    <td text-align:left colSpan=1><span id="spanLCUWConfirmInfoGrid"></span></td>
	  </tr>
	</table>
  </div>

  <div id="divPage2" align=center style="display: 'none' ">
	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </div>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden name=Sql>
</form>
<!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
