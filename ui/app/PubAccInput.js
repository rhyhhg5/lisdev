//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
parent.fraMain.rows = "0,0,0,0,*";
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if(!verifyInput2())
	return false;
	dealSumPrem();
	if(!checkDate())
	return false;
	
	if(fm.RiskCode.value=="690201"){
		if(!checkPub()){
			return false;
		}
	}
	
  if(fm.RiskCode.value=="370301"){
		var strSql = "select PremScope from lcgrpcont where  GrpContNo='"+GrpContNo+"'";
		//alert(InsuAccNo);
		var arr=easyExecSql(strSql);
		if(arr){
			var PremScope=arr[0][0];
			 for(var i=0;i<PublicAccGrid.mulLineCount;i++){
				 var Prem=PublicAccGrid.getRowColData(i,4);
				  if(PremScope != Prem){
					  alert("团体账户保费应与录入的总保费一致");
					  return false;
				 }
			}
		}
  }	
	
  fm.fmtransact.value = "INSERT||MAIN" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "./PubAccInputSave.jsp?GrpContNo="+GrpContNo;
  ChangeDecodeStr();
  fm.submit(); //提交
}

/*********************************************************************
 *  
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function saveHouston(){
	if(tLookFlag==0){
		if(ManageFeeGrid.mulLineCount<=0){
			alert("请先添加账户信息！");
			return false;
		}
	}
	 if(fm.Houston.value=="" || fm.Houston.value==null){
		alert("险种"+fm.RiskCode.value+"，是否进账必录！");
		return false;
	 }
	 if(fm.EntrustMoney.value=="" || fm.EntrustMoney.value==null){
			alert("险种"+fm.RiskCode.value+"，委托基金规模必录！");
			return false;
		 }
	  if(!checkDate()){
		return false;
	  }
	  if( verifyInput2() == false )
		    return false;
	  fm.fmtransact.value = "HOUSTO||MAIN" ;
	  var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.action = "./PubAccInputSave.jsp?GrpContNo="+GrpContNo;
	  ChangeDecodeStr();
	  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	UnChangeDecodeStr();
	calFee();
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"false"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDClassInfo.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  ChangeDecodeStr();
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDClassInfoQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("该删除操作将会删除该险种下全部帐户信息，您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.action = "./PubAccInputSave.jsp?GrpContNo="+GrpContNo;
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
}               
        
//查询系统中账户型险种的账户信息
function QueryAccInfo()
{
	//var strSql = "select distinct e.PayPlanName,a.dutycode,a.payplancode,e.riskcode,a.prem "
	//							+" from lcprem a,lmriskaccpay e,LMRiskInsuAcc b "
	//							+" where "
	//							+" a.polno in (select polno from lcpol where grpcontno='"+GrpContNo+"') "
	//							+" and a.needacc='1' "
	//							+" and a.PayPlanCode=e.PayPlanCode"
	//							+" and e.PayPlanCode=a.PayPlanCode"
	//							+" and b.InsuAccNo = e. InsuAccNo"
	//							+" and b.AccType = '001'";
	//var arr = easyExecSql(strSql);
	//if(!arr)
	//{
	//	alert("未查询到险种账户信息！");
	//	return false;
	//}
	//displayMultiline(arr, PublicAccGrid, turnPage);
}


/*********************************************************************
 *  查询保险计划
 *  参数  ：  集体合同投保单号
 *  返回值：  无
 *********************************************************************
 */
function getContPlanCode(tProposalGrpContNo) {
	//alert(tProposalGrpContNo);
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select ContPlanCode,ContPlanName from LCContPlan where ContPlanCode!='00' and ProposalGrpContNo='"+tProposalGrpContNo+"'";
  //alert("strsql :" + strsql);
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "") {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++) {
      j = i + 1;
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
    //divContPlan.style.display="";
  } else {
    //alert("保险计划没查到");
    //divContPlan.style.display="none";
  }
  //alert ("tcodedata : " + tCodeData);
  return tCodeData;
}
//页面信息反显
function showPubAccInfo()
{
	var tSql = "select rate from ldpromiserate where prtno=(select prtno from lcgrpcont where grpcontno='"+fm.GrpContNo.value+"') and riskcode='370301'";
	var arr = easyExecSql(tSql);
	if(arr){
		if(arr[0][0]==0){
			fm.GuaRate.value="0";
		}else{
			fm.GuaRate.value=arr[0][0];	
		}
	}
}

function GetInsuAccICodeData()
{
	var strSql = " select a.InsuAccNo,a.InsuAccName "
								+" from LMRiskInsuAcc a,LMRisktoAcc b "
								+" where a.InsuAccNo=b.InsuAccNo "
								+" and b.riskcode='"+RiskCode+"'"
								+" and a.AccType<>'002' and (a.AccCancelCode is null or a.AccCancelCode<>'1')";
		fm.InsuAccNo.CodeData = easyQueryVer3(strSql,1,0,1);
		if(fm.InsuAccNo.CodeData)
			return true;
		else
			return false;
}
function afterCodeSelect(tCode,tObj)
{
	fm.Prem.value="";
	if(tCode=="InsuAcc")
	{
		var InsuAccNo = tObj.value;
		var strSql = "select acctype from LMRiskInsuAcc where  InsuAccNo='"+InsuAccNo+"'";
		//alert(InsuAccNo);
		var arr=easyExecSql(strSql);
		if(arr)
		{
			if(arr[0][0]==001)
			{
			fm.PublicAccType.value="C";
			fm.InsuredName.value="团体理赔帐户";
			}
			else if(arr[0][0]==004)
			{
			fm.PublicAccType.value="G";
			fm.InsuredName.value="团体固定账户";
			}else{
			alert("没有指定帐户类型!");
			return false;
			}
		}else{
			alert("没有指定帐户类型!");
			return false;
			}
	}
	if(tCode=="AccTypeCode"||tCode=="InsuAcc")
	{
		if(fm.AccTypeCode.value!=""&&fm.InsuAccNo.value!="")
		{
			dealPubAccFactor(fm.AccTypeCode.value,fm.InsuAccNo.value)
		}
	}
}
function dealPubAccFactor(AccType,InsuAccNo)
{
	var strSql = "select distinct CalFactor,FactorName,FactorNoti,(select CalFactorValue from LCContPlanDutyParam where GrpContNo='"+GrpContNo+"' and ContPlanCode='11' and CalFactor=lmriskdutyfactor.CalFactor and InsuAccNo='"+InsuAccNo+"'),CalFactorType from lmriskdutyfactor where "
	+" RiskCode='"+RiskCode+"'"
	+" and InsuAccNo='"+InsuAccNo+"' and chooseflag ='1'";
	turnPage.queryModal(strSql,PublicAccGrid);
//	var strSql = "select PublicAcc from LCInsuredList where GrpContNo='"+GrpContNo
//			   + "' and PublicAccType='"+fm.PublicAccType.value+"' ";
//			   + "union "
//			   + "select PublicAcc from LBInsuredList where GrpContNo='"+GrpContNo+"' "
//			   + "and PublicAccType='"+fm.PublicAccType.value+"'";
	var strSql = "";
    if(tLookFlag==1){
    	strSql = "select PublicAcc from LCInsuredList where GrpContNo='"+GrpContNo
		       + "' and PublicAccType='"+fm.PublicAccType.value+"' "
		   	   + "union "
		   	   + "select PublicAcc from LBInsuredList where GrpContNo='"+GrpContNo+"' "
		   	   + "and PublicAccType='"+fm.PublicAccType.value+"'";
    }else{
    	strSql = "select PublicAcc from LCInsuredList where GrpContNo='"+GrpContNo
		   	   + "' and PublicAccType='"+fm.PublicAccType.value+"' ";
    }
	var arr = easyExecSql(strSql);
	if(arr)
	{
		fm.Prem.value = arr[0][0];
		queryManageFee();
		queryInterest();
	}
	else
	{
		fm.Prem.value="";
	}
}

function dealSumPrem()
{
	for(var i=0;i<PublicAccGrid.mulLineCount;i++)
	{
		if(PublicAccGrid.getRowColData(i,1)=="PublicAcc")
		{
			fm.Prem.value=PublicAccGrid.getRowColData(i,4);
		}
	}
}

function queryManageFee()
{
	strSql = "select distinct GrpPolNo,GrpContNo,RiskCode,"
	+"FeeCode,InsuAccNo,PayPlanCode,PayInsuAccName,"
	+"FeeCalMode,FeeCalModeType,FeeCalCode,(select FeeName from LMRiskFee where "
	+"FeeCode = LCGrpFee.FeeCode and InsuAccNo=LCGrpFee.InsuAccNo and PayPlanCode=LCGrpFee.PayPlanCode),FeeValue,"
	+"CompareValue,FeePeriod,MaxTime,case DefaultFlag when '0' then '约定管理费' when '1' then '标准管理费' end,Operator,"
	+"MakeDate,MakeTime,ModifyDate,ModifyTime from LCGrpFee where grppolno='"+GrpPolNo+"' and InsuAccNo in (select InsuAccNo from LMRiskInsuAcc where RiskCode='"+RiskCode+"' and AccType<>'002' union select '000000' from dual)";
	turnPage.queryModal(strSql,ManageFeeGrid);
	if(tLookFlag==0){
		if(ManageFeeGrid.mulLineCount<=0){
			alert("请先添加账户信息！");
		}
	}
	strSql = "select distinct GrpPolNo,GrpContNo,RiskCode,"
	+"FeeCode,InsuAccNo,PayPlanCode,PayInsuAccName,"
	+"FeeCalMode,FeeCalModeType,FeeCalCode,(select FeeName from LMRiskFee where "
	+"FeeCode = LCGrpFee.FeeCode and InsuAccNo=LCGrpFee.InsuAccNo and PayPlanCode=LCGrpFee.PayPlanCode),FeeValue,"
	+"CompareValue,FeePeriod,MaxTime,case DefaultFlag when '0' then '约定管理费' when '1' then '标准管理费' end,Operator,"
	+"MakeDate,MakeTime,ModifyDate,ModifyTime from LCGrpFee where grppolno='"+GrpPolNo+"' and InsuAccNo in (select InsuAccNo from LMRiskInsuAcc where RiskCode='"+RiskCode+"' and AccType='002')";
	turnPage.queryModal(strSql,IndManageFeeGrid);
	if(tLookFlag==0 && RiskCode!="370301"){
		if(IndManageFeeGrid.mulLineCount<=0){
			alert("请确认是否录入保费合计！");
		}
	}
	calFee();
	//add by zjd 反显特需险的赔付顺序
	var txsql=" select claimnum from lcgrpfee where grppolno='"+GrpPolNo+"'  ";
	var arr = easyExecSql(txsql);
    if(arr){
    	   fm.ClaimNum.value=arr[0][0];
    	   if(fm.ClaimNum.value=="1"){
    		   fm.ClaimNumName.value="仅从个人账户赔";
    	   }else if(fm.ClaimNum.value=="2"){
    		   fm.ClaimNumName.value="先从个人账户赔付再从公共账户赔付"; 
    	   }
    }
    var txsql=" select Houston,EntrustMoney from lcgrpfee where grppolno='"+GrpPolNo+"'  ";
	var arr = easyExecSql(txsql);
    if(arr){
    	   fm.Houston.value=arr[0][0];
    	   if(fm.Houston.value=="0"){
    		   fm.HoustonName.value="是";
    	   }else if(fm.Houston.value=="1"){
    		   fm.HoustonName.value="否"; 
    	   }
    	   fm.EntrustMoney.value=arr[0][1];
    }
}
function saveManageFee()
{
	beforeManageFeeSubmit();
		if(!checkDate())
	return false;
	//add by zjd 特需险增加赔付顺序为必录
    var tsql=" select riskcode from lmriskapp where risktype3='7' and riskcode='"+fm.RiskCode.value+"' ";
    var arr = easyExecSql(tsql);
    if(arr){
    	if(fm.ClaimNum.value=="" || fm.ClaimNum.value==null){
    		alert("险种"+fm.RiskCode.value+"为特需险，赔付顺序必录！");
    		return false;
    	}
    }
    
    if(fm.RiskCode.value=="370301"){
    	var a =  /^([1-9]\d{0,15}|0)(\.\d{1,6})?$/g;
    	var b =  /^([1-9]\d{0,15}|0)(\.\d{1,2})?$/g;
    	var ManageFee=ManageFeeGrid.getRowColData(0,12);
    	var initial=ManageFeeGrid.getRowColData(1,12);
    	if(a.test(initial)){
    	}else{
    		alert("初始费用比例录入值应为数字或小数,且范围为小数点后六位！");
	        return false;
    	}
    	if(b.test(ManageFee)){
    	}else{
    		alert("保单管理费录入值应为数字或小数,且范围为小数点后两位！");
	        return false;
    	}
    	if(ManageFee>5||ManageFee<0){
    		alert("保单管理费录入值范围为0-5！");
			return false;
		}
		if(initial<0||initial>0.05){
			alert("初始费用比例录入值范围为0-0.05！");
			return false;
		}
  }	

    
    
	fm.fmtransact.value = "MANAGEFEE||MAIN" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "./PubAccInputSave.jsp?GrpContNo="+GrpContNo;
  ChangeDecodeStr();
  fm.submit(); //提交
}
//计算管理费
function calFee()
{
	var pubAcc = 0.0;
	var InsuAccNo = fm.InsuAccNo.value;
	var tChargeFeeRateType="00";
	for(var i=0;i<PublicAccGrid.mulLineCount;i++){
		if(PublicAccGrid.getRowColData(i,1)=="PublicAcc")
		{
			pubAcc = PublicAccGrid.getRowColData(i,4);
		}
		if(PublicAccGrid.getRowColData(i,1)=="ChargeFeeRateType")
		{
			tChargeFeeRateType = PublicAccGrid.getRowColData(i,4);
		}
	}

	var strSql = "select a.FeeValue from LCGrpFee a,LMRiskFee b where a.GrpContNo='"
	+GrpContNo+"' and a.InsuAccNo='"+InsuAccNo+"' and a.FeeCode=b.FeeCode and a.InsuAccNo=b.InsuAccNo"
	+" and a.PayPlanCode=b.PayPlanCode and b.feeItemType='01'";
//    var strSql = "select a.FeeValue from LCGrpFee a,LMRiskFee b ,lmriskapp  c  where a.GrpContNo='"
//    +GrpContNo+"' and a.riskcode=c.riskcode and c.risktype8<>'6' and a.InsuAccNo='"+InsuAccNo+"' and a.FeeCode=b.FeeCode and a.InsuAccNo=b.InsuAccNo"
//    +" and a.PayPlanCode=b.PayPlanCode and b.feeItemType='01'";
	var arr = easyExecSql(strSql);
	var ManageFee = 0;
	var InsuAcc = 0;
	if(arr){		
        var rSql="select 'Y' from lmriskapp where riskcode='"+fm.RiskCode.value+"' and risktype8='6' ";
        var arisk=easyExecSql(rSql);
        rSql = "select 1 from ldcode where codetype='chargefeeratetype' and code='"+fm.RiskCode.value+"'";
        var ratetype=easyExecSql(rSql);
        if(arisk&&tChargeFeeRateType=="04"){
          if(ratetype){
             fm.ManageFee.value = Math.round(pubAcc*arr[0][0]*100)/100;
		     fm.GrpPubAcc.value=pubAcc;
          }else{
             fm.ManageFee.value = ManageFee;
		     fm.GrpPubAcc.value=pubAcc;
          }       
        }
        else 
        {
        //ManageFee = Math.round(pubAcc*arr[0][0],2);
		//InsuAcc = Math.round(pubAcc - ManageFee,2) ;
        // js取两位小数有误。
        //ManageFee = pointTwo(pubAcc*arr[0][0]);
        ManageFee = Math.round(pubAcc*arr[0][0]*100)/100
        InsuAcc = pointTwo(pubAcc - ManageFee) ;        
        
		fm.ManageFee.value = ManageFee;
		fm.GrpPubAcc.value = InsuAcc;
		}
	}
	else{
		fm.ManageFee.value = ManageFee;
		fm.GrpPubAcc.value=pubAcc;
	}
}
function queryInterest(){
    var rSql="select 'Y' from lmriskapp where riskcode='"+fm.RiskCode.value+"' and risktype8='6' ";
    var arisk=easyExecSql(rSql);
    if(!arisk)
    {
	var strSql="SELECT A.GRPPOLNO,A.GRPCONTNO,A.RISKCODE,A.INTERESTCODE,A.INSUACCNO,B.INSUACCNAME,A.INTERESTTYPE,A.DEFAULTCALTYPE,A.DEFAULTRATE FROM LCGRPINTEREST A,LMRISKINSUACC B WHERE A.GRPCONTNO='"+GrpContNo+"' and a.RISKCODE='"+RiskCode+"' and  a.INSUACCNO=b.INSUACCNO ";
	turnPage.queryModal(strSql,RiskInterestGrid);
	}
	else
	{
	alert("委托类产品不能录入利息信息!");
	}
}
function saveInterest(){
	if(!checkDate())
	{
		return false;
	}
	
	//校验利率
	if(!checkFee())
	{
		return false;
	}
	
	fm.fmtransact.value = "INTEREST||MAIN" ;
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PubAccInputSave.jsp?GrpContNo="+GrpContNo;
	fm.submit(); //提交
}
/** 不区分个人理赔账户和团体理赔账户,当修改一个时,另一个也需要随之修改 */
function beforeManageFeeSubmit() {
	for(var i=0 ; i<ManageFeeGrid.mulLineCount ; i++){
		for(var m=0 ; m<IndManageFeeGrid.mulLineCount ; m++){
			if(ManageFeeGrid.getRowColData(i,11) == IndManageFeeGrid.getRowColData(m,11)){
				IndManageFeeGrid.setRowColData(m,12,ManageFeeGrid.getRowColData(i,12));
			}
		}
	}
}
function checkDate(){
var StrSQL11="select 1 from lccont where Grpcontno='"+fm.GrpContNo.value+"' with ur ";
var arr = easyExecSql(StrSQL11);
//alert(arr); 
	if(arr)
	{
		alert("该保单已算费,请首先删除被保人、保障计划和账户信息后，再重新录入账户信息!");
		return false;
	}
	return true;
}

//校验费率，费率不能大于2.5％                   by zhangyang  2011-01-20
//2015-11-15 cz 放开注释，稍作修改
function checkFee()
{
	for(var i = 0; i < RiskInterestGrid.mulLineCount; i++)
	{
		var tFee = RiskInterestGrid.getRowColData(i,9);
		var doubleFee = tFee*1;
//		if(RiskCode == "170401" || RiskCode == "170301")
		//对170401险种,利率不能大于0.025
		if(RiskCode == "170401"){
			if(doubleFee > 0.025){
				alert("对于险种170401，利率不能大于0.025！");
				return false;
			}
		}
		//对170301险种，利率不能大于0
		if(RiskCode == "170301"){
			if(doubleFee > 0.00){
				alert("对于险种170301，利率不能大于0！");
				return false;
			}
		}
	}
	return true;
}

//校验最低保证利率
function checkGuaranteeRate(){
	var guaRate = fm.all('GuaRate').value;
	if(guaRate){
		var reg= /^[\+\-]?\d+(\.\d+)?$/;
		if(reg.test(guaRate)){
			if(guaRate<0 || guaRate>0.025){
				alert("保证利率必须大于等于0并且小于等于0.025！");
				return false;
			}
		}else{
			alert("保证利率只能录入数字！");
			return false;
		}
	}else{
		alert("保证利率不能为空！");
		return false;
	}
	return true;
}
function saveGuaranteeRate(){
	if(!checkGuaranteeRate()){
		return false;
	}
	var strSql1="select prtno,operator from lcgrpcont where Grpcontno='"+fm.GrpContNo.value+"'";
	var result = easyExecSql(strSql1);
	var prtno = result[0][0];
	var operator = result[0][1];
	var strSql2="select 1 from ldpromiserate where prtno='"+prtno+"' and riskcode='370301'";
	var arr2 = easyExecSql(strSql2);
	var flag ;
	if(arr2){
		flag=1;
	}else{
		flag=0;
	}
	fm.fmtransact.value = "GuaranteeRate||MAIN" ;
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PubAccInputSave.jsp?PrtNo="+ prtno +"&GuaRate="+fm.all('GuaRate').value +"&Operator="+operator+"&flag="+flag;
	fm.submit(); //提交
	
}


function checkPub(){
	for(var i=0;i<PublicAccGrid.mulLineCount;i++){
		if(PublicAccGrid.getRowColData(i,1)=="PublicAcc"&&PublicAccGrid.getRowColData(i,4)==""){
			alert("保费不能为空!");
			return false;
		}
		if(PublicAccGrid.getRowColData(i,1)=="AccType2"){
			var type=PublicAccGrid.getRowColData(i,4);
			if(type!="01"&&type!="02"&&type!="03"){
				alert("帐户类型只能是01、02、03，请录入正确值！");
				return false;
			}
		}
		if(PublicAccGrid.getRowColData(i,1)=="ChargeFeeRateType"){
			var RateType=PublicAccGrid.getRowColData(i,4);
			if(RateType!="02"&&RateType!="04"){
				alert("管理费比例收取方式只能是02、04，请录入正确值！");
				return false;
			}
		}
	}
	return true;
}

function initHouston(){
	if(RiskCode!=="690201"){
		fm.all("divHouston").style.display = "none";
	}
}

function initDisplay(){
	if(RiskCode =="370301"){
		fm.all("new2").style.display = "none";
		fm.all("new3").style.display = "none";
		fm.all("new4").style.display = "none";
		fm.all("divLDClassInfo4").style.display = "none";
		fm.all("new5").style.display = "none";
		fm.all("zhu1").style.display = "none";
		fm.all("zhu2").style.display = "";
		fm.all("divLDClassInfo5").style.display = "";
	}
}

function initButton(){
	if(tLookFlag != "0"){
		fm.all("SaveAccInfo").style.display = "none";
		fm.all("DelAccInfo").style.display = "none";
		fm.all("DefManageFee").style.display = "none";
		fm.all("SaveManaFee").style.display = "none";
		fm.all("DefRate").style.display = "none";
		fm.all("SaveRate").style.display = "none";
		fm.all("SaveGuaranteeRate").style.display = "none";
	}
}
function cancle(){
	top.close();
}