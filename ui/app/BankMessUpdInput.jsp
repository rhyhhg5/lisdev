<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：BankMessUpdInput.jsp
//程序功能：银行信息维护
//创建日期：2014-05-05 14:57:36
//创建人  ：zjd
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人   
    String tContNo = "";
    String tFlag = "";
  GlobalInput tGI = new GlobalInput();
    tGI = (GlobalInput)session.getValue("GI");
    tFlag = request.getParameter("type"); 
%>
<script>    
    var contNo = "<%=tContNo%>";          //个人单的查询条件.
    var operator = "<%=tGI.Operator%>";   //记录操作员
    var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
    var type = "<%=tFlag%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="BankMessUpdInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BankMessUpdInit.jsp"%>
  <title>银行信息维护</title>
</head>
<body  onload="initForm();" >
  <form action="./BankMessUpdInputSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
        <tr>
        <td class= titleImg align= center>查询条件</td>
    </tr>
    </table>
    <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>
            印刷号
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNoQ elementtype=nacessary >
          </TD>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class= common name=ContNoQ elementtype=nacessary >
          </TD>
          <TD  class= title>
            保单类型
          </TD>
          <TD  class= input>
            <Input class="codeNo"  name=ContTypeQ  verify="保单类型|code:conttype&NOTNULL" ondblclick="return showCodeList('conttype',[this,ContTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('conttype',[this,ContTypeName],[0,1],null,null,null,1);"><input class=codename name=ContTypeName readonly=true elementtype=nacessary>
          </TD>
          
        </TR>
    </table>
          <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
    <table>
        <tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrp1);">
            </td>
            <td class= titleImg>
                 银行信息
            </td>
            <tr>
  
          <INPUT  type= "hidden" class= Common name= prtno  value="">
        </tr>
        </tr>
        <tr>
  
          <INPUT  type= "hidden" class= Common name= MissionID  value="">
          <INPUT  type= "hidden" class= Common name= SubMissionID  value="">
        </tr>
    </table>
    <Div  id= "divLCGrp1" style= "display: ''" align=center>
        <table  class= common>
            <tr  class= common>
                <td text-align: left colSpan=1>
                    <span id="spanGrpGrid" >
                    </span> 
                </td>
            </tr>
        </table>                
    </Div>
        
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">                  
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  
  <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>
            银行编码
          </TD>
          <TD  class= input>
            <Input class= common name=bankcode elementtype=nacessary >
          </TD>
          <TD  class= title>
            户名
          </TD>
          <TD  class= input>
            <Input class= common name=accname elementtype=nacessary >
          </TD>
          <TD  class= title>
           账号
          </TD>
          <TD  class= input>
            <Input class= common name=bankaccno elementtype=nacessary >
          </TD>
          
        </TR>
    </table>
    <p>
      <INPUT VALUE="保  存" class = cssButton TYPE=button onclick="GoToInput();"> 
    </p>  
            <input class= common type=hidden name=tFlag value="<%=tFlag%>">
            <Input class= common type=hidden name=Operator >          
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
