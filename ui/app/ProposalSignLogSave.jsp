<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalSignLogSave.jsp
//程序功能：万能险帐户结算利率录入
//创建日期：2007-12-12
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
  //输出参数               
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");  //操作员信息，GI已在登录时放入session
  String transact = request.getParameter("fmtransact");
  
  System.out.println("transact=" + transact);
  
  //定义一个与页面需要处理数据对应的表的Schema存储页面数据
  LCSignLogSchema tLCSignLogSchema = new LCSignLogSchema();
  tLCSignLogSchema.setSeriNo(request.getParameter("SeriNo"));
  tLCSignLogSchema.setContNo(request.getParameter("ContNo"));
  tLCSignLogSchema.setContType(request.getParameter("ContType"));
  tLCSignLogSchema.setErrInfo(request.getParameter("ErrInfo"));
 
  //将需要处理的数据对象放入统一的接口数据集合VData
  VData tVData = new VData();
	tVData.add(tLCSignLogSchema);
	tVData.add(tG);
	
	//调用后台处理类接口
  ProposalSignLogUI tProposalSignLogUI = new ProposalSignLogUI();
  if(!tProposalSignLogUI.submitData(tVData,transact))
  {
    Content = "操作失败，原因是:" + tProposalSignLogUI.mErrors.getFirstError();
    FlagStr = "Fail";
  }
  else
  {                      
    Content = "操作成功! ";
    FlagStr = "Success";
  }
  
%>
<html>
<script language="javascript">
  //后台处理完毕后，调用父页面的afterSubmit页面显示后台提示信息
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
