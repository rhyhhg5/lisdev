
var showInfo;
window.onfocus = myonfocus;
// 使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		} catch (ex) {
			showInfo = null;
		}
	}
}

// 查询
function submitForm() {

	var Signdate = fm.Signdate.value;
	var EndDate = fm.EndDate.value;
	if (Signdate == "" || EndDate == "") {
		alert("请选择签单日期的起始和终止日期");
		return false;
	}
	if (dateDiff(Signdate, EndDate, "M") > 12) {
		alert("统计期最多为12个月！");
		return false;
	}

	var strSQL = " Select Comcode 省级机构代码,Comname 省级机构名称,"
			+ "  Managecom 机构代码,Managecomname 机构名称, "
			+ "  Salechnl 渠道,Contno 保单号,Mainriskcode 险种号, "
			+ "   (Case "
			+ "   When Mainriskcode In ('333701', '532601') Then "
			+ "    '福利双全个人护理保险' "
			+ "   When Mainriskcode In ('333702', '532602') Then "
			+ "    '福利双全个人护理保险' Else '福惠双全个人护理保险' "
			+ "  End) 险种名称,  Sum(Amnt) 保额, Sum(Prem) 保费, "
			+ "   Cvalidate 保单生效日,Signdate 保单签单日,"
			+ "    Appntno 投保人客户号, Appntname 投保人姓名,"
			+ "   Appntsex 投保人性别, Idtype 投保人证件类型, Idno 投保人证件号,"
			+ "    Mobile 联系电话, Insuredname 被保人姓名,"
			+ "  Insuredidtype 被保人证件类型, Insuredno 被保人证件号,"
			+ "  Agentgroup 营业单位代码, Agentcode 代理人代码,"
			+ "  Agentname 代理人姓名, Agentmobile 代理人电话 "
			+ " From (Select Substr(Lc.Managecom, 1, 4) Comcode, "
			+ "       (Select Name  From Ldcom "
			+ "          Where Comcode = Substr(Lc.Managecom, 1, 4)) Comname, "
			+ "         Lc.Managecom Managecom,      (Select Name  From Ldcom "
			+ "        Where Comcode = Lc.Managecom) Managecomname, "
			+ "     (select codename from ldcode  where codetype='salechnl' "
			+ "  and code=lc.Salechnl ) "
			+ "      Salechnl, Lc.Contno Contno,  Lp.Amnt Amnt, "
			+ "    Lp.Prem Prem, Lc.Cvalidate Cvalidate, "
			+ "       Lc.Signdate Signdate,   Lca.Appntno Appntno, "
			+ "       Lca.Appntname Appntname, "
			+ "    ( select codename from ldcode where codetype='sex'"
			+ " and code=lc.Appntsex) Appntsex, "
			+ "    ( select codename from ldcode where codetype='idtype' "
			+ " and code=lc.appntIdtype) Idtype, "
			+ "      Lca.Idno Idno, "
			+ "       (Select Mobile       From Lcaddress "
			+ "      Where Customerno = Lca.Appntno "
			+ "      And Addressno = Lca.Addressno) Mobile, "
			+ "    Lp.Insuredname Insuredname, "
			+ "   ( select codename from ldcode where codetype='idtype' "
			+ " and code = lc.Insuredidtype) Insuredidtype,"
			+ "     (Select Lci.Idno From Lcinsured Lci "
			+ "       Where Lp.Contno = Lci.Contno "
			+ "       And Lp.Insuredno = Lci.Insuredno) Insuredno, "
			+ "    (Select Branchattr From Labranchgroup "
			+ "       Where Agentgroup = Lc.Agentgroup) Agentgroup, "
			+ "     (Select Groupagentcode "
			+ "       From Laagent "
			+ "       Where Agentcode = Lc.Agentcode) Agentcode, "
			+ "     (Select Name "
			+ "       From Laagent "
			+ "       Where Agentcode = Lc.Agentcode) Agentname, "
			+ "      (Select Mobile "
			+ "       From Laagent "
			+ "       Where Agentcode = Lc.Agentcode) Agentmobile, "
			+ "      (Case "
			+ "				When Riskcode In ('333701', '532601') Then '333701' "
			+ " 				When Riskcode In ('333702', '532602') Then '333702' "
			+ " 				When Riskcode In ('334001', '532801') Then '334001' "
			+ " 				End) Mainriskcode    "
			+ "    From Lccont Lc "
			+ "   Inner Join Lcappnt Lca On Lc.Contno = Lca.Contno "
			+ "   Inner Join Lcpol Lp On Lc.Contno = Lp.Contno "
			+ "   Where 1 = 1 "
			+ "  And Lc.Conttype = '1' "
			+ "  And Lp.Riskcode In ('333701', '333702', '334001', '532601', '532602', '532801') "
			+ getWherePart('lc.ManageCom', 'ManageCom', 'like')
			+ getWherePart('lc.Signdate', 'Signdate', '>=')
			+ getWherePart('lc.Signdate', 'EndDate', '<=')
			+ getWherePart('lc.SaleChnl', 'SaleChnl')
			+ "   And Lc.Appflag = '1' "
			+ " And Lc.stateflag = '1' "
			+ " ) Temp "
			+ " Group By Comcode,Comname,Managecom,Managecomname,Salechnl,Contno,MainRiskcode,Cvalidate,Signdate,Appntno, "
			+ "      Appntname,Appntsex,Idtype,Idno,Mobile,Insuredname,Insuredidtype,Insuredno,Agentgroup,Agentcode, "
			+ "       Agentname,Agentmobile "
			

	fm.querySql.value = strSQL;

	var oldAction = fm.action;
	fm.action = "LcDetailDataSendSave.jsp";
	fm.submit();
	fm.action = oldAction;

}
