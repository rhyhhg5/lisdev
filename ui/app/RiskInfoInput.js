//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询事件
function easyQueryClick()
{
    var canSale = "";
    if(fm.canSale.value == "0")
        canSale = " and (EndDate is null or char(EndDate) = '' or EndDate > Current Date) ";
    else if(fm.canSale.value == "1")
        canSale = " and EndDate < Current Date ";
    var strSQL = "select RiskCode, RiskName, "
        + "case RiskProp when 'G' then '团体险' when 'I' then '个人险' when 'Y' then '银代险' else '其他' end, "
        + "case SubRiskFlag when 'M' then '主险' when 'S' then '附险' else '两者都可以' end, "
        + "case RiskType5 when '1' then '一年期以内' when '2' then '一年期' when '3' then '一年期以上(定期)' when '4' then '一年期以上(终身)' else '其他' end, "
        + "StartDate, EndDate, MinInsuredAge, MaxInsuredAge "
        + "from LMRiskApp "
        + "where 1 = 1 " + canSale 
        + getWherePart('RiskCode', 'riskCode')
        + getWherePart('RiskProp', 'riskProp')
        + getWherePart('SubRiskFlag', 'subRiskFlag')
        + getWherePart('RiskType5', 'riskType5')
        + getWherePart('EndDate', 'endDate', '<=')
        + " order by RiskCode";

    turnPage.queryModal(strSQL, RiskInfoGrid);

    fm.querySql.value = strSQL;
}

//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && RiskInfoGrid.mulLineCount > 0)
    {
        fm.action = "RiskInfoDownload.jsp";
        fm.target = "_blank";
        fm.submit();
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
