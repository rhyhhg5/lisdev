var showInfo;
var mDebug="0";

// 提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
	var tStartDate = fm.StartDate.value;
	var tEndDate = fm.EndDate.value;
	var tMngCom = fm.ManageCom.value;
	var tQYType = fm.QYType.value;
	var tStrSQL = "";
	if(dateDiff(tStartDate,tEndDate,"M")>3)
	{
		alert("统计期最多为三个月！");
		return false;
	}
	
	if(dateDiff(tStartDate,tEndDate,"M")<1){
      alert("统计止期比统计统计起期早");
      return false;
    }
	
    if(tQYType=="1"){
      tStrSQL =" select A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z, "
              +"        A1,B1,C1,D1,E1,G1,F1,H1,getUniteCode(I1),J1 "
              +" from (select a.contno A,a.prtno B, "
              +"                case a.appflag "
              +"                  when '1' then "
              +"                   '承保' "
              +"                  else "
              +"                   '投保' "
              +"                end C, "
              +"                b.appntname D,a.insuredname E,a.riskcode F,a.mult G,a.amnt H,a.prem I, "
              +"                a.polapplydate J,a.cvalidate K,b.ReceiveDate L,b.FirstTrialDate M,b.FirstTrialOperator N, "
              +"                (select makedate "
              +"                   from es_doc_main "
              +"                  where a.prtno = doccode "
              +"                    and busstype = 'TB' "
              +"                    and subtype = 'TB01') O, "
              +"                (select ScanOperator "
              +"                   from es_doc_main "
              +"                  where a.prtno = doccode "
              +"                    and busstype = 'TB' "
              +"                    and subtype = 'TB01') P, "
              +"                b.InputDate Q,b.InputOperator R,a.ApproveDate S,a.ApproveCode T,   "
              +"                (select '自核通过' "
              +"                   from LCUWMaster "
              +"                  where proposalcontno not in "
              +"                        (select proposalcontno "
              +"                           from lcuwerror "
              +"                          where grpcontno = '00000000000000000000') "
              +"                    and a.grpcontno = '00000000000000000000' "
              +"                    and ProposalNo = a.ProposalNo) U, "
              +"                (select count(1) "
              +"                   from LCIssuePol "
              +"                  where LCIssuePol.ProposalContNo = a.ProposalContNo "
              +"                    and a.grpcontno = '00000000000000000000') V, "
              +"                (select count(1) "
              +"                   from LCPENotice "
              +"                  where LCPENotice.ProposalContNo = a.ProposalContNo "
              +"                    and a.grpcontno = '00000000000000000000') W, "
              +"                (select count(1) "
              +"                   from LCRReport "
              +"                  where LCRReport.ProposalContNo = a.ProposalContNo "
              +"                    and a.grpcontno = '00000000000000000000') X, "
              +"                (select count(1) "
              +"                   from Loprtmanager "
              +"                  where Loprtmanager.otherNo = a.ProposalContNo " 
              +"                    and Loprtmanager.code = '05' "
              +"                    and a.grpcontno = '00000000000000000000') Y,b.UWOperator Z, "
              +"                case a.uwFlag "
              +"                  when '9' then "
              +"                   '正常承保' "
              +"                  when '4' then "
              +"                   '变更承保' "
              +"                  when '8' then "
              +"                   '延期承保' "
              +"                  when '5' then "
              +"                   '自核未通过' "
              +"                  when 'a' then "
              +"                   '撤销申请' "
              +"                  when '1' then "
              +"                   '谢绝承保' "
              +"                end A1, "
              +"                b.uwdate B1, "
              +"                case a.paymode "
              +"                  when '4' then "
              +"                   '银行转帐' "
              +"                  else "
              +"                   '自缴' "
              +"                end C1, "
              +"                (select min(EnterAccDate) "
              +"                   from ljtempfee "
              +"                  where otherno = a.contno "
              +"                    and riskcode = a.riskcode) D1, "
              +"                b.signdate E1,b.CustomGetPolDate F1,'' G1,C.NAME H1,a.agentcode I1, "
              +"                case a.payintv "
              +"                  when 1 then "
              +"                   '月缴' "
              +"                  when 3 then "
              +"                   '季缴' "
              +"                  when 6 then "
              +"                   '半年缴' "
              +"                  when 12 then "
              +"                   '年缴' "
              +"                end J1 "
              +"           from LcCONT B, lcpol a, ldcom C "
              +"          where a.renewcount <= 0 "
              +"            and C.COMCODE = B.MANAGECOM "
              +"            AND a.PROPOSALCONTNO = b.PROPOSALCONTNO "
              +"            and a.conttype = '1' "
              +"            and b.conttype = '1' "
              +"            and substr(b.ManageCom, 1, length('"+tMngCom+"')) = '"+tMngCom+"' "
              +"            and b.makedate between '"+tStartDate+"' and '"+tEndDate+"') as X "
              +"  order by A with ur "; 
    }else{
    	
      	tStrSQL ="select A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,A1,B1,C1,D1,E1,G1,F1,H1,getUniteCode(I1),J1    from     (    select a.contno A,a.prtno B,  '退保'  C,           b.appntname  D,a.insuredname E,a.riskcode F,a.mult G,  a.amnt H,a.prem I,a.polapplydate J,a.cvalidate K,         b.ReceiveDate  L,         b.FirstTrialDate  M,         b.FirstTrialOperator N,         (select makedate from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB01') O,         (select ScanOperator from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB01') P,         b.InputDate  Q,          b.InputOperator R,         a.ApproveDate  S,         a.ApproveCode  T,         (select '自核通过' from LCUWMaster where proposalcontno not in (select proposalcontno from lcuwerror where grpcontno='00000000000000000000') and a.grpcontno='00000000000000000000' and ProposalNo=a.ProposalNo)  U,         (select count(1) from LCIssuePol where LCIssuePol.ProposalContNo=a.ProposalContNo and a.grpcontno='00000000000000000000') V,           (select count(1) from LCPENotice where LCPENotice.ProposalContNo=a.ProposalContNo and a.grpcontno='00000000000000000000') W,           (select count(1) from LCRReport where LCRReport.ProposalContNo=a.ProposalContNo and a.grpcontno='00000000000000000000') X,          (select count(1) from  Loprtmanager where Loprtmanager.otherNo=a.ProposalContNo  and  Loprtmanager.code='05' and a.grpcontno='00000000000000000000') Y,        b.UWOperator Z,              case a.uwFlag when '9' then '正常承保' when '4' then '变更承保' when '8' then   '核保通知书待回复' when '5' then '自核未通过' when 'a' then '撤销申请' when '1' then '谢绝承保' end  A1,              b.uwdate  B1,         case a.paymode when  '4' then '银行转帐' else '自缴' end  C1,         (select min(EnterAccDate) from ljtempfee where  otherno=a.contno and riskcode=a.riskcode)  D1,         b.signdate E1,         b.CustomGetPolDate  F1,'' G1,    	 C.NAME H1,         a.agentcode I1, case a.payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' end J1        from LCCONT B,lBpol a,ldcom C  where a.edorno not like 'xb%' and C.COMCODE=B.MANAGECOM AND a.PROPOSALCONTNO=b.PROPOSALCONTNO and a.conttype='1' and b.conttype='1' and substr(b.ManageCom,1,length('"+tMngCom+"')) ='"+tMngCom+"' and b.makedate  between '"+tStartDate+"' and '"+tEndDate+"') as X     union      select A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,A1,B1,C1,D1,E1,G1,F1,H1,getUniteCode(I1),J1    from     (    select a.contno A,a.prtno B, '退保' C,           b.appntname  D,a.insuredname E,a.riskcode F,a.mult G,  a.amnt H,a.prem I,a.polapplydate J,a.cvalidate K,         b.ReceiveDate  L,         b.FirstTrialDate  M,         b.FirstTrialOperator N,         (select makedate from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB01') O,         (select ScanOperator from es_doc_main where a.prtno=doccode and busstype='TB' and subtype='TB01') P,         b.InputDate  Q,          b.InputOperator R,         a.ApproveDate  S,         a.ApproveCode  T,         (select '自核通过' from LCUWMaster where proposalcontno not in (select proposalcontno from lcuwerror where grpcontno='00000000000000000000') and a.grpcontno='00000000000000000000' and ProposalNo=a.ProposalNo)  U,         (select count(1) from LCIssuePol where LCIssuePol.ProposalContNo=a.ProposalContNo and a.grpcontno='00000000000000000000') V,           (select count(1) from LCPENotice where LCPENotice.ProposalContNo=a.ProposalContNo and a.grpcontno='00000000000000000000') W,           (select count(1) from LCRReport where LCRReport.ProposalContNo=a.ProposalContNo and a.grpcontno='00000000000000000000') X,          (select count(1) from  Loprtmanager where Loprtmanager.otherNo=a.ProposalContNo  and  Loprtmanager.code='05' and a.grpcontno='00000000000000000000') Y,        b.UWOperator Z,              case a.uwFlag when '9' then '正常承保' when '4' then '变更承保' when '8' then   '核保通知书待回复' when '5' then '自核未通过' when 'a' then '撤销申请' when '1' then '谢绝承保' end  A1,              b.uwdate  B1,         case a.paymode when  '4' then '银行转帐' else '自缴' end  C1,         (select min(EnterAccDate) from ljtempfee where  otherno=a.contno and riskcode=a.riskcode)  D1,         b.signdate E1,         b.CustomGetPolDate  F1,'' G1,    	 C.NAME H1,         a.agentcode I1, case a.payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' end J1        from LBCONT B,lBpol a,ldcom C  where B.edorno not like 'xb%' and C.COMCODE=B.MANAGECOM AND a.PROPOSALCONTNO=b.PROPOSALCONTNO and a.conttype='1' and b.conttype='1' and substr(b.ManageCom,1,length('"+tMngCom+"')) ='"+tMngCom+"' and b.makedate  between '"+tStartDate+"' and '"+tEndDate+"') as X order by A with ur  "  ;
              	
    }
    
     fm.querySql.value = tStrSQL;
     fm.action="AppStatisticSubDown.jsp";
     fm.submit();

}

