<%
//程序名称：ProposalManagerStatusInit.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox() { 
}                                     

function initForm() {
  try {
  	initInpBox();
  	initPolGrid();
  }
  catch(re) {
    alert("ProposalManagerStatusInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 领取项信息列表的初始化
var PolGrid;
function initPolGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            	//列宽
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="投保单印刷号";         		//列名
    iArray[1][1]="150px";            		//列宽
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="操作员名称";           //列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="录入日期";                //列名
    iArray[3][1]="100px";            		//列宽
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
    //这些属性必须在loadMulLine前
    PolGrid.mulLineCount = 0;   
    PolGrid.displayTitle = 1;
    PolGrid.hiddenPlus = 1;
    PolGrid.hiddenSubtraction = 1;
    PolGrid.canSel = 1;
    PolGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
    //PolGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>

	
