<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
                          
function initForm() {
  try {
      initClaimNumGrid(); 
  }
  catch(re) {
    alert("初始化界面错误!");
  }
}

var ClaimNumGrid;
function initClaimNumGrid() {                               
  var iArray = new Array();
    
  try {

      iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
      
      iArray[1]=new Array();
      iArray[1][0]="团体客户号";         	  //列名
      iArray[1][1]="50px";         		  //列名
    
      iArray[2]=new Array();
	  iArray[2][0]="险种号码";         	  //列名
	  iArray[2][1]="50px";            	  //列宽
	  iArray[2][2]=200;            		  //列最大值
	
      iArray[3]=new Array();
	  iArray[3][0]="赔付顺序";         	  //列名
	  iArray[3][1]="50px";            	  //列宽
	  iArray[3][2]=200;            		  //列最大值
	  
	  ClaimNumGrid = new MulLineEnter( "fm" , "ClaimNumGrid" );
      //这些属性必须在loadMulLine前
      
      ClaimNumGrid.mulLineCount = 4;
      ClaimNumGrid.displayTitle = 1;

      ClaimNumGrid.canSel=1;
      ClaimNumGrid.canChk=0;
      ClaimNumGrid.hiddenPlus=1;
      ClaimNumGrid.hiddenSubtraction=1;

      ClaimNumGrid.loadMulLine(iArray);
      ClaimNumGrid.selBoxEventFuncName ="query";//设置js，反选用
      //这些操作必须在loadMulLine后面
	
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
