<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	String tGrpPNo = "";
	try
	{
		tGrpPNo = request.getParameter("GrpPNo");
	}
	catch( Exception e )
	{
		tGrpPNo = "";
	}
%>
<head>
<script>
	var groupPNo = "<%=tGrpPNo%>" //判断从何处进入保单录入界面,该变量需要在界面出始化前设置.
</script>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="./GrpPolQuery.js"></SCRIPT>
  <%@include file="./GrpPolQueryInit.jsp"%>
  <title>集体投保单下个人投保单查询 </title>
</head>
<body  onload="initForm();" > 

<form action="./ProposalSave.jsp" method=post name=fm target="fraSubmit">

  <table class= common border=0 width=100%>
    <TR  class= common>
 <!--     <TD  class= titles>
        集体投保单号码
      </TD>-->
        <Input class="readonly" readonly name=GrpProposalNo type="hidden">
    </TR>
  </table>
    
  <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
	
    <table  class= common align=center>
      	<TR  class= common>
   <!--       <TD  class= title>
            投保单号码
          </TD>-->
            <Input class=common name=ProposalNo type="hidden">
          <TD  class= title>
            被保人名称
          </TD>
          <TD  class= input>
            <Input class=common name=InsuredName >
          </TD>
          <TD  class= title>
            被保人性别
          </TD>
          <TD  class= input>
            <Input class=code name=InsuredSex ondblclick="return showCodeList('Sex', [this]);" onkeyup="return showCodeListKey('Sex', [this]);" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
            <Input class=common name=InsuredBirthday >
          </TD>
          <TD  class= title>
            被保人投保年龄
          </TD>
          <TD  class= input>
            <Input class=common name=InsuredAppAge >
          </TD>
          <TD  class= title>
            职业类别/工种编码
          </TD>
          <TD  class= input>
            <Input class=common name=OccupationType >
          </TD>     
        </TR>
        <TR  class= common>
          <TD  class= title> 
           客户内部号码
          </TD>
          <TD  class= input>
            <Input class=common name=SequenceNo >
          </TD> 
        </TR>

    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();">   
   
   
   
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol1);">
    		</td>
    		<td class= titleImg>
    			 集体下个人投保单信息
    		</td>
    	</tr>
    </table>
    
  	<Div  id= "divLCGrpPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    
    <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </Div> 	
    				
  	</div>
  	
  	<input type=hidden id="fmAction" name="fmAction">
  	
  	<p>
      <INPUT VALUE="查询个人投保单明细" TYPE=button onclick="showPolDetail();"> 
      <INPUT VALUE="删除个人投保单" TYPE=button onclick="deleteClick();"> 
      <INPUT VALUE="返回" TYPE=button onclick="returnMain();"> 
  	</p>
  	
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
