<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		String GrpContNo = request.getParameter("GrpContNo");
		String LoadFlag = request.getParameter("LoadFlag");
		String Resource = request.getParameter("Resource");
		String PrtNo = request.getParameter("PrtNo");
	%>
	<head>
		<script>
			var GrpContNo="<%=GrpContNo%>";
			var PrtNo="<%=PrtNo%>"
			var LoadFlag="<%=LoadFlag%>";
 			var Resource="<%=Resource%>";
 		</script>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

		<SCRIPT src="LGrpQueryInsuredInput.js"></SCRIPT>
		<%@include file="LGrpQueryInsuredInit.jsp"%>
		<title>被保人错误信息查询</title>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;">
					</td>
					<td class=titleImg>
						合同信息
					</td>
				</tr>
			</table>
			<table class=common align=center>
				<TR class=common>
					<TD class=title>
						印刷号
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=PrtNo>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=ManageCom>
					</TD>
					<TD class=title>
						投保单位名称
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=GrpName>
					</TD>
				</TR>
			</table>
			<br>
			<hr>
			<table style="height:40px;">
       			<tr>
            		<td class=common>
                		<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;">
            	</td>
            		<td class= titleImg>被保险人导入错误信息：</td>
        		</tr>
			</table>
			<table class=common align=center>
				<tr class=common>
					<TD class=title>
						保障计划编码
					</TD>
					<TD class=input>
						<Input class='common' name=ContPlanCode readonly>
					</TD>
					<TD class=title>
						被保人姓名
					</TD>
					<TD class=input>
						<Input class='common' name=Name readonly>
					</TD>
					<TD class= title8>
        	  			错误类型
        			</TD>
        			<TD class=input>
        				<Input class=codeno name=ContPrintType  CodeData="0|^0|全部^1|校验错误^2|算费错误" ondblclick="return showCodeListEx('ContPrintType',[this,ContPrintTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContPrintType',[this,ContPrintTypeName],[0,1],null,null,null,1);" readonly="readonly" /><input class=codename name=ContPrintTypeName readonly=true > 		     		
        			</TD> 
				</tr>
			</table>
			<table>
				<tr>
					<input type="hidden" class="common" name="querySql" >
					<td><INPUT VALUE="查  询" class=cssButton TYPE=button onclick="queryError();"></td>
            		<td><INPUT VALUE="下载错误清单" class=cssButton TYPE=button onclick="downloadError();"></td>
				</tr>
			</table>
			<br>
    		<Div  id= "divDiskErr" style= "display: ''">
        		<Table  class= common>
            		<TR  class= common>
                		<TD text-align: left colSpan=1>
                    		<span id="spanDiskErrQueryGrid" ></span>
                		</TD>
            		</TR>
        		</Table>
        		<div align="center">
            		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
            		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
            		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
            		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
        		</div>
    		</Div>
    		<br>
    		<table>
    			<tr style= "display: 'none'">
    				<td><INPUT VALUE="修改被保险人" class=cssButton TYPE=button onclick="updateInsured();"></td>
    				<td><INPUT VALUE="删除被保险人" class=cssButton TYPE=button onclick="deleteInsured();"></td>
    			</tr>
    		</table>
    		<br>
    		<hr>
    		<table>
				<tr>
					<td>
						<input class="button" type="button" value="关  闭"
							onclick="goBack();">
					</td>
				</tr>
			</table>
			<br>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>