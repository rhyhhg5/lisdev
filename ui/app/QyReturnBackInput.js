var arrDataSet
var showInfo;
var turnPage = new turnPageClass();

// 查询按钮
function easyQueryClick()
{
    // 初始化表格
    initPolGrid();
    
    if(!verifyInput2())
    {
		return false;
    }
    
    if(fm.PrtNo.value == "" && fm.ContNo.value == "")
    {
        alert("印刷号和保单合同号至少录入一个！");
        return false;
    }
    
    // 书写SQL语句
    var strSQL = "";
    if(fm.ContType.value == 1)
    {
        strSQL = "select a.ContNo, b.PrtNo, b.CValiDate,b.AppntName,b.Prem,b.agentcode,a.agentName,"
            + "a.managecom,a.getpoldate,"
            + "a.getpolman,a.sendpolman"
            + " from LCContGetPol a,LCCont b "
            + "where a.contno=b.contno   and a.conttype='1' "
            + "and b.conttype='1' and b.uwflag != 'a' "
            + "and b.ManageCom like '" + manageCom + "%' "
            + getWherePart('a.ContNo','ContNo')
            + getWherePart('a.PrtNo','PrtNo')
            + getWherePart('a.ManageCom','ManageCom','like')
            + " order by a.ManageCom,b.AgentCode";
    }
    else if(fm.ContType.value == 2)
    {
        strSQL = "select a.ContNo, b.PrtNo, b.CValiDate,b.GrpName,b.Prem,b.agentcode,a.agentName,"
            + "a.managecom,a.getpoldate,"
            + "a.getpolman,a.sendpolman "
            + " from LCContGetPol a,LCGrpCont b "
            + "where a.contno=b.grpcontno  and a.conttype='2' "
            + "and b.ManageCom like '" + manageCom + "%' "
            + getWherePart('a.ContNo','ContNo')
            + getWherePart('a.PrtNo','PrtNo')
            + getWherePart('a.ManageCom','ManageCom','like')
            + " order by a.ManageCom,b.AgentCode";
    }
    turnPage.pageLineNum = 50;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    fm.querySql.value = strSQL;
}



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}


//初始化查询数据
function Polinit()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) 
  {
  	if (PolGrid.getSelNo(i)) 
  	{
  		checkFlag = PolGrid.getSelNo();
		break;
	}
  }
  if (checkFlag) 
  {
	var tContNo=PolGrid.getRowColData(checkFlag-1,1);

	fm.all('mContNo').value = tContNo;
	
  }
  else
  {
	alert("请先选择一条保单信息！"); 
  }
}


//修改保单的回执回销日期
function updatepol()
{

    fm.all('fmtransact').value = "UPDATE";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.submit();
}


