<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>";// 记录管理机构
    var comcode = "<%=tGI.ComCode%>";// 记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<!-- 通用录入校验要是用的 js 文件 -->
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!-- 使用双击下拉需要引入的 JS 文件 -->
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>契约保单修改市场类型</title>
<!-- 自己的输入验证 js -->
<script src="FloatRateAdjust.js"></script>
<!-- 初始化 mulline -->
<%@include file="FloatRateAdjustInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="FloatRateAdjustSave.jsp" method="post" name="fm" target="fraSubmit">

		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>

		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>修改类型</td>
				<td class=input>
					<Input class=codeNo readonly=true name=typecode
					CodeData="0|^0|学平险^1|一带一路^2|3人校验^3|一带一路保险年期" ondblClick="showCodeListEx('typecode',[this,typename],[0,1]);" onkeyup="showCodeListKeyEx('typecode',[this,typename],[0,1]);"><input
					class=codename name=typename readonly=true>
				</td>
				
			</tr>
		</table>
		<INPUT Value="查询保单" class="cssButton" TYPE=button
			onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divLCPol1);"></td>
				<td class=titleImg>保单信息</td>
			</tr>
		</table>
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1><span id="spanPolGrid" ></span>
					
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT Value="首  页" class="cssButton" TYPE=button
					onclick="getFirstPage();">
				<INPUT Value="上一页" class="cssButton" TYPE=button
					onclick="getPreviousPage();">
				<INPUT Value="下一页" class="cssButton" TYPE=button
					onclick="getNextPage();">
				<INPUT Value="尾  页" class="cssButton" TYPE=button
					onclick="getLastPage();">
			</table>
		</div>
		<br />
		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>修改前最小值</td>
				<td class=input><Input class=readonly readonly=true
					name=beforeMinValue  ></td>
					<td class=title id="bMaxValue" style="display: ''">修改前最大值</td>
					<td class=input><input class=readonly   style="display: ''" readonly=true
					name=beforeMaxValue ></td>
				<td class=title></td>
				<td class=input></td>
				<td class=title></td>
				<td class=input></td>
			</tr>
			<tr class=common>
				<td class=title>修改后最小值</td>
				<td class=input><Input class=input 
					name=afterMinValue  ></td>
					<td class=title id="aMaxValue" style="display: ''">修改后最大值</td>
					<td class=input><input class=input  style="display: ''"
					name=afterMaxValue ></td>
					
				<td class=title></td>
				<td class=input></td>
				<td class=title></td>
				<td class=input></td>
			</tr>
		</table>
		<INPUT Value="修改" class="cssButton" TYPE=button
			onclick="modifyValue();">
		<input type=hidden id="UpdateType" name="UpdateType">
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
