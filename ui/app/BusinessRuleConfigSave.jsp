<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：契约保单发送及下载规则设置
//创建日期：2008-3-13 02:24下午
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%@include file="../common/jsp/GetMutLine.jsp"%>

<%
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  String tRuleType = request.getParameter("RuleType");
  String tBussType = request.getParameter("BussType");
  
  LDBusinessRuleSet tLDBusinessRuleSet = new LDBusinessRuleSet();
  
  //String[] tRuleType = request.getParameterValues("BusinessRuleGrid1");
  String[] tRuleName = request.getParameterValues("BusinessRuleGrid2");
  //String[] tBussType = request.getParameterValues("BusinessRuleGrid3");
  String[] tBussName = request.getParameterValues("BusinessRuleGrid4");
  String[] tComCode = request.getParameterValues("BusinessRuleGrid5");
  String[] tRuleValue = request.getParameterValues("BusinessRuleGrid7");
  
  if(tComCode != null)
  {
  	for(int i = 0; i < tComCode.length; i++ )
  	{
  		LDBusinessRuleSchema tLDBusinessRuleSchema = new LDBusinessRuleSchema();
  		tLDBusinessRuleSchema.setRuleType(tRuleType);
  		tLDBusinessRuleSchema.setRuleName(tRuleName[i]);
  		tLDBusinessRuleSchema.setBussType(tBussType);
  		tLDBusinessRuleSchema.setBussName(tBussName[i]);
  		tLDBusinessRuleSchema.setComCode(tComCode[i]);
  		tLDBusinessRuleSchema.setRuleValue(tRuleValue[i]);
  		tLDBusinessRuleSet.add(tLDBusinessRuleSchema);
  	}
  }
  
  LDBusinessRuleSchema aLDBusinessRuleSchema = new LDBusinessRuleSchema();
  aLDBusinessRuleSchema.setRuleType(tRuleType);
  aLDBusinessRuleSchema.setBussType(tBussType);
  
  VData tVData = new VData();
	tVData.add(tG);
	tVData.add(tLDBusinessRuleSet);
	tVData.add(aLDBusinessRuleSchema);
	
  BusinessRuleConfigUI tBusinessRuleConfigUI = new BusinessRuleConfigUI();
	if (!tBusinessRuleConfigUI.submitData(tVData, ""))
	{
		Content = "操作失败，原因是:" + tBusinessRuleConfigUI.mErrors.getFirstError();
		FlagStr = "Fail";
	}
  else
  {
	  Content = "操作成功! ";
	  FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>