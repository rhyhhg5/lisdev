<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
	tGI=(GlobalInput)session.getValue("GI");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="RelatedTransactionQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="RelatedTransactionQueryInit.jsp"%>
  <title>关联方查询</title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="RelatedTransactionQuerySave.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询关联方名单条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
      	  <TD  class= title>
            关联方编号：
          </TD>
          <TD  class= input>
            <Input class= common name=RelateNo >
          </TD>
          <TD  class= title>
            关联方名称：
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
        <TD  class= title>
            关联方类别：
        </TD>
        <TD  class= input8>
            <Input class=codeno name=RelatedType CodeData="0|1^1|个人关联方^2|团体关联方" ondblclick="return showCodeListEx('RelatedType',[this,RelatedTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('RelatedType',[this,RelatedTypeName],[0,1],null,null,null,1);"><input class=codename name=RelatedTypeName readonly=true>
          </TD>
        </TR>
        <TR>
        	<TD class=input8>
        		关联方导入起期：
        	</TD>
        	<TD class=input8>
				<Input class="coolDatePicker" dateFormat="short" name=ImportDateBegin >
			</TD>
			<TD class=input8>
				关联方导入止期：
			</TD>
			<TD class=input8>
				<Input class="coolDatePicker" dateFormat="short" name=ImportDateEnd >
			</TD>
        </TR>
    </table>
          <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrp1);">
    		</td>
    		<td class= titleImg>
    			 关联方名单信息：
    		</td>
    	</tr>
    	<tr>
    	</tr>
    </table>
  	<Div  id= "divRelatedParty" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanRelatedPartyGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
  	</Div>
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  	<p>
  	  <INPUT VALUE="批次导入关联方信息" class = cssButton TYPE=button onclick="AddList();"> 
  	  <INPUT VALUE="新增关联方信息" class = cssButton TYPE=button onclick="AddInput();"> 
      <INPUT VALUE="修改关联方信息" class = cssButton TYPE=button onclick="UpdateInput();">      
      <INPUT VALUE="删除关联方信息" class = cssButton TYPE=button onclick="DeleteInput();">
      <input type=hidden name=RelateNobak value="">
      <input type=hidden name=transact value="">
  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
