//该文件中包含客户端需要处理的函数和事件
var arrDataSet;
var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();

//查询事件
function easyQueryClick() {
	if (fm.all("InputDataNo1").value == "" || fm.all("InputDataNo2").value == "") {
		alert("请输入查询日期");
		return false;
	}
	var sDate = fm.all("InputDataNo1").value;
	var eDate = fm.all("InputDataNo2").value;
	if ((eDate.split("-")[0] > sDate.split("-")[0]) || ((eDate.split("-")[0] == sDate.split("-")[0]) && (eDate.split("-")[1] - sDate.split("-")[1]) >= 3)) {
		alert("起始日期和終止日期月份之间不能大於三個月！！");
		return false;
	}
	var strSQL = " select '1',lgc.prtno,trim(lgc.prtno)||'801', "
	         + " (select codename from ldcode where codetype = 'paymode' and code = lgc.paymode ), "
	         + " lgc.prem,lgc.grpname,lgc.cvalidate,getUniteCode(lgc.agentcode),(select name from laagent where agentcode = lgc.agentcode) " 
	         + " from lcgrpcont lgc "
	         + " where lgc.uwflag in ('4','9') " 
	         + " and lgc.appflag in ('0', '9') " 
	         //+ " and cardflag is null " 
	         + " and lgc.prem<>0 and lgc.signdate is null " 
	         + " and (select count(1) from lcrnewstatelog where prtno=lgc.prtno)=0 " 
	         + " and lgc.paymode in ('1','2','3','5','6','11','12') " 
	         + " and not exists (select 1 from ljtempfee where otherno=lgc.prtno and othernotype = '5') " 
	         + getWherePart('ManageCom', 'ManageCom','like')
		     + getWherePart('Makedate', 'InputDataNo1','>=')
		     + getWherePart('Makedate', 'InputDataNo2','<=')
		     + getWherePart('PrtNo', 'PrtNo')
	         + " with ur ";
	turnPage.queryModal(strSQL, ContPrintDailyGrid);
	fm.querySql.value = strSQL;
}

//下载事件
function download() {
	if (fm.querySql.value != null && fm.querySql.value != "" && ContPrintDailyGrid.mulLineCount > 0) {
		fm.action = "GrpWaitFeeInputDownload.jsp";
		//fm.target = "_blank";
		fm.submit();
	} else {
		alert("请先查询后在执行下载操作！");
		return;
	}
}

