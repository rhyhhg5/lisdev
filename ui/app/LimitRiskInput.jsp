<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2009-05-04
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="LimitRiskInput.js"></script>
    <%@include file="LimitRiskInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="LimitRiskSave.jsp" method="post" name="fm" target="fraSubmit">
	  	<input type=hidden id="fmtransact" name=fmtransact>
        <table>
            <tr>
                <td class="titleImg">查询条件</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title8">险种编码</td>
                    <td class="input8">
<!--     					<Input name="RiskCode" class=codeNo  CodeData="0|^162501|团体管理式补充医疗保险（B款）^170301|守护专家特需医疗（推荐版）团体医疗保险^170401|守护专家健康无忧团体医疗保险^170501|补充团体医疗保险（A款）"  ondblclick="showCodeListEx('RiskCode',[this,RiskName],[0,1],null,null,null,1);"  onkeyup=" showCodeListKeyEx('RiskCode',[this,RiskName],[0,1],null,null,null,1);"><Input name="RiskName" class= "codename" readonly="readonly" /> -->
			          	<Input class="codeNo" name="RiskCode" verify="险种编码" ondblclick="return showCodeList('limintriskcode',[this,RiskName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('limintriskcode',[this,RiskName],[0,1],null,null,null,1);"><input class="codename" name="RiskName" readonly=true />
                    </td>
                    <td class="title8">销售渠道12</td>
                    <td class="input8">
			          	<Input class="codeNo" name="SaleChnl" verify="销售渠道" ondblclick="return showCodeList('unitesalechnlsgrp',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('unitesalechnlsgrp',[this,SaleChnlName],[0,1],null,null,null,1);"><input class="codename" name="SaleChnlName" readonly=true />
                    </td>
                </tr>
             </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="query();" /> 	
                </td>
          		<td class="common">
                    <input class="cssButton" type="button" value="新  增" onclick="save();" /> 	
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divCertifyListGrid)" />
                </td>
                <td class="titleImg">清单列表</td>
            </tr>
        </table>
        <div id="divCertifyListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertifyListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divCertifyListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>        
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="delete" value="删  除" onclick="deleteClick();" />   
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="query1" value="查看放开管理机构" onclick="queryManagecom();" />   
                </td>
            </tr>
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">管理机构列表</td>
            </tr>
        </table>
        <div id="divCertInsuListGrid" style="display: 'none'">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertInsuListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divCertInsuListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                     
            </div>
            <div id="divManageCom" style="display: 'none'">
	         	<table class="common">
	                <tr class="common">
<!-- 	                    <td class="title8">险种编码</td> -->
<!-- 	                    <td class="input8"> -->
<!-- 	    					<Input name="RiskCode" class=codeNo  CodeData="0|^162501|团体管理式补充医疗保险（B款）^170301|守护专家特需医疗（推荐版）团体医疗保险^170401|守护专家健康无忧团体医疗保险^170501|补充团体医疗保险（A款）"  ondblclick="showCodeListEx('RiskCode',[this,RiskName1],[0,1],null,null,null,1);"  onkeyup=" showCodeListKeyEx('RiskCode',[this,RiskName1],[0,1],null,null,null,1);"><Input name="RiskName1" class= "codename" readonly="readonly" /> -->
<!-- 	                    </td> -->
	                    <td class="title8">管理机构</td>
	                    <td class="input8">
							<input NAME="ManageCom"  CLASS=codeNo ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,'1 and #1# = #1# and Length(trim(comcode))=8','1',1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,'1 and #1# = #1# and Length(trim(comcode))=8','1',1);" verify="管理机构|code:comcode"><input class=codename name=ManageComName readonly=true />  
	                    </td>
	                    <td class="title8"></td>
	                    <td class="title8"></td>
	                </tr>
	        	</table>
	           	<table>
	            	<tr>
	           			<td class="common">
	               			<input class="cssButton" type="button" value="新增管理机构" onclick="savem();" /> 	
	                	</td>
	          			<td class="common">
	                    	<input class="cssButton" type="button" value="删除管理机构" onclick="deletem();" /> 	
	                	</td>
	            	</tr>
	        	</table>
        	</div>
        </div>
        
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
