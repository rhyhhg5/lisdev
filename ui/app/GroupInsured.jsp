
<DIV id=DivLCInsuredButton STYLE="display:''">
<!-- 被保人信息部分 -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCInsured);">
</td>
<td class= titleImg>
被保人信息（客户号：<Input class= common name=InsuredNo >
<INPUT id="butBack" VALUE="查询" class=cssButton TYPE=button onclick="queryInsuredNo();"> 首次投保客户无需填写客户号）
<Div  id= "divSamePerson" style="display:'none'">
<font color=red>
如投保人为被保险人本人，可免填本栏，请选择
<INPUT TYPE="checkbox" NAME="SamePersonFlag" onclick="isSamePerson();">
</font>
</div>
</td>
</tr>
</table>

</DIV>
<DIV id=DivLCInsured STYLE="display:''">
 <table  class= common>
        <TR  class= common>        
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name elementtype=nacessary verify="被保险人姓名|notnull&len<=20" >
          </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class="code" name="IDType" elementtype=nacessary verify="被保险人证件类型|code:IDType&notnull" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name="IDNo" elementtype=nacessary onblur="getBirthdaySexByIDNo(this.value);" verify="被保险人证件号码|notnull&len<=20" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class="code" name=Sex elementtype=nacessary verify="被保险人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
          </TD>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
          <input class="coolDatePicker" elementtype=nacessary dateFormat="short" name="Birthday" verify="被保险人出生日期|notnull&date" >
          </TD>
          <TD  class= title>
            年龄
          </TD>
          <TD  class= input>
          <input class="readonly" readonly name="Age" >
          </TD>
        </TR>
        <TR>
          <TD  class= title>
            地址代码
          </TD>
          <TD  class= input>
            <Input class="code" name="AddressNo"  ondblclick="return showCodeList('GetAddressNo',[this],null,null,fm.InsuredNo.value, 'CustomerNo');" onkeyup="return showCodeListKey('GetAddressNo',[this],null,null,fm.InsuredNo.value, 'CustomerNo');" onfocus="getdetailaddress();">
          </TD>
	</TR>     
        <TR  class= common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="PostalAddress"  verify="被保险人联系地址|len<=80" >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name="ZipCode"  MAXLENGTH="6" verify="被保险人邮政编码|zipcode" >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
          <input class= common name="Phone"  verify="被保险人家庭电话|len<=18" >
          </TD>
          <TD  class= title>
            移动电话
          </TD>
          <TD  class= input>
            <Input class= common name="Mobile" verify="被保险人移动电话|len<=15" >
          </TD>
          <TD  class= title>
            电子邮箱
          </TD>
          <TD  class= input>
            <Input class= common name="EMail" verify="被保险人电子邮箱|len<=20" >
          </TD>
        </TR>
	
	<TR>
	  <TD  class= title>
            与主被保险人关系</TD>
          <TD  class= input>
            <Input class="code" name="RelationToMainInsured" elementtype=nacessary verify="主被保险人关系|code:Relation&notnull" ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);"></TD>
          <TD  class= title>
            职业代码
          </TD>
          <TD  class= input>
            <Input class="code" name="OccupationCode"  elementtype=nacessary verify="被保险人职业代码|code:OccupationCode&notnull" ondblclick="return showCodeList('OccupationCode',[this]);" onkeyup="return showCodeListKey('OccupationCode',[this]);" onfocus="getdetailwork();">
          </TD>
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input>
            <Input class="code" name="OccupationType"  verify="被保险人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
          </TD>  
	</TR>     
	      
  </TABLE>
</DIV>
	<TABLE class= common>
		<TR class= common>
			<TD  class= title>
                <DIV id="divContPlan" style="display:'none'" >
	                <TABLE class= common>
		                <TR class= common>
			                <TD  class= title>
                                保险计划
                            </TD>
                            <TD  class= input>
                                <Input class="code" name="ContPlanCode" ondblclick="showCodeListEx('ContPlanCode',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ContPlanCode',[this],[0],'', '', '', true);">
                            </TD>
		                </TR>
	                </TABLE>
                </DIV>
            </TD>
            <TD  class= title>
                <DIV id="divExecuteCom" style="display:'none'" >
	                <TABLE class= common>
		                <TR class= common>
			                <TD  class= title>
                                处理机构
                            </TD>
                            <TD  class= input>
                                <Input class="code" name="ExecuteCom" ondblclick="showCodeListEx('ExecuteCom',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ExecuteCom',[this],[0],'', '', '', true);">
                            </TD>
		                </TR>
	                </TABLE>
                </DIV>
            </TD>
            <TD  class= title>
            </TD>		
		</TR>
	</TABLE>