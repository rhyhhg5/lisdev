
<%
	//程序名称：ComplexRiskInputInit.jsp
	//程序功能：功能描述
	//创建日期：2005-05-30 18:29:02
	//创建人  ：ctrHTML
	//更新人  ：
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script language="JavaScript">
function initInpBox() { 
  try {     
  }
  catch(ex) {
    alert("在ComplexBnfInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
        fm.all('ContNo').value = ContNo;
    	initInpBox();
    	initPolGrid();
		showRiskInfo();
  }
  catch(re) {
    alert("ComplexBnfInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();                    
	iArray[1][0]="被保险人";         		//列名    
	iArray[1][1]="40px";         		//列名    
	iArray[1][3]=0;         		//列名        
		
	iArray[2]=new Array();                    
	iArray[2][0]="险种代码";         		//列名    
	iArray[2][1]="30px";         		//列名    
	iArray[2][3]=0;         		//列名        
		
    iArray[3]=new Array();
    iArray[3][0]="受益人类别"; 		//列名
    iArray[3][1]="55px";		//列宽
    iArray[3][2]=40;			//列最大值
    iArray[3][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="BnfType";
    iArray[3][9]="受益人类别|notnull&code:BnfType";

    iArray[4]=new Array();
    iArray[4][0]="姓名"; 	//列名
    iArray[4][1]="60px";		//列宽
    iArray[4][2]=30;			//列最大值
    iArray[4][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[4][9]="姓名|notnull&len<=20";//校验

    iArray[5]=new Array();
    iArray[5][0]="证件类型"; 		//列名
    iArray[5][1]="50px";		//列宽
    iArray[5][2]=40;			//列最大值
    iArray[5][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[5][4]="IDType";
    iArray[5][9]="证件类型|notnull&code:IDType";

    iArray[6]=new Array();
    iArray[6][0]="证件号码"; 		//列名
    iArray[6][1]="120px";		//列宽
    iArray[6][2]=80;			//列最大值
    iArray[6][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[6][9]="证件号码|notnull&len<=20";

    iArray[7]=new Array();
    iArray[7][0]="与被保人关系"; 	//列名
    iArray[7][1]="70px";		//列宽
    iArray[7][2]=60;			//列最大值
    iArray[7][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[7][4]="Relation";
    iArray[7][9]="与被保人关系|code:Relation";

    iArray[8]=new Array();
    iArray[8][0]="受益比例"; 		//列名
    iArray[8][1]="50px";		//列宽
    iArray[8][2]=40;			//列最大值
    iArray[8][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[8][9]="受益比例|Decimal&len<=10";

    iArray[9]=new Array();
    iArray[9][0]="受益顺序"; 		//列名
    iArray[9][1]="50px";		//列宽
    iArray[9][2]=40;			//列最大值
    iArray[9][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[9][4]="OccupationType";
    iArray[9][9]="受益顺序|code:OccupationType";
            
    iArray[10]=new Array();  
    iArray[10][0]="polno";
    iArray[10][1]="0px";   
    iArray[10][3]=0;
    
    iArray[11]=new Array();  
    iArray[11][0]="bnfno";
    iArray[11][1]="0px";   
    iArray[11][3]=0;
		     
    
    PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
    //这些属性必须在loadMulLine前           
    
    PolGrid.mulLineCount = 0;   
    PolGrid.displayTitle = 1;
    PolGrid.hiddenPlus = 1;
    PolGrid.hiddenSubtraction = 1;
    PolGrid.canSel = 1;
    PolGrid.canChk = 0;
    PolGrid.selBoxEventFuncName = "showBnfInfo";
    PolGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
