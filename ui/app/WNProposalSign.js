//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var arrDataSet;
var k = 0;
//提交，保存按钮对应操作
function signPol()
{
	var checkedRowNum = 0 ;
  var rowNum = PolGrid. mulLineCount ; 
  var count = -1;
  
  for ( var i = 0 ; i< rowNum ; i++ )
  { 
  	if( checkedRowNum > 1)
      break;
  	if(PolGrid.getChkNo(i)) 
  	{
  	   checkedRowNum = checkedRowNum + 1;
       count = i;
    }
  }  
  if(checkedRowNum != 1)

  {
  	alert("请先选择一条保单!");
  }else{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.all("signbutton").disabled = true;
  //showSubmitFrame(mDebug);

  fm.submit(); //提交 
}
}


//缴费催办查询__暂时注销
function UrgPayQuery()
{ 
  var checkedRowNum = 0 ;
  var rowNum = PolGrid. mulLineCount ; 
  var count = -1;
  for ( var i = 0 ; i< rowNum ; i++ )
  { 
  	if( checkedRowNum > 1)
      break;
  	if(PolGrid.getChkNo(i)) 
  	{
  	   checkedRowNum = checkedRowNum + 1;
       count = i;
    }
  }  
  if(checkedRowNum == 1)
  {
  	var cProposalNo = PolGrid.getRowColData(count,1,PolGrid);//投保单号
  	//alert(cProposalNo);
  	window.open("../uw/OutTimeQueryMain.jsp?ProposalNo1="+cProposalNo);
  }
  if(checkedRowNum == 2 || checkedRowNum == 0)
  {			
	alert("请只选择一条投保单进行缴费催办查询!");  	
  }
  
}

//发缴费催办通知书_暂时注销,前台及后台程序均已实现.只是报表未描述.待民生提出需求后在启用
function SendUrgPay()
{
  var checkedRowNum = 0 ;
  var rowNum = PolGrid. mulLineCount ; 
  var count = -1;
  for ( var i = 0 ; i< rowNum ; i++ )
  { 
  	if( checkedRowNum > 1)
      break;
  	if(PolGrid.getChkNo(i))
  	 {
  	   checkedRowNum = checkedRowNum + 1;
       count = i;
      }
  } 
  if(checkedRowNum == 1)
  {
  	var cProposalNo = PolGrid.getRowColData(count,1,PolGrid);//投保单号
        cOtherNoType="00"; //个人投保单主险号码类型
        cCode="15";        //缴费催办通知书单据类型  
   if (cProposalNo != "")
   {
	  	showModalDialog("../uw/UWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");	  
   }
  }
  if(checkedRowNum == 2 || checkedRowNum == 0)
  {			
	alert("请只选择一条投保单进行发缴费催办通知书!");  	
  }
  
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.all("signbutton").disabled=false;
  if (FlagStr == "Fail" )
  {   
	//if(content.length>1480)
	//{
	  //content="后台反馈的提示信息过多，已超过最大显示范围！请减少提交纪录数量(建议18条数据以下)";	
	//} 
	 //alert(content);
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");   
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:800px;dialogHeight:450px");   
  }
  else
  { 
  	
	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");   
  //  showModalDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:800px;dialogHeight:450px");   
 	initForm();
//	easyQueryClick();
    //执行下一步操作
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{
  initPolGrid();
  if(!verifyInput2())
	return false;
  	k++;
  // alert( comcode );
/*
	// 书写SQL语句
  var strSql = "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName,UWDate from LCPol where 1=1 "
				// + " and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+comcode+"','Ph')=1 "
				 + "and AppFlag='0' "						// 投保状态				
				 + "and ApproveFlag='9' "					// 已经复核
				 + "and UWFlag in ('9','4') "			    // 核保通过
				 + "and GrpPolNo='00000000000000000000' "	// 个人投保单
				// + "and ContNo='00000000000000000000' "		// 个人投保单
				 + getWherePart( 'ProposalNo' )
				 + getWherePart( 'ManageCom','ManageCom', 'like' )
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'PrtNo' )
				 + "and PrtNo in (select PrtNo from lcpol where  1=1 " 			
				 + "and ApproveFlag='9' "					// 已经复核
				 + "and UWFlag in ('9','4') "			    // 核保通过
				 + "and GrpPolNo='00000000000000000000' "	// 个人投保单
				// + "and ContNo='00000000000000000000' "		// 个人投保单
				 + getWherePart( 'ManageCom','ManageCom', 'like' )
				 + getWherePart( 'AgentCode' )
				 + " and PolNo=MainPolNo )"	 
				 + " and PolNo=MainPolNo "  //主险
				 + " and ManageCom like '" + comcode + "%%'"
				 + " Order by UWDate";

 var strSql = " select contno, prtno, '', AppntName,'', uwdate from lccont where 1=1"
             + " and appflag!='1'" 
              + " and UWFlag in ('9','4') "			    // 核保通过             
	     + " and GrpContNo='00000000000000000000' "
	     + getWherePart( 'ProposalNo' )
		 + getWherePart( 'ManageCom','ManageCom', 'like' )
		 + getWherePart( 'AgentCode' )
		// + getWherePart( 'RiskCode' )
		 + getWherePart( 'PrtNo' )
		 + " and ManageCom like '" + comcode + "%%'"
		 + " Order by UWDate";
		 */
	var strSql = "select distinct lwmission.MissionProp1,lwmission.MissionProp2,lccont.appntname,lwmission.MissionProp6,lccont.managecom,LWMission.MissionID ,LWMission.SubMissionID from lwmission,lccont,ljtempfee where "+k+"="+k
	         + " and LWMission.ProcessID = '0000000003' " 
	         + " and lwmission.MissionProp1=lccont.proposalcontno "
	         + " and lwmission.MissionProp2=ljtempfee.otherno "
	         + " and ljtempfee.othernotype='4' "
	         + " and ljtempfee.confflag='0' "
	         + " and (ljtempfee.EnterAccDate is not null) "
             + " and LWMission.ActivityID = '0000001150' " 
     //        + getWherePart('lwmission.MissionProp1','ContNo')
			 + getWherePart('lwmission.MissionProp2','PrtNo')
			 + getWherePart('lwmission.MissionProp3','AgentCode')
			 + getWherePart('lccont.managecom','ManageCom','like')
					 ;

	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	//showInfo.close();
    alert("未查询到满足条件的数据！");
     return false;
  }
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  turnPage.pageLineNum = 20 ;
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql 
  
  PolGrid.SortPage=turnPage;
  //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,MAXSCREENLINES);
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  //showCodeName();
}

function easyQueryClick2()
{
  initPolGrid();
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	// 书写SQL语句
  var strSql = "select ProposalNo,PrtNo,RiskCode,AppntName,InsuredName,UWDate from LCPol where 1=1 "
				 + " and VERIFYOPERATEPOPEDOM(Riskcode,Managecom,'"+comcode+"','Ph')=1 "
				 + "and AppFlag='0' "						// 投保状态				
				 + "and ApproveFlag='9' "					// 已经复核
				 + "and UWFlag in ('9','4') "			    // 核保通过
				 + "and GrpPolNo='00000000000000000000' "	// 个人投保单
				 + "and ContNo='00000000000000000000' "		// 个人投保单
				 + getWherePart( 'ProposalNo' )
				 + getWherePart( 'ManageCom','ManageCom', 'like' )
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'PrtNo' )
				 + "and PrtNo in (select PrtNo from lcpol where  1=1 " 			
				 + "and ApproveFlag='9' "					// 已经复核
				 + "and UWFlag in ('9','4') "			    // 核保通过
				 + "and GrpPolNo='00000000000000000000' "	// 个人投保单
				 + "and ContNo='00000000000000000000' "		// 个人投保单
				 + getWherePart( 'ManageCom','ManageCom', 'like' )
				 + getWherePart( 'AgentCode' )
				 + " and PolNo=MainPolNo )"	 
				 + " and PolNo=MainPolNo "  //主险
				 + " and ManageCom like '" + comcode + "%%'"
				 + " Order by UWDate";

	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	//showInfo.close();
    alert("未查询到满足条件的数据！");
     return false;
  }
  
  //设置查询起始位置
  //turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  turnPage.pageLineNum = 20 ;
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql 
  
  
  //arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex,MAXSCREENLINES);
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  showInfo.close();
  //showCodeName();
}

//发首交通知书
function SendFirstNotice()
{

  cOtherNoType="00"; //其他号码类型
  cCode="07";        //单据类型
  var checkedRowNum = 0 ;
  var rowNum = PolGrid. mulLineCount ; 
  var count = -1;
  
  for ( var i = 0 ; i< rowNum ; i++ )
  { 
  	if( checkedRowNum > 1)
      break;
  	if(PolGrid.getChkNo(i)) 
  	{
  	   checkedRowNum = checkedRowNum + 1;
       count = i;
    }
  }  
  if(checkedRowNum == 1)
  {
  	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  	var cProposalNo = PolGrid.getRowColData(count,1,PolGrid);//投保单号
  	showModalDialog("../uw/UWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  	showInfo.close();
  }
  else
  {
  	alert("请先选择一条保单发首期缴费通知书!");
  }
}
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }

}