<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：YBTProposalPrintSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//修改人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
String FlagStr = "";
String Content = "";
String tOperate = "";
String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
String sOutXmlPath = application.getRealPath("");	//xml文件输出路径
System.out.println("sOutXmlPath  ::" + sOutXmlPath);
//获得mutline中的数据信息
int nIndex = 0;
String tLCContGrids[] = request.getParameterValues("ContGridNo");
String tContNo[] = request.getParameterValues("ContGrid1");
String tPrintCount[] = request.getParameterValues("ContGrid6");
String tChecks[] = request.getParameterValues("InpContGridSel");

//获得session中的人员喜讯你
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//操作对象及容器
LCContSchema tLCContSchema = new LCContSchema();
LCContF1PUI tLCContF1PUI = new LCContF1PUI();
VData vData = new VData();

//循环打印选中的团单
for(nIndex = 0; nIndex < tChecks.length; nIndex++ )
{
	//If this line isn't selected, continue，如果没有选中当前行，则继续
	if( !tChecks[nIndex].equals("1") )
	{
		continue;
	}
	//将数据放入合同保单集合
	tLCContSchema = new LCContSchema();
	tLCContSchema.setContNo(tContNo[nIndex]);
	//判定打印模式
	if (tPrintCount[nIndex].compareTo("0") == 0)
	{
		tOperate = "PRINT";
	}
	else
	{
		tOperate = "REPRINT";
	}
	System.out.println("tOperate  ::" + tOperate);
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("SetPrtTimes", "1");
        
	//将数据集合放入容器中，准备传入后台处理
	vData = new VData();
	vData.add(tG);
	vData.addElement(tLCContSchema);
	vData.add(szTemplatePath);
	vData.add(sOutXmlPath);
    vData.add(tTransferData);
	//执行后台操作
	try
	{
		if (!tLCContF1PUI.submitData(vData, "PRINT")){
			Content += "个单"+tContNo[nIndex]+"打印失败，原因是:" + tLCContF1PUI.mErrors.getFirstError()+"<BR>";
			FlagStr = "Fail";
			tLCContF1PUI.mErrors.clearErrors();
		}
	}
	catch(Exception ex)
	{
		//一旦有团单打印失败，则跳出循环
		Content += "团单"+tContNo[nIndex]+"打印失败，原因是:" + ex.toString() +"<BR>";
		FlagStr = "Fail";
		break;
	}
}
// Prepare data for submiting

//如果没有失败，则返回打印成功
if (!FlagStr.equals("Fail"))
{
	Content = "个单打印成功! ";
	FlagStr = "Succ";
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>