var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
    fm.QYType.value = QYType;
    
    if(!verifyInput())
        return false;

    var tAppStartDate = fm.AppStartDate.value;
    var tAppEndDate = fm.AppEndDate.value;
    var tBackStartDate = fm.BackStartDate.value;
    var tBackEndDate = fm.BackEndDate.value;
    
    var tAppQFlag = false;
    var tBackQFlag = false;
    
    if(tAppStartDate == "" && tAppEndDate == "") 
    {
        tAppQFlag = true;
    }
    
    if(tBackStartDate == "" && tBackEndDate == "")
    {
        tBackQFlag = true;
    }
    
    if(!tAppQFlag && !tBackQFlag)
    {
        alert("投保时间起或退保时间起必填其一。");
        return false;
    }
    
    if(tAppQFlag)
    {
        if(dateDiff(tAppStartDate, tAppEndDate, "M") > 3)
        {
            alert("统计期最多为三个月！");
            return false;
        }
    }
    else if(tBackQFlag)
    {
        if(dateDiff(tBackStartDate, tBackEndDate, "M") > 3)
        {
            alert("统计期最多为三个月！");
            return false;
        }
    }
    
    var sql = "" 
        + " select "
        + " lbc.ManageCom, "
        + " CodeName('lcsalechnl', lbc.SaleChnl), "
        + " lbc.ContNo, lbc.PrtNo, "
        + " '退保', "
        + " lpea.ConfDate EdorConfDate, "
        + " (select labg.Name from LABranchGroup labg where labg.AgentGroup = lbc.AgentGroup) BranchName, "
        + " lbc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lbc.AgentCode) AgentName, "
        + " lbc.AppntName, lbc.AppntSex, lbc.AppntBirthday, "
        + " lbp.InsuredName, lbp.InsuredSex, lbp.InsuredBirthday, "
        + " lbp.RiskCode, "
        + " (select lmr.RiskName from LMRisk lmr where lmr.RiskCode = lbp.RiskCode) RiskName, "
        + " lbp.Mult, lbp.Amnt, lbp.Prem, lbp.PayIntv, lbp.CValidate, "
        + " lbp.UWCode, CodeName('uwflag', lbp.UWFlag), "
        + " lbp.PayMode, lbp.SignDate, lbc.GetPolDate "
        + " from LBCont lbc "
        + " inner join LBPol lbp on lbp.ContNo = lbc.ContNo "
        + " inner join LPEdorItem lpei on lpei.EdorNo = lbc.EdorNo "
        + " inner join LPEdorApp lpea on lpea.EdorAcceptNo = lpei.EdorNo "
        + " where 1 = 1 "
        + " and lbc.ContType = '1' "
        + " and lpei.EdorType in ('WT','CT','XT') "
        + " and lbc.EdorNo not like 'xb%' "
        + getWherePart("lbc.MakeDate", "AppStartDate", ">=") 
        + getWherePart("lbc.MakeDate", "AppEndDate", "<=") 
        + getWherePart("lpea.ConfDate", "BackStartDate", ">=") 
        + getWherePart("lpea.ConfDate", "BackEndDate", "<=") 
        + " order by lbc.ContNo, lbc.PrtNo, lbp.RiskCode "
        ;
    
    fm.querySql.value = sql;
    
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.target = '_blank';
    fm.submit();
    showInfo.close();
}

