<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput) session.getValue("GI");
%>
	<head>
		<script>
		    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
		    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
		    var prtno = "<%=request.getParameter("PrtNo")%>";
		    var contno = "<%=request.getParameter("ContNo")%>";
		    var conttype = "<%=request.getParameter("ContType")%>";
		    var action = "<%=request.getParameter("Action")%>";
		</script>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<script src="../common/javascript/CommonTools.js"></script>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<title>契约保单信息运维功能维护页面</title>
		<script src="QYMaintenance.js"></script>
		<%@include file="QYMaintenanceInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form method="post" name="fm" target="fraTitle">
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						保单信息：
					</td>
				</tr>
			</table>
			<table class=common border=0 width=100%>
				<tr class=common>
					<td class=title>管理机构</td>
					<td class=input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ><input class=codename name=ManageComName readonly=true ></td>
					<td class=title>印 刷 号</td>
					<td class=input><input class=common name=PrtNo readonly=true /></td>
					<td class=title>合 同 号</td>
					<td class=input><input class=common name=ContNo readonly=true /></td>
				</tr>
				<tr class=common>
					<td class=title>保单类型</td>
					<td class=input><Input class=codeno name=ContType readonly=true CodeData="0|^1|个单^2|团单" /><input class=codename name=ContTypeName readonly=true ></td>
					<td class=title>生效日期</td>
					<td class=input><input class=common name=CValiDate readonly=true /></td>
					<td class=title>签单日期</td>
					<td class=input><input class=common name=SignDate readonly=true /></td>
				</tr>
			</table>
			
			<table>
				<tr>
					<td class=common>
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
					</td>
					<td class= titleImg>修改前的数据</td>
				</tr>
			</table>
			<table class=common border=0 width=100%>
				<tr class=common>
					<td class=title>销售渠道</td>
					<td class=input><input class=codeNo name=SaleChnl /><input class=codename name=SaleChnlName readonly=true ></td>
					<td class=title>中介机构代码</td>
					<td class=input><input class=common name=AgentCom /></td>
					<td class=title>中介机构名称</td>
					<td class=input><input class=common name=AgentComName /></td>
				</tr>
				<tr class=common>
					<td class=title>业务员代码</td>
					<td class=input><input class=common name=AgentCode /></td>
					<td class=title>业务员名称</td>
					<td class=input><input class=common name=AgentName /></td>
					<td class=title>业务员组别</td>
					<td class=input><input class=common name=AgentGroup /></td>
				</tr>
			</table>
			
			<hr />
			<table>
				<tr>
					<td class=common>
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
					</td>
					<td class= titleImg>需要修改的数据</td>
				</tr>
			</table>
			<table class=common border=0 width=100%>
				<tr class=common id="SaleChnoRow" style="display: ''">
					<td class=title>销售渠道</td>
					<td class=input><input class=common name=mSaleChnl elementtype=nacessary /></td>
					<td></td><td></td><td></td><td></td>
				</tr>
				<tr class=common id="AgentRow" style="display: ''">
					<td class=title>中介机构代码</td>
					<td class=input><input class=common name=mAgentCom elementtype=nacessary /></td>
					<td class=title>业务员代码</td>
					<td class=input><input class=common name=mAgentCode elementtype=nacessary /></td>
					<td class=title>业务员组别</td>
					<td class=input><input class=common name=mAgentGroup elementtype=nacessary /></td>
				</tr>
			</table>
			<br />
			<INPUT VALUE="进行财务充负" class="cssButton" TYPE=button onclick="negativeRecoil();">
			<INPUT VALUE="进行财务充正" class="cssButton" TYPE=button onclick="positiveRecoil();">
			<INPUT VALUE="修改业务数据" class="cssButton" TYPE=button onclick="update();">
			<INPUT VALUE="直接修改业务数据" class="cssButton" TYPE=button onclick="onlyupdate();">
			<input type="hidden" class=Common name="Action" value="" />
			<input type="hidden" class=Common name="FinancialAction" value="" />
			<input type="hidden" class=Common name="OnlyDoUpdate" value="" />
		</form>
		
		<div>
			<font color="red">
				<ul>操作步骤：
					<li>1、将原始数据进行备份</li>
					<li>2、填写要修改的信息</li>
					<li>3、进行财务充负</li>
					<li>4、修改业务数据</li>
					<li>5、进行财务充正</li>
				</ul>
				<ul>注意事项：
					<li>在修改业务员时，如果保单只是需要修改业务员代码，原中介机构代码不变时，
					请将原来的中介机构代码填写到需要修改的数据的[中介机构代码]录入框中，不然原中介机构代码会被置空。</li>
					<li>修改业务员时，[业务员组别]是必须填写的</li>
				</ul>
			</font>
		</div>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
