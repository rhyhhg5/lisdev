
<%
	//程序名称：LGrpCalSave.jsp
	//程序功能：大团单算费
	//创建日期：2013-5-20
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.lgrp.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%

	GlobalInput tGI = new GlobalInput(); //repair:
	tGI = (GlobalInput) session.getValue("GI");
	
	String tGrpContNo = request.getParameter("GrpContNo");
	String tPrtNo = request.getParameter("PrtNo");
	
	System.out.println("--团单号：" + tGrpContNo);

	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("GrpContNo", tGrpContNo);

	VData tVData = new VData();
	tVData.add(tTransferData);
	tVData.add(tGI);
	
	String flagStr = "Succ";
	String content = "算费处理中....该页面可以关闭，处理进度请参看相关被保人情况，点击刷新按钮即可。";
		
	LGrpCheckMainBL tLGrpCheckMainBL = new LGrpCheckMainBL();
	if(!tLGrpCheckMainBL.submitData(tVData, tPrtNo)) {
		
		CErrors errors = tLGrpCheckMainBL.getError();
		flagStr = "Fail";
		content = errors.getLastError();
	}

	System.out.println("-----被保人校验完毕----");
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flagStr%>","<%=content%>");
</script>
</html>
