var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;
  var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  var tManageCom = fm.ManageCom.value;
  var tsalechnl = fm.saleChnl.value;
  var tagentcom = fm.agentCom.value;
  var type = fm.QYType.value;
  var tSQL = "";
  if(dateDiff(tStartDate,tEndDate,"M")>12)
  {
	alert("统计期最多为十二个月！");
	return false;
  }
  if(dateDiff(tStartDate,tEndDate,"M")<1){
      alert("统计止期比统计统计起期早");
      return false;
  }
  var length=tManageCom.split("").length;
  var tManageCom1="";
  var tManageCom2="";
  if(length==8){
      tManageCom1="length('"+tManageCom+"')";
	  tManageCom2="length('"+tManageCom+"')";
  }else{
      tManageCom1="length('"+tManageCom+"')*2";
	  tManageCom2="length('"+tManageCom+"')";
  }	
  
  if(tsalechnl==""||tsalechnl==null){
     tsalechnl="1";
  }
  
  if(tagentcom==""||tagentcom==null){
     tagentcom="1"; 
  }
    
  
  if(type=='1'){
     tSQL=" select A,B, "
         +" to_zero(C1)+to_zero(C2)+to_zero(C3),"
         +" to_zero(D1)+to_zero(D2),"
         +" to_zero(E1)+to_zero(E2),  "
         +" to_zero(F1)+to_zero(F2)+to_zero(F3),'' "
         +"   from"
         +" (SELECT  A.COMCODE A,A.showname B,  " 
         +" (select count(distinct (prtno)) from lcgrppol where grpcontno in(select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) C1 ,"
         +" (select count(distinct (prtno)) from lbgrppol where grpcontno in(select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) C2 ,"
         +" (select count(distinct (prtno)) from lbgrppol where grpcontno in(select grpcontno from lbgrpcont where  edorno not like 'xb%' and makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) C3 ,"
         +" "
         +" (select count( distinct appntno ) from lcgrpcont  where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and substr(managecom,1,"+tManageCom1+")=a.COMCODE and makedate  between '"+tStartDate+"' and '"+tEndDate+"') D1 ,"
         +" (select count( distinct appntno ) from lBgrpcont  where edorno not like 'xb%' and substr(managecom,1,"+tManageCom1+")=a.COMCODE and makedate  between '"+tStartDate+"' and '"+tEndDate+"') D2 ,"
         +" "
         +" (select sum(peoples2) from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=a.COMCODE) E1 ,"
         +" (select sum(peoples2) from lBgrpcont where edorno not like 'xb%' and  makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=a.COMCODE) E2 , "
         +"  "
         +" (select sum(prem) from lcPOL  where conttype='2' and grpcontno in(select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) F1,  "     
         +" (select sum(prem) from lbPOL  where conttype='2' and grpcontno in(select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) F2, "      
         +" (select sum(prem) from lbPOL  where conttype='2' and grpcontno in(select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) F3"
         +"    FROM  LDCOM A where length(trim(A.comcode))="+tManageCom1+" and A.sign='1' and substr(A.comcode,1,"+tManageCom2+") = '"+tManageCom+"')"
         +" as X "
         +" union "
         +" select A,B,   "
         +" to_zero(C1)+to_zero(C2)+to_zero(C3),"
         +" to_zero(D1)+to_zero(D2),      "
         +" to_zero(E1)+to_zero(E2),"
         +" to_zero(F1)+to_zero(F2)+to_zero(F3),''"
         +"   from"
         +" (SELECT  '总计' A,'总计' B,   "
         +" (select count(distinct (prtno)) from lcgrppol where  grpcontno in(select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) C1 ,"
         +" (select count(distinct (prtno)) from lbgrppol where  grpcontno in(select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) C2 ,"
         +" (select count(distinct (prtno)) from lbgrppol where  grpcontno in(select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) C3 ,  "
         +" "
         +" (select count( distinct appntno ) from lcgrpcont  where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  substr(managecom,1,"+tManageCom2+")=a.COMCODE and makedate  between '"+tStartDate+"' and '"+tEndDate+"') D1 ,"
         +" (select count( distinct appntno ) from lBgrpcont  where edorno not like 'xb%' and substr(managecom,1,"+tManageCom2+")=a.COMCODE and makedate  between '"+tStartDate+"' and '"+tEndDate+"') D2 ,"
         +" "
         +" (select sum(peoples2) from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=a.COMCODE) E1 ,"
         +" (select sum(peoples2) from lBgrpcont where  edorno not like 'xb%' and makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=a.COMCODE) E2 , "
         +"  "
         +" (select sum(prem) from lcPOL  where conttype='2' and grpcontno in(select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) F1, "      
         +" (select sum(prem) from lbPOL  where conttype='2' and grpcontno in(select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) F2,  "     
         +" (select sum(prem) from lbPOL  where conttype='2' and grpcontno in(select grpcontno from lbgrpcont where edorno not like 'xb%' and makedate  between '"+tStartDate+"' and '"+tEndDate+"' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) F3      "            
         +"   FROM  LDCOM A where length(trim(A.comcode))="+tManageCom2+" and A.sign='1' and substr(A.comcode,1,"+tManageCom2+") = '"+tManageCom+"')"
         +" as X"
         +" order by A "  ;             
    
  }else if(type=='2'){
     tSQL="select A,B,VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) from (SELECT  A.COMCODE A,A.showname B,    	(select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b where a.grpcontno=b.grpcontno and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom1+")=a.COMCODE and a.passflag='a' and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else b.salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else b.agentcom end) = '"+tagentcom+"' ) G, 	(select sum(b.prem) from LCGCUWMaster a,lcgrpcont b where a.grpcontno=b.grpcontno and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom1+")=a.COMCODE and a.passflag='a' and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else b.salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else b.agentcom end) = '"+tagentcom+"' ) H,       	(select count(1) from lcgrpcont where  UWdate between '"+tStartDate+"' and '"+tEndDate+"' and uwflag in('9') and appflag<>'1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else agentcom end) = '"+tagentcom+"') L, 	(select sum(peoples2) from lcgrpcont where  UWdate between '"+tStartDate+"' and '"+tEndDate+"' and uwflag in('9') and appflag<>'1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else agentcom end) = '"+tagentcom+"') M, 	(select sum(prem) from lcpol where conttype='2' and prtno in(select prtno from lcgrpcont where  UWdate between '"+tStartDate+"' and '"+tEndDate+"' and uwflag in('9') and appflag<>'1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE) and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else agentcom end) = '"+tagentcom+"') N FROM  LDCOM A where length(trim(A.comcode))="+tManageCom1+" and A.sign='1' and substr(A.comcode,1,"+tManageCom2+") = '"+tManageCom+"') as X union select A,B,VALUE(G,0),VALUE(H,0),VALUE(L,0),VALUE(M,0),VALUE(N,0) from (SELECT  '总计' A,'总计' B,    	(select count(a.grpcontno) from LCGCUWMaster a,lcgrpcont b where a.grpcontno=b.grpcontno and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom2+")=a.COMCODE and a.passflag='a' and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else b.salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else b.agentcom end) = '"+tagentcom+"' ) G, 	(select sum(b.prem) from LCGCUWMaster a,lcgrpcont b where a.grpcontno=b.grpcontno and a.makedate between '"+tStartDate+"' and '"+tEndDate+"' and substr(b.managecom,1,"+tManageCom2+")=a.COMCODE and a.passflag='a' and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else b.salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else b.agentcom end) = '"+tagentcom+"' ) H,       	(select count(1) from lcgrpcont where  UWdate between '"+tStartDate+"' and '"+tEndDate+"' and uwflag in('9') and appflag<>'1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else agentcom end) = '"+tagentcom+"') L, 	(select sum(peoples2) from lcgrpcont where  UWdate between '"+tStartDate+"' and '"+tEndDate+"' and uwflag in('9') and appflag<>'1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else agentcom end) = '"+tagentcom+"') M, 	(select sum(prem) from lcpol where conttype='2' and prtno in(select prtno from lcgrpcont where  UWdate between '"+tStartDate+"' and '"+tEndDate+"' and uwflag in('9') and appflag<>'1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE) and (case '"+tsalechnl+"' when  '1' then '"+tsalechnl+"' else salechnl end) = '"+tsalechnl+"' and (case '"+tagentcom+"' when '1' then '"+tagentcom+"' else agentcom end) = '"+tagentcom+"') N FROM  LDCOM A where length(trim(A.comcode))="+tManageCom2+" and A.sign='1' and substr(A.comcode,1,"+tManageCom2+") = '"+tManageCom+"') as X order by A "; 
  }else{
     tSQL=" select A,B, "
         +" to_zero(O1)+to_zero(O2)+to_zero(O3), "
         +" to_zero(P1)+to_zero(P2), "
         +" to_zero(Q1)+to_zero(Q2),  "          
         +" to_zero(R1)+to_zero(R2)+to_zero(R3), "
         +" to_zero(R11)+to_zero(R21)+to_zero(R31) "
         +" from "
         +" (SELECT  A.COMCODE A,A.showname B,    "
         +"   (select count(distinct prtno) from lcgrpPOL where grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) O1 ,"
         +"   (select count(distinct prtno) from lbgrpPOL where grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) O2 ,"
         +"   (select count(distinct prtno) from lbgrpPOL where grpcontno in (select grpcontno from lbgrpcont where  edorno not like 'xb%' and signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) O3 ,  "
         +"  "
         +" (select count( distinct appntno ) from lcgrpcont  where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE and signdate  between '"+tStartDate+"' and '"+tEndDate+"' ) P1 ,"
         +" (select count( distinct appntno ) from lbgrpcont  where edorno not like 'xb%' and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE and signdate  between '"+tStartDate+"' and '"+tEndDate+"' ) P2,   "          
         +" "
         +" (select sum(peoples2) from lcgrpcont  where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE  and signdate  between '"+tStartDate+"' and '"+tEndDate+"') Q1,"
         +" (select sum(peoples2) from lbgrpcont  where edorno not like 'xb%' and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE  and signdate  between '"+tStartDate+"' and '"+tEndDate+"') Q2,"
         +"   "           
         +" "
         +" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) R1,"
         +" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where  edorno not like 'xb%' and signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE )) R2, "
         +" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where  prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE )) R3, "
         +" "
         +" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE)) R11,"
         +" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE )) R21, "
         +" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom1+")=a.COMCODE )) R31 "
         +" "
         +"  FROM  LDCOM A where length(trim(A.comcode))="+tManageCom1+" and A.sign='1' and substr(A.comcode,1,"+tManageCom2+") = '"+tManageCom+"')"
         +" as X"
         +" union"
         +" select A,B,"
         +"   to_zero(O1)+to_zero(O2)+to_zero(O3),"
         +" to_zero(P1)+to_zero(P2),"
         +" to_zero(Q1)+to_zero(Q2), "           
         +" to_zero(R1)+to_zero(R2)+to_zero(R3),"
         +" to_zero(R11)+to_zero(R21)+to_zero(R31)"
         +" from"
         +" (SELECT  '总计' A,'总计' B,  " 
         +"   (select count(distinct prtno) from lcgrpPOL where grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) O1 ,"
         +"   (select count(distinct prtno) from lbgrpPOL where grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) O2 ,"
         +"   (select count(distinct prtno) from lbgrpPOL where grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) O3 , " 
         +" "
         +" "
         +" (select count( distinct appntno ) from lcgrpcont  where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE and signdate  between '"+tStartDate+"' and '"+tEndDate+"' ) P1 ,"
         +" (select count( distinct appntno ) from lbgrpcont  where edorno not like 'xb%' and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE and signdate  between '"+tStartDate+"' and '"+tEndDate+"' ) P2,   "          
         +" "
         +" (select sum(peoples2) from lcgrpcont  where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE  and signdate  between '"+tStartDate+"' and '"+tEndDate+"') Q1,"
         +" (select sum(peoples2) from lbgrpcont  where edorno not like 'xb%' and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE  and signdate  between '"+tStartDate+"' and '"+tEndDate+"') Q2,"
         +"    "          
         +" "
         +" (select sum(prem) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) R1,"
         +" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE )) R2, "
         +" (select sum(prem) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE )) R3, "
         +" "
         +" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lcpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE)) R11,"
         +" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lbgrpcont where edorno not like 'xb%' and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE )) R21, "
         +" (select sum(case when payintv=0 then prem  when payintv=-1 then prem else (prem*12/payintv) end ) from lbpol where appflag='1' and conttype='2' and grpcontno in (select grpcontno from lcgrpcont where prtno not in(select prtno from lbgrpcont where edorno  like 'xb%') and  signdate between '"+tStartDate+"' and '"+tEndDate+"'  and appflag='1' and substr(managecom,1,"+tManageCom2+")=a.COMCODE )) R31 "
         +" "
         +"  FROM  LDCOM A where length(trim(A.comcode))="+tManageCom2+" and A.sign='1' and substr(A.comcode,1,"+tManageCom2+") = '"+tManageCom+"')"
         +" as X"
         +" order by A ";
  
  }
  
  
  
//  var i = 0;
//  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//  //showSubmitFrame(mDebug); 
//	//fm.fmtransact.value = "PRINT";
//	fm.target = "f1print";
//	fm.all('op').value = '';
//	fm.submit();
//	showInfo.close();
     
    fm.querySql.value=tSQL;
	fm.action="GrpBaseStatisticSubDown.jsp";
	fm.submit();
  



}


//下拉框选择后执行
function afterCodeSelect(cName, Filed)
{	
  if(cName=='salechnlall')
  {
  	if(fm.saleChnl.value == '04')
  	{
  		fm.agentCom.value = '';
    	fm.all("tdAgentComName").style.display = '';
    	fm.all("tdAgentCom").style.display = '';
  	}
  	else
  	{
  		fm.agentCom.value = '';
    	fm.all("tdAgentComName").style.display = 'none';
    	fm.all("tdAgentCom").style.display = 'none';
  	}
	}
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    var tTmpUrl = "../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01";
    showInfo=window.open(tTmpUrl);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.agentCom.value = arrQueryResult[0][0];
    }
}