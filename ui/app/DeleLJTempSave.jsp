<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.tb.*"%>

<%
System.out.println("DeleLJTempSave.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

CErrors tError = null;
String tAction = "";
String tOperate = "";

GlobalInput tG = (GlobalInput)session.getValue("GI");

String tTempfeenoID = null;
String tContnoID = null;
String tPrtnoID = null;
String tFinancialAction = request.getParameter("FinancialAction");
String[] tTempfeenoIDAll = request.getParameterValues("MissionGrid1");
String[] tContnoIDAll = request.getParameterValues("MissionGrid2");
String[] tPrtnoDAll = request.getParameterValues("MissionGrid3");

String[] tRadio = request.getParameterValues("InpMissionGridSel");
for (int i = 0; i < tRadio.length; i++)
{
    if(tRadio[i]!=null&&tRadio[i]!="")
    {
    	tTempfeenoID = tTempfeenoIDAll[i];
    	tContnoID = tContnoIDAll[i];
    	tPrtnoID = tPrtnoDAll[i];
    }
}


TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("LJTempFeeno", tTempfeenoID);
tTransferData.setNameAndValue("Contno", tContnoID);
tTransferData.setNameAndValue("Prtno", tPrtnoID);
tTransferData.setNameAndValue("FinancialAction", tFinancialAction);

VData tVData = new VData();
tVData.add(tG);
tVData.add(tTransferData);

DeleLJTempUI tDeleLJTempUI = new DeleLJTempUI();

if (!tDeleLJTempUI.submitData(tVData, ""))
{
    Content = " 暂收表处理! 原因是: " + tDeleLJTempUI.mErrors.getLastError();
    FlagStr = "Fail";
}
else
{
    Content = " 暂收表处理成功！";
    FlagStr = "Succ";
}

System.out.println("DeleLJTempSave.jsp End ...");
%>

<html>
<script language="javascript">
    parent.fraInterface.afterMissionTraceDelSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
