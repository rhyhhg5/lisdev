<%
//程序名称：
//程序功能：
//创建日期：2011-02-10
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String tManageCom = tGI.ComCode;
String tOperator = tGI.Operator;
%>

<script language="JavaScript">
var tManageCom = "<%=tManageCom%>";
var tOperator = "<%=tOperator%>";

function initForm()
{
    try
    {
        initGrpCustomerListGrid();
        
        fm.all('ManageCom').value = <%=tManageCom%>;

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initGrpCustomerListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="客户号";
        iArray[1][1]="80px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="投保单位名称";
        iArray[2][1]="150px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="是否关联交易方";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="关联交易方名称";
        iArray[4][1]="150px";
        iArray[4][2]=150;
        iArray[4][3]=2;
        iArray[4][4]="traderela";
        iArray[4][5]="4";
        iArray[4][6]="1";
        iArray[4][9]="关联交易方名称|code:traderela";
        iArray[4][18]=250;
        


        GrpCustomerListGrid = new MulLineEnter("fm", "GrpCustomerListGrid"); 

        GrpCustomerListGrid.mulLineCount = 0;   
        GrpCustomerListGrid.displayTitle = 1;
        GrpCustomerListGrid.canSel = 0;
        GrpCustomerListGrid.hiddenSubtraction = 1;
        GrpCustomerListGrid.hiddenPlus = 1;
        GrpCustomerListGrid.canChk = 1;
        GrpCustomerListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化 GrpCustomerListGrid 时出错：" + ex);
    }
}
</script>

