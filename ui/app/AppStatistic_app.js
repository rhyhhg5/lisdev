var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
    fm.QYType.value = QYType;
    
    if(!verifyInput())
        return false;
    
    var sql = "" 
        + " select "
        + " lcc.ManageCom, CodeName('lcsalechnl', lcc.SaleChnl), "
        + " (select labg.BranchAttr from LABranchGroup labg where labg.AgentGroup = lcc.AgentGroup) BranchAttr, "
        + " (select labg.Name from LABranchGroup labg where labg.AgentGroup = lcc.AgentGroup) BranchName, "
        + " lcc.AgentCode, "
        + " (select laa.Name from LAAgent laa where laa.AgentCode = lcc.AgentCode) AgentName, "
        + " lcc.ContNo, lcc.PrtNo, "
        + " CodeName('stateflag', lcc.StateFlag) ContStateName, "
        + " lcc.AppntName, lcc.AppntSex, lcc.AppntBirthday, "
        + " lcp.InsuredName, lcp.InsuredSex, lcp.InsuredBirthday, lcp.InsuredAppAge, "
        + " lcp.RiskCode, lcp.PayMode, lcp.PayIntv, lcp.EndDate, "
        + " lcp.Mult, lcp.Amnt, lcp.Prem, lcp.PolApplyDate, "
        + " lcp.CValidate, "
        + " (select case AutoUWFlag when '1' then '自核通过' else '自核未通过' end from LCUWMaster lcuwm where lcuwm.PolNo = lcp.PolNo), "
        + " lcp.UWCode, CodeName('uwflag', lcp.UWFlag) UWFlag, lcp.UWDate, "
        + " lcp.PayMode, lcp.FirstPayDate, lcp.SignDate, lcc.GetPolDate "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcp.ContNo = lcc.ContNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + getWherePart("lcc.MakeDate", "AppStartDate", ">=") 
        + getWherePart("lcc.MakeDate", "AppEndDate", "<=") 
        + " order by lcc.ManageCom, lcc.ContNo, lcc.PrtNo, lcp.RiskCode "
        ;
    
    fm.querySql.value = sql;
    
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.target = '_blank';
    fm.submit();
    showInfo.close();
}

