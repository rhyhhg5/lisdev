//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询客户。
 */
function queryGrpCustomerInfo()
{
    if(!verifyInput2())
    {
        return false;
    }

    var tGrpCustomerNo = fm.GrpCustomerNo.value;

    var tStrSql = ""
        + " select ldg.CustomerNo, ldg.GrpName "
        + " from LDGrp ldg "
        + " where 1 = 1 "
        + " and ldg.CustomerNo = '" + tGrpCustomerNo + "' "
        + " with ur "
        ;
    
    turnPage.pageDivName = "divGrpCustomerGridPage";
    turnPage.queryModal(tStrSql, GrpCustomerGrid);
}

/**
 * 查询客户既往信息。
 */
function queryHistoryInfo()
{
    var tRow = GrpCustomerGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = GrpCustomerGrid.getRowData(tRow);
    
    var tGrpCustomerNo = tRowDatas[0];
    window.open("../uw/GrpUWHistoryCont.jsp?pmAppntNo=" + tGrpCustomerNo, "window2");
}

