var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

function queryError() {
	var tSql = "select grpcontno,batchno,contid,Name,Sex,Birthday,IDType,IDNo,ContPlanCode,ChkRemark "
	         + "from LCGrpSubInsuredImport "
	         + "where prtno = '"+PrtNo+"' and DataChkFlag = '02' ";
	initDiskErrQueryGrid();
	
	turnPage1.queryModal(tSql, DiskErrQueryGrid);
	fm.querySql.value = tSql;
}

function initContInfo() {
	var contQuerySql = "select (select name from ldcom where comcode=gc.managecom),gc.grpname from lcgrpcont gc where prtno='" + PrtNo + "'";
	var arrResult = easyExecSql(contQuerySql);
	if(arrResult) {
		fm.PrtNo.value = PrtNo;
		fm.ManageCom.value = arrResult[0][0];
		fm.GrpName.value = arrResult[0][1];
	} else {
		alert("初始化保单信息异常！");
		return false;
	}
}

function goBack(){
	top.close();
}

//下载
function downloadError() {
	if(fm.querySql.value != null && fm.querySql.value != "" && DiskErrQueryGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "LGrpQueryInsuredDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}