var showInfo;
var turnPage = new turnPageClass();

// 保存
function add() {
	if (!verifyInput2()) {
		return false;
	}
	
	if(!check()){
		return false;
	}

	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();

}


function afterSubmit(FlagStr, Content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

function check(){
	var startdate = fm.all("StartDate").value;
	var enddate = fm.all("EndDate").value;
	if(dateDiff(startdate,enddate,"D") < 0)
	{
		alert("补提时间至少为一天!");
		 return false;
	}
	return true;
}

