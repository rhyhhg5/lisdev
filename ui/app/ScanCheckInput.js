//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var approvefalg;
var arrResult;
var mDebug = "0";
var mOperate = 0;
var mAction = "";
var flag="";
var mSwitch = parent.VD.gVSwitch;
var mShowCustomerDetail = "GROUPPOL";
var turnPage = new turnPageClass();
this.window.onfocus=myonfocus;

/*********************************************************************
 *  保存集体投保单的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
// by gzh 20101118
	//选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
	//增加校验：保存投保人资料中证件信息校验   ranguo 20170821
  	var tAppntIDType=fm.AppntIDType.value;
  	var tAppntIDNo=fm.AppntIDNo.value;
  	var tAppntBirthday=fm.AppntBirthday.value;
  	var tAppntSex=fm.AppntSex.value;
  	var checkResult=checkIdNo(tAppntIDType,tAppntIDNo,tAppntBirthday,tAppntSex);
  	if(checkResult!=null && checkResult!=""){
  		alert(checkResult);
  		return false;
  	}
    if (verifyInput2()== false) 
    {        	
        return false;
    }
    
    if(!checkNative()){
	   return false;
	}
	
    //销售渠道为中介时中介机构的校验
    if(!checkAgentCom())
    {
        return false;
    }
    //销售渠道和业务员的校验
    if(!checkSaleChnl())
    {
        return false;
    }
    if(!ExtendCheck()){
    	return false;
    }
	if(fm.all('PolApplyDate').value==""){
		alert("请录入投保申请日期!");
		return false;
	}
	//#1893 校验
	if(!checkAllapp()){
	   return false;
	}
	
	if(!checkTaxFlag()){
	   return false;
	}
    if(!checkdiscountfactor()){
	    	return false;
	}
    
    //2017-02-10 赵庆涛
    //社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
      if(!checkCrs_SaleChnl()){
      	return false;
      }

    // 使个单生效日期为保单投保日期
    fm.all('CValiDate').value=fm.all('PolApplyDate').value
    // --------------------
    
	if(!chkDayAndMonth()) return false;
	/*如果是符合修改时使用，执行更新操作 20041125 wzw*/
	if(LoadFlag=="3"){
	  updateClick();
	  return;	
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        
	ImpartGrid.delBlankLine();  
	if(BirthdaySexAppntIDNo()==false){return false;}       
	if( mAction == "" )
	{
		//showSubmitFrame(mDebug);
		mAction = "INSERT";
		fm.all( 'fmAction' ).value = mAction;
		ImpartGrid.delBlankLine();
		if (fm.all('ProposalContNo').value != "") {
		  alert("保存结果只能进行修改操作！");
		  mAction = "";
		} else {
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		  
		  //保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
		  tAction = fm.action;
		  fm.action = fm.action + "?BQFlag=" + BQFlag;
		  ChangeDecodeStr();		  
		  fm.submit(); //提交
		  fm.action = tAction;
		}
	}
}
//逻辑校验
function LogicVerify(){
//	var PrtNo = fm.PrtNo.value;
//	var ContNo=fm.ContNo.value;
	var i = 0;                                                                                                                                                                                                                                                                                                                                              
 	var showStr="正在校验数据，请您稍候并且不要修改屏幕上的值或链接其他页面";                                                                                                                                                                                                                                                                                
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;                                                                                                                                                                                                                                                                                
 	 	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");                                                                                                                                                                                                  	
 	    fm.action= "./ContLogicVerify.jsp";   
      fm.submit(); //提交                 
			return;

	}
/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	UnChangeDecodeStr();
	showInfo.close();
	window.focus();
	mAction = ""; 
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		showDiv(operateButton, "true"); 
		showDiv(inputButton, "false");
		if(approvefalg=="2")
		{
			top.close();
		}
		if (flag ==1 )
		{	
		top.window.close();
		}
	}
	//if(mWFlag == 1 && FlagStr != "Fail") {
    //window.location.href("./ContInput.jsp");
  //}
	//initForm();	
}

/*********************************************************************
 *  新单复核后的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit1( FlagStr, content )
{
	UnChangeDecodeStr();
	showInfo.close();
	window.focus();
	if( FlagStr == "Fail" )
	{             
		approvefalg = "";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		content = "新单复核成功！";
		cMissionID = fm.MissionID.value;
		strSQL = "select 1 from lwmission where 1=1 "
				 + " and missionid = '"+ cMissionID +"'"
				 + " and activityid = '0000001100' "
				 + " and processid = '0000000003'"
				 ;	 

	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  	//判断是否查询成功
  	if (turnPage.strQueryResult) {
			content = content + "但需提交人工核保！";
  	}
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		showDiv(operateButton, "true"); 
		showDiv(inputButton, "false");
		if(approvefalg=="2")
		{
            top.opener.queryCont();
			top.close();
		}	
	}
	mAction = ""; 
}
/********************************************************************

*/
function payModeHelp(){
	window.open("./PayMode.jsp");
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
	try
	{
		initForm();
		fm.all('PrtNo').value = prtNo;
	}
	catch( re )
	{
		alert("在GroupPolInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
} 

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
	showDiv(operateButton,"true"); 
	showDiv(inputButton,"false"); 
}
 
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	showDiv( operateButton, "false" ); 
	showDiv( inputButton, "true" ); 
/*暂时屏蔽，保全部分再作处理	fm.all('RiskCode').value = "";
	
//保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
  if (BQFlag=="2") {
    var strSql = "select grppolno, grpno from lcgrppol where prtno='" + prtNo + "' and riskcode in (select riskcode from lmriskapp where subriskflag='M')";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    mOperate = 1;
    afterQuery(arrResult);
    //strSql = "select GrpNo,GrpName,GrpAddress,Satrap from LDGrp where GrpNo='" + arrResult[0][1] + "'";
    //arrResult = easyExecSql(strSql);
    //mOperate = 2;
    //afterQuery(arrResult);
    
    fm.all('RiskCode').value = BQRiskCode;
    fm.all('RiskCode').className = "readonly";
    fm.all('RiskCode').readOnly = true;
    fm.all('RiskCode').ondblclick = "";
  }
	fm.all('ContNo').value = "";
	fm.all('GrpProposalNo').value = "";*/
}           

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick()
{
        if(this.ScanFlag == "1"){
	  alert( "有扫描件录入不允许查询!" );
          return false;	  
        }	
	if( mOperate == 0 )
	{
		mOperate = 1;
		cContNo = fm.all( 'ContNo' ).value;
		showInfo = window.open("./ContQueryMain.jsp?ContNo=" + cContNo);
	}
} 

/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
	
	if(fm.all('PolApplyDate').value==""){
		alert("请录入投保申请日期!");
		return false;
	}
	//增加校验：修改投保人资料中证件信息校验   ranguo 20170821
  	var tAppntIDType=fm.AppntIDType.value;
  	var tAppntIDNo=fm.AppntIDNo.value;
  	var tAppntBirthday=fm.AppntBirthday.value;
  	var tAppntSex=fm.AppntSex.value;
  	var checkResult=checkIdNo(tAppntIDType,tAppntIDNo,tAppntBirthday,tAppntSex);
  	if(checkResult!=null && checkResult!=""){
  		alert(checkResult);
  		return false;
  	}
    // 使个单生效日期为保单投保日期
    fm.all('CValiDate').value=fm.all('PolApplyDate').value
    // --------------------
    
	if(!chkDayAndMonth()) return false;
	var tGrpProposalNo = "";
	tGrpProposalNo = fm.all( 'ProposalContNo' ).value;
	if(BirthdaySexAppntIDNo()==false){return false;}
	if( tGrpProposalNo == null || tGrpProposalNo == "" )
		alert( "请先做投保单保存操作，再进行修改!" );
	else
	{
   if (verifyInput2()== false) {        	
	  return false;
                }
// by gzh 20101118
	//选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
	
	if(!checkNative()){
	   return false;
	}
	
	if(!checkPayintv())
	{
	  return false;
	}
	
	//销售渠道为中介时中介机构的校验
    if(!checkAgentCom())
    {
        return false;
    }
	
	//销售渠道和业务员的校验
    if(!checkSaleChnl())
    {
        return false;
    }
	
	if(!ExtendCheck()){
		return false;
	}

    // 校验集团交叉业务要素
    //if(!checkCrsBussParams())
    //{
        //return false;
    //} 
    //#1893 校验
	if(!checkAllapp()){
	   return false;
	}
	
	if(!checkTaxFlag()){
	   return false;
	}
	//税优折扣校验 20161107  lxs
    if(!checkdiscountfactor()){
    	return false;
    }
    
    //2017-02-08 赵庆涛
    //社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
      if(!checkCrs_SaleChnl()){
      	return false;
      }

	  ImpartGrid.delBlankLine();    		
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "UPDATE";
			fm.all( 'fmAction' ).value = mAction;
			UnChangeDecodeStr();
			fm.submit(); //提交
		}
	}
}           

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick()
{
	var tGrpProposalNo = "";
	var tProposalContNo = "";
	if(LoadFlag==1)
	{
		tGrpProposalNo = fm.all( 'GrpContNo' ).value;
	if( tGrpProposalNo == null || tGrpProposalNo == "" )
		alert( "请先做投保单保存操作，再进行删除!" );
	else
	{
		 if (!confirm("确认要删除该保单吗？"))
	{
		return;
	}
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		
		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	 }
  }
}           

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

/*********************************************************************
 *  当点击“进入个人信息”按钮时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoInsured()
{
    //销售渠道为中介时中介机构的校验
    if(!checkAgentCom())
    {
        return false;
    }
    //销售渠道和业务员的校验
    if(!checkSaleChnl())
    {
        return false;
    }
    //银行信息的校验
    if(!checkBankInfo())
    {
        return false;
    }
    //添加必录项校验，查询和随动定制不需要校验必录项
    if(LoadFlag != "6" && LoadFlag != "99" && !verifyInput2())
    {
        return false;
    }
	//下面增加相应的代码
	var tAppntNo = fm.AppntNo.value;
	var tAppntName = fm.AppntName.value;
	var cVideoFlag = fm.VideoFlag.value;
	var cXSFlag = fm.XSFlag.value;
	//fm.ContNo.value=fm.ProposalContNo.value;
	if( fm.ContNo.value == "" )
	{
		alert("您必须先录入合同信息才能进入被保险人信息部分。");
		return false
	}
	//把集体信息放入内存
	mSwitch = parent.VD.gVSwitch;  //桢容错
	putCont();
	if(DiskInputFlag!="null")
	{
		if(LoadFlag==1&&DiskInputFlag!=1)
		{
			var strSql = "select 1 from lwmission where missionid='"+tMissionID+"' and submissionid='1' and activityid='0000001098'  and processid = '0000000003'";
			var arr = easyExecSql(strSql);
			if(!arr)
			{
				alert("合同信息已保存！");
				return;
			}
		}
	}
	try { goToPic(2) } catch(e) {}
	
	try {
	  parent.fraInterface.window.location = "./ContInsuredInput.jsp?LoadFlag=" + LoadFlag + "&prtNo="+prtNo + "&type=" + type+ "&MissionID=" + tMissionID+ "&SubMissionID=" + tSubMissionID+ "&AppntNo=" + tAppntNo+ "&AppntName=" + tAppntName+"&checktype=1"+"&scantype="+scantype+"&ScanFlag="+ScanFlag+"&SaleChnl="+fm.SaleChnl.value+"&SaleChnlDetail="+fm.SaleChnlDetail.value+"&ContNo="+ContNo+"&VideoFlag="+cVideoFlag+"&XSFlag="+cXSFlag ;
	}
	catch (e) {
		parent.fraInterface.window.location = "./ContInsuredInput.jsp?LoadFlag=1&prtNo="+prtNo + "&type=" + type+ "&MissionID=" +tMissionID+ "&SubMissionID=" + tSubMissionID+ "&AppntNo=" + tAppntNo+ "&AppntName=" + tAppntName +"&ContNo="+ContNo;
	}
}           
function intoInsured1()
{
    //销售渠道为中介时中介机构的校验
    if(!checkAgentCom())
    {
        return false;
    }
    //销售渠道和业务员的校验
    if(!checkSaleChnl())
    {
        return false;
    }
    //银行信息的校验
    if(!checkBankInfo())
    {
        return false;
    }
    //添加必录项校验，查询和随动定制不需要校验必录项
    if(LoadFlag != "6" && LoadFlag != "99" && !verifyInput2())
    {
        return false;
    }
	//下面增加相应的代码
	var tAppntNo = fm.AppntNo.value;
	var tAppntName = fm.AppntName.value;
	var cVideoFlag = fm.VideoFlag.value;
	var cXSFlag = fm.XSFlag.value;
	//fm.ContNo.value=fm.ProposalContNo.value;
	if( fm.ContNo.value == "" )
	{
		alert("您必须先录入合同信息才能进入被保险人信息部分。");
		return false
	}
	var strSql = "select 1 from lcpol where contno='" + fm.ContNo.value +"' and riskcode in ('123201','220601') with ur";
	  var arrResult = easyExecSql(strSql);
	  if(!arrResult){
			alert("非医保卡产品不能通过此按钮查看被保人信息!");
			return false
	  }
	//把集体信息放入内存
	mSwitch = parent.VD.gVSwitch;  //桢容错
	putCont();
	if(DiskInputFlag!="null")
	{
		if(LoadFlag==1&&DiskInputFlag!=1)
		{
			var strSql = "select 1 from lwmission where missionid='"+tMissionID+"' and submissionid='1' and activityid='0000001098'  and processid = '0000000003'";
			var arr = easyExecSql(strSql);
			if(!arr)
			{
				alert("合同信息已保存！");
				return;
			}
		}
	}
	try { goToPic(2) } catch(e) {}
	
	try {
	  parent.fraInterface.window.location = "./ybkgz.jsp?LoadFlag=" + LoadFlag + "&prtNo="+prtNo + "&type=" + type+ "&MissionID=" + tMissionID+ "&SubMissionID=" + tSubMissionID+ "&AppntNo=" + tAppntNo+ "&AppntName=" + tAppntName+"&checktype=1"+"&scantype="+scantype+"&ScanFlag="+ScanFlag+"&SaleChnl="+fm.SaleChnl.value+"&SaleChnlDetail="+fm.SaleChnlDetail.value+"&ContNo="+ContNo+"&VideoFlag="+cVideoFlag+"&XSFlag="+cXSFlag ;
	}
	catch (e) {
		parent.fraInterface.window.location = "./ybkgz.jsp?LoadFlag=1&prtNo="+prtNo + "&type=" + type+ "&MissionID=" +tMissionID+ "&SubMissionID=" + tSubMissionID+ "&AppntNo=" + tAppntNo+ "&AppntName=" + tAppntName +"&ContNo="+ContNo;
	}
}     
/*********************************************************************
 *  把合同所有信息录入结束确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function inputConfirm(wFlag)
{
	var checkSQL = "select count(*) from lwmission where missionprop2 = '"+fm.ContNo.value+
			"' and activityid = '0000001025' with ur";
	var result = easyExecSql(checkSQL);
	if(result>0){
		alert("该保单下还有未回销的问题件，请核实！");
		return;
	}else{
		alert("复查成功！");
	}
/*	var exeSQL1 = new ExeSQL();
	alert("1");
	var unlock = "delete from ldcode1 where codetype = 'PadScanLock' and code = '" +
			fm.PrtNo.value + "'";
	alert("2");
	var isDelete = exeSQL1.execUpdateSQL(unlock);
	alert("3");
	if(!isDelete){
		alert("出现未知错误！");
	}*/
	fm.action = 'ScanCheckInputSave.jsp';
    fm.submit();
} 

/*********************************************************************
 *  把合同信息放入内存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function putCont()
{
	delContVar();
	addIntoCont();
}

/*********************************************************************
 *  把合同信息放入加到变量中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addIntoCont()
{
	//try { mSwitch.addVar( "intoPolFlag", "", "GROUPPOL" ); } catch(ex) { };
	// body信息
	try { mSwitch.addVar( "BODY", "", window.document.body.innerHTML ); } catch(ex) { };
	// 集体信息
	//由"./AutoCreatLDGrpInit.jsp"自动生成
    try{mSwitch.addVar('ContNo','',fm.ContNo.value);}catch(ex){};
    try{mSwitch.addVar('ProposalContNo','',fm.ProposalContNo.value);}catch(ex){};
    try{mSwitch.addVar('PrtNo','',fm.PrtNo.value);}catch(ex){};
    try{mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);}catch(ex){};
    try{mSwitch.addVar('ContType','',fm.ContType.value);}catch(ex){};
    try{mSwitch.addVar('FamilyType','',fm.FamilyType.value);}catch(ex){};
    try{mSwitch.addVar('PolType','',fm.PolType.value);}catch(ex){};
    try{mSwitch.addVar('CardFlag','',fm.CardFlag.value);}catch(ex){};
    try{mSwitch.addVar('ManageCom','',fm.ManageCom.value);}catch(ex){};
    try{mSwitch.addVar('AgentCom','',fm.AgentCom.value);}catch(ex){};
    try{mSwitch.addVar('AgentCode','',fm.AgentCode.value);}catch(ex){};
    try{mSwitch.addVar('AgentGroup','',fm.AgentGroup.value);}catch(ex){};
    try{mSwitch.addVar('AgentCode1','',fm.AgentCode1.value);}catch(ex){};
    try{mSwitch.addVar('AgentType','',fm.AgentType.value);}catch(ex){};
    try{mSwitch.addVar('SaleChnl','',fm.SaleChnl.value);}catch(ex){};
    try{mSwitch.addVar('Handler','',fm.Handler.value);}catch(ex){};
    try{mSwitch.addVar('Password','',fm.Password.value);}catch(ex){};
    try{mSwitch.addVar('AppntNo','',fm.AppntNo.value);}catch(ex){};
    try{mSwitch.addVar('AppntName','',fm.AppntName.value);}catch(ex){};
    try{mSwitch.addVar('AppntSex','',fm.AppntSex.value);}catch(ex){};
    try{mSwitch.addVar('AppntBirthday','',fm.AppntBirthday.value);}catch(ex){};
    try{mSwitch.addVar('AppntIDType','',fm.AppntIDType.value);}catch(ex){};
    try{mSwitch.addVar('AppntIDNo','',fm.AppntIDNo.value);}catch(ex){};
    try{mSwitch.addVar('InsuredNo','',fm.InsuredNo.value);}catch(ex){};
    try{mSwitch.addVar('AppIDStartDate','',fm.AppIDStartDate.value);}catch(ex){};
    try{mSwitch.addVar('AppIDEndDate','',fm.AppIDEndDate.value);}catch(ex){};
    try{mSwitch.addVar('InsuredName','',fm.InsuredName.value);}catch(ex){};
    try{mSwitch.addVar('InsuredSex','',fm.InsuredSex.value);}catch(ex){};
    try{mSwitch.addVar('InsuredBirthday','',fm.InsuredBirthday.value);}catch(ex){};
    try{mSwitch.addVar('InsuredIDType','',fm.InsuredIDType.value);}catch(ex){};
    try{mSwitch.addVar('InsuredIDNo','',fm.InsuredIDNo.value);}catch(ex){};
    try{mSwitch.addVar('PayIntv','',fm.PayIntv.value);}catch(ex){};
    try{mSwitch.addVar('PayMode','',fm.PayMode.value);}catch(ex){};
    try{mSwitch.addVar('PayLocation','',fm.PayLocation.value);}catch(ex){};
    try{mSwitch.addVar('DisputedFlag','',fm.DisputedFlag.value);}catch(ex){};
    try{mSwitch.addVar('OutPayFlag','',fm.OutPayFlag.value);}catch(ex){};
    try{mSwitch.addVar('GetPolMode','',fm.GetPolMode.value);}catch(ex){};
    try{mSwitch.addVar('SignCom','',fm.SignCom.value);}catch(ex){};
    try{mSwitch.addVar('SignDate','',fm.SignDate.value);}catch(ex){};
    try{mSwitch.addVar('SignTime','',fm.SignTime.value);}catch(ex){};
    try{mSwitch.addVar('ConsignNo','',fm.ConsignNo.value);}catch(ex){};
    try{mSwitch.addVar('BankCode','',fm.BankCode.value);}catch(ex){};
    try{mSwitch.addVar('BankAccNo','',fm.BankAccNo.value);}catch(ex){};
    try{mSwitch.addVar('AccName','',fm.AccName.value);}catch(ex){};
    try{mSwitch.addVar('PrintCount','',fm.PrintCount.value);}catch(ex){};
    try{mSwitch.addVar('LostTimes','',fm.LostTimes.value);}catch(ex){};
    try{mSwitch.addVar('Lang','',fm.Lang.value);}catch(ex){};
    try{mSwitch.addVar('Currency','',fm.Currency.value);}catch(ex){};
    try{mSwitch.addVar('Remark','',fm.Remark.value);}catch(ex){};
    try{mSwitch.addVar('Peoples','',fm.Peoples.value);}catch(ex){};
    try{mSwitch.addVar('Mult','',fm.Mult.value);}catch(ex){};
    try{mSwitch.addVar('Prem','',fm.Prem.value);}catch(ex){};
    try{mSwitch.addVar('Amnt','',fm.Amnt.value);}catch(ex){};
    try{mSwitch.addVar('SumPrem','',fm.SumPrem.value);}catch(ex){};
    try{mSwitch.addVar('Dif','',fm.Dif.value);}catch(ex){};
    try{mSwitch.addVar('PaytoDate','',fm.PaytoDate.value);}catch(ex){};
    try{mSwitch.addVar('FirstPayDate','',fm.FirstPayDate.value);}catch(ex){};
    try{mSwitch.addVar('CValiDate','',fm.CValiDate.value);}catch(ex){};
    try{mSwitch.addVar('InputOperator','',fm.InputOperator.value);}catch(ex){};
    try{mSwitch.addVar('InputDate','',fm.InputDate.value);}catch(ex){};
    try{mSwitch.addVar('InputTime','',fm.InputTime.value);}catch(ex){};
    try{mSwitch.addVar('ApproveFlag','',fm.ApproveFlag.value);}catch(ex){};
    try{mSwitch.addVar('ApproveCode','',fm.ApproveCode.value);}catch(ex){};
    try{mSwitch.addVar('ApproveDate','',fm.ApproveDate.value);}catch(ex){};
    try{mSwitch.addVar('ApproveTime','',fm.ApproveTime.value);}catch(ex){};
    try{mSwitch.addVar('UWFlag','',fm.UWFlag.value);}catch(ex){};
    try{mSwitch.addVar('UWOperator','',fm.UWOperator.value);}catch(ex){};
    try{mSwitch.addVar('UWDate','',fm.UWDate.value);}catch(ex){};
    try{mSwitch.addVar('UWTime','',fm.UWTime.value);}catch(ex){};
    try{mSwitch.addVar('AppFlag','',fm.AppFlag.value);}catch(ex){};
    try{mSwitch.addVar('PolApplyDate','',fm.PolApplyDate.value);}catch(ex){};
    try{mSwitch.addVar('GetPolDate','',fm.GetPolDate.value);}catch(ex){};
    try{mSwitch.addVar('GetPolTime','',fm.GetPolTime.value);}catch(ex){};
    try{mSwitch.addVar('CustomGetPolDate','',fm.CustomGetPolDate.value);}catch(ex){};
    try{mSwitch.addVar('State','',fm.State.value);}catch(ex){};
    try{mSwitch.addVar('Operator','',fm.Operator.value);}catch(ex){};
    try{mSwitch.addVar('MakeDate','',fm.MakeDate.value);}catch(ex){};
    try{mSwitch.addVar('MakeTime','',fm.MakeTime.value);}catch(ex){};
    try{mSwitch.addVar('ModifyDate','',fm.ModifyDate.value);}catch(ex){};
    try{mSwitch.addVar('ModifyTime','',fm.ModifyTime.value);}catch(ex){};
   
   //新的数据处理
try { mSwitch.addVar('AppntNo','',fm.AppntNo.value); } catch(ex) { };				
try { mSwitch.addVar('AppntName','',fm.AppntName.value); } catch(ex) { };			
try { mSwitch.addVar('AppntSex','',fm.AppntSex.value); } catch(ex) { };				
try { mSwitch.addVar('AppntBirthday','',fm.AppntBirthday.value); } catch(ex) { };		
try { mSwitch.addVar('AppntIDType','',fm.AppntIDType.value); } catch(ex) { };			
try { mSwitch.addVar('AppntIDNo','',fm.AppntIDNo.value); } catch(ex) { };
try { mSwitch.addVar('AppntPassword','',fm.AppntPassword.value); } catch(ex) { };		
try { mSwitch.addVar('AppIDStartDate','',fm.AppIDEndDate.value); } catch(ex) { };	
try { mSwitch.addVar('AppIDEndDate','',fm.AppIDEndDate.value); } catch(ex) { };		
try { mSwitch.addVar('AppntNativePlace','',fm.AppntNativePlace.value); } catch(ex) { };		
try { mSwitch.addVar('AppntNationality','',fm.AppntNationality.value); } catch(ex) { };
try { mSwitch.addVar('AddressNo','',fm.AddressNo.value); } catch(ex) { };		
try { mSwitch.addVar('AppntRgtAddress','',fm.AppntRgtAddress.value); } catch(ex) { };		
try { mSwitch.addVar('AppntMarriage','',fm.AppntMarriage.value); } catch(ex) { };		
try { mSwitch.addVar('AppntMarriageDate','',fm.AppntMarriageDate.value); } catch(ex) { };	
try { mSwitch.addVar('AppntHealth','',fm.AppntHealth.value); } catch(ex) { };			
try { mSwitch.addVar('AppntStature','',fm.AppntStature.value); } catch(ex) { };			
try { mSwitch.addVar('AppntAvoirdupois','',fm.AppntAvoirdupois.value); } catch(ex) { };		
try { mSwitch.addVar('AppntDegree','',fm.AppntDegree.value); } catch(ex) { };			
try { mSwitch.addVar('AppntCreditGrade','',fm.AppntCreditGrade.value); } catch(ex) { };		
try { mSwitch.addVar('AppntOthIDType','',fm.AppntOthIDType.value); } catch(ex) { };		
try { mSwitch.addVar('AppntOthIDNo','',fm.AppntOthIDNo.value); } catch(ex) { };			
try { mSwitch.addVar('AppntICNo','',fm.AppntICNo.value); } catch(ex) { };			
try { mSwitch.addVar('AppntGrpNo','',fm.AppntGrpNo.value); } catch(ex) { };			
try { mSwitch.addVar('AppntJoinCompanyDate','',fm.AppntJoinCompanyDate.value); } catch(ex) { };	
try { mSwitch.addVar('AppntStartWorkDate','',fm.AppntStartWorkDate.value); } catch(ex) { };	
try { mSwitch.addVar('AppntPosition','',fm.AppntPosition.value); } catch(ex) { };		
try { mSwitch.addVar('AppntSalary','',fm.AppntSalary.value); } catch(ex) { };			
try { mSwitch.addVar('AppntOccupationType','',fm.AppntOccupationType.value); } catch(ex) { };	
try { mSwitch.addVar('AppntOccupationCode','',fm.AppntOccupationCode.value); } catch(ex) { };  	
try { mSwitch.addVar('AppntWorkType','',fm.AppntWorkType.value); } catch(ex) { };		
try { mSwitch.addVar('AppntPluralityType','',fm.AppntPluralityType.value); } catch(ex) { };	
try { mSwitch.addVar('AppntDeathDate','',fm.AppntDeathDate.value); } catch(ex) { };		
try { mSwitch.addVar('AppntSmokeFlag','',fm.AppntSmokeFlag.value); } catch(ex) { };		
try { mSwitch.addVar('AppntBlacklistFlag','',fm.AppntBlacklistFlag.value); } catch(ex) { };	
try { mSwitch.addVar('AppntProterty','',fm.AppntProterty.value); } catch(ex) { };		
try { mSwitch.addVar('AppntRemark','',fm.AppntRemark.value); } catch(ex) { };			
try { mSwitch.addVar('AppntState','',fm.AppntState.value); } catch(ex) { };			
try { mSwitch.addVar('AppntOperator','',fm.AppntOperator.value); } catch(ex) { };		
try { mSwitch.addVar('AppntMakeDate','',fm.AppntMakeDate.value); } catch(ex) { };		
try { mSwitch.addVar('AppntMakeTime','',fm.AppntMakeTime.value); } catch(ex) { };		
try { mSwitch.addVar('AppntModifyDate','',fm.AppntModifyDate.value); } catch(ex) { };		
try { mSwitch.addVar('AppntModifyTime','',fm.AppntModifyTime.value); } catch(ex) { };
try { mSwitch.addVar('AppntHomeAddress','',fm.AppntHomeAddress.value); } catch(ex) { };
try { mSwitch.addVar('AppntHomeZipCode','',fm.AppntHomeZipCode.value); } catch(ex) { };
try { mSwitch.addVar('AppntHomePhone','',fm.AppntHomePhone.value); } catch(ex) { };
try { mSwitch.addVar('AppntHomeFax','',fm.AppntHomeFax.value); } catch(ex) { };		
try { mSwitch.addVar('AppntGrpName','',fm.AppntGrpName.value); } catch(ex) { };
try { mSwitch.addVar('AppntGrpPhone','',fm.AppntGrpPhone.value); } catch(ex) { };
try { mSwitch.addVar('CompanyAddress','',fm.CompanyAddress.value); } catch(ex) { };
try { mSwitch.addVar('AppntGrpZipCode','',fm.AppntGrpZipCode.value); } catch(ex) { };
try { mSwitch.addVar('AppntGrpFax','',fm.AppntGrpFax.value); } catch(ex) { };
try { mSwitch.addVar('AppntPostalAddress','',fm.AppntPostalAddress.value); } catch(ex) { };	
try { mSwitch.addVar('AppntZipCode','',fm.AppntZipCode.value); } catch(ex) { };
try { mSwitch.addVar('AppntPhone','',fm.AppntPhone.value); } catch(ex) { };
try { mSwitch.addVar('AppntMobile','',fm.AppntMobile.value); } catch(ex) { };
try { mSwitch.addVar('AppntFax','',fm.AppntFax.value); } catch(ex) { };
try { mSwitch.addVar('AppntEMail','',fm.AppntEMail.value); } catch(ex) { };
try { mSwitch.addVar('AppntBankAccNo','',fm.all('AppntBankAccNo').value); } catch(ex) { };    
try { mSwitch.addVar('AppntBankCode','',fm.all('AppntBankCode').value); } catch(ex) { };    
try { mSwitch.addVar('AppntAccName','',fm.all('AppntAccName').value); } catch(ex) { };   
try { mSwitch.addVar('ActivityID' ,'',ActivityID); } catch(ex) {};
try { mSwitch.addVar('MissionID' ,'',tMissionID); } catch(ex) {} ;
try { mSwitch.addVar('SubMissionID' ,'',tSubMissionID); } catch(ex) {} ;
try { mSwitch.addVar('LoadFlag' ,'',LoadFlag); } catch(ex) {} ;
try { mSwitch.addVar('appnt_PostalCity' ,'',fm.appnt_PostalCity.value); } catch(ex) {} ;
try { mSwitch.addVar('City' ,'',fm.City.value); } catch(ex) {} ;
try { mSwitch.addVar('appnt_PostalCommunity' ,'',fm.appnt_PostalCommunity.value); } catch(ex) {} ;
try { mSwitch.addVar('appnt_PostalCounty' ,'',fm.appnt_PostalCounty.value); } catch(ex) {} ;
try { mSwitch.addVar('County' ,'',fm.County.value); } catch(ex) {} ;
try { mSwitch.addVar('appnt_PostalProvince' ,'',fm.appnt_PostalProvince.value); } catch(ex) {} ;
try { mSwitch.addVar('Province' ,'',fm.Province.value); } catch(ex) {} ;
try { mSwitch.addVar('appnt_PostalStreet' ,'',fm.appnt_PostalStreet.value); } catch(ex) {} ;
try { mSwitch.addVar('AppntHomeCode' ,'',fm.AppntHomeCode.value); } catch(ex) {} ;
try { mSwitch.addVar('AppntHomeNumber' ,'',fm.AppntHomeNumber.value); } catch(ex) {} ;
try { mSwitch.addVar('AppntNativeCity' ,'',fm.AppntNativeCity.value); } catch(ex) {} ;
}  
   
/*********************************************************************
 *  把集体信息从变量中删除
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delContVar()
{  
	try { mSwitch.deleteVar( "intoPolFlag" ); } catch(ex) { };
	// body信息
	try { mSwitch.deleteVar( "BODY" ); } catch(ex) { };
	// 集体信息
	//由"./AutoCreatLDGrpInit.jsp"自动生成
   try { mSwitch.deleteVar('ContNo'); } catch(ex) { };
   try { mSwitch.deleteVar('ProposalContNo'); } catch(ex) { };
   try { mSwitch.deleteVar('PrtNo'); } catch(ex) { };
   try { mSwitch.deleteVar('GrpContNo'); } catch(ex) { };
   try { mSwitch.deleteVar('ContType'); } catch(ex) { };
   try { mSwitch.deleteVar('FamilyType'); } catch(ex) { };
   try { mSwitch.deleteVar('PolType'); } catch(ex) { };
   try { mSwitch.deleteVar('CardFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('ManageCom'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentCom'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentCode'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentGroup'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentCode1'); } catch(ex) { };
   try { mSwitch.deleteVar('AgentType'); } catch(ex) { };
   try { mSwitch.deleteVar('SaleChnl'); } catch(ex) { };
   try { mSwitch.deleteVar('Handler'); } catch(ex) { };
   try { mSwitch.deleteVar('Password'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntNo'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntName'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntSex'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntBirthday'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntIDType'); } catch(ex) { };
   try { mSwitch.deleteVar('AppntIDNo'); } catch(ex) { };
   try { mSwitch.deleteVar('AppIDStartDate'); } catch(ex) { };
   try { mSwitch.deleteVar('AppIDEndDate'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredNo'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredNam'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredSex'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredBirthday'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredIDType'); } catch(ex) { };
   try { mSwitch.deleteVar('InsuredIDNo'); } catch(ex) { };
   try { mSwitch.deleteVar('PayIntv'); } catch(ex) { };
   try { mSwitch.deleteVar('PayMode'); } catch(ex) { };
   try { mSwitch.deleteVar('PayLocation'); } catch(ex) { };
   try { mSwitch.deleteVar('DisputedFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('OutPayFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('GetPolMode'); } catch(ex) { };
   try { mSwitch.deleteVar('SignCom'); } catch(ex) { };
   try { mSwitch.deleteVar('SignDate'); } catch(ex) { };
   try { mSwitch.deleteVar('SignTime'); } catch(ex) { };
   try { mSwitch.deleteVar('ConsignNo'); } catch(ex) { };
   try { mSwitch.deleteVar('BankCode'); } catch(ex) { };
   try { mSwitch.deleteVar('BankAccNo'); } catch(ex) { };
   try { mSwitch.deleteVar('AccName'); } catch(ex) { };
   try { mSwitch.deleteVar('PrintCount'); } catch(ex) { };
   try { mSwitch.deleteVar('LostTimes'); } catch(ex) { };
   try { mSwitch.deleteVar('Lang'); } catch(ex) { };
   try { mSwitch.deleteVar('Currency'); } catch(ex) { };
   try { mSwitch.deleteVar('Remark'); } catch(ex) { };
   try { mSwitch.deleteVar('Peoples'); } catch(ex) { };
   try { mSwitch.deleteVar('Mult'); } catch(ex) { };
   try { mSwitch.deleteVar('Prem'); } catch(ex) { };
   try { mSwitch.deleteVar('Amnt'); } catch(ex) { };
   try { mSwitch.deleteVar('SumPrem'); } catch(ex) { };
   try { mSwitch.deleteVar('Dif'); } catch(ex) { };
   try { mSwitch.deleteVar('PaytoDate'); } catch(ex) { };
   try { mSwitch.deleteVar('FirstPayDate'); } catch(ex) { };
   try { mSwitch.deleteVar('CValiDate'); } catch(ex) { };
   try { mSwitch.deleteVar('InputOperator'); } catch(ex) { };
   try { mSwitch.deleteVar('InputDate'); } catch(ex) { };
   try { mSwitch.deleteVar('InputTime'); } catch(ex) { };
   try { mSwitch.deleteVar('ApproveFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('ApproveCode'); } catch(ex) { };
   try { mSwitch.deleteVar('ApproveDate'); } catch(ex) { };
   try { mSwitch.deleteVar('ApproveTime'); } catch(ex) { };
   try { mSwitch.deleteVar('UWFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('UWOperator'); } catch(ex) { };
   try { mSwitch.deleteVar('UWDate'); } catch(ex) { };
   try { mSwitch.deleteVar('UWTime'); } catch(ex) { };
   try { mSwitch.deleteVar('AppFlag'); } catch(ex) { };
   try { mSwitch.deleteVar('PolApplyDate'); } catch(ex) { };
   try { mSwitch.deleteVar('GetPolDate'); } catch(ex) { };
   try { mSwitch.deleteVar('GetPolTime'); } catch(ex) { };
   try { mSwitch.deleteVar('CustomGetPolDate'); } catch(ex) { };
   try { mSwitch.deleteVar('State'); } catch(ex) { };
   try { mSwitch.deleteVar('Operator'); } catch(ex) { };
   try { mSwitch.deleteVar('MakeDate'); } catch(ex) { };
   try { mSwitch.deleteVar('MakeTime'); } catch(ex) { };
   try { mSwitch.deleteVar('ModifyDate'); } catch(ex) { };
   try { mSwitch.deleteVar('ModifyTime'); } catch(ex) { };

    //新的删除数据处理
try { mSwitch.deleteVar  ('AppntNo'); } catch(ex) { };				
try { mSwitch.deleteVar  ('AppntName'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntSex'); } catch(ex) { };				
try { mSwitch.deleteVar  ('AppntBirthday'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntIDType'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntIDNo'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntPassword'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntNativePlace'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntNationality'); } catch(ex) { };	
try {mSwitch.deleteVar('AddressNo');}catch(ex){};   	
try { mSwitch.deleteVar  ('AppntRgtAddress'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntMarriage'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntMarriageDate'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntHealth'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntStature'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntAvoirdupois'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntDegree'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntCreditGrade'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntOthIDType'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntOthIDNo'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntICNo'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntGrpNo'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntJoinCompanyDate'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntStartWorkDate'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntPosition') } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntSalary'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntOccupationType'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntOccupationCode'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntWorkType'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntPluralityType');} catch(ex) { };	
try { mSwitch.deleteVar  ('AppntDeathDate'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntSmokeFlag'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntBlacklistFlag'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntProterty'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntRemark'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntState'); } catch(ex) { };			
try { mSwitch.deleteVar  ('AppntOperator'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntMakeDate'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntMakeTime'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntModifyDate'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntModifyTime'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntHomeAddress'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntHomeZipCode'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntHomePhone'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntHomeFax'); } catch(ex) { };		
try { mSwitch.deleteVar  ('AppntPostalAddress'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntZipCode'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntPhone'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntFax'); } catch(ex) { };	
try { mSwitch.deleteVar  ('AppntMobile'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntEMail'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntGrpName'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntGrpPhone'); } catch(ex) { };
try { mSwitch.deleteVar  ('CompanyAddress'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntGrpZipCode'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntGrpFax'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntBankAccNo'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntBankCode'); } catch(ex) { };
try { mSwitch.deleteVar  ('AppntAccName'); } catch(ex) { };
try { mSwitch.deleteVar  ('ActivityID' ); } catch(ex) {};
try { mSwitch.deleteVar  ('MissionID' ); } catch(ex) {} ;
try { mSwitch.deleteVar  ('SubMissionID'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('LoadFlag'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('appnt_PostalCity'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('appnt_PostalCommunity'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('appnt_PostalCounty'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('appnt_PostalProvince'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('appnt_PostalStreet'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('AppntHomeCode'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('AppntHomeNumber'); } catch(ex) {} ;

try { mSwitch.deleteVar  ('TaxFlag'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('TaxFlagName'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('TaxNo'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('TaxPayerType'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('TaxPayerTypeName'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('BatchNo'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('InsuredId'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('GrpNo'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('GTaxNo'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('TransFlag'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('PremMult'); } catch(ex) {} ;
try { mSwitch.deleteVar  ('PremMultName'); } catch(ex) {} ;
}


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult ) {
  //alert("here:" + arrQueryResult + "\n" + mOperate);
	if( arrQueryResult != null ) {
		arrResult = arrQueryResult;

		if( mOperate == 1 )	{		// 查询投保单	
			fm.all( 'ContNo' ).value = arrQueryResult[0][0];

			arrResult = easyExecSql("select ProposalContNo,PrtNo,ManageCom,SaleChnl,AgentCode,AgentGroup,AgentCode1,AgentCom,AgentType,Remark,ReceiveDate,FirstTrialOperator,FirstTrialDate from LCCont where LCCont = '" + arrQueryResult[0][0] + "'", 1, 0);

			if (arrResult == null) {
			  alert("未查到投保单信息");
			} else {
			   displayLCContPol(arrResult[0]);
			}
		}
		if( mOperate == 2 )	{		// 投保人信息
			arrResult = easyExecSql("select a.* from LDPerson a where 1=1  and a.CustomerNo = '" + arrQueryResult[0][0] + "'", 1, 0);
			if (arrResult == null) {
			  alert("未查到投保人信息");
			} else {
			   displayAppnt(arrResult[0]);
			   getaddresscodedata();
			  showAllCodeName(); 
			}
		}
	}
	mOperate = 0;		// 恢复初态
}
/*********************************************************************
 *  投保单信息初始化函数。以loadFlag标志作为分支
 *  参数  ：  投保单印刷号
 *  返回值：  无
 *********************************************************************
 */
function detailInit(PrtNo){
try{
  if(PrtNo==null)
    return;
  arrResult=easyExecSql("select * from LCCont where PrtNo='"+PrtNo+"'",1,0);
  if(arrResult==null){
    alert(未得到投保单信息);
    return;
  }else{
    displayLCCont();       //显示投保单详细内容
    showOneCodeName("PayIntv",fm.PayIntvName.name);
    showOneCodeName("PayMode",fm.PayModeName.name);
  }
  arrResult = easyExecSql("select a.* from LDPerson a where 1=1  and a.CustomerNo = '" + arrResult[0][19] + "'", 1, 0);
  if (arrResult == null) {
    alert("未查到投保人信息");
  } else {
     displayAppnt(arrResult[0]);
  }  
  arrResult=easyExecSql("select * from LCAppnt where PrtNo='"+PrtNo+"'",1,0);
  if(arrResult==null){
    alert("未得到投保人信息");
    return;
  }else{
    displayContAppnt();       //显示投保人的详细内容
    //displayAccount();			//显示头投保人帐户信息
  }
  var tContNo = arrResult[0][1];  
  var tCustomerNo = arrResult[0][3];		// 得到投保人客户号
  var tAddressNo = arrResult[0][9]; 		// 得到投保人地址号
  displaySYinfo(PrtNo);
  displaydiscountfactorinfo();
  fm.AddressNo.value=tAddressNo;
  getdetailaddress();//显示投保人地址详细内容
  var strSQL1="select ImpartVer,ImpartCode,ImpartContent,ImpartParamModle from LCCustomerImpart where CustomerNo='"+tCustomerNo+"' and ContNo='"+tContNo+"' and CustomerNoType='A'";
  turnPage.strQueryResult = easyQueryVer3(strSQL1, 1, 0, 1);
  //判断是否查询成功,成功则显示告知信息
  if (turnPage.strQueryResult) {
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = ImpartGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL1; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
  else{
    initImpartGrid();
  }
  var arr = easyExecSql("select GrpName from LCAddress where CustomerNo='"+tCustomerNo+"' and AddressNo='"+tAddressNo+"'");
  	if(arr)
  	{
  		fm.AppntGrpName.value = arr[0][0];
  	}
//  if(LoadFlag=="5"){
//     var arr = easyExecSql("select 1 from lcapprove where prtno='"+PrtNo+"' and customerno='"+tCustomerNo+"' and customertype='00' ");
//     if(!arr){
//        fm.AppntName.value="";
//        fm.AppntSex.value="";
//        fm.AppntBirthday.value="";
//        fm.AppntIDType.value="";
//        fm.AppntIDNo.value="";
//        fm.SaleChnl.value="";
//        fm.AgentCom.value="";
//        fm.GroupAgentCode.value="";
//        fm.AgentCode.value="";
//        fm.AgentSaleCode.value="";
//        fm.GrpAgentCom.value="";
//        fm.GrpAgentCode.value="";
//        fm.GrpAgentName.value="";
//        fm.Crs_SaleChnl.value="";
//        fm.Crs_BussType.value="";
//        fm.GrpAgentIDNo.value="";
//        var arr1 = easyExecSql("select payintv from lcpol where prtno='"+PrtNo+"' and payintv <>'0' ");
//        if(arr1){
//        fm.AppntBankCode.value="";
//        fm.AppntBankCodeName.value="";
//        fm.AppntBankAccNo.value="";
//        }
//     }    
//  }	
}catch(ex){}
 
}

/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAccount()
{
//	if(fm.all('PayMode').value=="4" 
//	||fm.all('PayMode').value=="6" 
//    || fm.all('PayMode').value=="1"
//        || fm.all('PayMode').value=="11" 
//        || fm.all('PayMode').value=="12")
//	{
	try{fm.all('AppntBankAccNo').value= arrResult[0][24]; }catch(ex){}; 
	try{fm.all('AppntBankCode').value= arrResult[0][23]; }catch(ex){}; 
	try{fm.all('AppntAccName').value= arrResult[0][25]; }catch(ex){};
	
// }
//else{
	      //fm.all('AppntBankCode').style.display="none";
        //fm.all('AppntBankCodeName').style.display="none";
        //fm.all('AppntAccName').style.display="none";
        //fm.all('AppntBankAccNo').style.display="none";
//	}
	
}
/*********************************************************************
 *  把查询返回的客户数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayCustomer()
{
	try{fm.all('AppntNationality').value= arrResult[0][8]; }catch(ex){}; 
}
/*********************************************************************
 *  把查询返回的客户地址数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAddress()
{
try{fm.all('AddressNo').value= arrResult[0][0];}catch(ex){}; 
try{fm.all('AppntPostalAddress').value= arrResult[0][1];}catch(ex){}; 
try{fm.all('AppntZipCode').value= arrResult[0][2];}catch(ex){}; 
try{fm.all('AppntPhone').value= arrResult[0][3];}catch(ex){}; 
try{fm.all('AppntMobile').value= arrResult[0][4];}catch(ex){}; 
try{fm.all('AppntEMail').value= arrResult[0][5];}catch(ex){}; 
//try{fm.all('GrpName').value= arrResult[0][6];}catch(ex){}; 
try{fm.all('AppntGrpPhone').value= arrResult[0][6];}catch(ex){}; 
try{fm.all('CompanyAddress').value= arrResult[0][7];}catch(ex){}; 
try{fm.all('AppntGrpZipCode').value= arrResult[0][8];}catch(ex){}; 
/*	
        try{fm.all('AppntPostalAddress').value= arrResult[0][2]; }catch(ex){}; 
	try{fm.all('AppntZipCode').value= arrResult[0][3]; }catch(ex){}; 
	try{fm.all('AppntPhone').value= arrResult[0][4]; }catch(ex){}; 
	try{fm.all('AppntMobile').value= arrResult[0][14]; }catch(ex){}; 
	try{fm.all('AppntEMail').value= arrResult[0][16]; }catch(ex){}; 
	//try{fm.all('AppntGrpName').value= arrResult[0][2]; }catch(ex){}; 
	try{fm.all('AppntGrpPhone').value= arrResult[0][12]; }catch(ex){}; 
	try{fm.all('CompanyAddress').value= arrResult[0][10]; }catch(ex){}; 
	try{fm.all('AppntGrpZipCode').value= arrResult[0][11]; }catch(ex){}; 
*/
}

/*********************************************************************
 *  把查询返回的合同中被保人数据显示到页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayContAppnt()
{

try{fm.all('AppntGrpContNo').value= arrResult[0][0];}catch(ex){};             
try{fm.all('AppntContNo').value= arrResult[0][1];}catch(ex){};                 
try{fm.all('AppntPrtNo').value= arrResult[0][2];}catch(ex){};                  
try{fm.all('AppntNo').value= arrResult[0][3];}catch(ex){};                
try{fm.all('AppntGrade').value= arrResult[0][4];}catch(ex){};             
try{fm.all('AppntName').value= arrResult[0][5];}catch(ex){};              
try{fm.all('AppntSex').value= arrResult[0][6];}catch(ex){};               
try{fm.all('AppntBirthday').value= arrResult[0][7];}catch(ex){};          
try{fm.all('AppntType').value= arrResult[0][8];}catch(ex){};              
try{fm.all('AppntAddressNo').value= arrResult[0][9]; }catch(ex){};             
try{fm.all('AppntIDType').value= arrResult[0][10]; }catch(ex){};               
try{fm.all('AppntIDNo').value= arrResult[0][11]; }catch(ex){};                 
try{fm.all('AppntNativePlace').value= arrResult[0][12];}catch(ex){};           
try{fm.all('AppntNationality').value= arrResult[0][13];}catch(ex){};           
try{fm.all('AppntRgtAddress').value= arrResult[0][14];}catch(ex){};            
try{fm.all('AppntMarriage').value= arrResult[0][15];}catch(ex){};             
try{fm.all('AppntMarriageDate').value= arrResult[0][16];}catch(ex){};          
try{fm.all('AppntHealth').value= arrResult[0][17];}catch(ex){};                
try{fm.all('AppntStature').value= arrResult[0][18];}catch(ex){};               
try{fm.all('AppntAvoirdupois').value= arrResult[0][19];}catch(ex){};           
try{fm.all('AppntDegree').value= arrResult[0][20];}catch(ex){};                          
 if(fm.all('PayMode').value!="")
 {
 		//alert(fm.all('PayMode').value);
	//if(fm.all('PayMode').value=="4"
	//||fm.all('PayMode').value=="6"
    //|| fm.all('PayMode').value=="1"
    //    || fm.all('PayMode').value == "11"
    //    || fm.all('PayMode').value == "12")
	//{
			//alert();
				try{fm.all('AppntBankCode').value= arrResult[0][22];}catch(ex){}; 
	      try{fm.all('AppntAccName').value= arrResult[0][24]; }catch(ex){};  
	      try{fm.all('AppntBankAccNo').value= arrResult[0][23];}catch(ex){};
	      var tsql="select BankName from LDBank where (BankUniteFlag is null or BankUniteFlag<>'1') and bankcode='"+fm.all('AppntBankCode').value+"'";
	     // alert(tsql);
	      var bankname=easyExecSql(tsql);
          bankname = bankname == "null" ? "" : bankname;
				//alert(bankname);
				try{fm.all('AppntBankCodeName').value= bankname;}catch(ex){}; 

  //}
  //else
  //{
	      //fm.all('AppntBankCode').style.display="none";
        //fm.all('AppntBankCodeName').style.display="none";
        //fm.all('AppntAccName').style.display="none";
        //fm.all('AppntBankAccNo').style.display="none";
	//}
}                     
try{fm.all('AppntJoinCompanyDate').value= arrResult[0][25];}catch(ex){};       
try{fm.all('AppntStartWorkDate').value= arrResult[0][26];}catch(ex){};         
try{fm.all('AppntPosition').value= arrResult[0][27];}catch(ex){};              
if(arrResult[0][28]=='-1'){
	try{fm.all('AppntSalary').value=""; }catch(ex){};               
}else{
	try{fm.all('AppntSalary').value= arrResult[0][28]; }catch(ex){};
}              
try{fm.all('AppntOccupationType').value= arrResult[0][29];}catch(ex){};     
try{fm.all('AppntOccupationCode').value= arrResult[0][30];}catch(ex){};
try {fm.all('AppntOccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][30]);} catch(ex) { };      
try{fm.all('AppntWorkType').value= arrResult[0][31]; }catch(ex){};             
try{fm.all('AppntPluralityType').value= arrResult[0][32];}catch(ex){};         
try{fm.all('AppntSmokeFlag').value= arrResult[0][33];}catch(ex){};             
try{fm.all('AppntOperator').value= arrResult[0][34];}catch(ex){};              
try{fm.all('AppntManageCom').value= arrResult[0][35];}catch(ex){};             
try{fm.all('AppntMakeDate').value= arrResult[0][36];}catch(ex){};              
try{fm.all('AppntMakeTime').value= arrResult[0][37]; }catch(ex){};             
try{fm.all('AppntModifyDate').value= arrResult[0][38];}catch(ex){};            
try{fm.all('AppntModifyTime').value= arrResult[0][39];}catch(ex){};  

try{fm.all('AppIDStartDate').value= arrResult[0][44];}catch(ex){};
try{fm.all('AppIDEndDate').value= arrResult[0][45];}catch(ex){};
try{fm.all('AppntNativeCity').value= arrResult[0][47];}catch(ex){};
setCodeName("AppntNativeCityName", "nativecity", arrResult[0][47]);  
//try{fm.all('ExiSpec').value= arrResult[0][46];}catch(ex){};  // 投保人不需要这个字段，投保备注栏应该是保单层的数据，这个字段估计是创建错了.. 
}
/*********************************************************************
 *  把查询返回的投保人数据显示到投保人部分
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayAppnt()
{
try{fm.all('AppntNo').value= arrResult[0][0]; }catch(ex){};              
try{fm.all('AppntName').value= arrResult[0][1]; }catch(ex){};            
try{fm.all('AppntSex').value= arrResult[0][2]; }catch(ex){};             
try{fm.all('AppntBirthday').value= arrResult[0][3]; }catch(ex){};        
try{fm.all('AppntIDType').value= arrResult[0][4]; }catch(ex){};          
try{fm.all('AppntIDNo').value= arrResult[0][5]; }catch(ex){};            
try{fm.all('AppntPassword').value= arrResult[0][6]; }catch(ex){};        
try{fm.all('AppntNativePlace').value= arrResult[0][7]; }catch(ex){};     
try{fm.all('AppntNationality').value= arrResult[0][8]; }catch(ex){};     
try{fm.all('AppntRgtAddress').value= arrResult[0][9]; }catch(ex){};     
try{fm.all('AppntMarriage').value= arrResult[0][10];}catch(ex){};        
try{fm.all('AppntMarriageDate').value= arrResult[0][11];}catch(ex){};    
try{fm.all('AppntHealth').value= arrResult[0][12];}catch(ex){};          
try{fm.all('AppntStature').value= arrResult[0][13];}catch(ex){};         
try{fm.all('AppntAvoirdupois').value= arrResult[0][14];}catch(ex){};     
try{fm.all('AppntDegree').value= arrResult[0][15];}catch(ex){};          
try{fm.all('AppntCreditGrade').value= arrResult[0][16];}catch(ex){};     
try{fm.all('AppntOthIDType').value= arrResult[0][17];}catch(ex){};       
try{fm.all('AppntOthIDNo').value= arrResult[0][18];}catch(ex){};         
try{fm.all('AppntICNo').value= arrResult[0][19];}catch(ex){};   
         
try{fm.all('AppntGrpNo').value= arrResult[0][20];}catch(ex){};           
try{fm.all('AppntJoinCompanyDate').value= arrResult[0][21];}catch(ex){}; 
try{fm.all('AppntStartWorkDate').value= arrResult[0][22];}catch(ex){};   
try{fm.all('AppntPosition').value= arrResult[0][23];}catch(ex){};        
try{fm.all('AppntSalary').value= arrResult[0][24];}catch(ex){};        
try{fm.all('AppntOccupationType').value= arrResult[0][25];}catch(ex){};  
try{fm.all('AppntOccupationCode').value= arrResult[0][26];}catch(ex){};
try {fm.all('AppntOccupationName').value= getNameByCode("trim(OccupationName)||'-'||trim(workname)","LDOccupation","OccupationCode",arrResult[0][26]);} catch(ex) { };    
try{fm.all('AppntWorkType').value= arrResult[0][27];}catch(ex){};        
try{fm.all('AppntPluralityType').value= arrResult[0][28];}catch(ex){};   
try{fm.all('AppntDeathDate').value= arrResult[0][29];}catch(ex){};       
try{fm.all('AppntSmokeFlag').value= arrResult[0][30];}catch(ex){};       
try{fm.all('AppntBlacklistFlag').value= arrResult[0][31];}catch(ex){};   
try{fm.all('AppntProterty').value= arrResult[0][32];}catch(ex){};        
try{fm.all('AppntRemark').value= arrResult[0][33];}catch(ex){};          
try{fm.all('AppntState').value= arrResult[0][34];}catch(ex){};
try{fm.all('VIPValue').value= arrResult[0][35];}catch(ex){};           
try{fm.all('AppntOperator').value= arrResult[0][36];}catch(ex){};        
try{fm.all('AppntMakeDate').value= arrResult[0][37];}catch(ex){};        
try{fm.all('AppntMakeTime').value= arrResult[0][38];}catch(ex){};      
try{fm.all('AppntModifyDate').value= arrResult[0][39];}catch(ex){};      
try{fm.all('AppntModifyTime').value= arrResult[0][40];}catch(ex){};      
try{fm.all('AppntGrpName').value= arrResult[0][41];}catch(ex){}; 
try{fm.all('AppntNativeCity').value= arrResult[0][47];}catch(ex){}; 
try{fm.all('AppntNativePlaceName').value= ""; }catch(ex){};
setCodeName("AppntNativeCityName", "nativecity", arrResult[0][47]);
controlNativeCity("");
showAllCodeName();
//try{fm.all('AppntPostalAddress').value= "";}catch(ex){}; 
//try{fm.all('AppntPostalAddress').value= "";}catch(ex){}; 
//try{fm.all('AppntZipCode').value= "";}catch(ex){}; 
//try{fm.all('AppntPhone').value= "";}catch(ex){}; 
//try{fm.all('AppntFax').value= "";}catch(ex){}; 
//try{fm.all('AppntMobile').value= "";}catch(ex){}; 
//try{fm.all('AppntEMail').value= "";}catch(ex){}; 
////try{fm.all('AppntGrpName').value= "";}catch(ex){}; 
//try{fm.all('AppntHomeAddress').value= "";}catch(ex){}; 
//try{fm.all('AppntHomeZipCode').value= "";}catch(ex){}; 
//try{fm.all('AppntHomePhone').value= "";}catch(ex){}; 
//try{fm.all('AppntHomeFax').value= "";}catch(ex){}; 
//try{fm.all('AppntGrpPhone').value= "";}catch(ex){}; 
//try{fm.all('CompanyAddress').value= "";}catch(ex){}; 
//try{fm.all('AppntGrpZipCode').value= "";}catch(ex){};  
//try{fm.all('AppntGrpFax').value= "";}catch(ex){};  

}
/**
 *投保单详细内容显示
 */
function displayLCCont() {
    try { fm.all('GrpContNo').value = arrResult[0][0]; } catch(ex) { };                    
    try { fm.all('ContNo').value = arrResult[0][1]; } catch(ex) { };                       
    try { fm.all('ProposalContNo').value = arrResult[0][2]; } catch(ex) { };               
    try { fm.all('PrtNo').value = arrResult[0][3]; } catch(ex) { };                        
    try { fm.all('ContType').value = arrResult[0][4]; } catch(ex) { };                     
    try { fm.all('FamilyType').value = arrResult[0][5]; } catch(ex) { };                
    try { fm.all('FamilyID').value = arrResult[0][6]; } catch(ex) { };                     
    try { fm.all('PolType ').value = arrResult[0][7]; } catch(ex) { };                     
    try { fm.all('CardFlag').value = arrResult[0][8]; } catch(ex) { };                     
    try { fm.all('ManageCom').value = arrResult[0][9]; } catch(ex) { };                    
    try { fm.all('ExecuteCom ').value = arrResult[0][10]; } catch(ex) { };                 
    try { fm.all('AgentCom').value = arrResult[0][11]; } catch(ex) { };                    
    try { fm.all('AgentCom1').value = arrResult[0][11]; } catch(ex) { };   
    try { fm.all('AgentCode').value = arrResult[0][12]; } catch(ex) { };
    try { fm.all('AgentGroup').value = arrResult[0][13];} catch(ex) { };                 
    try { fm.all('AgentCode1 ').value = arrResult[0][14]; } catch(ex) { };                 
    try { fm.all('AgentType').value = arrResult[0][15]; } catch(ex) { };                   
    try { fm.all('SaleChnl').value = arrResult[0][16]; } catch(ex) { };                    
    try { fm.all('Handler').value = arrResult[0][17]; } catch(ex) { };                    
    try { fm.all('Password').value = arrResult[0][18]; } catch(ex) { };                    
    try { fm.all('AppntNo').value = arrResult[0][19]; } catch(ex) { };                    
    try { fm.all('AppntName').value = arrResult[0][20]; } catch(ex) { };                   
    try { fm.all('AppntSex').value = arrResult[0][21]; } catch(ex) { };                    
    try { fm.all('AppntBirthday').value = arrResult[0][22]; } catch(ex) { };              
    try { fm.all('AppntIDType').value = arrResult[0][23]; } catch(ex) { };                 
    try { fm.all('AppntIDNo').value = arrResult[0][24]; } catch(ex) { };                   
    try { fm.all('InsuredNo').value = arrResult[0][25]; } catch(ex) { };                   
    try { fm.all('InsuredName').value = arrResult[0][26]; } catch(ex) { };                 
    try { fm.all('InsuredSex').value = arrResult[0][27]; } catch(ex) { };                 
    try { fm.all('InsuredBirthday').value = arrResult[0][28]; } catch(ex) { };             
    try { fm.all('InsuredIDType ').value = arrResult[0][29]; } catch(ex) { };              
    try { fm.all('InsuredIDNo').value = arrResult[0][30]; } catch(ex) { };                 
    try { fm.all('PayIntv').value = arrResult[0][31]; } catch(ex) { };    
    try { fm.all('PayIntvName').value = arrResult[0][31]; } catch(ex) { };                
    try { fm.all('PayMode').value = arrResult[0][32]; } catch(ex) { }; 
    try { fm.all('PayModeName').value = arrResult[0][32]; } catch(ex) { };                   
    try { fm.all('PayLocation').value = arrResult[0][33]; } catch(ex) { };                 
    try { fm.all('DisputedFlag').value = arrResult[0][34]; } catch(ex) { };                
    try { fm.all('OutPayFlag').value = arrResult[0][35]; } catch(ex) { };                 
    try { fm.all('GetPolMode').value = arrResult[0][36]; } catch(ex) { };                 
    try { fm.all('SignCom').value = arrResult[0][37]; } catch(ex) { };                    
    try { fm.all('SignDate').value = arrResult[0][38]; } catch(ex) { };                    
    try { fm.all('SignTime').value = arrResult[0][39]; } catch(ex) { };                    
    try { fm.all('ConsignNo').value = arrResult[0][40]; } catch(ex) { };                   
    try { fm.all('BankCode').value = arrResult[0][41]; } catch(ex) { };                    
    try { fm.all('BankAccNo').value = arrResult[0][42]; } catch(ex) { };                   
    try { fm.all('AccName').value = arrResult[0][43]; } catch(ex) { };                    
    try { fm.all('PrintCount ').value = arrResult[0][44]; } catch(ex) { };                 
    try { fm.all('LostTimes').value = arrResult[0][45]; } catch(ex) { };                   
    try { fm.all('Lang ').value = arrResult[0][46]; } catch(ex) { };                       
    try { fm.all('Currency').value = arrResult[0][47]; } catch(ex) { };                    
    try { fm.all('Remark').value = arrResult[0][48]; } catch(ex) { };                      
    try { fm.all('Peoples').value = arrResult[0][49]; } catch(ex) { };                    
    try { fm.all('Mult').value = arrResult[0][50]; } catch(ex) { };                       
    try { fm.all('Prem').value = arrResult[0][51]; } catch(ex) { };                       
    try { fm.all('Amnt').value = arrResult[0][52]; } catch(ex) { };                       
    try { fm.all('SumPrem').value = arrResult[0][53]; } catch(ex) { };                    
    try { fm.all('Dif').value = arrResult[0][54]; } catch(ex) { };                         
    try { fm.all('PaytoDate').value = arrResult[0][55]; } catch(ex) { };                   
    try { fm.all('FirstPayDate').value = arrResult[0][56]; } catch(ex) { };                
    try { fm.all('CValiDate').value = arrResult[0][57]; } catch(ex) { };                   
    try { fm.all('InputOperator ').value = arrResult[0][58]; } catch(ex) { };              
    try { fm.all('InputDate').value = arrResult[0][59]; } catch(ex) { };                   
    try { fm.all('InputTime').value = arrResult[0][60]; } catch(ex) { };                   
    try { fm.all('ApproveFlag').value = arrResult[0][61]; } catch(ex) { };                 
    try { fm.all('ApproveCode').value = arrResult[0][62]; } catch(ex) { };                 
    try { fm.all('ApproveDate').value = arrResult[0][63]; } catch(ex) { };                 
    try { fm.all('ApproveTime').value = arrResult[0][64]; } catch(ex) { };                 
    try { fm.all('UWFlag').value = arrResult[0][65]; } catch(ex) { };                      
    try { fm.all('UWOperator ').value = arrResult[0][66]; } catch(ex) { };                 
    try { fm.all('UWDate').value = arrResult[0][67]; } catch(ex) { };                      
    try { fm.all('UWTime').value = arrResult[0][68]; } catch(ex) { };                      
    try { fm.all('AppFlag').value = arrResult[0][69]; } catch(ex) { };                    
    try { fm.all('PolApplyDate').value = arrResult[0][70];} catch(ex) { };                
    try { fm.all('GetPolDate').value = arrResult[0][71]; } catch(ex) { };                 
    try { fm.all('GetPolTime').value = arrResult[0][72]; } catch(ex) { };                 
    try { fm.all('CustomGetPolDate').value = arrResult[0][73]; } catch(ex) { };           
    try { fm.all('State').value = arrResult[0][74]; } catch(ex) { };                       
    try { fm.all('Operator').value = arrResult[0][75]; } catch(ex) { };                    
    try { fm.all('MakeDate').value = arrResult[0][76]; } catch(ex) { };                    
    try { fm.all('MakeTime').value = arrResult[0][77]; } catch(ex) { };                    
    try { fm.all('ModifyDate').value = arrResult[0][78]; } catch(ex) { };                 
    try { fm.all('ModifyTime').value = arrResult[0][79]; } catch(ex) { };  
    try { fm.all('FirstTrialOperator').value = arrResult[0][80]; }catch(ex) { };
    try { fm.all('FirstTrialDate').value = arrResult[0][81]; } catch(ex) { }; 
    try { fm.all('FirstTrialTime').value = arrResult[0][82]; } catch(ex) { };
    try { fm.all('ReceiveOperator').value = arrResult[0][83]; } catch(ex) { };
    try { fm.all('ReceiveDate').value = arrResult[0][84]; } catch(ex) { }; 
    try { fm.all('ReceiveTime').value = arrResult[0][85]; } catch(ex) { }; 
    try { fm.all('SaleChnlDetail').value = arrResult[0][88]; } catch(ex) { }; 
    try { fm.all('TempFeeNo').value = arrResult[0][86]; } catch(ex) { };
    try { fm.all('GrpAgentCom').value = arrResult[0][102]; } catch(ex) { }; 
    try { fm.all('GrpAgentCode').value = arrResult[0][103]; } catch(ex) { }; 
    try { fm.all('GrpAgentName').value = arrResult[0][104]; } catch(ex) { };  
    try { fm.all('Crs_SaleChnl').value = arrResult[0][106]; } catch(ex) { };
    try { fm.all('Crs_BussType').value = arrResult[0][107]; } catch(ex) { };
    try { fm.all('GrpAgentIDNo').value = arrResult[0][108]; } catch(ex) { };
    try { fm.all('DueFeeMsgFlag').value = arrResult[0][109]; } catch(ex) { };
    //try { fm.all('PayMethod').value = arrResult[0][110]; } catch(ex) { };
    try { fm.all('ExiSpec').value = arrResult[0][111]; } catch(ex) { };
    try { fm.all('ExPayMode').value = arrResult[0][112]; } catch(ex) { };
    try { fm.all('AgentSaleCode').value = arrResult[0][113];} catch(ex) { };
    var agentResult = easyExecSql("select getUniteCode('"+arrResult[0][12]+"') from dual ");
	if (agentResult != null) {
		fm.GroupAgentCode.value = agentResult[0][0];
	}
    
    if(arrResult[0][102] != "" || arrResult[0][103] != "" || arrResult[0][104]!="" || arrResult[0][106]!="" || arrResult[0][107]!="" || arrResult[0][108]!="")
    {   
        fm.MixComFlag.checked = true;
        if(fm.MixComFlag.checked == true)
        {
            fm.all('GrpAgentComID').style.display = "";
	        fm.all('GrpAgentTitleID').style.display = "";
	        fm.all('GrpAgentTitleIDNo').style.display = "";
        }
        var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
		var arrResult1 = easyExecSql(strSql);
		if (arrResult1 != null) {
		     fm.GrpAgentComName.value = arrResult1[0][0];
		}
		else{  
		     fm.GrpAgentComName.value = "";
	    }
    }
    
    if(arrResult[0][16] == "10"||arrResult[0][16] == "04"||arrResult[0][16] == "15"||arrResult[0][16] == "03"){
    	fm.all("AgentSaleCodeID").style.display = "";
    	var strSql2 = "select Name from laagenttemp where '1337927697000'='1337927697000' and  AgentCode='" + fm.AgentSaleCode.value+"' with ur";
		var arrResult2 = easyExecSql(strSql2);
		if (arrResult2 != null) {
		     fm.AgentSaleName.value = arrResult2[0][0];
		}
    } else {
    	fm.all("AgentSaleCodeID").style.display = "none";
    	fm.all('AgentSaleCode').value = "";
    }
    
}                                                                                                                                                        
//使得从该窗口弹出的窗口能够聚焦                                             
function myonfocus()                                                         
{                                                                            
	if(showInfo!=null)                                                           
	{                                                                            
	  try                                                                        
	  {                                                                          
	    showInfo.focus();                                                        
      }
	  catch(ex)                                                                  
	  {                                                                          
	    showInfo=null;                                                           
	  }                                                                          
	}                                                                            
}                                                                            
                                                                             
                                                                             
                                                                             
//投保人客户号查询按扭事件                                                   
function queryAppntNo() { 
  if (fm.all("AppntNo").value == "" && loadFlag == "1") {                    
    showAppnt1();                                                            
    } else if (loadFlag != "1" && loadFlag != "2") {                         
     alert("只能在投保单录入时进行操作！");                                 
    } else {                                                    
   arrResult = easyExecSql("select a.* from LDPerson a where 1=1  and a.CustomerNo = '" + fm.all("AppntNo").value + "'", 1, 0);
    if (arrResult == null) {                                                 
      alert("未查到投保人信息");                                             
      displayAppnt(new Array());
      emptyUndefined();    
    } else {                                                                 
      displayAppnt(arrResult[0]);
      getaddresscodedata();                                            
    }                                                                  
  }                                                                          
}                                                                            
                                                                             
function showAppnt1()                                                        
{                                                                            
	if( mOperate == 0 )                                                          
	{                                                                            
		mOperate = 2;                                                        
		showInfo = window.open( "../sys/LDPersonQueryNew.html" );               
	}                                                                            
}                                                                            
                                                                             
function queryAgent()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！"); 
        return ;
    }
    
    if(fm.all('GroupAgentCode').value == "")
    {  
        //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=1","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        
        var tSaleChnl = (fm.SaleChnl != null && fm.SaleChnl != "undefined") ? fm.SaleChnl.value : "";
        var tAgentCom = (fm.AgentCom != null && fm.AgentCom != "undefined") ? fm.AgentCom.value : "";
        var tBranchType = 1;
        // 个险中的职团开拓表示由团险业务员卖个单。
        // 因此，当渠道为“职团开拓”（07）时，应查询出团险的业务员。
        if(tSaleChnl == "07" || tSaleChnl == "11" || tSaleChnl == "12")
            tBranchType = 2;
        //------------------------------------
        var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&SaleChnl=" + tSaleChnl
            + "&AgentCom=" + tAgentCom
            + "&branchtype= null " ;
        var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('GroupAgentCode').value != "")
    {
        var cAgentCode = fm.GroupAgentCode.value;  //保单号码	
        var strSql = "select GroupAgentCode,Name,AgentGroup,agentcode from LAAgent where GroupAgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentGroup.value = arrResult[0][2];
            fm.AgentCode.value = arrResult[0][3];
            alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value="";
            fm.AgentCode.value ="";
            alert("代码为:[" + fm.all('GroupAgentCode').value + "]的业务员不存在，请确认!");
        }
    }
}
//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryGrpAgent()
{
    if(fm.all('GrpAgentCode').value == "" && fm.all('GrpAgentIDNo').value == "" )
    {  
        var tGrpAgentCom = (fm.GrpAgentCom != null && fm.GrpAgentCom != "undefined") ? fm.GrpAgentCom.value : "";
        var tGrpAgentName = (fm.GrpAgentName != null && fm.GrpAgentName != "undefined") ? fm.GrpAgentName.value : "";
        var tGrpAgentIDNo = (fm.GrpAgentIDNo != null && fm.GrpAgentIDNo != "undefined") ? fm.GrpAgentIDNo.value : "";
        var strURL = "../sys/GrpAgentCommonQueryMain.jsp?GrpAgentCom=" + tGrpAgentCom +
                     "&GrpAgentName=" + tGrpAgentName + "&GrpAgentIDNo=" + tGrpAgentIDNo;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCode').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Sales_Cod='" + fm.all('GrpAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.GrpAgentCode.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }else if(fm.all('GrpAgentIDNo').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Id_No='" + fm.all('GrpAgentIDNo').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("身份证号码为:[" +  fm.GrpAgentIDNo.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}

//问题件录入
function QuestInput()
{
	cContNo = fm.ContNo.value;  //保单号码
	if(cContNo == "")
	{
		alert("尚无该投保单，请先保存!");
	}
	else
	{	
        var tStrUrl = "../uw/ProblemInputMain.jsp?AppFlag=1"
            + "&ContNo=" + cContNo
            + "&Flag=" + LoadFlag
            + "&MissionID=" + tMissionID
            + "&SubMissionID=" + tSubMissionID
            ;
		window.open(tStrUrl, "window1");
	}
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery1(arrQueryResult)
{     
      //fm.all("Donextbutton1").style.display="";
      //fm.all("Donextbutton2").style.display="none";
      //fm.all("Donextbutton3").style.display="";
      //fm.all("butBack").style.display="none";
      //详细信息初始化
      detailInit(arrQueryResult); 
    
}
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];//fm.all('AgentCodeName').value = easyExecSql("select name from laagent where agentcode='"+arrSelected[0][1]+"'");
  	fm.AgentGroup.value = arrResult[0][1];//fm.all('AgentGroupName').value = easyExecSql("select name from labranchgroup where  AgentGroup='"+arrResult[0][1]+"'");
    fm.GroupAgentCode.value = arrResult[0][95];
  }
}
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery3(arrResult)
{  
  if(arrResult!=null)
    {  	
  	fm.GrpAgentCode.value = arrResult[0][0];
  	fm.GrpAgentName.value = arrResult[0][1];
    fm.GrpAgentIDNo.value = arrResult[0][2];
  }
}

function queryAgent2()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	if(fm.all('GrpAgentCode').value != "" && fm.all('GrpAgentCode').value.length==10 )	 {
	var cAgentCode = fm.GrpAgentCode.value;  //保单号码	
	var strSql = "select GrpAgentCode,Name,AgentGroup,agentcode from LAAgent where GrpAgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentGroup.value = arrResult[0][2];
      fm.AgentCode.value = arrResult[0][3];
      alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     fm.AgentCode.value ="";
     alert("代码为:["+fm.all('GrpAgentCode').value+"]的业务员不存在，请确认!");
     }
	}	
}                                                                             
function getdetail()
{
var strSql = "select BankCode,AccName from LCAccount where BankAccNo='" + fm.AppntBankAccNo.value+"'";
var arrResult = easyExecSql(strSql);
if (arrResult != null) {
      fm.AppntBankCode.value = arrResult[0][0];
      fm.AppntAccName.value = arrResult[0][1];
    }
	
}	                                                                             
function getdetailwork()
{
var strSql = "select OccupationType from LDOccupation where OccupationCode='" + fm.AppntOccupationCode.value+"'";
var arrResult = easyExecSql(strSql);
if (arrResult != null) {
      fm.AppntOccupationType.value = arrResult[0][0];

    }
else{  
     fm.AppntOccupationType.value = '';
}	
}                                                                             
function getdetailaddress(){
 	var strSQL="select b.* from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.AppntNo.value+"'";
        arrResult=easyExecSql(strSQL);                         
        
        var sql1 = "select codename from ldcode1 where codetype='province1' and code='"+arrResult[0][28]+"' and code1='0' ";
        var sql2 = "select codename from ldcode1 where codetype='city1' and code='"+arrResult[0][29]+"' and code1='"+arrResult[0][28]+"' ";
        var sql3 = "select codename from ldcode1 where codetype='county1' and code='"+arrResult[0][30]+"' and code1='"+arrResult[0][29]+"' ";
        var arrResult1=easyExecSql(sql1);                         
        var arrResult2=easyExecSql(sql2);                         
        var arrResult3=easyExecSql(sql3); 
   try{fm.all('AppntNo').value= arrResult[0][0];}catch(ex){};         
   try{fm.all('AppntAddressNo').value= arrResult[0][1];}catch(ex){};          
   try{fm.all('AppntPostalAddress').value= arrResult[0][2];}catch(ex){};     
   try{fm.all('AppntZipCode').value= arrResult[0][3];}catch(ex){};            
   try{fm.all('AppntPhone').value= arrResult[0][4];}catch(ex){};              
   try{fm.all('AppntFax').value= arrResult[0][5];}catch(ex){};                
   try{fm.all('AppntHomeAddress').value= arrResult[0][6];}catch(ex){};        
   try{fm.all('AppntHomeZipCode').value= arrResult[0][7];}catch(ex){};        
   try{fm.all('AppntHomePhone').value= arrResult[0][8];}catch(ex){};          
   try{fm.all('AppntHomeFax').value= arrResult[0][9];}catch(ex){};            
   try{fm.all('CompanyAddress').value= arrResult[0][10];}catch(ex){};     
   try{fm.all('AppntGrpZipCode').value= arrResult[0][11];}catch(ex){};     
   try{fm.all('AppntGrpPhone').value= arrResult[0][12];}catch(ex){};       
   try{fm.all('AppntGrpFax').value= arrResult[0][13];}catch(ex){};         
   try{fm.all('AppntMobile').value= arrResult[0][14];}catch(ex){};             
   try{fm.all('AppntMobileChs').value= arrResult[0][15];}catch(ex){};          
   try{fm.all('AppntEMail').value= arrResult[0][16];}catch(ex){};              
   try{fm.all('AppntBP').value= arrResult[0][17];}catch(ex){};                 
   try{fm.all('AppntMobile2').value= arrResult[0][18];}catch(ex){};            
   try{fm.all('AppntMobileChs2').value= arrResult[0][19];}catch(ex){};         
   try{fm.all('AppntEMail2').value= arrResult[0][20];}catch(ex){};             
   try{fm.all('AppntBP2').value= arrResult[0][21];}catch(ex){}; 
   try{fm.all('AppntGrpName').value= arrResult[0][27];}catch(ex){}; 
   
   if(arrResult1 != null){
	   
	   try{fm.all('Province').value= arrResult[0][28];}catch(ex){};
	   try{fm.all('City').value= arrResult[0][29];}catch(ex){};
	   try{fm.all('County').value= arrResult[0][30];}catch(ex){};
	   try{fm.all('appnt_PostalProvince').value= arrResult1;}catch(ex){};
	   try{fm.all('appnt_PostalCity').value= arrResult2;}catch(ex){};
	   try{fm.all('appnt_PostalCounty').value= arrResult3;}catch(ex){};
   }else {

	  var arrResult4 = easyExecSql("select code from ldcode1 where codetype = 'province1' and codename ='"+arrResult[0][28]+"'");
	  var arrResult5 = easyExecSql("select code from ldcode1 where codetype = 'city1' and codename ='"+arrResult[0][29]+"'");
	  var arrResult6 = easyExecSql("select code from ldcode1 where codetype = 'county1' and codename ='"+arrResult[0][30]+"'");
	  
	  try{fm.all('Province').value= arrResult4;}catch(ex){};
	  try{fm.all('City').value= arrResult5;}catch(ex){};
	  try{fm.all('County').value= arrResult6;}catch(ex){};
	  try{fm.all('appnt_PostalProvince').value= arrResult[0][28];}catch(ex){};
	  try{fm.all('appnt_PostalCity').value= arrResult[0][29];}catch(ex){};
	  try{fm.all('appnt_PostalCounty').value= arrResult[0][30];}catch(ex){};
   }
   try{fm.all('appnt_PostalStreet').value= arrResult[0][31];}catch(ex){};
   try{fm.all('appnt_PostalCommunity').value= arrResult[0][32];}catch(ex){};
   try{fm.all('AppntHomeCode').value= arrResult[0][33];}catch(ex){};
   try{fm.all('AppntHomeNumber').value= arrResult[0][34];}catch(ex){};
}
function getaddresscodedata()
{
    var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;
    var strsql = "";
    var tCodeData = "0|";
    //strsql = "select AddressNo,PostalAddress from LCAddress where CustomerNo ='"+fm.AppntNo.value+"'";
    strsql = "select max(int(AddressNo)) from LCAddress where CustomerNo ='"+fm.AppntNo.value+"'";
    //alert("strsql :" + strsql);
    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
    if (turnPage.strQueryResult != "")
    {
    	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	m = turnPage.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		//tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    		tCodeData = turnPage.arrDataCacheSet[i][0];
    	}
    }
    //alert ("tcodedata : " + tCodeData);
    //return tCodeData;
    //fm.all("AddressNo").CodeData=tCodeData;
    fm.all("AddressNo").value=tCodeData;
    afterCodeSelect("GetAddressNo","" );
} 
function afterCodeSelect( cCodeName, Field ,InputObj)
{
	
	//关闭社保渠道勾选交叉销售功能（社保渠道不得选择交叉销售业务）
	if(cCodeName=="unitesalechnl"){
		fm.all("MixComFlag").style.display="";
		fm.MixComFlag.checked = false;
		if(fm.SaleChnl.value=="18"||fm.SaleChnl.value=="20"){
			fm.all("MixComFlag").style.display="none";
			fm.MixComFlag.checked = false;
		}
		isMixCom();
	}
	
	
	if(cCodeName=="GetAddressNo"){
 	var strSQL="select b.* from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.AppntNo.value+"'";
    arrResult=easyExecSql(strSQL);                         
try{fm.all('AppntNo').value= arrResult[0][0];}catch(ex){};         
try{fm.all('AppntAddressNo').value= arrResult[0][1];}catch(ex){};          
try{fm.all('AppntPostalAddress').value= arrResult[0][2];}catch(ex){};     
try{fm.all('AppntZipCode').value= arrResult[0][3];}catch(ex){};            
try{fm.all('AppntPhone').value= arrResult[0][4];}catch(ex){};              
try{fm.all('AppntFax').value= arrResult[0][5];}catch(ex){};                
try{fm.all('AppntHomeAddress').value= arrResult[0][6];}catch(ex){};        
try{fm.all('AppntHomeZipCode').value= arrResult[0][7];}catch(ex){};        
try{fm.all('AppntHomePhone').value= arrResult[0][8];}catch(ex){};          
try{fm.all('AppntHomeFax').value= arrResult[0][9];}catch(ex){};            
try{fm.all('CompanyAddress').value= arrResult[0][10];}catch(ex){};     
try{fm.all('AppntGrpZipCode').value= arrResult[0][11];}catch(ex){};     
try{fm.all('AppntGrpPhone').value= arrResult[0][12];}catch(ex){};       
try{fm.all('AppntGrpFax').value= arrResult[0][13];}catch(ex){};        
try{fm.all('AppntMobile').value= arrResult[0][14];}catch(ex){};             
try{fm.all('AppntMobileChs').value= arrResult[0][15];}catch(ex){};          
try{fm.all('AppntEMail').value= arrResult[0][16];}catch(ex){};              
try{fm.all('AppntBP').value= arrResult[0][17];}catch(ex){};                 
try{fm.all('AppntMobile2').value= arrResult[0][18];}catch(ex){};            
try{fm.all('AppntMobileChs2').value= arrResult[0][19];}catch(ex){};         
try{fm.all('AppntEMail2').value= arrResult[0][20];}catch(ex){};             
try{fm.all('AppntBP2').value= arrResult[0][21];}catch(ex){};   
try{fm.all('AppntGrpName').value= arrResult[0][27];}catch(ex){};
try{fm.all('Province').value= arrResult[0][28];}catch(ex){};
try{fm.all('City').value= arrResult[0][29];}catch(ex){};
try{fm.all('County').value= arrResult[0][30];}catch(ex){};
try{fm.all('appnt_PostalStreet').value= arrResult[0][31];}catch(ex){};
try{fm.all('appnt_PostalCommunity').value= arrResult[0][32];}catch(ex){};
try{fm.all('AppntHomeCode').value= arrResult[0][33];}catch(ex){}; 
try{fm.all('AppntHomeNumber').value= arrResult[0][34];}catch(ex){};           
}
if( cCodeName == "CheckPostalAddress")
{ 
 if(fm.CheckPostalAddress.value=="1") 
 {
 	fm.all('AppntPostalAddress').value=fm.all('CompanyAddress').value;
            fm.all('AppntZipCode').value=fm.all('AppntGrpZipCode').value;
//            fm.all('AppntPhone').value= fm.all('AppntGrpPhone').value;
            fm.all('AppntFax').value= fm.all('AppntGrpFax').value;

 }      
 else if(fm.CheckPostalAddress.value=="2") 
 {      
 	fm.all('AppntPostalAddress').value=fm.all('AppntHomeAddress').value;
            fm.all('AppntZipCode').value=fm.all('AppntHomeZipCode').value;
//            fm.all('AppntPhone').value= fm.all('AppntHomePhone').value;
            fm.all('AppntFax').value= fm.all('AppntHomeFax').value;
 }
 else if(fm.CheckPostalAddress.value=="3") 
 {
 	fm.all('AppntPostalAddress').value="";
            fm.all('AppntZipCode').value="";
//            fm.all('AppntPhone').value= "";
            fm.all('AppntFax').value= "";
 } 
 
}	 	
if(cCodeName=="OccupationCode")
{
	//alert();
	var t = Field.value;
	var strSql = "select Occupationtype from ldOccupation where Occupationcode='"+t+"'";
	//alert(strSql);
	var arr = easyExecSql(strSql);
	if( arr )
	{
		fm.AppntOccupationType.value = arr[0][0];
	}
} 
if(cCodeName=="PayMode")
{
    //if(fm.PayMode.value=="1" || fm.PayMode.value=="4"||fm.PayMode.value=="6") 
    //{   
        fm.all('AppntBankCode').style.display="";
        fm.all('AppntBankCodeName').style.display="";
        fm.all('AppntAccName').style.display="";
        //fm.all('AppntAccName').readOnly=false;
        fm.all('AppntBankAccNo').style.display="";
    //}
 
    // 加入对缴费凭证号的控制。
    // 可录入原则：缴费方式为（1：现金；11：银行代扣）
    //initPayMode();
}
else if(cCodeName=="unitesalechnl")
{
  if(Field.value == "04" )
  {
    fm.all("AgentComTitleID").style.display = "";
    fm.all("AgentComInputID").style.display = "";
    fm.all("AgentSaleCodeID").style.display = "";
  }
  else
  {
    fm.all("AgentComTitleID").style.display = "none";
    fm.all("AgentComInputID").style.display = "none";
    fm.all("AgentSaleCodeID").style.display = "none";
  }
  
  if(Field.value == "03" || Field.value == "11" || Field.value == "12"){
    fm.all("AgentComTitleID1").style.display = "";
    fm.all("AgentComInputID1").style.display = "";
    fm.all("AgentSaleCodeID").style.display = "";
    var tBranchType="2";
    var tBranchType2="";
    if (Field.value == "11"){
    	tBranchType2="04";	    	
    }else if (Field.value == "12"){
    	tBranchType2="04";
    }else if (Field.value == "03"){
        tBranchType2="02";
    }
    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
  }else if(Field.value == "15"){//by gzh 20140619 增加销售渠道互动中介
  	fm.all("AgentComTitleID1").style.display = "";
    fm.all("AgentComInputID1").style.display = "";
  	var tBranchType="5";
    var tBranchType2="01";
    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
    fm.all("AgentSaleCodeID").style.display = "";
  }else if(Field.value == "20"){
	  	fm.all("AgentComTitleID1").style.display = "";
	    fm.all("AgentComInputID1").style.display = "";
	  	var tBranchType="6";
	    var tBranchType2="02";
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
	    fm.all("AgentSaleCodeID").style.display = "";
  }else{
  	//加入个险中介的判断
  	if(Field.value == "10"){
  		fm.all("AgentComTitleID1").style.display = "";
	    fm.all("AgentComInputID1").style.display = "";
	    var tBranchType="#1";
	    var tBranchType2="02#,#04";
	    fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
	    fm.all("AgentSaleCodeID").style.display = "";
	  	//------------------------------------------------------------
	}else if(Field.value == "04"){
	    fm.all("AgentComTitleID1").style.display = "none";
	    fm.all("AgentComInputID1").style.display = "none";
	}else{
	    fm.all("AgentComTitleID1").style.display = "none";
	    fm.all("AgentComInputID1").style.display = "none";
	    fm.all("AgentSaleCodeID").style.display = "none";
	}
  }

  fm.AgentCom.value = "";
  //fm.AgentCode.value = "";
  fm.AgentCom1.value = "";
  fm.GrpAgentCom.value = "";
  fm.GrpAgentCode.value = "";
  fm.GrpAgentName.value = "";
  fm.AgentSaleCode.value = "";
  fm.AgentSaleName.value = "";
}

if(cCodeName=="NativePlace"&&InputObj.name=="AppntNativePlace"){
   if(Field.value == "OS"){
      fm.all("NativePlace").style.display = "";
      fm.all("NativeCity").style.display = "";
      fm.all("NativeCityTitle").innerHTML="国家";
   }else if(Field.value == "HK"){
      fm.all("NativePlace").style.display = "";
      fm.all("NativeCity").style.display = "";
      fm.all("NativeCityTitle").innerHTML="地区";
   }else{
      fm.all("NativePlace").style.display = "none";
      fm.all("NativeCity").style.display = "none";
   }
  fm.AppntNativeCity.value = ""; 
  fm.AppntNativeCityName.value = ""; 
}

if(cCodeName=="NativePlace"&&InputObj.name=="BPOInsuredNativePlace"){
   if(Field.value == "OS"){
      fm.all("NativePlaces").style.display = "";
      fm.all("NativeCitys").style.display = "";
      fm.all("NativeCityTitles").innerHTML="国家";
   }else if(Field.value == "HK"){
      fm.all("NativePlaces").style.display = "";
      fm.all("NativeCitys").style.display = "";
      fm.all("NativeCityTitles").innerHTML="地区";
   }else{
      fm.all("NativePlaces").style.display = "none";
      fm.all("NativeCitys").style.display = "none";
   }
  fm.BPOInsuredNativeCity.value = ""; 
  fm.BPOInsuredNativeCityName.value = ""; 
}	if(cCodeName=="taxpayertype"){
	if(fm.TaxFlag.value=="1"&&Field.value == "01"){
		   fm.all('GTaxNo').readOnly=false;
	       fm.all('GOrgancomCode').readOnly=false;
	}else{
		  fm.all('GTaxNo').value="";
	       fm.all('GOrgancomCode').value="";
		  fm.all('GTaxNo').readOnly=true;
	       fm.all('GOrgancomCode').readOnly=true;
	}
	}	if(cCodeName=="taxflag"&&Field.value == "2"&&(fm.GrpNo.value==""||fm.GrpNo.value==null)){
		 fm.all('GTaxNo').value="";
	       fm.all('GOrgancomCode').value="";
		  fm.all('GTaxNo').readOnly=true;
	       fm.all('GOrgancomCode').readOnly=true;
	}
	
	if(cCodeName=="DiscountMode"){
		if(Field.value == "1"){
			fm.all('AppointDiscountID').style.display = "none";
			fm.all('AppointDiscountID2').style.display = "none";
		    fm.all('Supdiscountfactor').readOnly=false;
		    fm.all('Grpdiscountfactor').readOnly=false;
			fm.all('discountID').style.display = "";
			fm.all('Grpdiscountfactor').value="";
			fm.all('Supdiscountfactor').value="";
			fm.all('Agediscountfactor').value="";
			fm.all('Totaldiscountfactor').value="";
			fm.all('AppointDiscount').value="";
		}else if(Field.value == "2"){ 
			fm.all('AppointDiscountID').style.display = "";
			fm.all('AppointDiscountID2').style.display = "";
		    fm.all('Supdiscountfactor').readOnly=true;
		    fm.all('Grpdiscountfactor').readOnly=true;
			
			fm.all('Grpdiscountfactor').value="";
			fm.all('Supdiscountfactor').value="";
			fm.all('Agediscountfactor').value="";
			fm.all('Totaldiscountfactor').value="";
			fm.all('AppointDiscount').value="";
		}else if(Field.value == "0"){ 
			fm.all('AppointDiscountID').style.display = "none";
			fm.all('AppointDiscountID2').style.display = "none";
		    fm.all('Supdiscountfactor').readOnly=true;
		    fm.all('Grpdiscountfactor').readOnly=true;
			fm.all('discountID').style.display = "";
			fm.all('Grpdiscountfactor').value="1";
			fm.all('Supdiscountfactor').value="1";
			fm.all('Agediscountfactor').value="1";
			fm.all('Totaldiscountfactor').value="1";	
		}
	}
	
	}                                                                
                                                                             
/*********************************************************************
 *  初始化工作流MissionID
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function initMissionID()
{
	if(tMissionID == "null" || tSubMissionID == "null")
	{
	  tMissionID = mSwitch.getVar('MissionID');
	  tSubMissionID = mSwitch.getVar('SubMissionID');
	}
	else
	{
	  mSwitch.deleteVar("MissionID");
	  mSwitch.deleteVar("SubMissionID");
	  mSwitch.addVar("MissionID", "", tMissionID);
	  mSwitch.addVar("SubMissionID", "", tSubMissionID);
	  mSwitch.updateVar("MissionID", "", tMissionID);
	  mSwitch.updateVar("SubMissionID", "", tSubMissionID);
	}
}   
function FillPostalAddress()
{
	 if(fm.CheckPostalAddress.value=="1") 
	 {
	 	fm.all('AppntPostalAddress').value=fm.all('CompanyAddress').value;
                fm.all('AppntZipCode').value=fm.all('AppntGrpZipCode').value;
                fm.all('AppntPhone').value= fm.all('AppntGrpPhone').value;
                fm.all('AppntFax').value= fm.all('AppntGrpFax').value;

	 }      
	 else if(fm.CheckPostalAddress.value=="2") 
	 {      
	 	fm.all('AppntPostalAddress').value=fm.all('AppntHomeAddress').value;
                fm.all('AppntZipCode').value=fm.all('AppntHomeZipCode').value;
                fm.all('AppntPhone').value= fm.all('AppntHomePhone').value;
                fm.all('AppntFax').value= fm.all('AppntHomeFax').value;
	 }
	 else if(fm.CheckPostalAddress.value=="3") 
	 {
	 	fm.all('AppntPostalAddress').value="";
                fm.all('AppntZipCode').value="";
                fm.all('AppntPhone').value= "";
                fm.all('AppntFax').value= "";
	 }
}                                                                                                                                                   
function AppntChk()
{
	var Sex=fm.AppntSex.value;
	var i=Sex.indexOf("-");
	Sex=Sex.substring(0,i);
        var sqlstr="select *from ldperson where Name='"+fm.AppntName.value+"' and Sex='"+fm.AppntSex.value+"' and Birthday='"+fm.AppntBirthday.value+"' and CustomerNo<>'"+fm.AppntNo.value+"'";
        //alert(sqlstr);        
        arrResult = easyExecSql(sqlstr,1,0);
        if(arrResult==null)
        {
	  alert("没有与该投保人相似的客户,无需校验");
	  return false;
        }	
	window.open("../uw/AppntChkMain.jsp?ProposalNo1="+fm.ContNo.value+"&Flag=A","window1");
}                                                                             
 
 
/*********************************************************************
 *  根据身份证号取得出生日期和性别
 *  参数  ：  身份证号
 *  返回值：  无
 *********************************************************************
 */

function getBirthdaySexByIDNo(iIdNo)
{
	if(fm.all('AppntIDType').value=="0")
	{
		fm.all('AppntBirthday').value=getBirthdatByIdNo(iIdNo);
		fm.all('AppntSex').value=getSexByIDNo(iIdNo);
	}	
	if(fm.all('AppntIDType').value=="5")
	{
		fm.all('AppntBirthday').value=getBirthdatByIdNo(iIdNo);
		fm.all('AppntSex').value=getSexByIDNo(iIdNo);
	}	
}  

/*********************************************************************
 *  验证身份证号、生日＆性别
 *  参数  ：  
 *  返回值：  无
 *********************************************************************
 */
function BirthdaySexAppntIDNo()
{
	if(fm.all('AppntIDType').value=="0")
	{
		if(fm.all('AppntBirthday').value!=getBirthdatByIdNo(fm.all('AppntIDNo').value) || fm.all('AppntSex').value!=getSexByIDNo(fm.all('AppntIDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
	if(fm.all('AppntIDType').value=="5")
	{
		if(fm.all('AppntBirthday').value!=getBirthdatByIdNo(fm.all('AppntIDNo').value) || fm.all('AppntSex').value!=getSexByIDNo(fm.all('AppntIDNo').value))
		{
			alert("生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
}


function checkidtype()
{
	if(fm.AppntIDType.value=="")
	{
		alert("请选择证件的类型");
		fm.AppntIDNo.value="";
        }
}                                                                    
function getCompanyCode()
{
    var strsql = "";
    var tCodeData = "";
    strsql = "select CustomerNo,GrpName from LDgrp ";
    fm.all("AppntGrpNo").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}                                                                            
//复合险种信息
function showComplexRiskInfo()
{
	window.open("./ComplexRiskMain.jsp?ContNo="+ContNo+"&prtNo="+fm.PrtNo.value,"", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}




//外包反馈错误信息 2007-9-27 20:21
function findIssue()
{
	window.open("./BPOIssueInput.jsp?prtNo="+prtNo,"window1");
}

function checkContPrem()
{
	// 在点击复核完毕时，进行整单录入保费与系统计算保费进行校验。
    var tContNo = fm.ContNo.value;
    var strSql = "select (select PremScope from LCCont where ContNo = '" + tContNo + "') PremScope, Sum(prem) Prem "
        + " from LCPol where ContNo ='" + tContNo + "' "
        + " with ur ";
    var tResult = easyExecSql(strSql);
    if(!tResult)
    {
        alert('未查询出合同保费相关信息。');
        return false;
    }
    var tPremScope = tResult[0][0];
    var tPrem = tResult[0][1];
    if(tPremScope != tPrem)
    {
        var tResultComment = "整单录入的保费，与系统计算出的保费不符。"
            + "整单录入的保费为：" + tPremScope
            + "；系统计算出的保费为：" + tPrem;
        return confirm(tResultComment);
    }
    return true;
    // ----------------------------------------------
}

//销售渠道和业务员的校验
function checkSaleChnl()
{
    //业务员离职校验
    var agentStateSql = "select 1 from LAAgent where AgentCode = '" 
        + fm.AgentCode.value + "' and AgentState < '06'";
    var arr = easyExecSql(agentStateSql);
    if(!arr){
        alert("该业务员已离职！");
        return;
    }
    //业务员和销售渠道的校验
    var agentCodeSql = "select 1 from LAAgent a, LDCode1 b where a.AgentCode = '" 
        + fm.AgentCode.value
        + "' and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
        + "and b.CodeType = 'salechnl' and b.Code = '" 
        + fm.SaleChnl.value + "' "
        + "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '10' = '" + fm.SaleChnl.value + "' and a.BranchType2 = '04'  "
		+ "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '03' = '" + fm.SaleChnl.value + "' and a.BranchType2 = '04'  "
		+ "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '02' = '" + fm.SaleChnl.value + "' and a.BranchType = '2' and a.BranchType2 = '02'  "
		;


    var arr = easyExecSql(agentCodeSql);
    if(!arr){
        alert("业务员和销售渠道不相符！");
        return;
    }
    
    
    return true;
}

/**
 * 加入根据PayMode对缴费凭证号的控制。
 * 可录入原则：缴费方式为（1：现金；11：银行代扣；12：银行代扣-导入）
 */
function initPayMode()
{
    var tPayMode = fm.all.PayMode.value;
    switch(tPayMode)
    {
        case "1":
            displayTempFeeNoInput(true);
            break;
        case "3":
            displayTempFeeNoInput(false);
            displayAccInfoInput(false);
            break;
        case "4":
            displayTempFeeNoInput(false);
            break;
        case "6":
            displayTempFeeNoInput(false);
            break;    
        case "11":
            displayTempFeeNoInput(true);
            displayAccInfoInput(true);
            break;
        case "12":
            displayTempFeeNoInput(true);
            displayAccInfoInput(true);
            break;
    }
    // ------------------------------------------
}

/**
 * 展现银行帐户信息。
 */
function displayAccInfoInput(isDisplay)
{
    displayInput("AppntBankCode", isDisplay, true);
    displayInput("AppntBankCodeName", isDisplay, true);
    displayInput("AppntAccName", isDisplay, true);
    displayInput("AppntBankAccNo", isDisplay, true);
}

/**
 * 展现暂缴收据号信息。
 */
function displayTempFeeNoInput(isDisplay)
{
    displayInput("TempFeeNo", isDisplay, true);
}

/**
 * 校验所填入缴费凭证号的，到帐情况
 */
function checkEnterAccOfTempFee()
{
    var tContNo = fm.ContNo.value;
    var tResult = easyExecSql("select PayMode, TempFeeNo from LCCont lcc where lcc.ContNo = '" + tContNo + "'");
    if(!tResult)
    {
        alert('未查询出该保单对应缴费相关信息。');
        return false;
    }
    
    var tPayMode = tResult[0][0];
    var tTempFeeNo = tResult[0][1];
    
    // 只对缴费方式为：1-现金；11-银行代收，进行校验。
    if(tPayMode != "1" && tPayMode != "11" && tPayMode != "12")
        return true;

    // 暂缴号为空或不填，不进行校验。
    if(tTempFeeNo == null || tTempFeeNo == "")
        return true;
        
    var tStrSql = "select nvl(sum(PayMoney), 0) "
	   + " from LJTempFeeClass ljtfc "
	   + " where ljtfc.EnterAccDate is not null and ljtfc.ConfMakeDate is not null "
       + " and ConfDate is null "
	   + " and ljtfc.TempFeeNo = '" + tTempFeeNo + "' ";
       
    tResult = easyExecSql(tStrSql);
    if(!tResult)
    {
        alert('未查询出该暂缴号对应保费相关信息。');
        return false;
    }
    var tEnterAccMoney = tResult[0][0];
    
    tResult = easyExecSql("select nvl(sum(Prem), 0) from LCPol lcp where lcp.ContNo = '" + tContNo + "' ");
    if(!tResult)
    {
        alert('未查询出该保单对应保费相关信息。');
        return false;
    }
    var tContPrem = tResult[0][0];
    
    var tComment = "该单为先收费保单:\n"
        + "缴费方式为：\t" + tPayMode + "\n"
        + "暂缴收据号为：\t" + tTempFeeNo + "\n"
        + "目前到帐保费为：" + tEnterAccMoney + "\n"
        + "该单系统保费为：" + tContPrem;
    return confirm(tComment);
}

function controlAgentComDisplay(displayFlag)
{
  if(fm.SaleChnl.value == "03" || fm.SaleChnl.value == "04")
  {
    fm.all("AgentComTitleID").style.display = displayFlag;
	  fm.all("AgentComInputID").style.display = displayFlag;
  }else if (fm.SaleChnl.value == "11" || fm.SaleChnl.value == "12" || fm.SaleChnl.value == "20") {
  	fm.all("AgentComTitleID1").style.display = displayFlag;
  	fm.all("AgentComInputID1").style.display = displayFlag;
  }else if (fm.SaleChnl.value == "10"){
  	fm.all("AgentComTitleID1").style.display = displayFlag;
  	fm.all("AgentComInputID1").style.display = displayFlag;
  }else if (fm.SaleChnl.value == "15"){
  	fm.all("AgentComTitleID1").style.display = displayFlag;
  	fm.all("AgentComInputID1").style.display = displayFlag;
  }
  initAgentCom();
}

//销售渠道为中介时中介机构的校验
function checkAgentCom()
{
	var saleChannel = fm.SaleChnl.value;
	var agentCom = fm.AgentCom.value;
	
	if(saleChannel != null && (saleChannel == "03" || saleChannel == "10" || saleChannel == "15"|| saleChannel == "04"|| saleChannel == "20"))
	{
	    if(agentCom == null || agentCom == "")
	    {
	        alert("中介机构为空，请填写！");
	        return false;
	    }
	    else
	    {
            var result = easyExecSql("select 1 from LAComToAgent where AgentCom = '" + agentCom + "' and AgentCode = '" + fm.AgentCode.value + "'");
            if(!result)
            {
                alert("中介机构和业务员不匹配！");
                return false;
            }
	    }
	}
	return true;
}

//校验银行信息
function checkBankInfo()
{
    //查询和随动定制不需要校验银行信息
    if(LoadFlag == "6" || LoadFlag == "99")
    {
        return true;
    }
    if(fm.all('PayMode').value=="4"||fm.all('PayMode').value=="6"||fm.all('ExPayMode').value=="4"||fm.all('PayMode').value=="8")
    {
        if(fm.all('AppntBankCode').value==null||fm.all('AppntBankCode').value=="")
        {
            alert("请录入开户银行！");
            fm.all('AppntBankCode').focus;
            return false;
        }
        if(fm.all('AppntAccName').value==null||fm.all('AppntAccName').value=="")
        {
            alert("请录入户名！");
            fm.all('AppntAccName').focus;
            return false;
        }
        if(fm.all('AppntBankAccNo').value==null||fm.all('AppntBankAccNo').value=="")
        {
            alert("请录入帐号！");
            fm.all('AppntBankAccNo').focus;
            return false;
        }
        var result = easyExecSql("select 1 from LCCont where PrtNo = '" + fm.PrtNo.value 
            + "' and PayMode in( '4' ,'6','8')and (BankCode is null or AccName is null or BankAccNo is null)");
        if(result)
        {
            alert("请先保存缴费信息！");
            return false;
        }
    }
    return true;
}

/**
 * 对万能类险种进行特殊处理。
 */
function initULIRiskInput()
{
    if(hasULIRisk(ContNo))
    {
        fm.all("CValiDateInput").style.display = "none";
    }
}

/**
 * 校验万能类险种缴费年期。
 */
function checkULIRisk()
{
    if(!hasULIRisk(ContNo))
    {
        return true;
    }
    
    var tStrSql = ""
        + " select lcp.InsuredAppAge,lcp.PayEndYearFlag,lcp.PayEndYear, lcp.InSuYearFlag, lcp.InSuYear from LCPol lcp "
        + " where lcp.ContNo = '" + ContNo + "' "
        + " and lcp.PayIntv != 0 and lcp.PolNo = lcp.MainPolNo "
        ;
    var tResult = easyExecSql(tStrSql);
    var tFlag = true;
    if(tResult)
    {
        for(i = 0, n = tResult.length; i < n; i++)
        {
        	var tPayYears;// 缴费期间
        	var tInsuYears;// 保障期间
        	
            var tInsuredAppAge = tResult[i][0];
            
            // 处理缴费期间
            var tPayEndYearFlag = tResult[i][1];
            var tPayEndYear = tResult[i][2];
            
            if(tPayEndYearFlag == 'A')
            {
            	tPayYears = tPayEndYear - tInsuredAppAge;
            }
            else if(tPayEndYearFlag == 'Y')
            {
            	tPayYears = tPayEndYear;
            }
            else
            {
            	continue;
            }
            // --------------------
            //处理保障期间
            var tInSuYearFlag = tResult[i][3];
            var tInSuYear = tResult[i][4];
            
             if(tInSuYearFlag == 'A')
            {
            	tInsuYears = tInSuYear - tInsuredAppAge;
            }
            else if(tInSuYearFlag == 'Y')
            {
            	tInsuYears = tInSuYear;
            }
            else
            {
            	continue;
            }
            
            
            // 缴费年期要小于被保年期
            if(tPayYears >tInsuYears)
            {
                alert("万能类险种，缴费年期要小于被保年期。");
                tFlag = false;
                break;
            }
        }
    }
    return tFlag;
}

/**
 * 校验万能类险种，只能存在一个被保险人。
 */
function checkULIInsured()
{
    if(!hasULIRisk(ContNo))
    {
        return true;
    }
    
    var tStrSql = ""
        + " select Count(1) "
        + " from LCInsured lci "
        + " where lci.ContNo = '" + ContNo + "' "
        ;
    var tResult = easyExecSql(tStrSql);
    var tFlag = true;
    if(tResult)
    {
        if(tResult[0][0] > 1)
        {
            alert("万能类险种，不能存在多被保人。");
            tFlag = false;
        }
    }
    return tFlag;
}

/**
 * 是否为万能类险种。
 */
function hasULIRisk(cContNo)
{
    var tStrSql = ""
        + " select 1 from LCPol lcp "
        + " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
        + " where lmra.RiskType4 = '4' " 
        + " and lcp.ContNo = '" + cContNo + "' "
        ;
    var tResult = easyExecSql(tStrSql);
    if(tResult)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * 问题件回销
 */
function questBack()
{
    var tContNo = fm.ContNo.value;
    var tStrSql = "select 1 from lcissuepol where proposalcontno = '" + tContNo + "' ";
    var tResult = easyExecSql(tStrSql);
    if(!tResult)
    {
        alert("未录入问题件!");
        return false;
    }

    var tLoadFlag = LoadFlag;
    var tFlag = "1";

    if(tContNo != "")
    {
        var tUrl = "../uw/UWManuProblemQMain.jsp"
            + "?ContNo=" + tContNo
            + "&Flag=" + tFlag
            + "&LoadFlag=" + tLoadFlag
            ;
        window.open(tUrl, "window2");
    }
    else
    {
        alert("请先选择保单!");
    }
}

//险种的校验
function checkRiskInfo()
{
    //该分公司险种是否停售校验
    var riskEnd = "select 1 from LCPol a, LMRiskApp b, LDCode c where a.PrtNo = '" 
        + fm.PrtNo.value + "' and b.RiskType4 = '4' and c.CodeType = 'UniversalSale' "
        + "and a.RiskCode = b.RiskCode and c.Code = substr(a.ManageCom, 1, 4) ";
    var result = easyExecSql(riskEnd);
    if(result)
    {
        alert("该分公司万能险已停售！");
        return false;
    }
    
    //保险期间单位为空的校验
    var riskInsu = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and (InsuYearFlag is null or InsuYearFlag = '')";
    result = easyExecSql(riskInsu);
    if(result)
    {
        alert("保险期间单位为空，请修改！");
        return false;
    }
    
    //个人防癌套餐的校验
    var risk230501And331001 = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and RiskCode in ('230501', '331001')";
    result = easyExecSql(risk230501And331001);
    if(result)
    {
        var riskInfoSql = "select 1 from LCPol where PrtNo = '" + fm.PrtNo.value + "' "
            + "and RiskCode in ('230501', '331001') "
            + "and not (InsuYear in (10, 15, 20) and InsuYearFlag = 'Y' "
            + "and PayEndYear = InsuYear and PayEndYearFlag = InsuYearFlag "
            + "and PayIntv = 12)";
        result = easyExecSql(riskInfoSql);
        if(result)
        {
            alert("请检查缴费频次、保险期间和缴费期间！\n(1).保险期间必须为10年、15年、20年。"
                + "\n(2).缴费年期必须与保险期间一致。\n(3).缴费频次必须为年缴。");
            return false;
        }
    }
    return true;
}

/**
 * 校验销售渠道与产品关联
 */
function checkSaleChnlRisk()
{   
    var tPrtNo = fm.PrtNo.value;
    var saleChannel = null;
    
    var arr = easyExecSql("select salechnl from lccont where prtno = '" + tPrtNo + "' ");
    if(arr)
    {
        saleChannel = arr[0][0];
    }
    
    if(saleChannel != null && saleChannel == "13")
    {
        var tStrSql = " select 1 "
            + " from LCPol lcp "
            + " left join LDCode1 ldc1 on lcp.SaleChnl = ldc1.Code and lcp.RiskCode = ldc1.code1 and ldc1.codetype = 'checksalechnlrisk' "
            + " where 1 = 1 "
            + " and ldc1.CodeType is null "
            + " and lcp.PrtNo = '" + tPrtNo + "' "
            ;
        var result = easyExecSql(tStrSql);
        
        if(result)
        {
            alert("该单为银代直销保单，但存在非银代直销产品，请核实。");
            return false;
        }
    }
    
    return true;
}

/**
 * 集团交叉要素校验
 */
function checkCrsBussParams()
{
    var tCrs_SaleChnl = trim(fm.Crs_SaleChnl.value);
    var tCrs_BussType = trim(fm.Crs_BussType.value);
   
    var tGrpAgentCom = trim(fm.GrpAgentCom.value);
    var tGrpAgentCode = trim(fm.GrpAgentCode.value);
    var tGrpAgentName = trim(fm.GrpAgentName.value);
    var tGrpAgentIDNo = trim(fm.GrpAgentIDNo.value);
    
    if(tCrs_SaleChnl != null && tCrs_SaleChnl != "")
    {
        if(tCrs_BussType == null || tCrs_BussType == "")
        {
            alert("选择集团交叉渠道时，集团交叉业务类型不能为空。");
            return false;
        }
        if(tGrpAgentCom == null || tGrpAgentCom == "")
        {
            alert("选择集团交叉渠道时，对方业务员机构不能为空。");
            return false;
        }
        if(tGrpAgentCode == null || tGrpAgentCode == "")
        {
            alert("选择集团交叉渠道时，对方业务员代码不能为空。");
            return false;
        }
        if(tGrpAgentName == null || tGrpAgentName == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名不能为空。");
            return false;
        }
        if(tGrpAgentIDNo == null || tGrpAgentIDNo == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名身份证不能为空。");
            return false;
        }
    }
    else
    {
        if(tCrs_BussType != null && tCrs_BussType != "")
        {
            alert("未选择集团交叉渠道时，集团交叉业务类型不能填写。");
            return false;
        }
        if(tGrpAgentCom != null && tGrpAgentCom != "")
        {
            alert("未选择集团交叉渠道时，对方业务员机构不能为填写。");
            return false;
        }
        if(tGrpAgentCode != null && tGrpAgentCode != "")
        {
            alert("未选择集团交叉渠道时，对方业务员代码不能为填写。");
            return false;
        }
        if(tGrpAgentName != null && tGrpAgentName != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名不能为填写。");
            return false;
        }
        if(tGrpAgentIDNo != null && tGrpAgentIDNo != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名身份证不能为填写。");
            return false;
        }
    }
    
    return true;
}

//显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
        
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value = "";
        fm.all('Crs_SaleChnlName').value = "";
        fm.all('Crs_BussType').value = "";
        fm.all('Crs_BussTypeName').value = "";
        fm.all('GrpAgentCom').value = "";
        fm.all('GrpAgentComName').value = "";
        fm.all('GrpAgentCode').value = "";
        fm.all('GrpAgentName').value = "";
        fm.all('GrpAgentIDNo').value = "";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}

//根据对方机构代码带出对方机构名称
function GetGrpAgentName()
{
	var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     fm.GrpAgentComName.value = "";
	}
}  
//根据机构代码显示机构名称
function getAgentName()
{
    var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     alert("对方机构代码有错误,请修改");
	     fm.GrpAgentCom.value = "";
	     return false;
	}
}

//校验被保人职业类别与职业代码
function checkOccTypeOccCode()
{
	var strSql = "select distinct OccupationType,OccupationCode from lcinsured "
				+ " where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSql);
	var i = 0;
	
	if(arrResult != null) {
		for(i = 0; i < arrResult.length; i++)
		{
			var OccupationType = arrResult[i][0];
			var OccupationCode = arrResult[i][1];
			
			if(OccupationType != null && OccupationType != "")
			{
				if(!CheckOccupationType(OccupationType))
				{
					return false;
				}
			}
			if(OccupationCode != null && OccupationCode != "")
			{
				if(!CheckOccupationCode(OccupationType,OccupationCode))
				{
					return false;
				}
			}
		}
	}
	return true;
}

//校验职业代码
function CheckOccupationCode(Type,Code)
{
	
	var strSql = "select 1 from ldoccupation where OccupationCode = '" + Code +"' and OccupationType = '" + Type + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别和职业代码与系统描述不一致，请查看！");
		return false;
	} 
	return true;
}

//校验职业类别
function CheckOccupationType(Type)
{
	var strSql = "select 1 from dual where '" + Type + "' in (select distinct occupationtype from ldoccupation)";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6");
		return false;
	} 
	return true;
}

//执行查询交叉销售对方机构代码 date 20101117 by gzh
function queryGrpAgentCom()
{
	if(fm.all('Crs_SaleChnl').value == "" || fm.all('Crs_SaleChnl').value == null)
	{
		alert("请先选择交叉销售渠道！！");
		return false;
	}
    if(fm.all('GrpAgentCom').value == "")
    {  
        var tCrs_SaleChnl =  fm.Crs_SaleChnl.value;
        var tCrs_SaleChnlName = fm.Crs_SaleChnlName.value;
        var strURL = "../sys/GrpAgentComQueryMain.jsp?Crs_SaleChnl="+tCrs_SaleChnl+"&Crs_SaleChnlName="+tCrs_SaleChnlName;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentComQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCom').value != "")
    {	
        var strGrpSql = "select grpagentcom,Under_Orgname  from lomixcom  where  grpagentcom ='"+fm.GrpAgentCom.value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery4(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("机构代码为:[" +  fm.GrpAgentCom.value + "]的机构不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery4(arrResult)
{  
  if(arrResult!=null)
    { 	
  	fm.GrpAgentCom.value = arrResult[0][0];
  	fm.GrpAgentComName.value = arrResult[0][1];
  }
}


//选择交叉销售后对交叉销售各不为空项的校验
    // by gzh 20101118
function MixComCheck()
{    
    if(fm.MixComFlag.checked == true)
    {
    	if(fm.Crs_SaleChnl.value == "" || fm.Crs_SaleChnl.value == null)
    	{
    		alert("选择交叉销售时，交叉销售渠道不能为空，请核查！");
    		fm.all('Crs_SaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.Crs_BussType.value == "" || fm.Crs_BussType.value == null)
    	{
    		alert("选择交叉销售时，交叉销售业务类型不能为空，请核查！");
    		fm.all('Crs_BussType').focus();
    		return false;
    	}
    	if(fm.GrpAgentCom.value == "" || fm.GrpAgentCom.value == null)
    	{
    		alert("选择交叉销售时，对方机构代码不能为空，请核查！");
    		fm.all('GrpAgentCom').focus();
    		return false;
    	}
    	/**
    	if(fm.GrpAgentComName.value == "" || fm.GrpAgentComName.value == null)
    	{
    		alert("选择交叉销售时，对方机构名称不能为空，请核查！");
    		fm.all('GrpAgentComName').focus();
    		return false;
    	}
    	*/
    	if(fm.GrpAgentCode.value == "" || fm.GrpAgentCode.value == null)
    	{
    		alert("选择交叉销售时，对方业务员代码不能为空，请核查！");
    		fm.all('GrpAgentCode').focus();
    		return false;
    	}
    	if(fm.GrpAgentName.value == "" || fm.GrpAgentName.value == null)
    	{
    		alert("选择交叉销售时，对方业务员姓名不能为空，请核查！");
    		fm.all('GrpAgentName').focus();
    		return false;
    	}
    	if(fm.GrpAgentIDNo.value == "" || fm.GrpAgentIDNo.value == null)
    	{
    		alert("选择交叉销售时，对方业务员身份证号码不能为空，请核查！");
    		fm.all('GrpAgentIDNo').focus();
    		return false;
    	}
      	return true;
    }else{
    	return true;
    }
}    
    // --------------------

//校验标准个险集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   add by 赵庆涛 2016-06-28
function checkCrsBussSaleChnl()
{
	if(fm.Crs_SaleChnl.value != null && fm.Crs_SaleChnl.value != "")
	{
		var strSQL = "select 1 from lccont lcc where lcc.ContType = '1' "  
			+ " and lcc.Crs_SaleChnl is not null " 
			+ " and lcc.Crs_BussType = '01' " 
			+ " and not (lcc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lcc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
			+ " and lcc.PrtNo = '" + fm.PrtNo.value + "' ";
		var arrResult = easyExecSql(strSQL);
        if (arrResult != null)
        {
        	alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
        	return false;
        }
	}
	return true;
}
//------------------------------

//初始化中介机构的显示
function initAgentCom()
{
	if(fm.SaleChnl.value != null && fm.SaleChnl.value != "")
	{
		if(fm.SaleChnl.value == "10")
		{
			var tBranchType="#1";
			var tBranchType2="02#,#04";
			fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";	
		}else if(fm.SaleChnl.value == "03")
		{
			var tBranchType="2";
			var tBranchType2="02";
			fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;	
		}else if(fm.SaleChnl.value == "15"){
		    var tBranchType="5";
	        var tBranchType2="01";
	        fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
		}
	}
}

//by gzh 20110402 录入完毕及复核完毕时，校验被保人告知第19项是否正确填写（即存在小于18周岁的未成年人，但还没选择被保人告知第19项）
function checkGZ19(){
	var strSql = "select insuredno,insuredappage from lcpol where prtno = '"+prtNo+"' and insuredappage<18";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		for(var i=0;i<arr.length;i++){
			var CustomerimpartSql = "select * from lccustomerimpart where prtno = '"+prtNo+"' and customerno = '"+arr[i][0]+"' and impartver = '001' and impartcode='250'";
			var arrCustomer = easyExecSql(CustomerimpartSql);
			if(arrCustomer==null){
				alert("被保人（"+arr[i][0]+"）年龄为"+arr[i][1]+"岁，请填写被保人告知第19项！");
				return false;
			}
		}
	}
	return true;
}

//by gzh 20110614 录入完毕及复核完毕时，校验被保人告知第19项是否正确填写（选择“是”时，保额必须填写）
function checkGZ19Prem(){
	var CustomerimpartSql = "select impartparammodle from lccustomerimpart where prtno = '"+prtNo+"' and impartver = '001' and impartcode='250'";
	var arrCustomer = easyExecSql(CustomerimpartSql);
	if(arrCustomer !=null){
		for(var i=0;i<arrCustomer.length;i++){
			var impartparammodle = arrCustomer[i][0].split(",");
			if(impartparammodle[0] == "Y" && (!isNumeric(impartparammodle[1]))){
				alert("被保人告知第19项,当选择“是”时，保额不能为空且必须是数字！");
				return false;
			}
		}
	}
	return true;
}

//校验保单的保险期间是否为负数     by zhangyang  2011-06-09
function checkInsuYear()
{
	var strSQL = "select Years from lcpol where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		for(var i = 0; i < arrResult.length; i++)
		{
			var tYears = arrResult[i][0];
			if(tYears != null && tYears!= '' && tYears < 0)
			{
				alert("保单的保险期间有误，请确认保单的保险期间录入值是否正确！");
				return false;
			}
		}
	}
	return true;
}

function Crs_BussTypeHelp(){
	window.open("../sys/JtjxYwlyInfo.jsp");
}

//校验黑名单
function checkBlackName(){
	//黑名单校验投保人
	var sql1 =  "select appntname,idtype,idno from lcappnt where prtno='"+fm.PrtNo.value+"' ";
	var arrResult = easyExecSql(sql1);
	
	if(arrResult){
		var sqlblack = "select 1 from lcblacklist where idnotype is not null and idno is not null  "+
					   "  and name='"+arrResult[0][0]+"' and idnotype='"+arrResult[0][1]+"' and idno='"+arrResult[0][2]+"' and type='0' "+
					   "  union  "+
					   "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
					   "  and name='"+arrResult[0][0]+"' and idno='"+arrResult[0][2]+"' and type='0' "+
					   "  union   "+
					   "  select 1 from lcblacklist where (idno is null or  idno='' or  idno='无')  and name='"+arrResult[0][0]+"' and type='0' ";
		var arrResult1 = easyExecSql(sqlblack);
		if(arrResult1){
			if(!confirm("该保单投保人："+arrResult[0][0]+"，存在于黑名单中，确认要复核通过吗？")){
				return false;
			}
		}
	}
	//黑名单校验被保人
	var sql2 =  "select name, idtype, idno from lcinsured where prtno='"+fm.PrtNo.value+"'  ";
	var arrRes = easyExecSql(sql2);
	var tInsuNames = "";
	if(arrRes){
		for(var i = 0; i < arrRes.length; i++){
			var sqlblack2  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrRes[i][0]+"' and idnotype='"+arrRes[i][1]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrRes[i][0]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrRes[i][0]+"' and type='0' ";
			var arrRes1 = easyExecSql(sqlblack2);
			if(arrRes1){
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ " , "+arrRes[i][0];
				}else{
					tInsuNames = tInsuNames + arrRes[i][0];
				}
			}
		}
	}
	if(tInsuNames != ""){
		if (!confirm("该保单被保人姓名："+tInsuNames+",存在于黑名单中，确认要复核通过吗？")){
			return false;
		}
	}
	//黑名单校验受益人
	var sql3 =  "select distinct name, idtype, idno from lcbnf where contno=(select contno from lccont where prtno='"+fm.PrtNo.value+"')  ";
	var arrR = easyExecSql(sql3);
	var tBnfNames = "";
	if(arrR){
		for(var i = 0; i < arrR.length; i++){
			var sqlblack3  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrR[i][0]+"' and idnotype='"+arrR[i][1]+"' and idno='"+arrR[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrR[i][0]+"' and idno='"+arrR[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrR[i][0]+"' and type='0' ";
			var arrR1 = easyExecSql(sqlblack3);
			if(arrR1){
				if(tBnfNames!=""){
					tBnfNames = tBnfNames+ " , "+arrR[i][0];
				}else{
					tBnfNames = tBnfNames + arrR[i][0];
				}
			}
		}
	}
	if(tBnfNames != ""){
		if (!confirm("该保单受益人："+tBnfNames+",存在于黑名单中，确认要复核通过吗？")){
			return false;
		}
	}
	return true;
}
//税优折扣校验 20161107  lxs
function checkdiscountfactor() {
	
	if (LoadFlag == "1") {
		// 新单录入时 对折扣因子的处理
		fm.Agediscountfactor.value = "";
		fm.Supdiscountfactor.value = "";
		fm.Grpdiscountfactor.value = "";
		fm.Totaldiscountfactor.value = "";
		fm.DiscountMode.value = "";
		return true;
	}
	//剔除不享用税优折扣的税优产品
	var sql = "select InsuredAppAge,amnt  from lcpol lc,lmriskapp lm  where lc.prtno='" + fm.PrtNo.value + "' "
			+ " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' and not exists (select 1 from ldcode where codetype='sypremnult' and code=lc.riskcode)";
	var arrResult = easyExecSql(sql);
	if (arrResult !=null && fm.DiscountMode.value=="1") {
	InsuredAppAge = arrResult[0][0];
	if (16 <= InsuredAppAge && InsuredAppAge <= 30) {
		fm.Agediscountfactor.value = 0.8;
	} else if (30 < InsuredAppAge && InsuredAppAge <= 35) {
		fm.Agediscountfactor.value = 0.85;
	} else if (35 < InsuredAppAge && InsuredAppAge <= 40) {
		fm.Agediscountfactor.value = 0.88;
	} else if (40 < InsuredAppAge && InsuredAppAge <= 45) {
		fm.Agediscountfactor.value = 0.92;
	} else if (45 < InsuredAppAge && InsuredAppAge <= 50) {
		fm.Agediscountfactor.value = 0.95;
	} else if (50 < InsuredAppAge && InsuredAppAge <= 55) {
		fm.Agediscountfactor.value = 0.98;
	} else {
		fm.Agediscountfactor.value = 1;

	}
	if (!(fm.Grpdiscountfactor.value == null || fm.Grpdiscountfactor.value == "" || fm.Grpdiscountfactor.value == "null")) {
		if (!(0.90 <= fm.Grpdiscountfactor.value && fm.Grpdiscountfactor.value <= 1)) {
			alert("团体投保人数折扣因子需在0.9-1之间");
			return false;
		}
	} else {
		fm.Grpdiscountfactor.value = 1;

	}
	if (!(fm.Supdiscountfactor.value == null || fm.Supdiscountfactor.value == "" || fm.Supdiscountfactor.value == "null")) {
		if (!(0.75 <= fm.Supdiscountfactor.value && fm.Supdiscountfactor.value <= 1)) {
			alert("补充医疗保险折扣因子需在0.75-1之间");
			return false;
		}
	} else {
		fm.Supdiscountfactor.value = 1;

	}
	var Total = fm.Agediscountfactor.value * fm.Grpdiscountfactor.value * fm.Supdiscountfactor.value;
	if (Total <= 0.70) {
		fm.Totaldiscountfactor.value = 0.70;
	} else {
		fm.Totaldiscountfactor.value = mathRound(Total);
	}
	if ((fm.Totaldiscountfactor.value < 1) && (arrResult[0][1] == 40000) ){
			alert("该被保人已患既往症,不能享受税优折扣");
			fm.Agediscountfactor.value = 1;
			fm.Supdiscountfactor.value = 1;
			fm.Grpdiscountfactor.value = 1;
			fm.Totaldiscountfactor.value = 1;
	}
	}else if(arrResult !=null && fm.DiscountMode.value=="2"){
		if(!dealAppointDiscount(fm.AppointDiscount.value)){
			return false;
		}
	}else if(!arrResult){
		fm.Agediscountfactor.value = "";
		fm.Supdiscountfactor.value = "";
		fm.Grpdiscountfactor.value = "";
		fm.Totaldiscountfactor.value = "";
		fm.AppointDiscount.value = "";
		fm.DiscountMode.value = "";
	}
	return true;
}

function checkCrs_SaleChnl(){
	var tCrs_BussType=fm.Crs_BussType.value;
	var tSaleChnl=fm.SaleChnl.value;
	if((tCrs_BussType=="01"||tCrs_BussType=="02")&&(tSaleChnl=="16"||tSaleChnl=="18")){
		alert("社保直销及社保综拓渠道出单若选择交叉销售，交叉销售业务类型不能为相互代理或联合展业！");
		return false;
	}
	return true;
}

/**
 * 检查：期缴保费上限不得高于“风险保险费+10万元”，对所有税优产品有效
 */
function checkPremUpperLimit() {
	var sql = "select 1 from lmriskapp where taxoptimal='Y' and riskcode = (select riskcode from  lcpol where prtno = '"+fm.PrtNo.value+"') ";
	var arrResult = easyExecSql(sql);
	//判断是否为税优保单
	if(arrResult) {
		//1、先获取计算风险保费的要素
		var getSomeFactorsSql = "select PolNo,PayIntv,InsuredAppAge,Prem from lcpol where prtno = '"+fm.PrtNo.value+"'";
		var Result = easyExecSql(getSomeFactorsSql);
		var PolNo = Result[0][0];
		var PayIntv = Result[0][1];
		var InsuredAppAge = Result[0][2];
		var Prem = Result[0][3];
		//2、再获取计算风险保费的算费公式
		var getCalSql = "select CalSql from lmcalmode where riskcode = (select riskcode from  lcpol where prtno = '"+fm.PrtNo.value+"') and remark = '风险保费扣费'";
		var CalSqlResult = easyExecSql(getCalSql);
		var CalSql = CalSqlResult[0][0];
		//3、拼出计算风险保费的算费SQL(九种税优险种的风险保费计算情况都考虑在内了...)
		//这里所有的税优总折扣因子都默认为：1
		var mtotaldiscountfactor =easyExecSql("select Totaldiscountfactor from lccontsub s where prtno='"+fm.PrtNo.value+"' ");
		//期缴保费上限不得高于“风险保险费*折扣因子+10万元”
		if(mtotaldiscountfactor !=""&&mtotaldiscountfactor !=null){
			if(CalSql.indexOf("?Totaldiscountfactor?") >= 0) {
				CalSql = CalSql.replace("?Totaldiscountfactor?",mtotaldiscountfactor);
			}
		}else{
			if(CalSql.indexOf("?Totaldiscountfactor?") >= 0) {
				CalSql = CalSql.replace("?Totaldiscountfactor?","1");
			}
		}
		/**
		 * 目前数据库中税优产品一共就9种，每种的风险保费算法都不同，但其中只有AppAge这个参数会出现两次以上，因此需要将该字符串
		 * 在总算法sql的字符串中全部替换掉，已知最多出现两次，因此只需循环两次
		 * 
		 * 但这样写有个缺点就是如果以后sql出现变动，AppAge出现的次数增加或者减少，那么还需更改代码的循环次数，这样写非常死板
		 * 并且如果以后循环的次数多了也会降低性能，因此这里可以换成另一种写法：JavaScript中没有replaceAll()方法,但是在js
		 * 中replace方法存在利用正则表达式的方式进行字符串替换，这样就可以避免多次循环。
		 * 
		 * 但是这里我就要吐槽一下，为啥我换成正则表达式就报错，各种不识别，难道我们亲爱的老Lis框架不屑于兼容正则表达式吗？无语中...
		 * 
		 * Created by JC 2017-07-20
		 * I am thinking in Java, not the JavaScript in LIS...
		 */
		for(var i = 0; i < 2; i++) {
			if(CalSql.indexOf("?AppAge?") >= 0) {
				CalSql = CalSql.replace("?AppAge?",InsuredAppAge);
			}
		}
		if(CalSql.indexOf("?PayIntv?") >= 0) {
			CalSql = CalSql.replace("?PayIntv?",PayIntv);
		}
		if(CalSql.indexOf("?PolNo?") >= 0) {
			CalSql = CalSql.replace("?PolNo?",PolNo);
		}
		//算出风险保费
		var riskPrem = easyExecSql(CalSql);
		//得到int类型的风险保费
		var a = parseInt(riskPrem[0][0]);
		//得到int类型的期交保费
		var b = parseInt(Prem);
		//期缴保费上限不得高于“风险保险费+10万元”
		if(PayIntv=="12"){
			if(b > (a+10000)) {
				alert("年化期缴保费上限不得高于“年风险保险费+1万元”!");
				return false;
			}
		}else if(PayIntv=="1"){
			if((b*12) > ((a)*12+10000)) {
				alert("年化期缴保费上限不得高于“年风险保险费+1万元”!");
				return false;
			}
	    }
		
	}
	return true;
}

/**
 * 增加以下税优相关校验：
 * 1.当投保标识为“个人”时，团体投保人数折扣因子必须等于1。
 * 2.当被保险人告知医疗费用支付方式中不包含“⑤补充医疗保险”时，补充医疗保险折扣因子必须等于1。
 * 3.目前仅当险种为122601、122901时，可以录入折扣因子，其他产品录入折扣因子报错提示不通过。
 * @returns {Boolean}
 */
function checkSYAbout() {
	//目前仅当险种为122601、122901时，可以录入折扣因子，其他产品录入折扣因子报错提示不通过。
	var risk122601And3122901 = "select 1 from LCPol p where PrtNo = '"+fm.PrtNo.value+"' and RiskCode in ('122601', '122901') ";
    result = easyExecSql(risk122601And3122901);
    var discountData =easyExecSql("select Agediscountfactor,Supdiscountfactor,Grpdiscountfactor,Totaldiscountfactor,DiscountMode,TaxFlag from lccontsub s where prtno='"+fm.PrtNo.value+"' ");
    if(!result){
    	if(discountData){
    		if(discountData[0][0]!=""||discountData[0][1]!=""||discountData[0][2]!=""||discountData[0][3]!=""||discountData[0][4]!=""){
    			alert("当前录入的险种不为122601或122901，不可以录入折扣方式!");
    			return false;
    		}
    	}
    }else{
    	if(discountData){
    		if(discountData[0][3]==""||discountData[0][4]==""){
    			alert("当前录入的险种为122601或122901，必须录入折扣方式!");
    			return false;
    		}
    		if (discountData[0][4]=="1") {
    			var Grpdiscountfactor = discountData[0][2];
    			//1、当投保标识为“个人”时，团体投保人数折扣因子必须等于1。
    			if((discountData[0][5] == "1") && (Grpdiscountfactor != null) && (Grpdiscountfactor != "")) {
    				if(Grpdiscountfactor != '1') {
    					alert("税优标识为个人时，团体投保人数折扣因子必须等于1！");
    					return false;
    				}
    			}
    			//2、当被保险人告知医疗费用支付方式中不包含“⑤补充医疗保险”时，补充医疗保险折扣因子必须等于1。
    			if(!checkGZ2Prem()) {
    				alert("当被保险人告知医疗费用支付方式中不包含“⑤补充医疗保险”时，补充医疗保险折扣因子必须等于1！");
    				return false;
    			}
    		}else if(discountData[0][4]=="2") {
				var rss0 =easyExecSql("select 1 from ldcode1 where codetype='discountmode' and  code='2' and code1=left('"+fm.all('ManageCom').value+"',4) ");
				var rss1 =easyExecSql("select grpno from lccontsub where prtno='"+fm.PrtNo.value+"'");
				var rss2;
				if(rss1){
					rss2 =easyExecSql("select 1 from ldcode1 where codetype='discountmode' and  code='2' and code1='"+rss1[0][0]+"' ");
				}
				if((rss0==""||rss0==null) && (rss2==""||rss2==null)){
					alert("当前机构或当前集团编号下不允许录入约定折扣方式！")
					return false;
				}
				if(!dealAppointDiscount(discountData[0][3])){
					return false;
				}
			}
    	}
    }
	return true;
}
/**
 * 当被保险人告知医疗费用支付方式中不包含“⑤补充医疗保险”时，补充医疗保险折扣因子必须等于1
 * @returns {Boolean}
 */
function checkGZ2Prem() {
	var CustomerimpartSql = "select impartparammodle from lccustomerimpart where prtno = '"+fm.PrtNo.value+"' and impartver = '001' and impartcode='170' and impartcontent = '被保险人的医疗费用支付方式   □公费医疗  □社会医疗保险  □商业医疗保险  □自费 □补充医疗保险' ";
	var arrCustomer = easyExecSql(CustomerimpartSql);
	var getFactorSql = " select Agediscountfactor,Supdiscountfactor,Grpdiscountfactor,Totaldiscountfactor from lccontsub where prtno = '"+fm.PrtNo.value+"' ";
	var factorResult = easyExecSql(getFactorSql);
	var Supdiscountfactor = factorResult[0][1];
	if(arrCustomer != null || arrCustomer != ""){
		var arry = arrCustomer[0][0].split(",");
		if(Supdiscountfactor != "" && Supdiscountfactor != null) {
			if(arry[0] == "Y" || arry[1] == "Y"){
				if(arry[4] == "N" && Supdiscountfactor != "1") {
					return false;
				}
			}
		}
	}
	return true;
}

function checkdiscountfactor1() {
	var sql = "select InsuredAppAge,amnt  from lcpol lc,lmriskapp lm  where lc.prtno='" + fm.PrtNo.value + "' "
			+ " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
			+ " and not exists (select 1 from ldcode where codetype='sypremnult' and code=lc.riskcode)";
	var arrResult = easyExecSql(sql);
	if (arrResult) {
	
	if ((fm.Totaldiscountfactor.value < 1)&&(arrResult[0][1] == 40000)) {
			alert("该被保人已患既往症,不能享受税优折扣，请修改折扣因子！");
			return false;
	}
	}
	return true;
}
function displaydiscountfactorinfo(){
	var sql ="select Agediscountfactor,Supdiscountfactor,Grpdiscountfactor,Totaldiscountfactor,DiscountMode from lccontsub "
		+ " where prtno='" + fm.PrtNo.value + "' ";
		var rs = easyExecSql(sql);
	if (rs) {
		fm.Agediscountfactor.value = rs[0][0];
		fm.Supdiscountfactor.value = rs[0][1];
		fm.Grpdiscountfactor.value = rs[0][2];
		fm.Totaldiscountfactor.value=rs[0][3];
		fm.DiscountMode.value=rs[0][4];
		if(rs[0][4] == "2"){
			fm.all('AppointDiscountID').style.display = "";
			fm.all('AppointDiscountID2').style.display = "";
			fm.AppointDiscount.value=rs[0][3];
			fm.all('Supdiscountfactor').readOnly = true;
			fm.all('Grpdiscountfactor').readOnly = true;
		}
		sql = "select codename from ldcode1 where codetype='discountmode' and code='"+rs[0][4]+"'";
		rs = easyExecSql(sql);
		if(rs){
			fm.DiscountMode_Name.value=rs[0][0];
		}
	}
}
//by zcx 20120524 增加对代理销售业务员的查询
function queryAgentSaleCode()
{
    var tAgentCom = "";
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        fm.all('ManageCom').focus();
        return ;
    }
    if(fm.all('SaleChnl').value == "04"){
       if(fm.all('AgentCom').value == "")
       {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom').focus();
        return ;
       }
       tAgentCom = fm.all('AgentCom').value;
    }else{
      if(fm.all('AgentCom1').value == "")
      {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom1').focus();
        return ;
      }
      tAgentCom = fm.all('AgentCom1').value;
    }
    
    var tAgentCom1 = tAgentCom;
    if(fm.all('AgentSaleCode').value == "")
    {
        var strURL = "../sys/AgentSaleCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&AgentCom=" + tAgentCom1
        var newWindow = window.open(strURL, "AgentSaleCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('AgentSaleCode').value != "")
    {
        var cAgentCode = fm.AgentSaleCode.value;  //保单号码
        var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + tAgentCom1+ "'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentSaleName.value = arrResult[0][1];
            //alert("查询结果:  代理销售业务员代码:["+arrResult[0][0]+"] ，代理销售业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value = "";
            alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
        }
    }
}

function queryAgentSaleCode2() {

  var tAgentCom = "";
  if(fm.all('AgentSaleCode').value != ""){
  	if(fm.all('SaleChnl').value == "04"){
       if(fm.all('AgentCom').value == "")
       {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom').focus();
        return ;
       }
       tAgentCom = fm.all('AgentCom').value;
    }else{
      if(fm.all('AgentCom1').value == "")
      {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom1').focus();
        return ;
      }
      tAgentCom = fm.all('AgentCom1').value;
    }
    var tAgentCom1 = tAgentCom;
    var cAgentCode = fm.AgentSaleCode.value;  //保单号码
    var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + tAgentCom1 + "'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentSaleName.value = arrResult[0][1];
      //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    } else {
      fm.AgentGroup.value="";
      alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
    }
  }
}

function afterQuery5(arrResult){
  if(arrResult!=null) {
    fm.AgentSaleCode.value = arrResult[0][0];
    fm.AgentSaleName.value = arrResult[0][1];
  }
}

//北分中介时，中介机构及代理销售业务员必须填写
function checkAgentComAndSaleCdoe(){
	var strSql = "select AgentCom,AgentSaleCode from lccont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%' and salechnl = '10'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCom = arrResult[0][0];
	    var tAgentSaleCode = arrResult[0][1];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCom == null || tAgentCom ==''){
	   		alert("中介公司代码必须录入，请核查！");
	   		return false;    	
	   	}
	   	if(tAgentSaleCode == null || tAgentSaleCode ==''){
	   		alert("代理销售业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	
	   	var tSqlCode = "select ValidStart,ValidEnd from LAQualification where agentcode = '"+tAgentSaleCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tValidStart = arrCodeResult[0][0];
	   		var tValidEnd = arrCodeResult[0][1];
	   		if(tValidStart == null || tValidStart == ''){
	   			alert("代理销售业务员资格证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tValidEnd == null || tValidEnd == ''){
	   			alert("代理销售业务员资格证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
	   			alert("代理销售业务员资格证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("代理销售业务员资格证信息不完整，请核查！");
	   		return false;
	   	}
	   	
	   	var tSqlCom = "select LicenseStartDate,LicenseEndDate,EndFlag from lacom where agentcom = '"+tAgentCom+"'";
	   	var arrComResult = easyExecSql(tSqlCom);
	   	if(arrComResult){
	   		var tLicenseStartDate = arrComResult[0][0];
	   		var tLicenseEndDate = arrComResult[0][1];
	   		var tEndFlag = arrComResult[0][2];
	   		if(tLicenseStartDate == null || tLicenseStartDate == ''){
	   			alert("中介公司许可证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tLicenseEndDate == null || tLicenseEndDate == ''){
	   			alert("中介公司许可证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == null || tEndFlag == ''){
	   			alert("中介机构合作终止状态为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == 'Y'){
	   			alert("中介机构合作终止状态为失效，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tLicenseEndDate)=='1'){
	   			alert("中介公司许可证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("中介公司许可证信息不完整，请核查！");
	   		return false;
	   	}
    }
    return true;
}
function initAgentSaleCode(){
	var tSqlCode = "select AgentSaleCode from LCCont where prtno='" + fm.PrtNo.value + "'";
	var arrCodeResult = easyExecSql(tSqlCode);
	if(arrCodeResult){
		if(arrCodeResult[0][0] != null && arrCodeResult[0][0] != ""){
			var tSQL = "select agentcode,name from laagenttemp where agentcode = '"+arrCodeResult[0][0]+"'";
			var arrResult = easyExecSql(tSQL);
			if(arrResult){
				fm.AgentSaleCode.value = arrResult[0][0];
    			fm.AgentSaleName.value = arrResult[0][1];
			}
		}
	}
}
//北分业务员必须填写
function checkAgentCode(){
	var strSql = "select AgentCode from lccont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCode = arrResult[0][0];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCode == null || tAgentCode ==''){
	   		alert("业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	var tSqlCode = "select branchtype,branchtype2 from laagent where agentcode = '"+tAgentCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tSqlValid = "";
	   		if(arrCodeResult[0][0] == "1" || arrCodeResult[0][0] == "2" || arrCodeResult[0][0] == "4" || arrCodeResult[0][0] == "5"|| arrCodeResult[0][0] == "7"){
	   			tSqlValid = "select ValidStart,ValidEnd from LAQUALIFICATION where agentcode = '"+tAgentCode+"' and state='0'";
	   		} else if (arrCodeResult[0][0] == "3"){
	   			tSqlValid = "select Quafstartdate,QuafEndDate from laagent where agentcode = '"+tAgentCode+"'";
	   		} else if(arrCodeResult[0][0]=="6"){
	   			return true;
	   		} else {
	   			alert("业务员资销售渠道异常，请核查！");
	   			return false;
	   		}
	   		var arrValid = easyExecSql(tSqlValid);
	   		if(arrValid){
		   		var tValidStart = arrValid[0][0];
		   		var tValidEnd = arrValid[0][1];
		   		if(tValidStart == null || tValidStart == ''){
		   			alert("业务员资格证有效起期为空，请核查！");
		   			return false;
		   		}
		   		if(tValidEnd == null || tValidEnd == ''){
		   			alert("业务员资格证有效止期为空，请核查！");
		   			return false;
		   		}
		   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
		   			alert("业务员资格证已失效，请核查！");
		   			return false;
		   		}
		   	}else{
		   		alert("业务员不存在有效资格证信息，请核查！");
		   		return false;
		   	}
	   	}else{
	   		alert("业务员不存在，请核查！");
	   		return false;
	   	}
    }
    return true;
}

function checkAppnt(wFlag){
	var tSql = "select cc.prtno,cad.Customerno,cc.Managecom,cad.Mobile,cad.Phone,cad.homephone,ca.idno,ca.idtype,ca.appntbirthday,ca.appntsex, "
	           +" cad.PostalProvince,cad.PostalCity,cad.PostalCounty,cad.PostalStreet,cad.PostalCommunity,cad.homecode,cad.homenumber,ca.appntname,ca.nativeplace,ca.nativecity, " 
	           +" cad.email "
	           +" from lccont cc inner join lcappnt ca on ca.appntno=cc.appntno and ca.contno=cc.contno " +
				"inner join lcaddress cad on cad.customerno=ca.appntno and cad.addressno=ca.addressno where cc.prtno='" + fm.PrtNo.value + "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		// 印刷号
		var prtno = appntResult[0][0];
		// 投保人客户号
		var customerno = appntResult[0][1];
		// 管理机构
		var managecom = appntResult[0][2];
		// 移动电话
		var mobile = appntResult[0][3];
		// 联系电话
		var phone = appntResult[0][4];
		// 家庭电话
		var homephone = appntResult[0][5];
		// 证件号
		var id = appntResult[0][6];
		// 证件类型
		var idtype = appntResult[0][7];
		// 出生日期
		var birthday = appntResult[0][8];
		// 性别
		var sex = appntResult[0][9];
		//联系地址 固定电话
		var homecode = trim(appntResult[0][15]);
        var homenumber = trim(appntResult[0][16]);
        var PostalProvince = trim(appntResult[0][10]);
        var PostalCity = trim(appntResult[0][11]);
        var PostalCounty = trim(appntResult[0][12]);
        var PostalStreet = trim(appntResult[0][13]);
        var PostalCommunity = trim(appntResult[0][14]);
        // 姓名
		var name = appntResult[0][17];
		var nativeplace = appntResult[0][18];
		var nativecity = appntResult[0][19];
		//邮箱
		var email = appntResult[0][20];
		
		if(!isNull(email)){
			   var checkemail =CheckEmail(email);
			   if(checkemail !=""){
				   alert("投保人"+checkemail);
				   return false;
			   }
		   }
		if(!isNull(homephone)){
			   var checkhomephone =CheckFixPhone(homephone);
			   if(checkhomephone !=""){
				   alert("投保人"+checkhomephone);
				   return false;
			   }
		   }
		
		// 共有校验
		if(!isNull(mobile)) {
			if(!isInteger(mobile) || mobile.length != 11){
				alert("投保人移动电话需为11位数字，请核查！");
		   		return false;
			}			
		}
		
		if(!checkMobile(1,mobile,'')){
		   return false;
		}
		
        if(!chenkIdNo(1,idtype,id,'')){
	       return false;
	    }
	
    	if(!chenkHomePhone(1,homecode,homenumber,'')){
	       return false;
	    }
	
	    if(!checkAddress(1,PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity,'','')){
	       return false;
      	}
		
		if(idtype == "0"){
			
			if(birthday != getBirthdatByIdNo(id)){
				alert("投保人身份证号与出生日期不符，请核查！");
				return false;
			}
			
			if(sex != getSexByIDNo(id)){
				alert("投保人身份证号与性别不符，请核查！");
				return false;
			}
		}
		
		if(!checkCity(1,name,nativeplace,nativecity)){
	        return false;
	    }
		
		// 机构特殊校验  --需要在数据库中配置 codetype = 字段名 + check

		// 暂时统一对投保人进行校验，若需要单独校验，可以调整为按字段进行校验
		// 若配置为空则会对全部机构进行校验
		if(manageCheck(managecom.substring(0, 4),"appnt")){
			
			if(isNull(id)){
				alert("投保人证件号码不能为空，请核查！");
				return false;
			}
			
			if(isNull(mobile) && isNull(phone) && isNull(homephone)){
				alert("投保人联系电话、固定电话、移动电话不能同时为空，请核查！");
		   		return false;
			}
		}
		
		// 由于个分公司都提出了电话的校验 因此新建phonecheck
		if(manageCheck(managecom.substring(0, 4),"phone")){
						
			if(!isNull(phone)){
				if(phone.length < 7){
					alert("投保人联系电话不能少于7位，请核查！");
			   		return false;
				}
			}
			
			// 复核时，对于3个以上重复号码，强制下发问题件
			if(wFlag == 2 && (!isNull(mobile) || !isNull(phone))){
				// 由于重复号码校验速度很慢，因此先对问题件进行判断
				
				// 空代表没有下发问题件 1代表已下发问题件未回销 2代表已回销
				// order by doc 为避免多次下发问题件联系电话问题件 只要有没有回销的 不让通过
				var issueSql = "Select (Case When (Select Docid From Es_Doc_Main Where Doccode = Ci.Prtseq And Subtype = 'TB22') Is Null Then 1 Else 2 End) doc "
						+ "From Lcissuepol Ci Where Errfieldname = '联系电话' And Questionobj Like '投保人%' And Contno = (Select Contno From Lccont Where Prtno = '" + prtno + "') " 
						+ "order by doc";
				var issueResult = easyExecSql(issueSql);
				
				// 页面提示信息
				var alterInf = "";
				if(issueResult == null){
					alterInf = "请下发投保人联系电话问题件";
				} else if(issueResult[0][0] == "1"){
					alterInf = "已下发的联系电话问题件尚未回销，请确认";
				}
				
				if(issueResult == null || issueResult[0][0] == "1"){
					if(!isNull(phone)){
						var phoneSql = "select count(distinct customerno) from lcaddress where phone='" + phone + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
						var result = easyExecSql(phoneSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人联系电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
				
					if(!isNull(mobile)){
						var mobilSql = "select count(distinct customerno) from lcaddress where mobile='" + mobile + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
						var result = easyExecSql(mobilSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人移动电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
					
					if(!isNull(homephone)){
						var mobilSql = "select count(distinct customerno) from lcaddress where homephone='" + homephone + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and lcaddress.addressno=addressno) ";
						var result = easyExecSql(mobilSql);
						if(result){
							var count = result[0][0];
							if(count >= 2){
								alert("该投保人家庭电话已在三个以上不同投保人的保单中出现，" + alterInf);
								return false;
							}
						}
					}
				}
			}
			
	    	if(manageCheck(managecom.substring(0, 4),"agent")){
	    		//校验是否是业务员本人，如果不是进行手机号校验
				// 业务员手机号码校验
					if(!isNull(phone)){
						var mobilSql = "select groupagentcode from laagent where phone='" + phone + "' and idno <> '"+id+"' with ur";
						var result = easyExecSql(mobilSql);
						if(result){
							alert("该投保人联系电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
							return false;
						}
						var mobilSql = "select groupagentcode from laagent where mobile='" + phone + "' and idno <> '"+id+"' with ur";
						var result = easyExecSql(mobilSql);
						if(result){
							alert("该投保人联系电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
							return false;
						}
						var mobilSql = "select agentcode from laagenttemp where phone='" + phone + "' and name <> '"+name+"' with ur";
						var result = easyExecSql(mobilSql);
						if(result){
							alert("该投保人联系电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
							return false;
						}
					}
					
					if(!isNull(mobile)){
						var phoneSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
						var result = easyExecSql(phoneSql);
						if(result){
							alert("该投保人移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
							return false;
						}
						var phoneSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
						var result = easyExecSql(phoneSql);
						if(result){
							alert("该投保人移动电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
							return false;
						}
						var mobilSql = "select agentcode from laagenttemp where mobile='" + mobile + "' and name <> '"+name+"' with ur";
						var result = easyExecSql(mobilSql);
						if(result){
							alert("该投保人移动电话与代理销售业务员" + result[0][0] + "手机号码相同，请核查！");
							return false;
						}
					}
					if(!isNull(homephone)){
						var phoneSql = "select groupagentcode from laagent where phone='" + homephone + "' and idno <> '"+id+"' with ur";
						var result = easyExecSql(phoneSql);
						if(result){
							alert("该投保人固定电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
							return false;
						}
						var phoneSql = "select groupagentcode from laagent where mobile='" + homephone + "' and idno <> '"+id+"' with ur";
						var result = easyExecSql(phoneSql);
						if(result){
							alert("该投保人固定电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
							return false;
						}
						var mobilSql = "select agentcode from laagenttemp where phone='" + homephone + "' and name <> '"+name+"' with ur";
						var result = easyExecSql(mobilSql);
						if(result){
							alert("该投保人固定电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
							return false;
						}
					}
			}
	    
			
		    
			
		}
	}
	return true;
}
function checkInsured(){
	var tSql = "select ca.name,ca.idno,ca.idtype,ca.birthday,ca.sex,cad.mobile,ca.RelationToAppnt,cad.phone,cad.homephone,ca.nativeplace,ca.nativecity,cad.email from lcinsured ca "
	         + "inner join lcaddress cad on cad.customerno=ca.insuredno and cad.addressno=ca.addressno "
	         + " where ca.prtno='" + fm.PrtNo.value + "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		for(var index = 0; index < appntResult.length; index++){
			// 被保人姓名
			var name = appntResult[index][0];
			// 证件号
			var id = appntResult[index][1];
			// 证件类型
			var idtype = appntResult[index][2];
			// 出生日期
			var birthday = appntResult[index][3];
			// 性别
			var sex = appntResult[index][4];
			//移动电话
			var mobile = appntResult[index][5];
			//与投保人关系
			var relation = appntResult[index][6];
		    // 联系电话
		    var phone = appntResult[index][7];
		    // 家庭电话
		    var homephone = appntResult[index][8];
		    // 国籍
		    var nativeplace = appntResult[index][9];
		    // 国家
		    var nativecity = appntResult[index][10];
		    //邮箱
		    var email = appntResult[index][11];
			
			if(idtype == "0"){
		
				if(birthday != getBirthdatByIdNo(id)){
					alert("被保人" + name + "身份证号与出生日期不符，请核查！");
					return false;
				}
			
				if(sex != getSexByIDNo(id)){
					alert("被保人" + name + "身份证号与性别不符，请核查！");
					return false;
				}
			}
			if(!chenkIdNo(2,idtype,id,name)){
	          return false;
	        }
	        
	        if(!checkCity(2,name,nativeplace,nativecity)){
	            return false;
	        }
	        
	        //校验被保人邮箱
	        if(!isNull(email)){
	     	   var checkemail =CheckEmail(email);
	     	   if(checkemail !=""){
	     		   alert("被保人" + name + checkemail);
	     		   return false;
	     	   }
	        }
	        
	        //校验被保人固定电话
	        if(!isNull(homephone)){
		     	   var checkhomephone =CheckFixPhone(homephone);
		     	   if(checkhomephone !=""){
		     		   alert("被保人" + name + checkhomephone);
		     		   return false;
		     	   }
		        }
	        
	        // 共有校验
		    if(!isNull(mobile)) {
			    if(!isInteger(mobile) || mobile.length != 11){
				    alert("被保人" + name + "移动电话需为11位数字，请核查！");
		   	      	return false;
			    } 
			    if(!checkMobile(2,mobile,name)){
			        return false;
			    }
		    }
		    
		    if(!checkAppInsr(relation,name,birthday)){
		       return false;
		    }
		    
			if(!isNull(phone)){
				var mobilSql = "select groupagentcode from laagent where phone='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "联系电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select groupagentcode from laagent where mobile='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "联系电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + phone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "联系电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
			
			if(!isNull(mobile)){
				var phoneSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "移动电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where mobile='" + mobile + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "移动电话与代理销售业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				
			}
			if(!isNull(homephone)){
				var phoneSql = "select groupagentcode from laagent where phone='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "固定电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where mobile='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "固定电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + homephone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "固定电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
			
			
			
		}
	}

	return true;
}

// 判断机构是否需要进行校验
// managecom 机构
// checkflag 校验项
function manageCheck(managecom, checkflag){
	var checkSql = "select (case (select count(1) from ldcode where codetype = '" + checkflag + "check') " +  
			"when 0 then 1 else (select distinct 1 from ldcode where codetype = '" + checkflag + "check' and code = '" + managecom + "') end ) " + 
			"from dual";
	var result = easyExecSql(checkSql);
	if(result){
		if(result[0][0] == "1"){
			// 该机构需要进行校验
			return true;
		}
	}
	// 该机构不需要进行校验
	return false;
}

// 判断是否为空
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}
//校验投保人银行账户信息
function checkBank()
{
    var strSql= easyExecSql("select PayMode,ExPayMode,BankCode, Accname,BankAccNo from LCCont where PrtNo = '" + fm.PrtNo.value 
            + "' and (BankCode is null or  Accname is null or BankAccNo is null )");
   if(strSql) {          
    if(strSql[0][0]== "4"||strSql[0][1]=="4")
    {
     	if(strSql[0][2] == null ||strSql[0][2] == '')
        {   
         	alert("银行代码为空，若修改以后请点击修改按钮！");
            return false;
        }      
        if(strSql[0][3] == null ||strSql[0][3] == '')
        {
            alert("户名为空，若修改以后请点击修改按钮！");
            return false;
        } 
        if(strSql[0][4] == null ||strSql[0][4] == '')
        {
            alert("账号为空，若修改以后请点击修改按钮！");
            return false;
        }     
             
      }
   }
    return true;
}

// 豁免险保额校验
function checkExemption(){
	var sql = "select 1 from lcpol where prtno = '" + fm.PrtNo.value 
					+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
	var result= easyExecSql(sql);
	if(result && result[0][0] == "1"){
		sql = "select 1 from lcpol where appntno=insuredno and prtno = '" + fm.PrtNo.value 
					+ "' and not exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
		result= easyExecSql(sql);
		if(result && result[0][0] == "1"){
			alert("非豁免险险种的被保人不能为投保人，请核查！");
			return false;
		}	
		sql = "select 1 from lccont where payintv=0 and prtno = '" + fm.PrtNo.value + "' ";
		result= easyExecSql(sql);
		if(result && result[0][0] == "1"){
			alert("保单中存在豁免险险种，缴费频次不能为趸缴！");
			return false;
		}	
		var sql1 = "select 1 from lcpol a where prtno = '" + fm.PrtNo.value + "' and exists (select 1 from lmriskapp where riskcode=a.riskcode and kindcode='S') ";
        var sql2 = "select 1 from ldcode1 b where codetype='checkexemptionrisk' and exists ("+sql1+" and a.riskcode=b.code1 )  ";
        var sql3 = "select 1 from ldcode1 c where codetype='checkappendrisk' and exists ("+sql2+" and c.code1=b.code ) ";
		var sql = "select sum(prem) from lcpol lc where prtno = '" + fm.PrtNo.value + "' and ( exists ("+sql2+" and lc.riskcode= b.code) or exists ("+sql3+" and lc.riskcode=c.code) ) ";
		result= easyExecSql(sql);
		if(result && !isNull(result[0][0]) && result[0][0] != "null"){
			var sumPrem = result[0][0];
			sql = "select amnt,appntno,insuredno from lcpol where prtno = '" + fm.PrtNo.value 
					+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
			result= easyExecSql(sql);
			if(sumPrem != result[0][0]){
				alert("豁免险保额异常，请到豁免险险种页面，点击修改按钮，重新计算豁免险保额！");
				return false;
			}
			if(result[0][1] != result[0][2]){
				alert("豁免险险种的被保人必须为投保人，请核查！");
				return false;
			}
		} else {
			alert("保单下尚未添加除豁免险外其他险种，或其他险种总保费为零，请核查！");
			return false;
		}
	}
	return true;
}

function checkPayintv(){
	var sql = "select payintv from lccont where prtno = '" + fm.PrtNo.value + "'";
	var result= easyExecSql(sql);
	if(result && !isNull(result[0][0]) && result[0][0] != "null"){
		var payinvt = result[0][0];
		if(payinvt != fm.PayIntv.value){
			sql = "select 1 from lcpol where prtno = '" + fm.PrtNo.value + "'";
			var result= easyExecSql(sql);
			if(result && result[0][0] == "1"){
				alert("修改缴费频次，需要先删除保单下的险种信息！");
				return false;
			}
		}
		return true;
	}
	return false;
}

//综合开拓信息显示
function isAssist()
{
    if(fm.ExtendFlag.checked == true)
    {
        fm.all('ExtendID').style.display = "";
    }
    if(fm.ExtendFlag.checked == false)
    {
        fm.all('AssistSalechnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value="";
        fm.all('AssistSaleChnl').value="";
        fm.all('ExtendID').style.display = "none";
    }
}

//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryAssistAgent()
{
    if(fm.all('AssistSaleChnl').value == "" || fm.all('AssistSaleChnl').value == null)
    {
    	alert("请先选择协助销售渠道！");
    	return false;
    }
    if(fm.all('AssistAgentCode').value == "" )
    {  
        var tAssistSaleChnl = (fm.AssistSaleChnl != null && fm.AssistSaleChnl != "undefined") ? fm.AssistSaleChnl.value : "";
        var strURL = "../sys/AssistAgentCommonQueryMain.jsp?AssistSaleChnl=" + tAssistSaleChnl;        
        var newWindow = window.open(strURL, "AssistAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('AssistAgentCode').value != "")
    {	
        var strGrpSql = "select agentcode,name from LAAgent where agentcode ='" + fm.all('AssistAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery6(arrResult);
        }
        else
        {            
            alert("代码为:[" +  fm.AssistAgentCode.value + "]的业务员不存在，请确认!");
        }
    }
}
function afterQuery6(arrResult){
	if(arrResult!=null) {
		fm.AssistAgentCode.value = arrResult[0][0];
		fm.AssistAgentName.value = arrResult[0][1];
	}
}
function ExtendCheck()
{    
    if(fm.ExtendFlag.checked == true)
    {
    	if(fm.AssistSaleChnl.value == "" || fm.AssistSaleChnl.value == null)
    	{
    		alert("选择综合开拓时，协助销售渠道不能为空，请核查！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.AssistSaleChnl.value != "02" && fm.AssistSaleChnl.value != "13")
    	{
    		alert("协助销售渠道只能选择团险直销或银代直销！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.AssistAgentCode.value == "" || fm.AssistAgentCode.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员代码不能为空，请核查！");
    		fm.all('AssistAgentCode').focus();
    		return false;
    	}
    	if(fm.AssistAgentName.value == "" || fm.AssistAgentName.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员姓名不能为空，请核查！");
    		fm.all('AssistAgentName').focus();
    		return false;
    	}
    	var tSQL = "select code1,codename from ldcode1 where codetype = 'salechnl' and code = '"+fm.AssistSaleChnl.value+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("销售渠道与业务员代码不匹配！");
			return false;
		}
		var tSQL1 = "select 1 from laagent where agentcode = '"+fm.AssistAgentCode.value+"' and BranchType = '"+arrResult[0][0]+"' and BranchType2 = '"+arrResult[0][1]+"'";
		var arrResult1 = easyExecSql(tSQL1);
		if(!arrResult1){
			alert("销售渠道与业务员代码不匹配，请核查！");
			return false;
		}
      	return true;
    }else{
    	fm.AssistSaleChnl.value == "";
    	fm.AssistAgentCode.value == "";
    	fm.AssistAgentName.value == "";
    	return true;
    }
}

function initExtend(){

	var tSql = "select AssistSalechnl,AssistAgentCode from LCExtend where prtno='" + fm.all('PrtNo').value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.ExtendFlag.checked = true;
			fm.all('ExtendID').style.display = "";
			fm.AssistSaleChnl.value = arrResult[0][0];
			fm.AssistAgentCode.value = arrResult[0][1];
			var  tSql1 = "select name from LAAgent where agentcode ='" + arrResult[0][1] + "'";
			var arrResult1 = easyExecSql(tSql1);
			fm.AssistAgentName.value = arrResult1[0][0];
		}
	}
}
function valiAddress(){
	
	var getAddrSQL = "select lcad.postaladdress from LCAppnt lca inner join LCAddress lcad on lca.appntno = lcad.customerno and lca.addressno = lcad.addressno where lca.prtno = '"+fm.PrtNo.value+"' ";
	var addrResult = easyExecSql(getAddrSQL);
	if(addrResult){
		var addr = addrResult[0][0];
		var managecom = fm.ManageCom.value;
		if(manageCheck(managecom.substring(0, 4),"addr")){
			if(addr.trim().length<6){
				alert("投保人联系地址信息低于字符限制要求，须提详细地址信息!");
				return false;
			}
		}
	}
	var insuredAddressData = easyExecSql("select ad.PostalProvince,ad.PostalCity,ad.PostalCounty,ad.PostalStreet,ad.PostalCommunity,ap.name from lcaddress ad,lcinsured ap where ad.customerno=ap.insuredno and ad.addressno=ap.addressno and ap.contno='"+fm.ContNo.value+"'");
	for(var i=0;i<insuredAddressData.length;i++){
		var checkAddressFlag = checkAddress(2,insuredAddressData[i][0],insuredAddressData[i][1],insuredAddressData[i][2],insuredAddressData[i][3],insuredAddressData[i][4],'',insuredAddressData[i][5]);
		if(!checkAddressFlag){
			return false;
		}
	}
	return true;
}

function valiMedical(){
	var strSQL = "Select cc.paymode,cc.expaymode,ca.idtype"
			+" From Lccont Cc"
			+" Inner Join Lcappnt Ca"
			+"   On Cc.Contno = Ca.Contno"
			+" Inner Join Lcaddress Cad"
			+"    On Ca.Appntno = Cad.Customerno"
			+"   And Ca.Addressno = Cad.Addressno "
			+" where  cc.prtno = '"+fm.PrtNo.value+"'"
			+" with ur" ;
	var riskResult = easyExecSql(strSQL);
	if(riskResult){
		var payMode = riskResult[0][0];
		var exPayMode = riskResult[0][1];
		var idtype = riskResult[0][2];
		/**
		 * 校验首续期的缴费方式
		 */
		if(payMode == '8' && exPayMode != '8'){
			alert("首期的缴费方式是8-医保个人账户的缴费方式，续期的缴费方式必须是8-医保个人账户！"); 
			return false;
		}
		if(payMode != '8' && exPayMode == '8'){
			alert("首期的缴费方式不是8-医保个人账户的缴费方式，续期的缴费方式不能是8-医保个人账户！"); 
			return false;
		}
		/**
		 * 选择8-医保个人账户转账的缴费方式，必须录入投保人的身份证号
		 */
		if(payMode == '8' && idtype != '0'){
			alert("缴费方式选择8-医保个人账户的缴费方式，证件类型必须是身份证！"); 
			return false;
		}
		
		if(payMode =="8"){
           var sql= "select 1 from LDmedicalcom ld "
	              + "where cansendflag='1' and exists(select 1 from lccont where prtno='"+fm.PrtNo.value+"' and bankcode=ld.medicalcomcode and managecom=ld.comcode) ";
	       
	       var rs = easyExecSql(sql);
           if(rs == null || rs[0][0] == "" || rs[0][0] == "null")
           {
                alert("没有医保编码对应的医保！");
                return false;
           }   
        }
        
	}
	
	/*
	 *  #3849 modify by zxs 2018-05-09
	 *   福建分公司（机构代码8635）极其下属机构放开险种为
	 *  232401,123501,240901的缴费方式或续期方式为8医疗个人保险不能通过校验的权限的放开
	 */
	var riskSQL= " select code from ldcode1 where codetype='medicaloutrisk' and code1 like '"+fm.all('ManageCom').value+"%' ";
	var strSQL;
	//
	if(fm.all('ManageCom').value.indexOf('8635')==0){
	 strSQL = "select 1 from lcpol where prtno = '"+fm.PrtNo.value+"' " +
		"and riskcode not in (select code from ldcode1 where CodeType = 'checkappendrisk' " +
		"and code1 in ("+riskSQL+")) " +
		"and riskcode not in ("+riskSQL+") and riskcode not in ('232401','123501','240901','335201','231701','333801') with ur";
	}else{
		strSQL = "select 1 from lcpol where prtno = '"+fm.PrtNo.value+"' " +
		"and riskcode not in (select code from ldcode1 where CodeType = 'checkappendrisk' " +
		"and code1 in ("+riskSQL+")) " +
		"and riskcode not in ("+riskSQL+") with ur";	
	}
		
//
//	 var strSQL = "select 1 from lcpol where prtno = '"+fm.PrtNo.value+"' " +
//				"and riskcode not in (select code from ldcode1 where CodeType = 'checkappendrisk' " +
//				"and code1 in ("+riskSQL+")) " +
//				"and riskcode not in ("+riskSQL+") with ur";	




	var riskResult = easyExecSql(strSQL);
	if(riskResult){
	var payModeStr = "select paymode from lccont where prtno ='" + fm.PrtNo.value+"'";
	var payModeResult = easyExecSql(payModeStr);
	if(payModeResult){
		if(payModeResult[0][0] == '8'){
			alert("你录入了不支持医保个人账户转账的险种，请查看！");
			return false;
		}
		}
	}
	return true;
}

function getMedicalName(){
	var getName = "select lcc.paymode,ldm.medicalcomname from lccont lcc left join ldmedicalcom  ldm on lcc.bankcode = ldm.medicalcomcode  where prtno = '"+fm.PrtNo.value+"'  with ur";
	var Result = easyExecSql(getName);
	if(Result){
		if(Result[0][0] == '8'){
			fm.AppntBankCodeName.value = Result[0][1];
		}
	}
}

function checkMainSubRiskRela(){
	var getPolSQL = "select riskcode,mainpolno from lcpol where prtno = '"+fm.PrtNo.value+"' and mainpolno != polno ";
	var tPolResult = easyExecSql(getPolSQL);
	if(tPolResult){
		for(var i=0;i<tPolResult.length;i++){
			var getMainRiskSQL = "select riskcode from lcpol where polno = '"+tPolResult[i][1]+"' ";
			var tMainRiskResult = easyExecSql(getMainRiskSQL);
			if(tMainRiskResult){
				var getRelaSQL = "select 1 from ldcode1 where codetype = 'mainsubriskrela' and code = '"+tPolResult[i][0]+"' and code1 = '"+tMainRiskResult[0][0]+"' ";
				var tRelaResult = easyExecSql(getRelaSQL);
				if(!tRelaResult){
					alert("附加险"+tPolResult[i][0]+",与对应的主险"+tMainRiskResult[0][0]+",没有主附险关系！");
					return false;
				}
			}else{
				alert("获取险种"+tPolResult[i][0]+"的主险信息失败！");
				return false;
			}
		}
	}
	return true;
}

function check121501(){
	var getPolSQL = "select riskcode,mainpolno,mult from lcpol where prtno = '"+fm.PrtNo.value+"' and mainpolno != polno and riskcode = '121501' ";
	var tPolResult = easyExecSql(getPolSQL);
	if(tPolResult){
		for(var i=0;i<tPolResult.length;i++){
			var getMainRiskSQL = "select prem,mult,payintv,riskcode,insuyear,insuyearflag from lcpol where polno = '"+tPolResult[i][1]+"' ";
			var tMainRiskResult = easyExecSql(getMainRiskSQL);
			if(tMainRiskResult){
				if(tMainRiskResult[0][2] == "0"){
					alert("121501不能附加在主险缴费频次为趸缴的产品上!");
					return false;
				}
				/*
				if(parseFloat(tMainRiskResult[0][0]) < 1500){
					alert("121501选择的主险期交保费不能小于1500!");
					return false;
				}
				if((tPolResult[0][2] == "3" || tPolResult[0][2] == "4" || tPolResult[0][2] == "5") && parseFloat(tMainRiskResult[0][0]) < 3000){
					alert("121501选择3-5档时，主险期交保费须大于等于3000!");
					return false;
				}
				*/
				
				var tSumPremSQL = "select sum(lcp.prem) from lcpol lcp where lcp.polno = '"+tPolResult[i][1]+"' or (lcp.mainpolno = '"+tPolResult[i][1]+"' and lcp.riskcode != '121501' and exists (select 1 from ldcode1 where codetype = 'checkappendrisk' and code1 = '"+tMainRiskResult[0][3]+"' and code = lcp.riskcode)) ";
				var tSumPremResult = easyExecSql(tSumPremSQL);
				if(parseFloat(tSumPremResult[0][0]) < 1500){
					alert("121501选择的主险及绑定附加险期交保费和不能小于1500!");
					return false;
				}
				if((tPolResult[i][2] == "3" || tPolResult[i][2] == "4" || tPolResult[i][2] == "5") && parseFloat(tSumPremResult[0][0]) < 3000){
					alert("121501选择3-5档时，主险及绑定附加险期交保费和须大于等于3000!");
					return false;
				}
				
				var tRiskAppSQL = "select RiskPeriod,RiskType4 from lmriskapp where riskcode = '"+tMainRiskResult[i][3]+"' ";
				var tRiskAppResult = easyExecSql(tRiskAppSQL);
				if(tRiskAppResult){
					if(tRiskAppResult[0][0] != "L"){
						alert("121501附加的主险必须为长期险!");
						return false;
					}
					if(tRiskAppResult[0][1] == "4"){
						alert("121501不能附加在主险为万能型的产品上!");
						return false;
					}
				}else{
					alert("获取险种信息失败!");
					return false;
				}
			}
		}
	}
	return true;
}

function check332301(){
   var sql = "select distinct lcp.insuredno,lcp.riskcode from lcpol lcp,ldcode ld where lcp.riskcode = ld.code and ld.codetype='checkmainlawssame' and prtno = '"+fm.PrtNo.value+"' " ;
   var Result = easyExecSql(sql);
   var Result1;
   if(Result){
      var length=Result.length;
      for(var i=0;i<length;i++){
         var riskcode=Result[i][1];
         var insuredno=Result[i][0];
         //保险期间
         var  insuyear = "select 1 from lcpol a ,lcpol b where a.riskcode='"+riskcode+"' and a.mainpolno=b.polno "
                       + " and a.insuyear=b.insuyear and a.insuyearflag=b.insuyearflag and a.prtno = '"+fm.PrtNo.value+"' and a.insuredno='"+insuredno+"'" ;
         Result1 = easyExecSql(insuyear);
         if(!Result1){
            alert("险种"+riskcode+"与主险的保险期间不一致，请检查");
            return false;
         } 
         //缴费期间
         var payendyear =  "select 1 from lcpol a ,lcpol b where a.riskcode='"+riskcode+"' and a.mainpolno=b.polno "
                       + " and a.PayEndYear=b.PayEndYear and a.PayEndYearFlag=b.PayEndYearFlag and a.prtno = '"+fm.PrtNo.value+"' and a.insuredno='"+insuredno+"'" ;        
         Result1 = easyExecSql(payendyear);
         if(!Result1){
            alert("险种"+riskcode+"与主险的缴费期间不一致，请检查");
            return false;
         } 
         //缴费频次
         var payintv =  "select 1 from lcpol a ,lcpol b where a.riskcode='"+riskcode+"' and a.mainpolno=b.polno "
                     + " and a.PayIntv=b.PayIntv  and a.prtno = '"+fm.PrtNo.value+"' and a.insuredno='"+insuredno+"'" ;           
         Result1 = easyExecSql(payintv);
         if(!Result1){
            alert("险种"+riskcode+"与主险的缴费频次不一致，请检查");
            return false;
         }
      }     
   }
   return true;
}

function approveClick(){

   var arr = easyExecSql("select AppntName,AppntSex,AppntBirthday,AppntIDType,AppntIDNo,"
                       + "SaleChnl,AgentCom,AgentGroup,getUniteCode(AgentCode),GrpAgentCom,GrpAgentCode,GrpAgentName,"
                       + "Crs_SaleChnl,Crs_BussType,GrpAgentIDNo,AgentSaleCode,BankCode,BankAccNo"
                       + " from lccont where prtno='"+fm.PrtNo.value+"'");
            
   if(arr[0][0]!=fm.AppntName.value){
      alert("录入的投保人姓名与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][1]!=fm.AppntSex.value){
      alert("录入的投保人性别与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][2]!=fm.AppntBirthday.value){
      alert("录入的投保人出生日期与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][3]!=fm.AppntIDType.value){
      alert("录入的投保人证件类型与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][4]!=fm.AppntIDNo.value){
      alert("录入的投保人证件号码与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][5]!=fm.SaleChnl.value){
      alert("录入的销售渠道与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][6]!=fm.AgentCom.value){
      alert("录入的代理机构与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][8]!=fm.GroupAgentCode.value){
      alert("录入的业务员与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][9]!=fm.GrpAgentCom.value){
      alert("录入的对方机构代码与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][10]!=fm.GrpAgentCode.value){
      alert("录入的对方业务员代码与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][11]!=fm.GrpAgentName.value){
      alert("录入的对方业务员姓名与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][12]!=fm.Crs_SaleChnl.value){
      alert("录入的交叉销售渠道与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][13]!=fm.Crs_BussType.value){
      alert("录入的交叉销售业务类型与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][14]!=fm.GrpAgentIDNo.value){
      alert("录入的代理人组别与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][15]!=fm.AgentSaleCode.value){
      alert("录入的代理销售业务员与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][16]!=fm.AppntBankCode.value){
      alert("录入的开户行与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   if(arr[0][17]!=fm.AppntBankAccNo.value){
      alert("录入的银行帐号与系统中不一致，请确定录入的是否正确，如果正确，请先进行修改！");
      return false;
   }
   
   var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ; 
   showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
   var ttAction = fm.action; 
   fm.action="./ApproveContSave.jsp?LoadFlag=" + LoadFlag + "&CustomerType=00&customerNo="+fm.AppntNo.value ;
   fm.submit();
   fm.action=ttAction;
   
}

function afterSubmit1( FlagStr, content )
{

	showInfo.close();
	window.focus();
	mAction = ""; 
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}	
	else
	{ 
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
	}

}

function customerClick(){
     var sFeatures = "status:no;help:0;close:0;dialogWidth:500px;dialogHeight:500px;resizable=1";
	 showInfo = window.open( "./SuspectedCustomerInfo.html?cusType=0&cusNo="+fm.AppntNo.value+"&prtNo="+prtNo,"",sFeatures);
}

function checkAppInsr(relation,name,birthday){
    if(relation=="03"){
       var appday = easyExecSql("select AppntBirthday + 18 year from lcappnt where prtno='"+fm.PrtNo.value+"'");
       var fdate = new Date(Date.parse(appday[0][0].replace(/-/g,"/")));
       var cdate = new Date(Date.parse(birthday.replace(/-/g,"/")));
       if(Date.parse(cdate)-Date.parse(fdate) < 0){
           if(!confirm("被保人"+name+"与投保人年龄差距小于18岁，是否继续？")){
               return false;
           }
       }
    }
    return true;
}

function chenkIdNo(type,idtype,idno,name){
        var str = "";
        if(type=="1"){
          str="投保人";
        }else{
          str="被保人:"+name;
        }
        if(idtype == "" || idtype == null){
             alert("证件类型不能为空！");
             return false;
        }else{
             var arr = easyExecSql("select code1,codealias,othersign from ldcode1 where codetype='checkidtype' and code='"+idtype+"'");
             if(arr!=null){
                var othersign = arr[0][2];
                var first = arr[0][0];
                var two = arr[0][1];
                if("double"==othersign){
                   if(idno.length!=first&&idno.length!=two){
                       alert("录入的"+str+"证件号码有误，请检查！");
                       return false;
                   }
                }
                if("min"==othersign){
                   if(idno.length<first){
                      alert("录入的"+str+"证件号码有误，请检查！");
                       return false;
                   }
                }
                if("between"==othersign){
                   if(idno.length<first||idno.length>two){
                      alert("录入的"+str+"证件号码有误，请检查！");
                      return false;
                   }
                }
                
             }else{
                alert("选择的"+str+"证件类型有误，请检查！");
                return false;
             }
        }
        
        return true;
}

function chenkHomePhone(type,homecode,homenumber,flag){
     var str = "";
     if(type=="1"){
        str="投保人";
     }else{
        str="被保人";
     }
    if(homecode == "" || homecode == null){
         if(homenumber != "" && homenumber != null){
            alert(str+"固定电话区号未进行录入！");
            return false;
         }
    }else{
        if(homecode.length!=3 && homecode.length!=4||!isInteger(homecode)){
            alert(str+"固定电话区号录入有误，请检查！");
            return false;
        }
        if(homenumber == "" || homenumber == null){
            alert(str+"固定电话号码未进行录入！");
            return false;
        }
        if(homenumber.length!=7&&homenumber.length!=8&&homenumber.length!=10||!isInteger(homenumber)){
            alert(str+"固定电话号码录入有误，请检查！");
            return false;
        }else{
           if(homenumber.length==10){
               if(homenumber.substring(0, 3)!=400&&homenumber.substring(0, 3)!=800){
                  alert(str+"固定电话号码录入有误，请检查！");
                  return false;
               }
           }
        
        }
    
    }
    if(flag=='yes'){
        fm.AppntHomePhone.value=homecode+homenumber;
    }
    return true;
}

function checkAddress(type,PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity,flag,name){
   var str = "";
   if(type=="1"){
      str="投保人" ;
   }else{
      str="被保人" + name;
   }
   if(PostalProvince == "" || PostalProvince == null || PostalCity == "" || PostalCity == null
      || PostalCounty == "" || PostalCounty == null || PostalStreet == "" || PostalStreet == null
      || PostalCommunity == "" || PostalCommunity == null){
        alert(str+"联系地址未填写!");
        return false;
   }
   //校验省市县信息是否存在
   var provinceFlag =  easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+PostalProvince+"' ");
   if(!provinceFlag){
	   alert(str + "省（自治区直辖市）信息录入不正确，请录入省（自治区直辖市）编码！")
	   return false;
   }
   var cityFlag =  easyExecSql("select 1 from ldcode1 where codetype='city1' and code='"+PostalCity+"' ");
   if(!cityFlag){
	   alert(str +"市信息录入不正确，请录入市编码！")
	   return false;
   }
   var county1Flag =  easyExecSql("select 1 from ldcode1 where codetype='county1' and code='"+PostalCounty+"' ");
   if(!county1Flag){
	   alert(str +"县（区）信息录入不正确，请录入县（区）编码！")
	   return false;
   }
   //校验联系地址级联关系
   var checkCityError = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+PostalProvince+"' "
		   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+PostalCity+"' )");
   if(checkCityError == null || checkCityError == ""){
	   alert(str +"省、市地址级联关系不正确，请检查！");
	   return false;
   }
   var checkCityError2 = easyExecSql("select code1 from ldcode1 where codetype='city1' and code='"+PostalCity+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+PostalCounty+"')");
   if(checkCityError2 == null || checkCityError2 == ""){
	   alert(str +"市、县地址级联关系不正确，请检查！");
	   return false;
   }
   if(flag=='yes' ){
	   //当省级单位下没有市县级单位或市级单位下没有县级单位的时候改变拼接完整地址的方式
	   if(fm.appnt_PostalCity.value == '空' && fm.appnt_PostalCounty.value == '空'){
		   fm.AppntPostalAddress.value=fm.appnt_PostalProvince.value+fm.appnt_PostalStreet.value+fm.appnt_PostalCommunity.value;
	   }else if(fm.appnt_PostalCity.value != '空' && fm.appnt_PostalCounty.value == '空') {
		   fm.AppntPostalAddress.value=fm.appnt_PostalProvince.value+fm.appnt_PostalCity.value+fm.appnt_PostalStreet.value+fm.appnt_PostalCommunity.value;
	   }else{
		   fm.AppntPostalAddress.value=fm.appnt_PostalProvince.value + fm.appnt_PostalCity.value+fm.appnt_PostalCounty.value+fm.appnt_PostalStreet.value+fm.appnt_PostalCommunity.value;
	   }
   }
   return true;
}

function checkMobile(type,mobile,name){
   var str = "";
   if(type=="1"){
      str="投保人";
   }else{	
      str="被保人:"+name;
   }
   if(mobile != null && mobile !="") {
	    if(!isInteger(mobile) || mobile.length != 11){
		    alert(str+"移动电话需为11位数字，请核查！");
  	      	return false;
	    } 
	}
   if(mobile != "" && mobile != null){
      if(mobile.length!=11){
          alert(str+"移动电话不符合规则，请核实！");
          return false;
      }else{
         if(mobile.substring(0,2)!=13&&mobile.substring(0,2)!=14&&mobile.substring(0,2)!=15&&mobile.substring(0,2)!=16&&mobile.substring(0,2)!=18&&mobile.substring(0,2)!=17&&mobile.substring(0,2)!=19){
            alert(str+"移动电话不符合规则，请核实！");
            return false;
         }
      }
   }else if(type=="1"){
	      if(!confirm("投保人移动电话为空，是否继续？")){
         return false;
	  }
   }
   return true;
}

function checkAllapp(){
   var mobile = fm.AppntMobile.value;
   var idtype = fm.AppntIDType.value;
   var idno = fm.AppntIDNo.value;
   var homecode = trim(fm.AppntHomeCode.value);
   var homenumber = trim(fm.AppntHomeNumber.value);
   var PostalProvince = fm.Province.value;
   var PostalCity = fm.City.value;
   var PostalCounty = fm.County.value;
   var PostalStreet = fm.appnt_PostalStreet.value;
   var PostalCommunity = fm.appnt_PostalCommunity.value;

    if(!chenkIdNo(1,idtype,idno,'')){
	  return false;
	}
	
	if(!chenkHomePhone(1,homecode,homenumber,'yes')){
	  return false;
	}
	   var homephone = fm.AppntHomePhone.value;
	   if(!isNull(homephone)){
		   var checkhomephone =CheckFixPhone(homephone);
		   if(checkhomephone !=""){
			   alert(checkhomephone);
			   return false;
		   }
	   }
	if(!checkAddress(1,PostalProvince,PostalCity,PostalCounty,PostalStreet,PostalCommunity,'yes','')){
	  return false;
	}
	if(!checkMobile(1,mobile,'')){
	  return false;
	}
	
   return true;
}

function checkSaleChnlInfo(){
	var tSQL = "select managecom,agentcom,agentcode,salechnl from lccont where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单数据失败！");
		return false;
	}
	var tManageCom = arr[0][0];
	var tAgentCom = arr[0][1];
	var tAgentCode = arr[0][2];
	var tSaleChnl = arr[0][3];
	
	var tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
	var arrCode = easyExecSql(tSQLCode);
	if(!arrCode){
		alert("业务员与管理机构不匹配！");
		return false;
	}
	var agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
	                 + " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
                     + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
                     + "union all "
		             + "select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
		             + "and '10' = '" + tSaleChnl + "' and a.BranchType2 = '04'  "
		             + "union all "
		             + "select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
		             + "and '03' = '" + tSaleChnl + "' and a.BranchType2 = '04'  "
		             + "union all "
		             + "select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
		             + "and '02' = '" + tSaleChnl + "' and a.BranchType = '2' and a.BranchType2 = '02'  "
		             ;
    var arrAgentCode = easyExecSql(agentCodeSql);
    if(!arrAgentCode){
    	alert("业务员和销售渠道不匹配！");
		return false;
    }
	if(tSaleChnl == "03" || tSaleChnl == "04" || tSaleChnl == "10" || tSaleChnl == "15"|| tSaleChnl == "20"){
	    if(tAgentCom == "" || tAgentCom == null || tAgentCom == "null"){
	        alert("请录入中介机构！");
			return false;
	    }
		var tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
		var arrCom = easyExecSql(tSQLCom);
		if(!arrCom){
			alert("中介机构与管理机构不匹配！");
			return false;
		}
		var tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
		var arrCom = easyExecSql(tSQLComCode);
		if(!arrCom){
			alert("业务员与中介机构不匹配！");
			return false;
		}
	}
	return true;
}
//双击对方业务员框要显示的页面
function otherSalesInfo(ManageCom){
	window.open("../sys/MixedSalesAgentMain.jsp?ManageCom="+fm.ManageCom.value);
}
//对方业务员页面查询完“返回”按钮，接收数据
function afterQueryMIX(arrResult){
	if(arrResult!=null){
		var arr = arrResult.split("#");
		fm.GrpAgentCom.value = arr[0]; //对方机构代码
		//fm.GrpAgentComName.value = arr[1]; //对方机构名称
		fm.GrpAgentCode.value = arr[2]; //对方业务员代码
		fm.GrpAgentName.value = arr[3]; //对方业务员姓名
		fm.GrpAgentIDNo.value = arr[4]; //对方业务员身份证
		
	}else{
		alert("返回过程出现异常，请重新操作");
	}	
}

function checkMixCom(){
    var tSQLMix = "select prtno,Crs_SaleChnl,Crs_BussType,GrpAgentIDNo,GrpAgentCom,GrpAgentCode,GrpAgentName from lccont where prtno='" + fm.PrtNo.value + "'";
	var arrMix = easyExecSql(tSQLMix);
	if(!arrMix){
		alert("获取保单数据失败！");
		return false;
	}
	var sale= arrMix[0][1];
	var buss= arrMix[0][2];
	var id= arrMix[0][3];
	var com= arrMix[0][4];
	var code= arrMix[0][5];
	var name= arrMix[0][6];
	if(sale==""&&buss==""&&id==""&&com==""&&code==""&&name==""){
	    return true;
	}else{
	   if(sale!=""&&buss!=""&&id!=""&&com!=""&&code!=""&&name!=""){
	       if(code.length!=10){
	          alert("对方业务员工号不为集团统一工号！");
		      return false;
	       }else if((sale=="01"&&code.substring(0,1)!=1)||(sale=="02"&&code.substring(0,1)!=3)){
	          alert("对方业务员工号不为集团统一工号！");
		      return false;
	       }
	   }else{
	      alert("交叉销售信息填写有误，请对其进行修改！");
		  return false;
	   }
	}
    return true;
}

function controlNativeCity(displayFlag)
{
  if(fm.AppntNativePlace.value=="OS"){
    fm.all("NativePlace").style.display = displayFlag;
	fm.all("NativeCity").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="国家";
  }else if(fm.AppntNativePlace.value=="HK"){
    fm.all("NativePlace").style.display = displayFlag;
	fm.all("NativeCity").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="地区";
  }else{
    fm.all("NativePlace").style.display = "none";
	fm.all("NativeCity").style.display = "none";
  }
}

function checkNative(){
   if(fm.AppntNativePlace.value=="OS"||fm.AppntNativePlace.value=="HK"){
      if(fm.AppntNativeCity.value==""||fm.AppntNativeCity.value==null){
         alert("投保人所属国家/地区未录入！");
         return false;
      }
   }
   return true;
}

function checkCity(type,name,place,city){
   var str = "";
   if(type=="1"){
      str="投保人";
   }else{
      str="被保人:"+name;
   }
   if((place=="OS"||place=="HK")&&(city==""||city==null)){
      alert(str+"所属国家/地区不能为空！");
      return false;
   }

   return true;
}

function intoBnfInfo()
{
	window.open("./ComplexBnfMain.jsp?ContNo="+ContNo+"&prtNo="+fm.PrtNo.value,"", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}

function chenkBnfBlack(){
      var insuredSql= " select insuredno ,insuredname,paymode ,sum(paymoney) "
             + " from "
             + " (select insuredno ,insuredname,paymode ,"
             + "(Case When Payintv = '0' Then Prem + Nvl(Supplementaryprem, 0) "
             + " When Payyears <> 0 Then Prem * Double(12 / Payintv) * Payyears + Nvl(Supplementaryprem, 0) "
             + " When Payyears = 0 Then "
             + " Case When Payintv='12' Then Prem + Nvl(Supplementaryprem, 0) "
             + " Else  Prem * (to_month(payenddate)-to_month(cvalidate))/payintv + Nvl(Supplementaryprem, 0) End "
             + " End )paymoney "
             + " from lcpol where prtno='" + fm.PrtNo.value + "'"
             + " ) temp group by insuredno ,insuredname,paymode "
             +" having ((paymode='1' and sum(paymoney)>=20000) or (paymode<>'1' and sum(paymoney)>=200000)) ";
      var insuredResult = easyExecSql(insuredSql); 
      var bnfSql="select a.name from lcbnf a,lcpol b where BnfType='1' and (trim(a.NativePlace) is null or trim(a.NativePlace)=''"
                +" or trim(a.OccupationCode) is null or trim(a.OccupationCode)='' "
                +" or trim(a.Phone) is null or trim(a.Phone)='' "
                +" or trim(a.PostalAddress) is null or trim(a.PostalAddress)='' "
                +" or trim(a.IDStartDate) is null or trim(a.IDStartDate)='' "
                +" or trim(a.Sex) is null or trim(a.Sex)='' "
                +" or trim(a.BnfType) is null or trim(a.BnfType)='' "
                +" or trim(a.BnfGrade) is null or trim(a.BnfGrade)='' "
                +" or trim(a.BnfLot) is null or trim(a.BnfLot)='' "
                +" or trim(a.Name) is null or trim(a.Name)='' "
                +" or trim(a.IDType) is null or trim(a.IDType)='' "
                +" or trim(a.IDNo) is null or trim(a.IDNo)='' "
                +" or trim(a.IDEndDate) is null or trim(a.IDEndDate)='' ) "
                +" and (a.RelationToInsured not in ('01','02','03','04','05' ) "
                +" or trim(a.RelationToInsured) is null or trim(a.RelationToInsured)='' )"
                +" and a.contno=b.contno and a.polno=b.polno and b.prtno='" + fm.PrtNo.value + "' ";

      if(insuredResult){
		for(var index = 0; index < insuredResult.length; index++){
		    bnfSql+=" and a.insuredno='"+insuredResult[index][0]+"' ";
		    var bnfResult = easyExecSql(bnfSql); 
		    if(bnfResult){
		         alert("达“反洗钱”标准且非法定继承人以外的身故受益人："+bnfResult[0][0]+" 信息不全!请对其进行【受益人信息补录】！");
		         return false;
		    }		  
		}
	  }
          
      return true;
}

//体检资料
function showHealth() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cContNo = fm.ContNo.value;
  var cPrtNo = fm.PrtNo.value;

  if (cContNo != "") {
      //var tSelNo = PolAddGrid.getSelNo()-1;
      //var tNo = PolAddGrid.getRowColData(tSelNo,1);
      //showHealthFlag[tSelNo] = 1 ;
      window.open("../uw/UWManuHealthMain.jsp?ContNo1="+cContNo+"&MissionID="+tMissionID+"&SubMissionID="+tSubMissionID+"&PrtNo="+cPrtNo,"window1");
      showInfo.close();
    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}

//体检资料查询
function showHealthQ() {
       var Str="select 1 from LCpenotice where proposalcontno='"+fm.ContNo.value+"' ";
  var ARR = easyExecSql(Str);
  if(!ARR) {
      alert("未录入体检件!");
      return false;
    }	  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cContNo = fm.ContNo.value;
  var cPrtNo = fm.PrtNo.value;
  
  if (cContNo!= ""  ) {
      //var tSelNo = PolAddGrid.getSelNo()-1;
      //var tNo = PolAddGrid.getRowColData(tSelNo,1);
      //showHealthFlag[tSelNo] = 1 ;
      window.open("../uw/UWManuHealthQMain.jsp?ContNo="+cContNo+"&MissionID="+tMissionID+"&SubMissionID="+tSubMissionID+"&PrtNo="+cPrtNo+"&LoadFlag="+LoadFlag,"window1");
      showInfo.close();

    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}

function checkCard()
{
	//体检件回销校验
	var checkSql = "select MissionProp1 from LWMission where processid ='0000000003' and MissionProp2='"
	              + fm.ContNo.value + "' and activityid in ('0000001106','0000001111')";
	var result = easyExecSql(checkSql);
	if(result)
	{
		alert("体检件未回销，不能新单复核通过！");
		return false;
	}
	return true;
}

function getAudioAndVideo(){
  window.open("http://10.214.4.143:9001/prpcard/video/"+trim(fm.PrtNo.value)+".rar","window1");
  fm.VideoFlag.value="1"; 
}

function checkVideo(){
   var videoSql="select 1 from lcpol lcp,lmriskapp lm,lccont lcc where lcc.contno=lcp.contno and lcc.managecom like '8644%' "
               +"and lcc.contno='"+ fm.ContNo.value + "' and lcp.riskcode=lm.riskcode and Year(lcc.PolApplyDate - lcc.AppntBirthday)>=60 and lm.RiskPeriod='L' ";
   var result = easyExecSql(videoSql);
   if(result&&fm.VideoFlag.value!='1'){
      alert("请进行录音录像调阅！");
      return false;
   }
   videoSql="select 1 from lccont where managecom like '8691%' and contno='"+ fm.ContNo.value + "'";
   result = easyExecSql(videoSql);
   if(result&&fm.XSFlag.value!='Y'){
      alert("未进行销售过程全程记录，不能复核通过！");
      return false;
   }
   return true;
}

//为代码赋汉字
function setCodeName(fieldName, codeType, code)
{
  var sql = "select CodeName('" + codeType + "', '" + code + "') from dual";
  var rs = easyExecSql(sql)
  if(rs)
  {
    fm.all(fieldName).value = rs[0][0];
  }
}

function checkRelation(){
  var sql = "select 1 from lcinsured where prtno='"+fm.PrtNo.value+"' and SequenceNo='1' and RelationToMainInsured<>'00' ";
  var rs = easyExecSql(sql)
  if(rs)
  {
    alert("第一被保险人与第一被保险人关系只能为本人！");
	return false;
  }
  return true;
}

function queryGrpTax(){
        var strURL = "../sys/GrpTaxCommonQueryMain.jsp?PrtNo=" + fm.all('PrtNo').value+"&IDType="+fm.all('AppntIDType').value+"&IDNo="+fm.all('AppntIDNo').value+"&Name="+fm.all('AppntName').value;
        var newWindow = window.open(strURL, "GrpTaxCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
}

//税优返回数据
function afterQueryTax(arrResult){
	if(arrResult!=null){
		fm.GrpNo.value = arrResult[0][0]; 
		fm.GTaxNo.value = arrResult[0][2]; 
		fm.BatchNo.value = arrResult[0][5]; 
		fm.InsuredId.value = arrResult[0][8]; 
		fm.GOrgancomCode.value = arrResult[0][3];	
	}	
}
function displaySYinfo(PrtNo){
	var sql1 = "select taxflag,TaxPayerType from lccontsub lc   "

		+ " where prtno='" + fm.PrtNo.value + "' ";
		var TaxFlag = easyExecSql(sql1);
		if (TaxFlag) {
			if (TaxFlag[0][0] == '1' && TaxFlag[0][1] == '01') {
				fm.all('GTaxNo').readOnly = false;
				fm.all('GOrgancomCode').readOnly = false;
			}
			if (TaxFlag[0][0] == '1') {
				var sql = "select lc.taxflag,codename('taxflag',lc.taxflag),lc.taxno,lc.taxpayertype,codename('taxpayertype',lc.taxpayertype),lc.batchno,lc.insuredid,"
						+ " ls.grpno,lc.GTaxNo,lc.transflag,codename('transflag',lc.transflag)  "
						+ " ,lc.CreditCode,lc.GOrgancomCode,printtype,uuid,lc.premmult,codename('premmult',lc.premmult) "
						+ " from lccontsub lc  left join lsbatchinfo ls on ls.batchno=lc.batchno "
						+ " left join lsgrp lg on lg.grpno=ls.grpno "
						+ " where lc.prtno='" + fm.PrtNo.value + "' ";
				var rs = easyExecSql(sql);
				if (rs) {
					fm.TaxFlag.value = rs[0][0];
					fm.TaxFlagName.value = rs[0][1];
					fm.TaxNo.value = rs[0][2];
					fm.TaxPayerType.value = rs[0][3];
					fm.TaxPayerTypeName.value = rs[0][4];
					fm.BatchNo.value = rs[0][5];
					fm.InsuredId.value = rs[0][6];
					// fm.GrpNo.value = rs[0][7];
					fm.GTaxNo.value = rs[0][8];
					fm.TransFlag.value = rs[0][9];
					fm.TransFlagName.value = rs[0][10];
					fm.CreditCode.value = rs[0][11];
					fm.GOrgancomCode.value = rs[0][12];
					fm.printtype.value = rs[0][13];
					fm.uuid.value = rs[0][14];
					fm.PremMult.value = rs[0][15];
					fm.PremMultName.value = rs[0][16];
				}
			} else {
				var sql = "select lc.taxflag,codename('taxflag',lc.taxflag),lc.taxno,lc.taxpayertype,codename('taxpayertype',lc.taxpayertype),lc.batchno,lc.insuredid,"
						+ " ls.grpno,lg.TaxRegistration,lc.transflag,codename('transflag',lc.transflag)  "
						+ " ,lc.CreditCode,lg.OrgancomCode,printtype,uuid,lc.premmult,codename('premmult',lc.premmult) "
						+ " from lccontsub lc  left join lsbatchinfo ls on ls.batchno=lc.batchno "
						+ " left join lsgrp lg on lg.grpno=ls.grpno "
						+ " where lc.prtno='" + fm.PrtNo.value + "' ";
				var rs = easyExecSql(sql);
				if (rs) {
					fm.TaxFlag.value = rs[0][0];
					fm.TaxFlagName.value = rs[0][1];
					fm.TaxNo.value = rs[0][2];
					fm.TaxPayerType.value = rs[0][3];
					fm.TaxPayerTypeName.value = rs[0][4];
					fm.BatchNo.value = rs[0][5];
					fm.InsuredId.value = rs[0][6];
					fm.GrpNo.value = rs[0][7];
					fm.GTaxNo.value = rs[0][8];
					fm.TransFlag.value = rs[0][9];
					fm.TransFlagName.value = rs[0][10];
					fm.CreditCode.value = rs[0][11];
					fm.GOrgancomCode.value = rs[0][12];
					fm.printtype.value = rs[0][13];
					fm.uuid.value = rs[0][14];
					fm.PremMult.value = rs[0][15];
					fm.PremMultName.value = rs[0][16];
				}
			}
		}}
function checkTaxFlag(){	
	   if(fm.TaxFlag.value == '2'&& (fm.GrpNo.value=="" || fm.GrpNo.value==null)){
	         alert("税优标识为团体时，需录入团体客户信息！");
	         return false;
	   }
	   if(fm.TaxFlag.value == '2'&&(fm.TaxPayerType.value!='01')){
	         alert("税优标识为团体时，个税征收方式只能选择代扣代缴！");
	         return false;
	   }
	   if(fm.TaxFlag.value == '1'){
	    
	    
		   if((fm.TaxPayerType.value=='01')&&(fm.GTaxNo.value=="" || fm.GTaxNo.value==null)&&(fm.GOrgancomCode.value=="" || fm.GOrgancomCode.value==null)){
			   alert("税优标识为个人时，如个税征收方式选择为“代扣代缴”、则“单位税务登记证代码”或“单位社会信用代码”不能同时为空！");
		         return false;
		   }
		   else if((fm.TaxPayerType.value=='02')&&(fm.TaxNo.value=="" || fm.TaxNo.value==null)&&(fm.CreditCode.value=="" || fm.CreditCode.value==null)){
			   alert("税优标识为个人时，如个税征收方式选择为“自行申报”、则“个人税务登记证代码”或“个人社会信用代码”不能同时为空！");
		         return false;
		   }
		   if(fm.GrpNo.value!=""){
			   alert("税优投保标识为“个人”时，不可录入团体编号！");
		         return false; 
		   }
	   }
	   if(fm.TaxPayerType.value=='01'){
		if((fm.TaxNo.value!="")||(fm.CreditCode.value!="")){
		   alert("个税征收方式选择为“代扣代缴”时、不允许录入“个人税务登记证代码”或“个人社会信用代码”！");
	         return false;
		}
	   }
	   if(fm.TaxPayerType.value=='02'){
		   
			if((fm.GTaxNo.value!="")||(fm.GOrgancomCode.value!="")){
			   alert("个税征收方式选择为“自行申报”时、不允许录入“单位税务登记证代码”或“单位社会信用代码”！");
		         return false;
			}
		   }
	   
	   return true;
}

function checkSYAll(){

   var sql = "select * "
            + " from lcpol lc,lmriskapp lm "
            + " where lc.prtno='" + fm.PrtNo.value + "' "
            + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' ";
   var rs = easyExecSql(sql);
   var mobile;
   if(rs){
      sql = "select lc.taxflag,lc.taxno,lc.taxpayertype,lc.BatchNo,lc.transflag,lc.creditcode, "
            + "lc.GTaxNo,lc.GOrgancomCode from lccontsub lc "
            + " where lc.prtno='" + fm.PrtNo.value + "' ";
      rs = easyExecSql(sql);
      if(rs){
         var taxflag=rs[0][0];
         var taxno=rs[0][1];
         var payertype=rs[0][2];
         var batchno=rs[0][3];
         var transflag=rs[0][4];
         var creditcode=rs[0][5];
         var GTaxNo=rs[0][6];
         var GOrgancomCode=rs[0][7];
         
         if(taxflag==""||taxflag==null){
            alert("承保为税优产品，请填写税优投保标识！");
            return false;
         }
         if(transflag==""||transflag==null){
            alert("承保为税优产品，请填写投保单来源！");
            return false;
         }
         
         if(payertype==""||payertype==null){
            alert("承保为税优产品，请填写个税征收方式！");
            return false;
         }
         
//         if(taxflag=="1"){
//             alert("承保为税优产品，仅限团体投保！");
//             return false;
//          }
         
         if(taxflag=="1"&&((taxno==null||taxno=="")&&(creditcode==null||creditcode=="")&&(GTaxNo==null||GTaxNo=="")&&(GOrgancomCode==null||GOrgancomCode==""))){
            alert("税优投保标识为个人，个人税务登记证、个人社会信用代码、单位税务登记证代码 、单位社会信用代码 信息需录入一项！");
            return false;
         }
         if(taxflag=="2"&&(batchno==null||batchno=="")){
            alert("税优投保标识为团体，需录入团体客户信息！");
            return false;
         }   
      }else{
    	     alert("承保为税优产品，请确认是否录入了税优相关字段！");
    	     return false;
      }
      
      sql = "select 1 "
          + " from lcpol lc,lmriskapp lm,LCCustomerImpartParams li "
          + " where lc.prtno='" + fm.PrtNo.value + "' "
          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
          + " and lc.contno=li.contno and lc.insuredno=li.customerno"
          + " and li.impartcode='170' and li.impartver='001' and li.impartparam='Y' ";
      rs = easyExecSql(sql);
      if(!rs){
          alert("被保险人医疗费用支付方式未勾选！");
          return false;
      }
      
//      sql = "select 1 "
//          + " from lcpol lc,lmriskapp lm,LCCustomerImpartParams li "
//          + " where lc.prtno='" + fm.PrtNo.value + "' "
//          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
//          + " and lc.contno=li.contno and lc.insuredno=li.customerno"
//          + " and li.impartcode='170' and li.impartver='001' and li.impartparam='Y' "
//          + " and exists (select 1 from ldcode1 where codetype='SYRiskNotImpartCode' and code=lc.riskcode and code1=li.impartparamno )";
//      rs = easyExecSql(sql);
//      if(rs){
//          alert("被保险人医疗费用支付方式与险种不匹配！");
//          return false;
//      }
      
      sql = "select li.customerno,count(distinct li.impartcode) "
          + " from lcpol lc,lmriskapp lm,LCCustomerImpart li "
          + " where lc.prtno='" + fm.PrtNo.value + "' "
          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
          + " and lc.contno=li.contno and lc.insuredno=li.customerno"
          + " and li.impartcode in ('010','170','180','190','110','260','270') and li.impartver='001' "
          + " group by li.customerno having count(distinct li.impartcode)<7 ";
      rs = easyExecSql(sql);      
      if(rs){
          alert("承保为税优产品，被保人告知第：1、2、3、4、12、20、21 项 为必录项！");
          return false;
      }
      
      sql =  "select 1 "
          + " from lcpol lc,lmriskapp lm  "
          + " where lc.prtno='" + fm.PrtNo.value + "' "
          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
          + " and not exists (select 1 from LCCustomerImpart where impartcode ='240' and impartver='001' and customerno=lc.insuredno and contno=lc.contno )"
          + " and lc.insuredsex='1' and lc.insuredappage>=16  ";
      rs = easyExecSql(sql);
      if(rs){
          alert("承保为税优产品，被保人为16周岁及以上女性，告知第18项为必录项！");
          return false;
      }
      
      sql =  "select 1 "
          + " from lcpol lc,lmriskapp lm  "
          + " where lc.prtno='" + fm.PrtNo.value + "' "
          + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
          + " and not exists (select 1 from ldcode where codetype='sypremnult' and code=lc.riskcode) "
          + " and exists (select 1 from LCCustomerImpartParams where impartcode ='260' and impartver='001' and customerno=lc.insuredno and contno=lc.contno and impartparam='Y')"
          + " and lc.mult<>2  ";
      rs = easyExecSql(sql);
      if(rs){
          alert("承保为税优产品，被保人投保保额不符合要求！");
          return false;
      }
      
      sql = "select 1 "
            + " from lcappnt lca,lcaddress lcd "
            + " where lca.prtno='" + fm.PrtNo.value + "' "
            + " and lca.appntno=lcd.customerno and lca.addressno=lcd.addressno And (Length(Trim(lcd.Mobile)) <> 11 Or lcd.Mobile Is Null Or lcd.Mobile = '' Or  lcd.Mobile not like '1%') "
            + " union all "
            + " select 1 "
            + " from lcinsured lca,lcaddress lcd "
            + " where lca.prtno='" + fm.PrtNo.value + "' "
            + " and lca.insuredno=lcd.customerno and lca.addressno=lcd.addressno And (Length(Trim(lcd.Mobile)) <> 11 Or lcd.Mobile Is Null Or lcd.Mobile = '' Or  lcd.Mobile not like '1%') ";
     rs = easyExecSql(sql);
     if(rs){
         alert("承保为税优产品，投、被保人移动电话为必录项，请核查是否录入，录入是否正确！");
         return false;
     }
   }else {
	   if(fm.TaxFlag.value!=""||fm.TransFlag.value!=""||fm.Supdiscountfactor.value!=""||fm.Grpdiscountfactor.value!=""){
		   alert("未承保税优产品,请检查是否录入了税优专属字段")
		   return false;
	   }
   }
   return true;  
    
}

function checkPremMult(){
	var sql1 = "select premmult from lccontsub where prtno='"+ fm.PrtNo.value + "' ";
	var arr1 = easyExecSql(sql1);
	if(!arr1){
		alert("信息录入不全，请核实！");
		return false;
	}
	var tPremMult=arr1[0][0];
	var sql = "select distinct riskcode from lcpol where prtno='"+ fm.PrtNo.value + "' ";
	var arr = easyExecSql(sql);
	if(!arr){
		alert("未录入险种信息，请核实！");
		return false;
	}
	for(var i=0;i<arr.length;i++){
		var tsql = "select 1 from ldcode where codetype='sypremnult' and code='"+ arr[i][0] + "' ";		
		var arrtsql = easyExecSql(tsql);
		if(arrtsql&&(tPremMult==null||tPremMult=="")){
			alert("对于险种"+arr[i][0]+"被保险人风险保险费档次字段必录！");
			return false;
		}		
		if(!arrtsql&&tPremMult!=""){
			alert("险种"+arr[i][0]+"不可录入被保险人风险保险费档次字段！");
			return false;
		}
		if(arrtsql){
			//险种风险保费和页面录入被保险人风险保险费档次对比
			var ttsql = " select 1 from lcpol lc where prtno='"+ fm.PrtNo.value + "' "
					  + " and riskcode ='"+ arr[i][0] + "' "
					  + " and mult='"+ tPremMult +"' "
			var arrttsql = easyExecSql(ttsql); 
			if(!arrttsql){
				alert("风险保费档次与保障计划不符！");
				return false;
			}
			//告知与页面录入被保险人风险保险费档次对比
			var ttsql1 = "select 1 from LCCustomerImpartParams where prtno='"+ fm.PrtNo.value + "' "
				       + "and impartcode='170' and impartparam='N' and impartparamno='5' "
			var arrttsql1 = easyExecSql(ttsql1); 
			if(arrttsql1&&tPremMult=="2"){
				alert("被保险人医疗费用支付方式与风险保险费档次不匹配！");
				return false;
			}
			var ttsql2 =  "select 1 "
		          	   + " from lcpol lc "
		          	   + " where lc.prtno='" + fm.PrtNo.value + "' "
		          	   + " and exists (select 1 from ldcode where codetype='sypremnult' and code=lc.riskcode) "
		          	   + " and exists (select 1 from LCCustomerImpartParams where impartcode ='260' and impartver='001' and customerno=lc.insuredno and contno=lc.contno and impartparam='Y')"
		          	   + " and lc.amnt='250000' ";
			var arrttsql2 = easyExecSql(ttsql2); 
			if(arrttsql2){
				alert("承保为税优产品，被保人投保保额不符合要求！");
				return false;
			}
		}
	}
	return true;  
}

function SYCheck(){
    var sql = "select * "
            + " from lcpol lc,lmriskapp lm "
            + " where lc.prtno='" + fm.PrtNo.value + "' "
            + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' ";
   var rs = easyExecSql(sql);
   if(!rs){
      alert("未承保税优产品，不需要进行税优验证！");
      return false;
   }
   var showStr = "正在进行验证，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ; 
   showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
   var ttAction = fm.action; 
   fm.action="./SYCheckConfirm.jsp";
   fm.submit();
   fm.action=ttAction;   
}

function afterSubmit2( FlagStr, content )
{	
	showInfo.close();
	window.focus();
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}

}

function TaxPolInfo(){

   window.open("./TransContInfoMain.jsp?PrtNo="+fm.PrtNo.value+"&AddFlag=1","", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}

function checkTrans(){

   var sql = "select 1 "
            + " from lcpol lcp,lccontsub lcc,lmriskapp lm "
            + " where lcp.prtno='" + fm.PrtNo.value + "' "
            + " and lcp.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
            + " and lcp.prtno=lcc.prtno and lcc.transflag<>'1' "
            + " and exists (select 1 from lstransinfo where prtno=lcc.prtno)";
   var rs = easyExecSql(sql);
   if(rs){
      alert("该单不为税优转入保单，请对税优转入保单信息进行删除！");
      return false;
   }
   
   sql = "select lcp.agentcode,lcp.managecom "
            + " from lcpol lcp,lccontsub lcc,lmriskapp lm "
            + " where lcp.prtno='" + fm.PrtNo.value + "' "
            + " and lcp.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
            + " and lcp.prtno=lcc.prtno and lcc.transflag='1' ";
   rs = easyExecSql(sql);
   if(rs){
      sql = "select case when ls.agentcode=lc.agentcode then '1' else '0' end,case when 1=(select count(1) from ldfinbank where bankaccno=ls.accno and managecom=lc.managecom and bankcode=ls.bankcode and finflag<>'F') then '1' else '0' end "
          + " from lstransinfo ls,lccont lc where lc.prtno='" + fm.PrtNo.value + "' and lc.prtno=ls.prtno ";
      rs = easyExecSql(sql);
      if(!rs){
         alert("该单为税优转入保单，请进行税优转入保单信息录入！");
         return false;
      }else{
         var flag1=rs[0][0];
         var flag2=rs[0][1];
         if(flag1=='0'){
            alert("税优转入保单录入联系人与投保单业务员不为同一人！");
            return false;
         }
         if(flag2=='0'){
            alert("税优转入保单录入归集帐号不为投保单管理机构归集帐号！");
            return false;
         }     
      }
      
   }
   
   sql = "select CheckFlag,CheckInfo,lcc.transflag "
      + " from lccontsub lcc,lcpol lcp,lmriskapp lm where lcc.prtno='" + fm.PrtNo.value + "' "
      + " and lcc.prtno=lcp.prtno and lcp.riskcode=lm.riskcode and lm.TaxOptimal='Y' ";
   rs = easyExecSql(sql);
   if(rs){
       var check=rs[0][0];
       var checkinfo=rs[0][1];
       var transflag=rs[0][2];
       if(rs[0][0]=='99'&&transflag!='1'){
          alert("该单为税优产品保单，该单税优验证失败，失败原因为："+checkinfo);
          return false;
       }
       if(rs[0][0]!='00'&&rs[0][0]!='99'){
          alert("该单为税优产品保单，需进行税优唯一性验证！");
          return false;
       }
   }
       
   return true;
}

function chagePostalAddress(flag){
	if(fm.appnt_PostalCity.value == '空' ){
		   fm.AppntPostalAddress.value=fm.appnt_PostalProvince.value+fm.appnt_PostalStreet.value+fm.appnt_PostalCommunity.value;
	   }else if(fm.appnt_PostalCity.value != '空' && fm.appnt_PostalCounty.value == '空') {
		   fm.AppntPostalAddress.value=fm.appnt_PostalProvince.value+fm.appnt_PostalCity.value+fm.appnt_PostalStreet.value+fm.appnt_PostalCommunity.value;
	   }else{
		   fm.AppntPostalAddress.value=fm.appnt_PostalProvince.value+fm.appnt_PostalCity.value+fm.appnt_PostalCounty.value+fm.appnt_PostalStreet.value+fm.appnt_PostalCommunity.value;
	   }
	//alert("nihao");
}
function checkBonusGetMode(){
	var rs4 =easyExecSql( "select a.risktype4,b.bonusgetmode from lmriskapp a, (select riskcode,BonusGetMode from lcpol where prtno = '"+fm.PrtNo.value+"') b where a.riskcode =b.riskcode");
	if(rs4 != "" && rs4 != null){
		for(var i=0;i<rs4.length;i++){
			if(rs4[i][0] == "2" && rs4[i][1] != "1" && rs4[i][1] != "2"){
				alert("分红险必须录入红利领取方式：1或2！");
				return false;
			}
		}
	}
	var rs5 = easyExecSql("select insuredno from lcpol where prtno='"+fm.PrtNo.value+"' with ur");
	if(rs5 != "" && rs5 != null){
		for(var k=0; k<rs5.length;k++){
			var rs6= easyExecSql("select 1 from lcpol where insuredno = '"+rs5[k][0]+"' and  prtno='"+fm.PrtNo.value+"' and riskcode='340601' with ur");
			if(rs6 == 1){
				var rs7 = easyExecSql("select bonusgetmode from lcpol where insuredno = '"+rs5[k][0]+"' and  prtno='"+fm.PrtNo.value+"' and riskcode='730503' with ur");
				if(rs7 != null && rs7 !="" && rs7 != 2){
					alert("730503与340601同时投保时，红利领取方式只能选择“累积生息”。");
					return false;
				}
			}
		}
	}
	return true;
}
//销售渠道与交叉销售校验	
function checkSalechnl_CrsBuss(){
	var sql = "select salechnl from lccont where prtno = '"+fm.PrtNo.value+"'";
	var sql2 = "select Crs_BussType from lccont where prtno = '"+fm.PrtNo.value+"'";
	var salechnl = easyExecSql(sql);
	var Crs_BussType = easyExecSql(sql2);
	
	//社保渠道不能有交叉销售信息
	if ((salechnl=="16" || salechnl=="18" || salechnl=="20") && (Crs_BussType != "")) {
			alert("社保渠道不能有交叉销售信息！");
			return false;
	}else if (salechnl == "14" && Crs_BussType != "") {
		if (Crs_BussType != "02" && Crs_BussType != "03" && Crs_BussType != "13" && Crs_BussType != "14") {
			alert("销售渠道为互动直销时，只可选择联合展业、渠道共享、互动部、农网共建！");
			return false;
		}
	}else if (salechnl == "15" && Crs_BussType != "") {
		if (Crs_BussType != "01") {
			alert("销售渠道为互动中介时，只可选择相互代理！");
			return false;
		}
	}else if (salechnl != "14" && salechnl != "15" && salechnl != "16" && salechnl != "18" && salechnl != "20" && Crs_BussType != "") {
		if (Crs_BussType != "01" && Crs_BussType != "02") {
			alert("非社保渠道、互动中介和互动直销时，只可选择相互代理或者联合展业！");
			return false;
		}
	}
	
	return true;
}

function dealAppointDiscount(num){
	if(num !=""){
		var reg = /(^[-+]?[1-9]\d*(\.\d{1,2})?$)|(^[-+]?[0]{1}(\.\d{1,2})?$)/;
		if (!reg.test(num)) {
            alert("约定折扣必须为合法数字(正数，最多两位小数)！");
            return false;
        } 
		if(num < 0.7){
			alert("约定折扣不能小于0.7！");
			return false;
		}
		fm.Totaldiscountfactor.value =fm.AppointDiscount.value;
	}else{
		alert("请录入约定折扣！");
		return false;
	}
	return true;
}


function checktaxCustomerImpart(){
	var sql = "select salechnl from lccont where prtno = '"+fm.PrtNo.value+"'";
	var salechnl = easyExecSql(sql);
	if (salechnl=="01") {
		sql =  "select 1 "    
	        + " from lcpol lc,lmriskapp lm  "
	        + " where lc.prtno='" + fm.PrtNo.value + "' "
	        + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
	        + " and  exists (select 1 from LCCustomerImpart where impartcode in('110','260','270') and impartver='001' and customerno=lc.insuredno and ProposalContNo=lc.contno and impartparammodle='Y' )";
		rs = easyExecSql(sql);
	    if(rs){
	        alert("健康状况存在异常，不允许通过本销售渠道投保。");
	        return false;
	    }
	
     }
	return true;  
}

function checktaxnoAndcreditcode(){
	sql = "select lc.taxno,lc.creditcode, "
	    + "lc.GTaxNo,lc.GOrgancomCode,lc.taxflag from lccontsub lc "
	    + " where lc.prtno='" + fm.PrtNo.value + "' ";
	rs = easyExecSql(sql);
	 if(rs){
         var taxno=rs[0][0];
         var creditcode=rs[0][1];
         var GTaxNo=rs[0][2];
         var GOrgancomCode=rs[0][3];
         var taxflag=rs[0][4];
         if(taxflag!=""&&taxflag!=null){
        	 if(taxno!=""&&taxno!=null&&taxno.length!=15&&taxno.length!=18&&taxno.length!=20){
                 alert("社会信用代码或税务登记号长度不符合要求！");
                 return false;
               }
      	     if(GTaxNo!=""&&GTaxNo!=null&&GTaxNo.length!=15&&GTaxNo.length!=18&&GTaxNo.length!=20){
                 alert("社会信用代码或税务登记号长度不符合要求！");
                 return false;
               }
      	     if(creditcode!=""&&creditcode!=null&&creditcode.length!=18){
                 alert("社会信用代码或税务登记号长度不符合要求！");
                 return false;
               }
      	     if(GOrgancomCode!=""&&GOrgancomCode!=null&&GOrgancomCode.length!=18){
                 alert("社会信用代码或税务登记号长度不符合要求！");
                 return false;
               } 
          } 
	 }
	 return true; 
}

//投保书图像查询
function ScanQuery(){
	showinfo=window.open("./TBImageQueryMain.jsp?PrtNo=" + fm.PrtNo.value + "&ManageCom=" + fm.all('ManageCom').value);
	/*fm.submit();*/ 
}

