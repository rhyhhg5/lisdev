//该文件中包含客户端需要处理的函数和事件

//程序名称：ContPlanShareAmnt.js
//程序功能：共用保额要素维护
//创建日期：2010-04-06
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();


//查询按钮
function searchGrpCont( )
{
	if ((fm.GrpContNo.value==null || fm.GrpContNo.value=="")
	    &&(fm.PrtNo.value==null || fm.PrtNo.value=="")) {
	    	alert("请输入印刷号或保单号");
	    	return false;
	}
	var strSql = " SELECT a.prtno,a.grpcontno,a.grpname,a.cvalidate,a.cinvalidate " +
  " FROM lcgrpcont a  "
  +"WHERE a.appflag='1' " 
  + " and a.managecom like '"+fm.ManageCom.value+"%'"
  + getWherePart("a.grpcontno","GrpContNo")
  + getWherePart("a.prtno","PrtNo")
  + " union all SELECT a.prtno,a.grpcontno,a.grpname,a.cvalidate,a.cinvalidate " +
  " FROM lbgrpcont a  "
  +"WHERE a.appflag='1' " 
  + " and a.managecom like '"+fm.ManageCom.value+"%'"
  + getWherePart("a.grpcontno","GrpContNo")
  + getWherePart("a.prtno","PrtNo");
  
  turnPage.queryModal(strSql,Mul12Grid);
  
  initMul11Grid();
	initMul10Grid();

}

//保存按钮
function saveDutyParam( )
{
	var tCount = 1;
	var tValue = "";
	if (Mul10Grid.mulLineCount < 1) {
		alert("没有可以保存的共用保额要素");
		return false;
		
	}
	for(var n=0;n<Mul10Grid.mulLineCount;n++) {
		if (Mul10Grid.getRowColDataByName(n,"CalFactorValue")!=""
		   &&Mul10Grid.getRowColDataByName(n,"CalFactorValue")!=null) {
		   	if (!isNumeric(Mul10Grid.getRowColDataByName(n,"CalFactorValue"))) {
				alert("共用保额要素值必须是数字");
				Mul10Grid.setFocus(n,8);
				return false;
			}

		    if (tCount == 1) {
		      tValue = Mul10Grid.getRowColDataByName(n,"CalFactorValue");
		    } else {
		    	if (tValue != Mul10Grid.getRowColDataByName(n,"CalFactorValue")) {
		    		alert("共用保额要素值必须一致");
				    return false;
		        }
		    }
		    tCount = tCount + 1;
		}
    }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.submit(); //提交
}


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function onSelSearchPlan() {
	var selNo = Mul12Grid.getSelNo();
	fm.GrpContNo.value= Mul12Grid.getRowColDataByName(selNo-1,"GrpContNo");
	fm.mGrpContNo.value= Mul12Grid.getRowColDataByName(selNo-1,"GrpContNo");
	fm.PrtNo.value = Mul12Grid.getRowColDataByName(selNo-1,"PrtNo");
	searchPlan();
}

function searchPlan() {
	if (fm.mGrpContNo.value==null || fm.mGrpContNo.value=="") {
		return false;
	}	
	
	var strSql = " select ContPlanCode,ContPlanName,Peoples3,Peoples2 " 
	           + " FROM lccontplan where ContPlanCode <>'11' "
	           + getWherePart("grpcontno","mGrpContNo")
	           + " union all select ContPlanCode,ContPlanName,Peoples3,Peoples2 " 
	           + " FROM lbcontplan where ContPlanCode <>'11' "
	           + getWherePart("grpcontno","mGrpContNo")
	           ;
  
  turnPage1.queryModal(strSql,Mul11Grid);		
}

function onSelSearchDutyParam() {
	var selNo = Mul11Grid.getSelNo();
	fm.ContPlanCode.value = Mul11Grid.getRowColDataByName(selNo-1,"ContPlanCode");
	searchDutyParam();
}

function searchDutyParam() {
	if (fm.GrpContNo.value == null || fm.GrpContNo.value == "" 
	    || fm.ContPlanCode.value == null || fm.ContPlanCode.value == "") {
		return false;
	}
	var strSQL = "select distinct X.riskname,X.riskcode,X.dutycode,X.dutyname,X.calfactor,X.FactorName,"
	           + " X.FactorNoti,d.CalFactorValue,d.Remark,X.RiskVer,X.GrpPolNo,X.MainRiskCode,X.CalFactorType,X.PayPlanCode,X.GetDutyCode,"
	           + " X.InsuAccNo"
	           + " from "
	           + " (select b.riskname riskname,a.RiskCode riskcode,a.DutyCode dutycode,c.DutyName dutyname,"
	           + " a.CalFactor calfactor,a.FactorName factorname,a.FactorNoti FactorNoti,b.RiskVer RiskVer,f.GrpPolNo GrpPolNo,e.MainRiskCode MainRiskCode,"
	           + " a.PayPlanCode PayPlanCode,a.GetDutyCode GetDutyCode,a.InsuAccNo InsuAccNo,e.grpcontno grpcontno,a.CalFactorType,e.contplancode contplancode "
	           + " from LMRiskDutyFactor a,LMRisk b,LMDuty c,LCContPlanRisk e,LCGrpPol f,LCContPlanDutyParam g "
	           + " where a.riskcode=b.riskcode and a.dutycode=c.dutycode and a.riskcode=e.riskcode "
	           + " and f.grpcontno=e.grpcontno and f.riskcode=a.riskcode "
	           + " and f.riskcode = g.riskcode and f.grpcontno = g.grpcontno and g.dutycode = a.dutycode "
	           + " and e.contplancode = g.contplancode "
	           + " and a.calfactor='ShareAmnt' and e.GrpContNo = '"
	           + fm.mGrpContNo.value
	           + "' and e.contplancode = '"+fm.ContPlanCode.value+"') as X "
	           + " left join LCContPlanDutyParam d on X.riskcode=d.riskcode and X.dutycode=d.dutycode "
	           + " and X.grpcontno=d.grpcontno and X.calfactor=d.calfactor and X.contplancode=d.contplancode "
	           + " union all select distinct X.riskname,X.riskcode,X.dutycode,X.dutyname,X.calfactor,X.FactorName,"
	           + " X.FactorNoti,d.CalFactorValue,d.Remark,X.RiskVer,X.GrpPolNo,X.MainRiskCode,X.CalFactorType,X.PayPlanCode,X.GetDutyCode,"
	           + " X.InsuAccNo"
	           + " from "
	           + " (select b.riskname riskname,a.RiskCode riskcode,a.DutyCode dutycode,c.DutyName dutyname,"
	           + " a.CalFactor calfactor,a.FactorName factorname,a.FactorNoti FactorNoti,b.RiskVer RiskVer,f.GrpPolNo GrpPolNo,e.MainRiskCode MainRiskCode,"
	           + " a.PayPlanCode PayPlanCode,a.GetDutyCode GetDutyCode,a.InsuAccNo InsuAccNo,e.grpcontno grpcontno,a.CalFactorType,e.contplancode contplancode "
	           + " from LMRiskDutyFactor a,LMRisk b,LMDuty c,LBContPlanRisk e,LBGrpPol f,LBContPlanDutyParam g "
	           + " where a.riskcode=b.riskcode and a.dutycode=c.dutycode and a.riskcode=e.riskcode "
	           + " and f.grpcontno=e.grpcontno and f.riskcode=a.riskcode "
	           + " and f.riskcode = g.riskcode and f.grpcontno = g.grpcontno and g.dutycode = a.dutycode "
	           + " and e.contplancode = g.contplancode "
	           + " and a.calfactor='ShareAmnt' and e.GrpContNo = '"
	           + fm.mGrpContNo.value
	           + "' and e.contplancode = '"+fm.ContPlanCode.value+"') as X "
	           + " left join LBContPlanDutyParam d on X.riskcode=d.riskcode and X.dutycode=d.dutycode "
	           + " and X.grpcontno=d.grpcontno and X.calfactor=d.calfactor and X.contplancode=d.contplancode "
	           ;
	turnPage2.queryModal(strSQL,Mul10Grid);	
}

