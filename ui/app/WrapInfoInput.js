//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

//查询事件
function easyQueryClick()
{
    var canSale = "";
    if(fm.canSale.value == "0")
        canSale = " and w.WrapType != '' ";
    else if(fm.canSale.value == "1")
        canSale = " and (w.WrapType is null or char(w.WrapType) = '') ";
    
    var strSQL = "select distinct w.RiskWrapCode, w.WrapName, "
        + "case r.RiskWrapType when 'G' then '团险' when 'I' then '个险' end, "
        + "case w.WrapType when '2' then '意外' when '5' then '电销' when '6' then '银邮' when '8' then '银保万能' when '9' then '国际业务' end "
        + "from LDWrap w, LDRiskWrap r "
        + "where w.RiskWrapCode = r.RiskWrapCode " + canSale 
        + getWherePart('w.RiskWrapCode', 'riskWrapCode')
        + getWherePart('r.RiskWrapType', 'riskWrapType')
        + getWherePart('w.WrapType', 'wrapType')
        + " order by w.RiskWrapCode with ur";

    turnPage.pageDivName = "divPage";
    turnPage.queryModal(strSQL, WrapInfoGrid);

    initRiskInfoGrid();
    initDutyInfoGrid();
    fm.querySql.value = strSQL;
}

//查询事件
function wrapInfoDetail()
{
    initRiskInfoGrid();
    initDutyInfoGrid();
    var row = WrapInfoGrid.getSelNo() - 1;
    var wrapCode = WrapInfoGrid.getRowColDataByName(row, "wrapCode");
    
    var strSQL = "select a.RiskWrapCode, a.RiskCode, b.RiskName, a.MainRiskCode, a.Prem "
        + "from LDRiskWrap a, LMRiskApp b "
        + "where a.RiskCode = b.RiskCode "
        + "and a.RiskWrapCode = '" + wrapCode + "' "
        + "order by a.RiskWrapCode, a.RiskCode with ur";

    turnPage1.pageDivName = "divPage1";
    turnPage1.queryModal(strSQL, RiskInfoGrid);
}

//查询事件
function riskInfoDetail()
{
    initDutyInfoGrid();
    var row = WrapInfoGrid.getSelNo() - 1;
    var wrapCode = WrapInfoGrid.getRowColDataByName(row, "wrapCode");
    row = RiskInfoGrid.getSelNo() - 1;
    var riskCode = RiskInfoGrid.getRowColDataByName(row, "riskCode");
    
    //合并要素值
    //select distinct a.RiskWrapCode, a.RiskCode, a.DutyCode, b.DutyName, 
    //(select CalFactorValue from LDRiskDutyWrap d where a.RiskWrapCode = d.RiskWrapCode and a.RiskCode = d.RiskCode and a.DutyCode = d.DutyCode and CalFactor = 'Prem') Prem, 
    //(select CalFactorValue from LDRiskDutyWrap d where a.RiskWrapCode = d.RiskWrapCode and a.RiskCode = d.RiskCode and a.DutyCode = d.DutyCode and CalFactor = 'Amnt') Amnt, 
    //(select CalFactorValue from LDRiskDutyWrap d where a.RiskWrapCode = d.RiskWrapCode and a.RiskCode = d.RiskCode and a.DutyCode = d.DutyCode and CalFactor = 'Mult') Mult, 
    //(select CalFactorValue from LDRiskDutyWrap d where a.RiskWrapCode = d.RiskWrapCode and a.RiskCode = d.RiskCode and a.DutyCode = d.DutyCode and CalFactor = 'InsuYear') InsuYear, 
    //(select CalFactorValue from LDRiskDutyWrap d where a.RiskWrapCode = d.RiskWrapCode and a.RiskCode = d.RiskCode and a.DutyCode = d.DutyCode and CalFactor = 'InsuYearFlag') InsuYearFlag, 
    //(select CalFactorValue from LDRiskDutyWrap d where a.RiskWrapCode = d.RiskWrapCode and a.RiskCode = d.RiskCode and a.DutyCode = d.DutyCode and CalFactor = 'GetLimit') GetLimit, 
    //(select CalFactorValue from LDRiskDutyWrap d where a.RiskWrapCode = d.RiskWrapCode and a.RiskCode = d.RiskCode and a.DutyCode = d.DutyCode and CalFactor = 'GetRate') GetRate  
    //from LDRiskDutyWrap a, LMDuty b where a.DutyCode = b.DutyCode and a.RiskWrapCode = 'WR0011' 
    //and a.RiskCode = '5503' 
    //order by a.DutyCode with ur

    var strSQL = "select a.RiskWrapCode, a.RiskCode, a.DutyCode, b.DutyName, "
        + "CalFactorName, CalFactorValue "
        + "from LDRiskDutyWrap a, LMDuty b "
        + "where a.DutyCode = b.DutyCode "
        + "and a.RiskWrapCode = '" + wrapCode + "' "
        + "and a.RiskCode = '" + riskCode + "' "
        + "order by a.DutyCode with ur";

    turnPage2.pageDivName = "divPage2";
    turnPage2.queryModal(strSQL, DutyInfoGrid);
}

//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && WrapInfoGrid.mulLineCount > 0)
    {
        fm.action = "WrapInfoDownload.jsp";
        fm.target = "_blank";
        fm.submit();
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
