<%
//程序名称：GrpShareAmntInput.jsp
//程序功能：团单共用保额
//创建日期：2014-07-28
//创建人  ：zjd
//更新记录：  更新人    更新日期    更新原因/内容 
%>


<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<html>
<%
 GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
String ProposalGrpContNo = request.getParameter("ProposalGrpContNo");
String GrpContNo = request.getParameter("GrpContNo");
String ContPlanCode = request.getParameter("ContPlanCode");
String LoadFlag = request.getParameter("LoadFlag");
%>
<head>
<script>
var ProposalGrpContNo ="<%=ProposalGrpContNo%>";
var GrpContNo="<%=GrpContNo%>";
var ContPlanCode="<%=ContPlanCode%>";
var LoadFlag="<%=LoadFlag%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT> 
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="GrpShareAmntInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="GrpShareAmntInit.jsp"%>
<title>团单共用保额计划</title>

</head>
<body onload="initForm();initElementtype();">
	<form method=post name=fm target="fraSubmit" action="GrpShareAmntSave.jsp">		
		<Div  id= "divGrpPayPlan" style= "display: ''">	
			<table>
	    	  	<tr>
		        	<td class=common>
					    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
		    		</td>
		    		<td class= titleImg>
		    			 被保人共用保额
		    		</td>
	    		</tr>
	    	</table>
	    	<table  class= common>
	        	<tr  class= common>
	    	  		<td text-align: left colSpan=1>
						<span id="spanGrpShareAmntGrid" ></span> 
					</td>
				</tr>
			</table>
			<Div  id= "divGrpPayPlanSave" style= "display: ''" align= left> 
				<input class=cssButton type=button value="新增共用计划" onclick="SubmitInfo();" id="submitButton" >
				<input class=cssButton type=button value="删除共用计划" onclick="DelShareAmntPlan();" id="delButton" > 
                <input class=cssButton type=button value="返  回" onclick="goback();" id="RetButton" >     	
			</Div>
			<br>
            <Div  id= "divShareAmntSave" style= "display: 'none'" >
		 	<table>
		    	<tr>
		        	<td class=common>
					    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
		    		</td>
		    		<td class= titleImg>
		    			 被保人共用保额
		    		</td>
		    	</tr>
		     </table>
    	     <table  class= common>
	        	<tr  class= common>
	    	  		<td text-align: left colSpan=1>
						<span id="spanGrpShareAmntRiskGrid"></span> 
					</td>
				</tr>
		     </table>
             
             <br>
             
             <table  class= common align=center>
            <TR  class= common>
                <TD  class= title>共用计划编码</TD>
                <TD  class= input>
                    <input class=common name=SharePlanCode readonly  elementtype=nacessary verify="共用计划编码|notnull&len<=100">
                </TD>
                <TD  class= title>共用计划名称</TD>
                <TD  class= input>
                    <input class=common name=SharePlanName  readonly  elementtype=nacessary verify="共用计划名称|notnull&len<=100">
                </TD>
                 <TD  class= title>保险金额类型</TD>
                <TD  class= input>
                    
                    <Input class=codeNo name=AmntType readonly   maxlength="2" verify="保险金额类型|notnull" onchange="ChangePlan();" CodeData="0|^1|保额^2|限额" ondblclick="showCodeListEx('ContPlanCode',[this,AmntTypeName],[0,1],null,null,null,1);" onkeyup="showCodeListEx('ContPlanCode',[this,AmntTypeName],[0,1],null,null,null,1);"><Input class=codename  elementtype=nacessary name="AmntTypeName" readonly=true>
                    
                </TD>
            </tr>
            <tr class=common>
                <TD  class= title>共用保险金额</TD>
                <TD  class= input>
                    <Input class=common name=ShareAmnt  readonly elementtype=nacessary  verify="共用保险金额|notnull&int">
                </TD>       
                <!-- <TD  class= title>给付比例</TD>
                <TD  class= input>
                    <Input class=common name=GetRate verify="给付比例|notnull&int">
                </TD> 
                 <TD  class= title>免赔额</TD>
                <TD  class= input>
                    <input class=common name=OutDutyAmnt verify="免赔额|notnull&len<=100">
                </TD>  -->         
                    
            </TR>
            </table>
            <br>
		      
				<input class=cssButton type=button value="保存计划" onclick="SubmitDetail();" id="submitButtonDetail" > 	
			</Div>
			<input type = hidden name = ShareAmntcode>
			<input type = hidden name = fmtransact>
            <input type = hidden name = prtNo>
            <input type = hidden name = GrpContNo>
            <input type = hidden name = ContPlanCode>
            
			<br>
			<div id="GrpShareAmntPoint" style="display: none">
			<font size=2 color="#ff0000">
			 *共用计划编码由系统自动生成不用填写！</font><br>
			<font size=2 color="#ff0000">
			 *该项目用于制定单个被保险人多险种共用保额！</font>
			 </div>
       </Div>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>