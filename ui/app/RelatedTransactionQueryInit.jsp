<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：ScanContInit.jsp
	//程序功能：个单新契约扫描件保单录入
	//创建日期：2004-12-22 11:10:36
	//创建人  ：HYQ
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）                         

function initForm()
{
  try
  {   
    	initRelatedPartyGrid();
   }
  catch(re)
  {
    alert("TaxIncentivesInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initRelatedPartyGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="关联方编号";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="关联方名称";         		//列名
      iArray[2][1]="50px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="注册资本";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="公司董事长姓名";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="公司总经理姓名";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="性别";         		//列名
      iArray[6][1]="50px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="职务";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="证件号码";         		//列名
      iArray[8][1]="50px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="其他法人或其他组织";         		//列名
      iArray[9][1]="50px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
      iArray[10]=new Array();
      iArray[10][0]="关联方类型代码";
      iArray[10][1]="50px";
      iArray[10][2]=100;
      iArray[10][3]=3;
      
      iArray[11]=new Array();
      iArray[11][0]="关联方类型";         		//列名
      iArray[11][1]="50px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="导入日期";         		//列名
      iArray[12][1]="50px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      RelatedPartyGrid = new MulLineEnter( "fm" , "RelatedPartyGrid" ); 
      //这些属性必须在loadMulLine前
      RelatedPartyGrid.mulLineCount = 5;   
      RelatedPartyGrid.displayTitle = 1;
      RelatedPartyGrid.locked = 1;
      RelatedPartyGrid.canSel = 1;
      RelatedPartyGrid.canChk = 0;
      RelatedPartyGrid.hiddenSubtraction = 1;
      RelatedPartyGrid.hiddenPlus = 1;
      RelatedPartyGrid.loadMulLine(iArray);  
      
      
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);        
      }
}
</script>
