<%
//程序名称：LCInuredListQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-07-27 17:39:01
//创建人  ：Yangming
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                   
<script language="JavaScript">                                 
function initForm() {
  try {
    initLCInuredListGrid();  
  }
  catch(re) {
    alert("LCInuredListQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LCInuredListGrid;
function initLCInuredListGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="主键号";         		
    iArray[1][1]="80px";         		//列名
    iArray[1][3]=3;        		//列名
    iArray[0][4]="Seqno"; 
    
    iArray[2]=new Array();
    iArray[2][0]="被保险人姓名";         		
    iArray[2][1]="80px";         		//列名
    iArray[2][3]=0;        		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="性别代码";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=3;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="性别";         		//列名
    iArray[4][1]="30px";         		//列名
    iArray[4][3]=0;         		//列名

    
    iArray[5]=new Array();
    iArray[5][0]="出生日期";         		//列名
    iArray[5][1]="100px";         		//列名
    iArray[5][3]=0;         		//列名

	iArray[6]=new Array();
    iArray[6][0]="证件类型代码";         		//列名
    iArray[6][1]="60px";         		//列名
    iArray[6][3]=3;         		//列名
    
    iArray[7]=new Array();
    iArray[7][0]="证件类型";         		//列名
    iArray[7][1]="60px";         		//列名
    iArray[7][3]=0;         		//列名

    
    iArray[8]=new Array();
    iArray[8][0]="证件号码";         		//列名
    iArray[8][1]="100px";         		//列名
    iArray[8][3]=0;         		//列名
 
    
    iArray[9]=new Array();
    iArray[9][0]="证件有效期起";         		//列名
    iArray[9][1]="80px";         		//列名
    iArray[9][3]=0;         		//列名
    
    
    iArray[10]=new Array();
    iArray[10][0]="证件有效期止";         		//列名
    iArray[10][1]="80px";         		//列名
    iArray[10][3]=0;         		//列名
    
    iArray[11]=new Array();
    iArray[11][0]="其他证件类型代码";         		//列名
    iArray[11][1]="100px";         		//列名
    iArray[11][3]=3;         		//列名
    
    iArray[12]=new Array();
    iArray[12][0]="其他证件类型";         		//列名
    iArray[12][1]="100px";         		//列名
    iArray[12][3]=0;         		//列名
    
    iArray[13]=new Array();
    iArray[13][0]="其他证件号码";         		//列名
    iArray[13][1]="100px";         		//列名
    iArray[13][3]=0;         		//列名
    
    iArray[14]=new Array();
    iArray[14][0]="保障计划";         		//列名
    iArray[14][1]="60px";         		//列名
    iArray[14][3]=0;         		//列名
    
    iArray[15]=new Array();
    iArray[15][0]="职业代码";         		//列名
    iArray[15][1]="60px";         		//列名
    iArray[15][3]=0;         		//列名
    
    iArray[16]=new Array();
    iArray[16][0]="职业类别代码";         		//列名
    iArray[16][1]="100px";         		//列名
    iArray[16][3]=3; 

    iArray[17]=new Array();
    iArray[17][0]="职业类别";         		//列名
    iArray[17][1]="100px";         		//列名
    iArray[17][3]=0;         		//列名

    iArray[18]=new Array();
    iArray[18][0]="在职/退休代码";         		//列名
    iArray[18][1]="60px";         		//列名
    iArray[18][3]=3;         		//列名
        
    iArray[19]=new Array();
    iArray[19][0]="在职/退休";         		//列名
    iArray[19][1]="60px";         		//列名
    iArray[19][3]=0;         		//列名
    
    iArray[20]=new Array();
    iArray[20][0]="员工姓名";         		//列名
    iArray[20][1]="60px";         		//列名
    iArray[20][3]=0;         		//列名
    
    iArray[21]=new Array();
    iArray[21][0]="与员工关系代码";         		//列名
    iArray[21][1]="70px";         		//列名
    iArray[21][3]=3;         		//列名
    
    iArray[22]=new Array();
    iArray[22][0]="与员工关系";         		//列名
    iArray[22][1]="70px";         		//列名
    iArray[22][3]=0;         		//列名
    
    iArray[23]=new Array();
    iArray[23][0]="理赔金转账银行";         		//列名
    iArray[23][1]="100px";         		//列名
    iArray[23][3]=0;         		//列名
    
    iArray[24]=new Array();
    iArray[24][0]="帐号";         		//列名
    iArray[24][1]="130px";         		//列名
    iArray[24][3]=0;         		//列名
    
    iArray[25]=new Array();
    iArray[25][0]="户名";         		//列名
    iArray[25][1]="100px";         		//列名
    iArray[25][3]=0;         		//列名
    
    
    LCInuredListGrid = new MulLineEnter( "fm" , "LCInuredListGrid" ); 
    //这些属性必须在loadMulLine前

    LCInuredListGrid.mulLineCount = 0;   
    LCInuredListGrid.displayTitle = 1;
    LCInuredListGrid.hiddenPlus = 1;
    LCInuredListGrid.hiddenSubtraction = 1;
    LCInuredListGrid.canSel = 1;
    LCInuredListGrid.canChk = 0;
    //LCInuredListGrid.selBoxEventFuncName = "showOne";

    LCInuredListGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LCInuredListGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
