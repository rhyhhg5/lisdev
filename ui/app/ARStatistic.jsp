<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：ARStatistic
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="ARStatistic.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form  action="./ARStatisticSub.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
					<TD  class= title> 管理机构</TD>
					<TD  class= input><Input class= "codeno"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
        <TR  class= common>

				<TD  class= title>
				契约类型
				</TD>
				<TD  class= Input>
				<input class=codeno name=QYType verify="|len<=20"  CodeData= "0|^个单投保状况明细汇总统计报表|1^个单核保状况明细汇总统计报表|2^个单承保状况明细汇总统计报表|3^团单投保状况明细汇总统计报表|4^团单核保状况明细汇总统计报表|5^团单承保状况明细汇总统计报表|6" ondblclick="return showCodeListEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);" onkeyup="return showCodeListKeyEx('QYType',[this,QYType_ch],[1,0],null,null,null,1);"><input class=codename name=QYType_ch></TD>
           
    </TR>
    </table>

    <input type="hidden" name=op value="">
		<INPUT VALUE="打  印"  class="cssButton" TYPE="button" onclick="Print()">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
