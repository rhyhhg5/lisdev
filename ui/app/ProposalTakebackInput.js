//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var PolNo;

//提交，保存按钮对应操作,提交申请
function printPol()
{
	var i = 0;
	var flag = 0;
	flag = 0;
	
	for( i = 0; i < PolGrid.mulLineCount; i++ )
	{
		if( PolGrid.getChkNo(i) == true )
		{
			flag = 1;
			break;
		}
	}
	
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//showSubmitFrame(mDebug);
	
	fm.submit();
	if(fmSave.fmtransact.value == "")
	{
		fmSave.fmtransact.value = "PRINT";
		fmSave.submit();
	}
	else
	{
		fmSave.submit();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}

function returnParent()
{
    tPolNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?PolNo="+tPolNo;
}


// 查询按钮
function easyQueryClick()
{
    // 初始化表格
    resetHiddenbutton();
    initPolGrid();
    if(!verifyInput2())
        return false;
    if(fm.PrtNo.value == "" && fm.ContNo.value == "" && fm.startSignDate.value == "" && fm.startGetPolDate.value == "")
    {
        alert("印刷号、保单号、签单起始日期和回销起始日期至少录入一个！");
        return false;
    }
    if(fm.PrtNo.value != "" || fm.ContNo.value != "")
    {
        fm.startSignDate.value = "";
        fm.endSignDate.value = "";
        fm.startGetPolDate.value = "";
        fm.endGetPolDate.value = "";
    }
    
    if(fm.GetpolState.value == "0" && fm.PrtNo.value == "" && fm.ContNo.value == "" && fm.startSignDate.value == "" && fm.startGetPolDate.value != "")
    {
    	alert("回销类型为未回销，不能选择回销日期进行查询！")
        return false;
    }
    
    if(fm.GetpolState.value == "0")
    {
        fm.startGetPolDate.value = "";
        fm.endGetPolDate.value = "";
    }
    
    //校验查询日期
    if(!chectDate())
    {
    	return false;
    }
    
    // 书写SQL语句
    var strSQL = "";
    if(fm.ContType.value == 1)
    {
        strSQL = "select a.ContNo, a.PrtNo, b.CValiDate,b.AppntName,b.Prem,getUniteCode(b.agentcode),a.agentName,"
            + "a.managecom,a.receivedate,a.GetpolMakeDate,c.PrintDate,a.GetpolState,a.getpoldate,"
            + "a.getpolman,a.sendpolman,a.GetPolOperator,a.conttype,b.SaleChnl,b.AgentCom "
            + ",case a.GetPolState when '0' then '未回销' when '1' then '已回销' end "
            + " from LCContGetPol a,LCCont b ,LCContReceive c "
            + "where a.contno=b.contno and c.contno=b.contno and c.dealstate='0' and a.conttype='1' "
            + "and b.conttype='1' and b.uwflag != 'a' "
            + "and b.ManageCom like '" + manageCom + "%' "
            + getWherePart('a.ContNo','ContNo')
            + getWherePart('a.PrtNo','PrtNo')
            + getWherePart('a.ManageCom','ManageCom','like')
            + getWherePart('a.GetPolState','GetpolState')
            + getWherePart('a.GetPolMakeDate','startGetPolDate','>=')
            + getWherePart('a.GetPolMakeDate','endGetPolDate','<=')
            + getWherePart('b.PolApplyDate','PolApplyDate')
            + getWherePart('b.CValiDate','CValiDate')
            + getWherePart('b.SignDate','startSignDate','>=')
            + getWherePart('b.SignDate','endSignDate','<=')
            + getWherePart('getUniteCode(b.agentcode)','AgentCode')
            + getWherePart('b.SaleChnl','SaleChnlCode')
            + getWherePart('b.AgentCom','AgentComBank')
            + getWherePart('b.AppntName','AppntName','like')
            + getWherePart('b.InsuredName','InsuredName','like')
            + " order by a.ManageCom,b.AgentCode";
    }
    else if(fm.ContType.value == 2)
    {
        strSQL = "select a.ContNo, a.PrtNo, b.CValiDate,b.GrpName,b.Prem,getUniteCode(b.agentcode),a.agentName,"
            + "a.managecom,a.receivedate,a.GetpolMakeDate ,c.PrintDate,a.GetpolState,a.getpoldate,"
            + "a.getpolman,a.sendpolman,a.GetPolOperator,a.conttype,b.SaleChnl,b.AgentCom "
            + ",case a.GetPolState when '0' then '未回销' when '1' then '已回销' end "
            + " from LCContGetPol a,LCGrpCont b ,LCContReceive c "
            + "where a.contno=b.grpcontno and c.contno=b.grpcontno and c.dealstate='0' and a.conttype='2' "
            + "and b.ManageCom like '" + manageCom + "%' "
            + getWherePart('a.ContNo','ContNo')
            + getWherePart('a.PrtNo','PrtNo')
            + getWherePart('a.ManageCom','ManageCom','like')
            + getWherePart('a.GetPolState','GetpolState')
            + getWherePart('a.GetPolMakeDate','startGetPolDate','>=')
            + getWherePart('a.GetPolMakeDate','endGetPolDate','<=')
            + getWherePart('b.PolApplyDate','PolApplyDate')
            + getWherePart('b.CValiDate','CValiDate')
            + getWherePart('b.SignDate','startSignDate','>=')
            + getWherePart('b.SignDate','endSignDate','<=')
            + getWherePart('getUniteCode(b.agentcode)','AgentCode')
            + getWherePart('b.SaleChnl','SaleChnlCode')
            + getWherePart('b.AgentCom','AgentComBank')
            + getWherePart('b.GrpName','AppntName','like')
            + " order by a.ManageCom,b.AgentCode";
    }
    turnPage.pageLineNum = 50;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    	if(IsReceive())
    	{
    		fmReceive.submit();
    		return false;
    	}
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    fm.querySql.value = strSQL;
}

//判断是否生成新数据
function dataConfirm(rr)
{
	if(rr.checked == true)
	{
		if( confirm("是否生成新数据？生成新数据点击“确定”，否则点“取消”。"))
		{
			rr.checked = true;
			fmSave.fmtransact.value = "CONFIRM";
		}
		else
		{
			rr.checked = false;
			fmSave.fmtransact.value = "PRINT";
		}
	}
	else
	{
		rr.checked = false;
		fmSave.fmtransact.value = "PRINT";
	}
}
function afterCodeSelect( cCodeName, Field )
{
    if(cCodeName=="comcode"){
        fm.AgentCode.value = "";
  	    fm.AgentCodeName.value = "";   
    }
    if (cCodeName == "salechnlall")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

function receivePol()
{
	if(fmSave.mReceiveID.value ==""||fmSave.mContNo.value ==""||fmSave.mPrtNo.value ==""||fmSave.mContType.value ==""){
		alert("请先选择选择一条记录"); 
		return;
		}
		fmSave.all('fmtransact').value = "RECEIVE";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//showSubmitFrame(mDebug);
	
	  fmSave.submit();
}

function rejectsPol()
{
	if(fmSave.BackReasonCode.value == ""||fmSave.BackReason.value == ""){
		alert("请先选择退回原因"); 
		return;
		}
	if(fmSave.mReceiveID.value ==""||fmSave.mContNo.value ==""||fmSave.mPrtNo.value ==""||fmSave.mContType.value ==""){
		alert("请先选择选择一条记录"); 
		return;
		}
		fmSave.all('fmtransact').value = "REJECT";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//showSubmitFrame(mDebug);
	
	  fmSave.submit();
}

function Polinit()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
     		 if (PolGrid.getSelNo(i)) { 
     			 checkFlag = PolGrid.getSelNo();
     			 break;
  		 	 }
  }
  if (checkFlag) { 
  						var tContNo=PolGrid.getRowColData(checkFlag-1,1);
  						var tAppntName=PolGrid.getRowColData(checkFlag-1,4);
  						var tAgentName=PolGrid.getRowColData(checkFlag-1,7);
  						var tPrintDate=PolGrid.getRowColData(checkFlag-1,11);
  						var tGetpolDate=PolGrid.getRowColData(checkFlag-1,13);
  						var tGetpolMan=PolGrid.getRowColData(checkFlag-1,14);
  						var tSendPolMan=PolGrid.getRowColData(checkFlag-1,15);
    					var tGetPolOperator=PolGrid.getRowColData(checkFlag-1,16);
    					var tGetpolState=PolGrid.getRowColData(checkFlag-1,12);
    					var tContType=PolGrid.getRowColData(checkFlag-1,17);
    					var tManageCom=PolGrid.getRowColData(checkFlag-1,8);
    					var tAgentCode=PolGrid.getRowColData(checkFlag-1,6);
    					
    					
  						  fmSave.all('mContNo').value =tContNo;
                fmSave.all('mPrintDate').value = tPrintDate;
                fmSave.all('mManageCom').value ="A"+tManageCom;
                fmSave.all('mAgentCode').value = "D"+tAgentCode;
                fmSave.all('mContType').value = tContType;
                
                
              
              if (tGetpolState==0){
              	fmSave.all("mGetpolDate").value="";
              	if(tContType==1){
              	fmSave.all("mGetpolMan").value=tAppntName;
              		}
              	else{
              	fmSave.all("mGetpolMan").value="";
              		}
              	fmSave.all("mSendPolMan").value=tAgentName;
              	fmSave.all("mGetPolOperator").value=fmSave.all("fmOperator").value;
                fmSave.all("savebtn").disabled=false;
                fmSave.all("updatebtn").disabled=true;
              	}
              else if (tGetpolState==1){
              	fmSave.all("mGetpolDate").value=tGetpolDate;
              	fmSave.all("mGetpolMan").value=tGetpolMan;
              	fmSave.all("mSendPolMan").value=tSendPolMan;
              	fmSave.all("mGetPolOperator").value=tGetPolOperator;
                fmSave.all("savebtn").disabled=true;
                fmSave.all("updatebtn").disabled=false;
              	}
              else{
                fmSave.all("savebtn").disabled=true;
                fmSave.all("updatebtn").disabled=true;
              	}
              if(tContType==1){
              	fmSave.all("mCertifyCode").value="9995";
              	}
              else if(tContType==2){
              	fmSave.all("mCertifyCode").value="9994";
              	}
              else{
              	fmSave.all("mCertifyCode").value="";
              	}
  }
  else{
  	 			alert("请先选择一条保单信息！"); 
 	}

}

function resetHiddenbutton()
{

    fmSave.all('mContNo').value = '';
    fmSave.all('mPrintDate').value = '';
    fmSave.all('mGetpolDate').value = '';
    fmSave.all('mGetpolMan').value = '';
    fmSave.all('fmtransact').value = '';
    fmSave.all('mSendPolMan').value = '';
    fmSave.all('mGetPolOperator').value = '';
    fmSave.all('mCertifyCode').value = '';
    fmSave.all('mManageCom').value ='';
    fmSave.all('mAgentCode').value = '';
    fmSave.all('mContType').value = '';
    fmSave.all("savebtn").disabled=false;
    fmSave.all("updatebtn").disabled=false;
}
function savepol()
{
    if(!checkbq()){
        return false;
    }
//    if(fmSave.mGetpolDate.value < fmSave.mPrintDate.value){
//    	alert("签收日期应该晚于打印时间,请修改");
//    	return false;
//    }
    if(fmSave.mGetpolDate.value==""||fmSave.mGetpolMan.value==""||fmSave.mSendPolMan.value=="")
    {
        alert("请录入签收日期,签收人员,递交人员后再保存！");
        return;
    }
    var res = easyExecSql("select Current Date from dual ");
    if(dateDiff(res[0][0],fmSave.mGetpolDate.value,"D") > 0)
	{
		alert("客户签收日期不得晚于系统的回销日期！");
		return false;
	}
    if(dateDiff(fmSave.mGetpolDate.value,fmSave.mPrintDate.value,"D") > 0)
	{
		alert("客户签收日期不能早于合同打印时间！");
		return false;
	}
    fmSave.all('fmtransact').value = "INSERT";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fmSave.submit();
}
function updatepol()
{
    if(!checkbq()){
        return false;
    }
    if(fmSave.mGetpolDate.value==""||fmSave.mGetpolMan.value==""||fmSave.mSendPolMan.value=="")
    {
        alert("请录入签收日期,签收人员,递交人员后再修改");
        return;
    }
    var res = easyExecSql("select Current Date from dual ");
    if(dateDiff(res[0][0],fmSave.mGetpolDate.value,"D") > 0)
	{
		alert("客户签收日期不得晚于系统的回销日期！");
		return false;
	}
//    res = easyExecSql("select * from LCContGetPol where ContNo = '" 
//        + fmSave.all('mContNo').value + "' and Current Date - GetPolMakeDate > 3");
//    if(res)
//    {
//        alert("客户签收日期在系统回销后３天以内，才可以修改！");
//        return;
//    }
    if(dateDiff(fmSave.mGetpolDate.value,fmSave.mPrintDate.value,"D") > 0)
	{
		alert("客户签收日期不能早于合同打印时间！");
		return false;
	}
    fmSave.all('fmtransact').value = "UPDATE";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fmSave.submit();
}
function checkbq()
{
	var ContNo=fmSave.all('mContNo').value;
	var ContType=fmSave.all('mContType').value;
	var Csql="";
	if(ContType==1){
		Csql="select a.* from LPEdorApp a, LPEdorMain b where a.edorAcceptNO = b.edorAcceptNo  "
		    +" and b.contNo ='"+ContNo+"' and a.edorState != '0'";
		}
	else if(ContType==2){
		Csql="select a.* from LPEdorApp a, LPGrpEdorMain b where a.edorAcceptNO = b.edorAcceptNo "
		    +" and b.grpContNo = '"+ContNo+"' and a.edorState != '0'";
		}
	else{
		alert("缺少保单状态！");
		return false;
		}
	var Carr=easyExecSql(Csql);
	if(Carr){
		alert("该单正在保全操作中，待保全确认后再进行回执回销操作！");
		return false;
		}
	return true;
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    showInfo=window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
    //alert(showInfo.fm);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
    }
}
//add by hyy
function downloadClick()
{
	  if(PolGrid.mulLineCount==0)
    {
        alert("列表中没有数据可下载");
    }
    var oldAction = fm.action;
    fm.action = "ProposalTakeback.jsp";
    fm.submit();
    fm.action = oldAction;
}

//查询业务员信息
function beforeQueryAgent()
{
    var saleChnl = fm.SaleChnlCode.value;
    if(saleChnl == null || saleChnl == "")
    {
        saleChnl = "01";
    }
    var saleChnlSql = "select Code1, CodeName from LDCode1 where Code = '"
        + saleChnl + "'";
    var result = easyExecSql(saleChnlSql);
    if(result)
    {
        branch = " and BranchType = #" + result[0][0] + "# and BranchType2 = #" + result[0][1] + "#";
    }
}

//查询保单是否已接收合同
function IsReceive()
{
	var PrtNo = fm.PrtNo.value;
	var ContNo = fm.ContNo.value;
	
	var strSQL = "select lcc.receiveid, lcc.contno, lcc.prtno, lcc.conttype, lcc.backreasoncode, lcc.backreason, "
		+ "(select codename from ldcode where codetype='backreasoncode' and code= lcc.backreasoncode)"
		+ " from LCContreceive lcc where 1 = 1 " ;
	if(PrtNo != null && PrtNo != "")
	{
		strSQL = strSQL + "and lcc.PrtNo = '" + fm.PrtNo.value + "' ";
	}
	if(ContNo != null && ContNo != "")
	{
		strSQL = strSQL + "and lcc.ContNo = '" + fm.ContNo.value + "' ";
	}
	strSQL = strSQL + "and lcc.ReceiveState = '0' fetch first 1 rows only ";
	
	var result = easyExecSql(strSQL);
	if(result)
	{	
		fmReceive.fmtransact.value = "RECEIVE";
		fmReceive.mReceiveID.value = result[0][0];
		fmReceive.mContNo.value = result[0][1];
		fmReceive.mPrtNo.value = result[0][2];
		fmReceive.mContType.value = result[0][3];
		fmReceive.BackReasonCode.value = result[0][4];
		fmReceive.BackReason.value = result[0][5];
		fmReceive.BackReasonDetail.value = result[0][6];
		return true;
	}
	else
		return false;
}

function afterReceiveSubmit( FlagStr, content )
{
	if (FlagStr == "Succ" )
	{
		easyQueryClick();
	}
}

//校验查询日期
function chectDate()
{
	var tStartSignDate = fm.startSignDate.value;
	var tEndSignDate = fm.endSignDate.value;
	var tStartGetPolDate = fm.startGetPolDate.value;
	var tEndGetPolDate = fm.endGetPolDate.value;
	
	if((tStartSignDate != null && tStartSignDate != "") 
		&& (tEndSignDate == null || tEndSignDate == ""))
	{
		alert("请选择签单日期止！");
		return false;
	}
	
	if((tStartSignDate != null && tStartSignDate != "") 
		&& (tEndSignDate != null && tEndSignDate != ""))
	{
		if(dateDiff(tStartSignDate,tEndSignDate,"M") > 3)
		{
			alert("签单日期统计最多为三个月！");
			return false;
		}
	}
	
	if((tStartGetPolDate != null && tStartGetPolDate != "") 
		&& (tEndGetPolDate == null || tEndGetPolDate == ""))
	{
		alert("请选择回销日期止！");
		return false;
	}
	
	if((tStartGetPolDate != null && tStartGetPolDate != "") 
		&& (tEndGetPolDate != null && tEndGetPolDate != ""))
	{
		if(dateDiff(tStartGetPolDate,tEndGetPolDate,"M") > 3)
		{
			alert("回销日期统计最多为三个月！");
			return false;
		}
	}
	
	return true;
}
