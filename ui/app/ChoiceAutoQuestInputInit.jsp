<%
//程序名称：ChoiceAutoQuestInputInit.jsp
//程序功能：
//创建日期：2005-01-19 14:47:23
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<%
		String GrpContNo = "";
		String BatchNo = "";
		if(request.getParameter("GrpContNo")!=null)
		{
			GrpContNo = request.getParameter("GrpContNo");
		}
		if(request.getParameter("BatchNo")!=null)
		{
			BatchNo = request.getParameter("BatchNo");
		}
%>
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
		fm.GrpContNo.value="<%=GrpContNo%>";
		fm.BatchNo.value="<%=BatchNo%>";
  }
  catch(ex)
  {
    alert("在ChoiceAutoQuestInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在ChoiceAutoQuestInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox(); 
    initGrpQuestGrid();  
    initQuest(); 
  }
  catch(re)
  {
    alert("ChoiceAutoQuestInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGrpQuestGrid()
{
	var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            	//列宽
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="问题类型";      //列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="问题描述";         	//列名
    iArray[2][1]="60px";            	//列宽
    iArray[2][3]=0;              	//是否允许输入,1表示允许，0表示不允许
 
    
    iArray[3]=new Array();
    iArray[3][0]="操作位置";         	//列名
    iArray[3][1]="40px";            	//列宽
    iArray[3][3]=2;              	//是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="";  
    
    iArray[4]=new Array();
    iArray[4][0]="退回对象类型";         	//列名
    iArray[4][1]="60px";            	//列宽
    iArray[4][3]=0;              	//是否允许输入,1表示允许，0表示不允许
        
    iArray[5]=new Array();
    iArray[5][0]="退回对象";         	//列名
    iArray[5][1]="60px";            	//列宽
    iArray[5][3]=0;              	//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="问题件内容";         	//列名
    iArray[6][1]="500px";            	//列宽
    iArray[6][3]=0;              	//是否允许输入,1表示允许，0表示不允许
        

    iArray[7]=new Array();
    iArray[7][0]="原填写内容";         	//列名
    iArray[7][1]="140px";            	//列宽
    iArray[7][3]=0;              	//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="团体合同号";         	//列名
    iArray[8][1]="80px";            	//列宽
    iArray[8][3]=3;              	//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="流水号";         	//列名
    iArray[9][1]="80px";            	//列宽
    iArray[9][3]=3;              	//是否允许输入,1表示允许，0表示不允许

    GrpQuestGrid = new MulLineEnter( "fm" , "GrpQuestGrid" ); 
    //这些属性必须在loadMulLine前                                
    GrpQuestGrid.mulLineCount = 3;                                   
    GrpQuestGrid.displayTitle = 1;                                
    GrpQuestGrid.hiddenPlus = 1;                                
    GrpQuestGrid.hiddenSubtraction = 1;                                
    GrpQuestGrid.canChk = 1;
    //GrpQuestGrid.selBoxEventFuncName ="getdetail";
    GrpQuestGrid.loadMulLine(iArray);  
    
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
