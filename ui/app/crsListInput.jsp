<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="crsListInput.js"></SCRIPT>
  <script>
  	var GrpContNo ="<%=request.getParameter("GrpContNo")%>";
  	var operator ="<%=tGI.Operator%>";
  </script>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>  
</head>
<body   >
 <form action="" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
<div id="divDiskInput" style="display:''">
    <table class=common border=0 width=100%>				
				<tr class=common style="display: ''">
					<td class=title>保单类型</td>
					<td class=input><Input class=codeno name=ContType value="1" CodeData="0|^1|个单^2|团单" ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" ><input class=codename name=ContTypeName value="个单" readonly=true ></td>
					<td class=title>交叉销售渠道</td>
					<td class=input> <input class="codeNo" name="mCrsSaleChnl"  verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,mCrs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,mCrs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="mCrs_SaleChnlName" readonly="readonly"/></td>
					<td class=title>交叉销售业务类型</td>
					<td class=input><input class="codeNo" name="mCrsBussType"  verify="交叉销售业务类型|code:crs_busstype" ondblclick="return showCodeList('crs_busstype',[this,mCrs_BussTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_busstype',[this,mCrs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="mCrs_BussTypeName" readonly="readonly" ></td>
				</tr>
				
	</table>
    <table class = common >
    	<TR>
      <TD  colspan=2>
        
      </TD>
	  	</TR>
    <TR  >
      <TD  width='8%' style="font:9pt">
        文件名：
      </TD>
      
      <TD  width='80%'>
        <input type="file"　width="100%" name=FileName type=file class= common>
        <input VALUE="上传TXT" class="cssbutton" type=button onclick = "InsuredListUpload();" >
        <input value="更新数据" class="cssbutton" type= button onclick = "updateCont();">      
        <input type="hidden" width="100%" name="insuredimport" value ="1">
        
      </TD>
    </TR>
    
	    <input type=hidden name=ImportFile>
	</table>   
	<div>
			<font color="red">				
				<ul>注意事项：
					<li>TXT格式要求：</li>
					<li>数据存储顺序为：合同号，中介机构代码，对方业务员证代码，对方业务员编号，对方业务员姓名</li>
					<li>两个数据之间以,隔开，不要留有空格</li>
					<li>txt必须以crstxt.txt命名</li>
					<li>所上报的保单，必须为已签发的保单</li>
				</ul>
			</font>
	</div> 
</div>
    <input type=hidden id="" name="fmtransact">
    <input type=hidden id="" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
