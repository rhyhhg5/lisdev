<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-08-01 18:29:02
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="PubAccAmntInput.js"></SCRIPT>
  <SCRIPT>
  	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
  	var RiskCode = "<%=request.getParameter("RiskCode")%>";
  	var GrpPolNo = "<%=request.getParameter("GrpPolNo")%>";
  	var tLookFlag = "<%=request.getParameter("LookFlag")%>";
  </SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PubAccAmntInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./PubAccAmntInputSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		<td class= titleImg>
        		团体公共保额定义
       	</td>
    	</tr>
    </table>
    <table class= common >
    	<tr class=common>
    		
      	<TD  class= title8>
      	  帐户选择
      	</TD>
      	<TD  class= input8>
      		<Input name=InsuAccNo class= codeNo  verify="帐户选择|code:InsuAcc" ondblclick="if(GetInsuAccICodeData()) showCodeListEx('InsuAcc',[this,InsuAccName],[0,1],null,null,null,1);"  onkeyup="if(GetInsuAccICodeData()) showCodeListKeyEx('InsuAcc',[this,InsuAccName],[0,1],null,null,null,1);"><Input name=InsuAccName class= codeName>
      	</TD>
      	<TD class= input8 id="divi" style="display: ''">
      		<span style="font-weight:normal;color:red;font:italic arial,sans-serif">
          		*利率录入为0.035。
          	</span>
      	</TD>
      	<td></td>
      	<td></td>
    	</tr>
    </table>
    <div id="divNone" style="display: 'none'">
    	<td class= title8>
      		保费
      	</td>
      	<td class= input8>
      		<Input name=Prem class= common>
      	</td>
    	<TD  class= title8>
        账户类型
      </TD>
      <TD  class= input8>
      	<Input name=AccTypeCode  class= codeNo CodeData="0|^001|集体公共账户^002|个人账户" ondblclick="showCodeListEx('AccTypeCode',[this,AccTypeName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('AccTypeCode',[this,AccTypeName],[0,1],null,null,null,1);" ><Input name=AccTypeName class= codeName>
      </TD>
      <TD  class= title8>
        审核类型
      </TD>
      <TD  class= input8>
      	<Input name=AccType2 class= codeNo  CodeData="0|^01|有审核型^02|普通型" ondblclick="showCodeListEx('AccType2',[this,AccType2Name],[0,1],null,null,null,1);"  onkeyup=" showCodeListKeyEx('AccType2',[this,AccType2Name],[0,1],null,null,null,1);"><Input name=AccType2Name class= codeName>
      </TD> 
      <td class=title8>
    		管理费收取方式
    	</td>
    	<td class=input8>
    		<Input name=ManageFeeType class= codeNo  CodeData="0|^01|一次^02|普通型" ondblclick="showCodeListEx('ManageFeeType',[this,ManageFeeTypeName],[0,1],null,null,null,1);"  onkeyup=" showCodeListKeyEx('ManageFeeType',[this,ManageFeeTypeName],[0,1],null,null,null,1);"><Input name=ManageFeeTypeName class= codeName>
    	</td>
    </div>
    <Div  id= "divLDClassInfo1" style= "display: 'none'">
			<table  class= common align='center' >
				<Div  id= "divLCPol2" style= "display: ''">
					<table  class= common>
						<tr  class= common>
							<td text-align: left colSpan=1>
								<span id="spanAccInfoGrid" >
								</span> 
							</td>
						</tr>
					</table>		
				</div>
			</table>
    </Div>
    <Div  id= "divLDClassInfo2" style= "display: ''">
			<table  class= common align='center' >
				<Div  id= "divLCPol2" style= "display: ''">
					<table  class= common>
						<tr  class= common>
							<td text-align: left colSpan=1>
								<span id="spanPublicAccGrid" >
								</span> 
							</td>
						</tr>
					</table>		
				</div>
			</table>
    </Div>
    <INPUT class=cssButton VALUE="保存信息"  TYPE=button name=saveAccInfo onclick="return submitForm();">
    <INPUT class=cssButton VALUE=" 删  除 "  TYPE=button name=deleteAccInfo onclick="return deleteClick();">
    <INPUT class=cssButton VALUE=" 退  出 "  TYPE=button name=close onclick="cancle();">
    <Div  id= "divClaimNum" style= "display: 'none'">
	    <table class= common >
	    	<tr class=common>
	      	<TD  class= title8>
	      	  赔付顺序
	      	</TD>
	      	<TD  class= input8>
	      		<Input name=ClaimNum class=codeNo  CodeData="0|^1|仅从个人保险金额给付^2|先从个人保险金额给付,后从公共保险金额给付^3|仅从公共保险金额给付"  ondblclick="showCodeListEx('ClaimNum',[this,ClaimNumName],[0,1],null,null,null,1);"  onkeyup=" showCodeListKeyEx('ClaimNum',[this,ClaimNumName],[0,1],null,null,null,1);"><Input name=ClaimNumName class= codeName elementtype=nacessary >
	      	</TD>
	    	</tr>
	    </table>
    <INPUT class=cssButton VALUE="保  存" name=SaveManaFee TYPE=button onclick="return saveManageFee();">
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="PublicAccType" name="PublicAccType">
    <input type=hidden id="RiskCode" name="RiskCode">
    <input type=hidden id="InsuredName" name="InsuredName">
    <input type=hidden id="GrpContNo" name="GrpContNo">
    <input type=hidden id="GrpPolNo" name="GrpPolNo">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
