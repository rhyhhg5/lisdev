
var turnPage = new turnPageClass();
var showInfo;

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function querygCont()
{
//	alert();
	var ContNo=fm.ContNo.value;
//	alert(ContNo);
	var InsuredNo=fm.InsuredNo.value;
	if(!verifyInput2())
	return false;
	initGrpGrid();
	var strSQL = "select a.ContNo,a.InsuredNo,a.InsuredName,b.Codename,a.InsuredBirthday,a.InsuredIDType,a.InsuredIDNo from LCCont a, LDCode b where PolType != '2' and a.InsuredSex = b.Code and b.Codetype = 'sex' and"
    				 + " a.GrpContNo='"+ GrpContNo +"' "
  				  + getWherePart( 'a.ContNo','ContNo' )
				 + getWherePart( 'a.InsuredNo','InsuredNo')+" order by a.InsuredNo";
    
  //alert(strSQL);
 turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    
  
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}


function deleteCont()
{
	fm.AllContDelFlag.value="SingleDel";
	fm.GrpContNo.value=GrpContNo;
  var i = 0;
	var flag = 0;
	for( i = 0; i < GrpGrid.mulLineCount; i++ ) {
		if( GrpGrid.getChkNo(i) == true ) {
			//如果发现有选择，则直接退出循环，继续
			flag = 1;
			break;
		}
	}
	if( flag == 0 ) {
		alert("请先选择一条记录");
		return false;
	}
  
  if (!confirm("确认要删除该团单下的被保险人吗？"))
	{
		return;
	}
    
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  fm.submit();

}

function afterSubmit( FlagStr, content )
{ 
	
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content);
  }
  else
  { 
    alert("操作成功");
  }
   querygCont();
  
}
function deleteAllCont(){	
	fm.AllContDelFlag.value="AllDel";
		fm.GrpContNo.value=GrpContNo;	
  if (!confirm("确认要删除该团单下的所有被保险人吗？"))
	{
		return;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  fm.submit();	
}