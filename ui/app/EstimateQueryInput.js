
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function queryClick() {
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == "null"){
		alert("管理机构不能为空，且必须为两位或四位机构！");
		return false;
	}
	initEstimateGrid();
	var strSql = "select lgc.managecom,lgc.grpcontno,lgcs.projectname,lgcs.projectno,lgc.grpname, "
			   + "(select EstimateBalanceRate from LCEstimate where grpcontno = lgc.grpcontno order by makedate desc fetch first 1 rows only), "
			   + "lgc.cvalidate,lgc.signdate " 
			   + "from lcgrpcont lgc " 
			   + "inner join lcgrpcontsub lgcs on lgc.prtno = lgcs.prtno "
			   + "where lgc.appflag = '1' "
			   + "and lgcs.projectname is not null and lgcs.projectname != ''"
			   + getWherePart("lgc.ManageCom", "ManageCom", "like")
			   + getWherePart("ProjectName", "ProjectName", "like")
			   + getWherePart("ProjectNo", "ProjectNo")
			   + getWherePart("lgc.GrpContNO", "GrpContNo") 
			   + getWherePart("lgc.CvaliDate", "StartDate", ">=") 
			   + getWherePart("lgc.CvaliDate", "EndDate", "<=") 
			   + getWherePart("lgc.SignDate", "StartSignDate", ">=") 
			   + getWherePart("lgc.SignDate", "EndSignDate", "<=") 
			   + " order by lgc.managecom,lgc.grpcontno ";
	turnPage1.strQueryResult = easyQueryVer3(strSql, 1, 0, 1);
	if (!turnPage1.strQueryResult) {
		alert("未查询到结果！");
		return "";
	}
	turnPage1.queryModal(strSql, EstimateGrid);
}
function EstimateSave() {
	var checkFlag = 0;
	for (i = 0; i < EstimateGrid.mulLineCount; i++) {
		if (EstimateGrid.getSelNo(i)) {
			checkFlag = EstimateGrid.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		var tGrpContNo = EstimateGrid.getRowColData(checkFlag - 1, 2);
		if (tGrpContNo == null || tGrpContNo == "" || tGrpContNo == "null") {
			alert("获取保单号码失败！");
			return false;
		}
		showInfo = window.open("./EstimateMain.jsp?GrpContNo=" + tGrpContNo +"&LookFlag=0");
	} else {
		alert("请先选择保单数据！");
		return false;
	}
}

