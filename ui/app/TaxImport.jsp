<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<html>
<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	String strCurDay = PubFun.getCurrentDate();
%>
<script>
	var strCurDay = "<%=strCurDay%>";
	var ComCode = "<%=tGI.ManageCom%>";
	var Operator = "<%=tGI.Operator%>";
</script>
	<head>
    	<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    	<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    	<script src="../common/javascript/Common.js"></script>
    	<script src="../common/cvar/CCodeOperate.js"></script>
    	<script src="../common/javascript/MulLine.js"></script>
   	 	<script src="../common/Calendar/Calendar.js"></script>

    	<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    	<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    	<script src="TaxImport.js"></script>
    	<%@include file="TaxImportInit.jsp" %>
	</head>
	<body onload="initForm();">
		<form action="" method=post name=fm target="fraSubmit">
			<table>
            	<tr>
                	<td class="common">
                    	<img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, SYGrpInfo);"/>
                	</td>
                	<td class="titleImg">查询税优团体客户：</td>
            	</tr>
       	 	</table>
       	 	<div id="SYGrpInfo" align="center" style="display:''">
       	 		<table class=common>
       	 			<tr class=common>
       	 				<td class=title>团体编号</td>
       	 				<td class=input>
       	 					<input class=common name=GrpNo>
       	 				</td>
       	 				<td class=title>单位名称</td>
       	 				<td class=input>
       	 					<input class=common name=GrpName>
       	 				</td>
       	 				<td class=title>税务登记证号码</td>
       	 				<td class=input>
       	 					<input class=common name=TaxRegistration>
       	 				</td>
       	 			</tr>
       	 			<tr class=common>
       	 				<td class=title>社会信用代码</td>
       	 				<td class=input>
       	 					<input class=common name=OrganComcode>
       	 				</td>
       	 				<td class=title></td>
       	 				<td class=input></td>
       	 				<td class=title></td>
       	 				<td class=input></td>
       	 			</tr>
       	 		</table>
       	 	</div>
       	 	<input type="button" class=cssButton value=" 查  询 " name=QueryGrpInfoButton onclick="QueryGrpInfoClick();">
       	 	<table>
            	<tr>
                	<td class="common">
                    	<img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, SYGrpList);"/>
                	</td>
                	<td class="titleImg">税优团体客户列表：</td>
            	</tr>
       	 	</table>
       	 	<div id="SYGrpList" align="center" style="display:''">
       	 		<table class="common">
                	<tr class="common">
                    	<td>
                        	<span id="spanGrpInfoGrid"></span> 
                    	</td>
               	 	</tr>
            	</table>
            	<div id="divGrpInfoGridPage" style="display: ''" align="center">
                	<input type="button" class="cssButton" value="首  页" onclick="turnPage.firstPage();" />
                	<input type="button" class="cssButton" value="上一页" onclick="turnPage.previousPage();" />
                	<input type="button" class="cssButton" value="下一页" onclick="turnPage.nextPage();" />
                	<input type="button" class="cssButton" value="尾  页" onclick="turnPage.lastPage();" />
            	</div>
       	 	</div>
       	 	<table>
       	 		<tr>
                	<td class="common">
                    	<img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, BatchInfo);"/>
                	</td>
                	<td class="titleImg">导入批次查询：</td>
            	</tr>
       	 	</table>
       	 	<div id="BatchInfo" style="display: ''" align="center">
       	 		<table class=common>
       	 			<tr class=common>
       	 			 	<td class=title>团体编号</td>
       	 			 	<td class=input>
       	 			 		<input class=common name=GroupNo readonly=true>
       	 			 	</td>
       	 			 	<td class=title>管理机构</td>
       	 			 	<td class=input>
       	 			 		<input class="codeNo" name="ManageCom" verify="管理机构|code:comcode" 
       	 			 		ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
       	 			 		onkeyup="return showCodelistKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
       	 			 		/><input class="codename" name="ManageComName" readonly=true>
       	 			 	</td>
       	 			 	<td class=title>申请日期</td>
       	 			 	<td class=input>
       	 			 		<input class="coolDatePicker" dateFormat="short" name="ApplyDate" verify="申请日期|date">
       	 			 	</td>
       	 			</tr>
       	 			<tr class=common>
       	 			 	<td class=title>批次号</td>
       	 			 	<td class=input>
       	 			 		<input class=common name=BatchNo>
       	 			 	</td>
       	 			 	<td class=title>批次状态</td>
       	 			 	<td class=input>
       	 			 		<input class="codeNo" name="BatchState" CodeData="0|^0|待导入^1|待内检^2|待确认^3|已确认"
       	 			 		ondblclick="return showCodeListEx('BatchState',[this,BatchStateName],[0,1],null,null,null,1);"
       	 			 		onkeyup="return showCodeListKeyEx('BatchState',[this,BatchStateName],[0,1],null,null,null,1);"
       	 			 		/><input class="codename" name="BatchStateName" readonly=true>
       	 			 	</td>
       	 			 	<td class=title></td>
       	 			 	<td class=input>
       	 			 		
       	 			 	</td>
       	 			</tr>
       	 		</table>
       	 	</div>
       	 	<input type="button" class=cssButton value=" 查  询 " name=QueryBatchButton onclick="QueryBatchClick();">
       	 	<input type="button" class=cssButton value=" 申  请 " name=ApplyButton onclick="Apply();">
       	 	<table>
       	 		<tr>
                	<td class="common">
                    	<img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, BatchDetail);"/>
                	</td>
                	<td class="titleImg">批次信息：</td>
            	</tr>
       	 	</table>
       	 	<div id="BatchDetail" style="display: ''" align="center">
       	 		<table class="common">
                	<tr class="common">
                    	<td>
                        	<span id="spanBatchDetailGrid"></span> 
                    	</td>
               	 	</tr>
            	</table>
            	<div id="divBatchDetailGridPage" style="display: ''" align="center">
                	<input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" />
                	<input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />
                	<input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" />
                	<input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />
            	</div>
       	 	</div>
       	 	<input type="button" class=cssButton value=" 批次导入" name=ImportButton onclick="batchImport();">
       	 	<input type="button" class=cssButton value=" 批次被保人详情 " name=InsuredInfoButton onclick="importInsuredDetail();">
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>