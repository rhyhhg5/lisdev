<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="RateAndLimitModification.js"></SCRIPT>
  <%@include file="RateAndLimitModificationInit.jsp"%> 
  <title>请输入查询条件</title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./RateAndLimitModificationSave.jsp" method="post" name="fm" target="fraSubmit">
    <table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输入查询条件：
					</td>
				</tr>
	</table>
    <table class="common" >
		<tr CLASS="common">
			<td CLASS="title">管理机构 
    		</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="ManageCom"  CLASS=codeNo ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,'1 and #1# = #1# and Length(trim(comcode))=8','1',1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,'1 and #1# = #1# and Length(trim(comcode))=8','1',1);" verify="管理机构|notnull&code:comcode"><input class=codename name=ManageComName readonly=true elementtype=nacessary>  
    		</td>
    		<td CLASS="title">印刷号码 
    		</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="PrtNo" CLASS="common" elementtype=nacessary>
    		</td>
    		
			<td CLASS="title">合同号
    		</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="GrpContNo" CLASS="common" elementtype=nacessary>
    		</td>    		
		</tr>
		<tr CLASS="common">
			<td CLASS="title">保障计划 
    		</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="ContPlanCode" VALUE CLASS="common" >
    		</td>
    		<td CLASS="title">险种编码 
    		</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="RiskCode" VALUE CLASS="common" >
    		</td>
			<td CLASS="title">责任编码
    		</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="DutyCode" VALUE CLASS="common" >
    		</td>    		
		</tr>
		<tr>
			<td><INPUT VALUE="查  询" class = cssButton TYPE=button onclick="queryByCondition();"></td>
		</tr>
	</table>
     <table>
		<tr>
			<td class=common>
				<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMrateAndLimitGrid);">
					</td>
					<td class= titleImg>保单信息</td>
				</tr>
			</table>
    <Div id="divLMrateAndLimitGrid" style="display: ''"> 
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
					<span id="spanLMrateAndLimitGrid" >
					</span> 
			  	</td>
			</tr>
		</table>
	</div>
	<Div id="divPage" align=center style="display: 'none' ">
		<table align=center>
			<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
			<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
			<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
			<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
		 </table>
	</Div>
	<table class=common  border=0 width=100%>
	<tr CLASS="common">
	        <td CLASS="title">修改后的给付比例</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="Getrate" VALUE CLASS="common" >
    		</td> 
    		
    		<td CLASS="title">修改后的免赔额</td>
			<td CLASS="input" COLSPAN="1">
			<input NAME="Getlimit" VALUE CLASS="common" >
    		</td> 
	</tr>
	<tr>
		<td colspan="4" align="left"><INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();"></td>
	</tr>
	
	</table>
			<input type=hidden id="fmtransact" name="fmtransact" value="UPDATE||MAIN">
   			<input type=hidden id="fmAction" name="fmAction">
    		<input type=hidden id="ruleNo" name="ruleNo" value="">
	</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
