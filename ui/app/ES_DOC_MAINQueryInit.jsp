<%
//程序名称：ES_DOC_MAINQueryInit.jsp
//程序功能：扫描单证下载的主页面Init
//创建日期：2005-08-25
//创建人  ：DongJianbin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = '' 	;
    fm.all('StartDate').value = ''	;
    fm.all('EndDate').value = ''	;    
    fm.all('ScanOperator').value = ''	;
    fm.all('SubType').value = 'TB01'	;
    fm.all('SubTypeName').value = '家庭投保书';
    fm.all('DocCode').value = ''	;
  }
  catch(ex)
  {
    alert("在CodeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在CodeInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCodeGrid();
    fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
  }
  catch(re)
  {
    alert("CodeInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化CodeGrid
 ************************************************************
 */
  var CodeGrid;          //定义为全局变量，提供给displayMultiline使用
function initCodeGrid()
{                               
    var iArray = new Array();
      
      try
      {
        
		    iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列宽
        iArray[0][2]=100;            //列最大值
        iArray[0][3]=3;              //是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="扫描号";         //列名
        iArray[1][1]="0px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=3;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="单证号码";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="单证类型";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="页数";         //列名
        iArray[4][1]="25px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
    
        iArray[5]=new Array();
        iArray[5][0]="扫描机构";         //列名
        iArray[5][1]="55px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

	    iArray[6]=new Array();
        iArray[6][0]="扫描日期";         //列名
        iArray[6][1]="50px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="扫描时间";         //列名
        iArray[7][1]="40px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="扫描人";         //列名
        iArray[8][1]="40px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

	    iArray[9]=new Array();
        iArray[9][0]="状态";         //列名
        iArray[9][1]="60px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[10]=new Array();
        iArray[10][0]="外包公司";         //列名
        iArray[10][1]="60px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
        
          
        CodeGrid = new MulLineEnter( "fm" , "CodeGrid" ); 

        //这些属性必须在loadMulLine前
        CodeGrid.mulLineCount = 0;   
        CodeGrid.displayTitle = 1;
        CodeGrid.canSel=1;
        CodeGrid.hiddenSubtraction=1;
        CodeGrid.hiddenPlus=1;
        CodeGrid.canChk=0;
        CodeGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化CodeGrid时出错："+ ex);
      }
}
</script>