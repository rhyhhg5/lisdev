var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var i = 0;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
    var tSignStartDate = fm.SignStartDate.value;
    var tSignEndDate = fm.SignEndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>12)
	{
		alert("统计期最多为12个月！");
		return false;
	}
    var tManageCom = fm.ManageCom.value; 
    var tSaleChnl = fm.SaleChnl.value;   
    var tTheContType = fm.TheContType.value;
    var strSQL =" select m1,m2,m3,m4,m5,m6,m7,m8,getUniteCode(m9),m10,m11,m12,case when m6 is not null then to_date(m6)-to_date(m8) else 0 end from (" 
             +"select a.managecom m1,a.prtno m2,a.CardFlag || ' - ' || (case a.CardFlag when '0' then '标准件' when '9' then '银保通' else '简易件' end) m3,a.prem m4,codename('lcsalechnl',a.salechnl) m5,(select min(enteraccdate) from ljtempfee where (otherno =a.prtno or otherno=a.contno)) m6,codename('paymode',a.paymode) m7, "
             + "a.uwdate m8,a.agentcode m9,(select name from laagent where agentcode =a.agentcode) m10,b.branchattr m11,b.name m12 "
             + "from lccont a,labranchgroup b where a.agentgroup = b.agentgroup and a.conttype='1' "
             + getWherePart('a.SaleChnl','SaleChnl')
             + " and not exists (select 1 from lbcont where prtno=a.prtno ) "
             + " and not exists (select 1 from lcrnewstatelog where prtno=a.prtno) "
             + " and a.managecom like '"+tManageCom+"%' and a.uwdate between '"+tStartDate+"' and '"+tEndDate+"' ";
    if(tSignStartDate!="" && tSignStartDate!=null && tSignStartDate!="null"){
    	strSQL += " and a.signdate>='"+tSignStartDate+"' ";
    }
    if(tSignEndDate!="" && tSignEndDate!=null && tSignEndDate!="null"){
    	strSQL += " and a.signdate<='"+tSignEndDate+"' ";
    }
    if(tTheContType!="" && tTheContType!=null && tTheContType!="null"){
    	if(tTheContType=="0" || tTheContType=="9"){
    		strSQL += " and a.cardflag='"+tTheContType+"' ";
    	}
    	if(tTheContType=="99"){
    		strSQL += " and a.cardflag not in ('0','9') ";
    	}
    }
    strSQL += " order by a.managecom,b.branchattr,a.agentcode) temp with ur";
     
    fm.AnalysisSql.value = strSQL;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}   
	fm.submit();
}