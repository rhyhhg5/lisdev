//TempFeeWithdrawInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = 1;
      fm.all('PrtNo').value = PolGrid.getRowColData(PolGrid.getSelNo(i) - 1, 1);
      break;
    }
  }
  
  if (checkFlag) { 
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
  }
  else {
    alert("请先选择一条投保单信息！"); 
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
}        

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//查询按钮
function easyQueryClick() { 
  var strSql = "select UWPopeDom from LDUser where UserCode='" + Operator + "'";
  var isUW = easyExecSql(strSql);
  
  //没有核保级别
  if (isUW == "") {
    //已进行关联（OtherNo）和已核销、退费（ConfFlag）的不显示出来
    strSql = "select DOC_CODE, Operator, InputStartDate from ES_DOC_MAIN where "
           + " InputState = '1' and Operator is not null "
           + getWherePart('DOC_CODE')
           + getWherePart('Operator')
           + getWherePart('InputStartDate')
           + " and DOC_CODE in (select prtNo from lcpol where AppFlag='0' and UWFlag='0')";
  }
  else {
    //已进行关联（OtherNo）和已核销、退费（ConfFlag）的不显示出来
    strSql = "select DOC_CODE, Operator, InputStartDate from ES_DOC_MAIN where "
           + " InputState = '1' and Operator is not null "
           + getWherePart('DOC_CODE')
           + getWherePart('Operator')
           + getWherePart('InputStartDate')
           + " and DOC_CODE in (select prtNo from lcpol where AppFlag='0')";
  }  
             
  turnPage.queryModal(strSql, PolGrid);
}
