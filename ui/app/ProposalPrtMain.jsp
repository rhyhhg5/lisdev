<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="ProposalPrtMain.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="ProposalPrtMainInit.jsp"%>
<title>重打个单</title>
</head>
<body onload="initForm();" >
	<form action="./ProposalPrtMainSave.jsp" method=post name=fm target="fraSubmit">
	<!-- 保单信息部分 -->
		<table class= common border=0 width=100%>
			<tr>
				<td class= titleImg align= center>请输入保单查询条件：</td>
			</tr>
		</table>
		<table class= common align=center>
			<TR class= common>
				<TD class= title>管理机构</TD>
				<TD  class= input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true > </TD>
				<TD class= title>印 刷 号</TD>
				<TD class= input><Input class= common name=PrtNo verify="印刷号|int"></TD>
				<TD class= title>合 同 号</TD>
				<TD class= input><Input class= common name=ContNo></TD>
				</TR>
			<TR class= common>
				<TD class= title>申请日期</TD>
				<TD class= input><Input class= common name=PolApplyDate></TD>
				<TD class= title>生效日期</TD>
				<TD class= input><Input class= common name=CValiDate></TD>
				<TD class= title>签单日期</TD>
				<TD class= input><Input class= common name=SignDate></TD>
			</TR>
			<TR class= common>
				<TD class= title>投 保 人</TD>
				<TD class= input><Input class= common name=AppntName verify="投保人|len<=20"></TD>
				<TD class= title>被 保 人</TD>
				<TD class= input><Input class= common name=InsuredName verify="被保人|len<=20"></TD>
			  <TD class= title>业务员代码</TD>
				<TD  class= input> <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true ></TD>
			</TR>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
				</td>
				<td class= titleImg>保单信息</td>
			</tr>
		</table>
		<Div id= "divLCPol1" style= "display: ''">
			<table class= common>
				<TR class= common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" ></span>
					</td>
				</tr>
			</table>
			<table align=center>
			<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
		 </table>
			<table>
				<tr>
					<td>
						<INPUT VALUE="提交申请" class="cssButton" TYPE=button onclick="printPol();">
					</td>
				</tr>
			</table>
		 	<table>
				<tr>
					<TD  class= titleImg>重打原因</TD>
				</tr>
				<tr>
					<TD  class= input><Input class=codeNo readonly=true name=RePrtReasonCode ondblclick="return showCodeList('reprtreasoncode',[this,RePrtReason],[0,1]);" onkeyup="return showCodeListKey('reprtreasoncode',[this,RePrtReason],[0,1]);"><input class=codename name=RePrtReason readonly=true > </TD>
        </tr>
        <tr>
        	<td>
        <textarea name=Remark verify="问题描述|len<800" verifyorder="1" cols="110" rows="3" class="common" value="">
        </textarea>
          </td>
       </tr>
			</table>
		</div>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="tContNo" name="tContNo" value="">
		<input type=hidden id="tPrtNo" name="tPrtNo" value="">
		<input type=hidden id="tManageCom" name="tManageCom" value="">
		
	</form>
<span id="spanCode"style="display: none; position:absolute; slategray"></span>
</body>
</html>
