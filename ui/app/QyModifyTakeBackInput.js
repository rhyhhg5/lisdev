var arrDataSet
var showInfo;
var turnPage = new turnPageClass();

// 查询按钮
function easyQueryClick()
{
    // 初始化表格
    resetHiddenbutton();
    initPolGrid();
    
    if(!verifyInput2())
    {
		return false;
    }
    
    if(fm.PrtNo.value == "" && fm.ContNo.value == "")
    {
        alert("印刷号和保单合同号至少录入一个！");
        return false;
    }
    
    // 书写SQL语句
    var strSQL = "";
    if(fm.ContType.value == 1)
    {
        strSQL = "select a.ContNo, a.PrtNo, b.CValiDate,b.AppntName,b.Prem,b.agentcode,a.agentName,"
            + "a.managecom,a.receivedate,a.GetpolMakeDate,c.PrintDate,a.GetpolState,a.getpoldate,"
            + "a.getpolman,a.sendpolman,a.GetPolOperator,a.conttype,b.SaleChnl,b.AgentCom "
            + ",case a.GetPolState when '0' then '未回销' when '1' then '已回销' end "
            + " from LCContGetPol a,LCCont b ,LCContReceive c "
            + "where a.contno=b.contno and c.contno=b.contno and c.dealstate='0' and a.conttype='1' "
            + "and b.conttype='1' and b.uwflag != 'a' "
            + "and b.ManageCom like '" + manageCom + "%' "
            + getWherePart('a.ContNo','ContNo')
            + getWherePart('a.PrtNo','PrtNo')
            + getWherePart('a.ManageCom','ManageCom','like')
            + getWherePart('a.GetPolMakeDate','startGetPolDate','>=')
            + getWherePart('a.GetPolMakeDate','endGetPolDate','<=')
            + " order by a.ManageCom,b.AgentCode";
    }
    else if(fm.ContType.value == 2)
    {
        strSQL = "select a.ContNo, a.PrtNo, b.CValiDate,b.GrpName,b.Prem,b.agentcode,a.agentName,"
            + "a.managecom,a.receivedate,a.GetpolMakeDate ,c.PrintDate,a.GetpolState,a.getpoldate,"
            + "a.getpolman,a.sendpolman,a.GetPolOperator,a.conttype,b.SaleChnl,b.AgentCom "
            + ",case a.GetPolState when '0' then '未回销' when '1' then '已回销' end "
            + " from LCContGetPol a,LCGrpCont b ,LCContReceive c "
            + "where a.contno=b.grpcontno and c.contno=b.grpcontno and c.dealstate='0' and a.conttype='2' "
            + "and b.ManageCom like '" + manageCom + "%' "
            + getWherePart('a.ContNo','ContNo')
            + getWherePart('a.PrtNo','PrtNo')
            + getWherePart('a.ManageCom','ManageCom','like')
            + getWherePart('a.GetPolMakeDate','startGetPolDate','>=')
            + getWherePart('a.GetPolMakeDate','endGetPolDate','<=')
            + " order by a.ManageCom,b.AgentCode";
    }
    turnPage.pageLineNum = 50;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    fm.querySql.value = strSQL;
}

//初始化查询数据
function Polinit()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) 
  {
  	if (PolGrid.getSelNo(i)) 
  	{
  		checkFlag = PolGrid.getSelNo();
		break;
	}
  }
  if (checkFlag) 
  {
	var tContNo=PolGrid.getRowColData(checkFlag-1,1);
	var tAppntName=PolGrid.getRowColData(checkFlag-1,4);
	var tAgentName=PolGrid.getRowColData(checkFlag-1,7);
	var tPrintDate=PolGrid.getRowColData(checkFlag-1,11);
	var tGetpolDate=PolGrid.getRowColData(checkFlag-1,13);
	var tGetpolMan=PolGrid.getRowColData(checkFlag-1,14);
	var tSendPolMan=PolGrid.getRowColData(checkFlag-1,15);
	var tGetPolOperator=PolGrid.getRowColData(checkFlag-1,16);
	var tGetpolState=PolGrid.getRowColData(checkFlag-1,12);
	var tContType=PolGrid.getRowColData(checkFlag-1,17);
	var tManageCom=PolGrid.getRowColData(checkFlag-1,8);
	var tAgentCode=PolGrid.getRowColData(checkFlag-1,6);

	fm.all('mContNo').value = tContNo;
	fm.all('mPrintDate').value = tPrintDate;
	fm.all('mManageCom').value = "A"+tManageCom;
	fm.all('mAgentCode').value = "D"+tAgentCode;
	fm.all('mContType').value = tContType;
	
	if (tGetpolState==0)
	{
		fm.all("mGetpolDate").value="";
		if(tContType==1)
		{
			fm.all("mGetpolMan").value=tAppntName;
		}
		else 
		{
			fm.all("mGetpolMan").value="";
		}
		fm.all("mSendPolMan").value=tAgentName;
        fm.all("mGetPolOperator").value=fm.all("fmOperator").value;
		fm.all("savebtn").disabled=false;
		fm.all("updatebtn").disabled=true;
	}else if (tGetpolState==1)
	{
		fm.all("mGetpolDate").value=tGetpolDate;
		fm.all("mGetpolMan").value=tGetpolMan;
		fm.all("mSendPolMan").value=tSendPolMan;
		fm.all("mGetPolOperator").value=tGetPolOperator;
		fm.all("savebtn").disabled=true;
		fm.all("updatebtn").disabled=false;
	}
	else
	{
		fm.all("savebtn").disabled=true;
		fm.all("updatebtn").disabled=true;
	}
	
	if(tContType==1)
	{
		fm.all("mCertifyCode").value="9995";
	}
	else if(tContType==2)
	{
		fm.all("mCertifyCode").value="9994";
	}
	else
	{
		fm.all("mCertifyCode").value="";
	}
  }
  else
  {
	alert("请先选择一条保单信息！"); 
  }
}

//重置隐藏的参数值
function resetHiddenbutton()
{

    fm.all('mContNo').value = '';
    fm.all('mPrintDate').value = '';
    fm.all('mGetpolDate').value = '';
    fm.all('mGetpolMan').value = '';
    fm.all('fmtransact').value = '';
    fm.all('mSendPolMan').value = '';
    fm.all('mGetPolOperator').value = '';
    fm.all('mCertifyCode').value = '';
    fm.all('mManageCom').value ='';
    fm.all('mAgentCode').value = '';
    fm.all('mContType').value = '';
    fm.all("updatebtn").disabled=false;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}

//修改保单的回执回销日期
function updatepol()
{
    if(fm.mGetpolDate.value == "" || fm.mGetpolMan.value == "" || fm.mSendPolMan.value == ""||fm.mGetpolMakeDate.value=="")
    {
        alert("请录入客户签收日期,签收人员,递交人员后再修改");
        return;
    }
    
	if(fm.mGetpolDate.value < fm.mPrintDate.value)
	{
		alert("客户签收日期应该晚于打印时间,请修改");
		return false;
		/*if(!confirm("客户签收日期" + fm.mGetpolDate.value + "早于打印时间" + fm.mPrintDate.value + ",是否确定进行修改"))
		{
			return false;
		}*/
	}
	
	var res = easyExecSql("select 1 from dual where Current Date - '" + fm.mGetpolDate.value + "' < 0");
    if(res)
    {
        alert("客户签收日期不得晚于系统的回销日期！");
        return false;
        /*if(!confirm("客户签收日期" + fm.mGetpolDate.value + "晚于系统的回销日期" + getCurrentDate('-') + ",是否确定进行修改"))
		{
			return false;
		}*/
    }
    
    fm.all('fmtransact').value = "UPDATE";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.submit();
}

//进行保单回执回销
function savepol()
{
    if(fm.mGetpolDate.value==""||fm.mGetpolMan.value==""||fm.mSendPolMan.value==""||fm.mGetpolMakeDate.value=="")
    {
        alert("请录入签收日期,签收人员,递交人员后再保存！");
        return false;
    }
    
    if(fm.mGetpolDate.value < fm.mPrintDate.value)
	{
		alert("客户签收日期应该晚于打印时间,请修改");
		return false;
		/*if(!confirm("客户签收日期" + fm.mGetpolDate.value + "早于打印时间" + fm.mPrintDate.value + ",是否确定进行修改"))
		{
			return false;
		}*/
	}
    
    var res = easyExecSql("select 1 from dual where Current Date - '" + fm.mGetpolDate.value + "' < 0");
    if(res)
    {
        alert("客户签收日期不得晚于系统的回销日期！");
        return false;
        /*if(!confirm("客户签收日期" + fm.mGetpolDate.value + "晚于系统的回销日期" + getCurrentDate('-') + ",是否确定进行修改"))
		{
			return false;
		}*/
    }
    
    fm.all('fmtransact').value = "INSERT";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.submit();
}