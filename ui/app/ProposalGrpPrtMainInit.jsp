<%
//程序名称：ReProposalPrtInit.jsp
//程序功能：
//创建日期：2003-04-3 11:10:36
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ManageCom;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 初始化
    fm.all('PrtNo').value = '';
    fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
    fm.all('AgentCode').value = '';
    fm.all('AgentCodeName').value = '';
    fm.all('PolApplyDate').value = '';
    fm.all('CValiDate').value = '';
    fm.all('SignDate').value = '';
    fm.all('AppntName').value = '';  
    fm.all('Remark').value = '';
    fm.all('fmtransact').value = '';
    fm.all('RePrtReasonCode').value = '';
    fm.all('RePrtReason').value = '';
    fm.all('tContNo').value = '';
    fm.all('tPrtNo').value = '';
    fm.all('tManageCom').value = '';
    fm.all('ContPrintType').value = '0';
   showAllCodeName();  
    
  }
  catch(ex)
  {
    alert("在ReProposalPrtInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
	  initGrpPolGrid();
	  
  }
  catch(re)
  {
    alert("ReProposalPrtInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var PolGrid;          //定义为全局变量，提供给displayMultiline使用

// 保单信息列表的初始化
// 保单信息列表的初始化
function initGrpPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="合同号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="印刷号";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="生效日期";         		//列名
      iArray[3][1]="70px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="投保人";         		//列名
      iArray[4][1]="70px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="保险费";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="业务员";         		//列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      iArray[7]=new Array();
      iArray[7][0]="管理机构";         		//列名
      iArray[7][1]="70px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      iArray[8]=new Array();
      iArray[8][0]="签单日期";         		//列名
      iArray[8][1]="70px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      iArray[9]=new Array();
      iArray[9][0]="打印时间";         		//列名
      iArray[9][1]="70px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      iArray[10]=new Array();
      iArray[10][0]="次数";         		//列名
      iArray[10][1]="30px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
      
      iArray[11]=new Array();
      iArray[11][0]="保单类型";         		//列名
      iArray[11][1]="10px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许   
      
       

      GrpPolGrid = new MulLineEnter( "fm" , "GrpPolGrid" ); 
      //这些属性必须在loadMulLine前
      GrpPolGrid.mulLineCount = 10;   
      GrpPolGrid.displayTitle = 1;
      GrpPolGrid.locked = 1;
      GrpPolGrid.canChk = 0;
      GrpPolGrid.canSel = 1;
      GrpPolGrid.selBoxEventFuncName ="GridFuncInit";
      GrpPolGrid.hiddenPlus = 1;
      GrpPolGrid.hiddenSubtraction = 1;
      GrpPolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>