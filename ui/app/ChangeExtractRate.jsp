<!--
@NAME:ChangeExtractRate.jsp
@DESC:这个页面是用来维护退保费率的
@AUTHOR:_LaoChen_ ;)
@SINCE:20070517
-->

<script>
	var tGrpContNo=<%=request.getParameter("GrpContNo")%>;	
	var LoadFlag=<%=request.getParameter("LoadFlag")%>;
	var scantype = "<%=request.getParameter("scantype")%>";
	var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
	var oldContNo ="<%=request.getParameter("oldContNo")%>";
</script>
<html>
 <%@page contentType="text/html;charset=GBK" %>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <head >
   <SCRIPT src="../common/javascript/Common.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js" type="text/javascript" ></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js" type="text/javascript" ></SCRIPT>
   <SCRIPT src="ChangeExtractRate.js" type="text/javascript" ></SCRIPT>
	<SCRIPT src="ProposalAutoMove.js"></SCRIPT>
	<%if(request.getParameter("scantype")!=null&&request.getParameter("scantype").equals("scan")){%>
	<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
	<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
	<%}%> 
 <%@include file="ChangeExtractRateInit.jsp"%>
 </head>
<body  onload="initForm();initElementtype();">

<form action="./ChangeExtractRateSave.jsp" method=post name=fm target='fraSubmit'>
<br />
<br />
     <table>
     
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskInfoGrid);">
         </TD>
         <TD class= titleImg>
         退费费率修改：
         </TD>
       </TR>
      </table>
      <br />

	<Div  id= "divRiskInfoGrid" style= "display: ''">
	      <table  class= common>
	        <TR  class= common>
	          <TD style="text-align:left"  colSpan=1>
	            <span id="spanRiskInfoGrid" >
	            </span>
	          </TD>
	        </TR>
	      </table>
   </Div>
<!--    <Div style="color:red; "> -->
<!--    被保险人参保第一年费用提取比例录入范围(0~0.1);被保险人参保第二年费用提取比例录入范围(0~0.08);<br/> -->
<!--    被保险人参保第三年费用提取比例录入范围(0~0.06);被保险人参保第四年费用提取比例录入范围(0~0.04);<br/> -->
<!-- 被保险人参保第五年费用提取比例录入范围(0~0.02);被保险人参保第六年之后费用提取比例录入值为0;<br/> -->
<!--    </Div> -->
   	<Div  id= "divChangeExtractRate" style= "display: ''">
	      <table  class= common>
	        <TR  class= common>
	          <TD style="text-align:left"  colSpan=1>
	            <span id="spanChangeExtractRateGrid" >
	            </span>
	          </TD>
	        </TR>
	      </table>
   </Div>
   <div id="autoMoveButton" style="display: none">
	<input type="button" name="autoMoveInput" value="随动定制确定" onclick="submitAutoMove('999');" class=cssButton>
	<%--<input type="button" name="Next" value="下一步" onclick="location.href='ContInsuredInput.jsp?LoadFlag='+tLoadFlag+'&prtNo='+prtNo+'&checktype=2&scantype='+scantype" class=cssButton>	
        --%>
        <INPUT VALUE="返回投保人页面定制 " class=cssButton TYPE=button onclick="returnparent();">     
        <input type=hidden id="" name="autoMoveFlag">
        <input type=hidden id="" name="autoMoveValue">   
        <input type=hidden id="" name="pagename" value="999">                         
      </div>  
	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="GrpPolNo" name="GrpPolNo">
	<input type=hidden id="PrtNo" name="PrtNo">
	<Div  id= "divRiskPlanSave" style= "display: ''" align= left> 
		<input class=cssButton type=button value="保  存" onclick="this.disabled=true; setEnable('submitButton');Submit();" id="submitButton" > 	
	</Div>
	<input class=cssButton type=button value="返  回" onclick="returnparent();" > 	
	
	<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divQuery);">
				</td>
				<td class=titleImg>服务管理费信息：</td>
			</tr>
		</table>
		<br />
		<div id="divQuery" style="display:''">
			<table class=common>
				<tr class=common>
<!-- 					<TD class=title8>服务管理费</TD> -->
<!-- 					<TD class=input8> -->
<!-- 						<input class=common8 name=BigProjectName elementtype="nacessary" > -->
						
<!-- 					</TD> -->
					<TD  class= title8>
            		详细地址
          			</TD>
          			<TD  class= input8>
      					<Input class= common8 name=DetailAddress elementtype=nacessary >
          			</TD>
					<td></td>
      				<td></td>
      				<td></td>
				</tr>
			</table>
			
		<Div style="color:red; "> 260901产品服务管理费录入范围(0~1);<br/></Div>
		<input type=button class=cssButton value=" 保  存 " name=Save onclick="save();" style="display:''">
		<input type=button class=cssButton value=" 修  改" name=Delete onclick="del();" style="display:''">
		<input type=button class=cssButton value=" 返  回 " name=Exit onclick="cancle();" style="display:''">
		</div>
		
  </form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
