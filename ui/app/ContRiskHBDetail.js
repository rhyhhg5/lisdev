var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartMakeDate = fm.StartMakeDate.value;
    var tEndMakeDate = fm.EndMakeDate.value;

	if(dateDiff(tStartMakeDate, tEndMakeDate, "M") > 12)
	{
		alert("统计期最多为十二个月！");
		return false;
	}

    var tStrSQL = ""
        + " select "
        + " tmp.ManageCom, tmp.ManageComName, tmp.RiskCode, tmp.RiskName, "
        + " count(1) SumTBCount, "
        + " sum(tmp.Prem) SumTBPrem, "
        + " sum(case tmp.UWFlag when '9' then 1 else 0 end) UW9Count, "
        + " sum(case tmp.UWFlag when '9' then tmp.Prem else 0 end) UW9Prem, "
        + " sum(case tmp.UWFlag when '4' then 1 else 0 end) UW4Count, "
        + " sum(case tmp.UWFlag when '4' then tmp.Prem else 0 end) UW4Prem, "
        + " sum(case tmp.UWFlag when '8' then 1 else 0 end) UW8Count, "
        + " sum(case tmp.UWFlag when '8' then tmp.Prem else 0 end) UW8Prem, "
        + " sum(case tmp.UWFlag when '1' then 1 else 0 end) UW1Count, "
        + " sum(case tmp.UWFlag when '1' then tmp.Prem else 0 end) UW1Prem, "
        + " sum(case tmp.UWFlag when 'a' then 1 else 0 end) UWaCount, "
        + " sum(case tmp.UWFlag when 'a' then tmp.Prem else 0 end) UWaPrem, "
        + " sum(case tmp.UWFlag when '0' then 1 else 0 end) UW0Count, "
        + " sum(case tmp.UWFlag when '0' then tmp.Prem else 0 end) UW0Prem, "
        + " sum(case tmp.UWFlag when '5' then 1 else 0 end) UW5Count, "
        + " sum(case tmp.UWFlag when '5' then tmp.Prem else 0 end) UW5Prem "
        + " from "
        + " ( "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcp.RiskCode, lmra.RiskName, "
        + " lcp.Prem, "
        + " lcp.UWFlag, "
        + " '' "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcp.ContNo = lcc.ContNo "
        + " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + " union all "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcp.RiskCode, lmra.RiskName, "
        + " lcp.Prem, "
        + " lcp.UWFlag, "
        + " ''  "
        + " from LCCont lcc "
        + " inner join LBPol lcp on lcp.ContNo = lcc.ContNo "
        + " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and not exists (select 1 from LCRNewStateLog lcrnsl where lcrnsl.ContNo = lcc.ContNo or lcrnsl.NewContNo = lcc.ContNo) "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + " union all "
        + " select "
        + " lcc.ManageCom, "
        + " (select ldc.Name from LDCom ldc where ldc.ComCode = lcc.ManageCom) ManageComName, "
        + " lcp.RiskCode, lmra.RiskName, "
        + " lcp.Prem, "
        + " lcp.UWFlag, "
        + " ''  "
        + " from LBCont lcc "
        + " inner join LBPol lcp on lcp.ContNo = lcc.ContNo "
        + " inner join LMRiskApp lmra on lmra.RiskCode = lcp.RiskCode "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.EdorNo not like 'xb%' "
        + " and lcp.EdorNo not like 'xb%' "
        + getWherePart("lcc.ManageCom", "ManageCom", "like")
        + getWherePart("lcc.InputDate", "StartMakeDate", ">=")
        + getWherePart("lcc.InputDate", "EndMakeDate", "<=")
        + " ) as tmp "
        + " group by tmp.ManageCom, tmp.ManageComName, tmp.RiskCode, tmp.RiskName "
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "ContRiskHBDetailSave.jsp";
    fm.submit();
    fm.action = oldAction;

}