<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ReGroupPolPrintSave.jsp
//程序功能：
//创建日期：2002-11-26
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
	String tLCGrpContNo = request.getParameter("tContNo");
	String tPrtNo = request.getParameter("tPrtNo");
	String tRePrtReasonCode = request.getParameter("RePrtReasonCode");
	String tRemark = request.getParameter("Remark");
	String tManageCom = request.getParameter("tManageCom");
	String strOperation = request.getParameter("fmtransact");
	
	String Content = "";
	System.out.println("重打模式"+strOperation);

	GlobalInput globalInput = new GlobalInput();
	
	if( (GlobalInput)session.getValue("GI") == null ) {
		Content=Content+ "网页超时或者是没有操作员信息，请重新登录";
	} else {
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
	}
	
	LCGrpContSet tLCGrpContSet = new LCGrpContSet();
	ReLCGrpContF1PUI tReLCGrpContF1PUI = new ReLCGrpContF1PUI();
		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		tLCGrpContSchema.setGrpContNo( tLCGrpContNo );
		tLCGrpContSchema.setContPrintType(request.getParameter("ContPrintType"));
		System.out.println("猪头"+request.getParameter("ContPrintType"));  
		tLCGrpContSet.add(tLCGrpContSchema);
		
	  LCContRePrtLogSchema tLCContRePrtLogSchema=new LCContRePrtLogSchema();
	  tLCContRePrtLogSchema.setContNo(tLCGrpContNo);
	  tLCContRePrtLogSchema.setPrtNo(tPrtNo);
	  tLCContRePrtLogSchema.setContType("2");
	  tLCContRePrtLogSchema.setRePrtReasonCode(tRePrtReasonCode);
	  tLCContRePrtLogSchema.setRemark(tRemark);
	  tLCContRePrtLogSchema.setManageCom(tManageCom);
	
	  VData vData = new VData();
	  
	  vData.addElement(tLCGrpContSet);
	  vData.addElement(tLCContRePrtLogSchema);
	  vData.add(globalInput);
	
	try {
		if( !tReLCGrpContF1PUI.submitData(vData, strOperation) ) {
	   	if ( tReLCGrpContF1PUI.mErrors.needDealError() ) {
	   		Content=Content+ tReLCGrpContF1PUI.mErrors.getFirstError();
		  } else {
		  	Content=Content+ "保存失败，但是没有详细的原因";
			}
		} 			
	} catch (Exception ex) {
		ex.printStackTrace();
		Content=Content+ex.getMessage();
	}
%>
<%
String FlagStr = "";
try {
	
	if( Content.equals("") ) {
	
		FlagStr = "Succ";
		Content = "提交申请成功";
	} else {
		FlagStr = "Fail";
	}
} catch (Exception ex) {
	ex.printStackTrace();
}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>