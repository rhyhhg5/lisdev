var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    {
        return false;
    }
    
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;

	if(dateDiff(tStartDate, tEndDate, "M") > 3)
	{
		alert("统计期最多为三个月！");
		return false;
	}

    var tStrSQL = ""
        + " select tmp.ComCode, tmp.Name, "
        + " nvl((FHCount_C + FHCount_B), 0) FHCount, "
        + " WTCount, "
        + " TJCount, "
        + " QDCount "
        + " from "
        + " ( "
        + " select "
        + " ldc.ComCode, ldc.Name, "
        + " (select nvl(count(distinct lcc.PrtNo), 0) ApproveCount from LCCont lcc where lcc.ManageCom = ldc.ComCode " + getWherePart("lcc.ApproveDate", "StartDate", ">=") + getWherePart("lcc.ApproveDate", "EndDate", "<=") + " ) FHCount_C, "
        + " (select nvl(count(distinct lbc.PrtNo), 0) ApproveCount from LBCont lbc where lbc.ManageCom = ldc.ComCode "  + getWherePart("lbc.ApproveDate", "StartDate", ">=") + getWherePart("lbc.ApproveDate", "EndDate", "<=") + " ) FHCount_B, "
        + " (select count(distinct lopm.PrtSeq) from LOPrtManager lopm where lopm.ManageCom = ldc.ComCode and lopm.Code = '85' " + getWherePart("lopm.MakeDate", "StartDate", ">=") + getWherePart("lopm.MakeDate", "EndDate", "<=") + " ) WTCount, "
        + " (select count(distinct lopm.PrtSeq) from LOPrtManager lopm where lopm.ManageCom = ldc.ComCode and lopm.Code = '03' " + getWherePart("lopm.MakeDate", "StartDate", ">=") + getWherePart("lopm.MakeDate", "EndDate", "<=") + " ) TJCount, "
        + " (select count(distinct lopm.PrtSeq) from LOPrtManager lopm where lopm.ManageCom = ldc.ComCode and lopm.Code = '04' " + getWherePart("lopm.MakeDate", "StartDate", ">=") + getWherePart("lopm.MakeDate", "EndDate", "<=") + " ) QDCount, "
        + " '' "
        + " from LDCom ldc  "
        + " where 1 = 1 "
        + " and length(trim(ldc.ComCode)) = 8 "
        + getWherePart("ldc.ComCode", "ManageCom", "like")
        + " ) as tmp "
        + " order by tmp.ComCode "
        ;
 
    fm.querySql.value = tStrSQL;

    var oldAction = fm.action;
    fm.action = "FirstTrialWorkload1DaySave.jsp";
    fm.submit();
    fm.action = oldAction;

}