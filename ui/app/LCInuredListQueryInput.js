/** 
 * 程序名称：LCInuredListInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-07-27 17:39:01
 * 创建人  ：Yangming
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
		if(!verifyInput2())
	return false;
	//此处书写SQL语句			     
/*	
	var strSql = "select a.GetNoticeNo, a.otherno, b.name, a.PayDate, a.SumDuePayMoney, a.bankcode, a.bankaccno, a.accname "
	           + " from LJSPay a, ldperson b where a.appntno=b.customerno and OtherNoType='2' "
          	 + getWherePart("a.GetNoticeNo", "GetNoticeNo2")
          	 + getWherePart("a.OtherNo", "OtherNo")
          	 + getWherePart("a.BankCode", "BankCode")
          	 + getWherePart("a.SumDuePayMoney", "SumDuePayMoney");
  
  if (fm.AppntName.value != "") strSql = strSql + " and a.appntno in (select c.customerno from ldperson c where name='" + fm.AppntName.value + "')";
  if (fm.PrtNo.value != "") strSql = strSql + " and a.otherno in (select polno from lcpol where prtno='" + fm.PrtNo.value + "')";
*/  			    
	var strSql = "select GrpContNo,InsuredID,State,ContNo,BatchNo,InsuredNo, case Retire when '1' then '在职' else '退休' end,EmployeeName,InsuredName,case Relation when '00' then '本人' when '01' then '妻子' when '02' then '丈夫' when '03' then '子女' else '其它' end ,case Sex when '1' then '女' else '男' end,Birthday,(select codename from ldcode where codetype='idtype' and code=IDType),IDNo,ContPlanCode,(select  CodeName from ldcode where codetype = 'occupationtype' and code=OccupationType),BankCode,BankAccNo,AccName,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,PublicAccType,PublicAcc,EdorNo,RiskCode,CalPremType,Schoolnmae,ClassName from LCInsuredList where 1=1 "
    + getWherePart("GrpContNo", "GrpContNo")
    + getWherePart("InsuredID", "InsuredID")
    + getWherePart("State", "State")
//    + getWherePart("ContNo", "ContNo")
//    + getWherePart("BatchNo", "BatchNo")
    + getWherePart("InsuredNo", "InusredNo")
    + getWherePart("Retire", "Retire")
    + getWherePart("EmployeeName", "EmployeeName")
    + getWherePart("InsuredName", "InsuredName")
    + getWherePart("Relation", "Relation")
    + getWherePart("Sex", "Sex")
    + getWherePart("Birthday", "Birthday")
    + getWherePart("IDType", "IDType")
    + getWherePart("IDNo", "IDNo")
    + getWherePart("ContPlanCode", "ContPlanCode")
    + getWherePart("OccupationType", "OccupationType")
    + getWherePart("BankCode", "BankCode")
    + getWherePart("BankAccNo", "BankAccNo")
    + getWherePart("AccName", "AccName")
    + getWherePart("Schoolnmae", "Schoolnmae")
    + getWherePart("ClassName", "ClassName")
/*    + getWherePart("Operator", "Operator")
    + getWherePart("MakeDate", "MakeDate")
    + getWherePart("MakeTime", "MakeTime")
    + getWherePart("ModifyDate", "ModifyDate")
    + getWherePart("ModifyTime", "ModifyTime")
*/
    + " and GrpContNo = '"+GrpContNo+"'" ;
  
  //alert(strSql);
	turnPage.queryModal(strSql, LCInuredListGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LCInuredListGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LCInuredListGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LCInuredListGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
