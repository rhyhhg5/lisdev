<%@page contentType="text/html;charset=GB2312"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.schema.LOPRTManagerSchema"%>
<%@page import="com.sinosoft.lis.vschema.LOPRTManagerSet"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.wiitb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.io.*"%>
<%
	System.out.println("--------------------PDAllFPrintStart------------------");
	
	CError cError = new CError();
	boolean operFlag = true;
	String FlagStr = "";
	String Content = "";

	String tGrpContNos[] = request.getParameterValues("GrpContGrid1");
	String tContPrintFlag[] = request.getParameterValues("GrpContGrid8");
	String tChk[] = request.getParameterValues("InpGrpContGridChk");
	String PrtSeq = request.getParameter("PrtSeq");//打印单证流水号
	String OtherNoType = request.getParameter("OtherNoType");//其它号码类型 
	String Code = request.getParameter("Code");//单证类型 （必填值项）
	String StandbyFlag1 = request.getParameter("StandbyFlag1");
	String StandbyFlag2 = request.getParameter("StandbyFlag2");
	String OldPrtSeq = request.getParameter("OldPrtSeq"); //旧印刷流水号
	String PatchFlag = request.getParameter("PatchFlag"); //补打标记
	String StandbyFlag3 = request.getParameter("StandbyFlag3");
	String StandbyFlag4 = request.getParameter("StandbyFlag4");
	String urgerflag = request.getParameter("urgerflag"); //催办标志

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	tG.ClientIP = request.getHeader("x-forwarded-for");
	if (tG.ClientIP == null || tG.ClientIP.length() == 0) {
		tG.ClientIP = request.getRemoteAddr();
	}
	System.out.println("------操作员的IP地址:" + tG.ClientIP);

	int grouppolCount = tGrpContNos.length;
	for (int i = 0; i < grouppolCount; i++) {
		if (tGrpContNos[i] != null && tChk[i].equals("1")) {
		    VData tVData = new VData();
			String tGrpContNo = tGrpContNos[i];
			String tPrintFlag = new String(tContPrintFlag[i].getBytes("ISO-8859-1"),"gbk");
			System.out.println("tContPrintFlag[i]=="+tContPrintFlag[i]); 
			System.out.println("tPrintFlag=="+tPrintFlag); 

			if ("打印".equals(tPrintFlag) || "打印".equals(tContPrintFlag[i])) {
				
				CErrors mErrors = new CErrors();
		
				LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		
				tLOPRTManagerSchema.setPrtSeq(PrtSeq);
		
				tLOPRTManagerSchema.setOtherNo(tGrpContNo);
				tLOPRTManagerSchema.setOtherNoType(OtherNoType);
				tLOPRTManagerSchema.setCode(Code);
				tLOPRTManagerSchema.setStandbyFlag1(StandbyFlag1);
				tLOPRTManagerSchema.setStandbyFlag2(StandbyFlag2);
				tLOPRTManagerSchema.setOldPrtSeq(OldPrtSeq);
				tLOPRTManagerSchema.setPatchFlag(PatchFlag);
				tLOPRTManagerSchema.setStandbyFlag3(StandbyFlag3);
				tLOPRTManagerSchema.setStandbyFlag4(StandbyFlag4);
				tLOPRTManagerSchema.setUrgerFlag(urgerflag);
				LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
				mLOPRTManagerSet.add(tLOPRTManagerSchema);
				tVData.addElement(mLOPRTManagerSet);
				tVData.addElement(tG);
		
				PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
				if (operFlag == true) {
					if (!tPDFPrintBatchManagerBL.submitData(tVData,"only")) {
						FlagStr = "Fail";
						Content += " <br /> 保单（"+tGrpContNo+"）打印失败，原因为："+tPDFPrintBatchManagerBL.mErrors.getFirstError().toString();
					} else {
						if (tPDFPrintBatchManagerBL.mErrors	.getErrorCount() > 0) {
							Content += " <br /> 保单（"+tGrpContNo+"）打印失败，原因为："+tPDFPrintBatchManagerBL.mErrors.getFirstError().toString();
							FlagStr = "PrintError";
						} else {
							Content += "<br /> 保单（"+tGrpContNo+"）打印成功！";
							FlagStr = "Succ";
						}
					}
				}
			} else if ("不打印".equals(tPrintFlag) || "不打印".equals(tContPrintFlag[i])) {
					TransferData tTransferData = new TransferData();
					tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
					tVData.add(tTransferData);
					tVData.add(tG);
			
					GrpGSNoPrtPrintBL tGrpGSNoPrtPrintBL = new GrpGSNoPrtPrintBL();
					if (!tGrpGSNoPrtPrintBL.submitData(tVData, "")) {
						Content +=  " <br /> 保单（"+tGrpContNo+"）打印失败，原因为："+ tGrpGSNoPrtPrintBL.mErrors.getFirstError();
						FlagStr = "Fail";
					} else {
						Content += " <br /> 保单（"+tGrpContNo+"）打印成功！";
						FlagStr = "Succ";
					}
			}
		}
	}
%>
<html>
	<script language="javascript">
  parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>");
</script>
</html>

