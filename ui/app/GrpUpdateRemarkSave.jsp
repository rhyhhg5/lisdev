<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>


<%
	//输入参数
	GrpUpdateRemarkUI tGrpUpdateRemarkUI = new GrpUpdateRemarkUI();

	String mAfterUpdateRemark = request.getParameter("AfterUpdateRemark");
	String tRadio[] = request.getParameterValues("InpPolGridSel");
	String tPrtNo[] = request.getParameterValues("PolGrid3");
	Date nowModifyDate = new Date();
	//String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	String nowModifyTime = Long.toString(nowModifyDate.getTime());
	
	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	String strInput = "";
	
	String uSql = "";
	
	TransferData transferData = new TransferData();
	
	for (int i=0; i< tRadio.length;i++){
		if("1".equals(tRadio[i])){
			transferData.setNameAndValue("PrtNo", tPrtNo[i]);
			transferData.setNameAndValue("AfterUpdateRemark", mAfterUpdateRemark);
			break;
		}
	}
	
	if(!"Fail".equals(FlagStr)){
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			tGrpUpdateRemarkUI.submitData(tVData, "");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = tGrpUpdateRemarkUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

