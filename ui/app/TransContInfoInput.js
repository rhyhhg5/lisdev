
function changeConttype(){
	if(fm.ContType.value=="1"){
		fm.all("TransContNoTitle").style.display = "none";
		fm.all("TransContNoInput").style.display = "none";
		fm.all("TransContNo").value= "";
	}else if(fm.ContType.value=="2"){
		fm.all("TransContNoTitle").style.display = "";
		fm.all("TransContNoInput").style.display = "";
		fm.all("TransContNo").value= "";
	}
}

function afterCodeSelect( cCodeName, Field ,InputObj)
{
    if(cCodeName=="getbankcode1")
	{
	  fm.all('Str').value = "1 and bankcode = #"+fm.BankCode.value+"# and ManageCom =#"+fm.ManageCom.value+"# ";
	}
	
} 

function insertCode(){
          
	//校验转入的保单号是否重复
	//private String TransGrpContNo;
	 var tResult = easyExecSql("select 1 from LSTransInfo  where prtno = '"+fm.PrtNo.value+"'");
     if(tResult)
     {
         alert("已保存，请进行修改操作！");
         return false;
     }
     
     if(!checkRisk()){
	    return false;
	 }
     
     if (verifyInput2()== false) {        	
	    return false;
     }
	
    
     if(fm.all('ContType').value=="2" && (fm.all('TransContNo').value==""||fm.all('TransContNo').value==null)){
    		alert("请填写转入的分单号！");
    		return false;
     }

    //客户交互提示窗
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.methodName.value = "save";
    fm.submit();
}


function deleteCode(){
	var tResult = easyExecSql("select SuccFlag from LSTransInfo  where prtno = '"+fm.PrtNo.value+"'");
	if(!tResult)
	{
		alert( "该单未保存，不需要进行删除操作！" );
		return ;
	}
	else
	{
	     if(tResult[0][0]=='00'||tResult[0][0]=='01'){
	         alert("保单转移申请已提交或已获取原保单预计终止日期，不能做删除操作！");
             return false;
	     }
		 if (!confirm("确认要删除吗？"))
		 {
			 return;
		 }
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.methodName.value = "delete";
		fm.submit(); //提交
		
	}
}

function updateCode(){
	var tResult = easyExecSql("select SuccFlag from LSTransInfo  where prtno = '"+fm.PrtNo.value+"'");
     if(!tResult)
     {
         alert("请先进行保存操作！");
         return false;
     }
	else
	{
	    if(!checkRisk()){
	       return false;
	    }
	    if(tResult[0][0]=='00'||tResult[0][0]=='01'){
	       alert("保单转移申请已提交或已获取预计终止日期，不能做修改操作！");
           return false;
	    }
	    if (verifyInput2()== false) {        	
	      return false;
        }
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.methodName.value = "update";
		fm.submit(); //提交
	}
}

//提交后（控制层执行完成返回结果后的操作）——save页面通过js调用这个函数
function afterSubmit(FlagStr, content, methodName){
	showInfo.close();//关闭Dialog
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	
	//清空页面数据
	if(methodName=="delete"&&FlagStr!="Fail"){
		deleteinfo();
	}
}

function deleteinfo(){
		fm.all('CompanyNo').value="";
		fm.all('CompanyName').value="";
		fm.all('ContType').value="";
		fm.all('ContTypeName').value="";
		fm.all('TransGrpContNo').value="";
		fm.all('SYCustomerNo').value="";
		fm.all('TransContNo').value="";
		fm.all('Name').value="";
		fm.all('Phone').value="";
		fm.all('Email').value="";
		fm.all('BankCode').value="";
		fm.all('BankName').value="";
		fm.all('AccName').value="";
		fm.all('AccNo').value="";
}

function initForm(){

     if(fm.all('AddFlag').value=='1'){
         fm.all("AddID").style.display = "";
         fm.all("ApplyID1").style.display = "none";
         fm.all("ApplyID2").style.display = "none";
     }else if(fm.all('AddFlag').value=='2'){
         fm.all("AddID").style.display = "none";
         fm.all("ApplyID1").style.display = "";
         fm.all("ApplyID2").style.display = "";
         var arr = easyExecSql("select 1 from LSTransInfo lc where lc.prtno = '"+fm.PrtNo.value+"' and SuccFlag in ('00','01','02') ");
         if(arr){
             fm.all("TransApp").disabled = true;
         }
     }else{
         fm.all("AddID").style.display = "none";
         fm.all("ApplyID1").style.display = "none";
         fm.all("ApplyID2").style.display = "none";     
     }

     var arr = easyExecSql("select ld.comcode , ld.name,lc.agentcode "
             + " from lccont lc,ldcom ld where lc.prtno = '"+fm.PrtNo.value+"'"
             + " and ld.comcode=lc.managecom ");
     var agentcode;
     var newmanagecom;
     if(arr)
     {
        fm.all('ManageCom').value=arr[0][0];
		fm.all('ManageComName').value=arr[0][1];
		fm.all('AgentCode').value=arr[0][2];
		fm.all('Str').value = "1 and bankcode = #0# and ManageCom =#"+fm.ManageCom.value+"# ";
		newmanagecom =arr[0][0];
     } 
     arr = easyExecSql("select CompanyNo,codename('transcompany',CompanyNo),"
             + "conttype,case conttype when '1' then '个单' else '团单' end,transgrpcontno,transcontno,"
             + "agentcode,name,phone,email,trim(bankcode)||'-'||trim(bankname),bankcode,accname,accno "
             + " ,(select ManageCom from laagent where agentcode=ls.agentcode), "
             + "case ls.succflag when '00' then '申请成功，待返回' when '01' then '申请已返回' when '02' then '申请已返回' when '99' then '待申请' else '待申请' end,ls.ExpectedDate,ls.RejectReason,ls.applydate  "
             + " from LSTransInfo ls,lccontsub lc where lc.prtno = '"+fm.PrtNo.value+"'"
             + " and lc.prtno=ls.prtno ");      
     if(arr)
     {
        fm.all('CompanyNo').value=arr[0][0];
		fm.all('CompanyName').value=arr[0][1];
		fm.all('ContType').value=arr[0][2];
		fm.all('ContTypeName').value=arr[0][3];
		fm.all('TransGrpContNo').value=arr[0][4];
		agentcode=arr[0][6];
		fm.all('TransContNo').value=arr[0][5];
		fm.all('Name').value=arr[0][7];
		fm.all('Phone').value=arr[0][8];
		fm.all('Email').value=arr[0][9];
		fm.all('BankName').value=arr[0][10];
		fm.all('BankCode').value=arr[0][11];
		fm.all('AccName').value=arr[0][12];
		fm.all('AccNo').value=arr[0][13];
		fm.all('AppState').value=arr[0][15];
		fm.all('ExpectedDate').value=arr[0][16];
		fm.all('RejectReson').value=arr[0][17];
		fm.all('ApplyDate').value=arr[0][18];
		var managecom=arr[0][14];
		if(fm.all('AgentCode').value!=agentcode){
           if (confirm("投保单业务员已变更，是否显示投保单业务员联系信息？"))
		   {
			   getAgentInfo(fm.all('AgentCode').value);
		   }else{
		       fm.all('AgentCode').value=agentcode;
		   }
        }
        if(managecom!=""&&managecom!=null&&managecom!=newmanagecom){
           if (confirm("投保单管理机构已变更，是否默认显示投保单管理机构归集账户？"))
		   {
			   getAccInfo(newmanagecom);
		   }
        }
     }else{
         getAgentInfo(fm.all('AgentCode').value);
     }
     
     if(fm.all('ContType').value=='2'){
        fm.all("TransContNoTitle").style.display = "";
		fm.all("TransContNoInput").style.display = "";
     }
     
            
}

function checkRisk(){
   var sql = "select * "
            + " from lcpol lc,lmriskapp lm,lccontsub lcc "
            + " where lc.prtno='" + fm.PrtNo.value + "' "
            + " and lcc.prtno=lc.prtno "
            + " and lcc.transflag='1' "
            + " and lc.riskcode=lm.riskcode and lm.TaxOptimal='Y' ";
   var rs = easyExecSql(sql);
   if(!rs){
      alert("未承保税优产品或保单不为转入保单，不需要进行转入保单录入！");
      return false;
   }
   return true;
}

function getAgentInfo(cagentcode){
   var sql = "select Name,case when mobile is not null then mobile else phone end ,EMail "
            + " from laagent "
            + " where agentcode='" + cagentcode + "' ";
   var arr = easyExecSql(sql);
   if(arr){
      fm.all('Name').value=arr[0][0];
	  fm.all('Phone').value=arr[0][1];
	  fm.all('Email').value=arr[0][2];
   }
   
}

function getAccInfo(cComcode){
   var sql = "select * "
            + " from ldfinbank "
            + " where comcode='" + cComcode + "' ";
   var arr = easyExecSql(sql);
   if(arr){

   }
   
}

function Apply(){

   if(!checkApply()){
      return false;
   }
   var showStr = "正在进行验证，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ; 
   showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
   var ttAction = fm.action; 
   fm.action="./TransApplyConfirm.jsp";
   fm.submit();
   fm.action=ttAction;  
}

function CloseCode(){
  top.close(); 
}

function checkApply(){
   var strSql2="select uwflag from lcpol where prtno='"+fm.PrtNo.value+"'";
   var arr2=easyExecSql(strSql2);
   if(arr2){
  	  for(var a=0;a<arr2.length;a++) {
      	if(arr2[a][0] == 5||arr2[a][0] ==""){
              alert("请确认是否对所有险种已下核保结论！");
              return false;
          }
      }     
   }
   strSql2="select 1 from lcpol where prtno='"+fm.PrtNo.value+"' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and TaxOptimal='Y') and uwflag not in ('4','9')";
   arr2=easyExecSql(strSql2);
   if(arr2){
  	   alert("税优险种为非正常承保/变更承保，不能进行税优转移申请！");
       return false; 
   }
   strSql2="select 1 from lwmission where missionprop1='"+fm.PrtNo.value+"' and activityid in ('0000001100','0000001160') ";
   arr2=easyExecSql(strSql2);
   if(!arr2){
      alert("投保单非人工核保状态，不能进行税优转移申请！");
      return false;
   }
    
   return true;
}

//提交后（控制层执行完成返回结果后的操作）——save页面通过js调用这个函数
function afterSubmit1(FlagStr, content){
	
	showInfo.close();
	window.focus();
	var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	initForm();
}
