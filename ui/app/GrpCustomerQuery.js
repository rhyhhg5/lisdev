//程序名称：
//程序功能：
//创建日期：2011-02-10
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 
 */
function qryGrpCustomerList()
{
    if(!verifyInput2())
    {
        return false;
    }
    //alert("查询！");
    //alert("校验");
    //alert(fm.TradeRelaFlag.value);
    
    var tConf = "";
    
    var tTradeRelaFlag = fm.TradeRelaFlag.value;
    var tGrpName = fm.GrpName.value;
    if(tTradeRelaFlag == "1")
    {
    	tConf = " and ldg.relationflag = '1' ";
    }
    else if(tTradeRelaFlag == "0")
    {
    	tConf = " and (ldg.relationflag = '0' or ldg.relationflag is null) ";
    }
    
    var tStrSql = ""
        + " select "
        + " ldg.CustomerNo, ldg.GrpName, "
        + " case when ldg.relationflag = '1' then '是' else '否' end , "
        + " ldg.RelaName "
        + " from LDGrp ldg "
        + " where 1 = 1 "
        + "and ldg.GrpName like '%"+tGrpName+"%'"   
        + tConf 
        + getWherePart("ldg.CustomerNo", "CustomerNo")
        + getWherePart("ldg.OrganComCode", "OrganComCode")
        //+ getWherePart("ldg.GrpName", "GrpName", "like")
        + getWherePart("ldg.Makedate", "StartDate", ">=")
        + getWherePart("ldg.Makedate", "EndDate", "<=")
        
       ;
       
    turnPage1.pageDivName = "divGrpCustomerListGridPage";
    turnPage1.queryModal(tStrSql, GrpCustomerListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("未找到符合条件的保单信息！");
        return false;
    }
    fm.querySql.value = tStrSql;
    
    return true;
}

/**
 * 进入卡折录入。
 */
function inputData()
{
    var tRow = WIIPolicyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = WIIPolicyListGrid.getRowData(tRow);
    
    var tPrtNo = tRowDatas[0];
    var tManageCom = tRowDatas[1];
    //var tScanDate = tRowDatas[2];
    //var tInputDate = tRowDatas[3];
    //var tInputOperator = tRowDatas[4];
    var tMissionId = tRowDatas[5];
    var tSubMissionId = tRowDatas[6];
    var tProcessId = tRowDatas[7];
    var tActivityId = tRowDatas[8];
    var tActivityStatus = tRowDatas[9];
    
    if(!lockOperator(tPrtNo))
    {
        return false;
    }
    
    var tStrUrl = "./BriGrpContInputScanMain.jsp"
        + "?PrtNo=" + tPrtNo
        + "&MissionID=" + tMissionId
        + "&SubMissionID=" + tSubMissionId
        + "&ProcessID=" + tProcessId
        + "&ActivityID=" + tActivityId
        + "&ActivityStatus=" + tActivityStatus
        + "&ManageCom=" + tManageCom
        ;

    window.open(tStrUrl, "");
    
    WIIPolicyListGrid.clearData();
}
//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && GrpCustomerListGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "GrpRelaDowload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再下载！");
        return ;
    }
}

/**
 * 用户锁定
 */
function lockOperator(cPrtNo)
{
    var tState = "0";
    var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    
    var urlStr = "../app/ProposalScanApply.jsp?prtNo=" + cPrtNo + "&operator=" + tOperator + "&state=" + tState;
    var strReturn = window.showModalDialog(urlStr, "", sFeatures);

    var strSql = "select * from ldsystrace where PolNo='" + cPrtNo + "' and PolState=1002 ";
    
    var arrResult = easyExecSql(strSql);
    if (arrResult != null && arrResult[0][1] != tOperator)
    {
        alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
        return false;
    }
    
    var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + cPrtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
    showModalDialog(urlStr, window, sFeatures);
    
    if (strReturn != "1")
    {
        return false;
    }
    
    return true;
}

function submitConfirm(){
  var lineCount = 0;
  var selLineCount=0;
  lineCount = GrpCustomerListGrid.mulLineCount;
 for (var i=0;i<lineCount;i++){
		if (GrpCustomerListGrid.getChkNo(i)){
			selLineCount=selLineCount+1;
			}
		}
		if(selLineCount==0)
		{
		alert("请选择一条记录");
		return false;
		}


	fm.all('fmtransact').value = "submit";
	//alert("确认关联！");
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}

function calCel(){
 var lineCount = 0;
  var selLineCount=0;
  lineCount = GrpCustomerListGrid.mulLineCount;
 for (var i=0;i<lineCount;i++){
		if (GrpCustomerListGrid.getChkNo(i)){
			selLineCount=selLineCount+1;
			}
		}
		if(selLineCount==0)
		{
		alert("请选择一条记录");
		return false;
		}


	fm.all('fmtransact').value ="calcel";
	var showStr="正在取消关联，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    qryGrpCustomerList();
  }
}
