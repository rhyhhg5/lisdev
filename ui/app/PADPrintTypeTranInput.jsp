<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>";// 记录管理机构
    var comcode = "<%=tGI.ComCode%>";// 记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<!-- 通用录入校验要是用的 js 文件 -->
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!-- 使用双击下拉需要引入的 JS 文件 -->
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>PAD打印类型转换</title>
<!-- 自己的输入验证 js -->
<script src="PADPrintTypeTranInput.js"></script>
<!-- 初始化 mulline -->
<%@include file="PADPrintTypeTranInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="PADPrintTypeTranSave.jsp" method="post" name="fm" target="fraSubmit">

		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>

		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>管理机构</td>
				<td class=input>
					<!-- 查询 comcode 和 ManageComName 字段,在这个输入框显示查询出来的字段,然后在 name 为 ManageComName 的输入框显示查询的另一个结果,这个[0,1]控制结果显示顺序,是不是和前面的[this,ManageComName]功能重复了 -->
					<Input class=codeNo readonly=true name=ManageCom
					verify="管理机构|code:comcode"
					ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
					onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input
					class=codename name=ManageComName readonly=true>
				</td>
				<td class=title>印 刷 号</td>
				<td class=input><input class=common name=PrtNo /></td>
				<td class=title>合 同 号</td>
				<td class=input><input class=common name=ContNo /></td>
			</tr>
		</table>
		<INPUT VALUE="查询保单" class="cssButton" TYPE=button
			onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divLCPol1);"></td>
				<td class=titleImg>保单信息</td>
			</tr>
		</table>
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1><span id="spanPolGrid"></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button
					onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button
					onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button
					onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button
					onclick="getLastPage();">
			</table>
		</div>
		<br />
		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>修改前打印类型</td>
				<td class=input><Input class=readonly readOnly=true
					name=beforePrinttype><Input class=readonly readOnly=true
					name=beforePrinttypename></td>
				<td class=title></td>
				<td class=input></td>
				<td class=title></td>
				<td class=input></td>
			</tr>
			<tr class=common>
				<td class=title>修改后打印类型</td>
				<td class=input><Input class=codeNo readonly=true
					name=afterPrinttype CodeData="0|^0|纸质保单^1|电子保单" ondblClick="showCodeListEx('typecode',[this,afterPrinttypename],[0,1]);" onkeyup="showCodeListKeyEx('typecode',[this,afterPrinttypename],[0,1]);"><input
					class=codename name=afterPrinttypename readonly=true></td>
				<td class=title></td>
				<td class=input></td>
				<td class=title></td>
				<td class=input></td>
			</tr>
		</table>
		<INPUT VALUE="修改打印类型" class="cssButton" TYPE=button
			onclick="modifyPrintType();">
		<input type=hidden id="fmtransact" name="fmtransact">
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
