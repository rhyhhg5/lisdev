var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var k = 0;

/*******************************************************************************
 * 查询
 * 
 * @return
 */
function easyQueryClick() {
	// 空校验
	if (fm.all("otherno").value == "" || fm.all("otherno").value == null) {
		alert("请输入保单号！");
		return false;
	}

	// 初始化表格
	initGrpGrid();
	initGrpGrid1();
	// 书写SQL语句
	var strSQL = "";
	var strSQL1 = "";

	strSQL = "select TempFeeNo, OtherNo, OtherNoType, PayMoney from LJtempfee where 1=1 "
			+ getWherePart('otherno', 'otherno');

	strSQL1 = "select TempFeeNo, ChequeNo, SerialNo,PayMoney from LJtempfeeclass where tempfeeno in (select TempFeeNo from LJtempfee where 1=1 "
			+ getWherePart('otherno', 'otherno') + ")";
	//strSQL1 = "select TempFeeNo, ChequeNo, SerialNo,PayMoney from LJtempfeeclass where tempfeeno in ('1130006843219801','1130006843219802')";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	turnPage1.strQueryResult = easyQueryVer3(strSQL1, 1, 0, 1);

	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有查到相关数据！");
		return "";
	}

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);

	// 设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = GrpGrid;
	turnPage1.pageDisplayGrid = GrpGridClass;

	// 保存SQL语句
	turnPage.strQuerySql = strSQL;
	turnPage1.strQuerySql = strSQL1;

	// 设置查询起始位置
	turnPage.pageIndex = 0;
	turnPage1.pageIndex = 0;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,
			turnPage.pageIndex, MAXSCREENLINES);
	var arrDataSet1 = turnPage1.getData(turnPage1.arrDataCacheSet,
			turnPage1.pageIndex, MAXSCREENLINES);

	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	displayMultiline(arrDataSet1, turnPage1.pageDisplayGrid);
	return true;
}

// ***************************************************
// * 点击“删除”暂收表信息进行的操作
// ***************************************************
function deleteClick() {
	var checkFlag = 0;
	
	for (i = 0; i < GrpGrid.mulLineCount; i++) {
		if (GrpGrid.getSelNo(i)) {
			checkFlag = GrpGrid.getSelNo();
			break;
		}
	}
	
	if (checkFlag) {
		var tTempFeeNo = GrpGrid.getRowColData(checkFlag - 1, 1);
		if (tTempFeeNo == null || tTempFeeNo == "") {
			alert("删除时，获取暂交费收据号码失败！");
			return false;
		}
		if (confirm("您确实想删除该记录吗?")) {
			var i = 0;
			var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
					+ showStr;
			showInfo = window
					.showModelessDialog(urlStr, window,
							"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.fmtransact.value = "DELETE||MAIN";
			fm.action = "./operationSave.jsp?TempFeeNo=" + tTempFeeNo;
			fm.submit(); // 提交
			fm.action = "";
			initForm();
		} else {
			alert("您取消了删除操作！");
		}
	} else {
		alert("请选择需要删除的暂收表信息！");
		return false;
	}
}

// ***************************************************
// * 提交操作完成后的处理
// ***************************************************
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	//easyQueryClick();
}
