var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();



//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput())
	{
      var i = 0;
      var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.submit(); //提交
	}
}

//查询险种信息
function QueryRiskButt()
{
	initQueryLDRiskGrid();
	
	var sql="";
	sql = "select riskcode,lm.riskname,ld.codename,case when ld.codename is not null then '1' else '0' end maxnum from lmriskapp lm left join ldcode ld on lm.riskcode=ld.code and ld.codetype='accidentageamnt' where lm.riskprop='G' "
	      + getWherePart( 'riskcode','RiskCode');
	//查询SQL，返回结果字符串
    turnPage.queryModal(sql,QueryLDRiskGrid); 

    //判断是否查询成功
    if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    QueryLDRiskGrid.clearData('QueryLDRiskGrid');  
    alert("没有查询到数据！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = QueryLDRiskGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = sql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
}

//显示修改明细
function ShowDetail(){
	
	 var tSel = 0;
	 tSel = QueryLDRiskGrid.getSelNo();
		try
		{
			if (tSel !=0 )
			   divLLMainAskInput5.style.display = "";
			else
				 divLLMainAskInput5.style.display = "none";
			fm.all('repRiskCode').value=QueryLDRiskGrid.getRowColData(tSel-1,1);	 
			fm.all('repCodeName').value=QueryLDRiskGrid.getRowColData(tSel-1,3);
			oprateButt();
		}
		catch(ex)
		{
			alert( "没有发现父窗口的接口" + ex );
		}
}

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput())
	{
      var i = 0;
      var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.submit(); //提交
	}
}

function afterSubmit(FlagStr,code,operate)
{
  showInfo.close();
  window.focus;
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "操作失败" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    window.location.reload();
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" +  "操作成功" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    window.location.reload();
  }     
	
}

//添加限制保额
function AddRiskButt()
{
	if (fm.repCodeName.value == null ||fm.repCodeName.value == "" )
	{
		alert('限制保额不能为空！');
		return;
	}
	
	fm.mOperate.value = "HEAD";
	
	submitForm();
}

//修改数据
function RepRiskButt()
{
	if (fm.repCodeName.value == null ||fm.repCodeName.value == "" )
	{
		alert('限制保额不能为空！');
		return;
	}	
	
	fm.mOperate.value = "REPAIR";
	
	submitForm();	
}

//删除数据
function DelRiskButt()
{
	fm.mOperate.value = "DELETE";
	if (confirm("您确实想删除该记录吗?"))
    {
      var i = 0;
      var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      
     // fm.submit(); //提交
     // fm.repRiskCode.value = '';
     // fm.repCodeName.value = '' ;
     // QueryRiskButt();
      submitForm();
    }
    else
    {
       alert("您取消了删除操作！");
    } 
}

//触发button置灰
function oprateButt()
{
	if (fm.repCodeName.value == null ||fm.repCodeName.value == "" )
	{
		document.getElementById("addbutton").disabled=""; 
		document.getElementById("updatebutton").disabled="none"; 
		document.getElementById("deletebutton").disabled="none";
	}
	else
	{
	    document.getElementById("addbutton").disabled="none"; 
	    document.getElementById("updatebutton").disabled=""; 
		document.getElementById("deletebutton").disabled=""; 
	}
}











