<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：changeFloatRate.jsp
//程序功能：浮动费率修改
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	String tPolNo = "";
	tPolNo = request.getParameter("PolNo");
	session.putValue("PolNo",tPolNo);
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	//var type = "ChangePlan";
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="changeFloatRate.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="changeFloatRateInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./">
    <!-- 保单查询条件 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
<!--          <TD  class= title>
            投保单号码
          </TD>-->
            <Input class= common name=QProposalNo type="hidden">
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="codeno" name=QRiskCode ondblclick="return showCodeList('RiskCode',[this,QRiskCodeName],[0,1]);" onkeyup="return showCodeListKey('RiskCode',[this,QRiskCodeName],[0,1]);"><input class = codename name = QRiskCodeName>
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
             <Input class="codeno" name=QManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,QManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,QManageComName],[0,1]);"><input class = codename name = QManageComName> 
          </TD>
          <TD  class= title>
            印刷号
          </TD>
          <TD  class= input>
            <Input class=common name=PrtNo>
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" class=common TYPE=button onclick="easyQueryClick();"> 
          <INPUT type= "hidden" name= "Operator" value= ""> 
    
    
    <!-- 保单查询结果部分（列表） -->
    <Div  id= "divLCPol1" style= "display: ''">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 主险保单查询结果
    		</td>
    	</tr>
    </table>
    
      	<table  class= common>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	
      <INPUT VALUE="首页" class=common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=common TYPE=button onclick="getLastPage();">      
    </div>
    
   <Div  id= "divMain" style= "display:''">

	<table class= common border=0 width=100% >
	  <hr>
	  </hr>     
          <span  id= "divAddButton10" style= "display: ''">
          	<input value="修改浮动费率" class=common type=button onclick="showChangePlan();">
          </span>
    </table>

  </Div>         
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
