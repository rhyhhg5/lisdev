<%
//程序名称：TbGrpConractorInit.jsp
//程序功能：
//创建日期：2006-5-17 15:19
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
  }
  catch(ex)
  {
    alert("在LDDrugClassInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在TbGrpConractorInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
  	//alert(LoadFlag);
  	if(LoadFlag=='16'||LoadFlag=='23')
  	{
  	 ButtonDiv.style.display="none"; 
  	}
    initInpBox();
    initSelBox();    
    initConractorGrid();
    initDutyFactor(); 
    displayConstruct();
  }
  catch(re)
  {
    alert("TbGrpConractorInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}
function initConractorGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            	//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="要素";         			//列名
		iArray[1][1]="220px";            	//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
		iArray[2][0]="要素名称";         	//列名
		iArray[2][1]="108px";            	//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="要素说明";         	//列名
		iArray[3][1]="265px";            	//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="要素值";         		//列名
		iArray[4][1]="150px";            	//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="计划要素类型";      //列名
		iArray[5][1]="120px";            	//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="账户金额";         	//列名
		iArray[6][1]="120px";            	//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许	
		
		
		ConractorGrid = new MulLineEnter( "fm" , "ConractorGrid" ); 
		//这些属性必须在loadMulLine前
		ConractorGrid.mulLineCount = 0;   
		ConractorGrid.displayTitle = 1;
		ConractorGrid.locked = 0;
		ConractorGrid.canSel = 0;
		ConractorGrid.canChk = 0;
		ConractorGrid.hiddenPlus = 1;
		ConractorGrid.hiddenSubtraction = 1;
		ConractorGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
}
</script>