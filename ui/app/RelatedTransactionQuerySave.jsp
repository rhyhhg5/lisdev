<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	//输出参数
	CErrors tError = null;
  	String FlagStr = "";
  	String Content = "";
  	GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");
  	String transact = request.getParameter("transact");
  	
  	LDRelatedPartySchema tLDRelatedPartySchema = new LDRelatedPartySchema();
  	LDRelatedPartyDB tLDRelatedPartyDB = new LDRelatedPartyDB();
  	String tRelateNo = request.getParameter("RelateNobak");
  	if("DELETE".equals(transact)){
  		if(tRelateNo == null || "".equals(tRelateNo)){
  			Content = "删除失败，原因是:获取关联方编号失败！";
  			FlagStr = "Fail";
  		}
  		tLDRelatedPartyDB.setRelateNo(tRelateNo);
  		LDRelatedPartySet tLDRelatedPartySet = tLDRelatedPartyDB.query();
  		if(tLDRelatedPartySet.size()!=1){
  		  	Content = "删除失败，原因是:获取关联方失败！";
  			FlagStr = "Fail";
  		}
  		tLDRelatedPartySchema=tLDRelatedPartySet.get(1);
  	}
  	
  	try{
  		VData tVData = new VData();
  		tVData.add(tG);
  		tVData.add(tLDRelatedPartySchema);
  		RelatedTransactionQueryUI tRelatedTransactionUIQuery = new RelatedTransactionQueryUI();
  		if(!tRelatedTransactionUIQuery.submitData(tVData,transact)){
  			Content = "处理失败，原因是:" + tRelatedTransactionUIQuery.mErrors.getFirstError();
            FlagStr = "Fail";
  		}else{
  			Content = "处理成功";
            FlagStr = "Succ";
  		}
  	}catch(Exception ex){
  		ex.printStackTrace();
  	}
  	
%>

<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

