<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：DownLoadGifSave.sjp
//程序功能：投保单扫描件删除功能
//创建日期：20190516
//创建人  ：zxs
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@ page import="com.sinosoft.utility.*" %>
<%@ page import="com.sinosoft.lis.pubfun.*" %>
<%@ page import="com.sinosoft.lis.tb.*" %>
<%@include file="../common/jsp/Download.jsp"%>

<%
System.out.println("begin DownLoadGifSave.jsp...");

//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";

GlobalInput tG = (GlobalInput)session.getValue("GI");

String tDocId = request.getParameter("DocId");
System.out.println("docId : " + tDocId);

TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("DocId", tDocId);

DownLoadGifUI tDownLoadGifUI = new DownLoadGifUI();
try
{
    VData tVData = new VData();
    tVData.add(tG);
    tVData.add(tTransferData);
    
    if (!tDownLoadGifUI.submitData(tVData, null))
    {
        Content = " 下载失败! 原因是: " + tDownLoadGifUI.mErrors.getLastError();
        FlagStr = "Fail";
    }
   
}
catch(Exception ex)
{
    Content = "下载失败，原因是:" + ex.toString();
    FlagStr = "Fail";
}
    String prtno =   new ExeSQL().getOneValue("select doccode from es_doc_main where docid = '"+tDocId+"'");
    System.out.println("end DownLoadGifSave.jsp...");
	downLoadFile(response,"E:/",prtno+".pdf");
	out.clear();
	out = pageContext.pushBody();
	
	 File file=new File("E:/"+prtno+".pdf");
     if(file.exists()&&file.isFile()){
    	 file.delete(); 
     }
%>
<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
