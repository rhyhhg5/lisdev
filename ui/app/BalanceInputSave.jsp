<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>

<%
	
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String CurrentDate = PubFun.getCurrentDate();
	String CurrentTime = PubFun.getCurrentTime();
	
	//输入参数
	LCGrpContSubSet tLCGrpContSubSet = new LCGrpContSubSet();
	BalanceInfoUI mBalanceInfoUI = new BalanceInfoUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	
	String mPrtNo = request.getParameter("PrtNo");

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");

	if(mPrtNo == null || "".equals(mPrtNo))
	{
		FlagStr = "Fail";
		Content = "获取保单印刷号码失败！";
	}
	System.out.println("保单印刷号:" + mPrtNo);
	
	if(!"Fail".equals(FlagStr)){
		LCGrpContSubSchema tLCGrpContSubSchema = new LCGrpContSubSchema();
		tLCGrpContSubSchema.setPrtNo(mPrtNo);
		tLCGrpContSubSchema.setProjectName(request.getParameter("ProjectName"));
		//解决BalanceTermFlag为N时无法传值的问题。
		String sss = request.getParameter("BalanceTermFlag");
		System.out.println("BalanceTermFlag:"+sss);
		if(sss == "" || sss == null){
			tLCGrpContSubSchema.setBalanceTermFlag(request.getParameter("transferN"));
		}else{
			tLCGrpContSubSchema.setBalanceTermFlag(request.getParameter("BalanceTermFlag"));
		}
		tLCGrpContSubSchema.setBalanceAmnt(request.getParameter("BalanceAmnt"));
		/* tLCGrpContSubSchema.setAccountCycle(request.getParameter("AccountCycle"));
		tLCGrpContSubSchema.setBalanceLine(request.getParameter("BalanceLine"));
		tLCGrpContSubSchema.setCostRate(request.getParameter("CostRate"));
		tLCGrpContSubSchema.setBalanceRate(request.getParameter("BalanceRate"));
		tLCGrpContSubSchema.setBalanceType(request.getParameter("BalanceType"));
		tLCGrpContSubSchema.setStopLine(request.getParameter("StopLine"));
		tLCGrpContSubSchema.setSharedLine(request.getParameter("SharedLine"));
		tLCGrpContSubSchema.setSharedRate(request.getParameter("SharedRate"));
		tLCGrpContSubSchema.setRevertantLine(request.getParameter("RevertantLine"));
		tLCGrpContSubSchema.setRevertantRate(request.getParameter("RevertantRate")); */
		tLCGrpContSubSchema.setBalanceTxt(request.getParameter("BalanceTxt"));
		tLCGrpContSubSchema.setbak1(request.getParameter("XBFlag"));
		tLCGrpContSubSchema.setRecharge(request.getParameter("Recharge"));
		tLCGrpContSubSchema.setRebalance(request.getParameter("Rebalance"));
		
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.addElement(tLCGrpContSubSchema);
		try {
			mBalanceInfoUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = mBalanceInfoUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>