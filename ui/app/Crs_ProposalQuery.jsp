<%
//程序名称：Grs_ProposalQuery.jsp
//程序功能：复核不通过修改
//创建日期：2010-11-05 17:06:57
//创建人  ：郭忠华
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ProposalQuery.js"></SCRIPT>
  <%@include file="ProposalQueryInit.jsp"%>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <title>集团交叉销售保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraTitle">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入集团交叉销售查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      <!--    <TD  class= title> 投保单号码  </TD>-->
          <Input class= common name=ContNo  type="hidden">
          <TD  class= title> 印刷号码 </TD>
          <TD  class= input><Input class= common name=PrtNo verify="印刷号码|int"> </TD>
          <TD  class= title>  投保人姓名  </TD>
          <TD  class= input> <Input class= common name=AppntName verify="投保人姓名|len<=120"> </TD>
          <TD  class= title>  被保人姓名</TD>
          <TD  class= input> <Input class= common name=InsuredName verify="被保人姓名|len<=120"> </TD>     
           </TR>
        <TR  class= common>
        	<TD  class= title> 申请日期 </TD>
        	<TD  class= input><Input class=coolDatePicker name=PolApplyDate>  </TD>  
          <TD  class= title> 生效日期 </TD>
        	<TD  class= input><Input class=coolDatePicker name=CValiDate>  </TD>  
          <TD  class= title> 管理机构 </TD>
          <TD  class= input><Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true > </TD>     
       </TR>
        <TR  class= common>
          <TD  class= title> 业务员代码  </TD>
          <TD class=input nowrap="true"><Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCodet" ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4],null,fm.all('ManageCom').value,'ManageCom',1);"><input class=codename name=AgentCodeName readonly=true >
            <input class="common"  type="button" value="?" onclick="return queryAgentCode();" style="width:20">
          <!--<TD  class= title> 被保人性别  </TD>-->
          <!--<<TD class=input>-->  <Input class= codeNo type=hidden name=InsuredSex verify="被保人性别|code:SexRegister" CodeData="0|^0|男^1|女^2|不详" ondblClick="showCodeListEx('SexRegister',[this,InsuredSexName],[0,1,2,3]);" onkeyup="showCodeListKeyEx('SexObjRegister',[this,InsuredSexName],[0,1,2,3]);" ><input class=codename type=hidden name=InsuredSexName readonly=true elementtype=nacessary>  <!--</TD>-->        
          <!--<TD  class= title> 保单状态 </TD>-->
          <!--<<TD  class= input>--><Input class=codeNo type=hidden name=PolState verify="管理机构|code:ContState" ondblclick="return showCodeList('ContState',[this,PolStateName],[0,1]);" onkeyup="return showCodeListKey('ContState',[this,PolStateName],[0,1]);"><input class=codename type=hidden name=PolStateName readonly=true > </TD>          
          <!--<TD  class= title>  核保结论</TD>-->
          <!--<<TD  class= input>--> <Input class=codeNo type=hidden name=UWStateQuery readonly =trun verify="核保结论|code:UWStateQuery" ondblclick= "return showCodeList('UWStateQuery',[this,UWStateQueryName],[0,1]);" onkeyup="return showCodeListKey('UWStateQuery',[this,UWStateQueryName],[0,1]);"><input class=codename type=hidden name=UWStateQueryName readonly=true >  <!--</TD>-->
          <!--<TD  class= title> 业务员组别 </TD>-->
          <!--<<TD  class= input>--><Input class="codeNo" type=hidden name=AgentGroup verify="业务员组别|code:branchcode" ondblclick="return showCodeList('agentgroup1',[this,AgentGroupName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('agentgroup1',[this,AgentGroupName],[0,1]);"><input class=codename type=hidden name=AgentGroupName readonly=true > <!--</TD>-->
          <TD  class= title>
            业务归档号
          </TD>
          <TD  class= input>
            <Input class= common name=ArchiveNo verify="业务归档号|len=18">
          </TD>
          <TD  class= title>
            渠道
          </TD>
          <TD  class= input>
            <input class="codeNo" name="SaleChnlCode" ondblclick="return showCodeList('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" />
          </TD>
        </TR> 
        <TR>
        	<TD class="title8">网点</TD>
            <TD class="input8">
                      <input id="AgentComBank" class="code" name="AgentComBank" style="display:none;" ondblclick="return queryAgentComBank();" />
            </TD>
        	<TD></TD><TD></TD><TD></TD>        	
        </TR>              
    </table>
    
          <INPUT class=cssbutton VALUE="查询投保单" TYPE=button onclick="Crs_easyQueryClick();"> 
          <INPUT class=cssbutton VALUE="清单下载" TYPE=button onclick="downloadClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 集团交叉销售保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: '' ">
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div> 					
  	</div>
  	  	<Div  id= "divLCPol2" style= "display: ''">
      	<table  class= common>
      		<tr>
    		 <td class= titleImg>
    			 状态明细
    		 </td>
    	   </tr>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanPolStatuGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>		
  	</div>
		<input type=hidden id="GrpPolNo" name="GrpPolNo">
		<table>
			<tr>
				<td>
					<INPUT class=cssbutton VALUE="投保件状态明细" TYPE=button onclick="queryStateClick();"> 	
          <!--<INPUT class=cssbutton VALUE="投保书明细" TYPE=button onclick="queryDetailClick();">--> 	
          <INPUT class=cssbutton VALUE="投保书图像查询" TYPE=button onclick="ScanQuery();">
          <!--<INPUT class=cssbutton VALUE="查看外包错误" TYPE=button onclick="viewBPOIssue();">-->
          <!--INPUT class=cssbutton VALUE="填单问题查询" TYPE=button onclick=""-->
          <!--INPUT class=cssbutton VALUE="体检查询" TYPE=button onclick=""-->
          <INPUT TYPE=hidden name=ProState value="">
				</td>
			</tr>
		</table>
	  <Div  id= "divEsDocPages1Title" style= "display: 'none'">
		 <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEsDocPages1);">
    		</td>
    		<td class= titleImg>
        		 扫描件基本信息
       	</td>   		 
    	</tr>
    </table>
  </div>

    <Div  id= "divEsDocPages1" style= "display: 'none'" align=center>
    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanEsDocPagesGrid">
	  				</span> 
			    </td>
				</tr>
		</table>
    </Div>


  <INPUT  type= "hidden" class= Common name= PrtNo2 >
  <INPUT  type= "hidden" class= Common name= querySql >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
