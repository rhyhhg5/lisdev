<%
  //程序名称：ContPlanShareAmntInit.jsp
  //程序功能：共用保额要素维护
  //创建日期：2010-04-06
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">

function initForm()
{
	try{
		initMul12Grid();
		initMul11Grid();
		initMul10Grid();
		fm.ManageCom.value = "<%=tGI.ManageCom%>";
	}
	catch(re){
		alert("ContPlanShareAmntInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initMul12Grid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="印刷号";
		iArray[1][1]="45px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		iArray[1][21]="PrtNo";

		iArray[2]=new Array();
		iArray[2][0]="保单号";
		iArray[2][1]="45px";
		iArray[2][2]=100;
		iArray[2][3]=0;
		iArray[2][21]="GrpContNo";

		iArray[3]=new Array();
		iArray[3][0]="投保单位";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="生效日期";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="失效日期";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;


		Mul12Grid = new MulLineEnter( "fm" , "Mul12Grid" ); 

		Mul12Grid.mulLineCount=1;
		Mul12Grid.displayTitle=1;
		Mul12Grid.canSel=1;
		Mul12Grid.canChk=0;
		Mul12Grid.hiddenPlus=1;
		Mul12Grid.hiddenSubtraction=1;
		Mul12Grid.selBoxEventFuncName = "onSelSearchPlan";

		Mul12Grid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initMul11Grid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="保障计划";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		iArray[1][21]="ContPlanCode";

		iArray[2]=new Array();
		iArray[2][0]="人员类别";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="参保人数";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="应保人数";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;


		Mul11Grid = new MulLineEnter( "fm" , "Mul11Grid" ); 

		Mul11Grid.mulLineCount=1;
		Mul11Grid.displayTitle=1;
		Mul11Grid.canSel=1;
		Mul11Grid.canChk=0;
		Mul11Grid.hiddenPlus=1;
		Mul11Grid.hiddenSubtraction=1;
		Mul11Grid.selBoxEventFuncName = "onSelSearchDutyParam";

		Mul11Grid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initMul10Grid()
{
	var iArray = new Array();
	try{
		  iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种名称";    	        //列名
      iArray[1][1]="190px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;                       //是否允许输入,1表示允许，0表示不允许 2表示代码选择

      iArray[2]=new Array();
      iArray[2][0]="险种代码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=150;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="责任代码";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]= 60;            			//列最大值
      iArray[3][3]= 3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][10]="DutyCode";


      iArray[4]=new Array();
      iArray[4][0]="险种责任";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[5]=new Array();
      iArray[5][0]="计算要素";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=3;
      iArray[5][10]="FactorCode";            			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="要素名称";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=150;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="要素说明";         		//列名
      iArray[7][1]="130px";            		//列宽
      iArray[7][2]=150;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="要素值";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=150;            			//列最大值
      iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
			iArray[8][21]="CalFactorValue";

      iArray[9]=new Array();
      iArray[9][0]="特别说明";         		//列名
      iArray[9][1]="150px";            		//列宽
      iArray[9][2]=150;            			//列最大值
      iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="险种版本";         		//列名
      iArray[10][1]="100px";            		//列宽
      iArray[10][2]=10;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[11]=new Array();
      iArray[11][0]="集体保单险种号码";         		//列名
      iArray[11][1]="100px";            		//列宽
      iArray[11][2]=10;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[12]=new Array();
      iArray[12][0]="主险代码";         		//列名
      iArray[12][1]="100px";            		//列宽
      iArray[12][2]=10;            			//列最大值
      iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[13]=new Array();
      iArray[13][0]="类型";         		//列名
      iArray[13][1]="100px";            		//列宽
      iArray[13][2]=10;            			//列最大值
      iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[14]=new Array();                                      
      iArray[14][0]="缴费计划代码";         		//列名                                      
      iArray[14][1]="100px";            		//列宽                                      
      iArray[14][2]=10;            			//列最大值                                      
      iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许      
      
      iArray[15]=new Array();                                      
      iArray[15][0]="给付责任代码";         		//列名                                      
      iArray[15][1]="100px";            		//列宽                                      
      iArray[15][2]=10;            			//列最大值                                      
      iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许    
      
      iArray[16]=new Array();                                      
      iArray[16][0]="账户号码";         		//列名                                      
      iArray[16][1]="100px";            		//列宽                                      
      iArray[16][2]=10;            			//列最大值                                      
      iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许


		Mul10Grid = new MulLineEnter( "fm" , "Mul10Grid" ); 

		Mul10Grid.mulLineCount=1;
		Mul10Grid.displayTitle=1;
		Mul10Grid.canSel=0;
		Mul10Grid.canChk=0;
		Mul10Grid.hiddenPlus=1;
		Mul10Grid.hiddenSubtraction=1;

		Mul10Grid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
