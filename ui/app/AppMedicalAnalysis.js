var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
    var i = 0;
    var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
	if(dateDiff(tStartDate,tEndDate,"M")>12)
	{
		alert("统计期最多为12个月！");
		return false;
	}
    var tManageCom = fm.ManageCom.value;
    var tSaleChnl = fm.SaleChnl.value;  
    var strSQL = "select a.managecom,a.prtseq,a.otherno,a.makedate,b.makedate,d.groupagentcode,d.name,c.branchattr,c.name "
             + "from  Loprtmanager a, es_doc_main b,labranchgroup c,laagent d where "
             + "a.agentcode= d.agentcode and d.agentgroup = c.agentgroup and a.code='03' and  a.prtseq like '16%' "
             + "and a.managecom like '"+tManageCom+"%' and a.otherno in (select contno from lccont where 1=1 "
             + getWherePart('SaleChnl','SaleChnl')
             + ") and a.makedate between '"+tStartDate+"' and '"+tEndDate+"'  and b.doccode=a.prtseq order by a.managecom,c.branchattr,a.agentcode with ur "           

    fm.AnalysisSql.value = strSQL; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	} 
	fm.submit();
}