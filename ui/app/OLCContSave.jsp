<%
//程序名称：OLCContInput.jsp
//程序功能：
//创建日期：2002-08-16 16:22:25
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LCContSchema tLCContSchema   = new LCContSchema();

  OLCContUI tOLCCont   = new OLCContUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLCContSchema.setProposalNo(request.getParameter("ProposalNo"));
    tLCContSchema.setPrtNo(request.getParameter("PrtNo"));
    tLCContSchema.setManageCom(request.getParameter("ManageCom"));
    tLCContSchema.setSaleChnl(request.getParameter("SaleChnl"));
    tLCContSchema.setAgentCom(request.getParameter("AgentCom"));
    tLCContSchema.setAgentCode(request.getParameter("AgentCode"));
    tLCContSchema.setGrpNo(request.getParameter("GrpNo"));
    tLCContSchema.setName(request.getParameter("Name"));
    tLCContSchema.setContType(request.getParameter("ContType"));
    tLCContSchema.setLinkMan(request.getParameter("LinkMan"));
    tLCContSchema.setPhone(request.getParameter("Phone"));
    tLCContSchema.setFax(request.getParameter("Fax"));
    tLCContSchema.setAddress(request.getParameter("Address"));
    tLCContSchema.setZipCode(request.getParameter("ZipCode"));
    tLCContSchema.setEMail(request.getParameter("EMail"));


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLCContSchema);
	tVData.add(tG);
  try
  {
    tOLCCont.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tOLCCont.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

