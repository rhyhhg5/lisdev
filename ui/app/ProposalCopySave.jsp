<%
//程序名称：ProposalCopyInput.jsp
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LCGrpPol1Schema tLCGrpPol1Schema   = new LCGrpPol1Schema();

  ProposalCopyUI tProposalCopy   = new ProposalCopyUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");


    tLCGrpPol1Schema.setProposalNo(request.getParameter("ProposalNo"));
    tLCGrpPol1Schema.setGrpProposalNo(request.getParameter("GrpProposalNo"));
    tLCGrpPol1Schema.setCValiDate(request.getParameter("CValiDate"));
    tLCGrpPol1Schema.setRiskCode(request.getParameter("RiskCode"));
    tLCGrpPol1Schema.setRiskVersion(request.getParameter("RiskVersion"));
    tLCGrpPol1Schema.setPayLocation(request.getParameter("PayLocation"));
    tLCGrpPol1Schema.setPayIntv(request.getParameter("PayIntv"));
    tLCGrpPol1Schema.setPayEndYear(request.getParameter("PayEndYear"));
    tLCGrpPol1Schema.setPayEndYearFlag(request.getParameter("PayEndYearFlag"));
    tLCGrpPol1Schema.setGetYear(request.getParameter("GetYear"));
    tLCGrpPol1Schema.setGetYearFlag(request.getParameter("GetYearFlag"));
    tLCGrpPol1Schema.setGetStartType(request.getParameter("GetStartType"));
    tLCGrpPol1Schema.setInsuYear(request.getParameter("InsuYear"));
    tLCGrpPol1Schema.setInsuYearFlag(request.getParameter("InsuYearFlag"));
    tLCGrpPol1Schema.setMult(request.getParameter("Mult"));
    tLCGrpPol1Schema.setPrem(request.getParameter("Prem"));
    tLCGrpPol1Schema.setAmnt(request.getParameter("Amnt"));
    tLCGrpPol1Schema.setGrpNo(request.getParameter("GrpNo"));
    tLCGrpPol1Schema.setName(request.getParameter("Name"));
    tLCGrpPol1Schema.setLinkMan(request.getParameter("LinkMan"));
    tLCGrpPol1Schema.setPhone(request.getParameter("Phone"));
    tLCGrpPol1Schema.setAddress(request.getParameter("Address"));
    tLCGrpPol1Schema.setZipCode(request.getParameter("ZipCode"));
    tLCGrpPol1Schema.setFax(request.getParameter("Fax"));
    tLCGrpPol1Schema.setEMail(request.getParameter("EMail"));


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLCGrpPol1Schema);
	tVData.add(tG);
  try
  {
    tProposalCopy.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tProposalCopy.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

