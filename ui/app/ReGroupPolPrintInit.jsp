<%
//程序名称：ReGroupPolPrintInit.jsp
//程序功能：
//创建日期：2002-11-26
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{
	try
	{
		// 保单查询条件
		fm.all('PrtNo').value = '';
		fm.all('GrpContNo').value = '';
		fm.all('AgentCode').value = '';
		fm.all('AgentGroup').value = '';
		fm.all('GrpName').value = '';
	}
	catch(ex)
	{
	alert("在ReGroupPolPrintInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm()
{
	try
	{
		initInpBox();
		initGrpPolGrid();
	  fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
	}
	catch(re)
	{
		alert("ReGroupPolPrintInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

// 保单信息列表的初始化
function initGrpPolGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="保险合同号";
		iArray[1][1]="100px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="印刷号";
		iArray[2][1]="100px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="团单保费";
		iArray[3][1]="80px";
		iArray[3][2]=200;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="投保人名称";
		iArray[4][1]="300px";
		iArray[4][2]=200;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="生效日期";
		iArray[5][1]="80px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		GrpPolGrid = new MulLineEnter( "fmSave" , "GrpPolGrid" );
		//这些属性必须在loadMulLine前
		GrpPolGrid.mulLineCount = 10;
		GrpPolGrid.displayTitle = 1;
		GrpPolGrid.locked = 1;
		GrpPolGrid.canChk = 1;
		GrpPolGrid.hiddenPlus = 1;
		GrpPolGrid.hiddenSubtraction = 1;
		GrpPolGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>