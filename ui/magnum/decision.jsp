<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sinosoft.utility.ExeSQL"%>
<%@page import="java.io.*"%>
<%@page import="java.net.*"%>
<!DOCTYPE html>
<html lang="en">
<% 
String Uuid = request.getParameter("Uuid");
System.out.println("Uuid==============:" + Uuid);
String mresponse = "";
String tSQL="select sysvarvalue from ldsysvar where sysvar = 'MagNum' ";
String	MagNumurl = new ExeSQL().getOneValue(tSQL);
System.out.println("MagNumurl:" + MagNumurl);
String url=MagNumurl+Uuid+"/detailedDecision.json?language=zh_CN";
//String url = "http://10.136.10.22:9080/webapp/engine/rest/v1/cases/cca2e09e-cdb9-4f4b-84c6-591712f108dc/detailedDecision.json?language=zh_CN";
System.out.println("url:" + url);
URL obj;
try {
	obj = new URL(url);
	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	  con.setRequestMethod("GET");
	  con.setRequestProperty("contentType", "UTF-8"); 
	  int responseCode = con.getResponseCode();
	  BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
	  String inputLine;
	  StringBuffer tresponse = new StringBuffer();
	  while ((inputLine = in.readLine()) != null) {
		  tresponse.append(inputLine);
	  }
	  in.close();
	  mresponse = tresponse.toString();
	  System.out.println("aa==="+mresponse);
} catch (MalformedURLException e) {
	e.printStackTrace();
} catch (IOException e) {
	e.printStackTrace();
}
%>
<head>
    <meta charset="GBK">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Magnum Pure Web Api UI Components</title>
       <script src="./build/vendor/underscore-min.js"></script>
    <script src="./build/vendor/jquery.min.js"></script>
    <script src="./build/vendor/bootstrap.min.js"></script>
    <script src="./build/vendor/angular.min.js"></script>
    <script src="./build/vendor/angular-ui-router.min.js"></script>
    <script src="./build/vendor/angular-local-storage.min.js"></script>
    <script src="./build/vendor/angular-translate.min.js"></script>
    <script src="./build/vendor/angular-translate-loader-static-files.min.js"></script>
    <script src="./build/vendor/angular-mocks.js"></script>
    <script src="./build/plain-app.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" href="./build/styles.css">
</head>
<body>
<div style="max-width: 800px; margin: 0 auto;">

    <div id="magnum-decision-ui-container"></div>
<a href="javascript :;" onClick="javascript :parent.close();">back</a>

    <script>
        var translations = {
            "app.details": "Details",
            "app.details.applicant": "Applicant",
            "app.details.benefit": "Benefits",
            "synopsis.application": "Application as function parameter",
            "synopsis.benefit": "Benefit",
            "synopsis.decision.adjustments": "Adjustments",
            "synopsis.decision.requirements": "Requirements",
            "synopsis.decision.sup.text": "Supporting text",
            "synopsis.insurance.product.decision": "Decision",
            "synopsis.product": "Product",
            "synopsis.risk.assessment": "Risk assessment"
        };
        var container = document.querySelector('#magnum-decision-ui-container');
        var jsontext= '<%=mresponse%>';
        var contact = JSON.parse(jsontext);
        window.mxgWebApiDecision(container, contact, 'AVUK-213123', translations)
    </script>
</div>
</body>
</html>
