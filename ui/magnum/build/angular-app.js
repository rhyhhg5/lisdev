/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	__webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var decisionModule_1 = __webpack_require__(2);
	var injectorModule_1 = __webpack_require__(27);
	var Injector_1 = __webpack_require__(10);
	__webpack_require__(29);
	var deps = [
	    'pascalprecht.translate',
	    injectorModule_1.injectorModule,
	    decisionModule_1.decisionModule
	];
	exports.mxgWebApiUiComponentsModule = angular.module('mxgWebApiUiComponents', deps)
	    .config(function ($translateProvider) {
	    $translateProvider.translations('default', __webpack_require__(38));
	    $translateProvider.preferredLanguage('default');
	}).run(function () {
	    Injector_1.injector.get('$translate')('');
	}).name;


/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var DecisionDirective_1 = __webpack_require__(3);
	var DecisionBenefitsSectionDirective_1 = __webpack_require__(14);
	var DecisionDetailsDirective_1 = __webpack_require__(17);
	var DecisionDetailsNodeDirective_1 = __webpack_require__(19);
	var VirtualTreeNodeDirective_1 = __webpack_require__(22);
	var VirtualTreeTogglerDirective_1 = __webpack_require__(25);
	var VirtualTreeDirective_1 = __webpack_require__(23);
	exports.decisionModule = angular.module('decision', [])
	    .directive(DecisionDirective_1.DecisionDirective.getName(), DecisionDirective_1.DecisionDirective.getDefinition())
	    .directive(DecisionBenefitsSectionDirective_1.DecisionBenefitsSectionDirective.getName(), DecisionBenefitsSectionDirective_1.DecisionBenefitsSectionDirective.getDefinition())
	    .directive(DecisionDetailsDirective_1.DecisionDetailsDirective.getName(), DecisionDetailsDirective_1.DecisionDetailsDirective.getDefinition())
	    .directive(DecisionDetailsNodeDirective_1.DecisionDetailsNodeDirective.getName(), DecisionDetailsNodeDirective_1.DecisionDetailsNodeDirective.getDefinition())
	    .directive(VirtualTreeDirective_1.VirtualTreeDirective.getName(), VirtualTreeDirective_1.VirtualTreeDirective.getDefinition())
	    .directive(VirtualTreeNodeDirective_1.VirtualTreeNodeDirective.getName(), VirtualTreeNodeDirective_1.VirtualTreeNodeDirective.getDefinition())
	    .directive(VirtualTreeTogglerDirective_1.VirtualTreeTogglerDirective.getName(), VirtualTreeTogglerDirective_1.VirtualTreeTogglerDirective.getDefinition())
	    .name;


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var Directive_1 = __webpack_require__(4);
	var BaseDirective_1 = __webpack_require__(7);
	var DecisionTexts_1 = __webpack_require__(9);
	var VirtualTreeService_1 = __webpack_require__(11);
	var IDecisionDirectiveScopeModel_1 = __webpack_require__(12);
	var DecisionDirective = (function (_super) {
	    __extends(DecisionDirective, _super);
	    function DecisionDirective($scope) {
	        var _this = this;
	        _super.call(this, $scope);
	        this.$scope = $scope;
	        this.texts = {
	            benefits: [],
	            level1: [],
	            level2: [],
	            'case': []
	        };
	        this.names = [];
	        this.decisionDetails = [];
	        this.getTexts = function () {
	            return this.texts;
	        };
	        if ($scope.model.decision.childNodes === undefined) {
	            $scope.model = {
	                decision: $scope.model
	            };
	        }
	        $scope.$watch('model', function () {
	            ($scope.model.decision.childNodes || []).forEach(function (level1, level1Index) {
	                _this.decisionDetails[level1Index] = [];
	                _this.texts.benefits[level1Index] = [];
	                _this.texts.level2[level1Index] = [];
	                _this.texts.level1[level1Index] = DecisionTexts_1.decisionTexts.getAll(level1.decision);
	                (level1.childNodes || []).forEach(function (level2, level2Index) {
	                    _this.names[level2.lifeId] = "" + level2.displayName;
	                    _this.decisionDetails[level1Index][level2Index] = VirtualTreeService_1.virtualTreeService.buildModelFrom(level2.decisionNodes);
	                    _this.texts.benefits[level1Index][level2Index] = [];
	                    _this.texts.level2[level1Index][level2Index] = DecisionTexts_1.decisionTexts.getAll(level2.decision);
	                    (level2.benefits || []).forEach(function (level3, level3Index) {
	                        _this.texts.benefits[level1Index][level2Index][level3Index] = DecisionTexts_1.decisionTexts.getAll(level3.decision);
	                    });
	                });
	            });
	            _this.texts.case = DecisionTexts_1.decisionTexts.getAll($scope.model.decision.decision);
	        });
	    }
	    DecisionDirective.prototype.isProduct = function (node) {
	        return node.decisionNodeType === 'PRODUCT';
	    };
	    ;
	    DecisionDirective.prototype.isLife = function (node) {
	        return node.decisionNodeType === 'PRODUCT_LIFE';
	    };
	    ;
	    DecisionDirective.prototype.getCaseTopLevelNodes = function () {
	        return this.$scope.model.decision.childNodes;
	    };
	    ;
	    DecisionDirective.prototype.getCaseDecision = function () {
	        return this.$scope.model.decision.decision;
	    };
	    ;
	    DecisionDirective.prototype.getDecisionDetails = function (productIndex, level2Index) {
	        return this.decisionDetails[productIndex][level2Index];
	    };
	    ;
	    DecisionDirective.prototype.getLifeDisplayName = function (lifeId) {
	        return this.names[lifeId] || '';
	    };
	    ;
	    DecisionDirective.prototype.hasBenefitAdjustments = function (level1Index, level2Index) {
	        return DecisionTexts_1.decisionTexts.hasDataOfType(this.texts.benefits[level1Index][level2Index], 'adjustments');
	    };
	    ;
	    DecisionDirective.prototype.hasSecondLevelRequirements = function (level1Index, level2Index) {
	        return DecisionTexts_1.decisionTexts.hasDataOfType(this.texts.level2[level1Index][level2Index], 'requirements');
	    };
	    ;
	    DecisionDirective.prototype.getSecondLevelRequirements = function (level1Index, level2Index) {
	        return this.texts.level2[level1Index][level2Index].requirements;
	    };
	    ;
	    DecisionDirective.prototype.hasSecondLevelSupportingTexts = function (level1Index, level2Index) {
	        return DecisionTexts_1.decisionTexts.hasDataOfType(this.texts.level2[level1Index][level2Index], 'supportingTexts');
	    };
	    ;
	    DecisionDirective.prototype.getSecondLevelSupportingTexts = function (level1Index, level2Index) {
	        return this.texts.level2[level1Index][level2Index].supportingTexts;
	    };
	    ;
	    DecisionDirective.prototype.hasFirstLevelRequirements = function (level1Index) {
	        return DecisionTexts_1.decisionTexts.hasDataOfType(this.texts.level1[level1Index], 'requirements');
	    };
	    ;
	    DecisionDirective.prototype.hasDecisionDetails = function () {
	        return !!this.decisionDetails;
	    };
	    ;
	    DecisionDirective.prototype.getFirstLevelRequirements = function (level1Index) {
	        return this.texts.level1[level1Index].requirements;
	    };
	    ;
	    DecisionDirective.prototype.hasFirstLevelSupportingTexts = function (level1Index) {
	        return DecisionTexts_1.decisionTexts.hasDataOfType(this.texts.level1[level1Index], 'supportingTexts');
	    };
	    ;
	    DecisionDirective.prototype.getFirstLevelSupportingTexts = function (level1Index) {
	        return this.texts.level1[level1Index].supportingTexts;
	    };
	    ;
	    DecisionDirective.prototype.hasCaseSupportingTexts = function () {
	        return DecisionTexts_1.decisionTexts.hasDataOfType(this.texts.case, 'supportingTexts');
	    };
	    ;
	    DecisionDirective.prototype.getCaseSupportingTexts = function () {
	        return this.texts.case.supportingTexts;
	    };
	    ;
	    DecisionDirective.controllerName = 'decisionDirective';
	    DecisionDirective = __decorate([
	        Directive_1.Directive('decisionUi', {
	            restrict: 'AE',
	            scope: {
	                caseId: '@',
	                model: '='
	            },
	            template: __webpack_require__(13)
	        }), 
	        __metadata('design:paramtypes', [(typeof (_a = typeof IDecisionDirectiveScopeModel_1.IDecisionDirectiveScopeModel !== 'undefined' && IDecisionDirectiveScopeModel_1.IDecisionDirectiveScopeModel) === 'function' && _a) || Object])
	    ], DecisionDirective);
	    return DecisionDirective;
	    var _a;
	}(BaseDirective_1.BaseDirective));
	exports.DecisionDirective = DecisionDirective;


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var Sugar_1 = __webpack_require__(5);
	var DirectiveType_1 = __webpack_require__(6);
	function Directive(name, definition) {
	    if (definition === void 0) { definition = {}; }
	    definition = Sugar_1.sugar.defaults(definition, {
	        restrict: 'C',
	        scope: false,
	        require: []
	    });
	    if (!Sugar_1.sugar.isArray(definition.require)) {
	        definition.require = [definition.require];
	    }
	    definition.require = definition.require.map(function (requireConfig) {
	        if (Sugar_1.sugar.isString(requireConfig)) {
	            return requireConfig;
	        }
	        else if (Sugar_1.sugar.isFunction(requireConfig)) {
	            return DirectiveType_1.DirectiveType.PARENT + requireConfig.getName();
	        }
	        else {
	            return (requireConfig.optional ? '?' : '') + requireConfig.type + requireConfig.directive.getName();
	        }
	    });
	    return function (target) {
	        target.$name = name;
	        target.$definition = definition;
	        var names = Sugar_1.sugar.clone(definition.require).map(function (name) {
	            return name.replace(/[\?\^]/g, '');
	        });
	        definition.require.unshift(name);
	        definition.controller = target;
	        definition.controllerAs = target.controllerName;
	        definition.link = function ($scope, $element, $attrs, controllers) {
	            var controller = controllers.shift();
	            if (controllers.length) {
	                controller.setControllers(Sugar_1.sugar.object(names, controllers));
	            }
	        };
	    };
	}
	exports.Directive = Directive;


/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	var Sugar = (function () {
	    function Sugar() {
	    }
	    Sugar.prototype.isFunction = function (object) {
	        return _.isFunction(object);
	    };
	    Sugar.prototype.isObject = function (object) {
	        return _.isObject(object);
	    };
	    Sugar.prototype.isArray = function (object) {
	        return _.isArray(object);
	    };
	    Sugar.prototype.isString = function (object) {
	        return _.isString(object);
	    };
	    Sugar.prototype.defaults = function (object, defaults) {
	        return _.defaults(object, defaults);
	    };
	    Sugar.prototype.object = function (keys, values) {
	        return _.object(keys, values);
	    };
	    Sugar.prototype.clone = function (object) {
	        return angular.copy(object);
	    };
	    Sugar.prototype.debounce = function (func, wait, immediate) {
	        return _.debounce(func, wait, immediate);
	    };
	    Sugar.prototype.keys = function (object) {
	        return Object.keys(object);
	    };
	    Sugar.prototype.values = function (object) {
	        return _.values(object);
	    };
	    return Sugar;
	}());
	exports.Sugar = Sugar;
	exports.sugar = new Sugar();


/***/ },
/* 6 */
/***/ function(module, exports) {

	"use strict";
	(function (DirectiveType) {
	    DirectiveType[DirectiveType["SELF"] = ''] = "SELF";
	    DirectiveType[DirectiveType["SELF_OR_PARENT"] = '^'] = "SELF_OR_PARENT";
	    DirectiveType[DirectiveType["PARENT"] = '^^'] = "PARENT";
	})(exports.DirectiveType || (exports.DirectiveType = {}));
	var DirectiveType = exports.DirectiveType;


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var BaseController_1 = __webpack_require__(8);
	var BaseDirective = (function (_super) {
	    __extends(BaseDirective, _super);
	    function BaseDirective() {
	        _super.apply(this, arguments);
	        this.controllers = {};
	    }
	    BaseDirective.prototype.getController = function (directive) {
	        var instance = this.controllers[directive.getName()];
	        if (instance === undefined) {
	            throw new Error('Controller not found');
	        }
	        return instance;
	    };
	    BaseDirective.prototype.setControllers = function (controllers) {
	        this.controllers = controllers;
	    };
	    BaseDirective.getName = function () {
	        return this.$name;
	    };
	    BaseDirective.getDefinition = function () {
	        var _this = this;
	        return function () {
	            return _this.$definition;
	        };
	    };
	    return BaseDirective;
	}(BaseController_1.BaseController));
	exports.BaseDirective = BaseDirective;


/***/ },
/* 8 */
/***/ function(module, exports) {

	"use strict";
	var BaseController = (function () {
	    function BaseController($scope) {
	    }
	    BaseController.$inject = ['$scope'];
	    return BaseController;
	}());
	exports.BaseController = BaseController;
	function Inject() {
	    var args = [];
	    for (var _i = 0; _i < arguments.length; _i++) {
	        args[_i - 0] = arguments[_i];
	    }
	    return function (target, propertyKey) {
	        if (propertyKey) {
	            target = target[propertyKey];
	        }
	        target.$inject = target.$inject ? target.$inject.concat(args) : args;
	    };
	}
	exports.Inject = Inject;


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var Injector_1 = __webpack_require__(10);
	var DecisionTexts = (function () {
	    function DecisionTexts() {
	    }
	    DecisionTexts.prototype.getAll = function (decision) {
	        return {
	            adjustments: this.processAdjustments(decision),
	            requirements: this.processRequirements(decision),
	            postpone: this.processPostpone(decision),
	            supportingTexts: this.processSupportingTexts(decision)
	        };
	    };
	    DecisionTexts.prototype.hasDataOfType = function (model, type) {
	        if (angular.isObject(model) && (type in model)) {
	            return notEmpty(model[type]);
	        }
	        if (angular.isArray(model)) {
	            return model.some(function (item) {
	                return (type in item) && notEmpty(item[type]);
	            });
	        }
	        return false;
	        function notEmpty(item) {
	            return angular.isArray(item) ? item.length > 0 : !!item;
	        }
	    };
	    DecisionTexts.prototype.getAdjustments = function (decision) {
	        return this.processAdjustments(decision);
	    };
	    DecisionTexts.prototype.getRequirements = function (decision) {
	        return this.processRequirements(decision);
	    };
	    DecisionTexts.prototype.getPostpone = function (decision) {
	        return this.processPostpone(decision);
	    };
	    ;
	    DecisionTexts.prototype.getSupportingTexts = function (decision) {
	        return this.processSupportingTexts(decision);
	    };
	    ;
	    DecisionTexts.prototype.processAdjustments = function (decision) {
	        var output = [];
	        var values;
	        if (decision && angular.isArray(decision.adjustments) && (decision.adjustments.length > 0)) {
	            decision.adjustments.forEach(function (adjustment) {
	                values = _.values(_.pick(adjustment, 'periodType', 'type', 'value', 'period', 'periodUnit'));
	                output.push(_.compact(values).join(' '));
	                if (angular.isArray(adjustment.exclusions) && (adjustment.exclusions.length > 0)) {
	                    _.each(adjustment.exclusions, function (line) {
	                        output.push(line.text);
	                    });
	                }
	                if (angular.isArray(adjustment.definitions) && (adjustment.definitions.length > 0)) {
	                    _.each(adjustment.definitions, function (line) {
	                        output.push(line.text);
	                    });
	                }
	            });
	        }
	        return output;
	    };
	    DecisionTexts.prototype.processRequirements = function (decision) {
	        var output = [];
	        if (decision && angular.isArray(decision.requirements) && (decision.requirements.length > 0)) {
	            decision.requirements.forEach(function (line) {
	                output.push(line.text);
	            });
	        }
	        return output;
	    };
	    DecisionTexts.prototype.processPostpone = function (decision) {
	        var output = [];
	        if (decision && decision.postponePeriod) {
	            output.push(Injector_1.injector.get('$filter')('translate')('synopsis.decision.postpone') + ' ' + decision.postponePeriod
	                + (decision.postponePeriodUnit ? ' ' + decision.postponePeriodUnit : ''));
	        }
	        return output;
	    };
	    DecisionTexts.prototype.processSupportingTexts = function (decision) {
	        var output = [];
	        if (decision && angular.isArray(decision.supportingTexts) && (decision.supportingTexts.length > 0)) {
	            decision.supportingTexts.forEach(function (line) {
	                output.push(line.text);
	            });
	        }
	        return output;
	    };
	    return DecisionTexts;
	}());
	exports.DecisionTexts = DecisionTexts;
	exports.decisionTexts = new DecisionTexts();


/***/ },
/* 10 */
/***/ function(module, exports) {

	"use strict";
	exports.INJECTOR_NOT_DEFINED = '$injector is not defined';
	var Injector = (function () {
	    function Injector() {
	    }
	    Injector.prototype.register = function ($injector) {
	        this.$injector = $injector;
	    };
	    Injector.prototype.get = function (name) {
	        return this.getInjector().get(name);
	    };
	    Injector.prototype.invoke = function (func, context, locals) {
	        return this.getInjector().invoke(func, context, locals);
	    };
	    Injector.prototype.getInjector = function () {
	        if (this.$injector == null) {
	            throw Error(exports.INJECTOR_NOT_DEFINED);
	        }
	        return this.$injector;
	    };
	    return Injector;
	}());
	exports.Injector = Injector;
	exports.injector = new Injector();


/***/ },
/* 11 */
/***/ function(module, exports) {

	"use strict";
	var VirtualTreeService = (function () {
	    function VirtualTreeService() {
	    }
	    VirtualTreeService.prototype.fetchUIDFactory = function () {
	        var currentFree = 0;
	        return function () {
	            return '' + currentFree++;
	        };
	    };
	    VirtualTreeService.prototype.flattenTree = function (source) {
	        var fetchUID = this.fetchUIDFactory();
	        var flatList = [];
	        angular.forEach(source, function (item) {
	            item._parent = null;
	            flattenNode(item, flatList, 0);
	        });
	        return flatList;
	        function flattenNode(node, resultList, level) {
	            node._id = fetchUID();
	            node._level = level || 0;
	            resultList.push(node);
	            if (angular.isArray(node.childNodes)) {
	                angular.forEach(node.childNodes, function (item) {
	                    item._parent = node;
	                    flattenNode(item, resultList, node._level + 1);
	                });
	            }
	        }
	    };
	    VirtualTreeService.prototype.buildModelFrom = function (source) {
	        return this.flattenTree(source);
	    };
	    return VirtualTreeService;
	}());
	exports.VirtualTreeService = VirtualTreeService;
	exports.virtualTreeService = new VirtualTreeService();


/***/ },
/* 12 */
/***/ function(module, exports) {

	"use strict";


/***/ },
/* 13 */
/***/ function(module, exports) {

	module.exports = "<div class=\"decision container\" ng-if=model ng-if=decisionDirective.hasDecisionDetails()><div class=decision-section__case><h3 class=decision-section_title><div class=decision-section_title__text><span ng-bind=\"'synopsis.application'|translate\"></span></div><span class=decision-label ng-class=\"'decision-label__' + decisionDirective.getCaseDecision().type.toLowerCase()\" ng-bind=decisionDirective.getCaseDecision().label></span></h3><table class=\"table table-hover table-condensed\"><tbody><tr ng-if=hasCaseSupportingTexts()><td><span ng-bind=\"'synopsis.decision.sup.text'|translate\"></span></td><td><div><span ng-repeat-start=\"line in decisionDirective.getCaseSupportingTexts() track by $index\" ng-bind=line></span><br ng-repeat-end></div></td></tr></tbody></table></div><div ng-repeat=\"firstLevelNode in decisionDirective.getCaseTopLevelNodes()\" ng-init=\"firstLevelNodeIndex = $index;\" class=decision-section__first-level ng-class=\"{\n        'decision-section__product': decisionDirective.isProduct(firstLevelNode),\n        'decision-section__life': decisionDirective.isLife(firstLevelNode)\n    }\"><h3 class=decision-section_title><div class=decision-section_title__text ng-if=decisionDirective.isProduct(firstLevelNode)><span ng-bind=\"'synopsis.product'|translate\"></span> &ndash; \"<span ng-bind=firstLevelNode.name></span>\"</div><div class=decision-section_title__text ng-if=decisionDirective.isLife(firstLevelNode)><span ng-bind=\"'app.details.applicant'|translate\"></span> &ndash; <span ng-bind=decisionDirective.getLifeDisplayName(firstLevelNode.lifeId)></span></div><span class=decision-label ng-class=\"'decision-label__' + firstLevelNode.decision.type.toLowerCase()\" ng-bind=firstLevelNode.decision.label></span></h3><table class=\"table table-hover table-condensed\"><tbody><tr ng-if=decisionDirective.hasFirstLevelSupportingTexts(firstLevelNodeIndex) class=first-level__supporting-texts ng-class=\"{\n            'life__supporting-texts': decisionDirective.isLife(firstLevelNode),\n            'product__supporting-texts': decisionDirective.isProduct(firstLevelNode)\n        }\"><td><span ng-bind=\"'synopsis.decision.sup.text'|translate\"></span></td><td><div><span ng-repeat-start=\"line in decisionDirective.getFirstLevelSupportingTexts(firstLevelNodeIndex) track by $index\" ng-bind=line></span><br ng-repeat-end></div></td></tr><tr ng-if=decisionDirective.hasFirstLevelRequirements(firstLevelNodeIndex) class=first-level__requirements ng-class=\"{\n            'life__requirements': decisionDirective.isLife(firstLevelNode),\n            'product__requirements': decisionDirective.isProduct(firstLevelNode)\n        }\"><td><span ng-bind=\"'synopsis.decision.requirements'|translate\"></span></td><td><div><span ng-repeat-start=\"line in decisionDirective.getFirstLevelRequirements(firstLevelNodeIndex) track by $index\" ng-bind=line></span><br ng-repeat-end></div></td></tr></tbody></table><div ng-repeat=\"secondLevelNode in firstLevelNode.childNodes\" ng-init=\"secondLevelNodeIndex = $index; detailsVisible=false;\" class=decision-section__second-level ng-class=\"{\n            'decision-section__product': decisionDirective.isProduct(secondLevelNode),\n            'decision-section__life': decisionDirective.isLife(secondLevelNode)\n        }\"><h3 class=decision-section_title><div class=decision-section_title__text ng-if=decisionDirective.isProduct(secondLevelNode)><span ng-bind=\"'synopsis.product'|translate\"></span> &ndash; \"<span ng-bind=secondLevelNode.name></span>\"</div><div class=decision-section_title__text ng-if=decisionDirective.isLife(secondLevelNode)><span ng-bind=\"'app.details.applicant'|translate\"></span> &ndash; <span ng-bind=decisionDirective.getLifeDisplayName(secondLevelNode.lifeId)></span></div><span class=decision-label ng-class=\"'decision-label__' + secondLevelNode.decision.type.toLowerCase()\" ng-bind=secondLevelNode.decision.label></span></h3><table class=\"table table-hover table-condensed\"><tbody><tr ng-if=\"decisionDirective.hasSecondLevelSupportingTexts(firstLevelNodeIndex, secondLevelNodeIndex)\" class=second-level__supporting-texts ng-class=\"{\n                'life__supporting-texts': decisionDirective.isLife(secondLevelNode),\n                'product__supporting-texts': decisionDirective.isProduct(secondLevelNode)\n            }\"><td><span ng-bind=\"'synopsis.decision.sup.text'|translate\"></span></td><td><div><span ng-repeat-start=\"line in decisionDirective.getSecondLevelSupportingTexts(firstLevelNodeIndex, secondLevelNodeIndex) track by $index\" ng-bind=line></span><br ng-repeat-end></div></td></tr><tr ng-if=\"decisionDirective.hasSecondLevelRequirements(firstLevelNodeIndex, secondLevelNodeIndex)\" class=second-level__requirements ng-class=\"{\n                'life__requirements': decisionDirective.isLife(secondLevelNode),\n                'product__requirements': decisionDirective.isProduct(secondLevelNode)\n            }\"><td><span ng-bind=\"'synopsis.decision.requirements'|translate\"></span></td><td><div><span ng-repeat-start=\"line in decisionDirective.getSecondLevelRequirements(firstLevelNodeIndex, secondLevelNodeIndex) track by $index\" ng-bind=line></span><br ng-repeat-end></div></td></tr></tbody></table><div class=decision-section__benefits ng-init=\"firstPositionIndex = decisionDirective.isLife(firstLevelNode) ? firstLevelNodeIndex : secondLevelNodeIndex; secondPositionIndex = decisionDirective.isLife(firstLevelNode) ? secondLevelNodeIndex : firstLevelNodeIndex\" ng-attr-id=section_benefits_{{firstPositionIndex}}_{{secondLevelNodeIndex}}><h4><span class=decision-section_title ng-bind=\"'app.details.benefit'|translate\"></span></h4><div class=\"benefit-container solid\"><decision-benefits-section benefits=secondLevelNode.benefits texts=decisionDirective.getTexts() first-level-node-index=firstLevelNodeIndex second-level-node-index=secondLevelNodeIndex></decision-benefits-section><h5 class=details-toggler ng-click=\"detailsVisible=!detailsVisible\"><span class=branch-toggler ng-class=\"{'branch-toggler__off':!detailsVisible}\"></span> <span ng-bind=\"'app.details'|translate\" class=\"\"></span></h5><decision-details first-position-index=firstPositionIndex second-position-index=secondPositionIndex benefits=secondLevelNode.benefits is-visible=detailsVisible decision-details-data=\"decisionDirective.getDecisionDetails(firstLevelNodeIndex, secondLevelNodeIndex)\"></decision-details></div><div class=\"benefit-container collapsed\" ng-repeat=\"benefit in secondLevelNode.benefits\"><decision-benefits-section benefits=[benefit] texts=decisionDirective.getTexts() first-level-node-index=firstLevelNodeIndex second-level-node-index=secondLevelNodeIndex></decision-benefits-section><h5 class=details-toggler ng-click=\"detailsVisible=!detailsVisible\"><span class=branch-toggler ng-class=\"{'branch-toggler__off':!detailsVisible}\"></span> <span ng-bind=\"'app.details'|translate\" class=\"\"></span></h5><decision-details first-position-index=firstPositionIndex second-position-index=secondPositionIndex benefits=[benefit] is-visible=detailsVisible decision-details-data=\"decisionDirective.getDecisionDetails(firstLevelNodeIndex, secondLevelNodeIndex)\"></decision-details></div></div></div></div><div class=row><div class=col-md-16><hr class=thin></div></div></div>";

/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var Directive_1 = __webpack_require__(4);
	var BaseDirective_1 = __webpack_require__(7);
	var DecisionTexts_1 = __webpack_require__(9);
	var IDecisionBenefitsSectionDirectiveScopeModel_1 = __webpack_require__(15);
	var DecisionBenefitsSectionDirective = (function (_super) {
	    __extends(DecisionBenefitsSectionDirective, _super);
	    function DecisionBenefitsSectionDirective($scope) {
	        _super.call(this, $scope);
	        this.$scope = $scope;
	        $scope.hasBenefitAdjustments = this.hasBenefitAdjustments.bind(this);
	        $scope.getBenefitAdjustments = this.getBenefitAdjustments.bind(this);
	        $scope.hasBenefitSupportingTexts = this.hasBenefitSupportingTexts.bind(this);
	        $scope.getBenefitSupportingTexts = this.getBenefitSupportingTexts.bind(this);
	        $scope.hasBenefitRequirements = this.hasBenefitRequirements.bind(this);
	        $scope.getBenefitRequirements = this.getBenefitRequirements.bind(this);
	    }
	    DecisionBenefitsSectionDirective.prototype.hasBenefitAdjustments = function (level1Index, level2Index) {
	        return DecisionTexts_1.decisionTexts.hasDataOfType(this.$scope.texts.benefits[level1Index][level2Index], 'adjustments');
	    };
	    DecisionBenefitsSectionDirective.prototype.getBenefitAdjustments = function (level1Index, level2Index, benefitIndex) {
	        return this.$scope.texts.benefits[level1Index][level2Index][benefitIndex].adjustments;
	    };
	    DecisionBenefitsSectionDirective.prototype.hasBenefitSupportingTexts = function (level1Index, level2Index) {
	        return DecisionTexts_1.decisionTexts.hasDataOfType(this.$scope.texts.benefits[level1Index][level2Index], 'supportingTexts');
	    };
	    DecisionBenefitsSectionDirective.prototype.getBenefitSupportingTexts = function (level1Index, level2Index, benefitIndex) {
	        return this.$scope.texts.benefits[level1Index][level2Index][benefitIndex].supportingTexts;
	    };
	    DecisionBenefitsSectionDirective.prototype.hasBenefitRequirements = function (level1Index, level2Index) {
	        return DecisionTexts_1.decisionTexts.hasDataOfType(this.$scope.texts.benefits[level1Index][level2Index], 'requirements');
	    };
	    DecisionBenefitsSectionDirective.prototype.getBenefitRequirements = function (level1Index, level2Index, benefitIndex) {
	        return this.$scope.texts.benefits[level1Index][level2Index][benefitIndex].requirements;
	    };
	    DecisionBenefitsSectionDirective.controllerName = 'decisionBenefitsSectionDirective';
	    DecisionBenefitsSectionDirective = __decorate([
	        Directive_1.Directive('decisionBenefitsSection', {
	            restrict: 'AE',
	            scope: {
	                benefits: '=benefits',
	                texts: '=texts',
	                firstLevelNodeIndex: '=firstLevelNodeIndex',
	                secondLevelNodeIndex: '=secondLevelNodeIndex'
	            },
	            template: __webpack_require__(16)
	        }), 
	        __metadata('design:paramtypes', [(typeof (_a = typeof IDecisionBenefitsSectionDirectiveScopeModel_1.IDecisionBenefitsSectionDirectiveScopeModel !== 'undefined' && IDecisionBenefitsSectionDirectiveScopeModel_1.IDecisionBenefitsSectionDirectiveScopeModel) === 'function' && _a) || Object])
	    ], DecisionBenefitsSectionDirective);
	    return DecisionBenefitsSectionDirective;
	    var _a;
	}(BaseDirective_1.BaseDirective));
	exports.DecisionBenefitsSectionDirective = DecisionBenefitsSectionDirective;


/***/ },
/* 15 */
/***/ function(module, exports) {

	"use strict";


/***/ },
/* 16 */
/***/ function(module, exports) {

	module.exports = "<table class=\"table table-hover table-condensed decision-section_benefits-table\"><thead><tr><th>&nbsp; <span class=benefit-text- ng-bind=\"'synopsis.benefit'|translate\"></span></th><th ng-repeat=\"benefit in benefits\" ng-init=\"benefitIndex = $index\"><span ng-bind=benefit.label></span></th></tr></thead><tbody><tr class=benefit-decision><td><span class=benefit-decision_title ng-bind=\"'synopsis.insurance.product.decision'|translate\"></span></td><td ng-repeat=\"benefit in benefits\" ng-init=\"benefitIndex = $index\"><span ng-attr-id=decisionperbenefit_decision_{{firstLevelNodeIndex}}_{{secondLevelNodeIndex}}_{{benefitIndex}} ng-bind=benefit.decision.label class=decision-label ng-class=\"'decision-label__' + benefit.decision.type.toLowerCase()\"></span></td></tr><tr ng-show=\"hasBenefitAdjustments(firstLevelNodeIndex, secondLevelNodeIndex)\" class=benefit__adjustments><td><span class=benefit-adjustments_title ng-bind=\"'synopsis.decision.adjustments'|translate\"></span></td><td ng-repeat=\"benefit in benefits\" ng-init=\"benefitIndex = $index\"><span ng-attr-id=decision-per-benefit-text___adjustments__{{firstLevelNodeIndex}}_{{secondLevelNodeIndex}}_{{benefitIndex}}><span ng-repeat-start=\"line in getBenefitAdjustments(firstLevelNodeIndex, secondLevelNodeIndex, benefitIndex) track by $index\" ng-bind=line></span><br ng-repeat-end></span></td></tr><tr ng-show=\"hasBenefitSupportingTexts(firstLevelNodeIndex, secondLevelNodeIndex)\" class=benefit__supporting-texts><td><span class=benefit-supporting-texts_title ng-bind=\"'synopsis.decision.sup.text'|translate\"></span></td><td ng-repeat=\"benefit in benefits\" ng-init=\"benefitIndex = $index\"><span ng-attr-id=decision-per-benefit-text___supporting-texts__{{firstLevelNodeIndex}}_{{secondLevelNodeIndex}}_{{benefitIndex}}><span ng-repeat-start=\"line in getBenefitSupportingTexts(firstLevelNodeIndex, secondLevelNodeIndex, benefitIndex) track by $index\" ng-bind=line></span><br ng-repeat-end></span></td></tr><tr ng-show=\"hasBenefitRequirements(firstLevelNodeIndex, secondLevelNodeIndex)\" class=benefit__requirements><td><span class=benefit-requirements_title ng-bind=\"'synopsis.decision.requirements'|translate\"></span></td><td ng-repeat=\"benefit in benefits\" ng-init=\"benefitIndex = $index\"><span ng-attr-id=decision-per-benefit-text___requirements__{{firstLevelNodeIndex}}_{{secondLevelNodeIndex}}_{{benefitIndex}}><span ng-repeat-start=\"line in getBenefitRequirements(firstLevelNodeIndex, secondLevelNodeIndex, benefitIndex) track by $index\" ng-bind=line></span><br ng-repeat-end></span></td></tr></tbody></table>";

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var Directive_1 = __webpack_require__(4);
	var BaseDirective_1 = __webpack_require__(7);
	var DecisionDetailsDirective = (function (_super) {
	    __extends(DecisionDetailsDirective, _super);
	    function DecisionDetailsDirective($scope) {
	        _super.call(this, $scope);
	    }
	    DecisionDetailsDirective.controllerName = 'decisionDetailsDirective';
	    DecisionDetailsDirective = __decorate([
	        Directive_1.Directive('decisionDetails', {
	            restrict: 'E',
	            scope: {
	                benefits: '=benefits',
	                visible: '=isVisible',
	                decisionDetailsData: '=decisionDetailsData',
	                firstPositionIndex: '=firstPositionIndex',
	                secondPositionIndex: '=secondPositionIndex'
	            },
	            template: __webpack_require__(18)
	        }), 
	        __metadata('design:paramtypes', [Object])
	    ], DecisionDetailsDirective);
	    return DecisionDetailsDirective;
	}(BaseDirective_1.BaseDirective));
	exports.DecisionDetailsDirective = DecisionDetailsDirective;


/***/ },
/* 18 */
/***/ function(module, exports) {

	module.exports = "<table virtual-tree class=\"table table-hover table-condensed decision-section_nodes-table\" ng-show=visible><thead><tr><th><span class=decision-details_node_title ng-bind=\"'synopsis.risk.assessment'|translate\"></span></th><th ng-repeat=\"benefit in benefits\" ng-init=\"benefitIndex = $index\"><span ng-bind=benefit.label id=decision-per-node_benefit_title__{{firstPositionIndex}}_{{secondPositionIndex}}_{{benefitIndex}}></span></th></tr></thead><tbody><tr ng-repeat=\"ddNode in decisionDetailsData\" virtual-tree-node=ddNode><td ng-click=virtualTree.toggleBranch(ddNode._id)><div class=decision-section_details_node-name ng-attr-title={{ddNode.name}}><div virtual-tree-toggler={{ddNode._id}}></div><span ng-bind=ddNode.name class=decision-section_details_node-name_binding id=decision-per-node_node_name__{{firstPositionIndex}}_{{secondPositionIndex}}_{{benefitIndex}}></span></div></td><td ng-repeat=\"b in benefits\" ng-init=\"benefitIndex = $index; d=ddNode.decisions[benefitIndex].value;\" ng-click=virtualTree.toggleBranch(ddNode._id)><span class=\"decision-label {{'decision-label__' + d.type.toLowerCase()}}\" id=decision-per-node_decision__{{firstPositionIndex}}_{{secondPositionIndex}}_{{benefitIndex}}_{{ddNode._id}} ng-bind=d.label></span> <span id=decision-per-node_tooltip__{{firstPositionIndex}}_{{secondPositionIndex}}_{{benefitIndex}}_{{ddNode._id}} decision-details-node=d node-name={{ddNode.name}}></span></td></tr></tbody></table>";

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var Directive_1 = __webpack_require__(4);
	var BaseDirective_1 = __webpack_require__(7);
	var DecisionTexts_1 = __webpack_require__(9);
	var BaseController_1 = __webpack_require__(8);
	var IDecisionDetailsNodeDirectiveScopeModel_1 = __webpack_require__(20);
	var DecisionDetailsNodeDirective = (function (_super) {
	    __extends(DecisionDetailsNodeDirective, _super);
	    function DecisionDetailsNodeDirective($scope, $element) {
	        _super.call(this, $scope);
	        this.$scope = $scope;
	        this.$element = $element;
	        var toggleElement = $($element[0].querySelector('.decision-details_toggle'));
	        var decisionDetailsFactory = this.decisionDetailsFactory.bind(this);
	        toggleElement.popover({
	            placement: 'auto',
	            trigger: 'hover click',
	            html: true,
	            content: function () {
	                var popoverContentElement = $($element[0].querySelector('.decision-details'));
	                return popoverContentElement.html();
	            }
	        });
	        $scope.$watch('decision', function (newValue) {
	            $scope.nodeDetails = newValue ? decisionDetailsFactory(newValue) : null;
	        });
	        $scope.$on('$destroy', function () {
	            $element.popover('destroy');
	        });
	    }
	    DecisionDetailsNodeDirective.prototype.decisionDetailsFactory = function (decision) {
	        var output = {
	            decisionType: decision.type,
	            decisionLabel: decision.label,
	            info: []
	        };
	        var info = DecisionTexts_1.decisionTexts.getAll(decision);
	        if (info.adjustments.length) {
	            output.info.push({
	                title: ('synopsis.decision.adjustments'),
	                type: 'adjustments',
	                text: info.adjustments
	            });
	        }
	        if (info.supportingTexts.length) {
	            output.info.push({
	                title: ('synopsis.decision.sup.text'),
	                type: 'supporting-texts',
	                text: info.supportingTexts
	            });
	        }
	        if (info.requirements.length) {
	            output.info.push({
	                title: ('synopsis.decision.requirements'),
	                type: 'requirements',
	                text: info.requirements
	            });
	        }
	        if (info.postpone.length) {
	            output.info.push({
	                title: '',
	                type: 'postpone',
	                text: info.postpone
	            });
	        }
	        return output;
	    };
	    DecisionDetailsNodeDirective.controllerName = 'decisionDetailsNodeDirective';
	    DecisionDetailsNodeDirective = __decorate([
	        Directive_1.Directive('decisionDetailsNode', {
	            restrict: 'AE',
	            scope: {
	                nodeName: '@',
	                decision: '=decisionDetailsNode'
	            },
	            template: __webpack_require__(21)
	        }),
	        BaseController_1.Inject('$element'), 
	        __metadata('design:paramtypes', [(typeof (_a = typeof IDecisionDetailsNodeDirectiveScopeModel_1.IDecisionDetailsNodeDirectiveScopeModel !== 'undefined' && IDecisionDetailsNodeDirectiveScopeModel_1.IDecisionDetailsNodeDirectiveScopeModel) === 'function' && _a) || Object, Object])
	    ], DecisionDetailsNodeDirective);
	    return DecisionDetailsNodeDirective;
	    var _a;
	}(BaseDirective_1.BaseDirective));
	exports.DecisionDetailsNodeDirective = DecisionDetailsNodeDirective;


/***/ },
/* 20 */
/***/ function(module, exports) {

	"use strict";


/***/ },
/* 21 */
/***/ function(module, exports) {

	module.exports = "<a role=button class=decision-details_toggle ng-show=\"nodeDetails.info.length>0\" tabindex=0><span class=\"glyphicon glyphicon-exclamation-sign\"></span></a><div ng-show=false><div class=decision-details><div class=decision-details_title><span ng-bind=nodeName class=decision-label ng-class=\"'decision-label__' + nodeDetails.decisionType.toLowerCase()\"></span></div><div class=decision-details_info ng-class=\"'decision-details_info__' + info.type\" ng-repeat=\"info in nodeDetails.info\"><strong ng-bind=info.title|translate></strong><p ng-repeat=\"line in info.text track by $index\" ng-bind=line></p></div></div></div>";

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var Directive_1 = __webpack_require__(4);
	var BaseDirective_1 = __webpack_require__(7);
	var VirtualTreeDirective_1 = __webpack_require__(23);
	var BaseController_1 = __webpack_require__(8);
	var IVirtualTreeNodeDirectiveScopeModel_1 = __webpack_require__(24);
	var VirtualTreeNodeDirective = (function (_super) {
	    __extends(VirtualTreeNodeDirective, _super);
	    function VirtualTreeNodeDirective($scope, $element) {
	        _super.call(this, $scope);
	        this.$scope = $scope;
	        this.$element = $element;
	        var nodeCtrl = this;
	        var source = null;
	        var visible = true;
	        var collapsed = false;
	        nodeCtrl.getSourceId = function () {
	            return source && source._id || null;
	        };
	        nodeCtrl.getSourceParentId = function () {
	            return source && source._parent ? source._parent._id : null;
	        };
	        nodeCtrl.getVisible = function () {
	            return visible;
	        };
	        nodeCtrl.setVisible = function (newVisibleState) {
	            visible = newVisibleState;
	            if (!visible && getCollapsible()) {
	                toggleCollapsed(true);
	            }
	        };
	        nodeCtrl.getCollapsible = getCollapsible;
	        nodeCtrl.getCollapsed = function () {
	            return !getCollapsible() || collapsed;
	        };
	        nodeCtrl.setCollapsed = toggleCollapsed;
	        nodeCtrl.init = function initTreeNodeCtrl(nodeSource) {
	            source = nodeSource;
	        };
	        function getCollapsible() {
	            return source && angular.isArray(source.childNodes) && source.childNodes.length > 0;
	        }
	        function toggleCollapsed(newState) {
	            if (!source || !getCollapsible() && (newState === collapsed)) {
	                return;
	            }
	            collapsed = newState;
	            if (angular.isArray(nodeCtrl.children)) {
	                nodeCtrl.children.forEach(function (itemCtrl) {
	                    itemCtrl.setVisible(!collapsed);
	                });
	            }
	        }
	    }
	    VirtualTreeNodeDirective.prototype.setControllers = function (controllers) {
	        _super.prototype.setControllers.call(this, controllers);
	        this.start();
	    };
	    VirtualTreeNodeDirective.prototype.start = function () {
	        var nodeCtrl = this;
	        var treeCtrl = this.getController(VirtualTreeDirective_1.VirtualTreeDirective);
	        nodeCtrl.init(this.$scope.item);
	        treeCtrl.registerNode(nodeCtrl);
	        var nodeClasses = 'virtual-tree-node-level-' + this.$scope.item._level;
	        nodeClasses += ' virtual-tree-node-' + (nodeCtrl.getCollapsible() ? 'branch' : 'leaf');
	        nodeCtrl.$element.addClass(nodeClasses);
	        this.$scope.$watch(function () {
	            return nodeCtrl.getVisible();
	        }, function (newValue) {
	            nodeCtrl.$element.toggleClass('ng-hide', !newValue);
	        });
	    };
	    VirtualTreeNodeDirective.controllerName = 'virtualTreeNodeController';
	    VirtualTreeNodeDirective = __decorate([
	        Directive_1.Directive('virtualTreeNode', {
	            restrict: 'AE',
	            require: [VirtualTreeDirective_1.VirtualTreeDirective],
	            scope: {
	                item: '=virtualTreeNode'
	            }
	        }),
	        BaseController_1.Inject('$element'), 
	        __metadata('design:paramtypes', [(typeof (_a = typeof IVirtualTreeNodeDirectiveScopeModel_1.IVirtualTreeNodeDirectiveScopeModel !== 'undefined' && IVirtualTreeNodeDirectiveScopeModel_1.IVirtualTreeNodeDirectiveScopeModel) === 'function' && _a) || Object, Object])
	    ], VirtualTreeNodeDirective);
	    return VirtualTreeNodeDirective;
	    var _a;
	}(BaseDirective_1.BaseDirective));
	exports.VirtualTreeNodeDirective = VirtualTreeNodeDirective;


/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var Directive_1 = __webpack_require__(4);
	var BaseDirective_1 = __webpack_require__(7);
	var VirtualTreeDirective = (function (_super) {
	    __extends(VirtualTreeDirective, _super);
	    function VirtualTreeDirective($scope) {
	        _super.call(this, $scope);
	        this.$scope = $scope;
	        var nodeCtrls = {};
	        var treeCtrl = this;
	        treeCtrl.registerNode = function (itemCtrl) {
	            var sourceParentId = itemCtrl.getSourceParentId();
	            var hasParent;
	            if (sourceParentId) {
	                angular.forEach(nodeCtrls, function (ctrl) {
	                    if (sourceParentId === ctrl.getSourceId()) {
	                        itemCtrl.parent = ctrl;
	                        ctrl.children = ctrl.children || [];
	                        ctrl.children.push(itemCtrl);
	                        hasParent = true;
	                    }
	                });
	            }
	            else {
	                hasParent = false;
	            }
	            nodeCtrls[itemCtrl.getSourceId()] = itemCtrl;
	            if (itemCtrl.getCollapsible()) {
	                itemCtrl.setCollapsed(true);
	            }
	            itemCtrl.setVisible(!hasParent || !itemCtrl.parent.getCollapsible() ? true : !itemCtrl.parent.getCollapsed());
	        };
	        treeCtrl.isNodeVisible = function (nodeId) {
	            var nodeCtrl = nodeCtrls[nodeId] || null;
	            return nodeCtrl && nodeCtrl.getVisible();
	        };
	        treeCtrl.isNodeBranch = function (nodeId) {
	            var nodeCtrl = nodeCtrls[nodeId] || null;
	            return nodeCtrl && nodeCtrl.getCollapsible();
	        };
	        treeCtrl.isBranchCollapsed = function (nodeId) {
	            var nodeCtrl = nodeCtrls[nodeId] || null;
	            return nodeCtrl && nodeCtrl.getCollapsible() && nodeCtrl.getCollapsed();
	        };
	        treeCtrl.toggleBranch = function (nodeId) {
	            var nodeCtrl = nodeCtrls[nodeId] || null;
	            if (nodeCtrl && nodeCtrl.getCollapsible()) {
	                nodeCtrl.setCollapsed(!nodeCtrl.getCollapsed());
	            }
	        };
	    }
	    VirtualTreeDirective.controllerName = 'virtualTree';
	    VirtualTreeDirective = __decorate([
	        Directive_1.Directive('virtualTree', {
	            restrict: 'AE',
	            scope: false
	        }), 
	        __metadata('design:paramtypes', [Object])
	    ], VirtualTreeDirective);
	    return VirtualTreeDirective;
	}(BaseDirective_1.BaseDirective));
	exports.VirtualTreeDirective = VirtualTreeDirective;


/***/ },
/* 24 */
/***/ function(module, exports) {

	"use strict";


/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var Directive_1 = __webpack_require__(4);
	var BaseDirective_1 = __webpack_require__(7);
	var VirtualTreeDirective_1 = __webpack_require__(23);
	var IVirtualTreeTogglerScopeModel_1 = __webpack_require__(26);
	var VirtualTreeTogglerDirective = (function (_super) {
	    __extends(VirtualTreeTogglerDirective, _super);
	    function VirtualTreeTogglerDirective($scope) {
	        _super.call(this, $scope);
	        this.$scope = $scope;
	        this.nodeId = $scope.nodeId;
	    }
	    VirtualTreeTogglerDirective.prototype.isNodeVisible = function () {
	        return this.getController(VirtualTreeDirective_1.VirtualTreeDirective).isNodeBranch(this.nodeId);
	    };
	    VirtualTreeTogglerDirective.prototype.isBranchCollapsed = function () {
	        return this.getController(VirtualTreeDirective_1.VirtualTreeDirective).isBranchCollapsed(this.nodeId);
	    };
	    VirtualTreeTogglerDirective.controllerName = 'virtualTreeToggler';
	    VirtualTreeTogglerDirective = __decorate([
	        Directive_1.Directive('virtualTreeToggler', {
	            restrict: 'AE',
	            require: VirtualTreeDirective_1.VirtualTreeDirective,
	            template: '<span class="branch-toggler" ng-class="{\'branch-toggler__off\': virtualTreeToggler.isBranchCollapsed(nodeId)}" ng-show="virtualTreeToggler.isNodeVisible()"> </span>',
	            scope: {
	                nodeId: '@virtualTreeToggler'
	            }
	        }), 
	        __metadata('design:paramtypes', [(typeof (_a = typeof IVirtualTreeTogglerScopeModel_1.IVirtualTreeTogglerScopeModel !== 'undefined' && IVirtualTreeTogglerScopeModel_1.IVirtualTreeTogglerScopeModel) === 'function' && _a) || Object])
	    ], VirtualTreeTogglerDirective);
	    return VirtualTreeTogglerDirective;
	    var _a;
	}(BaseDirective_1.BaseDirective));
	exports.VirtualTreeTogglerDirective = VirtualTreeTogglerDirective;


/***/ },
/* 26 */
/***/ function(module, exports) {

	"use strict";


/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var ProviderInjector_1 = __webpack_require__(28);
	var Injector_1 = __webpack_require__(10);
	exports.injectorModule = angular.module('injector', [])
	    .config(['$injector', function ($injector) {
	        ProviderInjector_1.providerInjector.register($injector);
	    }])
	    .run(['$injector', function ($injector) {
	        Injector_1.injector.register($injector);
	    }])
	    .name;


/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var Injector_1 = __webpack_require__(10);
	var ProviderInjector = (function (_super) {
	    __extends(ProviderInjector, _super);
	    function ProviderInjector() {
	        _super.apply(this, arguments);
	    }
	    return ProviderInjector;
	}(Injector_1.Injector));
	exports.ProviderInjector = ProviderInjector;
	exports.providerInjector = new ProviderInjector();


/***/ },
/* 29 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */
/***/ function(module, exports) {

	module.exports = {
		"app.details": "Details",
		"app.details.applicant": "Applicant",
		"app.details.benefit": "Benefits",
		"synopsis.application": "Application",
		"synopsis.benefit": "Benefit",
		"synopsis.decision.adjustments": "Adjustments",
		"synopsis.decision.requirements": "Requirements",
		"synopsis.decision.sup.text": "Supporting text",
		"synopsis.insurance.product.decision": "Decision",
		"synopsis.product": "Product",
		"synopsis.risk.assessment": "Risk assessment"
	};

/***/ }
/******/ ]);
//# sourceMappingURL=mxg-components-ui-angular-app.js.map