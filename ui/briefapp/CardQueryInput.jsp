<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：CardQueryInput.jsp
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="CardQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CardQueryInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./CardQueryReport.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>卡单查询：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            卡类型
          </TD>
          <TD  class= input>
            <Input class= common name=CardType elementtype=nacessary MAXLENGTH="11" verify="卡类型|len=2&NUM">
          </TD>
          <TD  class= title>
            卡号 
          </TD>
          <TD  class= input>
          	<Input class=common  name=CardNo  verify="卡号|len=11&NUM">
          </TD>
          <TD  class= title>
            下发日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker"  dateFormat="short" name=HandleDate verify="下发日期|date" >
          </TD>
        </TR>
       <TR  class= common>
          <TD  class= title>
            状态
          </TD>
          <TD  class= input>
            <Input class=common name=State>
          </TD>      	
          <TD  class= title>
            投保人
          </TD>
          <TD  class= input>
            <Input class= common name=AppntNo>
          </TD>
          <TD  class= title>
            被保险人
          </TD>
          <TD  class= input>
            <Input class= common name=InsuredName>
          </TD>
        </TR>
      <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD> 
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this],[0]);" onkeyup="return showCodeListKey('AgentCode',[this],[0]);">
          </TD>        
        </TR>
       <tr>
          <TD  class= title>
          生效日期自:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=CValiDate1 >
          </TD>
          <TD  class= title>
          至:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=CValiDate2 >
          </TD>  
          <TD  class= title>
            录入人员
          </TD>
          <TD  class= input>
            <Input class= common name=Operator>
          </TD>        
		   </tr>  
      <tr>
          <TD  class= title>
          录入日期:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=MakeDate1 >
          </TD>
          <TD  class= title>
          至:
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=MakeDate2 >
          </TD>  
		   </tr>    
    </table>
          <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="打  印" class = cssButton TYPE=button onclick="submitForm();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLZCard);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLZCard" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCardGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>				
  	</Div>
  	  <div align=left>
   总件数<input type='common' style='{border: 1px #9999CC solid;height: 18px}' name="SumPiece" size='8'>

   总金额<input type='common' style='{border: 1px #9999CC solid;height: 18px}' name="SumPrem" size='8'>
    </div>
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
  </Div>
    <input type='hidden' name="SQL">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
