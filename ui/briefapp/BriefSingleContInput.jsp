<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<script>
    var prtNo = "<%=request.getParameter("prtNo")%>";
    var ContType = "<%=request.getParameter("cContType")%>";
    var polNo = "<%=request.getParameter("polNo")%>";
    var scantype = "<%=request.getParameter("scantype")%>";
    var MissionID = "<%=request.getParameter("MissionID")%>";
    var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
    var ManageCom = "<%=request.getParameter("ManageCom")%>";
    var LoadFlag = "<%=request.getParameter("LoadFlag")%>";
    var CurrentDate = "<%=PubFun.getCurrentDate()%>";
    var MissionProp5 = "<%=request.getParameter("cContType")%>";
    var AgentType = "<%=request.getParameter("AgentType")%>";
    var AgentCom = "<%=request.getParameter("AgentCom")%>";
    var agentComCode = "";
    var manageComCode = "";
    var tsql = "1 and code in (select code from ldcode where codetype=#paymodebrief#)";
    var msql = "";
    if(MissionProp5 == "2"){
        msql = "1 and code <> #02#";
    }else{
        msql = "1 and code <> # #";
        
    }
    var tVideoFlag = "<%=request.getParameter("VideoFlag")%>";
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="BriefSingleContInputInit.jsp"%>
    <SCRIPT src="BriefSingleContInput.js"></SCRIPT>
    <SCRIPT src="InitDatabaseToCont.js"></SCRIPT>
    
    <SCRIPT src="../intlapp/ContInsuredLCImpart.js"></SCRIPT>
    
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./BriefSingleContSave.jsp" method=post name=fm target="fraSubmit">
<div id="CardDiv" style="display: 'none'">
		<table id="table7">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ManageInfoDiv);">
	  			</td>
	  			<td class="titleImg">卡单信息
	  			</td>
	  		</tr>
	  </table>
	  	<table class=common>
	  		<tr class=common>
	  			<td class=title>
	  				卡单类型
	  			</td>
	  			<td class=input>
	  				<Input class= readonly name=CardType readonly >
	  			</td>
	  			<td class=title>
	  				下发日期
	  			</td>
	  			<td class=input>
	  				<Input class= readonly name=ReleaseDate readonly>
	  			</td>
	  			<TD  class= title8>
	          最终激活日期
	        </TD>
	        <TD  class= input8>
	        	<Input class=readonly name=CardValiDate readonly >
	        </TD>
	  		</tr>
	  		<tr class=common>
	  			<td class=title>
	  				险种
	  			</td>
	  			<td class=input>
	  				<Input class= readonly name=CardRiskCode readonly >
	  			</td>
	  			<td class=title>
	  				保额＼档次
	  			</td>
	  			<td class=input>
	  				<Input class=readonly name=CardAmnt readonly >
	  			</td>
	  			<TD  class= title8>
	          保费
	        </TD>
	        <TD  class= input8>
	        	<Input class=readonly name=CardPrem readonly >
	        </TD>
	  		</tr>
	  	</table>
</div>
	  <table id="table1">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ManageInfoDiv);">
	  			</td>
	  			<td class="titleImg">管理信息
	  			</td>
	  		</tr>
	  </table>
  	<div id="ManageInfoDiv1" style="display: ''">
	  	<table class=common>
	  		<tr class=common>
	  			<td class=title>
	  				印刷号
	  			</td>
	  			<td class=input>
	  				<Input class= common8 name=PrtNo elementtype=nacessary TABINDEX="-1" readonly="true" />
	  			</td>
	  			<td class=title>
	  				管理机构
	  			</td>
	  			<td class=input>
	  				<Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull"  ondblclick="return showCodeListEx('JSComcode',[this,ManageComName],[0,1],null,null,null,1,280);" onkeyup="return showCodeListKeyEx('JSComcode',[this,ManageComName],[0,1],null,null,null,1,280);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
	  			</td>
	  			<TD  class= title8>
	          销售渠道
	        </TD>
	        <TD  class= input8>
	        	<!--<Input class=codeNo name=SaleChnl verify="销售渠道|notnull"  ondblclick="return showCodeList('salechnlall',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('salechnlall',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>-->
	            <Input class=codeNo name=SaleChnl verify="销售渠道|notnull"  ondblclick="return showCodeList('unitesalechnl',[this,SaleChnlName],[0,1],null,'1',null,1);" onkeyup="return showCodeListKey('unitesalechnl',[this,SaleChnlName],[0,1],null,'1',null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>
	        </TD>
	  		</tr>
	  		<tr class=common id="DLXSInput1" style="display: none">
	  		  <TD  class= title8>
                 是否已进行销售过程全程记录
              </TD>
              <TD  class= input8 >
      		    <input NAME="XSFlag" CLASS=codeNo CodeData="0|^Y|是^N|否" ondblclick="return showCodeListEx('XSFlag',[this,XSFlagName],[0,1]);" onkeyup="return showCodeListKeyEx('XSFlag',[this,XSFlagName],[0,1]);"><input class=codename name=XSFlagName readonly=true >
              </TD>
              <TD  class= title8></TD>
              <TD  class= input8></TD>
              <TD  class= title8></TD>
              <TD  class= input8></TD>  
	  		</tr>
            <!-- tr>
                <td colspan="6"><font color="red">对于非银行渠道的交叉销售业务，销售渠道选择“中介”，交叉销售渠道选择“财代健”或者“寿代健”；对于银行渠道的交叉销售，则填写“银行代理”渠道</font></td>
            </tr>
            <tr>
                <td colspan="6"><font color="red">以下“交叉销售渠道”，“交叉销售业务类型”，“对方机构代码”，“对方业务员代码”，“对方业务员姓名”信息，仅集团交叉销售业务需要填写。</font></td>
            </tr -->
            <tr>
            <td colspan="6"><font color="black">如果是交叉销售,请选择</font><INPUT TYPE="checkbox" NAME="MixComFlag" onclick="isMixCom();"></td>
	        </tr>
	        <%@include file="../sys/MixedSalesAgent.jsp"%>
	         <!--
	        <tr class="common8" id="GrpAgentComID" style="display: none">
	            <td class="title8">交叉销售渠道</td>
	            <td class="input8">
	                <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" elementtype=nacessary/>
	            </td>
	            <td class="title8">交叉销售业务类型</td>
	            <td class="input8">
	                <input class="codeNo" name="Crs_BussType" id="Crs_BussType" verify="交叉销售业务类型|code:crs_busstype" ondblclick="return showCodeList('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_BussTypeName" readonly="readonly" elementtype=nacessary/>
	            </td>
	            <td class="title8">&nbsp;</td>
	            <td class="input8">&nbsp;</td>
	        </tr>	
			<tr class=common id="GrpAgentTitleID" style="display: 'none'">
		    		<td CLASS="title" >对方机构代码</td>
					<td CLASS="input" COLSPAN="1" >
		    	      <Input class="code" name="GrpAgentCom" elementtype=nacessary ondblclick="return showCodeList('grpagentcom',[this],null,null, '1 and #1# = #1# and comp_cod =(case #' + fm.all('Crs_SaleChnl').value + '# when #01# then #000002# when #02# then #000100# end )', '1');" onkeyup="return showCodeListKey('grpagentcom',[this],null,null, '1 and #1# = #1# and comp_cod =(case #' + fm.all('Crs_SaleChnl').value + '# when #01# then #000002# when #02# then #000100# end)', '1');" onfocus="GetGrpAgentName();" onchange="getAgentName();">
		    	    </td>
		    	    <td  class= title>对方机构名称</td>
			        <td  class= input>
			          <Input class="common" name="GrpAgentComName" elementtype=nacessary TABINDEX="-1" readonly >
			        </td>  
					<td CLASS="title">对方业务员代码</td>
		    	    <td CLASS="input" COLSPAN="1">
			      <input NAME="GrpAgentCode" CLASS="common" elementtype=nacessary>
    		      </td>
		        </tr>
		        <tr class=common id="GrpAgentTitleIDNo" style="display: 'none'">
		            <td  class="title" >对方业务员姓名</td>
			        <td  class="input" COLSPAN="1">
			            <Input  name=GrpAgentName CLASS="common" elementtype=nacessary>
			        </td>
			        <td CLASS="title">对方业务员身份证</td>
			        
			     	<td CLASS="input" COLSPAN="1">
						<input NAME="GrpAgentIDNo" CLASS="common" elementtype=nacessary>
			        </td>
			         
                     
			        <td CLASS="input" COLSPAN="1">
			        <input NAME="GrpAgentIDNo" VALUE MAXLENGTH="18" CLASS="code" elementtype=nacessary ondblclick="return queryGrpAgent();" >
    	            </td>
                    
	            </tr>
	            -->
            <!--tr class="common8">
                <td class="title8">交叉销售渠道</td>
                <td class="input8">
                    <input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl" verify="交叉销售渠道|code:crs_salechnl" ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_SaleChnlName" readonly="readonly" />
                </td>
                <td class="title8">交叉销售业务类型</td>
                <td class="input8">
                    <input class="codeNo" name="Crs_BussType" id="Crs_BussType" verify="交叉销售业务类型|code:crs_busstype" ondblclick="return showCodeList('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_BussTypeName" readonly="readonly" />
                </td>
                <td class="title8">&nbsp;</td>
                <td class="input8">&nbsp;</td>
            </tr>
	  	</table>
	    </div>
        <div id="SaleManagecom" style="display: 'none'">
	    <table class=common>
	  		<tr class=common8>
	       <TD  class= title8>
            中介公司代码
          </TD>
          <TD  class= input8>
            <Input class="code" name=AgentCom elementtype=nacessary onblur="queryAgentCom()">
          </TD>
	  		<TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD>
	  		</tr>
	   </table>
	  </div>
	  -->

	   <div id="ManageInfoDiv2" style="display: ''">
	  	<table class=common>
	  		<TR class=common>
		  			<TD  class= title8 id=zhongjiecode   style="display: ''">
	            中介公司代码
	          </TD>
	          <TD  class= input8 id=zhongjiename style="display: ''">
	            <Input class="codeNo" name=AgentCom verify="中介公司代码|code:AgentCom"
	                ondblclick="return showCodeList(agentComCode,[this,AgentComName],[0,1],null, manageComCode, 'ManageCom');"
	                onkeyup="return showCodeListKey(agentComCode,[this,AgentComName],[0,1],null, manageComCode, 'ManageCom');"><Input class="codeName" name=AgentComName readonly >
	            <input name="btnQueryBranch" class="common" type="button" value="?" onclick="queryAgentCom()" style="width:20">
	          </TD>
			    	<TD  class= title8 id =AgentCodeG style="display: 'none'">
		          业务员代码
		        </TD>
			    	<TD  class= title8 id =TXCode style="display: ''">
		          中介专员代码
		        </TD>
		        <TD  class= input8>
		      		<Input NAME=GroupAgentCode VALUE="" MAXLENGTH=0 CLASS=code8 elementtype=nacessary ondblclick="return queryAgent();"onkeyup="return queryAgent2();" verify="代理人编码|notnull">
		      		<Input NAME=AgentCode VALUE="" MAXLENGTH=0 CLASS=code8 type='hidden'>
		        </TD>
		        <TD  class= title8 id=AgentNameG style="display: 'none'">
		          业务员名称
		        </TD>
		        <TD  class= title8 id=TXName style="display: ''">
		          中介专员名称
		        </TD>
		        <TD  class= input8>
		      		<Input NAME=AgentName VALUE=""  CLASS=common >
		       </TD>
		       <TD  class= title8 id= "BlankTD1" style="display: 'none'">
		       </TD>
		       <TD  class= input8 id= "BlankTD2" style="display: 'none'">
		       </TD>
	        <!--TD  class= title8>
	          业务员组别
	        </TD>
	        <TD  class= input8-->
	          <Input class="readonly"  type=hidden readonly name=AgentGroup verify="业务员组别notnull&len<=12" >
	        <!--/TD-->


	        	      </TR>
	      <TR class=common id="AgentSaleCodeID" style="display: 'none'">
	      	<TD  class= title8 >
            代理销售业务员编码
          </TD>
          <TD  class= input8>
      			<Input NAME=AgentSaleCode VALUE="" MAXLENGTH=10 CLASS=code8 ondblclick="return queryAgentSaleCode();"onblur="return queryAgentSaleCode2();">
         </TD>
          <TD  class= title8>
            代理销售业务员姓名
          </TD>
          <TD  class= input8>
      			<Input NAME=AgentSaleName VALUE=""  readonly CLASS=common >
         </TD>         
          <TD  class= title8>
          </TD>
          <TD  class= input8>
          </TD>       
        </TR>
	      <tr class=common>
	      	<TD  class= title8 id = InputDateText>
	          投保单申请日期
	        </TD>
	        <TD  class= input8 id = InputDateClass >
	          <Input class="cooldatepicker"  name="PolApplyDate"  verify="投保单申请日期|notnull&&date" elementtype=nacessary>
	        </TD>
	        <TD  class= title8 id = ReceiveDateText>
	          收单日期
	        </TD>
	        <TD  class= input8 id = ReceiveDateClass>
	          <Input class="cooldatepicker"  name=ReceiveDate  verify="收单日期|notnull&&date" >
	        </TD>

	  			<td class=title id=Tempfeetitle style="display: ''">
	  				缴费凭证号
	  			</td>
		       <TD  class= input  id=input1 style="display: ''">
		      		<Input name=TempFeeNo VALUE=""  CLASS=common  ><font style="font-size:7pt;color:red;">银行转账先收费时录入</font>
		       </TD>

	      </tr>
	  	</table>
	  </div>
        <!--div id="GrpAgentTitleID" style="display: ''" >
            <table class=common>
                <tr class=common>
                    <td CLASS="title" >对方机构代码</td>
                    <td CLASS="input" COLSPAN="1" ><input NAME="GrpAgentCom" CLASS="common" ></td>
                    <td CLASS="title">对方业务员代码</td>
                    <td CLASS="input" COLSPAN="1"><input NAME="GrpAgentCode" CLASS="common" ></td>
                    <td class="title" >对方业务员姓名</td>
                    <td class="input" COLSPAN="1"><Input  name=GrpAgentName CLASS="common" ></td>
                </tr>
            </table>
        </div-->
      	<table class=common>
      	<tr>
            <td colspan="6"><font color="black">综合开拓标志</font><INPUT TYPE="checkbox" NAME="ExtendFlag" onclick="isAssist();"></td>
        </tr>
		<tr class=common id="ExtendID" style="display: 'none'">
    		<td CLASS="title" >协助销售渠道</td>
			<td CLASS="input" COLSPAN="1" >
			  <Input class=codeNo name="AssistSaleChnl" ondblclick="return showCodeList('assistsalechnl_b',[this,AssistSalechnlName],[0,1]);" onkeyup="return showCodeListKey('assistsalechnl_b',[this,AssistSalechnlName],[0,1]);"><input class=codename name=AssistSalechnlName readonly=true elementtype=nacessary>
    	    </td>
    	    <td  class= title>协助销售人员代码</td>
	        <td  class= input>
	          <Input class="code8" name="AssistAgentCode" elementtype=nacessary ondblclick="return queryAssistAgent();">
	        </td>  
			<td CLASS="title">协助销售人员姓名</td>
			<td CLASS="input" COLSPAN="1">
				<input NAME="AssistAgentName" CLASS="common" TABINDEX="-1" readonly>
    		</td>
    	</tr>
    	</table>
    	
        <br />        
               
        <div id="divAppntAccInfo" style="display:'none'">
            <table>
                <tr>
                    <td>
                        <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                    </td>
                    <td class="titleImg">投保人帐户信息：</td>
                </tr>
            </table>
            <table class="common">
                <tr>
                    <td class="title">缴费模式</td>
                    <td class="input">
                        <input class="codeNo" name="PayMethod" ondblclick="return showCodeList('paymethod',[this,PayMethodName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('paymethod',[this,PayMethodName],[0,1],null,null,null,1);"><input class="codename" name="PayMethodName" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                <tr>
                    <td class="title">投保人帐户编号</td>
                    <td class="input">
                        <input class="readonly" name="CustNoAppAcc" readonly="readonly" />
                    </td>
                    <td class="title">投保人帐户户名</td>
                    <td class="input">
                        <input class="readonly" name="CustNameAppAcc" readonly="readonly" />
                    </td>
                    <td class="title">投保人帐户余额</td>
                    <td class="input">
                        <input class="readonly" name="BalanceAppAcc" readonly="readonly" />
                    </td>
                </tr>
            </table>
            
            <br />
            
        </div>
        
	  <table id="table2">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,AppntInfoDiv);">
	  			</td>
	  			<td class="titleImg">投保人信息
	  			</td>
	  		</tr>
	  </table>
	  <div id="AppntInfoDiv" style="display: ''">
	  	<table class=common>
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  				投保人客户号
	  			</td>
	  			<td class=input COLSPAN="1">
	  				<Input class= common name=appnt_AppntNo  verify="投保人客户号|len<=60">
	  			</td>
	  			<td><Input type=button class= cssbutton VALUE="查询" OnClick="queryAppnt()"></td><td></td>
	  		</tr>
	  	</table>
	  	 </div>
	  	<table class = common>
	  	  <div id="AppntInfoDiv1" style="display: ''">
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  				姓名
	  			</td>
	  			<td class=input COLSPAN="1">
	  				<Input class= common name=appnt_AppntName elementtype=nacessary verify="姓名|notnull&len<=60" onblur="getaccname()">
	  			</td>
	  			<td class=title COLSPAN="1" id = appnt_EnglishNameText>
	  				拼音
	  			</td>
	  			<td class=input COLSPAN="1" id = appnt_EnglishNameClass>
	  				<Input class=common name=appnt_EnglishName  verify="投保人拼音" >
	  			</td>
	  			 <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class= codeno name=appnt_IDType value="0" verify="证件类型|code:IDType1"  ondblclick="return showCodeList('IDType1',[this,appnt_IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType1',[this,appnt_IDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=appnt_IDTypeName readonly=true elementtype=nacessary verify="被保险人证件类型|code:IDType">
          </TD>
          <TD  class= title COLSPAN="1">
            证件号码
          </TD>
          <TD  class= input COLSPAN="1">
            <Input class= common name=appnt_IDNo elementtype=nacessary  verify="证件号码|notnull&len<=30" onblur="getAppntBirthdaySexByIDNo(this.value)">
          </TD>
          <TD  class= title COLSPAN="1">
	          客户类型
	        </TD>
	        <TD  class= input COLSPAN="1">
	        		<Input class= codeno name=appnt_CountyType CodeData="0|^1|城镇^2|农村" ondblclick="return showCodeListEx('appnt_CountyType',[this,appnt_CountyName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('appnt_CountyType',[this,appnt_CountyName],[0,1],null,null,null,1);"><input class=codename name=appnt_CountyName >
	        </TD>
	  		</tr>
	  		  		<tr class=common >
  	  		 <TD  class= title COLSPAN="1">
  	          证件生效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=appnt_IDStartDate  verify="证件生效日期|date">
  	        </TD>
  	        	 <TD  class= title COLSPAN="1" >
  	          证件失效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=appnt_IDEndDate  verify="证件失效日期|date">
  	        </TD>
  	        <TD  class= title COLSPAN="1">
	          性别
	        </TD>
	        <TD  class= input COLSPAN="1">
	        		<Input class= codeno name=appnt_AppntSex   verify="投保人性别|notnull"  " ondblclick="return showCodeList('Sex',[this,appnt_SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,appnt_SexName],[0,1],null,null,null,1);"><input class=codename name=appnt_SexName >
	        </TD>
	        <TD  class= title>
              家庭收入(万元)
            </TD>
            <TD  class= input>
              <Input class= common name=FamilySalary >
            </TD>
	        </tr>
	  		<tr class=common>
	        <TD  class= title COLSPAN="1" >
	          出生日期
	        </TD>
	        <TD  class= input COLSPAN="1">
	        	<Input class= cooldatepicker name=appnt_AppntBirthday  verify="出生日期|notnull&len<=20" onchange="getAge();">
	        </TD>
	        <TD  class= title id = OccupationCodeText>
	          职业代码
	        </TD>
	        <TD  class= input8 id = OccupationCodeClass>
	        	<Input class= code name=appnt_OccupationCode style="width:60px" verify="职业代码"  " ondblclick="return showCodeList('OccupationCode',[this,appnt_OccupationName,appnt_OccupationType],[0,1,2],null,null,null,1,300);" onkeyup="return showCodeListKey('OccupationCode',[this,appnt_OccupationName,appnt_OccupationType],[0,1,2],null,null,null,1,300);"><input class=codename name=appnt_OccupationName readonly=true  style="width:130px">
	        </TD>
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input>
            <Input class= common name=appnt_OccupationType >
          </TD>
          <TD  class= title>
            年收入(万元)
          </TD>
          <TD  class= input>
            <Input class= common name=appnt_Salary >
          </TD>
	       </tr>
	       <tr class=common>
	      	<td class=title COLSPAN="1" id = WorkNameText>
	      		工作单位
	      	</td>
	      	<td class=input8 COLSPAN="1" id = WorkNameClass>
		      	<input class= common3 name=WorkName style="width:150px" verify="工作单位">
		      </td>
	  			<td class=title id=ZhiweiApptitle >
	  				岗位职务
	  			</td>
		       <TD  class= input  id=ZhiweiApp >
		      		<Input name=appnt_Position  class=common  >
		       </TD>
		       <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <Input class=codeno name=appnt_NativePlace  verify="投保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,appnt_NativePlaceName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('NativePlace',[this,appnt_NativePlaceName],[0,1],null,null,null,1);" ><input class=codename name=appnt_NativePlaceName readonly=true >
          
          </TD>
          <TD  class= title id="NativePlace" style="display: none">
          <div id="NativeCityTitle">国家</div>
          </TD>
          <TD  class= input id="NativeCity" style="display: none">
          <input class=codeNo name="appnt_NativeCity" verify="投保人国家|code:NativeCity" ondblclick="return showCodeList('NativeCity',[this,appnt_NativeCityName],[0,1],null,fm.appnt_NativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,appnt_NativeCityName],[0,1],null,fm.appnt_NativePlace.value,'ComCode',1);"><input class=codename name=appnt_NativeCityName readonly=true >    
          </TD>
	      </tr>
	      <tr>
	      <td class=title8 COLSPAN="1">
	      		联系地址
	      	</td>
	      	<TD  class= input colspan=9>
			<Input class="codeno" name=Province verify="投保人联系地址|notnull"  readonly=true  ondblclick="return showCodeList('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1);"><input class=codename name=appnt_PostalProvince readonly=true elementtype=nacessary>省（自治区直辖市）
			<Input class="codeno" name=City verify="投保人联系地址|notnull"  readonly=true  ondblclick="return showCodeList('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onkeyup="return showCodeListKey('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1);"><input class=codename name=appnt_PostalCity readonly=true elementtype=nacessary>市
			<Input class="codeno" name=County verify="投保人联系地址|notnull"  readonly=true  ondblclick="return showCodeList('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onkeyup="return showCodeListKey('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1);"><input class=codename name=appnt_PostalCounty readonly=true elementtype=nacessary>县（区）
			<input  class= common10 name=appnt_PostalStreet verify="投保人联系地址|notnull"> 乡镇（街道）
	    	<input  class= common10 name=appnt_PostalCommunity verify="投保人联系地址|notnull"> 村（社区）
		      	<input class= common3 name=appnt_PostalAddress type="hidden" verify="联系地址">
	    </TD>
	      	<!-- <td class=input8 COLSPAN=5>
		      	<input class= common3 name=appnt_PostalAddress type="hidden" verify="联系地址">
		      	<input class= common10 name=appnt_PostalProvince verify="投保人联系地址|notnull"> 省（自治区直辖市）
		      	<input class= common10 name=appnt_PostalCity verify="投保人联系地址|notnull"> 市
		      	<input class= common10 name=appnt_PostalCounty verify="投保人联系地址|notnull"> 县（区）
		      	<input class= common10 name=appnt_PostalStreet verify="投保人联系地址|notnull"> 乡镇（街道）
		      	<input class= common10 name=appnt_PostalCommunity verify="投保人联系地址|notnull"> 村（社区）
		      </td> -->
	      </tr>
	      <tr class=common8 >
	      	<TD  class= title8 COLSPAN="1" id=appntHomePhoneTitle style="display: 'none'">
            固定电话
          </TD>
          <TD  class= input8 COLSPAN="1" id=appntHomePhoneInput style="display: 'none'">
            <Input class= common8 name=appnt_HomePhone  type="hidden" >
            <input class= common11 name=appnt_HomeCode> - <input class= common10 name=appnt_HomeNumber>
          </TD>
	      	<TD  class= title8 COLSPAN="1" id=appntPhoneTitle style="display: 'none'">
            联系电话
          </TD>
          <TD  class= input8 COLSPAN="1" id=appntPhoneInput style="display: 'none'">
            <Input class= common8 name=appnt_Phone  verify="联系电话|len<=30">
          </TD>
          <TD  class= title8>
            移动电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=appnt_Mobile   verify="移动电话|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1" id=appntCompanyPhoneTitle style="display: 'none'">
            办公电话
          </TD>
          <TD  class= input8 COLSPAN="1" id=appntCompanyPhoneInput style="display: 'none'">
            <Input class= common8 name=appnt_CompanyPhone verify="办公电话|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1">
            电子邮箱
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=appnt_EMail verify="电子邮箱|len<=60&Email">
          </TD>
	      </tr>
	      <tr class=common >
		      <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=appnt_ZipCode    verify="邮政编码|zipcode">
          </TD>
          <TD  class= title>
            婚姻
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntMarriage"  verify="投保人婚姻状况|notnull&code:Marriage" ondblclick="return showCodeList('Marriage',[this,AppntMarriageName],[0,1]);" onkeyup="return showCodeListKey('Marriage',[this,AppntMarriageName],[0,1]);"><input class=codename name=AppntMarriageName readonly=true elementtype=nacessary>    
          </TD>
		   <TD  class= title>
            		授权使用客户信息
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntAuth" readonly=true  verify="授权使用客户信息|notnull&code:Auth"  ondblclick="return false;return showCodeList('Auth',[this,AppntAuthName],[0,1]);" onkeyup="return false ;return showCodeListKey('Auth',[this,AppntAuthName],[0,1]);"><input class=codename name=AppntAuthName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title id="appnt_PassNumer" style="display: none">
          <div id="appnt_PassNumerTitle">原通行证号码</div>
          </TD>
          <TD  class= input id="appnt_PassIDNo" style="display: none">
           <Input class= common3 name="appnt_PassIDNoName" onblur=""  verify="投保人原通行证号码|len<=20" >  
          </TD> 
	      </tr>
       </table>

	  <table id="table2">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
	  			</td>
	  			<td class="titleImg">被保人信息
	  			</td>
	  		</tr>
       </div>
	  </table>
	  <table class=common>
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  					被保险人是投保人的
	  			</td>
	  			<td class=title COLSPAN="1">
	  				<Input class= codeno name=insured_RelationToAppnt  ondblclick="return showCodeList('Relation', [this,insured_RelationToAppntName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,insured_RelationToAppntName],[0,1]);" onchange="getAge();"><input class=codename name=insured_RelationToAppntName readonly=true elementtype=nacessary >
	  			</td>
	  			<td class=title COLSPAN="1" id = insured_InsuredNoText>
	  					被保险人客户号
	  			</td>
	  			<td class=input8 COLSPAN="1" id = insured_InsuredNoClass>
	  				<Input class= common name=insured_InsuredNo >
	  			</td>
	  			<td ><Input type=button class= cssbutton  name = "esayQuery" VALUE="查询" OnClick="queryInsured()"></td><td ></td>
	  		</tr>
	  	</table>
	  <div id="InsuredInfoDiv" style="display: 'none'">
	  	<table class=common>
	  		<!--tr class=common>
	  			<td class=title COLSPAN="1">
	  					被保险人是投保人的
	  			</td>
	  			<td class=title COLSPAN="1">
	  				<Input class= codeno name=insured_RelationToAppnt  ondblclick="return showCodeList('Relation', [this,insured_RelationToAppntName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,insured_RelationToAppntName],[0,1]);"><input class=codename name=insured_RelationToAppntName readonly=true elementtype=nacessary >
	  			</td>
	  			<!--TD  class= title8>
            单位地址编码
          </TD>
          <TD  class= input8>
             <Input class="code" name="GrpAddressNo"  ondblclick="getAddressCodeData();return showCodeListEx('GetGrpAddressNo',[this],[0],'', '', '', true);" onkeyup="getAddressCodeData();return showCodeListKeyEx('GetGrpAddressNo',[this],[0],'', '', '', true);">
          </TD>
	  		</tr-->
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  				姓名
	  			</td>
	  			<td class=input COLSPAN="1">
	  				<Input class= common name=insured_Name  elementtype=nacessary verify="姓名|len<=60">
	  			</td>
	  			<td class=title COLSPAN="1" id = insured_EnglishNameText>
	  				拼音
	  			</td>
	  			<td class=input COLSPAN="1" id = insured_EnglishClass>
	  				<Input class=common name=insured_EnglishName verify="投保人拼音" >
	  			</td>

	        <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
           <Input class= codeno name=insured_IDType verify="证件类型|code:IDType1" ondblclick="return showCodeList('IDType1',[this,insured_IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType1',[this,insured_IDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=insured_IDTypeName readonly=true elementtype=nacessary >
          </TD>
          <TD  class= title8>
            证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=insured_IDNo elementtype=nacessary onblur="getInsuredBirthdaySexByIDNo(this.value);"  verify="证件号码|len<=30">
          </TD>
		    <TD  class= title>
            		授权使用客户信息
          </TD>
          <TD  class= input>
            <Input class=codeNo name="InsuredAuth" readonly=true verify="授权使用客户信息|code:Auth"  ondblclick="return false;return showCodeList('Auth',[this,InsuredAuthName],[0,1]);" onkeyup="return false;return showCodeListKey('Auth',[this,InsuredAuthName],[0,1]);"><input class=codename name=InsuredAuthName readonly=true elementtype=nacessary>    
          </TD>
	</tr>
	  		<tr class=common>
  	  		 <TD  class= title COLSPAN="1" >
  	          证件生效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=IDStartDate  verify="证件生效日期|date">
  	        </TD>
  	        	 <TD  class= title COLSPAN="1" >
  	          证件失效日期
  	        </TD> 
  	        <TD  class= input COLSPAN="1" >
  	        	<Input class= coolDatePicker name=IDEndDate  verify="证件失效日期|date">
  	        </TD>
  	       	<TD  class= title8 >
	          性别
	        </TD>
	        <TD  class= input8 >
	        <Input class= codeno name=insured_Sex  verify="投保人性别|notnull&code:Sex"  " ondblclick="return showCodeList('Sex',[this,insured_SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,insured_SexName],[0,1],null,null,null,1);"><input class=codename name=insured_SexName readonly=true elementtype=nacessary>
	        </TD>  
	       <TD  class= title id="insured_PassNumer" style="display: none">
          <div id="insured_PassNumerTitle">原通行证号码</div>
          </TD>
          <TD  class= input id="insured_PassIDNo" style="display: none">
           <Input class= common3 name="insured_PassIDNoName" onblur=""  verify="被保人原通行证号码|len<=20" >  
          </TD> 
  	        </tr>
	  		<tr class=common8>
	        <TD  class= title8 COLSPAN="1" type="hidden" >
	          出生日期
	        </TD>
	        <TD  class= input8 COLSPAN="1">
	        	<Input class= cooldatepicker name=insured_Birthday  verify="出生日期|notnull&date&len<=20" onchange="getAge();">
	        </TD>
	        <TD  class= title id = insured_OccupationCodeText>
	          职业代码
	        </TD>
	        <TD  class= input8 id = insured_OccupationCodeClass>
	        	<Input class= code name=insured_OccupationCode style="width:60px" verify="职业代码"  " ondblclick="return showCodeList('OccupationCode',[this,insured_OccupationName,insured_OccupationType],[0,1,2],null,null,null,1,300);" onkeyup="return showCodeListKey('OccupationCode',[this,insured_OccupationName,insured_OccupationType],[0,1,2],null,null,null,1,300);"><input class=codename name=insured_OccupationName readonly=true  style="width:130px">
	        </TD>
          <TD  class= title>
            职业类别
          </TD>
          <TD  class= input>
            <Input class= common name=insured_OccupationType >
          </TD>
                <TD class=title >
		            年收入（万元）
		         </TD>
		        <TD class=input >
		            <input name=insured_Salary class=common>
		        </TD>
	      </tr>
	      <tr class=common>
	      	<td class=title COLSPAN="1" id = insured_WorkNameText>
	      		工作单位
	      	</td>
	      	<td class=input8 COLSPAN="1" id = insured_WorkNameClass>
		      	<input class= common3 name=insured_WorkName style="width:150px" verify="工作单位">
		      </td>

	  			<td class=title id=ZhiweiInstitle >
	  				岗位职务
	  			</td>
		       <TD  class= input  id=ZhiweiIns >
		      		<Input name=insured_Position   class=common  >
		       </TD>
		        <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <Input class=codeno name=insured_NativePlace  verify="被保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,insured_NativePlaceName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('NativePlace',[this,insured_NativePlaceName],[0,1],null,null,null,1);" ><input class=codename name=insured_NativePlaceName readonly=true >
          
          </TD>
          <TD  class= title id="NativePlace1" style="display: none">
          <div id="NativeCityTitle1">国家</div>
          </TD>
          <TD  class= input id="NativeCity1" style="display: none">
          <input class=codeNo name="insured_NativeCity" verify="被保人国家|code:NativeCity" ondblclick="return showCodeList('NativeCity',[this,insured_NativeCityName],[0,1],null,fm.insured_NativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,insured_NativeCityName],[0,1],null,fm.insured_NativePlace.value,'ComCode',1);"><input class=codename name=insured_NativeCityName readonly=true >    
          </TD>
	      </tr> 
	      <tr>
	      <td class=title8 COLSPAN="1">
	      		联系地址
	      	</td>
	      	<TD  class= input colspan=9>
			<Input class="codeno" name=Province2 verify="省（自治区直辖市）|notnull" ondblclick="return showCodeList('Province1',[this,insured_PostalProvince],[0,1],null,'0','Code1',1);"  readonly=true  onkeyup="return showCodeListKey('Province1',[this,insured_PostalProvince],[0,1],null,'0','Code1',1);"><input class=codename name=insured_PostalProvince readonly=true elementtype=nacessary>省（自治区直辖市）
			<Input class="codeno" name=City2 verify="市|notnull" ondblclick="return showCodeList('City1',[this,insured_PostalCity],[0,1],null,fm.Province2.value,'Code1',1);"  readonly=true  onkeyup="return showCodeListKey('City1',[this,insured_PostalCity],[0,1],null,fm.Province.value,'Code1',1);"><input class=codename name=insured_PostalCity readonly=true elementtype=nacessary>市
			<Input class="codeno" name=County2 verify="县（区）|notnull" ondblclick="return showCodeList('County1',[this,insured_PostalCounty],[0,1],null,fm.City2.value,'Code1',1);"  readonly=true  onkeyup="return showCodeListKey('County1',[this,insured_PostalCounty],[0,1],null,fm.City.value,'Code1',1);"><input class=codename name=insured_PostalCounty readonly=true elementtype=nacessary>县（区）
			<input  class= common10 name=insured_PostalStreet verify="乡镇（街道）|notnull"> 乡镇（街道）
	    	<input  class= common10 name=insured_PostalCommunity verify="村（社区）|notnull"> 村（社区）
		    <input class= common3 name=insured_PostalAddress type="hidden"  verify="联系地址">
	    </TD>
	      	<!-- <td class=input8 COLSPAN=3>
		      	<input class= common3 name=insured_PostalAddress  verify="联系地址">
		      	<input class= common10 name=insured_PostalProvince type="hidden"> 
		      	<input class= common10 name=insured_PostalCity type="hidden"> 
		      	<input class= common10 name=insured_PostalCounty type="hidden"> 
		      	<input class= common10 name=insured_PostalStreet type="hidden"> 
		      	<input class= common10 name=insured_PostalCommunity type="hidden">
		      </td> -->
	      </tr>
	      <tr class=common8 >
	      	<TD  class= title8 COLSPAN="1" id=insuredHomePhoneTitle style="display: 'none'">
            固定电话
          </TD>
          <TD  class= input8 COLSPAN="1" id=insuredHomePhoneInput style="display: 'none'">
            <Input class= common8 name=insured_HomePhone  >
                <input class= common11 name=insured_HomeCode type="hidden"><input class= common10 name=insured_HomeNumber type="hidden">
          </TD>
	      	<TD  class= title8 COLSPAN="1" id=insuredPhoneTitle style="display: 'none'">
            联系电话
          </TD>
          <TD  class= input8 COLSPAN="1" id=insuredPhoneInput style="display: 'none'">
            <Input class= common8 name=insured_Phone  verify="联系电话|len<=30">
          </TD>
          <TD  class= title8>
            移动电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=insured_Mobile elementtype=nacessary  verify="移动电话|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1" id=insuredCompanyPhoneTitle style="display: 'none'">
            办公电话
          </TD>
          <TD  class= input8 COLSPAN="1" id=insuredCompanyPhoneInput style="display: 'none'">
            <Input class= common8 name=insured_CompanyPhone verify="办公电话|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1">
            电子邮箱
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=insured_EMail verify="电子邮箱|len<=60&Email">
          </TD>
	      </tr>
	      <tr class=common8 >
		      <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=insured_ZipCode  verify="邮政编码|zipcode">
          </TD>
	      </tr>
	  	</table>
	  </div>
      
      <br />
      
      <%@include file="../intlapp/ContInsuredLCImpart.jsp"%>
      
      <br />
      
	 <div id="BnfInfoDiv" style="display: 'none'">
	  <table id="table3">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
	  			</td>
	  			<td class="titleImg">受益人信息
	  			</td>
	  		</tr>
	  </table>
	  <table id="bnf_table3" class=common>
	  	 <tr class=common8 >
	     	<TD  class= title8 COLSPAN="1">
           受益人姓名
         </TD>
         <TD  class= input8 COLSPAN="1">
           <Input class= common8 name=bnf_Name   verify="受益人姓名|len<=30">
         </TD>
         <TD  class= title8>
           性别
         </TD>
         <TD  class= input8>
           <Input class= codeno name=bnf_Sex verify="投保人性别|code:Sex"  " ondblclick="return showCodeList('Sex',[this,bnf_SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,bnf_SexName],[0,1],null,null,null,1);"><input class=codename name=bnf_SexName readonly=true >
         </TD>
         <TD  class= title8 COLSPAN="1">
           证件类型
         </TD>
         <TD  class= input8 COLSPAN="1">
           <Input class= codeno name=bnf_IDType  verify="证件类型|code:IDType1" ondblclick="return showCodeList('IDType1',[this,bnf_IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType1',[this,bnf_IDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=bnf_IDTypeName readonly=true  >
         </TD>
         <TD  class= title8 COLSPAN="1">
           证件号码
         </TD>
         <TD  class= input8 COLSPAN="1">
           <Input class= common8 name=bnf_IDNo verify="证件号码|len<=60&num" onblur="getBnfBirthdaySexByIDNo(this.value);">
         </TD>
	     </tr>
	  </table>
	</div>
		  <table id="table9" style="display: 'none'">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
	  			</td>
	  			<td class="titleImg">受益人信息
	  			</td>
	  		</tr>
	  </table>
	  <table  class= common id="MulBnfGrid" style="display: 'none'">
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanBnfGrid" >
					</span>
				</td>
			</tr>
		</table>
    	<hr>
     <div id="NationGridDiv" style="display: ''">
    <table id="table7" class=common>
	  		<tr>
	  			<td COLSPAN="1" class="titleImg">
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,NationGridDiv);">
	  			抵达国家
	  			</td>
	  		</tr>
	  </table>

    	<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanNationGrid" >
						</span>
					</td>
				</tr>
			</table>
    </div>
    <!--护照号码，全年多次保障标识，保险期间相关信息-->
    <div id="RiskInfoDiv" style="display : ''">
    	<table class=common>
	  		<tr class=common>
	  			<td class=title COLSPAN="1" id = PassPort>
	  					护照号码
	  			</td>
	  			<td class=title COLSPAN="1" id = PassPortNo>
	  				<Input class= common8 name=insured_OthIDNo >
	  				<Input class= common8 name=insured_OthIDType type="hidden" value="1">
	  			</td>
	  			<td class=title COLSPAN="1" id = TimesFlag>
	  					全年多次保障标识
	  			</td>
	  			<td class=title COLSPAN="1" id = TimesFlagNo>
	  				<Input class=codeNo name=DegreeType value=0 CodeData="0|^0|单次旅行^1|多次旅行" ondblclick="return showCodeListEx('StandbyFlag1',[this,StandbyFlag1Name],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StandbyFlag1',[this,StandbyFlag1Name],[0,1],null,null,null,1);"><input class=codename name=StandbyFlag1Name readonly=true elementtype=nacessary>
	  			</td>
	  			<TD  class= title8 COLSPAN="1" id = Reach>
			      目的
			    </TD>
			    <TD  class= input8 COLSPAN="1" id = ReachType>
			      <Input class=codeNo name=Intention CodeData="0|^1|商务^2|旅行^3|其它" ondblclick="return showCodeListEx('Intention',[this,IntentionName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('Intention',[this,IntentionName],[0,1],null,null,null,1);"><input class=codename name=IntentionName readonly=true >
			    </TD>
	  			<td class=common8 COLSPAN="1">
	  			</td>
	  			<td class=common8 COLSPAN="1">
	  			</td>
	  		</tr>
	  		<tr>
	  		<font size=2 color="#ff0000">
			电话销售、意外险平台和银行保险业务的责任生效日期为签单日的次日！</font>
	  		</tr>
	  		<tr class=common >
	  			<td class=title COLSPAN="1" >
	  				责任生效日期
	  			</td>
	  			<td class=common8 COLSPAN="1" >
	  				<Input class='coolDatePicker' name=CValiDate  verify="责任生效日期&date" onfocus="showDays()" elementtype=nacessary onchange="getAge();">
	  			</td>
	  			<span id=CInValiDate_span>
		  			<td id=CInValiDate_title_td class=title COLSPAN="1">
		  				责任终止日期
		  			</td>
		  			<td id=CInValiDate_common_td  class=common8 COLSPAN="1">
		  				<Input  class='coolDatePicker' name=CInValiDate  verify="责任终止日期&date" elementtype=nacessary onfocus="showDays()">
		  			</td>
		  			<td class=common COLSPAN="1" id = Days>
		  				共<Input class=common_A readonly=true name=Days >天
		  			</td>
	  			</span>
                <td class=title COLSPAN="1" >
                    被保人年龄
                </td>
                <td class=common8 COLSPAN="1" >
                    <Input class="common" type="text" id="age" name="age" readonly = "readonly">
                </td>
	  		</tr>
	  	</table>
	  	</div>
   	 <!--险种信息-->
			<div id="RiskInfoGridDiv" style="display: ''">
		  	<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanRiskGrid" >
							</span>
						</td>
					</tr>
				</table>
		  </div>
		  <div id="RiskWrapInfoGridDiv" style="display: 'none'">
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanRiskWrapGrid" >
							</span>
						</td>
					</tr>
				</table>
			</div>
    	<hr>


	  <!--table id="table4">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ContPlanDiv);">
	  			</td>
	  			<td class="titleImg">保险计划信息
	  			</td>
	  		</tr>
	  </table>
	  <div id="ContPlanDiv" style="display: ''">
	  </div-->

    <!--为以后扩展考虑将可选责任显示在下面-->
    <!--div id="RiskDutyGridDiv" style="display: ''">
    	<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanRiskDutyGrid" >
						</span>
					</td>
				</tr>
			</table>
    </div>
    <div id="ContPlanGridDiv" style="display: ''">
    	<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanContPlanGrid" >
						</span>
					</td>
				</tr>
			</table>
    </div-->
		<div id="PayInfoTitleDiv" style="display: ''">
			<table id="table5">
		  		<tr>
		  			<td>
		  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,PayInfoDiv);">
		  			</td>
		  			<td class="titleImg">缴费信息
		  			</td>
		  		</tr>
		  </table>
		</div>
	  <div id="PayInfoDiv" style="display: ''">
		  <table class="common">
		  	<tr class=common8>
			  	<TD  class= title8 COLSPAN="1">
			      缴费频次
			    </TD>
			    <TD  class= input8 COLSPAN="1">
			      <Input class=codeNo name=PayIntv  CodeData="0|^0|趸缴^12|年缴" verify="缴费频次|notnull" ondblclick="return showCodeListEx('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);"><input class=codename name=GrpContPayIntvName elementtype=nacessary  readonly=true >
			    </TD>
			    <TD  class= title8 COLSPAN="1">
			      缴费方式
			    </TD>
			    <TD  class= input8 COLSPAN="1">
			      <Input class=codeNo name=PayMode verify="缴费方式|notnull&code:PayModeBrief" ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);" onkeyup="showCodeListKey('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);"><input class=codename name=PayModeName readonly=true >
			    </TD>
				  </tr>
	    </table>
	  </div>
	   <div id="BankCom" style="display: ''">
	   	 <table class="common">
	   	   <tr>
			    <TD  class= title8 COLSPAN="1" >
	          开户银行
	        </TD>
	        <TD  class= input8 COLSPAN="1" >
	          <Input class=codeNo  name=BankCode  verify="开户银行|code:bank&len<=24" ondblclick="return showCodeList('SimipleBankBranchCom',[this,BankCodeName],[0,1],null,fm.PayMode.value,'ComCode',1);" onkeyup="return showCodeListKey('SimipleBankBranchCom',[this,BankCodeName],[0,1],null,fm.PayMode.value,'ComCode',1);"><input class=codename name=BankCodeName >
	        </TD>
			  </tr>
			  <tr class=common8>
			  	<TD  class= title8 >
	          账&nbsp;&nbsp;&nbsp;号
	        </TD>
	        <TD  class= input8 >
	          <Input class= common8 name=BankAccNo  verify="帐号|len<=40">
	        </TD>
	        <TD  class= title8 >
	          户&nbsp;&nbsp;&nbsp;名
	        </TD>
	        <TD  class= input8 >
	          <Input class= common8 name=AccName  verify="户名|len<=40">
	        </TD>
			  </tr>
			  <tr class="common8" id="ShowMsgFlag">
				  <td class="title8">
				      是否需要续期缴费提醒
				  </td>
				  <td class="input8" colspan="3">
				      <input name="DueFeeMsgFlag" class="codeNo" ondblclick="return showCodeList('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);" verify="是否需要续期缴费提醒|notnull&code:duefeemsgflag"><input class="codename" name="DueFeeMsgFlagName" readonly=true elementtype=nacessary />
				  </td>
			  </tr>
	    </table>
	  </div>
	  <font size=2 color="#ff0000">
	  <b>新、旧缴费方式对照说明</b> 
		&nbsp;&nbsp;1现金--->1现金、3转账支票--->3支票、4银行转账--->4银行转账
	  <table id="table6">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,RemarkDiv);">
	  			</td>
	  			<td class="titleImg">备注栏
	  			</td>
	  		</tr>
	  </table>
	    <div id="RemarkDiv" style="display ''">
	  	<table class=common >
			  <TR  class= common>
		      <TD  class= title8> 特别约定 </TD>
		    </TR>
		    <TR  class= common>
		      <TD  class= title8>
		        <textarea name="Remark" cols="110" rows="3" class="common3" width=100% value="" type="_moz">
		        </textarea>
		      </TD>
		    </TR>
		  </table>
	  </div>
	   	<!--table id="table3">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
	  			</td>
	  			<td class="titleImg">被保险人信息
	  			</td>
	  		</tr>
	  </table>
	   <div id="InsuredInfoDiv" style="display: ''">
	  	<hr>
		  	<table class="common">
		  		<tr class=common8>
				  	<TD  class= title8 COLSPAN="1">
			        被保险人总人数
			      </TD>
			      <TD  class= input8 COLSPAN="1">
			        <Input class= common8 name=InsuredNum elementtype=nacessary>
			      </TD>
	  <td>
		<INPUT class=cssButton VALUE="录入被保险人"  TYPE=button onclick="intoInsured()">
		</td>
			    </tr>
	    	</table>
	  <p align=right>
	  	<INPUT name="BriefModifyDiv" style="display: 'none'" class=cssButton VALUE="修    改"  TYPE=button onclick="updateClick()">
	  	<INPUT class=cssButton VALUE="录入完毕"  TYPE=button onclick="inputConfirm()"></p>
		</td>
		</tr>
		</table-->
		<table class=common>
			<tr class=common>
				<td>
					<!--INPUT class=cssButton VALUE="录入完毕"  TYPE=button onclick="BriefInputConfirm('0000007001')"-->
					<!--INPUT class=cssButton VALUE="签  单"  TYPE=button onclick="BriefInputConfirm('0000007001')"-->
				</td>
				<td>
					<span id="operateButton" >
						<table  class=common align=center>
							<tr align=right>
								<td class=button width="10%" align=right>
									<INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=hidden onclick="return queryClick();">
								</td>
								<td class=button width="10%" align=right>
									<INPUT class=cssButton name="deleteButton" VALUE="删  除"  TYPE=hidden onclick="return deleteClick();">
								</td>
								<td class=button >
								</td>
								<td class=button width="10%" align=right>
									<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=hidden onclick="return updateClick();">
								</td>
								<td class=button width="10%" align=right>
									<INPUT class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="return submitForm();">
								</td>
							</tr>
						</table>
					</span>
				</td>
			</tr>
	  </table>
	  <!--隐藏域-->
	  <%@include file="BriefSingleContHidden.jsp"%>
	 	<input type=hidden id="MissionProp5" name="MissionProp5">
	 	<!--add by zhangxing 增加签单后调用保单打印功能-->

    <INPUT TYPE=hidden name=mContNo>
    <Input type="hidden" name="Flag" value = "1">
    <input type="hidden" name=PrtFlag>
    <input type="hidden" name=mCardFlag>
    <input type="hidden" name=ContType>
    <input type="hidden" name=RiskWrapCol>
    <input type="hidden" name=RiskWrapFlag>
    <input type="hidden" name="InputDate" />
    <input type="hidden" name="InputTime" />
    <input type="hidden" class= Common name=VideoFlag>
    <INPUT  type= "hidden" class= Common name= prtn1 value= "<%=request.getParameter("prtNo")%>"><!-- 印刷号-->
    
  </form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>