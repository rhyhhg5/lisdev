<%
//程序名称：BriefContInputInit.jsp
//程序功能：
//创建日期：2005-09-05 11:48:43
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initBox();
    initContPageGrid();
	}
  catch(ex)
  {
    alert(ex)
  }
}
function initBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("初始化控件错误！");
    alert(ex);
  }
}
function initContPageGrid()
{
  var iArray = new Array();

  iArray[0]=new Array();
  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  iArray[0][1]="30px";         			//列宽
  iArray[0][2]=10;          			//列最大值
  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[1]=new Array();
  iArray[1][0]="个人合同号";    	//列名
  iArray[1][1]="100px";            		//列宽
  iArray[1][2]=100;            			//列最大值
  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[2]=new Array();
  iArray[2][0]="印刷号";    	//列名
  iArray[2][1]="100px";            		//列宽
  iArray[2][2]=100;            			//列最大值
  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[3]=new Array();
  iArray[3][0]="投保人名称";    	//列名
  iArray[3][1]="100px";            		//列宽
  iArray[3][2]=100;            			//列最大值
  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[4]=new Array();
  iArray[4][0]="保单类型";    	//列名
  iArray[4][1]="100px";            		//列宽
  iArray[4][2]=100;            			//列最大值
  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[5]=new Array();
  iArray[5][0]="cardfalg";    	//列名
  iArray[5][1]="100px";            		//列宽
  iArray[5][2]=100;            			//列最大值
  iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[6]=new Array();
  iArray[6][0]="营销机构";      //列名
  iArray[6][1]="100px";                 //列宽
  iArray[6][2]=100;                     //列最大值
  iArray[6][3]=0;                       //是否允许输入,1表示允许，0表示不允许
  
  iArray[7]=new Array();
  iArray[7][0]="业务员代码";      //列名
  iArray[7][1]="100px";                 //列宽
  iArray[7][2]=100;                     //列最大值
  iArray[7][3]=0;                       //是否允许输入,1表示允许，0表示不允许

  ContPageGrid = new MulLineEnter( "fm" , "ContPageGrid" );
  //这些属性必须在loadMulLine前
  ContPageGrid.mulLineCount = 10;
  ContPageGrid.displayTitle = 1;
  ContPageGrid.locked = 0;
  ContPageGrid.canSel = 1;
  ContPageGrid.canChk = 0;
  ContPageGrid.hiddenPlus = 1;
  ContPageGrid.hiddenSubtraction = 1;
  ContPageGrid.loadMulLine(iArray);
}

</script>
