<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolSignSave.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";
String Priview = "PREVIEW";

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
TransferData tTransferData = new TransferData();

//接收信息
// 投保单列表
LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();

String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
String sOutXmlPath = application.getRealPath("");	//xml文件输出路径

String tGrpContNo[] = request.getParameterValues("GrpGrid1");
String tRadio[] = request.getParameterValues("InpGrpGridSel");
String tMissionID[] = request.getParameterValues("GrpGrid7");
String tSubMissionID[] = request.getParameterValues("GrpGrid8");
String workType = request.getParameter("workType");
String tempGrpContNo = "";

boolean flag = false;
int grouppolCount = tGrpContNo.length;
for (int i = 0; i < grouppolCount; i++)
{
  if( tGrpContNo[i] != null && tRadio[i].equals( "1" ))
  {
    System.out.println("GrpContNo:"+i+":"+tGrpContNo[i]);
    tempGrpContNo = tGrpContNo[i];
    tLCGrpContSchema.setGrpContNo( tGrpContNo[i] );
    if("PREVIEW".equals(workType))
    {
      tLCGrpContSchema.setAppFlag("9");
    }
    tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
    tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );
    tTransferData.setNameAndValue("MissionID",tMissionID[i] );
    tTransferData.setNameAndValue("SubMissionID",tSubMissionID[i] );
    flag = true;
    break;
  }
}

if (flag == true)
{
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add( tLCGrpContSchema );
  tVData.add( tTransferData );
  tVData.add( tG );

  // 数据传输
  //LCGrpContSignBL tLCGrpContSignBL   = new LCGrpContSignBL();

  //boolean bl = tLCGrpContSignBL.submitData( tVData, "INSERT" );
  //int n = tLCGrpContSignBL.mErrors.getErrorCount();
  GrpBriefTbWorkFlowUI tGrpBriefTbWorkFlowUI = new GrpBriefTbWorkFlowUI();
  boolean bl= tGrpBriefTbWorkFlowUI.submitData( tVData, "0000005002");
  int n = tGrpBriefTbWorkFlowUI.mErrors.getErrorCount();
  if( n == 0 )
  {
    if ( bl )
    {
      Content = " 签单成功! ";
      FlagStr = "Succ";
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单成功! ";
      }
      if(!"PREVIEW".equals(workType)){
  			String tSQL = " select prtno from lcgrpcont where proposalgrpcontno = '"+tempGrpContNo+"' "
  					+ " and substr(managecom,1,4) in ( select code from ldcode where codetype = 'qysendmessgem' ) ";
	   		String tPrtNo = new ExeSQL().getOneValue(tSQL);
	   		if(tPrtNo != null && !"".equals(tPrtNo)){
	   			VData tSMSVData = new VData();
	   			TransferData tSMSTransferData = new TransferData();
	   			tSMSTransferData.setNameAndValue("PrtNo",tPrtNo );
	   			tSMSTransferData.setNameAndValue("ContTypeFlag","2" );
	   			tSMSVData.add(tSMSTransferData);
	   			tSMSVData.add( tG );
	  				QySendMessgeMBL tQySendMessgeMBL = new QySendMessgeMBL();
	  				if(tQySendMessgeMBL.submitData(tSMSVData,"QYMSG")){
	  					System.out.println(tPrtNo+"签单后即时发送短信成功！");
	  				}else{
	  					System.out.println(tPrtNo+"签单后即时发送短信失败！");
	  				}
	   		}
  		}
    }
    else
    {
      Content = "集体投保单签单失败";
      FlagStr ="Fail" ;
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单失败! ";
      }
    }
  }
  else
  {

    String strErr = "";
    for (int i = 0; i < n; i++)
    {
      strErr += (i+1) + ": " + tGrpBriefTbWorkFlowUI.mErrors.getError(i).errorMessage + "; ";
      System.out.println(tGrpBriefTbWorkFlowUI.mErrors.getError(i).errorMessage );
    }
    if ( bl )
    {
      Content = " 部分签单成功,但是有如下信息: " +strErr;
      FlagStr = "Succ";
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单成功! ";
      }
      if(!"PREVIEW".equals(workType)){
   			String tSQL = " select prtno from lcgrpcont where proposalgrpcontno = '"+tGrpContNo[0]+"' "
   					+ " and substr(managecom,1,4) in ( select code from ldcode where codetype = 'qysendmessgem' ) ";
    		String tPrtNo = new ExeSQL().getOneValue(tSQL);
    		if(tPrtNo != null && !"".equals(tPrtNo)){
    			VData tSMSVData = new VData();
    			TransferData tSMSTransferData = new TransferData();
    			tSMSTransferData.setNameAndValue("PrtNo",tPrtNo );
    			tSMSTransferData.setNameAndValue("ContTypeFlag","2" );
    			tSMSVData.add(tSMSTransferData);
    			tSMSVData.add( tG );
   				QySendMessgeMBL tQySendMessgeMBL = new QySendMessgeMBL();
   				if(tQySendMessgeMBL.submitData(tSMSVData,"QYMSG")){
   					System.out.println(tPrtNo+"签单后即时发送短信成功！");
   				}else{
   					System.out.println(tPrtNo+"签单后即时发送短信失败！");
   				}
    		}
   		}
    }
    else
    {
      Content = "集体投保单签单失败，原因是: " + strErr;
      FlagStr = "Fail";
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单失败! ";
      }
    }
  } // end of if
} // end of if

%>
<html>
<script language="javascript">
                 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
