<%
//程序名称：BriefRiskPreviewInit.jsp
//程序功能：
//创建日期：2006-4-19 20:23
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>
<script language="JavaScript">
function initForm()
{
  try
  {
  	fm.SignCont.style.display='none';
  	fm.OverWrite.style.display='none';
  	fm.PreviewPrint.style.display='none';
  	if(ContType=="5"){
  		fm.OverWrite.style.display='';
  	}
 		if(ContType=="2"){
 			fm.OverWrite.style.display=''; 	
 		}
 		if(ContType=="6"){
 			fm.PreviewPrint.style.display=''; 	
 		}
 		
 		if(fm.SignCont.style.display=="none" && fm.OverWrite.style.display=="none" && fm.PreviewPrint.style.display=="none"){
 			fm.SignCont.style.display=''; 	
 		}
    initBox();
    initRiskDutyGrid();
    initAmntGrid();
    queryContDetail();
    ShowFiveAmnt();
  }
  catch(ex)
  {
    alert(ex.message);
  }
}
function initBox()
{
  try
  {}
  catch(ex)
  {
    alert("初始化控件错误！");
    alert(ex);
  }
}
function initRiskDutyGrid()
{
  var iArray = new Array();

  iArray[0]=new Array();
  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  iArray[0][1]="30px";         			//列宽
  iArray[0][2]=10;          			//列最大值
  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[1]=new Array();
  iArray[1][0]="险种代码";    	//列名
  iArray[1][1]="100px";            		//列宽
  iArray[1][2]=100;            			//列最大值
  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[2]=new Array();
  iArray[2][0]="险种名称";    	//列名
  iArray[2][1]="100px";            		//列宽
  iArray[2][2]=100;            			//列最大值
  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[3]=new Array();
  iArray[3][0]="生效日期";    	//列名
  iArray[3][1]="100px";            		//列宽
  iArray[3][2]=100;            			//列最大值
  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[4]=new Array();      
  iArray[4][0]="失效日期";    	//列名
  iArray[4][1]="100px";            		//列宽
  iArray[4][2]=100;            			//列最大值
  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[5]=new Array();
  iArray[5][0]="保险期间";    	//列名
  iArray[5][1]="100px";            		//列宽
  iArray[5][2]=100;            			//列最大值
  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[6]=new Array();
  iArray[6][0]="份数";    	//列名
  iArray[6][1]="100px";            		//列宽
  iArray[6][2]=100;            			//列最大值
  if(ContType!=4){
  	iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  }else{
  	iArray[6][3]=3;
  }
  
  iArray[7]=new Array();
  iArray[7][0]="档次/";    	//列名
  //if(ContType!=4){
  //	iArray[7][0]="档次";              			//是否允许输入,1表示允许，0表示不允许
  //}else{
  	iArray[7][0]="档次/保额";
  //}
  iArray[7][1]="100px";            		//列宽
  iArray[7][2]=100;            			//列最大值
 	iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[8]=new Array();
  iArray[8][0]="保费";    	//列名
  iArray[8][1]="100px";            		//列宽
  iArray[8][2]=100;            			//列最大值
  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[9]=new Array();
  iArray[9][0]="缴费期间";        //列名
  iArray[9][1]="100px";                 //列宽
  iArray[9][2]=100;                     //列最大值
  iArray[9][3]=0;                       //是否允许输入,1表示允许，0表示不允许


  RiskDutyGrid = new MulLineEnter( "fm" , "RiskDutyGrid" );
  //这些属性必须在loadMulLine前
  RiskDutyGrid.mulLineCount = 0;
  RiskDutyGrid.displayTitle = 1;
  RiskDutyGrid.locked = 0;
  RiskDutyGrid.canSel = 0;
  RiskDutyGrid.canChk = 0;
  //RiskDutyGrid.chkBoxEventFuncName = "showRiskDutyFactor";
  RiskDutyGrid.hiddenPlus = 1;
  RiskDutyGrid.hiddenSubtraction = 0;
  RiskDutyGrid.loadMulLine(iArray);
}

function initAmntGrid()
{
  var iArray = new Array();

  iArray[0]=new Array();
  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  iArray[0][1]="30px";         			//列宽
  iArray[0][2]=10;          			//列最大值
  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[1]=new Array();
  iArray[1][0]="A类保额";    	//列名
  iArray[1][1]="50px";            		//列宽
  iArray[1][2]=100;            			//列最大值
  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[2]=new Array();
  iArray[2][0]="B类保额";    	//列名
  iArray[2][1]="50px";            		//列宽
  iArray[2][2]=100;            			//列最大值
  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[3]=new Array();
  iArray[3][0]="C类保额";    	//列名
  iArray[3][1]="50px";            		//列宽
  iArray[3][2]=100;            			//列最大值
  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[4]=new Array();      
  iArray[4][0]="D类保额";    	//列名
  iArray[4][1]="50px";            		//列宽
  iArray[4][2]=100;            			//列最大值
  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
  iArray[5]=new Array();
  iArray[5][0]="E类保额";    	//列名
  iArray[5][1]="50px";            		//列宽
  iArray[5][2]=100;            			//列最大值
  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
 	
  AmntGrid = new MulLineEnter( "fm" , "AmntGrid" );
  //这些属性必须在loadMulLine前
  AmntGrid.mulLineCount = 0;
  AmntGrid.displayTitle = 1;
  AmntGrid.locked = 0;
  AmntGrid.canSel = 0;
  AmntGrid.canChk = 0;
  //RiskDutyGrid.chkBoxEventFuncName = "showRiskDutyFactor";
  AmntGrid.hiddenPlus = 1;
  AmntGrid.hiddenSubtraction = 1;
  AmntGrid.loadMulLine(iArray);
}
</script>