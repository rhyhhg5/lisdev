//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var arrDataSet;
var ImportPath;
var ImportState = "no";
window.onbeforeunload = beforeAfterInput;
window.onunload= AfterInput;
parent.fraMain.rows = "0,0,0,0,*";
var mSignCont = false;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}
//提交，保存按钮对应操作
function submitForm()
{
    // 校验1502险种附加责任。
    if(false == checkRisk1502AppendDuty())
    {
        return false;
    }
    var i = 0;
//  	if(!verifyInput2()) return false;
    if(fmSave.all('IDNo').value==""){
    	alert("证件号码不能为空！");
    	return false;
    }
    if(fmSave.all('IDType').value==""){
    	alert("证件类型不能为空！");
    	return false;
    }
    if(!chkOccupation()){
    	return false; 
    }else{
    	//如果身份证号不为空就校验
    	var idtype=fmSave.all('IDType').value;
    	
      		//杜建建   被保人身份证号校验
      		var idno = fmSave.all('IDNo').value;  
      		var sex = fmSave.all('Sex').value;
      		var birthday = fmSave.all('Birthday').value;
      		if (idno != null && idno != "" ) {
      			var strChkIdNo = checkIdNo(idtype,idno,birthday,sex);
      			if (strChkIdNo != "") {
      				alert("被保人的证件信息有误！"+ strChkIdNo);
      				return false;
      			}
      		
  		}
    }
    
    if(!verifyInput2()) return false;
    
  fmSave.fmtransact.value = "INSERT||INSURED";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmSave.action = "./BriefDiskInsuredInputSave.jsp?GrpContNo="+GrpContNo;
  fmSave.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    //执行下一步操作
    queryInsured();
    if(mSignCont){
    	top.opener.initForm();
    	parent.fraInterface.window.location = "./GroupPolPrintTest.jsp?PrtNo="+mPrtNo;
    }
  }
  initForm();
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LCInuredList.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  //  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
  fm.fmtransact.value = "INSERT||MAIN" ;
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if(fmSave.all('IDNo').value==""){
    	alert("证件号码不能为空！");
    	return false;
    }
    if(fmSave.all('IDType').value==""){
    	alert("证件类型不能为空！");
    	return false;
    }
    if(!chkOccupation()){
    	return false;
    }else{
    	//如果身份证号不为空就校验
    	var idtype=fmSave.all('IDType').value;
    	
    		//杜建建   被保人身份证号校验
    		var idno = fmSave.all('IDNo').value;  
    		var sex = fmSave.all('Sex').value;
    		var birthday = fmSave.all('Birthday').value;
    		if (idno != null && idno != "" ) {
    			var strChkIdNo = checkIdNo(idtype,idno,birthday,sex);
    			if (strChkIdNo != "") {
    				alert("被保人的证件信息有误！"+ strChkIdNo);
    				return false;
    			}
    		}
		
 }
      
  if(!verifyInput2()) return false; 
  if (confirm("您确实想修改该记录吗?"))
  {
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fmSave.fmtransact.value = "UPDATE||INSURED";
    fmSave.action = "./BriefDiskInsuredInputSave.jsp";
    fmSave.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./BriefLCInuredListQuery.jsp?GrpContNo="+GrpContNo);
}
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fmSave.fmtransact.value = "DELETE||INSURED";
    fmSave.action = "./BriefDiskInsuredInputSave.jsp?GrpContNo="+GrpContNo;
  	fmSave.submit(); //提交
    initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
  var arrResult = new Array();

  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fmSave.all('GrpContNo').value= arrResult[0][0];
    fmSave.all('InsuredID').value= arrResult[0][1];
    fmSave.all('State').value= arrResult[0][2];
    fmSave.all('ContNo').value= arrResult[0][3];
    fmSave.all('BatchNo').value= arrResult[0][4];
    fmSave.all('InusredNo').value= arrResult[0][5];
    fmSave.all('Retire').value= arrResult[0][6];
    fmSave.all('EmployeeName').value= arrResult[0][7];
    fmSave.all('InsuredName').value= arrResult[0][8];
    fmSave.all('Relation').value= arrResult[0][9];
    fmSave.all('Sex').value= arrResult[0][10];
    fmSave.all('Birthday').value= arrResult[0][11];
    fmSave.all('IDType').value= arrResult[0][12];
    fmSave.all('IDNo').value= arrResult[0][13];
    fmSave.all('ContPlanCode').value= arrResult[0][14];
    fmSave.all('OccupationType').value= arrResult[0][15];
    fmSave.all('BankCode').value= arrResult[0][16];
    fmSave.all('BankAccNo').value= arrResult[0][17];
    fmSave.all('AccName').value= arrResult[0][18];
    fmSave.all('Operator').value= arrResult[0][19];
    fmSave.all('MakeDate').value= arrResult[0][20];
    fmSave.all('MakeTime').value= arrResult[0][21];
    fmSave.all('ModifyDate').value= arrResult[0][22];
    fmSave.all('ModifyTime').value= arrResult[0][23];
    fmSave.all('OthIDType').value= arrResult[0][29];
    fmSave.all('OthIDNo').value= arrResult[0][30];
    fmSave.all('EnglishName').value= arrResult[0][31];
    fmSave.all('Phone').value= arrResult[0][32];
    fmSave.all('OccupationType1').value= arrResult[0][15];
    fmSave.all('NativePlace').value= arrResult[0][64];
    controlNativeCity("");
    setCodeName("NativePlaceName", "nativeplace", arrResult[0][64]);
    fmSave.all('Position2').value= arrResult[0][65];
    fmSave.all('OccupationCode').value= arrResult[0][66];
    fmSave.all("AppntNativeCity").value= arrResult[0][67];
    setCodeName("AppntNativeCityName", "nativecity", arrResult[0][67]);
  }
}

function InsuredListUpload()
{
  if ( ImportState =="Succ")
  {
    if ( !confirm("确定您要再次磁盘投保吗?") )
    {
      return false ;
    }
  }
  var i = 0;
  getImportPath();

  ImportFile = fm.all('FileName').value;
  var prtno = getPrtNo();
  var tprtno = ImportFile;

  if ( tprtno.indexOf("\\")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("\\")+1);
  if ( tprtno.indexOf("/")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("/")+1);
  if ( tprtno.indexOf("_")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("_"));
  if ( tprtno.indexOf(".")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("."));

  if ( prtno!=tprtno )
  {
    alert("文件名与印刷号不一致,请检查文件名!");
    return ;
  }
  else
  {

    var showStr="正在上载数据……";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "../app/DiskApplySave.jsp?ImportPath="+ImportPath+"&GrpContNo="+GrpContNo;
    fm.submit(); //提交
  }
}
function getImportPath ()
{
  // 书写SQL语句
  var strSQL = "";

  strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    alert("未找到上传路径");
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  ImportPath = turnPage.arrDataCacheSet[0][0];

}
function AfterInput()
{
  if ( ImportState=="Importing" )
  {
    return false;
  }
}
function beforeAfterInput()
{
  if ( ImportState=="Importing" )
  {
    alert("磁盘投保尚未完成，请不要离开!");
    return false;
  }
}
function getPrtNo ()
{
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select PrtNo from lcgrpcont where GrpContNo ='"+ GrpContNo +"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    alert("未查找到保单:" + GrpContNo);
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  return turnPage.arrDataCacheSet[0][0];
}

function CalInsuredPrem()
{
  //var n = LCInuredListGrid.getSelNo();
  //if(n<0)
  //{
  //	alert()
  //}
  //	var mBatchNo = "";
  //	var strSql = "select distinct BatchNo from LCInsuredList where GrpContNo='"
  //							+GrpContNo+"' and State='0'";
  //	var arr = easyExecSql(strSql);
  //	if(arr)
  //	{
  //		if(arr.length>1)
  //		{
  //			alert("此团体合同下存在多批次号，暂时不支持多批次导入！");
  //			return;
  //		}
  //		mBatchNo = arr[0][0];
  //	}
  //else
  //	{
  //		return;
  //	}
  fm.fmtransact.value="INSERT||DATABASE";
  var mBatchNo = null;
  var showStr="保费计算过程将会在后台处理,预计将会处理几分钟,此页面可关闭<br>处理进度请参看本页面导入信息";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./BriefLCInuredListSave.jsp?BatchNo="+mBatchNo+"&fmtransact="+fm.fmtransact.value+"&GrpContNo="+GrpContNo;
  fm.submit();
}

function QueryInsuredList()
{
  var strSql = "select distinct '','','','',BatchNo,'','','','','','',''"
               +",'','','','','','','',Operator from LCInsuredList where GrpContNo='"
               +GrpContNo+"' and State='0'";
  turnPage.queryModal(strSql, LCInuredListGrid);
}

function showInsuredInfo()
{
  var BatchNo = "";
  var strSql = "select distinct BatchNo from LCInsuredList where GrpContNo='"
               +GrpContNo+"' and State='0'";
  //var arr = easyExecSql(strSql);
  //if(arr)
  //{
  //	if(arr.length>1)
  //	{
  //		alert("此团体合同下存在多批次号，暂时不支持多批次导入！");
  //		return;
  //	}
  //	BatchNo = arr[0][0];
  //}
  //else
  //{
  //	return;
  //}
  var strSql_1 = "select count(1) from LCInsuredList where 1=1"
                 +" and GrpContNo='"+GrpContNo+"'";

  var strSql_2 = " select count(1) from LCInsuredList where 1=1"
                 +" and GrpContNo='"+GrpContNo+"' and State='1' ";

  var strSql_3 = " select count(1) from LCInsuredList where 1=1"
                 +" and GrpContNo='"+GrpContNo+"' and State='0' ";

  var strSql_4 = " select count(1) from LCGrpImportLog where 1=1"
                 +" and GrpContNo='"+GrpContNo+"' and ErrorState='1' ";
  var arr = easyExecSql(strSql_1);
  if(arr)
  {
    fm.SumInsured.value = arr[0][0];
  }
  var arr = easyExecSql(strSql_2);
  if(arr)
  {
    fm.SuccInsured.value = arr[0][0];
  }
  var arr = easyExecSql(strSql_3);
  if(arr)
  {
    fm.HoldInsured.value = arr[0][0];
  }
  var arr = easyExecSql(strSql_4);
  if(arr)
  {
    fm.FailInsured.value = arr[0][0];
  }
  easyQueryClick();
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function easyQueryClick(BatchNo)
{
  initDiskErrQueryGrid();
  // 书写SQL语句
  var strSql = "select GrpContNo,BatchNo,ContID,InsuredID,InsuredName,ErrorInfo from LCGrpImportLog where 1=1 "
               + "and Errorstate='1' ";
  if(GrpContNo != null&& GrpContNo !="")
  {
    strSql=strSql + "and GrpContNo='"+GrpContNo+"'" ;
  }
  if(BatchNo !=null&& BatchNo !="")
  {
    strSql=strSql+ "and BatchNo='"+BatchNo+"'";
  }
  strSql=strSql+ " Order by BatchNo,integer(InsuredID)";
  //alert(strSql);
  turnPage.queryModal(strSql, DiskErrQueryGrid);
}
function showList()
{
  var row = PersonInsuredGrid.getSelNo()-1;
  if(row>=0)
  {
    var InsuredID = PersonInsuredGrid.getRowColData(row,11);
    
    var strSql = "select * from LCInsuredList where GrpContNo='"+GrpContNo+"' and InsuredID='"+InsuredID+"'";
    if(LoadFlag=="2"){
    	strSql = "select GrpContNo,InsuredID,State,ContNo,BatchNo,InsuredNo,Retire,EmployeeName,InsuredName,Relation,Sex,Birthday,IDType,IDNo,ContPlanCode,OccupationType,BankCode,BankAccNo,AccName,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,PublicAccType,PublicAcc,EdorNo,RiskCode,CalPremType,OthIDType,OthIDNo,EnglishName,Phone,CalRule,NoNamePeoples,EdorValiDate,Prem,EdorPrem,GrpNo,GrpName,SocialCenterNo,SocialCenterName from LBInsuredList where GrpContNo='"+GrpContNo+"' and InsuredID='"+InsuredID+"'";
    }
    var arr = easyExecSql (strSql);
    afterQuery(arr);
  }
}
function queryperinsure()
{

  var strSql = "select a.InsuredNo,a.Name,a.EnglishName,a.Sex,a.Birthday,a.IDType,a.IDNo,a.OthIDNo,a.grpinsuredphone,a.ContNo,(select Case When sum(Prem) Is Null Then 0 Else sum(Prem) End  from lcpol where lcpol.grpcontno='"
               +GrpContNo+
               "' and lcpol.InsuredNo=a.InsuredNo ),ContPlanCode,Position,OccupationCode from LCInsured a where a.GrpContNo='"
               + GrpContNo +"' and a.name<>'公共账户' "
               + " order by integer(a.diskimportno)";
  turnPage1.queryModal(strSql, PersonInsuredGrid);
}

function getdetail()
{
	mSwitch = parent.VD.gVSwitch; 	
  var arrReturn = new Array();
  var tSel = PersonInsuredGrid.getSelNo();
  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择一条记录，再点击返回按钮。" );
  }
  else
  {
    try
    {
      var tRow = PersonInsuredGrid.getSelNo() - 1;
      mSwitch.deleteVar("ContNo");      //个单合同号，集体投保单号已经赋值，不再重复
      mSwitch.addVar("ContNo", "", PersonInsuredGrid.getRowColData(tRow, 10));
      mSwitch.updateVar("ContNo", "", PersonInsuredGrid.getRowColData(tRow, 10));
			mSwitch.addVar("GrpContNo", "", GrpContNo);
      var strSQL="select PolType,peoples from lccont where contno='"+PersonInsuredGrid.getRowColData(tRow, 10)+"'";
      arrResult = easyExecSql(strSQL,1,0);
      if(arrResult!=null)
      {
        mSwitch.deleteVar("PolType");
        mSwitch.addVar("PolType", "", arrResult[0][0]);
        mSwitch.updateVar("PolType", "", arrResult[0][0]);

        mSwitch.deleteVar("InsuredPeoples");
        mSwitch.addVar("InsuredPeoples", "", arrResult[0][1]);
        mSwitch.updateVar("InsuredPeoples", "", arrResult[0][1]);
      }
      else
      {
        alert("没有合同信息");
        return false;
      }

      top.fraInterface.window.location="../app/ContInsuredInput.jsp?ContNo="+PersonInsuredGrid.getRowColData(tRow,10)+"&ContType=2&LoadFlag=24";
    }
    catch(ex)
    {
      alert( "出");
    }
  }
}
//保存完成被保人清单信息后,展现到页面上
function queryInsured(){
	var strSql = "select insuredno,InsuredName,EnglishName,sex,Birthday,IDType,IDType,IDNo,Phone,contno,InsuredID,(select codename from ldcode where codetype='nativeplace' and code = LCInsuredList.nativeplace),(select codename from ldcode where codetype='nativecity' and code=LCInsuredList.nativecity),to_zero((select sum(Prem) from lcpol where contno=LCInsuredList.contno and GrpContNo=LCInsuredList.GrpContNo)),'',Position2,(select occupationname from ldoccupation where occupationcode=LCInsuredList.occupationcode) from LCInsuredList where GrpContNo='"+GrpContNo+"'";
	if(LoadFlag == "2"){
		strSql = "select insuredno,InsuredName,EnglishName,sex,Birthday,IDType,IDType,IDNo,Phone,contno,InsuredID,(select codename from ldcode where codetype='nativeplace' and code = LBInsuredList.nativeplace),(select codename from ldcode where codetype='nativecity' and code=LBInsuredList.nativecity),to_zero((select sum(Prem) from lcpol where contno=LBInsuredList.contno and GrpContNo=LBInsuredList.GrpContNo)),'',Position2,(select occupationname from ldoccupation where occupationcode=LBInsuredList.occupationcode) from LBInsuredList where GrpContNo='"+GrpContNo+"'";
	}
	turnPage.queryModal(strSql,PersonInsuredGrid);
}
//用于个单显示刷出险种
function showRiskInfo(){

	var strSql = "select a.riskcode,a.riskname,"
								+"to_zero(DECIMAL((select calfactorvalue from lccontplandutyparam where calfactor='Amnt' and grpcontno='"+GrpContNo+"' and riskcode=a.riskcode and contplancode='00'),12,2))+ "
								+"to_zero(DECIMAL((select calfactorvalue from lccontplandutyparam where calfactor='Mult' and grpcontno='"+GrpContNo+"' and riskcode=a.riskcode and contplancode='00'),12,2)), "
								+"(case when exists(select 1 from lcduty where polno in (select polno from lcpol where grpcontno='"+GrpContNo+"' and riskcode=a.riskcode)) then (to_zero(DECIMAL((select max(FloatRate) from lcduty where polno in(select polno from lcpol where grpcontno='"+GrpContNo+"' and riskcode=a.riskcode)),12,2))) else (to_zero(DECIMAL((select calfactorvalue from lccontplandutyparam where calfactor='FloatRate' and grpcontno='"+GrpContNo+"' and riskcode=a.riskcode and contplancode='00'),12,2))) end), "
								+"to_zero(DECIMAL((select max(prem) from lcpol where  grpcontno='"+GrpContNo+"' and riskcode=a.riskcode and contplancode='00'),12,2))  "
								+"from lmriskapp a where  a.RiskProp='G'  and a.RiskType8='1' order by RiskCode";		
	var arr = easyExecSql(strSql);
	if(arr){
		displayMultiline(arr,RiskGrid,turnPage);
	}
	if(RiskGrid.mulLineCount==1){
		RiskGrid.checkBoxSel(1);
	}
	var risk_info = easyExecSql("select riskcode from lcgrppol where GrpContNo='"+GrpContNo+"'");
	if(risk_info){
		for(var i=0 ; i < risk_info.length ; i++){
			for(var m=0 ; m < RiskGrid.mulLineCount ; m++){
				if(risk_info[i][0] == RiskGrid.getRowColData(m,1)){
					RiskGrid.checkBoxSel(m+1);
				}
			}
		}
	}
	for(var i=0 ; i < RiskGrid.mulLineCount ; i++){
		for(var m=0 ; m < RiskGrid.colCount ; m++){
			if( m!=0 && RiskGrid.getRowColData(i,m) == 0){
				RiskGrid.setRowColData(i,m,"");
			}
		}
	}
	showContInfo();
}
//保存保障计划
function saveRiskPlan(){

    // 校验1502险种附加责任。
    if(false == checkRisk1502AppendDuty())
    {
        return false;
    }
    // -------------------
	
	 fmSave.fmAction.value = "INSERT||MAIN" ;
	// var i = 0;
	 for(var n=0;n<RiskGrid.mulLineCount;n++)
      {
        if(RiskGrid.getRowColData(n,4)!=""&&
        	 RiskGrid.getRowColData(n,5)!="")
        {
         alert("折扣与保费不能同时录入!");
         return false;
        }
       // alert(RiskGrid.getRowColData(n,4));
       // alert(RiskGrid.getRowColData(n,5));
      }
	 var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	 fmSave.action="./BriefInsuredSave.jsp?GrpContNo="+GrpContNo;
	 fmSave.submit(); //提交
}
//显示整单信息
function showContInfo(){
	var strSql_insured = "select to_zero(count(1)) from lccont where GrpContNo='"+GrpContNo+"'";
	fmSave.SumInsured.value=easyExecSql(strSql_insured);
	var strSql_sumPrem = "select to_zero(sum(prem)) from lcpol where GrpContNo='"+GrpContNo+"'";
	fmSave.SumPrem.value = easyExecSql(strSql_sumPrem);
	var strSql_cValiDate = "select CValiDate,CInValiDate from lcgrpcont where GrpContNo='"+GrpContNo+"'";
	var arr = easyExecSql(strSql_cValiDate);
	if(arr){
		fmSave.CValiDate.value = arr[0][0];
		fmSave.CInValiDate.value = arr[0][1];
	}
    queryAppendDutyFlag();
    queryPlanCode();
	queryDays();
}
function queryDays()
{
	//首先将系统中的日期正确显示成保险公司需要的。
//	var strSql = "select CInValiDate day from LCGrpCont where GrpContNo='"+fm.GrpContNo.value+"'";
//	var arr = easyExecSql(strSql);
//	if(arr){
//		fm.CInValiDate.value = arr[0][0];
//	}
	if(!isDate(fmSave.CInValiDate.value)||!isDate(fmSave.CValiDate.value))
		return;
	var strSql = "select integer(to_date('"+fmSave.CInValiDate.value+"')-to_date('"+fmSave.CValiDate.value+"'))+1 from dual";	
	var arr=easyExecSql(strSql);
	if(arr)
	{
		fmSave.Days.value = arr[0][0];
	}
}
//上一步
function previous(){
	var arr = easyExecSql("select * from lcgrpcont where GrpContNo='"+GrpContNo+"'");
	if(!arr){
		alert("发生异常错误！");
		return;
	}
	var tPrtNo = arr[0][2];
	var tManageCom = arr[0][6];
	var tPolApplyDate = arr[0][82];
	urlStr = "./BriefContInputMain.jsp?prtNo="+tPrtNo+"&ManageCom="+tManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PolApplyDate="+tPolApplyDate+"&AgentType="+AgentType+"&LoadFlag="+LoadFlag;
	parent.fraInterface.window.location = urlStr;
}
//进行签单动作
function signCont(){

    if(!checkMarkettypeSalechnl()){
    	return false;
    }
    if(!checkRiskSalechnl()){
    	return false;
    }
    
    //校验集团交叉业务销售渠道
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    // 对5501险种折扣比的相关校验。
    if(!checkRiskFactor())
    {
        return false;
    }
    
    //校验被保人职业类别与职业代码
    if(!checkOccTypeOccCode())
    {
    	return false;
    }
    
    //校验被保人性别
    if(!CheckSex())
    {
    	return false;
    }
    
    //校验被保险人证件类型
    if(!CheckIDType())
    {
    	return false;
    }
    
    //被保险人数校验
    if(!checkPeople()){
    	return false;
    }
    // --------------------------
    
    //校验代理机构及代理机构业务员
    if(!checkAgentComAndSaleCdoe()){
    	return false;
    }
    
    //by yuchunjian  date 20180730   redmine 3884
    if(!checkFXlevel()){
    	return false;
    }  
    
    if(!checkBlackName()){
    	return false;
    }
    
	mSignCont = true;
	var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	if(AgentType == "1"){
		fmSave.action="./BriefAgentSignSave.jsp?MissionID="+MissionID+"&GrpContNo="+GrpContNo;
	}else{
		var strSql = "select * from LCGrpCont where prtNo='"+mPrtNo+"'";
		var arr = easyExecSql(strSql);
		fmSave.action="./BriefInputConfirmSave.jsp?MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&GrpContNo="+arr[0][1]+"&AgentType="+AgentType+"&PrtNo="+mPrtNo+"&ManageCom="+arr[0][4]+"&AgentCode="+arr[0][7];
	}
  fmSave.submit();
}

//测试保单打印
function testPrintCont(){
	mSignCont = true;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	afterSubmit( "Succ", "" );
}
function insuredModify(){
	var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmSave.action = "./BriefDiskInsuredInputSave.jsp?LoadFlag="+LoadFlag;
  fmSave.submit();
}

function inputConfirm()
{
    if(!checkMarkettypeSalechnl()){
    	return false;
    }
    if(!checkRiskSalechnl()){
    	return false;
    }
    
    //校验集团交叉业务销售渠道
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    // 对5501险种折扣比的相关校验。
    if(!checkRiskFactor())
    {
        return false;
    }
    
    //校验被保人职业类别与职业代码
    if(!checkOccTypeOccCode())
    {
    	return false;
    }
    
    //校验被保人性别
    if(!CheckSex())
    {
    	return false;
    }
    
    //校验被保险人证件类型
    if(!CheckIDType())
    {
    	return false;
    }
    
    //被保险人数校验
    if(!checkPeople()){
    	return false;
    }
    // ------------------------  

	mSignCont = true;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmSave.fmAction.value = "CHANGE||GROUPPOL" ;
  fmSave.action="./BriefInputChangeSave.jsp?GrpContNo="+GrpContNo;
  fmSave.submit();
}
function getin()
{
	var Sql="select count(1)+1 from LCInsuredList where GrpContNo='"+GrpContNo+"'";
	var arr=easyExecSql(Sql);
	if(arr!="1"){
	alert("已经存在被保人,如果进行磁盘投保,磁盘投保模版上的序号应从"+[arr]+"开始!否则会覆盖系统中已存在的被保人!");
	}
	if ( GrpContNo =="")
	{
		alert("请先查询保单信息");
		return ;
	}
   //alert("成功导入");	
   var strUrl = "./DiskApplyInputMain.jsp?GrpContNo="+GrpContNo; 
   showInfo=window.open(strUrl,"被保人清单导入","width=1000px, height=600px, top=0, left=0");
}

function queryAppendDutyFlag()
{
    if(GrpContNo != null 
        && GrpContNo != "")
    {
        var tStrSql = ""
            + " select CalFactorValue from LCContPlanDutyParam "
            + " where CalFactor = 'StandbyFlag2' "
            + " and RiskCode = '1502' "
            + " and grpcontno = '" + GrpContNo + "' "
            ;
        var arr = easyExecSql(tStrSql);
        if(arr)
        {
            fmSave.AppendDutyFlag.value = arr[0][0];
        }
    }
    showAllCodeName();
}

function queryPlanCode()
{
    if(GrpContNo != null 
        && GrpContNo != "")
    {
        var tStrSql = ""
            + " select CalFactorValue from LCContPlanDutyParam "
            + " where CalFactor = 'PlanCode' "
            + " and RiskCode in ('1501', '1502', '5501','551201') "
            + " and grpcontno = '" + GrpContNo + "' "
            ;
        var arr = easyExecSql(tStrSql);
        if(arr)
        {
            fmSave.PlanCode.value = arr[0][0];
            var tSQL = "select distinct planname from LDWrapPlan where plancode='"+fmSave.PlanCode.value+"' ";
            var arrPlanName = easyExecSql(tSQL);
            if(arrPlanName){
            	fmSave.PlanName.value = arrPlanName[0][0];
            }
        }
    }
    
    showAllCodeName();
}

/**
 * 校验1502险种的附加规则。
 * 当1502选择为1、2档时，不能选取附加规则。
 */
function checkRisk1502AppendDuty()
{
    var tResult = true;
    var tAppendDutyFlag = fmSave.AppendDutyFlag.value;
    //add by zjd 客户选择了1502的8-15档（即新开发的保障方案），则不能选择险种5501。
    var tflag1502=false;
    var tflag5501=false;
    var tflag1501=false;
    for(var i = 0, n = RiskGrid.mulLineCount; i < n; i++)
    {
        if(RiskGrid.getChkNo(i))
        {
            var tValues = RiskGrid.getRowData(i);
            if(!tValues)
            {
                tResult = false;
                alert("获取数据信息失败。");
                break;
            }
            var tRiskCode = tValues[0];
            if("1502" == tRiskCode)
            {
                var tMult = tValues[2];
                if(("1" == tMult
                    || "2" == tMult)
                    && "1" == tAppendDutyFlag)
                {
                    alert("1502险种，档次为1或2档时，不能选择附加责任。");
                    tResult = false;
                }
                //add by zjd(2015-03-11) 客户选择了1502的8-15档（即新开发的保障方案），则不能选择险种5501。
                if(tMult >=8 && tMult <=15){
                	tflag1502=true;
                }
            }
            //add by zjd(2015-03-11) 客户选择了1502的8-15档（即新开发的保障方案），则不能选择险种5501。
            if("5501" == tRiskCode){
            	tflag5501=true;
            }
            if("1501" == tRiskCode){
            	tflag1501=true;
            }
        }
    }
    if(tflag1502==true && (tflag5501==true || tflag1501==true)){
    	alert("1502险种，档次为8-15档时，不能选择险种5501和1501。");
        tResult = false;
    }
    return tResult;
}

/**
 * 校验险种要素。
 */
function checkRiskFactor()
{
    var t5501ChkFlag = true;
    
    for(var i = 0, n = RiskGrid.mulLineCount; i < n; i++)
    {
        if(RiskGrid.getChkNo(i))
        {
            var tValues = RiskGrid.getRowData(i);
            if(!tValues)
            {
                alert("获取数据信息失败。");
                return false;
            }
            var tRiskCode = tValues[0];
            if("1502" == tRiskCode)
            {
                var tMult = tValues[2];
                if("7" == tMult)
                {
                    t5501ChkFlag = false;
                }
            }
        }
    }
    
    for(var i = 0, n = RiskGrid.mulLineCount; i < n; i++)
    {
        if(RiskGrid.getChkNo(i))
        {
            var tValues = RiskGrid.getRowData(i);
            if(!tValues)
            {
                alert("获取数据信息失败。");
                return false;
            }
            var tRiskCode = tValues[0];
            if("5501" == tRiskCode)
            {
                var tAmnt = tValues[2];
                var tFloat = tValues[3];
                var tPrem = tValues[4];
                if((tAmnt == null || tAmnt == "")
                    || (tFloat == null || tFloat == "")
                    || (tPrem == null || tPrem == "")
                    )
                {
                    alert("险种要素信息缺失。");
                    return false;
                }
                if(t5501ChkFlag)
                {
                    if(!chkRisk5501Float(tFloat))
                    {
                        alert("5501险种折扣比过低。");
                        return false;
                    }
                }
            }
        }
    }
    
    return true;
}

/**
 * 校验5501折扣比。
 */
function chkRisk5501Float(cFloat)
{
    var tInsuDay = fmSave.Days.value;
    
    var tStandbyFlag1 = null;
    var tStrSql = ""
        + " select CalFactorValue from LCContPlanDutyParam "
        + " where CalFactor = 'StandbyFlag1' "
        + " and RiskCode = '5501' "
        + " and GrpContNo = '" + GrpContNo + "' "
        ;
    var arr = easyExecSql(tStrSql);
    if(!arr)
    {
        alert("旅行要素信息不存在。");
        return false;
    }
    tStandbyFlag1 = arr[0][0];
    
    tStrSql = ""
        + " select Prem "
        + " from discountrate610 "
        + " where 1 = 1 "
        + " and type = " + tStandbyFlag1 + " "
        + " and " + tInsuDay + " >= minday "
        + " and " + tInsuDay + " <= maxday "
        + " with ur "
        ;
    arr = easyExecSql(tStrSql);
    var tStandFloat = null;
    if(!arr)
    {
        alert("未找到5501标准折扣信息。");
        return false;
    }
    tStandFloat = arr[0][0];
    
    return !(cFloat != 0 && cFloat < tStandFloat);
}

//校验被保人职业类别与职业代码
function checkOccTypeOccCode()
{
	var strSql = "select distinct OccupationType,OccupationCode from lcinsured "
				+ " where prtno = '" + mPrtNo + "'";
	var arrResult = easyExecSql(strSql);
	var i = 0;
	
	if(arrResult != null) {
		for(i = 0; i < arrResult.length; i++)
		{
			var OccupationType = arrResult[i][0];
			var OccupationCode = arrResult[i][1];
			
			if(OccupationType != null && OccupationType != "")
			{
				if(!CheckOccupationType(OccupationType))
				{
					return false;
				}
			}
			
			if(OccupationCode != null && OccupationCode != "")
			{
				
				if(!CheckOccupationCode(OccupationType,OccupationCode))
				{
					return false;
				}
				
			}
		}
	}
	return true;
}

//校验职业代码
function CheckOccupationCode(Type,Code)
{
	var strSql = "select 1 from ldoccupation where OccupationCode = '" + Code +"' and OccupationType = '" + Type + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别和职业代码与系统描述不一致，请查看！");
		return false
	} 
	return true;
}

//校验职业类别
function CheckOccupationType(Type)
{
	var strSql = "select 1 from dual where '" + Type + "' in (select distinct occupationtype from ldoccupation)";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6");
		return false
	} 
	return true;
}

//校验被保人性别
function CheckSex()
{
	var strSql = "select distinct Sex from lcinsured where prtno = '" + mPrtNo + "'";
	var arrResult = easyExecSql(strSql);
	var i = 0;
	if(arrResult != null)
	{
		for(i = 0; i < arrResult.length; i++)
		{
			var sex = arrResult[i][0];
			if(sex != "0" && sex != "1" && sex != "2")
			{
				alert("被保人的性别填写有误，请查看。\n性别必须是半角数字的0、1、2 ，规则是0-男，1-女，2-其他");
				return false;
			}
		}
	}
	return true;
}

//校验被保险证件类型
function CheckIDType()
{
	var strSql = "select name,idtype from lcinsured where prtno = '" + mPrtNo + "' and idtype not in ('0','1','2','3','4','a','b')";
	var arrResult = easyExecSql(strSql);
	var i = 0;
	if(arrResult != null)
	{
		for(i = 0; i < arrResult.length; i++)
		{
			var Name = arrResult[i][0];
			var IDType = arrResult[i][1];
			alert("被保险人[" + Name + "]的证件类型[" + IDType + "]填写有误！不在系统规定0到4,a,b范围内，请进行修改！");
			return false;
		}
	}
	return true;
}

function checkPeople(){
	var tSqlCount="select count(1) from LCInsured where PrtNo='" + mPrtNo + "'";
	var arrCount = easyExecSql(tSqlCount);
	if(arrCount<3){
		alert("被保险人数应大于等于3！");
		return false;
	}
	return true;
}

function afterCodeSelect( cCodeName, Field )
{ 
    if(cCodeName == "wrapplancode"){
        //var planSql = "select 1 from lccont where prtno='" + mPrtNo + "'";
        //var planArr = easyExecSql(planSql)
        //if(planArr != null){
        //    fmSave.PlanCode.value = "";
        //    fmSave.PlanName.value = "";
        //    alert("该保单下存在被保人，请清空投保人后再进行保障计划录入");
        //    return false;
        //}
    
        var sql = "select riskcode,mult,amnt,rate,dutyflag,rate From LDWrapPlan where plancode='" + Field.value + "'";
        var farr = easyExecSql(sql);
        if(farr == null){
            alert("未找到相应的承保方案描述！");
            return false;
        }
        if(farr[0][4] == "1"){
            fmSave.AppendDutyFlagName.value = "选择";
        }else if (farr[0][4] == "0"){
            fmSave.AppendDutyFlagName.value = "不选择";
        } else {
            clearRiskGrid();
            alert("承保方案附加责任描述错误！");
            return false;
        }
        fmSave.AppendDutyFlag.value = farr[0][4];
        RiskGrid.checkBoxAllNot("RiskGrid");
        for(var row = 0; row < farr.length; row++){
            var riskcode = farr[row][0];
            for(var mulrow = 0; mulrow < RiskGrid.mulLineCount; mulrow++){
                if(farr[row][0] == RiskGrid.getRowColData(mulrow,1)){
                    RiskGrid.checkBoxSel(mulrow + 1);
                    if(farr[row][1] != "null" && farr[row][1] != "" && farr[row][2] != "null" && farr[row][2] != ""){
                        clearRiskGrid();
                        alert("承保方案保额/档次描述重复！");
                        return false;
                    } else if(farr[row][1] != "null" && farr[row][1] != ""){
                        RiskGrid.setRowColData(mulrow,3,farr[row][1]);
                    } else if(farr[row][2] != "null" && farr[row][2] != ""){
                        RiskGrid.setRowColData(mulrow,3,farr[row][2]);
                    } else {
                        clearRiskGrid();
                        alert("承保方案保额/档次描述错误！");
                        return false;
                    }
                    if(farr[row][3] != "null" && farr[row][3] != ""){
                        RiskGrid.setRowColData(mulrow,4,farr[row][3]);
                    } else {
                        clearRiskGrid();
                        alert("承保方案折扣比例描述错误！");
                        return false;
                    }
                    riskcode = "";
                    break;
                }
            }
            //若描述表中有多余险种
            if(riskcode != ""){
                clearRiskGrid();
                alert("承保方案描述错误！不存在" + riskcode + "险种");
                return false;
            }
        }
        //清空未勾选项的档次、折扣
        for(var mulrow = 0; mulrow < RiskGrid.mulLineCount; mulrow++){
            if(RiskGrid.getChkNo(mulrow) == false){
                RiskGrid.setRowColData(mulrow,3,"");
                RiskGrid.setRowColData(mulrow,4,"");
                
            }
        }
            
    }
    if (cCodeName == "GrpNativePlace" ) {
		if (Field.value == "OS") {
			fmSave.all("NativeCityInfo").style.display = "";
			fmSave.all("NativeCity").style.display = "";
			fmSave.all("NativeCityTitle").innerHTML = "国家";
		} else if (Field.value == "HK") {
			fmSave.all("NativeCityInfo").style.display = "";
			fmSave.all("NativeCity").style.display = "";
			fmSave.all("NativeCityTitle").innerHTML = "地区";
		} else {
			fmSave.all("NativeCityInfo").style.display = "none";
			fmSave.all("NativeCity").style.display = "none";
		}
		fmSave.AppntNativeCity.value = "";
		fmSave.AppntNativeCityName.value = "";
	}
}
function clearRiskGrid(){
    fmSave.AppendDutyFlag.value = "";
    fmSave.AppendDutyFlagName.value = "";
    RiskGrid.checkBoxAllNot("RiskGrid");
    for(var mulrow = 0; mulrow < RiskGrid.mulLineCount; mulrow++){
        RiskGrid.setRowColData(mulrow,3,"");
        RiskGrid.setRowColData(mulrow,4,"");
    }
}

function showPlanCode(){
    if(GlManageCom != null && (GlManageCom == "86" || (GlManageCom.length >3 && GlManageCom.substring(0,4) == "8611"))){
        var planSql = "select managecom from lcgrpcont where prtno='" + mPrtNo + "'";
        var planArr = easyExecSql(planSql);
        if(planArr == null){
            fmSave.PlanCode.value = "";
            fmSave.PlanName.value = "";
            showDiv(divPlanCode,"false");
            return;
        } else if(planArr[0][0] == "86110000"){
           showDiv(divPlanCode,"true");   
        } else{
            showDiv(divPlanCode,"false");   
        }
    } else {
        showDiv(divPlanCode,"false");
    }
}

//by gzh 20120522 北分中介时，中介机构及代理销售业务员必须填写
function checkAgentComAndSaleCdoe(){
	var strSql = "select AgentCom,AgentSaleCode from lcgrpcont where prtno='" + mPrtNo + "' and managecom like '8611%' and salechnl = '03'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCom = arrResult[0][0];
	    var tAgentSaleCode = arrResult[0][1];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCom == null || tAgentCom ==''){
	   		alert("中介公司代码必须录入，请核查！");
	   		return false;    	
	   	}
	   	if(tAgentSaleCode == null || tAgentSaleCode ==''){
	   		alert("代理销售业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	
	   	var tSqlCode = "select ValidStart,ValidEnd from LAQualification where agentcode = '"+tAgentSaleCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tValidStart = arrCodeResult[0][0];
	   		var tValidEnd = arrCodeResult[0][1];
	   		if(tValidStart == null || tValidStart == ''){
	   			alert("代理销售业务员资格证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tValidEnd == null || tValidEnd == ''){
	   			alert("代理销售业务员资格证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
	   			alert("代理销售业务员资格证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("代理销售业务员资格证信息不完整，请核查！");
	   		return false;
	   	}
	   	
	   	var tSqlCom = "select LicenseStartDate,LicenseEndDate,EndFlag from lacom where agentcom = '"+tAgentCom+"'";
	   	var arrComResult = easyExecSql(tSqlCom);
	   	if(arrComResult){
	   		var tLicenseStartDate = arrComResult[0][0];
	   		var tLicenseEndDate = arrComResult[0][1];
	   		var tEndFlag = arrComResult[0][2];
	   		if(tLicenseStartDate == null || tLicenseStartDate == ''){
	   			alert("中介公司许可证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tLicenseEndDate == null || tLicenseEndDate == ''){
	   			alert("中介公司许可证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == null || tEndFlag == ''){
	   			alert("中介机构合作终止状态为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == 'Y'){
	   			alert("中介机构合作终止状态为失效，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tLicenseEndDate)=='1'){
	   			alert("中介公司许可证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("中介公司许可证信息不完整，请核查！");
	   		return false;
	   	}
    }
    return true;
}
function checkMarkettypeSalechnl(){
	var tSQL = "select salechnl,markettype from lcgrpcont where prtno = '"+mPrtNo+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单数据失败！");
		return false;
	}
	var tSaleChnl = arr[0][0];
	var tMarketType = arr[0][1];
	var tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '"+tMarketType+"' and code1 = '"+tSaleChnl+"' ";
	var arrCheck = easyExecSql(tcheckSQL);
	if(!arrCheck){
		alert("市场类型和销售渠道不匹配，请核查！");
		return false;
	}
	return true;
}
function checkRiskSalechnl(){
	var tSQL = "select salechnl,riskcode from lcgrppol where prtno = '"+mPrtNo+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	for(var i=0;i<arr.length;i++){
		var tSaleChnl = arr[i][0];
		var tRiskCode = arr[i][1];
		var tcheckSQL = "select codename from ldcode where codetype = 'unitesalechnlrisk' and code = '"+tRiskCode+"' ";
		var arrCheck = easyExecSql(tcheckSQL);
		if(!arrCheck){
			if(tSaleChnl == "16"){
				alert("险种"+tRiskCode+"为非社保险种，与销售渠道"+tSaleChnl+"不匹配！");
				return false;
			}
		}else{
			if(arrCheck[0][0] != "all" && arrCheck[0][0] != tSaleChnl){
				alert("险种"+tRiskCode+"与销售渠道"+tSaleChnl+"不匹配！");
				return false;
			}
		}
	}
	return true;
}

//校验简易团体录单 集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   add by 赵庆涛 2016-06-28
function checkCrsBussSaleChnl()
{
	var tSQL = "select Crs_SaleChnl from lcgrpcont where prtno = '"+mPrtNo+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单数据失败！");
		return false;
	}
	var tCrs_SaleChnl = arr[0][0];
	if(tCrs_SaleChnl != null && tCrs_SaleChnl != "")
	{
		var strSQL = "select 1 from lcgrpcont lgc where 1 = 1"  
			+ " and lgc.Crs_SaleChnl is not null " 
			+ " and lgc.Crs_BussType = '01' " 
			+ " and not (lgc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lgc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
			+ " and lgc.PrtNo = '" + mPrtNo + "' ";
		var arrResult = easyExecSql(strSQL);
        if (arrResult != null)
        {
        	alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
        	return false;
        }
	}
	return true;
}


//by yuchunjian  date 20180730   redmine 3884
function checkFXlevel(){
		var sql1 = "select name from lcgrpappnt where prtno='"+mPrtNo+"'  ";
		var arrResult = easyExecSql(sql1);
		if(arrResult){
			var sqlblack = "select max(HighestEvaLevel) from fx_FXQVIEW where AOGName= '"+arrResult[0][0]+"' ";
			var fxarr = easyExecSql(sqlblack);
			   if(sqlblack){
				   if(fxarr[0][0]=="3"){
					   if(!confirm(arrResult[0][0] +"，风险等级评估为高风险客户，确认要完成录入？")){
						   return false;
					   }
				   }else if(fxarr[0][0]=="2"){
					   if(!confirm(arrResult[0][0] +"，风险等级评估为中风险客户，确认要完成录入？")){
						   return false;
					   }
				   }
			   }
		}
		return true;
	}
//校验黑名单
function checkBlackName(){
	//黑名单校验投保人
	var sql1 = "select name from lcgrpappnt where prtno='"+mPrtNo+"' ";
	var arrResult = easyExecSql(sql1);
	if(arrResult){
		var sqlblack = "select 1 from lcblacklist where name='"+arrResult[0][0]+"'";
		var arrResult1 = easyExecSql(sqlblack);
		if(arrResult1){
			if(!confirm("该保单投保人："+arrResult[0][0]+"，存在于黑名单中，确认要完成录入？")){
				return false;
			}
		}
	}
	
	//黑名单校验被保人
	var sql2 = "select name,idtype,idno from lcinsured where prtno='"+mPrtNo+"'  ";
	var arrRes = easyExecSql(sql2);
	var tInsuNames = "";
	if(arrRes){
		for(var i = 0; i < arrRes.length; i++){
			var sqlblack2  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrRes[i][0]+"' and idnotype='"+arrRes[i][1]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrRes[i][0]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrRes[i][0]+"' and type='0' ";
			var arrRes1 = easyExecSql(sqlblack2);
			if(arrRes1){
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ " , "+arrRes[i][0];
				}else{
					tInsuNames = tInsuNames + arrRes[i][0];
				}
			}
		}
	}
	if(tInsuNames != ""){
		if (!confirm("该保单被保人："+tInsuNames+",存在于黑名单中，确认要完成录入？")){
			return false;
		}
	}
	return true;
}

//页面职业职业类别的显示
function getdetailwork(){
	var strSql = "select OccupationType from LDOccupation where OccupationCode='" + fmSave.OccupationCode.value+"'";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null) {
	    fmSave.OccupationType1.value = arrResult[0][0];

	  }
	else{  
		fmSave.OccupationType1.value = '';
	}
}

//国籍的显示
function getNativeName(){
	var nativePlace = fmSave.all('NativePlace').value;
	if(nativePlace){
		var strSql = "select codename from ldcode where code ='"+nativePlace+"' with ur ";
		var arrResult = easyExecSql(strSql);
		if (arrResult != null) {
		    fmSave.NativePlaceName.value = arrResult[0][0];
		    controlNativeCity("");
		  }
		else{  
			fmSave.NativePlaceName.value = '';
		}
	}
}

//保存或修改时港澳台国籍，证件类型，证件号码，职业代码，职业类别
function chkOccupation(){
	if(fmSave.all("Sex").value==""){
		alert("性别不能为空！");
		return false;
	}
	if(fmSave.all("Birthday").value==""){
		alert("出生日期不能为空！");
		return false;
	}
	var naticePlace = fmSave.all("NativePlace").value;
	var nativeCity = fmSave.all("AppntNativeCity").value;
	if(naticePlace==""){
		alert("国籍不能为空！");
		return false;
	}
	if(naticePlace!="HK" && naticePlace!="ML" && naticePlace!="OS"){
		alert("国籍录入错误！");
		return false;
	}
	if(naticePlace=="HK"||naticePlace=="OS"){
		if(!nativeCity){
			alert("输入有误，可能是如下错误中的一个："+document.getElementById("NativeCityTitle").innerText+"不能为空!");
			return false;
		}
		var strsql = "select 1 from ldcode where codetype='nativecity' and code='"+nativeCity+"' with ur ";
		var res = easyExecSql(strsql);
		if(!res){
			alert("输入有误，可能是如下错误中的一个："+document.getElementById("NativeCityTitle").innerText+"输入不符合数据库规定的取值范围！请查阅或双击输入框选择！");
			return false;
		}
		var sql = "select 1 from ldcode where codetype='nativecity' and comcode='"+naticePlace+"' and code='"+nativeCity+"' with ur ";
		var arrResult = easyExecSql(sql);
		if(!arrResult){
			alert("保存失败，原因是：国籍与"+document.getElementById("NativeCityTitle").innerText+"不匹配！");
			return false;
		}
	}
	if(fmSave.all("Position2").value==""){
		alert("岗位不能为空！");
		return false;
	}
	if(fmSave.all("OccupationCode").value==""){
		alert("职业代码不能为空！");
		return false;
	}
	if(fmSave.all("OccupationType1").value==""){
		alert("职业类别不能为空！");
		return false;
	}
	if(fmSave.all("IDType").value=="a"||fmSave.all("IDType").value=="b"){
		 if(fmSave.all("NativePlace").value!="HK"){
			 alert("国籍与证件类型不匹配！");
			 return false;
		 }
		 if(fmSave.all("IDType").value=='a'){
				if(fmSave.all("AppntNativeCity").value!='1'&&fmSave.all("AppntNativeCity").value!='2'){
					alert("联系人的证件类型和地区不匹配！");
					return false;
				}
			}
			if(fmSave.all("IDType").value=='b'){
				if(fmSave.all("AppntNativeCity").value!='3'){
					alert("联系人的证件类型和地区不匹配！");
					return false;	
				}
			}
			if(fmSave.all("AppntNativeCity").value=='1'){
				if(fmSave.all("IDNo").value.indexOf('810000')!=0){
					alert("联系人证件号码和地区不匹配！");
					return false;
				}
			}
			if(fmSave.all("AppntNativeCity").value=='2'){
				if(fmSave.all("IDNo").value.indexOf('820000')!=0){
					alert("联系人证件号码和地区不匹配！");
					return false;
				}
			}
			if(fmSave.all("AppntNativeCity").value=='3'){
				if(fmSave.all("IDNo").value.indexOf('830000')!=0){
					alert("联系人证件号码和地区不匹配！");
					return false;
				}
			}
	 }
	var strSql = "select 1 from LDOccupation where occupationcode='"+fmSave.all("OccupationCode").value+"' and occupationtype='"+fmSave.all("OccupationType1").value+"' with ur ";
	var arrResult = easyExecSql(strSql);
	if (!arrResult) {
		alert("职业代码与职业类别不匹配！");
		return false;
	}
	return true;
}

function controlNativeCity(displayFlag) {
	if (fmSave.NativePlace.value == "OS") {
		fmSave.all("NativeCityInfo").style.display = displayFlag;
		fmSave.all("NativeCity").style.display = displayFlag;
		fmSave.all("NativeCityTitle").innerHTML = "国家";
	} else if (fmSave.NativePlace.value == "HK") {
		fmSave.all("NativeCityInfo").style.display = displayFlag;
		fmSave.all("NativeCity").style.display = displayFlag;
		fmSave.all("NativeCityTitle").innerHTML = "地区";
	} else {
		fmSave.all("NativeCity").style.display = "none";
	}
}
//为代码赋汉字
function setCodeName(fieldName, codeType, code) {
	var sql = "select CodeName('" + codeType + "', '" + code + "') from dual";
	var rs = easyExecSql(sql)
	if (rs) {
		fmSave.all(fieldName).value = rs[0][0];
	}
}