//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var succFlag = false;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus(); 
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}
//提交，保存按钮对应操作
function submitForm()
{
	
	//校验投保人地址
	if (!CheckAddress()) {
		return false;
	}
   if(!checkSaleChnlInfo()){
   	return false;
   }
   
   // by gzh 20101118
	//选择交叉销售后，对交叉销售各不为空项校验。
  if(!MixComCheck())
  {
	return false;
  }
  
  //2017-02-08 赵庆涛
//社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
  if(!checkCrs_SaleChnl()){
  	return false;
  }
  
  //2017-07-04 赵庆涛
//纳税人信息录入校验
  if(!checkTaxpayer()){
  	return false;
  }

  
  if(fm.SaleChnl.value == "03"){
  	if(fm.AgentCom.value == "" || fm.AgentCom.value == "null"){
  		alert("销售渠道为中介，但没有录入中介公司代码！");
  		return false;
  	}
  }
  
  if( verifyInput2() == false )
    return false;
  
  if( beforeSubmit() == false )
    return false;
  
  //校验移动电话、联系电话
  if(!checkPhone(fm.Mobile1.value)){
	return false;
  }
  if(!checkPhone(fm.Phone1.value)){
	return false;
  }
  
    //by gzh 20130408 校验综合开拓
  if(!ExtendCheck())
  {
	return false;
  }
  
  fm.fmAction.value = "INSERT||GROUPPOL" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
  
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if(fm.fmAction.value=="DELETE||GROUPPOL" && FlagStr == "Succ"){
  	emptyFormElements();
	}
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
  if(fm.PrtNo.value!=null && fm.PrtNo.value!="")
  {
    displayLCGrpCont(easyExecSql("select * from lcgrpcont where prtno='"+fm.PrtNo.value+"'"));
    var strSql = "select 1 from LOBGrpCont where GrpContNo='"+fm.GrpContNo.value+"'";
    var arr = easyExecSql(strSql);
    if(arr){
    	succFlag = true;
    }
  }
  afterContPlanSava();
  //如果保存成功自动进入被保人页面
  if(FlagStr == "Succ" ){
  	intoInsured();
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LDClassInfo.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value = "";
        fm.all('Crs_SaleChnlName').value = "";
        fm.all('Crs_BussType').value = "";
        fm.all('Crs_BussTypeName').value = "";
        fm.all('GrpAgentCom').value = "";
        fm.all('GrpAgentComName').value = "";
        fm.all('GrpAgentCode').value = "";
        fm.all('GrpAgentName').value = "";
        fm.all('GrpAgentIDNo').value = "";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}
//取消按钮对应操作
function cancelForm()
{
  //  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
  if(AgentType == "1"){
  	if(fm.SaleChnl.value != "03"){
	  	alert("登陆系统为代理点出单系统，因此此单销售渠道为中介！");
	  	fm.SaleChnl.value = "03";
	  	return false;
	  }
	  if(dateDiff(fm.CValiDate.value,CurrentDate,"D")>0){
	  	alert("不允许生效日期在当前日期之前！");
	  	return false;
	  }
  }
  if(fm.SaleChnl.value == "03"){
  	if(fm.AgentCom.value == "" || fm.AgentCom.value == "null"){
  		alert("销售渠道为中介，但没有录入中介公司代码！");
  		return false;
  	}
  	
  	//校验代理机构及代理机构业务员
    
  }
  
    // 集团交叉业务要素校验
    if(!checkCrsBussParams())
    {
        return false;
    }
    // --------------------

  return true;
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmAction.value = "DELETE||GROUPPOL";
    fm.submit(); //提交
    initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
//代理人查询
function queryAgent()
{
  if(fm.all('ManageCom').value=="")
  {
    alert("请先录入管理机构信息！");
    return;
  }
  if(fm.all('GroupAgentCode').value == "")
  {
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=2"+ "&SaleChnl=" + fm.SaleChnl.value+ "&AgentCom=" + fm.AgentCom.value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  }
  if(fm.all('GroupAgentCode').value != "")
  {

    var cAgentCode = fm.GroupAgentCode.value;  //保单号码
    var strSql = "select GroupAgentCode,Name,AgentGroup,AgentCode from LAAgent where GroupAgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null)
    {
      fm.AgentName.value = arrResult[0][1];
      fm.AgentGroup.value = arrResult[0][2];
      fm.AgentCode.value = arrResult[0][3];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else
    {
      fm.AgentGroup.value="";
      fm.AgentCode.value ="";
      fm.GroupAgentCode.value ="";
      alert("编码为:["+fm.all('GroupAgentCode').value+"]的代理人不存在，请确认!");
    }
  }
}
//代理人查询返回
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{
  if(arrResult!=null)
  {
    fm.AgentCode.value = arrResult[0][0];
    fm.AgentName.value = arrResult[0][5];
    fm.AgentGroup.value = arrResult[0][1];
    fm.GroupAgentCode.value = arrResult[0][95];
  }
}
/*********************************************************************
 *  Click事件，当双击“投保单位客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt1()
{
  if( mOperate == 0 )
  {
    mOperate = 2;
    showInfo = window.open( "../sys/GroupMain.html" );
  }
}
function showAppnt()
{
  if (fm.all("GrpNo").value == "" )
  {
    showAppnt1();
  }
  else
  {
    arrResult = easyExecSql("select * from LDGrp b where  b.CustomerNo='" + fm.all("GrpNo").value + "'", 1, 0);
    if (arrResult == null)
    {
      alert("未查到投保单位信息");
    }
    else
    {
      afterQuery(arrResult);
    }
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组，为团体客户返回
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
  displayLDGrp(arrQueryResult);
}
//初始化地址编码
function getAddressCodeData()
{
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select AddressNo,GrpAddress from LCGrpAddress where CustomerNo ='"+fm.GrpNo.value+"'";
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "")
  {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++)
    {
      j = i + 1;
      tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    }
  }
  fm.all("GrpAddressNo").CodeData=tCodeData;
}
function afterCodeSelect( cCodeName, Field )
{
	//关闭社保渠道勾选交叉销售功能（社保渠道不得选择交叉销售业务）
	if(cCodeName=="unitesalechnl"){
		fm.all("MixComFlag").style.display="";
		fm.MixComFlag.checked = false;
		if(fm.SaleChnl.value=="18"||fm.SaleChnl.value=="20"){
			fm.all("MixComFlag").style.display="none";
			fm.MixComFlag.checked = false;
		}
		isMixCom();
	}
	
	
  if(cCodeName=="GetGrpAddressNo")
  {
    var strSQL="select * from LCGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
    arrResult=easyExecSql(strSQL);
    displayGrpAddress(arrResult);
  }
  if(cCodeName=="BriefRisk")
  {
    showRiskDuty(Field.value);
  }
  if(cCodeName=="PayMode")
  {
   if(Field.value=="4"||Field.value=="6")   
    {
      fm.all("BankCode").focus();
      fm.all("BankCode").className = "code8";
      fm.all("BankCode").readOnly = false;
      fm.all("BankCode").tabIndex = 0;
      //fm.all("BankCode").ondblclick = ;

      fm.all("BankAccNo").className = "common";
      fm.all("BankAccNo").readOnly = false;
      fm.all("BankAccNo").tabIndex = 0;

      fm.all("AccName").className = "common";
      fm.all("AccName").readOnly = false;
      fm.all("AccName").tabIndex = 0;
    }
    else
    {
    	fm.all("BankCode").className = "readonly";
      fm.all("BankCode").readOnly = true;
      fm.all("BankCode").tabIndex = -1;
      //fm.all("BankCode").ondblclick = "";

      fm.all("BankAccNo").className = "readonly";
      fm.all("BankAccNo").readOnly = true;
      fm.all("BankAccNo").tabIndex = -1;
      //fm.all("BankAccNo").ondblclick = "";

      fm.all("AccName").className = "readonly";
      fm.all("AccName").readOnly = true;
      fm.all("AccName").tabIndex = -1;
    }
  }

  if(cCodeName=="SaleChnl" || cCodeName=="unitesalechnl")
  {
    
    if(Field.value=="03" || Field.value=="15" || Field.value=="04" || Field.value=="10" || Field.value=="20")
    {
      fm.all('AgentCom').style.display="";
      fm.all("AgentSaleCode").style.display = "";
	  fm.all("AgentSaleName").style.display = "";
	  fm.all("AgentSaleCode").value = "";
	  fm.all("AgentSaleName").value = "";
    }else  if(Field.value!="03" && Field.value!="10")
    {
      fm.all('AgentCom').style.display="none";
      fm.all('AgentCom').value ="";
      fm.all('AgentComName').style.display="none";
      //fm.all('AgentCom').className = "readonly";
      //fm.all('AgentCom').readOnly = true;
      //fm.all('AgentCom').disable = true;
      //fm.all('AgentCom').ondblclick = "";
      //fm.all('AgentComName').className = "readonly";
      //fm.all('AgentComName').readOnly = true;
      //fm.all('AgentComName').ondblclick = "";
      fm.all("AgentSaleCode").style.display = "none";
	  fm.all("AgentSaleCode").value = "";
	  fm.all("AgentSaleName").style.display = "none";
    }
    else
    {
      fm.all('AgentCom').style.display="";
      fm.all('AgentComName').style.display="";
      
      fm.all('AgentCom').value ="";
      fm.all('AgentComName').value ="";

      fm.all('AgentCom').className = "common";
      //fm.all('AgentCom').readOnly = false;
      ////fm.all('AgentCom').ondblclick = " showCodeList('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');";
      fm.all('AgentComName').className = "common";
      //fm.all('AgentComName').readOnly = false;
      //fm.all('AgentComName').ondblclick = "";
      fm.all("AgentSaleCode").style.display = "none";
	  fm.all("AgentSaleCode").value = "";
	  fm.all("AgentSaleName").style.display = "none";
    }
  }
  if(cCodeName=="GrpPayMode")
  {
  	
  	if(Field.value == 2||3||4)
  	{
  		//alert();
  		fm.BankCode.style.display='';
  		
  		fm.BankCodeName.style.display='';
  		fm.BankAccNo.style.display='';
      fm.AccName.style.display='';
    
    	//fm.all('BankAccNo').readOnly=true;
    	//fm.AccName.className="readonly";
    	//fm.all('AccName').readOnly=false;
  	}
  if(Field.value==1)
  	{
  		fm.BankCode.style.display='none';
  		
  		fm.BankCodeName.style.display='none';
  		fm.BankAccNo.style.display='none';
      fm.AccName.style.display='none';
  	}
  }
  //双击交叉销售渠道时,清空对方机构代码和对方机构名称
  if(cCodeName=="crs_salechnl")
  {
      fm.GrpAgentCom.value = "";
  }
  //if(cCodeName=="grpagentcom")
  //{
      //GetGrpAgentName();
  //}
}
function queryGrpCont()
{ 
  displayLCGrpCont(easyExecSql("select * from lcgrpcont where prtno='"+prtNo+"'"));
 
  var yj = easyExecSql("select codename('crs_busstype',Crs_BussType) from lcgrpcont where 1=1 and PrtNo = '" + fm.PrtNo.value + "' ");
  try{fm.all('Crs_BussTypeName').value = yj[0][0]; } catch(ex) { };
  if(LoadFlag=="2"){
  	var strSql = "select 1 from LOBGrpCont where GrpContNo='"+fm.GrpContNo.value+"'";
    var arr = easyExecSql(strSql);
    if(arr){
    	succFlag = true;
    }
  }
}
function queryAppntInfo()
{
  if(fm.GrpNo.value!=""&&fm.GrpAddressNo.value!="")
  {
    displayLDGrp(easyExecSql("select * from LDGrp where CustomerNo='"+fm.GrpNo.value+"'"));
    displayGrpAddress(easyExecSql("select * from LCGrpAddress where CustomerNo='"+fm.GrpNo.value+"' and AddressNo='"+fm.GrpAddressNo.value+"'"));
  }
}

function updateClick()
{
	//校验投保人地址
	if (!CheckAddress()) {
		return false;
	}
	
	if(!checkSaleChnlInfo()){
   		return false;
   	}
	
	 // by gzh 20101118
	//选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
	
    //2017-02-08 赵庆涛
//  社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
    if(!checkCrs_SaleChnl()){
    	return false;
    }

    //2017-07-04 赵庆涛
//  纳税人信息录入校验
    if(!checkTaxpayer()){
    	return false;
    }

	if( verifyInput2() == false )
	{
		return false;
	}
	
	if(!beforeSubmit()){
		return false;
	}
	
	//校验移动电话、联系电话
	if(!checkPhone(fm.Mobile1.value)){
		return false;
	}
	if(!checkPhone(fm.Phone1.value)){
		return false;
	}
	
	//by gzh 20130408 校验综合开拓
	if(!ExtendCheck())
	{
		return false;
	}
  fm.fmAction.value = "UPDATE||GROUPPOL" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  if(LoadFlag=="2"){
  	var strSQL = "select '1' from lobgrpcont where GrpContNo='"+fm.GrpContNo.value+"'";
  	var arr = easyExecSql(strSQL);
  	if(!arr){
	  	if(checkCanModify()){
	  		fm.action="./BriefContSave.jsp?LoadFlag=2";
			}
		}
		fm.submit();
  }else{
	  fm.submit(); //提交
	}
}

function showRiskDuty(RiskCode)
{
  RiskDutyGrid.delBlankLine();
  var strSql = "select a.dutycode,b.dutyname,a.riskcode from lmriskduty a,lmduty b "
               +"where a.dutycode=b.dutycode and a.riskcode='"+RiskCode+"'";
  var arr = easyExecSql(strSql);
  if(!arr)
  {
    alert("查询责任失败！");
    return;
  }
  var mulLineCount = RiskDutyGrid.mulLineCount;
  for(var i=0 ; i<arr.length ; i++)
  {
    for(var m=0 ; m<mulLineCount ; m++)
    {
      if(arr[i][0] == RiskDutyGrid.getRowColData(m,1))
      {
        alert("险种不能重复添加！");
        return;
      }
    }
    RiskDutyGrid.addOne("RiskDutyGrid");
    RiskDutyGrid.setRowColData(mulLineCount+i,1,arr[i][0]);
    RiskDutyGrid.setRowColData(mulLineCount+i,2,arr[i][1]);
    RiskDutyGrid.setRowColData(mulLineCount+i,3,arr[i][2]);
  }
}
function showRiskDutyFactor(parm1,parm2)
{
  var getWhereDutyCode = "(";
  var chk = false;
  for (i=0;i<RiskDutyGrid.mulLineCount;i++)
  {
    if (RiskDutyGrid.getChkNo(i))
    {
      //alert(ContPlanDutyGrid.getRowColData(i,1));
      getWhereDutyCode = getWhereDutyCode + "'"+RiskDutyGrid.getRowColData(i,1)+"',"
                         chk = true;
    }
  }
  getWhereDutyCode = getWhereDutyCode.substring(0,getWhereDutyCode.length-1) + ")"
  if(chk)
  {
    showFactor(getWhereDutyCode);
  }
  else
  {
    showFactor("('')");
  }
}

function showFactor(getWhereDutyCode)
{
  //险种名称,险种编码,责任编码,险种责任,计算要素,要素名称,要素说明
  //要素值,特别说明,险种版本,集体保单险种号码,主险编码,类型,计算方法
  //总保费,缴费计划编码,给付责任编码,账户号码
  var strSql = "select a.riskname,a.riskcode,b.dutycode,c.dutyname,d.calfactor,d.factorname,d.factornoti,(select CalFactorValue from LCContPlanDutyParam z where z.grpcontno='"+fm.GrpContNo.value+"' and z.dutycode=d.dutycode and z.riskcode=d.riskcode and z.CalFactor=d.CalFactor),'',a.riskver, d.calfactortype,c.calmode,d.PayPlanCode,d.GetDutyCode,d.InsuAccNo "
               +"from lmrisk a,lmriskduty b,lmduty c,lmriskdutyfactor d "
               +"where 1=1  "
               +" and a.riskcode=b.riskcode "
               +" and b.dutycode=c.dutycode "
               +" and d.riskcode=a.riskcode "
               +" and d.dutycode=b.dutycode "
               +" and d.dutycode in "+getWhereDutyCode
               +" order by d.riskcode,d.FactorOrder";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    displayMultiline(arr,ContPlanGrid,turnPage);
  }
  else
  {
    ContPlanGrid.clearData();
  }
  for(var i=0;i<ContPlanGrid.mulLineCount;i++){
  	if(ContPlanGrid.getRowColData(i,5)=="StandbyFlag1"){
  		ContPlanGrid.setRowColData(i,8,fm.DegreeType.value);
  	}
  }
}
function saveRiskPlan()
{
	if(fm.GrpContNo.value=="" || fm.GrpContNo.value == "null" || fm.GrpContNo.value == null){
		alert("请先保存合同信息！");
		return;
	}
  fm.fmAction.value = "INSERT||CONTPLAN" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="./ContPlanRiskSava.jsp";
  fm.submit(); //提交
}
//目前采取保障计划保存时直接保存险种信息
//因为可以认为存在险种就一定存在计划信息
function afterContPlanSava()
{
  var strSql = "select riskcode from LCGrpPol where GrpContNo='"+fm.GrpContNo.value+"'";
  var arr = easyExecSql(strSql);
  var getWherePart = "(";
  if(arr)
  {
    //已经查询到险种信息

    for(var i=0 ; i<arr.length ; i++)
    {
      getWherePart += "'";
      getWherePart += arr[i][0];
      getWherePart += "'";
      if(i==arr.length-1)
      {
        getWherePart += ")";
      }
      else
      {
        getWherePart += ",";
      }
    }
    var strSql = "select a.riskname,a.riskcode,b.dutycode,c.dutyname,d.calfactor,d.factorname,d.factornoti,z.CalFactorValue,'',a.riskver, d.calfactortype,c.calmode,d.PayPlanCode,d.GetDutyCode,d.InsuAccNo,'','','',z.grppolno "
                 +"from lmrisk a,lmriskduty b,lmduty c,lmriskdutyfactor d,LCContPlanDutyParam z "
                 +"where 1=1  "
                 +" and a.riskcode=b.riskcode "
                 +" and b.dutycode=c.dutycode "
                 +" and d.riskcode=a.riskcode "
                 +" and d.dutycode=b.dutycode "
                 +" and z.dutycode=d.dutycode "
                 +" and z.riskcode=d.riskcode "
                 +" and z.CalFactor=d.CalFactor"
                 +" and d.riskcode in "+getWherePart
                 +" and z.GrpContNo='"+fm.GrpContNo.value+"'"
                 +" order by d.riskcode,d.FactorOrder";
    var relult = easyExecSql(strSql);
    if(relult)
    {
      displayMultiline(relult,ContPlanGrid,turnPage);
    }
  }
}
function deleteRiskPlan()
{
  fm.fmAction.value = "DELETE||CONTPLAN" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="./ContPlanRiskSava.jsp";
  fm.submit(); //提交
}

//校验中介机构是否是共保机构
function checkAgentCom() 
{
	var tSaleChnl = fm.SaleChnl.value;
	var tAgentCom = fm.AgentCom.value;
	
	if(tSaleChnl != null && tSaleChnl != "" && tSaleChnl == "03")
	{
		if(tAgentCom != null && tAgentCom != "")
		{
			var strSQL = "select actype from lacom where agentcom = '" + tAgentCom + "' ";
			var arrResult = easyExecSql(strSQL);
    		if(arrResult != null && arrResult == "05")
    		{
    			alert("团险中介的中介机构不能为共保的机构!");
    			return false;
    		}
		}
	}
	return true;
}
function intoInsured()
{
	//中介机构的校验
	if(!checkAgentCom())
	{
		return false;
	}
	
	if(fm.GrpContNo.value == null || fm.GrpContNo.value=="" || fm.GrpContNo.value == "null"){
		alert("请先保存投保合同信息才能进行下一步操作！");
		return;
	}
	parent.fraInterface.window.location = "./BriefLCInuredListInput.jsp?PrtNo="+ fm.PrtNo.value+"&GrpContNo="+ fm.GrpContNo.value+"&LoadFlag="+LoadFlag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&AgentType="+AgentType;
  //var strUrl = 
  //window.open(strUrl,"被保人清单导入","width=1000px, height=600px, top=0, left=0");
}
function inputConfirm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  if(LoadFlag=="2"){
  	fm.fmAction.value = "CHANGE||GROUPPOL" ;
  	fm.action="./BriefInputChangeSave.jsp";
  }else{
  	fm.action="./BriefInputConfirmSave.jsp";
  }
  fm.submit();
}
function queryNation()
{
	var strSql = "select nationno,ChineseName from lcnation where GrpContNo='"+fm.GrpContNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		displayMultiline(arr,NationGrid,turnPage);
	}
}
function queryImpart()
{
	var strSql = "select ImpartParamModle from lccustomerimpart where GrpContNo='"+fm.GrpContNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		fm.IntentionName.value = arr[0][0];
	}
}
function queryDays()
{
	//首先将系统中的日期正确显示成保险公司需要的。
//	var strSql = "select CInValiDate day from LCGrpCont where GrpContNo='"+fm.GrpContNo.value+"'";
//	var arr = easyExecSql(strSql);
//	if(arr){
//		fm.CInValiDate.value = arr[0][0];
//	}
	//单次多次
	if(fm.DegreeType.value == "1"){
		if(!isDate(fm.CValiDate.value)){
			alert("选择多次旅行只需录入生效日期！");
			return;
		}
		var strSql = "select date('"+fm.CValiDate.value+"')+ 1 year -1 day from dual";	
		var arr=easyExecSql(strSql);
		fm.CInValiDate.value = arr;
	}
	if(!isDate(fm.CInValiDate.value)||!isDate(fm.CValiDate.value))
		return;
		
	if(LoadFlag=="2"){
		dealDays();
	}
	var strSql = "select integer(to_date('"+fm.CInValiDate.value+"')-to_date('"+fm.CValiDate.value+"'))+1 from dual";	
	var arr=easyExecSql(strSql);
	if(arr)
	{
		fm.Days.value = arr[0][0];
	}
}
function modifyRiskPlan()
{
	fm.fmAction.value = "UPDATE||CONTPLAN" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="./ContPlanRiskSava.jsp";
  fm.submit(); //提交
}
//合同维护相关
//仅仅可以接受投保单位名称（包括英文）变更、联系地址、
//联系电话、被保险人姓名（包括拼音）变更、被保险人出
//生日期（不超出3－70岁范围，以及相应的年龄核保规则）
//、被保险人护照号码变更、目的地（抵达国家）、合同生
//效开始时间；
function checkCanModify()
{
	//首先校验是否需要生成新的合同，通过Sql实现，如果查询
	//到结果则表示不需要生成新的合同否则重新生成
	var needRePrintCont = false;
	var strSql = "select 1 from LDGrp where CustomerNo='"+fm.GrpNo.value+"' and grpname='"+fm.GrpName.value+"' and grpenglishname='"+fm.GrpEnglishName.value	+"' "
							+" union all "
							+" select 1 from lcnation where GrpContNo='"+fm.GrpContNo.value+"' "+getNationWherePart()
							+" union all"
							+" select 1 from lcgrpcont where GrpContNo='"+fm.GrpContNo.value+"' and CValiDate='"+fm.CValiDate.value+"'" 
							+" union all"
							+" select 1 from dual where (select count(1) from lcnation where  GrpContNo='"+fm.GrpContNo.value+"' )="+NationGrid.mulLineCount;
	var arr = easyExecSql(strSql);
	if(arr){
		if(arr.length<NationGrid.mulLineCount+3){
			needRePrintCont = true;
		}
	}
	else{
		needRePrintCont = true;
	}
	return needRePrintCont;
}
//拼国家代码的条件语句部分
function getNationWherePart(){
	var wherePart = "(";
	if(NationGrid.mulLineCount==0){
		return "";
	}
	for(var i=0;i<NationGrid.mulLineCount;i++){
		wherePart += "'";
		wherePart += NationGrid.getRowColData(i,1);
		wherePart += "'";
		if(i!=NationGrid.mulLineCount-1){
			wherePart += ",";
		}
	}
	wherePart += ")";
	return "and NationNo in " +wherePart;
}
//合同添加到备份表后进行修改操作
function afterContToBak(FlagStr,Content)
{
	if(FlagStr == "Succ"){
		fm.action="./BriefContSave.jsp"
		fm.submit();
	}
}
//调整生效失效日期
function dealDays(){
	//处理生效日期，失效日期
	var strSql = "select distinct InsuYear from lcpol where GrpContNo='"+fm.GrpContNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		var strSql = "select date('"+fm.CValiDate.value+"') + ("+arr[0][0]+"-1) day from dual";
		var CInValiDate = easyExecSql(strSql);
		if(CInValiDate)
		{
			fm.CInValiDate.value = CInValiDate[0][0];
		}
	}
}
//投保公司的查询
function QueryOnKeyDown() {
  arrResult = easyExecSql("select * from LDGrp  where GrpName like '%%" + fm.all('GrpName').value + "%%'", 1, 0);
  if (arrResult == null) {
    alert("对不起,找不到该投保单位!");
    return false;
  } else {
    arrResult = easyExecSql("select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,"
                            + " b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.CustomerNo,"
                            + " b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpEnglishName,b.TaxpayerType,b.TaxNo,b.CustomerBankCode,b.CustomerBankAccNo,b.OrgancomCode,b.UnifiedSocialCreditNo from LDGrp b where"
                            + " b.GrpName like '%%" + fm.all('GrpName').value + "%%'", 1, 0);
    if (arrResult == null) {
      alert("未查到投保单位信息");
    }
    if (arrResult.length == 1) {
      try {
        fm.all('GrpName').value= arrResult[0][0];
      } catch(ex) { }
      ;
      try {
        fm.all('BusinessType').value= arrResult[0][1];
      } catch(ex) { }
      ;
      try {
        fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][1]);
      } catch(ex) { }
      ;
      try {
        fm.all('GrpNature').value= arrResult[0][2];
      } catch(ex) { }
      ;
      try {
        fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][2]);
      } catch(ex) { }
      ;
      try {
        fm.all('Peoples').value= arrResult[0][3];
      } catch(ex) { }
      ;
      try {
        fm.all('RgtMoney').value= arrResult[0][4];
      } catch(ex) { }
      ;
      try {
        fm.all('Asset').value= arrResult[0][5];
      } catch(ex) { }
      ;
      try {
        fm.all('NetProfitRate').value= arrResult[0][6];
      } catch(ex) { }
      ;
      try {
        fm.all('MainBussiness').value= arrResult[0][7];
      } catch(ex) { }
      ;
      try {
        fm.all('Corporation').value= arrResult[0][8];
      } catch(ex) { }
      ;
      try {
        fm.all('ComAera').value= arrResult[0][9];
      } catch(ex) { }
      ;
      try {
        fm.all('Fax').value= arrResult[0][10];
      } catch(ex) { }
      ;
      try {
        fm.all('Phone').value= arrResult[0][11];
      } catch(ex) { }
      ;
      try {
        fm.all('FoundDate').value= arrResult[0][12];
      } catch(ex) { }
      ;
      try {
        fm.all('GrpNo').value= arrResult[0][13];
      } catch(ex) { }
      ;
      try {
        fm.all('AppntOnWorkPeoples').value= arrResult[0][14];
      } catch(ex) { }
      ;
      try {
        fm.all('AppntOffWorkPeoples').value= arrResult[0][15];
      } catch(ex) { }
      ;
      try {
        fm.all('AppntOtherPeoples').value= arrResult[0][16];
      } catch(ex) { }
      ;
      try {
        fm.all('GrpEnglishName').value= arrResult[0][17];
      } catch(ex) { }
      ;
      try {
    	  fm.all('TaxpayerType').value= arrResult[0][18];
      } catch(ex) { }
      ;
      try {
    	  fm.all('TaxNo').value= arrResult[0][19];
      } catch(ex) { }
      ; 
      try {
    	  fm.all('CustomerBankCode').value= arrResult[0][20];
      } catch(ex) { }
      ; 
      try {
    	  fm.all('CustomerBankAccNo').value= arrResult[0][21];
      } catch(ex) { }
      ; 
      try {
    	  fm.all('OrgancomCode').value= arrResult[0][22];
      } catch(ex) { }
      ; 
      try {
    	  fm.all('UnifiedSocialCreditNo').value= arrResult[0][23];
      } catch(ex) { }
      ; 

      var strSQL="select * from LCGrpAddress b where b.CustomerNo='"+fm.GrpNo.value+"' order by AddressNo desc";
	    var arrResult_AD=easyExecSql(strSQL);
	    displayGrpAddress(arrResult_AD);
    }
    if (arrResult.length > 1) {
      var varSrc = "&Grpname=" + fm.all('Grpname').value;
      showinfo = window.open("../app/FrameMainGrpAppntQuery.jsp?Interface= GrpAppntQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
    }
  }
}
function afterAppntQuery( arrReturn ) {

  arrResult = easyExecSql("select b.GrpName,b.BusinessType,b.GrpNature,b.Peoples,b.RgtMoney,b.Asset,"
                          + " b.NetProfitRate,b.MainBussiness,b.Corporation,b.ComAera,b.Fax,b.Phone,b.FoundDate,b.CustomerNo,"
                          + " b.OnWorkPeoples,b.OffWorkPeoples,b.OtherPeoples,b.GrpEnglishName,b.TaxpayerType,b.TaxNo,b.CustomerBankCode,b.CustomerBankAccNo,b.OrgancomCode,b.UnifiedSocialCreditNo from LDGrp b where"
                          + " b.Customerno='" + arrReturn[0][0] + "'", 1, 0);

  try {
    fm.all('GrpName').value= arrResult[0][0];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessType').value= arrResult[0][1];
  } catch(ex) { }
  ;
  try {
    fm.all('BusinessTypeName').value= getNameFromLDCode("BusinessType",arrResult[0][1]);
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNature').value= arrResult[0][2];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNatureName').value= getNameFromLDCode("GrpNature",arrResult[0][2]);
  } catch(ex) { }
  ;
  try {
    fm.all('Peoples').value= arrResult[0][3];
  } catch(ex) { }
  ;
  try {
    fm.all('RgtMoney').value= arrResult[0][4];
  } catch(ex) { }
  ;
  try {
    fm.all('Asset').value= arrResult[0][5];
  } catch(ex) { }
  ;
  try {
    fm.all('NetProfitRate').value= arrResult[0][6];
  } catch(ex) { }
  ;
  try {
    fm.all('MainBussiness').value= arrResult[0][7];
  } catch(ex) { }
  ;
  try {
    fm.all('Corporation').value= arrResult[0][8];
  } catch(ex) { }
  ;
  try {
    fm.all('ComAera').value= arrResult[0][9];
  } catch(ex) { }
  ;
  try {
    fm.all('Fax').value= arrResult[0][10];
  } catch(ex) { }
  ;
  try {
    fm.all('Phone').value= arrResult[0][11];
  } catch(ex) { }
  ;
  try {
    fm.all('FoundDate').value= arrResult[0][12];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpNo').value= arrResult[0][13];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOnWorkPeoples').value= arrResult[0][14];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOffWorkPeoples').value= arrResult[0][15];
  } catch(ex) { }
  ;
  try {
    fm.all('AppntOtherPeoples').value= arrResult[0][16];
  } catch(ex) { }
  ;
  try {
    fm.all('GrpEnglishName').value= arrResult[0][17];
  } catch(ex) { }
  ;
  try {
	  fm.all('TaxpayerType').value= arrResult[0][18];
  } catch(ex) { }
  ;
  try {
	  fm.all('TaxNo').value= arrResult[0][19];
  } catch(ex) { }
  ;
  try {
	  fm.all('CustomerBankCode').value= arrResult[0][20];
  } catch(ex) { }
  ; 
  try {
	  fm.all('CustomerBankAccNo').value= arrResult[0][21];
  } catch(ex) { }
  ;
  try {
	  fm.all('OrgancomCode').value= arrResult[0][22];
  } catch(ex) { }
  ; 
  try {
	  fm.all('UnifiedSocialCreditNo').value= arrResult[0][23];
  } catch(ex) { }
  ; 
  var strSQL="select * from LCGrpAddress b where b.CustomerNo='"+fm.GrpNo.value+"' order by AddressNo desc";
	arrResult=easyExecSql(strSQL);
	displayGrpAddress(arrResult);
}
//function intoInsured(){
//	var tUrl = "./"
//}
//查询中介机构
function queryAgentCom(){
	if(fm.AgentCom.value == "" ){
		alert("请录入中介公司代码!");
		return;
	}
	if(fm.SaleChnl.value == "03")
	{
		strSql = "select Name from lacom where AgentCom = '" + fm.AgentCom.value 
				+ "' and BranchType = '2' and BranchType2 = '02'";
	}
	else if(fm.SaleChnl.value == "10")
	{
		strSql = "select Name from lacom where AgentCom = '" + fm.AgentCom.value 
				+ "' and BranchType = '1' and BranchType2 = '02'";
	}
	else if(fm.SaleChnl.value == "20")
	{
		strSql = "select Name from lacom where AgentCom = '" + fm.AgentCom.value 
				+ "' and BranchType = '6' and BranchType2 = '02'";
	}
	else
	{
		var strSql = "select Name from lacom where AgentCom='"+fm.AgentCom.value+"'";
	}
	var arr = easyExecSql(strSql);
	if(!arr){
		alert("没有查询到该中介代码对应的中介公司！请确认是否录入正确");
		fm.AgentCom.value = "" ;
		return;
	}
	alert("录入中介代码对应中介公司为: "+arr);
	//fm.AgentCom.value = arr[0][0];
}
function queryAgent2()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	if(fm.all('GroupAgentCode').value != "" && fm.all('GroupAgentCode').value.length==10 )	 {
	var cAgentCode = fm.AgentCode.value;  //保单号码	
	var strSql = "select GroupAgentCode,Name,AgentGroup,AgentCode from LAAgent where GroupAgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentGroup.value = arrResult[0][2];
      fm.AgentCode.value = arrResult[0][3];
      alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     fm.AgentCode.value ="";
     alert("代码为:["+fm.all('GroupAgentCode').value+"]的业务员不存在，请确认!");
     }
	}	
}


/**
 * 集团交叉要素校验
 */
function checkCrsBussParams()
{
    var tCrs_SaleChnl = trim(fm.Crs_SaleChnl.value);
    var tCrs_BussType = trim(fm.Crs_BussType.value);
    
    var tGrpAgentCom = trim(fm.GrpAgentCom.value);
    var tGrpAgentCode = trim(fm.GrpAgentCode.value);
    var tGrpAgentName = trim(fm.GrpAgentName.value);
    var tGrpAgentIDNo = trim(fm.GrpAgentIDNo.value);
    
    if(tCrs_SaleChnl != null && tCrs_SaleChnl != "")
    {
        if(tCrs_BussType == null || tCrs_BussType == "")
        {
            alert("选择集团交叉渠道时，集团交叉业务类型不能为空。");
            return false;
        }
        if(tGrpAgentCom == null || tGrpAgentCom == "")
        {
            alert("选择集团交叉渠道时，对方业务员机构不能为空。");
            return false;
        }
        if(tGrpAgentCode == null || tGrpAgentCode == "")
        {
            alert("选择集团交叉渠道时，对方业务员代码不能为空。");
            return false;
        }
        if(tGrpAgentName == null || tGrpAgentName == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名不能为空。");
            return false;
        }
        if(tGrpAgentIDNo == null || tGrpAgentIDNo == "")
        {
            alert("选择集团交叉渠道时，对方业务员身份证不能为空。");
            return false;
        }
    }
    else
    {
        if(tCrs_BussType != null && tCrs_BussType != "")
        {
            alert("未选择集团交叉渠道时，集团交叉业务类型不能填写。");
            return false;
        }
        if(tGrpAgentCom != null && tGrpAgentCom != "")
        {
            alert("未选择集团交叉渠道时，对方业务员机构不能为填写。");
            return false;
        }
        if(tGrpAgentCode != null && tGrpAgentCode != "")
        {
            alert("未选择集团交叉渠道时，对方业务员代码不能为填写。");
            return false;
        }
        if(tGrpAgentName != null && tGrpAgentName != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名不能为填写。");
            return false;
        }
        if(tGrpAgentIDNo != null && tGrpAgentIDNo != "")
        {
            alert("未选择集团交叉渠道时，对方业务员身份证不能为填写。");
            return false;
        }
    }
    
    return true;
}

//根据对方机构代码带出对方机构名称
/*
function GetGrpAgentName()
{
	var strSql = "select org_lvl from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null) {
	      if(arrResult[0][0] == "0")
	      {
	          fm.GrpAgentComName.value = "总公司";
	      }
	      if(arrResult[0][0] == "1")
	      {
	          fm.GrpAgentComName.value = "省分公司";
	      }
	      if(arrResult[0][0] == "2")
	      {
	          fm.GrpAgentComName.value = "地市分公司";
	      }
	      if(arrResult[0][0] == "3")
	      {
	          fm.GrpAgentComName.value = "县支公司";
	      }
	      if(arrResult[0][0] == "4")
	      {
	          fm.GrpAgentComName.value = "基层机构";
	      }
	}
	else{  
	     fm.GrpAgentComName.value = '';
    }
}
*/


//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryGrpAgent()
{
    if(fm.all('GrpAgentCode').value == "" && fm.all('GrpAgentIDNo').value == "" )
    {  
        var tGrpAgentCom = (fm.GrpAgentCom != null && fm.GrpAgentCom != "undefined") ? fm.GrpAgentCom.value : "";
        var tGrpAgentName = (fm.GrpAgentName != null && fm.GrpAgentName != "undefined") ? fm.GrpAgentName.value : "";
        var tGrpAgentIDNo = (fm.GrpAgentIDNo != null && fm.GrpAgentIDNo != "undefined") ? fm.GrpAgentIDNo.value : "";
        var strURL = "../sys/GrpAgentCommonQueryMain.jsp?GrpAgentCom=" + tGrpAgentCom +
                     "&GrpAgentName=" + tGrpAgentName + "&GrpAgentIDNo=" + tGrpAgentIDNo;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCode').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Sales_Cod='" + fm.all('GrpAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.GrpAgentCode.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }else if(fm.all('GrpAgentIDNo').value != "")
    {	
        var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Id_No='" + fm.all('GrpAgentIDNo').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery3(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("身份证号码为:[" +  fm.GrpAgentIDNo.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery3(arrResult)
{  
  if(arrResult!=null)
    {  	
  	fm.GrpAgentCode.value = arrResult[0][0];
  	fm.GrpAgentName.value = arrResult[0][1];
    fm.GrpAgentIDNo.value = arrResult[0][2];
  }
}

//执行查询交叉销售对方机构代码 date 20101117 by gzh
function queryGrpAgentCom()
{
	if(fm.all('Crs_SaleChnl').value == "" || fm.all('Crs_SaleChnl').value == null)
	{
		alert("请先选择交叉销售渠道！！");
		return false;
	}
    if(fm.all('GrpAgentCom').value == "")
    {  
        var tCrs_SaleChnl =  fm.Crs_SaleChnl.value;
        var tCrs_SaleChnlName = fm.Crs_SaleChnlName.value;
        var strURL = "../sys/GrpAgentComQueryMain.jsp?Crs_SaleChnl="+tCrs_SaleChnl+"&Crs_SaleChnlName="+tCrs_SaleChnlName;        
        //alert(strURL);
        var newWindow = window.open(strURL, "GrpAgentComQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('GrpAgentCom').value != "")
    {	
        var strGrpSql = "select grpagentcom,Under_Orgname  from lomixcom  where  grpagentcom ='"+fm.GrpAgentCom.value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery4(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("机构代码为:[" +  fm.GrpAgentCom.value + "]的机构不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery4(arrResult)
{  
  if(arrResult!=null)
    { 	
  	fm.GrpAgentCom.value = arrResult[0][0];
  	fm.GrpAgentComName.value = arrResult[0][1];
  }
}


//选择交叉销售后对交叉销售各不为空项的校验
    // by gzh 20101118
function MixComCheck()
{    
    if(fm.MixComFlag.checked == true)
    {
    	if(fm.Crs_SaleChnl.value == "" || fm.Crs_SaleChnl.value == null)
    	{
    		alert("选择交叉销售时，交叉销售渠道不能为空，请核查！");
    		fm.all('Crs_SaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.Crs_BussType.value == "" || fm.Crs_BussType.value == null)
    	{
    		alert("选择交叉销售时，交叉销售业务类型不能为空，请核查！");
    		fm.all('Crs_BussType').focus();
    		return false;
    	}
    	if(fm.GrpAgentCom.value == "" || fm.GrpAgentCom.value == null)
    	{
    		alert("选择交叉销售时，对方机构代码不能为空，请核查！");
    		fm.all('GrpAgentCom').focus();
    		return false;
    	}
    	/**
    	if(fm.GrpAgentComName.value == "" || fm.GrpAgentComName.value == null)
    	{
    		alert("选择交叉销售时，对方机构名称不能为空，请核查！");
    		fm.all('GrpAgentComName').focus();
    		return false;
    	}
    	*/
    	if(fm.GrpAgentCode.value == "" || fm.GrpAgentCode.value == null)
    	{
    		alert("选择交叉销售时，对方业务员代码不能为空，请核查！");
    		fm.all('GrpAgentCode').focus();
    		return false;
    	}
    	if(fm.GrpAgentName.value == "" || fm.GrpAgentName.value == null)
    	{
    		alert("选择交叉销售时，对方业务员姓名不能为空，请核查！");
    		fm.all('GrpAgentName').focus();
    		return false;
    	}
    	if(fm.GrpAgentIDNo.value == "" || fm.GrpAgentIDNo.value == null)
    	{
    		alert("选择交叉销售时，对方业务员身份证号码不能为空，请核查！");
    		fm.all('GrpAgentIDNo').focus();
    		return false;
    	}
      	return true;
    }else{
    	return true;
    }
}    

function checkCrs_SaleChnl(){
	var tCrs_BussType=fm.Crs_BussType.value;
	var tSaleChnl=fm.SaleChnl.value;
	if((tCrs_BussType=="01"||tCrs_BussType=="02")&&(tSaleChnl=="16"||tSaleChnl=="18")){
		alert("社保直销及社保综拓渠道出单若选择交叉销售，交叉销售业务类型不能为相互代理或联合展业！");
		return false;
	}
	return true;
}

function checkTaxpayer(){
	var tTaxpayerType = fm.TaxpayerType.value;
	var tTaxNo = fm.TaxNo.value;
	var tUnifiedSocialCreditNo = fm.UnifiedSocialCreditNo.value;
	var tOrgancomCode = fm.OrgancomCode.value;
	var tCustomerBankCode = fm.CustomerBankCode.value;
	var tCustomerBankAccNo = fm.CustomerBankAccNo.value;
	var tE_Mail1 = fm.E_Mail1.value;
	if(tTaxpayerType  == "" ){
		if(tCustomerBankCode !=　""
			||tCustomerBankAccNo !=　""){
			alert("纳税人类型为空时，客户开户银行、 客户银行账户不可录入！");
			return false;
		}
	}else if(tTaxpayerType == "1"){
		if((tTaxNo =="" && tUnifiedSocialCreditNo =="" && tOrgancomCode =="")
			||tCustomerBankCode ==　""
			||tCustomerBankAccNo ==　""
			||tE_Mail1 ==　""){
			alert("当纳税人类型为“一般纳税人”时，税务登记证号码、统一社会信用代码 、组织机构代码三项必录其一，客户开户银行、 客户银行账户、电子邮箱为必录项！");
			return false;
		}
	}else if(tTaxpayerType == "2"){
		if(tTaxNo =="" && tUnifiedSocialCreditNo =="" && tOrgancomCode == ""){
			alert("当纳税人类型为“小规模纳税人”时，税务登记证号码、统一社会信用代码 、组织机构代码三项必录其一！");
			return false;
		}
	}
	if(tTaxNo != ""){
		var a = /^[0-9a-zA-Z]*$/g;
		var tlen = tTaxNo.length;
		if(!a.test(tTaxNo)){
			alert("税务登记证号码只允许数字和字母！");
			return false;
		}
		if(tlen != 15 && tlen != 18 && tlen != 20
				){
			alert("税务登记证号码允许的位数为15、18和20位");
			return false;
		}
	}
	if(tCustomerBankCode != ""){
		var tSQL = "select 1 from ldbank where bankcode = '"+tCustomerBankCode+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("请选择或输入正确的客户开户银行！");
			return false;
		}
	}
	return true;
}

    // --------------------  
    //by gzh 20120517 增加对代理销售业务员的查询
function queryAgentSaleCode()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        fm.all('ManageCom').focus();
        return ;
    }
    
    if(fm.all('AgentCom').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom1').focus();
        return ;
    }
    var tAgentCom = fm.all('AgentCom').value;
    //var tAgentCom1 = fm.all('AgentCom1').value;
    if(fm.all('AgentSaleCode').value == "")
    {
        var strURL = "../sys/AgentSaleCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&AgentCom=" + tAgentCom
        var newWindow = window.open(strURL, "AgentSaleCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('AgentSaleCode').value != "")
    {
        var cAgentCode = fm.AgentSaleCode.value;  //保单号码
        var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom1').value + "'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentSaleName.value = arrResult[0][1];
            //alert("查询结果:  代理销售业务员代码:["+arrResult[0][0]+"] ，代理销售业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value = "";
            alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
        }
    }
}

function queryAgentSaleCode2() {

  if(fm.all('AgentSaleCode').value != ""){
    if(fm.all('AgentCom').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom1').focus();
        return ;
    }
    var cAgentCode = fm.AgentSaleCode.value;  //保单号码
    var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom1').value + "'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentSaleName.value = arrResult[0][1];
      //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    } else {
      fm.AgentGroup.value="";
      alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
    }
  }
}

function afterQuery5(arrResult){
  if(arrResult!=null) {
    fm.AgentSaleCode.value = arrResult[0][0];
    fm.AgentSaleName.value = arrResult[0][1];
  }
}

//北分中介时，中介机构及代理销售业务员必须填写
function checkAgentComAndSaleCdoe(){
	var strSql = "select AgentCom,AgentSaleCode from lcgrpcont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%' and salechnl = '03'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCom = arrResult[0][0];
	    var tAgentSaleCode = arrResult[0][1];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCom == null || tAgentCom ==''){
	   		alert("中介公司代码必须录入，请核查！");
	   		return false;    	
	   	}
	   	if(tAgentSaleCode == null || tAgentSaleCode ==''){
	   		alert("代理销售业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	
	   	var tSqlCode = "select ValidStart,ValidEnd from LAQualification where agentcode = '"+tAgentSaleCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tValidStart = arrCodeResult[0][0];
	   		var tValidEnd = arrCodeResult[0][1];
	   		if(tValidStart == null || tValidStart == ''){
	   			alert("代理销售业务员资格证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tValidEnd == null || tValidEnd == ''){
	   			alert("代理销售业务员资格证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
	   			alert("代理销售业务员资格证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("代理销售业务员资格证信息不完整，请核查！");
	   		return false;
	   	}
	   	
	   	var tSqlCom = "select LicenseStartDate,LicenseEndDate,EndFlag from lacom where agentcom = '"+tAgentCom+"'";
	   	var arrComResult = easyExecSql(tSqlCom);
	   	if(arrComResult){
	   		var tLicenseStartDate = arrComResult[0][0];
	   		var tLicenseEndDate = arrComResult[0][1];
	   		var tEndFlag = arrComResult[0][2];
	   		if(tLicenseStartDate == null || tLicenseStartDate == ''){
	   			alert("中介公司许可证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tLicenseEndDate == null || tLicenseEndDate == ''){
	   			alert("中介公司许可证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == null || tEndFlag == ''){
	   			alert("中介机构合作终止状态为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == 'Y'){
	   			alert("中介机构合作终止状态为失效，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tLicenseEndDate)=='1'){
	   			alert("中介公司许可证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("中介公司许可证信息不完整，请核查！");
	   		return false;
	   	}
    }
    return true;
}
function initAgentSaleCode(){
	var tSqlCode = "select AgentSaleCode from LCGrpCont where prtno='" + fm.PrtNo.value + "'";
	var arrCodeResult = easyExecSql(tSqlCode);
	if(arrCodeResult){
		if(arrCodeResult[0][0] != null && arrCodeResult[0][0] != ""){
			var tSQL = "select agentcode,name from laagenttemp where agentcode = '"+arrCodeResult[0][0]+"'";
			var arrResult = easyExecSql(tSQL);
			if(arrResult){
				fm.AgentSaleCode.value = arrResult[0][0];
    			fm.AgentSaleName.value = arrResult[0][1];
			}
		}
	}
}
// by gzh 20120517 end
//显示或者综合开拓
function isExtend()
{
    if(fm.ExtendFlag.checked == true)
    {
        fm.all('ExtendID').style.display = "";
        fm.all('AssistSaleChnl').value ="";
        fm.all('AssistSaleChnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value ="";
    }
    if(fm.ExtendFlag.checked == false)
    {
        fm.all('AssistSaleChnl').value ="";
        fm.all('AssistSaleChnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value ="";
        fm.all('ExtendID').style.display = "none";
    }
}
//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryAssistAgent()
{
    if(fm.all('AssistSaleChnl').value == "" || fm.all('AssistSaleChnl').value == null)
    {
    	alert("请先选择协助销售渠道！");
    	return false;
    }
    if(fm.all('AssistAgentCode').value == "" )
    {  
        var tAssistSaleChnl = (fm.AssistSaleChnl != null && fm.AssistSaleChnl != "undefined") ? fm.AssistSaleChnl.value : "";
        var strURL = "../sys/AssistAgentCommonQueryMain.jsp?AssistSaleChnl=" + tAssistSaleChnl;        
        //alert(strURL);
        var newWindow = window.open(strURL, "AssistAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('AssistAgentCode').value != "")
    {	
        var strGrpSql = "select agentcode,name from LAAgent where agentcode ='" + fm.all('AssistAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery6(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.AssistAgentCode.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}
function afterQuery6(arrResult){
  if(arrResult!=null) {
    fm.AssistAgentCode.value = arrResult[0][0];
    fm.AssistAgentName.value = arrResult[0][1];
  }
}
function ExtendCheck()
{    
    if(fm.ExtendFlag.checked == true)
    {
    	if(fm.AssistSaleChnl.value == "" || fm.AssistSaleChnl.value == null)
    	{
    		alert("选择综合开拓时，协助销售渠道不能为空，请核查！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.AssistAgentCode.value == "" || fm.AssistAgentCode.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员代码不能为空，请核查！");
    		fm.all('AssistAgentCode').focus();
    		return false;
    	}
    	if(fm.AssistAgentName.value == "" || fm.AssistAgentName.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员姓名不能为空，请核查！");
    		fm.all('AssistAgentName').focus();
    		return false;
    	}
    	var tSQL = "select code1,codename from ldcode1 where codetype = 'salechnl' and code = '"+fm.AssistSaleChnl.value+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("销售渠道与业务员代码不匹配！");
			return false;
		}
		var tSQL1 = "select 1 from laagent where agentcode = '"+fm.AssistAgentCode.value+"' and BranchType = '"+arrResult[0][0]+"' and BranchType2 = '"+arrResult[0][1]+"'";
		var arrResult1 = easyExecSql(tSQL1);
		if(!arrResult1){
			alert("销售渠道与业务员代码不匹配，请核查！");
			return false;
		}
      	return true;
    }else{
    	return true;
    }
}
function initExtend(){
	var tSql = "select AssistSalechnl,AssistAgentCode from LCExtend where prtno='" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.ExtendFlag.checked = true;
			fm.all('ExtendID').style.display = "";
			fm.AssistSaleChnl.value = arrResult[0][0];
			fm.AssistAgentCode.value = arrResult[0][1];
			var  tSql1 = "select name from LAAgent where agentcode ='" + arrResult[0][1] + "'";
			var arrResult1 = easyExecSql(tSql1);
			fm.AssistAgentName.value = arrResult1[0][0];
		}
	}
}

function checkSaleChnlInfo(){
	var tManageCom = fm.ManageCom.value;
	var tAgentCom = fm.AgentCom.value;
	var tAgentCode = fm.AgentCode.value;
	var tSaleChnl = fm.SaleChnl.value;
	
	var tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
	var arrCode = easyExecSql(tSQLCode);
	if(!arrCode){
		alert("业务员与管理机构不匹配！");
		return false;
	}
	var agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
                 	+ " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
                    + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
                    + " union all "
					+ " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					+ " and '03' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  "
					+ " union all "
					+ " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					+ " and '10' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
    var arrAgentCode = easyExecSql(agentCodeSql);
    if(!arrAgentCode){
    	alert("业务员和销售渠道不匹配！");
		return false;
    }
	if(tSaleChnl == '03' || tSaleChnl == '04' || tSaleChnl == '10' || tSaleChnl == '15' || tSaleChnl == '20'){
		if(tAgentCom == null || tAgentCom == '' || tAgentCom == 'null'){
			alert("当销售渠道为中介时请填写中介机构代码。");
			return false;
		}
		var tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
		var arrCom = easyExecSql(tSQLCom);
		if(!arrCom){
			alert("中介机构与管理机构不匹配！");
			return false;
		}
		var tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
		var arrCom = easyExecSql(tSQLComCode);
		if(!arrCom){
			alert("业务员与中介机构不匹配！");
			return false;
		}
	}
	return true;
}
//双击对方业务员框要显示的页面
function otherSalesInfo(){
	//alert("双击对方业务员代码");
	window.open("../sys/MixedSalesAgentMain.jsp?ManageCom="+fm.ManageCom.value);
}
//对方业务员页面查询完“返回”按钮，接收数据
function afterQueryMIX(arrResult){
	if(arrResult!=null){
		var arr = arrResult.split("#");
		fm.GrpAgentCom.value = arr[0]; //对方机构代码
		//fm.GrpAgentComName.value = arr[1]; //对方机构名称
		fm.GrpAgentCode.value = arr[2]; //对方业务员代码
		fm.GrpAgentName.value = arr[3]; //对方业务员姓名
		fm.GrpAgentIDNo.value = arr[4]; //对方业务员身份证
		
	}else{
		alert("返回过程出现异常，请重新操作");
	}	
}

//省，市，县详细地址填完输入框失去焦点的时候自动在上面的“投保人地址（联系地址）”填充数据
function FillAddress(){
	var province = fm.Province.value;
	var city = fm.City.value;
	var county = fm.County.value;
	var detailAddress = fm.DetailAddress.value;
	if (fm.CityID.value == "000000") {
		fm.LinkManAddress1.value = province + detailAddress;
	}else if(fm.CountyID.value == "000000") {
		fm.LinkManAddress1.value = province + city + detailAddress;
	}else{
		fm.LinkManAddress1.value = province + city + county + detailAddress;
	}
}

//保存时校验联系人地址
function CheckAddress(){
	//校验判断联系人地址是否为空
 		if (fm.Province.value=="" || fm.City.value=="" || fm.County.value=="" || fm.DetailAddress.value=="") {
 			alert("团体基本资料中的省，市，县和详细地址不能为空，请核实！");
 			return false;
 		}
 		
 		//校验省和市是否与匹配
 		var provinceID = fm.ProvinceID.value;
 		var cityID = fm.CityID.value;
 		var countyID = fm.CountyID.value;
 		var strSql = "select code from ldcode1 where codetype='province1' and code = '" + provinceID + "'";
 		var strSql2 = "select code1 from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
 		var strSql3 = "select code from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
 		var strSql4 = "select code1 from ldcode1 where codetype='county1' and code = '" + countyID  + "'";
 		var arrResult = easyExecSql(strSql);
 		var arrResult2 = easyExecSql(strSql2);
 		var arrResult3 = easyExecSql(strSql3);
 		var arrResult4 = easyExecSql(strSql4);
 		
 		if (provinceID=="710000" || provinceID=="810000" || provinceID=="820000"||provinceID=="990000") {
 			if(cityID != "000000"){
 	 			alert("省和市不匹配，请核实！");
 	 			return false;
 			}
 			if(countyID != "000000"){
 				alert("市和县不匹配，请核实！");
 	 			return false;
 			}

 		}else if ((provinceID=="620000"&&cityID=="620200") || (provinceID=="440000"&&(cityID=="442000"||cityID=="441900")) ) {
			if(countyID != "000000"){
				alert("市和县不匹配，请核实！");
	 			return false;
			}
		}else{
 	 		if (arrResult[0][0] != arrResult2[0][0]) {
 	 			alert("省和市不匹配，请核实！");
 	 			return false;
 	 		}
 	 		if (arrResult3[0][0] != arrResult4[0][0]) {
 	 			alert("市和县不匹配，请核实！");
 	 			return false;
 	 		}
 		}
 		return true;

}

//对页面录入的联系电话进行校验
function checkPhone(pho){
	var str = "";
	if(pho!=null && pho!=""){
		if(pho.charAt(0)=="1"){
			str=CheckPhone(pho);
		}else{
			str=CheckFixPhone(pho);
		}
		if(str!=""){
			str="联系电话(移动电话)若为手机号，则必须为11位数字；若为固定电话，则仅允许包含数字、括号和“-”！";
			alert(str);
			return false;
		}else{
			return true;
		}
	}
	return true;
}
