<%--
    保存简易保单信息 2005-09-05 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%	         
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String mLoadFlag="";
  //合同信息
  VData tVData = new VData();
  TransferData tTransferData = new TransferData();
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  tAction = request.getParameter( "fmAction" );
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();      //集体保单
  LCGrpAppntSchema tLCGrpAppntSchema = new LCGrpAppntSchema();   //团单投保人
  LDGrpSchema tLDGrpSchema   = new LDGrpSchema();                //团体客户
  LCGrpAddressSchema tLCGrpAddressSchema = new LCGrpAddressSchema(); //团体客户地址
  LCNationSet tLCNationSet = new LCNationSet();
  LCCustomerImpartSchema tLCCustomerImpartSchema = new LCCustomerImpartSchema();
  
  //国家代码
  String[] tNationNo      =  request.getParameterValues("NationGrid1");
	String[] tNationName	    =  request.getParameterValues("NationGrid2");
  
  //从页面取值
  //合同信息取全集
  tLCGrpContSchema.setProposalGrpContNo(request.getParameter("ProposalGrpContNo"));  //集体投保单号码
	tLCGrpContSchema.setGrpContNo(request.getParameter("GrpContNo"));
	tLCGrpContSchema.setPrtNo(request.getParameter("PrtNo"));                  //印刷号码
	tLCGrpContSchema.setManageCom(request.getParameter("ManageCom"));          //管理机构
	tLCGrpContSchema.setSaleChnl(request.getParameter("SaleChnl"));            //销售渠道
	tLCGrpContSchema.setSaleChnlDetail("01"); 
	tLCGrpContSchema.setMarketType("1");                                       //默认市场类型为商业
	tLCGrpContSchema.setAgentCom(request.getParameter("AgentCom"));            //代理机构
	tLCGrpContSchema.setAgentCode(request.getParameter("AgentCode"));          //代理人编码
	tLCGrpContSchema.setAgentGroup(request.getParameter("AgentGroup"));        //代理人组别
	tLCGrpContSchema.setAgentSaleCode(request.getParameter("AgentSaleCode"));  //代理销售人员编码
	tLCGrpContSchema.setAppntNo(request.getParameter("GrpNo"));                //客户号码
	tLCGrpContSchema.setPayMode(request.getParameter("PayMode"));                // 缴费方式
	tLCGrpContSchema.setAddressNo(request.getParameter("GrpAddressNo"));       //地址号码
	tLCGrpContSchema.setGrpName(request.getParameter("GrpName"));              //单位名称
	tLCGrpContSchema.setGetFlag(request.getParameter("GetFlag"));              //付款方式
	tLCGrpContSchema.setPayIntv(request.getParameter("GrpContPayIntv")); 	    
	tLCGrpContSchema.setBankCode(request.getParameter("BankCode"));            //银行编码
	tLCGrpContSchema.setBankAccNo(request.getParameter("BankAccNo"));          //银行帐号
	tLCGrpContSchema.setAccName(request.getParameter("AccName"));            //银行帐号	    
	tLCGrpContSchema.setPolApplyDate(PubFun.getCurrentDate());    			 //保单投保日期
	System.out.println(PubFun.getCurrentDate()+"PubFun.getCurrentDate()");
	tLCGrpContSchema.setReceiveDate(request.getParameter("ReceiveDate"));    //收件日期
	tLCGrpContSchema.setReceiveOperator(request.getParameter("ReceiveOperator"));    //收件人
    tLCGrpContSchema.setRemark(request.getParameter("Remark"));			//备注
	tLCGrpContSchema.setHandlerPrint(request.getParameter("HandlerPrint"));//投保单位章
	tLCGrpContSchema.setHandlerDate(request.getParameter("HandlerDate"));//投保单填写日期
	tLCGrpContSchema.setCardFlag("0");//卡单标记
	tLCGrpContSchema.setDegreeType(request.getParameter("DegreeType"));//旅游类型，单次 多次
	tLCGrpContSchema.setCValiDate(request.getParameter("CValiDate"));//生效日期
	tLCGrpContSchema.setCInValiDate(request.getParameter("CInValiDate"));//失效日期
	
	tLCGrpContSchema.setAgentType(request.getParameter("AgentType"));
	tLCGrpContSchema.setAgentCode1(request.getParameter("AgentCode1"));
	tLCGrpContSchema.setPassword(request.getParameter("Password"));
	tLCGrpContSchema.setPassword2(request.getParameter("Password2"));
	tLCGrpContSchema.setBusinessType(request.getParameter("BusinessType"));
	tLCGrpContSchema.setGrpNature(request.getParameter("GrpNature"));
	tLCGrpContSchema.setRgtMoney(request.getParameter("RgtMoney"));
	tLCGrpContSchema.setAsset(request.getParameter("Asset"));
	tLCGrpContSchema.setNetProfitRate(request.getParameter("NetProfitRate"));
	tLCGrpContSchema.setMainBussiness(request.getParameter("MainBussiness"));
	tLCGrpContSchema.setCorporation(request.getParameter("Corporation"));
	tLCGrpContSchema.setComAera(request.getParameter("ComAera"));
	tLCGrpContSchema.setFax(request.getParameter("Fax"));
	tLCGrpContSchema.setPhone(request.getParameter("Phone"));
	tLCGrpContSchema.setSatrap(request.getParameter("Satrap"));
	tLCGrpContSchema.setEMail(request.getParameter("EMail"));
	tLCGrpContSchema.setFoundDate(request.getParameter("FoundDate"));
	tLCGrpContSchema.setGrpGroupNo(request.getParameter("GrpGroupNo"));
	tLCGrpContSchema.setDisputedFlag(request.getParameter("DisputedFlag"));
	tLCGrpContSchema.setOutPayFlag(request.getParameter("OutPayFlag"));
	tLCGrpContSchema.setGetPolMode(request.getParameter("GetPolMode"));
	tLCGrpContSchema.setLang(request.getParameter("Lang"));
	tLCGrpContSchema.setCurrency(request.getParameter("Currency"));
	tLCGrpContSchema.setLostTimes(request.getParameter("LostTimes"));
	tLCGrpContSchema.setPrintCount(request.getParameter("PrintCount"));
	tLCGrpContSchema.setRegetDate(request.getParameter("RegetDate"));
	tLCGrpContSchema.setLastEdorDate(request.getParameter("LastEdorDate"));
	tLCGrpContSchema.setLastGetDate(request.getParameter("LastGetDate"));
	tLCGrpContSchema.setLastLoanDate(request.getParameter("LastLoanDate"));
	tLCGrpContSchema.setSpecFlag(request.getParameter("SpecFlag"));
	tLCGrpContSchema.setGrpSpec(request.getParameter("GrpSpec"));
	tLCGrpContSchema.setSignCom(request.getParameter("SignCom"));
	tLCGrpContSchema.setSignDate(request.getParameter("SignDate"));
	tLCGrpContSchema.setSignTime(request.getParameter("SignTime"));
	tLCGrpContSchema.setManageFeeRate(request.getParameter("ManageFeeRate"));
	tLCGrpContSchema.setExpPeoples(request.getParameter("ExpPeoples"));
	tLCGrpContSchema.setExpPremium(request.getParameter("ExpPremium"));
	tLCGrpContSchema.setExpAmnt(request.getParameter("ExpAmnt"));
	tLCGrpContSchema.setPeoples(request.getParameter("Peoples"));
	tLCGrpContSchema.setMult(request.getParameter("Mult"));
	tLCGrpContSchema.setPrem(request.getParameter("Prem"));
	tLCGrpContSchema.setAmnt(request.getParameter("Amnt"));
	tLCGrpContSchema.setSumPrem(request.getParameter("SumPrem"));
	tLCGrpContSchema.setSumPay(request.getParameter("SumPay"));
	tLCGrpContSchema.setDif(request.getParameter("Dif"));
	tLCGrpContSchema.setStandbyFlag2(request.getParameter("StandbyFlag2"));
	tLCGrpContSchema.setStandbyFlag3(request.getParameter("StandbyFlag3"));
	tLCGrpContSchema.setInputOperator(request.getParameter("InputOperator"));
	tLCGrpContSchema.setInputDate(request.getParameter("InputDate"));
	tLCGrpContSchema.setInputTime(request.getParameter("InputTime"));
	tLCGrpContSchema.setApproveFlag(request.getParameter("ApproveFlag"));
	tLCGrpContSchema.setApproveCode(request.getParameter("ApproveCode"));
	tLCGrpContSchema.setApproveDate(request.getParameter("ApproveDate"));
	tLCGrpContSchema.setApproveTime(request.getParameter("ApproveTime"));
	tLCGrpContSchema.setUWOperator(request.getParameter("UWOperator"));
	tLCGrpContSchema.setUWFlag(request.getParameter("UWFlag"));
	tLCGrpContSchema.setUWDate(request.getParameter("UWDate"));
	tLCGrpContSchema.setUWTime(request.getParameter("UWTime"));
	tLCGrpContSchema.setAppFlag(request.getParameter("AppFlag"));
	tLCGrpContSchema.setCustomGetPolDate(request.getParameter("CustomGetPolDate"));
	tLCGrpContSchema.setGetPolDate(request.getParameter("GetPolDate"));
	tLCGrpContSchema.setGetPolTime(request.getParameter("GetPolTime"));
	tLCGrpContSchema.setState(request.getParameter("State"));
	tLCGrpContSchema.setOperator(request.getParameter("Operator"));
	tLCGrpContSchema.setMakeDate(request.getParameter("MakeDate"));
	tLCGrpContSchema.setMakeTime(request.getParameter("MakeTime"));
	tLCGrpContSchema.setModifyDate(request.getParameter("ModifyDate"));
	tLCGrpContSchema.setModifyTime(request.getParameter("ModifyTime"));
	tLCGrpContSchema.setEnterKind(request.getParameter("EnterKind"));
	tLCGrpContSchema.setAmntGrade(request.getParameter("AmntGrade"));
	tLCGrpContSchema.setPeoples3(request.getParameter("Peoples3"));
	tLCGrpContSchema.setOnWorkPeoples(request.getParameter("OnWorkPeoples"));
	tLCGrpContSchema.setOffWorkPeoples(request.getParameter("OffWorkPeoples"));
	tLCGrpContSchema.setOtherPeoples(request.getParameter("OtherPeoples"));
	tLCGrpContSchema.setRelaPeoples(request.getParameter("RelaPeoples"));
	tLCGrpContSchema.setRelaMatePeoples(request.getParameter("RelaMatePeoples"));
	tLCGrpContSchema.setRelaYoungPeoples(request.getParameter("RelaYoungPeoples"));
	tLCGrpContSchema.setRelaOtherPeoples(request.getParameter("RelaOtherPeoples"));
	tLCGrpContSchema.setFirstTrialOperator(request.getParameter("ReceiveOperator"));    //初审人
	tLCGrpContSchema.setFirstTrialDate(request.getParameter("FirstTrialDate"));
	tLCGrpContSchema.setFirstTrialTime(request.getParameter("FirstTrialTime"));
	tLCGrpContSchema.setReceiveTime(request.getParameter("ReceiveTime"));
	tLCGrpContSchema.setTempFeeNo(request.getParameter("TempFeeNo"));
	tLCGrpContSchema.setHandlerName(request.getParameter("HandlerName"));
	tLCGrpContSchema.setAgentDate(request.getParameter("AgentDate"));
	tLCGrpContSchema.setBusinessBigType(request.getParameter("BusinessBigType"));
	tLCGrpContSchema.setProposalType(request.getParameter("ProposalType"));
	tLCGrpContSchema.setContPrintLoFlag(request.getParameter("ContPrintLoFlag"));
	tLCGrpContSchema.setPremApportFlag(request.getParameter("PremApportFlag"));
	tLCGrpContSchema.setContPremFeeNo(request.getParameter("ContPremFeeNo"));
	tLCGrpContSchema.setCustomerReceiptNo(request.getParameter("CustomerReceiptNo"));
	tLCGrpContSchema.setRoleAgentCode(request.getParameter("RoleAgentCode"));
	tLCGrpContSchema.setAskGrpContNo(request.getParameter("AskGrpContNo"));
	tLCGrpContSchema.setCopys(request.getParameter("Copys"));
	tLCGrpContSchema.setOperationManager(request.getParameter("OperationManager"));
	tLCGrpContSchema.setInfoSource(request.getParameter("InfoSource"));
	tLCGrpContSchema.setPremScope(request.getParameter("PremScope"));
	tLCGrpContSchema.setPeoples2(request.getParameter("Peoples2"));
    
    // 集团交叉要素
    tLCGrpContSchema.setCrs_SaleChnl(request.getParameter("Crs_SaleChnl"));
    tLCGrpContSchema.setCrs_BussType(request.getParameter("Crs_BussType"));
    
    tLCGrpContSchema.setGrpAgentCom(request.getParameter("GrpAgentCom"));
    tLCGrpContSchema.setGrpAgentCode(request.getParameter("GrpAgentCode"));
    tLCGrpContSchema.setGrpAgentName(request.getParameter("GrpAgentName"));
    tLCGrpContSchema.setGrpAgentIDNo(request.getParameter("GrpAgentIDNo"));
    // --------------------
    
	//投保人信息
	tLCGrpAppntSchema.setGrpContNo(request.getParameter("GrpContNo"));     //集体保单号码
	tLCGrpAppntSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	tLCGrpAppntSchema.setPrtNo(request.getParameter("PrtNo"));                 //印刷号码
	tLCGrpAppntSchema.setName(request.getParameter("GrpName"));
	tLCGrpAppntSchema.setPostalAddress(request.getParameter("GrpAddress"));
	tLCGrpAppntSchema.setZipCode(request.getParameter("GrpZipCode"));
	tLCGrpAppntSchema.setAddressNo(request.getParameter("GrpAddressNo"));
	tLCGrpAppntSchema.setPhone(request.getParameter("Phone"));
	tLCGrpAppntSchema.setTaxpayerType(request.getParameter("TaxpayerType"));
	tLCGrpAppntSchema.setCustomerBankCode(request.getParameter("CustomerBankCode"));
	tLCGrpAppntSchema.setCustomerBankAccNo(request.getParameter("CustomerBankAccNo"));
	tLCGrpAppntSchema.setTaxNo(request.getParameter("TaxNo"));
	tLCGrpAppntSchema.setOrganComCode(request.getParameter("OrgancomCode"));
	tLCGrpAppntSchema.setUnifiedSocialCreditNo(request.getParameter("UnifiedSocialCreditNo"));
	//#4117 客户授权标识
	tLCGrpAppntSchema.setAuthorization(request.getParameter("auth"));

	//团体客户信息  LDGrp
	tLDGrpSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	tLDGrpSchema.setGrpName(request.getParameter("GrpName"));             //单位名称
	tLDGrpSchema.setGrpNature(request.getParameter("GrpNature"));         //单位性质
	tLDGrpSchema.setBusinessType(request.getParameter("BusinessType"));   //行业类别
	tLDGrpSchema.setPeoples(request.getParameter("Peoples"));             //总人数
	tLDGrpSchema.setOnWorkPeoples(request.getParameter("AppntOnWorkPeoples"));             //总人数
	tLDGrpSchema.setOffWorkPeoples(request.getParameter("AppntOffWorkPeoples"));             //总人数
	tLDGrpSchema.setOtherPeoples(request.getParameter("AppntOtherPeoples"));             //总人数	    	    	    
	tLDGrpSchema.setRgtMoney(request.getParameter("RgtMoney"));           //注册资本
	tLDGrpSchema.setAsset(request.getParameter("Asset"));                 //资产总额
	tLDGrpSchema.setNetProfitRate(request.getParameter("NetProfitRate")); //净资产收益率
	tLDGrpSchema.setMainBussiness(request.getParameter("MainBussiness")); //主营业务
	tLDGrpSchema.setCorporation(request.getParameter("Corporation"));     //法人
	tLDGrpSchema.setComAera(request.getParameter("ComAera"));             //机构分布区域
	tLDGrpSchema.setPhone(request.getParameter("Phone"));             //总机
	tLDGrpSchema.setFax(request.getParameter("Fax"));             //传真
	tLDGrpSchema.setFoundDate(request.getParameter("FoundDate"));             //成立时间
	tLDGrpSchema.setGrpEnglishName(request.getParameter("GrpEnglishName"));             //成立时间
	tLDGrpSchema.setTaxpayerType(request.getParameter("TaxpayerType"));
	tLDGrpSchema.setTaxNo(request.getParameter("TaxNo"));
	tLDGrpSchema.setCustomerBankCode(request.getParameter("CustomerBankCode"));
	tLDGrpSchema.setCustomerBankAccNo(request.getParameter("CustomerBankAccNo"));
	tLDGrpSchema.setOrganComCode(request.getParameter("OrgancomCode"));
	tLDGrpSchema.setUnifiedSocialCreditNo(request.getParameter("UnifiedSocialCreditNo"));

	//团体客户地址  LCGrpAddress	    
	tLCGrpAddressSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	tLCGrpAddressSchema.setAddressNo(request.getParameter("GrpAddressNo"));      //地址号码
	tLCGrpAddressSchema.setGrpAddress(request.getParameter("GrpAddress"));       //单位地址
	tLCGrpAddressSchema.setGrpZipCode(request.getParameter("GrpZipCode"));       //单位邮编
	//保险联系人一
	tLCGrpAddressSchema.setLinkMan1(request.getParameter("LinkMan1"));
	tLCGrpAddressSchema.setDepartment1(request.getParameter("Department1"));
	tLCGrpAddressSchema.setHeadShip1(request.getParameter("HeadShip1"));
	tLCGrpAddressSchema.setPhone1(request.getParameter("Phone1"));
	tLCGrpAddressSchema.setE_Mail1(request.getParameter("E_Mail1"));
	tLCGrpAddressSchema.setMobile1(request.getParameter("Mobile1"));
	tLCGrpAddressSchema.setFax1(request.getParameter("Fax1"));
	tLCGrpAddressSchema.setLinkManZipCode1(request.getParameter("LinkManZipCode1"));
	tLCGrpAddressSchema.setLinkManAddress1(request.getParameter("LinkManAddress1"));
	tLCGrpAddressSchema.setPostalProvince(request.getParameter("ProvinceID"));	 //省id
    tLCGrpAddressSchema.setPostalCity(request.getParameter("CityID"));			 //市id
	tLCGrpAddressSchema.setDetailAddress(request.getParameter("DetailAddress")); //详细地址
    tLCGrpAddressSchema.setPostalCounty(request.getParameter("CountyID"));			 //县id
	tLCGrpAddressSchema.setLinkManEnglishName1(request.getParameter("LinkManEnglishName1"));
	//地址编码
	int MulCount = 0;
	if(tNationNo != null ) MulCount = tNationNo.length;
	for(int n=0 ; n < MulCount ; n++){
		LCNationSchema tLCNationSchema = new LCNationSchema();
		tLCNationSchema.setGrpContNo(request.getParameter("GrpContNo"));
		tLCNationSchema.setNationNo(tNationNo[n]);
		tLCNationSchema.setContNo("00000000000000000000");
		tLCNationSchema.setNationNo(tNationNo[n]);
		tLCNationSchema.setChineseName(tNationName[n]);
		tLCNationSet.add(tLCNationSchema);
	}
	//客户告知
	tLCCustomerImpartSchema.setGrpContNo(request.getParameter("GrpContNo"));
	tLCCustomerImpartSchema.setProposalContNo("00000000000000000000");			
	tLCCustomerImpartSchema.setContNo("00000000000000000000");
	tLCCustomerImpartSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCCustomerImpartSchema.setCustomerNo(tLDGrpSchema.getCustomerNo());
	tLCCustomerImpartSchema.setCustomerNoType("0");
	tLCCustomerImpartSchema.setImpartCode("001");
	tLCCustomerImpartSchema.setImpartContent("□商务　□旅行　□其它");
	tLCCustomerImpartSchema.setImpartParamModle(request.getParameter("IntentionName"));
	tLCCustomerImpartSchema.setImpartVer("013") ;
	
	//合同维护需要的判断附
	tTransferData.setNameAndValue("LoadFlag",request.getParameter("LoadFlag"));
	
  //by gzh 20130408 增加对综合开拓数据的处理
  	String tAssistSaleChnl = request.getParameter("AssistSaleChnl");
  	String tAssistAgentCode = request.getParameter("AssistAgentCode");
	LCExtendSchema tLCExtendSchema = new LCExtendSchema();
	tLCExtendSchema.setPrtNo(request.getParameter("PrtNo"));
	tLCExtendSchema.setAssistSalechnl(tAssistSaleChnl);
	tLCExtendSchema.setAssistAgentCode(tAssistAgentCode);
	tVData.add(tLCExtendSchema);
  
  //by gzh 20130408 增加对综合开拓数据的处理 end
  
  // 准备传输数据 VData
  tVData.add( tLCGrpContSchema );
  tVData.add( tLCCustomerImpartSchema );
  tVData.add( tLCGrpAppntSchema );
  tVData.add( tLDGrpSchema );
  tVData.add( tLCGrpAddressSchema );
  tVData.add( tLCNationSet );
  tVData.add( tTransferData );
  tVData.add( tG );
  
  tOperate = tAction;
	  
	BriefGroupContInputUI tBriefGroupContInputUI = new BriefGroupContInputUI();
	System.out.println("操作符 "+tOperate);
	if( tBriefGroupContInputUI.submitData( tVData, tOperate ) == false )
	{
		Content = " 保存失败，原因是: " + tBriefGroupContInputUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}
	else
	{
		Content = " 保存成功! ";
		FlagStr = "Succ";

		tVData.clear();
		tVData = tBriefGroupContInputUI.getResult();
		
		if(( tAction.equals( "INSERT" ))||( tAction.equals( "UPDATE" )))
		{
			// 保单信息
			LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); 
			mLCGrpContSchema.setSchema(( LCGrpContSchema )tVData.getObjectByObjectName( "LCGrpContSchema", 0 ));
			%>
			   <script language="javascript">
			     parent.fraInterface.fm.all("ProposalGrpContNo").value = "<%=mLCGrpContSchema.getProposalGrpContNo()%>";
			     parent.fraInterface.fm.all("GrpContNo").value = "<%=mLCGrpContSchema.getGrpContNo()%>";
			     parent.fraInterface.fm.all("GrpNo").value = "<%=mLCGrpContSchema.getAppntNo()%>";
			     parent.fraInterface.fm.all("GrpAddressNo").value = "<%=mLCGrpContSchema.getAddressNo()%>"; 
			   </script>
			<%		
		}
		else
		{
			%>
			   <script language="javascript">
			     //parent.fraInterface.emptyFormElements();
			   </script>
			<%
		}
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>