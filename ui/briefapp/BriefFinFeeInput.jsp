<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<html>
	<script>
		var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BriefFinFeeInit.jsp"%>
  <SCRIPT src="BriefFinFeeInput.js"></SCRIPT>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="" method=post name=fm target="fraSubmit">
		<table>
      <tr>
      <td>
        <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpmulti);">
      </td>
      <td class= titleImg>
        团体境外救援应收清单
      </td>
     </tr>
    </table>
    <Div  id= "divGrpmulti" style= "display: ''">
      <Table  class= common>
        <tr>
		  		<TD  class= title>
		  			管理机构
		  		</TD>
          <TD  class= input>
          	<Input class="code" name=ManageCom ondblclick="return showCodeList('comcode',[this]);" onkeyup="return showCodeListKey('comcode',[this]);">
          </TD>
          <TD  class= title>
		  			代理机构
		  		</TD>
          <TD  class= input>
          	<Input class="codeNo" name=AgentCom ondblclick="return showCodeList('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');"><Input class="codename" name=AgentComName readonly >
          </TD>
		  		<TD  class= title>
          	开始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=StartDate >
          </TD>
          <TD  class= title>
          	结束日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EndDate >
          </TD>
		    </tr>
      </Table>
      <Table align="right">
      	<INPUT VALUE="查询可催收记录" class = cssbutton TYPE=button onclick="queryContInfo();">
      	<INPUT VALUE=" 生成应收清单 " class = cssbutton TYPE=button onclick="creatFin();">
      </Table>
    </Div>
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 团体保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpContGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
    	<div align=left>
    		总保费为<input type='common' style='{border: 1px #9999CC solid;height: 18px}' name="SumPrem" size='8'>
    	</div>
      <center>
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </center>
  	</div>
  	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol2);">
    		</td>
    		<td class= titleImg>
    			 应收批次记录
    		</td>
    	</tr>
    </table>
        <Div  id= "divGrpmulti" style= "display: ''">
      <Table  class= common>
        <tr>
		  		<TD  class= title>
		  			管理机构
		  		</TD>
          <TD  class= input>
          	<Input class="code" name=ManageCom_Fin ondblclick="return showCodeList('comcode',[this]);" onkeyup="return showCodeListKey('comcode',[this]);">
          </TD>
          <TD  class= title>
		  			代理机构
		  		</TD>
          <TD  class= input>
          	<Input class="codeNo" name=AgentCom_Fin ondblclick="return showCodeList('AgentCom',[this,AgentCom_FinName],[0,1],null, fm.all('ManageCom_Fin').value, 'ManageCom');" onkeyup="return showCodeListKey('AgentCom',[this,AgentCom_FinName],[0,1],null, fm.all('ManageCom_Fin').value, 'ManageCom');"><Input class="codename" name=AgentCom_FinName readonly >
          </TD>
		  		<TD  class= title>
          	批次号
          </TD>
          <TD  class= input>
            <Input class="common" name=BatchNo_Fin >
          </TD>
		    </tr>
		    <tr>
		    	<TD  class= title>
          	核销状态
          </TD>
          <TD  class= input>
            <Input class="codeNo"  name=FinishFlag value=0 CodeData="0|^0|未核销^1|已核销" ondblclick="return showCodeListEx('FinishFlag',[this,FinishFlagName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('FinishFlag',[this,FinishFlagName],[0,1],null,null,null,1);"><input class=codename  name=FinishFlagName readonly=true > 
          </TD>
          <TD  class= title>
          	打印状态
          </TD>
          <TD  class= input>
            <Input class="codeNo" dateFormat="short" name=StateFlag CodeData="0|^0|未打印^1|已打印" ondblclick="return showCodeListEx('StateFlag',[this,StateFlagName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('StateFlag',[this,StateFlagName],[0,1],null,null,null,1);"><input class=codename  name=StateFlagName readonly=true > 
          </TD>
          <TD  class= title>
          	财务到帐日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=EnterAccDate >
          </TD>
		    </tr>
      </Table>
      <div align="right">
      	<INPUT VALUE="查询应收清单" class = cssbutton TYPE=button onclick="queryFinInfo();">
      	<INPUT VALUE="  打印清单  " class = cssbutton TYPE=button onclick="printCont();">
      	<INPUT VALUE="  财务核销  " class = cssbutton TYPE=button onclick="inputConfirm();">
      </div>
    </Div>
  	<Div  id= "divLCPol2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanFinGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <center>
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();">
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
    </center>
  	</div>
	</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>