<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BriefSingleContSignSave.jsp
//程序功能：
//创建日期：2005-11-21 14:33
//创建人  Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";
String Priview = "PREVIEW";

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
TransferData tTransferData = new TransferData();

//接收信息
// 投保单列表
LCContSchema tLCContSchema = new LCContSchema();

String szTemplatePath = application.getRealPath("f1print/template/") + "/";	//模板路径
String sOutXmlPath = application.getRealPath("");	//xml文件输出路径

String tContNo[] = request.getParameterValues("GrpGrid1");
String tRadio[] = request.getParameterValues("InpGrpGridSel");
String tMissionID[] = request.getParameterValues("GrpGrid7");
String tSubMissionID[] = request.getParameterValues("GrpGrid8");
String workType = request.getParameter("workType");

boolean flag = false;
int grouppolCount = tContNo.length;
for (int i = 0; i < grouppolCount; i++)
{
  if( tContNo[i] != null && tRadio[i].equals( "1" ))
  {
    System.out.println("GrpContNo:"+i+":"+tContNo[i]);
    tLCContSchema.setContNo( tContNo[i] );
    tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
    tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );
    tTransferData.setNameAndValue("MissionID",tMissionID[i] );
    tTransferData.setNameAndValue("SubMissionID",tSubMissionID[i] );
    flag = true;
    break;
  }
}

if (flag == true)
{
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add( tLCContSchema );
  tVData.add( tTransferData );
  tVData.add( tG );

  // 数据传输
  //LCGrpContSignBL tLCGrpContSignBL   = new LCGrpContSignBL();

  //boolean bl = tLCGrpContSignBL.submitData( tVData, "INSERT" );
  //int n = tLCGrpContSignBL.mErrors.getErrorCount();
  BriefTbWorkFlowUI tBriefTbWorkFlowUI = new BriefTbWorkFlowUI();
  boolean bl= tBriefTbWorkFlowUI.submitData( tVData, "0000007002");
  int n = tBriefTbWorkFlowUI.mErrors.getErrorCount();
  if( n == 0 )
  {
    if ( bl )
    {
      Content = " 签单成功! ";
      FlagStr = "Succ";
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单成功! ";
      }
    }
    else
    {
      Content = "签单失败";
      FlagStr ="Fail" ;
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单失败! ";
      }
    }
  }
  else
  {

    String strErr = "";
    for (int i = 0; i < n; i++)
    {
      strErr += (i+1) + ": " + tBriefTbWorkFlowUI.mErrors.getError(i).errorMessage + "; ";
      System.out.println(tBriefTbWorkFlowUI.mErrors.getError(i).errorMessage );
    }
    if ( bl )
    {
      Content = " 部分签单成功,但是有如下信息: " +strErr;
      FlagStr = "Succ";
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单成功! ";
      }
    }
    else
    {
      Content = "集体投保单签单失败，原因是: " + strErr;
      FlagStr = "Fail";
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单失败! ";
      }
    }
  } // end of if
} // end of if

%>
<html>
<script language="javascript">
                 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
