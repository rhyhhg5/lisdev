<%
//程序名称：BriefContQueryQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-08-18 11:51:23
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initForm() {
  try {
  fm.all('ManageCom').value = manageCom;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
    initInpBox();
    initBriefContQueryGrid();  
  }
  catch(re) {
    alert("BriefContQueryQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var BriefContQueryGrid;
function initBriefContQueryGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="印刷号";         		//列名
    iArray[1][1]="80px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="保单号";         		//列名
    iArray[2][1]="80px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="管理机构";         		//列名
    iArray[3][1]="60px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="代理机构";         		//列名
    iArray[4][1]="50px";         		//列名
    iArray[4][3]=0;         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="代理人";         		//列名
    iArray[5][1]="70px";         		//列名
    iArray[5][3]=0;         		//列名
    
    iArray[6]=new Array();
    iArray[6][0]="投保人姓名";         		//列名
    iArray[6][1]="50px";         		//列名
    iArray[6][3]=0;         		//列名
    
    iArray[7]=new Array();
    iArray[7][0]="录入人员代码";         		//列名
    iArray[7][1]="40px";         		//列名
    iArray[7][3]=0;         		//列名
    
    iArray[8]=new Array();
    iArray[8][0]="保单申请日期";         		//列名
    iArray[8][1]="70px";         		//列名
    iArray[8][3]=0;         		//列名
    
    iArray[9]=new Array();
    iArray[9][0]="保单生效日期";         		//列名
    iArray[9][1]="70px";         		//列名
    iArray[9][3]=0;         		//列名
    
    iArray[10]=new Array();
    iArray[10][0]="银行编码";         		//列名
    iArray[10][1]="40px";         		//列名
    iArray[10][3]=0;         		//列名
    
    iArray[11]=new Array();
    iArray[11][0]="帐号";         		//列名
    iArray[11][1]="60px";         		//列名
    iArray[11][3]=0;         		//列名
    
    iArray[12]=new Array();
    iArray[12][0]="户名";         		//列名
    iArray[12][1]="50px";         		//列名
    iArray[12][3]=0;         		//列名
    
    iArray[13]=new Array();
    iArray[13][0]="保费";         		//列名
    iArray[13][1]="50px";         		//列名
    iArray[13][3]=0;         		//列名
    
    iArray[14]=new Array();
    iArray[14][0]="状态";         		//列名
    iArray[14][1]="50px";         		//列名
    iArray[14][3]=0;         		//列名
    
    iArray[15]=new Array();
    iArray[15][0]="销售渠道";                 //列名
    iArray[15][1]="40px";               //列名
    iArray[15][3]=0;                //列名

    iArray[16]=new Array();
    iArray[16][0]="网点";                 //列名
    iArray[16][1]="80px";               //列名
    iArray[16][3]=0;                //列名
    
    
    
    BriefContQueryGrid = new MulLineEnter( "fm" , "BriefContQueryGrid" ); 
    //这些属性必须在loadMulLine前

    BriefContQueryGrid.mulLineCount = 0;   
    BriefContQueryGrid.displayTitle = 1;
    BriefContQueryGrid.hiddenPlus = 1;
    BriefContQueryGrid.hiddenSubtraction = 1;
    BriefContQueryGrid.canSel = 0;
    BriefContQueryGrid.canChk = 0;
    //BriefContQueryGrid.selBoxEventFuncName = "showOne";
    BriefContQueryGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //BriefContQueryGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
