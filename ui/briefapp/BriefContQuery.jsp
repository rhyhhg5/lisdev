<%
//程序名称：BriefContQueryInput.jsp
//程序功能：功能描述
//创建日期：2006-08-18 11:51:23
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<%
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  String tCurrentDate = PubFun.getCurrentDate();
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var AgentCom = "<%=tG.AgentCom%>";
  var operator = "<%=tG.Operator%>";
  var Type = "<%=request.getParameter("Type")%>";
  var tCurrentDate = "<%=tCurrentDate%>";
</script>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="BriefContQuery.js"></SCRIPT> 
  <%@include file="BriefContQueryInit.jsp"%>
  
  <title></title>
</head>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divBriefContQueryGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD class= title>
      印刷号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PrtNo >
    </TD>
    <TD  class= title>
    管理机构
    </TD>
    <TD  class= input>
    <Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" readonly><input class=codename name=ManageComName readonly=true elementtype=nacessary >
    </TD>
    <TD  class= title>
      代理机构
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AgentCom >
    </TD>

  </TR>
  <TR  class= common>
    <TD  class= title>
      业务员
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AgentCode >
    </TD>
    <TD  class= title>
      投保人姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppntName >
    </TD>
    <TD  class= title>
      被保人姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InsuredName >
    </TD>
  </TR>
  <TR  class= common>
		<TD  class= title8 COLSPAN="1" >
	    银行编码
	  </TD>
	  <TD  class= input8 COLSPAN="1" >
	    <Input class=codeNo  name=BankCode  verify="开户银行|code:bank&len<=24" ondblclick="return showCodeList('Bank',[this,BankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName],[0,1],null,null,null,1);"><input class=codename name=BankCodeName >
	  </TD>
	 <TD  class= title8 >
	   账&nbsp;&nbsp;&nbsp;号
	 </TD>
	 <TD  class= input8 >
	   <Input class= common8 name=BankAccNo  verify="帐号|len<=40">
	 </TD>
	 <TD  class= title8 >
	   户&nbsp;&nbsp;&nbsp;名
	 </TD>
	 <TD  class= input8 >
	   <Input class= common8 name=AccName  verify="户名|len<=40">
	 </TD>
  </TR>
  <TR  class= common>
    <!--TD  class= title>
        保单申请日期
    </TD>
    <TD  class= input>
        <Input class="coolDatePicker"  dateFormat="short" name=InputDate verify="保单申请日期|date" >
    </TD-->
     <td class="title8">渠道</td>
     <td class="input8">
       <input class="codeNo" name="SaleChnlCode" ondblclick="return showCodeList('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('lcsalechnl',[this,SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="SaleChnlName" />
     </td>
     <td class="title8">网点</td>
     <td class="input8">
       <input id="AgentComBank" class="code" name="AgentComBank" style="display:none;" ondblclick="return queryAgentComBank();" />
     </td>
     <td class="title">保单状态</td>
     <td class="input">
       <input class="codeNo" name="state" CodeData="0|^0|未录单^1|录入中^5|待复核^2|待收费^3|待签单^4|已承保^6|已撤单" ondblclick="showCodeListEx('stateList',[this,stateName],[0,1]);" /><input class="codename" name="stateName" />
     </td>
  </TR>
  <tr>
    <td class= title>保单申请起期</td>
    <td class= input><Input class="coolDatePicker"  dateFormat="short" name=InputStartDate verify="保单申请日期|date" ></td>
    <td class= title>保单申请止期</td>
    <td class= input><Input class="coolDatePicker"  dateFormat="short" name=InputEndDate verify="保单申请日期|date" ></td>
  <tr>
	<TR class= common id="PrintStateTRID" style="display: ''">
		<TD class= title>个单类型</TD>
		<TD class= input colspan="5">
		  <Input class=codeNo name=CardFlag value="0" readonly CodeData="0|^0|其他保单^8|万能险" ondblclick="return showCodeListEx('ReCardFlag',[this,CardFlagName],[0,1]);" onkeyup="return showCodeListKey('ReCardFlag',[this,CardFlagName],[0,1]);"><input class=codename name=CardFlagName value="" readonly=true> 不选择即查询所有类型保单</TD>
	</TR>  
  
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
        <INPUT VALUE="清单下载" TYPE=button   class=cssbutton onclick="queryDown();">
        <input type="hidden" name="sql">
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBriefContQuery1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divBriefContQuery1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanBriefContQueryGrid">
  				</span> 
		    </td>
			</tr>
		</table>
		<!--<div align=left>
	  	总保费为<input type='common' style='{border: 1px #9999CC solid;height: 18px}' name="SumPrem" size='8'>元；
	  	总件数<input type='common' style='{border: 1px #9999CC solid;height: 18px}' name="Count" size='8'>件
	  </div>-->
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
