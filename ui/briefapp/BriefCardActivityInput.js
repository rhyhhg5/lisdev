//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if(fm.CardNo.value == "" || fm.CardNo.value == null || fm.CardNo.value == "null"){
		alert("没有录入卡单号码！");
		fm.CardNo.focus();
		return;
	}
	
	if(fm.Password.value == "" || fm.Password.value == null || fm.Password.value == "null"){
		alert("没有录入卡单密码！");
		fm.Password.focus();
		return;
	}
	if(fm.Password.Times >= 5){
		alert("此密码录入错误次数超过5次，不允许再次录入");
		return ;
	}
	var strQueryResult = "";
	var strURL = "BriefCardActiveSave.jsp?prtNo="+fm.CardNo.value +"&Password="+fm.Password.value;                                 
	try{
	  Request  =  new  ActiveXObject("Microsoft.XMLHTTP");
		Request.open("GET",  strURL,  false);
		Request.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		Request.send(null);
		try{
			//<jjjj></jjjj>
			strQueryResult = Request.responseText.trim().replace(/(<\D*>)/,"");
			strQueryResult = strQueryResult.replace(/(<\/\D*>)/,"");        
		}catch(e1){
		  alert("easyQueryVer3中 "+e1.message);
		}
	}catch(e){
		alert(e.message);
	}
	
	//var sFeatures = "status:no;help:0;close:0;dialogWidth:150px;dialogHeight:0px;resizable=1";
	//var strQueryResult = window.showModalDialog(strURL, "", sFeatures);  	 
	
	//此页面之所以作页面代码处理，原因是此页面以后会放入其他Web服务器作校验接口
	if(SUCC == strQueryResult){
		var urlStr = "./BriefSingleContInputMain.jsp?prtNo="+fm.CardNo.value+"&ManageCom="+manageCom+"&cContType="+cContType+"&AgentType="+AgentType+"&AgentCom="+AgentCom+"&LoadFlag="+LoadFlag;
		var sFeatures = "status:no;help:0;close:0;dialogWidth:1024px;dialogHeight:768px;resizable=1";
    //申请该印刷号
    //var strReturn = OpenWindowNew(urlStr, "简易投保", "left");
  var shouInfo = window.open(urlStr,"",sFeatures);
	}else if(CRC_ERROR == strQueryResult){
		alert("CRC校验错误，原因是卡号的非法");
	}else if(PASSWORD_ERROR == strQueryResult){
		alert("密码错误");
		if(fm.Password.CardNo==fm.CardNo.value){
			fm.Password.Times = fm.Password.Times+1;
		}else{
			fm.Password.CardNo = fm.CardNo.value;
			fm.Password.Times = 0;
		}
	}else if(NOT_RELEASE_ERROR == strQueryResult){
		alert("此保险卡还未下发给代理人");
	}else if(ACTIVITY_ERROR == strQueryResult){
		alert("卡单已激活");
	}else if(LOST_ERROR == strQueryResult){
		alert("此保险卡已挂失");
	}else if(EXCEPTION == strQueryResult){
		alert("异常错误");
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDDoctor.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  if(fm.all('HospitCode').value == "" ){fm.all('HospitCode').value = " ";}
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDoctorQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	if(fm.all('MakeDate').value == "")
	{
		alert("请先查询返回，再删除！");
		return false;
	}
	
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
