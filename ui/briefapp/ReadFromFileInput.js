//ReadFromFileInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var filePath;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm() {
 var BankCodeName = fm.BankCodeName.value;
 if(BankCodeName =="" || BankCodeName == null){
 		alert("请选择回盘的银行编码！");	
 	  return false;
 }
 var accsql = "select 1 from ldfinbank where bankcode ='" + fm.getbankcode.value + "' and bankaccno='" + fm.getbankaccno.value +"'";
 var arrBankResult = easyExecSql(accsql);
 if(arrBankResult==null){
     alert("归集账户不存在，请确认录入的归集账户");
     return false ;
 }
 if (!(fm.all('FileName').value==""||fm.all('FileName').value==null)){ 		 
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");       
    fm.action = "./ReadFromFileSave.jsp" + "?filePath=" + filePath    					
              + "&bankCode=" + fm.BankCode.value + "&getbankcode=" + fm.getbankcode.value + "&getbankaccno=" + fm.getbankaccno.value;             
    fm.submit(); //提交
 }else{
   	alert("请输入要读取的文件名称！");
 }
  
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr,content){
  try{ 
  	showInfo.close(); 
  }catch(e){  
  }  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

//获取文件上传保存路径
function getUploadPath() {
  var strSql = "select SysVarValue from LDSysVar where SysVar = 'ReturnFromBankPath'";
  filePath = easyExecSql(strSql);
}

 

//初始化银行代码
function initBankCode() {
  var strSql = "select BankCode, BankName from LDBank where ComCode = '" + comCode + "'";   
  var strResult = easyQueryVer3(strSql);
  if (strResult) {
     fm.all("bankCode").CodeData = strResult;
  }
}   
 
