<%
//程序名称：BriefGrpTravelAgentSave.jsp
//程序功能：境外救援应收清单
//创建日期：2006-8-14 16:54
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
境外救援应收清单
</title>
<head>
</head>
<body>
<%
String flag = "0";
String FlagStr = "";
String Content = "";
GlobalInput tG = (GlobalInput)session.getValue("GI");
System.out.println("=======start Jrp===========");
String tPrtSeq = request.getParameter("BatchNo_Fin");
String tFinTitle = request.getParameter("FinTitle");
SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
FDate fDate = new FDate();
JRptList tJRptList = new JRptList();
String tAgentComName = (new ExeSQL()).getOneValue("select name from lacom where agentcom in (select standbyflag1 from loprtmanager where prtseq='"+tPrtSeq+"')");
String strStartDate = (new ExeSQL()).getOneValue("select min(paydate) from ljspayb where serialno ='"+tPrtSeq+"'");
String tStartDate = df.format(fDate.getDate(strStartDate));
String strEndDate = (new ExeSQL()).getOneValue("select max(paydate) from ljspayb where serialno ='"+tPrtSeq+"'");
String tEndDate = df.format(fDate.getDate(strEndDate));
String tPayType = "收费";
String tCount = (new ExeSQL()).getOneValue("select count(1) from ljspayb where serialno ='"+tPrtSeq+"'");
String tSumPrem = (new ExeSQL()).getOneValue("select sum(sumduepaymoney) from ljspayb where serialno ='"+tPrtSeq+"'");
String tOperator = tG.Operator;
String tToday = df.format(new Date());
String tOutFileName = "";
String FileName= "GrpTravelAgent";
tJRptList.AddVar("AgentComName", tAgentComName);
tJRptList.AddVar("StartDate", tStartDate);
tJRptList.AddVar("EndDate", tEndDate);
tJRptList.AddVar("PayType", tPayType);
tJRptList.AddVar("Count", tCount);
tJRptList.AddVar("SumPrem", tSumPrem);
tJRptList.AddVar("PrtSeq", tPrtSeq);
tJRptList.AddVar("Operator", tOperator);
tJRptList.AddVar("Today", tToday);
tJRptList.AddVar("FinTitle", tFinTitle);
String strSql = "update loprtmanager set stateflag='1' where PrtSeq='"+tPrtSeq+"'";
(new ExeSQL()).execUpdateSQL(strSql);
tJRptList.Prt_RptList(pageContext,FileName);
tOutFileName = tJRptList.mOutWebReportURL;
String strVFFileName = FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
String strRealPath = application.getRealPath("/web/Generated").replace('\\','/');
String strVFPathName = strRealPath +"/"+ strVFFileName;
System.out.println("strVFPathName : "+ strVFPathName);
System.out.println("=======Finshed in JSP===========");
System.out.println(tOutFileName);
response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+"&RealPath="+strVFPathName);

%>
