<%--
    代理点签单 2006-7-5 17:22 Yangming
--%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%
  String FlagStr="";      //操作结果
  String Content = "";    //控制台信息
  String tAction = "";    //操作类型：delete update insert
  String tOperate = "";   //操作代码
  String transact = "";
  CErrors tError = null;
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  String tGrpContNo = request.getParameter("GrpContNo");
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
  tLCGrpContSchema.setProposalGrpContNo(tGrpContNo);
  TransferData tTransferData = new TransferData();
 	tTransferData.setNameAndValue("MissionID",request.getParameter("MissionID"));
  tTransferData.setNameAndValue("SubMissionID",request.getParameter("SubMissionID"));
  
  
  BriefAgentSignUI tBriefAgentSignUI = new BriefAgentSignUI();
	//调用江苏中介校验
  boolean tJSFlag = false;
  {
	  String tJSPrtNo = request.getParameter("PrtNo");
	  String tSaleChnl = new ExeSQL().getOneValue("select salechnl from lcgrpcont where prtno='"+tJSPrtNo+"'");
	  String tJSGrpContNo = new ExeSQL().getOneValue("select grpcontno from lcgrpcont where prtno='"+tJSPrtNo+"'");
	  String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
			  	  + "and code = '"+tSaleChnl+"'";
	  SSRS tSSRS = new ExeSQL().execSQL(tSql);
	  String tManageCom = new ExeSQL().getOneValue("select managecom from lcgrpcont where prtno='"+tJSPrtNo+"'");
	  String tSubManageCom = tManageCom.substring(0,4);
	  if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(tSubManageCom)){
		  ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
    	  TransferData tJSTransferData = new TransferData();
    	  tJSTransferData.setNameAndValue("ContNo",tJSGrpContNo);
    	  VData tJSVData = new VData();
    	  tJSVData.add(tJSTransferData);
    	  if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
    		  Content = tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
    		  FlagStr = "Fail";
    		  tJSFlag = true;
    	  }
	  }
  }
  if(!tJSFlag){
  try
  {
  	// 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLCGrpContSchema);
  	tVData.add(tTransferData);
  	tVData.add(tG);
    tBriefAgentSignUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  }
	//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tBriefAgentSignUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
    
//添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
