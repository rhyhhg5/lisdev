<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
	<script>
		var prtNo = "<%=request.getParameter("prtNo")%>";		
	</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BriefSingleContModifyInputInit.jsp"%>
  <SCRIPT src="BriefSingleContModifyInput.js"></SCRIPT>
  <SCRIPT src="ModifyInitDatabaseToCont.js"></SCRIPT>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./BriefSingleContModifySave.jsp" method=post name=fm target="fraSubmit">
	  <table id="table1">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ManageInfoDiv);">
	  			</td>
	  			<td class="titleImg">管理信息
	  			</td>
	  		</tr>
	  </table>
  	<div id="ManageInfoDiv" style="display: ''">
	  	<table class=common>
	  		<tr class=common>
	  			<td class=title>
	  				印刷号
	  			</td>
	  			<td class=input>
	  				<Input class= common8 name=PrtNo readonly elementtype=nacessary TABINDEX="-1" MAXLENGTH="11" verify="印刷号码|notnull&len=11" >
	  			</td>
	  			<td class=title>
	  				管理机构
	  			</td>
	  			<td class=input>
	  				<Input class= common8  name=ManageCom readonly>
	  				<!--Input class=codeNo name=ManageCom readonly verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary-->
	  			</td>
	  			<TD  class= title8>
	          销售渠道
	        </TD> 
	        <TD  class= input8>
	        	<Input class= common8  name=SaleChnl readonly>
	        	<!--Input class=codeNo name=SaleChnl readonly verify="销售渠道|notnull" ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary-->
	        </TD>
	  		</tr>
	  		<TR class=common>
		    	<TD  class= title8>
	          业务员代码
	        </TD>
	        <TD  class= input8>
	      		<Input NAME=AgentCode VALUE="" MAXLENGTH=0 readonly CLASS=code8 elementtype=nacessary ondblclick="return queryAgent();"onkeyup="return queryAgent2();" verify="代理人编码|notnull">
	       </TD>
	        <TD  class= title8>
	          业务员名称
	        </TD>
	        <TD  class= input8>
	      		<Input NAME=AgentName VALUE=""  readonly CLASS=common >
	       </TD>         
	        <!--TD  class= title8>
	          业务员组别
	        </TD>
	        <TD  class= input8-->
	          <Input class="readonly"  type=hidden readonly name=AgentGroup verify="业务员组别notnull&len<=12" >
	        <!--/TD-->       

	      	<TD  class= title8 id = InputDateText>
	          投保单填写日期
	        </TD>
	        <TD  class= input8 id = InputDateClass >
	          <Input class="cooldatepicker"  name=InputDate readonly verify="投保单填写日期|notnull&&date" >
	        </TD> 
	        	      </TR>
	      <tr class=common>
	        <TD  class= title8 id = ReceiveDateText>
	          收单日期
	        </TD>
	        <TD  class= input8 id = ReceiveDateClass>
	          <Input class="cooldatepicker"  name=ReceiveDate readonly verify="收单日期|notnull&&date" >
	        </TD>  
	      </tr>
	  	</table>
	  </div>		
	  <table id="table2">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,AppntInfoDiv);">
	  			</td>
	  			<td class="titleImg">投保人信息
	  			</td>
	  		</tr>
	  </table>
	  <div id="AppntInfoDiv" style="display: ''">
	  	<table class=common>
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  				投保人客户号
	  			</td>
	  			<td class=input COLSPAN="1">
	  				<Input class= common name=appnt_AppntNo  verify="投保人客户号|len<=60">
	  			</td>
	  			<td><Input type=button class= cssbutton VALUE="查询" OnClick="queryAppnt()"></td><td></td>
	  		</tr>
	  	</table>
	  	 </div>
	  	<table class = common>
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  				姓名
	  			</td>
	  			<td class=input COLSPAN="1">
	  				<Input class= common name=appnt_AppntName elementtype=nacessary verify="姓名|notnull&len<=60">
	  			</td>
	  			<td class=title COLSPAN="1" id = appnt_EnglishNameText>
	  				拼音
	  			</td>
	  			<td class=input COLSPAN="1" id = appnt_EnglishNameClass>
	  				<Input class=common name=appnt_EnglishName  verify="投保人拼音" >
	  			</td>
	  			 <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
            <Input class= codeno name=appnt_IDType type=hidden value="0" verify="证件类型|code:IDType"  ondblclick="return showCodeList('IDType',[this,appnt_IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType',[this,appnt_IDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=appnt_IDTypeName readonly=true  type=hidden verify="被保险人证件类型|code:IDType">
          <Input class= common readonly name=appnt_IDTypeName1>
          </TD>
          <TD  class= title8>
            证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=appnt_IDNo readonl elementtype=nacessary onblur="getAppntBirthdaySexByIDNo(this.value);" verify="证件号码|notnull&len<=30">
          </TD>
	  			
	  		</tr>
	  		<tr class=common>	       
         
          <TD  class= title8 COLSPAN="1">
	          性别
	        </TD> 
	        <TD  class= input8 COLSPAN="1">
	        	<Input class= codeno type=hidden name=appnt_AppntSex verify="投保人性别|notnull&code:Sex"  " ondblclick="return showCodeList('Sex',[this,appnt_SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,appnt_SexName],[0,1],null,null,null,1);"><input class=codename name=appnt_SexName type=hidden readonly=true >
	        	<Input class= common readonly name=appnt_AppntSexName>
	        </TD>
	        <TD  class= title8 COLSPAN="1">
	          出生日期
	        </TD> 
	        <TD  class= input8 COLSPAN="1">
	        	<Input class= common8 readonly name=appnt_AppntBirthday elementtype=nacessary verify="出生日期|notnull&len<=20">
	        </TD>
	        
	      </tr>
	      <tr class=common>
	      	<td class=title8 COLSPAN="1">
	      		联系地址
	      	</td>
	      	<td class=input8 COLSPAN="5">
		      	<input class= common3 name=appnt_PostalAddress elementtype=nacessary verify="联系地址|notnull">
		      </td>
		      <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=appnt_ZipCode  elementtype=nacessary   verify="邮政编码|notnull&zipcode">
          </TD>
	      </tr>
	      <tr class=common8 >
	      	<TD  class= title8 COLSPAN="1">
            家庭电话
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=appnt_HomePhone elementtype=nacessary  verify="联系电话|notnull&len<=30">
          </TD>
          <TD  class= title8>
            移动电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=appnt_Mobile elementtype=nacessary  verify="移动电话|notnull&len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1">
            办公电话
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=appnt_CompanyPhone verify="办公电话|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1">
            电子邮箱
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=appnt_EMail  verify="电子邮箱|len<=60&Email">
          </TD>
	      </tr>
	  	</table>
	 
	  <table id="table2">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
	  			</td>
	  			<td class="titleImg">被保人信息
	  			</td>
	  		</tr>
	  </table>
	  <table class=common>
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  					被保险人是投保人的 
	  			</td>
	  			<td class=title COLSPAN="1">
	  				<Input class=common  name=insured_RelationToAppnt>	
	  				<!--Input class= codeno name=insured_RelationToAppnt readonly ondblclick="return showCodeList('Relation', [this,insured_RelationToAppntName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,insured_RelationToAppntName],[0,1]);"><input class=codename name=insured_RelationToAppntName readonly=true elementtype=nacessary -->  
	  			</td>	
	  			<td class=title COLSPAN="1" id = insured_InsuredNoText>
	  					被保险人客户号
	  			</td>
	  			<td class=input8 COLSPAN="1" id = insured_InsuredNoClass>
	  				<Input class= common name=insured_InsuredNo >  
	  			</td>	
	  			<td ><Input type=button class= cssbutton  name = "esayQuery" VALUE="查询" OnClick="queryInsured()"></td><td ></td>
	  		</tr>
	  	</table>
	  <div id="InsuredInfoDiv" style="display: ''">
	  	<table class=common>
	  		<!--tr class=common>
	  			<td class=title COLSPAN="1">
	  					被保险人是投保人的 
	  			</td>
	  			<td class=title COLSPAN="1">
	  				<Input class= codeno name=insured_RelationToAppnt  ondblclick="return showCodeList('Relation', [this,insured_RelationToAppntName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,insured_RelationToAppntName],[0,1]);"><input class=codename name=insured_RelationToAppntName readonly=true elementtype=nacessary >  
	  			</td>
	  			<!--TD  class= title8>
            单位地址编码
          </TD>
          <TD  class= input8>
             <Input class="code" name="GrpAddressNo"  ondblclick="getAddressCodeData();return showCodeListEx('GetGrpAddressNo',[this],[0],'', '', '', true);" onkeyup="getAddressCodeData();return showCodeListKeyEx('GetGrpAddressNo',[this],[0],'', '', '', true);">
          </TD>		
	  		</tr-->
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  				姓名
	  			</td>
	  			<td class=input COLSPAN="1">
	  				<Input class= common name=insured_Name  elementtype=nacessary verify="姓名|len<=60">
	  			</td>
	  			<td class=title COLSPAN="1" id = insured_EnglishNameText>
	  				拼音
	  			</td>
	  			<td class=input COLSPAN="1" id = insured_EnglishClass>
	  				<Input class=common name=insured_EnglishName verify="投保人拼音" >
	  			</td>	  		
	        
	        <TD  class= title8>
            证件类型
          </TD>
          <TD  class= input8>
           <Input class= codeno type=hidden name=insured_IDType value="0" verify="证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,insured_IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType',[this,insured_IDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=insured_IDTypeName type=hidden readonly=true  >
          <Input class= common readonly name=insured_IDTypeName1>
          </TD>
          <TD  class= title8>
            证件号码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=insured_IDNo elementtype=nacessary onblur="getInsuredBirthdaySexByIDNo(this.value);" verify="证件号码|len<=30">
          </TD>
	  		</tr>
	  		<tr class=common>
	      	<TD  class= title8 COLSPAN="1">
	          性别
	        </TD> 
	        <TD  class= input8 COLSPAN="1">
	        <Input class= codeno name=insured_Sex type=hidden verify="被保险人性别|code:Sex" ondblclick="return showCodeList('Sex',[this,insured_SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,insured_SexName],[0,1],null,null,null,1);"><input class=codename type=hidden name=insured_SexName readonly=true >
	        <Input class= common readonly name=insured_SexName1>
	        </TD>
	        <TD  class= title8 COLSPAN="1">
	          出生日期
	        </TD> 
	        <TD  class= input8 COLSPAN="1">
	        	<Input class= cooldatepicker name=insured_Birthday elementtype=nacessary verify="出生日期|date&len<=20">
	        </TD>
	        
	      </tr>
	      <tr class=common>
	      	<td class=title8 COLSPAN="1">
	      		联系地址
	      	</td>
	      	<td class=input8 COLSPAN="5">
		      	<input class= common3 name=insured_PostalAddress elementtype=nacessary verify="联系地址|len<=130">
		      </td>
		      <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=insured_ZipCode   elementtype=nacessary  verify="邮政编码|zipcode">
          </TD>
	      </tr>
	      <tr class=common8 >
	      	<TD  class= title8 COLSPAN="1">
            家庭电话
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=insured_HomePhone elementtype=nacessary  verify="联系电话|len<=30">
          </TD>
          <TD  class= title8>
            移动电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=insured_Mobile elementtype=nacessary  verify="移动电话|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1">
            办公电话
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=insured_CompanyPhone verify="办公电话|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1">
            电子邮箱
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=insured_EMail  verify="电子邮箱|len<=60&Email">
          </TD>
	      </tr>
	  	</table>
	  </div>
	  <table id="table3">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
	  			</td>
	  			<td class="titleImg">受益人信息
	  			</td>
	  		</tr>
	  </table>
	  <table class=common>
	  	 <tr class=common8 >
	     	<TD  class= title8 COLSPAN="1">
           受益人姓名
         </TD>
         <TD  class= input8 COLSPAN="1">
           <Input class= common8 name=bnf_Name  readonly verify="受益人姓名|len<=30">
         </TD>
         <TD  class= title8>
           性别
         </TD>
         <TD  class= input8>
         	<Input class= common8  name=bnf_Sex readonly>
           <!--Input class= codeno name=bnf_Sex readonly verify="投保人性别|code:Sex"  " ondblclick="return showCodeList('Sex',[this,bnf_SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Sex',[this,bnf_SexName],[0,1],null,null,null,1);"><input class=codename name=bnf_SexName readonly=true -->
         </TD>
         <TD  class= title8 COLSPAN="1">
           证件类型
         </TD>
         <TD  class= input8 COLSPAN="1">
         	<Input class= common8  name=bnf_IDType readonly>
           <!--Input class= codeno name=bnf_IDType readonly  verify="证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,bnf_IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('IDType',[this,bnf_IDTypeName],[0,1],null,null,null,1);"  ><input class=codename name=bnf_IDTypeName readonly=true  -->
         </TD>
         <TD  class= title8 COLSPAN="1">
           证件号码
         </TD>
         <TD  class= input8 COLSPAN="1">
           <Input class= common8 name=bnf_IDNo readonly verify="证件号码|len<=60&num">
         </TD>
	     </tr>
	  </table>
    	<hr>
     <div id="NationGridDiv" style="display: ''">
    <table id="table7" class=common>
	  		<tr>
	  			<td COLSPAN="1" class="titleImg">
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,NationGridDiv);">
	  			抵达国家
	  			</td>
	  		</tr>
	  </table>
	 
    	<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanNationGrid" >
						</span>
					</td>
				</tr>
			</table>
    </div> 
    <!--护照号码，全年多次保障标识，保险期间相关信息-->
    <div id="RiskInfoDiv" style="display : ''">
    	<table class=common>
	  		<tr class=common>
	  			<td class=title COLSPAN="1">
	  					护照号码 
	  			</td>
	  			<td class=title COLSPAN="1">
	  				<Input class= common8 name=insured_OthIDNo>
	  				<Input class= common8 name=insured_OthIDType type="hidden" value="1">
	  			</td>
	  			<td class=title COLSPAN="1">
	  					全年多次保障标识 
	  			</td>
	  			<td class=title COLSPAN="1">
	  				<Input class=codeNo name=DegreeType readonly value=0 CodeData="0|^0|单次旅行^1|多次旅行" ondblclick="return showCodeListEx('StandbyFlag1',[this,StandbyFlag1Name],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StandbyFlag1',[this,StandbyFlag1Name],[0,1],null,null,null,1);"><input class=codename name=StandbyFlag1Name readonly=true elementtype=nacessary> 
	  			</td>
	  			<td class=common8 COLSPAN="1">
	  			</td>
	  			<td class=common8 COLSPAN="1">
	  			</td>
	  			<td class=common8 COLSPAN="1">
	  			</td>
	  			<td class=common8 COLSPAN="1">
	  			</td>
	  		</tr>
	  	</table>
	  	</div>
	  		<tr class=common >
	  			<td class=title COLSPAN="1" >
	  				责任生效日期
	  			</td>
	  			<td class=common8 COLSPAN="1" >
	  				<Input class='coolDatePicker' name=CValiDate  verify="责任生效日期|notnull&date" onfocus="showDays();DifDate();"  onkeyup="showDays();DifDate();"  onkeydown="showDays();DifDate();" elementtype=nacessary>
	  			</td>
	  			<td class=title COLSPAN="1">
	  				责任终止日期
	  			</td>
	  			<td class=common8 COLSPAN="1">
	  				<Input class=common8   name=CInValiDate readonly verify="责任终止日期|notnull&date" elementtype=nacessary onfocus="showDays();DifDate();">
	  			</td>
	  			<td class=common COLSPAN="1">
	  				共<Input class=common_A readonly=true name=Days >天
	  			</td>
	  		</tr>
   
   	 <!--险种信息-->
			<div id="RiskInfoGridDiv" style="display: ''">
		  	<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanRiskGrid" >
							</span>
						</td>
					</tr>
				</table>
		  </div> 
    	<hr>
		<div id="PayInfoTitleDiv" style="display: ''">
			<table id="table5">
		  		<tr>
		  			<td>
		  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,PayInfoDiv);">
		  			</td>
		  			<td class="titleImg">缴费信息
		  			</td>
		  		</tr>
		  </table>
		</div>
	  <div id="PayInfoDiv" style="display: ''">
	  	<hr>
		  <table class="common">
		  	<tr class=common8>
			  	<TD  class= title8 COLSPAN="1">
			      缴费频次
			    </TD>
			    <TD  class= input8 COLSPAN="1">
			      <Input class=codeNo name=PayIntv readonly  CodeData="0|^0|趸缴" verify="缴费频次|notnull" ondblclick="return showCodeListEx('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);"><input class=codename name=GrpContPayIntvName elementtype=nacessary  readonly=true >  
			    </TD>
			    <TD  class= title8 COLSPAN="1">
			      缴费方式
			    </TD>
			    <TD  class= input8 COLSPAN="1">
			      <Input class=codeNo name=PayMode readonly  VALUE="1" MAXLENGTH=1 CodeData="0|^1|现金" verify="缴费方式|notnull"  ondblclick="return showCodeListEx('GrpPayMode',[this,PayModeName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('GrpPayMode',[this,PayModeName],[0,1],null,null,null,1);"><input class=codename name=PayModeName readonly=true elementtype=nacessary>
			    </TD>
			    <TD  class= title8 COLSPAN="1" style="display: 'none'">
	          开户银行
	        </TD>
	        <TD  class= input8 COLSPAN="1" style="display: 'none'">
	          <Input class=codeNo  name=BankCode readonly style="display:'none'"  MAXLENGTH=1 verify="开户银行|code:bank&len<=24" ondblclick="return showCodeList('Bank',[this,BankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName],[0,1],null,null,null,1);"><input class=codename name=BankCodeName style="display:'none'" readonly=true >
	        </TD>
			  </tr>
			  <tr class=common8>
			  	<TD  class= title8 style="display: 'none'">
	          账&nbsp;&nbsp;&nbsp;号
	        </TD>
	        <TD  class= input8 style="display: 'none'">
	          <Input class= common8 name=BankAccNo readonly style="display:'none'" verify="帐号|len<=40">
	        </TD>
	        <TD  class= title8 style="display: 'none'">
	          户&nbsp;&nbsp;&nbsp;名
	        </TD>
	        <TD  class= input8 style="display: 'none'">
	          <Input class= common8 name=AccName readonly style="display:'none'" verify="户名|len<=40">
	        </TD>   
			  </tr>
	    </table>
	  </div>
	  <table id="table5">
	  		<tr>
	  			<td>
	  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,RemarkDiv);">
	  			</td>
	  			<td class="titleImg">备注栏
	  			</td>
	  		</tr>
	  </table>
	    <div id="RemarkDiv" style="display ''">
	  	<table class=common >
			  <TR  class= common> 
		      <TD  class= title8> 特别约定 </TD>
		    </TR>
		    <TR  class= common>	
		      <TD  class= title8>
		        <textarea name="Remark" cols="110" readonly rows="3" class="common3" width=100% value="">
		        </textarea>
		      </TD>
		    </TR>
		  </table>
	  </div>
		<table class=common>
			<tr class=common align=right>
				<td>
				<INPUT name="BriefModifyDiv"  class=cssButton VALUE="修    改"  TYPE=button onclick="updateClick()">
				<INPUT VALUE="修改完毕" name="SignPol" TYPE=button class= cssbutton  onclick="signPol();"> 
			</td>
			</tr>
	  </table>
	  <Input class= input type=hidden name=ContNo>
	  <Input class= input type=hidden name=insured_AddressNo>
	<Input class= input  type=hidden name=appnt_AddressNo>
	<Input class= input type=hidden name=fmAction>  
	<Input class= input type=hidden name=AppFlag>
	<Input class= input type=hidden name=insured_ExecuteCom>
	<Input class= input type=hidden name=appntchangecontnoFlag>
	<Input class= input type=hidden name=insuredchangecontnoFlag>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>