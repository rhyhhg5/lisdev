<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
    tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var comcode = "<%=tGI.ComCode%>";//记录登陆机构
    var LoadFlag = "<%=request.getParameter("Flag")%>";
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="BriefSingleContPrintInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="BriefSingleContPrintInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
    <form action="./BriefSingleContPrintSave.jsp" method=post name=fm target=fraSubmit>
        <table id="table1">
        </table>
        <table class=common border=0 width=100%>
            <tr>
                <td class=titleImg align=center>请输入查询条件：</td>
            </tr>
        </table>
        <table class=common align=center>
            <TR class=common>
                <TD class=title>销售渠道</TD>
                <TD class=input>
                    <Input class=codeNo name=SaleChnl verify="销售渠道|code:SaleChnl" ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName readonly=true >
                </TD>
                <TD class=title>管理机构</TD>
                <TD class=input>
                    <Input class=codeNo name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >
                </TD>
                <TD class=title>业务员代码</TD>
                <TD class=input>
                    <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCode',[this,AgentCodeNam],[0,1]);" onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeNam],[0,1]);"><input class=codename name=AgentCodeNam readonly=true>
                </TD>
            </TR>
            <TR class=common>
                <TD class=title>个人合同号</TD>
                <TD class=input>
                    <Input class=common name=ContNo>
                    <Input type=hidden name = ContNum>
                </TD>
                <TD class=title>印刷号码</TD>
                <TD class=input>
                    <Input class=common name=PrtNo>
                </TD>
                <TD class=title>保单类型</TD>
                <TD class=input>
                    <Input class=codeNo name=CardFlag CodeData="0|^1|境外救援^2|简易意外险^5|电话销售^6|银行保险" ondblclick="return showCodeListEx('CardFlag',[this,CardFlagName],[0,1]);" onkeyup="return showCodeListKey('CardFlag',[this,CardFlagName],[0,1]);"><input class=codename name=CardFlagName readonly=true>
                </TD>
            </TR>
            <tr>
                <td class="title">是否打印</td>
                <td class= input>
                    <input class="codeNo" name="PrintState" CodeData="0|^2|全部^0|未打印^1|已打印" ondblclick="return showCodeListEx('PrintState',[this,PrintStateName],[0,1]);" onkeyup="return showCodeListKey('PrintState',[this,PrintStateName],[0,1]);"><input class="codename" name="PrintStateName" readonly="true" />
                </td>
                <TD class= title>承保起期</TD>
                <TD class= input>
                    <Input name=SignStartDate class='coolDatePicker' dateFormat='short' verify="承保起期|notnull&Date" elementtype=nacessary>
                </TD>
                <TD class= title>承保止期</TD>
                <TD class= input>
                    <Input name=SignEndDate class='coolDatePicker' dateFormat='short' verify="承保止期|notnull&Date" elementtype=nacessary>
                </TD>
            </tr>
            <tr>
				<!-- modify by zxs -->
			<TD  class= title> 是否PAD出单</TD>
          	<TD  class= input><Input class="codeNo" name=IsPAD  CodeData="0|^2|全部^0|否^1|是" ondblclick="return showCodeListEx('IsPAD',[this,IsPADName],[0,1]);" onkeyup="return showCodeListKey('yesno1',[this,IsPADName],[0,1]);"><input class=codename name=IsPADName readonly=true ></TD>
      	   	<TD  class= title> 影像复查是否完成</TD>
          	<TD  class= input><Input class="codeNo" name=ChcekFlag  CodeData="0|^2|全部^0|否^1|是" ondblclick="return showCodeListEx('ChcekFlag',[this,ChcekFlagName],[0,1]);" onkeyup="return showCodeListKey('yesno1',[this,ChcekFlagName],[0,1]);"><input class=codename name=ChcekFlagName readonly=true ></TD>
			</tr>
            <TR class=common>
                <TD class=title style="display: 'none'">展业机构</TD>
                <td class="input" nowrap="true" style="display: 'none'">
                    <Input class="common" name="BranchGroup" >
                    <input name="btnQueryBranch" class="common" type="button" value="?" onclick="queryBranch()" style="width:20">
                </TD>
            </TR>
        </table>
        <input value="查询保单" class="cssButton" type="button" onclick="easyQueryClick1();">
        <input value="打印保单" class="cssButton" type="button" onclick="printGroupPol();" name='printButton'>
        <input value="打印保险卡" class="cssButton" type="button" onclick="printGroupPolCard();" name='printButton'>
        <input value="清单下载" class="cssButton" type="button" onclick="printList();">
        <input value="首次打印" type="button" onclick="firstPrintCont();" style="width:6;float: right"><!--运维功能-->
        <div id="RiskDutyGridDiv" style="display: ''">
            <table  class= common>
                <tr  class= common>
                    <td text-align: left colSpan=1>
                        <span id="spanContPageGrid" >
                        </span>
                    </td>
                </tr>
            </table>
        </div>
        <Div  id= "divLCGrp1" style= "display: ''" align=center>
            <INPUT VALUE="首  页" TYPE=button class= cssbutton onclick="getFirstPage();">
            <INPUT VALUE="上一页" TYPE=button class= cssbutton onclick="getPreviousPage();">
            <INPUT VALUE="下一页" TYPE=button class= cssbutton onclick="getNextPage();">
            <INPUT VALUE="尾  页" TYPE=button class= cssbutton  onclick="getLastPage();">
        </div>
        <input type=hidden name=fmAction>
        <input type=hidden name=mContNo>
        <input type=hidden name=PrtFlag>
        <input type=hidden name=mCardFlag>
        <input type=hidden name=querySql>
        <input type=hidden name=firstPrint value="0">
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
    <iframe name="printfrm" src="" width=0 height=0></iframe>
    <form id=printform target="printfrm" action="">
        <input type=hidden name=filename value=""/>
    </form>
</body>
</html>