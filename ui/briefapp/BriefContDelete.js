//程序名称：BriefContDelete.js
//程序功能：简易投保单删除
//创建日期：2007-11-21
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();
var flag1 = 0;
window.onfocus=myonfocus;

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  window.focus();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content);
  }
  else
  { 
    alert("操作成功");
  }  
  initBriefGrid();
  initInpBox();    
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//查询简易保单
function query()
{
	// 初始化表格                                                                                                                                                           
	initBriefGrid();	
	if(!verifyInput2())
	{
		//alert("input error.");
		return false;
	}
	// 书写SQL语句,简易件个单
   	var strsql="select prtno,contno,appntname,managecom,cardflag from lccont  "
   		+"  where  ContType = '1' "
   		+ getWherePart('cardflag','ContType','=')
   		+ getWherePart('Prtno','PrtNo','=')
   		+ getWherePart('managecom','ManageCom','=')
   		+ getWherePart('agentcode','AgentCode','=');
   		//alert(strsql);
    turnPage.queryModal(strsql,BriefGrid); 
    
  
  //判断是否查询成功,如果lccont中不存在要查一下lwmission中是否存在未录单前的投保件
  if (!turnPage.strQueryResult) 
  {
    	strsql = "select Missionprop1 ,(select '0000000000' from dual),(select '未录入' from dual),Missionprop3,Missionprop5 from lwmission where 1=1"
    			+ getWherePart('Missionprop5','ContType','=')
				+ getWherePart('Missionprop3','ManageCom','=')
				+ getWherePart('Missionprop1','PrtNo','=');
				
		//turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
		turnPage.queryModal(strsql,BriefGrid);		    			    
		if(!turnPage.strQueryResult)
		{
			alert("没有符合条件的投保单！")
			return "";
		}	
  }
  else
  {
  	flag1 = 1;
  }
  return true;
}

function deleteCont()
{
	//是否选择了要删除的记录
	var tRow = BriefGrid.getSelNo() - 1;
	if(tRow == null || tRow < 0)
	{
		alert("请选择一条要删除记录.");
		return false;
	}
	
	//查询要删除的扫描件信息
	var tResult=BriefGrid.getRowData(tRow);	
	var Prtno = tResult[0];
	var contno = tResult[1];
	fm.all("contno").value = contno;
	
	if(fm.DeleteReason.value == "")
	{
		alert("没有录入删除原因，不能新单删除!")
		return;
	}

	var strsql = "";
		
	//保单处于在途状态,不能删
	strsql = "select BankOnTheWayflag from ljspay where othernotype = '9' and Otherno = '"+Prtno+"'";	
	var arr = easyExecSql(strsql); 
	if(arr)
	{
		if(arr[0][0]!=null&&arr[0][0]!=0)
		{
			alert("保单处于发盘状态,不能删除.");
			return;
		}
	}
	
	//如果保单已经交费,不能删
	var strsql = "select 1 from ljtempfee where otherno='"+Prtno+"'";
	var arr = easyExecSql(strsql);
	if(arr){
		alert("保单已交费,不能删除!");
		return ;
	}
			
	//如果保单已经承保,不能删
	strsql = "select appflag from lccont  "
			+"  where prtno = '"+Prtno+"' and ContType = '1' and cardflag in ('1','2','3','6')";			
	var arr = easyExecSql(strsql); 
	if(arr)
	{
		if(arr[0][0]==1)
		{
			alert("保单已经承保,不能删除.");
			return;
		}
	}

	if (!confirm("确认要删除该保单吗？"))
	{
		return;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	//alert(flag1);
	fm.action = "./BriefContDeleteChk.jsp?flag1="+flag1;
	fm.submit();
}
 