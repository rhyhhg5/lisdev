<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
  <script>
    var ContNo = "<%=request.getParameter("ContNo")%>";
    var LoadFlag = "<%=request.getParameter("LoadFlag")%>";
    var ContType= "<%=request.getParameter("ContType")%>";
    var MissionProp5= "<%=request.getParameter("MissionProp5")%>";    
    var CurrentDate = "<%=PubFun.getCurrentDate()%>";
    var tVideoFlag = "<%=request.getParameter("VideoFlag")%>";
  </script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="BriefRiskPreview.js"></SCRIPT>
  <SCRIPT src="InitDatabaseToCont.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BriefRiskPreviewInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./BriefRiskPreviewSave.jsp" method=post name=fm target="fraSubmit">
<div id="CardDiv" style="display: 'none'">
    <table id="table7">
        <tr>
          <td>
          <img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ManageInfoDiv);">
          </td>
          <td class="titleImg">卡单信息
          </td>
        </tr>
    </table>
      <table class=common>
        <tr class=common>
          <td class=title>
            卡单号码
          </td>
          <td class=input>
            <Input class= readonly name=CardNo readonly >
          </td>
          <td class=title>
            卡单类型
          </td>
          <td class=input>
            <Input class= readonly name=CardType readonly >
          </td>
          <td class=title>
            下发日期
          </td>
          <td class=input>
            <Input class= readonly name=ReleaseDate readonly>
          </td>
          <TD  class= title8 id=zhongjiecode style="display: 'none'">
              中介公司代码
            </TD>
            <TD  class= input8 id=zhongjiename style="display: 'none'">
              <Input class="codeNo" name=AgentCom verify="中介公司代码|code:AgentCom"  ondblclick="return showCodeList('agentcombank',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('agentcombank',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');"><Input class="codeName" name=AgentComName readonly >
            </TD>
            <TD  class= title8 id=zhongjiecode1 style="display: 'none'" >
              中介公司代码
            </TD>
            <TD  class= input8 id=zhongjiename1 style="display: 'none'">
              <Input class="codeNo" name=AgentCom1 verify="中介公司代码|code:AgentCom"  ondblclick="return showCodeList('agentcombrief',[this,AgentComName1],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('agentcombrief',[this,AgentComName1],[0,1],null, fm.all('ManageCom').value, 'ManageCom');"><Input class="codeName" name=AgentComName1 readonly >
            </TD>
        </tr>
        <tr class=common>
          <td class=title>
            险种
          </td>
          <td class=input>
            <Input class= readonly name=CardRiskCode readonly >
          </td>
          <td class=title>
            保额＼档次
          </td>
          <td class=input>
            <Input class=readonly name=CardAmnt readonly >
          </td>
          <TD  class= title8>
            保费
          </TD> 
          <TD  class= input8>
            <Input class=readonly name=CardPrem readonly >
          </TD>
        </tr>
      </table>
</div>
    <table id="table1">
        <tr>
          <td>
          <img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ManageInfoDiv);">
          </td>
          <td class="titleImg">保单信息
          </td>
        </tr>
    </table>
    <div id="ManageInfoDiv" style="display: ''">
      <table class=common>
        <td class=title>
          印刷号
        </td>
        <td class=input>
          <Input class= readonly readonly name=PrtNo TABINDEX="-1" MAXLENGTH="11" verify="印刷号码|notnull&len=11" >
        </td>
        <td class=title>
          管理机构
        </td>
        <td class=input>
          <Input class=readonly readonly name=ManageCom TABINDEX="-1" MAXLENGTH="11" verify="印刷号码|notnull&len=11" >
        </td>
        <TD  class= title8>
            业务员代码
          </TD>
          <TD  class= input8>
            <Input NAME=GroupAgentCode VALUE="" MAXLENGTH=0 class=readonly readonly  verify="代理人编码|notnull">
            <Input NAME=AgentCode VALUE="" MAXLENGTH=0 class=readonly readonly type='hidden' >
         </TD>
         <TD  class= title8>
            业务员名称
          </TD>
          <TD  class= input8>
            <Input NAME=AgentName VALUE=""  class=readonly readonly >
         </TD>  
      </table>
    </div>
    <table id="table2">
      <tr>
        <td>
        <img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,ManageInfoDiv);">
        </td>
        <td class="titleImg">投保人信息
        </td>
      </tr>
    </table>
    <table class = common>
      <tr class=common>
        <td class=title COLSPAN="1">
          姓名
        </td>
        <td class=input COLSPAN="1">
          <Input class=readonly readonly name=appnt_AppntName >
        </td>
        <TD  class= title8 COLSPAN="1">
          性别
        </TD> 
        <TD  class= input8 COLSPAN="1">
          <Input class=readonly readonly name=appnt_AppntSex  >
        </TD>
        <TD  class= title8 COLSPAN="1">
          出生日期
        </TD> 
        <TD  class= input8 COLSPAN="1">
          <Input class=readonly readonly name=appnt_AppntBirthday >
        </TD>
      </tr>
      <tr class=common>
        <TD  class= title8>
          证件类型
        </TD>
        <TD  class= input8>
          <Input class=readonly readonly name=appnt_IDType  >
        </TD>
        <TD  class= title8>
          证件号码
        </TD>
        <TD  class= input8>
          <Input class=readonly readonly name=appnt_IDNo  >
        </TD>
      </tr>
    </table>
    <table id="table3">
        <tr>
          <td>
          <img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
          </td>
          <td class="titleImg">被保人信息
          </td>
        </tr>
    </table>
    <table class = common>
      <tr class=common>
        <td class=title COLSPAN="1">
          姓名
        </td>
        <td class=input COLSPAN="1">
          <Input class=readonly readonly name=insured_Name >
        </td>
        <TD  class= title8 COLSPAN="1">
          性别
        </TD> 
        <TD  class= input8 COLSPAN="1">
          <Input class=readonly readonly name=insured_Sex  >
        </TD>
        <TD  class= title8 COLSPAN="1">
          出生日期
        </TD> 
        <TD  class= input8 COLSPAN="1">
          <Input class=readonly readonly name=insured_Birthday >
        </TD>
      </tr>
      <tr class=common>
        <TD  class= title8>
          证件类型
        </TD>
        <TD  class= input8>
          <Input class=readonly readonly name=insured_IDType  >
        </TD>
        <TD  class= title8>
          证件号码
        </TD>
        <TD  class= input8>
          <Input class=readonly readonly name=insured_IDNo  >
        </TD>
      </tr>
    </table>
    <table id="table4">
      <tr>
        <td>
        <img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,InsuredInfoDiv);">
        </td>
        <td class="titleImg">受益人信息
        </td>
      </tr>
    </table>
    <table class = common>
      <tr class=common>
        <td class=title COLSPAN="1">
          受益人姓名
        </td>
        <td class=input COLSPAN="1">
          <Input class=readonly readonly name=bnf_Name >
        </td>
        <TD  class= title8 COLSPAN="1">
          性别
        </TD> 
        <TD  class= input8 COLSPAN="1">
          <Input class=readonly readonly name=bnf_Sex  >
        </TD>
        <TD  class= title8>
          证件类型
        </TD>
        <TD  class= input8>
          <Input class=readonly readonly name=bnf_IDType  >
        </TD>
        <TD  class= title8>
          证件号码
        </TD>
        <TD  class= input8>
          <Input class=readonly readonly name=bnf_IDNo  >
        </TD>
      </tr>
    </table>
    <!--险种信息-->
    <div id="RiskInfoGridDiv" style="display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1>
            <span id="spanRiskDutyGrid" >
            </span>
          </td>
        </tr>
      </table>
    </div> 
     <!--ABCDE五项保额-->
    <div id="AmntInfoGridDiv" style="display: ''">
      <table>
        <tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
            </td>
            <td class= titleImg>
                保额信息
            </td>
        </tr>
      </table>
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanAmntGrid" ></span></td>
        </tr>
      </table>
    </div> 
    <div id="showPremDiv" style="display: ''">
      <table class=common>
        <tr class=common>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td class=title>总保费：</td>
          <td class=input><Input class=common name=prem></td>
        </tr>
      </table>
    </div>
    <p align=right>
      <input type=hidden name="Flag" value = "2">
      <INPUT class=cssButton VALUE="返回修改"  TYPE=button onclick="top.close();">
      <INPUT class=cssButton VALUE="录音录像调阅"  TYPE=button onclick="getAudioAndVideo();" id="AudioAndVideo" style="display: none">
	  <INPUT type= "hidden" class=Common name=VideoFlag>
      <INPUT class=cssButton VALUE="签  单" name=SignCont TYPE=button onclick="top.opener.BriefInputConfirm('0000007001',ContType); top.close();" >
      <INPUT class=cssButton VALUE="录入完毕" name=OverWrite TYPE=button onclick="endInput('0000007001');">
      <INPUT class=cssButton VALUE="录入完毕" name=PreviewPrint TYPE=button onclick="endInput('0000007003');" ></p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>