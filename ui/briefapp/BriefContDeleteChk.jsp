<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BriefContDeleteChk.jsp
//程序功能：简易件单整单删除
//创建日期：2007-11-22
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容
System.out.println("Auto-begin:");
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.brieftb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
   System.out.println("BriefContDeleteChk.jsp=========begin");
   //公共变量
   CErrors tError = null;
   String FlagStr = "";
   String Content = "";
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");
   if(tG == null) 
   {
	 System.out.println("session has expired");
	 return;
    }
   
 	// 投保单列表,如果flag1=1为已经录入的保单,lccont中有记录
 	String flag1 = request.getParameter("flag1");
 	String mOperate = "DELETE";
 	
 	System.out.println("flag1:"+flag1);
	String tContNo = request.getParameter("ContNo");
	String tPrtNo = request.getParameter("PrtNo");
	String tDeleteReason = request.getParameter("DeleteReason");
	
	VData tVData = new VData();
	
	//System.out.println("tDeleteReason:"+tDeleteReason);
	boolean flag = false;	
	System.out.println("ready for UWCHECK ContNo: " + tContNo);	
	
	if(!flag1.equals("1"))
 	{
 		mOperate = "DELETE||LW";
 		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("tPrtNo",tPrtNo);
		tVData.add( tTransferData );
		tVData.add( tG );
 	}	
	else//走ContDeleBL
	{
		LCContSet tLCContSet = null;
		LCContDB tLCContDB = new LCContDB();
		TransferData tTransferData = new TransferData();
		if(!StrTool.cTrim(tContNo).equals(""))
		{     
			tLCContDB.setContNo(tContNo);   
			tLCContSet = tLCContDB.query();   
		}   
		else   
		{   
			LCContSchema mLCContSchema = new LCContSchema();
			mLCContSchema.setPrtNo(tPrtNo);
			tLCContSet = new LCContSet();
			tLCContSet.add(mLCContSchema);
		}
		if (tLCContSet == null)
		{
			System.out.println("（投）保单号为" + tContNo + "的合同查找失败！");
			return;
		}   

		LCContSchema tLCContSchema = tLCContSet.get(1);
	    tTransferData.setNameAndValue("DeleteReason",tDeleteReason);
	    tTransferData.setNameAndValue("tPrtNo",tPrtNo);
		// 准备传输数据 VData		
		tVData.add( tLCContSchema );
		tVData.add( tTransferData );
		tVData.add( tG );
	}
		
	// 数据传输
	try{
	
		BriefContDeleUI tBriefContDeleUI = new BriefContDeleUI();
		if (tBriefContDeleUI.submitData(tVData,mOperate) == false)
		{
			FlagStr = "Fail";
		}
		else FlagStr = "Succ";

		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tBriefContDeleUI.mErrors;
		    System.out.println("tError.getErrorCount:"+tError.getErrorCount());
		    if (!tError.needDealError())
		    {                          
		    	Content = " 整单删除成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 删除失败，原因是:";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
				}
		    	FlagStr = "Fail";
		    }
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
		Content = Content.trim()+".提示：异常终止!";
	}

%>                      
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
