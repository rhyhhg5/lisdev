<%
//程序名称：LCInuredListInput.jsp
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
	divHidden.style.display = "none";
  try
  {                                   
    fmSave.all('GrpContNo').value = "";
    fmSave.all('InsuredID').value = "";
    fmSave.all('State').value = "";
    fmSave.all('ContNo').value = "";
    fmSave.all('BatchNo').value = "";
    fmSave.all('InusredNo').value = "";
    fmSave.all('Retire').value = "";
    fmSave.all('EmployeeName').value = "";
    fmSave.all('InsuredName').value = "";
    fmSave.all('Relation').value = "";
    fmSave.all('Sex').value = "";
    fmSave.all('Birthday').value = "";
    fmSave.all('IDType').value = "";
    fmSave.all('IDNo').value = "";
    fmSave.all('ContPlanCode').value = "";
    fmSave.all('OccupationType').value = "";
    fmSave.all('BankCode').value = "";
    fmSave.all('BankAccNo').value = "";
    fmSave.all('AccName').value = "";
    fmSave.all('Operator').value = "";
    fmSave.all('MakeDate').value = "";
    fmSave.all('MakeTime').value = "";
    fmSave.all('ModifyDate').value = "";
    fmSave.all('ModifyTime').value = "";
    fmSave.saveButton.style.display = "none";
    fmSave.deleteButton.style.display = "none";
    fmSave.modifyButton.style.display = "none";
    fmSave.querybutton.style.display = "none";
  }
  catch(ex)
  {
    alert("在LCInuredListInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LCInuredListInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();   
    initLCInuredListGrid(); 
    initDiskErrQueryGrid();
    QueryInsuredList();
    showInsuredInfo();
  }
  catch(re)
  {
    alert("LCInuredListInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initLCInuredListGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="集体合同号码";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=3;         		//列名
    
    
    iArray[2]=new Array();
    iArray[2][0]="被保人序号";         		//列名
    iArray[2][1]="100px";         		//列名
    iArray[2][3]=3;         		//列名
    
    
    iArray[3]=new Array();
    iArray[3][0]="状态";         		//列名
    iArray[3][1]="100px";         		//列名
    iArray[3][3]=3;         		//列名
    
    
    iArray[4]=new Array();
    iArray[4][0]="合同号码 ";         		//列名
    iArray[4][1]="100px";         		//列名
    iArray[4][3]=3;         		//列名
    
    
    iArray[5]=new Array();
    iArray[5][0]="批次号";         		//列名
    iArray[5][1]="100px";         		//列名
    iArray[5][3]=3;         		//列名
    
    
    iArray[6]=new Array();
    iArray[6][0]="被保人客户号";         		//列名
    iArray[6][1]="100px";         		//列名
    iArray[6][3]=3;         		//列名
    
    
    iArray[7]=new Array();
    iArray[7][0]="在职/退休";         		//列名
    iArray[7][1]="100px";         		//列名
    iArray[7][3]=3;         		//列名
    
    iArray[8]=new Array();
    iArray[8][0]="员工姓名";         		//列名
    iArray[8][1]="100px";         		//列名
    iArray[8][3]=3;         		//列名
    
    iArray[9]=new Array();
    iArray[9][0]="被保人姓名";         		//列名
    iArray[9][1]="100px";         		//列名
    iArray[9][3]=3;         		//列名
    
    iArray[10]=new Array();
    iArray[10][0]="与员工关系";         		//列名
    iArray[10][1]="100px";         		//列名
    iArray[10][3]=3;         		//列名
    
    iArray[11]=new Array();
    iArray[11][0]="性别";         		//列名
    iArray[11][1]="100px";         		//列名
    iArray[11][3]=3;         		//列名
    
    iArray[12]=new Array();
    iArray[12][0]="出生日期";         		//列名
    iArray[12][1]="100px";         		//列名
    iArray[12][3]=3;         		//列名
    
    iArray[13]=new Array();
    iArray[13][0]="证件类型";         		//列名
    iArray[13][1]="100px";         		//列名
    iArray[13][3]=3;         		//列名
    
    iArray[14]=new Array();
    iArray[14][0]="证件号码";         		//列名
    iArray[14][1]="100px";         		//列名
    iArray[14][3]=3;         		//列名
    
    iArray[15]=new Array();
    iArray[15][0]="保险计划";         		//列名
    iArray[15][1]="100px";         		//列名
    iArray[15][3]=3;         		//列名
    
    iArray[16]=new Array();
    iArray[16][0]="职业类别";         		//列名
    iArray[16][1]="100px";         		//列名
    iArray[16][3]=3;         		//列名
    
    iArray[17]=new Array();
    iArray[17][0]="理赔金转帐银行";         		//列名
    iArray[17][1]="100px";         		//列名
    iArray[17][3]=3;         		//列名
    
    iArray[18]=new Array();
    iArray[18][0]="帐号";         		//列名
    iArray[18][1]="100px";         		//列名
    iArray[18][3]=3;         		//列名
    
    iArray[19]=new Array();
    iArray[19][0]="户名";         		//列名
    iArray[19][1]="100px";         		//列名
    iArray[19][3]=3;         		//列名
    
    iArray[20]=new Array();
    iArray[20][0]="操作员";         		//列名
    iArray[20][1]="100px";         		//列名
    iArray[20][3]=0;         		//列名
    
    iArray[21]=new Array();
    iArray[21][0]="入机日期";         		//列名
    iArray[21][1]="100px";         		//列名
    iArray[21][3]=3;         		//列名
    
    iArray[22]=new Array();
    iArray[22][0]="入机时间";         		//列名
    iArray[22][1]="100px";         		//列名
    iArray[22][3]=3;         		//列名

		iArray[23]=new Array();
    iArray[23][0]="最后一次修改日期";         		//列名
    iArray[23][1]="100px";         		//列名
    iArray[23][3]=3;         		//列名

		iArray[24]=new Array();
    iArray[24][0]="最后一次修改时间";         		//列名
    iArray[24][1]="100px";         		//列名
    iArray[24][3]=3;         		//列名
    
    
    LCInuredListGrid = new MulLineEnter( "fmSave" , "LCInuredListGrid" ); 
    //这些属性必须在loadMulLine前

    LCInuredListGrid.mulLineCount = 0;   
    LCInuredListGrid.displayTitle = 1;
    LCInuredListGrid.hiddenPlus = 1;
    LCInuredListGrid.hiddenSubtraction = 1;
    LCInuredListGrid.canSel = 1;
    LCInuredListGrid.canChk = 0;
    LCInuredListGrid.selBoxEventFuncName = "showInsuredInfo";

    LCInuredListGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LCInuredListGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

function initDiskErrQueryGrid()
{
    var iArray = new Array();
      
    try
     {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）             
      iArray[0][1]="30px";         			//列宽                                                     
      iArray[0][2]=10;          			//列最大值                                                 
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许                      
                                                                                                                   
      iArray[1]=new Array();                                                                                       
      iArray[1][0]="团体保单号";    			//列名                                                     
      iArray[1][1]="150px";            			//列宽                                                     
      iArray[1][2]=100;            			//列最大值                                                 
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许5                    
                                                                                                                   
      iArray[2]=new Array();                                                                                       
      iArray[2][0]="批次号";         		//列名                                                     
      iArray[2][1]="150px";            			//列宽                                                     
      iArray[2][2]=100;            			//列最大值                                                 
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许 0                     

      iArray[3]=new Array();                                                                                       
      iArray[3][0]="合同ID";         		//列名                                                     
      iArray[3][1]="100px";            			//列宽                                                     
      iArray[3][2]=100;            			//列最大值                                                 
      iArray[3][3]=3;             			//是否允许输入,1表示允许，0表示不允许1                    
         
      iArray[4]=new Array();                                                                                       
      iArray[4][0]="被保人ID";         		//列名                                                     
      iArray[4][1]="100px";            			//列宽                                                     
      iArray[4][2]=100;            			//列最大值                                                 
      iArray[4][3]=0;             			//是否允许输入,1表示允许，0表示不允许1                    
                                                                                                                          
      iArray[5]=new Array();                                                                                       
      iArray[5][0]="被保人姓名";         		//列名                                                     
      iArray[5][1]="100px";            			//列宽                                                     
      iArray[5][2]=100;            			//列最大值                                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用6     
                                                                                                                   
      iArray[6]=new Array();                                                                                       
      iArray[6][0]="错误信息";         		//列名                                                     
      iArray[6][1]="700px";            			//列宽                                                     
      iArray[6][2]=100;            			//列最大值                                                 
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用3
                                                                                                           
      DiskErrQueryGrid = new MulLineEnter( "fm" , "DiskErrQueryGrid" ); 
      //这些属性必须在loadMulLine前
      DiskErrQueryGrid.mulLineCount = 0;   
      DiskErrQueryGrid.displayTitle = 1;
	  	DiskErrQueryGrid.hiddenPlus = 1;
      DiskErrQueryGrid.hiddenSubtraction = 1;
      DiskErrQueryGrid.canSel = 1;
      DiskErrQueryGrid.selBoxEventFuncName = "showList";
      DiskErrQueryGrid.loadMulLine(iArray);  

      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
