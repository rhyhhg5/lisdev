//程序名称：BriefContApplyInput.js
//程序功能：简易投保
//创建日期：2005-09-05 11:10:36
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var k = 0;
parent.fraMain.rows = "0,0,0,0,*";
var cContType;
/*********************************************************************
 *  执行新契约扫描的EasyQuery
 *  描述:查询显示对象是扫描件.显示条件:扫描件已上载成功
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick(){
	// 初始化表格
	initGrpGrid();

	// 书写SQL语句

	var strSQL = "select prtno,managecom,appntname,insuredname,cvalidate,prem "
	+"from lccont where 1=1 "
	+ getWherePart('PrtNo','PrtNo')
	+ getWherePart('ManageCom','ManageCom','like')
	+" and conttype='1' and cardflag='1' and appflag='1'"
	+" order by prtno desc";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有该保单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入扫描随动页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GoToInput()
{
  var i = 0;
  var checkFlag = 0;
  var state = "0";
  
  for (i=0; i<GrpGrid.mulLineCount; i++) {
    if (GrpGrid.getSelNo(i)) { 
      checkFlag = GrpGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	prtNo = GrpGrid.getRowColData(checkFlag - 1, 1); 	
    var	ManageCom = GrpGrid.getRowColData(checkFlag - 1, 2);
		var urlStr;
		//alert(fm.ActivityID.value);
		urlStr = "./BriefSingleContModifyMain.jsp?prtNo="+prtNo;

    var sFeatures = "status:no;help:0;close:0;dialogWidth:1024px;dialogHeight:768px;resizable=1";
    //申请该印刷号
    //var strReturn = OpenWindowNew(urlStr, "简易投保", "left");
    window.open(urlStr,"");
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}
function submitData()
{
	if( verifyInput2() == false ) return false;
	cPrtNo = fm.PrtNo.value;
	cManageCom = fm.ManageCom.value;
	cContType=fm.ContType.value;
	if(cPrtNo == "")
	{
		alert("请录入印刷号！");
		return;
	}
	if(cManageCom == "")
	{
		alert("请录入管理机构！");
		return;		
	}
	if(isNumeric(cPrtNo)==false){
		 alert("印刷号应为数字");
		 return false;
	}
	//alert(cContType);
	//alert(cManageCom);		
		if(cContType=="1"&&cManageCom!="86110000"){
		alert("此机构不能申请境外救援的保单！");
		 return false;
	}
	var strSql = "select 1 from lcgrpcont where prtno='"+cPrtNo+"'"
								+" union "
								+"select 1 from lccont where prtno='"+cPrtNo+"'"
								+" union "
								+"select 1 from lwmission where MissionProp1='"+cPrtNo+"' ";
	if(easyExecSql(strSql)){
		alert("印刷号已存在");
		return false;
	}
	if(fm.ActivityID.value=="5999999999"){
		fm.action="BriefContApplyInputSave.jsp";
		fm.submit();
	}
	else if(fm.ActivityID.value=="7999999999"){
		if(cContType == "")
		{
			alert("请录入保单类型！");
			return;		
		}
		var strSQL1="select 1 from ljtempfee where otherno='"+cPrtNo+"'";
		if(!easyExecSql(strSQL1)){
			alert("没有缴费,不能生成保单!");
			return false;
		}	
		fm.action="BriefContSingleApplyInputSave.jsp";
		fm.submit();
	}
}
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
}



