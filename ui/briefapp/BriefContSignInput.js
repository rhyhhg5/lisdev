//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function signGrpPol()
{
	var tSel = GrpGrid.getSelNo();
	var cPolNo = "";
	fm.all("workType").value = "";
	if( tSel != null && tSel != 0 )
		cPolNo = GrpGrid.getRowColData( tSel - 1, 1 );

	if( cPolNo == null || cPolNo == "" )
		alert("请选择一张集体投保单后，再进行签单操作");
	else
	{
		//校验转账支票是否到账确认
		var prtno = GrpGrid.getRowColData(tSel-1,2);
		if(!isConfirm(prtno))
			return false;
		
		var tSRSQL = "select lcp.riskcode from lcpol lcp where lcp.prtno = '" + prtno + "' "
			        + "and lcp.riskcode in (select code from ldcode where codetype = 'stoprisk') "
			        + "and current date >= (select CODEALIAS from ldcode where codetype = 'stoprisk' and code = lcp.riskcode) ";
		var arrSRResult = easyExecSql(tSRSQL);
		if (arrSRResult!=null) {
		 alert("险种"+arrSRResult[0][0]+"已停售，不能签单！");
		 return false;
		}

		var i = 0;
		var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.submit(); //提交
	}
}

//提交，预打保单按钮对应操作
function priviewGrpPol()
{
	var tSel = GrpGrid.getSelNo();
	var cPolNo = "";
	fm.all("workType").value = "PREVIEW";
	if( tSel != null && tSel != 0 )
		cPolNo = GrpGrid.getRowColData( tSel - 1, 1 );

	if( cPolNo == null || cPolNo == "" )
		alert("请选择一张集体投保单后，再进行签单操作");
	else
	{
		var i = 0;
		var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.submit(); //提交
	}
}

/*********************************************************************
 *  集体投保单复核的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	fm.all("workType").value = "";
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	 	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

		// 刷新查询结果
		easyQueryClick();		
	}
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  调用EasyQuery查询保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
var turnPage = new turnPageClass();  
function easyQueryClick()
{
	// 初始化表格
	initGrpGrid();
	
	// 书写SQL语句
	var strSQL = "";
	/*
	strSQL = "select grpcontno,PrtNo,AppntNo,GrpName,ManageCom,AgentCode from LCGrpcont where 1=1 "
				 + "and AppFlag='0' "
				 + "and ApproveFlag='9' "
				 + "and UWFlag in ('9','4') "			// 核保通过
				 + getWherePart( 'GrpContNo' )
				 + getWherePart( 'ManageCom' )
				 + getWherePart( 'AgentCode' )
				 + getWherePart( 'AgentGroup' )
				 + getWherePart( 'GrpNo' )
				 + getWherePart( 'GrpName' );
				 */
	strSQL = "select a.missionprop1,a.missionprop2,b.AppntNo,b.GrpName,b.ManageCom,getUniteCode(b.AgentCode),a.missionid,a.submissionid from lwmission a,lcgrpcont b where 1=1 and b.UWFlag in ('4','9') and b.appflag='0'"			 
	       + " and a.processid = '0000000005'"
	       + " and a.activityid = '0000005002' "
	       + " and a.missionprop1=b.ProposalGrpContNo "
				 + getWherePart( 'a.missionprop1','GrpContNo' )
				 + getWherePart( 'a.missionprop2','PrtNo' )
				 + getWherePart( 'getUniteCode(b.AgentCode)','AgentCode' )
				// + getWherePart( 'a.missionprop6','AgentGroup' )
				 + getWherePart( 'a.missionprop9','GrpNo' )
				 + getWherePart( 'a.missionprop7','GrpName' )
				 + " and a.MissionProp3 like '" + comcode + "%%'"  //集中权限管理体现	
				 + " order by a.missionprop2";
	turnPage.queryModal(strSQL, GrpGrid);
}

/*********************************************************************
 *  显示EasyQuery的查询结果
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpGrid();
		//HZM 到此修改
		GrpGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpGrid.loadMulLine(GrpGrid.arraySave);		
		//HZM 到此修改	
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		GrpGrid.delBlankLine();
	} // end of if
}

//发首期交费通知书
function SendFirstNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var tSel = GrpGrid.getSelNo();
	var cProposalNo = "";
	if( tSel != null && tSel != 0 )
		cProposalNo = GrpGrid.getRowColData( tSel - 1, 1 );
 
  cOtherNoType="01"; //其他号码类型
  cCode="57";        //单据类型
  
  
  if (cProposalNo != "")
  {
  	showModalDialog("../uw/GrpUWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
    if(fm.all('AgentCode').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=2","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	  }
	if(fm.all('AgentCode').value != "")	 {

	var cAgentCode = fm.AgentCode.value;  //保单号码	
	var strSql = "select groupAgentCode,Name,AgentGroup from LAAgent where groupAgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];
      fm.AgentGroup.value = arrResult[0][2];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
     }
	}	
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][95];
  	fm.AgentGroup.value = arrResult[0][1];fm.all('AgentGroupName').value = easyExecSql("select name from labranchgroup where  AgentGroup='"+arrResult[0][1]+"'");
  }
}

//校验转账支票是否已经到账确认
function isConfirm(prtno) {

	var valSQL = "select 1 as result from ljtempfeeclass a left join ljtempfee b on a.tempfeeno = b.tempfeeno  where  b.otherno = '"
			+ prtno
			+ "' and a.paymode in ('2','3') and a.enteraccdate is null with ur ";

	var result = easyExecSql(valSQL);

	if (result != null) {
		alert("该保单的收费方式是'转账支票'，尚未进行到账确认，不能进行签单");
		return false;
	}
	 return true;
}