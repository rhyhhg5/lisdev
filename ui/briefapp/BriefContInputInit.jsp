<%
//程序名称：BriefContInputInit.jsp
//程序功能：
//创建日期：2005-09-05 11:48:43
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initBox();
    queryGrpCont();
    queryAppntInfo();
    initRiskDutyGrid();
    initContPlanGrid();
    initNationGrid();
    initAuth();
    afterContPlanSava();
    queryNation();
    queryImpart();
    queryDays();
    fm.all('AgentSaleCode').disable = true;
	fm.all('AgentSaleName').readOnly = true;
    
    if(AgentType==1){
    	//将操作按钮隐藏
    	fm.SaleChnl.value="03";
    	fm.SaleChnl.readOnly="true";
    	fm.SaleChnl.ondblclick="";
    	fm.SaleChnl.onkeyup="";
    	fm.all('AgentCom').className="common";
    	PayInfoTableDiv.style.display="none";
    	PayInfoDiv.style.display="none";
    	fm.AgentCom.value = AgentCom;
    	fm.AgentCom.className="readOnly";
    	fm.AgentCom.readOnly=true;
    	getAgnetToAgentCom();//获取中介专员。
    }
    if(fm.SaleChnl.value!="03" && fm.SaleChnl.value!="15" && fm.SaleChnl.value!="20" && fm.SaleChnl.value!="04")
    {
      fm.all('AgentCom').style.display="none";
      fm.all('AgentComName').style.display="none";
      fm.all('AgentSaleCode').style.display="none";
  	  fm.all('AgentSaleName').style.display="none";
    }else{
  		initAgentSaleCode();
    }
    if(fm.PayMode.value=="2"||fm.PayMode.value=="4")

    { //alert(fm.PayMode.value);
      fm.BankCode.style.display='';
      fm.BankCodeName.style.display='';
      fm.BankAccNo.style.display='';
      fm.AccName.style.display='';
    }
    //LoadFlag==2 合同维护
    if(LoadFlag==2){
    	//将操作按钮隐藏
    	showDiv(operateButton,"false");
    	showDiv(ContPlanOperateDiv,"false");
    	showDiv(BriefModifyDiv,"true");
    	SaleChnlTitleID.style.display = "none";
    	SaleChnlInputID.style.display = "none";
    }
    initExtend();
    showAllCodeName();
  }

  catch(ex)
  {
    alert(ex.message)
  }
}
function initBox()
{
  try
  {
    fm.PolApplyDate.value=PolApplyDate;
    fm.MissionID.value=MissionID;
    fm.SubMissionID.value=SubMissionID;
    fm.PrtNo.value = prtNo;
    fm.ManageCom.value = ManageCom;
    fm.Remark.value = "";
    fm.all('SaleChnl').value = '02';
  }
  catch(ex)
  {
    alert("初始化控件错误！");
    alert(ex);
  }
}
function initAuth(){
	var strSql = "select 1 from LCGrpCont where prtNo='"+fm.PrtNo.value+"'";
    var arr = easyExecSql(strSql);
    if(arr){
    	var strSql2 = "select authorization from lcgrpappnt where prtno = '"+fm.PrtNo.value+"' ";
    	var arr2 = easyExecSql(strSql2);
    	fm.auth.value = arr2[0][0];
    	var auname = document.getElementById('authName');
    	auname.setAttribute("elementtype", "")
    	var au = document.getElementById('auth');
    	au.setAttribute("verify","")
    }else{
    	fm.auth.value = '1';
    }
}
function initRiskDutyGrid()
{
  var iArray = new Array();

  iArray[0]=new Array();
  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  iArray[0][1]="30px";         			//列宽
  iArray[0][2]=10;          			//列最大值
  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[1]=new Array();
  iArray[1][0]="责任代码";    	//列名
  iArray[1][1]="100px";            		//列宽
  iArray[1][2]=100;            			//列最大值
  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[2]=new Array();
  iArray[2][0]="责任名称";    	//列名
  iArray[2][1]="100px";            		//列宽
  iArray[2][2]=100;            			//列最大值
  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[3]=new Array();
  iArray[3][0]="险种代码";    	//列名
  iArray[3][1]="100px";            		//列宽
  iArray[3][2]=100;            			//列最大值
  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  RiskDutyGrid = new MulLineEnter( "fm" , "RiskDutyGrid" );
  //这些属性必须在loadMulLine前
  RiskDutyGrid.mulLineCount = 0;
  RiskDutyGrid.displayTitle = 0;
  RiskDutyGrid.locked = 0;
  RiskDutyGrid.canSel = 0;
  RiskDutyGrid.canChk = 1;
  RiskDutyGrid.chkBoxEventFuncName = "showRiskDutyFactor";
  RiskDutyGrid.hiddenPlus = 1;
  RiskDutyGrid.hiddenSubtraction = 0;
  RiskDutyGrid.loadMulLine(iArray);
}

// 要约信息列表的初始化
function initContPlanGrid()
{
  var iArray = new Array();

  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="险种名称";    	        //列名
    iArray[1][1]="200px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=3;                       //是否允许输入,1表示允许，0表示不允许 2表示代码选择

    iArray[2]=new Array();
    iArray[2][0]="险种编码";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=150;            			//列最大值
    iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="责任编码";         		//列名
    iArray[3][1]="100px";            		//列宽
    iArray[3][2]= 60;            			//列最大值
    iArray[3][3]= 3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][10]="DutyCode";


    iArray[4]=new Array();
    iArray[4][0]="险种责任";         		//列名
    iArray[4][1]="200px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许


    iArray[5]=new Array();
    iArray[5][0]="计算要素";         		//列名
    iArray[5][1]="150px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=3;
    iArray[5][10]="FactorCode";            			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="要素名称";         		//列名
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=150;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="要素说明";         		//列名
    iArray[7][1]="350px";            		//列宽
    iArray[7][2]=150;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="要素值";         		//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=150;            			//列最大值
    iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][9]="要素值|num";

    iArray[9]=new Array();
    iArray[9][0]="特别说明";         		//列名
    iArray[9][1]="200px";            		//列宽
    iArray[9][2]=150;            			//列最大值
    iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[10]=new Array();
    iArray[10][0]="险种版本";         		//列名
    iArray[10][1]="100px";            		//列宽
    iArray[10][2]=10;            			//列最大值
    iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[11]=new Array();
    iArray[11][0]="集体保单险种号码";         		//列名
    iArray[11][1]="100px";            		//列宽
    iArray[11][2]=10;            			//列最大值
    iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[12]=new Array();
    iArray[12][0]="主险编码";         		//列名
    iArray[12][1]="100px";            		//列宽
    iArray[12][2]=10;            			//列最大值
    iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[13]=new Array();
    iArray[13][0]="类型";         		//列名
    iArray[13][1]="100px";            		//列宽
    iArray[13][2]=10;            			//列最大值
    iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[14]=new Array();
    iArray[14][0]="计算方法";         		//列名
    iArray[14][1]="100px";            		//列宽
    iArray[14][2]=10;            			//列最大值
    iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[15]=new Array();
    iArray[15][0]="总保费";         		//列名
    iArray[15][1]="100px";            		//列宽
    iArray[15][2]=10;            			//列最大值
    iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[16]=new Array();
    iArray[16][0]="缴费计划编码";         		//列名
    iArray[16][1]="100px";            		//列宽
    iArray[16][2]=10;            			//列最大值
    iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[17]=new Array();
    iArray[17][0]="给付责任编码";         		//列名
    iArray[17][1]="100px";            		//列宽
    iArray[17][2]=10;            			//列最大值
    iArray[17][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[18]=new Array();
    iArray[18][0]="账户号码";         		//列名
    iArray[18][1]="100px";            		//列宽
    iArray[18][2]=10;            			//列最大值
    iArray[18][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[19]=new Array();
    iArray[19][0]="险种代码";         		//列名
    iArray[19][1]="100px";            		//列宽
    iArray[19][2]=10;            			//列最大值
    iArray[19][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    ContPlanGrid = new MulLineEnter( "fm" , "ContPlanGrid" );
    //这些属性必须在loadMulLine前
    ContPlanGrid.mulLineCount = 0;
    ContPlanGrid.displayTitle = 1;
    ContPlanGrid.hiddenPlus = 1;
    ContPlanGrid.hiddenSubtraction = 0;
    ContPlanGrid.canChk=0;
    ContPlanGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
function initNationGrid()
{
  var iArray = new Array();

  iArray[0]=new Array();
  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
  iArray[0][1]="30px";         			//列宽
  iArray[0][2]=10;          			//列最大值
  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

  iArray[1]=new Array();
  iArray[1][0]="国家代码";    	//列名
  iArray[1][1]="100px";            		//列宽
  iArray[1][2]=100;            			//列最大值
  iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
  iArray[1][4]="ldnation";
  iArray[1][5]="1|2";
  iArray[1][6]="0|1";
  iArray[1][9]="国家代码|len<=20";
  iArray[1][15]="chinesename";
  iArray[1][17]="2";
  iArray[1][18]=250;

  iArray[2]=new Array();
  iArray[2][0]="国家名称";    	//列名
  iArray[2][1]="100px";            		//列宽
  iArray[2][2]=100;            			//列最大值
  iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许


  NationGrid = new MulLineEnter( "fm" , "NationGrid" );
  //这些属性必须在loadMulLine前
  NationGrid.mulLineCount = 0;
  NationGrid.displayTitle = 1;
  NationGrid.locked = 0;
  NationGrid.canSel = 0;
  NationGrid.canChk = 0;
  NationGrid.hiddenPlus = 0;
  NationGrid.hiddenSubtraction = 0;
  NationGrid.loadMulLine(iArray);
}
function getAgnetToAgentCom(){
	fm.AgentCode.readOnly=true;
	fm.AgentCode.onkeyup="";
	fm.AgentCode.ondblclick="";
	var strSql = "select agentcode from lacomtoagent where agentcom='"+AgentCom+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		if(arr.length>1){
			alert("中介专员不唯一，请重销售部门重新定义！");
			return false;
		}
		fm.AgentCode.value = arr;
		fm.AgentName.value = easyExecSql("select name from laagent where agentcode='"+fm.AgentCode.value+"'");
		fm.AgentGroup.value = easyExecSql("select AgentGroup from laagent where agentcode='"+fm.AgentCode.value+"'");
	}else{
			alert("销售部门没有定义该中介的中介专员，请联系分公司进行定义！");
			return false;
	}
}
</script>
