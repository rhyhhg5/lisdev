<%
//程序名称：
//程序功能：
//创建日期：2007-05-18
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initSearchBox();
        initElementtype();
        initBriefOverseasContInfoGrid();
        initInsuracePlanGrid();
        getNationList();
    }
    catch(e)
    {
        alert("初始化界面错误!");
    }
}

function initSearchBox()
{
    fm.all('ContNo').value = "";
    fm.all('Insured_Name').value = "";
    fm.all('Insured_EnglishName').value = "";
    fm.all('Insured_Sex').value = "";
    fm.all('Insured_Birthday').value = "";
    fm.all('Insured_OthidNo').value = "";
    fm.all('NationCode').value = "";
    fm.all('NationName').value = "";
}

var BriefOverseasContInfoGrid;
function initBriefOverseasContInfoGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号（SerialNumber）";         		//列名
    iArray[0][1]="50px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="团单合同号";         	  //列名
    iArray[1][1]="80px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="合同号码";         	  //列名
    iArray[2][1]="80px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[3]=new Array();
    iArray[3][0]="印刷号（ApplicationNo）";         	
    iArray[3][1]="140px";            	
    iArray[3][2]=200;            		 
    iArray[3][3]=0;              		 
    
    iArray[4]=new Array();
    iArray[4][0]="被保人（NameOfInsured）";         	  //列名
    iArray[4][1]="140px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="护照号码（Passport）";         	  //列名
    iArray[5][1]="120px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="生效日期（ValidFrom）";      //列名
    iArray[6][1]="130px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[7]=new Array();
    iArray[7][0]="终止日期（ValidTo）";              //列名
    iArray[7][1]="120px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="被保人编号";              //列名
    iArray[8][1]="0px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[9]=new Array();
    iArray[9][0]="总承保额（TotalSumOfInsured）";              //列名
    iArray[9][1]="180px";            	//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="保单性质";              //列名
    iArray[10][1]="0px";            	//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    BriefOverseasContInfoGrid = new MulLineEnter("fm", "BriefOverseasContInfoGrid");
    //设置Grid属性
    BriefOverseasContInfoGrid.mulLineCount = 0;
    BriefOverseasContInfoGrid.displayTitle = 1;
    BriefOverseasContInfoGrid.locked = 1;
    BriefOverseasContInfoGrid.canSel = 1;
    BriefOverseasContInfoGrid.canChk = 0;
    BriefOverseasContInfoGrid.hiddenSubtraction = 1;
    BriefOverseasContInfoGrid.hiddenPlus = 1;
    BriefOverseasContInfoGrid.loadMulLine(iArray);
    BriefOverseasContInfoGrid.selBoxEventFuncName = "showContInfo";
  }
  catch(ex) 
  {
    alert(ex);
  }
}

var InsuracePlanGrid;
function initInsuracePlanGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号（SerialNumber）";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="保单号码（PolicyNo）";         	  //列名
    iArray[1][1]="100px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="被保人（NameOfInsured）";         	  //列名
    iArray[2][1]="140px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[3]=new Array();
    iArray[3][0]="险种名称（Product）";         	
    iArray[3][1]="120px";            	
    iArray[3][2]=200;            		 
    iArray[3][3]=0;              		 
    
    iArray[4]=new Array();
    iArray[4][0]="险种英文名称（TheEnglishNameOfTheProducts）";         	  //列名
    iArray[4][1]="260px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="险种责任（Benefit）";         	  //列名
    iArray[5][1]="120px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="险种责任英文名称（TheEnglishNameOfBenefits）";      //列名
    iArray[6][1]="260px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    InsuracePlanGrid = new MulLineEnter("fm", "InsuracePlanGrid");
    //设置Grid属性
    InsuracePlanGrid.mulLineCount = 0;
    InsuracePlanGrid.displayTitle = 1;
    InsuracePlanGrid.locked = 1;
    InsuracePlanGrid.canSel = 0;
    InsuracePlanGrid.canChk = 0;
    InsuracePlanGrid.hiddenSubtraction = 1;
    InsuracePlanGrid.hiddenPlus = 1;
    InsuracePlanGrid.loadMulLine(iArray);
    //InsuracePlanGrid.selBoxEventFuncName = "showContInfo";
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>