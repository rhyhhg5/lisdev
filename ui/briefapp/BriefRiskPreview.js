//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
   fm.fmtransact.value = "INSERT||MAIN" ;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDClassInfo.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDClassInfoQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function queryContDetail(){
	//如果是境外救援则隐掉保额信息
	if(ContType!="2"){
		AmntInfoGridDiv.style.display='none';
	}
	var arr = easyExecSql("select * from lcappnt where contno='"+ContNo+"'");
	if(arr){
		displayLCAppnt(arr);
		fm.appnt_AppntSex.value = getSexBycode(fm.appnt_AppntSex.value);
		fm.appnt_IDType.value = getIDTypeBycode(fm.appnt_IDType.value);
	}
	arr = easyExecSql("select * from lccont where ContNo='"+ContNo+"'");
	if(arr){
		displayLCCont(arr);
	}
	arr = easyExecSql("select * from lcinsured where ContNo = '"+ContNo+"'");
	if(arr){
		displayLCInsured(arr);
		fm.insured_Sex.value = getSexBycode(fm.insured_Sex.value);
		fm.insured_IDType.value = getIDTypeBycode(fm.insured_IDType.value);
	}
	arr = easyExecSql("select Name,Sex,IDType,IDNo from lcbnf where ContNo = '"+ContNo+"'");
	if(arr){
		displayLCBnf(arr);
		fm.bnf_Sex.value = getSexBycode(fm.bnf_Sex.value);
		fm.bnf_IDType.value = getIDTypeBycode(fm.bnf_IDType.value);
	}
	//var strSql = "select a.riskcode,b.riskname,a.CValiDate,a.EndDate - 1 day,case when InsuYearFlag='Y' then rtrim(char(InsuYear))||'年' when InsuYearFlag='M' then rtrim(char(InsuYear))||'月' when InsuYearFlag='D' then rtrim(char(InsuYear))||'日' when InsuYearFlag='A' then '至'||rtrim(char(InsuYear))||'岁' end,a.Copys,case when a.mult >20 then a.amnt else a.mult end,a.prem from lcpol a,lmrisk b "
	//						+"where a.contno='"+ContNo+"' and a.riskcode=b.riskcode";
    
    // 预览保单录入的险种信息。
    var strSql = ""
        + " select a.riskcode,b.riskname,a.CValiDate,a.EndDate - 1 day, "
        + " case "
        + " when InsuYearFlag='Y' then rtrim(char(InsuYear))||'年' "
        + " when InsuYearFlag='M' then rtrim(char(InsuYear))||'月' "
        + " when InsuYearFlag='D' then rtrim(char(InsuYear))||'日' "
        + " when InsuYearFlag='A' then '至'||rtrim(char(InsuYear))||'岁' "
        + " end, "
        + " a.Copys, "
        + " case when a.mult = 0 then a.amnt else a.mult end, "
        + " a.prem, "
        + " case "
        + " when PayEndYearFlag='Y' then rtrim(char(PayEndYear))||'年' "
        + " when PayEndYearFlag='M' then rtrim(char(PayEndYear))||'月' "
        + " when PayEndYearFlag='D' then rtrim(char(PayEndYear))||'日' "
        + " when PayEndYearFlag='A' then '至'||rtrim(char(PayEndYear))||'岁' "
        + " end "
        + " from lcpol a,lmrisk b "
        + " where a.riskcode=b.riskcode "
        + " and a.contno='" + ContNo + "' "
        ;
    // ------------------------------------
    
	turnPage.queryModal(strSql,RiskDutyGrid);
	if(ContType==4){
		ManageInfoDiv.style.display='none';
		table1.style.display='none';
		CardDiv.style.display='';
		showCardInfo();
	}
	if(fm.all('ManageCom').value.length>=4){
      	if(fm.all('ManageCom').value.substring(0,4)=='8644'){
      	    fm.all("AudioAndVideo").style.display="";
      	    fm.VideoFlag.value=tVideoFlag;
      	}
    }
}
function getSexBycode(cCode){
	
	var strSql = "select codename from ldcode where codetype='sex' and code ='"+cCode+"'";
	return easyExecSql(strSql);
}
function getIDTypeBycode(cCode){
	
	if(cCode == null || cCode == "")
	{
		return "";
	}
  else
	{	
	  var strSql = "select codename from ldcode where codetype='idtype' and code ='"+cCode+"'";
	
	  return easyExecSql(strSql);
  }
}

function ShowFiveAmnt()
{
	var tSql = "select distinct A Amnt1,B Amnt2,C Amnt3,D Amnt4,E Amnt5 from ( 	select (select sum(standmoney) A from lcget where a.polno=polno and getdutycode in ('218201')), 		(select sum(standmoney) B from lcget where a.polno=polno and getdutycode in ('218203')), 		(select sum(standmoney) C from lcget where a.polno=polno and getdutycode in ('218205')), 		(select sum(standmoney) D from lcget where a.polno=polno and getdutycode in ('218207')), 		(select sum(standmoney) E from lcget where a.polno=polno and getdutycode in ('218210'))	from LCPol a where a.ContNo = '"+ContNo+"'  ) as x "
	turnPage.queryModal(tSql,AmntGrid);
}
//从卡单 读出险种信息
function showCardInfo(){
	var tCardNo = fm.PrtNo.value;
	fm.CardNo.value = tCardNo;
	var strSql = "select * from lzcard a,lzcardnumber b where a.subcode=b.cardtype and a.startno=b.cardserno and cardno='"+fm.PrtNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		var SubCode = arr[0][1];
		var CertifyCode = arr[0][0];
		fm.CardType.value = easyExecSql("select CertifyName from LMCertifyDes where  CertifyCode='"+CertifyCode+"'");
		fm.ReleaseDate.value = arr[0][12];
		var cardRiskInfo = easyExecSql("select a.riskcode,a.prem,a.PremLot,a.Mult,b.riskname from lmcardrisk a,lmriskapp b where a.riskcode=b.riskcode and CertifyCode='"+CertifyCode+"'");
		if(cardRiskInfo){
			fm.CardRiskCode.value = cardRiskInfo[0][0]+"-"+cardRiskInfo[0][4];
			if(cardRiskInfo[0][3] > 0 ){
				fm.CardAmnt.value = cardRiskInfo[0][3] +"档";
			}else{
				fm.CardAmnt.value = cardRiskInfo[0][2] +"元";
			}
			fm.CardPrem.value = cardRiskInfo[0][1];
		}
	}
}

/**
 * 校验销售渠道与产品关联
 */
function checkSaleChnlRisk()
{   
    var tPrtNo = fm.PrtNo.value;
    var saleChannel = null;
    
    var arr = easyExecSql("select salechnl from lccont where prtno = '" + tPrtNo + "' ");
    if(arr)
    {
        saleChannel = arr[0][0];
    }
    
    if(saleChannel != null && saleChannel == "13")
    {
        var tStrSql = " select 1 "
            + " from LCPol lcp "
            + " left join LDCode1 ldc1 on lcp.SaleChnl = ldc1.Code and lcp.RiskCode = ldc1.code1 and ldc1.codetype = 'checksalechnlrisk' "
            + " where 1 = 1 "
            + " and ldc1.CodeType is null "
            + " and lcp.PrtNo = '" + tPrtNo + "' "
            ;
        var result = easyExecSql(tStrSql);
        
        if(result)
        {
            alert("该单为银代直销保单，但存在非银代直销产品，请核实。");
            return false;
        }
    }
    
    return true;
}


function endInput(cActivityID)
{
    //银保渠道与险种校验
//    if(!checkSaleChnlRisk())
//    {
//        return false;
//    }
	
    //校验简易个险集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }

    
    var tPrtNo = fm.PrtNo.value;
    if(!chkAppAcc(tPrtNo))
    {
        return false;
    }
    //zxs 20190521
    if(!checkReAppDate()){
    	return false;
    }
    //#1868
    var qurSql="select 1 from LJSPay where OtherNo = '"+fm.PrtNo.value+"' and OtherNoType in ('9','16') and BankOnTheWayFlag = '1' ";
    var arrqurSql=easyExecSql(qurSql);
    if(arrqurSql){
       alert("目前保单为银行在途状态，不能进行录入完毕操作，请财务确认回盘后再次操作。");
       return;
    }
        
    //校验广东长期险是否调阅录音录像
    if(!checkVideo()){
        return false;
    }
    
    if(!checkAppandInsu()){
    	return false;
    }
    
    top.opener.BriefInputConfirm(cActivityID, ContType,fm.VideoFlag.value);
    top.close();
}

//校验简易个险集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   add by 赵庆涛 2016-06-28

function checkCrsBussSaleChnl()
{
	 var Crs_SaleChnl = null;  
	 var arr = easyExecSql("select Crs_SaleChnl from lccont where prtno = '" + fm.PrtNo.value + "' ");
	 if(arr){
		 Crs_SaleChnl = arr[0][0];
	    }
	if(Crs_SaleChnl != null && Crs_SaleChnl != "")
	{
		var strSQL = "select 1 from lccont lgc where 1 = 1"  
			+ " and lgc.Crs_SaleChnl is not null " 
			+ " and lgc.Crs_BussType = '01' " 
			+ " and not (lgc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lgc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
			+ " and lgc.PrtNo = '" + fm.PrtNo.value + "' ";
		var arrResult = easyExecSql(strSQL);
        if (arrResult != null)
        {
        	alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
        	return false;
        }
	}
	return true;
}

/**
 * 校验投保人帐户余额是否足额
 * 不通过约定：
 * 1、该投保单中“缴费模式”选择“01 - 从投保人帐户缴纳首期保费”；并且投保人帐户中可用剩余金额，小于该单首期保费。
 * 2、该投保单中“缴费模式”选择“01 - 从投保人帐户缴纳首期保费”；并且帐户信息出现多条。
 */
function chkAppAcc(cPrtNo)
{
    var tStrSql = ""
        + " select "
        + " tmp.ContNo, tmp.AppntNo, tmp.AppntName, tmp.SumPrem, nvl(sum(lcaa.AccGetMoney), 0) SumAccGetMoney "
        + " from "
        + " ( "
        + " select "
        + " lcc.ContNo, lcc.AppntNo, lcc.AppntName, nvl(sum(lcp.Prem), 0) SumPrem  "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcc.ContNo = lcp.ContNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.PayMethod = '01' "
        + " and lcc.PrtNo = '" + cPrtNo + "' "
        + " group by lcc.ContNo, lcc.AppntNo, lcc.AppntName "
        + " ) as tmp "
        + " left join LCAppAcc lcaa on lcaa.CustomerNo = tmp.AppntNo and lcaa.State = '1' "
        + " group by tmp.ContNo, tmp.AppntNo, tmp.AppntName, tmp.SumPrem "
        ;
    var arrAppAccInfo = easyExecSql(tStrSql);
    if(arrAppAccInfo != null)
    {
        if(arrAppAccInfo.length >= 2)
        {
            alert("投保人帐户信息出现异常。");
            return false;
        }
        var tAppAccInfo = arrAppAccInfo[0];
        var tSumPrem = tAppAccInfo[3];
        var tSumAccGetMoney = tAppAccInfo[4];
        if(Subtr(tSumPrem, tSumAccGetMoney) > 0)
        {
            alert("该单投保人帐户可用余额不足。该单首期保费[" + tSumPrem + "]，投保人帐户余额[" + tSumAccGetMoney + "]");
            return false;
        }
    }
    
    return true;
}

function Subtr(arg1, arg2)
{
    var r1, r2, m, n;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    n=(r1>=r2)?r1:r2;
    return ((arg1*m-arg2*m)/m).toFixed(n);
}

function getAudioAndVideo(){
  window.open("http://10.214.4.143:9001/prpcard/video/"+trim(fm.PrtNo.value)+".rar","window1");
  fm.VideoFlag.value="1"; 
}

function checkVideo(){
   var videoSql="select 1 from lcpol lcp,lmriskapp lm,lccont lcc where lcc.contno=lcp.contno and lcc.managecom like '8644%' "
               +"and lcc.contno='"+ ContNo + "' and lcp.riskcode=lm.riskcode and Year(lcc.PolApplyDate - lcc.AppntBirthday)>=60 and lm.RiskPeriod='L' ";
   var result = easyExecSql(videoSql);
   if(result&&fm.VideoFlag.value!='1'){
      alert("请进行录音录像调阅！");
      return false;
   }
   return true;
}

function checkAppandInsu(){
	var videoSql="select cad.email,cad.homecode,cad.homenumber,cad.homephone,cad.mobile  from lccont cc inner join lcappnt ca on ca.appntno=cc.appntno and ca.contno=cc.contno " +
				"inner join lcaddress cad on cad.customerno=ca.appntno and cad.addressno=ca.addressno where cc.prtno='" + fm.PrtNo.value + "'";
	var result = easyExecSql(videoSql);
	var email = result[0][0];
	var homecode = result[0][1];
	var homenumber = result[0][2];
	var homephone = result[0][3];
	var mobile = result[0][4];
	
	if(!isNull(email)){
		   var checkemail =CheckEmail(email);
		   if(checkemail !=""){
			   alert("投保人"+checkemail);
			   return false;
		   }
	   }

	if(!chenkHomePhone(1,homecode,homenumber)){
		  return false;
	}
	if(!isNull(homephone)){
		   var checkhomephone =CheckFixPhone(homephone);
		   if(checkhomephone !=""){
			   alert("投保人"+checkhomephone);
			   return false;
		   }
	   }
	if(!isNull(mobile)) {
		if(!isInteger(mobile) || mobile.length != 11){
			alert("投保人移动电话需为11位数字，请核查！");
	   		return false;
		}			
	}

	if(!checkMobile(1,mobile,'')){
	   return false;
	}
	//被保人部分
	videoSql = "select cad.email,cad.homephone,cad.mobile from lcinsured ca "
        + "inner join lcaddress cad on cad.customerno=ca.insuredno and cad.addressno=ca.addressno "
        + " where ca.prtno='" + fm.PrtNo.value + "'";
	result = easyExecSql(videoSql);
	email = result[0][0];
	homephone = result[0][1];
	mobile = result[0][2];

	if(!isNull(email)){
		   var checkemail =CheckEmail(email);
		   if(checkemail !=""){
			   alert("被保人"+checkemail);
			   return false;
		   }
	 }
	if(!isNull(homephone)){
		   var checkhomephone =CheckFixPhone(homephone);
		   if(checkhomephone !=""){
			   alert("被保人"+checkhomephone);
			   return false;
		   }
	 }
	if(!isNull(mobile)) {
		if(!isInteger(mobile) || mobile.length != 11){
			alert("被保人移动电话需为11位数字，请核查！");
	   		return false;
		}			
	}
	if(!checkMobile(2,mobile,'')){
	   return false;
	}
	
	return true;
}
function checkMobile(type,mobile,name){
	   var str = "";
	   if(type=="1"){
	      str="投保人";
	   }else{
	      str="被保人:"+name;
	   }
	   if(mobile != "" && mobile != null){
	      if(mobile.length!=11){
	          alert(str+"移动电话不符合规则，请核实！");
	          return false;
	      }else{
	         if(mobile.substring(0,2)!=13&&mobile.substring(0,2)!=16&&mobile.substring(0,2)!=14&&mobile.substring(0,2)!=15&&mobile.substring(0,2)!=18&&mobile.substring(0,2)!=17&&mobile.substring(0,2)!=19){
	            alert(str+"移动电话不符合规则，请核实！");
	            return false;
	         }
	      }
	   }else if(type=="1"){
	      if(!confirm("投保人移动电话为空，是否继续？")){ 
	         return false;
		  }
	   }
	   return true;
}
function chenkHomePhone(type,homecode,homenumber){
    var str = "";
    if(type=="1"){
       str="投保人";
    }else{
       str="被保人";
    }
   if(homecode == "" || homecode == null){
        if(homenumber != "" && homenumber != null){
           alert(str+"固定电话区号未进行录入！");
           return false;
        }
   }else{
       if(homecode.length!=3 && homecode.length!=4||!isInteger(homecode)){
           alert(str+"固定电话区号录入有误，请检查！");
           return false;
       }
       if(homenumber == "" || homenumber == null){
           alert(str+"固定电话号码未进行录入！");
           return false;
       }
       if(homenumber.length!=7&&homenumber.length!=8&&homenumber.length!=10||!isInteger(homenumber)){
           alert(str+"固定电话号码录入有误，请检查！");
           return false;
       }else{
          if(homenumber.length==10){
              if(homenumber.substring(0, 3)!=400&&homenumber.substring(0, 3)!=800){
                 alert(str+"固定电话号码录入有误，请检查！");
                 return false;
              }
          }
       }
   }
   return true;
}
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}

//zxs 20190521
function checkReAppDate(){
	var prtno = fm.PrtNo.value;
	var daysql = "select code from ldcode where  codetype = 'checkpolapplydate' ";
	var dayresult = easyExecSql(daysql);
	var sql = "select case when polApplyDate between current date -"+dayresult+" day and  current date  then 1 else 0 end from lccont where prtno = '"+prtno+"' ";
	var result = easyExecSql(sql);
	if(result!=null&&result!=""&&result=='0'){
		alert("投保申请日期录入有误，不得晚于当前日期，且不得早于当前日期-"+dayresult+"天。");
		return false;
	}

	var sql1 = "select case when receiveDate is null then 1 when receiveDate between current date -"+dayresult+" day and  current date  then 1 else 0 end from lccont where prtno = '"+prtno+"' ";
	var result1 = easyExecSql(sql1);
	if(result1=='0'){
			alert("投保收单日期录入有误，不得晚于当前日期，且不得早于当前日期-"+dayresult+"天。");
			return false;
	  }
	return true;
}