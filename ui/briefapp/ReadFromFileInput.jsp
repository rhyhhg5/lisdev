<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ReadFromFileInput.jsp
//程序功能：
//创建日期：2007-6-13 18:29
//创建人  ：NIcolE
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head > 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="ReadFromFileInput.js"></SCRIPT>
  <%@include file="ReadFromFileInit.jsp"%>
  
  <title>回盘文件导入</title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  comCode = "<%=tGlobalInput.ComCode%>";  
</script>

<body  onload="initForm();" >
<form action="./ReadFromFileSave.jsp" ENCTYPE="multipart/form-data" method=post name=fm target="fraTitle" >
      
  <table  class= common>
    <TR  class= common>     
 			<TD  class= title>
        银行代码
      </TD>
      <TD  class= input>
        <Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank" ><input class=codename name=BankCodeName readonly=true elementtype="nacessary">
      </TD>
      <TD  class= title>
         文件名称
      </TD>
      <TD  class= input>
        <Input class= common TYPE=file name=FileName verify="文件名称|notnull" > 
      </TD>       
       <TD  class= input>
        <INPUT VALUE=" 读 取 文 件 " class= cssbutton TYPE=button onclick="submitForm()">
      </TD>     
    </TR>
    </table>
    <br>
    <table class= common>
    <TR  class= common>     
      <TD  class= title>
        银行代码 
      </TD>
      <TD  class= input>
        <Input NAME=getbankcodename CodeData="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('getbankcode1',[this,getbankcode],[0,1],null,null,null,null,'50%');" onkeyup="return showCodeListKey('getbankcode1',[this,getbankcode],[0,1]);" verify="银行代码|notnull" ><input class=codename name=getbankcode readonly=true TYPE=hidden>
      </TD>
      <TD  class= title>
         银行账号
      </TD>
      <TD  class= input>
        <input NAME="getbankaccno" VALUE MAXLENGTH="20" CLASS="code" elementtype=nacessary ondblclick="return showCodeList('getbankaccno',[this],[0],null,fm.getbankcode.value,'BankCode');" onkeyup="return showCodeListKey('getbankaccno',[this],[0],null,fm.getbankcode.value,'BankCode');" verify="银行账号|notnull" >
      </TD>   
      <TD  class= input>
      </TD>         
    </TR>
    </table>
    
  </form>    
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

