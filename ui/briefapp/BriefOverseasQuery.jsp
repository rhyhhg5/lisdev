<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2007-05-18
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GB2312" %>
<head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
    <SCRIPT src="BriefOverseasQuery.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <LINK href="../common/css/mulLine.css" rel="stylesheet" type="text/css">
    <%@include file="BriefOverseasQueryInit.jsp"%>
</head>

<body onload="initForm();" >
    <form action="#;" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" />
                </td>
                <td class="titleImg">查询条件（Query Condition）</td>
            </tr>
        </table>
        <div id="divSearchInfo" style= "display:''">
            <table class="common" align='center' >
                <tr class="common">
                    <td class="title" style='display:none;'>保险合同号（Policy NO.）</td>
                    <td class="input" style='display:none;'><input class="common" name="ContNo" /></td>
                    <td class="title">类别（Species）</td>
                    <td class="input"><Input class="codeno" name="ContType" CodeData="0|^1|一类^2|二类" ondblclick="return showCodeListEx('ContTypeFlag',[this,ContTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContTypeFlag',[this,ContTypeName],[0,1],null,null,null,1);" verify="类别|notnull" readonly="readonly" /><Input class="codename" readonly="readonly" name="ContTypeName" /></td>
                </tr>
                <tr class="common">
                    <td class="title">被保人（Name Of Insured）</td>
                    <td class="input"><input class="common" name="Insured_Name" /></td>
                    <td class="title">拼音（EnglishName Of Insured）</td>
                    <td class="input"><input class="common" name="Insured_EnglishName" /></td>
                </tr>
                <tr>
                    <td class="title" style='display:none;'>性别（Gender）</td>
                    <td class="input" style='display:none;'>
                        <Input class="codeno" name="Insured_Sex" CodeData="0|^0|男-Man^1|女-Woman" ondblclick="return showCodeListEx('SexFlag',[this,Insured_Sex_Name],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('SexFlag',[this,Insured_Sex_Name],[0,1],null,null,null,1);" readonly="readonly" /><Input class="codename" readonly="readonly" name="Insured_Sex_Name" />
	                </td>
                    <td class="title">出生日期（Birthday）</td>
                    <td class="input"><input class="coolDatePicker" name="Insured_Birthday" verify="出生日期（Birthday）|date" /></td>
                </tr>
                <tr class="common">
                    <td class="title">护照号码（Passport NO.）</td>
                    <td class="input"><input class="common" name="Insured_OthidNo" /></td>
                    <td class="title">抵达国家（Destinnation Coutry）</td>
                    <td class="input"><input class="codeno" name="NationCode" CodeData="" ondblclick="return showCodeListEx('NationFlag',[this,NationName],[0,1],null,null,null,1,280);" onkeyup="return showCodeListKeyEx('NationFlag',[this,NationName],[0,1],null,null,null,1,280);" /><input class="codename" name="NationName" readonly="readonly" /></td>
                </tr>
            </table>
        </div>
        <input value="查  询" title="Query" class="cssButton" type="button" onclick="easyQueryClick();">
        <p />

        <!-- 查询结果部分 -->
        <table>
            <tr>
                <td class="common"><IMG  src= "../common/images/butExpand.gif"></td>
                <td class="titleImg">合同信息（Policy Information）</td>
            </tr>
        </table>
        <!-- 信息（列表） -->
        <div id="divBriefOverseasContInfoGrid" style="display:''">
            <span id="spanBriefOverseasContInfoGrid"></span>
        </div>
        <div id= "divPage" align="center" style= "display:''">
            <input class="cssButton" value="首  页" title="The First Page" type="button" onclick="turnPage1.firstPage();" /> 
            <input class="cssButton" value="上一页" title="Back" type="button" onclick="turnPage1.previousPage();" /> 					
            <input class="cssButton" value="下一页" title="Next" type="button" onclick="turnPage1.nextPage();" /> 
            <input class="cssButton" value="尾  页" title="The Last Page" type="button" onclick="turnPage1.lastPage();" />
        </div>
        <p />
        
        <!-- 查询结果部分 -->
        <div id="divShowInfo" style="display:'none'">
            <table>
                <tr>
                    <td class="common"><IMG  src= "../common/images/butExpand.gif"></td>
                    <td class="titleImg">合同相关信息（The Information Related To The Policy）</td>
                </tr>
            </table>
        
            <table class="common" align="center" >
                <tr class="common" style="display:none;">
                    <td class="title">保险合同号<br />（Policy NO.）</td>
                    <td class="input"><input class="readonly" name="R_PolicyNO" readonly="readonly" /></td>
                    <td class="title"></td>
                    <td class="input"></td>
                </tr>
                <tr class="common">
                    <td class="title">被保人<br />（Name Of Insured）</td>
                    <td class="input"><input class="readonly" name="R_Insured_Name" readonly="readonly" /></td>
                    <td class="title">拼音<br />（EnglishName Of Insured）</td>
                    <td class="input"><input class="readonly" name="R_Insured_EnglishName" readonly="readonly" /></td>
                </tr>
                <tr>
                    <td class="title">性别<br />（Gender）</td>
                    <td class="input">
                        <Input class="readonly" name="R_Insured_Sex" readonly="readonly" />
	                </td>
                    <td class="title">出生日期<br />（Birthday）</td>
                    <td class="input"><input class="readonly" name="R_Insured_Birthday" /></td>
                </tr>
                <tr class="common">
                    <td class="title">护照号码<br />（Passport NO.）</td>
                    <td class="input"><input class="readonly" name="R_Insured_OthidNo" readonly="readonly" /></td>
                    <td class="title"></td>
                    <td class="input"></td>
                </tr>
                <tr class="common">
                    <td class="title">抵达国家<br />（Destination Country）</td>
                    <td class="input" colspan="3"><input class="readonly" name="R_Insured_ToNation" style="width:100%;" readonly="readonly" /></td>
                </tr>
            </table>
            <input type="hidden" name="R_PolicyContType" />
            <p />
                
            <!-- 信息（列表） -->
            <table>
                <tr>
                    <td class="common"><IMG  src= "../common/images/butExpand.gif"></td>
                    <td class="titleImg">保障信息名称（Insurace Plan）</td>
                </tr>
            </table>
            <div id="divInsuracePlanGrid" style="display:''">
                <span id="spanInsuracePlanGrid"></span>
            </div>
                <div id= "divPage2" align="center" style= "display:''">
                <input class="cssButton" value="首  页" title="The First Page" type="button" onclick="turnPage2.firstPage();" /> 
                <input class="cssButton" value="上一页" title="Back" type="button" onclick="turnPage2.previousPage();" /> 					
                <input class="cssButton" value="下一页" title="Next" type="button" onclick="turnPage2.nextPage();" /> 
                <input class="cssButton" value="尾  页" title="The Last Page" type="button" onclick="turnPage2.lastPage();" />
            </div>
            <p />
        </div>
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>