//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
function submitForm()
{
  fm.fmtransact.value = "CREAT||SQL" ;
  dealSql();
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDClassInfo.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           

function dealSql(){
	fm.Sql.value = " select a.grpcontno,a.signdate,a.signcom,a.cvalidate,a.CInValiDate,b.riskcode,b.mult," +
          " ( select d1.standmoney from lcinsured a1, lcgrpcont b1, lcpol c1, lcget d1 where a1.grpcontNo=a.grpcontNo and a1.insuredno=e.insuredno and b1.grpcontNo=a1.grpcontNo and c1.grpcontNo=a1.grpcontNo and c1.insuredno=a1.insuredno and c1.riskcode='1502' and d1.grpcontNo=a1.grpcontNo and d1.polno=c1.polno and d1.getdutycode='613207' )," +
          " (select d2.standmoney from lcinsured a2, lcgrpcont b2, lcpol c2, lcget d2 where a2.grpcontNo=a.grpcontNo and a2.insuredno=e.insuredno and b2.grpcontNo=a2.grpcontNo and c2.grpcontNo=a2.grpcontNo and c2.insuredno=a2.insuredno and c2.riskcode='1502' and d2.grpcontNo=a2.grpcontNo and d2.polno=c2.polno and d2.getdutycode='613208')," +
          " (select d3.standmoney from lcinsured a3, lcgrpcont b3, lcpol c3, lcget d3 where a3.grpcontNo=a.grpcontNo and a3.insuredno=e.insuredno and b3.grpcontNo=a3.grpcontNo and c3.grpcontNo=a3.grpcontNo and c3.insuredno=a3.insuredno and c3.riskcode='1502'and d3.grpcontNo=a3.grpcontNo and d3.polno=c3.polno and d3.getdutycode='613209' )," +
          " b.insuredname,b.insuredno,e.EnglishName,e.Birthday,e.OthIDNo," +
          " d.linkmanaddress1,d.linkmanzipcode1,d.linkman1,d.phone1 from lcgrpcont a, lcpol b,lcgrpappnt c, lcgrpaddress d, lcinsured e " +
          " where a.standbyflag1 is null and a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno and c.CustomerNo=d.CustomerNo and c.AddressNo=d.AddressNo" +
          " and a.grpcontno=e.grpcontno and a.cardflag='0' and b.riskcode ='1502' and b.insuredno=e.insuredno and a.appflag='1' and a.grpcontno=e.grpcontno and b.contno=e.contno" +
          " union all " +
          " select a.grpcontno,a.signdate,a.signcom,a.cvalidate,a.CInValiDate,b.riskcode,b.mult," +
          " ( select d1.standmoney from lcinsured a1, lcgrpcont b1, lcpol c1, lcget d1 where a1.grpcontNo=a.grpcontNo and a1.insuredno=e.insuredno and b1.grpcontNo=a1.grpcontNo and c1.grpcontNo=a1.grpcontNo and c1.insuredno=a1.insuredno and c1.riskcode='1501' and d1.grpcontNo=a1.grpcontNo and d1.polno=c1.polno and d1.getdutycode='611207' )," +
          " 0," +
          " 0," +
          " b.insuredname,b.insuredno,e.EnglishName,e.Birthday,e.OthIDNo," +
          " d.linkmanaddress1,d.linkmanzipcode1,d.linkman1,d.phone1 from lcgrpcont a, lcpol b,lcgrpappnt c, lcgrpaddress d, lcinsured e " +
          " where a.standbyflag1 is null and a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno and c.CustomerNo=d.CustomerNo and c.AddressNo=d.AddressNo" +
          " and a.grpcontno=e.grpcontno and a.cardflag='0' and b.riskcode='1501' and b.insuredno=e.insuredno and a.appflag='1' and a.grpcontno=e.grpcontno and b.contno=e.contno";
}