var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

function returnParent()
{
    var tSel = -1;
    if (ComGrid.mulLineCount == 0)
    {
        alert("请先查询信息！");
        return false;
    }
    else if (ComGrid.mulLineCount == 1)
            tSel = 1;
        else
            tSel = ComGrid.getSelNo();
    var arrReturn = new Array();
    if( tSel == -1 || tSel == 0)
        alert( "请先选择一条记录，再点击返回按钮。" );
    else
    {
        try
        {
            arrReturn = getQueryResult();
            top.opener.afterAgentComQuery(arrReturn);
        }
        catch(ex)
        {
            alert( "没有发现父窗口的 afterAgentComQuery 接口。" + ex );
        }
        top.close();
    }
}

function getQueryResult()
{
    var arrSelected = new Array();
    var tRow = ComGrid.getSelNo();
    if (ComGrid.mulLineCount == 1)
        tRow = 1;
    if( tRow == 0 || tRow == null )
        return arrSelected;

    var	ResultSQL = "select AgentCom,Name,Address,Phone,LinkMan,GrpNature,BankType,SellFlag,ManageCom,AreaType,ACType "
        + "from lacom where agentcom='" + ComGrid.getRowColData(tRow-1,1) + "'";
    turnPage.strQueryResult = easyQueryVer3(ResultSQL, 1, 0, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult) {
        alert("没有满足条件的记录！");
        return false;
    }
    //查询成功则拆分字符串，返回二维数组
    arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
    return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
    // 书写SQL语句
    var strSQL = "select a.AgentCom,a.Name,a.Phone from LACom a where 1=1 "
        + getWherePart('a.ManageCom','ManageCom','like')
        + getWherePart('a.AgentCom','AgentCom','like')
        + getWherePart('a.BankType','BankType')
        + getWherePart('a.SellFlag','SellFlag')
        +" order by AgentCom";
    turnPage.queryModal(strSQL,ComGrid,0,1);
    if (ComGrid.mulLineCount == 0)
    {
        alert("没有查询到相应的代理机构信息！");
        return false;
    }
}
