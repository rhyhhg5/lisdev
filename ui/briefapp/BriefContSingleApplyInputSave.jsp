<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BriefContApplyInputSave.jsp
//程序功能：简易投保
//创建日期：2005-09-05 11:10:36
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
System.out.println("Auto-begin:");
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//输出参数
CErrors tError = null;
String FlagStr = "Succ";
String Content = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
if(tG == null)
{
  System.out.println("session has expired");
  return;
}
String tActivityID = request.getParameter("ActivityID");
VData tVData = new VData();
TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("PrtNo",request.getParameter("PrtNo"));
tTransferData.setNameAndValue("ManageCom",request.getParameter("ManageCom"));
tTransferData.setNameAndValue("InputDate",PubFun.getCurrentDate());
tTransferData.setNameAndValue("Operator",tG.Operator);
tTransferData.setNameAndValue("ContType",request.getParameter("ContType"));
System.out.println("ContType"+request.getParameter("ContType"));
tVData.add( tTransferData );
tVData.add( tG ); 
System.out.println("开始进入后台");
BriefTbWorkFlowUI tBriefTbWorkFlowUI = new BriefTbWorkFlowUI();
if (tBriefTbWorkFlowUI.submitData(tVData,tActivityID) == false)
{
  int n = tBriefTbWorkFlowUI.mErrors.getErrorCount();
  System.out.println("n=="+n);
  for (int j = 0; j < n; j++)
    System.out.println("Error: "+tBriefTbWorkFlowUI.mErrors.getError(j).errorMessage);
  Content = " 投保单申请失败，原因是: " + tBriefTbWorkFlowUI.mErrors.getError(0).errorMessage;
  FlagStr = "Fail";
}
//如果在Catch中发现异常，则不从错误类中提取错误信息
if ("Succ".equals(FlagStr))
{
  tError = tBriefTbWorkFlowUI.mErrors;
  //tErrors = tBriefTbWorkFlowUI.mErrors;
  Content = " 投保单申请成功! ";
  if (!tError.needDealError())
  {
    int n = tError.getErrorCount();
    if (n > 0)
    {
      for(int j = 0;j < n;j++)
      {
        //tError = tErrors.getError(j);
        Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
      }
    }

    FlagStr = "Succ";
  }
  else
  {
    int n = tError.getErrorCount();
    if (n > 0)
    {
      for(int j = 0;j < n;j++)
      {
        //tError = tErrors.getError(j);
        Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
      }
    }
    FlagStr = "Fail";
  }
}

%>
<html>
<script language="javascript">
//top.close();
//alert("<%=Content%>");
//top.close();
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
