var showInfo;
var mDebug="0";
var manageCom;
var turnPage = new turnPageClass();
var arrDataSet;
parent.fraMain.rows = "0,0,0,0,*";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,0,0,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,0,*";
	}
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = ContGrid.getSelNo();

	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
function easyQueryClick(){
	var PrtNo = fm.PrtNo.value;
	var ContNo = fm.ContNo.value;
	var InsuredName = fm.InsuredName.value;
	if(PrtNo==""&&ContNo==""&&InsuredName==""){
		alert("请输入查询条件！");
		return false;
	}
	// 初始化表格
	initContGrid();
	//加入管理机构查询限制
	var strManageComWhere = " AND ManageCom LIKE '" + manageCom + "%%' ";
	if( fm.BranchGroup.value != '' ) {
		strManageComWhere += " AND AgentGroup IN ( SELECT AgentGroup FROM LABranchGroup WHERE BranchAttr LIKE '" + fm.BranchGroup.value + "%%') ";
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL = "SELECT ContNo,PrtNo,Prem,insuredname,CValiDate,PrintCount FROM LCCont A"
		+ " WHERE AppFlag in ('1','9')  and cardflag='b' and uwflag in ('4','9') "
		+ getWherePart( 'PrtNo' )
		+ getWherePart( 'ContNo' )
		+ getWherePart( 'CValiDate','CValiDate' )
		+ getWherePart( 'InsuredName','InsuredName','like' )
		+ strManageComWhere
		+ " order by ManageCom,AgentGroup,AgentCode";

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("未查询到满足条件的数据！");
		return false;
	}
	//设置查询起始位置
	turnPage.pageIndex = 0;
	//在查询结果数组中取出符合页面显示大小设置的数组
	turnPage.pageLineNum = 10 ;
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = ContGrid;
	//保存SQL语句
	turnPage.strQuerySql     = strSQL ;
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}


//提交，保存按钮对应操作
function printContPol()
{
	var i = 0;
	var flag = 0;
	var rowNo = 0;
	flag = 0;
	//判定是否有选择打印数据
	for( i = 0; i < ContGrid.mulLineCount; i++ )
	{
		if (ContGrid.getSelNo(i))
		{
			flag = 1;
			break;
		}
	}
	//如果没有打印数据，提示用户选择
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	rowNo = ContGrid.getSelNo();
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=EC01&OtherNo="+ContGrid.getRowColData(rowNo-1,1);
	fm.submit();
	
}
//提交，保存按钮对应操作
function printContPolCard()
{
	var i = 0;
	var flag = 0;
	var rowNo = 0;
	flag = 0;
	//判定是否有选择打印数据
	for( i = 0; i < ContGrid.mulLineCount; i++ )
	{
		if (ContGrid.getSelNo(i))
		{
			flag = 1;
			break;
		}
	}
	//如果没有打印数据，提示用户选择
	if( flag == 0 )
	{
		alert("请先选择一条记录，再打印保单");
		return false;
	}
	rowNo = ContGrid.getSelNo();
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=J02&OtherNo="+ContGrid.getRowColData(rowNo-1,1);
	fm.submit();
	
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	//showInfo.close();
	//无论打印结果如何，都重新激活打印按钮
	fm.all("printButton").disabled=false;
	if (FlagStr == "Fail" )
	{
		//如果失败，则返回错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		//如果提交成功，则执行查询操作
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
}

function queryBranch()
{
	showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
	if(arrResult!=null)
	{
		fm.BranchGroup.value = arrResult[0][3];
	}
}

function dealInitPrint(){
	if(mPrtNo!=null && mPrtNo!="" && mPrtNo!="null"){
		fm.PrtNo.value = mPrtNo;
		easyQueryClick();
		if(ContGrid.mulLineCount==1){
			//ContGrid.checkBoxSel(1);
			try{
				fm.all('InpContGridSel').value=1;
	      fm.all('ContGridSel').checked=true;
	    }catch(ex){
	    	alert(ex.message);
	    }
	    //printGroupPol();
		}
	}
}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}