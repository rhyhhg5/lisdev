<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHHmBespeakManageInput.jsp
//程序功能：健管服务预约管理
//创建日期：2006-09-01 15:50:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="LHHmBespeakManageInput.js"></SCRIPT>
  <%@include file="LHHmBespeakManageInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHHmServBeManageGrid);">
    		</td>
    		<td class= titleImg>
        	服务事件任务信息
       	</td>
    	</tr>
    </table>
	<Div  id= "divLHHmServBeManageGrid" style= "display: ''">
	 		<Div id="LHCaseInfo" style="display:'' ">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHHmServBeManageGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>
	</Div>
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>

	<Div  id= "divLHTaskInfoGrid" style= "display: 'none'">
			<table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHTaskInfoGrid);">
    		</td>
    		<td class= titleImg>
        	服务任务状态信息

    	</tr>
    </table>
		<table  class= common align='center'>
			<TR  class= common>
				  <TD class= title >
        		 服务任务状态
       	</TD>   		 
				<TD  class= input>
				 <Input class= 'codename' style="width:40px" name=ServTaskState VALUE="2"><Input class= 'codeno' style="width:120px" VALUE="正在执行" name=ServTaskStateName  ondblclick="showCodeList('taskstatus',[this,ServTaskState],[1,0],null,null,null,'1',130);" codeClear(ServTaskState,ServTaskStateName);" onchange="afterCodeSelect(codeName,Field);">
				</TD>
        <!--<TD  class= title>
          任务完成时间
        </TD>-->
        <TD  class= input>
          <Input class= 'common' dateFormat="Short" type=hidden style="width:152px" name=TaskFinishDate verify="任务完成时间|DATE">
        </TD>
				<TD class=title></TD>
				<TD  class= input></TD>
			</TR>
		</table>
	</div>
	<table>
	    	<tr>
		    		<td>
		    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServBespeakManageInfoGrid);">
		    		</td>
	    		  <td class= titleImg>
	        		 服务预约信息
	       	  </td>   		 
	    	</tr>
    </table>
 <Div  id= "divLHServBespeakManageInfoGrid" style= "display: ''">

   <Div  id= "divLHServBespeakManageGrid" style= "display: ''">
		<table  class= common align='center' >
		  <TR  class= common>   
			    	<TD  class= title style='width:150' >
			      预约机构代码
			      </TD>
			      <TD  class= input>
					    <Input class=codeno name=HospitCode   style='width:160'ondblclick="return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');"  onkeydown="  return showCodeListKey('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName')	; codeClear(HospitName,HospitCode); "  verify="体检机构名称|len<=60"> 
				    </TD>
				   	<TD  class= title>
              预约机构名称
            </TD>
            <TD  class= input>
            	<Input class= 'codename' name=HospitName  style='width:160' >
            </TD>
				    <TD class = title >
				    服务预约日期
				    </TD>
				    <TD class = input>
					    	<Input class= 'coolDatePicker' style='width:142' dateFormat="Short" name=ServiceDate verify="服务预约时间|DATE">
			   	  </TD>
		  </TR>
			<TR  class= common>    
				 <TD class = title >
				    服务预约时间
				    </TD>
				  <TD class = input>
					    	 <Input class= 'common' name=ServiceTime style='width:160' ondblclick=" return dBespeakServiceTime();"> 
			   	</TD>
	        <TD  class= title>
              健康体检套餐
            </TD>
            <TD  class= input >
            	<Input class= 'codeno' id=MedicaItemName  name=MedicaItemGroup ondblclick="if(checkTestMode()==false) return false;fm.all('queryString').value = fm.all('TestModeCode').value + fm.all('HospitCode').value; showCodeList('hmchoosetest',[this,MedicaItemGroupName],[1,0],null,fm.queryString.value,'TestModeCode',1);" onkeydown=" showCodeListKey('hmchoosetest',[this,MedicaItemGroup],[1,0],null,fm.queryString.value,'TestModeCode',1);"><Input class= codename name=MedicaItemGroupName>
            </TD> 
            	<Input class=codeno type=hidden name=TestModeCode value='32' readonly  >                                                                                                                                                                                                                             
			    <TD  class= title>
			      结算方式
			    </TD>
			   <TD  class= input>
				   <Input class= 'codename' style="width:40px" name=BalanceManner ><Input class= 'codeno' style="width:120px"  name=BalanceMannerName  ondblclick="showCodeList('lhspbalancmode',[this,BalanceManner],[1,0],null,null,null,'1',160);" codeClear(BalanceManner,BalanceMannerName);" >
				 </TD>
		  </TR>
		  <tr class=common>
		  	<TD  class= title>
        			执行状态
       	</TD>
        <TD  class= input>
        		<Input class=codeno name=ExecState CodeData= "0|^未执行|1^已执行|2" ondblClick= "showCodeListEx('hmexecstate',[this,ExecState_ch],[1,0],null,null,null,1,100);"><Input class= 'codename' name=ExecState_ch  style="width:110px">
        </TD>
        <td style="width:400px;">
		  	</td>
     </tr>
   </table>
   	<table class=common border=0 width=100%>
				<TR class=common>  
					<TD  class=common>
						<INPUT VALUE="个人体检预约通知单"  name=ContSomiteInform style="width:120px"  TYPE=button   class=cssbutton onclick="BeTestPrint();">
					</td>
				<TD  class=common>
						<INPUT VALUE="团体体检预约通知单" name=GroupSomiteInform style="width:144px"  TYPE=hidden   class=cssbutton onclick="">
			 </td>
			 <td style="width:700px;">
		  </td>
				</tr>
	</table>
 </div>	
</div>	
	 <table>
	    	<tr>
		    		<td>
		    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHTaskDescGrid);">
		    		</td>
	    		  <td class= titleImg>
	        		 备注信息
	       	  </td>   		 
	    	</tr>
    </table>
<Div  id= "divLHTaskDescGrid" style= "display: ''">

	<table  class= common border=0 width=100%>
			<TR  class= common>
			<TD  class= title>
				  备注说明
			</TD>
		    <TD  class= input colspan='6'>
		      <textarea class= 'common' rows="3" cols="100" name=ServItemNote verify="备注说明|len<=500" ></textarea>
		    </TD>
		    <TD class=title></TD>
		  </TR>
		</table>
</div>
		<table  class= common border=0 width=100%>
			<TR  class= common>
		      	</td>
					<TD  class=common>
						<INPUT VALUE="保 存"   TYPE=button name=save   class=cssbutton onclick="submitForm();">
					</td>
					<TD  class=common>
						<INPUT VALUE="修 改"   TYPE=button name=update  class=cssbutton onclick="updateClick();">
					</td>
				 <TD class=title></TD>
				 <TD  class= input style='width:800'></TD>
			</tr>
		</table>
	</Div>
	 <input type=hidden id=queryString name=queryString>
	 <input type=hidden   id="fmtransact" name="fmtransact">
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="TaskExecNo" name="TaskExecNo">
	 <input type=hidden id="ServTaskNo" name="ServTaskNo">
	 <input type=hidden id="ServCaseCode" name="ServCaseCode">
	 <input type=hidden id="ServTaskCode" name="ServTaskCode">
	 <input type=hidden id="CustomerNo" name="CustomerNo">
	 <input type=hidden id="ServItemNo" name="ServItemNo">
	 <input type=hidden id="TPageTaskNo" name="TPageTaskNo">
	 <input type=hidden id="flag" name="flag">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
