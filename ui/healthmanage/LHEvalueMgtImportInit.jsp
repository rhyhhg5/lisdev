<%
//程序名称：LHEvalueMgtImportInit.jsp
//程序功能：健管评估报告导入
//创建日期：2006-12-12 15:51:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.*"%>
<%
	
	String ServTaskNo = request.getParameter("ServTaskNo");
	String sqlServTaskNo = "'"+ServTaskNo.replaceAll(",","','")+"'";
  String flag = request.getParameter("flag");
%>         
           
<script language="JavaScript">   
var ServTaskNo = "<%=ServTaskNo%>";  
var turnPage = new turnPageClass(); 
var aResult=new Array;  
                                  
function initForm()
{
	fm.all('TPageTaskNo').value= "<%=sqlServTaskNo%>";
	fm.all('flag').value= "<%=flag%>";
	try
	{
		initLHEvalueMgtImportGrid();
		//取得传入任务流水号的所有任务执行信息
		var SqlToExec  = " select TaskExecNo, ServCaseCode, ServTaskNo,ServItemNo from LHTaskCustomerRela "
						       +" where ServTaskNo in ("+"<%=sqlServTaskNo%>"+") order by ServTaskNo";
	  //alert(SqlToExec);
		aResult = easyExecSql(SqlToExec);
		//alert(aResult);
		//alert(aResult.length);
	    var flag= "<%=flag%>";
	    if(flag=="1")
	    {
	    	var execNo=new Array;
	    	for(var i=0; i < aResult.length; i++)
	    	{
	    		 execNo[i]=aResult[i][0];
	    	}
	    	var arrTaskExecNo=execNo.toString().split(",");
	      var sqlTaskExecNo = "";
	      for(var j=0; j < arrTaskExecNo.length; j++)
	      {
	          sqlTaskExecNo= sqlTaskExecNo+",'"+arrTaskExecNo[j]+"'";
	      }
	      sqlTaskExecNo = sqlTaskExecNo.substring(1);
	    	var    strsql="  select TaskExecNo from LHEvaReport d where d.TaskExecNo in ("+ sqlTaskExecNo+")" ;
		    var result=new Array;
		    result=easyExecSql(strsql);
        //alert(result);
        //alert(aResult.length);
        if(result==""||result==null||result=="null")//所有的服务任务号在评估报告导入表中没有存过
        {
        	    displayInfo();
	    	      fm.all('save').disabled=false;
        }
        else
        {
           if(aResult.length==result.length)
           {
		          if(result=="" || result==null || result=="null")
		          {//未保存过
	    	         displayInfo();//
	    	         fm.all('save').disabled=false;
	    	      }
	    	      if(result!="" && result!=null && result!="null")
		          {//保存过
	    	         displayConetent();//所有的服务任务号在评估报告导入表中有存过
	    	         fm.all('save').disabled=true;
	    	      }
	    	    }
	    	    else
	    	    {
	    	    	    displayDiff();//所有的服务任务号在评估报告导入表中有存过的和没有存过的
	    	    	    fm.all('save').disabled=true;
	    	    } 	
	    	 }
	    }
	}
	catch(re)
	{
		alert("LHEvalueMgtImportInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
 	}
}
function displayInfo()
{
		strsql=" select (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo ) ,"
		      +" (select a.CustomerName from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo ),"
		      +" '',"
		      +" d.ServTaskNo,d.ServCaseCode ,"
		      +" d.TaskExecNo ,d.ServItemNo,"
		      +" (select b.ServTaskCode from LHCaseTaskRela b where b.ServTaskNo=d.ServTaskNo ),"
		      +"( case  d.TaskExecState when '1' then '未执行' when  '2' then '已执行' else '' end ), "
		      +" d.TaskExecState "
		      +" from LHTaskCustomerRela d "
		      +" where d.ServTaskNo in ("+"<%=sqlServTaskNo%>"+")"
		      ;
		turnPage.pageLineNum = 10;  
		turnPage.queryModal(strsql, LHEvalueMgtImportGrid);
}
function displayDiff()
{		
	 strsql=" select distinct (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo ) ,"
		      +"  (select a.CustomerName from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo ),"
		      +"  '',"
		      +"  d.ServTaskNo,d.ServCaseCode ,"
		      +" d.TaskExecNo ,d.ServItemNo,"
		      +" (select b.ServTaskCode from LHCaseTaskRela b where b.ServTaskNo=d.ServTaskNo ),"
		      +"  ( case  d.TaskExecState when '1' then '未执行' when  '2' then '已执行' else '' end ),  "
		      +"  d.TaskExecState  "
		      +"  from LHTaskCustomerRela d "
		      +"   where d.ServTaskNo in ("+fm.all('TPageTaskNo').value+")"
		      +"   and d.ServTaskNo not  in (select c.ServTaskNo from LHEvaReport c ) "
		      +"  union  "
		      +"  select distinct d.CustomerNo  ,"
		      +" (select a.Name from LDPerson a where a.CustomerNo=d.CustomerNo),"
		      +"  d.ReportName,"
		      +"  d.ServTaskNo,d.ServCaseCode ,"
		      +"  d.TaskExecNo ,d.ServItemNo,"
		      +" (select b.ServTaskCode from LHCaseTaskRela b where b.ServTaskNo=d.ServTaskNo ),"
		      +"(select case when c.TaskExecState='1' then '未执行' when c.TaskExecState='2' then '已执行' else '' end from LHTASKCUSTOMERRELA c where c.TaskExecNo=d.TaskExecNo ),  "
		      +" (select b.TaskExecState from LHTASKCUSTOMERRELA b where b.TaskExecNo=d.TaskExecNo)  "
		      +" from LHEvaReport d "
		      +"   where d.ServTaskNo in ("+fm.all('TPageTaskNo').value+")"
		      +"  and d.ServTaskNo in (select c.ServTaskNo from LHTaskCustomerRela c )"
		      ;
		 //alert(strsql);
		 turnPage.pageLineNum = 10;  
		 turnPage.queryModal(strsql, LHEvalueMgtImportGrid);
}
function displayConetent()
{
	strsql=" select d.CustomerNo ,"
		      +" (select a.Name from LDPerson a where a.CustomerNo=d.CustomerNo),"
		      +" d.ReportName,"
		      +" d.ServTaskNo,d.ServCaseCode ,"
		      +" d.TaskExecNo ,d.ServItemNo,"
		      +" (select b.ServTaskCode from LHCaseTaskRela b where b.ServTaskNo=d.ServTaskNo ),"
		      +"(select case when c.TaskExecState='1' then '未执行' when c.TaskExecState='2' then '已执行' else '' end from LHTASKCUSTOMERRELA c where c.TaskExecNo=d.TaskExecNo ), "
		      +" (select b.TaskExecState from LHTASKCUSTOMERRELA b where b.TaskExecNo=d.TaskExecNo) "
		      +" from LHEvaReport d "
		      +" where d.ServTaskNo in ("+"<%=sqlServTaskNo%>"+")"
		      ;
		 turnPage.pageLineNum = 10;  
		 turnPage.queryModal(strsql, LHEvalueMgtImportGrid);
}
function initLHEvalueMgtImportGrid() 
{                               
	var iArray = new Array();
    
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名
      iArray[0][1]="30px";         		//列名
      iArray[0][3]=0;
       
      iArray[1]=new Array();
      iArray[1][0]="客户号";         		//列名
      iArray[1][1]="100px";         		//列名
      iArray[1][2]=20;        
      iArray[1][3]=0;         		//列名
    	
      iArray[2]=new Array(); 
		  iArray[2][0]="客户姓名";   
		  iArray[2][1]="100px";   
		  iArray[2][2]=20;        
		  iArray[2][3]=1;
	    
	    
	    iArray[3]=new Array(); 
	    iArray[3][0]="评估报告文件名";   
	    iArray[3][1]="280px";   
	    iArray[3][2]=20;        
	    iArray[3][3]=0;
	    
	    iArray[4]=new Array(); 
	    iArray[4][0]="ServTaskNo";   
	    iArray[4][1]="0px";   
	    iArray[4][2]=20;        
	    iArray[4][3]=3;
	    
	    iArray[5]=new Array(); 
	    iArray[5][0]="ServCaseCode";   
	    iArray[5][1]="0px";   
	    iArray[5][2]=20;        
	    iArray[5][3]=3;
	    
	    iArray[6]=new Array(); 
	    iArray[6][0]="TaskExecNo";   
	    iArray[6][1]="0px";   
	    iArray[6][2]=20;        
	    iArray[6][3]=3;
	    
	    iArray[7]=new Array(); 
	    iArray[7][0]="ServItemNo";   
	    iArray[7][1]="0px";   
	    iArray[7][2]=20;        
	    iArray[7][3]=3;
	    
	    	  
	    iArray[8]=new Array(); 
	    iArray[8][0]="ServTaskCode";   
	    iArray[8][1]="0px";   
	    iArray[8][2]=20;        
	    iArray[8][3]=3;
	    
	    
	    iArray[9]=new Array(); 
	    iArray[9][0]="执行状态";   
	    iArray[9][1]="100px";   
	    iArray[9][2]=20;        
	    iArray[9][3]=1;
	    
	    iArray[10]=new Array(); 
	    iArray[10][0]="taskExecState";   
	    iArray[10][1]="0px";   
	    iArray[10][2]=20;        
	    iArray[10][3]=3;
		 
		            
		
    	LHEvalueMgtImportGrid = new MulLineEnter( "fm" , "LHEvalueMgtImportGrid" ); 
    	//这些属性必须在loadMulLine前
    	
    	LHEvalueMgtImportGrid.mulLineCount = 0;   
    	LHEvalueMgtImportGrid.displayTitle = 1;
    	LHEvalueMgtImportGrid.hiddenPlus = 1;
    	LHEvalueMgtImportGrid.hiddenSubtraction = 1;
    	LHEvalueMgtImportGrid.canSel = 0;
    	LHEvalueMgtImportGrid.canChk = 1;
    	//LHEvalueMgtImportGrid.selBoxEventFuncName = "showOne";    
    	LHEvalueMgtImportGrid.chkBoxEventFuncName = "showTwo";            
    	
    	LHEvalueMgtImportGrid.loadMulLine(iArray);  
    	//这些操作必须在loadMulLine后面
    	//LHEvalueMgtImportGrid.setRowColData(1,1,"asdf");
	}
  	catch(ex) 
  	{
    	alert(ex);
	}
} 
function afterCodeSelect(codeName,Field)
{
	//alert(codeName);
	if(codeName == "hmexecstate")	
	{
		for(var row=0; row < LHEvalueMgtImportGrid.mulLineCount; row++)
		{
			var tChk =LHEvalueMgtImportGrid.getChkNo(row); 
			if(tChk == true)
			{
				LHEvalueMgtImportGrid.setRowColData(row,9,fm.all('ExecState_ch').value);	
				LHEvalueMgtImportGrid.setRowColData(row,10,fm.all('ExecState').value);
	  	}  
		}	
	}
} 
</script>