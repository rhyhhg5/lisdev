<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHScanReasonInHospitalSave.jsp
//程序功能：
//创建日期：郭丽颖
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  
  String inhospitno="";
  LHCustomInHospitalSchema tLHCustomInHospitalSchema   = new LHCustomInHospitalSchema();
  OLHCustomInHospitalUI tOLHCustomInHospitalUI   = new OLHCustomInHospitalUI();
  
  LHDiagnoSet tLHDiagnoSet= new LHDiagnoSet();
  LHCustomTestSet tLHCustomTestSet=new LHCustomTestSet();
  LHCustomOPSSet tLHCustomOPSSet=new LHCustomOPSSet();
  LHCustomOtherCureSet tLHCustomOtherCureSet=new LHCustomOtherCureSet();
  LHFeeInfoSet tLHFeeInfoSet = new LHFeeInfoSet();
  
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHCustomInHospitalSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLHCustomInHospitalSchema.setInHospitNo(request.getParameter("InHospitNo"));

    tLHCustomInHospitalSchema.setIsPhysicalExam("0");
    tLHCustomInHospitalSchema.setInHospitalDays(request.getParameter("InHospitalDays")); 
    tLHCustomInHospitalSchema.setInHospitMode(request.getParameter("InHospitModeCode"));
    tLHCustomInHospitalSchema.setInHospitDate(request.getParameter("InHospitDate"));
    tLHCustomInHospitalSchema.setHospitCode(request.getParameter("HospitCode"));
    tLHCustomInHospitalSchema.setMainItem(request.getParameter("MainItem"));
    
    
    tLHCustomInHospitalSchema.setOperator(request.getParameter("Operator"));
    tLHCustomInHospitalSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHCustomInHospitalSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHCustomInHospitalSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHCustomInHospitalSchema.setModifyTime(request.getParameter("ModifyTime"));
    tLHCustomInHospitalSchema.setManageCom(request.getParameter(tG.ManageCom));
    System.out.println(tG.ManageCom);
    String tICDCode[]=request.getParameterValues("DiagnoInfoGrid1");
    String tIsMainDiagno[]=request.getParameterValues("DiagnoInfoGrid3");
    String tDiagnoseNo[]=request.getParameterValues("DiagnoInfoGrid5");
    
    
    int DiagnoInfoCount = 0;
			if (tICDCode != null) DiagnoInfoCount = tICDCode.length;
	        
			for (int i = 0; i < DiagnoInfoCount; i++)	
			{
			LHDiagnoSchema tLHDiagnoSchema = new LHDiagnoSchema();
			  tLHDiagnoSchema.setCustomerNo(request.getParameter("CustomerNo"));
			  tLHDiagnoSchema.setICDCode(tICDCode[i]);
			  tLHDiagnoSchema.setIsMainDiagno(tIsMainDiagno[i]);
			  tLHDiagnoSchema.setDoctNo(request.getParameter("DoctNo"));
			  tLHDiagnoSchema.setMainCureMode(request.getParameter("MainCureModeCode"));
			  tLHDiagnoSchema.setCureEffect(request.getParameter("CureEffectCode"));
			  tLHDiagnoSchema.setDiagnoseNo(tDiagnoseNo[i]);
			  tLHDiagnoSchema.setInHospitNo(request.getParameter("InHospitNo"));
			  
			  
			  tLHDiagnoSchema.setOperator(request.getParameter("Operator"));
        tLHDiagnoSchema.setMakeDate(request.getParameter("MakeDate"));
        tLHDiagnoSchema.setMakeTime(request.getParameter("MakeTime"));
        tLHDiagnoSchema.setModifyDate(request.getParameter("ModifyDate"));
        tLHDiagnoSchema.setModifyTime(request.getParameter("ModifyTime"));
			  tLHDiagnoSchema.setManageCom(request.getParameter(tG.ManageCom));
				
				tLHDiagnoSet.add(tLHDiagnoSchema);
			}
			
			 String tTestMedicaItemCode[]=request.getParameterValues("TestInfoGrid2");
       String tTestDate[]=request.getParameterValues("TestInfoGrid3");
       String tTestResult[]=request.getParameterValues("TestInfoGrid4");
       String tTestUnit[] = request.getParameterValues("TestInfoGrid5");
       String tTestNo[]=request.getParameterValues("TestInfoGrid6");
       //String tTestDoctNo[]=request.getParameterValues("TestInfoGrid3");
       //String tTestFeeAmount[]=request.getParameterValues("TestInfoGrid8");

       
			int LHCustomTestCount = 0;
			if (tTestMedicaItemCode != null) LHCustomTestCount = tTestMedicaItemCode.length;
	        
			for (int i = 0; i < LHCustomTestCount; i++)	
			{
			LHCustomTestSchema tLHCustomTestSchema = new LHCustomTestSchema();
			  tLHCustomTestSchema.setCustomerNo(request.getParameter("CustomerNo"));
			  tLHCustomTestSchema.setMedicaItemCode(tTestMedicaItemCode[i]);
			  tLHCustomTestSchema.setTestResult(tTestResult[i]);
			  //tLHCustomTestSchema.setDoctNo(tTestDoctNo[i]);
			  tLHCustomTestSchema.setTestDate(tTestDate[i]);
			  //tLHCustomTestSchema.setTestFeeAmount(tTestFeeAmount[i]);
			  tLHCustomTestSchema.setTestNo(tTestNo[i]);
			  tLHCustomTestSchema.setInHospitNo(request.getParameter("InHospitNo"));
			  tLHCustomTestSchema.setResultUnitNum(tTestUnit[i]);
			  System.out.println(tTestNo[i]);
			    
			  tLHCustomTestSchema.setOperator(request.getParameter("Operator"));
        tLHCustomTestSchema.setMakeDate(request.getParameter("MakeDate"));
        tLHCustomTestSchema.setMakeTime(request.getParameter("MakeTime"));
        tLHCustomTestSchema.setModifyDate(request.getParameter("ModifyDate"));
        tLHCustomTestSchema.setModifyTime(request.getParameter("ModifyTime"));
			  tLHCustomTestSchema.setManageCom(request.getParameter(tG.ManageCom));
				
				tLHCustomTestSet.add(tLHCustomTestSchema);
			}
			
			 String tICDOPSCode[]=request.getParameterValues("OPSInfoGrid2");
       String tOPSDoctNo[]=request.getParameterValues("OPSInfoGrid3");
       String tOPSExecutDate[]=request.getParameterValues("OPSInfoGrid5");
       String tOPSCureEffect[]=request.getParameterValues("OPSInfoGrid6");
       String tOPSNo[]=request.getParameterValues("OPSInfoGrid7");
       //String tOPSFeeAmount[]=request.getParameterValues("OPSInfoGrid7");
       
       
       
			int LHCustomOPSCount = 0;
			if (tICDOPSCode != null) LHCustomOPSCount = tICDOPSCode.length;
	        
			for (int i = 0; i < LHCustomOPSCount; i++)	
			{
			LHCustomOPSSchema tLHCustomOPSSchema = new LHCustomOPSSchema();
			  tLHCustomOPSSchema.setCustomerNo(request.getParameter("CustomerNo"));
			  tLHCustomOPSSchema.setICDOPSCode(tICDOPSCode[i]);
			  tLHCustomOPSSchema.setDoctNo(tOPSDoctNo[i]);
			  tLHCustomOPSSchema.setExecutDate(tOPSExecutDate[i]);
			  tLHCustomOPSSchema.setCureEffect(tOPSCureEffect[i]);
			  //tLHCustomOPSSchema.setOPSFeeAmount(tOPSFeeAmount[i]);
			  tLHCustomOPSSchema.setOPSNo(tOPSNo[i]);
			  tLHCustomOPSSchema.setInHospitNo(request.getParameter("InHospitNo"));
			  
			 
			  
			    
			  tLHCustomOPSSchema.setOperator(request.getParameter("Operator"));
        tLHCustomOPSSchema.setMakeDate(request.getParameter("MakeDate"));
        tLHCustomOPSSchema.setMakeTime(request.getParameter("MakeTime"));
        tLHCustomOPSSchema.setModifyDate(request.getParameter("ModifyDate"));
        tLHCustomOPSSchema.setModifyTime(request.getParameter("ModifyTime"));
			  tLHCustomOPSSchema.setManageCom(request.getParameter(tG.ManageCom));
				
				tLHCustomOPSSet.add(tLHCustomOPSSchema);
			}
			
			 String tOtherMedicaItemCode[]=request.getParameterValues("OtherCureInfoGrid2");
       String tOtherDoctNo[]=request.getParameterValues("OtherCureInfoGrid3");
       String tOtherExecutDate[]=request.getParameterValues("OtherCureInfoGrid5");
       String tOtherCureEffect[]=request.getParameterValues("OtherCureInfoGrid6");
       String tOtherFeeAmount[]=request.getParameterValues("OtherCureInfoGrid7");
       String tOtherCureNo[]=request.getParameterValues("OtherCureInfoGrid8");
       
       
			int LHCustomOtherCureCount = 0;
			if (tOtherMedicaItemCode != null) LHCustomOtherCureCount = tOtherMedicaItemCode.length;
	        
			for (int i = 0; i < LHCustomOtherCureCount; i++)	
			{
			LHCustomOtherCureSchema tLHCustomOtherCureSchema = new LHCustomOtherCureSchema();
			  tLHCustomOtherCureSchema.setCustomerNo(request.getParameter("CustomerNo"));
			  tLHCustomOtherCureSchema.setMedicaItemCode(tOtherMedicaItemCode[i]);
			  tLHCustomOtherCureSchema.setDoctNo(tOtherDoctNo[i]);
			  tLHCustomOtherCureSchema.setExecutDate(tOtherExecutDate[i]);
			  tLHCustomOtherCureSchema.setCureEffect(tOtherCureEffect[i]);
			  tLHCustomOtherCureSchema.setOtherFeeAmount(tOtherFeeAmount[i]);
			  tLHCustomOtherCureSchema.setOtherCureNo(tOtherCureNo[i]);
			  tLHCustomOtherCureSchema.setInHospitNo(request.getParameter("InHospitNo"));
			  
			  tLHCustomOtherCureSchema.setOperator(request.getParameter("Operator"));
        tLHCustomOtherCureSchema.setMakeDate(request.getParameter("MakeDate"));
        tLHCustomOtherCureSchema.setMakeTime(request.getParameter("MakeTime"));
        tLHCustomOtherCureSchema.setModifyDate(request.getParameter("ModifyDate"));
        tLHCustomOtherCureSchema.setModifyTime(request.getParameter("ModifyTime"));
			  tLHCustomOtherCureSchema.setManageCom(request.getParameter(tG.ManageCom));
				
				tLHCustomOtherCureSet.add(tLHCustomOtherCureSchema);
			}
			
			 String tFeeCode[]=request.getParameterValues("FeeInfoGrid2");
       String tFeeAmount[]=request.getParameterValues("FeeInfoGrid3");
       String tFeeNo[]=request.getParameterValues("FeeInfoGrid5");
      
       
			int LHFeeInfoCount = 0;
			if (tFeeCode != null) LHFeeInfoCount = tFeeCode.length;
	        
			for (int i = 0; i < LHFeeInfoCount; i++)	
			{
			LHFeeInfoSchema tLHFeeInfoSchema = new LHFeeInfoSchema();
			  tLHFeeInfoSchema.setCustomerNo(request.getParameter("CustomerNo"));
			  tLHFeeInfoSchema.setFeeCode(tFeeCode[i]);
			  tLHFeeInfoSchema.setFeeAmount(tFeeAmount[i]);
			   tLHFeeInfoSchema.setFeeNo(tFeeNo[i]);
			   tLHFeeInfoSchema.setInHospitNo(request.getParameter("InHospitNo"));

			  tLHFeeInfoSchema.setOperator(request.getParameter("Operator"));
        tLHFeeInfoSchema.setMakeDate(request.getParameter("MakeDate"));
        tLHFeeInfoSchema.setMakeTime(request.getParameter("MakeTime"));
        tLHFeeInfoSchema.setModifyDate(request.getParameter("ModifyDate"));
        tLHFeeInfoSchema.setModifyTime(request.getParameter("ModifyTime"));
			  tLHFeeInfoSchema.setManageCom(request.getParameter(tG.ManageCom));
				
				tLHFeeInfoSet.add(tLHFeeInfoSchema);
			}
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLHCustomInHospitalSchema);
  	tVData.add(tG);   
  	tVData.add(tLHDiagnoSet); 
  	tVData.add(tLHCustomTestSet); 
  	tVData.add(tLHCustomOPSSet); 
  	//tVData.add(tLHCustomOtherCureSet);
  	tVData.add(tLHFeeInfoSet);
    tOLHCustomInHospitalUI.submitData(tVData,transact);
    //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHCustomInHospitalUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
   if ( FlagStr.equals("Success") && "INSERT||MAIN".equals(transact) )
    {
     VData res = tOLHCustomInHospitalUI.getResult();
     System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^"+res);
     LHCustomInHospitalSchema mLHCustomInHospitalSchema = new LHCustomInHospitalSchema();
     
     mLHCustomInHospitalSchema.setSchema((LHCustomInHospitalSchema) res.
                                         getObjectByObjectName("LHCustomInHospitalSchema",
                 0));
     
     
   	inhospitno = (String)mLHCustomInHospitalSchema.getInHospitNo();
   	System.out.println("^^^^^^^^^^^^^^^^^^^"+inhospitno);
    }
  } 
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  

  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	
	
	<%if(inhospitno.equals("")){%>
	// alert("save : Healthno为空");
   // 	parent.fraInterface.fm.all("InHospitNo").value="";
        <%}else{System.out.println(inhospitno);%>	
	parent.fraInterface.fm.all("InHospitNo").value = "<%=inhospitno%>";	 
	//alert("1111111111111"+parent.fraInterfaceww.fm.all("RelationNo2").value);
	parent.fraInterface.fms.all("RelationNo").value = parent.fraInterface.fm.all("CustomerNo").value;
	parent.fraInterface.fms.all("RelationNo2").value = "<%=inhospitno%>";	
	//alert("222222222222"+parent.fraInterfaceww.fm.all("RelationNo2").value);
	<%System.out.println(inhospitno);}%>
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
