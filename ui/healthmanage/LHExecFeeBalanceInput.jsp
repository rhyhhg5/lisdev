<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHExecFeeBalanceInput.jsp
//程序功能：健康费用结算管理
//创建日期：2006-09-21 9:39:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="LHExecFeeBalanceInput.js"></SCRIPT>
  
  <%@include file="LHExecFeeBalanceInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm >
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHExecFeeBalanceGrid);">
    		</td>
    		<td class= titleImg>
        	服务事件任务信息
       	</td>
    	</tr>
    </table>
	<Div  id= "divLHExecFeeBalanceGrid" style= "display: ''">
	 		<Div id="LHCaseInfo" style="display:'' ">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHExecFeeBalanceGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>
	</Div>
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	<table>
	    	<tr>
		    		<td>
		    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServBespeakManageInfoGrid);">
		    		</td>
	    		  <td class= titleImg>
	        		 服务费用结算信息
	       	  </td>   		 
	    	</tr>
    </table>
	<Div  id= "divLHServBespeakManageGrid" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>   
				<TD class= title>
         			费用结算机构
				</TD>
				<TD  class= input>
					<Input  class=codeno name= HospitCode ondblclick="showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName',1,300);"   codeClear(HospitName,HospitCode);"><Input class= 'codename' name=HospitName style="width:230px" >
				</TD> 
				<TD  class= title>
            		合同名称
				</TD>
		        <TD  class= input>
		           <Input class=codeno name=ContraNo ondblClick= "showCodeList('hmgrpcontrano',[this,ContraName],[0,1],null,fm.HospitCode.value,'ContraName',1,300);"><Input class= 'codename' name=ContraName style="width:230px">
		        </TD>
		     </tr>
		     <TR  class= common>  
				<TD  class= title>
        			合同责任项目名称
       			</TD>
        		<TD  class= input>
          			<Input class=codeno name=DutyItemCode ondblClick= "showCodeList('hmgrpdutyitemno',[this,DutyItemName,Contraitemno],[0,1,2],null,fm.ContraNo.value,'DutyItemName',1,300);"  codeClear(DutyItemName,DutyItemCode);"><Input class= 'codename' name=DutyItemName  style="width:230px">
        		</TD>
        		<input type=hidden name="Contraitemno">
				<TD  class= title>
        			执行状态
       			</TD>
        		<TD  class= input>
          			<Input class=codeno name=ExecState value="2" CodeData= "0|^未执行|1^已执行|2" ondblClick= "showCodeListEx('hmexecstate',[this,ExecState_ch],[1,0],null,null,null,1,100);"><Input class= 'codename' name=ExecState_ch value="已执行"  style="width:230px">
        		</TD>
      		</tr>
		</table>	
	</div>

		<table  class= common border=0 width=100%>
			<TR  class= common>
		      	</td>
					<TD  class=common>
						<INPUT VALUE="添加费用结算信息" style='width:110'  TYPE=button name=save   class=cssbutton onclick="submitForm();">
					</td>
					<TD  class=common>
						<INPUT VALUE="修改费用结算信息"  style='width:110'  TYPE=button name=update  class=cssbutton onclick="updateClick();">
					</td>
				 <TD class=title></TD>
				 <TD  class= input style='width:700'></TD>
			</tr>
		</table>
	</Div>
	 <input type=hidden id=queryString name=queryString>
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="TaskExecNo" name="TaskExecNo">
	 <input type=hidden id="ServTaskNo" name="ServTaskNo" VALUE="<%=request.getParameter("ServTaskNo")%>">
	 <input type=hidden id="ServCaseCode" name="ServCaseCode">
	 <input type=hidden id="ServTaskCode" name="ServTaskCode">
	 <input type=hidden id="CustomerNo" name="CustomerNo">
	 <input type=hidden id="ServItemNo" name="ServItemNo">
	 <input type=hidden id="manageComPrt" name="manageComPrt">
	 <input type=hidden id="TPageTaskNo" name="TPageTaskNo">
	 <input type=hidden id="flag" name="flag">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
