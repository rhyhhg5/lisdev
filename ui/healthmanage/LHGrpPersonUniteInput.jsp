<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHGrpPersonUniteInput.jsp
//程序功能：
//创建日期：2006-03-09 15:52:49
//创建人  ：林仁令
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
	 String GrpServPlanNo = request.getParameter("GrpServPlanNo"); 
	 String Level = request.getParameter("Level");
%>
<head >
	
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	
  <SCRIPT src="LHGrpPersonUniteInput.js"></SCRIPT>
  <%@include file="LHGrpPersonUniteInputInit.jsp"%>
</head>
<body  onload="initForm();passQuery();" >
  <form action="./LHGrpPersonUniteSave.jsp" method=post name=fm target="fraSubmit">
   <span id="operateButton" >
	<table  class=common align=center>
		<tr align=right>
					
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="return queryClick();">
			</td>			
		
		</tr>
	</table>
</span>
    <%@include file="../common/jsp/InputButton.jsp"%>

	<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHGrpCustomerGrid);">
    		</td>
			<td class= titleImg >
        		 团体客户名单设置
			</td> 
			<td class=titleImg >
				<INPUT class=cssButton name="saveButton" VALUE="契约名单下载"  TYPE=button onclick="return submit();">
			</td>     		 		 
    	</tr>
    </table>
    
   
	<Div id="divLHGrpCustomerGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHGrpCustomerGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>

  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="fmAction" name="fmAction">
  <Input type=hidden name=GrpServPlanNo1 >     
  <Input type=hidden name=GrpContNo >    
  <Input type=hidden name=GrpCustomerNo >    
  <Input type=hidden name=GrpName >    
  <Input type=hidden name=ManageCom >                            
  <Input type=hidden name=Operator >                            
  <Input type=hidden name=MakeDate >                            
  <Input type=hidden name=MakeTime >         
  <Input  type=hidden name=GrpServPlanNo ><!--团体服务计划号码-->
  <Input type=hidden name=GrpServItemNo ><!--团体服务项目号码-->

    	
  </form>
       <form action="./LHGrpPersonUniteImportSave.jsp?GrpServPlanNo=<%=GrpServPlanNo%>" method=post name=upfm target="fraSubmit" ENCTYPE="multipart/form-data">  
 	<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divupfile);">
    		</td>
    		<td class= titleImg>
        		 客户名单导入
       	</td>   	
       	 <Input type=hidden value=<%=GrpServPlanNo%> name=GrpServPlanNo ><!--团体服务计划号码-->	 
    	</tr>
    </table>
 		<Div id="divupfile">
	 <table class=common>
 	 		<tr class=common>
		 	 	  <TD  class= input  style="width:50%">
							<input class=common type=file name=getFile  style="width:80%">
							<input class=cssButton type=button name=submitFile value="上传" onclick="fileUpload();">
					</TD>
			</tr>
 	 </table>
 		</Div>
 </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
	<script>
		//通过综合查询进入此页面的处理
		var turnPage = new turnPageClass(); 
		function passQuery()
		{ 	
			//进入此函数
			var GrpServPlanNo = "<%=request.getParameter("GrpServPlanNo")%>";
			if(GrpServPlanNo!=null || GrpServPlanNo>0) 
			{		
				//如果已经上传，按钮失效
				var sql = "select count(GrpServPlanNo) from lhgrppersonunite where GrpServPlanNo ='"+GrpServPlanNo+"' " ;
			  	if( easyExecSql(sql) > 0)
			  	{
			  		upfm.all('submitFile').disabled =true;
			  	}
			  	
				var strSql = " select distinct GrpServPlanNo,GrpContNo,GrpCustomerNo,GrpName"
				  			+" from LHGrpServPlan "
				  			+" where GrpServPlanNo = '"+GrpServPlanNo+"'"
				  			;
				var arrResult = easyExecSql(strSql);
								
				fm.all('GrpServPlanNo1').value = arrResult[0][0];
				fm.all('GrpContNo').value = arrResult[0][1];
				fm.all('GrpCustomerNo').value = arrResult[0][2];
				fm.all('GrpName').value = arrResult[0][3];
																
				var strSql2 = " select CustomerNo,Name,Level,MakeDate,MakeTime"
      						  +" from LHGrpPersonUnite"
							  +" where GrpServPlanNo = '"+GrpServPlanNo+"' and level = '"+"<%=request.getParameter("Level")%>"+"'"
							  ; 										
      			//alert(strSql2); 	
				turnPage.queryModal(strSql2,LHGrpCustomerGrid); 
			} 
			else 
			{
				  alert("LHMainCustomerHealth.js->没有此团体客户名单");
			}
		    
	}
		
	</script>
</html>
