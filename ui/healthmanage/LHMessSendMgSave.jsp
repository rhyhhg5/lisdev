<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHMessSendMgSave.jsp
//程序功能：
//创建日期：2006-09-08 13:27:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	  TransferData mTransferData = new TransferData();
	  LHMessSendMgUI tLHMessSendMgUI   = new LHMessSendMgUI(); 
    LHMessSendMgSet tLHMessSendMgSet = new LHMessSendMgSet();		//服务预约管理表
    
    String tTaskExecNo[] = request.getParameterValues("LHMessSendMgGrid14");           //任务实施号码流水号
    String tServTaskNo[] = request.getParameterValues("LHMessSendMgGrid12");           //服务任务编号
    String tServCaseCode[] = request.getParameterValues("LHMessSendMgGrid13");					//服务事件号  
    String tServTaskCode[] = request.getParameterValues("LHMessSendMgGrid16");           //服务任务代码
    String tServItemNo[] = request.getParameterValues("LHMessSendMgGrid15");					//服务项目序号
    String tCustomerNo[] = request.getParameterValues("LHMessSendMgGrid1");					//客户号  	 
    String tMakeDate[] = request.getParameterValues("LHMessSendMgGrid17");					//客户号  	 
    String tMakeTime[] = request.getParameterValues("LHMessSendMgGrid18");					//客户号  	 

    String tChk[] = request.getParameterValues("InpLHMessSendMgGridChk"); //参数格式=” Inp+MulLine对象名+Chk”     	 
     
    //输出参数
    CErrors tError = null;
    String tRela  = "";                
    String FlagStr = "";
    String Content = "";
    String transact = "";
    GlobalInput tG = new GlobalInput(); 
    tG=(GlobalInput)session.getValue("GI");
    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
	mTransferData.setNameAndValue("ServTaskNo",request.getParameter("ServTaskNo"));
	mTransferData.setNameAndValue("HmMessCode",request.getParameter("HmMessCode"));
	mTransferData.setNameAndValue("ReMarkExp",request.getParameter("ReMarkExp"));
	mTransferData.setNameAndValue("ExecState",request.getParameter("ExecState"));
	
    int LHMessSendMgCount = 0;
	if(tTaskExecNo != null)
	{	
		LHMessSendMgCount = tTaskExecNo.length;
	}	
	System.out.println(" LHMessSendMgCount is : "+LHMessSendMgCount);
	for(int j = 0; j < LHMessSendMgCount; j++)
	{
		LHMessSendMgSchema tLHMessSendMgSchema = new LHMessSendMgSchema();
		        
		tLHMessSendMgSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		tLHMessSendMgSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		tLHMessSendMgSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		tLHMessSendMgSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		tLHMessSendMgSchema.setServItemNo(tServItemNo[j]);	//项目号码
		tLHMessSendMgSchema.setCustomerNo(tCustomerNo[j]);	//客户号  
		tLHMessSendMgSchema.setHmMessCode(request.getParameter("HmMessCode"));//通讯代码
		tLHMessSendMgSchema.setReMarkExp(request.getParameter("ReMarkExp"));  //备注说明
		tLHMessSendMgSchema.setMakeDate(tMakeDate[j]);	 
		tLHMessSendMgSchema.setMakeTime(tMakeTime[j]);
		
        if(transact.equals("INSERT||MAIN"))
        {
			tLHMessSendMgSet.add(tLHMessSendMgSchema);        
        }
        if(transact.equals("UPDATE||MAIN"))
        {
            if(tChk[j].equals("1"))  
            {        
               System.out.println("该行被选中 "+tTaskExecNo[j]);     
               tLHMessSendMgSet.add(tLHMessSendMgSchema);          
			}
        }   
	}

	try
	{
		// 准备传输数据 VData
  		VData tVData = new VData();
  		tVData.add(tLHMessSendMgSet);
  		tVData.add(tG);
  		tVData.addElement(mTransferData);
    	tLHMessSendMgUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
    	Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
    	tError = tLHMessSendMgUI.mErrors;
    	if (!tError.needDealError())
    	{                          
    		Content = " 操作成功! ";
    		FlagStr = "Success";
    	}
    	else                                                                           
    	{
    		Content = " 操作失败，原因是:" + tError.getFirstError();
    		FlagStr = "Fail";
    	}
 	 }
%>                      
<%=Content%>
<html>
<script language="javascript">
	var transact = "<%=transact%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>