<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-17 15:20:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHCustomGymInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHCustomGymInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHCustomGymSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomGym1);">
    		</td>
    		 <td class= titleImg>
        		 客户健身信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomGym1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      客户号码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=24" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerName>
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      健身项目代码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=GymItemCode elementtype=nacessary verify="健身项目代码|notnull&len<=20" ondblclick="getGymItemCode();return showCodeListEx('GymItemCode',[this],[0],'', '', '', true);" onkeyup="getGymItemCode();return showCodeListKeyEx('GymItemCode',[this],[0],'', '', '', true);">
    </TD>
    <TD  class= title>
      健身频率（次/周）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GymFreque verify="健身频率|num&len<=24">
    </TD>
    
  </TR>
  <TR  class= common>
  	<TD  class= title>
      每次健身时间（小时/次）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GymTime verify="健身时间|num&notnull&len<=5">
    </TD>
  </TR>

</table>
    </Div>
      <Div id= "div1" style= "display: 'none'">
  <table>
  <TR  class= common>
  
    <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator elementtype=nacessary verify="操作员代码|len<=10">
    </TD>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=MakeDate elementtype=nacessary verify="入机日期|len<=10" dateFormat="short">
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime elementtype=nacessary >
    </TD>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
  </TR>
  </table>
  </div>
  	<hr/>
    <INPUT VALUE="客户健康信息" class=cssButton TYPE=button onclick="CustomHealth();">
    <INPUT VALUE="客户就诊信息" class=cssButton TYPE=button onclick="CustomInHospital();">
    <INPUT VALUE="客户疾病信息" class=cssButton TYPE=button onclick="CustomDiseas();">
    <INPUT VALUE="客户检查信息" class=cssButton TYPE=button onclick="CustomTest();">
    <INPUT VALUE="客户治疗信息" class=cssButton TYPE=button onclick="CustomOPS();">
    <INPUT VALUE="客户家族病史" class=cssButton TYPE=button onclick="CustomFamily();">
    <!--INPUT VALUE="客户健身信息" class=cssButton TYPE=button onclick="CustomGym();"-->
  <hr/>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
      </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
