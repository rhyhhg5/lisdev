<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHItemPriceFactorSave.jsp
//程序功能：
//创建日期：2006-04-19 11:53:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHItemPriceFactorUI tLHItemPriceFactorUI   = new LHItemPriceFactorUI();
  
   LHItemPriceFactorSet tLHItemPriceFactorSet = new  LHItemPriceFactorSet();		//定价要素信息
  
    String tPriceFactorCode[] = request.getParameterValues("LHItemPriceFactorGrid1");					//MulLine的列存储数组
		String tPriceFactorName[] = request.getParameterValues("LHItemPriceFactorGrid2");					//MulLine的列存储数组
			
		
								
  
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
     
    int LHItemPriceFactorCount = 0;
		if(tPriceFactorCode != null)
		{	
			LHItemPriceFactorCount = tPriceFactorCode.length;
		}	
		System.out.println(" LHItemPriceFactorCount is : "+LHItemPriceFactorCount);
		
		for(int i = 0; i < LHItemPriceFactorCount; i++)
		{
			
				LHItemPriceFactorSchema tLHItemPriceFactorSchema = new LHItemPriceFactorSchema();
			
				  tLHItemPriceFactorSchema.setComID(request.getParameter("ComIDCode"));
				  tLHItemPriceFactorSchema.setServItemCode(request.getParameter("ServItemCode"));
			    tLHItemPriceFactorSchema.setServItemName(request.getParameter("ServItemName"));
			    System.out.println("********************"+tLHItemPriceFactorSchema.getComID());


				  tLHItemPriceFactorSchema.setPriceFactorCode(tPriceFactorCode[i]);	
				  tLHItemPriceFactorSchema.setPriceFactorName(tPriceFactorName[i]);	

				 
                              
				tLHItemPriceFactorSchema.setOperator(request.getParameter("Operator"));
				tLHItemPriceFactorSchema.setMakeDate(request.getParameter("MakeDate"));
				tLHItemPriceFactorSchema.setMakeTime(request.getParameter("MakeTime"));
				tLHItemPriceFactorSchema.setModifyDate(request.getParameter("ModifyDate"));
				tLHItemPriceFactorSchema.setModifyTime(request.getParameter("ModifyTime"));
                              
			                        
				tLHItemPriceFactorSet.add(tLHItemPriceFactorSchema);
			                        
		}
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  //	tVData.add(tLHHealthServPlanSchema);
  	tVData.add(tLHItemPriceFactorSet);
  	tVData.add(tG);
    
    tLHItemPriceFactorUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHItemPriceFactorUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>