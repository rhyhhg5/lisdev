<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHHmBespeakManageUpdate.jsp
//程序功能：
//创建日期：2006-09-6 18:37:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期 : 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  
  LHHmBespeakManageUpUI  tLHHmBespeakManageUpUI    = new LHHmBespeakManageUpUI ();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    
  	     LHHmServBeManageSchema tLHHmServBeManageSchema   = new LHHmServBeManageSchema();
  	     
  	     tLHHmServBeManageSchema.setTaskExecNo(request.getParameter("TaskExecNo"));
		     System.out.println("SSSSSSSSSSSSSSSS " +tLHHmServBeManageSchema.getTaskExecNo());//任务代码
				 tLHHmServBeManageSchema.setServTaskNo(request.getParameter("ServTaskNo")); 
				 tLHHmServBeManageSchema.setServCaseCode(request.getParameter("ServCaseCode"));
				 tLHHmServBeManageSchema.setServTaskCode(request.getParameter("ServTaskCode")); 
				 tLHHmServBeManageSchema.setCustomerNo(request.getParameter("CustomerNo"));
				 tLHHmServBeManageSchema.setServItemNo(request.getParameter("ServItemNo"));  
		     
		     tLHHmServBeManageSchema.setBespeakComID(request.getParameter("HospitCode"));//预约机构
				 tLHHmServBeManageSchema.setServBespeakDate(request.getParameter("ServiceDate"));  //预约日期
				 tLHHmServBeManageSchema.setServBespeakTime(request.getParameter("ServiceTime"));//预约时间 
				 tLHHmServBeManageSchema.setTestGrpCode(request.getParameter("MedicaItemGroup"));  //体检代码
				 System.out.println("SSSSSSSSSSSSSSSS " +tLHHmServBeManageSchema.getTestGrpCode());//任务代码
				 tLHHmServBeManageSchema.setBalanceManner(request.getParameter("BalanceManner"));//结算方式
				 tLHHmServBeManageSchema.setServDetail(request.getParameter("ServItemNote"));     //备注      

   
         
    
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLHHmServBeManageSchema);
  	tVData.add(tG);
    
    tLHHmBespeakManageUpUI .submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHHmBespeakManageUpUI .mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	
</script>
</html>