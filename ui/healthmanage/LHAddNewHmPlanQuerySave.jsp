<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHAddNewHmPlanQuerySave.jsp
//程序功能：
//创建日期：2006-07-06 14:15:41
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  
  LHServCaseRelaSchema tLHServCaseRelaSchema   = new LHServCaseRelaSchema();
  LHAddNewHmPlanQueryUI tLHAddNewHmPlanQueryUI   = new LHAddNewHmPlanQueryUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");

    
    
    tLHServCaseRelaSchema.setServCaseCode(request.getParameter("ServCaseCode2"));//事件号
    tLHServCaseRelaSchema.setServPlanNo(request.getParameter("ServPlanNo"));//计划号
    tLHServCaseRelaSchema.setServPlanName(request.getParameter("ServPlanName"));//计划名字
    tLHServCaseRelaSchema.setComID(request.getParameter("ComID"));//机构标识
    tLHServCaseRelaSchema.setServPlanLevel(request.getParameter("ServPlanLevel"));//计划档次
    tLHServCaseRelaSchema.setServItemNo(request.getParameter("ServItemNo"));//项目序号
    tLHServCaseRelaSchema.setServItemCode(request.getParameter("ServItemCode"));//标准服务项目代码
    tLHServCaseRelaSchema.setContNo(request.getParameter("ContNo"));//保单号
    tLHServCaseRelaSchema.setCustomerNo(request.getParameter("CustomerNo"));//客户号
    tLHServCaseRelaSchema.setCustomerName(request.getParameter("CustomerName"));//客户名
    
    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLHServCaseRelaSchema);
  	tVData.add(tG);
    tLHAddNewHmPlanQueryUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHAddNewHmPlanQueryUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
