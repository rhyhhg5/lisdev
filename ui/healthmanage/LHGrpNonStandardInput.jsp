<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHGrpNonStandardInput.jsp
//程序功能：
//创建日期：2006-12-14 14:34:16
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	
  <SCRIPT src="LHGrpNonStandardInput.js"></SCRIPT>
  <%@include file="LHGrpNonStandardInit.jsp"%>
</head>
<SCRIPT language=javascript1.2>
function showsubmenu()
{

}
</SCRIPT>
<body  onload="initForm();" >
  <form  method=post name=fm target="fraSubmit">
 <span id="operateButton" >
	<table  class=common align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="return submitForm();">
			</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
			</td>			
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="return queryClick();">
			</td>			
		
		</tr>
	</table>
</span>
    <%@include file="../common/jsp/InputButton.jsp"%>
		<div style="display:''" id='submenu1'>  
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 团体服务计划表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHGrpServPlan1" style= "display: ''">
		<table  class= common align='center' >
			<tr class=common>
				<TD class= title >
		          契约事件类型
		        </TD>   		 
				<TD  class= input>
				<!--Input class= 'codename' style="width:40px" name=ServPlayType  CodeData="0|3^1|个人保单^2|团体保单^3|非标业务"><Input class= 'codename' style="width:120px"  name=ServPlayTypeName VALUE="非标业务"-->
				<input class=codeno CodeData="0|3^2|团体保单^3|非标业务"  name=ServPlayType 
				ondblclick="return showCodeListEx('DealWith',[this,ServPlayTypeName],[0,1],null,null,null,1);" 
				onkeyup="return showCodeListKeyEx('DealWith',[this,ServPlayTypeName],[0,1],null,null,null,1);" verify="契约事件类型|notnull&len=1" ><input class=codename name=ServPlayTypeName>
				</TD>
				<TD  class= title style="width:120">
	            	事件原始编号
	        	</TD>
				<TD  class= input>
		        <Input class= 'common' name=ContNo verify="事件原始编号|notnull&len<=20" ondblclick="getOtherNo()">
				</TD>
				<TD id=aa1 class= title>
					契约事件状态
				</TD>
	        	<TD id=aa2 class= input>
	         	   <Input class= 'codename' style="width:40px" VALUE="1" name=CaseState verify="契约事件状态|notnull&len=1"><Input class= 'codeno' VALUE="未进行服务信息设置" style="width:120px" name=CaseStateName ondblclick="return showCodeList('lhcasestate',[this,CaseState],[1,0],null,null,null,null,160);" >
				</TD>
			</tr>
			<TR  class= common>
			    <TD  class= title>
			      团体客户号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpCustomerNo verify="团体客户号|NOTNULL&len<=20" >
			    </TD>
			    <TD  class= title>
			      团体客户名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpName verify="团体客户名称|NOTNULL&len<=300" >
			    </TD>
			    <TD  class= title style="width:120" >
	          		契约机构标识
	        	</TD>
	        	<TD  class= input>
	        	    <Input class= 'codename' readonly style="width:40px" name=ComID verify="契约机构标识|notnull"><Input class= 'code' style="width:120px" name=ComID_cn ondblclick="showCodeList('hmhospitalmng', [this, ComID], [0,1], null, null, null, 1,150);">
	        	</TD>
		  	</TR>
		  	<TR  class= common>
				<TD  class= title>
        			团体服务计划代码
       			</TD>
       			<TD  class= input>
  					<Input class= 'code' name=ServPlanCode   ondblclick="showCodeList('riskcode1',[this,ServPlanName],[0,1],null,fm.ServPlanCode.value,'Riskcode','1',300);" >
       			</TD>
       			<TD  class= title>
       			  团体服务计划名称
       			</TD>
       			<TD  class= input>
					    <Input class= 'code' name=ServPlanName  ondblclick="showCodeList('riskcode1',[this,ServPlanCode],[1,0],null,fm.ServPlanCode.value,'Riskcode','1',300);" >
       			</TD>
       			<TD class= title>
       			</TD>
       			<TD  id=tdServPlanLevelCode class= input>
				</TD> 
			</TR> 
			<TR  class= common>
			    <TD  class= title>
			      服务计划起始时间
			    </TD>
			    <TD  class= input>
			      <Input class= 'CoolDatePicker' name=StartDate verify="服务计划起始时间|DATE&NOTNULL">
			    </TD>
			    <TD  class= title>
			      服务计划终止时间
			    </TD>
			    <TD  class= input>
			      <Input class= 'CoolDatePicker' name=EndDate verify="服务计划终止时间|DATE&NOTNULL">
			    </TD>
			    <TD  class= title>
			      健管保费（元）
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServPrem >
			    </TD>
			</TR>
		</table>
    </Div>
    
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHGrpServPlanGrid);">
    		</td>
    		<td class= titleImg>
        		 团体服务计划信息列表
       	</td>      		 		 
    	</tr>
    </table>
    
	<Div id="divLHGrpServPlanGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHGrpServPlanGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	<table class=common>
		<tr align=left>
			<td>
       	 <input type= button class=cssButton name=GrpItemSet value="客户名称设置" OnClick="toGrpCustomerSet();">
       	 <input type= button class=cssButton name=GrpCustomerSet value="服务信息设置" OnClick="toGrpItemSet();"><br><br>
			   <input  type=button class=cssButton name=ServFinish value="契约记录完成" onclick="ServRecordFinish();">
			   <input  type=button class=cssButton name=ServSet value="服务事件设置" onclick="ServEventSet();">
			   <input  type=hidden class=cssButton name=CaseFinish value="事件设置完成" onclick="CaseSetFinish();">
			</td>
		</tr>
	</table>
    
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
	<Input type=hidden name=ManageCom >
	<Input type=hidden name=Operator >
	<Input type=hidden name=MakeDate >
	<Input type=hidden name=MakeTime >
	<Input type=hidden name=ModifyDate >
	<Input type=hidden name=ModifyTime >    
	<Input type=hidden name=GrpServPlanNo ><!--团体服务计划号码-->
	<Input type=hidden name=ServPlanLevel >
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
	<script>
		function passQuery()
		{ 	
		
	  }
   </script>
</html>
