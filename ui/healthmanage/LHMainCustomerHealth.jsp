<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-18 10:11:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人  St.GN  更新日期  2005-6-1 12:42   更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<%
	String tCustomerno = "";
	String tHealthno = "";
	
	try
	{
		tCustomerno = request.getParameter("Customerno");
		tHealthno = request.getParameter("Healthno");
		System.out.println(tCustomerno+"----"+tHealthno);
	}
	catch( Exception e )
	{
		tCustomerno = "";
		tHealthno = "";
		System.out.println(e);
	}
%>

<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js">  	</SCRIPT>
  <SCRIPT src="LHMainCustomerHealth.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHCustomHealthStatusInputInit.jsp"%>
</head>
   
<body  onload="initForm();initElementtype();passQuery();" >
   <form  action="./LHCustomHealthStatusSave.jsp" method=post name=fm target="fraSubmit">
      <%@include file="../common/jsp/OperateButton.jsp"%>
      <%@include file="../common/jsp/InputButton.jsp"%>
  	  <table>
      	<tr>
      		<td>
      		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomHealthStatus1);">
      		</td>
      		<td class= titleImg>
          		 一般信息
         	</td>   		 
      	</tr>
      </table>
      
      <Div  id= "divLHCustomHealthStatus1" style= "display: ''">
          <table  class= common align='center' style= "width: '94%'">
            <TR  class= common>
              <TD  class= title>
                客户号码
              </TD>
              <TD  class= input>
                <Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=24" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
              </TD>
              <TD  class= title>
                客户姓名
              </TD>
              <TD  class= input>
                <Input class= 'common' name=CustomerName >
              </TD>
              <TD  class= title>
                记录时间
              </TD>
              <TD  class= input>
                <Input class= 'coolDatePicker' dateFormat="Short" name=AddDate verify="记录时间|DATE&len<=10">
              </TD>
             </TR>
          </table>
      </div>
      <table>
      	<tr>
      		<td>
      		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomHealthStatus2);">
      		</td>
      		<td class= titleImg>
          		 基础健康状况
          </td>   		 
      	</tr>
      </table>
      
      <div id="divLHCustomHealthStatus2" style="display: ''">
            <table  class= common align='center' style= "width: '94%'">
                <TR  class= common>
                  <TD  class= title>
                    血压舒张压（mmHg）
                  </TD>
                  <TD  class= input>
                    <Input class= 'common' name=BloodPressLow verify="血压舒张|INT&len<=5" onkeyup="return computebp1();"> 
                  </TD>
                  <TD  class= title>
                    血压收缩压（mmHg）
                  </TD>
                  <TD  class= input>
                    <Input class= 'common' name=BloodPressHigh verify="血压收缩压|INT&len<=5" onkeyup="return computebp1();"> 
                  </TD>
                  <TD  class= title>
                    脉压差（mmHg)
                  </TD>
                  <TD  class= input>
                    <Input class= readonly readonly name=BloodPress verify="脉压差（mmHg)|num&len<=10" > 
                  </TD>
                </TR>
                <TR  class= common>
                  <TD  class= title>
                    身高（m）
                  </TD>
                  <TD  class= input>
                    <Input class= 'common' name=Stature verify="身高|num&len<=5" onkeyup="return computekgm2();">
                  </TD>
                  <TD  class= title>
                    体重（kg）
                  </TD>
                  <TD  class= input>
                    <Input class= 'common' name=Avoirdupois verify="体重|num&len<=6" onkeyup="return computekgm2();">
                  </TD>
                  <TD  class= title>
                    体重指数（kg/m2）
                  </TD>
                  <TD  class= input>
                    <Input class=readonly readonly name=AvoirdIndex MAXLENGTH="5" verify="体重指数|num&len<=10">
                  </TD>
                </TR>
            </table>
      </div>
      <table>
      	<tr>
      		<td>
      		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomGym1);">
      		</td>
      		<td class= titleImg>
          		 客户健身信息
         </td>   		 
      	</tr>
      </table>
      
      <Div  id= "divLHCustomGym1" style= "display: ''">
      	   <table  class= common align='center' style= "width: '94%'">
      	      <tr class=common>
      	      	<TD  >
                   <span id="spanLHCustomGymGrid" ></span>
                 </TD>
                 
      	      </tr>
      	   </table>
          
      </Div>
       <table>
       	<tr>
       		<td>
       		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomHealthStatus3);">
       		</td>
       		<td class= titleImg>
          		 不良嗜好
          </td>   		 
       	</tr>
      </table>
      <div id="divLHCustomHealthStatus3" style="display: ''">
           <table  class= common align='center' style= "width: '94%'">
               <TR  class= common>
               	<TD  class= title>
                   吸烟（支/天）
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=Smoke verify="吸烟|INT&len<=5">
                 </TD>
                 <TD  class= title>
                   饮酒（两/周）
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=KissCup verify="饮酒|num&len<=5">
                 </TD>
                 <TD  class= title>
                   熬夜（次/月）
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=SitUp verify="熬夜|INT&len<=5">
                 </TD>
                 
               </TR>
               <TR  class= common>
                 <TD  class= title>
                   饮食不规律（餐/周）
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=DiningNoRule verify="饮食不规律|INT&len<=5">
                 </TD>
                 <TD  class= title>
                   其它不良嗜好
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=BadHobby verify="不良嗜好|len<=200">
                 </TD>
                 <TD  class=title>
                 </TD>
                 <TD   class=input>
                 </TD>
               </tr>
           </table>
      </Div>
      <table>
      	  <tr>
      	  	<td>
      	  	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomFamilyDiseas1);">
      	  	</td>
      	  	 <td class= titleImg>
            		 客户家族史信息基本信息
           	 </td>   		 
      	  </tr>
      </table>
      <Div  id= "divLHCustomFamilyDiseas1" style= "display: ''">
         	<table  class= common align='center' style= "width: '94%'">
      	      <tr class=common>
      	      	<TD  >
                 <span id="spanLHCustomFamilyDiseasGrid"></span>
                 </TD>
                 
      	      </tr>
      	   </table>
       </Div>
        <div id="div1" style="display: 'none'">
      <table>
        <TR  class= common>
            <TD  class= title>
              操作员代码
            </TD>
            <TD  class= input>
              <Input class= 'common' name=Operator >
            </TD>
            <TD  class= title>
              入机日期
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeDate >
            </TD>
            <TD  class= title>
              入机时间
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeTime >
            </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最后一次修改日期
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyDate >
          </TD>
          <TD  class= title>
            最后一次修改时间
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyTime >
          </TD>
        </TR>
      </table>
    </div>
      <input type=hidden id="fmtransact" name="fmtransact">
      <input type=hidden id="fmAction" name="fmAction">
      <input type=hidden name="HealthNo" >
      <input type=hidden id=iscomefromquery name =iscomefromquery>
      <input type=hidden name="CustomerGymNo">
      <input type=hidden name="FamilyDiseaseNo">
   </form>
   
      
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

	<script>
		//通过综合查询进入此页面的处理
		var turnPage = new turnPageClass(); 
		function passQuery()
		{
		  //如果不是综合查询进入则跳过此函数
		  if("<%=request.getParameter("Customerno")%>" == "" || "<%=request.getParameter("Customerno")%>" == "null" || "<%=request.getParameter("Healthno")%>" =="" || "<%=request.getParameter("Healthno")%>" =="null")
		  {
		  		return;
		  }
		  //进入此函数
		  else
		  { 		  
				  var CustomerNo_s = "<%=request.getParameter("Customerno")%>";
				  var Healthno_s = "<%=request.getParameter("Healthno")%>";
		//			alert(CustomerNo_s+"-----"+Healthno_s);
				
			  	if(CustomerNo_s!=null && Healthno_s!=null) 
			  	{
			  	
					  	  fm.CustomerNo.value = CustomerNo_s;
								fm.CustomerName.value = easyExecSql("select name from ldperson where customerno = '"+fm.CustomerNo.value+"'");
					  	  fm.HealthNo.value=Healthno_s;
					  	  
					  	  var strSql="select AddDate,BloodPressLow,BloodPressHigh,"
					  	             +" Stature,Avoirdupois,AvoirdIndex,Smoke,KissCup,SitUp,DiningNoRule,BadHobby,HealthNo,MakeDate,MakeTime "
					  	             +" from LHCustomHealthStatus where 1=1 "
					  	             +getWherePart("HealthNo","HealthNo")
					  	             +getWherePart("CustomerNo","CustomerNo");
			
			  	  		var arrLHCustomHealthStatus=easyExecSql(strSql);
//			  	      alert(strSql);alert(arrLHCustomHealthStatus);
			  	  
					  	  if(arrLHCustomHealthStatus!=null) 
					  	  {
					  	  	
						  	    fm.AddDate.value=arrLHCustomHealthStatus[0][0];
						  	    fm.BloodPressLow.value=arrLHCustomHealthStatus[0][1];
						  	    fm.BloodPressHigh.value=arrLHCustomHealthStatus[0][2];
						  	    fm.Stature.value=arrLHCustomHealthStatus[0][3];
						  	    fm.Avoirdupois.value=arrLHCustomHealthStatus[0][4];
						  	    fm.AvoirdIndex.value=arrLHCustomHealthStatus[0][5];
						  	    fm.Smoke.value=arrLHCustomHealthStatus[0][6];
						  	    fm.KissCup.value=arrLHCustomHealthStatus[0][7];
						  	    fm.SitUp.value=arrLHCustomHealthStatus[0][8];
						  	    fm.DiningNoRule.value=arrLHCustomHealthStatus[0][9];
						  	    fm.BadHobby.value=arrLHCustomHealthStatus[0][10];
						  	    fm.HealthNo.value=arrLHCustomHealthStatus[0][11];
						  	    fm.MakeDate.value=arrLHCustomHealthStatus[0][12];
						  	    fm.MakeTime.value=arrLHCustomHealthStatus[0][13];
						  	    fm.BloodPress.value=Math.round(((fm.BloodPressHigh.value-fm.BloodPressLow.value)*100)/100);
					  	  }
					//	  else {
					//	      		alert("LHMainCustomerHealth.js->afterquery()出现错误:arrLHCustomHealthStatus为空");
					//	   		 }
						  	try
						  	{
					 			   strSql="select a.GymItemCode,b.codename, a.GymTime,a.GymFreque,a.CustomerGymNo "
								         +" from LHCustomGym a, ldcode b where a.CustomerNo = '"+CustomerNo_s
								         +"' and b.codetype='gymitem' and a.GymItemCode=b.code and a.CustomerGymNo = '"+Healthno_s+"'"
								         ;
					  	  
					  	     turnPage.queryModal(strSql, LHCustomGymGrid);
						  	}
							  catch(e)
							  {
							  	alert(e);
							  }
			  	
					  	 strSql="select b.codename,a.FamilyCode,a.ICDCode,c.ICDName,a.FamilyDiseaseNO "
					  	        +" from LHCustomFamilyDiseas a, ldcode b,LDDisease c where a.CustomerNo = '"+CustomerNo_s
								      +"' and b.codetype='familycode' and a.FamilyCode=b.code and a.ICDCode=c.ICDCode and a.FamilyDiseaseNo ='"+Healthno_s+"'"
					  	        ;
					  	 
					  	 
					  	 
						  	 turnPage.queryModal(strSql, LHCustomFamilyDiseasGrid);
						  	 fm.all('iscomefromquery').value=="1";
				  	} 
				  	else 
				  	{
				  	  alert("LHMainCustomerHealth.js->afterquery()出现错误");
				  	}
		  	}
	}
		
	</script>

</html>
