<%
//程序名称：LHHealthServPlanQueryInput.jsp
//程序功能：功能描述
//创建日期：2006-03-20 17:10:48
//创建人  ：郭丽颖
//更新记录： 对新字段内容的操作
// 更新人 : 
// 更新日期 : 
// 更新原因/内容: 
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHHealthServPlanQueryInput.js"></SCRIPT> 
  <%@include file="LHHealthServPlanQueryInputInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
 
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLHHealthServPlanGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
   <TD  class= title>
      服务计划代码
    </TD>
    <TD  class= input>
     
      <Input class= 'code' name=Riskcode  verify="服务计划代码|len<=50" ondblclick="showCodeList('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode');"   onkeydown="return showCodeListKey('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode'); codeClear(Riskname,Riskcode);">
    </TD>
    <TD  class= title>
      服务计划名称
    </TD>
    <TD  class= input>
      <Input class= 'code' name=Riskname  verify="个人服务计划名称|NOTNULL&len<=300" ondblclick="showCodeList('hmriskcode',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskcode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmriskcode',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskcode');"   onkeydown="return showCodeListKey('hmriskcode',[this,Riskcode],[0,1],null,fm.Riskcode.value,'Riskcode'); codeClear(Riskname,Riskcode);">
    </TD>
    <TD class= title>
    	服务计划档次
    </TD>
    <TD  class= input>
    	<Input type=hidden  name= ServPlanLevelCode>
    	<Input class= 'code' name=ServPlanLevelName verify="个人服务计划档次|len<=10" ondblclick="showCodeList('hmservplanlevel',[this,ServPlanLevelCode],[0,1],null,fm.ServPlanLevelName.value,'ServPlanLevelName','',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservplanlevel',[this,ServPlanLevelCode],[0,1],null,fm.ServPlanLevelName.value,'ServPlanLevelName');"   onkeydown="return showCodeListKey('hmservplanlevel',[this,ServPlanLevelCode],[0,1],null,fm.ServPlanLevelName.value,'ServPlanLevelName'); codeClear(ServPlanLevelName,ServPlanLevelCode);">
   </TD>   
  </TR>
  <TR  class= common>
  	 <TD  class= title>
      机构属性标识
    </TD>
    <TD  class= input>
      <Input type=hidden name=ComIDCode>
      <Input class= 'code' name=ComIDName verify="机构属性标识|len<=10" ondblclick="showCodeList('hmhospitalmng',[this,ComIDCode],[0,1],null,fm.ComIDName.value,'ComIDName','1',260);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmhospitalmng',[this,ComIDCode],[0,1],null,fm.ComIDName.value,'ComIDName');" onkeydown="return showCodeListKey('hmhospitalmng',[this,ComIDCode],[0,1],null,fm.ComIDName.value,'ComIDName'); codeClear(ComIDName,ComIDCode);">	
    </TD>
	<TD  class= title>
      计划类型
    </TD>
    <TD  class= input>
      <Input  class = codeno  name=PlanType CodeData= "0|^1|按档次^2|按保费" verify="计划类型|NOTNULL&len=1" ondblclick="showCodeListEx('hmPlanType',[this,PlanTypeName],[0,1],null,null,null,'1',200);" onkeyup=" if(event.keyCode ==13)  return showCodeListEx('hmPlanType',[this,PlanTypeName],[0,1],null,null,null,1,200);" onkeydown="return showCodeListKeyEx('hmPlanType',[this,PlanTypeName],[0,1],null,null,null,1,200); "><Input class= 'codename' name=PlanTypeName >	
    </TD>
   </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT class =cssbutton VALUE="查询"   TYPE=button   class=common onclick="easyQueryClick();">
        <INPUT class =cssbutton VALUE="返回"   TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHHealthServPlan1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHHealthServPlanGrid1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHHealthServPlanGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
