<%
//程序名称：LHGrpServPlanInput.jsp
//程序功能：
//创建日期：2006-03-09 11:34:17
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('GrpServPlanNo').value = "";
    fm.all('GrpContNo').value = "";
    fm.all('GrpCustomerNo').value = "";
    fm.all('GrpName').value = "";
    fm.all('ServPlanCode').value = "";
    fm.all('ServPlanName').value = "";
    fm.all('ServPlanLevel').value = "";
    fm.all('ServPlayType').value = "";
    fm.all('ComID').value = "";
    fm.all('StartDate').value = "";
    fm.all('EndDate').value = "";
    fm.all('ServPrem').value = "";
    fm.all('ManageCom').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHGrpServPlanInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {}
  catch(ex)
  {
    alert("在LHGrpServPlanInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    fm.all('ServTrace').disabled=true;
    initLHGrpServPlanGrid();
    initLHServItemGrid();
    
  }
  catch(re)
  {
    alert("LHGrpServPlanInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHGrpServPlanGrid;
function initLHGrpServPlanGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";			//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="团体客户分档";   
	iArray[1][1]="140px";   
	iArray[1][2]=20;        
	iArray[1][3]=1;
	
	iArray[2]=new Array(); 
	iArray[2][0]="约定客户人数";   
	iArray[2][1]="140px";   
	iArray[2][2]=20;        
	iArray[2][3]=1;
	
	iArray[3]=new Array();                               
  iArray[3][0]="实际客户人数";         		//列名  
  iArray[3][1]="140px";         		//列名   
  iArray[2][2]=20;              
  iArray[3][3]=0;         		//列名         
  
  iArray[4]=new Array();                               
  iArray[4][0]="档次说明";         		//列名  
  iArray[4][1]="286px";         		//列名   
  iArray[4][2]=20;              
  iArray[4][3]=0;         		//列名   
             
    
    LHGrpServPlanGrid = new MulLineEnter( "fm" , "LHGrpServPlanGrid" ); 
    //这些属性必须在loadMulLine前
  
    LHGrpServPlanGrid.mulLineCount = 0;   
    LHGrpServPlanGrid.displayTitle = 1;
    LHGrpServPlanGrid.hiddenPlus = 0;
    LHGrpServPlanGrid.hiddenSubtraction = 0;
    LHGrpServPlanGrid.canSel = 1;
    LHGrpServPlanGrid.canChk = 0;
    //LHGrpServPlanGrid.selBoxEventFuncName = "showOne";

    LHGrpServPlanGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHGrpServPlanGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
var LHServItemGrid;
function initLHServItemGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";			//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="服务项目代码";   
	iArray[1][1]="170px";   
	iArray[1][2]=20;        
	iArray[1][3]=1;
	
	iArray[2]=new Array(); 
	iArray[2][0]="服务项目名称";   
	iArray[2][1]="170px";   
	iArray[2][2]=20;        
	iArray[2][3]=1;
	
	iArray[3]=new Array();                               
  iArray[3][0]="服务项目序号";         		//列名  
  iArray[3][1]="170px";         		//列名   
  iArray[2][2]=20;              
  iArray[3][3]=0;         		//列名         

  iArray[4]=new Array(); 
	iArray[4][0]="服务事件类型";   
	iArray[4][1]="170px";   
	iArray[4][2]=20;        
	iArray[4][3]=1;
	
  //iArray[5]=new Array(); 
	//iArray[5][0]="服务提供机构标识";   
	//iArray[5][1]="90px";   
	//iArray[5][2]=20;        
	//iArray[5][3]=1;
	
	//iArray[6]=new Array(); 
	//iArray[6][0]="定价方式代码";   
	//iArray[6][1]="80px";   
	//iArray[6][2]=20;        
	//iArray[6][3]=1;
	
	//iArray[7]=new Array();                               
  //iArray[7][0]="定价方式名称";         		//列名  
  //iArray[7][1]="90px";         		//列名   
  //iArray[7][2]=20;              
  //iArray[7][3]=0;         		//列名                 
    
    LHServItemGrid = new MulLineEnter( "fm" , "LHServItemGrid" ); 
    //这些属性必须在loadMulLine前
  
    LHServItemGrid.mulLineCount = 0;   
    LHServItemGrid.displayTitle = 1;
    LHServItemGrid.hiddenPlus = 0;
    LHServItemGrid.hiddenSubtraction = 0;
    LHServItemGrid.canSel = 1;
    LHServItemGrid.canChk = 0;
    LHServItemGrid.selBoxEventFuncName = "showTwo";

    LHServItemGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHServItemGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
