<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDDoctoSearchrSave.jsp
//程序功能：
//创建日期：2007-12-06
//创建人  ：ll创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="java.io.*"%>
<%
   boolean operFlag = true;
   String FlagStr = "";
   String fmAction = request.getParameter("fmAction");

    CErrors tError = null;
    GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");
    LDDoctorSchema tLDDoctorSchema   = new LDDoctorSchema();
    if(fmAction.equals("1")){
        tLDDoctorSchema.setDoctNo("State");
        tLDDoctorSchema.setMakeDate(request.getParameter("MakeDate1"));//起始日期
        tLDDoctorSchema.setModifyDate(request.getParameter("MakeDate2"));//借用修改日期传终止日期
    }else if(fmAction.equals("2")){
    	tLDDoctorSchema.setDoctNo("SERACH");
    	tLDDoctorSchema.setManageCom(request.getParameter("ManageCom"));//管理机构
        tLDDoctorSchema.setWorkPlace(request.getParameter("AreaCode"));//地区名称
        tLDDoctorSchema.setHospitCode(request.getParameter("HospitCode"));//所属机构,医院号码
        tLDDoctorSchema.setEngageClass(request.getParameter("EngageClass"));//聘用方式
        tLDDoctorSchema.setSecName(request.getParameter("SecName"));//科室专业
        tLDDoctorSchema.setCarreerClass(request.getParameter("CarreerClass")); //职业类型
        tLDDoctorSchema.setTechPostCode(request.getParameter("TechPostCode"));//职称
        tLDDoctorSchema.setExpertFlag(request.getParameter("ExpertFlag"));//专家标识
        tLDDoctorSchema.setEduLevelCode(request.getParameter("EduLevelCode"));//学历
        tLDDoctorSchema.setDoctName(request.getParameter("Doctname"));//姓名
        tLDDoctorSchema.setBirthYear(request.getParameter("BirthYear"));//出生年
        tLDDoctorSchema.setSex(request.getParameter("Sex"));  //性别
	}

	VData mResult = new VData();
    // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLDDoctorSchema);
  	  tVData.add(tG);
    LDDoctorSearchBL tLDDoctorSearchBL = new LDDoctorSearchBL();
    XmlExport txmlExport = new XmlExport();
   if(!tLDDoctorSearchBL.submitData(tVData,"")){
        System.out.println("后台处理出错!");
        operFlag = false;
   }else{
        mResult = tLDDoctorSearchBL.getResult();
        txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
        if(txmlExport==null){
            operFlag = false;
            System.out.println("没有获取要打印的文件.");
        }
   }

   LDSysVarDB tLDSysVarDB = new LDSysVarDB();
   tLDSysVarDB.setSysVar("VTSFilePath");
   tLDSysVarDB.getInfo();
   String vtsPath = tLDSysVarDB.getSysVarValue();
   if (vtsPath == null)
   {
    vtsPath = "vtsfile/";
   }

   String filePath = application.getRealPath("/").replace('\\', '/') + "/" + vtsPath;
   String fileName = "TASK" + FileQueue.getFileName()+".vts";
   String realPath = filePath + fileName;

   if (operFlag==true)
	{
    //合并VTS文件
    String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);

    //把dataStream存储到磁盘文件
    AccessVtsFile.saveToFile(dataStream, realPath);
    response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath=" + realPath);
	}
	else
	{
    	 FlagStr = "Fail";
    }
%>

