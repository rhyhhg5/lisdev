<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDDoctorSave.jsp
//程序功能：
//创建日期：2005-01-19 10:10:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LDDoctorSchema tLDDoctorSchema   = new LDDoctorSchema();
  OLDDoctorUI tOLDDoctorUI   = new OLDDoctorUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    tLDDoctorSchema.setDoctNo(request.getParameter("DoctNo"));
    tLDDoctorSchema.setDoctName(request.getParameter("DoctName"));
    tLDDoctorSchema.setManageCom(request.getParameter("ManageCom"));
    tLDDoctorSchema.setBirthYear(request.getParameter("BirthYear"));
   // System.out.println("QQQQQQQQQQQ PPPPPPPPP"+request.getParameter("BirthYear"));//BirthYear
    
    tLDDoctorSchema.setSex(request.getParameter("Sex"));
    tLDDoctorSchema.setEduLevelCode(request.getParameter("EduLevelCode"));
    tLDDoctorSchema.setTechPostCode(request.getParameter("TechPostCode"));
    tLDDoctorSchema.setCarreerClass(request.getParameter("CarreerClass"));
    
    tLDDoctorSchema.setExpertFlag(request.getParameter("ExpertFlag"));
    tLDDoctorSchema.setHospitCode(request.getParameter("HospitCode"));
    tLDDoctorSchema.setSecName(request.getParameter("SecName"));
    tLDDoctorSchema.setTechSpec(request.getParameter("TechSpec"));
    //zsjing修改
    tLDDoctorSchema.setWorkPlace(request.getParameter("AreaCode"));
    tLDDoctorSchema.setEngageClass(request.getParameter("EngageClass"));
    tLDDoctorSchema.setContact(request.getParameter("Contact"));
    tLDDoctorSchema.setFixFlag(request.getParameter("FixFlag1"));
    //tLDDoctorSchema.setDoctEmail(request.getParameter("DoctEmail"));
    //tLDDoctorSchema.setSpecialFLag(request.getParameter("SpecialFLag"));
    //tLDDoctorSchema.setDiagTime(request.getParameter("DiagTime"));
    //tLDDoctorSchema.setCExportFlag(request.getParameter("CExportFlag"));
    //tLDDoctorSchema.setJobExplain(request.getParameter("JobExplain"));
    
    tLDDoctorSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDDoctorSchema.setMakeTime(request.getParameter("MakeTime"));
    tLDDoctorSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLDDoctorSchema.setModifyTime(request.getParameter("ModifyTime"));
    
    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLDDoctorSchema);
  	tVData.add(tG);
    tOLDDoctorUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDDoctorUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
