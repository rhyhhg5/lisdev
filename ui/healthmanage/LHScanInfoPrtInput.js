//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./LHScanInfoPrtSave.jsp"
  fm.target = "fraSubmit";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

	    if(fm.all('GrpFlag').value == "1")
	    {
		    var maxno = easyExecSql("select max(substr(SerialNo,1,5)) from lhscaninfo where Comment = '"+fm.all('GrpContNo').value+"' and othernotype = '"+fm.all('ScanTypeGrp').value+"'");
		
		    var sql = " select otherno, b.name, SerialNo, othernotype from lhscaninfo a, ldperson b "
					+ " where a.otherno = b.customerno and Comment = '"+fm.all('GrpContNo').value+"' "
					+ " and othernotype = '"+fm.all('ScanTypeGrp').value+"' and substr(SerialNo,1,5) = '"+maxno+"'"
					;
	    	turnPage.queryModal(sql,LHScanNoGrid);
	    }
	    if(fm.all('GrpFlag').value == "0")
	    {
	    	var maxno = easyExecSql("select max(SerialNo) from lhscaninfo where substr(SerialNo,6) = '"+fm.all('CustomerNo').value+"' and othernotype = '"+fm.all('ScanTypePerson').value+"'");
	    	
	    	var sql = " select otherno, b.name, SerialNo, othernotype from lhscaninfo a, ldperson b "
	    			+ " where a.otherno = b.customerno and SerialNo = '"+maxno+"'"
	    			;
			turnPage.queryModal(sql,LHScanNoGrid);	    			
	    }
  	}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDDrug.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDrugQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
               

function displayDiv(objInput)
{
	
	if(objInput.value == "0")
	{
		grpContDiv.style.display="";
		customerDiv.style.display="none";
		fm.all('GrpContNo').verify="团体客户号码|notnull";
		fm.all('GrpContName').verify="";
		fm.all('ScanTypeGrp').verify="扫描件类型|notnull";
		
		fm.all('GrpFlag').value = "1";
	}
	else if(objInput.value == "1")
	{
		grpContDiv.style.display="none";
		customerDiv.style.display="";
		fm.all('CustomerNo').verify = "客户号码|len<=24&NOTNULL";
		fm.all('CustomerName').verify = "客户姓名|len<=24&NOTNULL";
		fm.all('ScanTypePerson').verify = "扫描件类型|notnull";
		
		fm.all('GrpFlag').value = "0";
	}
}

function printClick()
{

	if(LHScanNoGrid.mulLineCount < 1)
	{
		alert("请先生成号码");	
		return false;
	}
	else 
	{
		if(LHScanNoGrid.getRowColData(0,3) == "" || LHScanNoGrid.getRowColData(0,3) == null)
		{
			alert("数据有误请确认");	
			return false;
		}
		else
		{
			fm.all('SerialNo').value = LHScanNoGrid.getRowColData(0,3);
			fm.action="./LHScanInfoBatchPrt.jsp"
			fm.target="fraSubmit";
			fm.submit();
		}
	}
}

function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")
	{
		var newWindow = window.open("../sys/LDPersonQuery.html");	  
	}
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	    if (arrResult != null) 
	    {
	    	fm.CustomerNo.value = arrResult[0][0];
	      	alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
		}
    	else
    	{
			alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
        }
	}	
}

function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}


function AddQueue()
{
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="./LHScanInfoQueueSave.jsp";
  fm.target = "fraSubmit";
  fm.submit(); //提交	
}

function afterSubmitQueue(tCustomerNo,tSerialNo,tScanType)
{
	showInfo.close();
	LHScanNoGrid.addOne();//添加
	var maxRow = LHScanNoGrid.mulLineCount;
	
	var tCustomerName = easyExecSql("select distinct name from LDPerson where CustomerNo = '"+tCustomerNo+"' ");
	
	LHScanNoGrid.setRowColData(maxRow-1,1,tCustomerNo);
	LHScanNoGrid.setRowColData(maxRow-1,2,tCustomerName.toString().trim());
	LHScanNoGrid.setRowColData(maxRow-1,3,tSerialNo);
	LHScanNoGrid.setRowColData(maxRow-1,4,tScanType);
}