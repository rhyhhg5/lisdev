<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LOMsgInfoSave.jsp
//程序功能：
//创建日期：2005-11-02 14:02:32
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.health.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%@page import="javax.activation.*"%>
<%@page import="java.io.*"%>
<%@page import="java.net.URLEncoder"%>
<html> 	  
	<body>
		 <script language="javascript"> 
<%
	//接收信息，并作校验处理
	//输入参数
	LOMsgInfoSchema tLOMsgInfoSchema = new LOMsgInfoSchema();
	LOMsgReceiveSet tLOMsgReceiveSet = new LOMsgReceiveSet();
	OLOMsgInfoUI tOLOMsgInfoUI   = new OLOMsgInfoUI();
	
	SendMsgMail tSendMsgMail = new SendMsgMail();
	TransferData tTransferData = new TransferData();
	
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	String ttext = "";
	String HaveBeenSent = request.getParameter("HaveBeenSent");
	//立即发送标志
	String IsSend = request.getParameter("MsgSend");
	
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	transact = request.getParameter("fmtransact");
 	System.out.println(HaveBeenSent+"-------------"+transact);
	if(transact.equals("INSERT||MAIN") && !HaveBeenSent.equals("0"))
	{//判断是否需要Insert, HaveBeenSent为0都是Update
		//客户信息
		String MoNo = request.getParameter("mobile");
		String CusNo = request.getParameter("CustomerNo");
		String GrpContNo = request.getParameter("GrpContNo");
		String ContNo = request.getParameter("ContNo");                           

		if(IsSend.equals("1") )
		{
			tTransferData.setNameAndValue("mobile",request.getParameter("mobile"));
			tTransferData.setNameAndValue("content",request.getParameter("content"));

			try
			{
			  	VData aVData = new VData();  
			  	aVData.add(tG);                                                      
			  	aVData.add(tTransferData);                     
				tSendMsgMail.submitData(aVData,"Message"); 
			}
			catch(Exception ex)
			{
				Content = "短信发送失败，原因是:" + ex.toString();
				FlagStr = "Fail";
			}		  		
		}
		//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		  	
		tLOMsgInfoSchema.setMsgNo(request.getParameter("MsgNo"));  	
		tLOMsgInfoSchema.setSendEmail(request.getParameter("from"));
		tLOMsgInfoSchema.setPassWord(request.getParameter("password"));
		tLOMsgInfoSchema.setMsgContent(request.getParameter("content"));
		tLOMsgInfoSchema.setSendType(request.getParameter("SendType"));
		tLOMsgInfoSchema.setMsgSend(request.getParameter("MsgSend"));
		tLOMsgInfoSchema.setMakeDate(request.getParameter("MakeDate"));
		tLOMsgInfoSchema.setMakeTime(request.getParameter("MakeTime"));
          
		if(MoNo.indexOf(",") == -1)
		{//如果不是群发
        
			if(MoNo.equals(""))	
			{//如果没有电话号码
				LOMsgReceiveSchema tLOMsgReceiveSchema = new LOMsgReceiveSchema();
				tLOMsgReceiveSchema.setMakeDate(request.getParameter("MakeDate"));
				tLOMsgReceiveSchema.setMakeTime(request.getParameter("MakeTime"));
				tLOMsgReceiveSet.add(tLOMsgReceiveSchema);		 
			}
			else	
			{//只有一个电话号码
				LOMsgReceiveSchema tLOMsgReceiveSchema = new LOMsgReceiveSchema();
				tLOMsgReceiveSchema.setMobileNo(MoNo);
				tLOMsgReceiveSchema.setCustomerNo(CusNo);
				tLOMsgReceiveSchema.setGrpContNo(GrpContNo);
				tLOMsgReceiveSchema.setContNo(ContNo);
				tLOMsgReceiveSchema.setMakeDate(request.getParameter("MakeDate"));
				tLOMsgReceiveSchema.setMakeTime(request.getParameter("MakeTime"));
				tLOMsgReceiveSet.add(tLOMsgReceiveSchema);
			}
		}
		  		
		//群发
		else
		{
			String[] MobileNoArr = MoNo.split(",");
			String[] CustomerNoArr = CusNo.split(",");
			String[] GrpContNoArr = GrpContNo.split(",");
			String[] ContNoArr = ContNo.split(",");
			int count = MobileNoArr.length;

			for(int i = 0; i < count; i++)
			{System.out.println("--------第"+i+"次循环--------");
				LOMsgReceiveSchema sLOMsgReceiveSchema = new LOMsgReceiveSchema();
				
				sLOMsgReceiveSchema.setMobileNo(MobileNoArr[i]);
				sLOMsgReceiveSchema.setCustomerNo(CustomerNoArr[i]);
				sLOMsgReceiveSchema.setGrpContNo(GrpContNoArr[i]);
				sLOMsgReceiveSchema.setContNo(ContNoArr[i]);
				
				sLOMsgReceiveSchema.setMsgNo(request.getParameter("MsgNo"));
				sLOMsgReceiveSchema.setMakeDate(request.getParameter("MakeDate"));
				sLOMsgReceiveSchema.setMakeTime(request.getParameter("MakeTime"));
				tLOMsgReceiveSet.add(sLOMsgReceiveSchema);
			}
		}
	}
	if(transact.equals("UPDATE||MAIN") && HaveBeenSent.equals("0"))
	{   //如果是没被发送的才会被修改
		//客户信息
		String MoNo = request.getParameter("mobile");
		String SeqNo = request.getParameter("MSGSEQ");
		String CusNo = request.getParameter("CustomerNo");
		String GrpContNo = request.getParameter("GrpContNo");
		String ContNo = request.getParameter("ContNo");

		if(IsSend.equals("1") )
		{
			tTransferData.setNameAndValue("mobile",request.getParameter("mobile"));
			tTransferData.setNameAndValue("content",request.getParameter("content"));	
		  	try
		  	{
				VData aVData = new VData(); 
				aVData.add(tG);                                                        
				aVData.add(tTransferData);                     
				tSendMsgMail.submitData(aVData,"Message"); 
			}
			catch(Exception ex)
			{
				Content = "短信发送失败，原因是:" + ex.toString();
				FlagStr = "Fail";
			}
		}
		
		tLOMsgInfoSchema.setMsgNo(request.getParameter("MsgNo"));  	
		tLOMsgInfoSchema.setSendEmail(request.getParameter("from"));
		tLOMsgInfoSchema.setPassWord(request.getParameter("password"));
		tLOMsgInfoSchema.setMsgContent(request.getParameter("content"));
		tLOMsgInfoSchema.setSendType(request.getParameter("SendType"));
		tLOMsgInfoSchema.setMsgSend(request.getParameter("MsgSend"));
		tLOMsgInfoSchema.setMakeDate(request.getParameter("MakeDate"));
		tLOMsgInfoSchema.setMakeTime(request.getParameter("MakeTime"));


		if(MoNo.indexOf(",") == -1)
		{//如果不是群发
			if(MoNo.equals(""))	
			{//如果没有电话号码
				LOMsgReceiveSchema tLOMsgReceiveSchema = new LOMsgReceiveSchema();
				tLOMsgReceiveSchema.setMakeDate(request.getParameter("MakeDate"));
				tLOMsgReceiveSchema.setMakeTime(request.getParameter("MakeTime"));
				tLOMsgReceiveSet.add(tLOMsgReceiveSchema);		 
			}
			else	
			{//只有一个电话号码
				LOMsgReceiveSchema tLOMsgReceiveSchema = new LOMsgReceiveSchema();
				tLOMsgReceiveSchema.setMobileNo(MoNo);
				tLOMsgReceiveSchema.setSeqNo(SeqNo);    
				tLOMsgReceiveSchema.setCustomerNo(CusNo);
				tLOMsgReceiveSchema.setGrpContNo(GrpContNo);
				tLOMsgReceiveSchema.setContNo(ContNo);
				tLOMsgReceiveSchema.setMsgNo(request.getParameter("MsgNo"));
				tLOMsgReceiveSchema.setMakeDate(request.getParameter("MakeDate"));
				tLOMsgReceiveSchema.setMakeTime(request.getParameter("MakeTime"));
				tLOMsgReceiveSet.add(tLOMsgReceiveSchema);
			}		  			
		}
		  		
		//群发
		else
		{
			String[] MobileNoArr = MoNo.split(",");
			String[] SeqNoArr = SeqNo.split(",");
			String[] CustomerNoArr = CusNo.split(",");
			String[] GrpContNoArr = GrpContNo.split(",");
			String[] ContNoArr = ContNo.split(",");
			int count = MobileNoArr.length;
				
			for(int i = 0; i < count; i++)
			{
			  	LOMsgReceiveSchema sLOMsgReceiveSchema = new LOMsgReceiveSchema();
			  	
			  	sLOMsgReceiveSchema.setMobileNo(MobileNoArr[i]);
			  	sLOMsgReceiveSchema.setSeqNo(SeqNoArr[i]);
			  	sLOMsgReceiveSchema.setCustomerNo(CustomerNoArr[i]);
			  	sLOMsgReceiveSchema.setGrpContNo(GrpContNoArr[i]);
			  	sLOMsgReceiveSchema.setContNo(ContNoArr[i]);
			  	
				sLOMsgReceiveSchema.setMsgNo(request.getParameter("MsgNo"));
			  	sLOMsgReceiveSchema.setMakeDate(request.getParameter("MakeDate"));
			  	sLOMsgReceiveSchema.setMakeTime(request.getParameter("MakeTime"));
				tLOMsgReceiveSet.add(sLOMsgReceiveSchema);	
		  	}
		}				
	}
		
	if(transact.equals("DELETE||MAIN"))
	{
		tLOMsgInfoSchema.setMsgNo(request.getParameter("MsgNo"));
		String SeqNo = request.getParameter("MSGSEQ");
		int count = (SeqNo.length()+1)/12;

		for(int i = 0; i < count; i++)
		{
			LOMsgReceiveSchema tLOMsgReceiveSchema = new LOMsgReceiveSchema();
			tLOMsgReceiveSchema.setMsgNo(request.getParameter("MsgNo"));
			tLOMsgReceiveSet.add(tLOMsgReceiveSchema);
		}
	}
		
	  
  	try
  	{// 准备传输数据 VData
		System.out.println("------准备传输数据 VData--------");
  		VData tVData = new VData();
		tVData.add(tLOMsgInfoSchema);
		tVData.add(tLOMsgReceiveSet);
  		tVData.add(tG);
    	tOLOMsgInfoUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
	    Content = "操作失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
    tError = tOLOMsgInfoUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    	System.out.println(Content);
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
}
  
//添加各种预处理
%>     
				parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
			</script> 
	</body>
</html>
