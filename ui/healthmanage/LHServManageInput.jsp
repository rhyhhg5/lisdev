<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-02-27 18:16:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHServManageInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHServManageInputInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHServManageSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    
   <table  class= common align='center'>
  <TR  class= common>   
    <TD  class= title>
      客户号
    </TD>
    <TD  class= input>
      <Input class= 'code' name=CustomerNo  verify="客户号|len<=2" CodeData= "0|^1|a^2|b" ondblClick= "showCodeListEx('',[this,''],[0,1]);" onkeyup= "showCodeListKeyEx('',[this,''],[0,1]);">
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerName  verify="客户姓名|len<=20">
    </TD>
    <TD  class= title>
      首次纪录时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=FirstRecoDate verify=" ">
       <script language="JavaScript">
     <!--
     var FirstRecoTime=new timeControl("FirstRecoTime","txtDateE");
     document.write(FirstRecoTime);
     //-->
     </script>
    </TD>
	</TR>
	
	<TR class = common>
		<TD class = title>
			服务状态
		</TD>
		<TD class = input>
			<Input class = 'common' name =ServiceState >
		</TD>
		<TD class = title>
		</TD>
		<TD class = input>
		</TD>
		<TD class = title>
		</TD>
		<TD class = input>
		</TD>
	</TR>
    </table>
    
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServManage1);">
    		</td>
    		 <td class= titleImg>
        		 服务申请记录
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHServManage1" style= "display: ''">
    	<div id="divDescriptionDetail"  style="display: none; position:absolute; slategray">
    	     <textarea name="textDescriptionDetail" cols="100%" rows="3" witdh=25% class="common" onkeydown="backDescriptionDetail();">
    	     </textarea>
			 </div>
<table  class= common align='center' >
  <TR  class= common>   
    <TD  class= title>
      服务项目
    </TD>
    <TD  class= input>
    	<input type=hidden name=ApplyDutyItemCode>
      <Input class= 'code' name=ApplyDutyItemName verify="|len<=2" CodeData= "0|^1|a^2|b" ondblClick= "showCodeListEx('DutyItemCode',[ApplyDutyItemCode,this],[0,1]);" onkeyup= "showCodeListKeyEx('DutyItemCode',[ApplyDutyItemCode,this],[0,1]);">
    </TD>
    <TD  class= title>
      详细描述
    </TD>
    <TD  class= input>
      <Input class= 'common' id=DescriptionDetail name=DescriptionDetail ondblclick="return inputDescriptionDetail();" verify="详细描述|len<=600">
    </TD>
    <TD class = title >
    	服务时间
    </TD>
    <TD class = input>
    	<Input class= 'coolDatePicker' dateFormat="Short" name=ServiceDate verify=" ">
    	<script language="JavaScript">
     <!--
     var ServiceTime =new timeControl("ServiceTime","txtDateE");
     document.write(ServiceTime);
     //-->
     </script>
    </TD>
    	
	</TR>
	<TR  class= common>    

    <TD  class= title>
      服务机构	
    </TD>
    <TD  class= input>
    	<input type=hidden name=HospitCode>
      <Input class= 'common' name=HospitName  verify="服务机构|len<=20">
    </TD>
    <TD  class= title>
      服务人员
    </TD>
    <TD  class= input>
    	<input type=hidden name =DoctNo>
      <Input class= 'common' name=DoctName  verify="服务人员|len<=20">
    </TD>
  	<TD  class= title>
      申请受理方式
    </TD>
    <TD  class= input>
       <Input class= 'code' name=ApplyReceiveType verify="|len<=2" CodeData= "0|^1|a^2|b" ondblClick= "showCodeListEx('',[this,''],[0,1]);" onkeyup= "showCodeListKeyEx('',[this,''],[0,1]);">
    </TD>
  </TR>
	<TR  class= common> 
    <TD  class= title>
      是否需要预约
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ServiceFlag  verify="是否需要预约|len<=20" ondblClick="showCodeListEx('ServerFlag',[this,''],[0,1]);" verify="定点属性标识符|len<=2"  CodeData="0|^1|是^0|否" onkeyup="showCodeListKeyEx('ServerFlag',[this,''],[0,1]);">
    </TD>
    <TD class = title>
		</TD>
		<TD class = input>
		</TD>
		<TD class = title>
		</TD>
		<TD class = input>
		</TD>
  </TR>
</table>
	</div>
<Div id = "mainBook" style= "display: 'none'" >	
	 <table id = "ServBookReco" style= "display: ''">
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServManage2);">
    		</td>
    		 <td class= titleImg>
        		 服务预约记录
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHServManage2" style= "display: ''">
    	<div id="divApplyServiceDes"  style="display: none; position:absolute; slategray">
    	     <textarea name="textApplyServiceDes" cols="100%" rows="3" witdh=25% class="common" onkeydown="backApplyServiceDes();">
    	     </textarea>
			 </div>
<table  class= common align='center'>
	<TR class = comnon>
		<TD class = title>
			是否预约成功
		</TD>
		<TD class = input>
			 <Input class= 'code' name=IsBespeakOk verify="|len<=2" CodeData= "0|^1|a^2|b" ondblClick= "showCodeListEx('',[this,''],[0,1]);" onkeyup= "showCodeListKeyEx('',[this,''],[0,1]);">
		</TD>
		<TD class = title>
		</TD>
		<TD class = input>
		</TD>
		<TD class = title>
		</TD>
		<TD class = input>
		</TD>
	</TR>
	
	<TR><TD><br></TD></TR>
	
	<TR  class= common>    

    <TD  class= title>
      预约服务项目
    </TD>
    <TD  class= input>
    	<input type=hidden name=BespeakServiceItem>
      <Input class= 'common' name=BespeakServiceItemName  verify=" ">
    </TD>
    <TD  class= title>
      详细描述
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ApplyServiceDes ondblclick="return inputApplyServiceDes();" verify=" ">
    </TD>
  	<TD  class= title>
      预约服务时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=BespeakServiceDate verify=" ">
     <script language="JavaScript">
     <!--
     var BespeakServiceTime =new timeControl("BespeakServiceTime","txtDateE");
     document.write(BespeakServiceTime);
     //-->
     </script>  
    </TD>
  </TR>
	<TR  class= common>    

    <TD  class= title>
      预约服务机构
    </TD>
    <TD  class= input>
    	<input type=hidden name=BespeakServiceHospit>
      <Input class= 'common' name=BespeakServiceHospitName  verify=" ">
    </TD>
    <TD  class= title>
      预约服务人员
    </TD>
    <TD  class= input>
    	<input type=hidden name=BespeakServiceDoctNo>
      <Input class= 'common' name=BespeakServiceDoctName   verify=" ">
    </TD>
  	<TD  class= title>
      不相符原因
    </TD>
    <TD  class= input>
      <Input class= 'common' name=NoCompareCause  verify=" ">
    </TD>
  </TR>
  <TR class = common >
  	<TD class = title>
  		预约失败原因
  	</TD>
  	<TD class = input>
  		<Input class = 'common' name=BespeaLostCause>
  	</TD>
  	<TD class = title>
		</TD>
		<TD class = input>
		</TD>
		<TD class = title>
		</TD>
		<TD class = input>
		</TD>	
	</table>
</div>

		<table id = "ServImplement" style = "display:''">
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServManage3);">
    		</td>
    		 <td class= titleImg>
        		 服务执行情况
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHServManage3" style= "display: ''">
    	<div id="divExcuteDescription"  style="display: none; position:absolute; slategray">
    	     <textarea name="textExcuteDescription" cols="100%" rows="3" witdh=25% class="common" onkeydown="backExcuteDescription();">
    	     </textarea>
			 </div>
<table   class= common align='center' >
		<TR class = common>
			<TD class = title>
				服务执行状态
			</TD>
			<TD class = input>
				<Input class= 'code' name=ServiceExecState verify="|len<=2" CodeData= "0|^1|a^2|b" ondblClick= "showCodeListEx('',[this,''],[0,1]);" onkeyup= "showCodeListKeyEx('',[this,''],[0,1]);">
			</TD>
			<TD class = title>
		</TD>
		<TD class = input>
		</TD>
		<TD class = title>
		</TD>
		<TD class = input>
		</TD>
		</TR>
		<TR><TD><br></TD></TR>
		<TR class = common>
			<TD class = title>
				预约剩余时间（天）
			</TD>
			<TD class = input>
				<Input class = 'common' name=>
			</TD>
			<TD class = title>
			</TD>
			<TD class = input>
			</TD>
			<TD class = title>
			</TD>
			<TD class = input>
			</TD>
		</TR> 
		<TR class = common>
			<TD class = title>
				执行服务项目
			</TD>
			<TD class = input>
				<input type=hidden name=ExcuteServiceItem>
				<Input class = 'common' name=ExcuteServiceItemName>
			</TD>
			<TD class = title>
				详细描述
			</TD>
			<TD class = input>
				<Input class = 'common' name=ExcuteDescription ondblclick="return inputExcuteDescription();">
			</TD>
			<TD class = title>
				执行服务时间
			</TD>
			<TD class = input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=ExcuteServiceDate verify=" ">
			<script language="JavaScript">
     <!--
     var ExcuteServiceTime =new timeControl("ExcuteServiceTime","txtDateE");
     document.write(ExcuteServiceTime);
     //-->
     </script>  
			</TD>
		</TR>
		<TR class = common>
			<TD class = title>
				执行服务机构
			</TD>
			<TD class = input>
				<input type=hidden name=ExcuteServiceHospit>
				<Input class = 'common' name=ExcuteServiceHospitName>
			</TD>
			<TD class = title>
				执行服务人员
			</TD>
			<TD class = input>
				<input type=hidden name=ExcuteServiceDocNo>
				<Input class = 'common' name=ExcuteServiceDocName>
			</TD>
			<TD class = title>
				不相符原因
			</TD>
			<TD class = input>
				<Input class = 'common' name=ExNoCompareCause>
			</TD>
		</TR>
		<TR class = common>
			<TD class = title>
				实际支付费用（元）
			</TD>
			<TD class = input>
				<Input class = 'common' name=ExcutePay>
			</TD>
			<TD class = title>
				客户满意度
			</TD>
			<TD class = input>
				<Input class= 'code' name=SatisfactionDegree >
			</TD>
			<TD class = title>
				其他服务信息
			</TD>
			<TD class = input>
				<Input class = 'common' name=OtherServiceInfo>
			</TD>
		</TR>
		<TR class = common>	
			<TD class = title>
				服务取消原因
			</TD>
			<TD class = input>
				<Input class = 'common' name=CancleServiceCause>
			</TD>
			<TD class = title>
			</TD>
			<TD class = input>
			</TD>
			<TD class = title>
			</TD>
			<TD class = input>
			</TD>
		</TR>
		<TR><TD><br></TD></TR>
		
		
</table>
	</div>
	
</div>	
     <div id="div1" style="display: 'none'">
      <table>
        <TR  class= common>
            <TD  class= title>
              操作员代码
            </TD>
            <TD  class= input>
              <Input class= 'common' name=Operator >
            </TD>
            <TD  class= title>
              入机日期
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeDate >
            </TD>
            <TD  class= title>
              入机时间
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeTime >
            </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最后一次修改日期
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyDate >
          </TD>
          <TD  class= title>
            最后一次修改时间
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyTime >
          </TD>
        </TR>
      </table>
    </div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="RecordNo" name="RecordNo">
     <input type=hidden id="iscomefromquery" name="iscomefromquery">
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
