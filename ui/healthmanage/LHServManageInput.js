//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHServManage.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHServManageQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
	var strSQL="select * from LHServManage where FDSNo='"+arrQueryResult[0][0]+"'";
	arrResult = easyExecSql(strSQL);
	if( arrResult != null )
	{
        fm.all('FDSNo').value= arrResult[0][0];
        fm.all('CustomerNo').value= arrResult[0][1];
        fm.all('Name').value= arrResult[0][2];
        fm.all('FamilyDoctorNo').value= arrResult[0][3];
        fm.all('FamilyDoctorName').value= arrResult[0][4];
        fm.all('FDSCode').value= arrResult[0][5];
        fm.all('FDSSubject').value= arrResult[0][6];
        fm.all('FDSContent').value= arrResult[0][7];
        fm.all('State').value= arrResult[0][8];
        fm.all('Operator').value= arrResult[0][9];
        fm.all('MakeDate').value= arrResult[0][10];
        fm.all('MakeTime').value= arrResult[0][11];
        fm.all('ModifyDate').value= arrResult[0][12];
        fm.all('ModifyTime').value= arrResult[0][13];
        fm.all('FDSDate').value= arrResult[0][14];
	}
}
function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.Name.value = arrResult[0][1];
  }
}
function queryCustomerNo2()
{
	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
	var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}
function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.jsp");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}

//需要预约时,显示服务预约纪录与服务执行情况
function afterCodeSelect(codeName,Field)
{
	if(codeName=="ServerFlag")
	{
		if(Field.value=="1")
		{
			mainBook.style.display='';
		}
		else
		{
			if(Field.value=="0")
			{
				mainBook.style.display='none';
			}
		}
	}
}


// Written by cloudchen, 2004/03/15
// @name:必须：时间控件名称，自定义，不可重复
// @forObj:必须：对话数据项控件名称,不允许重复
// @fName:可选：时间控件组件,不允许重复
// last modfiy by 农民程序员([email]191301587@163.com[/email]) 2004年9月7日 19:01:02
function timeControl(name,forObj,fName) {
 this.name = name;
 this.fName = fName || "mctr_input_"+this.name;
 
 this.timer = null;
 this.fObj = null;
 
 this.toString = function() {
  var objDate = new Date();
  //
  var sMinute_Common = "class=\"mctr_input\" maxlength=\"2\" name=\""+this.fName+"\" onfocus=\""+
   this.name+".setFocusObj(this)\" onblur=\""+this.name+".setTime(this)\" onkeyup=\""+
   this.name+".prevent(this)\" onkeypress=\"if (!/[0-9]/.test(String.fromCharCode(event.keyCode)))event.keyCode=0\""+
   " onpaste=\"return false\" ondragenter=\"return false\" style=\"ime-mode:disabled\" onPropertychange=\""+
   this.name+".setForObjValue()\"";
  //
  var sButton_Common = "class=\"mctr_arrow\" onfocus=\"this.blur()\" onmouseup=\""+this.name+".controlTime()\" disabled";
  
  var str = "";
  str +=" <style type=\"text/css\">"
  str +=" .mctr_frameborder {"
  str +="  border-left: 1px inset #D4D0C8;"
  str +="  border-top: 1px inset #D4D0C8;"
  str +="  border-right: 1px inset #D4D0C8;"
  str +="  border-bottom: 1px inset #D4D0C8;"
  str +="  width: 130px;"
  str +="  height: 20px;"
  str +="  background-color: #FFFFFF;"
  str +="  overflow: hidden;"
  str +="  text-align: left;"       //text-align: right;
  str +="  vertical-align: text-top;"       
  str +="  font-family: \"Tahoma\";"
  str +="  font-size: 14px;"
  str +=" }"
  str +=" .mctr_arrow {"
  str +="  width: 16px;"
  str +="  height: 10px;"
  str +="  font-family: \"Webdings\";"
  str +="  font-size: 9px;"
  str +="  line-height: 2px;"
  str +="  background-color: #FFFFFF;"
  str +="  padding-left: 2px;"
  str +="  cursor: default;"
  str +=" }"
  str +=" .mctr_table {"
  str +="  padding-top: 0px;"
  str +="  padding-right: 0px;"
  str +="  padding-bottom: 0px;"
  str +="  padding-left: 0px;"

  str +=" }"
  str +=" .mctr_input {"
  str +="  width: 18px;"
  str +="  height: 16px;"
  str +="  border: 0px solid black;"
  str +="  font-size: 12px;"
  str +="  text-align: right;"
  str +="  background-color: transparent;"  
  str +=" }"
  str +=" </style>";
  str += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mctr_table\">"
  str += "<tr class=\"mctr_table\">"
  str += "<td class=\"mctr_table\">"
  str += "<span class=\"mctr_frameborder\">"
  str += "<input radix=\"24\" value=\""+this.formatTime(objDate.getHours())+"\" "+sMinute_Common+">:"
  str += "<input radix=\"60\" value=\""+this.formatTime(objDate.getMinutes())+"\" "+sMinute_Common+">:"
  str += "<input radix=\"60\" value=\""+this.formatTime(objDate.getSeconds())+"\" "+sMinute_Common+">"
  str += "</span>"
  str += "</td>"
  str += "<td class=\"mctr_table\">"
  str += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mctr_table\">"
  str += "<tr class=\"mctr_table\"><td class=\"mctr_table\"><button id=\""+this.fName+"_up\" "+sButton_Common+">5</button></td></tr>"
  str += "<tr class=\"mctr_table\"><td class=\"mctr_table\"><button id=\""+this.fName+"_down\" "+sButton_Common+">6</button></td></tr>"
  str += "</table>"
  str += "</td>"
  str += "<td>"
  str += "<input type=\"Text\" name=\""+forObj+"\" id=\""+forObj+"\" value=\""+this.getNowTime()+"\" style=\"display:'none'\">"
  str += "</td>"
  str += "</tr>"
  str += "</table>"
  return str;
 }
 this.getNowTime=function(){
  var objDate = new Date();
  return this.formatTime(objDate.getHours())+":"+
    this.formatTime(objDate.getMinutes())+":"+
    this.formatTime(objDate.getSeconds());
 }
 this.play = function() {
  this.timer = setInterval(this.name+".playback()",1000);
 }
 this.formatTime = function(sTime) {
  sTime = ("0"+sTime);
  return sTime.substr(sTime.length-2);
 }
 this.playback = function() {
  var objDate = new Date();
  var arrDate = [objDate.getHours(),objDate.getMinutes(),objDate.getSeconds()];
  var objMinute = document.getElementsByName(this.fName);
  for (var i=0;i<objMinute.length;i++) {
   objMinute[i].value = this.formatTime(arrDate[i])
  }
 }
 this.setForObjValue=function(){
  eval("document.all."+forObj+".value=this.getTime()");
 }
 this.prevent = function(obj) {
  clearInterval(this.timer);
  this.setFocusObj(obj);
  var value = parseInt(obj.value,10);
  var radix = parseInt(obj.radix,10)-1;
  if (obj.value>radix||obj.value<0) {
   obj.value = obj.value.substr(0,1);
  }
 }
 this.controlTime = function(cmd) {
  event.cancelBubble = true;
  if (!this.fObj) return;
  clearInterval(this.timer);
  var cmd = event.srcElement.innerText=="5"?true:false;
  var i = parseInt(this.fObj.value,10);
  var radix = parseInt(this.fObj.radix,10)-1;
  if (i==radix&&cmd) {
   i = 0;
  } else if (i==0&&!cmd) {
   i = radix;
  } else {
   cmd?i++:i--;
  }
  this.fObj.value = this.formatTime(i);
  this.fObj.select();
 }
 this.setTime = function(obj) {
  obj.value = this.formatTime(obj.value);
 }
 this.setFocusObj = function(obj) {
  eval("document.all."+this.fName+"_up").disabled = eval("document.all."+this.fName+"_down").disabled = false;
  this.fObj = obj;
 }
 this.getTime = function() {
  var arrTime = new Array(2);
  for (var i=0;i<document.getElementsByName(this.fName).length;i++) {
   arrTime[i] = document.getElementsByName(this.fName)[i].value;
  }
  return arrTime.join(":")
 }
 this.setInitControl=function(){
  var iTime=eval("document.all."+forObj+".value");
  var arrTime=new Array();
  arrTime=str2Array(iTime,":");
  for (var i=0;i<arrTime.length;i++){
   document.getElementsByName(this.fName)[i].value=arrTime[i];
  }
 }
}
//将字符串str按照字符串ch拆分产生数组
function str2Array(str,ch)
{
 var a=new Array();
 var i=0,j=-1,k=0;
 while (i<str.length)
 {  
  j=str.indexOf(ch,j+1);   
  if (j!=-1)
  {
   if (j==0){a[k]="";}else{a[k]=str.substring(i,j);}
   i=j+1;
  }else
  {
   a[k]=str.substring(i,str.length);
   i=str.length;
  }
  k++;
 }
 return a;
}

function inputDescriptionDetail()
{ 

	divDescriptionDetail.style.display='';
	 fm.all('textDescriptionDetail').value=fm.all('DescriptionDetail').value;
	

	
}
function backDescriptionDetail()

{

	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('DescriptionDetail').value=fm.all('textDescriptionDetail').value;
		fm.all('textDescriptionDetail').value="";
		divDescriptionDetail.style.display='none';
		
	}
}

function inputApplyServiceDes()
{ 

	divApplyServiceDes.style.display='';
	fm.all('textApplyServiceDes').value = fm.all('ApplyServiceDes').value;
	
	
	
}
function backApplyServiceDes()

{

	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('ApplyServiceDes').value=fm.all('textApplyServiceDes').value;
		fm.all('textApplyServiceDes').value="";
		divApplyServiceDes.style.display='none';
		
	}
}
function inputExcuteDescription()
{ 

	divExcuteDescription.style.display='';
	fm.all('textExcuteDescription').value = fm.all('ExcuteDescription').value;
	
	
	
}
function backExcuteDescription()

{

	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('ExcuteDescription').value=fm.all('textExcuteDescription').value;
		fm.all('textExcuteDescription').value="";
		divExcuteDescription.style.display='none';
		
	}
}