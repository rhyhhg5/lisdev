//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
//window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}
//提交，保存按钮对应操作
function submitForm() {
	
	if(fm.all('HealthNo').value != "")
	{
			alert("新建 '客户健康信息' 请点左栏对应菜单刷新本页面！");
			return false;
	}

	if( CustomerExit()== false )
	{return false;}
  var i = 0;
  if( verifyInput2() == false )
    return false;
     // if( checkValue2() == false )
  //  return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
  
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
  try {
    initForm();
  } catch(re) {
    alert("在LHCustomInHospital.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm() {
  //  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit() {
  //添加操作
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
  if(cDebug=="1") {
    parent.fraMain.rows = "0,0,50,82,*";
  } else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
//Click事件，当点击增加图片时触发该函数
function addClick() {
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
  fm.fmtransact.value = "INSERT||MAIN" ;
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick() {
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?")) {
//  	if(fm.all('iscomefromquery').value=="0")
//    {
//     alert("请先保存当前结果！");
//     return false;
//    }
    var i = 0;
    if( verifyInput2() == false )
      return false;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmtransact.value = "UPDATE||MAIN";
    fm.submit(); //提交
  } else {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick() {
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHMainCustomHealthQuery.html");
}
//Click事件，当点击“删除”图片时触发该函数
function deleteClick() {
	
	if(fm.all('HealthNo').value == "")
	{alert("无法删除，请重新查询一条信息");return false;}
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?")) {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmtransact.value = "DELETE||MAIN";
    fm.submit(); //提交
    initForm();
  } else {
    alert("您取消了删除操作！");
  }
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow) {
  if (cShow=="true") {
    cDiv.style.display="";
  } else {
    cDiv.style.display="none";
  }
}

function queryCustomerNo() 
{
  	if(fm.all('CustomerNo').value == "")	
  	{
  	  	//var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  	  	var newWindow = window.open("../sys/LDPersonQuery.html");
  	}
  	if(fm.all('CustomerNo').value != "") 
  	{
  	  	var cCustomerNo = fm.CustomerNo.value;  //客户代码
  	  	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
  	  	var arrResult = easyExecSql(strSql);
  	  	//alert(arrResult);
  	  	if (arrResult != null) {
  	  	  fm.CustomerNo.value = arrResult[0][0];
  	  	  alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
  	  	} else {
  	  	  //fm.DiseasCode.value="";
  	  	  alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
  	  	}
  	}
}

function afterQuery1(arrResult) 
{

  	if(arrResult!=null) {
  	
  	  fm.CustomerNo.value = arrResult[0][0];//fm.all('CustomerNo').readOnly = true;
  	  fm.CustomerName.value = arrResult[0][1];
  	  fm.HealthNo.value=arrResult[0][7];
  	  
  	  var strSql="select AddDate,BloodPressLow,BloodPressHigh,"
  	             +" Stature,Avoirdupois,AvoirdIndex,Smoke,KissCup,SitUp,DiningNoRule,BadHobby,HealthNo,MakeDate,MakeTime "
  	             +" from LHCustomHealthStatus where 1=1 "
  	             +getWherePart("HealthNo","HealthNo")
  	             +getWherePart("CustomerNo","CustomerNo")
//  	             +" and ManageCom like '"+tG.ManageCom+"%%'
									;

  	  var arrLHCustomHealthStatus=easyExecSql(strSql);
  	  
  	  
  	  if(arrLHCustomHealthStatus!=null) {
  	  	
  	    fm.AddDate.value=arrLHCustomHealthStatus[0][0];
  	    fm.BloodPressLow.value=arrLHCustomHealthStatus[0][1];
  	    fm.BloodPressHigh.value=arrLHCustomHealthStatus[0][2];
  	    fm.Stature.value=arrLHCustomHealthStatus[0][3];
  	    fm.Avoirdupois.value=arrLHCustomHealthStatus[0][4];
  	    fm.AvoirdIndex.value=arrLHCustomHealthStatus[0][5];
  	    fm.Smoke.value=arrLHCustomHealthStatus[0][6];
  	    fm.KissCup.value=arrLHCustomHealthStatus[0][7];
  	    fm.SitUp.value=arrLHCustomHealthStatus[0][8];
  	    fm.DiningNoRule.value=arrLHCustomHealthStatus[0][9];
  	    fm.BadHobby.value=arrLHCustomHealthStatus[0][10];
  	    fm.HealthNo.value=arrLHCustomHealthStatus[0][11];
  	    fm.MakeDate.value=arrLHCustomHealthStatus[0][12];
  	    fm.MakeTime.value=arrLHCustomHealthStatus[0][13];
  	    fm.BloodPress.value=Math.round(((fm.BloodPressHigh.value-fm.BloodPressLow.value)*100)/100);
  	  }
//	     else {
//	      alert("LHMainCustomerHealth.js->afterquery()出现错误:arrLHCustomHealthStatus为空");
//	    }
  	try
  	{
 			   strSql="select a.GymItemCode,b.codename, a.GymTime,a.GymFreque,a.CustomerGymNo "
			         +" from LHCustomGym a, ldcode b where a.CustomerNo = '"+arrResult[0][0]
			         +"' and b.codetype='gymitem' and a.GymItemCode=b.code and a.CustomerGymNo = '"+arrResult[0][7]+"'"
			         ;
  	  
  	     turnPage.queryModal(strSql, LHCustomGymGrid);
  	}
	  catch(e)
	  {
	  	alert(e);
	  }
  	
//  	 strSql= " select b.codename,a.FamilyCode,a.ICDCode,c.ICDName,a.FamilyDiseaseNO "
//  	        +" from LHCustomFamilyDiseas a, ldcode b,LDDisease c where a.CustomerNo = '"+arrResult[0][0]
//			      +" ' and b.codetype='familycode' and a.FamilyCode=b.code and a.ICDCode=c.ICDCode and a.FamilyDiseaseNo ='"+arrResult[0][7]+"'"
//  	        ;
  	 strSql = " select (select b.codename from ldcode b where  b.codetype='familycode' and b.code = a.Familycode),"
  	 		 		 +" a.FamilyCode,a.ICDCode, (select c.ICDName from lddisease c where c.ICDCode = a.ICDCode), "
  	 		 		 +" a.FamilyDiseaseNo   	 from LHCustomFamilyDiseas a  "
  	         +" where a.CustomerNo = '"+arrResult[0][0]+"' "
  	         +" and a.FamilyDiseaseNO = '"+arrResult[0][7]+"'"
  	         ;

  	 turnPage.queryModal(strSql, LHCustomFamilyDiseasGrid);
  	 fm.all('iscomefromquery').value=="1";
  	} 
  	else {
  	  alert("LHMainCustomerHealth.js->afterquery()出现错误");
  	}
}

function queryCustomerNo2() {

  if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24) {
    var cCustomerNo = fm.CustomerNo.value;  //客户号码
    var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
    // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    } else {
      //fm.DiseasCode.value="";
      alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
    }
  }
}

function queryDiseas() {
  if(fm.all('DiseasCode').value == "")	{
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("LDDiseaseQueryInput.jsp");
  }
  if(fm.all('DiseasCode').value != "") {
    var cDiseasCode = fm.DiseasCode.value;  //疾病代码
    var strSql = "select ICDCode,ICDName from LDDisease where ICDCode='" + cDiseasCode +"'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.DiseasCode.value = arrResult[0][0];
      alert("查询结果:  疾病代码:["+arrResult[0][0]+"] 疾病名称:["+arrResult[0][1]+"]");
    } else {
      //fm.DiseasCode.value="";
      alert("疾病代码为:["+fm.all('DiseasCode').value+"]的疾病不存在，请确认!");
    }
  }
}

function afterQuery00(arrResult) {
  if(arrResult!=null) {
    fm.DiseasCode.value = arrResult[0][0];
    fm.DiseasName.value = arrResult[0][1];
  }
}

function queryDiseas2() {
  if(fm.all('DiseasCode').value != ""&& fm.all('DiseasCode').value.length==20) {
    var cDiseasCode = fm.DiseasCode.value;  //疾病代码
    var strSql = "select ICDCode,ICDName from LDDisease where trim(ICDCode)='" + cDiseasCode +"'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.DiseasCode.value = arrResult[0][0];
      alert("查询结果:  疾病代码:["+arrResult[0][0]+"] 疾病名称:["+arrResult[0][1]+"]");
    } else {
      //fm.DiseasCode.value="";
      alert("疾病代码为:["+fm.all('DiseasCode').value+"]的疾病名称不存在，请确认!");
    }
  }
}


function computekgm2() {
  if(fm.all('Stature').value != ""&& fm.all('Avoirdupois').value != "") {
    var sStature = fm.Stature.value;
    var aAvoirdupois = fm.Avoirdupois.value;
    var aAvoirdIndex = aAvoirdupois/(sStature*sStature);
    var a=Math.round(aAvoirdIndex*100)/100;
		
    try {
      if(a.toString() == "NaN" || a.toString() == "Infinity") fm.all('AvoirdIndex').value = "";
      else{fm.all('AvoirdIndex').value = a ;}
    } catch(ex) {
      alert("体重指数的值不存在，请确认体重和身高!");
    }
  }
  else{fm.all('AvoirdIndex').value = "";}
}

//收缩压一定大于舒张压
function computebp() 
{
    if(fm.all('BloodPressHigh').value != ""&& fm.all('BloodPressLow').value != "") 
  {
    var bBloodPressHigh = fm.BloodPressHigh.value;
    var bBloodPressLow = fm.BloodPressLow.value;
    if(bBloodPressHigh >= bBloodPressLow)
    {
    	var bBloodPress = bBloodPressHigh-bBloodPressLow;
    	var a=Math.round(bBloodPress*100)/100;
    	try 
	    {
	      fm.all('BloodPress').value = a ;
	    } catch(ex) {alert("脉压差的值不存在，请确认血压舒张压和血压收缩压!");}
		}
		else
		{
			fm.all('BloodPress').value = "0";
		}
  }	
  
}

function computebp1()
{
	if(event.keyCode == "13")
	{

	  if(fm.all('BloodPressLow').value < 60)
	  {
	  	alert("舒张压一定大于60");
	  	fm.all('BloodPressLow').value ="";
	  }
		
		if(fm.all('BloodPressHigh').value != "" && fm.all('BloodPressLow').value != "") 
	  {
	    var bBloodPressHigh = fm.BloodPressHigh.value;
	    var bBloodPressLow = fm.BloodPressLow.value;
	    var cha = bBloodPressHigh - bBloodPressLow;
	    
	    if( cha > 0 )
	    {
	    	var bBloodPress = bBloodPressHigh-bBloodPressLow;
	    	var a=Math.round(bBloodPress*100)/100;
	    	try 
		    {
		      fm.all('BloodPress').value = a ;
		    } catch(ex) {alert("脉压差的值不存在，请确认血压舒张压和血压收缩压!");}
			}
	    
	  	else
			{
				alert("收缩压一定大于舒张压");
				fm.all('BloodPressHigh').value = "";    
			}	    
	  }	
	}
}

function afterQuery(arrResult) {
	
  if(arrResult!=null) {
    fm.CustomerNo.value = arrResult[0][0];
    fm.CustomerName.value = arrResult[0][1];
    //fm.all('iscomeformquery').value="0";
//    var strSql="select AddDate,BloodPressLow,BloodPressHigh,"
//               +" Stature,Avoirdupois,AvoirdIndex,Smoke,KissCup,SitUp,DiningNoRule,BadHobby "
//               +" from LHCustomHealthStatus where 1=1 "+getWherePart("CustomerNo","CustomerNo");
//
//    var arrLHCustomHealthStatus=easyExecSql(strSql);
//    
//    
//    if(arrLHCustomHealthStatus!=null) {
//      fm.AddDate.value=arrLHCustomHealthStatus[0][0];
//      fm.BloodPressLow.value=arrLHCustomHealthStatus[0][1];
//      fm.BloodPressHigh.value=arrLHCustomHealthStatus[0][2];
//      fm.Stature.value=arrLHCustomHealthStatus[0][3];
//      fm.Avoirdupois.value=arrLHCustomHealthStatus[0][4];
//      fm.AvoirdIndex.value=arrLHCustomHealthStatus[0][5];
//      fm.Smoke.value=arrLHCustomHealthStatus[0][6];
//      fm.KissCup.value=arrLHCustomHealthStatus[0][7];
//      fm.SitUp.value=arrLHCustomHealthStatus[0][8];
//      fm.DiningNoRule.value=arrLHCustomHealthStatus[0][9];
//      fm.BadHobby.value=arrLHCustomHealthStatus[0][10];
//      fm.BloodPress.value=Math.round(((fm.BloodPressHigh.value-fm.BloodPressLow.value)*100)/100);
//    }
////     else {
////      alert("LHMainCustomerHealth.js->afterquery()出现错误:arrLHCustomHealthStatus为空");
////    }
// try{  strSql="select a.GymItemCode,b.codename, a.GymTime,a.GymFreque "
//         +" from LHCustomGym a, ldcode b where 1=1 and b.codetype='gymitem' and a.GymItemCode=b.code "
//          +getWherePart("a.CustomerNo","CustomerNo");
//          //strSql="seletct a.GymItemCode,'', a.GymTime,a.GymFreque from LHCustomGym a where 1=1 "
//          //+getWherePart("a.CustomerNo","CustomerNo");
//
//    
//    turnPage.queryModal(strSql, LHCustomGymGrid);
//  }
//  catch(e)
//  {
//  	alert(strSql);
//  	}
//
//   strSql="select b.codename,a.FamilyCode,a.ICDCode,c.ICDName,a.FamilyDiseaseNO "
//          +" from LHCustomFamilyDiseas a, ldcode b,LDDisease c where 1=1 and b.codetype='familycode' and a.FamilyCode=b.code and a.ICDCode=c.ICDCode "
//          +getWherePart("a.CustomerNo","CustomerNo");
//   
//   turnPage.queryModal(strSql, LHCustomFamilyDiseasGrid);

  } else {
    alert("LHMainCustomerHealth.js->afterquery()出现错误");
  }
}

function CustomerExit()
{
	var cCustomerNo = fm.CustomerNo.value;  //客户代码
  var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
  var arrResult = easyExecSql(strSql);
  	  	//alert(arrResult);
  if (arrResult == null) 
  {
   	  alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
   	  return false;
  }
}
