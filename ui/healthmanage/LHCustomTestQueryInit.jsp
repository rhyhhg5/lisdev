<%
//程序名称：LHCustomTestQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-17 18:32:42
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CustomerNo').value = "";
    fm.all('CustomerName').value = "";
   // fm.all('TestNo').value = "";
   // fm.all('MedicaItemCode').value = "";
   // fm.all('TestDate').value = "";
   // fm.all('HospitCode').value = "";
   // fm.all('Doctor').value = "";
  }
  catch(ex) {
    alert("在LHCustomTestQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHCustomTestGrid();  
  }
  catch(re) {
    alert("LHCustomTestQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHCustomTestGrid;
function initLHCustomTestGrid() {                               
  var iArray = new Array();
    
  try {
     iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="客户号码";   
	  iArray[1][1]="60px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
    
    iArray[2]=new Array(); 
	  iArray[2][0]="客户姓名";   
	  iArray[2][1]="40px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
    
    iArray[5]=new Array(); 
	  iArray[5][0]="InHospitNo";   
	  iArray[5][1]="0px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=3;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="医疗机构名称";   
	  iArray[6][1]="150px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=1;
	         
	  iArray[3]=new Array(); 
	  iArray[3][0]="体检时间";   
	  iArray[3][1]="50px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=0;
	  
	  
	  
	 // iArray[6]=new Array(); 
	 // iArray[6][0]="体检医师姓名";   
	 // iArray[6][1]="60px";   
	 // //iArra6[5][2]=20;        
	 // iArray[6][3]=0;
	 // 
	  iArray[4]=new Array(); 
	  iArray[4][0]="体检方式 ";   
	  iArray[4][1]="60px";   
	  iArray[4][3]=0;
    
    LHCustomTestGrid = new MulLineEnter( "fm" , "LHCustomTestGrid" ); 
    //这些属性必须在loadMulLine前
    
    LHCustomTestGrid.mulLineCount = 0;   
//    LHCustomTestGrid.displayTitle = 1;
    LHCustomTestGrid.hiddenPlus = 1;
    LHCustomTestGrid.hiddenSubtraction = 1;
    LHCustomTestGrid.canSel = 1;
//    LHCustomTestGrid.canChk = 0;
   //LHCustomTestGrid.selBoxEventFuncName = "showOne";

    LHCustomTestGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomTestGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
