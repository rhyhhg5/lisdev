var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";
var cAskType="";
//提交，保存按钮对应操作
function submitForm()
{
  
  if ( fm.LogNo.value!="")
      {
      	alert("你不能执行改操作,请刷新页面再造作");
      	return false;
      	}
    if (confirm("您确实想保存该记录吗?"))
    {
        tSaveFlag = "1";
     
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      	
        fm.fmtransact.value="INSERT||MAIN";
        fm.submit(); //提交
        tSaveFlag ="0";

      
      
    }
    else
    {
      alert("您取消了修改操作！");
    }

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
	
  showInfo.close();
  var arrResult = new Array();
  if(fm.SerialNo.value!="")
  {
  	arrResult=easyExecSql("select AskType from LLMainAsk where  logno='"+ fm.LogNo.value +"'");
  	cAskType=arrResult[0][0];
  }	
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    if (fm.fmtransact.value=="DELETE||MAIN")
    {
    	fm.reset();
    	initCustomerGrid();
    }
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
 function addClick()
{

  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if(cAskType==fm.AskType.value)
	{
	if (confirm("您确实想修改该记录吗?"))
 	{
 		
 		
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      //showSubmitFrame(mDebug);
      fm.fmtransact.value = "UPDATE||MAIN"
      fm.submit(); //提交
	    }//end of else
	 
	  else
	  {
	    mOperate="";
	    alert("您取消了修改操作！");
	  }
	}
else
	{
		alert("登陆类型不能修改！");
		}
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	window.open("./LHMainAskQuery.html");
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function deleteClick()
{
	  if ( fm.LogNo.value=="")
      {
      	alert("请先查询数据");
      	return false;
    }
    
	if (confirm("删除记录会删除所有咨询记录，您确实想删除该记录吗?"))
 	{	
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      showSubmitFrame(mDebug);
      fm.fmtransact.value = "DELETE||MAIN"
      fm.submit(); //提交
 }//end of else 
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function submitForm1()
{
  tSaveFlag = "0";
	if((fm.PeopleType.value=="")||(fm.PeopleType.value=="null"))
	{
		alert("请您录入事故者类型！！！");
		return;
	}
	if((fm.RptObj.value=="")||(fm.RptObj.value=="null"))
	{
		alert("请您录入号码类型！！！");
		return;
	}
	if (fm.RptObjNo.value=="")
	{
		alert("请您收入要号码！");
 		return ;
  }
  if(fm.RptObj.value=="0")
  {
  	if(fm.PeopleType.value!="0")
  	{
  		alert("号码类型是团单，事故者类型只能是被保险人，请重新录入事故者类型");
  		return;
  	}
  	else
  	{
  		var i = 0;
   		var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  		initSubReportGrid();
  		fm.action = "./ReportQueryOut1.jsp";
  		fm.submit(); //提交
  	}
  }
  else
  {
  	var i = 0;
   	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	initSubReportGrid();
  	fm.action = "./ReportQueryOut1.jsp";
  	fm.submit(); //提交
  }
}
//完成按照”客户号“在lcpol中进行查询，显示该客户的保单明细
function showInsuredLCPol()
{
  var row;
	var a_count=0;//判断选中了多少行
  var t = SubReportGrid.mulLineCount;//得到Grid的行数
  for(var i=0;i<t;i++)
  {
    varCount = SubReportGrid.getChkNo(i);
    if(varCount==true)
    {
       a_count++;
       row=i;
    }
  }
  if(a_count>1)
  {
    alert("您只能选中一行记录！");
    return;
  }
  else if(a_count<1)
  {
    alert("请您选中一条分案记录!");
    return;
  }
  else
  {
    var varInsuredNo;
    var varCount;
    varInsuredNo=SubReportGrid.getRowColData(row,1);
    if ((varInsuredNo=="null")||(varInsuredNo==""))
    {
      alert("客户号为空，不能进行查询操作！");
      return;
    }
    var varSrc = "&InsuredNo=" + SubReportGrid.getRowColData(row,1);
    var newWindow = window.open("./FrameMainReportLCPol.jsp?Interface=ReportLCPolInput.jsp"+varSrc,"ReportLCPolInput",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
}

function showCustomerInfo()
{
	window.open("../sys/FrameCQPersonQuery.jsp");
}

function afterCodeSelect( cCodeName, Field ) {
	try	{	 
	    if ( cCodeName =="AskType")
	    {
	    //	if ( Field.value =="0" )
	    //	{
	    //		fm.NoticeIn.style.display='none';
	    //		fm.AskIn.style.display='';	
	    //	}else if ( Field.value =="1" ){
	    //		fm.AskIn.style.display='none';	
	    //		fm.NoticeIn.style.display='';
	    //	}else 
	   
	  // if ( Field.value=="0")
	  //  {
    //         showConsult();
   	//          fm.NoticeIn.style.display='none';
		//	fm.AskIn.style.display='none';
	  //  }
	  //    if ( Field.value=="1")
	  //  {
	  //  	showNotice();
	  //  	fm.NoticeIn.style.display='none';
		//	fm.AskIn.style.display='none';
	  //  }
	  //  if ( Field.value =="2" ){
	  //  		fm.NoticeIn.style.display='';
	  //  		fm.AskIn.style.display='';
	  //  	}
	    
	    }
	    
	    
	   
	   }catch(ex)
	   {
	   	alert(ex.message);
	}
}
function showConsult()
{
		    divCustomerInfo.style.display='';
		    DivComCusInfo.style.display='';
	    	DivCusInfo.style.display='';
	    	divEventInfo.style.display='';
	    	AnswerInfo.style.display='';
	    	//
	    	DivNoticeInfo.style.display='none';
	    	
}
function showNotice()
{
	divCustomerInfo.style.display='';
	DivComCusInfo.style.display='';
	divEventInfo.style.display='';
	DivNoticeInfo.style.display='';
	//
	DivCusInfo.style.display='none';	    	
	AnswerInfo.style.display='none';
 }
function showButton(para)
{
	if ( para==0)
	{
		showConsult();
	}else
		{
			showNotice();
			}
	
}
function inputNotice()
{
}
function inputConsult()
{
	
	if ( fm.LogNo.value=="")
	{
		alert("请先保存登记");
		return ;
	}
	
	
	parent.fraInterface.window.location="LLConsultInput.jsp?loadflag=1&logNo="+fm.LogNo.value;

}

function onSelSelected1(parm1,parm2)
{
	var row=CustomerGrid.getSelNo()-1;
	fm.ConsultNotmp.value=CustomerGrid.getRowColData(row,4);
	fm.CNNo.value=CustomerGrid.getRowColData(row,4);
	if(fm.AskType.value=="0")
	{
		fm.ConsultNo.value =CustomerGrid.getRowColData(row,4);
		showConsult();
		Consultbutton.style.display='';
		arrResult = new Array();
		arrResult0 = new Array();
		if(CustomerGrid.getRowColData(row,1)!=null)
		{
			var strsql="select * from LLConsult where CustomerNo='"+CustomerGrid.getRowColData(row,4)+"'";
			arrResult = easyExecSql(strsql);
			alert(arrResult);
			try{fm.all('CustomerNo').value=arrResult[0][2]}catch(ex){alert(ex.message+"CustomerNo")}
			try{fm.all('CustomerName').value=arrResult[0][3]}catch(ex){alert(ex.message+"CustomerName")}
			try{fm.all('CustomerType').value=arrResult[0][4]}catch(ex){alert(ex.message+"CustomerType")}
			try{fm.all('DiseaseCode').value=arrResult[0][11]}catch(ex){alert(ex.message+"DiseaseCode")}
			try{fm.all('DiseaseDesc').value=arrResult[0][13]}catch(ex){alert(ex.message+"DiseaseDesc")}
			try{fm.all('AccCode').value=arrResult[0][12]}catch(ex){alert(ex.message+"AccCode")}
			try{fm.all('AccDesc').value=arrResult[0][14]}catch(ex){alert(ex.message+"AccDesc")}
			try{fm.all('HospitalCode').value=arrResult[0][15]}catch(ex){alert(ex.message+"HospitalCode")}
			try{fm.all('HospitalName').value=arrResult[0][16]}catch(ex){alert(ex.message+"HospitalName")}
			try{fm.all('InHospitalDate').value=arrResult[0][17]}catch(ex){alert(ex.message+"InHospitalDate")}
			try{fm.all('OutHospitalDate').value=arrResult[0][18]}catch(ex){alert(ex.message+"OutHospitalDate")}
			try{fm.all('CustStatus').value=arrResult[0][22]}catch(ex){alert(ex.message+"CustStatus")}
			try{fm.all('CSubject').value=arrResult[0][5]}catch(ex){alert(ex.message+"CSubject")}
			try{fm.all('CContent').value=arrResult[0][6]}catch(ex){alert(ex.message+"CContent")}
			try{fm.all('ExpertFlag').value=arrResult[0][20]}catch(ex){alert(ex.message+"ExpertFlag")}
			try{fm.all('ExpertNo').value=arrResult[0][21]}catch(ex){alert(ex.message+"ExpertNo")}
			try{fm.all('ExpertName').value=arrResult[0][22]}catch(ex){alert(ex.message+"ExpertName")}
			try{fm.all('AvaiFlag').value=arrResult[0][24]}catch(ex){alert(ex.message+"AvaiFlag")}
			try{fm.all('ExpRDate').value=arrResult[0][9]}catch(ex){alert(ex.message+"ExpRDate")}
			try{fm.all('AskGrade').value=arrResult[0][8]}catch(ex){alert(ex.message+"AskGrade")}
			try{fm.all('AvaiFlag').value=arrResult[0][24]}catch(ex){alert(ex.message+"AvaiFlag")}
			strsql="select Answer from LLAnswerInfo where ConsultNo='"+CustomerGrid.getRowColData(row,4)+"'";
			arrResult0 = easyExecSql(strsql);
			if(arrResult0!=null&arrResult0!="")
			try{fm.all('Answer').value=arrResult0[0][0]}catch(ex){alert(ex.message+"Answer")}
			
			}
		
	}
	if(fm.AskType.value=="1")
	{
		showNotice();
		Noticebutton.style.display='';  
		arrResult = new Array();
		if(CustomerGrid.getRowColData(row,1)!=null)
		{
			var strsql="select * from LLNotice where CustomerNo='"+CustomerGrid.getRowColData(row,1)+"'";
			arrResult = easyExecSql(strsql);
			try{fm.all('CustomerNo').value=arrResult[0][2]}catch(ex){alert(ex.message+"CustomerNo")}
			try{fm.all('CustomerName').value=arrResult[0][3]}catch(ex){alert(ex.message+"CustomerName")}
			try{fm.all('CustomerType').value=arrResult[0][4]}catch(ex){alert(ex.message+"CustomerType")}
			try{fm.all('DiseaseCode').value=arrResult[0][9]}catch(ex){alert(ex.message+"DiseaseCode")}
			try{fm.all('DiseaseDesc').value=arrResult[0][11]}catch(ex){alert(ex.message+"DiseaseDesc")}
			try{fm.all('AccCode').value=arrResult[0][10]}catch(ex){alert(ex.message+"AccCode")}
			try{fm.all('AccDesc').value=arrResult[0][12]}catch(ex){alert(ex.message+"AccDesc")}
			try{fm.all('HospitalCode').value=arrResult[0][13]}catch(ex){alert(ex.message+"HospitalCode")}
			try{fm.all('HospitalName').value=arrResult[0][14]}catch(ex){alert(ex.message+"HospitalName")}
			try{fm.all('InHospitalDate').value=arrResult[0][15]}catch(ex){alert(ex.message+"InHospitalDate")}
			try{fm.all('OutHospitalDate').value=arrResult[0][16]}catch(ex){alert(ex.message+"OutHospitalDate")}
			try{fm.all('CustStatus').value=arrResult[0][17]}catch(ex){alert(ex.message+"CustStatus")}
			try{fm.all('NSubject').value=arrResult[0][7]}catch(ex){alert(ex.message+"NSubject")}
			try{fm.all('NContent').value=arrResult[0][8]}catch(ex){alert(ex.message+"NContent")}
			
		}
	}
	if(fm.AskType.value=="2")
	{
		
		divCustomerInfo.style.display='';
		DivComCusInfo.style.display='';
	    DivCusInfo.style.display='';
	    divEventInfo.style.display='';
	    DivNoticeInfo.style.display='';
	    Consultbutton.style.display='none';
        Noticebutton.style.display='none';
        AnswerInfo.style.display='none';   
        CNSave.style.display='';

		arrResult = new Array();
		arrResult0 = new Array();
		if(CustomerGrid.getRowColData(row,1)!=null)
		{
			var strsql="select * from LLConsult where CustomerNo='"+CustomerGrid.getRowColData(row,1)+"'";
			arrResult = easyExecSql(strsql);
			try{fm.all('CustomerNo').value=arrResult[0][2]}catch(ex){alert(ex.message+"CustomerNo")}
			try{fm.all('CustomerName').value=arrResult[0][3]}catch(ex){alert(ex.message+"CustomerName")}
			try{fm.all('CustomerType').value=arrResult[0][4]}catch(ex){alert(ex.message+"CustomerType")}
			try{fm.all('DiseaseCode').value=arrResult[0][11]}catch(ex){alert(ex.message+"DiseaseCode")}
			try{fm.all('DiseaseDesc').value=arrResult[0][13]}catch(ex){alert(ex.message+"DiseaseDesc")}
			try{fm.all('AccCode').value=arrResult[0][12]}catch(ex){alert(ex.message+"AccCode")}
			try{fm.all('AccDesc').value=arrResult[0][14]}catch(ex){alert(ex.message+"AccDesc")}
			try{fm.all('HospitalCode').value=arrResult[0][15]}catch(ex){alert(ex.message+"HospitalCode")}
			try{fm.all('HospitalName').value=arrResult[0][16]}catch(ex){alert(ex.message+"HospitalName")}
			try{fm.all('InHospitalDate').value=arrResult[0][17]}catch(ex){alert(ex.message+"InHospitalDate")}
			try{fm.all('OutHospitalDate').value=arrResult[0][18]}catch(ex){alert(ex.message+"OutHospitalDate")}
			try{fm.all('CustStatus').value=arrResult[0][19]}catch(ex){alert(ex.message+"CustStatus")}
			try{fm.all('CSubject').value=arrResult[0][5]}catch(ex){alert(ex.message+"CSubject")}
			try{fm.all('CContent').value=arrResult[0][6]}catch(ex){alert(ex.message+"CContent")}
			try{fm.all('ExpertFlag').value=arrResult[0][20]}catch(ex){alert(ex.message+"ExpertFlag")}
			try{fm.all('ExpertNo').value=arrResult[0][21]}catch(ex){alert(ex.message+"ExpertNo")}
			try{fm.all('ExpertName').value=arrResult[0][22]}catch(ex){alert(ex.message+"ExpertName")}
			try{fm.all('AvaiFlag').value=arrResult[0][24]}catch(ex){alert(ex.message+"AvaiFlag")}
			try{fm.all('ExpRDate').value=arrResult[0][9]}catch(ex){alert(ex.message+"ExpRDate")}
			try{fm.all('AskGrade').value=arrResult[0][8]}catch(ex){alert(ex.message+"AskGrade")}
			try{fm.all('AvaiFlag').value=arrResult[0][24]}catch(ex){alert(ex.message+"AvaiFlag")}
			strsql="select NSubject,NContent from LLNotice where CustomerNo='"+CustomerGrid.getRowColData(row,1)+"'";
			arrResult = easyExecSql(strsql);
			try{fm.all('NSubject').value=arrResult[0][0]}catch(ex){alert(ex.message+"NSubject")}
			try{fm.all('NContent').value=arrResult[0][1]}catch(ex){alert(ex.message+"NContent")}
			
			strsql="select Answer from LLAnswerInfo where ConsultNo='"+CustomerGrid.getRowColData(row,4)+"'";
			arrResult0 = easyExecSql(strsql);
			if(arrResult0!=null&arrResult0!="")
			try{fm.all('Answer').value=arrResult0[0][0]}catch(ex){alert(ex.message+"Answer")}
			AnswerInfo.style.display=''; 
		}
		
		
		
	}
	if(fm.AskType.value==0)
	{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject from LLSubReport,LLAskRela where LLSubReport.CustomerNo='"+CustomerGrid.getRowColData(row,1)+"' and LLAskRela.SubRptNo=LLSubReport.SubRptNo";
	}
	if(fm.AskType.value==1)
	{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject from LLSubReport,LLNoticeRela where LLSubReport.CustomerNo='"+CustomerGrid.getRowColData(row,1)+"' and LLNoticeRela.SubRptNo=LLSubReport.SubRptNo";
	}
	if(fm.AskType.value==2)
	{
		strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject from LLSubReport,LLNoticeRela,LLAskRela where LLSubReport.CustomerNo='"+CustomerGrid.getRowColData(row,1)+"' and LLNoticeRela.SubRptNo=LLSubReport.SubRptNo and LLAskRela.SubRptNo=LLSubReport.SubRptNo";
	}
	
	turnPage.queryModal(strsql, SubReportGrid);
	/*
	  var len =CustomerGrid.getSelNo() - 1;	
	   
	  var i = 0;
	  var logno= fm.LogNo.value ;
	  if (logno=="" ) 
	  {
	  	alert("请先保存登记");
	  	return ;
	  	}
      var ConsultNo =	CustomerGrid.getRowColData(len,4);
      var CustomerNo =	CustomerGrid.getRowColData(len,1);
      var CustomerName =CustomerGrid.getRowColData(len,2);
      if ( fm.AskType.value=="0")
      {
      	tSaveType="Consult";
 	  window.open("LLConsultMain.html?logNo="+logno+"&ConsultNo="+ConsultNo+"&CustomerNo="+CustomerNo+"&AskType="+fm.AskType.value+"&CustomerName="+CustomerName,"咨询录入",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
 	}
 	if ( fm.AskType.value=="1")
      {
      	tSaveType="Notice"
      	alert("待完成");
 	  //window.open("LLConsultMain.html?logNo="+logno+"&ConsultNo="+ConsultNo+"&CustomerNo="+CustomerNo,"咨询录入",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
 	}
 	*/
  
}




function afterQueryLL(arr)
{
/**
*由客户查询返回调用
*/
	
	if ( arr)
	{
		var len = CustomerGrid.mulLineCount;
		if (len<=0) len = 0;
		for ( i=0;i<arr.length;i++)
		{
			row =len+i;
			
			CustomerGrid.addOne();			
			CustomerGrid.setRowColData(row, 1,arr[i][0]);
			CustomerGrid.setRowColData(row, 2,arr[i][1]);
		}
	}
}

function afterQuery(arr)
{
	
	var arrReturn = arr;
	
	 divCustomerInfo.style.display='none';
	 DivComCusInfo.style.display='none';
	 DivCusInfo.style.display='none';
	 divEventInfo.style.display='none';
	 AnswerInfo.style.display='none';    	
	 DivNoticeInfo.style.display='none';
	 Consultbutton.style.display='none';
     Noticebutton.style.display='none';  
	 CNSave.style.display='none';
	
	if ( tSaveType=="EventQuery")
 	{
 	for ( i=0;i<arr.length;i++)
 	{
 		len = SubReportGrid.mulLineCount;
 	
 		SubReportGrid.addOne();                          
 		
 		SubReportGrid.setRowColData(len,1,arrReturn[i][0]);
 		SubReportGrid.setRowColData(len,2,arrReturn[i][1]);
 		SubReportGrid.setRowColData(len,3,arrReturn[i][2]);
 		SubReportGrid.setRowColData(len,4,arrReturn[i][3]);
 		SubReportGrid.setRowColData(len,5,arrReturn[i][4]);
 				
 	}
 	  return;
 	}
	try{
	var logno = arr[0][0] ;
	
	fm.LogNo.value = logno ;
	var strSQL = "select * from LLMainAsk where logno='"+ logno +"'";
		var qryResult = easyQueryVer3(strSQL,1,1,1);
	if ( !qryResult )
	{
	  alert("没有符合条件的记录");
	  return ;
	}
	var de = decodeEasyQueryResult(qryResult);
	if ( de )
	{
		setMainAskInfo(de)
		var sql="";
		if ( fm.AskType.value=="0")
		{
			 sql ="select customerno,customername,'咨询',consultno from llconsult where logno='"+ logno +"'";
		}                
		if (fm.AskType.value=="1") 
        {
        	 sql ="select customerno,customername,'通知',noticeno from llnotice where logno='"+ logno +"'";
        }                 
		if (fm.AskType.value=="2")  
		{
			 sql ="select customerno,customername,'咨询/通知',consultno from llconsult  where  logno='"+ logno +"'";
		}
		 
		
		var arrResult = new Array();
		arrResult=easyExecSql("select AskType from LLMainAsk where  logno='"+ logno +"'");
		cAskType=arrResult[0][0];
		turnPage.queryModal(sql, CustomerGrid);
	}else
		{
			alert("查询错误!");
		}
	}catch(ex)
        {
        	alert( ex.message );
        }
    if ( fm.AskType.value=="0")
	    {
             divCustomerInfo.style.display='';
   	    }
	      
	if ( fm.AskType.value=="1")
	    {
	    	divCustomerInfo.style.display='';
	    	CNSave.style.display='none';
	    }
	if ( fm.AskType.value =="2" )
		{
			divCustomerInfo.style.display='';
	    }    
        
	

//	var de = decode
//	if ( arr)
//	{
//		var len = CustomerGrid.mulLineCount;
//		if (len<=0) len = 0;
//		for ( i=0;i<arr.length;i++)
//		{
//			row =len+i;
//			
//			CustomerGrid.addOne();			
//			CustomerGrid.setRowColData(row, 1,arr[i][0]);
//			CustomerGrid.setRowColData(row, 2,arr[i][1]);
//		}
//	}
}

function setMainAskInfo(arrQueryResult)
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		
		 try{fm.all('LogNo').value=arrResult[0][0]}catch(ex){alert(ex.message+"LogNo")}
		try{fm.all('LogState').value=arrResult[0][1]}catch(ex){alert(ex.message+"LogState")}
		try{fm.all('AskType').value=arrResult[0][2]}catch(ex){alert(ex.message+"AskType")}
		try{fm.all('OtherNo').value=arrResult[0][3]}catch(ex){alert(ex.message+"OtherNo")}
		try{fm.all('OtherNoType').value=arrResult[0][4]}catch(ex){alert(ex.message+"OtherNoType")}
		try{fm.all('AskMode').value=arrResult[0][5]}catch(ex){alert(ex.message+"AskMode")}
		try{fm.all('LogerNo').value=arrResult[0][6]}catch(ex){alert(ex.message+"LogerNo")}
		try{fm.all('LogName').value=arrResult[0][7]}catch(ex){alert(ex.message+"LogName")}
		try{fm.all('LogComp').value=arrResult[0][8]}catch(ex){alert(ex.message+"LogComp")}
		try{fm.all('LogCompNo').value=arrResult[0][9]}catch(ex){alert(ex.message+"LogCompNo")}
		try{fm.all('LogDate').value=arrResult[0][10]}catch(ex){alert(ex.message+"LogDate")}
		try{fm.all('LogTime').value=arrResult[0][11]}catch(ex){alert(ex.message+"LogTime")}
		try{fm.all('Phone').value=arrResult[0][12]}catch(ex){alert(ex.message+"Phone")}
		try{fm.all('Mobile').value=arrResult[0][13]}catch(ex){alert(ex.message+"Mobile")}
		try{fm.all('PostCode').value=arrResult[0][14]}catch(ex){alert(ex.message+"PostCode")}
		try{fm.all('AskAddress').value=arrResult[0][15]}catch(ex){alert(ex.message+"AskAddress")}
		try{fm.all('Email').value=arrResult[0][16]}catch(ex){alert(ex.message+"Email")}
		try{fm.all('AnswerType').value=arrResult[0][17]}catch(ex){alert(ex.message+"AnswerType")}
		try{fm.all('AnswerMode').value=arrResult[0][18]}catch(ex){alert(ex.message+"AnswerMode")}
		try{fm.all('SendFlag').value=arrResult[0][19]}catch(ex){alert(ex.message+"SendFlag")}
		try{fm.all('SwitchCom').value=arrResult[0][20]}catch(ex){alert(ex.message+"SwitchCom")}
		try{fm.all('SwitchDate').value=arrResult[0][21]}catch(ex){alert(ex.message+"SwitchDate")}
		try{fm.all('SwitchTime').value=arrResult[0][22]}catch(ex){alert(ex.message+"SwitchTime")}
		try{fm.all('ReplyFDate').value=arrResult[0][23]}catch(ex){alert(ex.message+"ReplyFDate")}
		try{fm.all('DealFDate').value=arrResult[0][24]}catch(ex){alert(ex.message+"DealFDate")}
		//try{//fm.all('Remark').value=arrResult[0][25]}catch(ex){alert(ex.message+"Remark")}
		try{fm.all('AvaiFlag').value=arrResult[0][26]}catch(ex){alert(ex.message+"AvaiFlag")}
		try{fm.all('NotAvaliReason').value=arrResult[0][27]}catch(ex){alert(ex.message+"NotAvaliReason")}
		try{fm.all('Operator').value=arrResult[0][28]}catch(ex){alert(ex.message+"Operator")}
		try{fm.all('MngCom').value=arrResult[0][29]}catch(ex){alert(ex.message+"MngCom")}
		try{fm.all('MakeDate').value=arrResult[0][30]}catch(ex){alert(ex.message+"MakeDate")}
		try{fm.all('MakeTime').value=arrResult[0][31]}catch(ex){alert(ex.message+"MakeTime")}
		try{fm.all('ModifyDate').value=arrResult[0][32]}catch(ex){alert(ex.message+"ModifyDate")}
		try{fm.all('ModifyTime').value=arrResult[0][33]}catch(ex){alert(ex.message+"ModifyTime")}
        
	}
}

  function RelaQuery()
  {
  	var paramNo;
  	if ( fm.AskType.value=="0")
  	{
  		paramNo= fm.ConsultNo.value;
  		
  	}
  	else if (fm.AskType.value=="1")
  	{
  			paramNo = fm.NoticeNo.value ;
  			
  	}
  	else if( fm.AskType.value=="2")
  	{
  		  paramNo = fm.CNNo.value ;
  	}	  
  	
    tSaveType="EventQuery"
  	showInfo= window.open("./LLSubReportQuery.jsp?CustomerNo="+fm.CustomerNo.value+"&CustomerName="+fm.CustomerName.value+"&AskType="+fm.AskType.value+"&ParamNo="+paramNo,"客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  	}
  	
 function ReplySave()
 {
  
  if(fm.AskType.value=="0")
  {
   if ( fm.ConsultNo.value =="")
	 {
	  alert("没有咨询信息");
	  return false;
	 }
  }
  if(fm.AskType.value=="2")
  {
  	if(fm.CNNo.value=="")
  	{
  	alert("没有咨询通知信息");
  	return false;
  	}
  }	
  
  if ( fm.SerialNo.value =="") 
  {
	  fm.fmtransact.value="INSERT||MAIN";
  }
  else
 	{
 		fm.fmtransact.value="UPDATE||MAIN";
 	}
	
	
	fm.action= "./LLAnswerInfoSave.jsp";
  fm.submit();
	tSaveType=="Answer";

 }
 
 function DelRela()
{
 
 
 if (fm.AskType.value=="0")
 {
 	
   if ( fm.ConsultNo.value =="")
	  {
	   alert("没有咨询信息");
	   return false;
	  }
 }
 if (fm.AskType.value=="1")
 {
 	
   if ( fm.NoticeNo.value =="")
	  {
	   alert("没有通知信息");
	   return false;
	  }
 }
 
 if( fm.AskType.value=="2")
 {
 	 if (fm.CNNo.value=="")
 	 {
 	 	alert("没有咨询通知信息");
 	 	return false;
 	 	}
}
 if ( 	SubReportGrid.mulLineCount<=0)
 {
 	alert("没有要删除的关联事件。");
 	return false;
 	}
	fm.fmtransact.value="DELETE||RELA";
	var row=SubReportGrid.getSelNo()-1;
	var tEventNo=SubReportGrid.getRowColData(row,1);
		
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action= "./LLSubReportSave.jsp?tEventNo="+tEventNo;

	tSaveType="Event";
	fm.submit(); 	

}
function delcnsave()
 {
 	
 	if ( fm.LogNo.value=="")
    {
      	alert("请先保存登记信息");
      	return false;
    }
    if (confirm("您确实想删除该咨询记录吗?"))
    {
      //  tSaveFlag = "1";
     
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      
        fm.fmtransact.value="DELETE||MAIN";
        var row=CustomerGrid.getSelNo()-1;
        fm.ConsultNo.value=CustomerGrid.getRowColData(row,4);
        fm.NoticeNo.value=CustomerGrid.getRowColData(row,4);
        fm.action = "LLConsultNoticeSave.jsp";
        fm.submit(); //提交
        tSaveFlag ="0";
	}
    else
    {
      alert("您取消了修改操作！");
    }
}

 
 function insertCust()
{
	updateCustomer();
	
	/*
	if ( fm.LogNo.value=="")
	{
		alert("没有登记号");
	}
	if ( fm.ConsultNo.value =="")
	{
		fm.fmtransact.value="INSERT||MAIN";
	}else
		{
			fm.fmtransact.value="UPDATE||MAIN";
		}
		alert( fm.fmtransact.value );
	fm.action= "./LLConsultSave.jsp";
	tSaveType="Consult"
	submitForm();
	*/
}
function updateCustomer(CNNo)
{
	var strSQL="";
	
	if(fm.AskType.value=="0")
		strSQL="select CustomerNo,CustomerName,'咨询',ConsultNo from LLConsult where ConsultNo='"+fm.ConsultNo.value+"'";
	if(fm.AskType.value=="1")
		strSQL="select CustomerNo,CustomerName,'通知',NoticeNo from LLNotice where NoticeNo='"+fm.NoticeNo.value+"'";
	if(fm.AskType.value=="2")
		strSQL="select CustomerNo,CustomerName,'咨询/通知',NoticeNo from LLNotice where NoticeNo='"+fm.CNNo.value+"'";
		
	turnPage.queryModal(strSQL,CustomerGrid);
}
function 	ClientQuery()
{
	//openWindow("ClientQueryMain.html","客户查询");
	//window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  	var strSql="select CustomerNo,Name,IDNo from ldperson where CustomerNo='"+fm.LogerNo.value+"' or CustomerName='"+fm.LogName.value+"'";
  	var arr=easyExecSql(strSql);
  	if(arr!=null)
  	{
  		fm.LogerNo.value=arr[0][0];
  		fm.LogName.value=arr[0][1];
  		fm.IDNo.value   =arr[0][2];
  	}
}

function EventSave()
{
	
	row=CustomerGrid.getSelNo()-1;
	if(row<0)
	{
		alert("请选择客户信息！");
		return false;
	}
	
	tSaveType="Event";
	if(fm.AskType.value=="0")
	{
	  if ( fm.ConsultNo.value =="")
	  {
		  alert("请先保存咨询信息");
		  return 
	  }
	}
	if(fm.AskType.value=="1")
	{
		if(fm.NoticeNo.value=="")
		 {
		 	 alert("请先保存咨询信息");
		 	 return
		 }
	}	   
	if(fm.AskType.value=="2")
	{
		if ( fm.CNNo.value=="")
		{
			 alert("请先保存咨询通知信息");
			 return
		}
	}		 
	
	if ( trim(fm.SubRptNo.value) !="")
	{
		if ( !confirm("确定要增加新事件吗?"))
		{
			return;
		}
  }
	
	fm.fmtransact.value="INSERT||MAIN";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action= "./LLSubReportSave.jsp";
  fm.submit();	

}

function updateEventGrid(EventNo)
{

	var row=CustomerGrid.getSelNo()-1;
	if (fm.fmtransact.value=="INSERT||MAIN")
	{
		    
    	if(fm.AskType.value==0)
		{
			strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.InHospitalDate,LLSubReport.OutHospitalDate from LLSubReport,LLAskRela where LLSubReport.CustomerNo='"+CustomerGrid.getRowColData(row,1)+"' and LLAskRela.SubRptNo=LLSubReport.SubRptNo";
		}
		if(fm.AskType.value==1)
		{
			strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.InHospitalDate,LLSubReport.OutHospitalDate from LLSubReport,LLNoticeRela where LLSubReport.CustomerNo='"+CustomerGrid.getRowColData(row,1)+"' and LLNoticeRela.SubRptNo=LLSubReport.SubRptNo";
		}
		if(fm.AskType.value==2)
		{
			strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.InHospitalDate,LLSubReport.OutHospitalDate from LLSubReport,LLNoticeRela,LLAskRela where LLSubReport.CustomerNo='"+CustomerGrid.getRowColData(row,1)+"' and LLNoticeRela.SubRptNo=LLSubReport.SubRptNo and LLAskRela.SubRptNo=LLSubReport.SubRptNo";
		}
		
		turnPage.queryModal(strsql, SubReportGrid);
	}
	if(fm.fmtransact.value=="DELETE||RELA")
	{

		if(fm.AskType.value==0)
		{
			strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject from LLSubReport,LLAskRela where LLSubReport.CustomerNo='"+CustomerGrid.getRowColData(row,1)+"' and LLAskRela.SubRptNo=LLSubReport.SubRptNo";
		}
		if(fm.AskType.value==1)
		{
			strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject from LLSubReport,LLNoticeRela where LLSubReport.CustomerNo='"+CustomerGrid.getRowColData(row,1)+"' and LLNoticeRela.SubRptNo=LLSubReport.SubRptNo";
		}
		if(fm.AskType.value==2)
		{

			strsql="select LLSubReport.SubRptNo,LLSubReport.AccDate,LLSubReport.AccPlace,LLSubReport.AccidentType,LLSubReport.AccSubject from LLSubReport,LLNoticeRela,LLAskRela where LLSubReport.CustomerNo='"+CustomerGrid.getRowColData(row,1)+"' and LLNoticeRela.SubRptNo=LLSubReport.SubRptNo and LLAskRela.SubRptNo=LLSubReport.SubRptNo";
		}

		turnPage.queryModal(strsql, SubReportGrid);
	}

}

function NoticeSave()
{
	  tSaveType="Notice";
	 
	  if ( fm.LogNo.value=="")
      {
      	alert("请先保存登记信息");
      	return false;
      	}
    if (confirm("您确实想保存该记录吗?"))
    {
      //  tSaveFlag = "1";
     
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      
        fm.fmtransact.value="INSERT||MAIN";
        fm.action = "LHNoticeSave.jsp";
        fm.submit(); //提交
        tSaveFlag ="0";


	}
}


function CNSave1()
{

	 
	 tSaveType="CNSave";
	 
	  if ( fm.LogNo.value=="")
      {
      	alert("请先保存登记信息");
      	return false;
      }
    if (confirm("您确实想保存该记录吗?"))
    {
      //  tSaveFlag = "1";
     
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      
        fm.fmtransact.value="INSERT||MAIN";
        fm.action = "LHConsultNoticeSave.jsp";
        fm.submit(); //提交
        tSaveFlag ="0";


	  }
	 
}






function ConsultSave()
{

	 
	 tSaveType="Consult";
	 
	  if ( fm.LogNo.value=="")
      {
      	alert("请先保存登记信息");
      	return false;
      	}
    if (confirm("您确实想保存该记录吗?"))
    {
      //  tSaveFlag = "1";
     
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      
        fm.fmtransact.value="INSERT||MAIN";
        fm.action = "LHConsultSave.jsp";
        fm.submit(); //提交
        tSaveFlag ="0";

      
    }
    else
    {
      alert("您取消了修改操作！");
    }
	
}	 	


function deleteCust()
{

	 
	 tSaveType="Consult";
	 
	  if ( fm.LogNo.value=="")
      {
      	alert("请先保存登记信息");
      	return false;
      	}
    if (confirm("您确实想删除该咨询记录吗?"))
    {
      //  tSaveFlag = "1";
     
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      
        fm.fmtransact.value="DELETE||MAIN";
        var row=CustomerGrid.getSelNo()-1;
        fm.ConsultNo.value=CustomerGrid.getRowColData(row,4);
        fm.action = "LLConsultSave.jsp";
        fm.submit(); //提交
        tSaveFlag ="0";

      
    }
    else
    {
      alert("您取消了修改操作！");
    }
	
}	
function displayMain()
{
	  	
	  	
	  /*	if ( fm.AskType.value =="0" )
	    	{
	   
	    		fm.NoticeIn.style.display='none';
	    		fm.AskIn.style.display='';	
	    	}
	    
	    else if ( fm.AskType.value =="1" )
	    	{
	    	
	    		fm.AskIn.style.display='none';	
	    		fm.NoticeIn.style.display='';
	    	}*/
	   fm.CustomerNo.value=fm.LogerNo.value;
	   fm.CustomerName.value=fm.LogName.value;
	   if ( fm.AskType.value=="0")
	    {
             showConsult();
   	         Consultbutton.style.display='';
             Noticebutton.style.display='none';  

	    }
	      
	    if ( fm.AskType.value=="1")
	    {
	    	showNotice();
	    	CNSave.style.display='none';

	    }
	    if ( fm.AskType.value =="2" )
	    {
	        
	      divCustomerInfo.style.display='';
		    DivComCusInfo.style.display='';
	    	DivCusInfo.style.display='';
	    	divEventInfo.style.display='';
	    	AnswerInfo.style.display='';    	
	    	DivNoticeInfo.style.display='';
	       Consultbutton.style.display='none';
           Noticebutton.style.display='none';  
	      CNSave.style.display='';
	      //fm.CNSave.type='button';
		}

}    		
function EventQuery()
{
	var row=SubReportGrid.getSelNo()-1;
	
	if(row<0)
	{
		alert("没有选中事件！");
		return false;
	}
    else
	{
		divLLLLEventInput1.style.display="";
	}
	if(SubReportGrid.getRowColData(row,1)!=null)
	{
		var sql="select * from LLSubReport where SubRptNo='"+SubReportGrid.getRowColData(row,1)+"'";
		arrResult = new Array();
		arrResult = easyExecSql(sql);
		if (arrResult!=null&&arrResult!="")
		{
			try{fm.all('AccidentType1').value=arrResult[0][5]}catch(ex){alert(ex.message+"AccidentType")}
			try{fm.all('AccDate1').value=arrResult[0][6]}catch(ex){alert(ex.message+"AccDate")}
			try{fm.all('AccEndDate1').value=arrResult[0][7]}catch(ex){alert(ex.message+"AccEndDate")}
			try{fm.all('AccPlace1').value=arrResult[0][10]}catch(ex){alert(ex.message+"AccPlace")}
			try{fm.all('HospitalCode1').value=arrResult[0][11]}catch(ex){alert(ex.message+"HospitalCode")}
			try{fm.all('HospitalName1').value=arrResult[0][12]}catch(ex){alert(ex.message+"HospitalName")}
			try{fm.all('InHospitalDate1').value=arrResult[0][13]}catch(ex){alert(ex.message+"InHospitalDate")}
			try{fm.all('OutHospitalDate1').value=arrResult[0][14]}catch(ex){alert(ex.message+"OutHospitalDate")}
			try{fm.all('SeriousGrade1').value=arrResult[0][16]}catch(ex){alert(ex.message+"SeriousGrade")}
			try{fm.all('AccSubject1').value=arrResult[0][4]}catch(ex){alert(ex.message+"AccSubject")}
			try{fm.all('AccDesc1').value=arrResult[0][8]}catch(ex){alert(ex.message+"AccDesc")}
			try{fm.all('SeriousGrade1').value=arrResult[0][16]}catch(ex){alert(ex.message+"SeriousGrade")}
		}		
	}
}
function DelNotice()
{
	if ( fm.LogNo.value=="")
    {
      	alert("请先保存登记信息");
      	return false;
    }
    if (confirm("您确实想删除该咨询记录吗?"))
    {
      //  tSaveFlag = "1";
     
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      	
        fm.fmtransact.value="DELETE||MAIN";
        var row=CustomerGrid.getSelNo()-1;
        if(row<0)
        {
        	alert("请在咨询列表中选择要删除的数据！");
        	return false;
        }
        fm.NoticeNo.value=CustomerGrid.getRowColData(row,4);
        fm.action = "LLNoticeSave.jsp";
        fm.submit(); //提交
        tSaveFlag ="0";
	}
    else
    {
      alert("您取消了修改操作！");
    }
}
//点击事件Mulline激活查询事件
function ShowRela()
{
	EventQuery();
}
//判断查询输入窗口的案件类型是否是回车， 
//如果是回车调用查询客户函数
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClientQuery();
	}
}
function QueryOnKeyDown1()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClientQuery1();
	}
}
function ClientQuery1()
{
	var strSql="select CustomerNo,Name,IDNo from ldperson where CustomerNo='"+fm.CustomerNo.value+"' or CustomerName='"+fm.CustomerName.value+"'";
  	var arr=easyExecSql(strSql);
  	if(arr!=null)
  	{
  		fm.CustomerNo.value=arr[0][0];
  		fm.CustomerName.value=arr[0][1];
  	}
}
function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitalCode1").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}