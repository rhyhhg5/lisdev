//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{ 
	if(fm.ClassLevel.value=="4")
		{
			fm.action="./LHCountrMedicaItemSave.jsp";
			divStyle4.style.display='';
		}
		else
		{
			fm.action="./LDClassInfoSave.jsp";		
			divStyle4.style.display='none';
		}
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCountrMedicaItem.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if(fm.ClassLevel.value=="4")
		{
			fm.action="./LHCountrMedicaItemSave.jsp";
			divStyle4.style.display='';
		}
		else
		{
			fm.action="./LDClassInfoSave.jsp";		
			divStyle4.style.display='none';
		}
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //alert();
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  if(fm.ClassLevel.value=="4")
		{
			fm.action="./LHCountrMedicaItemSave.jsp";
			divStyle4.style.display='';
		}
		else
		{
			fm.action="./LDClassInfoSave.jsp";		
			divStyle4.style.display='none';
		}
  showInfo=window.open("./LHCountrMedicaItemQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if(fm.ClassLevel.value=="4")
		{
			fm.action="./LHCountrMedicaItemSave.jsp";
			divStyle4.style.display='';
		}
		else
		{
			fm.action="./LDClassInfoSave.jsp";		
			divStyle4.style.display='none';
		}
  
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult,flag )
{
	var arrResult = new Array();
	if( arrQueryResult !=  null )
	{
		if (flag==4)
		{
			divStyle4.style.display='';
	    	var strSQL="select MedicaItemCode,MedicaItemName,PriceUnit,MedicaMeanin,ExcConten,Explai,StandardMeasureUnit,InBelly,Frequencyflag,Nomalvalue,ClassCode from LHCountrMedicaItem  where MedicaItemCode='"+arrQueryResult[0][0]+"'";
      		arrResult=easyExecSql(strSQL,1,0);
        
	        fm.all('ClassLevel').value='4';
	        fm.all('ClassLevel_ch').value='4级代码';
	        fm.all('MedicaItemCode').value= arrResult[0][0];
	        fm.all('MedicaItemName').value= arrResult[0][1];
	        fm.all('PriceUnit').value     =arrResult[0][2];
	        fm.all('MedicaMeanin').value= arrResult[0][3];
	        fm.all('ExcConten').value= arrResult[0][4];
	        fm.all('Explai').value= arrResult[0][5];
	        fm.all('StandardMeasureUnit').value =arrResult[0][6];
	      	fm.all('InBelly').value = arrResult[0][7];
	      	fm.all('ResuDescriptStyle').value = arrResult[0][8];
	      	if (fm.all('ResuDescriptStyle').value=='1')
	      	  fm.all('ResuDescriptStyle_ch').value='数值描述'
	      	else
	      		fm.all('ResuDescriptStyle_ch').value='文字描述'
      		fm.all('StandardValue').value = arrResult[0][9];
      			if(arrResult[0][7]=="1"){fm.all('InBelly_ch').value="是";}
      			if(arrResult[0][7]=="0"){fm.all('InBelly_ch').value="否";}
      		fm.all('ClassCode').value = arrResult[0][10];
		}
	    else
		{
			divStyle4.style.display='none';
			var strSQL="select Classcode,Classname,Operator,Makedate,Maketime from LDClassinfo  where Classcode='"+arrQueryResult[0][0]+"'";
	        arrResult=easyExecSql(strSQL);
	        fm.all('ClassLevel').value=flag;
	        fm.all('ClassLevel_ch').value=flag+'级代码';
	        fm.all('MedicaItemCode').value= arrResult[0][0];
	        fm.all('MedicaItemName').value= arrResult[0][1];
	        fm.all('Operator').value= arrResult[0][2];
	        fm.all('MakeDate').value= arrResult[0][3];
	        fm.all('MakeTime').value= arrResult[0][4];
	     
		}
	}
}               
        
function afterCodeSelect(codeName,Field)
{
	if(codeName=="ClassLevell")
	{
		if(fm.ClassLevel.value=="4")
		{
//			fm.action="./LHCountrMedicaItemSave.jsp";
			divStyle4.style.display='';
		}
		else
		{
//			fm.action="./LDClassInfoSave.jsp";		
			divStyle4.style.display='none';
		}
	}
}