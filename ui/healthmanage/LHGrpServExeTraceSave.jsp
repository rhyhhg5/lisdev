<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHGrpServExeTraceSave.jsp
//程序功能：
//创建日期：2006-03-09 16:05:14
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数

  OLHGrpServExeTraceUI tOLHGrpServExeTraceUI   = new OLHGrpServExeTraceUI();
  LHGrpServExeTraceSet tLHGrpServExeTraceSet   = new LHGrpServExeTraceSet();

  String  tGrpServItemNo[] = request.getParameterValues("LHServItemGrid10");					//MulLine的列存储数组
  String  tExeState[] = request.getParameterValues("LHServItemGrid4");					//MulLine的列存储数组
  String  tExeDate[] = request.getParameterValues("LHServItemGrid5");					//MulLine的列存储数组
  String  tServDesc[] = request.getParameterValues("LHServItemGrid6");					//MulLine的列存储数组
  String  tMakeDate[] = request.getParameterValues("LHServItemGrid7");					//MulLine的列存储数组
  String  tMakeTime[] = request.getParameterValues("LHServItemGrid8");					//MulLine的列存储数组
  String  tGrpServPlanNo[]= request.getParameterValues("LHServItemGrid9");	
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
   // tLHGrpServExeTraceSchema.setGrpServPlanNo(request.getParameter("GrpServPlanNo"));
   // tLHGrpServExeTraceSchema.setGrpServItemNo(request.getParameter("GrpServItemNo"));
   // tLHGrpServExeTraceSchema.setGrpContNo(request.getParameter("GrpContNo"));
   // tLHGrpServExeTraceSchema.setExeState(request.getParameter("ExeState"));
   // tLHGrpServExeTraceSchema.setExeDate(request.getParameter("ExeDate"));
   // tLHGrpServExeTraceSchema.setServDesc(request.getParameter("ServDesc"));
   // tLHGrpServExeTraceSchema.setManageCom(request.getParameter("ManageCom"));
   // tLHGrpServExeTraceSchema.setOperator(request.getParameter("Operator"));
   // tLHGrpServExeTraceSchema.setMakeDate(request.getParameter("MakeDate"));
   // tLHGrpServExeTraceSchema.setMakeTime(request.getParameter("MakeTime"));
   // tLHGrpServExeTraceSchema.setModifyDate(request.getParameter("ModifyDate"));
   // tLHGrpServExeTraceSchema.setModifyTime(request.getParameter("ModifyTime"));
    
   
    int LHGrpServExeTraceCount = 0;
		if(tGrpServItemNo != null)
		{	
			LHGrpServExeTraceCount = tGrpServItemNo.length;
		}	
		System.out.println(" LHGrpServExeTraceCount is : "+LHGrpServExeTraceCount);
		
		for(int i = 0; i < LHGrpServExeTraceCount; i++)
		{
				LHGrpServExeTraceSchema tLHGrpServExeTraceSchema = new LHGrpServExeTraceSchema();
			
				tLHGrpServExeTraceSchema.setGrpServItemNo(tGrpServItemNo[i]);	
				tLHGrpServExeTraceSchema.setExeState(tExeState[i]);	
				tLHGrpServExeTraceSchema.setExeDate(tExeDate[i]);
				tLHGrpServExeTraceSchema.setServDesc(tServDesc[i]);	
				tLHGrpServExeTraceSchema.setGrpServPlanNo(tGrpServPlanNo[i]);	
				tLHGrpServExeTraceSchema.setGrpContNo(request.getParameter("GrpContNo"));
				tLHGrpServExeTraceSchema.setOperator(request.getParameter("Operator"));
				tLHGrpServExeTraceSchema.setMakeDate(tMakeDate[i]);
				tLHGrpServExeTraceSchema.setMakeTime(tMakeTime[i]);
				tLHGrpServExeTraceSchema.setManageCom(request.getParameter("ManageCom")); 

				tLHGrpServExeTraceSet.add(tLHGrpServExeTraceSchema);
			                        
		}
       
    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLHGrpServExeTraceSet);
  	tVData.add(tG);
    tOLHGrpServExeTraceUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHGrpServExeTraceUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
