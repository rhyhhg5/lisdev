<%
//程序名称：LHGrpPersonUniteQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-03-09 15:52:53
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('GrpServPlanNo').value = "";
//    fm.all('GrpServItemNo').value = "";
    fm.all('GrpContNo').value = "";
    fm.all('GrpCustomerNo').value = "";
    fm.all('GrpName').value = "";
    fm.all('CustomerNo').value = "";
//  fm.all('Name').value = "";
//    fm.all('Sex').value = "";
//    fm.all('Birthday').value = "";
//    fm.all('IDType').value = "";
//    fm.all('IDNo').value = "";
//    fm.all('Level').value = "";
//    fm.all('ManageCom').value = "";
//    fm.all('Operator').value = "";
//    fm.all('MakeDate').value = "";
//    fm.all('MakeTime').value = "";
//    fm.all('ModifyDate').value = "";
//    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LHGrpPersonUniteQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHGrpPersonUniteGrid();  
  }
  catch(re) {
    alert("LHGrpPersonUniteQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHGrpPersonUniteGrid;
function initLHGrpPersonUniteGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="团体服务计划号码";   
	  iArray[1][1]="60px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=1;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="客户号";   
	  iArray[2][1]="60px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=1;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="客户姓名";   
	  iArray[3][1]="80px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=1;
	  
    iArray[4]=new Array(); 
    iArray[4][0]="团体内客户分级标识";   
    iArray[4][1]="70px";   
    iArray[4][2]=20;        
    iArray[4][3]=1;
    
    iArray[5]=new Array(); 
    iArray[5][0]="入机日期";   
    iArray[5][1]="0px";   
    iArray[5][2]=20;        
    iArray[5][3]=3;
    
    iArray[6]=new Array(); 
    iArray[6][0]="入机时间";   
    iArray[6][1]="0px";   
    iArray[6][2]=20;        
    iArray[6][3]=3;
    LHGrpPersonUniteGrid = new MulLineEnter( "fm" , "LHGrpPersonUniteGrid" ); 
    //这些属性必须在loadMulLine前

    LHGrpPersonUniteGrid.mulLineCount = 0;   
    LHGrpPersonUniteGrid.displayTitle = 1;
    LHGrpPersonUniteGrid.hiddenPlus = 0;
    LHGrpPersonUniteGrid.hiddenSubtraction = 0;
    LHGrpPersonUniteGrid.canSel = 1;
    LHGrpPersonUniteGrid.canChk = 0;
   // LHGrpPersonUniteGrid.selBoxEventFuncName = "showOne";

    LHGrpPersonUniteGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHGrpPersonUniteGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
