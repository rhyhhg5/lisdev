<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-19 13:58:55
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDDrugInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDDrugInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <Div id = "divQueryButton" style="display: 'none'" align=right>
    	<table>
    		<tr>
    			<td class=button width="10%" align=right>
						<INPUT class=cssButton name="querybutton1" VALUE="查  询"  TYPE=button onclick="return queryClick();">
					</td>	
				</tr>	
			</table>
			<hr>
		</Div>	
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDDrug1);">
    		</td>
    		 <td class= titleImg>
        		 药品基本信息
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDDrug1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      药品（分类）代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ClassCode elementtype=nacessary verify="">
    </TD>
    <TD  class= title>
      药品（分类）名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ClassName elementtype=nacessary verify="">
    </TD>
    <TD  class= title>
      代码分类等级
    </TD>
    <TD  class= input>
    	<Input class= common type=hidden name= ClassLevel>
      <Input class= 'code	' name=ClassLevel_ch  verify="|len<=2" CodeData= "0|^1|1级^2|2级^3|3级^4|4级" ondblClick= "showCodeListEx('DrugClassLevel1',[this,ClassLevel],[1,0],null,null,null,1);" >
    </TD>
    <!--TD  class= title>
      药品编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugCode elementtype=nacessary verify="药品编号|notnull&len<=50">
    </TD>
    <TD  class= title>
      英文名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugEnName verify="药品名称|len<=50">
    </TD-->
    
  </TR>
</Table>
</Div>

<Div  id= "divLDDrug2" style= "display: 'none'">
 <table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      英文名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugEnName verify="药品名称|len<=50">
    </TD>
    <TD  class= title>
      曾用名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugNameAbbr  verify="中文简称|len<=50">
    </TD>
    <TD  class= title>
      名称注释
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrgUsage  verify="使用说明|len<=50">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      医保药品目录标志
    </TD>
    <TD  class= 'input'>
      <Input class= 'code' name=IndexFlag verify="药品名称|len<=2" CodeData= '0|^1|甲类目录^2|乙类目录^3|未归类' ondblClick="showCodeListEx('',[this,''],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('FixFlag',[this,''],[0,1]);">
    </TD>
    <TD  class= title>
      药品剂型
    </TD>
    <TD  class= input>
      <Input class= 'code' elementtype=nacessary name=DrugDoseCode verify="药品剂型|NUM & len<=2" CodeData= '0|^1|a^2|b^3|c' ondblClick="return showCodeListEx('DrugDoseCode',[this],[0]);" onkeyup="return showCodeListKeyEx('DrugDoseCode',[this],[0]);">
    </TD> 
           
  </TR>

	 <TR class= common>
		 <TD  class= title>
		      限制使用范围:
		 </TD>
		  <TD  class= title>
		 </TD>
		  <TD  class= title>
		 </TD>	
	 </TR>  
	  </table>
<table  class= common align='center' >     
  <TR  class= common>                        
    <TD  class= input>
      <!--<Input class= 'common' type=hidden name=DocContent verify="内容简介|len<=2000">-->
      <TextArea class='common' cols="108%" rows="5" witdh="120%" name=LimitUse verify="内容简介|len<=50000"></TextArea>
    </TD>
  </TR>
</table>

    </Div>

		
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
		<input type=hidden name="LoadFlag">
		<input type=hidden name= "ClassInfoCode" value= "DrugClass">
    <input type=hidden name="Operator" value="hm">
    <input type=hidden name="MakeDate">
    <input type=hidden name="MakeTime">
    <input type=hidden name="ModifyDate">
    <input type=hidden name="ModifyTime">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
