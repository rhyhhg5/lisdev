<%
//程序名称：LHGroupContInput.jsp
//程序功能：
//创建日期：2005-03-19 15:05:48
//创建人  ：CrtHtml程序创建
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-03 10:38:48
// 更新原因/内容: 插入新的字段
%>
<!--用户校验类-->
<%
      String HospitalNo = request.getParameter("HospitalNo");
      String ContraNo = request.getParameter("ContraNo");
      String ContFlag=request.getParameter("ContFlag");
      System.out.println(ContFlag);

%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ContraNo').value = "";
    fm.all('HospitCode').value = "";
    fm.all('HospitName').value = "";
    fm.all('ContraName').value = "";
    fm.all('IdiogrDate').value = "";
    fm.all('ContraBeginDate').value = "";
    fm.all('ContraEndDate').value = "";
    fm.all('ContraState').value = "";
    fm.all('ContraState_ch').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";	
    fm.all('ContraItemNo').value = "";
	  fm.all('LHFlag').value = "2";
	  fm.all('HiddenBtn').value="<%=ContFlag%>";

  }
  catch(ex)
  {
    alert("在LHGroupContInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LHGroupContInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
  //  initSelBox();
    initGroupContGrid();
    
	  fm.all('txtFlag').value="<%=ContFlag%>";
   if(fm.all('txtFlag').value!=""&&fm.all('txtFlag').value!="null"&&fm.all('txtFlag').value!=null)//从医疗机构检索进入此页面为1
    {
    	  fm.all('querybutton').disabled=true;
        fm.all('deleteButton').disabled=true;
        fm.all('modifyButton').disabled=true;
        fm.all('saveButton').disabled=true;
        
        InGroupContInfo();
    }
  }
  catch(re)
  {
    alert("LHGroupContInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}  
/*

function initGroupContGrid() {                               
  var iArray = new Array();    
        
  try {    
    iArray[0]=new Array();    
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="";         //列名    
    
    iArray[1]=new Array(); 
		iArray[1][0]="责任项目名称";   
		iArray[1][1]="80px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
		iArray[1][4]="hmdutyitemdef";
		iArray[1][5]="1|10";
		iArray[1][6]="0|1";
		iArray[1][15]="DutyItemName";
		iArray[1][17]="1";
		iArray[1][19]="1" ;      //强制刷新数据源  
		
		iArray[2]=new Array(); 
		iArray[2][0]="责任当前状态";   
		iArray[2][1]="80px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;   
		iArray[2][5]="2|9";    //引用MulLine的对应第几列，'|'为分割符 
		iArray[2][6]="1|0";    //上面的列中放置下拉菜单中第几位值 
		iArray[2][4]="dutystate";
		iArray[2][17]="1"; 
		iArray[2][18]="160";     //下拉框的宽度 
		iArray[2][19]="1" ;      //强制刷新数据源
		
		
		
		iArray[3]=new Array(); 
		iArray[3][0]="具体责任描述";   
		iArray[3][1]="120px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;
		iArray[3][7]="inputDutyExolai";
		
		iArray[4]=new Array(); 
		iArray[4][0]="责任执行人";   
		iArray[4][1]="0px";   
		iArray[4][2]=20;        
		iArray[4][3]=3;
		
		iArray[5]=new Array(); 
		iArray[5][0]="联系人";   
		iArray[5][1]="60px";   
		iArray[5][2]=20;        
		iArray[5][3]=1;
		
		iArray[6]=new Array(); 
		iArray[6][0]="联系方式";   
		iArray[6][1]="80px";   
		iArray[6][2]=20;        
		iArray[6][3]=1;
		
		iArray[7]=new Array(); 
		iArray[7][0]="费用说明";   
		iArray[7][1]="120px";
		iArray[7][2]=20;        
		iArray[7][3]=1;
		iArray[7][7]="inputFeeExplai";
		
		iArray[8]=new Array(); 
		iArray[8][0]="ContraItemNo";   
		iArray[8][1]="0px";
		iArray[8][2]=2;        
		iArray[8][3]=3;   
		
		iArray[9]=new Array();      
		iArray[9][0]="责任当前状态代码";
		iArray[9][1]="0px";         
		iArray[9][2]=2;
		iArray[9][3]=3;             
		 
		iArray[10]=new Array();          
		iArray[10][0]="责任代码";
		iArray[10][1]="0px";             
		iArray[10][2]=2;                 
		iArray[10][3]=3;                 
		 
		 
    GroupContGrid = new MulLineEnter( "fm" , "GroupContGrid" ); 
    //这些属性必须在loadMulLine前

    GroupContGrid.mulLineCount = 0;
    GroupContGrid.displayTitle = 1;
    GroupContGrid.hiddenPlus = 0;
    GroupContGrid.hiddenSubtraction = 0;
    GroupContGrid.canSel = 0;
//    GroupContGrid.canChk = 1;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    GroupContGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDiseaseGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}*/  
 function initGroupContGrid() 
 {                            
   var iArray = new Array();                               
                                                           
   try 
   {                                                   
     iArray[0]=new Array();                                
     iArray[0][0]="序号";         		//列名               
     iArray[0][1]="30px";         		//列名               
     iArray[0][3]=0;         				//列名                 
     iArray[0][4]="";         //列名   
    
    iArray[1]=new Array(); 
		iArray[1][0]="合同责任项目类型";   
		iArray[1][1]="98px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
		iArray[1][4]="hmdeftype";
		iArray[1][5]="1|6";
		iArray[1][6]="1|0";
		//iArray[1][7]="testDutyItemType";
		iArray[1][17]="1";
		iArray[1][18]="200"; 
		iArray[1][19]="1" ;
     
		iArray[2]=new Array(); 
		iArray[2][0]="合同责任项目名称";   
		iArray[2][1]="128px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="hmdutyitemdef";
		iArray[2][5]="2|7";
		iArray[2][6]="0|1";
		//iArray[2][10]="Dutyitemtye";
		iArray[2][15]="DutyItemName";
		iArray[2][17]="6";
		iArray[2][18]="200"; 
		iArray[2][19]="1" ;      //强制刷新数据源DutyItemName  

		
		iArray[3]=new Array(); 
		iArray[3][0]="责任当前状态";   
		iArray[3][1]="68px";   
		iArray[3][2]=20;        
		iArray[3][3]=2;   
		iArray[3][5]="3|8";    //引用MulLine的对应第几列，'|'为分割符 
		iArray[3][6]="1|0";    //上面的列中放置下拉菜单中第几位值 
		iArray[3][4]="dutystate";
		iArray[3][17]="3"; 
		iArray[3][18]="200";     //下拉框的宽度 
		iArray[3][19]="1" ;      //强制刷新数据源
		
		iArray[4]=new Array(); 
		iArray[4][0]="责任联系人";   
		iArray[4][1]="88px";   
		iArray[4][2]=20;        
		//iArray[4][3]=2;   
		//iArray[4][5]="4|9";    //引用MulLine的对应第几列，'|'为分割符 
		//iArray[4][6]="0|1";    //上面的列中放置下拉菜单中第几位值 
		//iArray[4][4]="doctor";
		//iArray[4][15]="Doctname";
		//iArray[4][17]="4"; 			 //传参的列号
		//iArray[4][18]="200";     //下拉框的宽度 
		//iArray[4][19]="1" ;      //强制刷新数据源
		
		iArray[5]=new Array(); 
		iArray[5][0]="责任联系方式";   
		iArray[5][1]="108px";   
		iArray[5][2]=50;        
		iArray[5][3]=1;  
		
		
		iArray[6]=new Array(); 
		iArray[6][0]="hmconttype";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3; 
		
		iArray[7]=new Array(); 
		iArray[7][0]="DutyItemCode";   
		iArray[7][1]="0px";   
		iArray[7][2]=20;        
		iArray[7][3]=3;   
		
		iArray[8]=new Array(); 
		iArray[8][0]="dutystate";   
		iArray[8][1]="0px";   
		iArray[8][2]=20;        
		iArray[8][3]=3;  
		
		iArray[9]=new Array(); 
		iArray[9][0]="linkman";   
		iArray[9][1]="0px";   
		iArray[9][2]=20;        
		iArray[9][3]=3;  
		 
		
		iArray[10]=new Array(); 
		iArray[10][0]="ContraItemNo";   
		iArray[10][1]="0px";   
		iArray[10][2]=20;        
		iArray[10][3]=3; 
		     
   	iArray[11]=new Array(); 
		iArray[11][0]="有无关联责任";   
		iArray[11][1]="68px";   
		iArray[11][2]=50;        
		iArray[11][3]=0;  

    //Array[12]=new Array(); 
		//Array[12][0]="flag";   
		//Array[12][1]="0px";   
		//Array[12][2]=20;        
		//Array[12][3]=3; 
     
     
    GroupContGrid = new MulLineEnter( "fm" , "GroupContGrid" );                         
    //这些属性必须在loadMulLine前                                                                                              
                                                                                                               
    GroupContGrid.mulLineCount = 0;                                          
    GroupContGrid.displayTitle = 1;                                          
    GroupContGrid.hiddenPlus = 0;                                            
    GroupContGrid.hiddenSubtraction = 0;                                     
    GroupContGrid.canSel = 1;                                                
//    GroupContGrid.canChk = 1;                                              
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";                         
                                                                          ;  
    GroupContGrid.loadMulLine(iArray);                                       
    //这些操作必须在loadMulLine后面                                          
    //LDDiseaseGrid.setRowColData(1,1,"asdf");                               
  }                                                                          
  catch(ex) {                                                                
    alert(ex);                                                               
  }                                                                          
}                                                                        
function InGroupContInfo()
{
	    var arrResult = new Array();

				var mulSql = "select a.contrano,a.ContraName,a.HospitCode,"
				            +"(select  HospitName from LDHospital where LDHospital.HospitCode=a.HospitCode),"
				            +"a.IdiogrDate,a.ContraBeginDate,a.ContraEndDate,a.ContraState ,"
				            +"(select case when a.ContraState = '1' then '有效' else '无效' end from dual) "
										+" from lhgroupcont a where a.contrano ='"+ "<%=ContraNo%>" +"' ";
        //alert(mulSql);
        arrResult=easyExecSql(mulSql);
       // alert(arrResult);
		   // alert(arrResult[0][1]);
        fm.all('ContraNo').value				= arrResult[0][0];
        fm.all('HospitName').value			= arrResult[0][3];
        fm.all('HospitCode').value      = arrResult[0][2];
        fm.all('ContraName').value			= arrResult[0][1];
        fm.all('IdiogrDate').value			= arrResult[0][4];
        fm.all('ContraBeginDate').value	= arrResult[0][5];
        fm.all('ContraEndDate').value		= arrResult[0][6];
        fm.all('ContraState').value 		= arrResult[0][7];
        fm.all('ContraState_ch').value 		= arrResult[0][8];

   
   
     var mulSql2 = "select (select Codename from ldcode where b.ContraType=ldcode.code  and ldcode.Codetype='hmdeftype'),"
		            +"(select c.DutyItemName from LDContraItemDuty c where c.DutyItemCode = b.dutyItemCode),"                                  
								+"(select case when b.dutystate = '1' then '有效' else '无效' end from dual),"
								+" b.dutylinkman, "
								+"b.DutyContact,b.Contratype,b.DutyItemCode,b.dutystate,b.dutylinkman,b.ContraItemNo, "
								+" case when ( select distinct ContraItemNo from LHContraAssoSetting where ContraItemNo=b.ContraItemNo) is null then '无关联' else '有关联' end "
								+" from lhgroupcont a,lhcontitem b where a.contrano ='"+ "<%=ContraNo%>" +"' and b.contrano='"+ "<%=ContraNo%>" +"'";
				//alert(mulSql2);
				turnPage.pageLineNum = 200;  
				turnPage.queryModal(mulSql2, GroupContGrid);   
}  
      
</script>
  