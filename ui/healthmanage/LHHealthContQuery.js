//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var turnPage3 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  	if(HealthContGrid.getSelNo() < 1)
	{
  		alert("请先选择一张保单");
  		return false;
	}
	if(HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 3) == "个人保单" && ContCustomerGrid.getSelNo() < 1)
	{
  		alert("请先选择一位客户");
  		return false;
	}
	
  fm.all('Missionid').value = HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 4);
  fm.all('SubMissionid').value = HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 5);
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(inputButton,"false"); 
  
    if(fm.all('ContType').value == "1" || fm.all('ContType').value == "2")
    {
    	var PersonFee  = "";
    	if(fm.all('ContType').value == "1")
    	{
			var sql = " select sum(Prem) from lcpol where  contno = '"
					+HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 1)
					+"' and insuredno = '"+ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1, 1)+"' ";
			 PersonFee =  easyExecSql(sql);
		}
	    var count = easyExecSql(" select count(c.insuredno) from lcinsured c, ldperson d where c.insuredNo = d.customerno and c.contno = '"+HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1)+"' and c.grpcontno = '00000000000000000000' ");

	    if( count == "1" ||  count == "0" || count == "null" || count == "")
	    	inputCont(PersonFee);
	    else
	    	inputCont2(PersonFee);
	}
	else if(fm.all('ContType').value == "0")
	{//对按保费查询的跳转处理
		var sql = " select sum(Prem) from lcpol where  contno = '"
				+HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 1)
				+"' and insuredno = '"+ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1, 1)+"' ";
		var PersonFee =  easyExecSql(sql);
		inputCont3(PersonFee);
	}
  }
}
function toServPlanInfo()
{
	//alert(ContCustomerGrid.getSelNo());
	if(fm.all('ContType').value == "1"||fm.all('ContType').value == "0")
	{
		if(HealthContGrid.getSelNo() >= 1)
		{
	      if(ContCustomerGrid.getSelNo() >= 1)
	      {
	        if(fm.all('ContType').value == "1" || fm.all('ContType').value == "2")
          {
          	var PersonFee  = "";
          	if(fm.all('ContType').value == "1")
          	{
	      		   var sql = " select sum(Prem) from lcpol where  contno = '"
	      				       +HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 1)
	      				       +"' and insuredno = '"+ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1, 1)+"' ";
	      		   PersonFee =  easyExecSql(sql);
	      	  }
	      	  //下面的ldperson表多余2007.05.23
	          var count = easyExecSql(" select count(c.insuredno) from lcinsured c, ldperson d where c.insuredNo = d.customerno and c.contno = '"+HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1)+"' and c.grpcontno = '00000000000000000000' ");
	          //alert(count);
	          if( count == "1" ||  count == "0" || count == "null" || count == "")
	          	inputCont(PersonFee);
	          else
	          	inputCont2(PersonFee);
	        }
	        else if(fm.all('ContType').value == "0")
	        {//对按保费查询的跳转处理
	      	   var sql = " select sum(Prem) from lcpol where  contno = '"
	      		     	+HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 1)
	      			    +"' and insuredno = '"+ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1, 1)+"' ";
	          	var PersonFee =  easyExecSql(sql);
	      	  inputCont3(PersonFee);
	        }
        }
        else
	      { 		alert("请选择一条客户信息");
        		return false;
	      } 
	  }
    else
	  {   alert("请选择一条保单信息!");
    		return false;
	  }   
	}
	else
	{
		if(HealthContGrid.getSelNo() >= 1)
		{
		   if(fm.all('ContType').value == "1" || fm.all('ContType').value == "2")
       {
       	  var PersonFee  = "";
       	  if(fm.all('ContType').value == "1")
       	  {
	  	 	     var sql = " select sum(Prem) from lcpol where  contno = '"
	  	 	  		       +HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 1)
	  	 	  		       +"' and insuredno = '"+ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1, 1)+"' ";
	  	 	     PersonFee =  easyExecSql(sql);
	  	     }
	         var count = easyExecSql(" select count(c.insuredno) from lcinsured c, ldperson d where c.insuredNo = d.customerno and c.contno = '"+HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1)+"' and c.grpcontno = '00000000000000000000' ");
	         //alert(count);
	         if( count == "1" ||  count == "0" || count == "null" || count == "")
	         	 inputCont(PersonFee);
	         else
	         	 inputCont2(PersonFee);
	     }
	     else if(fm.all('ContType').value == "0")
	     {//对按保费查询的跳转处理
	  	    var sql = " select sum(Prem) from lcpol where  contno = '"
	  	 	     	+HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 1)
	  	 		    +"' and insuredno = '"+ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1, 1)+"' ";
	       	var PersonFee =  easyExecSql(sql);
	  	   inputCont3(PersonFee);
	     }
	  }
    else
	  {   alert("请选择一条保单信息!");
    		return false;
	  }  
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDDrug.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
        
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}       
    
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
	  var i = 0;
	  if( verifyInput2() == false ) return false;
	  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	  fm.fmtransact.value = "UPDATE||MAIN";
	  fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}  
         
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  showInfo=window.open("./LHHealthContQuery.html");
}        
   
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
	var i = 0;
	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  
	fm.fmtransact.value = "DELETE||MAIN";
	fm.submit(); //提交
	initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}          
 
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
 		arrResult = arrQueryResult; 
	}
}               

//主查询函数    
function easyQueryClick()
{

	initContCustomerGrid();
	//对保单及契约事件状态的判断
	var contState = "";		//保单状态代码
	var contstatename = ""; //保单状态名称
	if(fm.all('ContStateName').value == "1")
	{//契约事件信息未设置
		contState = " and (w.missionprop20 is null or w.missionprop20 = '' or  w.MissionProp1 in (select distinct contno from lhservplan where casestate = '1' and servplaytype <> '1H'))";
		grpcontState = " and ( missionprop20 is null or missionprop20 = '') "
		contstatename = "未进行服务信息设置";
	}
	else if(fm.all('ContStateName').value == "2")
	{//契约事件信息已设置
		
		contState = " and (w.missionprop20 = '1' or w.missionprop20 = '2') ";
		if(fm.all('CaseStateName').value == "2")
		{
			contState = contState + " and w.MissionProp1 in (select distinct contno from lhservplan where casestate = '2'  and servplaytype <> '1H') ";
			grpcontState = " and missionprop20 = '1' " ;
			contstatename = "未进行服务事件设置";     
		}
		else if(fm.all('CaseStateName').value == "3" )
		{
			contState = contState + " and w.MissionProp1 in (select distinct contno from lhservplan where casestate = '3'  and servplaytype <> '1H') ";
			grpcontState = " and missionprop20 = '2' ";		
			contstatename = "已进行服务事件设置";     	
		}	
	}
	
	if(fm.all('ContType').value == 1 || fm.all('ContType').value == 0)
	{fm.all('ContType_ForQ').value = "1";
		var sql = " select distinct w.MissionProp1, '"+contstatename+"', "
				 +" '个人保单', missionid, submissionid, (select distinct m.name from ldcom m where  m.comcode = w.MissionProp7), "
				 +"c.cvalidate,w.missionprop8 "
				 +" from lwmission w, lccont c , lcinsured r"
				 +" where w.activityid = '0000001190' and w.MissionProp1 = c.contno and c.conttype = '1' and c.contno = r.contno "
				 +getWherePart("c.conttype","ContType_ForQ")
				 +getWherePart("c.cvalidate","LHContStart",">") 
				 +getWherePart("c.cvalidate","LHContEnd","<") 
				 +getWherePart("r.Name","CustomerName") 
				 +getWherePart("r.insuredNo","CustomerNo") 
				 +getWherePart("c.ContNo","ContNo") 
				 +contState
				 +" and c.managecom like '"+manageCom+"%%'"
				 ; //alert(sql);fm.all('LHContStart').value = sql;
		turnPage3.queryModal(sql, HealthContGrid);
	}
	
	if(fm.all('ContType').value == 2)
	{
			var sql = " select distinct w.MissionProp1, '"+contstatename+"', "
				 +" '团体保单', missionid, submissionid, "
				 +" (select distinct m.name from ldcom m where  m.comcode = w.MissionProp4) ,c.cvalidate  "
				 +" from lwmission w, "
				 +"(select distinct  g.grpcontno as grpcontno ,g.cvalidate as  cvalidate from lcgrpcont g, lccont c "
				 +" where g.grpcontno = c.grpcontno  "
				 +getWherePart("c.appntName","CustomerName") 
				 +getWherePart("c.appntNo","CustomerNo") 
				 +getWherePart("g.grpcontno","ContNo")
				 +" ) as c "
				 +" where w.activityid = '0000002010' and w.MissionProp1 = c.grpcontno  "
				 +getWherePart("c.cvalidate","LHContStart",">") 
				 +getWherePart("c.cvalidate","LHContEnd","<")
				 +grpcontState
				 +" and w.MissionProp4 like '"+manageCom+"%%'"
				 ;
				 //alert(sql);
				 //fm.all('LHContStart').value = sql;
				 turnPage3.queryModal(sql, HealthContGrid);
	}
}

function getContList()
{
		var sql = " select MissionProp1, '未处理保单', '个人保单' from lwmission "
			 +" where activityid = '0000001190' and defaultoperator = '"+operator+"' "
			 ; 
	turnPage.queryModal(sql, OperatorContGrid);
}

//跳转到服务事件设置页面
function toServCase()
{
	
	if(HealthContGrid.getSelNo() < 1)
	{
		alert("请先选择一条保单信息");	
		return false;
	}
	if( easyExecSql(" select distinct grpcontno  from lccont where contno = '"+HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1)+"'") == "00000000000000000000")
	{//个单处理
		if(HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 3) == "个人保单" && ContCustomerGrid.getSelNo() < 1)
		{
	  		alert("请先选择一位客户");
	  		return false;
		}
		var tContNo = HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1);
		var tCustomerNo = ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1,1);
		var tPrtNo = getPrtNo(tContNo);
		//校验该保单改客户的LHServPlan的casestate是否为2
		var sql_casestate = easyExecSql(" select casestate from lhservplan where contno = '"
										 +tContNo +"' and customerno = '"+tCustomerNo +"' ");
										 
		if(sql_casestate.length == 1  && (sql_casestate == "2" || sql_casestate == "3"))
		{
			window.open("./LHContPlanSettingMain.jsp?ContNo="+tContNo+"&PrtNo="+tPrtNo+"&CustomerNo="+tCustomerNo,"个人服务计划事件设置",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
		}
		else
		{
			if(sql_casestate.length != 1)
			{
				alert("出现多条信息，契约数据出错，请向管理员反映");	
				return false;
			}
			if(sql_casestate != "2" && sql_casestate != "3")
			{
				alert("该保单服务契约信息未设置完成，不能设置服务事件信息");	
				return false;
			}
		}
	}
	else
	{//团单处理
		alert("团体保单请先进入信息设置页面选择客户组");	
		return false;
	}	
}


//打开录入界面函数
function inputCont(PersonFee)
{
	if(HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 3) == "个人保单")
	{
		var TempContNo = HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1)
		var TempPrtNo = getPrtNo(TempContNo);
		window.open("LHServPlanInputMain.jsp?ContNo="+TempContNo+"&PrtNo="+TempPrtNo+"&CustomerNo="+ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1,1)+"&PersonFee="+PersonFee,"",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
	}
	
	if(HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 3) == "团体保单")
	{ 
		var TempGrpContNo = HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1);
		var TempGrpPrtNo = getGrpPrtNo(TempGrpContNo);       
		window.open("LHGrpServPlanMain.jsp?ContNo="+HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1)+"&PrtNo="+TempGrpPrtNo,"",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
	}
}     

function inputCont2(PersonFee)
{
	if(HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1, 3) == "个人保单")
	{
		var TempContNo = HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1)
		var TempPrtNo = getPrtNo(TempContNo);
		window.open("LHServPlanInputMain.jsp?ContNo="+TempContNo+"&PrtNo="+TempPrtNo+"&CustomerNo="+ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1,1)+"&PersonFee="+PersonFee,"",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
	}
}       

//按保费查询后的跳转
function inputCont3(  PersonFee )
{
	var TempContNo = HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1)
	var TempPrtNo = getPrtNo(TempContNo);
	window.open("LHServPlanInputMain.jsp?ContNo="+TempContNo+"&PrtNo="+TempPrtNo+"&CustomerNo="+ContCustomerGrid.getRowColData(ContCustomerGrid.getSelNo()-1,1)+"&PersonFee="+PersonFee+"&flag=Fee","",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
}   
                               

function getPrtNo(TempContNo)
{
	var strSql = "select distinct PrtNo from lccont where ContNo='"+TempContNo+"'";
	prtNo = easyExecSql(strSql);
	return prtNo;
}
function getGrpPrtNo(TempGrpContNo)
{
	var strSql = "select distinct PrtNo from lcgrpcont where grpContNo='"+TempGrpContNo+"'";
	prtNo = easyExecSql(strSql);
	return prtNo;
}

//展现客户列表函数
function showCustomer()
{
	if(fm.all('ContType').value == 1 || fm.all('ContType').value == 0)
	{
		if(fm.all('CaseStateName').value == "")
		{
			alert("请先选择“契约事件状态”");	
			return false;
		}
	}
	var selno = HealthContGrid.getSelNo();
	var contno = HealthContGrid.getRowColData(HealthContGrid.getSelNo()-1,1);
//	var casestate = "";
//	var casestatename = "";
//	if(fm.all('ContStateName').value == "1")
//	{
//		casestate = " and (select distinct sp.customerno from lhservplan sp where sp.contno = '"+contno+"' and sp.customerno = c.insuredno and  (sp.casestate = '2' or sp.casestate = '3')) is null";
//		casestatename = "服务信息未设置";
//	}
//	else if(fm.all('ContStateName').value == "2")
//	{
//		if(fm.all('CaseStateName').value == "2")
//		{
//			casestate = " and (select distinct sp.customerno from lhservplan sp where sp.contno = '"+contno+"' and sp.customerno = c.insuredno and sp.casestate = '2') is not null";
//			casestatename = "服务事件未设置";
//		}
//		else if(fm.all('CaseStateName').value == "3" )
//		{
//			casestate = " and (select distinct sp.customerno from lhservplan sp where sp.contno = '"+contno+"' and sp.customerno = c.insuredno and sp.casestate = '3') is not null";
//			casestatename = "服务事件已启动";
//		}
//	}       
	var sql = " select c.insuredno, d.name, " 
           +" case when c.Sex='0' then '男' else '女' end, c.BirthDay," 
           +" (select m.riskname from lcpol po, lmriskapp m where  po.riskcode = m.riskcode and po.contno='"+contno+"'  "
           +" and po.insuredno = c.insuredno and m.risktype2 = '5' order by po.riskcode desc fetch first 1 rows only), " 
           +" (select po.mult from lcpol po, lmriskapp m where  po.riskcode = m.riskcode and po.contno='"+contno+"'  " 
           +" and po.insuredno = c.insuredno and m.risktype2 = '5' order by po.riskcode desc fetch first 1 rows only), "
           +" case when (select distinct casestate from lhservplan sp where sp.contno = c.contno and sp.customerno = c.insuredno AND SP.SERVPLAYTYPE <> '1H') = '2' then '未进行服务事件设置' " 
           +" when (select distinct casestate from lhservplan sp where sp.contno = c.contno and sp.customerno = c.insuredno AND SP.SERVPLAYTYPE <> '1H') = '3' then '已进行服务事件设置' " 
           +" else '未进行服务信息设置'   end " 
           +" from lcinsured c, ldperson d where c.insuredNo = d.customerno and c.contno = '"+contno+"' and c.grpcontno = '00000000000000000000' "
           +" and ( select distinct p.insuredno from lcpol p where p.contno = '"+contno+"'  and p.insuredno = c.insuredno and " 
           +" p.riskcode in (select distinct m.riskcode from lmriskapp m where  m.risktype2 = '5')) is not null "
	         ;
	         //alert(sql);
	         
	         
	turnPage2.queryModal(sql, ContCustomerGrid);
}

function afterCodeSelect(codeName,Field)
{
//	if(codeName == "lhconttype")
//	{//保单类型
//		if(Field.value == "1")
//		{
//			aa1.style.display="";
//			aa2.style.display="";
//		}
//		if(Field.value == "2")
//		{
//			fm.all('CaseStateName').value ="";
//			fm.all('CaseState').value ="";
//			aa1.style.display="none";
//			aa2.style.display="none";
//		}
//	}
	
	if(codeName=="lhmanagecont")
	{//保单状态
		if(Field.value=="1")
		{
			if(fm.all('ContType').value == "1" || fm.all('ContType').value == "0" )
			{
				fm.all('CaseStateName').value = "1";
				fm.all('CaseState').value = "未进行服务信息设置";
			}
			if(fm.all('ContType').value == "2")
			{
				fm.all('CaseStateName').value = "";
				fm.all('CaseState').value = "";
			}
		}
		else if(Field.value=="2" )
		{
			if(fm.all('ContType').value == "1" || fm.all('ContType').value == "0")
			{
				fm.all('CaseStateName').value = "2";
				fm.all('CaseState').value = "未进行服务事件设置";
			}
			if(fm.all('ContType').value == "2")
			{
				fm.all('CaseStateName').value = "";
				fm.all('CaseState').value = "";
			}
		}
	}
	
	if(codeName=="lhcasestate")
	{//契约事件状态
//		if( fm.all('ContStateName').value != "1" && fm.all('ContStateName').value != "2")
//		{
//			alert("请先选择正确保单维护状态信息");
//			fm.all('CaseStateName').value = "";
//			fm.all('CaseState').value = "";
//			return false;	
//		}	
//		if( fm.all('ContStateName').value == "1" )
//		{
//			fm.all('CaseStateName').value = "1";
//			fm.all('CaseState').value = "服务信息未设置";
//		}

		if( Field.value == "1" )       
        {                                                
        	fm.all('ContStateName').value = "1";         
//        	fm.all('CaseState').value = "服务信息未设置";
        }     
                                                   
//		if( fm.all('ContStateName').value == "2" )
//		{
//			if(Field.value == "1")
//			{
//				alert("保单维护状态为 ‘契约事件已设置’ 时，契约事件状态不能为 ‘服务信息未设置’ ");
//				fm.all('CaseStateName').value = "";
//				fm.all('CaseState').value = "";	
//			}
//		}
		
		if( Field.value == "2" || Field.value == "3" )       
        {                                                
        	fm.all('ContStateName').value = "2";         
//        	fm.all('CaseState').value = "服务信息未设置";
        } 		
	}
}
function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.html");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      fm.CustomerName.value = arrResult[0][1];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}

function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}

function queryCustomerNo2()
{	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
	var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}