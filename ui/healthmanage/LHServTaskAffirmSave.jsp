<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServTaskAffirmSave.jsp
//程序功能：
//创建日期：2006-08-23 10:55:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHServTaskAffirmUI tLHServTaskAffirmUI   = new LHServTaskAffirmUI();
  
  LHCaseTaskRelaSet tLHCaseTaskRelaSet = new LHCaseTaskRelaSet();		//服务事件任务关联表
  
    String tServTaskNo[] = request.getParameterValues("LHSettingSlipQueryGrid7");           //任务编号 
    String tServTaskAffirm[] = request.getParameterValues("LHSettingSlipQueryGrid9");					//任务确认    
    String tChk[] = request.getParameterValues("InpLHSettingSlipQueryGridChk"); //参数格式=” Inp+MulLine对象名+Chk”     	 
     
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");

    int LHCaseTaskRelaCount = 0;
		if(tServTaskNo != null)
		{	
			   LHCaseTaskRelaCount = tServTaskNo.length;
		}	
		System.out.println(" LHCaseTaskRelaCount is : "+LHCaseTaskRelaCount);
		
		for(int i = 0; i < LHCaseTaskRelaCount; i++)
		{
			
				LHCaseTaskRelaSchema tLHCaseTaskRelachema = new LHCaseTaskRelaSchema();
			
				 
				  
				  tLHCaseTaskRelachema.setServTaskNo(tServTaskNo[i]);	 //事件号
				  tLHCaseTaskRelachema.setServTaskAffirm(tServTaskAffirm[i]);	//事件状态
				                                              
            if(tChk[i].equals("1"))  
            {         
                 System.out.println("该行被选中 "+tServTaskNo[i]);
                 tLHCaseTaskRelaSet.add(tLHCaseTaskRelachema);         
                 
             }
            if(tChk[i].equals("0"))  
            {     
                  System.out.println("该行未被选中 "+tServTaskNo[i]);
            } 
        
                 
			                        
		}
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHCaseTaskRelaSet);
  	tVData.add(tG);
  	System.out.println("- DDDDDDDDDD "+transact);
    tLHServTaskAffirmUI.submitData(tVData,transact);
    System.out.println("- SSSSSSSSSSSS "+transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHServTaskAffirmUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");   
</script>
</html>