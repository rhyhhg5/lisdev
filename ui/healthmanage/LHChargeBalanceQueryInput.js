/** 
 * 程序名称：LHChargeBalanceQueryInput.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2006-03-09 15:27:48
 * 创建人  ：郭丽颖
 * 更新记录：
 * 更新人 : 
 * 更新日期 : 
 * 更新原因/内容: 插入新的字段
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
var no;
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句		
//alert(no);	
	if(no=="1")
	{      			 
		var LEN = manageCom.length;
	  if(LEN == 8){LEN = 4; }   
		var strSql = "select distinct a.contrano,b.ContraName,"
							+"(select HospitName from LDHospital where LDHospital.HospitCode=b.HospitCode),"
							+"(select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=c.ContraType),"
		          +"(select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode)"
		          //+"a.BalanceDate,a.BalanceRemindDate,"
		         // +" ( case when a.BalanceType = '1' then '支票' else '转帐' end),a.Accounts"
		          +" from LHChargeBalance a ,LHGroupCont b, LHContItem c"
					    +" where  a.contrano=b.contrano and b.contrano=c.contrano and "
					    +"exists(select 'x' from lhgroupcont b where b.contrano=a.contrano) and "
					    +" a.ContraItemNo=c.ContraItemNo "
					    + getWherePart("a.ContraNo", "ContraNo", "like")
					    + getWherePart("a.BalanceDate", "BalanceDate", "like")
					    + getWherePart("a.BalanceRemindDate", "BalanceRemindDate", "like")
					    + getWherePart("a.BalanceType", "BalanceTypeCode", "like")
					    +" and a.managecom like '"+manageCom.substring(0,LEN)+"%%' order by a.ContraNo"
		 // alert(strSql);
		 // alert(easyExecSql(strSql));
		  turnPage.queryModal(strSql, LHChargeBalanceGrid); 
		  //BalanceRemindDate BalanceTypeCode
	}
	if(no=="2")
	{
		var LEN = manageCom.length;
	  if(LEN == 8){LEN = 4; }   
		var strSql = "select distinct a.contrano,b.ContraName,"
							+"(select DoctName from  LDDoctor where LDDoctor.Doctno=b.Doctno),"
							+"(select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=c.ContraType),"
		          +"(select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode),"
		         // +"a.BalanceDate,a.BalanceRemindDate,"
		          //+" ( case when a.BalanceType = '1' then '支票' else '转帐' end),a.Accounts,"   
		          +"(select Doctno from LDDoctor where LDDoctor.Doctno=b.Doctno)"
		          +" from LHChargeBalance a ,lhunitcontra b, LHContItem c"
					    +" where  a.contrano=b.contrano and b.contrano=c.contrano and "
					    +" exists(select 'x' from lhunitcontra b where b.contrano=a.contrano) and "
					    +"a.ContraItemNo=c.ContraItemNo "
					    + getWherePart("a.ContraNo", "ContraNo", "like")
					    + getWherePart("a.BalanceDate", "BalanceDate", "like")
					    + getWherePart("a.BalanceRemindDate", "BalanceRemindDate", "like")
					    + getWherePart("a.BalanceType", "BalanceTypeCode", "like")
					    +" and a.managecom like '"+manageCom.substring(0,LEN)+"%%' order by ContraNo"
	 //  alert(easyExecSql(strSql));
	   
		  turnPage.queryModal(strSql, LHChargeBalanceGrid);
		
  }
}


function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
  var arrReturn = new Array();
	var tSel = LHChargeBalanceGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				arrReturn = getQueryResult();
				//alert(arrReturn);
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHChargeBalanceGrid.getSelNo();
	//alert("111" + tRow);
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = LHChargeBalanceGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
