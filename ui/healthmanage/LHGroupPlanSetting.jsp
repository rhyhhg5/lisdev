<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2006-03-09 11:34:16
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	
  <SCRIPT src="LHGroupPlanSetting.js"></SCRIPT>
  <%@include file="LHGroupPlanSettingInit.jsp"%>
</head>
<SCRIPT language=javascript1.2>
function showsubmenu()
{
		var ContNo = "<%=request.getParameter("ContNo")%>";
    whichEl = eval('submenu' );
    if (whichEl.style.display == 'none')
    {
         //submenu1.style.display='';
    	   eval("submenu1.style.display='';");
         submenu.style.display='';
    }
    else
    {
    	  submenu.style.display='none';
        eval("submenu1.style.display='';");
    }
    var strSql = "select PrtNo from lccont where ContNo='"+ContNo+"'";
    
        prtNo = easyExecSql(strSql);
    var url="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo="+prtNo+"&SubType=TB1002&BussType=TB&BussNoType=12"
    document.getElementById("iframeG").src=url;
   // alert(document.getElementById("iframeG").src);
}
</SCRIPT>
<body  onload="initForm();passQuery();" >
  <form action="" method=post name=fm target="fraSubmit">
   <span id="operateButton" >
	<table  class=common align=center>
		<tr align=right>
			<td>
		</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="return submitForm();">
			</td>
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
			</td>	
		</tr>
	</table>
</span>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
			<td class=button width="100%" align=left>
				<INPUT class=cssButton VALUE="查看保单扫描件"  TYPE=button onclick=" showsubmenu();">
			</td>
		</tr>	
		<tr>
        <td style='display:none' id='submenu'>
			<div>
				<table class=common cellpadding=0 cellspacing=0 >   
					<tr class=common>
						<td class=input>
	  		       			<iframe scrolling="auto" width="1000" height="300" id=iframeG src=''>
	  		        		</iframe>
						</td>
					</tr>
	  	       </table>
			</div>
			  </td>
		 </tr>
		</table>
		<div style="display:''" id='submenu1'>  
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 团体服务计划表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHGrpServPlan1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
			    <TD  class= title>
			      团体客户号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpCustomerNo verify="团体客户号|NOTNULL&len<=20" readonly>
			    </TD>
			    <TD  class= title>
			      团体客户名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpName verify="团体客户名称|NOTNULL&len<=300" readonly>
			    </TD>
				<TD  class= title>
			      团体保单号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpContNo verify="团体保单号|NOTNULL&len<=20" readonly>
			    </TD>
		  	</TR>
		  	<TR  class= common>
		  		<TD  class= title>
			      保单所属机构标识
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ComID verify="保单所属机构标识|NOTNULL&len<=10" readonly>
			    </TD>
			   <TD  class= title>
			      团体服务计划名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=SerPlanName verify="团体服务计划名称|NOTNULL&len<=10" readonly>
			    </TD>
			</TR>
			<TR  class= common>
			    <TD  class= title>
			      服务计划起始时间
			    </TD>
			    <TD  class= input>
			      <Input class= 'CoolDatePicker' name=StartDate verify="服务计划起始时间|DATE&NOTNULL">
			    </TD>
			    <TD  class= title>
			      服务计划终止时间
			    </TD>
			    <TD  class= input>
			      <Input class= 'CoolDatePicker' name=EndDate verify="服务计划终止时间|DATE&NOTNULL">
			    </TD>
			    <TD  class= title>
			      健管保费（元）
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServPrem  readonly>
			    </TD>
			</TR>
		</table>
    </Div>
    
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHGrpServPlanGrid);">
    		</td>
    		 <td class= titleImg>
        		 团体服务计划信息列表
       		 </td>      		 		 
    	</tr>
    </table>
    
	<Div id="divLHGrpServPlanGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHGrpServPlanGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
	
	    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServItemGrid);">
    		</td>
    		 <td class= titleImg>
        		 服务项目信息列表
       		 </td>      		 		 
    	</tr>
    </table>
    
	<Div id="divLHServItemGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServItemGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>

  <table>
    	<tr>
       		 <td>
       		 	<input type= hidden class=cssButton name=PriceSetting value="定价方式设置" OnClick="ToPriceSetting();">
       		 </td>   		 
    	</tr>
    </table>
 <Div id="divLHServEventInfo" style="display:''">
 	<hr>
 	    <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    	 	</td>
    	 <td class= titleImg>
        	 服务事件信息
       	</td>   		 
       
    	</tr>
    </table>
  <table  class=common  align='center'>
  <TR  class= common>
    <TD  class= title>
      是否归为相同事件
    </TD>
    <TD  class= input>
      <Input  type=hidden name=ContraState>
      <Input class= 'code' name=ContraState_ch style="width:152px" verify="是否归为相同事件|len<=4" CodeData= "0|^1|是^2|否" ondblClick= "showCodeListEx('',[this,ContraState],[1,0],null,null,null,'1',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('',[this,ContraState],[0,1],null,fm.ContraState_ch.value,'ContraState_ch');"   onkeydown="return showCodeListKey('',[this,ContraState],[0,1],null,fm.ContraState_ch.value,'ContraState_ch'); codeClear(ContraState_ch,ContraState);">	
    </TD>
    <TD  class= title>
    	服务事件编号
    </TD>
	  <TD  class= input>
    	<Input class= 'common' name=CustomerNo>
    </TD> 
    <TD  class= title>
		服务事件名称
	</TD>
	<TD  class= input>
    	<Input class= 'common' name=CustomerNo>
    </TD> 
  <TD  class= title>
	</TD>
	<TD  class= input>
    	
  </TD> 
   <!--<TD  class= title>
		服务事件类型
    </TD>
		<TD  class= input>
		 <Input class= 'codename' style="width:40px" value=1 name=ContType><Input class= 'codeno' style="width:120px"  name=ContTypeName verify="服务事件类型|NOTNULL&len<=20" CodeData= "0|^1|个人服务事件^2|集体服务事件"  value=个人服务事件   ondblclick="showCodeListEx('ContType',[this,ContType],[1,0],null,null,null,'1',160);" codeClear(ContTypeName,ContType);">
	  </TD>-->
  </TR>
 </table>
 <table>
    	<tr>
       		 <td>
       		 	<input type= button class=cssButton name=ServTrace value="新增服务事件" OnClick="toServTrace();">
       		 </td>  
       		 <td>
       		 	<input type= button class=cssButton name=ServEevetQuery value="服务事件查询" OnClick="">
       		 </td>  		 
    	</tr>
    </table>
    <hr>
</div>
    
     <table>
    	<tr>
       		 <td>
       		 	<input type= button class=cssButton name=EventSettSuc value="服务事件设置完成" OnClick="">
       		 </td>  
       		 <td>
       		 	<input type= button class=cssButton  style="width:75px"  name=saveButton value="保 存" OnClick="">
       		 </td>  		  		 
    	</tr>
    </table>

    
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
	<Input type=hidden name=ManageCom >
	<Input type=hidden name=Operator >
	<Input type=hidden name=MakeDate >
	<Input type=hidden name=MakeTime >
	<Input type=hidden name=ModifyDate >
	<Input type=hidden name=ModifyTime >    
	<Input type=hidden name=GrpServPlanNo ><!--团体服务计划号码-->

	<Input type=hidden name=ServPlanCode >
	<Input type=hidden name=ServPlanName >
	<Input type=hidden name=ServPlanLevel >
	<Input type=hidden name=ServPlayType >
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
<script>
		function passQuery()
		{ 	
			//进入此函数
			var ContNo = "<%=request.getParameter("ContNo")%>";
			//alert(ContNo);
			if(ContNo!='null' || ContNo > 0 ) 
			{
				var strSqlInit = "select appntno,grpname,grpcontno,managecom from lcgrpcont "
				+"where grpcontno = '"+ContNo+"'"
				;
                var arrResultInit = easyExecSql(strSqlInit);
                //alert(strSqlInit);
                if(arrResultInit!=null&&arrResultInit!="null"&&arrResultInit!=null)
                {
                	fm.all('GrpCustomerNo').value = arrResultInit[0][0];
                	fm.all('GrpName').value = arrResultInit[0][1];
                  fm.all('GrpContNo').value = arrResultInit[0][2];
                  fm.all('ComID').value = arrResultInit[0][3].substring (0,4);
                }
                 var sql_i2 = " select distinct startdate, enddate from LHGrpServPlan where GrpContNo = '"+fm.all('GrpContNo').value+"'";
                 var arr_i2 = easyExecSql(sql_i2);
                 //alert(arr_i2);
                if(arr_i2 != "" && arr_i2 != "null" && arr_i2 != null)
                {
                	
	                fm.all('StartDate').value = arr_i2[0][0];
	                fm.all('EndDate').value = arr_i2[0][1];
	                fm.all('ServPrem').value= easyExecSql("select sum(ServPrem) from LHGrpServPlan where GrpContNo = '"+fm.all('GrpContNo').value+"' "); 
					         
					         
					        
				        	var sql_g = " select  ServPlanLevel,ServPrem,GrpServPlanNo,MakeDate,MakeTime"    
	 							            +" from LHGrpServPlan "                                             
	      						        +" where GrpContNo = '"+fm.all('GrpContNo').value+"'"                      
	      						        +" order by ServPlanLevel"                                                  
	      						        ;  
	      			     turnPage.queryModal(sql_g,LHServItemGrid); 
	      		    }

		  }
			else 
			{
				alert("没有此保单信息，请确认!");      
				return false;
			}
		    
	  }
   </script>
</html>
