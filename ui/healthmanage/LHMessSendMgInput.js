var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;


//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
    var i = 0;
    fm.fmtransact.value="INSERT||MAIN";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    //fm.action="./LHMessSendMgSave.jsp";
    fm.action="./LHMessSendMgSave.jsp";
    fm.target="fraSubmit";
    fm.submit(); //提交

}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    showInfo.close();
    if (FlagStr == "Fail" )
    {             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
        initForm();
    }   
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCommTrans.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}                                          
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */              
function updateClick()
{
	var rowNum=LHMessSendMgGrid. mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHMessSendMgGrid.getChkNo(row); 
		if(tChk == true)
		{
			aa[xx] = row;
		}
	}
	if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
	{
		  alert("请选择要修改的信息记录!");
		  return false;
	}	
	if(aa.length>="1")
	{
    	if (confirm("您确实想修改该记录吗?"))
    	{
			var i = 0;
    	    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    	    fm.fmtransact.value = "UPDATE||MAIN";
    	    fm.action="./LHMessSendMgSave.jsp";
    	    fm.target="fraSubmit";
    	    fm.submit(); //提交
    	    initLHMessSendMgGrid();
    	    displayConetent();
		}
    	else
    	{
    	  alert("您取消了修改操作！");
    	} 
	}
}

function dBespeakServiceTime()
{ 
	var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.ServiceTime.value = h+":"+m;	
}
function showOne()
{
	var rowNum=LHMessSendMgGrid. mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHMessSendMgGrid.getChkNo(row); 
		if(tChk == true)
		{
			aa[xx++] = row;	
		}  
	}
	if(aa.length=="1")
	{
		 //alert("LHMessSendMgGrid");
		 var HmMessCode =LHMessSendMgGrid.getRowColData(aa,9); 
		 fm.HmMessCode.value=HmMessCode;
		 if(fm.HmMessCode.value!=""&&fm.HmMessCode.value!="null"&&fm.HmMessCode.value!=null)
		 {
		    fm.all('HmMessName').value=easyExecSql("select  a.HmMessName from LHMessManage  a where a.HmMessCode='"+ fm.all('HmMessCode').value +"'");
		 }
		 var ReMarkExp =LHMessSendMgGrid.getRowColData(aa,11); 
		 fm.ReMarkExp.value=ReMarkExp;
	}
	if(aa.length>"1")
	{
		 fm.HmMessCode.value="";
		 fm.all('HmMessName').value="";
		 fm.ReMarkExp.value="";
	}
}       
        
function printClick()
{
	fm.all('manageComPrt').value = manageCom;
	fm.action="./LHMessSendPrt.jsp";
	fm.target="f1print";
	fm.submit();
} 

function getMobile()
{
	if(fm.all('save').disabled == false)
	{
		alert("请先点击‘添加通讯信息’按钮，进行保存");
		return false;
	}				
	window.open("./LOMsgInfoInputMain.jsp?f=1&s="+fm.all('ServTaskNo').value,"短信发送",'width=1024,height=768,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}

function getAppMobile()
{
	if(fm.all('save').disabled == false)
	{
		alert("请先点击‘添加通讯信息’按钮，进行保存");
		return false;
	}				
	window.open("./LOMsgInfoInputMain.jsp?f=2&s="+fm.all('ServTaskNo').value,"短信发送",'width=1024,height=768,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == "hmexecstate")	
	{
		for(var row=0; row < LHMessSendMgGrid.mulLineCount; row++)
		{
			var tChk =LHMessSendMgGrid.getChkNo(row); 
			if(tChk == true)
			{
				LHMessSendMgGrid.setRowColData(row,19,fm.all('ExecState_ch').value);	
				LHMessSendMgGrid.setRowColData(row,20,fm.all('ExecState').value);
	  		}  
		}	
	}
}