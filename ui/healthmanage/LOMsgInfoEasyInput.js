//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var isSendOk;
 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
//	
//	if(fm.all('CustomerNo').value == "")
//	{
//		alert("请从数据库提取客户手机号码");
//		return false;	
//	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //alert(fm.fmtransact.value);return;
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
  	showInfo.close();
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
  	showInfo.close();
  	if(fm.all('MsgSend').value == "1")
  	{
    	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=短信发送成功! " + content ;
    }
    else
    {
    	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    }	
    	  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    	if(fm.fmtransact.value == "DELETE||MAIN")
    	{
    		resetForm();
    	}
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
    showDiv(inputButton,"false"); 
}
          
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
 	else 
 	{
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
        
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  showInfo=window.open("./LOMsgInfoQuery.html","信息查询",'width=1024,height=748,top=0,left=0,resizable=yes');
}           

function deleteClick()
{
	//下面增加相应的删除代码
	if (confirm("您确实想删除该记录吗?"))
	{
		var i = 0;
	    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	    
	    //showSubmitFrame(mDebug);
	    fm.fmtransact.value = "DELETE||MAIN";
	    fm.submit(); //提交
	    initForm();
	}
	else
	{
	    alert("您取消了删除操作！");
	}
}     
      
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{    
	var arrMsgInfo = new Array();
	var arrMsgReceive = new Array();
	
	if( arrQueryResult != null )
	{
		arrMsgInfo = arrQueryResult;  
//		getReceInfo(arrMsgInfo[0][7]);//将客户号，保单号拼成一个字符串保存到隐藏控件
		
		var sql = " select * from LOMsgInfo where MsgNo = '"+arrMsgInfo[0][7]+"' ";
		arrMsgReceive = easyExecSql(sql);
		alert(arrMsgReceive);
		fm.all('MsgNo').value = arrMsgReceive[0][0];
		fm.all('MsgType').value = arrMsgReceive[0][2];
		fm.all('from').value = arrMsgReceive[0][3];
		fm.all('content').value = arrMsgReceive[0][6];
		fm.all('MsgSend').value = arrMsgReceive[0][10];fm.all('HaveBeenSent').value = arrMsgReceive[0][10];
		fm.all('MakeDate').value = arrMsgReceive[0][15];
		fm.all('MakeTime').value = arrMsgReceive[0][16];
	}
}

//从短信组设置得到客户信息
function afterQuery0( arrQueryResult )    
{ 
	var len = arrQueryResult[0].length;
	var iArray = new Array();
	iArray[0] = "";
	iArray[1] = "";
	iArray[2] = "";
	iArray[3] = "";
	
	for(var j = 0; j < 4; j++)
	{
		for(var i = 0; i < len; i++)
		{
			if(len-1 == i)
			{
				iArray[j] = iArray[j] + arrQueryResult[j][i];
			}
			else
			{ 
				iArray[j] = iArray[j] + arrQueryResult[j][i]+",";
			}
		} 
	}
	fm.all('mobile').value = iArray[0];
	fm.all('CustomerNo').value = iArray[1];
	fm.all('GrpContNo').value = iArray[2];
	fm.all('ContNo').value = iArray[3];
}
//自动判断称谓函数
function getTitle()
{
		var temp = "";
		var temp_sql = " select distinct a.name, a.sex from ldperson a, "
					  +"( select distinct customerno from lcaddress where mobile = '"
					  +fm.all('mobile').value+"' ) b where a.customerno = b.customerno "
					  ;
		var temp_arr = easyExecSql(temp_sql);
		if(temp_arr == null){alert("客户信息有误：无此客户！");return false;}
		if(temp_arr.length > 2){alert("客户信息有误：有多条！");return false;}
		var temp_sex = temp_arr[0][1]=="0"?"先生您好！":"女士您好！";
		temp = temp_arr[0][0]+temp_sex;
		return temp;
}


function SendAndSave()
{
	if(fm.all('mobile').value == "" )
	{alert("请输入正确手机号码");return false};
	
//	var temp = "";
//	if(fm.typeRadio[0].checked)
//	{//自动带出称谓
//	
//		if(fm.all('mobile').value.length == 11)				//自动判断称谓
//		{ 
//			temp =  getTitle();
//		}
//		else temp = "尊敬的客户您好！";
//	}

	if(fm.all('MsgSend').value == "")
	{//是新建的短信,新建并发送
		fm.all('MsgSend').value = '1';//状态置为已发送
		if(verifyInput2()==false) return false;
		fm.fmtransact.value = "INSERT||MAIN";//HaveBeenSent=""
//		alert(fm.all('content').value);
//		fm.all('content').value="来自:"+fm.all('from').value+"\n"+temp+fm.all('content').value;
	}
	else
	{//是已保存的短信
		if(fm.all('MsgSend').value == "1")
		{//如果此已保存的短信是发送过的，不让再次发送
			alert("此条短信已经被发送过，请新建短信发送");
			return false;
//			if(verifyInput2()==false) return false;
//			fm.fmtransact.value = "INSERT||MAIN";//HaveBeenSent=1
//			fm.all('content').value="来自:"+fm.all('from').value+"\n"+temp+fm.all('content').value;      
		}
		if(fm.all('MsgSend').value == "0")
		{//如果此已保存的短信没被发送过，修改并发送
			if(verifyInput2()==false) return false;
			fm.all('MsgSend').value = '1';
			fm.fmtransact.value = "UPDATE||MAIN";//HaveBeenSent=0  
//			fm.all('content').value="来自:"+fm.all('from').value+"\n"+temp+fm.all('content').value;
		}
	}
	submitForm();
}

function SaveNotSend()
{
	if(fm.all('MsgSend').value == "")
	{//是新建的短信,新建不发送
		fm.fmtransact.value = "INSERT||MAIN";//HaveBeenSent=""
		fm.all('MsgSend').value = '0';
	}
	else
	{//已保存过的
		if(fm.all('MsgSend').value == "0")
		{fm.fmtransact.value = "UPDATE||MAIN";}//HaveBeenSent=0
		if(fm.all('MsgSend').value == "1")
		{alert("此条短信已被发送过，无修改必要");return;}
	}
	submitForm();
	
}

function SendMsg( ttext,tto,MsgFlag )
{
	var tsname= fm.all('from').value;      	
	var tlogin=fm.all('title').value;      
	var tmobile=tto;
   				         
// dt为电话号码，feecode 为计费， feetype计费类型 ，svid 服务代码，msg 为内容 spno 用户长号码 reserve 保留字段 type 短信类型 
	var tUrl="http://211.100.6.183/TSmsPortal/smssend?dt="+tmobile+"&feecode=000000&feetype=01&svid=picch&spno=5467&msg="+ ttext +"&reserve=0000000000000000&type=0";
	var win=window.open(tUrl,"fraSubmit",'height=110,width=220,top=260,left=400,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');
}
 
 
function openTask()
{
	showInfo=window.open("./LOMsgInfoTask.html","信息查询",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
}

function getReceInfo(Msgno)
{
	var arr = new Array();
	arr[0] = "";
	arr[1] = "";
	arr[2] = "";
	arr[3] = "";
	arr[4] = "";
	arr[5] = "";
	
	var sql = " select a.seqno, a.customerno, b.name, a.mobileno, a.GrpContNo, a.ContNo "
			 +" from lomsgreceive a, ldperson b where a.msgno = '"+Msgno+"' and a.customerno = b.customerno "
			 ;
	var resultArr = easyExecSql(sql);

	for(var i = 0; i < 6; i++)
	{
		for(var j = 0; j < resultArr.length; j++ )
		{
			arr[i] = arr[i] + resultArr[j][i]+",";
		}
	}
	
	fm.all('MSGSEQ').value = arr[0].substring(0,arr[0].length-1);
	fm.all('CustomerNo').value = arr[1].substring(0,arr[1].length-1);
	fm.all('CustomerName').value = arr[2].substring(0,arr[2].length-1);
	fm.all('mobile').value = arr[3].substring(0,arr[3].length-1);
	fm.all('GrpContNo').value = arr[4].substring(0,arr[4].length-1);
	fm.all('ContNo').value = arr[5].substring(0,arr[5].length-1);
}   


function getCustomerGrp()
{
	var	showInfo2=window.open("LOMsgReceiveGrpInput.html","客户组选择",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');
}

//个人客户手机号码提取控件
function showGetMobile()
{
	if(iGetMoblie.style.display=="none")
	{
		iGetMoblie.style.display="";
	}
	else
	{
		iGetMoblie.style.display="none";
		fm.all('mobile').value = "";
		fm.all('CustomerNo_Mobile').value = "";
	}
}

//提取个人客户手机号码
function getMobileNo()
{
	var sql =  " select distinct ca.mobile, ca.customerno from lcaddress ca, "
			  +" (select char(max(integer(a.addressno))) as max, a.customerno"
			  +" from lcaddress a where a.customerno  = '"+fm.all('CustomerNo_Mobile').value+"' group by a.customerno ) xx "
			  +" where  ca.addressno = xx.max and ca.customerno = xx.customerno  "
			  ;
	var temp_mobile = easyExecSql(sql); 
	if(temp_mobile==null)
	{
		fm.all('mobile').value = "";
	}
	else
	{
		fm.all('mobile').value = temp_mobile[0][0];
		fm.all('CustomerNo').value = temp_mobile[0][1];
		fm.all('GrpContNo').value = fm.all('GrpContNo_Mobile').value;
		fm.all('ContNo').value = fm.all('ContNo_Mobile').value;
	}
}


//套用模版
function useTemplate()
{
	var showInfoTemp=window.open("./LHMsgTemplateQuery.html");
}

function afterQueryPub(arrResult)
{
	fm.all('content').value = arrResult[0][2];
}

//个人发送,输入客户号后回车显示客户姓名
function getCustomerName()
{
	var temp_customername = easyExecSql("select name from ldperson where customerno = '"+fm.all('CustomerNo_Mobile').value+"'");	
	
	if (temp_customername == "" || temp_customername == null)
	{
		alert("无此客户，请确认");
		return false;
	}
	else
	{
		fm.all('CustomerName_Mobile').value = temp_customername;
	}
}

//个人发送，确定客户后，选择保单后带出手机号码
function afterCodeSelect(codeName,Field)
{
	if(codeName=="lhmsggetcontinfo")
	{
		getMobileNo();
	}
}

function getMobile()
{
	if(flag == "1")
	{
		var sqlMoblie =  "select distinct a.mobile from LCAddress a where a.customerno in " 
		    + " (select distinct s.customerno from lhservitem s, "
		    + " (select  distinct a.servitemno from lhtaskcustomerrela a  where servtaskno = '"+ServTaskNo+"')" 
            + " u where s.servitemno = u.servitemno) and db2inst1.to_number(addressno) = (select max(db2inst1.to_number(addressno))"
            + " from LCAddress where CustomerNo = a.CustomerNo) and a.mobile is not null and a.mobile like '1%' and length(db2inst1.trim(a.mobile))=11 order by a.mobile with ur"
			;  
			fm.all('mobile').value = getEasyQuery(sqlMoblie);
	}
	if(flag == "2")	
	{
		var sqlMoblie =  "select distinct a.mobile from LCAddress a where a.customerno in "
            + "(select distinct v.appntno from lhservitem s, (select  distinct a.servitemno from lhtaskcustomerrela a "  
            +" where servtaskno = '"+ServTaskNo+"') u ,LCCONT v where s.servitemno = u.servitemno AND s.contno = v.contno)" 
            + " and db2inst1.to_number(addressno) ="
            + " (select max(db2inst1.to_number(addressno))from LCAddress where CustomerNo = a.CustomerNo) "
            + " and a.mobile like '1%' and length(db2inst1.trim(a.mobile))=11 and a.mobile is not null order by a.mobile with ur "
			;           
			fm.all('mobile').value = getEasyQuery(sqlMoblie);
	}
	
}