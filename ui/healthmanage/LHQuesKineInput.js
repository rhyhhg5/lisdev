//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var Source_Js ; //6-健管人员内部录入,从一般服务任务管理进入；1-CRM传入，客户为核心系统客户；0-CRM传入，客户为准客户

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
		if(fm.all('SubmitDate').value == "")
		{
			alert("请填‘写填表日期’");
			fm.all('SubmitDate').focus();
			fm.all('SubmitDate').className="warn";
			return false;
		}
	   //alert(fm.all('_05010007_1').value);
	   if(fm.all('_05010007_1').value=="mg/dl")//空腹血糖
	   {
	   	  fm.all('05010007').value=fm.all('05010007').value/18;
	   	  fm.all('05010007').value=fm.all('05010007').value.substring(0,parseInt((fm.all('05010007').value.indexOf(".")+3)));
	      fm.all('_05010007_1').value="mmol/l";
	   }
	   if(fm.all('_05010008_1').value=="mg/dl")//总胆固醇：
	   {
	   	  fm.all('05010008').value=fm.all('05010008').value/38.46;
	   	  fm.all('05010008').value=fm.all('05010008').value.substring(0,parseInt((fm.all('05010008').value.indexOf(".")+3)));
	      fm.all('_05010008_1').value="mmol/l";
	   }
	   if(fm.all('_05010009_1').value=="mg/dl")//甘油三脂：
	   {
	   	  fm.all('05010009').value=fm.all('05010009').value/90.91;
	   	  fm.all('05010009').value=fm.all('05010009').value.substring(0,parseInt((fm.all('05010009').value.indexOf(".")+3)));
	      fm.all('_05010009_1').value="mmol/l";
	   }
	   if(fm.all('_05010010_1').value=="mg/dl")//高密度脂蛋白：
	   {
	   	  fm.all('05010010').value=fm.all('05010010').value/38.46;
	   	  fm.all('05010010').value=fm.all('05010010').value.substring(0,parseInt((fm.all('05010010').value.indexOf(".")+3)));
	      fm.all('_05010010_1').value="mmol/l";
	   }
	   if(fm.all('_05010018_1').value=="mg/dl")//极低密度脂蛋白
	   {
	   	  fm.all('05010018').value=fm.all('05010018').value/38.46;
	   	  fm.all('05010018').value=fm.all('05010018').value.substring(0,parseInt((fm.all('05010018').value.indexOf(".")+3)));
	      fm.all('_05010018_1').value="mmol/l";
	   }
	   if(fm.all('_05010011_1').value=="mg/dl")//低密度脂蛋白
	   {
	   	  fm.all('05010011').value=fm.all('05010011').value/38.46;
	   	  fm.all('05010011').value=fm.all('05010011').value.substring(0,parseInt((fm.all('05010011').value.indexOf(".")+3)));
	      fm.all('_05010011_1').value="mmol/l";
	   }
	   var sqlCusNo=" select CusRegistNo  from  LHQuesImportMain "
	               +" where CusRegistNo='"+ fm.all('CusRegistNo').value+"'";
        //alert(sqlCusNo);
        //alert(easyExecSql(sqlCusNo));
     var CusNo =easyExecSql(sqlCusNo);
     //alert(CusRegistNo);
     if(CusNo==""||CusNo=="null"||CusNo==null)
     {
         var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
         var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
         showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
         fm.fmtransact.value="INSERT||MAIN";
         fm.action="./LHQuesKineSave.jsp";
         //alert(fm.all('HealthTest').checked);  
         fm.all('WhethAchieve').value="1";
         fm.all('QuesType').value="01";
         fm.all('Source').value=Source_Js;
         fm.submit(); //提交
         return false;
      }
      if(CusNo!=""&&CusNo!="null"&&CusNo!=null)
      {
      	 if (confirm("您确实要继续保存该记录吗?"))
         {
      	      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
              var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
              showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
              fm.fmtransact.value="UPDATE||MAIN";
              fm.action="./LHQuesKineSave.jsp";        
              fm.all('WhethAchieve').value="1";
              fm.all('QuesType').value="01";
              fm.all('Source').value=Source_Js;
              fm.submit(); //提交
         }
         else
         {
          //mOperate="";
          alert("您取消了继续保存的操作！");
         }
      }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    queryClick();
    //执行下一步操作
     var mulSql = "select a.WhethAchieve"
		 						+" from LHQuesImportMain a where a.CusRegistNo  = '"+ fm.all('CusRegistNo').value +"' ";
     //alert(mulSql);
     var mainRest=easyExecSql(mulSql);
     //alert(mainRest);
     if(mainRest=="1")
     {
       	fm.all('modifyButton').disabled=false;//保存之后可再改
        fm.all('saveButton').disabled=false;
     }
     else
     {
        fm.all('modifyButton').disabled=true;//提交之后不可再改
        fm.all('saveButton').disabled=true;
      }
  }
} 
function queryClick()
{
	if(fm.all('CusRegistNo').value!=""&&fm.all('CusRegistNo').value!=null&&fm.all('CusRegistNo').value!="null")
	{
	 	 var mulSql = "select a.CustomerNo ,SubmitDate ,MakeDate ,MakeTime "
		 						+" from LHQuesImportMain a where a.CusRegistNo  like '"+ fm.all('CusRegistNo').value +"' ";
     //alert(mulSql);
     var mainRest=easyExecSql(mulSql);
     //alert(mainRest);
		 if(mainRest!=""&&mainRest!=null&&mainRest!="null")
		 {
        fm.all('CustomerNo').value				= mainRest[0][0];
        fm.all('CustomerName').value=easyExecSql("select a.Name from ldperson a where a.CustomerNo='"+fm.all('CustomerNo').value+"'");
        fm.all('SubmitDate').value=mainRest[0][1];
        fm.all('MakeDate').value=mainRest[0][2];
        fm.all('MakeTime').value=mainRest[0][3];
     }
     var relaSql = "select a.ContNo ,a.ServPlanNo ,a.ServItemNo ,a.ServCaseCode ,"
                +" a.ServTaskNo ,a.ServTaskCode,a.TaskExecNo "
		 						+" from LHQuesImportMain a where a.CusRegistNo  like '"+ fm.all('CusRegistNo').value +"' ";
     //alert(relaSql);
     var relaRest=new Array;
     relaRest=easyExecSql(relaSql);
     //alert(relaRest);
		 if(relaRest!=""&&relaRest!=null&&relaRest!="null")
		 {
        fm.all('ContNo').value = relaRest[0][0];
        fm.all('ServPlanNo').value=relaRest[0][1];;
        fm.all('ServItemNo').value=relaRest[0][2];
        fm.all('ServCaseCode').value=relaRest[0][3];
        fm.all('ServTaskNo').value=relaRest[0][4];
        fm.all('ServTaskCode').value=relaRest[0][5];
        fm.all('TaskExecNo').value=relaRest[0][6];
     }

     var strSql = " select a.StandCode ,a.LetterResult  "
				 +" from LHQuesImportDetail a "
				 +" where a.CusRegistNo  = '"+ fm.all('CusRegistNo').value +"' ";
     //alert(strSql);
     var detailRest=new Array;
     detailRest=easyExecSql(strSql);
     //alert(detailRest);
		 if(detailRest!=""&&detailRest!=null&&detailRest!="null")
		 {
        for(var i=0;i<detailRest.length;i++)
        {
        	 if(detailRest[i][1]!=""&&detailRest[i][1]!=null&&detailRest[i][1]!="null")
        	 {
							if(fm.all(detailRest[i][0]).type == "checkbox")
        	    {
        	     	if(fm.all(detailRest[i][0]).value =="on")
        	     	{
        	     		fm.all(detailRest[i][0]).checked =true;
        	     	}
        	    }
        	    else
        	    {
        	    	//alert("detailRest[i][1] 111111111111 "+detailRest[i][0]+"  "+ detailRest[i][1]);
        	    	fm.all(detailRest[i][0]).value = detailRest[i][1];
        	    }
        	 }
        }
        //alert(fm.all('03010001').value);
        HighBlood();
        selectRadio();
        if(fm.all('05010001').value=="51"&&fm.all('05010002').value=="26"&&fm.all('05010003').value=="51"&&fm.all('05010004').value=="51"&&fm.all('05010005').value=="61"&&fm.all('05010006').value=="51"&&fm.all('05010007').value=="3"&&fm.all('05010008').value=="2"&&fm.all('05010009').value=="1"&&fm.all('05010010').value=="1"&&fm.all('05010011').value=="1"&&fm.all('05010012').value=="6"&&fm.all('05010015').value=="2"&&fm.all('05010016').value=="2"&&fm.all('05010017').value=="51"&&fm.all('05010018').value=="1"&&fm.all('05010013').value=="6"&&fm.all('05010014').value=="11")
	      //if(fm.all('05010001').value=="51")
	      {
	      	  fm.all('HealthTest').checked == false;
		        divLHQuesInputGrid.style.display="none";
	          fm.all('05010001').value="";//身高
            fm.all('05010002').value="";//体重
            fm.all('05010003').value="";//腰围
            fm.all('05010004').value="";//臀围
            fm.all('05010005').value="";//收缩压
            fm.all('05010006').value="";//舒张压
            fm.all('05010007').value="";//空腹血糖
            fm.all('05010008').value="";//总胆固醇
            fm.all('05010009').value="";//甘油三脂
            fm.all('05010010').value="";//高密度脂蛋白胆固醇
            fm.all('05010011').value="";//低密度脂蛋白胆固醇
            fm.all('05010012').value="";//白蛋白	
            fm.all('05010015').value="";//糖化血红蛋白
            fm.all('05010016').value="";//胰岛素Insulin
            fm.all('05010017').value="";//血红蛋白
            fm.all('05010018').value="";//极低密度脂蛋白胆固醇
            fm.all('05010013').value="";//球蛋白
            fm.all('05010014').value="";//总蛋白 
            fm.all('05010019').value="";//纤维蛋白原Fibrinogen  
            fm.all('05010020').value="";// 脂蛋白α(Lipoprotein α)   
            fm.all('05010021').value="";//前列腺特异性抗原(Prostate specific antigen)   
         }
     }
     //alert(fm.all('08010003').value);
     var occSql = " select trim(OccupationName)||'-'||workname "
		 						+" from LDOccupation a where  a.OccupationCode = '"+ fm.all('08010003').value +"' ";
     var occRest=easyExecSql(occSql);
		 if(occRest!=""&&occRest!=null&&occRest!="null")
		 {
        fm.all('_08010003_1').value=occRest[0][0];
     }
     var ageSql = " select Code, CodeName "
		 						+" from ldcode  a where  codetype='lhschoolage' and a.code = '"+ fm.all('08010004').value +"' ";
     //alert(ageSql);
     var ageRest=easyExecSql(ageSql);
     //alert(ageRest);
		 if(ageRest!=""&&ageRest!=null&&ageRest!="null")
		 {
        fm.all('08010004').value				= ageRest[0][0];
        fm.all('_08010004_1').value=ageRest[0][1];
     }
     var mulSql = "select a.WhethAchieve"
		 						+" from LHQuesImportMain a where a.CusRegistNo  = '"+ fm.all('CusRegistNo').value +"' ";
     //alert(mulSql);
     var mainRest=easyExecSql(mulSql);
     //alert(mainRest);
     if(mainRest=="3")
     {
        fm.all('HealthTest').checked =false;
		    divLHQuesInputGrid.style.display="none";
	   }
	   else
	   {
	   	  fm.all('HealthTest').checked =true;
		    divLHQuesInputGrid.style.display="";
		 }
     if(mainRest=="1"||mainRest==""||mainRest==null||mainRest=="null")
     {
       	fm.all('modifyButton').disabled=false;//保存之后可再改
        fm.all('saveButton').disabled=false;
     }
     else
     {
        fm.all('modifyButton').disabled=true;//提交之后不可再改
        fm.all('saveButton').disabled=true;
      }
  }
  else
  {
  	alert("请输入客户登记号码!");
  	return false;
  }
}    
function selectRadio()
{
	fm.all('BirthDate').value=fm.all('08010001').value;
	if(fm.all('03020003').value=="1")//距上次医生给您做前列腺检查有多长时间了?
	{
		  fm.MyearCheck[0].checked=true;
	}
	if(fm.all('03020003').value=="2")
	{
		  fm.MyearCheck[1].checked=true;
	}
	if(fm.all('03020003').value=="3")
	{
		  fm.MyearCheck[2].checked=true;
	}
	if(fm.all('03020003').value=="4")
	{
		  fm.MyearCheck[3].checked=true;
	}
	if(fm.all('03020003').value=="5")
	{
		  fm.MyearCheck[4].checked=true;
	}
	if(fm.all('01020001').value=="1")//男性是否有人曾患前列腺癌
	{
		  fm.ManYesNo[0].checked=true;
	}
	if(fm.all('01020001').value=="2")
	{
		  fm.ManYesNo[1].checked=true;
	}
	if(fm.all('01020001').value=="3")
	{
		  fm.ManYesNo[2].checked=true;
	}
	if(fm.all('01020002').value=="1")//男性、外甥中是否有人曾患前列腺癌?
	{
		  fm.FatYesNo[0].checked=true;
	}
	if(fm.all('01020002').value=="2")
	{
		  fm.FatYesNo[1].checked=true;
	}
	if(fm.all('01020002').value=="3")
	{
		  fm.FatYesNo[2].checked=true;
	}
		if(fm.all('01020003').value=="1")//男性您的表兄弟中是否有人曾患前列腺癌?
	{
		  fm.BroYesNo[0].checked=true;
	}
	if(fm.all('01020003').value=="2")
	{
		  fm.BroYesNo[1].checked=true;
	}
	if(fm.all('01020003').value=="3")
	{
		  fm.BroYesNo[2].checked=true;
	}
	if(fm.all('08010002').value=="1")//性别
	{
		  checkSex('1');
		  fm.sex[0].checked=true;
	}
	if(fm.all('08010002').value=="2")
	{
		  checkSex('2');
		  fm.sex[1].checked=true;
	}
	//alert(fm.all('03010001').value);
	if(fm.all('03010001').value=="1")//你有服高血压药物史吗? 
	{
		  testMedication('1');
		  fm.HighYesNo[0].checked=true;
	}
	if(fm.all('03010001').value=="2")
	{
		  testMedication('2');
		  fm.HighYesNo[1].checked=true;
	}
	if(fm.all('05010022').value=="1")//房颤
	{
		  fm.RealYesNo[0].checked=true;
	}
	if(fm.all('05010022').value=="2")
	{
		  fm.RealYesNo[1].checked=true;
	}
	if(fm.all('05010023').value=="1")//左心室肥大
	{
		  fm.LevoYesNo[0].checked=true;
	}
	if(fm.all('05010023').value=="2")
	{
		  fm.LevoYesNo[1].checked=true;
	}
  if(fm.all('01010002').value=="1")//是否曾人有人患乳腺癌
	{
		  fm.WomYesNo[0].checked=true;
	}
	if(fm.all('01010002').value=="2")
	{
		  fm.WomYesNo[1].checked=true;
	}
	if(fm.all('01010002').value=="3")
	{
		  fm.WomYesNo[2].checked=true;
	}
	if(fm.all('01010004').value=="1")//表姐妹是否曾人有人患乳腺癌
	{
		  fm.SisYesNo[0].checked=true;
	}
	if(fm.all('01010004').value=="2")
	{
		  fm.SisYesNo[1].checked=true;
	}
	if(fm.all('01010004').value=="3")
	{
		  fm.SisYesNo[2].checked=true;
	}
  if(fm.all('06010001').value=="1")//是否做过子宫切除手术
	{
		  fm.OpsYesNo[0].checked=true;
	}
	if(fm.all('06010001').value=="2")
	{
		  fm.OpsYesNo[1].checked=true;
	}
	if(fm.all('03020001').value=="1")//多长时间做一次乳腺自我检查 
	{
		  fm.MeCheck[0].checked=true;
	}
	if(fm.all('03020001').value=="2")
	{
		  fm.MeCheck[1].checked=true;
	}
	if(fm.all('03020001').value=="3")
	{
		  fm.MeCheck[2].checked=true;
	}
	if(fm.all('03020001').value=="4")
	{
		  fm.MeCheck[3].checked=true;
	}
  if(fm.all('03020002').value=="1")//距上一次医生或护士给您检查乳腺有多长时间了
	{
		  fm.YearCheck[0].checked=true;
	}
	if(fm.all('03020002').value=="2")
	{
		  fm.YearCheck[1].checked=true;
	}
	if(fm.all('03020002').value=="3")
	{
		  fm.YearCheck[2].checked=true;
	}
	if(fm.all('03020002').value=="4")
	{
		  fm.YearCheck[3].checked=true;
	}
	if(fm.all('03020002').value=="5")
	{
		  fm.YearCheck[4].checked=true;
	}
	if(fm.all('03010002').value=="1")//您是否服用雌激素类的药物? 
	{
		  getMediInfo('1');
		  fm.MediYesNo[0].checked=true;
	}
	if(fm.all('03010002').value=="2")
	{
		  getMediInfo('2');
		  fm.MediYesNo[1].checked=true;
	}
	//alert(fm.all('01030004').value);
	if(fm.all('01030004').value=="1")//糖尿病 父母、兄弟姐妹、子女  
	{
		  getForty('1');
		  fm.DiabYesNo[0].checked=true;
	}
	if(fm.all('01030004').value=="2")
	{
		  getForty('2');
		  fm.DiabYesNo[1].checked=true;
	}
	if(fm.all('01030004').value=="3")
	{
		  getForty('3');
		  fm.DiabYesNo[2].checked=true;
	}
	
	if(fm.all('01030001').value=="1")//糖尿病 是否有人在40 岁以前?
	{
		  
		  fm.FortTesNo[0].checked=true;
		  
	}
	if(fm.all('01030001').value=="2")
	{
		  fm.FortTesNo[1].checked=true;
	}
	if(fm.all('01030001').value=="3")
	{
		  fm.FortTesNo[2].checked=true;
	}
	if(fm.all('01030002').value=="1")//糖尿病、(外)祖父母、叔舅、姑姨、侄子(女)、外甥(女)
	{
		  fm.UnYesNo[0].checked=true;
	}
	if(fm.all('01030002').value=="2")
	{
		  fm.UnYesNo[1].checked=true;
	}
	if(fm.all('01030002').value=="3")
	{
		  fm.UnYesNo[2].checked=true;
	}
	if(fm.all('01030003').value=="1")//糖尿病、表兄妹
	{
		  fm.CousinYesNo[0].checked=true;
	}
	if(fm.all('01030003').value=="2")
	{
		  fm.CousinYesNo[1].checked=true;
	}
	if(fm.all('01030003').value=="3")
	{
		  fm.CousinYesNo[2].checked=true;
	}
	if(fm.all('01040001').value=="1")// 冠心病 父母
	{
		  getFifty('1');
		  fm.ParYesNo[0].checked=true;
	}
	if(fm.all('01040001').value=="2")
	{
		  getFifty('2');
		  fm.ParYesNo[1].checked=true;
	}
	if(fm.all('01040001').value=="3")
	{
		  getFifty('3');
		  fm.ParYesNo[2].checked=true;
	}
	if(fm.all('01040002').value=="1")// 冠心病是否有人在50 岁以前
	{
		  
		  fm.FifYesNo[0].checked=true;
		  
	}
	if(fm.all('01040002').value=="2")
	{
		  fm.FifYesNo[1].checked=true;
		  
	}
	if(fm.all('01040002').value=="3")
	{
		  fm.FifYesNo[2].checked=true;
		  
	}
	if(fm.all('01050001').value=="1")// 中 风  父母
	{
		  getSixty('1');
		  fm.PartsYesNo[0].checked=true;
	}
	if(fm.all('01050001').value=="2")
	{
		  getSixty('2');
		  fm.PartsYesNo[1].checked=true;
	}
	if(fm.all('01050001').value=="3")
	{
		  getSixty('3');
		  fm.PartsYesNo[2].checked=true;
	}
	if(fm.all('01050002').value=="1")// 中 风 是否有人在60 岁以前
	{
		
		  fm.SixYesNo[0].checked=true;
		 
	}
	if(fm.all('01050002').value=="2")
	{
		
		  fm.SixYesNo[1].checked=true;
		  
	}
	if(fm.all('01050002').value=="3")
	{
		 
		  fm.SixYesNo[2].checked=true;
		  
	}
	if(fm.all('01060001').value=="1")//  高血压  父母、兄弟姐妹、子女 
	{
		  fm.FamYesNo[0].checked=true;
	}
	if(fm.all('01060001').value=="2")
	{
		  fm.FamYesNo[1].checked=true;
	}
	if(fm.all('01060001').value=="3")
	{
		  fm.FamYesNo[2].checked=true;
	}
	if(fm.all('01060002').value=="1")//  高血压 外)祖父母、叔舅、姑姨、侄子(女)、外甥(女)
	{
		  fm.PlyYesNo[0].checked=true;
	}
	if(fm.all('01060002').value=="2")
	{
		  fm.PlyYesNo[1].checked=true;
	}
	if(fm.all('01060002').value=="3")
	{
		  fm.PlyYesNo[2].checked=true;
	}
	if(fm.all('01090001').value=="1")//  骨 折 
	{
		  fm.FampaYesNo[0].checked=true;
	}
	if(fm.all('01090001').value=="2")
	{
		  fm.FampaYesNo[1].checked=true;
	}
	if(fm.all('01090001').value=="3")
	{
		  fm.FampaYesNo[2].checked=true;
	}
	if(fm.all('01070001').value=="1")//   肺 癌父母、兄弟姐妹、子女   
	{
		  fm.ConYesNo[0].checked=true;
	}
	if(fm.all('01070001').value=="2")
	{
		  fm.ConYesNo[1].checked=true;
	}
	if(fm.all('01070001').value=="3")
	{
		  fm.ConYesNo[2].checked=true;
	}
	if(fm.all('01070002').value=="1")//   肺 癌 )祖父母、叔舅、姑姨、侄子(女)、外甥(女 
	{
		  fm.ConplyYesNo[0].checked=true;
	}
	if(fm.all('01070002').value=="2")
	{
		  fm.ConplyYesNo[1].checked=true;
	}
	if(fm.all('01070002').value=="3")
	{
		  fm.ConplyYesNo[2].checked=true;
	}
	if(fm.all('02010001').value=="1")//您自己认为您的口味是：  
	{
		  fm.tasteYesNo[0].checked=true;
	}
	if(fm.all('02010001').value=="2")
	{
		  fm.tasteYesNo[1].checked=true;
	}
	if(fm.all('02010001').value=="3")
	{
		  fm.tasteYesNo[2].checked=true;
	}
	if(fm.all('02010002').value=="1")//油炸及多脂类食品食用习惯是： 
	{
		  fm.habitYesNo[0].checked=true;
	}
	if(fm.all('02010002').value=="2")
	{
		  fm.habitYesNo[1].checked=true;
	}
	if(fm.all('02010002').value=="3")
	{
		  fm.habitYesNo[2].checked=true;
	}
	if(fm.all('02020001').value=="1")//你现在吸烟吗： 
	{
		  YesNoSmoke('1');
		  fm.togeDay[0].checked=true;
	}
	if(fm.all('02020001').value=="2")
	{
		  YesNoSmoke('2');
		  fm.togeDay[1].checked=true;
	}
	if(fm.all('02020001').value=="3")
	{
		  YesNoSmoke('3');
		  fm.togeDay[2].checked=true;
	}
	if(fm.all('02020011').value=="1")//是否有人吸烟
	{
		  getSmokeTime('1');
		  fm.togeYesNo[0].checked=true;
	}
	if(fm.all('02020011').value=="2")
	{
		  getSmokeTime('2');
		  fm.togeYesNo[1].checked=true;
	}
	if(fm.all('02020012').value=="1")//您平均每周和他们呆在一起的时间是?
	{
		  fm.togeTime[0].checked=true;
	}
	if(fm.all('02020012').value=="2")
	{
		  fm.togeTime[1].checked=true;
	}
	if(fm.all('02020012').value=="3")
	{
		  fm.togeTime[2].checked=true;
	}
	if(fm.all('02040003').value=="1")//你的出行方式是:
	{
		  fm.RideMode[0].checked=true;
	}
	if(fm.all('02040003').value=="2")
	{
		  fm.RideMode[1].checked=true;
	}
	if(fm.all('02040003').value=="3")
	{
		  fm.RideMode[2].checked=true;
	}
	if(fm.all('02040003').value=="4")
	{
		  fm.RideMode[3].checked=true;
	}
	if(fm.all('02040003').value=="5")
	{
		  fm.RideMode[4].checked=true;
	}
	if(fm.all('02040003').value=="6")
	{
		  fm.RideMode[5].checked=true;
	}
	if(fm.all('02040005').value=="1")//您参加体育锻炼吗?
	{
		  getExeice('1');
		  fm.execYesNo[0].checked=true;
	}
	if(fm.all('02040005').value=="2")
	{
		  getExeice('2');
		  fm.execYesNo[1].checked=true;
	}
	if(fm.all('02040007').value=="1")// 您最常用锻炼方式是什么?
	{
		  fm.ExisMode[0].checked=true;
	}
	if(fm.all('02040007').value=="2")
	{
		  fm.ExisMode[1].checked=true;
	}
	if(fm.all('02040007').value=="3")
	{
		  fm.ExisMode[2].checked=true;
	}
	if(fm.all('02040007').value=="4")
	{
		  fm.ExisMode[3].checked=true;
	}
	if(fm.all('02040007').value=="5")
	{
		  fm.ExisMode[4].checked=true;
	}
	if(fm.all('02040007').value=="6")
	{
		  fm.ExisMode[5].checked=true;
	}
	if(fm.all('02050001').value=="1")//你经历了哪些不愉快或不幸的事情
	{
		  fm.DispMode[0].checked=true;
	}
	if(fm.all('02050001').value=="2")
	{
		  fm.DispMode[1].checked=true;
	}
	if(fm.all('02050002').value=="1")// 你认为自己现在的健康状况如何? 
	{
		  fm.HealpMode[0].checked=true;
	}
	if(fm.all('02050002').value=="2")
	{
		  fm.HealpMode[1].checked=true;
	}
	if(fm.all('02050002').value=="3")
	{
		  fm.HealpMode[2].checked=true;
	}
	if(fm.all('02050002').value=="4")
	{
		  fm.HealpMode[3].checked=true;
	}
	if(fm.all('02050002').value=="5")
	{
		  fm.HealpMode[4].checked=true;
	}
	if(fm.all('02050003').value=="1")//您的情绪问题(如紧张、急躁、焦虑)，在工作或其它活动中是否出现了以下问题? 
	{
		  fm.tenspmode[0].checked=true;
	}
	if(fm.all('02050003').value=="2")
	{
		  fm.tenspmode[1].checked=true;
	}
	if(fm.all('02050003').value=="3")
	{
		  fm.tenspmode[2].checked=true;
	}
	if(fm.all('02050003').value=="4")
	{
		  fm.tenspmode[3].checked=true;
	}
		if(fm.all('07010001').value==""||fm.all('07010001').value=="null"||fm.all('07010001').value==null)
	{
		fm.all('07010001').className = "gray2";
	}
	else
	{
		fm.all('07010001').className = "test";
	}
	if(fm.all('07010002').value==""||fm.all('07010002').value=="null"||fm.all('07010002').value==null)
	{
		fm.all('07010002').className = "gray2";
	}
	else
	{
		fm.all('07010002').className = "test";
	}
	if(fm.all('07010003').value==""||fm.all('07010003').value=="null"||fm.all('07010003').value==null)
	{
		fm.all('07010003').className = "gray2";
	}
	else
	{
		fm.all('07010003').className = "test";
	}
	if(fm.all('07010005').value==""||fm.all('07010005').value=="null"||fm.all('07010005').value==null)
	{
		fm.all('07010005').className = "gray2";
	}
	else
	{
		fm.all('07010005').className = "test";
	}
	if(fm.all('07010006').value==""||fm.all('07010006').value=="null"||fm.all('07010006').value==null)
	{
		fm.all('07010006').className = "gray2";
	}
	else
	{
		fm.all('07010006').className = "test";
	}
	if(fm.all('07010007').value==""||fm.all('07010007').value=="null"||fm.all('07010007').value==null)
	{
		fm.all('07010007').className = "gray2";
	}
	else
	{
		fm.all('07010007').className = "test";
	}
	if(fm.all('07010009').value==""||fm.all('07010009').value=="null"||fm.all('07010009').value==null)
	{
		fm.all('07010009').className = "gray2";
	}
	else
	{
		fm.all('07010009').className = "test";
	}
	if(fm.all('07010010').value==""||fm.all('07010010').value=="null"||fm.all('07010010').value==null)
	{
		fm.all('07010010').className = "gray2";
	}
	else
	{
		fm.all('07010010').className = "test";
	}
	if(fm.all('07010011').value==""||fm.all('07010011').value=="null"||fm.all('07010011').value==null)
	{
		fm.all('07010011').className = "gray2";
	}
	else
	{
		fm.all('07010011').className = "test";
	}
	if(fm.all('07010013').value==""||fm.all('07010013').value=="null"||fm.all('07010013').value==null)
	{
		fm.all('07010013').className = "gray2";
	}
	else
	{
		fm.all('07010013').className = "test";
	}
	if(fm.all('07010014').value==""||fm.all('07010014').value=="null"||fm.all('07010014').value==null)
	{
		fm.all('07010014').className = "gray2";
	}
	else
	{
		fm.all('07010014').className = "test";
	}
	if(fm.all('07010015').value==""||fm.all('07010015').value=="null"||fm.all('07010015').value==null)
	{
		fm.all('07010015').className = "gray2";
	}
	else
	{
		fm.all('07010015').className = "test";
	}
	if(fm.all('07010017').value==""||fm.all('07010017').value=="null"||fm.all('07010017').value==null)
	{
		fm.all('07010017').className = "gray2";
	}
	else
	{
		fm.all('07010017').className = "test";
	}
	if(fm.all('07010018').value==""||fm.all('07010018').value=="null"||fm.all('07010018').value==null)
	{
		fm.all('07010018').className = "gray2";
	}
	else
	{
		fm.all('07010018').className = "test";
	}
	if(fm.all('07010019').value==""||fm.all('07010019').value=="null"||fm.all('07010019').value==null)
	{
		fm.all('07010019').className = "gray2";
	}
	else
	{
		fm.all('07010019').className = "test";
	}
	if(fm.all('07010021').value==""||fm.all('07010021').value=="null"||fm.all('07010021').value==null)
	{
		fm.all('07010021').className = "gray2";
	}
	else
	{
		fm.all('07010021').className = "test";
	}
	if(fm.all('07010022').value==""||fm.all('07010022').value=="null"||fm.all('07010022').value==null)
	{
		fm.all('07010022').className = "gray2";
	}
	else
	{
		fm.all('07010022').className = "test";
	}
	if(fm.all('07010023').value==""||fm.all('07010023').value=="null"||fm.all('07010023').value==null)
	{
		fm.all('07010023').className = "gray2";
	}
	else
	{
		fm.all('07010023').className = "test";
	}
	if(fm.all('07010025').value==""||fm.all('07010025').value=="null"||fm.all('07010025').value==null)
	{
		fm.all('07010025').className = "gray2";
	}
	else
	{
		fm.all('07010025').className = "test";
	}
	if(fm.all('07010026').value==""||fm.all('07010026').value=="null"||fm.all('07010026').value==null)
	{
		fm.all('07010026').className = "gray2";
	}
	else
	{
		fm.all('07010026').className = "test";
	}
	if(fm.all('07010027').value==""||fm.all('07010027').value=="null"||fm.all('07010027').value==null)
	{
		fm.all('07010027').className = "gray2";
	}
	else
	{
		fm.all('07010027').className = "test";
	}
	if(fm.all('07010029').value==""||fm.all('07010029').value=="null"||fm.all('07010029').value==null)
	{
		fm.all('07010029').className = "gray2";
	}
	else
	{
		fm.all('07010029').className = "test";
	}
	if(fm.all('07010030').value==""||fm.all('07010030').value=="null"||fm.all('07010030').value==null)
	{
		fm.all('07010030').className = "gray2";
	}
	else
	{
		fm.all('07010030').className = "test";
	}
	if(fm.all('07010031').value==""||fm.all('07010031').value=="null"||fm.all('07010031').value==null)
	{
		fm.all('07010031').className = "gray2";
	}
	else
	{
		fm.all('07010031').className = "test";
	}
	if(fm.all('07010033').value==""||fm.all('07010033').value=="null"||fm.all('07010033').value==null)
	{
		fm.all('07010033').className = "gray2";
	}
	else
	{
		fm.all('07010033').className = "test";
	}
	if(fm.all('07010034').value==""||fm.all('07010034').value=="null"||fm.all('07010034').value==null)
	{
		fm.all('07010034').className = "gray2";
	}
	else
	{
		fm.all('07010034').className = "test";
	}
	if(fm.all('07010035').value==""||fm.all('07010035').value=="null"||fm.all('07010035').value==null)
	{
		fm.all('07010035').className = "gray2";
	}
	else
	{
		fm.all('07010035').className = "test";
	}
	if(fm.all('07010037').value==""||fm.all('07010037').value=="null"||fm.all('07010037').value==null)
	{
		fm.all('07010037').className = "gray2";
	}
	else
	{
		fm.all('07010037').className = "test";
	}
	if(fm.all('07010038').value==""||fm.all('07010038').value=="null"||fm.all('07010038').value==null)
	{
		fm.all('07010038').className = "gray2";
	}
	else
	{
		fm.all('07010038').className = "test";
	}
	if(fm.all('07010039').value==""||fm.all('07010039').value=="null"||fm.all('07010039').value==null)
	{
		fm.all('07010039').className = "gray2";
	}
	else
	{
		fm.all('07010039').className = "test";
	}
	//alert(fm.all('07010069').value);
	if(fm.all('07010069').value==""||fm.all('07010069').value=="null"||fm.all('07010069').value==null)
	{
		fm.all('07010069').className = "gray2";
	}
	else
	{
		fm.all('07010069').className = "test";
	}
	if(fm.all('07010070').value==""||fm.all('07010070').value=="null"||fm.all('07010070').value==null)
	{
		fm.all('07010070').className = "gray2";
	}
	else
	{
		fm.all('07010070').className = "test";
	}
	if(fm.all('07010071').value==""||fm.all('07010071').value=="null"||fm.all('07010071').value==null)
	{
		fm.all('07010071').className = "gray2";
	}
	else
	{
		fm.all('07010071').className = "test";
	}
	if(fm.all('07010041').value==""||fm.all('07010041').value=="null"||fm.all('07010041').value==null)
	{
		fm.all('07010041').className = "gray2";
	}
	else
	{
		fm.all('07010041').className = "test";
	}
	if(fm.all('07010042').value==""||fm.all('07010042').value=="null"||fm.all('07010042').value==null)
	{
		fm.all('07010042').className = "gray2";
	}
	else
	{
		fm.all('07010042').className = "test";
	}
	if(fm.all('07010043').value==""||fm.all('07010043').value=="null"||fm.all('07010043').value==null)
	{
		fm.all('07010043').className = "gray2";
	}
	else
	{
		fm.all('07010043').className = "test";
	}
	if(fm.all('07010045').value==""||fm.all('07010045').value=="null"||fm.all('07010045').value==null)
	{
		fm.all('07010045').className = "gray2";
	}
	else
	{
		fm.all('07010045').className = "test";
	}
	if(fm.all('07010046').value==""||fm.all('07010046').value=="null"||fm.all('07010046').value==null)
	{
		fm.all('07010046').className = "gray2";
	}
	else
	{
		fm.all('07010046').className = "test";
	}
	if(fm.all('07010047').value==""||fm.all('07010047').value=="null"||fm.all('07010047').value==null)
	{
		fm.all('07010047').className = "gray2";
	}
	else
	{
		fm.all('07010047').className = "test";
	}
	if(fm.all('07010049').value==""||fm.all('07010049').value=="null"||fm.all('07010049').value==null)
	{
		fm.all('07010049').className = "gray2";
	}
	else
	{
		fm.all('07010049').className = "test";
	}
	if(fm.all('07010050').value==""||fm.all('07010050').value=="null"||fm.all('07010050').value==null)
	{
		fm.all('07010050').className = "gray2";
	}
	else
	{
		fm.all('07010050').className = "test";
	}
	if(fm.all('07010051').value==""||fm.all('07010051').value=="null"||fm.all('07010051').value==null)
	{
		fm.all('07010051').className = "gray2";
	}
	else
	{
		fm.all('07010051').className = "test";
	}
	if(fm.all('07010053').value==""||fm.all('07010053').value=="null"||fm.all('07010053').value==null)
	{
		fm.all('07010053').className = "gray2";
	}
	else
	{
		fm.all('07010053').className = "test";
	}
	if(fm.all('07010054').value==""||fm.all('07010054').value=="null"||fm.all('07010054').value==null)
	{
		fm.all('07010054').className = "gray2";
	}
	else
	{
		fm.all('07010054').className = "test";
	}
	if(fm.all('07010055').value==""||fm.all('07010055').value=="null"||fm.all('07010055').value==null)
	{
		fm.all('07010055').className = "gray2";
	}
	else
	{
		fm.all('07010055').className = "test";
	}
	if(fm.all('07010057').value==""||fm.all('07010057').value=="null"||fm.all('07010057').value==null)
	{
		fm.all('07010057').className = "gray2";
	}
	else
	{
		fm.all('07010057').className = "test";
	}
	if(fm.all('07010058').value==""||fm.all('07010058').value=="null"||fm.all('07010058').value==null)
	{
		fm.all('07010058').className = "gray2";
	}
	else
	{
		fm.all('07010058').className = "test";
	}
	if(fm.all('07010059').value==""||fm.all('07010059').value=="null"||fm.all('07010059').value==null)
	{
		fm.all('07010059').className = "gray2";
	}
	else
	{
		fm.all('07010059').className = "test";
	}
	if(fm.all('07010061').value==""||fm.all('07010061').value=="null"||fm.all('07010061').value==null)
	{
		fm.all('07010061').className = "gray2";
	}
	else
	{
		fm.all('07010061').className = "test";
	}
	if(fm.all('07010062').value==""||fm.all('07010062').value=="null"||fm.all('07010062').value==null)
	{
		fm.all('07010062').className = "gray2";
	}
	else
	{
		fm.all('07010062').className = "test";
	}
	if(fm.all('07010063').value==""||fm.all('07010063').value=="null"||fm.all('07010063').value==null)
	{
		fm.all('07010063').className = "gray2";
	}
	else
	{
		fm.all('07010063').className = "test";
	}
	if(fm.all('07010065').value==""||fm.all('07010065').value=="null"||fm.all('07010065').value==null)
	{
		fm.all('07010065').className = "gray2";
	}
	else
	{
		fm.all('07010065').className = "test";
	}
	if(fm.all('07010066').value==""||fm.all('07010066').value=="null"||fm.all('07010066').value==null)
	{
		fm.all('07010066').className = "gray2";
	}
	else
	{
		fm.all('07010066').className = "test";
	}
	if(fm.all('07010067').value==""||fm.all('07010067').value=="null"||fm.all('07010067').value==null)
	{
		fm.all('07010067').className = "gray2";
	}
	else
	{
		fm.all('07010067').className = "test";
	}
	
}     
//Click事件，当点击“修改”图片时触发该函数
function submitClick()
{	 
		 //alert(fm.all('_05010007_1').value);
	   if(fm.all('_05010007_1').value=="mg/dl")//空腹血糖
	   {
	   	  fm.all('05010007').value=fm.all('05010007').value/18;
	   	  fm.all('05010007').value=fm.all('05010007').value.substring(0,parseInt((fm.all('05010007').value.indexOf(".")+3)));
	      fm.all('_05010007_1').value="mmol/l";
	   }
	   if(fm.all('_05010008_1').value=="mg/dl")//总胆固醇：
	   {
	   	  fm.all('05010008').value=fm.all('05010008').value/38.46;
	   	  fm.all('05010008').value=fm.all('05010008').value.substring(0,parseInt((fm.all('05010008').value.indexOf(".")+3)));
	      fm.all('_05010008_1').value="mmol/l";
	   }
	   if(fm.all('_05010009_1').value=="mg/dl")//甘油三脂：
	   {
	   	  fm.all('05010009').value=fm.all('05010009').value/90.91;
	   	  fm.all('05010009').value=fm.all('05010009').value.substring(0,parseInt((fm.all('05010009').value.indexOf(".")+3)));
	      fm.all('_05010009_1').value="mmol/l";
	   }
	   if(fm.all('_05010010_1').value=="mg/dl")//高密度脂蛋白：
	   {
	   	  fm.all('05010010').value=fm.all('05010010').value/38.46;
	   	  fm.all('05010010').value=fm.all('05010010').value.substring(0,parseInt((fm.all('05010010').value.indexOf(".")+3)));
	      fm.all('_05010010_1').value="mmol/l";
	   }
	   if(fm.all('_05010018_1').value=="mg/dl")//极低密度脂蛋白
	   {
	   	  fm.all('05010018').value=fm.all('05010018').value/38.46;
	   	  fm.all('05010018').value=fm.all('05010018').value.substring(0,parseInt((fm.all('05010018').value.indexOf(".")+3)));
	      fm.all('_05010018_1').value="mmol/l";
	   }
	   if(fm.all('_05010011_1').value=="mg/dl")//低密度脂蛋白
	   {
	   	  fm.all('05010011').value=fm.all('05010011').value/38.46;
	   	  fm.all('05010011').value=fm.all('05010011').value.substring(0,parseInt((fm.all('05010011').value.indexOf(".")+3)));
	      fm.all('_05010011_1').value="mmol/l";
	   }
		 var sqlCusRegistNo=" select CusRegistNo  from  LHQuesImportMain "
	               +" where CusRegistNo='"+ fm.all('CusRegistNo').value+"'";
       
        //alert(easyExecSql(sqlCusRegistNo));
     var CusRegistNo =easyExecSql(sqlCusRegistNo);
     //alert(CusRegistNo);
     if(CusRegistNo!=""&&CusRegistNo!="null"&&CusRegistNo!=null)
     {
     	   if(fm.all('HealthTest').checked == true)//填写体格信息
         {
            fm.all('WhethAchieve').value="2";
         }
         else//不填写体格和实验室信息
         {
         	 fm.all('WhethAchieve').value="3";
         	 fm.all('05010001').value="51";//身高
         	 fm.all('05010002').value="26";//体重
         	 fm.all('05010003').value="51";//腰围
         	 fm.all('05010004').value="51";//臀围
         	 fm.all('05010005').value="61";//收缩压
         	 fm.all('05010006').value="51";//舒张压
         	 fm.all('05010007').value="3";//空腹血糖
         	 fm.all('05010008').value="2";//总胆固醇
         	 fm.all('05010009').value="1";//甘油三脂
         	 fm.all('05010010').value="1";//高密度脂蛋白胆固醇
         	 fm.all('05010011').value="1";//低密度脂蛋白胆固醇
         	 fm.all('05010012').value="6";//白蛋白	
         	 fm.all('05010015').value="2";//糖化血红蛋白
         	 fm.all('05010016').value="2";//胰岛素Insulin
         	 fm.all('05010017').value="51";//血红蛋白
         	 fm.all('05010018').value="1";//极低密度脂蛋白胆固醇
         	 fm.all('05010013').value="6";//球蛋白
         	 fm.all('05010014').value="11";//总蛋白
         }
         if(verifyInput2()==false) return false;
         if (confirm("您确实要提交该记录吗?"))
         {
      	      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
              var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
              showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
              fm.fmtransact.value="UPDATE||MAIN";
              fm.action="./LHQuesKineSave.jsp";
              
              fm.all('QuesType').value="01";
              fm.all('Source').value=Source_Js;
              fm.submit(); //提交
             
         }
         else
         {
            alert("您取消了提交的操作！");
         }
     }
     else
     {
     	  alert("您提交的客户登记号不存在，请先保存!");
     	  return false;
     }
}           
 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function getHeart()
{
	   fm.all('HighYesNo').readOnly=false;
	   if(fm.all('HeartDis').value=="on")
	   {
	      fm.all('MediName').readOnly =false;
	   }
}
function getRadioInfo(txtYesNo,Field)
{
    //alert(txtName);
    //alert(Field);
    fm.all(txtYesNo).value=Field;
}
function checkSex(SexV)
{
	if(SexV=="1")
	{
		fm.all('08010002').value = "1";
		divManInfo.style.display="";
		divWomanInfo.style.display="none";
	}
	else
	{
		fm.all('08010002').value = "2";
		divWomanInfo.style.display="";
		divManInfo.style.display="none";
	}
}
function HighBlood()
{
	//alert(fm.all('04010004').checked);
	if(fm.all('04010004').checked == true||fm.all('04010008').checked == true)
	{
		//alert(fm.all('03010001').value);
		if(fm.all('03010001').value=="1")//有服高血压药物史吗?
		{
			fm._03010001_1.disabled = false; 
	        fm._03010001_2.disabled = false; 
			if(fm.all('04010004').value=="on"||fm.all('04010008').value=="on")
			{	   
				//alert(fm._03010001_1.id);
				if(fm._03010001_1.id=="_03010001_1")//有高血压药物史的记录
				{
					fm._03010001_1.checked = true;
					fm.all('03010001').value="1";
					fm.all('03010008').readOnly=false;
					fm.all('03010008').className="test";
				}
			}
		}
		if(fm.all('03010001').value=="2")//无服高血压药物史吗?
		{
			fm._03010001_1.disabled = false; 
	        fm._03010001_2.disabled = false; 
		  	fm._03010001_2.checked = true;
		  	fm.all('03010008').readOnly=true;
		    fm.all('03010008').className="gray2";	
		    fm.all('03010008').value="";	   	    
		}
	}
	else
	{
		fm._03010001_1.disabled = true; 
	    fm._03010001_2.disabled = true; 
	    fm._03010001_2.checked = true; 
	    fm.all('03010008').readOnly=true;
		fm.all('03010008').className="gray2";	
		fm.all('03010008').value="";	   	    
	}
	
}
function testMedication(Field)
{
		 if(Field=="1")
	   {
	   	  fm.all('03010001').value="1";
	   	  fm.all('03010008').readOnly=false;
	   	  fm.all('03010008').className="test";
	   }
	   if(Field=="2")
	   {
	   	  fm.all('03010001').value="2";
	   	  fm.all('03010008').readOnly=true;
	   	  fm.all('03010008').className="gray2";
	   	  fm.all('03010008').value="";
	   }
}
function getMealInfo(txtMeal,txtGray1,txtGray2)
{
    //alert(fm.all(txtMeal).className);
    if(fm.all(txtMeal).value == "")
    {
    	fm.all(txtMeal).className="gray2"; 
    }
    else
    {
    	fm.all(txtMeal).className="test";
    	
    	fm.all(txtGray1).value = ""
    	fm.all(txtGray2).value = ""
    	
    	fm.all(txtGray1).className = "gray2";
    	fm.all(txtGray2).className = "gray2";
    }
}
function YesNoSmoke(smoke)
{
	fm.all('02020001').value=smoke;
	if(fm.all('02020001').value=="1")
	{
		fm.all('02020002').className="test";
		fm.all('02020006').className="test";
		fm.all('02020007').className="test";
		fm.all('02020003').className="test";
		fm.all('02020008').readOnly=true;
		fm.all('02020009').readOnly=true;
		fm.all('02020010').readOnly=true;
		fm.all('02020008').className="gray2";
		fm.all('02020009').className="gray2";
		fm.all('02020010').className="gray2";		
		fm.all('02020002').readOnly=false;
		fm.all('02020006').readOnly=false;
		fm.all('02020007').readOnly=false;
		fm.all('02020003').readOnly=false;
		fm._02020011_1.disabled=true;
		fm._02020011_2.disabled=true;
		fm._02020012_1.disabled=true;
		fm._02020012_2.disabled=true;
		fm._02020012_3.disabled=true;
		fm.all('02020008').value="";
		fm.all('02020009').value="";
		fm.all('02020010').value="";
	}
	if(fm.all('02020001').value=="2")
	{
		fm.all('02020002').className="gray2";
		fm.all('02020006').className="gray2";
		fm.all('02020007').className="gray2";
		fm.all('02020003').className="gray2";
		fm.all('02020008').className="gray2";
		fm.all('02020009').className="gray2";
		fm.all('02020010').className="gray2";
		fm.all('02020002').readOnly=true;
		fm.all('02020006').readOnly=true;
		fm.all('02020007').readOnly=true;
		fm.all('02020009').readOnly=true;
		fm.all('02020003').readOnly=true;
		fm.all('02020008').readOnly=true;
		fm.all('02020009').readOnly=true;
		fm.all('02020010').readOnly=true;
		fm._02020011_1.disabled=false;
		fm._02020011_2.disabled=false;
		fm._02020012_1.disabled=false;
		fm._02020012_2.disabled=false;
		fm._02020012_3.disabled=false;		
		fm.all('02020002').value="";
		fm.all('02020006').value="";
		fm.all('02020007').value="";
		fm.all('02020009').value="";
		fm.all('02020003').value="";
		fm.all('02020008').value="";
		fm.all('02020009').value="";
		fm.all('02020010').value="";
	}
  if(fm.all('02020001').value=="3")
	{
		fm.all('02020002').readOnly=true;
		fm.all('02020006').readOnly=true;
		fm.all('02020007').readOnly=false;
		fm.all('02020003').readOnly=false;
		fm.all('02020008').readOnly=false;
		fm.all('02020009').readOnly=false;
		fm.all('02020010').readOnly=false;
		fm.all('02020002').className="gray2";
		fm.all('02020006').className="gray2";
		fm.all('02020007').className="test";
		fm.all('02020003').className="test";
		fm.all('02020008').className="test";
		fm.all('02020009').className="test";
		fm.all('02020010').className="test";
		fm._02020011_1.disabled=false;
		fm._02020011_2.disabled=false;
		fm._02020012_1.disabled=false;
		fm._02020012_2.disabled=false;
		fm._02020012_3.disabled=false;			
		fm.all('02020002').value="";
		fm.all('02020006').value="";

	}
}
function getMediInfo(Medi)
{
	fm.all('03010002').value=Medi;
	if(fm.all('03010002').value=="1")
	{
		  fm.all('03010003').className="test";
      fm.all('03010003').readOnly=false;
	}
	if(fm.all('03010002').value=="2")
	{
		  fm.all('03010003').className="gray2";
      fm.all('03010003').readOnly=true;
	}
}
function getExeice(Exeice)
{
	fm.all('02040005').value=Exeice;
	if(fm.all('02040005').value=="1")
	{
		 fm.all('02040006').className = "test"; 
		 fm.all('02040006').readOnly=false;
		 fm.all('02040008').className = "test"; 
		 fm.all('02040008').readOnly=false;
		 fm._02040007_1.disabled=false;
     fm._02040007_2.disabled=false;
     fm._02040007_3.disabled=false;
     fm._02040007_4.disabled=false;
     fm._02040007_5.disabled=false;
     fm._02040007_6.disabled=false;
	}
	if(fm.all('02040005').value=="2")
	{
		 fm.all('02040006').className = "gray2"; 
		 fm.all('02040006').value="";
		 fm.all('02040006').readOnly=true;
		 fm.all('02040008').className = "gray2"; 
		 fm.all('02040008').value="";
		 fm.all('02040008').readOnly=true;
		 fm._02040007_1.disabled=true;
     fm._02040007_2.disabled=true;
     fm._02040007_3.disabled=true;
     fm._02040007_4.disabled=true;
     fm._02040007_5.disabled=true;
     fm._02040007_6.disabled=true;
	}
}

function getWrongAge()//验证出吸烟开始年龄不能大于本人年龄
{
	if(fm.all('BirthDate').value!=""&&fm.all('BirthDate').value!=null&&fm.all('BirthDate').value!="null")
	{
		 //alert(calAge(fm.all('BirthDate').value));
		 fm.all('txtAge').value=calAge(fm.all('BirthDate').value);
		 //alert(fm.all('02020007').value);
		 //alert(fm.all('txtAge').value);
		 if(fm.all('02020007').value > fm.all('txtAge').value )
		 {
		 	 alert("您输入的吸烟开始年龄不得大于本人年龄");
		 	 fm.all('02020007').value="";
		 	 return false;
		 }
		 else
		 {
		  	fm.all('txtAgeYear').value=fm.all('txtAge').value-fm.all('02020007').value;
		 }
	}
	else
	{
		 alert("请输入您的出生日期!");
		 return false;
	}
	
}
function getSmokeYear()
{
	//alert(fm.all('02020003').value);
	//alert(fm.all('txtAgeYear').value);
	if(fm.all('02020003').value > fm.all('txtAgeYear').value)
	{
		 alert("您输入的吸烟年限不得大于本人年龄减开始年龄!");
		 fm.all('02020003').value="";
		 return false;
	}
	else
	{
	}
}
function getForty(Field)
{
	//alert("AA "+Field);
	fm.all('01030004').value=Field;
	//alert(fm.all('01030004').value);
	if(fm.all('01030004').value=="1")
	{
		 fm._01030001_1.disabled = false; 
     fm._01030001_2.disabled = false; 
     fm._01030001_3.disabled = false; 
	}
	else
	{
		 fm._01030001_1.disabled = true; 
     fm._01030001_2.disabled = true; 
     fm._01030001_3.disabled = true; 
	}
}
function getFifty(Field)
{
	//alert(Field);
	fm.all('01040001').value=Field;
	if(fm.all('01040001').value=="1")
	{
     fm._01040002_1.disabled = false; 
     fm._01040002_2.disabled = false; 
     fm._01040002_3.disabled = false; 
	}
	else
	{
     fm._01040002_1.disabled = true; 
     fm._01040002_2.disabled = true; 
     fm._01040002_3.disabled = true;  
	}
}
function getSixty(Field)
{
	//alert(Field);
	fm.all('01050001').value=Field;
	if(fm.all('01050001').value=="1")
	{
     fm._01050002_1.disabled = false; 
     fm._01050002_2.disabled = false; 
     fm._01050002_3.disabled = false; 
	}
	else
	{
     fm._01050002_1.disabled = true; 
     fm._01050002_2.disabled = true; 
     fm._01050002_3.disabled = true;  
	}
}
function getSmokeTime(Field)
{
	//alert(Field);
	fm.all('02020011').value=Field;
	//alert(fm.all('02020011').value);
	if(fm.all('02020011').value=="1")
	{
		 fm._02020012_1.disabled = false; 
     fm._02020012_2.disabled = false; 
     fm._02020012_3.disabled = false; 
	}
	else
	{
		 fm._02020012_1.disabled = true; 
     fm._02020012_2.disabled = true; 
     fm._02020012_3.disabled = true; 
	}
}
function queryCustomerNo() 
{
  	if(fm.all('CustomerNo').value == "")	
  	{
  	  	//var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  	  	var newWindow = window.open("../sys/LDPersonQuery.html");
  	}
  	if(fm.all('CustomerNo').value != "") 
  	{
  	  	var cCustomerNo = fm.CustomerNo.value;  //客户代码
  	  	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
  	  	var arrResult = easyExecSql(strSql);
  	  	//alert(arrResult);
  	  	if (arrResult != null) {
  	  	  fm.CustomerNo.value = arrResult[0][0];
  	  	  alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
  	  	} else {
  	  	  //fm.DiseasCode.value="";
  	  	  alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
  	  	}
  	}
}

function queryCustomerNo2() {

  if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24) {
    var cCustomerNo = fm.CustomerNo.value;  //客户号码
    var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
    // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    } else {
      //fm.DiseasCode.value="";
      alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
    }
  }
}
function afterQuery(arrResult) {
	
  if(arrResult!=null) {
    fm.CustomerNo.value = arrResult[0][0];
    fm.CustomerName.value = arrResult[0][1];
  } else {
    alert("LHMainCustomerHealth.js->afterquery()出现错误");
  }
}
function getBirthDay()
{
	fm.all('08010001').value=fm.all('BirthDate').value;
}
function afterCodeSelect(codeName,Field)
{
	//alert(codeName);
	//alert(Field.value);
	if(codeName == "selelimosis")//空腹血糖
	{
		 if(fm.all('05010007').value!=""&&fm.all('05010007').value!=null&&fm.all('05010007').value!="null")
		 {
		    if(fm.all('_05010007_1').value=="mg/dl")//空腹血糖
	      {
	      	  //alert(fm.all('05010007').value);
	      	  fm.all('05010007').value=fm.all('05010007').value*18;
	      	  //alert(fm.all('05010007').value.indexOf("."));
	      	  //alert(fm.all('05010007').value.indexOf(".")+2);alert(fm.all('05010007').value);alert(parseInt((fm.all('05010007').value.indexOf(".")+3)));
	      	  fm.all('05010007').value=fm.all('05010007').value.substring(0,parseInt((fm.all('05010007').value.indexOf(".")+3)));
	      	  return false;
	      }
	      else
	      {
	      	  fm.all('05010007').value=fm.all('05010007').value/18;
	      	  fm.all('05010007').value=fm.all('05010007').value.substring(0,parseInt((fm.all('05010007').value.indexOf(".")+3)));
	      	  return false;
	      }
	   }
	}
	if(codeName == "selecholes")//总胆固醇
	{
		 //fm.all('05010008').value=fm.all('_05010008_1').value;
		 if(fm.all('05010008').value!=""&&fm.all('05010008').value!=null&&fm.all('05010008').value!="null")
		 {
		    if(fm.all('_05010008_1').value=="mg/dl")//总胆固醇
	      {
	      	  //alert(fm.all('05010007').value);
	      	  fm.all('05010008').value=fm.all('05010008').value*38.46;
	      	  fm.all('05010008').value=fm.all('05010008').value.substring(0,parseInt((fm.all('05010008').value.indexOf(".")+3)));
	      	  return false;
	      }
	      else
	      {
	      	  fm.all('05010008').value=fm.all('05010008').value/38.46;
	      	  fm.all('05010008').value=fm.all('05010008').value.substring(0,parseInt((fm.all('05010008').value.indexOf(".")+3)));
	      	  return false;
	      }
	   }
	}  
	if(codeName == "seleglycer")//甘油三脂
	{
		   //fm.all('05010009').value=fm.all('_05010009_1').value;
		 if(fm.all('05010009').value!=""&&fm.all('05010009').value!=null&&fm.all('05010009').value!="null")
		 {
		    if(fm.all('_05010009_1').value=="mg/dl")//甘油三脂
	      {
	      	  //alert(fm.all('05010007').value);
	      	  fm.all('05010009').value=fm.all('05010009').value*90.91;
	      	  fm.all('05010009').value=fm.all('05010009').value.substring(0,parseInt((fm.all('05010009').value.indexOf(".")+3)));
	      	  return false;
	      }
	      else
	      {
	      	  fm.all('05010009').value=fm.all('05010009').value/90.91;
	      	  fm.all('05010009').value=fm.all('05010009').value.substring(0,parseInt((fm.all('05010009').value.indexOf(".")+3)));
	      	  return false;
	      }
	   }
	}
	if(codeName == "selehighchol")//高密度脂蛋白胆固醇
	{
		  // fm.all('05010010').value=fm.all('_05010010_1').value;
		 if(fm.all('05010010').value!=""&&fm.all('05010010').value!=null&&fm.all('05010010').value!="null")
		 {
		    if(fm.all('_05010010_1').value=="mg/dl")//高密度脂蛋白胆固醇
	      {
	      	  //alert(fm.all('05010007').value);
	      	  fm.all('05010010').value=fm.all('05010010').value*38.46;
	      	  fm.all('05010010').value=fm.all('05010010').value.substring(0,parseInt((fm.all('05010010').value.indexOf(".")+3)));
	      	  return false;
	      }
	      else
	      {
	      	  fm.all('05010010').value=fm.all('05010010').value/38.46;
	      	  fm.all('05010010').value=fm.all('05010010').value.substring(0,parseInt((fm.all('05010010').value.indexOf(".")+3)));
	      	  return false;
	      }
	   }
	}    
	if(codeName == "selechols")//极低密度脂蛋白胆固醇
	{  
		   //fm.all('05010018').value=fm.all('_05010018_1').value;
		 if(fm.all('05010018').value!=""&&fm.all('05010018').value!=null&&fm.all('05010018').value!="null")
		 {
		    if(fm.all('_05010018_1').value=="mg/dl")//极低密度脂蛋白胆固醇
	      {
	      	  //alert(fm.all('05010007').value);
	      	  fm.all('05010018').value=fm.all('05010018').value*38.46;
	      	  fm.all('05010018').value=fm.all('05010018').value.substring(0,parseInt((fm.all('05010018').value.indexOf(".")+3)));
	      	  return false;
	      }
	      else
	      {
	      	  fm.all('05010018').value=fm.all('05010018').value/38.46;
	      	  fm.all('05010018').value=fm.all('05010018').value.substring(0,parseInt((fm.all('05010018').value.indexOf(".")+3)));
	      	  return false;
	      }
	   }
	}    
	if(codeName == "selechol")//低密度脂蛋白胆固醇
	{  
		 //fm.all('05010011').value=fm.all('_05010011_1').value;
		 if(fm.all('05010011').value!=""&&fm.all('05010011').value!=null&&fm.all('05010011').value!="null")
		 {
		    if(fm.all('_05010011_1').value=="mg/dl")//低密度脂蛋白胆固醇
	      {
	      	  //alert(fm.all('05010007').value);
	      	  fm.all('05010011').value=fm.all('05010011').value*38.46;
	      	  fm.all('05010011').value=fm.all('05010011').value.substring(0,parseInt((fm.all('05010011').value.indexOf(".")+3)));
	      	  return false;
	      }
	      else
	      {
	      	  fm.all('05010011').value=fm.all('05010011').value/38.46;
	      	  fm.all('05010011').value=fm.all('05010011').value.substring(0,parseInt((fm.all('05010011').value.indexOf(".")+3)));
	      	  return false;
	      }
	   }
	}  
} 
function showHealthTest()
{
	if(fm.all('HealthTest').checked == true)
	{
		divLHQuesInputGrid.style.display="";
	}
	else
	{
	   divLHQuesInputGrid.style.display="none";	  
	   if(fm.all('05010001').value=="51"&&fm.all('05010002').value=="26"&&fm.all('05010003').value=="51"&&fm.all('05010004').value=="51"&&fm.all('05010005').value=="61"&&fm.all('05010006').value=="51"&&fm.all('05010007').value=="3"&&fm.all('05010008').value=="2"&&fm.all('05010009').value=="1"&&fm.all('05010010').value=="1"&&fm.all('05010011').value=="1"&&fm.all('05010012').value=="6"&&fm.all('05010015').value=="2"&&fm.all('05010016').value=="2"&&fm.all('05010017').value=="51"&&fm.all('05010018').value=="1"&&fm.all('05010013').value=="6"&&fm.all('05010014').value=="11")
	   //if(fm.all('05010001').value=="51")
	   {
	       fm.all('05010001').value="";//身高
         fm.all('05010002').value="";//体重
         fm.all('05010003').value="";//腰围
         fm.all('05010004').value="";//臀围
         fm.all('05010005').value="";//收缩压
         fm.all('05010006').value="";//舒张压
         fm.all('05010007').value="";//空腹血糖
         fm.all('05010008').value="";//总胆固醇
         fm.all('05010009').value="";//甘油三脂
         fm.all('05010010').value="";//高密度脂蛋白胆固醇
         fm.all('05010011').value="";//低密度脂蛋白胆固醇
         fm.all('05010012').value="";//白蛋白	
         fm.all('05010015').value="";//糖化血红蛋白
         fm.all('05010016').value="";//胰岛素Insulin
         fm.all('05010017').value="";//血红蛋白
         fm.all('05010018').value="";//极低密度脂蛋白胆固醇
         fm.all('05010013').value="";//球蛋白
         fm.all('05010014').value="";//总蛋白 
         fm.all('05010019').value="";//纤维蛋白原Fibrinogen  
         fm.all('05010020').value="";// 脂蛋白α(Lipoprotein α)   
         fm.all('05010021').value="";//前列腺特异性抗原(Prostate specific antigen)    
      }
	}
}    
           