/** 
 * 程序名称：LHCustomInHospitalInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-05-18 09:08:27
 * 创建人  ：hm
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     

//  var strSql="select distinct a.CustomerNo,b.name, a.InHospitNo, d.HospitName,a.InHospitDate,"
//  +" e.DoctName,a.InHospitMode,c.MainCureMode"
//  +" from LHCustomInHospital a, ldperson b,LHDiagno c,LDHospital d,LDDoctor e where   "
//  +" a.CustomerNo=b.CustomerNo and b.CustomerNo=c.CustomerNo and a.InHospitNo = c.InHospitNo and a.HospitCode=d.HospitCode"
//  +" and c.DoctNo=e.DoctNo "
//  +getWherePart("a.CustomerNo","CustomerNo") 
//  +getWherePart("c.CustomerNo","CustomerNo")
//  +getWherePart("b.CustomerNo","CustomerNo")
//  ;

//	turnPage.queryModal(strSql,  LHCustomInHospitalGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	      var tSel = LHCustomInHospitalGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				//alert("12")
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery0接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHCustomInHospitalGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LHCustomInHospitalGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function queryDiseas()
{
	if(fm.all('DiseasCode').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("LDDiseaseQueryInput.jsp");	  
	  }
	if(fm.all('DiseasCode').value != "")	 
	{
		var cDiseasCode = fm.DiseasCode.value;  //疾病代码	
		var strSql = "select ICDCode,ICDName from LDDisease where ICDCode='" + cDiseasCode +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.DiseasCode.value = arrResult[0][0];
	      alert("查询结果:  疾病代码:["+arrResult[0][0]+"] 疾病名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("疾病代码为:["+fm.all('DiseasCode').value+"]的疾病不存在，请确认!");
     }
	}	
}

function afterQuery00(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.DiseasCode.value = arrResult[0][0];
  }
}

function queryDiseas2()
{
	if(fm.all('DiseasCode').value != ""&& fm.all('DiseasCode').value.length==20)	 
	{
	var cDiseasCode = fm.DiseasCode.value;  //疾病代码	
	var strSql = "select ICDCode,ICDName from LDDisease where trim(ICDCode)='" + cDiseasCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.DiseasCode.value = arrResult[0][0];
      alert("查询结果:  疾病代码:["+arrResult[0][0]+"] 疾病名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("疾病代码为:["+fm.all('DiseasCode').value+"]的疾病名称不存在，请确认!");
     }
	}	
}


function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.jsp");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}


function queryCustomerNo2()
{
	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
	var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}


function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  	//var strSql="select name from ldperson  where 1=1 "+getWherePart("CustomerNo","CustomerNo");	  
  }
}

function afterCodeSelect(codeName,Field)
{

}

//基础健康信息查询
function healthQuery()
{
	//客户号码
	var str_CustomerNo = null;
	if(fm.CustomerNo.value != "")
		{str_CustomerNo = " b.CustomerNo = '"+fm.CustomerNo.value+"'";}
	else{str_CustomerNo = "1=1";}
	
	//客户姓名
	var str_CustomerName = null;
	if(fm.CustomerName.value != "")
		{str_CustomerName = " a.Name = '"+fm.CustomerName.value+"'";}
	else{str_CustomerName = "1=1";}
	
	//添加日期
	var str_AddDate = null;
	if(fm.AddDate.value != "" )
		{str_AddDate = " AddDate "+fm.AddDate_h.value+"'"+fm.AddDate.value+"'";}
	else{str_AddDate = "1=1";}
	
	//性别
	var str_Sex = null;
	if(fm.Sex.value != "" )
		{str_Sex = " a.Sex = '"+fm.Sex.value+ "'";}
	else{str_Sex = " 1=1 ";}
	
	//年龄    
	var str_Age = null;                       
	if(fm.Age.value != "")                    
	{str_Age =" Integer(( current date - a.birthday)/10000) "+fm.Age_h.value+fm.Age.value;} 
	else{str_Age = " 1=1 ";}	                 
	
	
	//舒张压    
	var str_BloodPressLow = null;               
	if(fm.BloodPressLow.value != "")            
		{str_BloodPressLow = " b.BloodPressLow "+fm.BloodPressLow_h.value+fm.BloodPressLow.value;}
	else{str_BloodPressLow = " 1=1 ";}        
	
	//收缩压    
	var str_BloodPressHigh = null;               
	if(fm.BloodPressHigh.value != "")            
		{str_BloodPressHigh = " b.BloodPressHigh "+fm.BloodPressHigh_h.value+fm.BloodPressHigh.value;}
	else{str_BloodPressHigh = " 1=1 ";}      
	
	//脉压差	
	var str_BloodPress = null;               
  if(fm.BloodPress.value != "")            
	{str_BloodPress = " b.BloodPressHigh-b.BloodPressLow "+fm.BloodPress_h.value+fm.BloodPress.value;}
  else{str_BloodPress = " 1=1 ";}

	//体重指数
	var str_AvoirdIndex = null;                      
	if(fm.AvoirdIndex.value != "")
	{str_AvoirdIndex = " b.AvoirdIndex "+fm.AvoirdIndex_h.value+fm.AvoirdIndex.value;}
	else{str_AvoirdIndex= " 1=1 ";}	                
		
//吸烟
	var str_Smoke = null;
	if(fm.Smoke.value !="")
	{str_Smoke = " b.Smoke "+fm.Smoke_h.value+fm.Smoke.value;}
  else{str_Smoke= " 1=1 ";}
	
//饮酒	
	var str_KissCup = null;               
  if(fm.KissCup.value != "")            
	{str_KissCup = " b.KissCup "+fm.KissCup_h.value+fm.KissCup.value;}
  else{str_KissCup = " 1=1 ";}	

//熬夜
	var str_SitUp = null;               
  if(fm.SitUp.value != "")            
	{str_SitUp = " b.SitUp "+fm.SitUp_h.value+fm.SitUp.value;}
  else{str_SitUp = " 1=1 ";}	

//饮食不规律
	var str_DiningNoRule = null;                       
  if(fm.DiningNoRule.value != "")                    
  {str_DiningNoRule = " b.DiningNoRule "+fm.DiningNoRule_h.value+fm.DiningNoRule.value;} 
  else{str_DiningNoRule = " 1=1 ";}	                 

//是否有家族病史
	var str_IsFamilyDisease = null;               
  if(fm.IsFamilyDisease.value == "2")                                                                           
  	{
  			str_IsFamilyDisease = " b.CustomerNo not in (select distinct c.customerno from lhcustomfamilydiseas c)  ";
 		}
 	else
 	{
 				if(fm.IsFamilyDisease.value == "1")
  			{str_IsFamilyDisease = " b.CustomerNo in (select distinct c.customerno from lhcustomfamilydiseas c) ";}
 				 else{str_IsFamilyDisease = " 1=1 ";}          
	}	

//家族病史      
		var str_FamilyDisease = null;
    if(fm.FamilyDisease.value != "")
    	{str_FamilyDisease = " c.ICDCode= '"+fm.FamilyDiseaseCode.value+"' and c.CustomerNo = b.CustomerNo ";}
    else{str_FamilyDisease = " 1=1 ";}



//		var str_ = null;
//	  if(fm..value != "")
//		{str_ = " b. "+fm._h.value+fm..value;}
//	  else{str_ = " 1=1 ";}	

//	var str_ = null;
//	if(fm..value != "")
//		{str_ = " = '"+fm..value+"'";}
//	else{str_ = " 1=1 ";}
	
	var SQL = "select distinct b.CustomerNo,a.name,a.birthday,b.AddDate,b.BloodPressLow,b.BloodPressHigh,b.AvoirdIndex,b.Smoke,b.Diningnorule,b.healthno "
			 +"from LDPerson a,LHCustomHealthStatus b,lhcustomfamilydiseas c "
			 +"where b.CustomerNo = a.CustomerNo "
//			 +" and c.CustomerNo = b.CustomerNo "
			 +" and "+str_CustomerNo
//			 +" and "+str_CustomerName
			 +" and "+str_AddDate 
			 +" and "+str_Age
			 +" and "+str_Sex
			 +" and "+str_BloodPressLow
			 +" and "+str_BloodPressHigh
			 +" and "+str_BloodPress
			 +" and "+str_AvoirdIndex
			 +" and "+str_Smoke			  
			 +" and "+str_KissCup
			 +" and "+str_SitUp					 
			 +" and "+str_DiningNoRule
		   +" and "+str_IsFamilyDisease  
		   +" and "+str_FamilyDisease
			 +" order by b.CustomerNo, b.healthno ";                          
	window.open("../healthmanage/LHHealthQueryInputOpen.jsp?SQL="+SQL);
	
}

//客户就诊信息查询
function diagnoseQuery()
{
		
//就诊时间		
		var str_InHospitalDate = null;                      
		if(fm.InHospitalDate.value != "")                   
		{str_InHospitalDate = " a.InHospitDate "+fm.InHospitalDate_h.value+" '"+fm.InHospitalDate.value+"'";}
		else{str_InHospitalDate = " 1=1 ";}	 
		
//医疗机构	
		var str_HospitCode = null;                
		if(fm.HospitCode.value != "")             
			{str_HospitCode = " a.HospitCode= '"+fm.HospitCode.value+"'";} 
		else{str_HospitCode = " 1=1 ";}

//就诊方式
		var str_InHospitModeCode = null;                
		if(fm.InHospitModeCode.value != "")             
			{str_InHospitModeCode = " a.InHospitMode= '"+fm.InHospitModeCode.value+"'";} 
		else{str_InHospitModeCode = " 1=1 ";}

//治疗方式
		var str_MainCureModeCode = null;                
		if(fm.MainCureModeCode.value != "")             
			{str_MainCureModeCode = " e.MainCureMode= '"+fm.MainCureModeCode.value+"'";} 
		else{str_MainCureModeCode = " 1=1 ";}
		
//疾病治疗转归
		var str_CureEffect = null;                
		if(fm.CureEffect.value != "")             
			{str_CureEffect = " e.CureEffect= '"+fm.CureEffectCode.value+"'";} 
		else{str_CureEffect = " 1=1 ";}

//住院天数
			var str_InHospitalDays = null;                      
      if(fm.InHospitalDays.value != "")                   
      {str_InHospitalDays = " a.InHospitalDays "+fm.InHospitalDays_h.value+fm.InHospitalDays.value;}
      else{str_InHospitalDays = " 1=1 ";}	                

//诊断疾病名称
		var str_ICDName = null;               
		if(fm.ICDName.value != "")            
			{str_ICDName = " e.ICDCode= '"+fm.ICDCode.value+"'";}
		else{str_ICDName = " 1=1 ";}          
		
//检查费用  
		var str_testfeeamount = null;                       
    if(fm.testfeeamount.value != "")                    
    {str_testfeeamount = " (select sum(testfeeamount) from LHCustomTest g where g.Customerno = a.Customerno and g.inhospitno = a.InhospitNo) "+fm.testfeeamount_h.value+fm.testfeeamount.value;} 
    else{str_testfeeamount = " 1=1 ";}	                 
		                      
//手术费用
		var str_OPSFeeAmount = null;                       
    if(fm.OPSFeeAmount.value != "")                    
    {str_OPSFeeAmount = " (select sum(OPSFeeAmount) from LHCustomOPS h where h.Customerno = a.Customerno and h.inhospitno = a.InhospitNo) "+fm.OPSFeeAmount_h.value+fm.OPSFeeAmount.value;} 
    else{str_OPSFeeAmount = " 1=1 ";}

//其他治疗费用
		var str_CustomOtherCure = null;                       
    if(fm.CustomOtherCure.value != "")                    
    {str_CustomOtherCure = " (select sum(OtherFeeAmount) from LHCustomOtherCure j where j.Customerno = a.Customerno and j.inhospitno = a.InhospitNo) "+fm.CustomOtherCure_h.value+fm.CustomOtherCure.value;} 
    else{str_CustomOtherCure = " 1=1 ";}

//总费用
		var str_FeeAmount = null;                       
    if(fm.FeeAmount.value != "")                    
    {str_FeeAmount = " (select sum(FeeAmount) from LHFeeInfo k where k.Customerno = a.Customerno and k.inhospitno = a.InhospitNo) "+fm.FeeAmount_h.value+fm.FeeAmount.value;} 
    else{str_FeeAmount = " 1=1 ";}



		var SQL = "select distinct a.Customerno,b.name,a.InHospitDate,(select c.hospitname from ldhospital c where c.Hospitcode= a.Hospitcode), "
						 +" (select d.codename from ldcode d where d.codetype='hminhospitalmode' and d.code = a.inhospitmode),"
						 +" (select d.codename from ldcode d where d.codetype='maincuremode' and d.code = e.Maincuremode ),	"
						 +" (select d.codename from ldcode d where d.codetype='diagnosecureeffect' and d.code = e.CureEffect),	"
						 +" a.Inhospitaldays,"
						 +" '', "
//						 +" (select f.ICDName from lddisease f where e.ICDCode = f.ICDCode ), "//若不想同一人有多项疾病显示多行，修改此行
						 +" (select sum(testfeeamount) from LHCustomTest g where g.Customerno = a.Customerno and g.Inhospitno = a.InhospitNo), "
						 +" (select sum(OPSFeeAmount) from LHCustomOPS h where h.Customerno = a.Customerno and h.Inhospitno = a.InhospitNo), "
						 +" (select sum(OtherFeeAmount) from LHCustomOtherCure j where j.Customerno = a.Customerno and j.Inhospitno = a.InhospitNo), "
						 +" (select sum(FeeAmount) from LHFeeInfo k where k.Customerno = a.Customerno and k.inhospitno = a.InhospitNo), "
						 +" a.Inhospitno "
						 +"	from LHCustomInHospital a,LDPerson b,LHDiagno e,lddisease f "
						 +" where b.CustomerNo = a.CustomerNo "
						 +" and   a.CustomerNo = e.CustomerNo "
						 +" and   a.InhospitNo = e.Inhospitno "
						 +" and "+str_InHospitalDate
						 +" and "+str_HospitCode
						 +" and "+str_InHospitModeCode
						 +" and "+str_MainCureModeCode
						 +" and "+str_CureEffect
						 +" and "+str_InHospitalDays
						 +" and "+str_ICDName
						 +" and "+str_testfeeamount
						 +" and "+str_OPSFeeAmount
						 +" and "+str_CustomOtherCure
						 +" and "+str_FeeAmount
					 
						 +" order by a.Customerno "
						 ;   
//						 alert(SQL);              
		window.open("../healthmanage/LHDiagnoseQueryInputOpen.jsp?SQL="+SQL);
}


function testQuery()
{
//体检时间		                                                                                              
		var str_TestDate = null;                                                                          
		if(fm.TestDate.value != "")                                                                       
		{str_TestDate = " a.TestDate "+fm.TestDate_h.value+" '"+fm.TestDate.value+"'";}   
		else{str_TestDate = " 1=1 ";}	                                                                    

//体检医疗机构
	var str_TestHospitName = null;                
	if(fm.TestHospitName.value != "")             
		{str_TestHospitName = " c.HospitCode = '"+fm.TestHospitCode.value+"'";}
	else{str_TestHospitName = " 1=1 ";}           
	
//体检方式
  var str_TestMode = null;
  if(fm.TestModeCode.value != "")
  	{str_TestMode = " c.InHospitMode = '"+fm.TestModeCode.value+"'";}
  else{str_TestMode = " 1=1 ";}	

//体检费用	
	  var str_TestAllFeeAmount = null;                       
    if(fm.TestAllFeeAmount.value != "")                    
    {str_TestAllFeeAmount = " (select g.Feeamount from LHFeeInfo g where g.Customerno = a.CustomerNo and g.Inhospitno = a.Inhospitno) "+fm.TestAllFeeAmount_h.value+fm.TestAllFeeAmount.value;} 
    else{str_TestAllFeeAmount = " 1=1 ";}

	
	var SQL =  " select distinct a.Customerno,b.name,a.TestDate,(select d.HospitName from LDHospital d where c.HospitCode = d.HospitCode),  "
						+" (select f.codename from ldcode f where f.codetype = 'inhospitmode' and f.code = c.inhospitmode ), "
						+" (select g.Feeamount from LHFeeInfo g where g.Customerno = a.CustomerNo and g.Inhospitno = a.Inhospitno),a.inhospitno "
						+" from LHCustomtest a,LDPerson b,LHCustomInHospital c,LDHospital d "
						+" where b.CustomerNo = a.CustomerNo " 
						+" and   c.Inhospitmode in ('31','32') "
						+" and   a.CustomerNo = c.CustomerNo "
	    			+" and   a.InHospitNo = c.InHospitNo "
	    			+" and "+str_TestDate
						+" and "+str_TestHospitName
						+" and "+str_TestAllFeeAmount	
						+" and "+str_TestMode
						+" order by a.Customerno"
					;
					
	window.open("../healthmanage/LHTestQueryInputOpen.jsp?SQL="+SQL);
}

//function mainQuery()
//{
///*************************************************************	
//	基础健康信息查询
// ************************************************************/
// 
// 
//	//客户号码
//	var str_CustomerNo = null;
//	if(fm.CustomerNo.value != "")
//		{str_CustomerNo = " b.CustomerNo = '"+fm.CustomerNo.value+"'";}
//	else{str_CustomerNo = "1=1";}
//	
//	//客户姓名
//	var str_CustomerName = null;
//	if(fm.CustomerName.value != "")
//		{str_CustomerName = " a.Name = '"+fm.CustomerName.value+"'";}
//	else{str_CustomerName = "1=1";}
//	
//	//添加日期
//	var str_AddDate = null;
//	if(fm.AddDate.value != "" )
//		{str_AddDate = " AddDate "+fm.AddDate_h.value+"'"+fm.AddDate.value+"'";}
//	else{str_AddDate = "1=1";}
//	
//	//性别
//	var str_Sex = null;
//	if(fm.Sex.value != "" )
//		{str_Sex = " a.Sex = '"+fm.Sex.value+ "'";}
//	else{str_Sex = " 1=1 ";}
//	
//	//年龄    
//	var str_Age = null;                       
//	if(fm.Age.value != "")                    
//	{str_Age =" Integer(( current date - a.birthday)/10000) "+fm.Age_h.value+fm.Age.value;} 
//	else{str_Age = " 1=1 ";}	                 
//	
//	
//	//舒张压    
//	var str_BloodPressLow = null;               
//	if(fm.BloodPressLow.value != "")            
//		{str_BloodPressLow = " b.BloodPressLow "+fm.BloodPressLow_h.value+fm.BloodPressLow.value;}
//	else{str_BloodPressLow = " 1=1 ";}        
//	
//	//收缩压    
//	var str_BloodPressHigh = null;               
//	if(fm.BloodPressHigh.value != "")            
//		{str_BloodPressHigh = " b.BloodPressHigh "+fm.BloodPressHigh_h.value+fm.BloodPressHigh.value;}
//	else{str_BloodPressHigh = " 1=1 ";}      
//	
//	//脉压差	
//	var str_BloodPress = null;               
//  if(fm.BloodPress.value != "")            
//	{str_BloodPress = " b.BloodPressHigh-b.BloodPressLow "+fm.BloodPress_h.value+fm.BloodPress.value;}
//  else{str_BloodPress = " 1=1 ";}
//
//	//体重指数
//	var str_AvoirdIndex = null;                      
//	if(fm.AvoirdIndex.value != "")
//	{str_AvoirdIndex = " b.AvoirdIndex "+fm.AvoirdIndex_h.value+fm.AvoirdIndex.value;}
//	else{str_AvoirdIndex= " 1=1 ";}	                
//		
////吸烟
//	var str_Smoke = null;
//	if(fm.Smoke.value !="")
//	{str_Smoke = " b.Smoke "+fm.Smoke_h.value+fm.Smoke.value;}
//  else{str_Smoke= " 1=1 ";}
//	
////饮酒	
//	var str_KissCup = null;               
//  if(fm.KissCup.value != "")            
//	{str_KissCup = " b.KissCup "+fm.KissCup_h.value+fm.KissCup.value;}
//  else{str_KissCup = " 1=1 ";}	
//
////熬夜
//	var str_SitUp = null;               
//  if(fm.SitUp.value != "")            
//	{str_SitUp = " b.SitUp "+fm.SitUp_h.value+fm.SitUp.value;}
//  else{str_SitUp = " 1=1 ";}	
//
////饮食不规律
//	var str_DiningNoRule = null;                       
//  if(fm.DiningNoRule.value != "")                    
//  {str_DiningNoRule = " b.DiningNoRule "+fm.DiningNoRule_h.value+fm.DiningNoRule.value;} 
//  else{str_DiningNoRule = " 1=1 ";}	                 
//
////是否有家族病史
//	var str_IsFamilyDisease = null;               
//	if(fm.IsFamilyDisease.value == "1")            
//  	{str_IsFamilyDisease = " c.icdcode is not null ";}
//  if(fm.IsFamilyDisease.value == "2")                                 
//  	{str_IsFamilyDisease = " c.icdcode is  null ";} 
//  else{str_IsFamilyDisease = " 1=1 ";}          
//
////家族病史      
//		var str_FamilyDisease = null;                 
//    if(fm.FamilyDisease.value != "")              
//    	{str_FamilyDisease = " c.= '"+fm.FamilyDisease.value+"'";} 
//    else{str_FamilyDisease = " 1=1 ";}         
//    
//    
//	
///*************************************************
//				客户就诊信息查询                        
// ************************************************/	                      
//		                    
////就诊时间		
//		var str_InHospitalDate = null;                      
//		if(fm.InHospitalDate.value != "")                   
//		{str_InHospitalDate = " d.InHospitDate "+fm.InHospitalDate_h.value+" '"+fm.InHospitalDate.value+"'";}
//		else{str_InHospitalDate = " 1=1 ";}	                      
//		
////医疗机构	
//		var str_HospitCode = null;                
//		if(fm.HospitCode.value != "")             
//			{str_HospitCode = " d.HospitCode= '"+fm.HospitCode.value+"'";} 
//		else{str_HospitCode = " 1=1 ";}
//
////就诊方式
//		var str_InHospitModeCode = null;                
//		if(fm.InHospitModeCode.value != "")             
//			{str_InHospitModeCode = " d.InHospitMode= '"+fm.InHospitModeCode.value+"'";} 
//		else{str_InHospitModeCode = " 1=1 ";}
//
////治疗方式
//		var str_MainCureModeCode = null;                
//		if(fm.MainCureModeCode.value != "")             
//			{str_MainCureModeCode = " e.MainCureMode= '"+fm.MainCureModeCode.value+"'";} 
//		else{str_MainCureModeCode = " 1=1 ";}
//		
////疾病治疗转归
//		var str_CureEffect = null;                
//		if(fm.CureEffect.value != "")             
//			{str_CureEffect = " e.MainCureMode= '"+fm.CureEffectCode.value+"'";} 
//		else{str_CureEffect = " 1=1 ";}
//
////住院天数
//			var str_InHospitalDays = null;                      
//      if(fm.InHospitalDays.value != "")                   
//      {str_InHospitalDays = " d.InHospitalDays "+fm.InHospitalDays_h.value+fm.InHospitalDays.value;}
//      else{str_InHospitalDays = " 1=1 ";}	                
//
////诊断疾病名称
//		var str_ICDName = null;               
//		if(fm.ICDName.value != "")            
//			{str_ICDName = " e.ICDCode= '"+fm.ICDCode.value+"'";}
//		else{str_ICDName = " 1=1 ";}          
//		
////检查费用  
//		var str_testfeeamount = null;                       
//    if(fm.testfeeamount.value != "")                    
//    {str_testfeeamount = " (select sum(testfeeamount) from LHCustomTest g where g.Customerno = d.Customerno and g.inhospitno = d.InhospitNo) "+fm.testfeeamount_h.value+fm.testfeeamount.value;} 
//    else{str_testfeeamount = " 1=1 ";}	                 
//		                      
////手术费用
//		var str_OPSFeeAmount = null;                       
//    if(fm.OPSFeeAmount.value != "")                    
//    {str_OPSFeeAmount = " (select sum(OPSFeeAmount) from LHCustomOPS m where m.Customerno = d.Customerno and m.inhospitno = d.InhospitNo) "+fm.OPSFeeAmount_h.value+fm.OPSFeeAmount.value;} 
//    else{str_OPSFeeAmount = " 1=1 ";}
//
////其他治疗费用
//		var str_CustomOtherCure = null;                       
//    if(fm.CustomOtherCure.value != "")                    
//    {str_CustomOtherCure = " (select sum(OtherFeeAmount) from LHCustomOtherCure j where j.Customerno = d.Customerno and j.inhospitno = d.InhospitNo) "+fm.CustomOtherCure_h.value+fm.CustomOtherCure.value;} 
//    else{str_CustomOtherCure = " 1=1 ";}
//
////总费用
//		var str_FeeAmount = null;                       
//    if(fm.FeeAmount.value != "")                    
//    {str_FeeAmount = " (select sum(FeeAmount) from LHFeeInfo k where k.Customerno = d.Customerno and k.inhospitno = d.InhospitNo) "+fm.FeeAmount_h.value+fm.FeeAmount.value;} 
//    else{str_FeeAmount = " 1=1 ";}
//	
//	
//	
//	
//	var SQL = " select distinct b.CustomerNo,a.name,a.birthday,b.AddDate "
//			     +" from "
//			     +" LDPerson a,LHCustomHealthStatus b,lhcustomfamilydiseas c,LHCustomInHospital d,"
//			     +" LHDiagno e,lddisease f,LHCustomtest g,LDHospital h,LHCustomOtherCure j,LHFeeInfo k,LHCustomOPS m "
//			     +" where b.CustomerNo = a.CustomerNo "
//			     +" and b.CustomerNo = c.CustomerNo "
//			     +" and b.CustomerNo = d.CustomerNo "
//			     +" and b.CustomerNo = e.CustomerNo "
//			     +" and b.CustomerNo = g.CustomerNo "
//			     +" and b.CustomerNo = j.CustomerNo "
//			     +" and b.CustomerNo = k.CustomerNo "
//			     +" and b.CustomerNo = m.CustomerNo "
//			     +" and "+str_CustomerNo
//    			 +" and "+str_CustomerName
//					 +" and "+str_AddDate 
//					 +" and "+str_Age
//					 +" and "+str_Sex
//					 +" and "+str_BloodPressLow
//					 +" and "+str_BloodPressHigh
//					 +" and "+str_BloodPress
//					 +" and "+str_AvoirdIndex
//					 +" and "+str_Smoke			  
//					 +" and "+str_KissCup			 			    
//					 +" and "+str_SitUp					 
//					 +" and "+str_DiningNoRule
//					 +" and "+str_InHospitalDate
//					 +" and "+str_HospitCode
//					 +" and "+str_InHospitModeCode
//					 +" and "+str_MainCureModeCode
//					 +" and "+str_CureEffect
//					 +" and "+str_InHospitalDays
//					 +" and "+str_ICDName
//					 +" and "+str_testfeeamount
//					 +" and "+str_OPSFeeAmount
//					 +" and "+str_CustomOtherCure
//					 +" and "+str_FeeAmount     
//					 
//		window.open("../healthmanage/LHMainQueryInputOpen.jsp?SQL="+SQL);
//			     
//}


function inputClear()
{
		fm.all('CustomerNo').value = "";
		fm.all('CustomerName').value = "";
		fm.all('AddDate_h').value = "";
		fm.all('AddDate_sp').value = "";
		fm.all('AddDate').value = "";
		fm.all('Age_h').value = "";
		fm.all('Age_sp').value = "";
		fm.all('Age').value = "";
		fm.all('Sex').value = "";
		fm.all('Sexch').value = "";
		fm.all('BloodPressLow_h').value = "";
		fm.all('BloodPressLow_sp').value = "";
		fm.all('BloodPressLow').value = "";
		fm.all('BloodPressHigh_h').value = "";
		fm.all('BloodPressHigh_sp').value = "";
		fm.all('BloodPressHigh').value = "";
		fm.all('BloodPress_h').value = "";
		fm.all('BloodPress_sp').value = "";
		fm.all('BloodPress').value = "";
		fm.all('AvoirdIndex_h').value = "";
		fm.all('AvoirdIndex_sp').value = "";
		fm.all('AvoirdIndex').value = "";
		fm.all('Smoke_h').value = "";
		fm.all('Smoke_sp').value = "";
		fm.all('Smoke').value = "";
		fm.all('KissCup_h').value = "";
		fm.all('KissCup_sp').value = "";
		fm.all('KissCup').value = "";
		fm.all('SitUp_h').value = "";
		fm.all('SitUp_sp').value = "";
		fm.all('SitUp').value = "";
		fm.all('DiningNoRule_h').value = "";
		fm.all('DiningNoRule_sp').value = "";
		fm.all('DiningNoRule').value = "";
		fm.all('IsFamilyDisease').value = "";
		fm.all('IsFamilyDisease_ch').value = "";
		fm.all('FamilyDiseaseCode').value = "";
		fm.all('FamilyDisease').value = "";
		fm.all('InHospitalDate_h').value = "";
		fm.all('InHospitalDate_sp').value = "";
		fm.all('InHospitalDate').value = "";
		fm.all('HospitCode').value = "";
		fm.all('HospitName').value = "";
		fm.all('InHospitModeCode').value = "";
		fm.all('InHospitMode').value = "";
		fm.all('MainCureModeCode').value = "";
		fm.all('MainCureMode_ch').value = "";
		fm.all('CureEffectCode').value = "";
		fm.all('CureEffect').value = "";
		fm.all('InHospitalDays_h').value = "";
		fm.all('InHospitalDays_sp').value = "";
		fm.all('InHospitalDays').value = "";
		fm.all('ICDCode').value = "";
		fm.all('ICDName').value = "";
		fm.all('testfeeamount_h').value = "";
		fm.all('testfeeamount_sp').value = "";
		fm.all('testfeeamount').value = "";
		fm.all('OPSFeeAmount_h').value = "";
		fm.all('OPSFeeAmount_sp').value = "";
		fm.all('OPSFeeAmount').value = "";
		fm.all('CustomOtherCure_h').value = "";
		fm.all('CustomOtherCure_sp').value = "";
		fm.all('CustomOtherCure').value = "";
		fm.all('FeeAmount_h').value = "";
		fm.all('FeeAmount_sp').value = "";
		fm.all('FeeAmount').value = "";
		fm.all('TestDate_h').value = "";
		fm.all('TestDate_sp').value = "";
		fm.all('TestDate').value = "";
		fm.all('TestHospitCode').value = "";
		fm.all('TestHospitName').value = "";
		fm.all('TestAllFeeAmount_h').value = "";
		fm.all('TestAllFeeAmount_sp').value = "";
		fm.all('TestAllFeeAmount').value = "";
//		fm.all('IsToHospital').value = "";
//		fm.all('IsToHospital_ch').value = "";		
//		fm.all('IsToTest').value = "";
		fm.all('TestMode').value = "";
		fm.all('TestModeCode').value = "";
//		fm.all('ItemFeeRate_h').value = "";
//		fm.all('ItemFeeRate_sp').value = "";
//    fm.all('ItemFeeRate').value = "";
		
}

function edorQuery()
{
	window.open("LHAllPBqQueryMain.jsp");	
}