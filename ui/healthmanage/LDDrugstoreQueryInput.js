/** 
 * 程序名称：LDDrugstoreInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-01-15 11:16:51
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
/*	
	var strSql = "select a.GetNoticeNo, a.otherno, b.name, a.PayDate, a.SumDuePayMoney, a.bankcode, a.bankaccno, a.accname "
	           + " from LJSPay a, ldperson b where a.appntno=b.customerno and OtherNoType='2' "
          	 + getWherePart("a.GetNoticeNo", "GetNoticeNo2")
          	 + getWherePart("a.OtherNo", "OtherNo")
          	 + getWherePart("a.BankCode", "BankCode")
          	 + getWherePart("a.SumDuePayMoney", "SumDuePayMoney");
  
  if (fm.AppntName.value != "") strSql = strSql + " and a.appntno in (select c.customerno from ldperson c where name='" + fm.AppntName.value + "')";
  if (fm.PrtNo.value != "") strSql = strSql + " and a.otherno in (select polno from lcpol where prtno='" + fm.PrtNo.value + "')";
*/  			    
	var strSql = "select DrugStoreCode,DrugstoreName,address,Phone,AreaCode,ZipCode,MakeDate,MakeTime from LDDrugstore where 1=1 "
    + getWherePart("DrugStoreCode", "DrugStoreCode")
    + getWherePart("DrugstoreName", "DrugstoreName")
    + getWherePart("address", "address")
    +" and managecom like '"+manageCom+"%%'"
					    /*+ getWherePart("FixFlag", "FixFlag")
					    + getWherePart("CommunFixFlag", "CommunFixFlag")
					    + getWherePart("AreaCode", "AreaCode")
					    + getWherePart("EconomElemenCode", "EconomElemenCode")
					    + getWherePart("BusiScope", "BusiScope")
					    + getWherePart("StartDate", "StartDate")
					    + getWherePart("ZipCode", "ZipCode")
					    + getWherePart("Phone", "Phone")
					    + getWherePart("WebAddress", "WebAddress")
					    + getWherePart("Fax", "Fax")
					    + getWherePart("DrugstoreLicencNo", "DrugstoreLicencNo")
					    + getWherePart("bankCode", "bankCode")
					    + getWherePart("AccName", "AccName")
					    + getWherePart("bankAccNO", "bankAccNO")
					    + getWherePart("SatrapName", "SatrapName")
					    + getWherePart("Linkman", "Linkman")
					    + getWherePart("LastModiDate", "LastModiDate")*/
  ;
  //alert(strSql);
	turnPage.queryModal(strSql, LDDrugstoreGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LDDrugstoreGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
		return false;
	}
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LDDrugstoreGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LDDrugstoreGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
function getSuperDrugStoreCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select DrugStoreCode,DrugstoreName from LDDrugstore ";
    fm.all("SuperDrugStoreCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}