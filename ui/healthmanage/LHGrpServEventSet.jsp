<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHGrpServEventSet.jsp
//程序功能：
//创建日期：2006-07-10 10:21:05
//创建人  ：CrtHtml程序创建
%>
<%@page contentType="text/html;charset=GBK" %>
<%
	 String GrpServlevel = request.getParameter("GrpServlevel");
	 String GrpContNo = request.getParameter("GrpContNo");
	 String flag = request.getParameter("flag");
%>
<head >
  
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
	<SCRIPT src="LHGrpServEventSet.js"></SCRIPT>
	<%@include file="LHGrpServEventSetInit.jsp"%>
</head>
<body  onload="initForm();QueryInit();" >
  <form method=post name=fm target="fraSubmit">
    
    <%@include file="../common/jsp/InputButton.jsp"%>

	<div style="display:''" id='submenu1'>  
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 团体服务计划表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHGrpServPlan1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
			    <TD  class= title>
			      团体客户号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpCustomerNo verify="团体客户号|NOTNULL&len<=20" readonly>
			    </TD>
			    <TD  class= title>
			      团体客户名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpName verify="团体客户名称|NOTNULL&len<=300" readonly>
			    </TD>
				<TD  class= title>
			      团体保单号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpContNo verify="团体保单号|NOTNULL&len<=20" readonly>
			    </TD>
		  	</TR>
		  	<TR  class= common>
		  		<TD  class= title>
			      保单所述机构标识
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ComID verify="保单所述机构标识|NOTNULL&len<=10" readonly>
			    </TD>
			    <TD  class= title>
			      团体服务计划名称
			    </TD>
			    <TD  class= input colSpan=3>
			      <Input class= 'common' style="width:480px" name=ServPlan readonly>
			    </TD>
			</TR>
			<TR  class= common>
			    <TD  class= title>
			      服务计划起始时间
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=StartDate verify="服务计划起始时间|DATE&NOTNULL">
			    </TD>
			    <TD  class= title>
			      服务计划终止时间
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=EndDate verify="服务计划终止时间|DATE&NOTNULL">
			    </TD>
			    <TD  class= title>
			      健管保费（元）
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServPrem  readonly>
			    </TD>
			</TR>
		</table>
    </Div>
    
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHGrpServPlanGrid);">
    		</td>
    		 <td class= titleImg>
        		 团体服务计划信息列表
       		 </td>   
       		   		 		 
    	</tr>
    </table>
    
	<Div id="divLHGrpServPlanGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHGrpServPlanGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
       <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHGrpServItemGrid);">
    		</td>
    		 <td class= titleImg>
        		 该档次下服务项目信息
       		 </td>   
       		   		 		 
    	</tr>
    </table>
   	<Div id="divLHGrpServItemGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHGrpServItemGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
		<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</Div>
   
	<table  class= common align='left' >
  		<TR class= common>
  			<TD class=title>  &nbsp;&nbsp&nbsp;&nbsp					
		  	 	<Input value="集体服务事件设置" type=button class=cssbutton onclick="CreatEventManually();">
		  	 	<Input value="生成个人服务事件" type=button class=cssbutton onclick="CreatPersonCase();">
		  	</TD>
		</TR>
	</table>
   
</Div>
<input type=hidden name=GrpServPlanNo>
<input type=hidden name=MakeDate>
<input type=hidden name=MakeTime>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script>
	function QueryInit()
	{
		if("<%=flag%>" == "UCONT")
		{
			UCONTQuery();
		}
		else
		{
			passQuery();
		}
	}
	
	function UCONTQuery()
	{
		var GrpContNo = "<%=GrpContNo%>";
		var ServLevel = "<%=GrpServlevel%>";
		if(GrpContNo == "" || GrpContNo == null)
		{
			alert("参数有误！");return false;	
		}
		var arrResult = new Array();
		//此函数中的GrpContNo是 GrpServPlanNo
		var strSql = "select GrpContNo,GrpCustomerNo,GrpName,ServPlanLevel,ServPrem,ComID,"
	           +" StartDate,EndDate,MakeDate,MakeTime ,GrpServplanno from LHGrpServPlan "
	           +" where GrpServPlanNo ='"+GrpContNo+"'"
	           ;
	    arrResult = easyExecSql(strSql);
	    
		fm.all('GrpServPlanNo').value= arrResult[0][10];
        fm.all('GrpContNo').value= arrResult[0][0];
        fm.all('GrpCustomerNo').value= arrResult[0][1];
        fm.all('GrpName').value= arrResult[0][2];
    //    fm.all('ServPlanCode').value= easyExecSql("select ServPlancode from LHGrpServPlan where GrpServplanno = '"+fm.all('GrpServplanno').value+"' "); 
        fm.all('ServPlan').value= easyExecSql("select ServPlanname from LHGrpServPlan where GrpServplanno = '"+fm.all('GrpServplanno').value+"' ");
        //fm.all('ComID').value= arrResult[0][5];
        fm.all('ComID').value= easyExecSql("select showname from ldcom where  sign = '1' and length(trim(comcode)) = 4 and  Comcode = '"+ arrResult[0][5]+"'");
        fm.all('StartDate').value= arrResult[0][6];
        fm.all('EndDate').value= arrResult[0][7];
        fm.all('ServPrem').value= easyExecSql("select sum(ServPrem) from LHGrpServPlan where GrpServplanno = '"+fm.all('GrpServplanno').value+"' "); 
        fm.all('MakeDate').value= arrResult[0][8];
        fm.all('MakeTime').value= arrResult[0][9];
        
        
        var sql_g = " select ServPlanLevel,ServPrem,GrpServPlanNo,MakeDate,MakeTime,Customnumindate,Customnumactrually,Leveldescription"    
							+" from LHGrpServPlan where GrpServPlanNo = '"+fm.all('GrpServPlanNo').value+"' " 
	  						;  
		turnPage.queryModal(sql_g,LHGrpServPlanGrid); 
	    showOne();       
	}
	function passQuery()
	{ 	
		//进入此函数
		var GrpContNo = "<%=GrpContNo%>";
		var ServLevel = "<%=GrpServlevel%>";
		//alert(ServLevel);
		if(GrpContNo!='null' || GrpContNo > 0 ) 
		{
			var strSqlInit = "select appntno,grpname,grpcontno,managecom from lcgrpcont "
			+"where grpcontno = '"+GrpContNo+"'"
			;
            var arrResultInit = easyExecSql(strSqlInit);
            //alert(strSqlInit);
            fm.all('GrpCustomerNo').value = arrResultInit[0][0];
            fm.all('GrpName').value = arrResultInit[0][1];
            fm.all('GrpContNo').value = arrResultInit[0][2];
            fm.all('ComID').value = arrResultInit[0][3].substring (0,4);
//            var sql_c = "select Riskcode from lcpol where Grpcontno = '"+GrpContNo+"'";
			var sql_c = "select distinct Riskcode from lcpol where Grpcontno = '"+GrpContNo+"' and Riskcode in (select distinct riskcode from lmriskapp where  risktype2 = '5') ";
			//alert(easyExecSql(sql_c));
            fm.all('ServPlan').value = easyExecSql("select Riskname from lmriskapp where Riskcode = '"+easyExecSql(sql_c)+"'");
           
            var sql_i2 = " select distinct startdate, enddate from LHGrpServPlan where GrpContNo = '"+fm.all('GrpContNo').value+"'";
            var arr_i2 = easyExecSql(sql_i2);
            if(arr_i2 != "" && arr_i2 != "null" && arr_i2 != null)
            {
            	fm.all('StartDate').value = arr_i2[0][0];
	            fm.all('EndDate').value = arr_i2[0][1];
	            fm.all('ServPrem').value= easyExecSql("select sum(ServPrem) from LHGrpServPlan where GrpContNo = '"+fm.all('GrpContNo').value+"' "); 
				
				var sql_g = " select ServPlanLevel,ServPrem,GrpServPlanNo,MakeDate,MakeTime,Customnumindate,Customnumactrually,Leveldescription"    
							+" from LHGrpServPlan "                                             
	  						+" where GrpContNo = '"+fm.all('GrpContNo').value+"' and ServPlanLevel = '"+ServLevel+"'"                      	                                      
	  						;  
	  					
	  			turnPage.queryModal(sql_g,LHGrpServPlanGrid); 
	  			showOne();
	  		}
		} 
		else 
		{
			alert("没有此保单信息，请确认!");      
			return false;
		}		    
	}
   </script>
</html>