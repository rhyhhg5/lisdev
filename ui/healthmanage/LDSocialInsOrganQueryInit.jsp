<%
//程序名称：LDSocialInsOrganQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-15 13:07:18
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('SIOrganCode').value = "";
    fm.all('SIOrganName').value = "";

  }
  catch(ex) {
    alert("在LDSocialInsOrganQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDSocialInsOrganGrid();  
  }
  catch(re) {
    alert("LDSocialInsOrganQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDSocialInsOrganGrid;
function initLDSocialInsOrganGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="医保机构代码";         	
    iArray[1][1]="30px";         	
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="医保机构名称";         	
    iArray[2][1]="150px";         	
    iArray[2][3]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="地址";         	
    iArray[3][1]="100px";         	
    iArray[3][3]=0;           
    
    iArray[4]=new Array();  
    iArray[4][0]="地区代码";    
    iArray[4][1]="0px";    
    iArray[4][3]=3;         
    
    iArray[5]=new Array();  
    iArray[5][0]="电话";    
    iArray[5][1]="0px";    
    iArray[5][3]=3;         
            
    iArray[6]=new Array();          
    iArray[6][0]="邮编";            
    iArray[6][1]="0px";            
    iArray[6][3]=3;              
    
    iArray[7]=new Array();          
    iArray[7][0]="MakeDate";            
    iArray[7][1]="0px";            
    iArray[7][3]=3;  
    
    iArray[8]=new Array();          
    iArray[8][0]="MakeTime";            
    iArray[8][1]="0px";            
    iArray[8][3]=3;     
            
    
    LDSocialInsOrganGrid = new MulLineEnter( "fm" , "LDSocialInsOrganGrid" ); 
    //这些属性必须在loadMulLine前

    LDSocialInsOrganGrid.mulLineCount = 0;   
    LDSocialInsOrganGrid.displayTitle = 1;
    LDSocialInsOrganGrid.hiddenPlus = 1;
    LDSocialInsOrganGrid.hiddenSubtraction = 1;
    LDSocialInsOrganGrid.canSel = 1;
    LDSocialInsOrganGrid.canChk = 0;


    LDSocialInsOrganGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDSocialInsOrganGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
