<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHCustomOPSSave.jsp
//程序功能：
//创建日期：2005-01-17 17:17:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHCustomOPSSchema tLHCustomOPSSchema   = new LHCustomOPSSchema();
  OLHCustomOPSUI tOLHCustomOPSUI   = new OLHCustomOPSUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHCustomOPSSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLHCustomOPSSchema.setOPSNo(request.getParameter("OPSNo"));
    tLHCustomOPSSchema.setMedicaItemCode(request.getParameter("MedicaItemCode"));
    tLHCustomOPSSchema.setRxSummar(request.getParameter("RxSummar"));
    tLHCustomOPSSchema.setExecutDate(request.getParameter("ExecutDate"));
    tLHCustomOPSSchema.setHospitCode(request.getParameter("HospitCode"));
    tLHCustomOPSSchema.setDoctor(request.getParameter("Doctor"));
    tLHCustomOPSSchema.setImpartFlag(request.getParameter("ImpartFlag"));
    tLHCustomOPSSchema.setImpartDate(request.getParameter("ImpartDate"));
    tLHCustomOPSSchema.setInHospitNo(request.getParameter("InHospitNo"));
    tLHCustomOPSSchema.setInHospitFee(request.getParameter("InHospitFee"));
    tLHCustomOPSSchema.setOperator(request.getParameter("Operator"));
    tLHCustomOPSSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHCustomOPSSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHCustomOPSSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHCustomOPSSchema.setModifyTime(request.getParameter("ModifyTime"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLHCustomOPSSchema);
  	tVData.add(tG);
    tOLHCustomOPSUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHCustomOPSUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
