<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LHSettingSlipQuery.jsp
//程序功能：
//创建日期：2006-06-26 09:27:55
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<% String Come=request.getParameter("Come"); %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<SCRIPT src="LHSettingSlipQuery.js"></SCRIPT>
	<%@include file="LHSettingSlipQueryInit.jsp"%>
</head>
<%
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
		<div style="display:''">
    <table>
		<tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHSettingSlipQueryGrid1);">
    	 	</td>
			<td class= titleImg>
        	 服务事件查询信息
			</td>
		</tr>
    </table>
    <Div  id= "divLHSettingSlipQueryGrid1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
				<TD  class= title>
			      保单号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ContNo  >
			    </TD>
				<TD  class= title>
                客户号码
				</TD>
				<TD  class= input>
                	<Input class= 'code' name=CustomerNo ondblclick=" showCodeList('hmldperson',[this,CustomerName],[0,1],null,fm.CustomerName.value,'Name',1);">
				</TD>
				<TD  class= title>
                客户姓名
				</TD>
				<TD  class= input>
                	<Input class= 'code' name=CustomerName ondblclick=" return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name',1);"  onkeyup=" if( event.keyCode==13 ) return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name',1);" >
				</TD>
			</TR>
			<TR  class= common>
		        <TD  class= title style="width:120">
		           服务项目代码
		        </TD>
		        <TD  class= input>
		          <Input class= 'code' name=ServItemCode  ondblclick="showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode'); ">
		        </TD>
		        <TD  class= title style="width:120">
		           服务项目名称
		        </TD>
				<TD  class= input>
		           <Input class= 'code' name=ServItemName   ondblclick="showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,'','ServItemName','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemName.value,'ServItemName');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemName.value,'ServItemName');">
		         </TD>
		        <TD class= title >
		        		 服务事件类型
		       	</TD>
				<TD  class= input>
				 <!--<Input class= 'codename' style="width:40px" name=ServCaseType value=1><Input class= 'codeno' style="width:120px"  name=ServCaseTypeName value="个人服务事件" ondblclick="showCodeList('eventtype',[this,ServCaseType],[1,0],null,null,null,'1',160);" codeClear(ServCaseTypeName,ServCaseType);">-->
				<Input class= 'codename' style="width:50px" name=ServCaseType ><Input class= 'codeno' style="width:110px"  name=ServCaseTypeName ondblclick="showCodeList('eventtype',[this,ServCaseType],[1,0],null,null,null,'1',160);" codeClear(ServCaseTypeName,ServCaseType);">
				</TD>

			</TR>
			<TR  class= common>
				<TD  class= title style="width:120">
	            服务事件编号
	         	</TD>
				<TD  class= input>
		           <Input class= 'code' name=ServCaseCode  ondblclick="showCodeList('lhservcasecode',[this,ServCaseName],[0,1],null,fm.ServCaseCode.value,'ServCaseCode','1',450);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhservcasecode',[this,ServCaseName],[0,1],null,fm.ServCaseCode.value,'ServCaseCode','1',450);"   onkeydown="return showCodeListKey('lhservcasecode',[this,ServCaseName],[0,1],null,fm.ServCaseCode.value,'ServCaseCode','1',450); ">
				</TD>
				<TD  class= title style="width:120">
		            服务事件名称
				</TD>
				<TD  class= input>
		           <Input class= 'code' name=ServCaseName   ondblclick="showCodeList('lhservcasecode',[this,ServCaseCode],[1,0],null,fm.ServCaseName.value,'ServCaseName','1',450);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhservcasecode',[this,ServCaseCode],[1,0],null,fm.ServCaseName.value,'ServCaseName','1',450);"   onkeydown="return showCodeListKey('lhservcasecode',[this,ServCaseCode],[1,0],null,fm.ServCaseName.value,'ServCaseName','1',450);">
				</TD>
				<TD class= title >
					服务事件状态
				</TD>
				<TD  class= input>
				 <!--<Input class= 'codename' style="width:40px" name=ServCaseState value=2><Input class= 'codeno' style="width:120px"  name=ServCaseStateName value="服务事件已启动" ondblclick="showCodeList('eventstate',[this,ServCaseState],[1,0],null,null,null,'1',160);" codeClear(ServCaseState,ServCaseStateName);">-->
				 	<Input class= 'codename' readonly style="width:50px" name=ServCaseState><Input class= 'codeno' readonly style="width:110px"  name=ServCaseStateName  ondblclick="showCodeList('eventstate',[this,ServCaseState],[1,0],null,null,null,'1',160);" codeClear(ServCaseState,ServCaseStateName);">
				</TD>
			</TR>
			<tr>
				<!--<TD class= title >
        		 费用结算状态
				</TD>
				<TD  class= input>
					<Input class= 'codename' style="width:40px" name=FeeSuessState ><Input class= 'codeno' style="width:120px"  name=FeeSuessStateName  ondblclick="showCodeList('feesucessstate',[this,FeeSuessState],[1,0],null,null,null,'1',160);" codeClear(FeeSuessState,FeeSuessStateName);">
				</TD>-->

				<TD class= title >保单机构</TD>
				<TD  class= input>
					<Input class= "codeno"  name=MngCom  ondblclick="return showCodeList('hmhospitalmng',[this,MngCom_ch],[1,0],null,null,null,1,180);" onkeyup="return showCodeListKey('hmhospitalmng',[this,MngCom_ch],[1,0],null,null,null,1,180);" readonly verify="管理机构|NOTNULL" ><Input class=codename  name=MngCom_ch>
				</TD>
                                <TD class=title>服务计划起始时间        </TD>
                                <TD class=input>
          <Input class='CoolDatePicker' dateFormat="Short" name=LHContStart1 verify="保单生效时间(起始)|DATE">
        </TD>
           <TD class=title align="center"> 至  </TD>
         <TD class=input>
          <Input class='CoolDatePicker' dateFormat="Short" name=LHContStart2 verify="保单生效时间(起始)|DATE">
        </TD>
        </TR>
       <TR>
        <TD class=title>服务计划结束时间        </TD>
        <TD class=input>
          <Input class='CoolDatePicker' dateFormat="Short" name=LHContEnd1 verify="保单生效时间(起始)|DATE">
          </TD>
      <TD class=title align="center"> 至  </TD>
      <TD class=input>
          <Input class='CoolDatePicker' dateFormat="Short" name=LHContEnd2 verify="保单生效时间(起始)|DATE">
        </TD>
				 <TD class= title ></TD>
				<TD  class= input></TD>
			</TR>
		</table>
	</Div>
<Div  id= "divLHServItemInfo" style= "display: 'none'">
<table  class= common align='center' >
  <TR  class= common>
     <TD  class= title>
      有无服务信息
    </TD>
    <TD  class= input>
      <Input  class=codeno name=ServItemIN CodeData= "0|^1|有^2|无" ondblClick= "showCodeListEx('',[this,ServItemIN_ch],[0,1],null,null,null,'1',160);"><Input class= 'codename' name=ServItemIN_ch style="width:109px" >
    </TD>
    <TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
    <TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
  </TR>
</table>
</div>
	<table>
		<tr>
			<td>
				<input type= button class=cssButton  style="width:130px" name=ServTrace value="查询" OnClick="showQueryInfo();">
      </td>
       <td>
       		 	<input type= button class=cssButton name=ServQieYue  style="width:130px" value="服务信息查看" OnClick="ServQieYueInfo();">
       	</td>
       	<td style="width:365px;">
		  		</td>
       		<td >
     			<input type= button class=cssButton  style="width:130px" name=Result value="返回" OnClick="ReturnResult();">
       		</td>
		</tr>
    </table>



	<Div id="divLHSettingSlipQueryGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHSettingSlipQueryGrid">
					</span>
		    	</td>
			</tr>
		</table>
		<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	</div>

	<br><br>

  <Div id="divLHQueryDetailGrid" style="display:'none'">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHQueryDetailGrid">
					</span>
		    	</td>
			</tr>
		</table>
		<Div id= "divPage2" align=center style= "display: 'none' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();">
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	  </Div>
	</div>

	<table>
		<tr>
				<td >
     		<input type= button class=cssButton name=CaseShiShi   style="width:130px" value="启动服务事件" OnClick="ServCaseRun();">
       </td>
		</tr>
     <tr style="width:50px" >
     	<td >
     		<input type= button class=cssButton name=CaseShiShi2   style="width:130px" value="服务事件实施管理" OnClick="CaseExecManage();">
       </td>

       	<td>
       		 	<input type= hidden class=cssButton name=FeeJieSuan   style="width:130px" value="费用结算信息管理" OnClick="">
       	</td>
		   </tr>
  </table>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmAction" name="fmAction">
		<!--团体服务计划号码-->
		<Input class= 'common' type=hidden  name=GrpServPlanNo >

		<!--个人服务计划号码-->
		<Input class= 'common' type=hidden   name=ServPlanNo >

		<!--服务计划类型-->
		<Input class= 'common' type=hidden name=ServPlayType >
		<Input class= 'common' type=hidden name=ManageCom >
	    <Input class= 'common' type=hidden name=Operator >
	    <Input class= 'common' type=hidden name=MakeDate >
	    <Input class= 'common' type=hidden name=MakeTime >
  	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
</html>
