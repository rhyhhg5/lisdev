<%
//程序名称：LHItemPriceFactorInputInit.jsp 
//程序功能：服务定价要素设置(初始化页面)
//创建日期：2006-04-19 11:40:30
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     String ComID = request.getParameter("ComID");
     String ServItemCode = request.getParameter("ServItemCode");
     System.out.println("#######"+ComID);
     System.out.println("XXXXXXXXX"+ServItemCode);
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ServItemCode').value ="<%=ServItemCode%>";
    fm.all('ServItemName').value ="";
    fm.all('ComIDCode').value = "<%=ComID%>";
    fm.all('ComIDName').value = "";
  }
  catch(ex)
  {
    alert("在LHItemPriceFactorInputInitInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                        
function initForm()
{
  try
  {
    initInpBox();
    getNo();
    fm.all('querybutton').disabled=true;
    initLHItemPriceFactorGrid();
    getFactorInfo();
  }
  catch(re)
  {
    alert("LHItemPriceFactorInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}  
function getNo()
{
	try
	{
		 var mulSql = "select showname from ldcom "
		            +" where  sign = '1'  and  ldcom.comcode='"+ <%=ComID%>+"'"
					      //alert(mulSql);
					      //alert(easyExecSql(mulSql));
					      var arrResult=new Array;
					      arrResult = easyExecSql(mulSql);		  
					      fm.all('ComIDName').value              = arrResult[0][0];	
		 var mulSql2 = " select   ServItemName from LHHealthServItem "
		            +" where ServItemCode='"+fm.all('ServItemCode').value+"'"
					      //alert(mulSql2);
					      //alert(easyExecSql(mulSql2));
					      var arrResult2=new Array;
					      arrResult2 = easyExecSql(mulSql2);		  
					      fm.all('ServItemName').value              = arrResult2[0][0];	
	}
	catch(ex)
	{
		alert("LHItemPriceFactorInputInit.jsp-->获得编号函数No中发生异常:初始化界面错误!");
	}
}
	
function getFactorInfo()
{
	try
	{
		 var mulSql3 = "select PriceFactorCode,PriceFactorName from LHItemPriceFactor "
		            +" where LHItemPriceFactor.ComID='"+ <%=ComID%>+"' and ServItemCode='"+fm.all('ServItemCode').value+"'"
					      //alert(mulSql3);
					     // alert(easyExecSql(mulSql3));
	   turnPage.queryModal(mulSql3, LHItemPriceFactorGrid);
	}
	catch(ex)
	{
		alert("LHItemPriceFactorInputInit.jsp-->传输入信息Info中发生异常:初始化界面错误!");
	}
}
 function initLHItemPriceFactorGrid() 
 {                            
   var iArray = new Array();                               
                                                           
   try 
   {                                                   
     iArray[0]=new Array();                                
     iArray[0][0]="序号";         		//列名               
     iArray[0][1]="30px";         		//列名               
     iArray[0][3]=0;         				//列名                 
     iArray[0][4]="";         //列名   
    
	    iArray[1]=new Array(); 
			iArray[1][0]="定价要素代码";   
			iArray[1][1]="50px";   
			iArray[1][2]=20;        
			iArray[1][3]=1;
	   
  
	    iArray[2]=new Array(); 
			iArray[2][0]="定价要素名称";   
			iArray[2][1]="50px";   
			iArray[2][2]=20;        
			iArray[2][3]=1;
		
			
			
		  
		 
	 

     
     
   LHItemPriceFactorGrid = new MulLineEnter( "fm" , "LHItemPriceFactorGrid" );                         
    //这些属性必须在loadMulLine前                                                                                              
                                                                                                              
    LHItemPriceFactorGrid.mulLineCount = 0;                                          
    LHItemPriceFactorGrid.displayTitle = 1;                                          
    LHItemPriceFactorGrid.hiddenPlus = 0;                                            
    LHItemPriceFactorGrid.hiddenSubtraction = 0;                                     
    LHItemPriceFactorGrid.canSel = 1;                                                
                                            
    //LHItemPriceFactorGrid.selBoxEventFuncName = "showOne";                         
             ;                                                             
    LHItemPriceFactorGrid.loadMulLine(iArray);                                       
    //这些操作必须在loadMulLine后面                                          
    ////LHItemPriceFactorGrid.setRowColData(1,1,"asdf");                                                                                                
                                                                                                                                                                     
  }                                                                          
  catch(ex) {                                                                
    alert(ex);                                                               
  }                                                                          
}                                                                        
      
      
</script>
  