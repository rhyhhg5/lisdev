/** 
 * 程序名称：LHHealthAskQuery.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-06-18 09:08:27
 * 创建人  ：hm
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}

function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     

//  var strSql="select distinct a.CustomerNo,b.name, a.InHospitNo, d.HospitName,a.InHospitDate,"
//  +" e.DoctName,a.InHospitMode,c.MainCureMode"
//  +" from LHCustomInHospital a, ldperson b,LHDiagno c,LDHospital d,LDDoctor e where   "
//  +" a.CustomerNo=b.CustomerNo and b.CustomerNo=c.CustomerNo and a.InHospitNo = c.InHospitNo and a.HospitCode=d.HospitCode"
//  +" and c.DoctNo=e.DoctNo "
//  +getWherePart("a.CustomerNo","CustomerNo") 
//  +getWherePart("c.CustomerNo","CustomerNo")
//  +getWherePart("b.CustomerNo","CustomerNo")
//  ;

//	turnPage.queryModal(strSql,  LHCustomInHospitalGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}


function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function afterQuery00(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.DiseasCode.value = arrResult[0][0];
  }
}


function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.jsp");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}


function query()
{
		var SQL;

//	咨询时间
		var str_LogDate = null;
		if(fm.LogDate.value != "" )
		{str_LogDate = " a.LogDate "+fm.LogDate_h.value+"'"+fm.LogDate.value+"'";}
		else{str_LogDate = "1=1";}
		
//咨询总延时
		var str_AskTotalTime = null;
		if(fm.AskTotalTime.value != "" )
		{str_AskTotalTime = " a.AskTotalTime "+fm.AskTotalTime_h.value+"'"+fm.AskTotalTime.value+"'";}
		else{str_AskTotalTime = "1=1";}
		
//解答花费时间
		var str_AnswerCostTime = null;
		if(fm.AnswerCostTime.value != "" )
		{str_AnswerCostTime = " integer(c.AnswerCostTime) "+fm.AnswerCostTime_h.value+" integer('"+fm.AnswerCostTime.value+"')";}
		else{str_AnswerCostTime = "1=1";}
		                                                
//预约剩余时间                                      
//		var str_ = null;                              
//		if(fm..value != "" )                          
//		{str_ = " a. "+fm._h.value+"'"+fm..value+"'";}
//		else{str_ = "1=1";}

		SQL = " select a.logerno,d.name,a.logdate,a.AskMode,a.AskStyle,b.ReplyState,a.AnswerMode, "
         +" a.PromiseAnswerType,a.AskTotalTime,a.AnswerType,a.OtherNoType,c.AnswerCostTime,a.logno "  
         +" from   llmainask a,LLConsult b,LLAnswerInfo c ,LDPerson d"
				 +" where  a.logno = b.logno   and    a.logno = c.logno and a.logerno = d.Customerno and a.Operator = 'hm' "
				 +getWherePart("a.logerno","CustomerNo")
//				 +getWherePart("a.logname","CustomerName")				 				 
				 +getWherePart("a.AskMode","AskMode")
				 +getWherePart("a.AskStyle","AskStyle")
				 +getWherePart("b.ReplyState","ReplyState")
				 +getWherePart("a.AnswerMode","AnswerMode")
				 +getWherePart("a.PromiseAnswerType","PromiseAnswerType")
				 +getWherePart("a.AnswerType","AnswerType")
				 +getWherePart("a.OtherNoType","DelayAnswerType")
				 +" and "+str_LogDate
				 +" and "+str_AskTotalTime
				 +" and "+str_AnswerCostTime

				 ;
		
		window.open("../healthmanage/LHHealthAskQueryOpen.jsp?SQL="+SQL);
}


function clearInput()      
{

		fm.all('CustomerNo').value = "";
		fm.all('CustomerName').value = "";
		fm.all('LogDate_h').value = "";
		fm.all('LogDate_sp').value = "";
		fm.all('LogDate').value = "";
		fm.all('ReplyState').value = "";
		fm.all('ReplyState_ch').value = "";
		fm.all('AskMode').value = "";
		fm.all('AskModeName').value = "";
		fm.all('AskStyle').value = "";
		fm.all('AskStyle_ch').value = "";
		fm.all('AnswerMode').value = "";
		fm.all('AnswerMode_ch').value = "";
		fm.all('PromiseAnswerType').value = "";
		fm.all('PromiseAnswerType_ch').value = "";
		fm.all('AskTotalTime_h').value = "";
		fm.all('AskTotalTime_sp').value = "";
		fm.all('AskTotalTime').value = "";
		fm.all('AnswerType').value = "";
		fm.all('AnswerType_ch').value = "";
		fm.all('DelayAnswerType').value = "";
		fm.all('DelayAnswerType_ch').value = "";
		fm.all('AnswerCostTime_h').value = "";
		fm.all('AnswerCostTime_sp').value = ""; 
		fm.all('AnswerCostTime').value = "";
		fm.all('BookLeftTime_sp').value = "";
		fm.all('BookLeftTime').value = "";
		fm.all('BookLeftTime_h').value = "";
}  


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	//var arrResult = new Array();
	var strSQL = "select ICDCode,ICDName,FrequencyFlag,ChronicMark from LDDisease where ICDCode='"+arrQueryResult[0][0]+"'";
	arrResult = easyExecSql(strSQL);
	
	if( arrResult != null )
	{
        fm.all('ICDCode').value= arrResult[0][0];                                   
        fm.all('ICDName').value= arrResult[0][1];  
        fm.all('FrequencyFlag').value= arrResult[0][2];                    
        fm.all("ChronicMark").value= arrResult[0][3]; 
        if(arrResult[0][2]=="5")
        {
        		ChronicMark1.style.display='';
        		if(arrResult[0][3]=="0"){fm.all("ChronicMark_ch").value="否"}
        		if(arrResult[0][3]=="1"){fm.all("ChronicMark_ch").value="是"}
        } 
	}
} 
