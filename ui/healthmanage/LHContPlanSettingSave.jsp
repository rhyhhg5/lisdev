<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHContPlanSettingSave.jsp
//程序功能：
//创建日期：2006-07-04 11:20:57
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.health.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	String inServPlanNo="";
	LHServCaseRelaSet tLHServCaseRelaSet   = new LHServCaseRelaSet();
	LHContPlanSettingUI tLHContPlanSettingUI   = new LHContPlanSettingUI();
    LHServCaseDefSet tLHServCaseDefSet = new LHServCaseDefSet();	
    String tServCaseType[] = request.getParameterValues("LHServPlanGrid6");					//事件类型
    String tServCaseName[] = request.getParameterValues("LHServPlanGrid8");					//事件名称
    String tServCaseState[] = request.getParameterValues("LHServPlanGrid9");					//服务事件状态
    String tServCaseCode[] = request.getParameterValues("LHServPlanGrid7");					//服务事件编号
    String tServItemNo[] = request.getParameterValues("LHServPlanGrid3");					//服务项目号码
    String tServItemCode[] = request.getParameterValues("LHServPlanGrid1");					//标准服务项目代码
    String tServItemName[] = request.getParameterValues("LHServPlanGrid2");					//标准服务项目代码
    
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	int j;
	String CaseType[];
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");
    int LHServCaseDefCount = 0;
	if(tServItemCode != null)
	{	
		LHServCaseDefCount = tServItemCode.length;
	}	
	System.out.println(" LHServCaseDefCount is : "+LHServCaseDefCount);
		  
	for(int i = 0; i < LHServCaseDefCount; i++)
	{
		if(tServCaseType[i].equals("1"))
		{
			LHServCaseRelaSchema tLHServCaseRelaSchema   = new LHServCaseRelaSchema();
            tLHServCaseRelaSchema.setServPlanNo(request.getParameter("ServPlanNo"));        //服务计划号码                                                            
            tLHServCaseRelaSchema.setServPlanName(request.getParameter("ServPlanName"));        //服务计划名称                                                            
            tLHServCaseRelaSchema.setComID(request.getParameter("ComID"));                        //机构属性标识                                            
            tLHServCaseRelaSchema.setServPlanLevel(request.getParameter("ServPlanLevel"));     //服务计划档次
            tLHServCaseRelaSchema.setContNo(request.getParameter("ContNo"));                  //保单号
            tLHServCaseRelaSchema.setCustomerNo(request.getParameter("CustomerNo"));          //客户号
            tLHServCaseRelaSchema.setCustomerName(request.getParameter("Name"));               //姓名                                                         
            tLHServCaseRelaSchema.setManageCom(request.getParameter("ManageCom"));                                                                    
            tLHServCaseRelaSchema.setOperator(request.getParameter("Operator"));                                                                    
            tLHServCaseRelaSchema.setMakeDate(request.getParameter("MakeDate"));                                                                    
            tLHServCaseRelaSchema.setMakeTime(request.getParameter("MakeTime"));                                                          
            tLHServCaseRelaSchema.setServItemCode(tServItemCode[i]);	
			tLHServCaseRelaSchema.setGrpName(tServItemName[i]);	
			tLHServCaseRelaSchema.setServItemNo(tServItemNo[i]);	
	        tLHServCaseRelaSet.add(tLHServCaseRelaSchema);
	        
	        LHServCaseDefSchema tLHServCaseDefSchema = new LHServCaseDefSchema();
			       
	        tLHServCaseDefSchema.setServCaseType(tServCaseType[i]);
	        tLHServCaseDefSchema.setServCaseCode(tServCaseCode[i]);	
	        tLHServCaseDefSchema.setServCaseName(tServCaseName[i]);	
	        tLHServCaseDefSchema.setServCaseState("1");	
	        tLHServCaseDefSchema.setOperator(request.getParameter("Operator"));
	        tLHServCaseDefSchema.setMakeDate(request.getParameter("MakeDate"));
	        tLHServCaseDefSchema.setMakeTime(request.getParameter("MakeTime"));
	        tLHServCaseDefSchema.setManageCom(request.getParameter("ComID"));  //liuli修改于08/06 
	        tLHServCaseDefSchema.setModifyDate(request.getParameter("ModifyDate"));
	        tLHServCaseDefSchema.setModifyTime(request.getParameter("ModifyTime"));                    
	        tLHServCaseDefSet.add(tLHServCaseDefSchema);
		}
	}
	try
	{
  		// 准备传输数据 VData
  		VData tVData = new VData();
        tVData.add(tLHServCaseDefSet);
        tVData.add(tLHServCaseRelaSet);
		tVData.add(tG);
    	tLHContPlanSettingUI.submitData(tVData,transact);
	}
    catch(Exception ex)
	{
    	Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tLHContPlanSettingUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
}

%>                      
<%=Content%>
<html>
<script language="javascript">
	<%if(inServPlanNo.equals(""))
	{%>
		//alert("save : ServPlanNo为空");
		parent.fraInterface.fm.all("ServPlanNo").value="";
	<%}
	else
	{%>	
//		alert("save : ServPlanNo不为空");
		parent.fraInterface.fm.all("ServPlanNo").value = "<%=inServPlanNo%>";
//		alert(parent.fraInterface.fm.all("ServPlanNo").value);
	<%}%>	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
