<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHIndivServAcceptSave.jsp
//程序功能：个人服务受理操作
//创建日期：2006-08-26 14:42:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期 : 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  
  LHIndiServAcceptUI tLHIndiServAcceptUI   = new LHIndiServAcceptUI();
  
  //LHIndiServAcceptSet tLHIndiServAcceptSet = new LHIndiServAcceptSet();		//个人服务受理
  // String tServCaseCode[] = request.getParameterValues("LHServCaseGrid1");					// 事件编号
  // String tServAccepNo[] = request.getParameterValues("LHServCaseGrid12");					// 受理事件号
		
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    //int LHIndiServAcceptCount = 0;
		//if(tServCaseCode != null)
		//{	
		//	LHIndiServAcceptCount = tServCaseCode.length;
		//}	
		//System.out.println(" LHIndiServAcceptCount is : "+LHIndiServAcceptCount);
		//
		//for(int i = 0; i < LHIndiServAcceptCount; i++)
		//{
  	     LHIndiServAcceptSchema tLHIndiServAcceptSchema   = new LHIndiServAcceptSchema();
  	     
  	     tLHIndiServAcceptSchema.setServAccepNo(request.getParameter("ServAccepNo"));//服务受理编号
  	     tLHIndiServAcceptSchema.setServCaseCode(request.getParameter("ServCaseCode"));//服务受理事件号码 
  	     tLHIndiServAcceptSchema.setTaskModelType(request.getParameter("TaskModelType"));//服务受理模块类型
         tLHIndiServAcceptSchema.setServAccepChannel(request.getParameter("ServAccepChannel"));//服务受理渠道
         tLHIndiServAcceptSchema.setServAceptDate(request.getParameter("ServAceptDate"));//服务受理时间
         tLHIndiServAcceptSchema.setCustomerNo(request.getParameter("CustomerNo"));
         tLHIndiServAcceptSchema.setServItemCode(request.getParameter("ServItemCode"));
         tLHIndiServAcceptSchema.setBespeakComID(request.getParameter("HospitCode"));//预约服务台机构
         tLHIndiServAcceptSchema.setBespeakAcceptType(request.getParameter("BespeakAcceptType"));//预约受理类型
         tLHIndiServAcceptSchema.setBespeakDateRequest(request.getParameter("BespeakDateRequest"));//预约时间要求
         String TaskModelType=request.getParameter("TaskModelType");
         if(TaskModelType.equals("1"))
         {
           tLHIndiServAcceptSchema.setCusServRequest(request.getParameter("CusOtherRequest"));//客户其它要求
         }
         if(TaskModelType.equals("3"))
         {
            tLHIndiServAcceptSchema.setCusServRequest(request.getParameter("CusServRequest"));//客户其它要求
         }
         if(transact.equals("INSERT||MAIN"))
         {
           tLHIndiServAcceptSchema.setServTaskCode(request.getParameter("ServTaskCode"));//任务号
         }
         if(transact.equals("UPDATE||MAIN"))
         {
           tLHIndiServAcceptSchema.setServTaskCode(request.getParameter("ServTaskNO"));//任务号
         }
         tLHIndiServAcceptSchema.setModelTypeNo(request.getParameter("ModelTypeNo"));//类型流水号码
         System.out.println(" CusServRequest is : "+tLHIndiServAcceptSchema.getCusServRequest());
         //tLHIndiServAcceptSchema.setServCaseCode(tServCaseCode[i]);	
        
         // tLHIndiServAcceptSchema.setServAccepNo(tServAccepNo[i]);	
         //System.out.println(" ServAccepNo is : ");
         //tLHIndiServAcceptSet.add(tLHIndiServAcceptSchema);
    //}
    
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLHIndiServAcceptSchema);
  	tVData.add(tG);
    
    tLHIndiServAcceptUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHIndiServAcceptUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	var FlagStr="<%=FlagStr%>";
		if(FlagStr!="Fail")
    {
	     var transact = "<%=transact%>";
	     if(transact!="DELETE||MAIN")
	     {
	     	  if(transact=="INSERT||MAIN")
	     	  {
	   	       var arrResult = new Array();
	           arrResult[0] =new Array();
	           arrResult[0][0] = parent.fraInterface.fm.all("ServCaseCode").value;
	           //alert(arrResult[0][0]);
	           parent.fraInterface.afterQueryAdd(arrResult); 
	        }
	        if(transact=="UPDATE||MAIN")
	     	  {
	   	       var arrResult = new Array();
	           arrResult[0] =new Array();
	           arrResult[0][0] = parent.fraInterface.fm.all("ServCaseCode").value;
	           //alert(arrResult[0][0]);
	           parent.fraInterface.afterQueryUpdate(arrResult); 
	        }
	     }
	  }
</script>
</html>