<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-03-08 17:08:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDICDOPSInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDICDOPSInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDICDOPSSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDICDOPS1);">
    		</td>
    		 <td class= titleImg>
        		 手术ICD代码基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDICDOPS1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      手术分类代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICDOPSCode elementtype=nacessary verify="手术分类代码|notnull&len<=20">
    </TD>
    <TD  class= title>
      手术分类名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICDOPSName elementtype=nacessary verify="手术分类名称|notnull&len<=200">
    </TD>
    <TD  class= title>
      代码等级
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=OpsGrag  verify="代码等级|notnull&len<=2" CodeData= "0|^2|2级代码^3|3级代码^4|4级代码^5|5级代码" ondblClick= "showCodeListEx('OpsGrag',[this,OpsGrag_ch],[0,1],null,null,null,1);" ><Input class=codename name=OpsGrag_ch elementtype=nacessary>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      手术风险等级
    </TD>
    <TD  class= input id= FrequencyFlag1 style="display:'none'">
      <Input class=codeno name=FrequencyFlag verify="手术风险等级|len<=15" CodeData= "0|^0|未分类^1|1类手术^2|2类手术^3|3类手术^4|4类手术" ondblClick= "showCodeListEx('',[this,FrequencyFlag_ch],[0,1],null,null,null,1);"><Input class= 'codename' name=FrequencyFlag_ch >
    </TD>
    
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
