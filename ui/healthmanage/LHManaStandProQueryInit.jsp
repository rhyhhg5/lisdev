<%
//程序名称：LHManaStandProQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-09-11 20:27:48
//创建人  ：郭丽颖
//更新记录：
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() 
{ 
  try 
  {     
    fm.all('StandCode').value = "";
    fm.all('StandContent').value = "";
    fm.all('StandLevel').value = "";
    fm.all('StandLevelName').value = "";
    fm.all('StandUnit').value = "";
    fm.all('QuesStauts').value = "";
    fm.all('QuesStautsName').value = "";
    fm.all('QuerAffiliated').value = "";
    fm.all('QuerAffiliatedName').value = "";
    fm.all('QuerResType').value = "";
    fm.all('QuerResTypeName').value = "";
    fm.all('ResuAffiInfo').value = "";
    fm.all('StandQuerType').value = "";
    fm.all('StandQuerTypeName').value = "";

  }
  catch(ex) {
    alert("在LHManaStandProQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() 
{
  try 
  {
    initInpBox();
    initLHStaQuOptionGrid();  
  }
  catch(re) {
    alert("LHManaStandProQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }                                                      
}
//领取项信息列表的初始化
var LHStaQuOptionGrid;
function initLHStaQuOptionGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="";         				//列名
    
    iArray[1]=new Array();                
    iArray[1][0]="问题(分类)代码";     		//列名
    iArray[1][1]="90px";         		//列名
    iArray[1][3]=0;         				//列名
    iArray[1][4]="";         				//列名
    
    iArray[2]=new Array();                
    iArray[2][0]="问题(分类)名称";        //列名
    iArray[2][1]="90px";         		//列名
    iArray[2][3]=0;         				//列名
    iArray[2][4]="";         				//列名
    
    iArray[3]=new Array();                
    iArray[3][0]="代码分类等级";      //列名
    iArray[3][1]="70px";         		//列名
    iArray[3][3]=0;         					//列名
    iArray[3][4]="";         					//列名
    
    iArray[4]=new Array();                         
    iArray[4][0]="标准单位";      //列名 
    iArray[4][1]="90px";         		//列名       
    iArray[4][3]=0;         					//列名         
    iArray[4][4]="";   
    
    iArray[5]=new Array();                
    iArray[5][0]="问题状态";      //列名
    iArray[5][1]="60px";         		//列名
    iArray[5][3]=1;         					//列名
    iArray[5][4]="";         					//列名
          					//列名         
    
    iArray[6]=new Array();                
    iArray[6][0]="问题所属问卷";      //列名
    iArray[6][1]="120px";         		//列名
    iArray[6][3]=1;         					//列名
    iArray[6][4]="";         					//列名
    
    
    iArray[7]=new Array();                
    iArray[7][0]="问题答案类型";      //列名
    iArray[7][1]="90px";         		//列名
    iArray[7][3]=1;         					//列名
    
        
    iArray[8]=new Array();                
    iArray[8][0]="问题附属信息";      //列名
    iArray[8][1]="90px";         		//列名
    iArray[8][3]=1;         					//列名
    
        
    iArray[9]=new Array();                
    iArray[9][0]="问题类型";      //列名
    iArray[9][1]="70px";         		//列名
    iArray[9][3]=1;         					//列名
    
    LHStaQuOptionGrid = new MulLineEnter( "fm" , "LHStaQuOptionGrid" ); 
    //这些属性必须在loadMulLine前

		LHStaQuOptionGrid.canSel = 1;     
		LHStaQuOptionGrid.canChk = 0;
		LHStaQuOptionGrid.mulLineCount = 0;     
		LHStaQuOptionGrid.displayTitle = 1;     
		LHStaQuOptionGrid.hiddenPlus = 1;       
		LHStaQuOptionGrid.hiddenSubtraction = 1;

    LHStaQuOptionGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    
  }
  catch(ex) {
    alert(ex);
  }
}
</script>





