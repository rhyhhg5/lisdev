// 该文件中包含客户端需要处理的函数和事件


var mDebug="0";
var mOperate="";
var showInfo;
//window.onfocus=myonfocus;
var turnPage = new turnPageClass(); 
var firstNo;
var selectedTestRow;
var DataCache = new Array();
var type = "SAVE";

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
function TestHithIndex()
{	
		var rowNum=CustomTestInfoGrid.mulLineCount ; //行数 	
	if(rowNum!="0"&&rowNum!=""&&rowNum!="null"&&rowNum!=null)
	{
		var Stature="";//身高
		var Avoirdupois="";//体重
		var AvoirdIndex="";//体重指数
		var hStature="";//身高的值
		var hAvoirdupois="";//体重的值
		var hAvoirdIndex="";//体重指数的值
		var hIndex="";//将体重指数转换为保存两位数字
		var testIndex=new Array;//查询是否存在体重指数
		for(var row=0; row < rowNum; row++)
		{
			testIndex=CustomTestInfoGrid.getRowColData(row,4);
			if(testIndex=="110500014")//体重指数
			{
				for(var rowStature=0; rowStature < rowNum; rowStature++)
	        	{
					Stature=CustomTestInfoGrid.getRowColData(rowStature,4);
					if(Stature=="110500009")//身高
					{
	        	 		hStature=CustomTestInfoGrid.getRowColData(rowStature,3);
	        	 	}
	        	}
	        	for(var rowAvoirdupois=0; rowAvoirdupois < rowNum; rowAvoirdupois++)
	        	{
					Avoirdupois=CustomTestInfoGrid.getRowColData(rowAvoirdupois,4);
	        	 	if(Avoirdupois=="110500010")//体重
	        	 	{
						hAvoirdupois=CustomTestInfoGrid.getRowColData(rowAvoirdupois,3);
	        	 	}
	        	}
	        	
	        	hAvoirdIndex=hAvoirdupois/(hStature*hStature);
	        	hIndex=Math.round(hAvoirdIndex*100)/100;
	        	try 
	        	{
                	if(hIndex.toString()=="NaN"||hIndex.toString()=="Infinity") 
                	{  
                		CustomTestInfoGrid.setRowColData(row,3,"0");
                	}
                	else
                	{
                		CustomTestInfoGrid.setRowColData(row,3,hIndex.toString());
                	}
               	} 
               	catch(ex) 
               	{
                	alert("体重指数的值不存在，请确认体重和身高!");
               	}
            }
		}	  
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	  convertValue();
	  TestHithIndex();
	  var sqlSerialno="select Serialno , RelationNo from LHScanInfo"
	                +" where  Serialno='"+fms.all('ScanNo').value+"' and RelationNo='"+fm.all('CustomerNo').value+"' ";
     var arrResult2 = new Array(); 
     arrResult2=easyExecSql(sqlSerialno);
     if(arrResult2!=""&&arrResult2!="null"&&arrResult2!=null)
     {
       var  ScanNo2=arrResult2[0][0];
       var  CustomerNo2=arrResult2[0][1];
     }
     if(fms.all('ScanNo').value==ScanNo2&&fm.all('CustomerNo').value==CustomerNo2)
     {
    	 alert("此用户编号的扫描件信息已经存在,不允许重复保存!");
    	 return false;
     }
      if((ScanNo2==""&&CustomerNo2=="")||(ScanNo2=="null"&&CustomerNo2=="null")||(ScanNo2==null&&CustomerNo2)==null)
     {
		      if( CustomerExit()== false )
		      {return false;}
	        var i = 0;
	        if( verifyInput2() == false ) return false;
	        fm.fmtransact.value="INSERT||MAIN";
	        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  	      fm.submit(); //提交
  	      
  	  }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    fm.all("iscomefromquery").value="1";
    if(fm.fmtransact.value == "INSERT||MAIN")
    {
    	 fms.fmtransact.value = "UPDATE||MAIN";
       fms.submit(); //提交
    }
  }
}
function afterSubmit2( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    fm.all("iscomefromquery").value="1";
    if(fm.fmtransact.value == "INSERT||MAIN")
    {
    	 fms.fmtransact.value = "INSERT||MAIN";
       fms.submit(); //提交
    }
  }
}
function txtSubmit(FlagStr, content)
{
	var ScanNo=parent.fraInterface.fms.all('ScanNo').value;
	var strSql="select SerialNo from LHScanInfo where SerialNo='"+ScanNo+"'";
	var arrResult2=new Array;
  arrResult2=easyExecSql(strSql);

	if(arrResult2==""||arrResult2=="null"||arrResult2==null)
	{
		
		afterSubmit2(FlagStr, content);//对两个表同时保存
	}
	if(arrResult2!=""&&arrResult2!="null"&&arrResult2!=null)
	{
		 	 
		afterSubmit(FlagStr, content);//对一个表保存，对一个表修改
	}
}
function s_afterSubmit( FlagStr, content )
{
	//alert("fms.RelationNo2.valueAAAA "+fms.RelationNo2.value)
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCustomTest.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
}
 
//提交前的校验、计算  
function beforeSubmit()
{
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateAllClick()
{
	
	if(fms.all('BussType').value=="HM")
	{
		updateClick();
	}
	if(fms.all('BussType').value=="TB")
	{
		 var strSql="select SerialNo from LHScanInfo where SerialNo='"+fms.all('ScanNo').value+"'";
		 var arrResult2=new Array;
		 arrResult2=easyExecSql(strSql);
		 if(arrResult2==""||arrResult2=="null"||arrResult2==null)
		 {
		   updateClick2();//对一个表修改，一个表保存
		 }
		 if(arrResult2!=""&&arrResult2!="null"&&arrResult2!=null)
		 {
		   updateClick();//对两个表同时修改
		 }
	}
}
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  	if(fm.all('iscomefromquery').value=="0")
    {
     alert("请先保存当前结果！");
     return false;
    }
    convertValue();
	  TestHithIndex(); 
      var i = 0;
      if( verifyInput2() == false ) return false;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      
      //showSubmitFrame(mDebug);
       fms.fmtransact.value = "UPDATE||MAIN";
       fms.submit(); //提交
       fm.fmtransact.value = "UPDATE||MAIN";
       fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}  
function updateClick2()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  	if(fm.all('iscomefromquery').value=="0")
    {
     alert("请先保存当前结果！");
     return false;
    }
    TestHithIndex(); 
      var i = 0;
      if( verifyInput2() == false ) return false;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      
      //showSubmitFrame(mDebug);
       fms.fmtransact.value = "INSERT||MAIN";
       fms.submit(); //提交
       fm.fmtransact.value = "UPDATE||MAIN";
       fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  showInfo=window.open("./LHCustomTestQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	if(fm.all('InHospitNo').value == "")
	{alert("无法删除，请重新查询一条信息");return false;}
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//UPLOAD
function fileUpload()
{
	upfm.action = "./LHCustomTestImportSave.jsp";
	var showStr="正在上传数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	upfm.submit();
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
 
function QueryFirst()
{
	 showInfo=window.open("./LHCustomTestQuery.html","客户体检查询页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
} 
function afterQuery0( arrQueryResult )
{
	 fms.all('saveButton').disabled=true;
	 fms.all('RelationNo').value= arrQueryResult[0][0];
	 fms.all('RelationNo2').value= arrQueryResult[0][4];
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{ 
			var strSQL ="select distinct a.CustomerNo,b.name,a.InHospitDate,a.HospitCode,'',a.InHospitMode," 
       +" a.MakeDate,a.MakeTime,"
       //,(select distinct c.DoctNo from LHCustomTest c where a.CustomerNo = c.CustomerNo and a.Inhospitno = c.Inhospitno )
       +"a.InHospitNo from LHCustomInHospital a,ldperson b "//,LDHospital e" 
       +" where a.CustomerNo=b.CustomerNo "  
       +" and a.CustomerNo='"+arrQueryResult[0][0]+"'"
       +" and a.Inhospitno = '"+arrQueryResult[0][4]+"'"
       ;  
		arrResult =easyExecSql(strSQL);

		if(arrResult!=null)
		{
			var tempcustomerno=arrResult[0][0];
			var tempinhospitno=arrResult[0][2];
        fm.all('CustomerNo').value= arrResult[0][0];fm.all('CustomerNo').readOnly = true;
        fm.all('CustomerName').value= arrResult[0][1];

        fm.all('InHospitDate').value= arrResult[0][2];
        fm.all('HospitCode').value= arrResult[0][3];
        if(arrResult[0][3] != "")
        {    fm.all('HospitName').value= easyExecSql("select hospitname from ldhospital where hospitcode = '"+arrResult[0][3]+"'");}
        fm.all('TestModeCode').value=arrResult[0][5]; 
        fm.all('TestMode').value= easyExecSql("select codename from ldcode where codetype='inhospitmode' and code ='"+arrResult[0][5]+"'");  
        fm.all('MakeDate').value= arrResult[0][6];
        fm.all('MakeTime').value= arrResult[0][7];
        //fm.all('DoctNo').value= arrResult[0][8];
        //if(arrResult[0][8] != "")
        //{		fm.all('DoctName').value = easyExecSql("select DoctName from lddoctor where DoctNo = '"+arrResult[0][8]+"'");}
        fm.all('InHospitNo').value = arrQueryResult[0][4];
      	
      	fm.all('TestFee').value = easyExecSql( "select Feeamount from LHFeeInfo where Customerno = '"+arrResult[0][0]+"' and InhospitNo ='"+arrQueryResult[0][4]+"' and Feecode = '5'" );
				if(fm.all('TestFee').value == "null" || fm.all('TestFee').value == null)
				{
					fm.all('TestFee').value = "";
				}
							
      var MulSQL =	 " select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = a.MedicaItemCode)  aaa, "
                    +" a.IsNormal,a.TestResult, a.Medicaitemcode,(select distinct c.standardmeasureunit from lhcountrmedicaitem c where c.Medicaitemcode = a.MedicaItemCode ), a.testno,"
                    +" char( rownumber() over() ),"
                    +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = a.MedicaItemCode ) "
                    +"  from LHCustomTest a "
      							+" where a.CustomerNo = '"+arrResult[0][0]+"' and a.InHospitNo = '"+arrQueryResult[0][4]+"' order by a.Medicaitemcode  "
      							;
				    							
   			turnPage.pageLineNum = 100;  
      	turnPage.queryModal(MulSQL, CustomTestInfoGrid); 
      	
      	var SQL2 = " select a.ICDCode,(select b.ICDName from LDDisease b where b.ICDCode = a.ICDCode),a.SortDepart"
				 				 + " from LCPENoticeResult a where a.ProposalContNo = '"+arrResult[0][0]
				 				 + "' and a.PrtSeq = '"+arrQueryResult[0][4]+"'"
				 				 ;

				turnPage.queryModal(SQL2, TestResultGrid); 
      	 fm.all('iscomefromquery').value="1"; 
      	 fm.all('MedicaItemGroup').value="";
//      	 showCodeName();
     }  
	}
}
        
function getMedicaItemCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select MedicaItemCode,MedicaItemName from LHCountrMedicaItem ";
    fm.all("MedicaItemCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	
	{  
		var newWindow = window.open("../sys/LDPersonQuery.html");	  
	}
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
		var arrResult = easyExecSql(strSql);
		if (arrResult != null) 
		{
	      fm.CustomerNo.value = arrResult[0][0];
	      fm.CustomerName.value = arrResult[0][1];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else
		{
			alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
		}
	}	
}

function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}

function queryCustomerNo2()
{	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户号码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null) 
    {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else
    {
			alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
		}
	}	
}

function inputTestResult(a)
{ 
	divTestResult.style.display='';
	var tempTestResult = fm.all(a).all('CustomTestInfoGrid3').value;
	selectedTestRow = fm.all(a).all('CustomTestInfoGridNo').value-1;//alert(selectedTestRow);
	
	firstNo = CustomTestInfoGrid.getRowColData(0,7);
	
	if(firstNo != 1)
	{
		selectedTestRow = selectedTestRow%10;
	}
	fm.all('textTestResult').value=tempTestResult;	
}

function backTestResult()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) 
	{
		CustomTestInfoGrid.setRowColData(selectedTestRow,3,fm.textTestResult.value);
		fm.textTestResult.value="";
		divTestResult.style.display='none';
	}
}

function easyQueryClickSingle()
{
		if(fm.all('MedicaItemGroup').value != "")
		{						
			var strSQL = "select b.Medicaitemname,a.TestItemCode from ldtestgrpmgt a,lhcountrmedicaitem b where testgrpcode = '"+fm.all('MedicaItemName').value+"' and a.TestItemCode = b.Medicaitemcode ";
	    fm.all("SelectedItem").CodeData = easyQueryVer3(strSQL);
  	}

  	else
  	{
  		var MulSQL = ""
      							;  
      fm.all("SelectedItem").CodeData // =  easyQueryVer3(MulSQL);
  	}
}

function getBack()
{
	divTestResult.style.display='none';
	CustomTestInfoGrid.setRowColData(selectedTestRow,3,fm.textTestResult.value);
	fm.textTestResult.value="";
}

function afterCodeSelect(codeName,Field)
{
	var temp;
	var strSQL = " select b.Medicaitemname,'正常','',a.TestItemCode,b.StandardMeasureUnit,'','1',b.Nomalvalue from ldtestgrpmgt a,lhcountrmedicaitem b where testgrpcode = '"+fm.all('MedicaItemName').value+"' and a.TestItemCode = b.Medicaitemcode order by a.TestItemCode";

	if(codeName == "hmchoosetest")
	{
			turnPage.pageLineNum = 100;  
			turnPage.queryModal(strSQL,CustomTestInfoGrid);
			DataCache = turnPage.arrDataCacheSet;
	}
	
	
	//处理填写结果
	
	if(codeName == "SelectedItem")
	{			
		var setDate = "select MakeDate,MakeTime from LHCustomInHospital where 1=1 "
		+getWherePart("CustomerNo","CustomerNo")
		+getWherePart("InHospitNo","InHospitNo")
		;    
		var arr1 = new Array();
		arr1 = easyExecSql(setDate);
		
		fm.all('MakeDate').value = arr1[0][0];
		fm.all('MakeTime').value = arr1[0][1];
				                                                                      
//		var strSQL_2 = "select codename from ldcode where codetype in (select codealias from ldcode where codetype='medicalitemgroupcode'  and  code = '"+fm.all('MedicaItemName').value+"') and codename = '"+Field.value+"'";
		
//		var strSQL_2 = "select distinct a.codename from ldcode a,lhcustomtest b where b.customerno = '"+fm.CustomerNo.value+"'"
//						+ " and b.inhospitno = '"+fm.InHospitNo.value+"'"
////						+ " and b.testno = '"+fm.TestNo.value+"'"
//						+ " and a.codetype = b.medicaitemcode "
//						+ " and a.code = '"+fm.TestNo.value+"'"  

		var strSQL_2 = " select a.medicaitemname ,'正常','',a.medicaitemcode,a.Priceunit,b.TestNo from  lhcountrmedicaitem a,LHCustomTest b where b.CustomerNo ='"+fm.all('Customerno').value+"' and b.InhospitNo = '"+fm.all('InHospitNo').value+"' and a.medicaitemcode ='"+fm.all('SelectedItemCode').value+"' and a.medicaitemcode = b.medicaitemcode ";                                                          
		turnPage.queryModal(strSQL_2, CustomTestInfoGrid);   
//		CustomTestInfoGrid.setRowColData(0,2,fm.TestNo.value);    
//		CustomTestInfoGrid.setRowColData(0,4,fm.tempGroup.value); 		
	}   

	if(codeName == "lhtestmode")
	{
	    if(Field.value=="体检服务")
	    {
	    	  //alert("请选择体检机构名称!");
	    } 
	} 
//		if(fm.all('HospitCode').value!=""&&fm.all('HospitCode').value!=""&&fm.all('HospitCode').value!=null)
//	{
//		var temp;
//	    var strSQL = " select b.Medicaitemname,'正常','',a.TestItemCode,"
//               +" b.StandardMeasureUnit,'','1',Nomalvalue from ldtestgrpmgt a,lhcountrmedicaitem b "
//               +" where testgrpcode = '"+fm.all('MedicaItemName').value+"' and "
//               +" a.TestItemCode = b.Medicaitemcode and a.Explain='"+fm.all('HospitCode').value+"' "
//               +" order by a.TestItemCode";
//		  turnPage.pageLineNum = 100;  
//		  turnPage.queryModal(strSQL,CustomTestInfoGrid);
//	}
}	

//扫描检查询
function getTestCopy()
{
		showInfo=window.open("LHCustomTestMain.jsp?ContNo="+fm.all('ContNo').value);
}

function getIE(e){
   var t=e.offsetTop;
   var l=e.offsetLeft;
   while(e=e.offsetParent){
      t+=e.offsetTop;
      l+=e.offsetLeft;
   }
   alert("top="+t+"\nleft="+l);
}

//修改保存时校验客户是否存在
function CustomerExit()
{
	var cCustomerNo = fm.CustomerNo.value;  //客户代码
  var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
  var arrResult = easyExecSql(strSql);
  	  	//alert(arrResult);
  if (arrResult == null) 
  {
   	  alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
   	  return false;
  }
}
function checkTestMode()
{
	if(fm.all('TestModeCode').value == "") 
	{alert('请先选择体检方式');return false;}
	if(fm.all('HospitCode').value == "") 
	{
		if(fm.all('TestModeCode').value=="32")
		{
		   alert("请选择机构名称!");
		   return false; 
		}
	}
}
function convertValue()
{
	var rowNum=CustomTestInfoGrid.mulLineCount ; //行数 	

	for(var row=0; row < rowNum; row++)
	{
		var tChk =CustomTestInfoGrid.getChkNo(row); 
		if(tChk == true)
		{
			var itemCode = CustomTestInfoGrid.getRowColData(row,4);
			var cValue = easyExecSql(" select classcode from LHCountrMedicaItem where Medicaitemcode = '"+itemCode+"'");
			if(cValue=="")
			{
				alert("“"+CustomTestInfoGrid.getRowColData(row,1)+"”项目没有换算系数，结果未进行换算");
			}
			if(cValue!="")
			{
				//alert(CustomTestInfoGrid.getRowColData(row,3));
				//alert(CustomTestInfoGrid.getRowColData(row,3)*cValue);
				var tValue = (CustomTestInfoGrid.getRowColData(row,3)*cValue).toString();
				if(tValue.indexOf(".")>=0)
				{//有小数点则进入
					if((tValue.length-tValue.indexOf("."))>2)
					{//小数点后大于两位进入
						tValue = tValue.substring(0,tValue.indexOf(".")+3);
					}
				}
				CustomTestInfoGrid.setRowColData(row,3,tValue);
			}
		}  
	}	
	
}