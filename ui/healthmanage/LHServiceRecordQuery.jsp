<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-06-18 09:08:27
//创建人  ：
//更新记录：  更新人  	  更新日期     更新原因/内容 重新布局
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHServiceRecordQuery.js"></SCRIPT>

</head>

<body  onload="" >
  <BR>
  <form action="" method=post name=fm target="fraSubmit">
  	<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,basicInfo1);">
    		</td>
    		<td class= titleImg>
        		 基本信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "basicInfo1" style= "display: ''">
      <table  class= common align='center' style= "width: '94%'">
  				<TR  class= common>
		          <TD  class= title>
		      			客户号
				    	</TD>
				    	<TD  class= input>    
				    		<Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号|notnull&len<=24" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
					    </TD>
					    <TD  class= title>
					      客户姓名
					    </TD>
					    <TD  class= input>
					      <Input class= 'common' name=CustomerName  verify="客户姓名|len<=20">
					    </TD>
					    <TD  class= title>
                记录时间
              </TD>
              <TD  class= input>
                <Input type=hidden name=FirstRecoDate_h>
              	<Input style='width=20' class= 'code' name=FirstRecoDate_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,FirstRecoDate_h],[1,1],null,null,null,1,75);" >
              	<Input style='width=114' class= 'coolDatePicker' name=FirstRecoDate >                
              </TD>
          </TR>
          <TR  class= common>
          		<TD class = title>
								服务状态
							</TD>
							<TD class = input>
								<input type=hidden name=ServiceState>
								<Input class = 'code' name =ServiceState_ch verify="服务状态|len<=4" CodeData= "0|^1|进行中^2|服务结束" ondblClick= "showCodeListEx('ServiceState',[this,ServiceState],[1,0],null,null,null,1);" >
							</TD>
							<TD class = title>
							</TD>
							<TD class = input>
							</TD>
							<TD class = title>
							</TD>
							<TD class = input>
							</TD>
          </TR>
      </table>
    </Div>
  	<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,serviceApp1);">
    		</td>
    		<td class= titleImg>
        		 服务申请情况
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "serviceApp1" style= "display: ''">
      <table  class= common align='center' style= "width: '94%'">
  				<TR  class= common>
           		<TD  class= title>
				      	服务项目
				      </TD>
				      <TD  class= input>
						    	<input type=hidden name=DutyItemName>
									<Input class= 'common' name=ApplyDutyItemCode verify="服务项目|len<=100"> 
					    </TD>
					    <TD class = title >
					    	服务时间
					    </TD>
					    <TD class = input>
									<Input type=hidden name=ServiceDate_h>
	              	<Input style='width=20' class= 'code' name=ServiceDate_sp CodeData= "0|^大于|>^大于等于|>=^等于|=^小于等于|<=^小于|<" onclick="return showCodeListEx('simble',[this,ServiceDate_h],[1,1],null,null,null,1,75);" >
	              	<Input style='width=114' class= 'coolDatePicker' name=ServiceDate >
				   	  </TD>
				   	  <TD  class= title>
					      服务机构	
					    </TD>
					    <TD  class= input>
					    	<input type=hidden name=HospitCode>
					      <Input class= 'code' name=HospitName  ondblclick="return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName');" verify="医疗机构名称|len<=100">
			  		  </TD>   
          </TR>
          <TR class= common>
          		<TD  class= title>
					      申请受理方式
					    </TD>
					    <TD  class= input>
					       <Input type=hidden name=ApplyReceiveType>
					       <Input class= 'code' name=ApplyReceiveType_ch verify="|len<=2" CodeData= "0|^1|电话^2|电子邮件^3|其他" ondblClick= "showCodeListEx('hmapplyreceivetype',[this,ApplyReceiveType],[1,0],null,null,null,1);">
					    </TD>
					    <TD  class= title>
					      是否需要预约
					    </TD>
					    <TD  class= input>
					      <Input type=hidden name=ServiceFlag>
					      <Input class= 'code' name=ServiceFlag_ch  verify="是否需要预约|len<=20" CodeData="0|^1|是^0|否" ondblclick="showCodeListEx('ServerFlag',[this,ServiceFlag],[1,0],null,null,null,1);">
					    </TD>
					    <TD class = title>
								是否预约成功
							</TD>
							<TD class = input>
								 <Input type=hidden name=IsBespeakOk>
								 <Input class= 'code' name=IsBespeakOk_ch  verify="|len<=2" CodeData= "0|^1|预约成功^2|预约失败" ondblClick= "showCodeListEx('IsBespeakOk',[this,IsBespeakOk],[1,0],null,null,null,1);" >
							</TD>
          </TR>
      </table>
    </Div>
    <table>
    	<tr>
    		<td>
    			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,serviceExec1);">
  			</td>
  			<td class=titleImg>
  				服务执行情况
  			</td>
  		</tr>
  	</table>
  	<Div  id= "serviceExec1" style= "display: ''">
      <table  class= common align='center' style= "width: '94%'">
  				<TR  class= common>
           	<TD class = title>
							执行服务项目
						</TD>
						<TD class = input>
							<input type=hidden name=ExcuteServiceItem>
							<Input class = 'common' name=ExcuteServiceItemName>
						</TD>	  
						<TD class = title>
							执行服务机构
						</TD>
						<TD class = input>
							<input type=hidden name=ExcuteServiceHospit>
							<Input class = 'code' name=ExcuteServiceHospitName ondblclick=" return showCodeList('lhhospitname',[this,ExcuteServiceHospit],[0,1],null,fm.ExcuteServiceHospitName.value,'HospitName');" onkeyup=" if(event.keyCode ==13) return showCodeList('lhhospitname',[this,ExcuteServiceHospit],[0,1],null,fm.ExcuteServiceHospitName.value,'HospitName');" verify="医疗机构名称|len<=100">
						</TD>
						<TD class = title>
							服务执行状态
						</TD>
						<TD class = input>
							<Input type=hidden name=ServiceExecState>
							<Input class= 'code' name=ServiceExecState_ch verify="|len<=2" ondblClick= "showCodeList('servicestate',[this,ServiceExecState],[1,0],null,null,null,1);" >
						</TD>
          </TR>
          <TR class= common>
          		<TD class = title>
								客户满意度
							</TD>
							<TD class = input>
								<Input class= 'code' name=SatisfactionDegree >
							</TD>
          </TR>
      </table>
    </Div>
    <BR>
  	<hr>
  	<Input type=button class=cssbutton value= "查询" onclick="query();">
  	<Input type=button class=cssbutton value= "重置" onclick="clearInput();"> 
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>