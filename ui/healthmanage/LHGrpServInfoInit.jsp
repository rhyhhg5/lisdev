<%
//程序名称：LHGrpServEventSet.jsp
//程序功能：
//创建日期：2006-07-18 11:12:17
//创建人  ：CrtHtml程序创建
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {         
  	 flag = <%=request.getParameter("flag")%>;       
  	 GrpServItemNo = "<%=request.getParameter("GrpServItemNo")%>";
  	               
    if (flag == 1)
    {
      fm.all('GrpContNo').readOnly = true;
      fm.all('ServItemName').readOnly = true;
	  fm.all('ServeEventStyle').value = "2";
	  fm.all('ServeEventStyle_ch').value = "集体服务事件";
	  fm.all('ServeEventStyle_ch').ondblclick = "";
    }
    else
    {
		fm.all('GrpContNo').readOnly = false;
		fm.all('ServItemName').readOnly = false;
    }
  } 	
  catch(ex)
  {
    alert("在LHGrpServInfoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                     
function initForm()
{
  try
  {
    initInpBox();
    initLHCustomInfoGrid();
    }
  catch(re)
  {
    alert("LHGrpServInfoInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var LHCustomInfoGrid;
function initLHCustomInfoGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";			//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="客户号";   
	iArray[1][1]="40px";   
	iArray[1][2]=20;        
	iArray[1][3]=1;
	
	iArray[2]=new Array(); 
	iArray[2][0]="客户姓名";   
	iArray[2][1]="40px";   
	iArray[2][2]=20;        
	iArray[2][3]=1;
	
	iArray[3]=new Array();                               
  iArray[3][0]="保单号";         		//列名  
  iArray[3][1]="50px";         		//列名   
  iArray[2][2]=20;              
  iArray[3][3]=1;         		//列名         
  
  iArray[4]=new Array();                               
  iArray[4][0]="服务代码";         		//列名  
  iArray[4][1]="40px";         		//列名   
  iArray[4][2]=20;              
  iArray[4][3]=1;         		//列名   
  
  iArray[5]=new Array();                               
  iArray[5][0]="服务项目名称";         		//列名  
  iArray[5][1]="100px";         		//列名   
  iArray[5][2]=20;              
  iArray[5][3]=1;         		//列名  
  
  iArray[6]=new Array();                               
  iArray[6][0]="序号";         		//列名  
  iArray[6][1]="20px";         		//列名   
  iArray[6][2]=20;              
  iArray[6][3]=1;
  
  iArray[7]=new Array();                               
  iArray[7][0]="服务执行状态";         		//列名  
  iArray[7][1]="50px";         		//列名   
  iArray[7][2]=20;              
  iArray[7][3]=1;
  
  iArray[8]=new Array();                               
  iArray[8][0]="机构";         		//列名  
  iArray[8][1]="20px";         		//列名   
  iArray[8][2]=1000;              
  iArray[8][3]=1;           
   
  iArray[9]=new Array();                               
  iArray[9][0]="定价方式代码";         		//列名  
  iArray[9][1]="60px";         		//列名   
  iArray[9][2]=1000;              
  iArray[9][3]=1; 
  
  iArray[10]=new Array();                               
  iArray[10][0]="定价方式名称";         		//列名  
  iArray[10][1]="60px";         		//列名   
  iArray[10][2]=1000;              
  iArray[10][3]=1;   
    
  iArray[11]=new Array();                               
  iArray[11][0]="个人服务项目号码";         		//列名  
  iArray[11][1]="0px";         		//列名   
  iArray[11][2]=20;              
  iArray[11][3]=3;   
    
    LHCustomInfoGrid = new MulLineEnter( "fm" , "LHCustomInfoGrid" ); 
    //这些属性必须在loadMulLine前
  
    LHCustomInfoGrid.mulLineCount = 0;   
    LHCustomInfoGrid.displayTitle = 1;
    LHCustomInfoGrid.hiddenPlus = 1;
    LHCustomInfoGrid.hiddenSubtraction = 1;
    LHCustomInfoGrid.canSel = 0;
    LHCustomInfoGrid.canChk = 1;
    //LHCustomInfoGrid.selBoxEventFuncName = "showOne";

    LHCustomInfoGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHGrpServPlanGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>