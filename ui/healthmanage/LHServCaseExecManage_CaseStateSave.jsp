<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： LHServCaseExecManage_CaseStateSave.jsp
//程序功能：
//创建日期：2006-07-05 11:01:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  
<%
	//接收信息，并作校验处理。
	//输入参数
	
	String tLHServCaseNo[] = request.getParameterValues("LHSettingSlipQueryGrid2");
	String tCaseState[] = request.getParameterValues("LHSettingSlipQueryGrid5");
	
	//输出参数
	CErrors tError = null;
	           
	String FlagStr = "";
	String Content = "";
	String transact = "";
	
	LHServCaseDefSet tLHServCaseDefSet = new LHServCaseDefSet();
	OLHServCaseChangeStateUI tOLHServCaseChangeStateUI = new OLHServCaseChangeStateUI();
	
	int RowNo = 0;
	String tCheckedNo[] = new String[20];
	for(int tN = 0; tN < 20; tN++)
	{
		tCheckedNo[tN] = "-1";
	}
	String tChk[] = request.getParameterValues("InpLHSettingSlipQueryGridChk");
	
	for(int index=0;index<tChk.length;index++)       
	{                                        
		if(tChk[index].equals("1"))  
		{          	
	    	System.out.println(index+"该行被选中"); 
	    	tCheckedNo[RowNo++] = ""+index;
	    }
	}        
	 
	RowNo = 0;
	int LineNo = tLHServCaseNo.length;
	for(int i = 0; i < LineNo; i++)
	{
		if (tCheckedNo[RowNo].equals(""+i))
		{
			LHServCaseDefSchema tLHServCaseDefSchema = new LHServCaseDefSchema();
			tLHServCaseDefSchema.setServCaseCode(tLHServCaseNo[i]);
			tLHServCaseDefSchema.setServCaseState(tCaseState[i]);
			tLHServCaseDefSet.add(tLHServCaseDefSchema);
			RowNo++;
		}
	}
	            
	try
	{
	  	VData tVData = new VData();
	    tVData.add(tLHServCaseDefSet);
		tOLHServCaseChangeStateUI.submitData(tVData,"INSERT||MAIN");
	}
	catch(Exception ex)
	{
	  Content = "操作失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
	}
  
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
    	tError = tOLHServCaseChangeStateUI.mErrors;
    	if (!tError.needDealError())
    	{                          
    		Content = "事件设置完成! ";
    		FlagStr = "Success";
    	}
    	else                                                                           
    	{
    		Content = "事件设置失败，原因是:" + tError.getFirstError();
    		FlagStr = "Fail";
    	}
	}
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterChangeCaseStateFun("<%=FlagStr%>","<%=Content%>");
</script>
</html>
