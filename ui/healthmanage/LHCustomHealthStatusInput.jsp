<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-18 10:11:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHCustomHealthStatusInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHCustomHealthStatusInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHCustomHealthStatusSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomHealthStatus1);">
    		</td>
    		 <td class= titleImg>
        		 客户基本健康状况及生活习惯基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomHealthStatus1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      客户号码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|len<=24" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerName >
    </TD>
    
  </TR>
  <TR  class= common>
  	<TD  class= title>
      序号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HealthNo >
    </TD>
    <TD  class= title>
      增加日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=AddDate verify="新增日期|len<=20">
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      身高（m）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Stature verify="身高|num&len<=4" onkeyup="return computekgm2();">
    </TD>
    <TD  class= title>
      体重（kg）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Avoirdupois verify="体重|num&len<=5" onkeyup="return computekgm2();">
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      体重指数（kg/m2）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AvoirdIndex MAXLENGTH="5" verify="体重指数|num&len<=10">
    </TD>
    <TD  class= title>
      血压舒张压（mmHg）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BloodPressHigh verify="血压舒张|num&len<=10" > 
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      血压收缩压（mmHg）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BloodPressLow verify="血压收缩压|num&len<=10" > 
    </TD>
    <TD  class= title>
      吸烟（支/日）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Smoke verify="吸烟|num&len<=10">
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      饮酒（两/周）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=KissCup verify="饮酒|num&len<=10">
    </TD>
    <TD  class= title>
      熬夜（次/月）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SitUp verify="熬夜|num&len<=10">
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      饮食不规律（餐/周）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiningNoRule verify="饮食不规律|num&len<=40">
    </TD>
    <TD  class= title>
      不良嗜好
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BadHobby verify="不良嗜好|len<=40">
    </TD>

 

</table>
    </Div>
    <div id="div1" style="display: 'none'">
    <table>
     <tr class= common>
     <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
  </TR>
    <TR  class= common>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
  </TR>
  </table>
  </div>
  <hr/>
    
    <INPUT VALUE="客户就诊信息" class=cssButton TYPE=button onclick="CustomInHospital();">
    <INPUT VALUE="客户疾病信息" class=cssButton TYPE=button onclick="CustomDiseas();">
    <INPUT VALUE="客户检查信息" class=cssButton TYPE=button onclick="CustomTest();">
    <INPUT VALUE="客户治疗信息" class=cssButton TYPE=button onclick="CustomOPS();">
    <INPUT VALUE="客户家族病史" class=cssButton TYPE=button onclick="CustomFamily();">
    <INPUT VALUE="客户健身信息" class=cssButton TYPE=button onclick="CustomGym();">
  <hr/>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden name="iscomefromquery">
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
