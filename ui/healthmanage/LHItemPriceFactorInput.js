//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass(); 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		    fm.submit(); //提交
}    
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHHealthServPlanInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHHealthServPlanQueryInput.html","信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initLHItemPriceFactorGrid();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{ 
       //alert(arrQueryResult[0][4]);  
    	 arrResult = arrQueryResult;
			 var mulSql = "select distinct ServPlanCode,ServPlanName,"
				            +"ServPlanLevel,ComID,"                                  
										+"(select  CodeName from LDCode where codetype = 'servplanlevel' and ldcode.code=a.ServPlanLevel),"
										+"(select showname from ldcom  where  ldcom.comcode=a.ComID),"
										+" ServPlayType,ServItemCode "
										+" from  LHHealthServPlan a  "
										+" where a.ServPlanCode ='"+arrResult[0][0]+"'and a.ServPlanLevel='"+arrQueryResult[0][4]+"'";		 
				//alert(mulSql);   
		    arrResult=easyExecSql(mulSql);
		    //alert(arrResult[0][2]);
		    //alert(arrResult[0][5]); and a.ServItemCode ='"+arrResult[0][2]+"' 
        fm.all('Riskcode').value				    = arrResult[0][0];
        fm.all('Riskname').value			      = arrResult[0][1];
        fm.all('ServPlanLevelCode').value   = arrResult[0][2];
        fm.all('ComIDCode').value			      = arrResult[0][3];
        fm.all('ServPlanLevelName').value	  = arrResult[0][4];
        fm.all('ComIDName').value	          = arrResult[0][5];
        fm.all('ServPlayType').value	      = arrResult[0][6];
      
         var strSql = "select ServItemCode,ServItemName, "
				            +" ServItemSN "                                  
										+" from  LHHealthServPlan a  "
										+" where a.ServPlanCode ='"+arrResult[0][0]+"'and a.ServPlanLevel='"+arrQueryResult[0][4]+"'";
			
				turnPage.queryModal(strSql, LHHealthServPlanGrid);
			
	}     
}      

 