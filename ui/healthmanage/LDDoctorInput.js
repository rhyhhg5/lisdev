//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  var sql ="select * from lddoctor where DoctNo='"+fm.all('DoctNo').value+"' with ur";
  var res = easyExecSql(sql);
  if(res != null){
     alert("该记录己经存在,不能重复保存!")
     return;	
  }
  fm.fmtransact.value="INSERT||MAIN";
  if(fm.all('HospitCode').value == "" ){fm.all('HospitCode').value = " ";}
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  initInpBox();
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDDoctor.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  if(fm.all('HospitCode').value == "" ){fm.all('HospitCode').value = " ";}
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDDoctorQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	if(fm.all('MakeDate').value == "")
	{
		alert("请先查询返回，再删除！");
		return false;
	}
	
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		//arrResult = arrQueryResult;
//		var strSQL = "select * from LDDoctor where DoctNo='"+arrQueryResult[0][0]+"'";
//		arrResult = easyExecSql(strSQL);
				 
				
				arrResult = arrQueryResult;
				//alert(arrQueryResult[0][2]);
        fm.all('DoctNo').value= (arrResult[0][0]==null)?'':arrResult[0][0];
        fm.all('DoctName').value= arrQueryResult[0][1]; 
        fm.all('BirthYear').value= arrResult[0][2];  
        fm.all('Sex').value= arrResult[0][3];
        if(arrResult[0][3]=="0"){fm.all('Sex_ch').value="男";}
      	if(arrResult[0][3]=="1"){fm.all('Sex_ch').value="女";} 
      	if(arrResult[0][3]=="2"){fm.all('Sex_ch').value="不详";} 
      	fm.all('EduLevelCode').value= (arrResult[0][5]==null)?'':arrResult[0][5];
        var eduLevelCode_ch=easyExecSql("select codename from ldcode where codetype='hmedulevelcode' and code ='"+arrResult[0][5]+"'");
        fm.EduLevelCode_ch.value=(eduLevelCode_ch==null)?'':eduLevelCode_ch;
        fm.all('TechPostCode').value= arrQueryResult[0][4];
         var techPostCode_ch=easyExecSql("select a.codename from ldcode a where a.codetype='TechPostCode' and a.code ='"+arrQueryResult[0][4]+"'");
        fm.all('TechPostCode_ch').value= (techPostCode_ch==null)?'':techPostCode_ch;
        fm.all('HospitCode').value= (arrResult[0][10]==null)?'':arrResult[0][10];
				var hospitName= easyExecSql("select HospitName from ldhospital where HospitCode = '"+arrResult[0][10]+"'");
				fm.all('HospitName').value= (hospitName==null)?'':hospitName;
     		fm.all('SecName').value= arrQueryResult[0][6];if(arrQueryResult[0][6] == "") fm.all('SecName_cn').value == "";else{fm.all('SecName_cn').value = easyExecSql(" select codename from ldcode where codetype = 'hmsecname' and code ='"+arrQueryResult[0][6]+"'");}
				fm.all('ExpertFlag').value= arrResult[0][7]; 
        if(arrResult[0][7]=="1"){fm.all('ExpertFlag_ch').value="是";}
        if(arrResult[0][7]=="0"){fm.all('ExpertFlag_ch').value="否";}
				fm.all('Carreerclass').value= (arrResult[0][8]==null)?'':arrResult[0][8];
				arrResult[0][8]=easyExecSql("select codename from ldcode where codetype='carreertype' and code='"+arrResult[0][8]+"'");
				fm.all('Carreerclass_ch').value= (arrResult[0][8]==null)?'':arrResult[0][8];
				fm.all('TechSpec').value= arrResult[0][9];
        fm.all('MakeDate').value= arrResult[0][11];
        fm.all('ManageCom').value= arrResult[0][12];
        fm.all('AreaCode').value= arrResult[0][17];
        fm.all('AreaName').value=arrResult[0][13];
        fm.all('EngageClass').value= (arrResult[0][14]==null)?'':arrResult[0][14];
        var engageclass_ch=easyExecSql("select codename from ldcode where codetype='engageclasscode' and code ='"+arrResult[0][14]+"'");
        fm.all('EngageClass_ch').value= (engageclass_ch==null)?'':engageclass_ch;
        fm.all('Contact').value= arrResult[0][15];
        fm.all('FixFlag1').value= arrResult[0][16];
        
       
        //fm.all('DoctTitCode').value= arrResult[0][5]; 
        ////fm.all('TechPostCode').value= arrResult[0]3];//此处有问题，本来应该有这句，但是加上后会出现错误，去掉后错误消失，可能是这个值不能为空
        //fm.all('MakeTime').value= arrResult[0][12];
        //fm.all('FixFlag').value= arrResult[0][13];
        //if(arrResult[0][13]=="1"){fm.all('FixFlag_ch').value="专职";gangwei.style.display = '';
        //fm.HospitName.style.display ='none';}
        //if(arrResult[0][13]=="2"){fm.all('FixFlag_ch').value="兼职";gangwei.style.display = '';} 
        //if(arrResult[0][13]=="3"){fm.all('FixFlag_ch').value="未签约";} 
        //
        //arrResult[0][14]=easyExecSql("select codename from ldcode where codetype='carreertype' and code='"+arrResult[0][14]+"'");
        //fm.all('Carreerclass_ch').value= (arrResult[0][14]==null)?'':arrResult[0][14];
        //fm.all('JobExplain').value= arrResult[0][15];
	      //fm.all('WorkPlace').value= arrResult[0][16];				
	      //fm.all('Contact').value= arrResult[0][17];	
						

//        fm.all('DoctStartTime').value= arrResult[0][9];
//        fm.all('PartTimeFlag').value= arrResult[0][10];
//        fm.all('DoctPhone').value= arrResult[0][11];
//        fm.all('DoctEmail').value= arrResult[0][12];
//        fm.all('SpecialFLag').value= arrResult[0][13];
        
//        fm.all('DiagTime').value= arrResult[0][15];
        
//        fm.all('CExportFlag').value= arrResult[0][17];
//        fm.all('ModifyDate').value= arrResult[0][18];
	}
}               
        
function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == "FixFlag")
	{
		if(Field.value == "1")
		{
			gangwei.style.display = '';
			fm.HospitName.style.display ='none';
			fm.HospitCode.value = "  ";
			fm.HospitName.value = "";
		}
	  if(Field.value == "2")
	  {
	  	gangwei.style.display = '';
	  	fm.HospitName.style.display ='';
//	  	fm.HospitCode.value = "";
	  }
	  if(Field.value == "3")
	  {
	  	gangwei.style.display = 'none';
	  	fm.HospitName.style.display ='';
//	  	fm.HospitCode.value = "";
	  }
	}
	

	
}

function TechSpecDescriptionShow()
{
	 divTechSpecDescription.style.display='';
	 fm.all('TechSpecDescription').value=fm.all('TechSpec').value;	
}

function backTechSpecDescription()
{
		var keycode=event.keyCode;
		if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('TechSpec').value=fm.all('TechSpecDescription').value;
		fm.all('TechSpecDescription').value="";
		divTechSpecDescription.style.display='none';		
	}
}

function backTechSpecDescriptionButton()
{
		fm.all('TechSpec').value=fm.all('TechSpecDescription').value;
		fm.all('TechSpecDescription').value="";
		divTechSpecDescription.style.display='none';	
}

function getNo()
{
    fm.all('DoctNo').value = "";
   // alert(fm.all('DoctNo').value);
    var sql="select max(to_number(DoctNo)) from lddoctor ";
    //alert(sql);
    var result = easyExecSql(sql);
    var result2=parseInt(result)+1;
    var result3=result2.toString();
    var result4;
    
    if(result3.length==9)
    {
      result4=result3.toString();
    }
    else if(result3.length==8)
    {
    	result4="0"+result3.toString();
    }
    else if(result3.length==7)
    {
    	result4="00"+result3.toString();
    }
    else if(result3.length==6)
    {
    	result4="000"+result3.toString();
    }
    else if(result3.length==5)
    {
    	result4="0000"+result3.toString();
    }
    else if(result3.length==4)
    {
    	result4="00000"+result3.toString();
    }
    else if(result3.length==3)
    {
    	result4="000000"+result3.toString();
    }
    else if(result3.length==2)
    {
    	result4="0000000"+result3.toString();
    }
    else if(result3.length==1)
    {
    	result4="00000000"+result3.toString();
    }
    if(result==""||result==null|result=="null")
    {
       fm.DoctNo.value=fm.all('DoctNo').value+"00000000"+"1";
       return false;
    }
    else
    {
	   	fm.DoctNo.value=fm.all('DoctNo').value+result4;	   
	}
}