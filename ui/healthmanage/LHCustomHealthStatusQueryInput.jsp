<%
//程序名称：LHCustomHealthStatusInput.jsp
//程序功能：功能描述
//创建日期：2005-01-18 10:11:20
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHCustomHealthStatusQueryInput.js"></SCRIPT> 
  <%@include file="LHCustomHealthStatusQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
<Div  id= "divLHCustomHealthStatusGrid1" style= "display: ''">    
 <table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      客户号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerNo >
    </TD>
    <TD  class= title>
      序号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HealthNo >
    </TD>
        <TD  class= title>
      增加日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=AddDate >
    </TD>
  </TR>

<TR  class= common>
    <TD  class= title>
      血压舒张压（mmHg）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BloodPressHigh >
    </TD>
    <TD  class= title>
      血压收缩压（mmHg）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=BloodPressLow >
    </TD>
  </TR>
  
  
  <TR  class= common>
    <TD  class= title>
      吸烟（支/日）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Smoke >
    </TD>
    <TD  class= title>
      饮酒（两/周）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=KissCup >
    </TD>
  </TR>
  
  
  <TR  class= common>
    <TD  class= title>
      熬夜（次/月）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SitUp >
    </TD>
    <TD  class= title>
      饮食不规律（餐/周）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiningNoRule >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      不良嗜好
    </TD>
       <TD  class= input>
        <Input class= 'common' name=BadHobby >
       </TD>


  
</table>
  </Div>
  <div id="div1" style="display: 'none'">
  <table>
    <TR  class= common>
      <TD  class= title>
      身高（m）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Stature >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      体重（kg）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Avoirdupois >
    </TD>
    <TD  class= title>
      体重指数（kg/m2）
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AvoirdIndex >
    </TD>
  </TR>
  
    
    <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
  </TR>
  </table>
  </div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomHealthStatus1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  
  
  <!-- 信息（列表） -->
	<Div id="divLHCustomHealthStatus1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHCustomHealthStatusGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
