<%
//程序名称：LHServerPriceInputInit.jsp
//程序功能：服务价格表设置
//创建日期：2006-02-28 17:50:48
//创建人  ：GuoLiying
//CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     String no = request.getParameter("ContraNo");
     String itemno = request.getParameter("ContraItemNo");
     String groupunit=request.getParameter("groupunit");
     System.out.println("#######"+no);
     System.out.println("###########"+itemno);
     System.out.println("############"+groupunit);
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ContraNo').value = "";
    fm.all('ContraName').value = "";
    fm.all('HospitCode').value = "";
    fm.all('HospitName').value = "";
    fm.all('ContraType').value = "";
    fm.all('ContraType_ch').value = "";
    fm.all('ContraNowName').value = "";
    fm.all('ContraNowName_ch').value = "";
    fm.all('ContraNowState_ch').value = "";
    fm.all('ServicePriceName').value="";
    fm.all('BalanceTypeName').value="";
	  fm.all('ContraNowState').value="";
	  fm.all('ContraItemNo').value="";
	  fm.all('HiddenBtn').value="<%=groupunit%>";

  }
  catch(ex)
  {
    alert("在LHServerPriceInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    fm.all('querybutton').disabled=true;
    fm.all('deleteButton').disabled=true;
		var flag = "<%=groupunit%>";

    if(flag=="1")//团体服务进入此页面
    {
       getFirstPageNo();
    }
    if(flag=="2")//个人服务进入此页面
    {
   	  getFirstPageNo2();
    }
    if(flag=="6")//从医疗机构检索中查询进入此页面
    {
    	  fm.all('querybutton').disabled=true;
        fm.all('deleteButton').disabled=true;
        fm.all('modifyButton').disabled=true;
        fm.all('saveButton').disabled=true;
        getFirstPageNo();
    }

 //   initTestItemPriceGrid(); 
 //   initTestGrpPriceGrid(); 
  }
  catch(re)
  {
    alert("LHServerPriceInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}  


function getFirstPageNo()
{  
  try                 
  {

		     var sql="	select a.ContraNo, a.ContraName,b.HospitName,"
				 +" (select c.Codename from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
				 +" (select d.DutyItemName from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode), "
		     +" ( case when a.ContraState = '1' then '有效' else '无效' end),"	 
		     +"  e.ContraItemNo ,b.HospitCode, "
		     +"(select c.code from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
		     +"(select d.DutyItemCode from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode),"
		     +" a.ContraState "
				 +" from   LHGroupcont a,LDhospital b, lhcontitem e "
				 +" where  a.ContraNo='"+ "<%=no%>" +"' "
				 +"	and    a.hospitcode = b.hospitcode "
				 +" and    e.ContraItemNo='"+ "<%=itemno%>" +"' "
			   +" and    a.ContraNo=e.ContraNo" 
		     ;
		     //alert(sql);
		     var arrResult = easyExecSql(sql);
		    // alert(arrResult[0][6]);
		     	fm.all('ContraNo').value				  = arrResult[0][0];
		     	fm.all('ContraName').value			  = arrResult[0][1];
		      fm.all('HospitName').value        = arrResult[0][2];
		      fm.all('ContraType_ch').value 		= arrResult[0][3];
		      fm.all('ContraNowName_ch').value	= arrResult[0][4];
		      fm.all('ContraNowState_ch').value	= arrResult[0][5];
          fm.all('ContraItemNo').value      = arrResult[0][6];//系统流水号码
          fm.all('HospitCode').value        = arrResult[0][7];//合同合作机构编号
          fm.all('ContraType').value        = arrResult[0][8];//合同责任类型编号
          fm.all('ContraNowName').value     = arrResult[0][9];//合同责任项目编号
          fm.all('ContraNowState').value    = arrResult[0][10];//合同责任项目编号
         
          var sqlType="select ServPriceType from LHContServPrice "
                      +"where contrano='"+ "<%=no%>" +"' and ContraItemNo='"+ "<%=itemno%>" +"'";
         // alert(sqlType);
         // alert(easyExecSql(sqlType));
          fm.ServicePriceCode.value=easyExecSql(sqlType);
         //alert(fm.ServicePriceCode.value);
          if(fm.all('ContraType').value=="01")
          {
          	divTestServPrice.style.display='';
          }
          if(fm.all('ContraType').value== "02"&&fm.ServicePriceCode.value=="null"||fm.ServicePriceCode.value==null||fm.ServicePriceCode.value=="")//核保体检 && fm.all('ContraNowName').value== "HB0001"
		      {
		      	//alert("2");
		      	fm.ServicePriceCode.value='2';
		      	fm.ServicePriceName.value='核保体检价格';
		      	//alert(fm.ServicePriceName.value);
		        fm.BalanceTypeCode.value='5';
		      	fm.BalanceTypeName.value='核保体检费用';
		      	divTestMoney.style.display='none';
		      	divTestItemPriceGrid.style.display='';
		      	divTestGrpPriceGrid.style.display='';
		      	divTestMoney2.style.display='none';
		      	divShowInfo.style.display='';
		      	initTestItemPriceGrid(); 
            initTestGrpPriceGrid(); 
		      }
          else
          {
                var sql2Type="select a.ServPriceType,a.ExpType,"
                            //+"(select  case when a.ServPriceType = '1' then '服务单价' else '体检价格' end from dual),"
					      			      +"( case a.ServPriceType when '2' then '核保体检价格' when '1' then '按次计费价格(元/次)' when '3' then '按人计费价格(元/人)' when '4' then '按人包干价格' else '未知价格' end ),"
					      			      +"( case a.ExpType when '1' then '健康体检费用' when '2' then '健康管理技术费用' when '3' then '医师费用' when '4' then '其他费用' when '5' then '核保体检费用' else '未知费用' end )"
                            +" from LHContServPrice a " 
                            +"where a.contrano='"+ "<%=no%>" +"'  and a.ContraItemNo='"+ "<%=itemno%>" +"'";
                var arrResult2 = new Array();
                arrResult2 = easyExecSql(sql2Type);		
                if(arrResult2!="null"&&arrResult2!=""&&arrResult2!=null)
                {
                  fm.ServicePriceCode.value=arrResult2[0][0];
                  fm.BalanceTypeCode.value=arrResult2[0][1];
                  fm.ServicePriceName.value=arrResult2[0][2];
                  fm.BalanceTypeName.value=arrResult2[0][3];
                }
               // alert(fm.ServicePriceCode.value);
                if(fm.ServicePriceCode.value=="2")
				        {
					      	  divTestMoney.style.display='none';
					      		divTestItemPriceGrid.style.display='';
					      		divTestGrpPriceGrid.style.display='';
					      		 divShowInfo.style.display='';
					      		initTestItemPriceGrid(); 
                    initTestGrpPriceGrid(); 
					      		 var itemPriceSql= "select a.MedicaItemCode,"
					                       +"  CASE  WHEN "
 	                               +"(select b.MedicaItemCode from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) not like 'ZH%%' "
     			      								 +" THEN (select b.MedicaItemname from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) "
                                 +" ELSE (select distinct c.Testgrpname from ldtestgrpmgt c where c.testgrpcode = a.MedicaItemCode ) END ,"
					                       +" MedicaItemPrice ,Explain,PriceClass,SerialNo "
					                       +" from LDTestPriceMgt a "
		                             +" where a.contrano='"+ "<%=no%>" +"' and a.ContraItemNo='"+ "<%=itemno%>" +"'"
		                             +" and a.PriceClass='JCXM'";
		                      turnPage2.pageLineNum = 100;
				              turnPage2.queryModal(itemPriceSql, TestItemPriceGrid);

		                  if(fm.all('ContraType').value=="02")
				              {
				               var testGrpPriceSql= "select a.MedicaItemCode,"
					                       +" (select distinct b.Testgrpname from ldtestgrpmgt b where b.Testgrpcode = a.MedicaItemCode and  testgrptype='1' ) ,"
					                       +" a.MedicaItemPrice ,a.Explain,PriceClass,SerialNo "
					                       +" from LDTestPriceMgt  a"
		                             +" where a.contrano='"+ "<%=no%>" +"' and a.ContraItemNo='"+ "<%=itemno%>" +"'"
		                             +" and a.PriceClass='TJTC'";
		                  }
		                  //alert(testGrpPriceSql);
		                  turnPage3.pageLineNum = 100;
		                  turnPage3.queryModal(testGrpPriceSql, TestGrpPriceGrid);
                 
				        }
				        //alert(fm.ServicePriceCode.value);
				        if(fm.ContraType.value=="01")
				        {
				        	  var MedicaItemCode= "select a.MedicaItemCode,"
					               +" (select distinct b.Testgrpname from ldtestgrpmgt b where b.Testgrpcode = a.MedicaItemCode ) "
					               //+" PriceClass"
					               +" from LHContServPrice  a"
		                     +" where a.contrano='"+ "<%=no%>" +"' and a.ContraItemNo='"+ "<%=itemno%>" +"'";
		                     //+" and a.PriceClass='TJTC'";

				        		var arrResult4=new Array;
					      		 arrResult4 = easyExecSql(MedicaItemCode);	
					      		 		 		if(arrResult4!="null"&&arrResult4!=""&&arrResult4!=null) 
					      				 		{ 
					      				 			fm.all('MedicaItemCode').value   = arrResult4[0][0];
					      		          fm.all('MedicaItemName').value   = arrResult4[0][1];	
					      		        }
				        }
				        if(fm.ServicePriceCode.value=="1")
				        {
				        	  divTestMoney.style.display='';
					      		divTestItemPriceGrid.style.display='none';
					      		divTestGrpPriceGrid.style.display='none';
					      		divShowInfo.style.display='none';
					      		var mulSql3 = "select a.ContraNo ,"
					      						            +"a.ContraItemNo ,"
					      			                  +"a.MoneySum ,a.ExplanOther"
					      			                  +" from LHContServPrice a "
					      		                    +" where a.contrano ='"+ "<%=no%>" +"' "
					      		                    +"  and a.ContraItemNo ='"+ "<%=itemno%>" +"'";
					      		        // alert(mulSql3);
					      		        // alert(easyExecSql(mulSql3));
					      		         var arrResult3=new Array;
					      				 		arrResult3 = easyExecSql(mulSql3);		
					      				 		if(arrResult3!="null"&&arrResult3!=""&&arrResult3!=null) 
					      				 		{ 
					      		          fm.all('MoneySum').value              = arrResult3[0][2];	
					      		          fm.all('ExplanOther').value              = arrResult3[0][3];	
					      		          
					      		        }
				        }
				        if(fm.ServicePriceCode.value=="3")
				        {
				        	  divTestMoney2.style.display='';
					      		divTestItemPriceGrid.style.display='none';
					      		divTestGrpPriceGrid.style.display='none';
					      		divShowInfo.style.display='none';
					      		var mulSql3 = "select a.ContraNo ,"
					      						            +"a.ContraItemNo ,"
					      			                  +"a.MoneySum ,a.ServPros,a.ExplanOther,a.MedicaItemCode"
					      			                  +" from LHContServPrice a "
					      		                    +" where a.contrano ='"+ "<%=no%>" +"' "
					      		                    +"  and a.ContraItemNo ='"+ "<%=itemno%>" +"'";
					      		 var arrResult3=new Array;
					      				 		arrResult3 = easyExecSql(mulSql3);		
					      				 		if(arrResult3!="null"&&arrResult3!=""&&arrResult3!=null) 
					      				 		{ 
					      		          fm.all('MoneySum2').value              = arrResult3[0][2];	
					      		          fm.all('ServPros2').value              = arrResult3[0][3];	
					      		          fm.all('ExplanOther2').value           = arrResult3[0][4];	
					      		        }
					         
				        }
				        if(fm.ServicePriceCode.value=="4")
				        {
				        	  divTestMoney3.style.display='';
					      		divTestItemPriceGrid.style.display='none';
					      		divTestGrpPriceGrid.style.display='none';
					      		divShowInfo.style.display='none';
					      		var mulSql3 = "select a.ContraNo ,"
					      						            +"a.ContraItemNo ,"
					      			                  +"a.MoneySum ,a.ServPros,a.MaxServNum,a.ExplanOther,a.MedicaItemCode"
					      			                  +" from LHContServPrice a "
					      		                    +" where a.contrano ='"+ "<%=no%>" +"' "
					      		                    +"  and a.ContraItemNo ='"+ "<%=itemno%>" +"'";
					      		                    //alert(mulSql3);
					      		                    //alert(easyExecSql(mulSql3));
					      		 var arrResult3=new Array;
					      				 		arrResult3 = easyExecSql(mulSql3);
					      				 				
					      				 		if(arrResult3!="null"&&arrResult3!=""&&arrResult3!=null) 
					      				 		{ 
					      		          fm.all('MoneySum3').value              = arrResult3[0][2];	
					      		          fm.all('ServPros3').value              = arrResult3[0][3];	
					      		          fm.all('MaxServNum3').value            = arrResult3[0][4];	
					      		          fm.all('ExplanOther3').value           = arrResult3[0][5];	
					      		        }
					         
				        }
            }       

 
  }
  catch(ex)
  {
    alert("在LHServerPriceInputInit.jsp-->getFirstPageNo 函数中发生异常:初始化界面错误11111!");
  }
}

function getFirstPageNo2()
{  
  try                 
  {

		     var sql="	select a.ContraNo, a.ContraName,b.DoctName,"
				 +" (select c.Codename from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
				 +" (select d.DutyItemName from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode), "
		     +" ( case when a.ContraState = '1' then '有效' else '无效' end),	"	 
		     +"  e.ContraItemNo,b.Doctno, "
		     +"(select c.code from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
		     +"(select d.DutyItemCode from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode),"
		     +" a.ContraState "
				 +"	from   LHUnitContra a,LDDoctor b, lhcontitem e  "
				 +" where  a.ContraNo='"+ "<%=no%>" +"' "
				 +"	and    a.DoctNo = b.DoctNo "
				 +" and    e.ContraItemNo='"+ "<%=itemno%>" +"' "
			   +" and    a.ContraNo=e.ContraNo" 
			       
		      var arrResult = easyExecSql(sql);
		     	fm.all('ContraNo').value				  = arrResult[0][0];
		     	fm.all('ContraName').value			  = arrResult[0][1];
		      fm.all('HospitName').value        = arrResult[0][2];
		      fm.all('ContraType_ch').value 		= arrResult[0][3];
		      fm.all('ContraNowName_ch').value	= arrResult[0][4];
		      fm.all('ContraNowState_ch').value	= arrResult[0][5];if(arrResult[0][5]=="1"){fm.all('ContraState_ch').value="有效";}if(arrResult[0][6]=="2"){fm.all('ContraState_ch').value="无效";}//else{fm.all('ContraState_ch').value="";}alert(fm.all('ContraState_ch').value);
          fm.all('ContraItemNo').value      = arrResult[0][6];//系统流水号码
          fm.all('HospitCode').value        = arrResult[0][7];//合同合作机构编号
          fm.all('ContraType').value        = arrResult[0][8];//合同责任类型编号
          fm.all('ContraNowName').value     = arrResult[0][9];//合同责任项目编号
          fm.all('ContraNowState').value    = arrResult[0][10];//合同责任项目编号
          
          
            var sqlType="select ServPriceType from LHContServPrice "
                      +"where contrano='"+ "<%=no%>" +"' and ContraItemNo='"+ "<%=itemno%>" +"'";
          fm.ServicePriceCode.value=easyExecSql(sqlType);
         // alert(fm.ServicePriceCode.value);
          if(fm.all('ContraType').value=="01")
          {
          	divTestServPrice.style.display='';
          }
          if(fm.all('ContraType').value== "02"&&fm.ServicePriceCode.value=="null"||fm.ServicePriceCode.value==null||fm.ServicePriceCode.value=="")//核保体检 && fm.all('ContraNowName').value== "HB0001"
		      {
		      	//alert("2");
		      	fm.ServicePriceCode.value='2';
		      	fm.ServicePriceName.value='核保体检价格';
		      	//alert(fm.ServicePriceName.value);
		        fm.BalanceTypeCode.value='5';
		      	fm.BalanceTypeName.value='核保体检费用';
		      	divTestMoney.style.display='none';
		      	divTestItemPriceGrid.style.display='';
		      	divTestGrpPriceGrid.style.display='';
		      	divTestMoney2.style.display='none';
		      	divShowInfo.style.display='';
		      	initTestItemPriceGrid(); 
            initTestGrpPriceGrid(); 
		      }
          else
          {
          var sql2Type="select a.ServPriceType,a.ExpType,"
                		      +"( case a.ServPriceType when '2' then '核保体检价格' when '1' then '按次计费价格(元/次)' when '3' then '按人计费价格(元/人)' when '4' then '按人包干价格' else '未知价格' end ),"
					      			    +"( case a.ExpType when '1' then '健康体检费用' when '2' then '健康管理技术费用' when '3' then '医师费用' when '4' then '其他费用' when '5' then '核保体检费用' else '未知费用' end )"
                          +" from LHContServPrice a " 
                          +"where a.contrano='"+ "<%=no%>" +"'  and a.ContraItemNo='"+ "<%=itemno%>" +"'";
          var arrResult2 = new Array();
          arrResult2 = easyExecSql(sql2Type);		
           if(arrResult2!="null"&&arrResult2!=""&&arrResult2!=null)
           {
             fm.ServicePriceCode.value=arrResult2[0][0];
             fm.BalanceTypeCode.value=arrResult2[0][1];
             fm.ServicePriceName.value=arrResult2[0][2];
             fm.BalanceTypeName.value=arrResult2[0][3];
           }
         // alert(fm.ServicePriceCode.value);
          if(fm.ServicePriceCode.value=="2")
				 {
					    			      	  divTestMoney.style.display='none';
					    			      		divTestItemPriceGrid.style.display='';
					    			      		divTestGrpPriceGrid.style.display='';
					    			      		 divShowInfo.style.display='';
					    			      		initTestItemPriceGrid(); 
              initTestGrpPriceGrid(); 
					    			      		 var itemPriceSql= "select a.MedicaItemCode,"
					    			                       +"  CASE  WHEN "
 	                         +"(select b.MedicaItemCode from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) not like 'ZH%%' "
     			    			      								 +" THEN (select b.MedicaItemname from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) "
                           +" ELSE (select distinct c.Testgrpname from ldtestgrpmgt c where c.testgrpcode = a.MedicaItemCode ) END ,"
					    			                       +" MedicaItemPrice ,Explain,PriceClass,SerialNo "
					    			                       +" from LDTestPriceMgt a "
		                       +" where a.contrano='"+ "<%=no%>" +"' and a.ContraItemNo='"+ "<%=itemno%>" +"'"
		                       +" and a.PriceClass='JCXM'";
		                turnPage2.pageLineNum = 100;
				        turnPage2.queryModal(itemPriceSql, TestItemPriceGrid);
				        

		                  if(fm.all('ContraType').value=="02")
				              {
				               var testGrpPriceSql= "select a.MedicaItemCode,"
					                       +" (select distinct b.Testgrpname from ldtestgrpmgt b where b.Testgrpcode = a.MedicaItemCode and  testgrptype='1') ,"
					                       +" a.MedicaItemPrice ,a.Explain,PriceClass,SerialNo "
					                       +" from LDTestPriceMgt  a"
		                             +" where a.contrano='"+ "<%=no%>" +"' and a.ContraItemNo='"+ "<%=itemno%>" +"'"
		                             +" and a.PriceClass='TJTC'";
		                  }
		            turnPage3.pageLineNum = 100;
		            turnPage3.queryModal(testGrpPriceSql, TestGrpPriceGrid);
           
				  }
				  			if(fm.ContraType.value=="01")
				        {
				        	  var MedicaItemCode= "select a.MedicaItemCode,"
					               +" (select distinct b.Testgrpname from ldtestgrpmgt b where b.Testgrpcode = a.MedicaItemCode ) "
					               //+" PriceClass"
					               +" from LHContServPrice  a"
		                     +" where a.contrano='"+ "<%=no%>" +"' and a.ContraItemNo='"+ "<%=itemno%>" +"'";
		                     //+" and a.PriceClass='TJTC'";

				        		var arrResult4=new Array;
					      		 arrResult4 = easyExecSql(MedicaItemCode);	
					      		 		 		if(arrResult4!="null"&&arrResult4!=""&&arrResult4!=null) 
					      				 		{ 
					      				 			fm.all('MedicaItemCode').value   = arrResult4[0][0];
					      		          fm.all('MedicaItemName').value   = arrResult4[0][1];	
					      		        }
				        }
				  if(fm.ServicePriceCode.value=="1")
				  {
				  	  divTestMoney.style.display='';
					    			      		divTestItemPriceGrid.style.display='none';
					    			      		divTestGrpPriceGrid.style.display='none';
					    			      		 divShowInfo.style.display='none';
					    			      		var mulSql3 = "select a.ContraNo ,"
					    			      						            +"a.ContraItemNo ,"
					    			      			                  +"a.MoneySum,a.ExplanOther"
					    			      			                  +" from LHContServPrice a "
					    			      		                    +" where a.contrano ='"+ "<%=no%>" +"' "
					    			      		                    +"  and a.ContraItemNo ='"+ "<%=itemno%>" +"'";
					    			      		        // alert(mulSql3);
					    			      		        // alert(easyExecSql(mulSql3));
					    			      		         var arrResult3=new Array;
					    			      				 		arrResult3 = easyExecSql(mulSql3);	
					    			      				 		if(arrResult3!="null"&&arrResult3!=""&&arrResult3!=null)
                                      {	  
					    			      		          fm.all('MoneySum').value              = arrResult3[0][2];	
					    			      		          fm.all('ExplanOther').value              = arrResult3[0][3];
					    			      		          
					    			      		        }
				   }
				   			if(fm.ServicePriceCode.value=="3")
				        {
				        	  divTestMoney2.style.display='';
					      		divTestItemPriceGrid.style.display='none';
					      		divTestGrpPriceGrid.style.display='none';
					      		divShowInfo.style.display='none';
					      		var mulSql3 = "select a.ContraNo ,"
					      						            +"a.ContraItemNo ,"
					      			                  +"a.MoneySum ,a.ServPros,a.ExplanOther,a.MedicaItemCode"
					      			                  +" from LHContServPrice a "
					      		                    +" where a.contrano ='"+ "<%=no%>" +"' "
					      		                    +"  and a.ContraItemNo ='"+ "<%=itemno%>" +"'";
					      		 var arrResult3=new Array;
					      				 		arrResult3 = easyExecSql(mulSql3);		
					      				 		if(arrResult3!="null"&&arrResult3!=""&&arrResult3!=null) 
					      				 		{ 
					      		          fm.all('MoneySum2').value              = arrResult3[0][2];	
					      		          fm.all('ServPros2').value              = arrResult3[0][3];	
					      		          
					      		          fm.all('ExplanOther2').value           = arrResult3[0][4];	
					      		         // fm.all('MedicaItemCode').value         = arrResult3[0][6];	
					      		        }
					         
				        }
				        if(fm.ServicePriceCode.value=="4")
				        {
				        	  divTestMoney3.style.display='';
					      		divTestItemPriceGrid.style.display='none';
					      		divTestGrpPriceGrid.style.display='none';
					      		divShowInfo.style.display='none';
					      		var mulSql3 = "select a.ContraNo ,"
					      						            +"a.ContraItemNo ,"
					      			                  +"a.MoneySum ,a.ServPros,a.MaxServNum,a.ExplanOther,a.MedicaItemCode"
					      			                  +" from LHContServPrice a "
					      		                    +" where a.contrano ='"+ "<%=no%>" +"' "
					      		                    +"  and a.ContraItemNo ='"+ "<%=itemno%>" +"'";
					      		 var arrResult3=new Array;
					      				 		arrResult3 = easyExecSql(mulSql3);		
					      				 		if(arrResult3!="null"&&arrResult3!=""&&arrResult3!=null) 
					      				 		{ 
					      		          fm.all('MoneySum3').value              = arrResult3[0][2];	
					      		          fm.all('ServPros3').value              = arrResult3[0][3];	
					      		          fm.all('MaxServNum3').value            = arrResult3[0][4];	
					      		          fm.all('ExplanOther3').value           = arrResult3[0][5];	
					      		         // fm.all('MedicaItemCode').value         = arrResult3[0][6];	
					      		        }
					         
				        }
         }
  }
  catch(ex)
  {
    alert("在LHServerPriceInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误2!");
  }
}

//var TestItemPriceGrid;
function initTestItemPriceGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="检查项目代码";   
		iArray[1][1]="150px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;													//选‘2’背景色为蓝色
		iArray[1][4]="hmtestgrpquery";           //CodeQueryBL中的引用参数                 
		iArray[1][5]="1|2";     								//引用代码对应第几列，'|'为分割符
		iArray[1][6]="0|1";    									//上面的列中放置引用代码中第几位值
		iArray[1][9]="检查项目代码|LEN<20";                      
		iArray[1][15]="code";         //数据库中字段，用于模糊查询                 
		iArray[1][17]="1";                                       
		iArray[1][19]="1" ;                                      
		
		
		iArray[2]=new Array(); 
		iArray[2][0]="检查项目名称";   
		iArray[2][1]="200px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;   
		iArray[2][4]="hmtestgrpquery";                           
		iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
		iArray[2][6]="1|0";    //上面的列中放置引用代码中第几位值
		iArray[2][15]="name";                          
		iArray[2][17]="2";                                       
		iArray[2][19]="1" ;  
		iArray[2][18]=325 ;                                          
		
		
		
		iArray[3]=new Array(); 
		iArray[3][0]="检查项目单价（元）";   
		iArray[3][1]="130px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="补充说明";
		iArray[4][1]="170px";         
		iArray[4][2]=20;            
		iArray[4][3]=1;
		
		iArray[5]=new Array();
		iArray[5][0]="价格类别";
		iArray[5][1]="0px";         
		iArray[5][2]=20;            
		iArray[5][3]=3;
		iArray[5][14]="JCXM";
		
		iArray[6]=new Array(); 
		iArray[6][0]="SerialNo";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3; 
		             
		
    TestItemPriceGrid = new MulLineEnter( "fm" , "TestItemPriceGrid" ); 
    //这些属性必须在loadMulLine前

    TestItemPriceGrid.mulLineCount = 0;   
    TestItemPriceGrid.displayTitle = 1;
    TestItemPriceGrid.hiddenPlus = 0;
    TestItemPriceGrid.hiddenSubtraction = 0;
    TestItemPriceGrid.canSel = 0;
    TestItemPriceGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    TestItemPriceGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //TestItemPriceGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
//var TestGrpPriceGrid;
function initTestGrpPriceGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="体检套餐代码";   
		iArray[1][1]="150px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
		iArray[1][9]="体检套餐代码|LEN<60"
		
		iArray[2]=new Array(); 
		iArray[2][0]="体检套餐名称";   
		iArray[2][1]="200px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		if(fm.all('ContraType').value=="01")
		{ 
		 iArray[2][4]="hmchoosetest";  
	  }  
	  if(fm.all('ContraType').value=="02")
		{ 
		 iArray[2][4]="hmchoosetest2";  
	  }                        
		iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
		iArray[2][6]="0|1";    //上面的列中放置引用代码中第几位值
		iArray[2][18]=325;
		
		iArray[3]=new Array(); 
		iArray[3][0]="体检套餐总价格（元）";   
		iArray[3][1]="130px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="补充说明";
		iArray[4][1]="170px";         
		iArray[4][2]=20;            
		iArray[4][3]=1;    
		
		iArray[5]=new Array();  
		iArray[5][0]="价格类别";         
		iArray[5][1]="0px";    
    iArray[5][2]=20;       
    iArray[5][3]=3;         //这些属性必须在loadMulLine前
		iArray[5][14]="TJTC";
		
		iArray[6]=new Array(); 
		iArray[6][0]="SerialNo2";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3; 

    TestGrpPriceGrid = new MulLineEnter( "fm" , "TestGrpPriceGrid" ); 
    TestGrpPriceGrid.mulLineCount = 0;   
    TestGrpPriceGrid.displayTitle = 1;
    TestGrpPriceGrid.hiddenPlus = 0;
    TestGrpPriceGrid.hiddenSubtraction = 0;
    TestGrpPriceGrid.canSel = 0;
    TestGrpPriceGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    TestGrpPriceGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //TestGrpPriceGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
      
</script>
  