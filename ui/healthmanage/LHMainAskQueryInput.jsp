<%
//程序名称：LLMainAskInput.jsp
//程序功能：功能描述
//创建日期：2005-01-12 16:10:37
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHMainAskQueryInput.js"></SCRIPT> 
  <%@include file="LHMainAskQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLLMainAskGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      登记号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=LogNo >
    </TD>
    <TD  class= title>
      登记状态
    </TD>
    <TD  class= input>
      <Input class= 'common' name=LogState >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      登记类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AskType >
    </TD>
    <TD  class= title>
      其他对应号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OtherNo >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      其它对应号码类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OtherNoType >
    </TD>
    <TD  class= title>
      登记方式
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AskMode >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      登记人客户号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerNo >
    </TD>
    <TD  class= title>
      登记人姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=LogName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      登记人单位名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=LogComp >
    </TD>
    <TD  class= title>
      单位客户号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=LogCompNo >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      登记日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=LogDate >
    </TD>
    <TD  class= title>
      登记时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=LogTime >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      登记人电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Phone >
    </TD>
    <TD  class= title>
      登记人手机
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Mobile >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      登记人邮政编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PostCode >
    </TD>
    <TD  class= title>
      登记人通讯地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AskAddress >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      登记人电邮
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Email >
    </TD>
    <TD  class= title>
      答疑类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AnswerType >
    </TD>
  </TR>
  
</table>
  </Div>
  <div id= "div1" style="display: 'none'">
  	<table>
	  	<TR  class= common>
	    <TD  class= title>
	      答复方式
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=AnswerMode >
	    </TD>
	    <TD  class= title>
	      是否邮寄资料
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=SendFlag >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      转入部门
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=SwitchCom >
	    </TD>
	    <TD  class= title>
	      转入日期
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=SwitchDate >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      转入时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=SwitchTime >
	    </TD>
	    <TD  class= title>
	      答复完成时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=ReplyFDate >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      处理完成时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=DealFDate >
	    </TD>
	    <TD  class= title>
	      备注
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=Remark >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      登记有效标志
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=AvaiFlag >
	    </TD>
	    <TD  class= title>
	      登记无效原因
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=NotAvaliReason >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      操作员
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=Operator >
	    </TD>
	    <TD  class= title>
	      管理机构
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MngCom >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      入机日期
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MakeDate >
	    </TD>
	    <TD  class= title>
	      入机时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MakeTime >
	    </TD>
	  </TR>
	  <TR  class= common>
	    <TD  class= title>
	      最后一次修改日期
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=ModifyDate >
	    </TD>
	    <TD  class= title>
	      最后一次修改时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=ModifyTime >
	    </TD>
	  </TR>
	 </table>
	</div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLMainAsk1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLLMainAsk1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLLMainAskGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
