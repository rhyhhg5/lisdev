<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：HealthArchiveInput.jsp
//程序功能：F1报表生成
//创建日期：2005-07-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
    
%>
<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="HealthArchiveInput.js"></SCRIPT>   
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>

<body>    
  <form method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>          
          <TD  class=title>选择客户</TD>
          <TD  class=input><input name=CustomerNo class=codeno style="width:45%;" ondblclick="showCodeList('hmldperson',[this,CustomerNo_ch],[0,1],null,fm.CustomerNo.value,'CustomerNo',1) " ><Input name=CustomerNo_ch class=codename style="width:40%;background-color: #D7E1F6;" ondblclick=" showCodeList('hmldperson',[CustomerNo,this],[0,1],null,fm.CustomerNo_ch.value,'Name',1) "  onkeyup=" if( event.Keycode ='13' ) showCodeList('hmldperson',[this],[0],null,fm.CustomerNo.value,'Name',1) "></TD>
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
    </table>
    <br>
		<INPUT VALUE="就诊档案打印" style="width:100px;" class="cssButton" TYPE="button" onclick="submitForm()">
		<INPUT VALUE="体检档案打印" class="cssButton" style="width:100px;" TYPE="button" onclick="TestPrint()"> 
		<INPUT VALUE="服务档案打印" class="cssButton" style="width:100px;" TYPE="button" onclick="ServPtint()"> 	
		<input type= hidden name=fmtransact>
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 