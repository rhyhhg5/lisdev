<%
    //程序名称：LHHealthServItemQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-03-16 10:49:43
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@ page import="com.sinosoft.lis.pubfun.*" %>
<script language="JavaScript">
    function initInpBox() {
        try {
            fm.all('HmMessCode').value = "";
            fm.all('HmMessName').value = "";
            fm.all('CodeTypeStep').value = "";
        }
        catch(ex) {
            alert("在LHMessManageQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
        }
    }
    function initForm() {
        try {
            initInpBox();
            initLHMessManageGrid();
        }
        catch(re) {
            alert("LHMessManageQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
        }
    }
    //领取项信息列表的初始化
    var LHMessManageGrid;
    function initLHMessManageGrid() {
        var iArray = new Array();

        try {
            iArray[0] = new Array();
            iArray[0][0] = "序号";
            iArray[0][1] = "30px";
            iArray[0][3] = 0;

            iArray[1] = new Array();
            iArray[1][0] = "健康通讯代码";
            iArray[1][1] = "60px";
            iArray[1][3] = 0;

            iArray[2] = new Array();
            iArray[2][0] = "健康通讯名称";
            iArray[2][1] = "90px";
            iArray[2][3] = 0;

            iArray[3] = new Array();
            iArray[3][0] = "代码分类等级";
            iArray[3][1] = "60px";
            iArray[3][3] = 1;

            iArray[4] = new Array();
            iArray[4][0] = "健康通讯内容";
            iArray[4][1] = "160px";
            iArray[4][3] = 0;


            iArray[5] = new Array();
            iArray[5][0] = "补充说明";
            iArray[5][1] = "160px";
            iArray[5][3] = 0;

    

            LHMessManageGrid = new MulLineEnter("fm", "LHMessManageGrid");
            //这些属性必须在loadMulLine前

            //    LHMessManageGrid.mulLineCount = 0;
            //    LHMessManageGrid.displayTitle = 1;
            LHMessManageGrid.hiddenPlus = 1;
            LHMessManageGrid.hiddenSubtraction = 1;
            LHMessManageGrid.canSel = 1;
            //    LHMessManageGrid.canChk = 0;
            //    LHMessManageGrid.selBoxEventFuncName = "showOne";

            LHMessManageGrid.loadMulLine(iArray);
            //这些操作必须在loadMulLine后面
            //LHMessManageGrid.setRowColData(1,1,"asdf");
        }
        catch(ex) {
            alert(ex);
        }
    }
</script>
