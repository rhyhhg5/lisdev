<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHStandardTaskDefine.jsp
//程序功能：
//创建日期：2006-07-29 14:55:39
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHStandardTaskDefine.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHStandardTaskDefineInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LHStandardTaskDefineSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHTaskDefine);">
    		</td>
    		 <td class= titleImg>
        		 标准服务任务定义
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHTaskDefine" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      标准任务编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=TaskNo  style="width:152px"  verify="标准服务任务编号|NOTNULL&len<=50"><font color=red> *</font>
    </TD>
    <TD  class= title>
      标准任务名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=TaskName style="width:152px"  verify="标准服务任务名称|NOTNULL&len<=50">
    </TD>  
	 <TD class= title >
		    功能模块设置
	 </TD>   		 
	 <TD  class= input>
		<Input class= 'codename' style="width:40px" name=ServTaskModule ><Input class= 'codeno' style="width:120px"  name=ServTaskModuleName ondblclick="showCodeList('funcmodule',[this,ServTaskModule],[1,0],null,null,null,'1',160);" codeClear(ServTaskModuleName,ServTaskModule);">
	</TD>
  </TR>
    <TR  class= common>
    <TD  class= title colspan='6'>
      服务任务说明
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= input colspan='6'>
      <textarea class= 'common' rows="3" cols="100" style="width:800px"  name=ServTaskDes verify="服务任务说明|len<=300" ></textarea>
    </TD>
  </TR>
</table>
    </Div>
     <Div id="divShowServTaskInfo" style="display:''">
	 <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHQueryDetailGrid);">
    	 	</td>
    	 <td class= titleImg>
        	 服务任务信息列表
       	</td>   		 		 
    	</tr>
    </table>
  <Div id="divLHQueryDetailGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHQueryDetailGrid">
					</span> 
		    </td>
			</tr>
		</table>
	</div>
</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
