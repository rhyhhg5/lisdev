<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="java.io.*"%>
<%
  System.out.println("------------------start------------------");
  CError cError = new CError();
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";

  String ManageCom = ""; //管理机构
  String MakeDate1 = ""; //入机日期,起始日期
  String MakeDate2 = ""; //入机日期,终止日期
  String FeeType = "";//费用类型
  String hospital = "";//给付一家医院或给付多家医院
  
  ManageCom = (String) request.getParameter("ManageCom");
  MakeDate1 = (String) request.getParameter("MakeDate1");
  MakeDate2 = (String) request.getParameter("MakeDate2");
  FeeType = (String) request.getParameter("FeeType");
  hospital = (String)request.getParameter("hospital");

  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("ManageCom",ManageCom);
  tTransferData.setNameAndValue("MakeDate1",MakeDate1);
  tTransferData.setNameAndValue("MakeDate2",MakeDate2);
  tTransferData.setNameAndValue("FeeType",FeeType);
  tTransferData.setNameAndValue("hospital",hospital);

  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");
  VData tVData = new VData();
  VData mResult = new VData();
  CErrors mErrors = new CErrors();
  tVData.addElement(tTransferData);
  tVData.addElement(tG);

  HospitalClaimBL tHospitalClaimBL = new HospitalClaimBL();
  XmlExport txmlExport = new XmlExport();
  if (!tHospitalClaimBL.submitData(tVData, "PRINT")) {
    operFlag = false;
    System.out.println("if_one");
    Content=tHospitalClaimBL.mErrors.getFirstError().toString();
  }
  else {
    System.out.println("if_two");
    mResult = tHospitalClaimBL.getResult();
    txmlExport = (XmlExport) mResult.getObjectByObjectName("XmlExport", 0);
    if (txmlExport == null) {
      System.out.println("if_three");
      operFlag = false;
      Content="没有得到要显示的数据文件";
    }
  }

  LDSysVarDB tLDSysVarDB = new LDSysVarDB();
  tLDSysVarDB.setSysVar("VTSFilePath");
  tLDSysVarDB.getInfo();
  String vtsPath = tLDSysVarDB.getSysVarValue();
  if (vtsPath == null)
  {
    vtsPath = "vtsfile/";
  }

  String filePath = application.getRealPath("/").replace('\\', '/') + "/" + vtsPath;
  String fileName = "TASK" + FileQueue.getFileName()+".vts";
  String realPath = filePath + fileName;

	if (operFlag==true)
	{
    //合并VTS文件
    String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);

    //把dataStream存储到磁盘文件
    AccessVtsFile.saveToFile(dataStream, realPath);
    response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath=" + realPath);
	}
	else
	{
    	 FlagStr = "Fail";

%>
<html>
<script language="javascript">
	alert("<%=Content%>");
	//top.close();
</script>
</html>
<%
  	}

%>
