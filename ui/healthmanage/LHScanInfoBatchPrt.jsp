<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： LHScanInfoBatchPrt.jsp
//程序功能：
//创建日期：2007-3-13
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.health.*"%>
<%@page import="java.lang.String"%>
<%
    boolean operFlag = true;
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "INSERT";
	String tPrintServerPath = "";
	int tCount=0;
	
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	tG=(GlobalInput)session.getValue("GI");
	//tG.ClientIP = request.getRemoteAddr(); 
	tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
	tG.ServerIP =tG.GetServerIP();

    String tOutXmlFile  = application.getRealPath("")+"\\";
	LDSysVarSchema	tLDSysVarSechma =new LDSysVarSchema();
	tLDSysVarSechma.setSysVarValue(tOutXmlFile);
	LHScanInfoSet tLHScanInfoSet = new LHScanInfoSet();

	try
  	{
		String tCustomerNo[] = request.getParameterValues("LHScanNoGrid1");
		String tSerialNo[] = request.getParameterValues("LHScanNoGrid3");
		String tScanType[] = request.getParameterValues("LHScanNoGrid4");
	    String tChk[] = request.getParameterValues("InpLHScanNoGridChk"); //参数格式=” Inp+MulLine对象名+Chk” 
	
		tCount = tSerialNo.length;
		System.out.println("----tCount:"+tCount+"-----");
		for(int j = 0; j < tCount; j++)
		{
				LHScanInfoSchema tLHScanInfoSchema = new LHScanInfoSchema();
				tLHScanInfoSchema.setSerialNo(tSerialNo[j]);
				tLHScanInfoSchema.setOtherNo(tCustomerNo[j]);
				tLHScanInfoSchema.setOtherNoType(tScanType[j]);
				tLHScanInfoSet.add(tLHScanInfoSchema);
				System.out.println("---------setSerialNo: "+tSerialNo[j]);
  		}
  		
  	
		// 准备传输数据 VData
  		VData tVData = new VData();
		tVData.add(tLHScanInfoSet);
		tVData.add(tLDSysVarSechma);
  		tVData.add(tG);
  		LHScanBatchPrtBL tLHScanBatchPrtBL = new LHScanBatchPrtBL();
    	if(tLHScanBatchPrtBL.submitData(tVData,transact))
    	{
				tCount = tLHScanBatchPrtBL.getCount();
        		String tFileName[] = new String[tCount];
        		VData tResult = new VData();
        		tResult = tLHScanBatchPrtBL.getResult();
        		tFileName=(String[])tResult.getObject(0);

        		String mFileNames = "";
        		for (int i = 0;i<=(tCount-1);i++)
        		{
          			System.out.println("~~~~~~~~~~~~~~~~~~~"+i);
          			System.out.println(tFileName[i]);
          			mFileNames = mFileNames + tFileName[i]+":";
        		}
          		System.out.println("===================="+mFileNames+"=======================");
				System.out.println("=========tFileName.length==========="+tFileName.length);

       			String strRealPath = application.getRealPath("/").replace('\\','/');
				String sql = "Select sysvarvalue from LDSysvar where sysvar = 'PrintServerInterface'";
       			ExeSQL mExeSQL = new ExeSQL();
       			tPrintServerPath =  mExeSQL.getOneValue(sql);
%>
				<html> 	
					<script language="javascript">
        	    		var printform = parent.fraInterface.document.getElementById("printform");
        	    		printform.elements["filename"].value = "<%=mFileNames%>";
        	    		printform.action = "<%=tPrintServerPath%>";
        	    		printform.submit();
        			</script>
        		</html>
<%
		}
	}
	catch(Exception ex)
	{
    	Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}

%>
<html>
<script language="javascript">	
	if("<%=operFlag%>" == "true")
	{	
	}
	else
	{
	}
</script>
</html>