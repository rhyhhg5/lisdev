<%
//程序名称：LHTotalInfoQueryInit.jsp
//程序功能：客户综合信息查询(初始化页面)
//创建日期：2006-05-20 16:03:30
//创建人  ：hm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%> 
<Script language=javascript>
function initForm()
{
	try
	{  
		//alert(HospitalCode);
		if (HospitalCode != null && HospitalCode != "null" && HospitalCode != "")
		{
		  var sql="select Hospitaltype, Hospitname from ldhospital where Hospitcode= '"+HospitalCode+"'";
		  var re = easyExecSql(sql);   
		  //alert(re);
		  fm.all('HospitName').value=re[0][1];
		  fm.all('HospitalType').value=re[0][0];
		  if (re[0][0]==1)
		     fm.all('HospitalType_ch').value="医疗机构";
		  else
		  	 fm.all('HospitalType_ch').value="健康体检机构";
		  fm.all('HospitCode').value=HospitalCode;
		  BasicInfoQuery(); 
		 } 
		  fm.all('DiseasTypeLevel').value="2级代码"
		  fm.all('DiseasTypeLevel_origin').value="2"
	    initSpecSecDiaGrid();
	    initInstrumentGrid();
	    initComHospitalGrid();
	    initLHGroupContGrid();
	    initGroupContItemGrid();
	    initLHDutySpecialInfo();
	    initDutyDoneState();
	    initInhospitQuery();
	    initHospitalInfoGrid();
	}
	catch(re)
	{
    	alert("LHTotalInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}  


function initSpecSecDiaGrid() 
{ 
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         		//列名
	    iArray[0][4]="station";         		//列名
	    
	    iArray[1]=new Array(); 
		iArray[1][0]="专科名称";   
		iArray[1][1]="140px";   
		iArray[1][2]=60;        
		iArray[1][3]=1;
		iArray[1][9]="专科名称|LEN<60";
		
		iArray[2]=new Array(); 
		iArray[2][0]="特色介绍";   
		iArray[2][1]="300px";   
		iArray[2][2]=500;        
		iArray[2][3]=1;
		iArray[2][7]="inputSpecSecDia";
		iArray[2][9]="特色介绍|LEN<500";
		
		iArray[3]=new Array(); 
		iArray[3][0]="SpecialClass";   
		iArray[3][1]="0px";   
		iArray[3][2]=20;        
		iArray[3][3]=3;
		iArray[3][14]="SpecSecDia";    
		
		iArray[4]=new Array();
		iArray[4][0]="FlowNo";
		iArray[4][1]="0px";         
		iArray[4][2]=20;            
		iArray[4][3]=3;             
		
	    SpecSecDiaGrid = new MulLineEnter( "fm" , "SpecSecDiaGrid" ); 
	    //这些属性必须在loadMulLine前
	
	    SpecSecDiaGrid.mulLineCount = 0;   
	    SpecSecDiaGrid.displayTitle = 1;
	    SpecSecDiaGrid.hiddenPlus = 1;
	    SpecSecDiaGrid.hiddenSubtraction = 1;
	    SpecSecDiaGrid.canSel = 1;
	    SpecSecDiaGrid.canChk = 0;
	    SpecSecDiaGrid.selBoxEventFuncName = "showTech";
	
	    SpecSecDiaGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //LDDiseaseGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
    	alert(ex);
  	}
}
  
function initInstrumentGrid() 
{ 
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         	     	//列名
	    iArray[0][4]="station";         		//列名
	    
	    iArray[1]=new Array(); 
		iArray[1][0]="设备名称";   
		iArray[1][1]="150px";   
		iArray[1][2]=60;        
		iArray[1][3]=1;
		iArray[1][9]="设备名称|LEN<60";
		
		iArray[2]=new Array(); 
		iArray[2][0]="设备介绍";   
		iArray[2][1]="300px";   
		iArray[2][2]=500;        
		iArray[2][3]=1;
		iArray[2][7]="inputInstrument";
		iArray[2][9]="设备介绍|LEN<500";
		                          
	    iArray[3]=new Array();       
	    iArray[3][0]="SpecialClass"; 		                          
		iArray[3][1]="0px";                                    
		iArray[3][2]=20;                                       
		iArray[3][3]=3;                                        
	    iArray[3][14]="Instrument";      
	    
	    iArray[4]=new Array();
	    iArray[4][0]="FlowNo";
	    iArray[4][1]="0px";   
	    iArray[4][2]=20;      
	    iArray[4][3]=3;       
    
	    InstrumentGrid = new MulLineEnter( "fm" , "InstrumentGrid" ); 
	    //这些属性必须在loadMulLine前
	
	    InstrumentGrid.mulLineCount = 0;   
	    InstrumentGrid.displayTitle = 1;
	    InstrumentGrid.hiddenPlus = 0;
	    InstrumentGrid.hiddenSubtraction = 0;
	    InstrumentGrid.canSel = 0;
	    InstrumentGrid.canChk = 0;
	    //LDDiseaseGrid.selBoxEventFuncName = "showOne";
	
	    InstrumentGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //LDDiseaseGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
    	alert(ex);
  	}
}

function initComHospitalGrid() 
{                               
	var iArray = new Array();
    
    try 
    {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         				//列名
	    iArray[0][4]="station";         //列名
	    
	    iArray[1]=new Array(); 
		iArray[1][0]="合作级别";   
		iArray[1][1]="140px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
		iArray[1][4]='llhospiflag';
		iArray[1][5]="1|1"; 
		iArray[1][6]="0|1"; 
		iArray[1][9]="合作级别|LEN<20"
		
		iArray[2]=new Array(); 
		iArray[2][0]="起始时间";   
		iArray[2][1]="120px";   
		iArray[2][2]=20;        
		iArray[2][3]=1;
		iArray[2][9]="起始时间|DATE"
				
		iArray[3]=new Array(); 
		iArray[3][0]="终止时间";   
		iArray[3][1]="120px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;
		iArray[3][9]="终止时间|DATE"
				
		iArray[4]=new Array();
		iArray[4][0]="SerialNo";
		iArray[4][1]="0px";         
		iArray[4][2]=20;            
		iArray[4][3]=3;   
		
		iArray[5]=new Array();
		iArray[5][0]="医院代码";
		iArray[5][1]="0px";         
		iArray[5][2]=20;            
		iArray[5][3]=3; 
		
	    ComHospitalGrid = new MulLineEnter( "fm" , "ComHospitalGrid" ); 
	    //这些属性必须在loadMulLine前
	
	    ComHospitalGrid.mulLineCount = 0;   
	    ComHospitalGrid.displayTitle = 1;
	    ComHospitalGrid.hiddenPlus = 1;
	    ComHospitalGrid.hiddenSubtraction = 1;
	    ComHospitalGrid.canSel = 0;
	    ComHospitalGrid.canChk = 0;
	    //ComHospitalGrid.selBoxEventFuncName = "showOne";
	
	    ComHospitalGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //ComHospitalGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
    	alert(ex);
  	}
}
 
var LHGroupContGrid;
function initLHGroupContGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="";         				//列名
    
    iArray[1]=new Array();                
    iArray[1][0]="合同编号";     		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=0;         				//列名
    iArray[1][4]="";         				//列名
    
    iArray[2]=new Array();                
    iArray[2][0]="合同名称";        //列名
    iArray[2][1]="100px";         		//列名
    iArray[2][3]=0;         				//列名
    iArray[2][4]="";         				//列名
    
    //iArray[3]=new Array();                
    //iArray[3][0]="合作机构名称";      //列名
    //iArray[3][1]="100px";         		//列名
    //iArray[3][3]=0;         					//列名
    //iArray[3][4]="";         					//列名
    
    iArray[3]=new Array();                         
    iArray[3][0]="合同签订时间";      //列名 
    iArray[3][1]="100px";         		//列名       
    iArray[3][3]=0;         					//列名         
    iArray[3][4]="";         					//列名         
    
    iArray[4]=new Array();                
    iArray[4][0]="合同起始时间";      //列名
    iArray[4][1]="100px";         		//列名
    iArray[4][3]=0;         					//列名
    iArray[4][4]="";         					//列名
    
    iArray[5]=new Array();                
    iArray[5][0]="合同终止时间";         		//列名
    iArray[5][1]="100px";         		//列名
    iArray[5][3]=0;         				//列名
    iArray[5][4]="";         //列名
    
    iArray[6]=new Array();                
    iArray[6][0]="合同当前状态";         		//列名
    iArray[6][1]="100px";         		//列名
    //iArray[6][3]=0;         				//列名
    //iArray[6][4]="";         //列名
    iArray[6][2]=20;        
		iArray[6][3]=2;   
	
	
    
    iArray[7] = new Array();
    iArray[7][0]="合同创建日期";
    iArray[7][1]="0px";
    iArray[7][3]=3;  
    iArray[7][4]="3";
                
    iArray[8]= new Array();    
    iArray[8][0]="合同创建时间";            
    iArray[8][1]="0px";                        
    iArray[8][3]=3;                         
    iArray[8][4]="3";              
    
    iArray[9]= new Array();      
    iArray[9][0]="ContrItemNo"; 
    iArray[9][1]="0px";  
    iArray[9][3]=3;           
    iArray[9][4]="";                        
                        
    
    LHGroupContGrid = new MulLineEnter( "fm" , "LHGroupContGrid" ); 
    //这些属性必须在loadMulLine前

		LHGroupContGrid.canSel = 1;     
		LHGroupContGrid.canChk = 0;
		LHGroupContGrid.mulLineCount = 0;     
		LHGroupContGrid.displayTitle = 1;     
		LHGroupContGrid.hiddenPlus = 1;       
		LHGroupContGrid.hiddenSubtraction = 1;
		LHGroupContGrid.selBoxEventFuncName = "showOne";

    LHGroupContGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHGroupContGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}


function initGroupContItemGrid() 
 {                            
   var iArray = new Array();                               
                                                           
   try 
   {                                                   
     iArray[0]=new Array();                                
     iArray[0][0]="序号";         		//列名               
     iArray[0][1]="30px";         		//列名               
     iArray[0][3]=0;         				//列名                 
     iArray[0][4]="";         //列名   
    
    iArray[1]=new Array(); 
		iArray[1][0]="合同责任项目类型";   
		iArray[1][1]="98px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;

     
		iArray[2]=new Array(); 
		iArray[2][0]="合同责任项目名称";   
		iArray[2][1]="128px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;

		
		iArray[3]=new Array(); 
		iArray[3][0]="责任当前状态";   
		iArray[3][1]="68px";   
		iArray[3][2]=20;        
		iArray[3][3]=2;   

		
		iArray[4]=new Array(); 
		iArray[4][0]="责任联系人";   
		iArray[4][1]="88px";   
		iArray[4][2]=20;        
		//iArray[4][3]=2;   
		//iArray[4][5]="4|9";    //引用MulLine的对应第几列，'|'为分割符 
		//iArray[4][6]="0|1";    //上面的列中放置下拉菜单中第几位值 
		//iArray[4][4]="doctor";
		//iArray[4][15]="Doctname";
		//iArray[4][17]="4"; 			 //传参的列号
		//iArray[4][18]="200";     //下拉框的宽度 
		//iArray[4][19]="1" ;      //强制刷新数据源
		
		iArray[5]=new Array(); 
		iArray[5][0]="责任联系方式";   
		iArray[5][1]="108px";   
		iArray[5][2]=50;        
		iArray[5][3]=1;  
		
		
	//	iArray[6]=new Array(); 
	//	iArray[6][0]="hmconttype";   
	//	iArray[6][1]="0px";   
	//	iArray[6][2]=20;        
	//	iArray[6][3]=3; 
		
	//	iArray[7]=new Array(); 
	//	iArray[7][0]="DutyItemCode";   
	//	iArray[7][1]="0px";   
	//	iArray[7][2]=20;        
//		iArray[7][3]=3;   
		
	//	iArray[8]=new Array(); 
	//	iArray[8][0]="dutystate";   
	//	iArray[8][1]="0px";   
//		iArray[8][2]=20;        
	//	iArray[8][3]=3;  
		
	//	iArray[9]=new Array(); 
	//	iArray[9][0]="linkman";   
	//	iArray[9][1]="0px";   
	//	iArray[9][2]=20;        
	//	iArray[9][3]=3;  
		 
		
	//	iArray[10]=new Array(); 
//		iArray[10][0]="ContraItemNo";   
	//	iArray[10][1]="0px";   
	//	iArray[10][2]=20;        
	//	iArray[10][3]=3; 
		     
   	iArray[6]=new Array(); 
		iArray[6][0]="有无关联责任";   
		iArray[6][1]="68px";   
		iArray[6][2]=50;        
		iArray[6][3]=0;  

    //Array[12]=new Array(); 
		//Array[12][0]="flag";   
		//Array[12][1]="0px";   
		//Array[12][2]=20;        
		//Array[12][3]=3; 
     
     
    GroupContItemGrid = new MulLineEnter( "fm" , "GroupContItemGrid" );                         
    //这些属性必须在loadMulLine前                                                                                              
                                                                                                               
    GroupContItemGrid.mulLineCount = 0;                                          
    GroupContItemGrid.displayTitle = 1;                                          
    GroupContItemGrid.hiddenPlus = 1;                                            
    GroupContItemGrid.hiddenSubtraction = 1;                                     
    GroupContItemGrid.canSel = 1;                                                
    GroupContItemGrid.canChk = 0;                                              
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";                    
                                                                  
    GroupContItemGrid.loadMulLine(iArray);                                       
    //这些操作必须在loadMulLine后面                                          
    //LDDiseaseGrid.setRowColData(1,1,"asdf");                               
  }                                                                          
  catch(ex) {                                                                
    alert(ex);                                                               
  }                                                                          
}     

function initLHDutySpecialInfo() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="";         				//列名
    
    iArray[1]=new Array();                
    iArray[1][0]="责任项目类型";     		//列名
    iArray[1][1]="75px";         		//列名
    iArray[1][3]=0;         				//列名
    iArray[1][4]="";         				//列名
    
    iArray[2]=new Array();                
    iArray[2][0]="责任项目名称";        //列名
    iArray[2][1]="90px";         		//列名
    iArray[2][3]=0;         				//列名
    iArray[2][4]="";         				//列名
    
    iArray[3]=new Array();                         
    iArray[3][0]="责任状态";      //列名 
    iArray[3][1]="50px";         		//列名       
    iArray[3][3]=0;         					//列名         
    iArray[3][4]="";         					//列名         
    
    iArray[4]=new Array();                
    iArray[4][0]="合同编号";      //列名
    iArray[4][1]="60px";         		//列名
    iArray[4][3]=0;         					//列名
    iArray[4][4]="";         					//列名
    
    iArray[5]=new Array();                
    iArray[5][0]="合同名称";         		//列名
    iArray[5][1]="140px";         		//列名
    iArray[5][3]=0;         				//列名
    iArray[5][4]="";         //列名
    
    iArray[6]=new Array();                
    iArray[6][0]="有无关联责任";         		//列名
    iArray[6][1]="75px";         		//列名
    iArray[6][3]=0;         				//列名
    iArray[6][4]="";         //列名 
            
    iArray[7]=new Array();                
    iArray[7][0]="责任执行次数";         		//列名
    iArray[7][1]="75px";         		//列名
    iArray[7][3]=0;         				//列名
    iArray[7][4]="";         //列名 
    
    iArray[8]=new Array();                
    iArray[8][0]="责任结算次数";         		//列名
    iArray[8][1]="75px";         		//列名
    iArray[8][3]=0;         				//列名
    iArray[8][4]="";         //列名 
    
    iArray[9]=new Array();                
    iArray[9][0]="责任结算费用";         		//列名
    iArray[9][1]="80px";         		//列名
    iArray[9][3]=0;         				//列名
    iArray[9][4]="";         //列名  
    
    iArray[10]=new Array();                
    iArray[10][0]="ContraItemNo";         		//列名
    iArray[10][1]="0px";         		//列名
    iArray[10][3]=3;         				//列名
    
        
    iArray[11]=new Array();                
    iArray[11][0]="ServTaskno";         		//列名
    iArray[11][1]="0px";         		//列名
    iArray[11][3]=3;         				//列名
    
    LHDutySpecialInfo = new MulLineEnter( "fm" , "LHDutySpecialInfo" ); 
    //这些属性必须在loadMulLine前

		LHDutySpecialInfo.canSel = 1;     
		LHDutySpecialInfo.canChk = 0;
		LHDutySpecialInfo.mulLineCount = 0;     
		LHDutySpecialInfo.displayTitle = 1;     
		LHDutySpecialInfo.hiddenPlus = 1;       
		LHDutySpecialInfo.hiddenSubtraction = 1;
		LHDutySpecialInfo.selBoxEventFuncName = "showOne1";

    LHDutySpecialInfo.loadMulLine(iArray);  
  
  }
  catch(ex) {
    alert(ex);
  }
}


function initDutyDoneState() 
{ 
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         		//列名
	    
	    iArray[1]=new Array(); 
		iArray[1][0]="责任实施对象";   
		iArray[1][1]="220px";   
		iArray[1][2]=60;        
		iArray[1][3]=1;

		
		iArray[2]=new Array(); 
		iArray[2][0]="责任实施时间";   
		iArray[2][1]="150px";   
		iArray[2][2]=500;        
		iArray[2][3]=1;
		
		iArray[3]=new Array(); 
		iArray[3][0]="责任实施状态";   
		iArray[3][1]="150px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;
		
		iArray[4]=new Array(); 
		iArray[4][0]="应付费用";   
		iArray[4][1]="120px";   
		iArray[4][2]=20;        
		iArray[4][3]=1;
		
		iArray[5]=new Array(); 
		iArray[5][0]="实付费用";   
		iArray[5][1]="120px";   
		iArray[5][2]=20;        
		iArray[5][3]=1;
             
		
	    DutyDoneState = new MulLineEnter( "fm" , "DutyDoneState" ); 
	    //这些属性必须在loadMulLine前
	
	    DutyDoneState.mulLineCount = 0;   
	    DutyDoneState.displayTitle = 1;
	    DutyDoneState.hiddenPlus = 1;
	    DutyDoneState.hiddenSubtraction = 1;
	    DutyDoneState.canSel = 0;
	    DutyDoneState.canChk = 0;
	    //LDDiseaseGrid.selBoxEventFuncName = "showOne";
	
	    DutyDoneState.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //LDDiseaseGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
    	alert(ex);
  	}
}

function initInhospitQuery() 
{ 
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         		//列名
	    
	     iArray[1]=new Array(); 
		iArray[1][0]="疾病分类代码";   
		iArray[1][1]="100px";   
		iArray[1][2]=60;        
		iArray[1][3]=1;
	    
	    iArray[2]=new Array(); 
		iArray[2][0]="疾病分类名称";   
		iArray[2][1]="100px";   
		iArray[2][2]=60;        
		iArray[2][3]=1;

		
		iArray[3]=new Array(); 
		iArray[3][0]="就诊人次-门诊";   
		iArray[3][1]="100px";   
		iArray[3][2]=500;        
		iArray[3][3]=1;
		
		iArray[4]=new Array(); 
		iArray[4][0]="就诊人次-住院";   
		iArray[4][1]="100px";   
		iArray[4][2]=20;        
		iArray[3][3]=1;
             
    iArray[5]=new Array(); 
		iArray[5][0]="平均门诊费用";   
		iArray[5][1]="100px";   
		iArray[5][2]=20;        
		iArray[5][3]=1;
		
		iArray[6]=new Array(); 
		iArray[6][0]="平均住院费用";   
		iArray[6][1]="100px";   
		iArray[6][2]=20;        
		iArray[6][3]=1;
		
		iArray[7]=new Array(); 
		iArray[7][0]="平均住院日";   
		iArray[7][1]="100px";   
		iArray[7][2]=20;        
		iArray[7][3]=1;
		
	    InhospitQuery = new MulLineEnter( "fm" , "InhospitQuery" ); 
	    //这些属性必须在loadMulLine前
	
	    InhospitQuery.mulLineCount = 0;   
	    InhospitQuery.displayTitle = 1;
	    InhospitQuery.hiddenPlus = 0;
	    InhospitQuery.hiddenSubtraction = 0;
	    InhospitQuery.canSel = 0;
	    InhospitQuery.canChk = 1;
	    //LDDiseaseGrid.selBoxEventFuncName = "showOne";
	
	    InhospitQuery.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //LDDiseaseGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
    	alert(ex);
  	}
}

function initHospitalInfoGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="";         				//列名
    
    iArray[1]=new Array();                
    iArray[1][0]="客户号";     		//列名
    iArray[1][1]="55px";         		//列名
    iArray[1][3]=0;         				//列名
    iArray[1][4]="";         				//列名
    
    iArray[2]=new Array();                
    iArray[2][0]="客户姓名";        //列名
    iArray[2][1]="40px";         		//列名
    iArray[2][3]=0;         				//列名
    iArray[2][4]="";         				//列名
    
    iArray[3]=new Array();                         
    iArray[3][0]="就诊方式";      //列名 
    iArray[3][1]="40px";         		//列名       
    iArray[3][3]=0;         					//列名         
    iArray[3][4]="";         					//列名         
    
    iArray[4]=new Array();                
    iArray[4][0]="就诊日期";      //列名
    iArray[4][1]="60px";         		//列名
    iArray[4][3]=0;         					//列名
    iArray[4][4]="";         					//列名
    
    iArray[5]=new Array();                
    iArray[5][0]="就诊天数";         		//列名
    iArray[5][1]="40px";         		//列名
    iArray[5][3]=0;         				//列名
    iArray[5][4]="";         //列名
    
    iArray[6]=new Array();                
    iArray[6][0]="费用总额(元)";         		//列名
    iArray[6][1]="60px";         		//列名
    iArray[6][3]=0;         				//列名
    iArray[6][4]="";         //列名 
            
    iArray[7]=new Array();                
    iArray[7][0]="公司支付(元)";         		//列名
    iArray[7][1]="0px";         		//因为字段值不准确,所以隐掉不显示.刘莉改于2007-11-12
    iArray[7][3]=0;         				//列名
    iArray[7][4]="";         //列名 
    
    iArray[8]=new Array();                
    iArray[8][0]="自付费用(元)";         		//列名
    iArray[8][1]="0px";         		//因为字段值不准确,所以隐掉不显示.刘莉改于2007-11-12
    iArray[8][3]=0;         				//列名
    iArray[8][4]="";         //列名 
    
    iArray[9]=new Array();                
    iArray[9][0]="赔付险种类别";         		//列名
    iArray[9][1]="160px";         		//列名
    iArray[9][3]=0;         				//列名
    iArray[9][4]="";         //列名  
    
    iArray[10]=new Array();                
    iArray[10][0]="MainFeeNo";         		//列名
    iArray[10][1]="10px";         		//列名
    iArray[10][3]=1;         				//列名
    iArray[10][4]="";         //列名  
    
    
    HospitalInfoGrid = new MulLineEnter( "fm" , "HospitalInfoGrid" ); 
    //这些属性必须在loadMulLine前

		HospitalInfoGrid.canSel = 0;     
		HospitalInfoGrid.canChk = 0;
		HospitalInfoGrid.mulLineCount = 0;     
		HospitalInfoGrid.displayTitle = 1;     
		HospitalInfoGrid.hiddenPlus = 1;     
		HospitalInfoGrid.hiddenSubtraction = 1;

    HospitalInfoGrid.loadMulLine(iArray);  
  
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
  