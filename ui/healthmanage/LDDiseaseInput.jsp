<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-19 14:47:23
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDDiseaseInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDDiseaseInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDDiseaseSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <Div id = "divQueryButton" style="display: 'none'" align=right>
    	<table>
    		<tr>
    			<td class=button width="10%" align=right>
						<INPUT class=cssButton name="querybutton1" VALUE="查  询"  TYPE=button onclick="return queryClick();">
					</td>	
				</tr>	
			</table>
			<hr>
		</Div>	
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDDisease1);">
    		</td>
    		 <td class= titleImg>
        		 疾病信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDDisease1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      疾病分类代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICDCode elementtype=nacessary verify="疾病分类代码|notnull&len<=20">
    </TD>
    <TD  class= title>
      疾病分类名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICDName elementtype=nacessary verify="疾病分类名称|notnull&len<=50">
    </TD>

    <TD  class= title8>代码等级</TD>
	  <TD  class= input8> 
	  <input class=codeno CodeData= "0|^3|3级代码^4|4级代码^5|5级代码"  name=FrequencyFlag ondblclick="return showCodeListEx('FrequencyFlag',[this,FrequencyFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('FrequencyFlag',[this,FrequencyFlagNam],[0,1],null,null,null,1);" verify="代码等级|len<=20" ><input class=codename name=FrequencyFlagName></TD>

  </TR>
  <TR  class= common>
    <TD  class= title>
      慢性病标志
    </TD>
    <TD  class= input id = ChronicMark1 style= "display: 'none'">                                                                                                                                                                                                                            
      <input class=codeno name=ChronicMark verify="慢性病标志|len<=4" CodeData= "0|^0|否^1|是" ondblClick="showCodeListEx('ChronicMark',[this,ChronicMark_ch],[0,1],null,null,null,1);"><Input  class='codename' name=ChronicMark_ch  >                                                                
    </TD>
    
  </TR>
  
</table>
    </Div>
    <div id="div" style="display: 'none'">
    	<table>
    		<TR  class= common>
    			    <TD  class= title>
					  诊疗科目
					</TD>
					<TD  class= input>
					  <Input class= 'common' name=ICDSubjectClass verify="疾病按科目分类|len<=20">
					</TD>
		    <TD  class= title>
		      疾病分类代码
		    </TD>
		    <TD  class= input>
		      <Input class= 'code' name=DiseasTypeCode verify="疾病分类代码|len<=20" ondblclick="getDiseasTypeCode();return showCodeListEx('getDiseasTypeCode',[this],[0],'', '', '', true);" onkeyup="getDiseasTypeCode();return showCodeListKeyEx('getDiseasTypeCode',[this],[0],'', '', '', true);">
		    </TD>
		  </TR>
		   </table>
		  </div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
		<input type=hidden name="LoadFlag">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
