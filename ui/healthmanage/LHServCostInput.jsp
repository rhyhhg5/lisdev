<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2006-03-29 10:45:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
	String PriceCode = request.getParameter("PriceCode");
	System.out.println(PriceCode);
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="LHServCostInput.js"></SCRIPT>
  <%@include file="LHServCostInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LHServCostSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 第三方成本记录表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHServCost1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
			    <TD  class= title>
			      定价方式代码
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServPriceCode >
			    </TD>
			    <TD  class= title>
			      定价方式名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServPriceName >
			    </TD>
			    <TD  class= title>
			    	标准服务项目名称
			    </TD>
			    <TD  class= input>
			    	<Input class= 'common' name=ServItemName >
			    </TD>
			</TR>
		</table>
		  <!-- 信息（列表） -->
			<Div id="divLHServCostGrid" style="display:''">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHServCostGrid">
		  				</span> 
				    </td>
					</tr>
				</table>
			</div>
			<Div id= "divPage" align=center style= "display: '' ">
			    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
			</Div>
		
    </Div>
     <Div id="divLHContServPrice" style="display:'none'">
		 <table> 
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        		 服务价格信息
       	</td>   		 
    	</tr>
		  </table>
			<table  class= common align='center' >
      <TR  class= common>
		    <TD  class= title>
		      服务价格表类型
		    </TD>
		    <TD   class= input>
		      <Input  type=hidden  name=ServicePriceCode >
		      <Input class= 'code' name=ServicePriceName  readonly>	
		    </TD>
		    <TD  class= title>
		      费用类型
		    </TD>
		    <TD  class= input>
		      <Input type=hidden name=BalanceTypeCode>
		      <Input class= 'code' name=BalanceTypeName  readonly>	
		    </TD>
		    <TD  class= title>
		    </TD>
		    <TD  class= input></td>
	    </TD>
		  </tr>
		  </table>
    </Div>
        <Div  id= "divTestMoney" style= "display: 'none'">
			<table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        		 请输入价格信息
       	</td>   		 
    	</tr>
		  </table>
					<table class=common>
						<TR  class= common>
						 <TD   class= title >
				     	   服务价格
				     </TD>
				     <TD  class= input>
		          <Input class= 'common' name=MoneySum readonly>	
				     </TD>

				     <TD  type = hidden class= input>
				     	<Input  type = hidden class= 'common' name=MaxServNum readonly>	
				     </TD>
				     <TD   class= title >
				     </TD>
				     <TD  class= input>
				     </TD>
				    </tr>
				    <tr>
				  	<TD  class= title >
				     	   补充说明
				     </TD>
				     <TD  class= input colSpan=5>
             <textarea class= 'common'  name=ExplanOther style="width:883px;height:60px" readonly></textarea>    
             </TD>
		       
		       </tr>
				 </table>
</div>
<Div  id= "divTestMoney2" style= "display: 'none'">
			<table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        		 请输入价格信息
       	</td>   		 
    	</tr>
		  </table>
					<table class=common>
						<TR  class= common>
						 <TD   class= title >
				     	   服务价格
				     </TD>
				     <TD  class= input>
		          <Input class= 'common' name=MoneySum2 readonly>	
				     </TD>
				     <TD   class= title >
				     	   服务时程
				     </TD>
				     <TD  class= input>
				     	<Input class= 'common' name=ServPros2 readonly>	
				     </TD>

				     <TD type = hidden  class= input>
				     	<Input type = hidden  class= 'common' name=MaxServNum2 readonly>	
				     </TD>
				    </tr>
				    <tr>
				  	<TD  class= title >
				     	   补充说明
				     </TD>
				     <TD  class= input colSpan=5>
             <textarea class= 'common'  name=ExplanOther2 style="width:883px;height:60px" readonly></textarea>    
             </TD>
		       
		       </tr>
				 </table>
</div>
<Div  id= "divTestMoney3" style= "display: 'none'">
			<table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        		 请输入价格信息
       	</td>   		 
    	</tr>
		  </table>
					<table class=common>
						<TR  class= common>
						 <TD   class= title >
				     	   服务价格
				     </TD>
				     <TD  class= input>
		          <Input class= 'common' name=MoneySum3 readonly>	
				     </TD>
				     <TD   class= title >
				     	   服务时程
				     </TD>
				     <TD  class= input>
				     	<Input class= 'common' name=ServPros3 readonly>	
				     </TD>
				     <TD   class= title >
				     	   最大人数
				     </TD>
				     <TD  class= input>
				     	<Input class= 'common' name=MaxServNum3 readonly>	
				     </TD>
				    </tr>
				    <tr>
				  	<TD  class= title >
				     	   补充说明
				     </TD>
				     <TD  class= input colSpan=5>
             <textarea class= 'common'  name=ExplanOther3 style="width:883px;height:60px" readonly></textarea>    
             </TD>
		       
		       </tr>
				 </table>
</div>
<Div  id= "divTestServPrice" style= "display: 'none'">
			<table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        		 体检价格信息
       	</td>   		 
    	</tr>
		  </table>
					<table class=common>
						<TR  class= common>
						 <TD  class= title >
				     	   体检套餐代码
				     </TD>
				     <TD  class= input>
		             <Input class= 'common' name=MedicaItemCode readonly>	
				     </TD>
				     	<TD  class= title >
				     	   体检套餐名称
				     </TD>
				     <TD  class= input>
		           <Input class= 'code' name=MedicaItemName readonly>	
				     </TD>
				     <td style="width:900px;">
				  	 </td>
				     </TD>
				     <TD  class= input colSpan=5>
             </TD>
		       </tr>
				 </table>
</div>
<Div  id= "divShowInfo" style= "display: 'none'">	
			  <table class=common>
	    	<tr class=common>
	    		<table>
		    		<tr>
				      <td class=common>
						    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTestItemPriceGrid);">
				  		</td>
				  		<td class=titleImg>
				  			 检查项目单价
				  		</td>
				  	</tr>
			  	</table>	
<Div  id= "divTestItemPriceGrid" style= "display: 'none'">	
	 
	 <table class=common> 	
	 		<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanTestItemPriceGrid"></span> 
	  				<br>
	  			</td>
	  	</tr>
		</table>

</div>

	  		<table>
		      <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTestGrpPriceGrid);">
		  		</td>
		  		<td class=titleImg>
		  			 体检套餐价格
		  		</td>
		  	</table>
<Div  id= "divTestGrpPriceGrid" style= "display: 'none'">

		  	</tr>
            <span id="spanTestGrpPriceGrid"></span> 
			    </td>
				</tr>
			</table>

</div>

</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type=hidden id="fmAction" name="ContraType">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
