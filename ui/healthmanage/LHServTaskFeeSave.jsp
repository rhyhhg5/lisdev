<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称： LHServTaskFeeSave.jsp
//程序功能：
//创建日期：2005-05-24 10:05:39
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.health.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//输入参数
	OLHServFeeUI tOLHServFeeUI = new OLHServFeeUI();
	LHCaseTaskRelaSet tLHCaseTaskRelaSet = new LHCaseTaskRelaSet();       
	
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");
	String tServTaskNo[] = request.getParameterValues("LHSettingSlipQueryGrid7"); 
	String tChk[] = request.getParameterValues("InpLHSettingSlipQueryGridChk"); //参数格式=Inp+MulLine对象名+Chk  
	
	int ServTaskCount = 0;
    if (tServTaskNo != null)
    {ServTaskCount = tServTaskNo.length;}

    for(int i = 0; i < ServTaskCount; i++)
    {
		if(tChk[i].equals("1"))  
		{
			LHCaseTaskRelaSchema tLHCaseTaskRelaSchema = new LHCaseTaskRelaSchema();
    		tLHCaseTaskRelaSchema.setServTaskNo(tServTaskNo[i]);
    		tLHCaseTaskRelaSet.add(tLHCaseTaskRelaSchema);      
		}
    }

	try
	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tLHCaseTaskRelaSet);
		tVData.add(tG);
		tOLHServFeeUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
		Content = " 操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tOLHServFeeUI.mErrors;
	if (!tError.needDealError())
	{                          
		Content = "财务数据提交成功! ";
		FlagStr = "Success";
	}
	else
	{
		Content = "财务数据提交失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterFeeSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
