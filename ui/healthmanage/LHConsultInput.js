//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function equalInput()
{
	fm.ConsultNo.value = fm.LogNo.value;
}


//提交，保存按钮对应操作
function submitForm()
{
	if(fm.all('LogNo').value != "")
	{
			alert("新建 '健康咨询信息' 请点左栏对应菜单刷新本页面！");
			return false;
	}
	
  var i = 0;
  if( verifyInput2() == false ) return false;
  
  fm.ConsultNo.value = document.all.LogNo.value;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
 
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHConsult.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if( verifyInput2() == false ) return false;
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
 // showInfo=window.open("./LHConsultQuery.html");
  showInfo=window.open("./LHConsultQuery.html","信息查询",'width=1024,height=748,top=0,left=0,resizable=yes');
} 

  


          
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	if(fm.all('LogNo').value == "")
	{
			alert("请重新查询，再删除");
			return false;
	}
	
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
	var strSQL="select a.LogNo,a.LogState,a.AskType,a.AskMode,a.LogerNo,b.name,a.LogDate,a.AnswerType,a.AnswerMode, "
	+"a.ExpectRevertDate,a.AskStyle,a.PromiseAnswerType,a.DelayRemain,a.DelayEndDate,a.DelayBeginDate ,a.makedate,a.maketime, "
	+"a.SendFlag,a.AvaiFlag,a.DelayRemain,a.LogTime,a.DelayBeginTime,a.DelayEndTime,a.ExpectRevertTime,a.OtherNoType "
	+" from LLMainAsk a,ldperson b where a.LogerNo=b.customerno and LogNo='"+arrQueryResult[0][0]+"'";
	                   	
	arrResult = easyExecSql(strSQL);
	var arrResult1 = arrResult;
	
	if( arrResult != null )
	{
        switch(arrResult[0][3])
        {
        case "1" : 
        		fm.all('AskModeName').value = "电话";break;
				case "2" :
        		fm.all('AskModeName').value = "电子邮件";break;
        case "3" :
        		fm.all('AskModeName').value = "传真";break;
        case "4" :
        		fm.all('AskModeName').value = "短信";break;
        case "5" :
        		fm.all('AskModeName').value = "其他";break;  	
        default  : fm.all('AskModeName').value ="";
        }  
                    
        fm.all('LogNo').value= arrResult[0][0];
        //alert(arrResult[0][0]);
 //       fm.all('LogState').value= arrResult[0][1];
        fm.all('AskType').value= arrResult[0][2];
        fm.all('AskMode').value= arrResult[0][3];
        fm.all('LogerNo').value= arrResult[0][4];
        fm.all('CustomerName').value= arrResult[0][5];
        fm.all('LogDate').value= arrResult[0][6];
        fm.all('AnswerType').value= arrResult[0][7];if(arrResult[0][7]=="2"){fm.all('AnswerType_ch').value="否";}if(arrResult[0][7]=="1"){fm.all('AnswerType_ch').value="是";}
        fm.all('AnswerMode').value= arrResult[0][8];fm.all('AnswerMode_ch').value=easyExecSql("select codename from ldcode where codetype='hmaskmode' and code = '"+arrResult[0][8]+"'");
        fm.all('ExpectRevertDate').value= arrResult[0][9];
        fm.all('AskStyle').value= arrResult[0][10];fm.all('AskStyle_ch').value=easyExecSql("select codename from ldcode where codetype='hmconsulttype' and code ='"+arrResult[0][10]+"'");
        fm.all('PromiseAnswerType').value= arrResult[0][11];
        fm.all('PromiseAnswerType_ch').value= easyExecSql("select codename from ldcode where codetype='hmaskmode' and code='"+arrResult[0][11]+"'");
        fm.all('DelayRemain').value= arrResult[0][12];
        fm.all('DelayEndDate').value= arrResult[0][13];
        fm.all('DelayBeginDate').value= arrResult[0][14];
        fm.all('MakeDate').value= arrResult[0][15];
        fm.all('MakeTime').value= arrResult[0][16];
        fm.all('IsAnswer').value= arrResult[0][17];if(arrResult[0][17]=="1"){fm.all('IsAnswer_ch').value="是";}if(arrResult[0][17]=="2"){fm.all('IsAnswer_ch').value="否";}
        fm.all('IsContinueAnswer').value= arrResult[0][18];if(arrResult[0][18]=="1"){fm.all('IsContinueAnswer_ch').value="是";}if(arrResult[0][18]=="2"){fm.all('IsContinueAnswer_ch').value="否";}
				fm.all('DelayRemain').value= arrResult[0][19];
				fm.all('LogTime').value= arrResult[0][20];
				fm.all('DelayBeginTime').value= arrResult[0][21];
				fm.all('DelayEndTime').value= arrResult[0][22];
				fm.all('ExpectRevertTime').value= arrResult[0][23];
				fm.all('DelayAnswerType').value= arrResult[0][24];if(arrResult[0][24]=="1"){fm.all('DelayAnswerType_ch').value="短时延时";}if(arrResult[0][24]=="2"){fm.all('DelayAnswerType_ch').value="长时延时";}
				
        arrResult=null;
        strSQL="select ConsultNo,CContent,ReplyState from LLConsult where 1=1 "
        +  getWherePart("LogNo", "LogNo");
        +  getWherePart("ConsultNo","LogerNo");
        arrResult = easyExecSql(strSQL); 
        fm.all('ConsultNo').value= arrResult[0][0];            
        fm.all('CContent').value= arrResult[0][1];         
         
         switch(arrResult[0][2])                             
         {                                                   
         case "1" :                                          
         		fm.all('ReplyStateName').value = "进行中";
         		divConsultDelay1.style.display= '';
						divConsultDelay3.style.display='';
						divConsultDelay2.style.display='none';         
						divConsultDelay4.style.display='none';
						divConsultDelay5.style.display='none';         
						divConsultDelay6.style.display='none';
						break;      
         case "2" :                                          
         		fm.all('ReplyStateName').value = "咨询结束--问题已解答";
         		divConsultDelay1.style.display= 'none';
						divConsultDelay3.style.display='none';
						divConsultDelay2.style.display='';           
						divConsultDelay4.style.display= ''; 
						divConsultDelay5.style.display='';           
						divConsultDelay6.style.display= ''; 
         		break;  
         case "3" :                                          
         		fm.all('ReplyStateName').value = "咨询结束--问题未解答";
         		divConsultDelay1.style.display= 'none';
				    divConsultDelay3.style.display='none';
						divConsultDelay2.style.display='none';        
						divConsultDelay4.style.display='none'; 
						divConsultDelay5.style.display='none';        
						divConsultDelay6.style.display='none';
         		break;    
         default :   	   
         }	
         
         
                                                            
        arrResult=null;                                                       
        strSQL="select Answer ,AnswerDate,AnswerEndDate ,AnswerCostTime,Remark,AnswerTime,AnswerEndTime from LLAnswerInfo where 1=1 "                       
        +  getWherePart("LogNo", "LogNo")   
        +  getWherePart("ConsultNo", "ConsultNo");                                
        arrResult = easyExecSql(strSQL);                                     
                              
        fm.all('Answer').value= arrResult[0][0]; 
        fm.all('AnswerDate').value= arrResult[0][1];
        fm.all('AnswerEndDate').value= arrResult[0][2];
        fm.all('AnswerCostTime').value= arrResult[0][3];
        fm.all('Remark').value= arrResult[0][4];
        fm.all('AnswerTime').value= arrResult[0][5];
        fm.all('AnswerEndTime').value= arrResult[0][6];
        	             
        fm.all('iscomefromquery').value="1";	 
        
        
        if(arrResult1[0][7]==1)
				{
						NeedAnswerTable.style.display='';
				}
				if(arrResult1[0][7]==2)
				{//alert(arrResult1[0][17]);
						if(arrResult1[0][17]==1)
						{
							NeedAnswerTable.style.display='';
						}
						if(arrResult1[0][17]==2)
						{
							NeedContinueAnswerTable.style.display='';
							if(arrResult1[0][18]==1)
							{
								NeedAnswerTable.style.display='';
							}
							if(arrResult1[0][18]==2)
							{
								NoAnswerCauseTable.style.display= '';
							}
						}
						DelayAnswerTypeTable.style.display='';						
				}
			        	                    
		}     	                
}                       
function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.LogerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}
function queryCustomerNo2()
{
	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
	var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}
function queryCustomerNo_wrong()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.html");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}



//下拉菜单选择处理
function afterCodeSelect(codeName,Field)
{  
	if(codeName=="AnswerType")
	{
		
		if(Field.value=="1")
		{ 
		
			NeedAnswerTable.style.display='';
			DelayAnswerTypeTable.style.display='none';
			NoAnswerCauseTable.style.display = 'none';
			NeedContinueAnswerTable.style.display = 'none';
		}//if
		else
		{
			if(Field.value=="2")
			{
				DelayAnswerTypeTable.style.display='';
				NeedAnswerTable.style.display='none';
			}
		}//else
	}//if
	
		if(codeName=="IsAnswer")
		{
			if(Field.value== "1")
			{
				//NoAnswerCauseTable.style.display= 'none';
				//NotAnswerTable.style.display='none';          
				//NeedContinueAnswerTable.style.display= 'none';
				NeedAnswerTable.style.display='';
				NeedContinueAnswerTable.style.display='none';
				NoAnswerCauseTable.style.display= 'none';
			}
			else
			{
				if(Field.value=="2" && fm.all('DelayAnswerType').value =="2" )
				{
					NoAnswerCauseTable.style.display= '';
					//NeedAnswerTable.style.display='none';
					//NotAnswerTable.style.display='none';
					NeedContinueAnswerTable.style.display= 'none';
					NeedAnswerTable.style.display='none';
				}//if
			  else
			  {
			  	//NoAnswerCauseTable.style.display= '';         
			  	//NeedAnswerTable.style.display='none';       
			  	//NotAnswerTable.style.display='none';          
			  	NeedContinueAnswerTable.style.display= '';  
			  	NeedAnswerTable.style.display='none';           
			  	NoAnswerCauseTable.style.display= 'none';       			  	  
			  }
			}//else
		}//if
		
	 			if(codeName=="IsContinueAnswer")
				{
					if(Field.value=="1")
					{
						DelayAnswerTypeTable.style.display='';
						NoAnswerCauseTable.style.display= 'none';
						NeedContinueAnswerTable.style.display= 'none';
						NeedAnswerTable.style.display='none';
						fm.all('DelayAnswerType_ch').value = "长时延时";
						fm.all('DelayAnswerType').value = "2";
						fm.all('DelayBeginDate').value = "";
						fm.all('DelayBeginTime').value = "";
						fm.all('DelayEndDate').value = "";
						fm.all('DelayEndTime').value = "";
						fm.all('ExpectRevertDate').value = "";
						fm.all('ExpectRevertTime').value = "";
						fm.all('PromiseAnswerType').value = "";
						fm.all('PromiseAnswerType_ch').value = "";
						fm.all('IsAnswer').value = "";
						fm.all('IsAnswer_ch').value = "";
						//NotAnswerTable.style.display= ''; 
					}//if
					else
						{
							if(Field.value=="2")
							{
       					NoAnswerCauseTable.style.display= ''; 
								NeedAnswerTable.style.display='none';
								
							}
						}//else
				 }//if

	      if(codeName=="hmreplystate")		
				{  
					if(fm.all('ReplyState').value == "1")
					{ //alert(fm.LogState.value());
						divConsultDelay1.style.display= '';
						divConsultDelay3.style.display='';
						divConsultDelay2.style.display='none';         
						divConsultDelay4.style.display='none';
						divConsultDelay5.style.display='none';         
						divConsultDelay6.style.display='none';
					
					}//if
					
					if(fm.all('ReplyState').value=="2")
							{
								divConsultDelay1.style.display= 'none';
						    divConsultDelay3.style.display='none';
								divConsultDelay2.style.display='';           
								divConsultDelay4.style.display= ''; 
								divConsultDelay5.style.display='';           
								divConsultDelay6.style.display= ''; 
								
							}//if
					if(fm.all('ReplyState').value=="3")
							{
								divConsultDelay1.style.display= 'none';
						    divConsultDelay3.style.display='none';
								divConsultDelay2.style.display='none';        
								divConsultDelay4.style.display='none'; 
								divConsultDelay5.style.display='none';        
								divConsultDelay6.style.display='none'; 
								
							}//if
			 }//if

}




//将字符串str按照字符串ch拆分产生数组
function str2Array(str,ch)
{
 var a=new Array();
 var i=0,j=-1,k=0;
 while (i<str.length)
 {  
  j=str.indexOf(ch,j+1);   
  if (j!=-1)
  {
   if (j==0){a[k]="";}else{a[k]=str.substring(i,j);}
   i=j+1;
  }else
  {
   a[k]=str.substring(i,str.length);
   i=str.length;
  }
  k++;
 }
 return a;
}


function inputDescriptionDetail()
{ 
	divDescriptionDetail.style.display='';
	 fm.all('textDescriptionDetail').value=fm.all('DescriptionDetail').value;
}



function backDescriptionDetail()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('DescriptionDetail').value=fm.all('textDescriptionDetail').value;
		fm.all('textDescriptionDetail').value="";
		divDescriptionDetail.style.display='none';
	}
}


function inputApplyServiceDes()
{ 
	divApplyServiceDes.style.display='';
	fm.all('textApplyServiceDes').value = fm.all('ApplyServiceDes').value;
}



function backApplyServiceDes()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('ApplyServiceDes').value=fm.all('textApplyServiceDes').value;
		fm.all('textApplyServiceDes').value="";
		divApplyServiceDes.style.display='none';
		}
}

function inputExcuteDescription()
{ 
	divExcuteDescription.style.display='';
	fm.all('textExcuteDescription').value = fm.all('ExcuteDescription').value;
}

function backExcuteDescription()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		fm.all('ExcuteDescription').value=fm.all('textExcuteDescription').value;
		fm.all('textExcuteDescription').value="";
		divExcuteDescription.style.display='none';
	}
}


function queryCustomerNo() {
  if(fm.all('LogerNo').value == "")	{
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("../sys/LDPersonQuery.html");
  }
  if(fm.all('LogerNo').value != "") {
    var cCustomerNo = fm.LogerNo.value;  //客户代码
    var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.LogerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    } else {
      //fm.DiseasCode.value="";
      alert("客户代码为:["+fm.all('LogerNo').value+"]的客户不存在，请确认!");
    }
  }
}


function queryCustomerNo2() {

  if(fm.all('LogerNo').value != ""&& fm.all('LogerNo').value.length==24) {
    var cCustomerNo = fm.LogerNo.value;  //客户号码
    var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
    // alert(arrResult);
    if (arrResult != null) {
      fm.LogerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    } else {
      //fm.DiseasCode.value="";
      alert("客户代码为:["+fm.all('LogerNo').value+"]的客户不存在，请确认!");
    }
  }
}


function inputQuestion(a)
{ 
	divQuestion.style.display='';	
	fm.all('textQuestion').value=fm.all('CContent').value;
}


function backQuestion()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) 
	{
		fm.all('CContent').value=fm.all('textQuestion').value;
		fm.textQuestion.value="";
		divQuestion.style.display='none';	
	}
}

function backQuestion2()
{
	  fm.all('CContent').value=fm.all('textQuestion').value;
		fm.textQuestion.value="";
		divQuestion.style.display='none';
}

function dLogTime()
{	
  var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.all('LogTime').value = h+":"+m;
}

function dDelayBeginTime()
{
  var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.DelayBeginTime.value = h+":"+m;	
}



function dDelayEndTime()
{
  var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.DelayEndTime.value = h+":"+m;	
}

function dExpectRevertTime()
{
  var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.ExpectRevertTime.value = h+":"+m;	
}

var hour_cha;
var minute_cha;
function dAnswerTime()
{
  var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	
	hour_cha = h;
	minute_cha = m;
	
	fm.AnswerTime.value = h+":"+m;	 
	fm.AnswerEndDate.value = fm.AnswerDate.value;
}

function dAnswerEndTime()
{
  var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.AnswerEndTime.value = h+":"+m;	
	hour_cha = fm.AnswerTime.value.substring(0,2); 
	minute_cha = fm.AnswerTime.value.substring(3,5); 
	if((h-hour_cha)<0)
	{alert("解答时间输入有误");return;}
	if((h-hour_cha)<1)
	{
			if((m-minute_cha)<0)      
			{alert("解答时间输入有误");return;}
			else{fm.AnswerCostTime.value = (m-minute_cha);}
	}
	else
	{
			if(m<minute_cha)
			{
				fm.AnswerCostTime.value = (h-hour_cha)*60-(minute_cha-m);
			}
		   else
		   	{
		   		fm.AnswerCostTime.value = (h-hour_cha)*60+(m-minute_cha);
		   	}
	}
}

function dAnswerEndTime2()
{                                                      
	var h = fm.AnswerEndTime.value.substring(0,2);         
	var m = fm.AnswerEndTime.value.substring(3,5);       

	hour_cha = fm.AnswerTime.value.substring(0,2); 
	minute_cha = fm.AnswerTime.value.substring(3,5);       
	if((h-hour_cha)<0)      
	{alert("解答时间输入有误");return;}
	if((h-hour_cha)<1)
	{
			if((m-minute_cha)<0)                           
			{alert("解答时间输入有误");return;}                       
			else{fm.AnswerCostTime.value = (m-minute_cha);}
	}
	else
	{
			if(m<minute_cha)
			{
				fm.AnswerCostTime.value = (h-hour_cha)*60-(minute_cha-m);
			}
		   else
		   	{
		   		fm.AnswerCostTime.value = (h-hour_cha)*60+(m-minute_cha);
		   	}
	}
}