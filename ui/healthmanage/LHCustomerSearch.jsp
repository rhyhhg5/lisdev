<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称： LHCustomerSearch.jsp
//程序功能:
//创建日期: 2007-03-24 14:25:18
//创建人  : CrtHtml程序创建
//更新记录:
//更新人  : 
//更新日期: 2006-06-13 11:00:18
//更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LHCustomerSearch.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="LHCustomerSearchInit.jsp"%>
</head>

<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>

<body onload="initForm(); initElementtype();" >
<form method=post name=fm target="fraSubmit">
	<table>
		<tr>
			<td>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCont);">
    		</td>
			<td class= titleImg>
				契约查询条件
			</td>   		 
		</tr>
	</table>

<Div  id= "divCont" style= "display: ''">
	<table  class= common align='center' >
		<TR  class= common>
		    <TD  class= title>
		      保单号
		    </TD>
		    <TD  class= input>
		      <Input class=common name=ContNo >
		    </TD>
		    <TD  class= title>
		      	服务项目代码
		    </TD>
		     <TD  class= input>
		     	  <Input class= 'code' name=ServItemCode ondblclick="showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
		    </TD> 
		    <TD  class= title>
				服务项目名称
		    </TD>
		    <TD  class= input colSpan=3>
		      <Input class= 'code' name=ServItemName  ondblclick="showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
		    </TD>
		</TR>
		<TR  class= common>
			<TD  class= title>
				性别
			</TD>
			<TD  class= input8> 
				<input class=codeno CodeData="0|8^0|男^1|女^2|其他 "  name=Sex ondblclick="return showCodeListEx('Sex',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Sex',[this,SexName],[0,1],null,null,null,1);"><input class=codename name=SexName>
			</TD>
		    <TD  class= title>
				出生日期（起始）
			</TD>
			<TD  class= input>
				<Input class= 'coolDatePicker' dateFromat="Short" name=StartDate >  
		    </TD>
		    <TD  class= title>
				出生日期（终止）
			</TD>
			<TD  class= input>
				<Input class= 'coolDatePicker' dateFromat="Short" name=EndDate >  
			</TD>
		</TR>
		<!--TR class= title >
			<TD  class= title>
				记录日期（起始）
			</TD>
			<TD  class= input>
				<Input class= 'coolDatePicker' dateFromat="Short" name=RecordStartDate >  
			</TD>
			<TD  class= title>
				记录日期（终止）
			</TD>
			<TD  class= input>
				<Input class= 'coolDatePicker' dateFromat="Short" name=RecordEndDate >  
			</TD>
		</TR-->
	</table>
		<!-- 一个中间的多行,把 契约查询条件 这个table项分开 -->
			<Div id="ServerQues" style= "display: ''">
				<table class=common>
					<tr class=common>
					<td text-align:left colSpan=1>
						<span id="spanServerGrid">
						</span> 
					</td>
					</tr>
				</table>
			</Div>   
</Div>

<table>
	<tr>
		<td>
		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,HealthItem);">
		</td>
	<td class= titleImg>
		医疗项目条件
	</td>   		 
	</tr>
</table>
  
<Div id="HealthItem" style= "display: ''">
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1>
				<span id="spanHealtItemGrid">
				</span> 
			</td>
		</tr>
	</table>
</Div>    



<table>
	<tr>
		<td>
		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DiseaseQues);">
		</td>
	<td class= titleImg>
		疾病查询条件
	</td>   		 
	</tr>
</table>
    
<Div id="DiseaseQues" style= "display: ''">
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1>
				<span id="spanDiseaseItemGrid">
				</span> 
			</td>
		</tr>
	</table>
</Div>    


<table>
	<tr>
		<td>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,HealthQues);">
		</td>
		<td class= titleImg>
			健康问题条件
		</td>   		 
	</tr>
</table>




<Div id="HealthQues" style= "display: ''">
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1>
				<span id="spanHealthQuesGrid">
				</span> 
			</td>
		</tr>
	</table>
</Div>
<Div id="div1" style= "display: ''">
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1>
				<span id="spanLHHighPartGrid">
				</span> 
			</td>
		</tr>
	</table>
</Div>



<hr>
<table class= common border=0 width=100%>
  	<TR class= common>
  		<TD class=title>
			<Input value="查询" style="width:130px"  type=button class=cssbutton onclick="mainQuery();">
			<Input value="重置" type=hidden class=cssbutton onclick="inputClear();">
  	 	</TD>
  	</TR>
</table>  

<Div id="div1" style= "display: ''">
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1>
				<span id="spanHospitalComGrid">
				</span> 
			</td>
		</tr>
	</table>
</Div>



<table>
	<tr>
		<td>
		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,ServiceInformation);">
		</td>
	<td class= titleImg>
		服务信息
	</td>   		 
	</tr>
</table>
    
<Div id="ServiceInformation" style= "display: ''">
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1>
				<span id="spanLHSettingSlipQueryGrid">
				</span> 
			</td>
		</tr>
	</table>
</Div>  

<Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
</Div>
<hr>
<table  class= common align='center' >
  	
  	<TR  class= common>
  		<TD  class= title>
	      	服务事件类型
	    </TD>
	    <TD  class= input>
	    	  <Input type=hidden name=ServeEventStyle>
	     	  <Input class= 'common' readonly name=ServeEventStyle_ch verify="服务事件类型|notnull" elementtype=nacessary > 
	    </TD> 
	    <TD  class= title>
	      	服务事件编号
	    </TD>
	    <TD  class= input>
	     	  <Input class= 'common' readonly name=ServeEventNum verify="服务事件编号|notnull" elementtype=nacessary > 
	    </TD> 
	    <TD  class= title>
	      	服务事件名称
	    </TD>
	    <TD  class= input>
	     	  <Input class= 'common' readonly name=ServeEventName verify="服务事件名称|notnull" elementtype=nacessary > 
	    </TD> 
	
	</TR>
	<TR class= common>
		<td class=button width="10%" align=right>
			<INPUT class=cssButton name="querybutton" style="width:80px" VALUE="服务事件查询"  TYPE=button onclick="return queryClick();">
		</td>
  		<TD class=title>
			<Input value="添加服务信息" style="width:80px"  type=button class=cssbutton onclick="AddServCaseOld();">
			<Input value="重置" type=hidden class=cssbutton onclick="inputClear();">
  	 	</TD>
  	 	
  	 	<!--TD class=title>
			<Input value="导入事件查询" style="width:130px"  type=button class=cssbutton onclick="mainQuery();">
			<Input value="重置" type=hidden class=cssbutton onclick="inputClear();">
  	 	</TD-->
  	</TR>
  	
		
		
</table>  
<input type=hidden name=MakeDate>
<input type=hidden name=MakeTime>
<input type=hidden name=fmtransact>

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>



