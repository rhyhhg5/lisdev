//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput2() == false ) return false;
	fm.fmtransact.value="INSERT||MAIN";
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHServerItemPrice.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHServerItemPriceQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function FindInfo()
{
				var strSql = " select a.servitemcode, a.servitemname,a.servitemnote "
			        	+ " from LHHealthServItem a  where 1=1  "
                + getWherePart("a.servitemcode", "ServItemCode","like")
                + getWherePart("a.servitemname", "ServItemName","like")
                ;
 // alert(strSql);
 // alert(easyExecSql(strSql));
	turnPage.queryModal(strSql, ItemGrid);
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		var strSql = "select ServItemCode, ServItemName, ServPriceCode, ServPriceName,  makedate, maketime from LHServerItemPrice where ServItemCode = '"+arrResult[0][0]+"'";
		turnPage.queryModal(strSql, ItemPriceGrid);
		
//        fm.all('ServItemCode').value= arrResult[0][0];
//        fm.all('ServItemName').value= arrResult[0][1];
//        fm.all('ServPriceCode').value= arrResult[0][2];
//        fm.all('ServPriceName').value= arrResult[0][3];
        fm.all('ComID').value= arrResult[0][4];fm.all('ComID_cn').value = easyExecSql("select showname from ldcom where  sign = '1' and comcode = '"+arrResult[0][4]+"' and length(trim(comcode)) = 4 ");
//        fm.all('MakeDate').value= arrResult[0][5];
//        fm.all('MakeTime').value= arrResult[0][6];
	}
}               
        
function toServCost()
{
	if(ItemPriceGrid.getSelNo() < 1)
	{
		alert("请先选择定价方式信息");
		return false;	
	}
	window.open("LHServCostInputMain.jsp?PriceCode="+ItemPriceGrid.getRowColData(ItemPriceGrid.getSelNo()-1,3),"",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');	
}

function toOperateCost()
{
	if(ItemPriceGrid.getSelNo() < 1)
	{
		alert("请先选择定价方式信息");
		return false;	
	}
	window.open("LHBusinCostInputMain.jsp?PriceCode="+ItemPriceGrid.getRowColData(ItemPriceGrid.getSelNo()-1,3),"",'width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');	
}

function toPriceFactorSetting()
{

	if(ItemGrid.getSelNo()=="")
	{
		alert("请先选择服务项目信息!");
		//showOne();
		return false;
	} 
  	if(fm.all('ComID').value=="")
	{
		alert("请选择机构代码!");
		return false;
	}
	//alert(ItemGrid.getSelNo());
	if(ItemGrid.getSelNo()==""&&fm.all('ComID').value==""&&ItemGrid.getSelNo()!=1)
	{
		  alert("请先选择服务项目信息和机构信息代码!");
	    return false;	
	}
	else
	{
		var ComID=fm.all('ComID').value;
		var ServItemCode=ItemGrid.getRowColData(ItemGrid.getSelNo()-1,1);
		window.open("./LHItemPriceFactorMain.jsp?ComID="+ComID+"&ServItemCode="+ServItemCode+"","服务价格表设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
    }
}


function afterCodeSelect(codeName,Field)
{
	if( ItemGrid.getSelNo() > 0)
	{
		if(codeName=="hmhospitalmng")	
		{
			var getSelNo = ItemGrid.getSelNo();
			var ServItemCode = ItemGrid.getRowColData(getSelNo-1, 1);
			var sql = " select servitemcode, servitemname, servpricecode, servpricename, MakeDate, MakeTime from LHServerItemPrice "
			 +" where servitemcode = '"+ServItemCode+"' and comid like '"+fm.all('ComID').value+"%%'"
			 ;
			turnPage.queryModal(sql, ItemPriceGrid);
		}
	}
}

function showOne()
{	
	var getSelNo = ItemGrid.getSelNo();
	var ServItemCode = ItemGrid.getRowColData(getSelNo-1, 1);
	var sql = " select servitemcode, servitemname, servpricecode, servpricename, MakeDate, MakeTime from LHServerItemPrice "
			 +" where servitemcode = '"+ServItemCode+"' and comid like '"+fm.all('ComID').value+"%%'"
			 ;
	turnPage.queryModal(sql, ItemPriceGrid);		 
}

function addEvent()
{ 
	//alert(ItemGrid.getSelNo()); 
	if(ItemGrid.getSelNo()=="")
	{
		  alert("请先选择服务项目信息!");
      initItemPriceGrid();
      return false;
	} 
  if(fm.all('ComID').value=="")
	{
		  alert("请选择机构代码!");

      showOne();
      return false;
	}  
	if(ItemGrid.getSelNo()==""&&fm.all('ComID').value=="")
	{
      alert("请先选择服务项目信息和机构代码!");
      initItemPriceGrid();
      return false;
	}
	else
  {
	    var getSelNo = ItemGrid.getSelNo();
	    var ServItemCode = ItemGrid.getRowColData(getSelNo-1, 1);
	    var ServItemName = ItemGrid.getRowColData(getSelNo-1, 2);
	    var servpricecode=ServItemCode+fm.all('ComID').value;
	    //alert(servpricecode);
	    ItemPriceGrid.setRowColData(ItemPriceGrid.mulLineCount-1, 1, ServItemCode);
	    ItemPriceGrid.setRowColData(ItemPriceGrid.mulLineCount-1, 2, ServItemName);
	    ItemPriceGrid.setRowColData(ItemPriceGrid.mulLineCount-1, 3, servpricecode);  
	 }
}