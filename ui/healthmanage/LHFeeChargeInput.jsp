<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHFeeChargeInput.jsp
//程序功能：健康费用结算管理
//创建日期：2006-09-21 9:39:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="LHFeeChargeInput.js"></SCRIPT>
  <%@include file="LHFeeChargeInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();" >
  <form action="LHFeeChargeSave.jsp" method=post name=fm >
    <table>
    	<tr>
    		<td>
    		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHFeeChargeGrid);">
    		</td>
    		<td class= titleImg>
        	服务事件任务信息
       	</td>
    	</tr>
    </table>
	<Div  id= "divLHFeeChargeGrid" style= "display: ''">
		<Div id="LHCaseInfo" style="display:'' ">
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHFeeChargeGrid">
		  				</span> 
				    </td>
				</tr>
			</table>
		</div>
	</Div>
	
	
	<Div id="LHTestInfo" style="display:'none' ">
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanCustomTestInfoGrid">
	  				</span> 
			    </td>
			</tr>
		</table>
	</div>
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	<table>
		<tr>
			<td>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServBespeakManageInfoGrid);">
			</td>
			<td class= titleImg>
				服务费用结算信息
			</td>   		 
		</tr>
    </table>
    
    <table  class= common align='center' >
		<TR  class= common>   
			<TD class= title>
         		实际结算费用
         	</TD>
			<TD  class= input>
         		<Input  class=common name=Fee >
         	</TD> 
			<TD  class= title>
				执行状态
			</TD>
			<TD  class= input>
				<Input class=codeno name=ExecState VALUE="2" CodeData= "0|^未执行|1^已执行|2" ondblClick= "showCodeListEx('hmexecstate',[this,ExecState_ch],[1,0],null,null,null,1,100);"><Input class= 'codename' name=ExecState_ch VALUE="已执行" >
			</TD>
         	<TD class= title></TD><TD  class= input></TD> 
        </tr>
	</table>	
		<table  class= common border=0 width=100%>
			<TR  class= common>
		      	</td>
					<TD  class=common>
						<INPUT VALUE="添加费用" style='width:110'  TYPE=button name=save   class=cssbutton onclick="submitForm();">
					</td>
					<TD  class=common>
						<INPUT VALUE="修改费用"  style='width:110'  TYPE=button name=update  class=cssbutton onclick="updateClick();">
					</td>
				 <TD class=title></TD>
				 <TD  class= input style='width:700'></TD>
			</tr>
		</table>
	</Div>
	    
	<Div  id= "divLHServBespeakManageGrid" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>   
				<TD class= title>
         			医院名称
         		</TD>
         		<TD  class= input>
					<Input  class=codeno name= HospitCode verify="医院名称|notnull" ondblclick="showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName','',200);"   codeClear(HospitName,HospitCode);"><Input class= 'codename' name=HospitName style="width:110px" >
				</TD> 
        		<TD  class= title>
            		合同名称
       			</TD>
       			<TD  class= input>
           			<Input class=codeno name=ContraNo  ondblClick= "showCodeList('hmgrpcontrano',[this,ContraName],[0,1],null,fm.HospitCode.value,'ContraName','',200);"><Input class= 'codename' name=ContraName style="width:130px">
       			</TD>
				<TD  class= title>
        			合同责任项目名称
       			</TD>
        		<TD  class= input>
					<Input class=codeno name=DutyItemCode    ondblClick= "showCodeList('hmgrpdutyitemno',[this,DutyItemName],[0,1],null,fm.ContraNo.value,'DutyItemName','',200);"  codeClear(DutyItemName,DutyItemCode);"><Input class= 'codename' name=DutyItemName  style="width:130px">
        		</TD>
			</tr>
			<TR  class= common>          
          <TD  class= title>起始日期</TD>
          <TD  class= input> <Input name=StartDate style='width:160' class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>终止日期</TD>
          <TD  class= input> <Input name=EndDate style='width:180' class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> </TD> 
			<TD  class= title>
				确认状态
			</TD>
			<TD  class= input>
				<Input class=codeno name=AffirmState VALUE="0" CodeData= "0|^全部|0^未确认|1^已确认|2" ondblClick= "showCodeListEx('hmaffirmstate',[this,AffirmState_ch],[1,0],null,null,null,1,100);"><Input class= 'codename' style="width:130px" name=AffirmState_ch VALUE="全部" >
			</TD>
        </TR>
		</table>	
			<table  class= common border=0 width=100%>
			<TR  class= common>
		      	</td>
					<TD  class=common>
						<INPUT VALUE="打印费用清单" style='width:110'  TYPE=button name=printFeeNote   class=cssbutton onclick="feePrint();">
					</td>
				 <TD  class=common>
						<INPUT VALUE="打印结算单" style='width:110'  TYPE=button name=printFee   class=cssbutton onclick="printFeeInfo();">
					</td>
					<td style="width:750px;">
		  		</td>
			</tr>
		</table>
	</div>
	 <input type=hidden id=queryString name=queryString>
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="TaskExecNo" name="TaskExecNo">
	 <input type=hidden id="ServTaskNo" name="ServTaskNo" VALUE="<%=request.getParameter("ServTaskNo")%>">
	 <input type=hidden id="ServCaseCode" name="ServCaseCode">
	 <input type=hidden id="ServTaskCode" name="ServTaskCode">
	 <input type=hidden id="CustomerNo" name="CustomerNo">
	 <input type=hidden id="ServItemNo" name="ServItemNo">
	 <input type=hidden id="manageComPrt" name="manageComPrt">
	 <input type=hidden  name="tServTaskno">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
