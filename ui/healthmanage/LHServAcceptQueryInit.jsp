<%
//程序名称：LHIndivServAcceptInit.jsp
//程序功能：
//创建日期：2006-08-24 10:47:23
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类--> 
<%
        String ServTaskCode = request.getParameter("ServTaskCode");
        String sqlServTaskNo = "'"+ServTaskCode.replaceAll(",","','")+"'";
        String flag = request.getParameter("flag");
%>                          
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    //fm.all('GrpServPlanNo').value = "";
  }
  catch(ex)
  {
    alert("在LHIndivServAcceptInit.jsp->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    initServAcceptGrid();
    var accFlag = "<%=flag%>";
    //alert(accFlag);
    fm.all('accFlag').value=accFlag;
    if(accFlag=="1")
    {
    	  var taskNo="<%=sqlServTaskNo%>";
        fm.all('ServTaskNo').value=taskNo;
        var strSql=" select distinct  a.ServCaseCode,a.ServItemNO  from LHTASKCUSTOMERRELA a "
    	           +" where a.ServTaskno in ("+ fm.all('ServTaskNo').value +") ";
    	    //alert(strSql);
    	    //alert(easyExecSql(strSql));
    	    var relaRest=new Array;
    	    relaRest=easyExecSql(strSql);
    	    if(relaRest!=""&&relaRest!=null&&relaRest!="null")
    	    {  
    	    	 var caseCode=new Array;
    	    	 var itemCode=new Array;
    	    	 for (var i=0;i<relaRest.length;i++)
    	    	 {
    	    	 	 var caseNo=new Array;
    	    	 	 caseNo[i]=relaRest[i][0];
    	    	 	 caseCode[i]=caseNo[i];
    	    	 	 var itemNo=new Array;
    	    	 	 itemNo[i]=relaRest[i][1];
    	    	 	 itemCode[i]=itemNo[i];
    	    	 }
    	    	 fm.all('ServCaseCode').value= "'"+caseCode.toString().replace(",","','")+"'";
    	    	 fm.all('ServItemNO').value= "'"+itemCode.toString().replace(",","','")+"'";
    	    	 var saSql=" select  a.ServItemCode,a.Customerno  from LHSERVCASERELA a "
    	                +" where a.Servcasecode in ("+ fm.all('ServCaseCode').value +")"
    	                +" and a.ServItemNO in ("+ fm.all('ServItemNO').value +") ";
    	       //alert(saSql);
    	       var tRest=new Array;
    	       tRest=easyExecSql(saSql); 
    	       //alert(tRest);
    	       if(tRest!=""&&tRest!="null"&&tRest!=null)
    	       {
    	       	   var cusNo=new Array;
    	    	     var itCode=new Array;
    	    	     for (var i=0;i<relaRest.length;i++)
    	    	     {
    	    	     	 var cusCode=new Array;
    	    	     	 cusCode[i]=tRest[i][0];
    	    	     	 cusNo[i]=cusCode[i];
    	    	     	 var itNO=new Array;
    	    	     	 itNO[i]=tRest[i][1];
    	    	     	 itCode[i]=itNO[i];
    	    	     }
    	    	     fm.all('HidCusNo').value= "'"+cusNo.toString().replace(",","','")+"'";
    	    	     fm.all('HidItemCode').value= "'"+itCode.toString().replace(",","','")+"'";
    	    	     if(fm.all('CustomType').value=="1")//个人客户
    	    	     {
    	    	         var mulSql = "select "
	                           +" distinct a.CustomerNo,"
				                     +" (select d.Name from ldperson d where d.CustomerNo=a.CustomerNo),"
	                           +"  b.ServItemCode, "
	                           +"  ( select distinct c.ServItemName from   LHHealthServItem c  where  c.ServItemCode = b.ServItemCode),"
	                           +" a.ServAceptDate, "
	                           +" '',"
	                           +" a.ServAccepChannel ,"
	                           +" a.ServAccepNo,a.ServCaseCode,a.ModelTypeNo "
	                           + " from LHIndiServAccept a ,LHServCaseRela b "
	                           + " where  b.ServCaseCode = a.ServCaseCode "
	                           + " and b.ServCaseCode in a.ServCaseCode   "
	                           + " and b.ServItemCode = a.ServItemCode   "
	                           +" and b.ServItemCode in ("+ fm.all('HidCusNo').value +") "
	                           +" and  a.CustomerNo in ("+ fm.all('HidItemCode').value +") "
	                           ;
			               //alert(mulSql);
			               //alert(easyExecSql(mulSql));
			               turnPage.queryModal(mulSql, ServAcceptGrid); 
			              // fm.all('ExecServItem').disabled=true;
			           }
			           if(fm.all('CustomType').value=="2")//团体客户
	               {
	                	 initServAcceptGrid();
	               }    
    	       }     
    	   }
     }
  }
  catch(re)
  {
    alert("LHIndivServAcceptInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initServAcceptGrid() 
 {                            
   var iArray = new Array();                               
                                                           
   try 
   {                                                   
     iArray[0]=new Array();                                
     iArray[0][0]="序号";         		//列名               
     iArray[0][1]="30px";         		//列名               
     iArray[0][3]=0;         				//列名                 
     iArray[0][4]="";         //列名   
    
    iArray[1]=new Array(); 
		iArray[1][0]="客户号";   
		iArray[1][1]="120px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;
     
		iArray[2]=new Array(); 
		iArray[2][0]="客户姓名";   
		iArray[2][1]="120px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;

		
		iArray[3]=new Array(); 
		iArray[3][0]="服务项目代码";   
		iArray[3][1]="130px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;   
		
		iArray[4]=new Array(); 
		iArray[4][0]="服务项目名称";   
		iArray[4][1]="130px";   
		iArray[4][2]=20;  
		iArray[4][3]=0;         
		
		iArray[5]=new Array(); 
		iArray[5][0]="服务受理时间";   
		iArray[5][1]="100px";   
		iArray[5][2]=50;        
		iArray[5][3]=0;  
		
		
		iArray[6]=new Array(); 
		iArray[6][0]="服务执行情况";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3; 
		
		iArray[7]=new Array(); 
		iArray[7][0]="服务受理渠道";   
		iArray[7][1]="100px";   
		iArray[7][2]=20;        
		iArray[7][3]=0;  

		iArray[8]=new Array(); 
		iArray[8][0]="ServAccepNo";   
		iArray[8][1]="0px";   
		iArray[8][2]=20;        
		iArray[8][3]=3;  
		
		
		iArray[9]=new Array(); 
		iArray[9][0]="ServCaseCode";   
		iArray[9][1]="0px";   
		iArray[9][2]=20;        
		iArray[9][3]=3;  
		
	  iArray[10]=new Array(); 
		iArray[10][0]="ModelTypeNo";   
		iArray[10][1]="0px";   
		iArray[10][2]=20;        
		iArray[10][3]=3;  
     
     
    ServAcceptGrid = new MulLineEnter( "fm" , "ServAcceptGrid" );                         
    //这些属性必须在loadMulLine前                                                                                              
                                                                                                               
    ServAcceptGrid.mulLineCount = 0;                                          
    ServAcceptGrid.displayTitle = 1;                                          
    ServAcceptGrid.hiddenPlus = 1;                                            
    ServAcceptGrid.hiddenSubtraction = 1;                                     
    ServAcceptGrid.canSel = 1;                                                                       
     ;  
    ServAcceptGrid.loadMulLine(iArray);                                       
    //这些操作必须在loadMulLine后面                                          
    //LDDiseaseGrid.setRowColData(1,1,"asdf");                               
  }                                                                          
  catch(ex) {                                                                
    alert(ex);                                                               
  }                                                                          
}                                           
 
</script>
