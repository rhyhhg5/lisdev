<html> 
<%
//程序名称：LHQuesKineInput.jsp
//程序功能：
//创建日期：2006-09-19 14:18:39
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  	String custid = request.getParameter("custid");
  	String serviceItemNo = request.getParameter("serviceItemNo");
  	String custtype = request.getParameter("custtype");
  	String ServTaskNo = request.getParameter("ServTaskNo");
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<SCRIPT src="LHQuesKineInput.js"></SCRIPT>
	<%@include file="LHQuesKineInit.jsp"%>
</head>
	
<body  onload="initForm();">
  <form action="" method=post name=fm target="fraSubmit">
    <table align='center' >
    	<tr class=common align='center'' >
       	<td class=common >
			<font    size=6  height=75 style="font-weight:700" face="Bold"> 		 
			&nbsp;个人健康及生活方式信息记录表<br>
       	</td> 
    	</tr>
    </table>
<br>
<table border=0 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none' align='center'  width="800">
<tr class=common align='right'>
   <td class=common  width="265"></td>
   <td class=common  width="175"></td>
   <td class=common  width="175"></td>
   <!--<td class=common  width="275">&nbsp;医生签字:<input class=test type=text name=txtDoc size=20></td>-->
   <td class=common  width=210>
  	     填表日期：<Input class= 'coolDatePicker' dateFormat="Short"  style="width:115px" name=SubmitDate verify="日期|notnull&DATE"><font color=red> *</font>
  </td>
</tr>
</table>
    <table align='center' >
    	<tr class=common align='left'>
    		<td >
    				<hr size=2 color=#D5E2F9>
    	  </td>
      </tr>
    	<tr class=common align='left' >
       	<td class=common bgcolor=#D5E2F9>
       		 <font  align="left" size=3  > 
        	 	      欢迎您加入人保健康(PICCHEALTH)的"个人健康评估服务"项目。我们尊重您个人的隐私权，您所提供的信息将仅用于与您健康有关的服务。在未经您同意的情况下，任何其它个人或单位都不会获得与您个人有关的信息。
        	 </font> 
        </td>
      </tr>
      <tr class=common align='left'>
    		<td >
    				<hr size=2 color=#D5E2F9>
    	  </td>
      </tr>
     </table>
  <!--  <table align='center'> 
      <tr class=common align='center'>
      	<td class=common >
      		<strong>
    		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KYN个人编码</strong><input class=test type=text  name=oneTest style="width:20px;height:20px">&nbsp;<input class=test type=text name=twoTest style="width:20px;height:20px">&nbsp;<input class=test type=text name=threeTest style="width:20px;height:20px">&nbsp;<input class=test type=text name=fourTest style="width:20px;height:20px">&nbsp;<input class=test type=text  name=fiveTest style="width:20px;height:20px">&nbsp;<input class=test type=text name=sixTest style="width:20px;height:20px">
    		
    	  </td>
    	</tr> 
   </table>-->
   <br>
   <br>
   <table align='center'>
   		<tr  align='center' >
    		<td  >
    		
    		 </td>
    		 <td   align="center">
    		 	 <font  color=#2E3FD3  size=4 height=75 style="font-weight:700" face="Bold"> 	
    		  	  A&nbsp;一般信息
    		</td>
    		<td class=common >
    		 
    		 </td>
    	</tr>
  </table>
<br>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none'  align='center' width="800">
  	<tr class=common align='left'>
  	  <TD  class=common width="260px" align='left''><!--<input name=CustomerNo class=codeno style="width:26%;" ondblclick="showCodeList('hmldperson',[this,CustomerNo_ch],[0,1],null,fm.CustomerNo.value,'CustomerNo',1) " ><Input name=CustomerNo_ch class=codename style="width:84px;background-color: #D7E1F6;" ondblclick=" showCodeList('hmldperson',[CustomerNo,this],[0,1],null,fm.CustomerNo_ch.value,'Name',1) "  onkeyup=" if( event.Keycode ='13' ) showCodeList('hmldperson',[this],[0],null,fm.CustomerNo.value,'Name',1)">-->
  	  	&nbsp;&nbsp;&nbsp;姓&nbsp;&nbsp;&nbsp;&nbsp;名：<Input class= 'common' style="width:70px" name=CustomerNo ><Input class= 'code' style="width:90px" name=CustomerName elementtype=nacessary verify="客户姓名|notnull&len<=24" ondblclick="return queryCustomerNo();" ><font color=red> *</font>
  	  </TD>
  	  <td class=common  width="200px" align='left'>
        &nbsp;&nbsp;&nbsp;性&nbsp;&nbsp;&nbsp;&nbsp;别：&nbsp;<input type="hidden" value=2 name=08010002><input  type=radio class=hmradio name=sex onclick="checkSex('1');">男&nbsp;&nbsp;<input  type=radio class=hmradio name=sex checked onclick="checkSex('2');">女<font color=red> *</font>
  	  </td>
  	  <td class=common  width="250px">
  	     &nbsp;出生日期：<input type="hidden" name=08010001><Input class= 'coolDatePicker' dateFormat="Short"  style="width:133px" name=BirthDate verify="出生日期|NOTNULL&DATE" onblur="getBirthDay();"><font color=red> *</font>
  	  </td>
    </tr>
    <tr class=common align='left' >
  	  <td class=common   ><!--<input class=test type=text name=08010003 size=2 >-->
  	  	 &nbsp;&nbsp;&nbsp;职业代码：<Input class="code" name=08010003 elementtype=nacessary  ondblclick="return showCodeList('OccupationCode',[this,_08010003_1],[0,1],null,null,null,null,220);">
  	  </td>
  	  <td class=common   >
  	  	 &nbsp;&nbsp;&nbsp;职业名称：<Input class="common" name=_08010003_1  elementtype=nacessary TABINDEX="-1" readonly >
  	  </td>
    	<td class=common   ><!--<input class=test type=text name=08010004   size=2>-->
  		  &nbsp;文化程度：<Input class=codeno name=08010004 ondblClick="showCodeList('lhschoolage',[this,_08010004_1],[0,1],null,null,null,1,160);" ><Input class= 'codename' name=_08010004_1 >
  	  </td>
    </tr>
  </table>
<br>
<br>
<table align='center'>
   	<tr class=common align='center' >
    		<td class=common >
    		  
    		</td>
    		<td class=common  >
    		   <font color=#2E3FD3 size=4 align="center" height=75 style="font-weight:700" face="Bold"> 	 	
    		  	B&nbsp;目前健康状态及家族史
    		</td>
    		<td class=common >
    		  
    		</td>
   </tr>
</table>
  <br>
  <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center' width="800" >
        <tr class=common align='center'>
            <td class=common  colspan=5 align='center' bgcolor=ECF5FF height=25>
  			         <font  size=3 align="center" style="font-weight:700" face="Bold">一、目前健康状况
            </td>
  	  </tr>
  	  <tr class=common align='left'>
             <td class=common  width="800" colspan=5 height=25>
  			<font  size=2 face="Bold">&nbsp;1、您目前及曾经患下以种何种疾病?
             </td>
  	  </tr>
  	  <tr class=common align='left' >
	     <td class=common >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox name=04010001 >慢性支气管炎 
             </td>
             <td class=common >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010002 >肺气肿 
             </td>
             <td class=common  >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010003 >哮喘 
             </td>
             <td class=common  >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010004 onclick="HighBlood();">高血压 
             </td>
             <td class=common >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010005 >脑出血 
             </td>
  	  </tr>
  	  <tr class=common align='left' >
             <td class=common >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010006  >脑血栓 
             </td>
             <td class=common >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010007 >冠心病 
             </td>
             <td class=common  >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010008 onclick="HighBlood();">高血压性心脏病 
             </td>
             <td class=common  >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010009 >肺心病 
             </td>
             <td class=common >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010010  >先心病 
             </td>
  	  </tr>
  	  <tr class=common align='left' >
             <td class=common >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010011 >其它心脏病 
             </td>
             <td class=common >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010012 >I型糖尿病 
             </td>
             <td class=common  >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010013 >II型糖尿病 
             </td>
             <td class=common  >
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010014  >乳腺癌 
             </td>                                       
             <td class=common >                          
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox  name=04010015 >前列腺癌 
             </td>
  	  </tr>
  	  <tr>
             <td class=common  align='left'>
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox name=04010016 >肺癌 
             </td>
             <td class=common  align='left'>
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox name=04010017 >乳腺增生 
             </td>
             <td class=common  colspan=3 align='left'> 
  		&nbsp;&nbsp;<input  type=checkbox class=hmbox name=04010018 >其它疾病
             </td>
  	  </tr>
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center' width="800">
  	  <tr class=common align='center' >
             <td class=common  colspan=3 align='left' >
  		&nbsp;&nbsp;如果你是高血压患者，你有服高血压药物史吗?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="03010001" value=2><input  type=radio class=hmradio name=HighYesNo id=_03010001_1  onclick="testMedication('1');">是&nbsp;&nbsp;<input  type=radio class=hmradio  id=_03010001_2  name=HighYesNo checked onclick="testMedication('2');">否
             </td>
             <td class=common >
                 &nbsp;&nbsp; 药物名称：<input class=test type=text name=03010008 >
            </td>
  	  </tr>
</table>
<br>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center' width="800">
      <tr class=common align='left'>
              <td class=common   align='left' height=25 bgcolor=ECF5FF colspan=2>
  			<font  size=2 align="left">&nbsp;2、心电图诊断
             </td>
  	  </tr>
       <tr class=common align='left'>
              <td class=common   align='left' height=25 >
  			&nbsp;① 房颤
             </td>
             <td class=common >
                    &nbsp;&nbsp;&nbsp;<input type="hidden" name="05010022" value=2><input  type=radio class=hmradio name=RealYesNo onclick="getRadioInfo('05010022',1)">是&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=RealYesNo checked onclick="getRadioInfo('05010022',2)">否
             </td>
  	  </tr>
       <tr class=common align='left'>
              <td class=common  align='left' height=25>
  			&nbsp;② 左心室肥大
             </td>
             <td class=common >
                   &nbsp;&nbsp;&nbsp;<input type="hidden" name="05010023" value=2><input  type=radio class=hmradio name=LevoYesNo onclick="getRadioInfo('05010023',1)">是&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=LevoYesNo checked onclick="getRadioInfo('05010023',2)">否
             </td>
  	  </tr>
</table>
<br>
  <Div  id= "divWomanInfo" style= "display: ''">
 <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center' width="800">
       <tr class=common align='center'>
              <td class=common  colspan=3 align='left' height=25 bgcolor=ECF5FF>
  			<font  size=2 align="left">&nbsp;3、如果您是女性请回答以下问题：
             </td>
  	   </tr>
        <tr class=common align='left'>
              <td class=common   align='left' width="400px"> 
                     &nbsp;&nbsp;初潮年龄(岁)：<input class=test type=text name=03030001 size=8>
              </td>
              <td class=common   align='left' width="400px"> 
                     &nbsp;&nbsp;绝经年龄(岁)：<input class=test type=text name=03030002 size=8>
              </td>
             <td class=common   align='left' width="400px"> 
                     &nbsp;&nbsp;结婚年龄(岁)：<input class=test type=text name=03030003 size=8>
              </td>
       </tr>
       <tr class=common align='left'>
              <td class=common  align='left'width="400px">
                     &nbsp;&nbsp;生每个孩子时您的年龄(岁)：
              </td>
              <td class=common  align='left' colspan=2>
                    &nbsp;&nbsp;1、<input class=test type=text name=03030004 size=8>&nbsp;&nbsp;&nbsp;&nbsp;2、<input class=test type=text name=03030005 size=8>&nbsp;&nbsp;&nbsp;&nbsp;3、<input class=test type=text name=03030006 size=8>&nbsp;&nbsp;&nbsp;&nbsp;4、<input class=test type=text name=03030007 size=8>&nbsp;&nbsp;&nbsp;&nbsp;5、<input class=test type=text name=03030008 size=8>&nbsp;
              </td>
  	  </tr>
           <tr class=common align='left'>
              <td class=common  align='left' width="30%"  height=""80" >  
                     &nbsp;&nbsp;乳腺癌家方族史：
                     <br>
                     &nbsp;&nbsp;(没有填写"0")
              </td>
              <td class=common  align='left'colspan=2> 
                     &nbsp;&nbsp;1、您的母亲、姐妹及女儿中有多少人曾患乳腺癌?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input class=test type=text name=01010001 size=8> 个<br>
                     &nbsp;&nbsp;2、您的祖母、外祖母、姑姨、侄女、外甥女中是否曾人有人患乳腺癌?
                        <input type="hidden" name="01010002" value=2><input  type=radio class=hmradio name=WomYesNo onclick="getRadioInfo('01010002',1)">是&nbsp;&nbsp;<input  type=radio class=hmradio name=WomYesNo  onclick="getRadioInfo('01010002',2)" checked>否&nbsp;&nbsp;<input  type=radio class=hmradio name=WomYesNo onclick="getRadioInfo('01010002',3)">不知道
                     &nbsp;&nbsp;3、您的表姐妹中是否曾人有人患乳腺癌? &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="01010004" value=2><input  type=radio class=hmradio name=SisYesNo onclick="getRadioInfo('01010004',1)">是&nbsp;&nbsp;<input  type=radio class=hmradio name=SisYesNo onclick="getRadioInfo('01010004',2)" checked>否&nbsp;&nbsp;<input  type=radio class=hmradio name=SisYesNo onclick="getRadioInfo('01010004',3)">不知道
              </td>
  	  </tr>
       <tr class=common align='left'>
              <td class=common  align='left'colspan=3> 
                     &nbsp;&nbsp;您做过子宫切除手术吗?&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <input type="hidden" name="06010001" value=2><input  type=radio class=hmradio name=OpsYesNo onclick="getRadioInfo('06010001',1)">是&nbsp;&nbsp;<input  type=radio class=hmradio name=OpsYesNo onclick="getRadioInfo('06010001',2)" checked>否
              </td>
        </tr>
        <tr class=common align='left'>
              <td class=common  colspan=3 align='left' height=25>
  		     &nbsp;&nbsp;您多长时间做一次乳腺自我检查&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="03020001" value=4><input  type=radio class=hmradio name=MeCheck onclick="getRadioInfo('03020001',1)">每月&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=MeCheck onclick="getRadioInfo('03020001',2)">每隔数月&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=MeCheck onclick="getRadioInfo('03020001',3)">每年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=MeCheck onclick="getRadioInfo('03020001',4)" checked>很少或从未做过
         </td>
        </tr>
        <tr class=common align='left'>
              <td class=common  colspan=3 align='left' height=25>
           &nbsp;&nbsp;距上一次医生或护士给您检查乳腺有多长时间了?
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="03020002" value=4><input  type=radio class=hmradio name=YearCheck onclick="getRadioInfo('03020002',1)">少于1年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=YearCheck onclick="getRadioInfo('03020002',2)">1年前&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=YearCheck onclick="getRadioInfo('03020002',3)">2年前&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=YearCheck onclick="getRadioInfo('03020002',4)">3年前&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=YearCheck onclick="getRadioInfo('03020002',5)" checked>从未做过             
              </td>
        </tr>
        <tr class=common align='left'>
              <td class=common  colspan=3 align='left' height=25>
  		     &nbsp;&nbsp;您是否服用雌激素类的药物?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="hidden" name="03010002" value=2><input  type=radio class=hmradio name=MediYesNo  onclick="getMediInfo('1');">是&nbsp;&nbsp;<input  type=radio class=hmradio name=MediYesNo onclick="getMediInfo('2');" checked>否
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如是，服务用多长时间了&nbsp;&nbsp;&nbsp;&nbsp;<input class=test type=text name="03010003" size=13> 年         
              </td>
  	  </tr>
</table>
</div>
<br>
<Div  id= "divManInfo" style= "display: ''">
 <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
      <tr class=common align='center' rowspan=2> 
              <td class=common   align='left' height=25 bgcolor=ECF5FF>
  			<font  size=2 align="left">&nbsp;3、如果您是男性，请回答如下问题
             </td>
  	  </tr>
      <tr class=common align='left'>
            <td class=common   align='left' height=25>
  			&nbsp;距上次医生给您做前列腺检查有多长时间了?
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="03020003" value=5><input  type=radio class=hmradio name=MyearCheck onclick="getRadioInfo('03020003',1)">少于1年&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=MyearCheck onclick="getRadioInfo('03020003',2)">1年前&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=MyearCheck onclick="getRadioInfo('03020003',3)">2年前&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=MyearCheck onclick="getRadioInfo('03020003',4)">3年前&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=MyearCheck onclick="getRadioInfo('03020003',5)" checked>从未做过
             </td>
  	  </tr>
 </table>
 <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
          <tr class=common >
            <td class=common  rowspan=3 width="130"> 
             &nbsp;前列腺癌家族史
            </td>
            <td class=common width="470">&nbsp;1、您的父亲、兄弟及儿子中是否有人曾患前列腺癌?
            </td> 
            <td class=common > 
                <input type="hidden" name="01020001" value=2><input  type=radio class=hmradio name=ManYesNo onclick="getRadioInfo('01020001',1)">是&nbsp;<input  type=radio class=hmradio name=ManYesNo onclick="getRadioInfo('01020001',2)" checked>否&nbsp;<input  type=radio class=hmradio name=ManYesNo onclick="getRadioInfo('01020001',3)">不知道
            </td>
          </tr>
           <tr>
             <td class=common >&nbsp;2、您的祖父、外祖父、侄子、外甥中是否有人曾患前列腺癌?</td> 
             <td class=common > 
                <input type="hidden" name="01020002" value=2><input  type=radio class=hmradio name=FatYesNo onclick="getRadioInfo('01020002',1)">是&nbsp;<input  type=radio class=hmradio name=FatYesNo onclick="getRadioInfo('01020002',2)" checked>否&nbsp;<input  type=radio class=hmradio name=FatYesNo onclick="getRadioInfo('01020002',3)">不知道
            </td>
          </tr>
          <tr>
             <td class=common >&nbsp;3、您的表兄弟中是否有人曾患前列腺癌?</td>
             <td class=common > 
                <input type="hidden" name="01020003" value=2><input  type=radio class=hmradio name=BroYesNo onclick="getRadioInfo('01020003',1)">是&nbsp;<input  type=radio class=hmradio name=BroYesNo  onclick="getRadioInfo('01020003',2)" checked>否&nbsp;<input  type=radio class=hmradio name=BroYesNo  onclick="getRadioInfo('01020003',3)">不知道
            </td>
         </tr>    
</table>
</div>
<br>

   <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
       <tr class=common align='center' rowspan=2> 
              <td class=common   align='center' height=25 bgcolor=ECF5FF>
  			<font  size=3 align="center" style="font-weight:700" face="Bold">&nbsp;二、家族史 
             </td>
  	  </tr>
      <tr class=common align='left'>
              <td class=common   align='left' height=25>
  			&nbsp;请问您的亲属中是否有人曾患有以下疾病?
             </td>
  	  </tr>
 </table>
 <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800" bgcolor=ECF5FF>
          <tr class=common >
            <td class=common  rowspan=4  width="130"> 
            &nbsp;糖尿病
            </td>
            <td class=common width="470">&nbsp;1、父母、兄弟姐妹、子女
            </td> 
            <td class=common > 
                <input type="hidden" name="01030004" value=2><input  type=radio class=hmradio name=DiabYesNo  onclick="getForty('1');">是&nbsp;<input  type=radio class=hmradio name=DiabYesNo onclick="getForty('2');" checked>否&nbsp;<input  type=radio class=hmradio name=DiabYesNo onclick="getForty('3');">不知道
            </td>
          </tr>
           <tr>
             <td class=common >&nbsp;2、如是，是否有人在40 岁以前?</td> 
             <td class=common > 
                <input type="hidden" name="01030001" value=2><input  type=radio class=hmradio name=FortTesNo id=_01030001_1 onclick="getRadioInfo('01030001',1)">是&nbsp;<input  type=radio class=hmradio name=FortTesNo onclick="getRadioInfo('01030001',2)" id=_01030001_2 >否&nbsp;<input  type=radio class=hmradio name=FortTesNo id=_01030001_3 onclick="getRadioInfo('01030001',3)">不知道
            </td>
          </tr>
          <tr>
             <td class=common >&nbsp;3、(外)祖父母、叔舅、姑姨、侄子(女)、外甥(女)</td>
             <td class=common > 
                <input type="hidden" name="01030002" value=2><input  type=radio class=hmradio name=UnYesNo onclick="getRadioInfo('01030002',1)">是&nbsp;<input  type=radio class=hmradio name=UnYesNo onclick="getRadioInfo('01030002',2)" checked>否&nbsp;<input  type=radio class=hmradio name=UnYesNo onclick="getRadioInfo('01030002',3)">不知道
            </td>
         </tr>
          <tr>
             <td class=common >&nbsp;4、表兄妹</td>
             <td class=common > 
                <input type="hidden" name="01030003" value=2><input  type=radio class=hmradio name=CousinYesNo onclick="getRadioInfo('01030003',1)">是&nbsp;<input  type=radio class=hmradio name=CousinYesNo onclick="getRadioInfo('01030003',2)" checked>否&nbsp;<input  type=radio class=hmradio name=CousinYesNo onclick="getRadioInfo('01030003',3)">不知道
            </td>
         </tr>
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
       <tr class=common >
            <td class=common  rowspan=2  width="130"> 
            &nbsp;冠心病
            </td>
            <td class=common  width="470">&nbsp;1、父母
            </td> 
            <td class=common > 
                <input type="hidden" name="01040001" value=2><input  type=radio class=hmradio name=ParYesNo onclick="getFifty('1')">是&nbsp;<input  type=radio class=hmradio name=ParYesNo onclick="getFifty('2')" checked>否&nbsp;<input  type=radio class=hmradio name=ParYesNo onclick="getFifty('3')">不知道
            </td>
        </tr>
        <tr>
             <td class=common >&nbsp;2、如是，是否有人在50 岁以前?</td> 
             <td class=common > 
                <input type="hidden" name="01040002" value=2><input  type=radio class=hmradio name=FifYesNo id=_01040002_1 onclick="getRadioInfo('01040002',1)">是&nbsp;<input  type=radio class=hmradio name=FifYesNo  id=_01040002_2 onclick="getRadioInfo('01040002',2)" >否&nbsp;<input  type=radio class=hmradio  id=_01040002_3 name=FifYesNo onclick="getRadioInfo('01040002',3)">不知道
            </td>
        </tr>          
</table>
<!--
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
        <tr class=common >
            <td class=common  rowspan=2  width="130"> 
            &nbsp;心脏病
            </td>
            <td class=common  width="470">&nbsp;1、父母
            </td> 
            <td class=common > 
                <input type="hidden" name="" value=2><input  type=radio class=hmradio name=PartYesNo >是&nbsp;<input  type=radio class=hmradio name=PartYesNo checked>否&nbsp;<input  type=radio class=hmradio name=PartYesNo>不知道
            </td>
        </tr>
        <tr>
             <td class=common >&nbsp;2、如是，是否有人在50 岁以前?</td> 
             <td class=common > 
                <input type="hidden" name="" value=2><input  type=radio class=hmradio  name=FifyYesNo>是&nbsp;<input  type=radio class=hmradio name=FifyYesNo checked>否&nbsp;<input  type=radio class=hmradio name=FifyYesNo>不知道
            </td>
        </tr>          
</table>
-->
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800" bgcolor=ECF5FF>
        <tr class=common >
            <td class=common  rowspan=2  width="130"> 
            &nbsp;中 风
            </td>
            <td class=common  width="470">&nbsp;1、父母
            </td> 
            <td class=common > 
                <input type="hidden" name="01050001" value=2><input  type=radio class=hmradio name=PartsYesNo onclick="getSixty('1');">是&nbsp;<input  type=radio class=hmradio name=PartsYesNo  onclick="getSixty('2');" checked>否&nbsp;<input  type=radio class=hmradio name=PartsYesNo onclick="getSixty('3');">不知道
            </td>
         </tr>
         <tr>
             <td class=common >&nbsp;2、如是，是否有人在60 岁以前?</td> 
             <td class=common > 
                <input type="hidden" name="01050002" value=2><input  type=radio class=hmradio id=_01050002_1 name=SixYesNo onclick="getRadioInfo('01050002',1)">是&nbsp;<input  type=radio class=hmradio name=SixYesNo id=_01050002_2 onclick="getRadioInfo('01050002',2)"  >否&nbsp;<input  type=radio class=hmradio  id=_01050002_3 name=SixYesNo onclick="getRadioInfo('01050002',3)">不知道
            </td>
        </tr>          
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
         <tr class=common >
            <td class=common  rowspan=2  width="130"> 
            &nbsp;高血压
            </td>
            <td class=common  width="470">&nbsp;1、父母、兄弟姐妹、子女
            </td> 
            <td class=common > 
                <input type="hidden" name="01060001" value=2><input  type=radio class=hmradio name=FamYesNo onclick="getRadioInfo('01060001',1)">是&nbsp;<input  type=radio class=hmradio name=FamYesNo onclick="getRadioInfo('01060001',2)" checked>否&nbsp;<input  type=radio class=hmradio name=FamYesNo onclick="getRadioInfo('01060001',3)">不知道
            </td>
         </tr>
         <tr>
             <td class=common >&nbsp;2、(外)祖父母、叔舅、姑姨、侄子(女)、外甥(女)</td> 
             <td class=common > 
                <input type="hidden" name="01060002" value=2><input  type=radio class=hmradio name=PlyYesNo onclick="getRadioInfo('01060002',1)">是&nbsp;<input  type=radio class=hmradio name=PlyYesNo onclick="getRadioInfo('01060002',2)" checked>否&nbsp;<input  type=radio class=hmradio name=PlyYesNo onclick="getRadioInfo('01060002',3)">不知道
            </td>
        </tr>          
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800" bgcolor=ECF5FF>
        <tr class=common >
            <td class=common  rowspan=2  width="130"> 
            &nbsp;骨 折
            </td>
            <td class=common  width="470">&nbsp;父母及(外)祖父母中是否曾有人有过非外力性的骨折  &nbsp;<input type="image" name="" style="vertical-align:-20%"  title="此处的非外力行骨折是特指发生在肢体的、非外伤性骨折如老年人的股骨颈骨折"  src="../common/images/wenhao.gif"  border="0"> 
            </td> 
            <td class=common > 
                <input type="hidden" name="01090001" value=2><input  type=radio class=hmradio name=FampaYesNo onclick="getRadioInfo('01090001',1)">是&nbsp;<input  type=radio class=hmradio name=FampaYesNo onclick="getRadioInfo('01090001',2)" checked>否&nbsp;<input  type=radio class=hmradio name=FampaYesNo onclick="getRadioInfo('01090001',3)">不知道
            </td>
        </tr>         
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
          <tr class=common >
            <td class=common  rowspan=2  width="130"> 
            &nbsp;肺 癌
            </td>
            <td class=common  width="470">&nbsp;1、父母、兄弟姐妹、子女
            </td> 
            <td class=common > 
                <input type="hidden" name="01070001" value=2><input  type=radio class=hmradio name=ConYesNo onclick="getRadioInfo('01070001',1)">是&nbsp;<input  type=radio class=hmradio name=ConYesNo onclick="getRadioInfo('01070001',2)" checked>否&nbsp;<input  type=radio class=hmradio name=ConYesNo onclick="getRadioInfo('01070001',3)">不知道
            </td>
          </tr>
           <tr>
             <td class=common >&nbsp;2、(外)祖父母、叔舅、姑姨、侄子(女)、外甥(女)</td> 
             <td class=common > 
                <input type="hidden" name="01070002" value=2><input  type=radio class=hmradio name=ConplyYesNo onclick="getRadioInfo('01070002',1)">是&nbsp;<input  type=radio class=hmradio name=ConplyYesNo onclick="getRadioInfo('01070002',2)" checked>否&nbsp;<input  type=radio class=hmradio name=ConplyYesNo onclick="getRadioInfo('01070002',3)">不知道
            </td>
          </tr>          
</table>
<br>
<br>
<table align='center'>
   		<tr class=common align='center' >
    		<td class=common >
    	   
    		</td>
    		  <td class=common  >
    		 	 <font  color=#2E3FD3 size=4 align="center" height=75 style="font-weight:700" face="Bold">  	
    		  	 C&nbsp;膳食
    		</td>
    		<td class=common >
    		  
    		 </td>
    	</tr>
</table>
<br>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
          <tr class=common bgcolor=ECF5FF>
            <td class=common   rowspan=2 width="637"> 
            &nbsp;食 物 名 称 
            </td>
            <td class=common   width="120" colspan=3 align='center'> 
              &nbsp;&nbsp;&nbsp;&nbsp;平均食用次数&nbsp;<input type="image" name="ImageButton3" id="ImageButton3" title="填写“平均食用次数”时，请只选择其中一个最接近的食用频率周期，在此周期下填写相应的次数。如服务对象每周平均使用次数在7次以上，则按每天食用记录; 平均每次食用量只保留一位小数; 如果不吃某种食物，请在平均每次食用量下填“0”" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
            </td>
           <td class=common   width="190" rowspan=2 align='center'> 
            &nbsp;每次食用量 
            </td>
         </tr>
         <tr class=common >
            <td class=common align='center' width="70">每天 
            </td> 
            <td class=common align='center' width="70">每周 
            </td> 
            <td class=common align='center' width="70">每月
            </td> 
         </tr>            
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
        <tr class=common >
            <td class=common  rowspan=3  width="80"> 
           &nbsp;谷类 
            </td>
            <td class=common  width="384">&nbsp;大 米 类：米饭，米粥，其它
            </td> 
            <td class=common   width="55">  
                <input class=test type=text name=07010001 style="width:50px" onkeyup="getMealInfo(this.name,'07010002','07010003');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010002 style="width:50px"  onkeyup="getMealInfo(this.name,'07010001','07010003');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010003 style="width:50px" onkeyup="getMealInfo(this.name,'07010002','07010001');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010004 size=9 >&nbsp;两
           </td>
         </tr>
         <tr class=common >
             <td class=common  >&nbsp;面粉制品：馒头,烙饼,面包,面条,包子,饺子,其它&nbsp;<input type="image" name="ImageButton4" id="ImageButton4" title="面粉指小麦面粉" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" /></td> 
             <td class=common > 
                <input class=test type=text name=07010005 style="width:50px" onkeyup="getMealInfo(this.name,'07010006','07010007');">
            </td>
             <td class=common > 
                <input class=test type=text name=07010006 style="width:50px"  onkeyup="getMealInfo(this.name,'07010005','07010007');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010007 style="width:50px" onkeyup="getMealInfo(this.name,'07010005','07010006');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010008 size=9 >&nbsp;两
            </td>
        </tr>    
        <tr class=common >
            <td class=common  width="384">&nbsp;其它粮谷：小米,玉米,燕麦,荞麦,红薯,其它&nbsp;<input type="image" name="ImageButton5" id="ImageButton5" title="其它谷物包括：小米、玉米、高粱等杂粮；" onclick="javascript:return false;"src="../common/images/wenhao.gif" alt="" border="0" />
            </td> 
            <td class=common   width="55">  
                <input class=test type=text name=07010009 style="width:50px" onkeyup="getMealInfo(this.name,'07010010','07010011');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010010 style="width:50px"  onkeyup="getMealInfo(this.name,'07010009','07010011');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010011 style="width:50px" onkeyup="getMealInfo(this.name,'07010009','07010010');">
            </td>
           <td class=common>   
                <input class=test type=text name=07010012 size=9 >&nbsp;两
           </td>
        </tr>            
</table>

<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  bgcolor=ECF5FF width="800">
          <tr class=common >
            <td class=common  rowspan=3 width="80"> 
           &nbsp;肉 类
            </td>
            <td class=common  width="384">&nbsp;猪肉及制品&nbsp;<input type="image" name="ImageButton6" id="ImageButton6" title="猪肉包括肉、排骨肉和内脏等；" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
            </td> 
            <td class=common   width="55">  
                <input class=test type=text name=07010013 style="width:50px" onkeyup="getMealInfo(this.name,'07010014','07010015');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010014 style="width:50px" onkeyup="getMealInfo(this.name,'07010013','07010015');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010015 style="width:50px" onkeyup="getMealInfo(this.name,'07010014','07010013');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010016 size=9 >&nbsp;两
           </td>
         </tr>
         <tr>
             <td class=common >&nbsp;牛羊肉及制品&nbsp;<input type="image" name="ImageButton7" id="ImageButton7" title="牛羊肉包括肉、排骨肉和内脏" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" /></td> 
             <td class=common > 
                <input class=test type=text name=07010017 style="width:50px" onkeyup="getMealInfo(this.name,'07010018','07010019');">
            </td>
             <td class=common > 
                <input class=test type=text name=07010018 style="width:50px"  onkeyup="getMealInfo(this.name,'07010017','07010019');"> 
            </td>
            <td class=common > 
                <input class=test type=text name=07010019 style="width:50px" onkeyup="getMealInfo(this.name,'07010017','07010018');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010020 size=9 >&nbsp;两
            </td>
         </tr>  
         <tr>
             <td class=common >&nbsp;禽肉及制品&nbsp;<input type="image" name="ImageButton8" id="ImageButton8" title="禽肉包括鸡肉、鸭肉、鹅肉等禽类的肉" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" /></td> 
             <td class=common > 
                <input class=test type=text name=07010021 style="width:50px" onkeyup="getMealInfo(this.name,'07010022','07010023');">
            </td>
             <td class=common > 
                <input class=test type=text name=07010022 style="width:50px" onkeyup="getMealInfo(this.name,'07010021','07010023');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010023 style="width:50px" onkeyup="getMealInfo(this.name,'07010022','07010021');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010024 size=9 >&nbsp;两
            </td>
        </tr> 
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
         <tr class=common >
            <td class=common  colspan=2 width="300">&nbsp;鱼及水产品&nbsp;<input type="image" name="ImageButton9" id="ImageButton9" title="鱼虾类指各种水产动物性食品，如鱼、虾、蟹等" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010025 style="width:50px" onkeyup="getMealInfo(this.name,'07010026','07010027');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010026 style="width:50px"  onkeyup="getMealInfo(this.name,'07010025','07010027');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010027 style="width:50px" onkeyup="getMealInfo(this.name,'07010026','07010025');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010028 size=9 >&nbsp;两
           </td>
        </tr>          
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800" bgcolor=ECF5FF>
          <tr class=common >
            <td class=common  colspan=2 width="300">&nbsp;蛋类及制品&nbsp;<input type="image" name="ImageButton10" id="ImageButton10" title="蛋类包括鸡蛋、鸭蛋、鹅蛋等；摄入量以鸡蛋为准（1个鸡蛋约为1两）1个鹅蛋折合为2个鸡蛋；5个鹌鹑蛋可折合为一个鸡蛋，同时食用多种蛋时应进行累加；" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" /> 
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010029 style="width:50px" onkeyup="getMealInfo(this.name,'07010030','07010031');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010030 style="width:50px"  onkeyup="getMealInfo(this.name,'07010031','07010029');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010031 style="width:50px" onkeyup="getMealInfo(this.name,'07010030','07010029');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010032 size=9 >&nbsp;个
           </td>
        </tr>          
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
        <tr class=common >
            <td class=common  colspan=2 width="300">&nbsp;奶及奶制品&nbsp;<input type="image" name="ImageButton11" id="ImageButton11" title="奶及奶制品包括鲜牛奶、鲜羊奶、奶粉、酸奶等；一杯指中等大小（约250ml）的杯子；" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010033 style="width:50px" onkeyup="getMealInfo(this.name,'07010034','07010035');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010034 style="width:50px"  onkeyup="getMealInfo(this.name,'07010033','07010035');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010035 style="width:50px" onkeyup="getMealInfo(this.name,'07010033','07010034');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010036 size=9 >&nbsp;杯
           </td>
        </tr>          
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800" bgcolor=ECF5FF>
        <tr class=common >
            <td class=common  rowspan=2  width="80"> 
           &nbsp;干豆品 
            </td>
            <td class=common  width="384">&nbsp;干豆类：黄豆，绿豆，蚕豆，豌豆等&nbsp;<input type="image" name="ImageButton12" id="ImageButton12" title="干豆类包括黄豆，黑豆，绿豆，红豆，蚕豆和云豆等各种豆类及制品，但不包括各种豆芽；" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
            </td> 
            <td class=common   width="55">  
                <input class=test type=text name=07010037 style="width:50px" onkeyup="getMealInfo(this.name,'07010038','07010039');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010038 style="width:50px"  onkeyup="getMealInfo(this.name,'07010037','07010039');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010039 style="width:50px" onkeyup="getMealInfo(this.name,'07010037','07010038');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010040 size=9 >&nbsp;两
           </td>
         </tr>
         <tr>
             <td class=common >&nbsp;豆制品：豆腐，豆浆，各种豆制品&nbsp;<input type="image" name="ImageButton13" id="ImageButton13" title="豆制品指以大豆为原料加工的各种制品，如豆腐，豆浆，豆腐丝，豆腐皮和腐乳等" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" /></td> 
             <td class=common > 
                <input class=test type=text name=07010069 style="width:50px" onkeyup="getMealInfo(this.name,'07010070','07010071');">
            </td>
             <td class=common > 
                <input class=test type=text name=07010070 style="width:50px"  onkeyup="getMealInfo(this.name,'07010069','07010071');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010071 style="width:50px" onkeyup="getMealInfo(this.name,'07010069','07010070');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010072 size=9 >&nbsp;两
            </td>
        </tr>          
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800" >
        <tr class=common >
            <td class=common  colspan=2 width="300">&nbsp;新 鲜 蔬 菜&nbsp;<input type="image" name="ImageButton14" id="ImageButton14" title="新鲜蔬菜包括各种鲜豆类、根茎类、瓜类（不包括西瓜、哈密瓜等），茄果类、嫩茎、叶、苔、花类蔬菜" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010041 style="width:50px" onkeyup="getMealInfo(this.name,'07010042','07010043');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010042 style="width:50px"  onkeyup="getMealInfo(this.name,'07010041','07010043');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010043 style="width:50px" onkeyup="getMealInfo(this.name,'07010041','07010042');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010044 size=9 >&nbsp;两
           </td>
       </tr>          
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800" bgcolor=ECF5FF>
        <tr class=common >
            <td class=common  colspan=2 width="300">&nbsp;新鲜水果及果汁&nbsp;<input type="image" name="ImageButton15" id="ImageButton15" title="新鲜水果指未经加工的，直接食用的水果；" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010045 style="width:50px" onkeyup="getMealInfo(this.name,'07010046','07010047');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010046 style="width:50px"  onkeyup="getMealInfo(this.name,'07010045','07010047');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010047 style="width:50px" onkeyup="getMealInfo(this.name,'07010045','07010046');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010048 size=9 >&nbsp;两
           </td>
        </tr>          
 </table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
          <tr class=common >
            <td class=common  colspan=2 width="300">&nbsp;咸 菜&nbsp;<input type="image" name="ImageButton16" id="ImageButton16" title="咸菜是指用盐腌制的各种菜类" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010049 style="width:50px" onkeyup="getMealInfo(this.name,'07010050','07010051');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010050 style="width:50px"  onkeyup="getMealInfo(this.name,'07010049','07010051');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010051 style="width:50px" onkeyup="getMealInfo(this.name,'07010049','07010050');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010052 size=9 >&nbsp;两
           </td>
          </tr>          
 </table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
          <tr class=common >
            <td class=common  colspan=2 width="300">&nbsp;糖&nbsp;<input type="image" name="ImageButton17" id="ImageButton17" title="糖包括白糖、砂糖、红糖、冰糖和各种糖果（水果糖，奶糖）等；" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010053 style="width:50px" onkeyup="getMealInfo(this.name,'07010054','07010055');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010054 style="width:50px" onkeyup="getMealInfo(this.name,'07010053','07010055');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010055 style="width:50px" onkeyup="getMealInfo(this.name,'07010053','07010054');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010056 size=9 >&nbsp;两
           </td>
          </tr>          
   </table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800" bgcolor=ECF5FF>
          <tr class=common >
            <td class=common  rowspan=3 width="80"> 
              &nbsp;酒 类 
            </td>
            <td class=common  width="384">&nbsp;白酒&nbsp;<input type="image" name="ImageButton18" id="ImageButton18" title="白酒指各种度数达38oC度的蒸馏酒；" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" /> 
            </td> 
            <td class=common   width="55">  
                <input class=test type=text name=07010057 style="width:50px" onkeyup="getMealInfo(this.name,'07010058','07010059');">
            </td>
            <td class=common   width="55">  
               <input class=test type=text name=07010058 style="width:50px"  onkeyup="getMealInfo(this.name,'07010057','07010059');">
            </td>
            <td class=common   width="55">  
                <input class=test type=text name=07010059 style="width:50px" onkeyup="getMealInfo(this.name,'07010057','07010058');">
            </td>
           <td class=common  >   
                <input class=test type=text name=07010060 size=9 >&nbsp;两
           </td>
          </tr>
           <tr>
             <td class=common >&nbsp;啤酒&nbsp;<input type="image" name="ImageButton19" id="ImageButton19" title="啤酒：一杯指中等大小（约250ml）的杯子；" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" /></td> 
             <td class=common > 
                <input class=test type=text name=07010061 style="width:50px" onkeyup="getMealInfo(this.name,'07010062','07010063');">
            </td>
             <td class=common > 
                <input class=test type=text name=07010062 style="width:50px"  onkeyup="getMealInfo(this.name,'07010061','07010063');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010063 style="width:50px" onkeyup="getMealInfo(this.name,'07010061','07010062');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010064 size=9 >&nbsp;杯
            </td>
          </tr>  
          <tr>
             <td class=common >&nbsp;葡萄酒&nbsp;<input type="image" name="ImageButton20" id="ImageButton20" title="果酒包括用葡萄酿造的酒（红葡萄酒，白葡萄酒）以及用其它果类酿造的酒；" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" /></td> 
             <td class=common > 
                <input class=test type=text name=07010065 style="width:50px"  onkeyup="getMealInfo(this.name,'07010066','07010067');">
            </td>
             <td class=common > 
                <input class=test type=text name=07010066 style="width:50px"  onkeyup="getMealInfo(this.name,'07010065','07010067');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010067 style="width:50px" onkeyup="getMealInfo(this.name,'07010065','07010066');">
            </td>
            <td class=common > 
                <input class=test type=text name=07010068 size=9 >&nbsp;两
            </td>
         </tr> 
   </table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
  <tr>
     <td class=common  width="468">&nbsp;您自己认为您的口味是：
     </td>
    <td class=common > 
    <input type="hidden" name="02010001" value=2><input  type=radio class=hmradio name=tasteYesNo onclick="getRadioInfo('02010001',1)">轻&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=tasteYesNo onclick="getRadioInfo('02010001',2)" checked>适中&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=tasteYesNo onclick="getRadioInfo('02010001',3)">重
    </td>
  </tr>
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
  <tr>
     <td class=common  width="468">&nbsp;油炸及多脂类食品食用习惯是：&nbsp;<input type="image" name="ImageButton21" id="ImageButton21" title="植物油包括花生油、豆油、菜籽油、芝麻油及各种色拉油；动物油包括猪油、牛油、羊油、黄油等。" onclick="javascript:return false;" src="../common/images/wenhao.gif" alt="" border="0" />
     </td>
    <td class=common > 
    <input type="hidden" name="02010002" value=2><input  type=radio class=hmradio name=habitYesNo onclick="getRadioInfo('02010002',1)">多(≥5次/周)&nbsp;<input  type=radio class=hmradio name=habitYesNo onclick="getRadioInfo('02010002',2)" checked>中(2-4次/周)&nbsp;<input  type=radio class=hmradio name=habitYesNo onclick="getRadioInfo('02010002',3)">少(0-1次/周)
    </td>
  </tr>
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center' height="25"  width="800">
  <tr class=common align='left''>
     <td class=common ><font style="font-weight:700">&nbsp;注：请选用一种最接近你食用次数的表示方法.如：你不每天吃米饭,但每周吃5次,即填每周一栏。
     </td>
  </tr>
</table>
<br>
<br>
   <table align='center'>
   		<tr class=common align='center' >
    		<td class=common >
    		 
    		</td>
    		<td class=common >
    		 	  <font  size=4 color=#2E3FD3  align="center" height=75 style="font-weight:700" face="Bold">  	 	
    		  	 D&nbsp;生活方式
    		</td>
    		<td class=common >
    		  
    		</td>
    	</tr>
  </table>
<br>
 <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
       <tr class=common align='center' rowspan=2> 
          <td class=common   align='center' height=25 bgcolor=ECF5FF>
  			     <font  size=3 align="center" style="font-weight:700" face="Bold">&nbsp;&nbsp;一、吸烟情况 
          </td>
  	  </tr>
 </table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
          <tr class=common >
            <td class=common  rowspan=9 width=170"> 
          &nbsp; 1、你现在吸烟吗?<br>
                 <p>
                 &nbsp;<input type="hidden" name="02020001" value=2><input  type=radio class=hmradio name=togeDay  onclick="YesNoSmoke('1')">是<br>&nbsp;&nbsp;&nbsp;&nbsp;请回答第2至5题&nbsp;<br><p>
                 &nbsp;<input  type=radio class=hmradio name=togeDay checked  onclick="YesNoSmoke('2')">否<br>&nbsp;&nbsp;&nbsp;&nbsp;请回答9和10题&nbsp;<br><p>
                 &nbsp;<input  type=radio class=hmradio name=togeDay onclick="YesNoSmoke('3')">戒烟<br>&nbsp;&nbsp;&nbsp;&nbsp;请回答第4至10题
            </td>
            <td class=common  width="220">&nbsp;2、你平均每天抽多少支香烟?</td> 
            <td class=common   width=130">  
                <input class=test type=text name=02020002 style="width:50px">&nbsp;支/天
            </td>
          </tr>
           <tr>
             <td class=common >&nbsp;3、您平均每月抽烟叶或自制卷烟的量是多少<input type="image" name="ImageButton27" id="ImageButton27" title="如果您不吸烟叶也不吸自制卷烟，请填写0。" onclick="javascript:return false;"  src="../common/images/wenhao.gif" alt="" border="0" /></td> 
             <td class=common > 
                <input class=test type=text name=02020006 style="width:50px">&nbsp;两/月
            </td>
          </tr>  
           <tr>
             <td class=common >&nbsp;4、你是多大年龄开始吸烟的?</td>
             <td class=common > 
             	  <input type="hidden" name="txtAge"  style="width:50px">
                <input class=test type=text name=02020007 style="width:50px" onblur="getWrongAge();" >&nbsp;岁 
            </td>
          </tr>
          <tr>
             <td class=common >&nbsp;5、你已经吸烟多少年了?</td> 
             <td class=common > 
             	  <input type="hidden" name="txtAgeYear"  style="width:50px">
                <input class=test type=text name=02020003 style="width:50px" onchange="getSmokeYear();">&nbsp;年
            </td>
          </tr> 
          <tr>
             <td class=common >&nbsp;6、如果戒烟，你是多大年龄戒的烟?</td> 
             <td class=common > 
                <input class=test type=text name=02020008 style="width:50px">&nbsp;岁
            </td>
          </tr> 
          <tr>
             <td class=common >&nbsp;7、戒烟前2年，您平均每天抽多少支香烟 </td> 
             <td class=common > 
                <input class=test type=text name=02020009 style="width:50px">&nbsp;支/天
            </td>
          </tr> 
          <tr>
             <td class=common >&nbsp;8、戒烟前2年，您平均每月抽烟叶或自制卷烟的量是多少?</td> 
             <td class=common > 
                <input class=test type=text name=02020010 style="width:50px">&nbsp;两/月
            </td>
          </tr> 
          <tr>
             <td class=common >&nbsp;9、和您一起工作的同事或一起生活的家人中是否有人吸烟?</td> 
             <td class=common > 
                <input type="hidden" name="02020011" value=2><input  type=radio class=hmradio name=togeYesNo id=_02020011_1 onclick="getSmokeTime('1')">是&nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio id=_02020011_2 name=togeYesNo onclick="getSmokeTime('2')" checked>否&nbsp;
            </td>
          </tr> 
          <tr>
             <td class=common >&nbsp;10、如果是，您平均每周和他们呆在一起的时间是?</td> 
             <td class=common > 
                 <input type="hidden" name="02020012" value=3><input  type=radio class=hmradio name=togeTime id=_02020012_1 onclick="getRadioInfo('02020012',1)">1-2天&nbsp;<input  type=radio class=hmradio name=togeTime id=_02020012_2 onclick="getRadioInfo('02020012',2)">3-6天&nbsp;<input  type=radio class=hmradio name=togeTime id=_02020012_3 onclick="getRadioInfo('02020012',3)" >7天
            </td>
          </tr> 
        </tr>
   </table>
<br>
<br>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
       <tr class=common align='center' rowspan=2> 
          <td class=common   align='center' height=25 bgcolor=ECF5FF>
  			    <font  size=3 align="center" style="font-weight:700" face="Bold">&nbsp;二、体力活动及锻炼 
          </td>
  	  </tr>
    </table>
 <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
          <tr class=common >
            <td class=common  rowspan=4  width="170"> 
             &nbsp;1、工作时间
            </td>
           <td class=common > 
              &nbsp;你平均每周工作多少天?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class=test type=text name=02040001 style="width:50px" verify="工作日|NOTNULL&num&value>=0&value<=7">&nbsp;天/周
            </td>
          </tr>
           <tr>
             <td class=common >&nbsp;你平均每天工作多少小时?&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class=test type=text name=02040002 style="width:50px" verify="工作时间|NOTNULL&num&value>=0&value<=24">&nbsp;小时/天
             </td> 
          </tr>
   </table>
 <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
          <tr class=common >
            <td class=common  rowspan=4  width="170"> 
             &nbsp;2、出行方式
            </td>
           <td class=common > 
              &nbsp;一般情况下，你的出行方式是：<input type="image" name="ImageButton22" id="ImageButton22" title="出行方式是指服务对象每天从家出发到达工作地点的交通方式;如果选择“基本呆在家中”,则无需再填写&quot;外出在交通上所需时间&quot;" onclick="javascript:return false;"  src="../common/images/wenhao.gif" alt="" border="0" />
             <input type="hidden" name="02040003" value=6><input  type=radio class=hmradio name=RideMode onclick="getRadioInfo('02040003',1)">步行&nbsp;<input  type=radio class=hmradio name=RideMode onclick="getRadioInfo('02040003',2)">自行车&nbsp;<input  type=radio class=hmradio name=RideMode onclick="getRadioInfo('02040003',3)">公共汽车&nbsp;<input  type=radio class=hmradio name=RideMode onclick="getRadioInfo('02040003',4)">摩托车&nbsp;&nbsp;<input  type=radio class=hmradio name=RideMode onclick="getRadioInfo('02040003',5)">其它车&nbsp;<input  type=radio class=hmradio name=RideMode onclick="getRadioInfo('02040003',6)" checked>基本呆在家中
            </td>
          </tr>
           <tr>
             <td class=common>&nbsp;一般情况下，你每次外出在交通上所需时间大概是多长?&nbsp;<input type="image" width="14" name="ImageButton23" id="ImageButton23" title="外出所用时间指服务对象每天到达工作地点所需的总时间" onclick="javascript:return false;"  src="../common/images/wenhao.gif" alt="" border="0" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class=test type=text name=02040004 style="width:50px">&nbsp;分钟
             </td> 
          </tr>
   </table>
 <table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
          <tr class=common >
            <td class=common  rowspan=5  width="170"> 
             &nbsp;3、体育锻炼
            </td>
          </tr>
          <tr>
           <td class=common > 
             &nbsp;您参加体育锻炼吗?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="02040005" value=2><input  type=radio class=hmradio name=execYesNo onclick="getExeice('1');">是&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=execYesNo onclick="getExeice('2');" checked>否
            </td>
          </tr>
           <tr>
             <td class=common >&nbsp;如果是，您平均每周锻炼几次?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class=test type=text name=02040006 style="width:50px">&nbsp;次/周
             </td> 
          </tr>
           <tr>
           <td class=common > 
              &nbsp;您最常用锻炼方式是什么?
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="02040007" value=1><input  type=radio class=hmradio name=ExisMode id=_02040007_1 onclick="getRadioInfo('02040007',1)" ><label for="ExisMode" >走路</label>&nbsp;&nbsp;<input  type=radio class=hmradio name=ExisMode id=_02040007_2 onclick="getRadioInfo('02040007',2)">跑步&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=ExisMode id=_02040007_3 onclick="getRadioInfo('02040007',3)">游泳 &nbsp;&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=ExisMode id=_02040007_4 onclick="getRadioInfo('02040007',4)">球类运动<input  type=radio class=hmradio name=ExisMode id=_02040007_5 onclick="getRadioInfo('02040007',5)">气功&nbsp;&nbsp;&nbsp;<input  type=radio id=_02040007_6 class=hmradio name=ExisMode onclick="getRadioInfo('02040007',6)">其他
            </td>
          </tr>
          <tr>
             <td class=common >&nbsp;您平均每次的锻炼时间是多少分钟?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input class=test type=text name=02040008 style="width:50px">&nbsp;分钟
             </td> 
          </tr>
   </table>
<br>
<br>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
       <tr class=common align='center' rowspan=2> 
           <td class=common   align='center' height=25 bgcolor=ECF5FF>
  			      <font  size=3 align="center" style="font-weight:700" face="Bold">&nbsp;三、精神及社会因素 
           </td>
  	  </tr>
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
  <tr class=common align='left'>
     <td class=common >&nbsp;1、 在上一年中，你经历了哪些不愉快或不幸的事情吗?&nbsp;(如，失业、致残、离婚、亲属死亡、重大疾病等)
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="02050001" value=2><input  type=radio class=hmradio name=DispMode onclick="getRadioInfo('02050001',1)">是&nbsp;&nbsp;&nbsp;<input  type=radio class=hmradio name=DispMode onclick="getRadioInfo('02050001',2)" checked>否
     </td>
</tr>
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
  <tr class=common align='left'>
     <td class=common >&nbsp;2、 和一年前比较，你认为自己现在的健康状况如何?<br>
       &nbsp;<input type="hidden" name="02050002" value=3><input  type=radio class=hmradio name=HealpMode onclick="getRadioInfo('02050002',1)">比一年前好多了&nbsp;<input  type=radio class=hmradio name=HealpMode onclick="getRadioInfo('02050002',2)"> 比一年前有所好转&nbsp;<input  type=radio class=hmradio name=HealpMode onclick="getRadioInfo('02050002',3)" checked> 和一年前一样&nbsp;<input  type=radio class=hmradio name=HealpMode onclick="getRadioInfo('02050002',4)"> 不如一年前&nbsp;<input  type=radio class=hmradio name=HealpMode onclick="getRadioInfo('02050002',5)">比一年前差多了
     </td>
</tr>
</table>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
  <tr class=common align='left'>
     <td class=common >&nbsp;3、 在过去一年里，由于您的情绪问题(如紧张、急躁、焦虑)，在工作或其它活动中是否出现了以下问题?
     </td>
</tr>
  <tr class=common align='left'>
     <td class=common >
     	   <input type="hidden" name="02050003" value=4>
        &nbsp;<input  type=radio class=hmradio name=tenspmode onclick="getRadioInfo('02050003',1)">减少了你的工作或其他活动时间<br>
        &nbsp;<input  type=radio class=hmradio name=tenspmode onclick="getRadioInfo('02050003',2)">没有完成你预期要完成的工作或活动<br>
        &nbsp;<input  type=radio class=hmradio name=tenspmode onclick="getRadioInfo('02050003',3)">没有能够象过去那样认真地工作或参加其他活动<br>
        &nbsp;<input  type=radio class=hmradio name=tenspmode onclick="getRadioInfo('02050003',4)" checked>没有影响
     </td>
</tr>
</table>
<br>
<br>
<table >
  	  <tr class=common  align='left' >
	     <td class=common >
  		&nbsp;&nbsp;<input  type=checkbox checked class=hmbox name=HealthTest onclick="showHealthTest();">是否填写体格检查询和实验室检查信息 
             </td>
      </tr>
</table>
<Div  id= "divLHQuesInputGrid" style= "display: ''">
    <table align='center'>
   		<tr class=common align='center' >
    		<td class=common >
    		
    		 </td>
    		  <td class=common  >
    		 	 <font  size=4 color=#2E3FD3 align="center" height=75 style="font-weight:700" face="Bold">  	 	
    		  	 E&nbsp;体格检查
    		  </td>
    		<td class=common >
    		  
    		 </td>
    	</tr>
  </table>
<br>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800">
  <tr class=common align='left'>
     <td class=common >&nbsp;身 高：<font color=red> *</font><input class=test  verify="身高|NOTNULL&num&value>50&value<250"  name=05010001 style="width:50px">&nbsp;cm <input type="image" name="ImageButton24" id="ImageButton24" title="身高测量记录到小数后一位，以厘米为单位，测量两次取平均值；" onclick="javascript:return false;"  src="../common/images/wenhao.gif" alt="" border="0" />   
     </td>
    <td class=common >&nbsp;体 重：<font color=red> *</font><input class=test  verify="体重|NOTNULL&num&value>25&value<125"  type=text name=05010002 style="width:50px">&nbsp;kg <input type="image" name="ImageButton25" id="ImageButton25" title="体重测量记录到小数后一位，以公斤为单位。" onclick="javascript:return false;"  src="../common/images/wenhao.gif" alt="" border="0" />
     </td>
    <td class=common >&nbsp;腰 围：<font color=red> *</font><input class=test  verify="腰围|NOTNULL&num&value>50&value<150"  type=text name=05010003 style="width:50px">&nbsp;cm 
     </td>
    <td class=common >&nbsp;臀 围：<font color=red> *</font><input class=test  verify="臀围|NOTNULL&num&value>50&value<200" type=text name=05010004 style="width:50px">&nbsp;cm
     </td>
    <td class=common >&nbsp;血 压：<font color=red> *</font><input class=test  verify="血压|NOTNULL&num&value>60&value<300"  type=text name=05010005 style="width:50px">/<input class=test type=text name=05010006 verify="血压|NOTNULL&num&value>50&value<300" style="width:50px">&nbsp;mmHg <input type="image" name="ImageButton26" id="ImageButton26" title="血压测量要取用户静息后的血压值" onclick="javascript:return false;"  src="../common/images/wenhao.gif" alt="" border="0" /> 
     </td>
  </tr>
</table>
<br>
<br>
    <table align='center'>
   		<tr class=common align='center' >
    		<td class=common >
    		
    		 </td>
    		  <td class=common  >
    		 	 <font  size=4 align="center" color=#2E3FD3 height=75 style="font-weight:700" face="Bold">  	 	
    		  	 F&nbsp;实验室检查
    		</td>
    		<td class=common >
    		  
    		 </td>
    	</tr>
  </table>
<br>
<table border=1 cellspacing=1 cellpadding=1 style='border-collapse:collapse;border:none' align='center'  width="800" >
<tr class=common align='center''>
     <td class=common >&nbsp;实验室检测指标
     </td>
    <td class=common >&nbsp;检测结果(单位)
     </td>
    <td class=common >&nbsp;实验室检测指标
     </td>
    <td class=common >&nbsp;检测结果(单位)
     </td>
  </tr>
<tr class=common align='left' bgcolor=ECF5FF>
     <td class=common >&nbsp;空腹血糖(Blood glucose, Glu)<font color=red> *</font>
     </td>
     <td class=common ><!--<input class=test type=text verify="空腹血糖|NOTNULL" name=05010007 style="width:50px">-->
      <Input class= 'codename' style="width:70px" name=05010007 verify="空腹血糖|NOTNULL&num&value>2&value<20"><Input class= 'codeno' style="width:50px" value=mmol/l  name=_05010007_1  ondblclick="showCodeList('selelimosis',[this,_05010007_1],[1,1],null,null,null,'1',130);" codeClear(_05010007_1,05010007);"> 
     </td>
    <td class=common >&nbsp;糖化血红蛋白(Glycohemoglobin, GHb)
     </td>
    <td class=common ><input class=test type=text name=05010015 style="width:70px" verify="糖化血红蛋白|num&value>1&value<20">&nbsp;%
     </td>
  </tr>
<tr class=common align='left'>
     <td class=common >&nbsp;总胆固醇(Total cholesterol)<font color=red> *</font>
     </td>
     <td class=common ><!--<input class=test type=text verify="总胆固醇|NOTNULL"  name=05010008 style="width:50px">-->
        <Input class= 'codename' style="width:70px" name=05010008 verify="总胆固醇|NOTNULL&num&value>1&value<20" ><Input class= 'codeno' style="width:50px" value=mmol/l name=_05010008_1  ondblclick="showCodeList('selecholes',[this,05010008],[1,0],null,null,null,'1',130);" codeClear(05010008,_05010008_1);"> 
     </td>
    <td class=common >&nbsp;胰岛素Insulin
     </td>
    <td class=common ><input class=test type=text name=05010016 style="width:70px" verify="胰岛素|num&value>1&value<300">&nbsp;pmol/l
     </td>
  </tr>
<tr class=common align='left' bgcolor=ECF5FF>
     <td class=common >&nbsp;甘油三脂(Triglycerides, TG)<font color=red> *</font>
     </td>
     <td class=common ><!--<input class=test type=text verify="甘油三脂|NOTNULL" name=05010009 style="width:50px">-->
          <Input class= 'codename' style="width:70px" name=05010009 verify="甘油三脂|NOTNULL&num&value>0&value<15"><Input class= 'codeno' style="width:50px" value=mmol/l name=_05010009_1  ondblclick="showCodeList('seleglycer',[this,05010009],[1,0],null,null,null,'1',130);" codeClear(05010009,_05010009_1);"> 
     </td>
    <td class=common >&nbsp;血红蛋白(Hemoglobin)
     </td>
    <td class=common ><input class=test type=text name=05010017 style="width:70px" verify="血红蛋白|num&value>50&value<500">&nbsp;g/l
     </td>
  </tr>
<tr class=common align='left'>
     <td class=common >&nbsp;高密度脂蛋白胆固醇(HDL cholesterol)<font color=red> *</font>
     </td>
     <td class=common ><!--<input class=test  type=text verify="高密度脂蛋白胆固醇|NOTNULL"  name=05010010 style="width:50px">-->
          <Input class= 'codename' style="width:70px" name=05010010 verify="高密度脂蛋白胆固醇|NOTNULL&num&value>0&value<15" ><Input class= 'codeno' style="width:50px" value=mmol/l name=_05010010_1  ondblclick="showCodeList('selehighchol',[this,05010010],[1,0],null,null,null,'1',130);" codeClear(05010010,_05010010_1);"> 
     </td>
    <td class=common >&nbsp;极低密度脂蛋白胆固醇(VLDL cholesterol)
     </td>
    <td class=common ><!--<input class=test type=text    name=05010018 style="width:50px">-->
          <Input class= 'codename' style="width:70px" name=05010018 verify="极低密度脂蛋白胆固醇|num&value>0&value<10"><Input class= 'codeno' style="width:50px"  name=_05010018_1 value=mmol/l ondblclick="showCodeList('selechols',[this,05010018],[1,0],null,null,null,'1',60);" codeClear(05010018,_05010018_1);"> 
     </td>
  </tr>
<tr class=common align='left' bgcolor=ECF5FF>
     <td class=common >&nbsp;低密度脂蛋白胆固醇(LDL cholesterol)<font color=red> *</font>
     </td>
     <td class=common ><!--<input class=test type=text verify="低密度脂蛋白胆固醇|NOTNULL"  name=05010011 style="width:50px">-->
        <Input class= 'codename' style="width:70px" name=05010011 verify="低密度脂蛋白胆固醇|NOTNULL&num&value>0&value<15"><Input class= 'codeno' style="width:50px" value=mmol/l  name=_05010011_1  ondblclick="showCodeList('selechol',[this,05010011],[1,0],null,null,null,'1',130);" codeClear(05010011,_05010011_1);"> 
     </td>
    <td class=common >&nbsp;纤维蛋白原Fibrinogen
     </td>
    <td class=common ><input class=test type=text name=05010019 style="width:70px">&nbsp;g/l
     </td>
  </tr>
<tr class=common align='left'>
     <td class=common >&nbsp;白蛋白(Albumin, A)<font color=red> *</font> 
     </td>
    <td class=common ><input class=test type=text  verify="白蛋白|NOTNULL&num&value>5&value<150"  name=05010012 style="width:70px">&nbsp;g/l
     </td>
    <td class=common >&nbsp;脂蛋白α(Lipoprotein α)
     </td>
    <td class=common ><input class=test type=text  name=05010020 style="width:70px">&nbsp;mg/l
     </td>
  </tr>
<tr class=common align='left' bgcolor=ECF5FF>
     <td class=common >&nbsp;球蛋白(Globulin, G)
     </td>
    <td class=common ><input class=test type=text name=05010013 verify="球蛋白|num&value>5&value<150"  style="width:70px">&nbsp;g/l
     </td>
    <td class=common >&nbsp;前列腺特异性抗原(Prostate specific antigen)
     </td>
    <td class=common ><input class=test type=text name=05010021 style="width:70px">&nbsp;ug/ml
     </td>
  </tr>
<tr class=common align='left'>
     <td class=common >&nbsp;总蛋白(Total protein, TP)
     </td>
    <td class=common ><input class=test type=text name=05010014  verify="总蛋白|num&value>10&value<200"  style="width:70px">&nbsp;g/l
     </td>
    <td class=common >
    </td>
     <td class=common > </td>
  </tr>
</table>
</div>
<br>
<table  class=common align=center>
		<tr align=center>
			<td style="width:50px;">
		  </td>
			<td class=button width="10%" align=center>
				<INPUT class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="submitForm();">
			</td>
			<td class=button width="10%" align=center>
				<INPUT class=cssButton name="modifyButton" VALUE="提  交"  TYPE=button onclick="submitClick();">
			</td>	
		</tr>
	</table>
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <Div  id= "divLHCusRegistNo" style= "display: 'none'">
	   <td class=common  width="280">&nbsp;客户登记号码:<input class=test style="width:160px" name=CusRegistNo>
	   <INPUT class=cssButton name="queryButton" VALUE="查 询"  TYPE=button onclick="queryClick();">
	   <INPUT class=cssButton name=tempButton VALUE="打 开"  TYPE=hidden onclick="window.open('LHQuesKineMain.jsp?custid=111111&serviceItemNo=111111111111111111&custtype=0','CRM接口','width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=yes');">
	 </div>
	 <Input class= 'common' type = hidden  name=WhethAchieve >
	 <input type=hidden  name=QuesType readonly>
	 <input type=hidden  name=Source readonly>
	 <input type=hidden  name=MakeDate readonly>
	 <input type=hidden  name=MakeTime readonly>
	 <input type=hidden  name=ServItemNo >
	 <input type=hidden  name=custtype >
	 <input type=hidden  name=ServCaseCode >
	 <input type=hidden  name=ServTaskNo >
	 <input type=hidden  name=ServTaskCode >
	 <input type=hidden  name=TaskExecNo >
	 <input type=hidden  name=ServPlanNo >
	 <input type=hidden  name=ContNo >
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
