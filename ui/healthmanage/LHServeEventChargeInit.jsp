<%
//程序名称：LHServeEventCharge.jsp
//程序功能：
//创建日期：2006-06-26 09:08:27
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<Script language=javascript>
function initForm()
{
  try
  {
  	initInpBox();
    initLHServPlanGrid();
    initLHServEventGrid();
    initLHServMissionGrid();
  }
  catch(re)
  {
    alert("LHServeEventChargeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initInpBox()
{ 
  try
  {                                   
  divLHServCustom.style.display='none';
	divLHServPlanGrid.style.display='none';
	divLHPricePlan.style.display='none';
	divLHFeeGrid.style.display='none';
	divLHServGrid.style.display='none';
	divLHServEventGrid.style.display='none';
	divLHServMissionGrid.style.display='none';
  }
  catch(ex)
  {
    alert("在LHServPlanInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initLHServPlanGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="客户号";         		//列名
    iArray[1][1]="40px";         		//列名
    iArray[1][3]=1;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="客户姓名";         		//列名
    iArray[2][1]="40px";         		//列名
    iArray[2][3]=1;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="保单号";         		//列名
    iArray[3][1]="40px";         		//列名
    iArray[3][3]=1;         		//列名
       
    iArray[4]=new Array(); 
	  iArray[4][0]="标准服务项目代码";   
	  iArray[4][1]="0px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	  
	  iArray[5]=new Array(); 
	  iArray[5][0]="标准服务项目名称";   
	  iArray[5][1]="120px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=0;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="个人服务项目号码";   
	  iArray[6][1]="0px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=3;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="服务项目序号";   
	  iArray[7][1]="50px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array();
    iArray[8][0]="服务项目状态";         		//列名
    iArray[8][1]="40px";         		//列名
    iArray[8][3]=1;         		//列名
	  
    iArray[9]=new Array(); 
    iArray[9][0]="服务提供机构标识";                             
    iArray[9][1]="70px";                     
    iArray[9][2]=20;                                  
    iArray[9][3]=2;      
    iArray[9][4]="hmhospitalmng";
    iArray[9][5]="5";     //引用代码对应第几列，'|'为分割符
    iArray[9][6]="1";     //上面的列中放置引用代码中第几位值
                     
                              
    iArray[10]=new Array();                           
    iArray[10][0]="定价方式代码";                             
    iArray[10][1]="80px";                             
    iArray[10][2]=20;                                  
    iArray[10][3]=2;  
    iArray[10][4]="lhserveritemcode";
    iArray[10][5]="6|7";     //引用代码对应第几列，'|'为分割符
    iArray[10][6]="0|1";     //上面的列中放置引用代码中第几位值
    iArray[10][9]="定价方式名称|len<=10";   
    iArray[10][15]="servpricecode";
    iArray[10][17]="1";  
    iArray[10][19]="1" ;           //是否允许强制刷新数据源,1表示允许，0表示不允许
    
    iArray[11]=new Array(); 
	  iArray[11][0]="定价方式名称";   
	  iArray[11][1]="100px";   
	  iArray[11][2]=20;        
	  iArray[11][3]=2;   
    iArray[11][4]="lhserveritemcode";
    iArray[11][5]="7|6";     //引用代码对应第几列，'|'为分割符
    iArray[11][6]="1|0";     //上面的列中放置引用代码中第几位值
    iArray[1][9]="定价方式名称|len<=10";   
    iArray[11][15]="servpricecode";
    iArray[11][17]="1";  
    iArray[11][19]="1" ;           //是否允许强制刷新数据源,1表示允许，0表示不允许    
	            
                              
    LHServPlanGrid = new MulLineEnter( "fm" , "LHServPlanGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHServPlanGrid.mulLineCount = 0;                             
    LHServPlanGrid.displayTitle = 1;                          
    LHServPlanGrid.hiddenPlus = 1;                          
    LHServPlanGrid.hiddenSubtraction = 1;                          
    LHServPlanGrid.canSel = 1;                          
    LHServPlanGrid.canChk = 0;                          
    LHServPlanGrid.selBoxEventFuncName = "showOne";                       
                                                             
    LHServPlanGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHServPlanGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}

function initLHServEventGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="服务事件编号";         		//列名
    iArray[1][1]="40px";         		//列名
    iArray[1][3]=1;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="服务事件名称";         		//列名
    iArray[2][1]="40px";         		//列名
    iArray[2][3]=1;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="服务事件状态";         		//列名
    iArray[3][1]="40px";         		//列名
    iArray[3][3]=1;         		//列名      
                              
    LHServEventGrid = new MulLineEnter( "fm" , "LHServEventGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHServEventGrid.mulLineCount = 0;                             
    LHServEventGrid.displayTitle = 1;                          
    LHServEventGrid.hiddenPlus = 1;                          
    LHServEventGrid.hiddenSubtraction = 1;                          
    LHServEventGrid.canSel = 1;                          
    LHServEventGrid.canChk = 0;                          
    LHServEventGrid.selBoxEventFuncName = "showOne";                       
                                                             
    LHServEventGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHServPlanGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}

function initLHServMissionGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="服务任务编号";         		//列名
    iArray[1][1]="0px";         		//列名
    iArray[1][3]=1;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="服务任务名称";         		//列名
    iArray[2][1]="50px";         		//列名
    iArray[2][3]=1;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="服务任务状态";         		//列名
    iArray[3][1]="50px";         		//列名
    iArray[3][3]=1;         		//列名
       
    iArray[4]=new Array(); 
	  iArray[4][0]="任务操作方式";   
	  iArray[4][1]="50px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	  
	  iArray[5]=new Array(); 
	  iArray[5][0]="功能模块设置";   
	  iArray[5][1]="60px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=0;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="计划完成时间";   
	  iArray[6][1]="50px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=0;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="任务完成时间";   
	  iArray[7][1]="50px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="服务任务说明";   
	  iArray[8][1]="100px";   
	  iArray[8][2]=20;        
	  iArray[8][3]=0;  
	    
    LHServMissionGrid = new MulLineEnter( "fm" , "LHServMissionGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHServMissionGrid.mulLineCount = 0;                             
    LHServMissionGrid.displayTitle = 1;                          
    LHServMissionGrid.hiddenPlus = 1;                          
    LHServMissionGrid.hiddenSubtraction = 1;                          
    LHServMissionGrid.canSel = 1;                          
    LHServMissionGrid.canChk = 0;                          
    LHServMissionGrid.selBoxEventFuncName = "showOne";                       
                                                             
    LHServMissionGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHServPlanGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}

</script>