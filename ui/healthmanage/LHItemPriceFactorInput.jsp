<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHItemPriceFactorInput.jsp
//程序功能：服务定价要素设置(页面显示)
//创建日期：2006-04-19 11:30:30
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHItemPriceFactorInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHItemPriceFactorInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHItemPriceFactorSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHItemPriceFactor1);">
    		</td>
    		<td class= titleImg>
        		 服务定价要素设置
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHItemPriceFactor1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
  	 <TD  class= title>
      机构属性标识
    </TD>
    <TD  class= input>
	      <Input class= 'codename' style="width:50px" name=ComIDCode verify="机构代码|notnull" readonly><Input class= 'codename' style="width:100px" name=ComIDName readonly>
    </TD>
    <TD  class= title>
      标准服务项目代码
    </TD>
    <TD  class= input>
      <Input class= 'codename' name=ServItemCode style="width=150px" verify="标准服务项目代码|len<=50" readonly>
    </TD>
    <TD  class= title>
       标准服务项目名称
    </TD>
        <TD  class= input>
      <Input class= 'codename' name=ServItemName style="width=150px" verify="标准服务项目名称|len<=50" readonly>
    </TD>
  </TR>
</table>
    </Div>

     
      <table>
		  	<tr>
		      <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHItemPriceFactorGrid);">
		  		</td>
		  		<td class=titleImg>
		  			 信息
		  		</td>
	
		  	</tr>
		  </table>
		  <!-- 信息（列表） -->
		<Div id="divLHHealthServPlanGrid1" style="display:''">
	     <div id="divDutyExolai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textDutyExolai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backDutyExolai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backDutyExolaiButton()">
			 </div>
			 <div id="divFeeExplai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textFeeExplai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backFeeExplai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backFeeExplaiButton()">
			 </div>
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHItemPriceFactorGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
		</div>

		</table>
    </Div>
		<div id="divLHItemPriceFactorGrid1" style="display: 'none'">
			  <table class=common>
			  <TR  class= common>
			    <TD  class= title>
			      执行者
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=linkman >
			    </TD>
			    <TD  class= title>
			      责  任
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=Phone >
			    </TD>
			    <TD >
			    </TD>
			  </TR>
			  </table>
		</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type = hidden name = "Operator">
    <input type = hidden name = "MakeDate">
    <input type = hidden name = "MakeTime">
    <input type = hidden name = "ModifyDate">
    <input type = hidden name = "ModifyTime">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
