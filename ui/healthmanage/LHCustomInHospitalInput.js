//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();  
var selectedTestRow;
var selectedOPSRow;
var selectedOtherCureRow;
var row1=0;

//window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if(fm.all('InHospitNo').value != "")
	{
			alert("新建 '客户就诊信息' 请点左栏对应菜单刷新本页面！");
			return false;
	}
	if( CustomerExit()== false )
	{return false;}
	if(mainDiag())
	{alert("诊断信息-->诊断标志中，只能有一个主要诊断");return false;}
	
  fm.all('InHospitNo').value="";
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCustomInHospital.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if (confirm("您确实想修改该记录吗?"))
  { 
//  	if(fm.all('iscomefromquery').value=="0")
//    {
//     alert("请先保存当前结果！");
//     return false;
//    }
	
	if(mainDiag())
	{alert("诊断信息-->诊断标志中，只能有一个主要诊断");return;}

  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHCustomInHospitalQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
		if(fm.all('InHospitNo').value == "")
	{alert("无法删除，请重新查询一条信息");return false;}
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
	
		try
		{
			var strSql=" select a.CustomerNo,b.name, a.InHospitNo, a.HospitCode,'',a.InHospitDate,"
      					+" (select distinct c.DoctNo from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"
      					+" '',a.InHospitMode,(select distinct c.MainCureMode from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),'',"
//      					+" (select distinct c.DiagnoseNo from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"     					
      					+" (select distinct c.CureEffect from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"
      					+" a.InHospitalDays,a.MakeDate,a.MakeTime ,a.MainItem "
      					+" from LHCustomInHospital a, ldperson b "     					
      					+" where a.CustomerNo=b.CustomerNo and a.CustomerNo='"+arrQueryResult[0][0]+"'"
      					+" and a.InHospitNo='"+ arrQueryResult[0][2]+"'"      
      					;
      					//alert(strSql);
      arrResult = easyExecSql(strSql); //alert(arrResult);
		
	  }
	  catch(ex){  alert("LHCustomInHospitalInput.js->afterquery0出错");}
      
      
      fm.all('CustomerNo').value= arrResult[0][0];fm.all('CustomerNo').readOnly = true;
      fm.all('CustomerName').value= arrResult[0][1];
      fm.all('InHospitNo').value= arrResult[0][2];
      fm.all('HospitCode').value= arrResult[0][3];
      if(arrResult[0][3] != "")
      {  fm.all('HospitName').value= easyExecSql("select HospitName from LDHospital where hospitcode = '"+arrResult[0][3]+"'");}
      fm.all('InHospitDate').value= arrResult[0][5];
      fm.all('DoctNo').value= arrResult[0][6];
      if(arrResult[0][6] != "")
      {  fm.all('DoctName').value= easyExecSql("select DoctName from LDDoctor where DoctNo = '"+arrResult[0][6]+"'");}
      fm.all('InHospitModeCode').value= arrResult[0][8];if(arrResult[0][8]=="1"){fm.all('InHospitMode').value="门诊";strsqlinit=" code like #1%# and ";initFeeInfoGrid();};if(arrResult[0][8]=="2"){fm.all('InHospitMode').value="住院";strsqlinit=" code like #2%# and ";initFeeInfoGrid();}if(arrResult[0][8]=="3"){fm.all('InHospitMode').value="急诊";}
      fm.all('MainCureModeCode').value= arrResult[0][9];
      var tempsql="select codename from ldcode where codetype='maincuremode' and code='"+arrResult[0][9]+"'";
      var curemodeResult=easyExecSql(tempsql);
      if(curemodeResult!=null)
      {
      	fm.all('MainCureMode').value= curemodeResult[0][0];
      }
      //fm.all('MainCureModeCode').value= arrResult[0][10];
      //fm.all('DiagnoseNo').value= arrResult[0][10];
      fm.all('CureEffectCode').value= arrResult[0][11];fm.all('CureEffect').value= easyExecSql("select codename from ldcode where codetype='diagnosecureeffect' and code ='"+arrResult[0][11]+"'");
      fm.all('InHospitalDays').value= arrResult[0][12];if(arrResult[0][8]=="2"){fm.InHospitalDays.style.display='';fm.CureEffect.style.display='';}else{fm.CureEffect.style.display='none';fm.InHospitalDays.style.display='none';}
      fm.all('MakeDate').value= arrResult[0][13];
      fm.all('MakeTime').value= arrResult[0][14];
      fm.all('MainItem').value= arrResult[0][15];
       
      strSql="select a.ICDCode,b.ICDName, a.IsMainDiagno,c.CodeName,a.DiagnoseNo "         
     +" from LHDiagno a, LDDisease b,LDCode c where 1=1  "            
     +" and a.ICDCode=b.ICDCode and a.IsMainDiagno=c.Code and c.codetype='ismaindiagno'"
     +getWherePart("a.CustomerNo","CustomerNo")                       
     +getWherePart("a.InHospitNo","InHospitNo");                      
		  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
		  turnPage.queryModal(strSql, DiagnoInfoGrid);
		  
		   strSql=" select b.MedicaItemName,a.MedicaItemCode,"
						 +" a.TestDate,a.TestResult,b.StandardMeasureUnit,"
				     +" a.TestNo ,a.IsNormal  from LHCustomTest a, LHCountrMedicaItem b where 1=1  "
				     +" and a.MedicaItemCode=b.MedicaItemCode "
				     + getWherePart("a.CustomerNo","CustomerNo") 
				     + getWherePart("a.InHospitNo","InHospitNo");
				     //  a.DoctNo,(select c.DoctName from LDDoctor c where a.DoctNo=c.DoctNo ),a.TestFeeAmount,
//			alert(strSql);
					  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
	     //alert(easyExecSql(strSql));
	  turnPage.queryModal(strSql, TestInfoGrid);
	  
	   strSql="select b.ICDOPSName,a.ICDOPSCode, a.DoctNo,(select c.DoctName from LDDoctor c where a.DoctNo=c.DoctNo ),a.ExecutDate,a.CureEffect,"
     +" a.OPSNo from LHCustomOPS a, LDICDOPS b where 1=1  "
     +" and a.ICDOPSCode=b.ICDOPSCode   "
     +getWherePart("a.CustomerNo","CustomerNo") 
     +getWherePart("a.InHospitNo","InHospitNo");
	  //var   arrLHDiagnoResult = easyExecSql(strSQL); a.OPSFeeAmount,
	  
	  turnPage.queryModal(strSql, OPSInfoGrid);
	  
	    
	   strSql="select b.MedicaItemName,a.MedicaItemCode, a.DoctNo,(select c.DoctName from LDDoctor c where a.DoctNo=c.DoctNo ),a.ExecutDate,a.CureEffect,"
     +" a.OtherFeeAmount,a.OtherCureNo from LHCustomOtherCure a, LHCountrMedicaItem b where 1=1  "
     +" and a.MedicaItemCode=b.MedicaItemCode "
     +getWherePart("a.CustomerNo","CustomerNo") 
     +getWherePart("a.InHospitNo","InHospitNo");
     //alert(strSql);
     //alert(easyExecSql(strSql));
	  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
	  
	  turnPage.queryModal(strSql,OtherCureInfoGrid);
	  
	    strSql="select b.codename,a.FeeCode, a.FeeAmount,'',a.FeeNo "
     +"from LHFeeInfo a, ldcode b where 1=1  "
     +" and a.FeeCode=b.code and b.codetype='llfeeitemtype' "
     +getWherePart("a.CustomerNo","CustomerNo") 
     +getWherePart("a.InHospitNo","InHospitNo");
   
	  turnPage.queryModal(strSql, FeeInfoGrid);
	  
//	  arrResult=null;
//	  strSql="select FeeAmount from LHFeeInfo where FeeCode='7'"
//	  +getWherePart("CustomerNo","CustomerNo") 
//    +getWherePart("InHospitNo","InHospitNo");
//     
//     arrResult = easyExecSql(strSql);
//     if(arrResult!=null)
//     {
//     		fm.all('TotalFee').value= arrResult[0][0];
//	   }
//计算分费用比例	   
	    var totalfee=0;
			var tempProportion;
			var rowcount=FeeInfoGrid.mulLineCount;
		
			for (i=0;i<=rowcount-1; i++)
			{
		   tempProportion=FeeInfoGrid.getRowColData(i,3)/1;
		   totalfee+= tempProportion     ;
			}

			for(i=0;i<=rowcount-1; i++)
			{   			
				tempProportion=FeeInfoGrid.getRowColData(i,3)/totalfee*100;
				tempProportion=Math.round(tempProportion*100)/100;  
				tempProportion=tempProportion+"%";
				FeeInfoGrid.setRowColData(i,4,tempProportion);
			}
			fm.all('TotalFee').value=totalfee;
   
//	  fm.all('iscomefromquery').value="1";
	}
}               
        
function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}

function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.html");	  
	   //var newWindow = window.open("../case/ClaimProposalQuery.jsp");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}

function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}


function afterQuery00(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.DiseasCode.value = arrResult[0][0];
  	fm.DiseasName.value = arrResult[0][1];
  }
}


//function CustomHealth()
//{
//	parent.fraInterface.window.location = "LHCustomHealthStatusInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
//}
//function CustomDiseas()
//{
//	parent.fraInterface.window.location = "LHCustomDiseasInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
//}
//function CustomTest()
//{
//	parent.fraInterface.window.location = "LHCustomTestInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
//}
//function CustomOPS()
//{
//	parent.fraInterface.window.location = "LHCustomOPSInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
//}
//function CustomFamily()
//{
//	parent.fraInterface.window.location = "LHCustomFamilyDiseasInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
//}
//function CustomGym()
//{
//	parent.fraInterface.window.location = "LHCustomGymInput.jsp?tCustomerNo="+fm.CustomerNo.value+"&tCustomerName="+fm.CustomerName.value;
//	
//}  

// 以下是双击控件出现大的TEXTAREA
function inputTestResult(a)
{ 
	
	divTestResult.style.display='';
	var tempTestResult = fm.all(a).all('TestInfoGrid4').value;
	selectedTestRow = fm.all(a).all('TestInfoGridNo').value-1;

	fm.all('textTestResult').value=tempTestResult;
	
}


function backTestResult()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true))
	{
		//alert(selectedTestRow);
		TestInfoGrid.setRowColData(selectedTestRow,4,fm.textTestResult.value);
		fm.textTestResult.value="";
		divTestResult.style.display='none';
		
	}
}

function backTestResult2()
{
		TestInfoGrid.setRowColData(selectedTestRow,4,fm.textTestResult.value);
		fm.textTestResult.value="";
		divTestResult.style.display='none';
}


function inputOPSResult(a)
{ 
	//alter(fm.all( a ).all('TestInfoGrid1').value); 
	divOPSResult.style.display='';
	var tempOPSResult = fm.all(a).all('OPSInfoGrid6').value;
	selectedOPSRow = fm.all(a).all('OPSInfoGridNo').value-1;
	fm.all('textOPSResult').value=tempOPSResult;	
}


function backOPSResult()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
			OPSInfoGrid.setRowColData(selectedOPSRow,6,fm.textOPSResult.value);
		fm.textOPSResult.value="";
		divOPSResult.style.display='none';
		}
}


function backOPSResult2()
{
		OPSInfoGrid.setRowColData(selectedOPSRow,6,fm.textOPSResult.value);
		fm.textOPSResult.value="";
		divOPSResult.style.display='none';
}

function inputCureResult(a)
{ 
	//alter(fm.all( a ).all('TestInfoGrid1').value); 
	divCureResult.style.display='';
	var tempCureResult = fm.all(a).all('OtherCureInfoGrid6').value;
	selectedOtherCureRow = fm.all(a).all('OtherCureInfoGridNo').value-1;
	fm.all('textCureResult').value=tempCureResult;
	
}
function backCureResult()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		OtherCureInfoGrid.setRowColData(selectedOtherCureRow,6,fm.textCureResult.value);
		fm.textCureResult.value="";
		divCureResult.style.display='none';		
	}
}

function backCureResult2()
{
		OtherCureInfoGrid.setRowColData(selectedOtherCureRow,6,fm.textCureResult.value);
		fm.textCureResult.value="";
		divCureResult.style.display='none';	
}

//计算费用比率
function countProportion(a) 
{
	var totalfee=0;
	var tempProportion;
	var rowcount=FeeInfoGrid.mulLineCount;

	for (i=0;i<=rowcount-1; i++)
	{
   tempProportion=FeeInfoGrid.getRowColData(i,3)/1;
   totalfee+= tempProportion     ;
	}
		//alert(totalfee) ;
	for(i=0;i<=rowcount-1; i++)
	
	{   
	
		tempProportion=FeeInfoGrid.getRowColData(i,3)/totalfee*100;
		tempProportion=Math.round(tempProportion*100)/100;  
		tempProportion=tempProportion+"%";
		FeeInfoGrid.setRowColData(i,4,tempProportion);
	}
	fm.all('TotalFee').value=totalfee;
}


//function getCurrentTime(pram1)
//{
//
//  row = parseInt(pram1.substring(16,17)); 
//	var d = new Date();
//	var h = d.getYear();
//	var m = d.getMonth();
//	var day = d.getDate();  
//	var Date1;       
//	if(h<10){h = "0"+d.getYear();}  
//	if(m<10){m = "0"+(++m);}
//	if(day<10){day = "0"+d.getDate();}
//	Date1 = h+"-"+m+"-"+day;
//	
//	TestInfoGrid.setRowColData(row,5,Date1);
//}
//
//function getCurrentTime2(pram1)
//{
//	var row = pram1.substring(15,16);
//	var d = new Date();
//	var h = d.getYear();
//	var m = d.getMonth(); 
//	var day = d.getDate();  
//	var Date1; 
//	if(h<10){h = "0"+d.getYear();}  
//	if(m<10){m = "0"+(++m);}
//	if(day<10){day = "0"+d.getDate();}
//	Date1 = h+"-"+m+"-"+day;
//	
//	OPSInfoGrid.setRowColData(row,5,Date1);
//}
//
//function getCurrentTime3(pram1)
//{
//	var row = pram1.substring(21,22);
//	var d = new Date();
//	var h = d.getYear();
//	var m = d.getMonth(); 
//	var day = d.getDate();  
//	var Date1;       
//	if(h<10){h = "0"+d.getYear();}  
//	if(m<10){m = "0"+(++m);}
//	if(day<10){day = "0"+d.getDate();}
//	Date1 = h+"-"+m+"-"+day;
//	OtherCureInfoGrid.setRowColData(row,5,Date1);
//}


//出现两个主要诊断时报错
function mainDiag()
{
		var count = 0; 					//主要诊断出现次数
		var MullineRow = DiagnoInfoGrid.mulLineCount;
		for(j = 0; j < MullineRow; j++ )
		{
				if( DiagnoInfoGrid.getRowColData(j,4) == "主要诊断")
				count++;
		}
		if(count > 1)
			{return true;}				//出现了两个主要诊断，错误
	  else
	  	{return false;}				//只有一个主要诊断，正确
}

//当页面控件值用按键清除时，其对应的隐藏代码控件值也被清除
function codeClear( a, b)
{
	if(a.value == "")
	b.value = "";
}


//修改保存时校验客户是否存在
function CustomerExit()
{
	var cCustomerNo = fm.CustomerNo.value;  //客户代码
  var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
  var arrResult = easyExecSql(strSql);
  	  	//alert(arrResult);
  if (arrResult == null) 
  {
   	  alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
   	  return false;
  }
}