//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass();
//window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}
//提交，保存按钮对应操作
function submitForm() {

  var i = 0;
  if( verifyInput2() == false )
    return false;

     // if( checkValue2() == false )
  //  return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="./LHAddServEventSave.jsp";
  fm.submit(); //提交
  
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  if (FlagStr == "Fail" ) 
  {
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } 
  else 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

function updateClick()
{
	//下面增加相应的代码
	if (confirm("您确实想修改该记录吗?"))
	{
	var i = 0;
	if( verifyInput2() == false ) return false;
	var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	//showSubmitFrame(mDebug);
	fm.fmtransact.value = "UPDATE||MAIN";
	fm.action="./LHAddServEventSave.jsp";
	fm.submit(); //提交
	}
	else
	{
	  //mOperate="";
	  alert("您取消了修改操作！");
	}
}

//function deleteClick()
//{
//	alert("开发中");
//	return true;
//}

function deleteClick()
{
	if(fm.all('MakeDate').value == "")
	{
		alert("请先查询返回，再删除！");
		return false;
	}
	
  //下面增加相应的删除代码
	if (confirm("您确定要彻底删除事件及其所有关联信息吗?\n不可恢复，请慎重操作"))
	{
  		var i = 0;
  		var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  		
  		fm.fmtransact.value = "DELETE||MAIN";
		fm.action="./LHAddServEventSave.jsp";  		
  		fm.submit(); //提交
  		initForm();
	}
	else
	{
	  alert("您取消了删除操作！");
	}
}
function queryClick() 
{
  showInfo=window.open("./LHServEventQuery.html");
}
function afterQuery1(arrResult)
{
	var strSql="select ServCaseType,ServCaseCode,ServCaseName,MakeDate,MakeTime,ServCaseState,ServCasedes,ManageCom from LHServCaseDef where ServCaseCode ='"+ arrResult[0][1]+"'";
	var arrLHServEventStatus=easyExecSql(strSql);
    if(arrLHServEventStatus!=null) 
    {
  	  	fm.all('ServeEventStyle').value=arrLHServEventStatus[0][0];
  	  	if (arrLHServEventStatus[0][0]==1)
  	  		fm.all('ServeEventStyle_ch').value="个人服务事件"
  	  	else
  	  		fm.all('ServeEventStyle_ch').value="集体服务事件"	
  	    fm.all('ServeEventNum').value=arrLHServEventStatus[0][1];
  	    fm.all('ServeEventName').value=arrLHServEventStatus[0][2];
  	    fm.all('MakeDate').value=arrLHServEventStatus[0][3];
  	    fm.all('MakeTime').value=arrLHServEventStatus[0][4];
  	    fm.all('ServCaseState').value=arrLHServEventStatus[0][5];
  	    if(arrLHServEventStatus[0][5]==0){fm.all('ServCaseStateName').value = "未进行启动设置";}
  	    else if(arrLHServEventStatus[0][5]==1){fm.all('ServCaseStateName').value = "服务事件已启动";}
  	    else if(arrLHServEventStatus[0][5]==2){fm.all('ServCaseStateName').value = "服务事件已完成";}
  	    else if(arrLHServEventStatus[0][5]==3){fm.all('ServCaseStateName').value = "服务事件失败";}
  	    fm.all('ServItemCode').value=arrLHServEventStatus[0][6];
  	    fm.all('ComCode').value=arrLHServEventStatus[0][7];	//新增机构
  	    
  	    var sqlGrid = " select r.customerno,r.customername,r.contno,r.ServItemCode,"
  	    			 +" (select distinct e.servitemname from LHHealthServItem e where e.ServItemCode = r.ServItemCode),"
  	    			 +" (select distinct i.ServItemType from LHServItem i where i.ServItemNo = r.ServItemNo),"
  	    			 +" (select distinct d.name from ldcom d where d.comcode = r.comid),r.ServItemNo "
  	    			 +" from LHServCaseRela r where r.ServCaseCode = '"+arrLHServEventStatus[0][1]+"' "
  	    			 ;
  	    turnPage.queryModal(sqlGrid, LHCaseRelaGrid);
	}
}
function getNo()
{
	if(fm.all('ServeEventStyle').value!=""&&fm.all('ServeEventStyle').value!="null"&&fm.all('ServeEventStyle').value!=null)
	{
		var d = new Date();
	    var h = d.getYear();
	    var m = d.getMonth(); 
	    var day = d.getDate();  
	    var Date2;       
	    if(h<10){h = "0"+d.getYear();}  
	    if(m<9){ m++; m = "0"+m;}
	    else{m++;}
	    if(day<10){day = "0"+d.getDate();}
	    Date2 = h+"-"+m+"-"+day;
	    Date1 = h+""+m+""+day;
	    fm.all('Date2').value=Date2;
        fm.all('HiddenCaseCode').value=Date2+fm.all('ServeEventStyle').value;
        var sql2="select  max(substr(ServCaseCode, 10,5))  from  LHServCaseDef where makedate='"+fm.all('Date2').value+"' ";
        var result = easyExecSql(sql2);
        var len=result.toString().length;
        while (result.toString().substring(0,1)=="0" )
        {
			result = result.toString().replace("0","");
        }
        var result2=parseInt(result)+1;
        var result3=result2.toString();
        if(result==""||result==null|result=="null")
        {
	        fm.ServeEventNum.value=Date1+fm.all('ServeEventStyle').value+"0"+"000"+"1";
        }
        else
        {
             if(result3.length==4)
             {
               result4="0"+result3.toString();
             }
             else if(result3.length==3)
             {
             	result4="00"+result3.toString();
             }
             else if(result3.length==2)
             {
             	result4="000"+result3.toString();
             }
             else if(result3.length==1)
             {
             	result4="0000"+result3.toString();
             }
             else 
             {
             	result4=result3.toString();
             }
             fm.EventCode.value=fm.all('HiddenCaseCode').value+fm.all('ServeEventNum').value+result4;
             var Enentcode=fm.EventCode.value;
             if(Enentcode.toString().substring(5,4)=="-")
             {
               Enentcode = Enentcode.toString().replace("-","");
             }
             if(Enentcode.toString().substring(7,6)=="-")
             {
               Enentcode = Enentcode.toString().replace("-","");
               //alert("EEE"+Enentcode);
             }
             fm.all('ServeEventNum').value=Enentcode;
        }
    }
    else
    {
    	alert("请您先选择服务事类型！");
    }
}

function delCaseRelaInfo()
{
	if (!confirm("您确定要彻底删除所有选定事件的关联信息吗?\n不可恢复，请慎重操作"))
	{
		alert("您取消了删除操作！");
		return true;
	}
	
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LHCaseRelaGrid.mulLineCount; i++)
	{
		if(LHCaseRelaGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	
	if(tChked.length == 0)
	{
		alert("请选择要删除的信息");
		return false;	
	}
	fm.fmtransact.value="DELRELA||MAIN";
	fm.action="./LHDelServEventSave.jsp";
	fm.submit();
}