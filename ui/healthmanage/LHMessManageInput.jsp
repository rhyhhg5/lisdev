<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHMessManageInput.jsp
//程序功能：
//创建日期：2006-09-07 15:13:39
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHMessManageInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHMessManageInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LHMessManageSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHMessManage);">
    		</td>
    		 <td class= titleImg>
        		 健康通讯基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHMessManage" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      健康通讯代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HmMessCode  style="width:152px"    verify="健康通讯代码|NOTNULL&len<=20"><font color=red> *</font>
    </TD>
    <TD  class= title>
      健康通讯名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HmMessName   verify="健康通讯名称|len<=50">
    </TD>
    <TD class= title >
       代码分类等级
    </TD>   		 
		<TD  class= input>
				 <Input class= 'codename' style="width:40px" name=CodeTypeStep><Input class= 'codeno' style="width:120px"  name=CodeTypeStepName ondblclick="showCodeList('lhtypestep',[this,CodeTypeStep],[1,0],null,null,null,'1',130);" codeClear(CodeTypeStep,CodeTypeStepName);">
		</TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      健康通讯内容
    </TD>

    <TD  class= input colspan='6'>
      <textarea class= 'common' rows="3" cols="100" style="width:682px"  name=HmMessCont verify="健康通讯内容|len<=500" ></textarea>
    </TD>

  </TR>
  <tr class=common>
  	<TR  class= common>
  	<TD  class= title>
      补充说明
    </TD>
  	<TD  class= input colspan='6'>
      <textarea class= 'common' rows="3" cols="100" style="width:682px"  name=SupExplan verify="补充说明|len<=500" ></textarea>
    </TD>
  </tr>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
