<%
//程序名称：LHAddServEvent.jsp
//程序功能：
//创建日期：2006-07-04 15:07:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script language="JavaScript">
function initForm()
{
  try
  {
    fm.all('ServeEventStyle').value='';
    fm.all('ServeEventStyle_ch').value='';
    fm.all('ServeEventNum').value='';
    fm.all('ServeEventName').value='';
    fm.all('ServCaseState').value='';
    fm.all('ServCaseStateName').value='';
    fm.all('ServItemCode').value='';
    fm.all('ServItemName').value='';
    fm.all('ComCode').value='';//机构属性
    fm.all('ComName').value='';
    
    initLHCaseRelaGrid();
  }
  catch(re)
  {
    alert("LDDrugInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHCaseRelaGrid;
function initLHCaseRelaGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="40px";         		//列名
	    iArray[0][3]=1;         		//列名
	    iArray[0][4]="station";         		//列名
	    
	    iArray[1]=new Array(); 
	 	iArray[1][0]="客户号";   
	 	iArray[1][1]="55px";   
	 	iArray[1][2]=20;        
	 	iArray[1][3]=0;
	 	
	 	iArray[2]=new Array(); 
	 	iArray[2][0]="客户姓名";   
	 	iArray[2][1]="55px";   
	 	iArray[2][2]=20;        
	 	iArray[2][3]=0;
	 	
	 	iArray[3]=new Array(); 
	 	iArray[3][0]="保单号";   
	 	iArray[3][1]="80px";   
	 	iArray[3][2]=20;        
	 	iArray[3][3]=1;
     	
	 	
	 	iArray[4]=new Array(); 
	 	iArray[4][0]="服务项目代码";   
	 	iArray[4][1]="0px";   
	 	iArray[4][2]=20;        
	 	iArray[4][3]=3;
     	
	 	iArray[5]=new Array(); 
	 	iArray[5][0]="服务项目名称";   
	 	iArray[5][1]="110px";   
	 	iArray[5][2]=20;        
	 	iArray[5][3]=0;
	 	
	 	iArray[6]=new Array(); 
	 	iArray[6][0]="序号";   
	 	iArray[6][1]="40px";   
	 	iArray[6][2]=20;        
	 	iArray[6][3]=0;


    	iArray[7]=new Array(); 
    	iArray[7][0]="服务机构";                             
    	iArray[7][1]="50px";                     
    	iArray[7][2]=20;                                  
    	iArray[7][3]=0;  
    	
    	iArray[8]=new Array(); 
    	iArray[8][0]="ServItemNo";                             
    	iArray[8][1]="0px";                     
    	iArray[8][2]=20;                                  
    	iArray[8][3]=3;     
    	        
    	LHCaseRelaGrid = new MulLineEnter( "fm" , "LHCaseRelaGrid" );           
    	//这些属性必须在loadMulLine前                          
    	                      
    	LHCaseRelaGrid.mulLineCount = 0;                             
    	LHCaseRelaGrid.displayTitle = 1;                          
    	LHCaseRelaGrid.hiddenPlus = 1;                          
    	LHCaseRelaGrid.hiddenSubtraction = 1;                          
    	LHCaseRelaGrid.canSel = 0;                          
    	LHCaseRelaGrid.canChk = 1;                          
    	LHCaseRelaGrid.selBoxEventFuncName = "showTwo";                       
    	                                                         
    	LHCaseRelaGrid.loadMulLine(iArray);                                     
    	//这些操作必须在loadMulLine后面                                         
    	//LHCaseRelaGrid.setRowColData(1,1,"asdf");                                   
	}
	catch(ex) 
	{alert(ex);}
}

</script>
