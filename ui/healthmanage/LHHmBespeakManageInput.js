var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	  //alert(sqlServTaskNo);
    var i = 0;
    fm.fmtransact.value="INSERT||MAIN";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.action="./LHHmBespeakManageTPageSave.jsp";
    fm.submit(); //提交
    //initLHHmServBeManageGrid();
    //displayConetent();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	fm.all('save').disabled = true;
	initLHHmServBeManageGrid();
	displayConetent();
    //执行下一步操作
  }
}
function displayInfo()
{
	 	var caseNo=null;
	  caseNo=new Array();
	  xxxx = easyExecSql("select ServCaseCode from LHHmServBeManage");
	  caseNo=xxxx.split(",");
	 
	  	  strsql=" select CustomerNo ,"
		        +" (select a.Name from LDPerson a where a.CustomerNo=d.CustomerNo),"
		        +" BespeakComID,"
		        +" (select  HospitName from LDHospital where LDHospital.HospitCode=d.BespeakComID),"
		        +" ServBespeakDate,TestGrpCode, "
		        +" BalanceManner,ServDetail,"
		        +" d.ServTaskNo,d.ServCaseCode ,"
		        +" d.TaskExecNo ,d.ServItemNo,"
		        +" d.ServTaskCode"
		        +"  from   LHHmServBeManage d "
		        +" where d.ServCaseCode='"+ caseNo[i] +"'"
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCommTrans.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}                                          
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */              
function updateClick()
{
    var rowNum=LHHmServBeManageGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHHmServBeManageGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx] = row;
	        }
	  }
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要修改的信息记录!");
			  return false;
		}	
		if(aa.length>="1")
		{
    	if (confirm("您确实想修改该记录吗?"))
    	{
          var i = 0;
          var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
     
          fm.fmtransact.value = "UPDATE||MAIN";
          fm.action="./LHHmBespeakManageSave.jsp";
          fm.submit(); //提交
          initLHHmServBeManageGrid();
          displayConetent();
         
		  }
      else
      {
        alert("您取消了修改操作！");
      } 
   }
}


function afterSubmitParentState()
{
	fm.all('ServTaskState').value = "2";	
	fm.all('ServTaskStateName').value = "正在执行";
	saveClick();
}

function checkTestMode()
{
	if(fm.all('TestModeCode').value == "") 
	{alert('请先选择体检方式');return false;}
	if(fm.all('HospitCode').value == "") 
	{
		if(fm.all('TestModeCode').value=="32")
		{
		   alert("请选择机构名称!");
		   return false; 
		}
	}
}
function dBespeakServiceTime()
{ 
	var d = new Date();              
	var h = d.getHours();            
	var m = d.getMinutes();          
	if(h<10){h = "0"+d.getHours();}  
	if(m<10){m = "0"+d.getMinutes();}
	fm.ServiceTime.value = h+":"+m;	
}
function showOne()
{
	   var TaskExecNo=LHHmServBeManageGrid.getRowColData(LHHmServBeManageGrid.getSelNo()-1,12);  
	   fm.all('TaskExecNo').value=TaskExecNo;
	   var ServTaskNo=LHHmServBeManageGrid.getRowColData(LHHmServBeManageGrid.getSelNo()-1,10);  
	   fm.all('ServTaskNo').value=ServTaskNo;
	   var ServCaseCode=LHHmServBeManageGrid.getRowColData(LHHmServBeManageGrid.getSelNo()-1,11); 
	   fm.all('ServCaseCode').value=ServCaseCode; 
	   var ServTaskCode=LHHmServBeManageGrid.getRowColData(LHHmServBeManageGrid.getSelNo()-1,14); 
	   fm.all('ServTaskCode').value=ServTaskCode; 
	   var CustomerNo=LHHmServBeManageGrid.getRowColData(LHHmServBeManageGrid.getSelNo()-1,1);  
	   fm.all('CustomerNo').value=CustomerNo;
	   var ServItemNo=LHHmServBeManageGrid.getRowColData(LHHmServBeManageGrid.getSelNo()-1,13);  
	   fm.all('ServItemNo').value=ServItemNo;
	   //alert(TaskExecNo);
	    strsql=" select BespeakComID,"
		        +" ServBespeakDate,"
		        +" TestGrpCode,"
		        +" BalanceManner, "
		        +" ServDetail,ServBespeakTime"
		        +"  from   LHHmServBeManage d "
		        +" where d.TaskExecNo='"+ TaskExecNo +"'"
		        ;
		   //alert(strsql);
		   //alert( easyExecSql(strsql));
		  var result=new Array;
		  result=easyExecSql(strsql);
		  //alert(result[0][12]);
		  if(result!=""&&result!=null&&result!="null")
		  {
		     fm.all('HospitCode').value=result[0][0];
		     fm.all('HospitName').value=easyExecSql("select  HospitName from LDHospital where LDHospital.HospitCode='"+ fm.all('HospitCode').value +"'");
		     fm.all('ServiceDate').value=result[0][1];
		     fm.all('MedicaItemGroup').value=result[0][2];
		     fm.all('MedicaItemGroupName').value=easyExecSql("select distinct Testgrpname from ldtestgrpmgt where ( testgrptype='2' or testgrptype='3') and TestGrpCode='"+ fm.all('MedicaItemGroup').value +"'");
		     fm.all('BalanceManner').value=result[0][3];
		     fm.all('BalanceMannerName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhspbalancmode' and d.code='"+ fm.all('BalanceManner').value +"'");
		     fm.all('ServItemNote').value=result[0][4];  
		     fm.all('ServiceTime').value=result[0][5];  
		  }
		  var HospitCode=LHHmServBeManageGrid.getRowColData(LHHmServBeManageGrid.getSelNo()-1,3); 
		  if(HospitCode==""||HospitCode=="null"||HospitCode==null)
		  {
		  	fm.all('HospitName').value="";
		  } 
		  var MedicaItemGroup=LHHmServBeManageGrid.getRowColData(LHHmServBeManageGrid.getSelNo()-1,6); 
		  if(MedicaItemGroup==""||MedicaItemGroup=="null"||MedicaItemGroup==null)
		  {
		  	fm.all('MedicaItemGroupName').value="";
		  } 
		  var BalanceManner=LHHmServBeManageGrid.getRowColData(LHHmServBeManageGrid.getSelNo()-1,7); 
		  //alert(BalanceManner);
		  if(BalanceManner==""||BalanceManner=="null"||BalanceManner==null)
		  {
		  	fm.all('BalanceMannerName').value="";
		  } 
		  if(result==",,,,")
		  {
		  	  fm.all('HospitCode').value="";    
          fm.all('HospitName').value="";
          fm.all('ServiceDate').value="";   
          fm.all('MedicaItemGroup').value="";
          fm.all('MedicaItemGroupName').value="";
          fm.all('BalanceManner').value=""; 
          fm.all('BalanceMannerName').value="";
          fm.all('ServItemNote').value="";  
		  } 
}       
function BeTestPrint()
{
	var rowNum=LHHmServBeManageGrid. mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHHmServBeManageGrid.getChkNo(row); 
		if(tChk == true)
		{
			aa[xx++] = row;	
	  }  
	}
	if(aa.length>1)
	{
		alert("您只能对一条客户信息进行体检通知单打印!");
		return false;
	}
	if(aa.length=="0")
	{
		 alert("请选择一条客户信息!");
		 return false;
  }
  if(aa.length=="1")
  {
	    //if( TestHmBeInfo()== false )
	    //{return false;}
	    var i = 0;
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      //showSubmitFrame(mDebug); 
	    fm.fmtransact.value = "PRINT";
	    fm.action="./LHHmBespeakPrintSave.jsp";
	    fm.target = "f1print";
	    fm.submit();
	    showInfo.close();
	 }
	 
}
function TestHmBeInfo()
{
	  if(fm.all('HospitCode').value==""||fm.all('HospitCode').value=="null"||fm.all('HospitCode').value==null)
	  {
	  	alert("预约机构代码不许为空!");
	  	fm.all('HospitCode').focus();
	  	return false;
	  }
	  if(fm.all('ServiceDate').value==""||fm.all('ServiceDate').value=="null"||fm.all('ServiceDate').value==null)
	  {
	  	alert("预约时间不许为空!");
	  	fm.all('ServiceDate').focus();
	  	return false;
	  }
    if(fm.all('MedicaItemGroup').value==""||fm.all('MedicaItemGroup').value=="null"||fm.all('MedicaItemGroup').value==null)
	  {
	  	alert("健康体检套餐不许为空!");
	  	fm.all('MedicaItemGroup').focus();
	  	return false;
	  }
	  if(fm.all('BalanceManner').value==""||fm.all('BalanceManner').value=="null"||fm.all('BalanceManner').value==null)
	  {
	  	alert("结算方式不许为空!");
	  	fm.all('BalanceManner').focus();
	  	return false;
	  } 
}
function showTwo()
{
  var rowNum=LHHmServBeManageGrid. mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHHmServBeManageGrid.getChkNo(row); 
		if(tChk == true)
		{
			aa[xx++] = row;	
	  }  
	}
	if(aa.length=="1")
	{
		 //alert("LHHmServBeManageGrid");
		// var HmMessCode =LHHmServBeManageGrid.getRowColData(aa,9); 
		 var TaskExecNo=LHHmServBeManageGrid.getRowColData(aa,12);  
	   fm.all('TaskExecNo').value=TaskExecNo;
	   var ServTaskNo=LHHmServBeManageGrid.getRowColData(aa,10);  
	   fm.all('ServTaskNo').value=ServTaskNo;
	   var ServCaseCode=LHHmServBeManageGrid.getRowColData(aa,11); 
	   fm.all('ServCaseCode').value=ServCaseCode; 
	   var ServTaskCode=LHHmServBeManageGrid.getRowColData(aa,14); 
	   fm.all('ServTaskCode').value=ServTaskCode; 
	   var CustomerNo=LHHmServBeManageGrid.getRowColData(aa,1);  
	   fm.all('CustomerNo').value=CustomerNo;
	   var ServItemNo=LHHmServBeManageGrid.getRowColData(aa,13);  
	   fm.all('ServItemNo').value=ServItemNo;
	   //alert(TaskExecNo);
	    strsql=" select BespeakComID,"
		        +" ServBespeakDate,"
		        +" TestGrpCode,"
		        +" BalanceManner, "
		        +" ServDetail,ServBespeakTime"
		        +"  from   LHHmServBeManage d "
		        +" where d.TaskExecNo='"+ TaskExecNo +"'"
		        ;
		   //alert(strsql);
		   //alert( easyExecSql(strsql));
		  var result=new Array;
		  result=easyExecSql(strsql);
		  //alert(result[0][12]);
		  if(result!=""&&result!=null&&result!="null")
		  {
		     fm.all('HospitCode').value=result[0][0];
		     fm.all('HospitName').value=easyExecSql("select  HospitName from LDHospital where LDHospital.HospitCode='"+ fm.all('HospitCode').value +"'");
		     fm.all('ServiceDate').value=result[0][1];
		     fm.all('MedicaItemGroup').value=result[0][2];
		     fm.all('MedicaItemGroupName').value=easyExecSql("select distinct Testgrpname from ldtestgrpmgt where ( testgrptype='2' or testgrptype='3') and TestGrpCode='"+ fm.all('MedicaItemGroup').value +"'");
		     fm.all('BalanceManner').value=result[0][3];
		     fm.all('BalanceMannerName').value=easyExecSql("select d.codename from ldcode d where d.codetype='lhspbalancmode' and d.code='"+ fm.all('BalanceManner').value +"'");
		     fm.all('ServItemNote').value=result[0][4];  
		     fm.all('ServiceTime').value=result[0][5];  
		  }
		  var HospitCode=LHHmServBeManageGrid.getRowColData(aa,3); 
		  if(HospitCode==""||HospitCode=="null"||HospitCode==null)
		  {
		  	fm.all('HospitName').value="";
		  } 
		  var MedicaItemGroup=LHHmServBeManageGrid.getRowColData(aa,6); 
		  if(MedicaItemGroup==""||MedicaItemGroup=="null"||MedicaItemGroup==null)
		  {
		  	fm.all('MedicaItemGroupName').value="";
		  } 
		  var BalanceManner=LHHmServBeManageGrid.getRowColData(aa,7); 
		  //alert(BalanceManner);
		  if(BalanceManner==""||BalanceManner=="null"||BalanceManner==null)
		  {
		  	//fm.all('BalanceMannerName').value="";
		  	fm.all('MedicaItemGroupName').value="";//健康体检套餐名称
		  } 
		  if(result==",,,,")
		  {
		  	  fm.all('HospitCode').value="";    
          fm.all('HospitName').value="";
          fm.all('ServiceDate').value="";   
          fm.all('MedicaItemGroup').value="";
          fm.all('MedicaItemGroupName').value="";
          fm.all('BalanceManner').value=""; 
          fm.all('BalanceMannerName').value="";
          fm.all('ServItemNote').value=""; 
          fm.all('ServiceTime').value="";   
		  }
		   sqlExecState=" select d.TaskExecState from LHTaskCustomerRela d"
		         +" where d.TaskExecNo='"+ TaskExecNo +"'";
		   var rExecState=new Array;
		   rExecState=easyExecSql(sqlExecState);
		   //alert(rExecState);
		   if(rExecState!=""&&rExecState!=null&&rExecState!="null")
		   {
		   	  if(rExecState=="1")
		   	  {
		   	  	 fm.all('ExecState').value="1";
		   	  	 fm.all('ExecState_ch').value="未执行";
		   	  }
		   	  if(rExecState=="2")
		   	  {
		   	  	 fm.all('ExecState').value="2";
		   	  	 fm.all('ExecState_ch').value="已执行";
		   	  }
		   }
		     
	}
	if(aa.length>"1")
	{
		   fm.all('HospitCode').value="";    
       fm.all('HospitName').value="";
       fm.all('ServiceDate').value="";   
       fm.all('MedicaItemGroup').value="";
       fm.all('MedicaItemGroupName').value="";
       fm.all('BalanceManner').value=""; 
       fm.all('BalanceMannerName').value="";
       fm.all('ServItemNote').value="";  
       fm.all('ServiceTime').value="";  
	}
}       
     
        
        
        
        
  