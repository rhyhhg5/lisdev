//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var prtNo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var StrSql;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput2() == false ) return false;
	if(checkDate() == false) return false;
	for(var i = 0; i < LHServPlanGrid.mulLineCount; i++)
	{
		if(LHServPlanGrid.getRowColData(i, 9) == "")
		{
			alert( "第"+(i+1)+"行服务事件类型未定义，请先定义");
			return false;	
		}	
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.action="./LHServPlanSave.jsp";
  fm.all('CaseState').value="1";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ServPlanType )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    return false;
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  fm.all('saveButton').disabled = true;
  fms.all('QieSuccess').disabled = false;
  if(ServPlanType=="1H")
  {
  	NQuery();
  }
  else
  {
  	passQuery();
  }
//  var sqlSp = " select ServPlanno from LHServPlan where ContNo = '"+fm.all('ContNo').value+"'";
//  var arr_Sp = easyExecSql(sqlSp);
//   fm.all('ServPlanNo').value = arr_Sp;
//   var strSql2 = " select distinct b.ServItemCode ,(select distinct a.servitemname from LHHealthServItem a where a.servitemcode=b.servitemcode),b.ServItemNo,b.ServItemType,b.ComID,b.ServPriceCode,(select distinct c.servpricename from LHServerItemPrice c where c.servpricecode = b.servpricecode) "
//				    +" from LHServItem b where ServPlanNo = '"+arr_Sp+"'"
//      			+" order by ServItemNo"
//      			;
//      			//alert(strSql2);
//      			turnPage.queryModal(strSql2, LHServPlanGrid); 
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHServPlan.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}  
function submitForm2()
{
	 if(fms.all('ServItemNo2').value==""||fms.all('ServItemNo2').value=="null"||fms.all('ServItemNo2').value==null)
	 {
	 	
	 	alert("请先保存服务项目信息!");
	 }
	 else
	 {
	    var sqlServItemNo="select ServItemNo, ComID from LHItemFactorRela"
	                   +" where  ServItemNo='"+fms.all('ServItemNo2').value+"'"
	                   +" and  ComID='"+fms.all('ComID2').value+"'";
        //alert(sqlServItemNo);
        // alert(easyExecSql(sqlServItemNo));
        var arrResult = new Array(); 
        arrResult=easyExecSql(sqlServItemNo);
        if(arrResult!=""&&arrResult!="null"&&arrResult!=null)
        {
          var  ServItemNo=arrResult[0][0];
          var  ComID=arrResult[0][1];
        }
       
      
        if(fms.all('ServItemNo2').value==ServItemNo&&fms.all('ComID2').value==ComID)
        {
       	 alert("此标识的项目代码要素信息已经存在,不允许重复保存!");
       	 return false;
        }
        if((ServItemNo==""&&ComID=="")||(ServItemNo=="null"&&ComID=="null")||(ServItemNo==null&&ComID==null))
        {
	            //if( verifyInput2() == false ) return false;
	            //if(checkDate() == false) return false;
              var i = 0;
              var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
              var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
              showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
              fms.all('fmtransact2').value = "INSERT||MAIN";
              fms.submit(); //提交
        }
     }
}   
function updateClick2()
{
  //下面增加相应的代码
  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改该记录吗?"))
  {
  	if(checkDate() == false) return false;
  	
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fms.all('fmtransact2').value = "UPDATE||MAIN"; 
  fms.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}       
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if(fm.all('ServPlanNo').value == "")
  {
  	alert("请先进行保存");	
  	return false;
  }
//  var sql = " select ServPlanno from LHServPlan where ContNo = '"+fm.all('ContNo').value+"' and CustomerNo = '"+fm.all('CustomerNo').value+"'";
//  var arr_ex = easyExecSql(sql);
  //alert(arr_ex);
  
  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改该记录吗?"))
  {
  	if(checkDate() == false) return false;
  	
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.all('fmtransact').value = "UPDATE||MAIN"; //fm.fmtransact.value = "UPDATE||MAIN";
  fm.action="./LHServPlanSave.jsp";
  fm.all('CaseState').value="1";
//  fm.all('ServPlanNo').value=arr_ex;
  //alert(fm.all('ServPlanNo').value);
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
} 
function updateClick3()
{
  //下面增加相应的代码
  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改契约事件状态吗?"))
  {
  	if(checkDate() == false) return false;
  	
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.all('fmtransact').value = "UPDATE||MAIN"; //fm.fmtransact.value = "UPDATE||MAIN";
  fm.all('CaseState').value="2";
  fm.action="./LHServPlanUpSave.jsp";
  fm.submit(); //提交
//  showInfo.close();
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}  
        
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHServPlanQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.all('fmtransact').value = "DELETE||MAIN"; //fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
      
//       var ServPlanNo=fm.all('ServPlanNo').value;
        fm.all('ServPlanNo').value= arrResult[0][0]; 
         fm.all('GrpServPlanNo').value= easyExecSql("select distinct GrpServPlanNo from LHServPlan where ServPlanNo = '"+fm.all('ServPlanNo').value+"' ");
        fm.all('ContNo').value= arrResult[0][1];
        fm.all('CustomerNo').value= arrResult[0][2];
        fm.all('Name').value= arrResult[0][3];
        fm.all('ServPlanCode').value= easyExecSql("select distinct ServPlanCode from LHServPlan where ServPlanNo = '"+fm.all('ServPlanNo').value+"' ");
        fm.all('ServPlanName').value= easyExecSql("select distinct ServPlanName from LHServPlan where ServPlanNo = '"+fm.all('ServPlanNo').value+"' "); //arrResult[0][10];
        fm.all('ServPlanLevel').value= easyExecSql("select distinct ServPlanLevel from LHServPlan where ServPlanNo = '"+fm.all('ServPlanNo').value+"' ");
        fm.all('ServPlayType').value= easyExecSql("select distinct ServPlayType from LHServPlan where ServPlanNo = '"+fm.all('ServPlanNo').value+"' ");
        fm.all('ComID').value= easyExecSql("select distinct ComID from LHServPlan where ServPlanNo = '"+fm.all('ServPlanNo').value+"' ");
        fm.all('StartDate').value= arrResult[0][5];
        fm.all('EndDate').value= arrResult[0][6];
        fm.all('ServPrem').value= arrResult[0][7];
//        fm.all('ManageCom').value= arrResult[0][13];
 //       fm.all('Operator').value= arrResult[0][14];
        fm.all('MakeDate').value= easyExecSql("select distinct MakeDate from LHServPlan where ServPlanNo = '"+fm.all('ServPlanNo').value+"' ");
        fm.all('MakeTime').value= easyExecSql("select distinct MakeTime from LHServPlan where ServPlanNo = '"+fm.all('ServPlanNo').value+"' ");
//        fm.all('ModifyDate').value= arrResult[0][17];
//        fm.all('ModifyTime').value= arrResult[0][18];

 var strSql2 = " select distinct  b.ServItemCode ,(select distinct a.servitemname from LHHealthServPlan a where a.servitemcode=b.servitemcode),b.ServItemNo,b.ServItemType,b.ComID,b.ServPriceCode,(select c.servpricename from LHServerItemPrice c where c.servpricecode = b.servpricecode) "
 
 							 						 +" from LHServItem b where ServPlanNo = '"+fm.all('ServPlanNo').value+"'"
      										 +" order by ServItemNo"
      											 ;
      											 //alert(strSql2);
      											 turnPage.queryModal(strSql2, LHServPlanGrid); 
	}
}               
        
function toServTrace(arrQueryResult)
{
	if(fm.all('ComID').value == "")
	{
			alert("请先填写信息并保存!");
			return false;
	}
	if(LHServPlanGrid.getRowColData(0,3) == "")
	{
			alert("请先保存!");
			return false;
	}
	window.open("./LHServExeTrace.jsp?ServPlanNo="+fm.all('ServPlanNo').value,"","width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto");
}

function checkDate()
{
	if(fm.all('StartDate').value != "" && fm.all('EndDate').value != "")
	{
			if(compareDate(fm.all('StartDate').value,fm.all('EndDate').value) == true)
			{
				alert("服务计划起始时间应早于服务计划结束时间 ");
				return false;
			}
	}
}

function getPersonContCopy()
{
			showInfo=window.open("LHServPlanMain.jsp?ServPlanNo="+fm.all('ServPlanNo').value);
}


      											 
      											 //选择类型为医院时的处理
function afterCodeSelect(codeName,Field)
{
	if(codeName=="lhservplanname")
	{
    	var strSql2 = " select servitemcode,servitemname,'',servitemsn,comid,'',''"
 							  						 +" from LHHealthServPlan"
      											 +" where ServPlanCode = '"+fm.all('ServPlanCode').value+"'"
      											 ;      											
      	// alert(easyExecSql(strSql2));
      										
      	 turnPage.queryModal(strSql2, LHServPlanGrid); 
	}
}


function ScanFrameCtrl()
{
	
		
}
function showOne()
{
	var getSelNo = LHServPlanGrid.getSelNo();
	var ServItemCode = LHServPlanGrid.getRowColData(getSelNo-1, 1);
	var ComID = LHServPlanGrid.getRowColData(getSelNo-1, 6);
	var ServItemNo = LHServPlanGrid.getRowColData(getSelNo-1, 3);
	

	fms.all('ServItemCode2').value=ServItemCode;
	fms.all('ComID2').value=ComID;
	//alert(fms.all('ComID2').value);
	//alert(fms.all('ServItemCode2').value);
	//alert(ServItemNo);
	fms.all('ServItemNo2').value=ServItemNo;
	
	
	try
	{ 
		//var mulSql2 = "select distinct ServItemNo "
		//            +" from  LHItemFactorRela c "
		//            +" where c.ServItemNo='"+ ServItemNo+"'";
		//var arrResult2=new Array;
		//			      arrResult2=easyExecSql(mulSql2);
		//			    //alert(arrResult2);
		 var mulSql5 = "select PriceFactorCode,PriceFactorName,'' "
		            +" from LHItemPriceFactor  "
		            +" where "
		            +" ServItemCode='"+ServItemCode+"' "
		            +" and " 
		            +" ComID='"+ComID+"'";
					      //alert(mulSql5);
					      var arrResult5=new Array;
					      arrResult5=easyExecSql(mulSql5);
					      //alert(arrResult5);
					      
		if(arrResult5==""||arrResult5=="null"||arrResult5==null)
		{	 
			   divShowInfo.style.display='none';                          
         divLHItemPriceFactorGrid.style.display='none';             

		   //var mulSql3 = "select PriceFactorCode,PriceFactorName,'' "
		   //         +" from LHItemPriceFactor  "
		   //         +" where "
		   //         +" ServItemCode='"+ServItemCode+"' "
		   //         +" and " 
		   //         +" ComID='"+ComID+"'";
				// 	      //alert(mulSql3);
				// 	      var arrResult3=new Array;
				// 	      arrResult3=easyExecSql(mulSql3);
				// 	      //alert(arrResult3);
				// if(arrResult3==""||arrResult3=="null"||arrResult3==null)
				// {	    
				// 	  
	     //      initLHItemPriceFactorGrid();
	     // }
	     // else
	     // {
	     // 	  turnPage2.queryModal(mulSql3, LHItemPriceFactorGrid);
	     // 		
	     //  }
	   }
	   else 
	   {
	   	  divShowInfo.style.display='';                          
        divLHItemPriceFactorGrid.style.display=''; 
	   	  var mulSql2 = "select distinct ServItemNo "
		            +" from  LHItemFactorRela c "
		            +" where c.ServItemNo='"+ ServItemNo+"'";
		    var arrResult2=new Array;
					      arrResult2=easyExecSql(mulSql2);
					   // alert(arrResult2);
	   	  if(arrResult2==""||arrResult2=="null"||arrResult2==null)
	   	  {        
           var mulSql3 = "select PriceFactorCode,PriceFactorName,'' "
		            +" from LHItemPriceFactor  "
		            +" where "
		            +" ServItemCode='"+ServItemCode+"' "
		            +" and " 
		            +" ComID='"+ComID+"'";
				 	      //alert(mulSql3);
				 	      var arrResult3=new Array;
				 	      arrResult3=easyExecSql(mulSql3);
				 	      //alert(arrResult3);
				   if(arrResult3==""||arrResult3=="null"||arrResult3==null)
				   {	    
				 	  
	           initLHItemPriceFactorGrid();
	         }
	         else
	         {
	      	  turnPage2.queryModal(mulSql3, LHItemPriceFactorGrid);
	      		
	         }
	       }
	       else
	       {
	   	      var mulSql4 = "select distinct a.PriceFactorCode,  a.PriceFactorName ,c.PriceFactorCon  "
		                 +"  from LHItemPriceFactor a ,LHItemFactorRela c   "
		                 +" where a.ServItemCode=c.ServItemCode and a.ComID=c.ComID and a.PriceFactorCode=c.PriceFactorCode "
		                 +"and  c.ServItemNo='"+ ServItemNo+"'"
		                 +" and c.ComID='"+ComID+"'"
		                 +"and c.ServItemCode='"+ServItemCode+"'";
			  	   	      //alert(mulSql4);
			  	   	      var arrResult4=new Array;
			  	   	      arrResult4=easyExecSql(mulSql4);
			  	   	      //alert(arrResult4);
			  	   if(arrResult4==""||arrResult4=="null"||arrResult4==null)
			  	   {	      
	              initLHItemPriceFactorGrid();
	           }
	           else
	           {
	           	turnPage.queryModal(mulSql4, LHItemPriceFactorGrid);
	           }
        }
	   }
	}
	catch(ex)
	{
		alert("LHItemPriceFactorInputInit.jsp-->传输入信息Info中发生异常:初始化界面错误!");
	}
}

function ServSetSucced()
{      
	var mulSql4 = "select CaseState from LHServPlan a where a.ServPlanNo='"+fm.all('ServPlanNo').value+"'";
    var arrResult4=new Array;
	arrResult4=easyExecSql(mulSql4);
	
	var ContNo = "";
	var CustomerNo = "";
	var PrtNo = "";

    if(arrResult4==""||arrResult4=="null"||arrResult4==null)
	{
		alert("请先对事件类型及项目信息进行保存或修改!");
	}
	if(arrResult4=="1")
	{	      
		updateClick3();//使契约状态改为2
		fm.all('modifyButton').disabled = true;
		showInfo.close();
		ContNo=fm.all('ContNo').value;
		CustomerNo=fm.all('CustomerNo').value;
		PrtNo=fm.all('prtNo').value;
	}
	if(arrResult4=="2")
	{
		fm.all('saveButton').disabled = true;
		fm.all('modifyButton').disabled = true;
		ContNo=fm.all('ContNo').value;
	    CustomerNo=fm.all('CustomerNo').value;
	    PrtNo=fm.all('prtNo').value;		
	}
	if(arrResult4=="3")
	{
		fm.all('saveButton').disabled = true;
		fm.all('modifyButton').disabled = true;
		ContNo=fm.all('ContNo').value;
	    CustomerNo=fm.all('CustomerNo').value;
	    PrtNo=fm.all('prtNo').value;
	}
	if(flag=="NCONT")
	{
		window.open("./LHContPlanSettingMain.jsp?ContNo="+ContNo+"&PrtNo="+PrtNo+"&CustomerNo="+CustomerNo+"&flag=NCONT","个人服务计划事件设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');  
	}
	else
	{
		window.open("./LHContPlanSettingMain.jsp?ContNo="+ContNo+"&PrtNo="+PrtNo+"&CustomerNo="+CustomerNo,"个人服务计划事件设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');  
	}
}
function ServExeState()
{
	 window.open("./LHEventTypeSettingMain.jsp?","事件类型自动设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}