<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2006-06-26 09:54:27
//创建人  ：CrtHtml程序创建
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHServeEventCharge.js"></SCRIPT>

  <%@include file="LHServeEventChargeInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
  	 <table  class= common align='center'>
        <TR  class= common>
          <TD  class= title>
             服务事件类型
          </TD>
          <TD  class= input>
             <Input class= readonly readonly name=ServeEventStyle > 
          </TD>
          <TD  class= title>
             服务事件编号
          </TD>
          <TD  class= input>
             <Input class= readonly readonly name=ServeEventNum > 
          </TD>
          <TD  class= title>
             服务事件名称
          </TD>
          <TD  class= input>
             <Input class= readonly readonly name=ServeEventName > 
          </TD>
        </TR>
    </table>
    <table  class= common align='center'>
      <TR class= common>
  		   <TD class=title>  				
  	 		   <Input value="服务契约信息" type=button class=cssbutton style="width:158px;"  onclick="ServeQuery();">&nbsp;&nbsp;
  	 		   <Input value="费用结算信息" type=button class=cssbutton style="width:158px;" onclick="FeeCount();">&nbsp;&nbsp;
  	 		   <Input value="服务实施设置" type=button class=cssbutton style="width:158px;"  onclick="ServerSet();"> 
  	 	   </TD>
  	  </tr>
  	</table>
  	<br>
  <Div id="divLHServCustom" style="display:''">
  	<table  class= common align='center'>
  		<TR class= common>
  		  <TD  class= title>
		       客户号码
	      </TD>
	      <TD  class= input>
    	     <Input class= readonly readonly name=CustomerNo >
        </TD>
        <TD  class= title>
    	     客户姓名
        </TD>
	      <TD  class= input>
	        <Input class= readonly readonly name=CustomerName >
	      </TD>  
	      <TD  class= title>
	       	保单号
        </TD>
        <TD  class= input>
            <Input class= readonly readonly name=ContNo >
        </TD>
      </TR>
    </table>
  </div>
    
    <Div id="divLHServPlanGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServPlanGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
	
	<Div id="divLHPricePlan" style="display:''">
	<table  class= common align='center'>
		<TR class= common>  
      <TD  class= title>
       <INPUT CLASS=cssbutton VALUE="定价方式设置" TYPE=button onclick="ServePriceSet();"> 
      </TD>
    </TR>
  </table>
</div>
  
  <Div id="divLHFeeGrid" style="display:''">
  <table  class= common align='center'>
  	<tr class=common>
     <TD  class= title>
	       	费用结算状态
     </TD>
     <TD  class= input>
     	  <Input type=hidden name=FeeCountStatus>
         <Input class= 'code' name=FeeCountStatus_ch CodeData= "0|^1|不可结算^2|可以结算^3|结算完成" ondblclick= "showCodeListEx('',[this,FeeCountStatus],[1,0],null,null,null,1);" >
        
     </TD>
     
  	 <TD  class= title>
	       结算标识时间
     </TD>	
     <TD  class= input>
			   <Input class= 'CoolDatePicker' name=FeeCountDate>
		 </TD>
     <TD  class= title>
	       服务实施方式
     </TD>
     <TD  class= input>
     	 <Input type=hidden name=ServeDoneStyle>
         <Input class= 'code' name=ServeDoneStyle_ch CodeData= "0|^1|面向个人实施服务^2|面向团体实施服务" ondblclick="showCodeListEx('',[this,ServeDoneStyle],[0,1],null,null,null,1);" >
     </TD>
    </TR>
   <tr class=common>
     <TD  class= title>
	       	服务提供机构标识
     </TD>
     <TD  class= input>
    	   <Input class= readonly readonly name=ServeManacom >
     </TD>
     <TD  class= title>
	       	定价方式代码
     </TD>
     <TD  class= input>
    	   <Input class= readonly readonly name=ServePriceCode >
     </TD>
     <TD  class= title>
	       	定价方式名称
     </TD> 
     <TD  class= input>
    	   <Input class= readonly readonly name=ServePriceName >
     </TD>
    </Tr>
  </table>
</div>
	
	<Div id="divLHServGrid" style="display:''">
	<table  class= common align='center'>
	 <TR class = common>
		<TD  class= title>
	       	服务事件状态
     </TD>
     <TD  class= input>
     	   <Input type=hidden name=ServeEventStatus>
         <Input class= 'code' name=ServeEventStatus_ch CodeData= "0|^1|未进行启动设置^2|待其它事件完成^3|服务事件已启动^4|服务事件已完成^5|服务事件失败" ondblclick="showCodeListEx('',[this,ServeEventStatus],[0,1],null,null,null,1);" >
     </TD>
     <TD  class= title>
	   </TD> 
     <TD  class= input>
     </TD>
      <TD  class= title>
	   </TD> 
     <TD  class= input>
     </TD>
   </TR>
  </table>
</div>
  
  <Div id="divLHServEventGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServEventGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
	
  <Div id="divLHServMissionGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServMissionGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
	
	<br>	
 <table  class= common align='center'>
	 <TR class = common>
	 	 <Input value="新增服务任务" type=button class=cssbutton style="width:158px;"  onclick="NewServe();">&nbsp;&nbsp;
  	 <Input value="服务任务实施" type=button class=cssbutton style="width:158px;" onclick="ServeDone();">
	 </TR>
 </table>
		
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>