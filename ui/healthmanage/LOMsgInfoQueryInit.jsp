<%
//程序名称：QueryInit.jsp
//程序功能：功能描述
//创建日期：2005-11-02 14:02:34
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
  }
  catch(ex) {
    alert("在QueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLOMsgInfoGrid();  
    initLOMsgInfoReceiveGrid();
  }
  catch(re) {
    alert("QueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LOMsgInfoGrid;
function initLOMsgInfoGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="station";         //列名
    
    iArray[1]=new Array();
    iArray[1][0]="客户号码";         		//列名
    iArray[1][1]="0px";         		//列名
    iArray[1][3]=3;         				//列名
    
    iArray[2]=new Array();
    iArray[2][0]="客户姓名";         		//列名
    iArray[2][1]="0px";         		//列名
    iArray[2][3]=3;         				//列名
    
    iArray[3]=new Array();
    iArray[3][0]="手机号码";         		//列名
    iArray[3][1]="0px";         		//列名
    iArray[3][3]=3;         				//列名
    
    iArray[4]=new Array();
    iArray[4][0]="是否发送";         		//列名
    iArray[4][1]="40px";         		//列名
    iArray[4][3]=0;         				//列名
    
    iArray[5]=new Array();
    iArray[5][0]="短信类型"             //列名
    iArray[5][1]="0px";         		//列名
    iArray[5][3]=3;         				//列名       
    
    iArray[6]=new Array();                
    iArray[6][0]="设定日期";				//列名
    iArray[6][1]="40px";         		//列名
    iArray[6][3]=0;         				//列名      
    
    iArray[7]=new Array();                
    iArray[7][0]="信息主题";				//列名
    iArray[7][1]="0px";         		//列名
    iArray[7][3]=3;         				//列名      
    
    iArray[8]=new Array();                 
    iArray[8][0]="信息序号";				//列名 
    iArray[8][1]="0px";         		//列名 
    iArray[8][3]=3;         				//列名 
    
    iArray[9]=new Array();                 
    iArray[9][0]="内容";				//列名 
    iArray[9][1]="360px";         		//列名 
    iArray[9][3]=0;         				//列名 
    
    iArray[10]=new Array();                 
    iArray[10][0]="序号2";				//列名 
    iArray[10][1]="0px";         		//列名 
    iArray[10][3]=3;         				//列名 
                                          
                                          
    LOMsgInfoGrid = new MulLineEnter( "fm" , "LOMsgInfoGrid" ); 
    
    //这些属性必须在loadMulLine前

//    LOMsgInfoGrid.mulLineCount = 15;   
//    Grid.displayTitle = 1;
    LOMsgInfoGrid.hiddenPlus = 1;
    LOMsgInfoGrid.hiddenSubtraction = 1;
    LOMsgInfoGrid.canSel = 1;
//  LOMsgInfoGrid.canChk = 0;
    LOMsgInfoGrid.selBoxEventFuncName = "showCustomer";

    LOMsgInfoGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LOMsgInfoGridGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}


//领取项信息列表的初始化
var LOMsgInfoReceiveGrid;
function initLOMsgInfoReceiveGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="station";         //列名
    
    iArray[1]=new Array();
    iArray[1][0]="客户号码";         		//列名
    iArray[1][1]="50px";         		//列名
    iArray[1][3]=0;         				//列名
    
    iArray[2]=new Array();
    iArray[2][0]="客户姓名";         		//列名
    iArray[2][1]="40px";         		  //列名
    iArray[2][3]=0;         				    //列名
    
    iArray[3]=new Array();
    iArray[3][0]="手机号码";         		//列名
    iArray[3][1]="60";         		//列名
    iArray[3][3]=0;         				//列名
    
    iArray[4]=new Array();
    iArray[4][0]="团单号码";         		//列名
    iArray[4][1]="70px";         		//列名
    iArray[4][3]=0;         				//列名
    
    iArray[5]=new Array();
    iArray[5][0]="个单号码";				//列名
    iArray[5][1]="60px";         		//列名
    iArray[5][3]=0;         				//列名       
    
    iArray[6]=new Array();                
    iArray[6][0]="投保人";				//列名
    iArray[6][1]="120px";         		//列名
    iArray[6][3]=0;         				//列名     
    
    iArray[7]=new Array();                
    iArray[7][0]="Msgno";				//列名
    iArray[7][1]="0px";         		//列名
    iArray[7][3]=3;         				//列名 

    iArray[8]=new Array();                
    iArray[8][0]="Seqno";				//列名
    iArray[8][1]="0px";         		//列名
    iArray[8][3]=3;         				//列名          
    
    LOMsgInfoReceiveGrid = new MulLineEnter( "fm" , "LOMsgInfoReceiveGrid" ); 
    
    //这些属性必须在loadMulLine前

//    LOMsgInfoReceiveGrid.mulLineCount = 15;   
//    LOMsgInfoReceiveGrid.displayTitle = 1;
    LOMsgInfoReceiveGrid.hiddenPlus = 1;
    LOMsgInfoReceiveGrid.hiddenSubtraction = 1;
    LOMsgInfoReceiveGrid.canSel = 0;
//  LOMsgInfoReceiveGrid.canChk = 0;
//    LOMsgInfoReceiveGrid.selBoxEventFuncName = "showOne";

    LOMsgInfoReceiveGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LOMsgInfoReceiveGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
