<%
//程序名称：LHServEventQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-07-04 15:06:27
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
	function initForm() {
  try {
   
    initLHServEventGrid();  
  }
  catch(re) {
    alert("LHServEventQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHServEventGrid;
function initLHServEventGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="服务事件类型";   
	  iArray[1][1]="60px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
    
    iArray[2]=new Array(); 
	  iArray[2][0]="服务事件号码";   
	  iArray[2][1]="60px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
    
    iArray[3]=new Array(); 
	  iArray[3][0]="服务事件名称";   
	  iArray[3][1]="200px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=0;
	             
    iArray[4]=new Array(); 
	  iArray[4][0]="MakeDate";   
	  iArray[4][1]="0px";   
    iArray[4][3]=3;
    
    iArray[5]=new Array(); 
	  iArray[5][0]="MakeTime";   
	  iArray[5][1]="0px";   
    iArray[5][3]=3;
    
    
            
    LHServEventGrid = new MulLineEnter( "fm" , "LHServEventGrid" ); 
    //这些属性必须在loadMulLine前

    LHServEventGrid.mulLineCount = 0;   
    LHServEventGrid.displayTitle = 1;
    LHServEventGrid.hiddenPlus = 1;
    LHServEventGrid.hiddenSubtraction = 1;
    LHServEventGrid.canSel = 1;
    LHServEventGrid.canChk = 0;
   // LHServEventGrid.selBoxEventFuncName = "showOne";

    LHServEventGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomInHospitalGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
