var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value="INSERT||MAIN";
    fm.action="./LHEvalueMgtImportTSave.jsp";
    fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  initForm();
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCommTrans.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}                                          
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */              
function updateClick()
{
    var rowNum=LHEvalueMgtImportGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  var TaskExecNo=new Array;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHEvalueMgtImportGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx++] = row;
	             var ExecNo=new Array;
			         ExecNo=LHEvalueMgtImportGrid.getRowColData(aa,6);//服务任务实施号码
			         //alert(ExecNo);
			         TaskExecNo[row]=ExecNo;
	        }
	  }
	  //alert(TaskExecNo);
	  var arrTaskExecNo=TaskExecNo.toString().split(",");
	  var sqlTaskExecNo = "";
	  for(var j=0; j < arrTaskExecNo.length; j++)
	  {
	      sqlTaskExecNo= sqlTaskExecNo+",'"+arrTaskExecNo[j]+"'";
	      //alert(" "+sqlTaskExecNo);
	  }
	  sqlTaskExecNo = sqlTaskExecNo.substring(1);
	  //alert("SSSS "+sqlTaskExecNo);
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要进行关联的信息记录!");
			  return false;
		}	
		if(aa.length>="1")
		{
         var sqlExecNo=" select d.TaskExecNo from LHEvaReport d "
			            +"where d.TaskExecNo in ("+ sqlTaskExecNo +")";
			   //alert(sqlExecNo);
		     var  aResult = easyExecSql(sqlExecNo);
		     //alert(aResult);
		     if(aResult==""||aResult==null||aResult=="null")
		     {
             var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
             var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
             showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
             fm.fmtransact.value="INSERT||MAIN";
             fm.action="./LHEvalueMgtImportUpDate.jsp";
             fm.submit(); //提交 
         }
         else
         {
         	  alert("您选择的评估报告信息，有未删除关联关系的信息，不能进行关联!");
         	  return false;
         }
   }
}


function showTwo()
{
  var rowNum=LHEvalueMgtImportGrid. mulLineCount ; //行数 	
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHEvalueMgtImportGrid.getChkNo(row); 
		if(tChk == true)
		{
			aa[xx++] = row;	
	  }  
	}
	if(aa.length=="1")
	{
		 var TaskExecNo=LHEvalueMgtImportGrid.getRowColData(aa,6);//服务任务实施号码
	   fm.all('TaskExecNo').value=TaskExecNo;
		 sqlExecState=" select d.TaskExecState from LHTaskCustomerRela d where d.TaskExecNo='"+ TaskExecNo +"'";
		 var rExecState=new Array;
		 rExecState=easyExecSql(sqlExecState);
		 if(rExecState!=""&&rExecState!=null&&rExecState!="null")
		 {
		 	  if(rExecState=="1")
		 	  {
		 	  	 fm.all('ExecState').value="1";
		 	  	 fm.all('ExecState_ch').value="未执行";
		 	  }
		 	  if(rExecState=="2")
		 	  {
		 	  	 fm.all('ExecState').value="2";
		 	  	 fm.all('ExecState_ch').value="已执行";
		 	  }
		 }    
	}
	if(aa.length>"1")
	{
		   fm.all('ExecState').value=""; 
		   fm.all('ExecState_ch').value="";   
	}
}   
function updateStatus()//修改服务执行状态
{
	  var rowNum=LHEvalueMgtImportGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHEvalueMgtImportGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx] = row;
	        }
	  }
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要修改状态的信息记录!");
			  return false;
		}	
		if(aa.length>="1")
		{
    	if (confirm("您确实想修改这些记录吗?"))
    	{
          var i = 0;
          var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
          fm.fmtransact.value = "UPDATE||MAIN";
          fm.action="./LHEvalueMgtImportStateSave.jsp";
          fm.submit(); //提交         
		  }
      else
      {
        alert("您取消了修改操作！");
      } 
   }
}
function LookReport()
{
	 var rowNum=LHEvalueMgtImportGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  var FileName="";
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHEvalueMgtImportGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx++] = row;
	        	   FileName=LHEvalueMgtImportGrid.getRowColData(aa,3);//评估报告文件名
	        }
	  }
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要查看评估报告的信息记录!");
			  return false;
		}	
		//alert(aa.length);
		if(aa.length>"1")
		{
    	alert("您只能对一条健管评估报告进行查看，请选择一条服务信息!");
    	return false;
   }
   //alert(FileName);
   if(aa.length=="1")
   {
       //ftp://jianguan:guanjian@10.252.1.121/home/jianguan/991.pdf
       window.open("../hmpdf/"+FileName+".pdf","查看评估报告",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
   }
}
function deleteClick()
{
	  var rowNum=LHEvalueMgtImportGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  var TaskExecNo=new Array;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHEvalueMgtImportGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx++] = row;
			         var ExecNo=new Array;
			         ExecNo=LHEvalueMgtImportGrid.getRowColData(aa,6);//服务任务实施号码
			         //alert(ExecNo);
			         TaskExecNo[row]=ExecNo;
	        }
	  }
	  var arrTaskExecNo=TaskExecNo.toString().split(",");
	  var sqlTaskExecNo = "";
	  for(var j=0; j < arrTaskExecNo.length; j++)
	  {
	      sqlTaskExecNo= sqlTaskExecNo+",'"+arrTaskExecNo[j]+"'";
	      //alert(" "+sqlTaskExecNo);
	  }
	  sqlTaskExecNo = sqlTaskExecNo.substring(1);
	  //alert("SSSS "+sqlTaskExecNo);
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要删除评估报告的信息记录!");
			  return false;
		}	
		if(aa.length>="1")
		{
			var sqlExecNo=" select d.TaskExecNo from LHEvaReport d "
			            +"where d.TaskExecNo in ("+ sqlTaskExecNo +")";
			//alert(sqlExecNo);
		  var  aResult = easyExecSql(sqlExecNo);
		  //alert("DDDD "+aResult.length);
		  if(aResult==""||aResult==null||aResult=="null")//如果选择的值在数据库中不存在
		  {
		  	 alert("您选择的评估报告信息，有未关联的服务信息，不能删除!");
		  	 return false;
		  }
		  else
		  {
		     if(aResult.length==aa.length)//如果选择的信息和数据库的信息正好相等
		     {
		     	  if (confirm("您确实想删除这些记录吗?"))
    	      {
                 var i = 0;
                 var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
                 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
                 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
                 fm.fmtransact.value = "DELETE||MAIN";
                 fm.action="./LHEvalueMgtImportUpDate.jsp";
                 fm.submit(); //提交         
		         }
             else
             {
               alert("您取消了删除操作！");
               return false;
             } 
		     }
		     else
		     {
		     	 alert("您选择的评估报告信息不全为已关联状态!");
		     	 return false;
		     }
		  }
   }
}