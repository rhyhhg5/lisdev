<%
//程序名称：HealthContQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-06-06 18:10:02
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() 
{
	try {
			fm.all('ContType').value = '0';
			fm.all('ContTypeName').value = '个人保单－按保费';
			fm.all('ContStateName').value = "1" 
			fm.all('ContState').value = "契约事件未设置" 
			fm.all('CaseStateName').value = "1";
			fm.all('CaseState').value = "未进行服务信息设置";
		}
	catch(ex) {alert("在HealthContQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");}
}                                 

function initForm() 
{
	try 
	{
	    initInpBox();
	    initHealthContGrid();  
	    initContCustomerGrid();
  	}
	catch(re)
	{
	    alert("HealthContQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

//领取项信息列表的初始化
var HealthContGrid;
function initHealthContGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";		//列名
	    iArray[0][1]="50px";		//列名
	    iArray[0][3]=0;				//列名
	    
	    iArray[1]=new Array();		
		iArray[1][0]="保单号";		//列名    
		iArray[1][1]="100px";		//列名    
		iArray[1][3]=0;				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="保单状态";	//列名    
		iArray[2][1]="100px";       //列名    
		iArray[2][3]=0;         	//列名        
			
		iArray[3]=new Array();                    
		iArray[3][0]="保单类型";    //列名    
		iArray[3][1]="100px";       //列名    
		iArray[3][3]=0;         	//列名   
		
		iArray[4]=new Array();  
		iArray[4][0]="missionid";
		iArray[4][1]="0px";   
		iArray[4][3]=3;         
		
		iArray[5]=new Array();  
		iArray[5][0]="submissionid";
		iArray[5][1]="0px";   
		iArray[5][3]=3;   
		
		iArray[6]=new Array();  
		iArray[6][0]="所属机构";
		iArray[6][1]="100px";   
		iArray[6][3]=0;    
		
		iArray[7]=new Array();  
		iArray[7][0]="保单生效时间";
		iArray[7][1]="100px";   
		iArray[7][3]=0;   
		
		iArray[8]=new Array();  
		iArray[8][0]="续保次数";
		iArray[8][1]="40px";   
		iArray[8][3]=0;            
			     
	    
	    HealthContGrid = new MulLineEnter( "fm" , "HealthContGrid" ); 
	    //这些属性必须在loadMulLine前           
	    
		HealthContGrid.canSel = 1;
		HealthContGrid.hiddenPlus = 1;
		HealthContGrid.hiddenSubtraction = 1;       
		HealthContGrid.selBoxEventFuncName ="showCustomer"
	/*
	    HealthContGrid.mulLineCount = 0;   
	    HealthContGrid.displayTitle = 1;
	    HealthContGrid.canSel = 1;
	    HealthContGrid.canChk = 0;
	    HealthContGrid.selBoxEventFuncName = "showOne";
	*/
	    HealthContGrid.loadMulLine(iArray);
	    //这些操作必须在loadMulLine后面
	    //HealthContGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
		alert(ex);
	}
}

//领取项信息列表的初始化
var ContCustomerGrid;
function initContCustomerGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";		//列名
	    iArray[0][1]="50px";		//列名
	    iArray[0][3]=0;				//列名
	    
	    iArray[1]=new Array();		
		iArray[1][0]="客户号";		//列名    
		iArray[1][1]="60px";		//列名    
		iArray[1][3]=0;				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="客户姓名";	//列名    
		iArray[2][1]="60px";       //列名    
		iArray[2][3]=0;         	//列名   
		
		iArray[3]=new Array();		
		iArray[3][0]="性别";		//列名    
		iArray[3][1]="40px";		//列名    
		iArray[3][3]=0;				//列名        
		
		iArray[4]=new Array();                    
		iArray[4][0]="生日";	//列名    
		iArray[4][1]="60px";       //列名    
		iArray[4][3]=0;         	//列名         

		iArray[5]=new Array();              
		iArray[5][0]="服务计划名称";	//列名      
		iArray[5][1]="90px";       //列名   
		iArray[5][3]=0;         	//列名  

		iArray[6]=new Array();              
		iArray[6][0]="服务计划档次";	//列名      
		iArray[6][1]="60px";       //列名   
		iArray[6][3]=0;         	//列名  

		
		iArray[7]=new Array();              
		iArray[7][0]="契约事件状态";	//列名      
		iArray[7][1]="120px";       //列名   
		iArray[7][3]=0;         	//列名  
			

	    
	    ContCustomerGrid = new MulLineEnter( "fm" , "ContCustomerGrid" ); 
	    //这些属性必须在loadMulLine前           
	    
		ContCustomerGrid.canSel = 1;
		ContCustomerGrid.hiddenPlus = 1;
		ContCustomerGrid.hiddenSubtraction = 1;
	/*
	    ContCustomerGrid.mulLineCount = 0;   
	    ContCustomerGrid.displayTitle = 1;
	    ContCustomerGrid.canSel = 1;
	    ContCustomerGrid.canChk = 0;
	    ContCustomerGrid.selBoxEventFuncName = "showOne";
	*/
	    ContCustomerGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //ContCustomerGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
		alert(ex);
	}
}


//领取项信息列表的初始化
var OperatorContGrid;
function initOperatorContGrid() 
{
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";		//列名
	    iArray[0][1]="50px";		//列名
	    iArray[0][3]=0;				//列名
	    
	    iArray[1]=new Array();		
		iArray[1][0]="保单号";		//列名    
		iArray[1][1]="100px";		//列名    
		iArray[1][3]=0;				//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="保单状态";	//列名    
		iArray[2][1]="100px";       //列名    
		iArray[2][3]=0;         	//列名        
			
		iArray[3]=new Array();                    
		iArray[3][0]="保单类型";    //列名    
		iArray[3][1]="100px";       //列名    
		iArray[3][3]=0;         	//列名   
		
		//	iArray[4]=new Array();  
		//	iArray[4][0]="Operator";
		//	iArray[4][1]="0px";   
		//	iArray[4][3]=3;         
		//	
		//	iArray[5]=new Array();  
		//	iArray[5][0]="MakeDate";
		//	iArray[5][1]="0px";   
		//	iArray[5][3]=3;         
		//	
		//	iArray[6]=new Array();  
		//	iArray[6][0]="MakeTime";
		//	iArray[6][1]="0px";   
		//	iArray[6][3]=3;         
			     
	    
	    OperatorContGrid = new MulLineEnter( "fm" , "OperatorContGrid" ); 
	    //这些属性必须在loadMulLine前           
	    
		OperatorContGrid.canSel = 1;
		OperatorContGrid.hiddenPlus = 1;
		OperatorContGrid.hiddenSubtraction = 1;
	/*
	    OperatorContGrid.mulLineCount = 0;   
	    OperatorContGrid.displayTitle = 1;
	    OperatorContGrid.canSel = 1;
	    OperatorContGrid.canChk = 0;
	    OperatorContGrid.selBoxEventFuncName = "showOne";
	*/
	    OperatorContGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //OperatorContGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
		alert(ex);
	}
}
</script>
