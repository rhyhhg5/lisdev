<%
//程序名称：LHHealthServPlanQueryInputInit.jsp
//程序功能：功能描述
//创建日期：2006-03-20 17:16:48
//创建人  ：郭丽颖
//更新记录：
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
     fm.all('Riskcode').value = "";
     fm.all('Riskname').value = "";
     //fm.all('ServItemCode').value = "";
     //fm.all('ServItemName').value = "";
     fm.all('ComIDCode').value = "";
     fm.all('ComIDName').value = "";

  }
  catch(ex) {
    alert("在LHHealthServPlanQueryInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHHealthServPlanGrid();  
  }
  catch(re) {
    alert("LHHealthServPlanQueryInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }                                                      
}
//领取项信息列表的初始化
var LHHealthServPlanGrid;
function initLHHealthServPlanGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="";         				//列名
    
    iArray[1]=new Array();                
    iArray[1][0]="服务计划代码";     		//列名
    iArray[1][1]="150px";         		//列名
    iArray[1][3]=0;         				//列名
    iArray[1][4]="";         				//列名
    
    iArray[2]=new Array();                
    iArray[2][0]="服务计划名称";        //列名
    iArray[2][1]="150px";         		//列名
    iArray[2][3]=0;         				//列名
    iArray[2][4]="";         				//列名
    
    iArray[3]=new Array();                
    iArray[3][0]="个人服务计划档次";      //列名
    iArray[3][1]="150px";         		//列名
    iArray[3][3]=0;         					//列名
    iArray[3][4]="";         					//列名
    
    iArray[4]=new Array();                         
    iArray[4][0]="机构属性标识";      //列名 
    iArray[4][1]="150px";         		//列名       
    iArray[4][3]=0;         					//列名         
    iArray[4][4]="";   
    
    iArray[5]=new Array();                
    iArray[5][0]="档次编号";      //列名
    iArray[5][1]="0px";         		//列名
    iArray[5][3]=3;         					//列名
    iArray[5][4]="";         					//列名
          					//列名         
    
    iArray[6]=new Array();                
    iArray[6][0]="ComID";      //列名
    iArray[6][1]="0px";         		//列名
    iArray[6][3]=3;         					//列名
    iArray[6][4]="";         					//列名
    
    
    iArray[7]=new Array();                
    iArray[7][0]="PlanType";      //列名
    iArray[7][1]="0px";         		//列名
    iArray[7][3]=3;         					//列名
    
    LHHealthServPlanGrid = new MulLineEnter( "fm" , "LHHealthServPlanGrid" ); 
    //这些属性必须在loadMulLine前

		LHHealthServPlanGrid.canSel = 1;     
		LHHealthServPlanGrid.canChk = 0;
		LHHealthServPlanGrid.mulLineCount = 0;     
		LHHealthServPlanGrid.displayTitle = 1;     
		LHHealthServPlanGrid.hiddenPlus = 1;       
		LHHealthServPlanGrid.hiddenSubtraction = 1;

    LHHealthServPlanGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    
  }
  catch(ex) {
    alert(ex);
  }
}
</script>





