<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHIndivServCaseSendSave.jsp
//程序功能：个人服务受理事件任务关联发送操作
//创建日期：2006-08-30 16:04:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期 : 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  
  LHIndivServCaseSendUI tLHIndivServCaseSendUI   = new LHIndivServCaseSendUI();
  LHTaskCustomerRelaSet tLHTaskCustomerRelaSet   = new LHTaskCustomerRelaSet();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String ItemNo="";
 
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    
  	     LHCaseTaskRelaSchema tLHCaseTaskRelaSchema   = new LHCaseTaskRelaSchema();
  	     
  	     tLHCaseTaskRelaSchema.setServCaseCode(request.getParameter("ServCaseCode"));//事件编号
  	     tLHCaseTaskRelaSchema.setServTaskCode(request.getParameter("ServTaskCode"));//任务代码
  	     
  	     tLHCaseTaskRelaSchema.setTaskFinishDate(request.getParameter("ServFinDate"));//任务完成时间
         tLHCaseTaskRelaSchema.setServTaskAffirm("1");//任务确认
         tLHCaseTaskRelaSchema.setServTaskState("3");//任务状态
         
  	     ItemNo=request.getParameter("ServItemNo");
         String[] ServItemNo  = null;
  	     ServItemNo=ItemNo.split(",");
  	     for(int i=0;i<ServItemNo.length;i++)
  	     {
  	         LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema   = new LHTaskCustomerRelaSchema();
             tLHTaskCustomerRelaSchema.setServCaseCode(request.getParameter("ServCaseCode"));//事件编号
  	         tLHTaskCustomerRelaSchema.setServTaskCode(request.getParameter("ServTaskCode"));//任务代码
  	         tLHTaskCustomerRelaSchema.setTaskExecState("2");//任务代码
  	         tLHTaskCustomerRelaSchema.setServItemNo(ServItemNo[i]);
  	        
  	         tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);
  	     }
  	     
         LHIndiServAcceptSchema tLHIndiServAcceptSchema   = new LHIndiServAcceptSchema();
         tLHIndiServAcceptSchema.setServAccepNo(request.getParameter("ServAccepNo"));//服务受理编号
         
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLHCaseTaskRelaSchema);
	  tVData.add(tLHTaskCustomerRelaSet);
	  tVData.add(tLHIndiServAcceptSchema);
  	tVData.add(tG);
    
    tLHIndivServCaseSendUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHIndivServCaseSendUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	
</script>
</html>