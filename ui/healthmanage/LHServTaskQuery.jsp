<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHServTaskQueryInit.jsp
//程序功能：
//创建日期：2006-07-24 10:39:55
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<SCRIPT src="LHServTaskQuery.js"></SCRIPT>
	<%@include file="LHServTaskQueryInit.jsp"%>
</head>
<%
	GlobalInput tG = new GlobalInput(); 
	tG = (GlobalInput)session.getValue("GI");
%>
<script>
	var comCode = "<%=tG.ComCode%>";
	var manageCom = "<%=tG.ManageCom%>";
	var operator = "<%=tG.Operator%>";
</script>
<body  onload="initForm();" >
<form action="" method=post name=fm target="fraSubmit">
<%@include file="../common/jsp/InputButton.jsp"%>
	<div style="display:''">
	<table>
    	<tr>
			<td>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHSettingSlipQueryGrid1);">
     		</td>
			<td class= titleImg>
    	 		服务任务查询信息
    		</td>   		 
    	</tr>
	</table>
    <Div  id= "divLHSettingSlipQueryGrid1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
        		<TD class= title >
				服务事件类型
       			</TD>   		 
				<TD  class= input>
				 <!--<Input class= 'codename' style="width:40px" name=ServCaseType value=1><Input class= 'codeno' style="width:120px"  name=ServCaseTypeName value="个人服务事件" ondblclick="showCodeList('eventtype',[this,ServCaseType],[1,0],null,null,null,'1',160);" codeClear(ServCaseTypeName,ServCaseType);">-->
					<Input class= 'codename' style="width:40px" name=ServCaseType ><Input class= 'codeno' style="width:120px"  name=ServCaseTypeName ondblclick="showCodeList('eventtype',[this,ServCaseType],[1,0],null,null,null,'1',160);" codeClear(ServCaseTypeName,ServCaseType);">
				</TD>
         		<TD  class= title style="width:120">
            		服务事件编号
         		</TD>
         		<TD  class= input>
         		  	<Input class= 'code' name=ServCaseCode  ondblclick="showCodeList('lhservcasecode',[this,ServCaseName],[0,1],null,fm.ServCaseCode.value,'ServCaseCode','1',300);"  codeClear(ServCaseName,ServCaseCode);">
         		</TD>
         		<TD  class= title style="width:120">
         		   服务事件名称
         		</TD>
         		<TD  class= input>
         			<Input class= 'code' name=ServCaseName   ondblclick="showCodeList('lhservcasecode',[this,ServCaseCode],[1,0],null,fm.ServCaseName.value,'ServCaseName','1',300);"  codeClear(ServCaseName,ServCaseCode);">
         		</TD>
			</TR> 
			<TR  class= common>
			    <TD class= title >
        			服务事件状态
       			</TD>   		 
				<TD  class= input>
				 <!--<Input class= 'codename' style="width:40px" name=ServCaseState value=2><Input class= 'codeno' style="width:120px"  name=ServCaseStateName value="服务事件已启动" ondblclick="showCodeList('eventstate',[this,ServCaseState],[1,0],null,null,null,'1',160);" codeClear(ServCaseState,ServCaseStateName);">-->
					<Input class= 'codename' style="width:40px" name=ServCaseState ><Input class= 'codeno' style="width:120px"  name=ServCaseStateName ondblclick="showCodeList('eventstate',[this,ServCaseState],[1,0],null,null,null,'1',160);" codeClear(ServCaseState,ServCaseStateName);">
				</TD>
         		<TD  class= title>
			    	服务任务名称
			    </TD>
        		<TD  class= input>
    	      		<Input  class=codeno name= ServTaskCode  ondblclick="showCodeList('lhservtaskcode',[this,ServTaskName],[0,1],null,fm.ServTaskName.value,'ServTaskName','',200);"  codeClear(ServTaskName,ServTaskCode);"><Input class= 'codename' name=ServTaskName style="width:112px" verify="服务任务名称|NOTNULL">
		        </TD>  
       			<TD class= title >
        			服务任务状态
       			</TD>   		 
				<TD  class= input>
					<Input class= 'codename' style="width:40px" name=ServTaskState  value="1"><Input class= 'codeno' style="width:120px"  name=ServTaskStateName  value="等待执行" ondblclick="showCodeList('taskstatus',[this,ServTaskState],[1,0],null,null,null,'1',130);" codeClear(ServTaskState,ServTaskStateName);">
				</TD>
			</TR> 
			<tr>
				<TD  class= title>
					计划实施时间(起始)
				</TD>
          		<TD  class= input>
          		   <Input name=StartDate class='coolDatePicker' dateFormat='short' style="width:142px" verify="计划实施时间(起始)|notnull&Date" > 
          		</TD> 
          		<TD  class= title>
          			计划实施时间(终止)
          		</TD>
          		<TD  class= input> 
          			<Input name=EndDate class='coolDatePicker' dateFormat='short' style="width:142px" verify="计划实施时间(终止)|notnull&Date" >
          		</TD> 
          		<TD class= title >
        			服务任务确认
       			</TD>   		 
				<TD  class= input>
					<Input class= 'codename' style="width:40px" name=TaskValidate><Input class= 'codeno' style="width:120px"  name=TaskValidateName  ondblclick="showCodeList('taskvalidate',[this,TaskValidate],[1,0],null,null,null,'1',130);" codeClear(TaskValidate,TaskValidateName);">
				</TD>
			</tr>
			<TR  class= common>
				<TD  class= title>
					实际实施时间(起始)
				</TD>
          		<TD  class= input>
          		   <Input name=FinStartDate class='coolDatePicker' dateFormat='short' style="width:142px" verify="实际实施时间(起始)|notnull&Date" > 
          		</TD> 
          		<TD  class= title>
          			实际实施时间(终止)
          		</TD>
          		<TD  class= input> 
          			<Input name=FinEndDate class='coolDatePicker' dateFormat='short' style="width:142px" verify="实际实施时间(终止)|notnull&Date" >
          		</TD> 
          		<TD  class= title></TD><TD  class= input></TD>
			</tr>
			<TR  class= common id=iFeePay style="display:'none'">
			    <TD  class= title> 
			    	医疗机构名称
			    </TD>
			    <TD  class= input>
					<Input class= 'code' name= HospitName ondblclick="return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,300); ">
			    </TD>
				<TD  class= title>
					医疗机构代码
			    </TD>
			    <TD  class= input>
					<Input class= 'common' name=HospitCode >
			    </TD> 
			    <TD  class= title colspan=3>
			    	<input type= button class=cssButton name=queryFee style="width:80px" value="查询财务数据" OnClick="feeQuery()">	
			    	<input type= button class=cssButton name=subFee style="width:80px" value="提交选定数据" OnClick="feeSubmit()">
			    	<input type= button class=cssButton name=subFee style="width:80px" value="提交全部数据" OnClick="feeAllSubmit()">
			    </TD>
    		</TR> 
		</table>
		<br>
		<table  class= common align='center' >
			<TR  class= common>
				<td>
           			<input type= button class=cssButton  style="width:130px" value="查询" OnClick="showQueryInfo();">
       			</td> 
       			<td >
					<input type= button class=cssButton name=CaseShiShi   style="width:130px" value="服务事件实施管理" OnClick="CaseExecManage();">
				</td> 
				<td style="width:500px;">
				</td>
			</tr>
		</table>
    </Div>    
	<Div id="divLHSettingSlipQueryGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHSettingSlipQueryGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
</div>

	<br><br>
	
<Div id="divLHQueryDetailGrid" style="display:'none'">
	<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHQueryDetailGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	<Div id= "divPage2" align=center style= "display: 'none' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	  </Div>
	</div>
  	
	<table>
     <tr style="width:50px" > 

       <td>
       		 	<input type= button class=cssButton name=ServQieYue  style="width:130px" value="服务任务实施管理" OnClick="TaskExecManage();">
       	</td>  
       	<td>
       		 	<input type= button class=cssButton  style="width:130px" value="确认已完成任务" OnClick="ServAffirm();">
       	</td>
		   </tr> 
  </table>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmAction" name="fmAction">
		<!--团体服务计划号码-->
		<Input class= 'common' type=hidden  name=GrpServPlanNo >
		
		<!--个人服务计划号码-->
		<Input class= 'common' type=hidden   name=ServPlanNo >
		  
		<!--服务计划类型-->
		<Input class= 'common' type=hidden name=ServPlayType >
		<Input class= 'common' type=hidden name=ManageCom >
		<Input type=hidden name=Operator >
		<Input type=hidden name=MakeDate >
		<Input type=hidden name=MakeTime >
		<input type=hidden id=ManageCom name="ComID">
		<Input type=hidden name=FeeFlag >
		<Input type=hidden name=FeeAllSql >
  	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
</html>
