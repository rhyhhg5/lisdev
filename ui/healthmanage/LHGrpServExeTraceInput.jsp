<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHGrpServExeTraceInput.jsp
//程序功能：
//创建日期：2006-03-09 16:05:13
//创建人  ：林仁令
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
	<SCRIPT src="LHGrpServExeTraceInput.js"></SCRIPT>
	<%@include file="LHGrpServExeTraceInputInit.jsp"%>
</head>
<body  onload="initForm();passQuery();" >
  <form action="./LHGrpServExeTraceSave.jsp" method=post name=fm target="fraSubmit">
  
     <span id="operateButton" >
	<table  class=common align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
				<td class=button width="10%" align=right>
				<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
			</td>			
			<td class=button width="10%" align=right>
				<INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="return queryClick();">
			</td>			
		</tr>
	</table>
</span>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 团单项目执行轨迹信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHGrpServExeTrace1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
			    <TD  class= title>
			      团体客户名称
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpName readonly>
			    </TD>
			    <TD  class= title>
			      保单号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=GrpContNo readonly>
			    </TD>
			    <TD  class= title>
			      团体客户分级标识
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ServPlayType readonly>
			    </TD>
			</TR>
		</table>
    </Div>
    
     <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServItemGrid);">
    		</td>
    		 <td class= titleImg>
        		 项目执行状态信息
       		 </td>   		 
    	</tr>
    </table>
    
	<Div id="divLHServItemGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHServItemGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
    
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    
    <Input type=hidden name=GrpServItemNo >    <!--团体服务项目号码-->
    <Input type=hidden name=GrpServPlanNo >    <!--团体服务计划号码-->
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
		<script>
		//通过综合查询进入此页面的处理
		var turnPage = new turnPageClass(); 
		function passQuery()
		{ 	
			  //进入此函数
			  		  
					  var GrpServPlanNo = "<%=request.getParameter("GrpServPlanNo")%>";
					
					
				  	if(GrpServPlanNo!=null||GrpServPlanNo>0) 
				  	{
				  	//	alert(GrpServPlanNo+"-----"+GrpServPlanNo);
				  			var strSql = " select distinct GrpName,GrpContNo,ServPlanLevel"
				  									+" from LHGrpServPlan "
				  									+" where GrpServPlanNo = '"+GrpServPlanNo+"'"
				  									;
				  			var arrResult = easyExecSql(strSql);
				  		//	alert("----------"+strSql);
              //   alert("-------------"+arrResult);
								
								fm.all('GrpName').value = arrResult[0][0];
								fm.all('GrpContNo').value = arrResult[0][1];
								fm.all('ServPlayType').value = arrResult[0][2];
								
								var strSql2 = " select distinct c.ServItemCode,(select distinct d.ServItemName from LHHealthServItem d where d.ServItemCode=(select distinct c.ServItemCode from LHGrpServItem c where c.GrpServItemNo =b.GrpServItemNo and c.OrgType='2')) ,(select distinct e.ServItemSerial from LHGrpServItem e where e.GrpServItemNo= b.GrpServItemNo and e.OrgType='2' ) ,b.ExeState,b.ExeDate,b.ServDesc,b.MakeDate,b.MakeTime,b.GrpServPlanNo,b.GrpServItemNo"
      											 +" from LHGrpServExeTrace b ,LHGrpServItem c"
				  									 +" where  "
				  									 +"b.GrpServPlanNo = '"+GrpServPlanNo+"' and c.GrpServItemNo =b.GrpServItemNo and c.OrgType='2' "
      											 +" order by b.GrpServItemNo"
      											 ;  
  								
// alert(strSql2);
      											
      											 turnPage.queryModal(strSql2,LHServItemGrid); 
								
					  } 
					  else 
					  {
					  	  alert("没有此个单项目执行轨迹信息!");
					  }
		    
	}
		
	</script>
</html>
