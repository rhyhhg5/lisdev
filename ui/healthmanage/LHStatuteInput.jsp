<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-19 13:04:32
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHStatuteInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHStatuteInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHStatuteSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <Div id = "divQueryButton" style="display: 'none'" align=right>
    	<table>
    		<tr>
    			<td class=button width="10%" align=right>
						<INPUT class=cssButton name="querybutton1" VALUE="查  询"  TYPE=button onclick="return queryClick();">
					</td>	
				</tr>	
			</table>
			<hr>
		</Div>	
    
   <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHStatute1);">
    		</td>
    		 <td class= titleImg>
        		 社保政策法规信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHStatute1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    
    <TD  class= title>
      发文号
    </TD>   
    <TD  class= input>
      <Input class= 'common' name=StatuteNo elementtype=nacessary verify="发文号|notnull&len<=20">
    </TD>
    <TD  class= title>
      政策法规文件名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StatuteTitle elementtype=nacessary verify="政策法规文件名称|notnull&len<=60">
    </TD>
    <TD  class= title>
      文件类型
    </TD>
    <TD  class= input>
      <Input class='code' name=ContTypeCode  verify="文件类型|len<=20">
    </TD>
  </TR>  
  <TR  class= common>
<!--    
		<TD  class= title>
      发文号
    </TD>   
    <TD  class= input>
      <Input class= 'common' name=DocumentNum verify="发文号|len<=50">
    </TD>
		
		<TD  class= title>
      内容分类代码
    </TD>
    <TD  class= input>
       <Input class= 'code' name=ContType ondblClick="showCodeListEx('ContType',[this,''],[0,1]);" verify="定点属性标识符|len<=2"  CodeData="0|^1|^2|" onkeyup="showCodeListEx('ContType',[this,''],[0,1]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      政策法规文件代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StatuteNo elementtype=nacessary verify="政策法规文件代码|NUM&notnull&len<=20">
    </TD>
    <TD  class= title>
      颁布单位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IssueOrganCode verify="颁布单位|len<=20">
    </TD>
-->    
    <TD  class= title>
      颁布日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=IssueDate verify="颁布日期|DATE">
    </TD>
    <TD  class= title>
      实施日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=StartDate verify="实施日期|DATE">
    </TD>
    <TD  class= title>
      终止日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=EndDate verify="终止日期|DATE">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      内容简介
    </TD>
    </TR> 
</table>
<table  class= common align='center' >     
  <TR  class= common>                        
    <TD  class= input>
      <!--<Input class= 'common' type=hidden name=DocContent verify="内容简介|len<=2000">-->
      <TextArea class='common' cols="108%" rows="25" witdh="120%" name=DocContent verify="内容简介|len<=50000"></TextArea>
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
		<input type=hidden name="LoadFlag">
		<input type=hidden name=MakeDate>
		<input type=hidden name=MakeTime>
		
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
