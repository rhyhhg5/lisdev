<%
//程序名称：LHHmBespeakManageInit.jsp
//程序功能：健管服务预约管理
//创建日期：2006-09-01 15:51:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.*"%>
<%
	
	String ServTaskNo = request.getParameter("ServTaskNo");
	String sqlServTaskNo = "'"+ServTaskNo.replaceAll(",","','")+"'";
	System.out.println("任务编号信息 "+sqlServTaskNo);
  String flag = request.getParameter("flag");
%>         
           
<script language="JavaScript">   
var ServTaskNo = "<%=ServTaskNo%>";  
var turnPage = new turnPageClass(); 
var aResult=new Array;  
                                  
function initForm()
{
	fm.all('TPageTaskNo').value= "<%=sqlServTaskNo%>";
	//alert(fm.all('TPageTaskNo').value);
	fm.all('flag').value= "<%=flag%>";
	try
	{
		initLHHmServBeManageGrid();
		
		//取得传入任务流水号的所有任务执行信息
		var SqlToExec  = " select TaskExecNo, ServCaseCode, ServTaskNo,ServItemNo from LHTaskCustomerRela "
						+" where ServTaskNo in ("+"<%=sqlServTaskNo%>"+") order by ServTaskNo";
	  //alert(SqlToExec);
		aResult = easyExecSql(SqlToExec);
		
	    var flag= "<%=flag%>";
	    if(flag=="1")
	    {
	    	var    strsql="  select TaskExecNo from LHHmServBeManage d where d.TaskExecNo='"+ aResult[0][0]+"'" ;
		    var result=new Array;
		    result=easyExecSql(strsql);
        //alert(result);
		    if(result=="" || result==null || result=="null")
		    {//未保存过
	    	   displayInfo();
	    	   fm.all('save').disabled=false;
	    	}
	    	if(result!="" && result!=null && result!="null")
		    {//保存过
	    	   displayConetent();
	    	   fm.all('save').disabled=true;
	    	}
	    	//fm.all('ContSomiteInform').disabled=true;
	    	fm.all('GroupSomiteInform').disabled=true;
	    }
	}
	catch(re)
	{
		alert("LHHmBespeakManageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
 	}
}
function displayInfo()
{
		strsql=" select (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo ) ,"
		      +" (select a.CustomerName from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo ),"
		      +" '','','','','','','',"
		      +" d.ServTaskNo,d.ServCaseCode ,"
		      +" d.TaskExecNo ,d.ServItemNo,"
		      +" (select b.ServTaskCode from LHCaseTaskRela b where b.ServTaskNo=d.ServTaskNo ),'',"
		      +"( case  d.TaskExecState when '1' then '未执行' when  '2' then '已执行' else '' end ), "
		      +" d.TaskExecState "
		      +" from LHTaskCustomerRela d "
		      +" where d.ServTaskNo in ("+"<%=sqlServTaskNo%>"+")"
		      ;
		
		turnPage.pageLineNum = 15;  
		turnPage.queryModal(strsql, LHHmServBeManageGrid);
}
function displayConetent()
{		
		  strsql=" select CustomerNo,(select a.Name from LDPerson a where a.CustomerNo=d.CustomerNo),"
		        +" BespeakComID,"
		        +" (select  HospitName from LDHospital where LDHospital.HospitCode=d.BespeakComID),"
		        +" ServBespeakDate,ServBespeakTime,"
		        +" (select distinct c.Testgrpname from ldtestgrpmgt c where ( c.testgrptype='2' or c.testgrptype='3') and c.Testgrpcode=d.TestGrpCode), "
		        +" (select b.codename from ldcode b where b.codetype='lhspbalancmode' and b.code=d.BalanceManner),"
		        +" ServDetail,"
		        +" d.ServTaskNo,d.ServCaseCode ,"
		        +" d.TaskExecNo ,d.ServItemNo,"
		        +" d.ServTaskCode,"
		        +" (select a.ServItemType from LHServItem a where a.ServItemNo=d.ServItemNo), "
		        +" (select case  when  a.TaskExecState='1' then '未执行' when a.TaskExecState='2' then '已执行'  else '' end from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo ),"
		        +" (select  a.TaskExecState from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo )"
		        +"  from   LHHmServBeManage d "
		        +" where d.ServTaskNo in ("+"<%=sqlServTaskNo%>"+") "
		        ;
		 turnPage.pageLineNum = 15;  
		 turnPage.queryModal(strsql, LHHmServBeManageGrid);
}
function initLHHmServBeManageGrid() 
{                               
	var iArray = new Array();
    
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名
      iArray[0][1]="30px";         		//列名
      iArray[0][3]=0;
       
      iArray[1]=new Array();
      iArray[1][0]="客户号";         		//列名
      iArray[1][1]="70px";         		//列名
      iArray[1][2]=20;        
      iArray[1][3]=0;         		//列名
    	        		//列名
    	
      iArray[2]=new Array(); 
		  iArray[2][0]="客户姓名";   
		  iArray[2][1]="65px";   
		  iArray[2][2]=20;        
		  iArray[2][3]=1;
	    
		  iArray[3]=new Array(); 
		  iArray[3][0]="预约机构代码";   
		  iArray[3][1]="0px";   
		  iArray[3][2]=20;        
		  iArray[3][3]=3;
	    
		  iArray[4]=new Array(); 
		  iArray[4][0]="预约机构名称";   
		  iArray[4][1]="140px";   
		  iArray[4][2]=20;        
		  iArray[4][3]=0;
	   
	    iArray[5]=new Array();
	    iArray[5][0]="服务预约日期";
	    iArray[5][1]="75px";         
	    iArray[5][2]=20;            
	    iArray[5][3]=0;     
	    
	    iArray[6]=new Array();
	    iArray[6][0]="预约时间";
	    iArray[6][1]="60px";         
	    iArray[6][2]=20;            
	    iArray[6][3]=0;     
	    
	    iArray[7]=new Array();
	    iArray[7][0]="健康体检套餐";
	    iArray[7][1]="120px";         
	    iArray[7][2]=20;            
	    iArray[7][3]=0; 
	    //iArray[6][14]=Date2;
		
	    iArray[8]=new Array(); 
	    iArray[8][0]="结算方式";   
	    iArray[8][1]="55px";   
	    iArray[8][2]=20;        
	    iArray[8][3]=0;
	    
	    iArray[9]=new Array(); 
	    iArray[9][0]="备注说明";   
	    iArray[9][1]="100px";   
	    iArray[9][2]=20;        
	    iArray[9][3]=0;
	    
	    iArray[10]=new Array(); 
	    iArray[10][0]="ServTaskNo";   
	    iArray[10][1]="0px";   
	    iArray[10][2]=20;        
	    iArray[10][3]=3;
	    
	    iArray[11]=new Array(); 
	    iArray[11][0]="ServCaseCode";   
	    iArray[11][1]="0px";   
	    iArray[11][2]=20;        
	    iArray[11][3]=3;
	    
	    iArray[12]=new Array(); 
	    iArray[12][0]="TaskExecNo";   
	    iArray[12][1]="0px";   
	    iArray[12][2]=20;        
	    iArray[12][3]=3;
	    
	    iArray[13]=new Array(); 
	    iArray[13][0]="ServItemNo";   
	    iArray[13][1]="0px";   
	    iArray[13][2]=20;        
	    iArray[13][3]=3;
	    
	    	  
	    iArray[14]=new Array(); 
	    iArray[14][0]="ServTaskCode";   
	    iArray[14][1]="0px";   
	    iArray[14][2]=20;        
	    iArray[14][3]=3;
	    
	    iArray[15]=new Array(); 
	    iArray[15][0]="服务项目序号";   
	    iArray[15][1]="90px";   
	    iArray[15][2]=20;        
	    iArray[15][3]=1;
	    
	    iArray[16]=new Array(); 
	    iArray[16][0]="执行状态";   
	    iArray[16][1]="70px";   
	    iArray[16][2]=20;        
	    iArray[16][3]=1;
	    
	    iArray[17]=new Array(); 
	    iArray[17][0]="taskExecState";   
	    iArray[17][1]="0px";   
	    iArray[17][2]=20;        
	    iArray[17][3]=3;
		 
		            
		
    	LHHmServBeManageGrid = new MulLineEnter( "fm" , "LHHmServBeManageGrid" ); 
    	//这些属性必须在loadMulLine前
    	
    	LHHmServBeManageGrid.mulLineCount = 0;   
    	LHHmServBeManageGrid.displayTitle = 1;
    	LHHmServBeManageGrid.hiddenPlus = 1;
    	LHHmServBeManageGrid.hiddenSubtraction = 1;
    	LHHmServBeManageGrid.canSel = 0;
    	LHHmServBeManageGrid.canChk = 1;
    	//LHHmServBeManageGrid.selBoxEventFuncName = "showOne";    
    	LHHmServBeManageGrid.chkBoxEventFuncName = "showTwo";            
    	
    	LHHmServBeManageGrid.loadMulLine(iArray);  
    	//这些操作必须在loadMulLine后面
    	//LHHmServBeManageGrid.setRowColData(1,1,"asdf");
	}
  	catch(ex) 
  	{
    	alert(ex);
	}
} 
function afterCodeSelect(codeName,Field)
{
	//alert(codeName);
	if(codeName == "hmexecstate")	
	{
		for(var row=0; row < LHHmServBeManageGrid.mulLineCount; row++)
		{
			var tChk =LHHmServBeManageGrid.getChkNo(row); 
			if(tChk == true)
			{
				LHHmServBeManageGrid.setRowColData(row,16,fm.all('ExecState_ch').value);	
				LHHmServBeManageGrid.setRowColData(row,17,fm.all('ExecState').value);
	  	}  
		}	
	}
} 

</script>