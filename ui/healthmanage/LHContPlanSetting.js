//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var prtNo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var StrSql;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput2() == false ) return false;
	if(checkDate() == false) return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
  
  fm.action="./LHContPlanSettingSave.jsp";
  fm.submit(); //提交
  
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	    return false;
	}
	else
    { 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		fms.all('saveButton').disabled = true; 
		if(fm.all('ServPlayType').value=="1H")
	  	{
	  		NQuery();
	  	}
	  	else if(fm.all('ServPlayType').value=="3")
	  	{
	  		UQuery();
	  	}
	  	else
	  	{
	  		if(allflag == "UCONT")
	  		{
	  			UQuery();
	  		}
	  		else
	  		{
	  			passQuery();
	  		}
	  	}
    }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHServPlan.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
   // showDiv(operateButton,"true"); 
   // showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
//  showDiv(operateButton,"false"); 
 // showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}  
function submitForm2()
{
	 var sqlServItemNo="select ServItemNo, ComID from LHItemFactorRela"
	                +" where  ServItemNo='"+fms.all('ServItemNo2').value+"'"
	                +" and  ComID='"+fms.all('ComID2').value+"'";
     //alert(sqlServItemNo);
     // alert(easyExecSql(sqlServItemNo));
     var arrResult = new Array(); 
     arrResult=easyExecSql(sqlServItemNo);
     if(arrResult!=""&&arrResult!="null"&&arrResult!=null)
     {
       var  ServItemNo=arrResult[0][0];
       var  ComID=arrResult[0][1];
     }

     if(fms.all('ServItemNo2').value==ServItemNo&&fms.all('ComID2').value==ComID)
     {
    	 alert("此标识的项目代码要素信息已经存在,不允许重复保存!");
    	 return false;
     }
     if((ServItemNo==""&&ComID=="")||(ServItemNo=="null"&&ComID=="null")||(ServItemNo==null&&ComID==null))
     {
	         if( verifyInput2() == false ) return false;
	         if(checkDate() == false) return false;
           var i = 0;
           var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
           var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
           showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
           fms.all('fmtransact2').value = "INSERT||MAIN";
           fms.submit(); //提交
     }
}   
function updateClick2()
{
  //下面增加相应的代码
  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改该记录吗?"))
  {
  	if(checkDate() == false) return false;
  	
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fms.all('fmtransact2').value = "UPDATE||MAIN"; 
  fms.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}       
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改该记录吗?"))
  {
  	if(checkDate() == false) return false;
  	
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.all('fmtransact').value = "UPDATE||MAIN"; //fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}  
        
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LHServPlanQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.all('fmtransact').value = "DELETE||MAIN"; //fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	 //alert(arrQueryResult[0][7]);//计划项目序号
	var arrResult = new Array();
	//alert(fm.all('ServPlanNo').value);
	if( arrQueryResult != null )
	{
		 var strSqlInit3 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
      	      			 +" a.servitemno, a.servitemtype,"
      	      			 +" (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end)"
      	      			 +" ,a.ServCaseType,"
      	      			 +"(select b.ServCaseCode from LHServCaseRela  b where b.ServItemNo=a.servitemno  ),"
      	      			 +"(select distinct c.ServCaseName from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ),"
      	      			 +"(select distinct c.ServCaseState from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ) "
      	      			 +" from lhservitem a where a.ServPlanNo = '"+fm.all('ServPlanNo').value+"'"
      	      					     ;
      	      		  //alert(strSqlInit3);alert(easyExecSql(strSqlInit3));
      	      	    turnPage.queryModal(strSqlInit3, LHServPlanGrid); 
	}
}               
function initServCase()
{    
	 var str = "";
	  if(allflag != "UCONT")//非标业务标志
	  {

	    	str = " and a.servitemcode not in ('006','015')";
	  }
	 var strSqlInit4 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
      	      			 +" a.servitemno, a.servitemtype,"
      	      			 +" (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end)"
      	      			 +" ,a.ServCaseType,"
      	      			 +"(select b.ServCaseCode from LHServCaseRela  b where b.ServItemNo=a.servitemno  ),"
      	      			 +"(select distinct c.ServCaseName from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ),"
      	      			 +"(select distinct c.ServCaseState from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ) "
      	      			 +" from lhservitem a where a.ServPlanNo = '"+fm.all('ServPlanNo').value+"'"
      	      			 +str
      	      			 ;
      	      		  //alert(strSqlInit4);alert(easyExecSql(strSqlInit4));
      	      	    turnPage.queryModal(strSqlInit4, LHServPlanGrid); 
}        
function toServTrace(arrQueryResult)
{
	if(fm.all('ComID').value == "")
	{
			alert("请先填写信息并保存!");
			return false;
	}
	if(LHServPlanGrid.getRowColData(0,3) == "")
	{
			alert("请先保存!");
			return false;
	}
	window.open("./LHServExeTrace.jsp?ServPlanNo="+fm.all('ServPlanNo').value,"","width=1024,height=748,top=0,left=0,resizable=yes,location=yes,directories=yes,menubar=yes,scrollbars=auto");
}

function checkDate()
{
	if(fm.all('StartDate').value != "" && fm.all('EndDate').value != "")
	{
			if(compareDate(fm.all('StartDate').value,fm.all('EndDate').value) == true)
			{
				alert("服务计划起始时间应早于服务计划结束时间 ");
				return false;
			}
	}
}

function getPersonContCopy()
{
			showInfo=window.open("LHServPlanMain.jsp?ServPlanNo="+fm.all('ServPlanNo').value);
}


      											 
      											 //选择类型为医院时的处理
function afterCodeSelect(codeName,Field)
{
	if(codeName=="lhservplanname")
	{
    	var strSql2 = " select servitemcode,servitemname,'',servitemsn,comid,'',''"
					 +" from LHHealthServPlan"
					 +" where ServPlanCode = '"+fm.all('ServPlanCode').value+"'"
					;      											
      	 turnPage.queryModal(strSql2, LHServPlanGrid); 
	}
}


function ScanFrameCtrl()
{
	
		
}
function showOne()
{
	var getSelNo = LHServPlanGrid.getSelNo();
	var ServItemCode = LHServPlanGrid.getRowColData(getSelNo-1, 1);
	var ComID = LHServPlanGrid.getRowColData(getSelNo-1, 5);
	var ServItemNo = LHServPlanGrid.getRowColData(getSelNo-1, 3);
	var EventType = LHServPlanGrid.getRowColData(getSelNo-1, 6);
	//alert(EventType);
  if(EventType=="2")
  {
  	divShowInfo.style.display='';
  }
  if(EventType=="1")
  {
  	divShowInfo.style.display='none';
  }
	fms.all('ServItemCode2').value=ServItemCode;
	fms.all('ComID2').value=ComID;
	fms.all('ServItemNo2').value=ServItemNo;
	
	//divLHItemPriceFactorGrid.style.display='';
	try
	{ 
		var mulSql2 = "select distinct ServItemNo "
		            +" from  LHItemFactorRela c "
		            +" where c.ServItemNo='"+ ServItemNo+"'";
		var arrResult2=new Array;
					      arrResult2=easyExecSql(mulSql2);
					    // alert(arrResult2);
		if(arrResult2==""||arrResult2=="null"||arrResult2==null)
		{	 
		   var mulSql3 = "select PriceFactorCode,PriceFactorName,'' "
		            +" from LHItemPriceFactor  "
		            +" where "
		            +" ServItemCode='"+ServItemCode+"' "
		            +" and " 
		            +" ComID='"+ComID+"'";
					      //alert(mulSql3);
					      var arrResult3=new Array;
					      arrResult3=easyExecSql(mulSql3);
					      //alert(arrResult3);
				if(arrResult3==""||arrResult3=="null"||arrResult3==null)
				{	    
					  
	           initLHItemPriceFactorGrid();
	      }
	      else
	      {
	      	  turnPage2.queryModal(mulSql3, LHItemPriceFactorGrid);
	      		
	      }
	   }
	   else 
	   {
	   	 
	   	 var mulSql4 = "select distinct a.PriceFactorCode,  a.PriceFactorName ,c.PriceFactorCon  "
		            +"  from LHItemPriceFactor a ,LHItemFactorRela c   "
		            +" where a.ServItemCode=c.ServItemCode and a.ComID=c.ComID and a.PriceFactorCode=c.PriceFactorCode "
		            +"and  c.ServItemNo='"+ ServItemNo+"'"
		            +" and c.ComID='"+ComID+"'"
		            +"and c.ServItemCode='"+ServItemCode+"'";
					      //alert(mulSql4);
					      var arrResult4=new Array;
					      arrResult4=easyExecSql(mulSql4);
					      //alert(arrResult4);
				if(arrResult4==""||arrResult4=="null"||arrResult4==null)
				{	      
	         initLHItemPriceFactorGrid();
	      }
	      else
	      {
	      	turnPage.queryModal(mulSql4, LHItemPriceFactorGrid);
	      }

	   }
	}
	catch(ex)
	{
		alert("LHItemPriceFactorInputInit.jsp-->传输入信息Info中发生异常:初始化界面错误!");
	}
}

function ServSetSucced()
{
	   	var rowNum=LHServPlanGrid.mulLineCount ; //行数 
	   	var k;	
	    for( var i=0; i<rowNum; i++)
	    {
	    		 k=(LHServPlanGrid.getRowColData(i,7));
	    		 if(k!=""&&k!="null"&&k!=null)
	    		 {	
	    		 }
	    		 else
	    		 {
	    		 	  alert("所有事件信息未设置完成!");
	    		 	  return false;
	    		 }
      }
      updateClick3();
}
function updateClick3()
{
  //下面增加相应的代码
  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改契约事件状态吗?"))
  {
  	 if(checkDate() == false) return false;
  	
     var i = 0;
     var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
     
     //showSubmitFrame(mDebug);
     fm.all('fmtransact').value = "UPDATE||MAIN"; //fm.fmtransact.value = "UPDATE||MAIN";
     fm.all('CaseState').value="3";    
     fm.action="./LHServPlanUpSave.jsp";
     fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}  

function afterSubmitUp(FlagStr,content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    	return false;
	}
	else
	{ 
    	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
  	fms.all('QieSuccess').disabled = true; 	
	
}


function AddNewHmPlan()
{  var getSelNo = LHServPlanGrid.getSelNo();
	
	if(getSelNo==""||getSelNo=="null"||getSelNo==null)
	{
    alert("请选择一条记录数据！");
	}
	else
  {
  	 
  	 	  var caseCode = LHServPlanGrid.getRowColData(getSelNo-1, 7);
	  //alert(caseCode);
	  if(caseCode==""||caseCode=="null"||caseCode==null)
	  {
	  
	     var ServItemCode = LHServPlanGrid.getRowColData(getSelNo-1, 1);
	     var ServItemNo = LHServPlanGrid.getRowColData(getSelNo-1, 3);
	     var ServPlanNo=fm.all('ServPlanNo').value;
	     //alert(ServPlanNo);
	     window.open("./LHAddNewHmPlanMain.jsp?ServItemNo="+ServItemNo+"&ServItemCode="+ServItemCode+"&ServPlanNo="+ServPlanNo+"","新增服务事件设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
  	}
  	else
  	{
  		 alert("此项目编号的事件编号已经存在，不充许重新生成!");
  	}
  }
}
function AddNewQuery()
{
	  var getSelNo = LHServPlanGrid.getSelNo();
	  var getSelNo = LHServPlanGrid.getSelNo();
	
	if(getSelNo==""||getSelNo=="null"||getSelNo==null)
	{
    alert("请选择一条记录数据！");
	}
	else
  {
	  var caseCode = LHServPlanGrid.getRowColData(getSelNo-1, 7);
	  //alert(caseCode);
	  if(caseCode==""||caseCode=="null"||caseCode==null)
	  {
	     var getSelNo = LHServPlanGrid.getSelNo();
	     var ServItemNo = LHServPlanGrid.getRowColData(getSelNo-1, 3);
	     var ServItemCode = LHServPlanGrid.getRowColData(getSelNo-1, 1);
	     var ServPlanNo=fm.all('ServPlanNo').value;
	     //alert(ServPlanNo);
	     window.open("./LHAddNewHmPlanQuery.html?ServItemNo="+ServItemNo+"&ServItemCode="+ServItemCode+"&ServPlanNo="+ServPlanNo+"","新增服务事件查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	  }
  	else
  	{
  		 alert("此项目编号的事件编号已经存在，不充许重新查询!");
  	}
  }
}