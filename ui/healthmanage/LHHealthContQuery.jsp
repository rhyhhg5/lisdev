<%
//程序名称：LHHealthContQuery.jsp
//程序功能：功能描述
//创建日期：2006-03-6 10:40:02
//创建人  ：
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHHealthContQuery.js"></SCRIPT> 
  <%@include file="LHHealthContQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form action="./LHHealthContQuerySave.jsp" method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
	<table class=common border=0 width=100%>
	    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divHealthContGrid1" style= "display: ''">   
  	 
<table  class=common  align='center'>
  <TR  class= common>
    <TD  class= title>
		保单类型
    </TD>
    <TD  class= input>
     <Input class= 'codename' style="width:50px" name=ContType><Input class= 'codeno'  style="width:110px" name=ContTypeName ondblclick="return showCodeList('lhconttype',[this,ContType],[1,0],null,null,null,null,160);" >
    </TD>
    <TD  class= title>
       客户号码
     </TD>
     <TD  class= input>
       <Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=24" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
     </TD>
     <TD  class= title>
       客户姓名
     </TD>
     <TD  class= input>
       <Input class= 'common' name=CustomerName  >
     </TD>
  </TR>
  <TR class=common>
	<TD  class= title>
		保单号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContNo  >
    </TD>
	<TD class=title>
    	保单生效时间(起始)
    </TD>
    <TD class=input>
    	<Input class= 'CoolDatePicker' dateFormat="Short"  name=LHContStart verify="保单生效时间(起始)|DATE">
    </TD>
     <TD class=title>
    	保单生效时间(终止)
    </TD>
    <TD class=input>
    	<Input class= 'CoolDatePicker' dateFormat="Short"  name=LHContEnd verify="保单生效时间(终止)|DATE">
    </TD>
  <TR>
	<!--TD  class= title>
		保单维护状态
		<Input type=hidden class= 'codename' style="width:50px" name=ContStateName><Input type=hidden class= 'codeno'  style="width:110px" name=ContState ondblclick="return showCodeList('lhmanagecont',[this,ContStateName],[1,0],null,null,null,null,160);" >
    <TD  class= input-->
		

	<!--TD  class= title>保单维护状态</TD-->
	<!--TD  class= input--><Input type=hidden name=ContStateName id=ContStateName><Input type=hidden name=ContState id=ContState ><!--ondblclick="return showCodeList('lhmanagecont',[this,ContStateName],[1,0],null,null,null,null,160);" ></TD-->
	
	<TD id=aa1 class= title>契约事件状态</TD>
	<TD id=aa2 class= input><Input class= 'codename' style="width:50px" readonly name=CaseStateName><Input class= 'codeno' readonly style="width:125px" name=CaseState ondblclick="return showCodeList('lhcasestate',[this,CaseStateName],[1,0],null,null,null,null,160);" ></TD>
	
	<TD  class= title></TD><TD class= input></TD><TD  class= title></TD><TD class= input></TD>
  </TR>
  </table>
	<table  class= common align='center' style="display:'none'">
		<Input  class= 'common' name=Operator >
		<Input  class= 'common' name=MakeDate >
		<Input  class= 'common' name=MakeTime >
		<Input  class= 'common' name=ModifyDate >
		<Input  class= 'common' name=ModifyTime >
		<Input  class= 'common' name=ClassInfoCode >
		
	</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查  询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">		
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHealthCont1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divHealthCont1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanHealthContGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage3.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage3.lastPage();">
	</Div>
	
	<br><br>
	<Div id="" style="display:''">
		<table class=common>
			<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanContCustomerGrid">
	  				</span> 
			    </td>
			</tr>
		</table>
	</div>    
	<Div id= "divPage" align=center style= "display: 'none' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</Div>
	<table class=common border=0 width=100%>
		<TR class=common>  
			<TD  class=common>
				<!--INPUT VALUE="添加到我的任务队列"   TYPE=button   class=cssbutton onclick="submitForm();"-->		
				<INPUT VALUE="服务信息设置"   TYPE=button   class=cssbutton onclick="toServPlanInfo();">
				<INPUT VALUE="服务事件设置"   TYPE=button   class=cssbutton onclick="toServCase();">
			</TD>
	    </TR>
	</table>
	    
    <!--table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divOperatorCont);">
  		</td>
  		<td class=titleImg>
  			 业务员保单信息
  		</td>
  	</tr>
  </table>

	<Div id="divOperatorCont" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanOperatorContGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	<table class=common border=0 width=100%>
		<TR class=common>  
			<TD  class=common>
				<INPUT VALUE="开始录入"   TYPE=button   class=cssbutton onclick="submitForm();">		
			</TD>
	    </TR>
	</table-->
		
		<INPUT type=hidden name=serialNo>
		<INPUT type=hidden id="fmtransact" name="fmtransact">
		<INPUT type=hidden name=Missionid>
		<INPUT type=hidden name=SubMissionid>
		<Input type=hidden name=ContType_ForQ >
	</form>	   
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
               
</body>

</html>
 






