<%
//程序名称：LHCustomTestInput.jsp
//程序功能：
//创建日期：2005-01-17 18:32:42
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     String Customerno = request.getParameter("Customerno");
     String Healthno = request.getParameter("Healthno");
%> 

<!--用户校验类-->
<script language="JavaScript">
function initCustomTestInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
{
	var iArray = new Array(); //数组放置各个列
	try
    {
    	iArray[0]=new Array();
    	iArray[0][0]="序号";  					//列名（序号列，第1列）
    	iArray[0][1]="30px";  					//列宽
    	iArray[0][2]=10;      					//列最大值
    	iArray[0][3]=0;   							//1表示允许该列输入，0表示只读且不响应Tab键
    	               									// 2 表示为容许输入且颜色加深.
    	iArray[1]=new Array();
    	iArray[1][0]="检查项目名称";  	//列名（第2列）
    	iArray[1][1]="81px";  	  			//列宽
    	iArray[1][2]=10;        				//列最大值
    	iArray[1][3]=2;          				//是否允许输入,1表示允许，0表示不允许
    	iArray[1][4]="lhtestname";
    	iArray[1][5]="1|4|5";    				//引用代码对应第几列，'|'为分割符
    	iArray[1][6]="0|1|2";    				//上面的列中放置引用代码中第几位值
    	iArray[1][9]="检查项目名称|len<=120";
    	iArray[1][15]="MedicaItemName";
    	iArray[1][17]="1";  
    	iArray[1][19]=1 ;
    	
    	iArray[2]=new Array();                   			
		iArray[2][0]="是否异常";  			//列名（第2列）                                                 
    	iArray[2][1]="40px";  	  		  		//列宽                                                    
    	iArray[2][2]=1;        					//列最大值                                                  
    	iArray[2][3]=2;
    	iArray[2][10]="Isnormal";
    	iArray[2][11]= "0|^正常|1^异常|2";  	//虚拟数据源
    	iArray[2][14]= "正常";  				//虚拟数据源
    	iArray[2][19]=1;               			//强制刷新
    	
    	iArray[3]=new Array();
    	iArray[3][0]="检查结果";  		  		//列名（第2列）
    	iArray[3][1]="90px";  	  		  		//列宽
    	iArray[3][2]=130;        			  	//列最大值
    	iArray[3][3]=1;     
    	iArray[3][7]="inputTestResult"; 		//你写的JS函数名，不加扩号                   
    	
    	iArray[4]=new Array();                                          
    	iArray[4][0]="检查项目代码";   			//列名（第2列）                   
    	iArray[4][1]="0px";  	 			 	//列宽                                  
    	iArray[4][2]=0;       		 		 	//列最大值                              
    	iArray[4][3]=3;          			 	//是否允许输入,1表示允许，0表示不允许  
    	
    	iArray[5]=new Array();                             
    	iArray[5][0]="标准单位";        		//列名（第2列）          
    	iArray[5][1]="70px";  	        		//列宽                   
    	iArray[5][2]=10;        	      		//列最大值                 
    	iArray[5][3]=0;   
    	                                           
		iArray[6]=new Array();                                            
    	iArray[6][0]="流水号";   				//列名（第2列）                           
    	iArray[6][1]="0px";  	   				//列宽                                   
    	iArray[6][2]=10;         				//列最大值                                
    	iArray[6][3]=3;          				//是否允许输入,1表示允许，0表示不允许   
    	
    	iArray[7]=new Array();                             
    	iArray[7][0]="3num";        			//列名（第2列）          
    	iArray[7][1]="0px";  	        		//列宽                   
    	iArray[7][2]=10;        	      		//列最大值
    	iArray[7][3]=3;
    	                   
    	iArray[8]=new Array();                             
    	iArray[8][0]="正常值";        			//列名（第2列）          
    	iArray[8][1]="70px";  	        		//列宽                   
    	iArray[8][2]=10;        	      		//列最大值
    	iArray[8][3]=0;
    	                                          
    	//生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
    	CustomTestInfoGrid= new MulLineEnter( "fm" , "CustomTestInfoGrid" ); 
    	//设置属性区 (需要其它特性，在此设置其它属性)
    	CustomTestInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
    	CustomTestInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0 隐藏标题       
    	//CustomTestInfoGrid.canSel =1; 
    	CustomTestInfoGrid.canChk =1;      
    	      
    	//对象初始化区：调用对象初始化方法，属性必须在此前设置
    	CustomTestInfoGrid.loadMulLine(iArray); 
	}
	catch(ex){ alert(ex); }
}
    
function initTestResultGrid()    //函数名为init+MulLine的对象名ObjGrid
{
	var iArray = new Array(); //数组放置各个列
	try
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";  					//列名（序号列，第1列）
    	iArray[0][1]="30px";  					//列宽
    	iArray[0][2]=10;      					//列最大值
    	iArray[0][3]=0;   							//1表示允许该列输入，0表示只读且不响应Tab键
    	               									// 2 表示为容许输入且颜色加深.
    	                     
    	iArray[1]=new Array();
    	iArray[1][0]="诊断疾病代码";  			//列名（第2列）
    	iArray[1][1]="150px";  	  			//列宽
    	iArray[1][2]=10;        				//列最大值
    	iArray[1][3]=1;          				//是否允许输入,1表示允许，0表示不允许
    	
    	iArray[2]=new Array();
    	iArray[2][0]="诊断疾病名称";  		  //列名（第2列）
    	iArray[2][1]="200px";  	  		  //列宽
    	iArray[2][2]=10;        			  //列最大值
    	iArray[2][3]=2;     
    	iArray[2][4]="lddiseasename";
    	iArray[2][5]="2|1";    				//引用代码对应第几列，'|'为分割符
    	iArray[2][6]="1|0";    				//上面的列中放置引用代码中第几位值
    	iArray[2][9]="诊断疾病代码/名称|len<=120";
    	iArray[2][15]="ICDName";
    	iArray[2][17]="2";  
    	iArray[2][18]=400;
    	iArray[2][19]="1";  
    	
    	iArray[4]=new Array();                                          
    	iArray[4][0]="xxx";   					//列名（第2列）                   
    	iArray[4][1]="0px";  	 			 		//列宽                                  
    	iArray[4][2]=0;       		 		 	//列最大值                              
    	iArray[4][3]=3;          			 	//是否允许输入,1表示允许，0表示不允许  
    	
    	iArray[3]=new Array();                             
    	iArray[3][0]="department";         //列名（第2列）          
    	iArray[3][1]="0px";  	       		 //列宽                   
    	iArray[3][2]=10;        	      	 //列最大值                 
    	iArray[3][3]=0;   
    	
    	//生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
    	TestResultGrid= new MulLineEnter( "fm" , "TestResultGrid" ); 
    	//设置属性区 (需要其它特性，在此设置其它属性)
    	TestResultGrid.mulLineCount = 0 ;  //行属性：设置行数=3    
    	TestResultGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0 隐藏标题       
    	//TestResultGrid.canSel =1; 

    	//对象初始化区：调用对象初始化方法，属性必须在此前设置
    	TestResultGrid.loadMulLine(iArray); 
	}
	catch(ex){ alert(ex); }
}
</script>  
                  
<script language="JavaScript">
function initInpBox()
{ 
	try
	{
	    fm.all('CustomerNo').value = "";
	    fm.all('CustomerName').readOnly=true;
	    fm.all('CustomerName').value = "" ;   
	    fm.all('HospitCode').value = "";     
	    fm.all('HospitName').value=""; 
	    fm.all('InHospitDate').value = "";
	    fm.all('TestModeCode').value= "";
	    fm.all('TestMode').value= "";
		fm.all('DoctNo').value = "";
	    fm.all('DoctName').value = "";
	    fm.all('TestFee').value = "";
	    fm.all('Operator').value = "";
	    fm.all('MakeDate').value = "";
	    fm.all('MakeTime').value = "";
	    fm.all('ModifyDate').value = "";
	    fm.all('ModifyTime').value = "";
	    fm.all('iscomefromquery').value="0";
	    fm.all('MainItem').value="";
	    fm.all('FeeNo').value="0";
	}
	catch(ex)
	{
		alert("在LHCustomTestInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}
function initSelBox()
{  
	try                 
	{     
	}
	catch(ex)
	{
		alert("在LHCustomTestInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
	}
}                                        
function initForm()
{
	try
	{
	    initInpBox();
	    initSelBox();    
	    initCustomTestInfoGrid();
	    initTestResultGrid();
	    var flag = "<%=Customerno%>";
	    //alert(flag);
	    if(flag!=""&&flag!="null"&&flag!=null)
	    {
			fm.all('querybutton').disabled=true;
	        fm.all('deleteButton').disabled=true;
	        fm.all('modifyButton').disabled=true;
	        fm.all('saveButton').disabled=true;
	        divupfile.style.display='none';
	        upfm.style.display='none';
	        getCustomerNo();
    	}
	    else
	    {
	    	passQuery();
	    } 
	}
	catch(re)
	{
    	alert("LHCustomTestInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}
function getCustomerNo()
{
	var strSQL ="select distinct a.CustomerNo,b.name,a.InHospitDate,a.HospitCode,'',a.InHospitMode," 
       		   +" a.MakeDate,a.MakeTime,"
       		   +"a.InHospitNo from LHCustomInHospital a,ldperson b "
       		   +" where trim(a.CustomerNo)=trim(b.CustomerNo) "  
       		   +" and a.CustomerNo='"+ "<%=Customerno %>" +"'"
       		   +" and a.Inhospitno = '"+ "<%=Healthno%>" +"'"
       		   ;  
	arrResult =easyExecSql(strSQL);
	if(arrResult!=null)
	{
		var tempcustomerno=arrResult[0][0];
		var tempinhospitno=arrResult[0][2];
	    fm.all('CustomerNo').value= arrResult[0][0];fm.all('CustomerNo').readOnly = true;
	    fm.all('CustomerName').value= arrResult[0][1];
	
	    fm.all('InHospitDate').value= arrResult[0][2];
	    fm.all('HospitCode').value= arrResult[0][3];
	    if(arrResult[0][3] != "")
	    {    
	    	fm.all('HospitName').value= easyExecSql("select hospitname from ldhospital where hospitcode = '"+arrResult[0][3]+"'");
	    }
	    fm.all('TestModeCode').value=arrResult[0][5]; 
	    fm.all('TestMode').value= easyExecSql("select codename from ldcode where codetype='inhospitmode' and code ='"+arrResult[0][5]+"'");  
	    fm.all('MakeDate').value= arrResult[0][6];
	    fm.all('MakeTime').value= arrResult[0][7];
	    fm.all('InHospitNo').value =  '"+ "<%=Healthno%>" +"';
	    
	    fm.all('TestFee').value = easyExecSql( "select Feeamount from LHFeeInfo where Customerno = '"+arrResult[0][0]+"' and InhospitNo ='"+ "<%=Healthno%>" +"' and Feecode = '5'" );
		if(fm.all('TestFee').value == "null" || fm.all('TestFee').value == null)
		{
			fm.all('TestFee').value = "";
		}
      							
      	var MulSQL =	 " select (select distinct c.Medicaitemname from lhcountrmedicaitem c where c.Medicaitemcode = a.MedicaItemCode)  aaa, "
             +" a.IsNormal,a.TestResult, a.Medicaitemcode,(select distinct c.standardmeasureunit from lhcountrmedicaitem c where c.Medicaitemcode = a.MedicaItemCode ), a.testno,"
             +" char( rownumber() over() ),"
             +" (select distinct c.Nomalvalue from lhcountrmedicaitem c where c.Medicaitemcode = a.MedicaItemCode ) "
             +"  from LHCustomTest a "
             +" where a.CustomerNo = '"+"<%=Customerno%>" +"' and a.InHospitNo = '"+"<%=Healthno%>"+"' order by a.Medicaitemcode  "
             ;
			//alert(MulSQL); 
			//alert(easyExecSql(MulSQL));  				
		turnPage.pageLineNum = 100;  
		turnPage.queryModal(MulSQL, CustomTestInfoGrid); 

		var SQL2 = " select a.ICDCode,(select b.ICDName from LDDisease b where b.ICDCode = a.ICDCode),a.SortDepart"
			 	+ " from LCPENoticeResult a where a.ProposalContNo = '"+arrResult[0][0]
			 	+ "' and a.PrtSeq = '"+ "<%=Healthno%>" +"'"
			 	;

		turnPage.queryModal(SQL2, TestResultGrid); 
		fm.all('iscomefromquery').value="1"; 
		fm.all('MedicaItemGroup').value="";
	}  
}
</script>
