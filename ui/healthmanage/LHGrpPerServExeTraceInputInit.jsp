<%
//程序名称：LHGrpPerServExeTraceInput.jsp
//程序功能：
//创建日期：2006-03-09 14:31:19
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
   // fm.all('ServPlanNo').value = "";
    fm.all('CustomerNo').value = "";
    fm.all('Name').value = "";
    fm.all('ServItemCode').value = "";
    fm.all('servitemname').value = "";
//    fm.all('ServDesc').value = "";
//    fm.all('ManageCom').value = "";
//    fm.all('Operator').value = "";
//    fm.all('MakeDate').value = "";
//    fm.all('MakeTime').value = "";
//    fm.all('ModifyDate').value = "";
//    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHGrpPerServExeTraceInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {}
  catch(ex)
  {
    alert("在LHGrpPerServExeTraceInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox(); 
    initLHGrpPerServExeTraceGrid();
  }
  catch(re)
  {
    alert("LHGrpPerServExeTraceInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHGrpPerServExeTraceGrid;
function initLHGrpPerServExeTraceGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="客户号";   
	  iArray[1][1]="40px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="客户姓名";   
	  iArray[2][1]="60px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
    
    iArray[3]=new Array(); 
	  iArray[3][0]="标准服务项目代码";   
	  iArray[3][1]="70px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=0;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="标准服务项目名称";   
	  iArray[4][1]="120px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	  
	  iArray[5]=new Array(); 
	  iArray[5][0]="团体服务项目号码";   
	  iArray[5][1]="0px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=3;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="服务项目序号";   
	  iArray[6][1]="60px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=0;
	    
	  iArray[7]=new Array(); 
	  iArray[7][0]="服务执行状态";   
	  iArray[7][1]="50px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=2;
	  iArray[7][4]="lhservplanexestate";
    iArray[7][5]="7";     //引用代码对应第几列，'|'为分割符
    iArray[7][6]="0";     //上面的列中放置引用代码中第几位值
    iArray[7][9]="服务执行状态|len<=10|NOTNULL";
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="服务执行时间";   
	  iArray[8][1]="50px";   
	  iArray[8][2]=20;        
	  iArray[8][3]=1;
	  iArray[8][9]="服务执行时间|NOTNULL";
	  
	  iArray[9]=new Array(); 
	  iArray[9][0]="服务详细描述";   
	  iArray[9][1]="50px";   
	  iArray[9][2]=20;        
	  iArray[9][3]=1;
	  iArray[9][9]="服务详细描述|NOTNULL";
	         
	  iArray[10]=new Array(); 
	  iArray[10][0]="入机日期";   
	  iArray[10][1]="0px";   
	  iArray[10][2]=20;        
	  iArray[10][3]=3;
	  
	  iArray[11]=new Array(); 
	  iArray[11][0]="入机时间";   
	  iArray[11][1]="0px";   
	  iArray[11][2]=20;        
	  iArray[11][3]=3;
	      
	  iArray[12]=new Array(); 
	  iArray[12][0]="客户号";   
	  iArray[12][1]="0px";   
	  iArray[12][2]=20;        
	  iArray[12][3]=3;
	         
	  iArray[13]=new Array(); 
	  iArray[13][0]="流水号";   
	  iArray[13][1]="0px";   
	  iArray[13][2]=20;        
	  iArray[13][3]=3;
	  
	  iArray[14]=new Array(); 
	  iArray[14][0]="标准服务项目代码";   
	  iArray[14][1]="0px";   
	  iArray[14][2]=20;        
	  iArray[14][3]=3;	
    LHGrpPerServExeTraceGrid = new MulLineEnter( "fm" , "LHGrpPerServExeTraceGrid" ); 
    //这些属性必须在loadMulLine前

    LHGrpPerServExeTraceGrid.mulLineCount = 1;   
    LHGrpPerServExeTraceGrid.displayTitle = 1;
    LHGrpPerServExeTraceGrid.hiddenPlus = 1;
    LHGrpPerServExeTraceGrid.hiddenSubtraction = 0;
    LHGrpPerServExeTraceGrid.canSel = 0;
    LHGrpPerServExeTraceGrid.canChk = 0;
    //LHServItemGrid.selBoxEventFuncName = "showOne";

    LHGrpPerServExeTraceGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHServItemGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}


</script>
