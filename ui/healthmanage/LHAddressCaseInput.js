//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

function AddressCaseQuery() //通讯事件查询
{
	if(verifyInput() == false){
  	return false;
  }
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
  fm.tFlag.value="address";
  fm.submit();
}
function MobilesServQuery() //短信服务查询
{
	if(verifyInput() == false){
  	return false;
  }
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
  fm.tFlag.value="mobile";
  fm.submit();
}
function ConsultServQuery() //个人咨询服务查询
{
	if(verifyInput() == false){
  	return false;
  }
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
  fm.tFlag.value="zixun";
  fm.submit();
}
function LectureCourseQuery() //健康讲座服务查询
{
	if(verifyInput() == false){
  	return false;
  }
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
  fm.tFlag.value="jiangzuo";
  fm.submit();
}
function StartRun() //健管服务计划流转
{
	if(verifyInput() == false){
  	return false;
  }
  window.open("../healthmanage/LhServPlanTask.jsp?StartDate="+fm.StartDate.value+"&EndDate="+fm.EndDate.value+"&ManageCom="+fm.ManageCom.value);
}
function closeShowInfo(){
	showInfo.close();
}