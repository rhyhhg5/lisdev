<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHEvalueMgtSave.jsp
//程序功能：
//创建日期：2006-10-16 10:44:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHEvalueMgtUI tLHEvalueMgtUI   = new LHEvalueMgtUI();
  
  LHEvalueExpMainSet tLHEvalueExpMainSet = new LHEvalueExpMainSet();		//健管评估导出中间主表
  LHEvalueExpDetailSet tLHEvalueExpDetailSet = new LHEvalueExpDetailSet();		//健管评估导出中间明细表
    
    String tTaskExecNo[] = request.getParameterValues("LHEvalueMgtGrid10");           //任务实施号码流水号
    String tServTaskNo[] = request.getParameterValues("LHEvalueMgtGrid8");           //服务任务编号
    String tServCaseCode[] = request.getParameterValues("LHEvalueMgtGrid9");					//服务事件号  
    String tServTaskCode[] = request.getParameterValues("LHEvalueMgtGrid12");           //服务任务代码
    String tServItemNo[] = request.getParameterValues("LHEvalueMgtGrid11");					//服务项目序号
    String tCustomerNo[] = request.getParameterValues("LHEvalueMgtGrid1");					//客户号 
    String tCusEvalCode[] = request.getParameterValues("LHEvalueMgtGrid13");					//客户号  	  	 
     
    String tChk[] = request.getParameterValues("InpLHEvalueMgtGridChk"); //参数格式=” Inp+MulLine对象名+Chk”     	 
     
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String HidCusNo="";
  String StandCode="";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
	 //System.out.println(" KKKKKKKKKKKKKKKKKKKKKKKKK "+transact);
	  TransferData StartEnd = new TransferData();
    String Begin = request.getParameter("StartDate");
  	String End = request.getParameter("EndDate");
	 	String sd = AgentPubFun.formatDate(Begin, "yyyyMMdd");
		String ed = AgentPubFun.formatDate(End, "yyyyMMdd");
  	
  	if(sd.compareTo(ed) > 0)
		{
			FlagStr = "Fail";
			Content = "操作失败，原因是:终止日期比起始日期早";
		}
   int LHEvalueExpMainCount = 0;
   System.out.println("R R  "+tTaskExecNo.length);
	 if(tTaskExecNo != null)
	 {	
		    LHEvalueExpMainCount = tTaskExecNo.length;
	 }	
	  //System.out.println(" LHEvalueExpMainCount is : "+LHEvalueExpMainCount);
    for(int j = 0; j < LHEvalueExpMainCount; j++)
	  {
	    
        StartEnd.setNameAndValue("Begin",Begin);
	  	  StartEnd.setNameAndValue("End",End);
   
		    LHEvalueExpMainSchema tLHEvalueExpMainSchema = new LHEvalueExpMainSchema();
		    
		     tLHEvalueExpMainSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		     tLHEvalueExpMainSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		     
		     tLHEvalueExpMainSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		     tLHEvalueExpMainSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		     tLHEvalueExpMainSchema.setServItemNo(tServItemNo[j]);	//项目号码
		     tLHEvalueExpMainSchema.setCustomerNo(tCustomerNo[j]);	//客户号  
		     tLHEvalueExpMainSchema.setEvalType(request.getParameter("EvalueType"));//问卷类型
         //tLHEvalueExpMainSchema.setServPlanNo(request.getParameter("CusRegistNo"));//将客户登记号码存为服务计划号码中
           	     
         if(transact.equals("INSERT||MAIN"))
         {
             tLHEvalueExpMainSet.add(tLHEvalueExpMainSchema);   
         }  
   
    }
                              
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
    tVData.add(tLHEvalueExpMainSet);
  	//tVData.add(tLHEvalueExpDetailSet);
  	tVData.add(tG);
  	tVData.addElement(StartEnd);
    tLHEvalueMgtUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHEvalueMgtUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 数据导出成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	  var transact = "<%=transact%>";
	      parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>