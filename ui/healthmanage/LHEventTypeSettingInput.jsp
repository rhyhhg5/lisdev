<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHEventTypeSettingInput.jsp
//程序功能：
//创建日期：2006-07-04 10:22:39
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHEventTypeSettingInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHEventTypeSettingInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LHHealthServItemSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServSetting);">
    		</td>
    		 <td class= titleImg>
        		 事件启动自动设置
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHServSetting" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
  	<TD  class= title>
		   服务事件类型
    </TD>
    <TD  class= input>
     <Input class= 'codename' style="width:50px" name=EventType><Input class= 'codeno'  style="width:110px" name=EventTypeName ondblclick="return showCodeList('eventtype',[this,EventType],[1,0],null,null,null,null,160);" >
    </TD>
    <TD  class= title>
      保单所属机构标识
    </TD>
     <TD  class= input>
    	<Input  type=hidden name= ComID>
      <Input class= 'code' name=ComIDName style="width:160px"  verify="保单所属机构标识|NOTNULL&len<=50" ondblclick="showCodeList('hmhospitalmng',[this,ComID],[0,1],null,fm.ComIDName.value,'ComIDName','',200);"  codeClear(ComIDName,ComID);" >
    </TD>
    <TD  class= title>
      服务项目代码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ServItemNO   verify="服务项目代码|notnull&len<=50" ondblclick="showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemName.value,'ServItemName','',200);"  codeClear(ServItemName,ServItemNO);" >
    </TD>

  </TR>
  <TR  class= common>
    <TD  class= title>
      服务项目名称
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ServItemName  verify="服务项目名称|notnull&len<=50" ondblclick="showCodeList('hmservitemcode',[this,ServItemNO],[1,0],null,fm.ServItemName.value,'ServItemName','',200);"  codeClear(ServItemName,ServItemNO);" >
    </TD>

  </TR>
</table>
    </Div>
     <Div id="divShowServTaskInfo" style="display:''">
	 <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHQueryDetailGrid);">
    	 	</td>
    	 <td class= titleImg>
        	 服务任务信息列表
       	</td>   		 		 
    	</tr>
    </table>
  <Div id="divLHQueryDetailGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHQueryDetailGrid">
					</span> 
		    </td>
			</tr>
		</table>
	</div>
</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
