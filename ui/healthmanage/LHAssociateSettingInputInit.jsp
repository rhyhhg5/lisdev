<%
//程序名称：LHAssociateSettingInputInit.jsp
//程序功能：关联设置信息管理(初始化页面)
//创建日期：2006-03-01 9:43:20
//创建人  ：GuoLiying
//CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>  
<%
        String no = request.getParameter("ContraNo");
        String itemno = request.getParameter("ContraItemNo");
        String groupunit=request.getParameter("groupunit");
        System.out.println("--------------"+no);
        System.out.println("--------------"+itemno);
        String no2 = request.getParameter("ContraNo2");
        String itemno2 = request.getParameter("ContraItemNo2");
%>                             
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('ContraNo').value = "";
    fm.all('ContraName').value = "";
    fm.all('HospitName').value = "";
    fm.all('ContraType_ch').value = "";
    fm.all('ContraNowName_ch').value = "";
    fm.all('ContraNowState_ch').value = "";
    fm.all('ContraItemNo').value="<%=itemno%>";
	  fm.all('ContraItemNo2').value="<%=no2%>";
	  fm.all('ContraNo2').value = "<%=itemno2%>";

  }
  catch(ex)
  {
    alert("在LHAssociateSettingInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
function initForm()
{
  try
  {
    initInpBox();
    fm.all('querybutton').disabled=true;
    fm.all('deleteButton').disabled=true;
    fm.all('modifyButton').disabled=true;
    fm.all('saveButton').disabled=true;
    var flag = "<%=groupunit%>";
   // alert(flag);
    if(flag=="4")//团体合同进入此页面
    {
      getFirstPageNo();
    }
    if(flag=="3")//个人合同进入此页面
    {
   	  getFirstPageNo2();
    }
    if(flag=="8")//医疗机构检索进入此页面
    {
    	 fm.all('subLine').disabled=true;
       fm.all('getAssociateSetting').disabled=true;
       getFirstPageNo();
    }
   // initLHContraAssoSettingGrid();
  }
  catch(re)
  {
    alert("LHAssociateSettingInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}  
function getFirstPageNo()
{  
  try                 
  {

     		    var sql="	select a.ContraNo, a.ContraName,b.HospitName, "
						 +" (select c.Codename from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
						 +" (select d.DutyItemName from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode), "
				     +" ( case when a.ContraState = '1' then '有效' else '无效' end),	"	 
				     +"(select d.dutyitemcode from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode),"
				     +"  e.ContraItemNo "
						 +"	from   LHGroupcont a,LDhospital b, lhcontitem e "
						 +" where  a.ContraNo='"+ "<%=no%>" +"' "
						 +"	and    a.hospitcode = b.hospitcode "
						 +" and    e.ContraItemNo='"+ "<%=itemno%>" +"' "
					   +" and    a.ContraNo=e.ContraNo" 
				     
				//     var arrResult = new Array();
				     var arrResult = easyExecSql(sql);
				     
				     	fm.all('ContraNo').value				  = arrResult[0][0];
				     	fm.all('ContraName').value			  = arrResult[0][1];
				      fm.all('HospitCode').value			  = arrResult[0][2];
				      fm.all('HospitName').value        = fm.all('HospitCode').value;
				      fm.all('ContraType_ch').value 		= arrResult[0][3];
				      fm.all('ContraNowName_ch').value	= arrResult[0][4];
				      fm.all('ContraNowState_ch').value	= arrResult[0][5];if(arrResult[0][5]=="1"){fm.all('ContraState_ch').value="有效";}if(arrResult[0][6]=="2"){fm.all('ContraState_ch').value="无效";}//else{fm.all('ContraState_ch').value="";}alert(fm.all('ContraState_ch').value);
              fm.all('ContraNowName').value	    = arrResult[0][6];
           
             initLHContraAssoSettingGrid();
             var mySql2="select (select distinct contraname from lhunitcontra c where c.contrano=a.IndivContraNo ),"
		                +" (select Doctno from LDDoctor where LDDoctor.Doctno=b.Doctno),"
		                +" (select DoctName from  LDDoctor where LDDoctor.Doctno=b.Doctno),"
		                +" (select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=c.ContraType),"
		                +" (select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode),"
		                +" (select case when c.dutystate = '1' then '有效' else '无效' end from dual),"
		                +" (select case when c.Contraflag = 'Y' then '是' else '否' end from ldcode where Codetype='yesno' and c.Contraflag=ldcode.code), "
		                +" c.ContraType, "
		                +" (select DutyItemCode from LDContraItemDuty where LDContraItemDuty.DutyItemCode= c.DutyItemCode), "
		                +" c.DutyState,c.Contraflag,a.IndivContraItemNo,a.IndivContraNo "
		                +" from LHContraAssoSetting a ,lhunitcontra b, LHContItem c "
		                +" where  b.contrano=c.contrano  and a.contrano ='"+ "<%=no%>" +"' "
		                +" and a.ContraItemNo ='"+ "<%=itemno%>" +"' "
		                +" and a.IndivContraNo=b.contrano and a.IndivContraItemNo=c.ContraItemNo"
		   
		    //  alert(mySql2);
			// alert(easyExecSql(mySql2));
					 turnPage.queryModal(mySql2,LHContraAssoSettingGrid); 

  }
  catch(ex)
  {
    alert("在LHAssociateSettingnputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误group!");
  }
}
function getFirstPageNo2()
{  
  try                 
  {

			     var sql="	select a.ContraNo, a.ContraName,b.DoctName,"
						 +" (select c.Codename from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
						 +" (select d.DutyItemName from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode), "
				     +" ( case when a.ContraState = '1' then '有效' else '无效' end),	"	 
				     +"  e.ContraItemNo "
						 +"	from   LHUnitContra a,LDDoctor b, lhcontitem e  "
						 +" where  a.ContraNo='"+ "<%=no%>" +"' "
						 +"	and    a.DoctNo = b.DoctNo "
						 +" and    e.ContraItemNo='"+ "<%=itemno%>" +"' "
					   +" and    a.ContraNo=e.ContraNo" 
				   
				//     var arrResult = new Array();
				     var arrResult = easyExecSql(sql);
				     
				     	fm.all('ContraNo').value				  = arrResult[0][0];
				     	fm.all('ContraName').value			  = arrResult[0][1];
				      fm.all('HospitName').value        = arrResult[0][2];
				      fm.all('ContraType_ch').value 		= arrResult[0][3];
				      fm.all('ContraNowName_ch').value	= arrResult[0][4];
				      fm.all('ContraNowState_ch').value	= arrResult[0][5];if(arrResult[0][5]=="1"){fm.all('ContraState_ch').value="有效";}if(arrResult[0][6]=="2"){fm.all('ContraState_ch').value="无效";}//else{fm.all('ContraState_ch').value="";}alert(fm.all('ContraState_ch').value);
     

  }
  catch(ex)
  {
    alert("在LHAssociateSettingnputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误group2!");
  }
}


 function initLHContraAssoSettingGrid() 
 {                            
   var iArray = new Array();                               
                                                           
   try 
   {                                                   
     iArray[0]=new Array();                                
     iArray[0][0]="序号";         		//列名               
     iArray[0][1]="30px";         		//列名               
     iArray[0][3]=0;         				//列名                 
     iArray[0][4]="";         //列名   
		 
		 iArray[1]=new Array(); 
		 iArray[1][0]="合同名称";   
		 iArray[1][1]="110px";   
		 iArray[1][2]=20;        
		 iArray[1][3]=0;
		 iArray[1][5]="1|13";
	   iArray[1][6]="1|0";
		 
		 iArray[2]=new Array(); 
		 iArray[2][0]="关联责任人员编号";   
		 iArray[2][1]="110px";   
		 iArray[2][2]=20;        
		 iArray[2][3]=0;
	//	 iArray[2][4]="doctor";
		 iArray[2][15]="DoctNo";
		 iArray[2][17]="2";
		 iArray[2][18]="350"; 
		 iArray[2][19]="1" ;      //强制刷新数据源  
		 
		 iArray[3]=new Array(); 
		 iArray[3][0]="关联责任人员名称";   
		 iArray[3][1]="110px";   
		 iArray[3][2]=20;        
		 iArray[3][3]=0;
		 
		  iArray[4]=new Array(); 
			iArray[4][0]="合同责任项目类型";   
			iArray[4][1]="110px";   
			iArray[4][2]=20;        
			iArray[4][3]=0;
		//	iArray[4][4]="hmdeftype";
		//	iArray[4][5]="4|8";
		//	iArray[4][6]="1|0";
		//	iArray[4][17]="1";
		//	iArray[4][18]="200"; 
		//	iArray[4][19]="1" ;
			
			iArray[5]=new Array(); 
			iArray[5][0]="合同责任项目名称";   
			iArray[5][1]="110px";   
			iArray[5][2]=20;        
			iArray[5][3]=0;
		// iArray[5][4]="hmdutyitemdef";
		//iArray[5][5]="5|9";
		//iArray[5][6]="0|1";
		//iArray[5][15]="DutyItemName";
		//iArray[5][17]="2";
		//iArray[5][18]="200"; 
		//iArray[5][19]="1" ;      //强制刷新数据源  
		
			iArray[6]=new Array(); 
			iArray[6][0]="责任当前状态";   
			iArray[6][1]="110px";   
			iArray[6][2]=20;        
			iArray[6][3]=0;
		//	iArray[6][4]="dutystate";
		//	iArray[6][5]="6|10";     //引用代码对应第几列，'|'为分割符            
	  //  iArray[6][6]="1|0";     //上面的列中放置引用代码中第几位值     
		//	iArray[6][17]="1"; 
		//	iArray[6][18]="200";
	  //  iArray[6][19]="1" ;
			
			iArray[7]=new Array(); 
			iArray[7][0]="关联责任标识";   
			iArray[7][1]="110px";   
			iArray[7][2]=20;        
			iArray[7][3]=0;
		//iArray[7][4]="yesno";
		//iArray[7][5]="7|11";     //引用代码对应第几列，'|'为分割符            
	  // iArray[7][6]="1|0";     //上面的列中放置引用代码中第几位值          
		////iArra7[6][14]="否";   
		//iArray[7][17]="1"; 
		//iArray[7][18]="200";
	  // iArray[7][19]="1" ;
	      
		 iArray[8]=new Array(); 
		 iArray[8][0]="hmdeftype";   
		 iArray[8][1]="0px";   
		 iArray[8][2]=20;        
		 iArray[8][3]=3; 

     iArray[9]=new Array(); 
		 iArray[9][0]="DutyItemCode";   
		 iArray[9][1]="0px";   
		 iArray[9][2]=20;        
		 iArray[9][3]=3; 
		 
		 iArray[10]=new Array(); 
		 iArray[10][0]="dutystate";   
		 iArray[10][1]="0px";   
		 iArray[10][2]=20;        
		 iArray[10][3]=3; 
		 
		 iArray[11]=new Array(); 
		 iArray[11][0]="yesno";   
		 iArray[11][1]="0px";   
		 iArray[11][2]=20;        
		 iArray[11][3]=3; 
		 
		 iArray[12]=new Array(); 
		 iArray[12][0]="ContraItemNo";   
		 iArray[12][1]="0px";   
		 iArray[12][2]=20;        
		 iArray[12][3]=3; 
		 
		 iArray[13]=new Array(); 
		 iArray[13][0]="contraNo";   
		 iArray[13][1]="0px";   
		 iArray[13][2]=20;        
		 iArray[13][3]=3; 


     
     
    LHContraAssoSettingGrid = new MulLineEnter( "fm" , "LHContraAssoSettingGrid" );                         
    //这些属性必须在loadMulLine前                                                                                              
                                                                                                               
    LHContraAssoSettingGrid.mulLineCount = 0;                                          
    LHContraAssoSettingGrid.displayTitle = 1;                                          
    LHContraAssoSettingGrid.hiddenPlus =1;                                            
    LHContraAssoSettingGrid.hiddenSubtraction = 1;                                     
    LHContraAssoSettingGrid.canSel = 0;                                                
    LHContraAssoSettingGrid.canChk = 1;                                              
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";                         
                                                                          ;  
    LHContraAssoSettingGrid.loadMulLine(iArray);                                       
    //这些操作必须在loadMulLine后面                                          
    //LDDiseaseGrid.setRowColData(1,1,"asdf");                               
  }                                                                          
  catch(ex) {                                                                
    alert(ex);                                                               
  }                                                                          
}                                                                        
      
      
</script>
  