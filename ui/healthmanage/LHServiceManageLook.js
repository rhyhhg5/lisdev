//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHSettingSlipQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    //showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
          
//Click事件，当点击增加图片时触发该函数
  

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function ReturnService()
{
	 window.close();
	 top.opener.focus(); 		
	 top.close();
}
//提交，保存按钮对应操作
function submitForm()
{

    var i = 0;
    fm.fmtransact.value="INSERT||MAIN";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.action="./LHServiceManageLookSave.jsp";
    fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    fm.all('SaveExec').disabled=true;
    initForm();
    top.opener.afterSubmitParentState();
  }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit2( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    fm.all('SaveExec').disabled=true;
    initForm();
  }
}
function updateClick()
{
	var rowNum=LHCaseInfotwoGrid. mulLineCount ; //行数 	
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHCaseInfotwoGrid.getChkNo(row); 
		if(tChk == true)
		{
			var caseNo=LHCaseInfotwoGrid.getRowColData(row,1);  
			var taskNo=LHCaseInfotwoGrid.getRowColData(row,3);
			var sqlRela="select h.ServTaskAffirm from LHCaseTaskRela h where h.ServCaseCode='"+ caseNo +"' and h.ServTaskNo='"+ taskNo+"'";
		    var affirm=easyExecSql(sqlRela);
		    if(affirm=="2")
			{
				alert(taskNo+"号服务任务已经确认,不允许再修改任务执行状态!");
				return false; 
			}
		}
	}
	if (confirm("您确实想修改此记录吗?"))
    {
        var i = 0;
        var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        
        fm.fmtransact.value = "UPDATE||MAIN";
        fm.action="./LHServiceManageLookSave.jsp";
        fm.submit(); //提交
	}
    else
    {
       alert("您取消了修改操作！");
    } 
}
function showBespeakManage()
{
	  var arrSelected = null;
	  arrSelected = new Array();
	  var rowNum=LHCaseInfotwoGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	       aa[xx] = row;	        	   
           arrSelected[xx++] = LHCaseInfotwoGrid.getRowColData(aa,3);//任务号
	  }
	  var caseCode = null;
	  caseCode = new Array();
	  var rowNum2=LHCaseInfotwoGrid. mulLineCount ; //行数 	
	  var aa2 = new Array();
	  var xx2 = 0;
	  for(var row2=0; row2 < rowNum2; row2++)
	  {
       	   aa2[xx2] = row2;
           caseCode[xx2++] = LHCaseInfotwoGrid.getRowColData(aa2,1);//事件号
	  }
	  var servitemno = null;
	  servitemno = new Array();
	  var rowNum3=LHCaseInfotwoGrid. mulLineCount ; //行数 	
	  var aa3 = new Array();
	  var xx3 = 0;
	  for(var row3=0; row3 < rowNum3; row3++)
	  {
		aa3[xx3] = row3;
		servitemno[xx3++] = LHCaseInfotwoGrid.getRowColData(aa3,13);//项目序号
	  }
	  var taskexeno = null;
	  taskexeno = new Array();
	  var rowNum1=LHCaseInfotwoGrid. mulLineCount ; //行数 	
	  var aa1 = new Array();
	  var xx1 = 0;
	  for(var row1=0; row1 < rowNum1; row1++)
	  {
	        	   aa1[xx1] = row1;
	             taskexeno[xx1++] = LHCaseInfotwoGrid.getRowColData(aa1,14);//流水号

	             var no= LHCaseInfotwoGrid.getRowColData(aa1,14);
	             var sqlRela="select h.ServTaskNo from LHTaskCustomerRela h"
	                         +" where h.TaskExecNo='"+ no+"'";

	             var pp=easyExecSql(sqlRela);
	             if(pp==""||pp==null||pp=="null")
	             { 
	             	  alert("请先对事件服务信息进行保存!");
	             	  return false;
	             }
	  }   

	  window.open("./LHHmBespeakManageMain.jsp?caseCode="+caseCode+"&arrSelected="+arrSelected+"&taskexeno="+taskexeno+"&servitemno="+servitemno+"&flag=1","健管服务预约管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
		
}
function ExportAllInfo()
{
			var sql = " select ServTaskno from LHTASKCUSTOMERRELA where ServTaskno in ("+fm.all('taskNo').value+")";
			//alert(sql);
     	var re = easyExecSql(sql);
     	//alert(re);
     	if(re!=""&&re!=null&&re!=null)
     	{
	        var showStr="正在导出数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
          fm.all('fmtransact').value = "INSERT||MAIN";
          fm.action="./LHServiceManageLookExport.jsp";
          fm.submit(); //提交
      }
      else
      {
      		alert("请您先保存服务执行状态,然后再导出!");
      		return false;
      }
}
function fileUpload()
{
	var showStr="正在导入数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	upfm.all('fmtransact').value = "UPDATE||MAIN";
	upfm.submit();
}