<%
//程序名称：LHGrpServItemInput.jsp
//程序功能：
//创建日期：2006-03-09 15:20:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('GrpServPlanNo').value = "";
    fm.all('GrpServItemNo').value = "";
    fm.all('GrpCustomerNo').value = "";
    fm.all('GrpName').value = "";
    fm.all('GrpContNo').value = "";
    fm.all('ManageCom').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHGrpServItemInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {}
  catch(ex)
  {
    alert("在LHGrpServItemInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initLHGrpItemGrid();
  }
  catch(re)
  {
    alert("LHGrpServItemInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHGrpItemGrid;
function initLHGrpItemGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="40px";         		//列名
	    iArray[0][3]=0;         		//列名
	    iArray[0][4]="station";         		//列名
    
    	iArray[1]=new Array(); 
		  iArray[1][0]="标准服务项目";   
		  iArray[1][1]="120px";   
		  iArray[1][2]=20;        
		  iArray[1][3]=2;
		  iArray[1][4]="hmservitemcode";         //列名  
		  iArray[1][5]="1|9";    //引用MulLine的对应第几列，'|'为分割符 
		  iArray[1][6]="1|0";    //上面的列中放置下拉菜单中第几位值 
		  iArray[1][15]="ServItemName"; 
		  iArray[1][17]="1"; 
		  iArray[1][18]="310";     //下拉框的宽度 
		  iArray[1][19]="1" ;      //强制刷新数据源
	    
		  iArray[2]=new Array(); 
		  iArray[2][0]="服务项目序号";   
		  iArray[2][1]="40px";   
		  iArray[2][2]=20;        
		  iArray[2][3]=1;

  
		  iArray[3]=new Array(); 
		  iArray[3][0]="组织实施方式代码";   
		  iArray[3][1]="0px";   
		  iArray[3][2]=20;        
		  iArray[3][3]=3;
	    
		  iArray[4]=new Array(); 
		  iArray[4][0]="费用计算方式";   
		  iArray[4][1]="0px";   
		  iArray[4][2]=20;        
		  iArray[4][3]=3;
		  iArray[4][4]="lhattribflag";
      iArray[4][5]="4";     //引用代码对应第几列，'|'为分割符
		  iArray[4][6]="0";     //上面的列中放置引用代码中第几位值
		  iArray[4][9]="个/团属性标识|len<=10";
	  
		  iArray[5]=new Array(); 
		  iArray[5][0]="服务提供机构标识";   
		  iArray[5][1]="70px";   
		  iArray[5][2]=20;        
		  iArray[5][3]=2;		
		  iArray[5][4]="hmhospitalmng";
		  iArray[5][5]="5";     //引用代码对应第几列，'|'为分割符
		  iArray[5][6]="1";     //上面的列中放置引用代码中第几位值
		  iArray[5][14]="<%=request.getParameter("ComID")%>";
		
		
		  iArray[6]=new Array(); 
		  iArray[6][0]="定价方式";   
		  iArray[6][1]="100px";   
		  iArray[6][2]=20;        
		  iArray[6][3]=2;	  
      iArray[6][4]="lhserveritemcode";
      iArray[6][5]="6|11";     //引用代码对应第几列，'|'为分割符
      iArray[6][6]="1|0";     //上面的列中放置引用代码中第几位值
      iArray[6][9]="定价方式名称|len<=10";   
      iArray[6][14]="未设置"
      iArray[6][15]="servpricecode";
      iArray[6][17]="9";  
      iArray[6][19]="1" ;           //是否允许强制刷新数据源,1表示允许，0表示不允许
	  
		  iArray[7]=new Array(); 
		  iArray[7][0]="入机日期";   
		  iArray[7][1]="0px";   
		  iArray[7][2]=20;        
		  iArray[7][3]=3;
		  
		  iArray[8]=new Array(); 
		  iArray[8][0]="入机时间";   
		  iArray[8][1]="0px";   
		  iArray[8][2]=20;        
		  iArray[8][3]=3;
		  
		  iArray[9]=new Array(); 
		  iArray[9][0]="标准服务项目代码";   
		  iArray[9][1]="70px";   
		  iArray[9][2]=20;        
		  iArray[9][3]=0;
    	
      iArray[10]=new Array(); 
		  iArray[10][0]="团体服务项目号码";   
		  iArray[10][1]="0px";   
		  iArray[10][2]=20;        
		  iArray[10][3]=3;
		  
		  iArray[11]=new Array(); 
		  iArray[11][0]="定价方式代码";   
		  iArray[11][1]="0px";   
		  iArray[11][2]=20;        
		  iArray[11][3]=3;
		  iArray[11][14]="0"
		
		  iArray[12]=new Array(); 
	    iArray[12][0]="组织实施方式";                             
	    iArray[12][1]="50px";                     
	    iArray[12][2]=20;                                  
	    iArray[12][3]=2;      
	    iArray[12][4]="eventtype";
	    iArray[12][5]="12|3";     //引用代码对应第几列，'|'为分割符
	    iArray[12][6]="1|0";     //上面的列中放置引用代码中第几位值
	    //iArray[12][14]="个人服务事件";
	    iArray[12][17]="1"; 
	    iArray[12][18]="160";
	    iArray[12][19]="1" ;
		
    	LHGrpItemGrid = new MulLineEnter( "fm" , "LHGrpItemGrid" ); 
    	//这些属性必须在loadMulLine前
    	
    	LHGrpItemGrid.mulLineCount = 0;   
    	LHGrpItemGrid.displayTitle = 1;
    	LHGrpItemGrid.hiddenPlus = 0;
    	LHGrpItemGrid.hiddenSubtraction = 0;
    	LHGrpItemGrid.canSel = 0;
    	LHGrpItemGrid.canChk = 0;
    	//LHGrpItemGrid.selBoxEventFuncName = "showOne";

		  LHGrpItemGrid.loadMulLine(iArray);  
		  //这些操作必须在loadMulLine后面
		  //LHGrpItemGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}


</script>
