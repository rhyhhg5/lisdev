<%
//程序名称：LHConsultQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-02-27 18:16:36
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('LogNo').value = "";
    fm.all('LogerNo').value = "";
    //fm.all('Name').value = "";
    //fm.all('FamilyDoctorNo').value = "";
    //fm.all('FamilyDoctorName').value = "";
    //fm.all('FDSCode').value = "";
    //fm.all('FDSSubject').value = "";
    //fm.all('FDSContent').value = "";
    //fm.all('State').value = "";
    //fm.all('Operator').value = "";
    //fm.all('MakeDate').value = "";
    //fm.all('MakeTime').value = "";
    //fm.all('ModifyDate').value = "";
    //fm.all('ModifyTime').value = "";
    //fm.all('FDSDate').value = "";
  }
  catch(ex) {
    alert("在LHConsultQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHConsultGrid();  
  }
  catch(re) {
    alert("LHConsultQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHConsultGrid;
function initLHConsultGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="station";         //列名
    
    iArray[1]=new Array();
    iArray[1][0]="咨询号码";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=0;         
    
    iArray[2]=new Array();
    iArray[2][0]="客户号";         		//列名
    iArray[2][1]="60px";         		//列名
    iArray[2][3]=0;         		//列名		//列名

    iArray[3]=new Array();
    iArray[3][0]="客户姓名";         		//列名
    iArray[3][1]="60px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="咨询日期";         		//列名
    iArray[4][1]="80px";         		//列名
    iArray[4][3]=0;         		//列名
    
    
    LHConsultGrid = new MulLineEnter( "fm" , "LHConsultGrid" ); 
    //这些属性必须在loadMulLine前

    LHConsultGrid.mulLineCount = 0;   
    LHConsultGrid.displayTitle = 1;
    LHConsultGrid.hiddenPlus = 1;
    LHConsultGrid.hiddenSubtraction = 1;
    LHConsultGrid.canSel = 1;
    LHConsultGrid.canChk = 0;
    //LHConsultGrid.selBoxEventFuncName = "showOne";

    LHConsultGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHConsultGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
