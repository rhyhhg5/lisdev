<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHCustomHealthStatusSave.jsp
//程序功能：
//创建日期：2005-06-18 10:11:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  
  String Healthno="";
  LHCustomHealthStatusSchema tLHCustomHealthStatusSchema   = new LHCustomHealthStatusSchema();
  OLHCustomHealthStatusUI tOLHCustomHealthStatusUI   = new OLHCustomHealthStatusUI();
  
  LHCustomGymSet tLHCustomGymSet=new LHCustomGymSet();//客户健身信息
  LHCustomFamilyDiseasSet tLHCustomFamilyDiseasSet=new LHCustomFamilyDiseasSet();//家庭疾病
 
	String tGymItemCode[] = request.getParameterValues("LHCustomGymGrid1");         
	String tGymTime[] = request.getParameterValues("LHCustomGymGrid3");        
	String tGymFreque[] = request.getParameterValues("LHCustomGymGrid4"); 
	String tGymNo[] = request.getParameterValues("LHCustomGymGrid5");
	
	String tFamilyCode[] = request.getParameterValues("LHCustomFamilyDiseasGrid2");         
	String tICDCode[] = request.getParameterValues("LHCustomFamilyDiseasGrid3"); 
	String tFamilyDiseaseNO[]=request.getParameterValues("LHCustomFamilyDiseasGrid5");      
	
	
	
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHCustomHealthStatusSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLHCustomHealthStatusSchema.setHealthNo(request.getParameter("HealthNo"));
    tLHCustomHealthStatusSchema.setAddDate(request.getParameter("AddDate"));
    tLHCustomHealthStatusSchema.setStature(request.getParameter("Stature"));
    tLHCustomHealthStatusSchema.setAvoirdupois(request.getParameter("Avoirdupois"));
    tLHCustomHealthStatusSchema.setAvoirdIndex(request.getParameter("AvoirdIndex"));
    tLHCustomHealthStatusSchema.setBloodPressHigh(request.getParameter("BloodPressHigh"));
    tLHCustomHealthStatusSchema.setBloodPressLow(request.getParameter("BloodPressLow"));
    tLHCustomHealthStatusSchema.setSmoke(request.getParameter("Smoke"));
    tLHCustomHealthStatusSchema.setKissCup(request.getParameter("KissCup"));
    tLHCustomHealthStatusSchema.setSitUp(request.getParameter("SitUp"));
    tLHCustomHealthStatusSchema.setDiningNoRule(request.getParameter("DiningNoRule"));
    tLHCustomHealthStatusSchema.setBadHobby(request.getParameter("BadHobby"));
    tLHCustomHealthStatusSchema.setOperator(request.getParameter("Operator"));
    tLHCustomHealthStatusSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHCustomHealthStatusSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHCustomHealthStatusSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHCustomHealthStatusSchema.setModifyTime(request.getParameter("ModifyTime"));
    tLHCustomHealthStatusSchema.setManageCom(request.getParameter(tG.ManageCom));
    
      int GymCount = 0;
			if (tGymItemCode != null) GymCount = tGymItemCode.length;
	       System.out.println(" GymCount is : "+GymCount); 
			for (int i = 0; i < GymCount; i++)	
			{
		   	if(tGymItemCode[i]!=""){
		   	LHCustomGymSchema tLHCustomGymSchema = new LHCustomGymSchema();
			  tLHCustomGymSchema.setCustomerNo(request.getParameter("CustomerNo"));
			  tLHCustomGymSchema.setGymItemCode(tGymItemCode[i]);
			  tLHCustomGymSchema.setGymFreque(tGymFreque[i]);
			  tLHCustomGymSchema.setGymTime(tGymTime[i]);
			  tLHCustomGymSchema.setOperator(request.getParameter("Operator"));
        tLHCustomGymSchema.setMakeDate(request.getParameter("MakeDate"));
        tLHCustomGymSchema.setMakeTime(request.getParameter("MakeTime"));
        tLHCustomGymSchema.setModifyDate(request.getParameter("ModifyDate"));
        tLHCustomGymSchema.setModifyTime(request.getParameter("ModifyTime"));
			  tLHCustomGymSchema.setCustomerGymNo(tGymNo[i]);
			  
				tLHCustomGymSet.add(tLHCustomGymSchema);
				}
			}
			
			int FamilyDiseasCount = 0;
			if (tFamilyCode != null) FamilyDiseasCount = tFamilyCode.length;     
			 System.out.println(" FamilyDiseasCount is : "+FamilyDiseasCount); 
	        
			for (int i = 0; i < FamilyDiseasCount; i++)	
			{
			LHCustomFamilyDiseasSchema tLHCustomFamilyDiseasSchema = new LHCustomFamilyDiseasSchema();
			  tLHCustomFamilyDiseasSchema.setCustomerNo(request.getParameter("CustomerNo"));
			  tLHCustomFamilyDiseasSchema.setFamilyCode(tFamilyCode[i]);
			  tLHCustomFamilyDiseasSchema.setICDCode(tICDCode[i]);
			  tLHCustomFamilyDiseasSchema.setOperator(request.getParameter("Operator"));
        tLHCustomFamilyDiseasSchema.setMakeDate(request.getParameter("MakeDate"));
        tLHCustomFamilyDiseasSchema.setMakeTime(request.getParameter("MakeTime"));
        tLHCustomFamilyDiseasSchema.setModifyDate(request.getParameter("ModifyDate"));
        tLHCustomFamilyDiseasSchema.setModifyTime(request.getParameter("ModifyTime"));  
        tLHCustomFamilyDiseasSchema.setFamilyDiseaseNO(tFamilyDiseaseNO[i]);
			  
				
				tLHCustomFamilyDiseasSet.add(tLHCustomFamilyDiseasSchema);
			}
  try
  {
  // 准备传输数据 VData
    System.out.println("--------------Start to Submit---------");
  	VData tVData = new VData();
  	tVData.add(tLHCustomHealthStatusSchema);
  	tVData.add(tG);
  	tVData.add(tLHCustomGymSet);
  	tVData.add(tLHCustomFamilyDiseasSet);
    tOLHCustomHealthStatusUI.submitData(tVData,transact);
    if ( FlagStr.equals("Success") && "INSERT||MAIN".equals(transact) )
     {
      VData res = tOLHCustomHealthStatusUI.getResult();
      System.out.println("^^^^^^^^^^^^^^^^^"+res);
      LHCustomHealthStatusSchema mLHCustomHealthStatusSchema = new LHCustomHealthStatusSchema();
      
      mLHCustomHealthStatusSchema.setSchema((LHCustomHealthStatusSchema) res.
                                          getObjectByObjectName("LHCustomHealthStatusSchema",
                  0));
      

    	Healthno = (String)mLHCustomHealthStatusSchema.getHealthNo();
     }

  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHCustomHealthStatusUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	<%if(Healthno.equals("")){%>
	 //alert("save : Healthno为空");
   // 	parent.fraInterface.fm.all("Healthno").value="";
        <%}else{%>	
	parent.fraInterface.fm.all("Healthno").value = "<%=Healthno%>";	 
	<%}%>
</script>
</html>
