<%
//程序名称：LDHospitalInput.jsp
//程序功能：
//创建日期：2005-01-15 14:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<%
		String LoadFlag = "";
		if(request.getParameter("LoadFlag")!=null)
		{
			LoadFlag = request.getParameter("LoadFlag");
		}
%>
<%
     String HospitalNo = request.getParameter("HospitalNo");  
     String flag=request.getParameter("flag");
     System.out.println("1111"+HospitalNo);

%>
<script language="JavaScript">
	
	var d = new Date();
	var h = d.getYear();
	var m = d.getMonth(); 
	var day = d.getDate();  
	var Date1;       
	if(h<10){h = "0"+d.getYear();}  
	if(m<9){ m++; m = "0"+m;}
	else{m++;}
	if(day<10){day = "0"+d.getDate();}
	Date1 = h+"-"+m+"-"+day;
	
function initInpBox()
{ 
  try
  {                                   
    fm.all('HospitalType').value = "";
    fm.all('HospitalType_ch').value = "";
    fm.all('HospitCode').value = "";
    fm.all('HospitCode').readOnly =true;
    fm.all('HospitName').value = "";
    fm.all('HospitShortName').value = "";
    fm.all('AreaCode').value = "";
    fm.all('AreaName').value = "";
    fm.all('LevelCode').value = "";
    fm.all('LevelCode_ch').value = "";
    fm.all('BusiTypeCode').value = "";
    fm.all('BusiTypeCode_ch').value = "";
    fm.all('Address').value = "";
    fm.all('ZipCode').value = "";
    fm.all('Phone').value = "";
    fm.all('WebAddress').value = "";   
    fm.all('CommunFixFlag').value = "";
    fm.all('CommunFixFlag_ch').value = "";
    fm.all('adminisortcode').value = "";
    fm.all('adminisortcode_ch').value = "";
    fm.all('Economelemencode').value = "";
    fm.all('Economelemencode_ch').value = "";
    fm.all('AssociateClass').value = "";
    fm.all('AssociateClass_ch').value = "";
    fm.all('HospitalStandardCode').value = "";
    fm.all('HospitalStandardCode').readOnly = true;
    //fm.all('SuperiorNo').value = "";
    //fm.all('InterNo').value = "";
   // fm.all('ElementaryNo').value = "";
   // fm.all('TotalNo').value = "";
    fm.all('BedAmount').value = "";
//    fm.all('FlowNo').value = "";
    fm.all('PatientPerDay').value = "";
    fm.all('OutHospital').value = "";
    fm.all('SpecialClass').value = "";
    fm.all('MngCom').value = "";
    fm.all('MngCom_ch').value = "";
    fm.all('UrbanOptionName').value = "";
    fm.all('UrbanOption').value = "";
    fm.all('Route').value ="";
    fm.all('SecurityNo').value = "";
    UrbanName.style.display='none';
    UrbanOpt.style.display='none';
     fm.all('HiddenBtn').value="<%=flag%>";

  }
  catch(ex)
  {
    alert("在LDHospitalInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LDHospitalInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{

  try
  {
    initInpBox();
    initSelBox();  
    initSpecSecDiaGrid();
    initInstrumentGrid();
    initComHospitalGrid();
	fm.LoadFlag.value ="<%=LoadFlag%>";
  	if (fm.LoadFlag.value=="1")
  	{
  		operateButton.style.display='none';
  		divQueryButton.style.display='';
  	}
     var flag = "<%=flag%>";
	
    if(flag!=""&&flag!="null"&&flag!=null)//从医疗机构检索进入此页面为1
    {
    	  fm.all('querybutton').disabled=true;
        fm.all('deleteButton').disabled=true;
        fm.all('modifyButton').disabled=true;
        fm.all('saveButton').disabled=true;
       InHospitalInfo();
    }
  }
  catch(re)
  {
    alert("LDHospitalInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initSpecSecDiaGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="专科名称";   
		iArray[1][1]="150px";   
		iArray[1][2]=60;        
		iArray[1][3]=2;
	//	iArray[1][9]="专科名称|LEN<60";
		iArray[1][4]="lhspecname";               
	  iArray[1][5]="1|5";     //引用代码对应第几列，'|'为分割符
    iArray[1][6]="1|0";     //上面的列中放置引用代码中第几位
    iArray[1][15]="codename";
    iArray[1][17]="1"; 
    iArray[1][18]="340";
    iArray[1][19]="1" ;
		
		iArray[2]=new Array(); 
		iArray[2][0]="特色介绍";   
		iArray[2][1]="318px";   
		iArray[2][2]=500;        
		iArray[2][3]=1;
		iArray[2][7]="inputSpecSecDia";
		iArray[2][9]="特色介绍|LEN<500";
		
		iArray[3]=new Array(); 
		iArray[3][0]="SpecialClass";   
		iArray[3][1]="0px";   
		iArray[3][2]=20;        
		iArray[3][3]=3;
		iArray[3][14]="SpecSecDia";    
		
		iArray[4]=new Array();
		iArray[4][0]="FlowNo";
		iArray[4][1]="0px";         
		iArray[4][2]=20;            
		iArray[4][3]=3;       
		
	  iArray[5]=new Array();
		iArray[5][0]="code";
		iArray[5][1]="0px";         
		iArray[5][2]=20;            
		iArray[5][3]=3;             
		      
		
    SpecSecDiaGrid = new MulLineEnter( "fm" , "SpecSecDiaGrid" ); 
    //这些属性必须在loadMulLine前

    SpecSecDiaGrid.mulLineCount = 0;   
    SpecSecDiaGrid.displayTitle = 1;
    SpecSecDiaGrid.hiddenPlus = 0;
    SpecSecDiaGrid.hiddenSubtraction = 0;
    SpecSecDiaGrid.canSel = 0;
    SpecSecDiaGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    SpecSecDiaGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDiseaseGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
  
  function initInstrumentGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         	     	//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="设备名称";   
		iArray[1][1]="150px";   
		iArray[1][2]=60;        
		iArray[1][3]=1;
		iArray[1][9]="设备名称|LEN<60";
		
		iArray[2]=new Array(); 
		iArray[2][0]="设备介绍";   
		iArray[2][1]="150px";   
		iArray[2][2]=500;        
		iArray[2][3]=1;
		iArray[2][7]="inputInstrument";
		iArray[2][9]="设备介绍|LEN<500";
		                          
    iArray[3]=new Array();       
    iArray[3][0]="SpecialClass"; 		                          
		iArray[3][1]="0px";                                    
		iArray[3][2]=20;                                       
		iArray[3][3]=3;                                        
    iArray[3][14]="Instrument";      
    
    iArray[4]=new Array();
    iArray[4][0]="FlowNo";
    iArray[4][1]="0px";   
    iArray[4][2]=20;      
    iArray[4][3]=3;       
    
    InstrumentGrid = new MulLineEnter( "fm" , "InstrumentGrid" ); 
    //这些属性必须在loadMulLine前

    InstrumentGrid.mulLineCount = 0;   
    InstrumentGrid.displayTitle = 1;
    InstrumentGrid.hiddenPlus = 0;
    InstrumentGrid.hiddenSubtraction = 0;
    InstrumentGrid.canSel = 0;
    InstrumentGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    InstrumentGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDDiseaseGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

function initComHospitalGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="station";         //列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="合作级别";   
		iArray[1][1]="220px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;
		iArray[1][4]='llhospiflag';
		iArray[1][5]="1|1"; 
		iArray[1][6]="0|1"; 
		iArray[1][9]="合作级别|LEN<20"
		
		iArray[2]=new Array(); 
		iArray[2][0]="起始时间";   
		iArray[2][1]="120px";   
		iArray[2][2]=20;        
		iArray[2][3]=1;
		iArray[2][9]="起始时间|DATE"
		iArray[2][14]=Date1;
		
		iArray[3]=new Array(); 
		iArray[3][0]="终止时间";   
		iArray[3][1]="120px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;
		iArray[3][9]="终止时间|DATE"
		iArray[3][14]=Date1;
		
		iArray[4]=new Array();
		iArray[4][0]="SerialNo";
		iArray[4][1]="0px";         
		iArray[4][2]=20;            
		iArray[4][3]=3;   
		
		iArray[5]=new Array();
		iArray[5][0]="医院代码";
		iArray[5][1]="0px";         
		iArray[5][2]=20;            
		iArray[5][3]=3; 
		
		          
		
    ComHospitalGrid = new MulLineEnter( "fm" , "ComHospitalGrid" ); 
    //这些属性必须在loadMulLine前

    ComHospitalGrid.mulLineCount = 0;   
    ComHospitalGrid.displayTitle = 1;
    ComHospitalGrid.hiddenPlus = 0;
    ComHospitalGrid.hiddenSubtraction = 0;
    ComHospitalGrid.canSel = 0;
    ComHospitalGrid.canChk = 0;
    //ComHospitalGrid.selBoxEventFuncName = "showOne";

    ComHospitalGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ComHospitalGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

function InHospitalInfo()
{
	 var arrResult = new Array();
		//initForm();
		if(easyExecSql("select HospitalType from LDHospital where HospitCode='"+ "<%=HospitalNo%>" +"'") == 1)
				{
					servPart.style.display='';
					hospitalSrcInfo.style.display='';fm.all('HospitalType_ch').value = "医疗机构";
					arrResult = easyExecSql("select HospitalType,HospitName,HospitShortName,AreaCode,AreaName,"
					          +"HospitCode,CommunFixFlag,AdminiSortCode,EconomElemenCode,LevelCode,"
				           	+"BusiTypeCode,AssociateClass,HospitalStandardCode,Address,Phone,ZipCode,"
				           	+"WebAddress,SuperiorNo,InterNo,ElementaryNo,TotalNo,BedAmount,PatientPerDay,"
				           	+"OutHospital, Route,ManageCom, UrbanOption, securityno from LDHospital where  HospitCode='"+ "<%=HospitalNo%>" +"'");
									
					fm.all('HospitalType').value = 				arrResult[0][0] ;
			    fm.all('HospitName').value =					arrResult[0][1] ;
			    fm.all('HospitShortName').value = 		arrResult[0][2] ;
			    fm.all('AreaCode').value = 						arrResult[0][3] ;
			    fm.all('AreaName').value = 						arrResult[0][4] ;
			    fm.all('HospitCode').value =   				arrResult[0][5] ;
			    fm.all('CommunFixFlag').value = 		  arrResult[0][6] ;if(arrResult[0][6]=="0"){fm.all('CommunFixFlag_ch').value="社保非定点医院";}if(arrResult[0][6]=="1"){fm.all('CommunFixFlag_ch').value="社保定点医院";}
			    fm.all('AdminiSortCode').value =  		arrResult[0][7] ;var tempAdminiSortCode = easyExecSql("select codename from ldcode where codetype='hmadminisortcode' and code = '"+arrResult[0][7]+"'");fm.all('AdminiSortCode_ch').value= tempAdminiSortCode==null?"":tempAdminiSortCode;
			    fm.all('EconomElemenCode').value = 		arrResult[0][8] ;var tempEconomElemenCode=easyExecSql("select codename from ldcode where codetype='hmeconomelemencode' and code = '"+arrResult[0][8]+"'");fm.all('EconomElemenCode_ch').value= tempEconomElemenCode==null?"":tempEconomElemenCode;
			    fm.all('LevelCode').value = 					arrResult[0][9] ;var tmepLevelCode = easyExecSql("select codename from ldcode where codetype='hospitalclass' and code = '"+arrResult[0][9]+"'");fm.all('LevelCode_ch').value= tmepLevelCode==null?"":tmepLevelCode;
			    fm.all('BusiTypeCode').value = 				arrResult[0][10];var tempBusiTypeCode = easyExecSql("select codename from ldcode where codetype='hmbusitype' and code = '"+arrResult[0][10]+"'");fm.all('BusiTypeCode_ch').value= tempBusiTypeCode==null?"":tempBusiTypeCode;
//			    fm.all('AssociateClass').value = 			arrResult[0][11];var tempAssociateClass = easyExecSql("select codename from ldcode where codetype='llhospiflag' and code = '"+arrResult[0][11]+"'");fm.all('AssociateClass_ch').value= tempAssociateClass==null?"":tempAssociateClass;
				var tempAssociateClass_ch = easyExecSql("select distinct Associatclass from ldcomhospital where hospitcode = '"+arrResult[0][5]+"' and startdate = ( select distinct max(startdate) from ldcomhospital where hospitcode = '"+arrResult[0][5]+"')");
				if(tempAssociateClass_ch == "" || tempAssociateClass_ch == "null" || tempAssociateClass_ch ==null)
				{
					fm.all('AssociateClass').value = arrResult[0][11];
					var tempAssociateClass = easyExecSql("select codename from ldcode where codetype='llhospiflag' and code = '"+arrResult[0][11]+"'");
					fm.all('AssociateClass_ch').value= tempAssociateClass==null?"":tempAssociateClass;	
				}
				else
				{
					fm.all('AssociateClass_ch').value = tempAssociateClass_ch;
					var tempAssociateClass = easyExecSql("select code from ldcode where codetype='llhospiflag' and codename = '"+tempAssociateClass_ch+"'");
					fm.all('AssociateClass').value= tempAssociateClass==null?"":tempAssociateClass;
				}

			    fm.all('HospitalStandardCode').value =arrResult[0][12];
			    fm.all('Address').value = 						arrResult[0][13];
			    fm.all('Phone').value = 							arrResult[0][14];
			    fm.all('ZipCode').value = 						arrResult[0][15];		

	    
			    fm.all('WebAddress').value = 					arrResult[0][16];  
			    fm.all('SuperiorNo').value = 					arrResult[0][17];
			    fm.all('InterNo').value = 						arrResult[0][18];
			    fm.all('ElementaryNo').value = 				arrResult[0][19];
			    fm.all('TotalNo').value = 						arrResult[0][20];
			    fm.all('BedAmount').value = 					arrResult[0][21];
//			   fm.all('FlowNo').value =							arrResult[0][22];
			    fm.all('PatientPerDay').value = 			arrResult[0][22];
			    fm.all('OutHospital').value = 				arrResult[0][23];
			    fm.all('Route').value = 				      arrResult[0][24];
			    fm.all('MngCom').value =              arrResult[0][25];
			    fm.all('MngCom_ch').value = easyExecSql("select showname from ldcom where comcode='"+arrResult[0][25]+"'");
			    fm.all('SecurityNo').value = arrResult[0][27];
			    if( manageCom.substring(0,4)=="8653" || manageCom=="86")
			    {
					    if(arrResult[0][9]=="00" || arrResult[0][9]=="10" || arrResult[0][9]=="11" )
					    {
									UrbanName.style.display='';
									UrbanOpt.style.display='';
							    fm.all('UrbanOption').value =  				arrResult[0][26];
							    if(arrResult[0][26]=="0"){fm.all('UrbanOptionName').value = "非城区"}
							    if(arrResult[0][26]=="1"){fm.all('UrbanOptionName').value = "城区"}
							 }
			    }                           
			    var GridSQL1 = "select SpecialName,SpecialIntro,SpecialClass,FlowNo from LDHospitalInfoIntro where SpecialClass = 'SpecSecDia' " + getWherePart("HospitCode", "HospitCode");
			    turnPage.queryModal(GridSQL1, SpecSecDiaGrid);
			    //var arr = easyExecSql(GridSQL1);alert(arr[0][3]);
			    
			    var GridSQL2 = "select SpecialName,SpecialIntro,SpecialClass,FlowNo from LDHospitalInfoIntro where SpecialClass = 'Instrument' " + getWherePart("HospitCode", "HospitCode");
			    turnPage.queryModal(GridSQL2, InstrumentGrid);
			    
			    var GridSQL3 = "select AssociatClass,StartDate,EndDate,SerialNo,HospitCode from LDComHospital where 1=1 "+ getWherePart("HospitCode", "HospitCode");
					turnPage.queryModal(GridSQL3, ComHospitalGrid);
				}
				
				
				if(easyExecSql("select HospitalType from LDHospital where HospitCode='"+ "<%=HospitalNo%>" +"'") == 2)
				{

					fm.all('HospitalType_ch').value = "健康体检机构";
					arrResult = easyExecSql("select HospitalType,HospitName,HospitShortName,AreaCode,AreaName,"
					          +"HospitCode,CommunFixFlag,AdminiSortCode,EconomElemenCode,LevelCode,"
					          +"BusiTypeCode,AssociateClass,HospitalStandardCode,Address,Phone,ZipCode,"
					          +"WebAddress,SuperiorNo,InterNo,ElementaryNo,TotalNo,BedAmount,PatientPerDay,"
					          +"OutHospital,Route, ManageCom from LDHospital where  HospitCode='"+ "<%=HospitalNo%>" +"'");
									
					fm.all('HospitalType').value = 				arrResult[0][0] ;
			    fm.all('HospitName').value =					arrResult[0][1] ;
			    fm.all('HospitShortName').value = 		arrResult[0][2] ;
			    fm.all('AreaCode').value = 						arrResult[0][3] ;
			    fm.all('AreaName').value = 						arrResult[0][4] ;
			    fm.all('HospitCode').value =   				arrResult[0][5] ;
			    fm.all('CommunFixFlag').value = 		  arrResult[0][6] ;if(arrResult[0][6]=="0"){fm.all('CommunFixFlag_ch').value="社保非定点医院";}if(arrResult[0][6]=="1"){fm.all('CommunFixFlag_ch').value="社保定点医院";}
			    fm.all('AdminiSortCode').value =  		arrResult[0][7] ;fm.all('AdminiSortCode_ch').value=easyExecSql("select codename from ldcode where codetype='hmadminisortcode' and code = '"+arrResult[0][7]+"'");
			    fm.all('EconomElemenCode').value = 		arrResult[0][8] ;fm.all('EconomElemenCode_ch').value=easyExecSql("select codename from ldcode where codetype='hmeconomelemencode' and code = '"+arrResult[0][8]+"'");
			    fm.all('LevelCode').value = 					arrResult[0][9] ;fm.all('LevelCode_ch').value=easyExecSql("select codename from ldcode where codetype='hospitalclass' and code = '"+arrResult[0][9]+"'");
			    fm.all('BusiTypeCode').value = 				arrResult[0][10];fm.all('BusiTypeCode_ch').value=easyExecSql("select codename from ldcode where codetype='hmbusitype' and code = '"+arrResult[0][10]+"'");
			    fm.all('AssociateClass').value = 			arrResult[0][11];fm.all('AssociateClass_ch').value=easyExecSql("select codename from ldcode where codetype='llhospiflag' and code = '"+arrResult[0][11]+"'");
			    fm.all('HospitalStandardCode').value =arrResult[0][12];
			    fm.all('Address').value = 						arrResult[0][13];
			    fm.all('Phone').value = 							arrResult[0][14];
			    fm.all('ZipCode').value = 						arrResult[0][15];		

	    
			    fm.all('WebAddress').value = 					arrResult[0][16];  
			    fm.all('SuperiorNo').value = 					arrResult[0][17];
			    fm.all('InterNo').value = 						arrResult[0][18];
			    fm.all('ElementaryNo').value = 				arrResult[0][19];
			    fm.all('TotalNo').value = 						arrResult[0][20];
			    fm.all('BedAmount').value = 					arrResult[0][21];
//			   fm.all('FlowNo').value =							arrResult[0][22];
			    fm.all('PatientPerDay').value = 			arrResult[0][22];
			    fm.all('OutHospital').value = 				arrResult[0][23];
			    fm.all('Route').value = 				      arrResult[0][24];
			    fm.all('MngCom').value =              arrResult[0][25];
			    fm.all('MngCom_ch').value = easyExecSql("select showname from ldcom where comcode='"+arrResult[0][25]+"'");
 
				}
}

</script>
