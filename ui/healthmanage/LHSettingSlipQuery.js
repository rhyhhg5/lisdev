//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var StrSql;
var checkedrow;
var Come;//页面来源,用来标记从哪个页面传入的,对应不同的初始化
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHSettingSlipQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    //showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算

//Click事件，当点击增加图片时触发该函数


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function showQueryInfo()
{
	var strSql="";
	var tempCom = fm.all('MngCom').value.length==8?fm.all('MngCom').value.substring(0,4):fm.all('MngCom').value;

        var LHContStartEnd="";
        var start;
        var end;
        if(fm.all('ServCaseType').value=="1")
        {

               var LHContStart1="";
                var LHContStart2="";
                var LHContEnd1="";
                var LHContEnd2="";
              if(fm.all('LHContStart1').value != null&&  fm.all('LHContStart1').value != ""){
               LHContStart1 = fm.all('LHContStart1').value;
              }
              if(fm.all('LHContStart2').value != null){
                LHContStart2 = fm.all('LHContStart2').value;
              }
              if(fm.all('LHContEnd1').value!= null)
              {
              LHContEnd1 = fm.all('LHContEnd1').value;
              }
              if(fm.all('LHContEnd2').value!= null)
              {
                 LHContEnd2 = fm.all('LHContEnd2').value;
              }

                 //起始日期
                 if(LHContStart1 != "" && LHContStart2 != "" && LHContStart1<LHContStart2)
                 {
                   start =" c.startdate between '"+LHContStart1+"' and '"+LHContStart2+"' ";
                 }
                 else if(LHContStart1 != "" && LHContStart2 != "" && LHContStart1 == LHContStart2)
                 {
                     start =" c.startdate='"+LHContStart1+"' ";
                 }
                  else if(LHContStart1 != "" && LHContStart2 != "" && LHContStart1 > LHContStart2 )
                 {
                     alert("您输入的生效起始日期大于生效终止日期！");
                     start = "";
                 }
                 else if(LHContStart1 != "" && LHContStart2 == "" )
                 {
                      start =" c.startdate='"+LHContStart1+"' ";
                 }
                 else if(LHContStart1 == "" && LHContStart2 != "" )
                 {
                      start =" c.startdate='"+LHContStart2+"' ";
                 }
                 else
                 {

                      start = "";
                 }

              //终止日期
               if(LHContEnd1 != "" && LHContEnd2 != "" && LHContEnd1 < LHContEnd2)
                 {
                   end =  "  c.EndDate between '"+LHContEnd1+"' and '"+LHContEnd2+"'";
                 }
                 else if(LHContEnd1 != "" && LHContEnd2 != "" && LHContEnd1 == LHContEnd2)
                 {
                      end = "  c.EndDate='" +LHContEnd1+"'";
                 }
                 else if(LHContEnd1 != "" && LHContEnd2 != "" && LHContEnd1 > LHContEnd2)
                 {
                     alert("您输入的终止开始日期大于终止结束日期！");
                     end = "";
                 }
                 else if(LHContEnd1 != "" && LHContEnd2 == "" )
                 {
                      end = "  c.EndDate='" +LHContEnd1+"'";
                 }
                  else if(LHContEnd1 == "" && LHContEnd2 != "" )
                  {
                     end = "  c.EndDate='" +LHContEnd2+"'";
                  }
                  else
                  {
                      end = "";
                  }

                  if(start != "" && end != "")
                  {
                       LHContStartEnd = " and a.servcasecode in"
                                          +" (select distinct b.servcasecode from lhservcaserela b where b.servplanNo in ( "
                                          +" select c.ServPlanNo from lhservplan c where "+start+" and "+end+"))";
                  }
                  else if(start == "" && end !="")
                  {
                        LHContStartEnd = " and a.servcasecode in"
                                          +" (select distinct b.servcasecode from lhservcaserela b where b.servplanNo in ( "
                                          +" select c.ServPlanNo from lhservplan c where "+end+"))";
                  }
                  else if(start != "" && end =="")
                  {
                         LHContStartEnd = " and a.servcasecode in"
                                          +" (select distinct b.servcasecode from lhservcaserela b where b.servplanNo in ( "
                                          +" select c.ServPlanNo from lhservplan c where "+start+"))";
                  }
                  else
                  {
                         LHContStartEnd = "";
                  }
        }
	if(fm.all('ServCaseState').value == "0")
	{
		    strContNO=" select  distinct  conttype from lccont where grpcontno='"+fm.all('ContNo').value+"' "
		    var grpContNo=easyExecSql(strContNO);
		    //alert(grpContNo);
		    var sqlContNo="";
  	    if(fm.all('ContNo').value != "")
  	    {
  	    	 if(grpContNo=="2")//此保单号为团单号
  	    	 {
  	    	    sqlContNo =" and a.ServCaseCode not in ( select distinct   b.ServCaseCode from  LHServCaseRela b where   b.grpcontno = '"+fm.all('ContNo').value+"')";
  	    	 }
  	    	 if(grpContNo=="1")//此保单号为个单号
  	    	 {
  	    	    sqlContNo =" and a.ServCaseCode in ( select distinct   b.ServCaseCode from  LHServCaseRela b where   b.ContNo = '"+fm.all('ContNo').value+"')";
  	    	}
  	    }
            var ServItemCode="";
  	    if(fm.all('ServItemCode').value != "")
  	    {
  	    	  ServItemCode =" and a.ServCaseCode in ( select distinct   b.ServCaseCode from  LHServCaseRela b where   b.ServItemCode = '"+fm.all('ServItemCode').value+"')";
  	    }
  	    var CustomerNo="";
  	    if(fm.all('CustomerNo').value != "")
  	    {
  	    	  CustomerNo =" and a.ServCaseCode in ( select distinct   b.ServCaseCode from  LHServCaseRela b  where   b.CustomerNo = '"+fm.all('CustomerNo').value+"')";
  	    }
  	    var CustomerName="";
  	    if(fm.all('CustomerName').value != "")
  	    {
  	    	  CustomerName =" and a.ServCaseCode in ( select distinct  b.ServCaseCode from  LHServCaseRela  b where   b.CustomerName = '"+fm.all('CustomerName').value+"')";
  	    }
  	    var ServItemIN="";
  	    if(fm.all('ServItemIN').value != "")
  	    {
  	    	  if(fm.all('ServItemIN').value== "1")
  	    	  {
  	    	     ServItemIN =" and a.ServCaseCode in "
  	    	             +" ( select distinct   b.ServCaseCode from  LHServCaseRela b )";
  	    	  }
  	    	  if(fm.all('ServItemIN').value== "2")
  	    	  {
  	    	     ServItemIN =" and a.ServCaseCode not in "
  	    	             +" ( select distinct   b.ServCaseCode from  LHServCaseRela b )";
  	    	  }
  	    }

		    strSql = " select distinct a.ServCaseCode ,a.ServCaseName, "
               +" (case a.ServCaseType when '1' then '个人服务事件' when '2' then '集体服务事件' when '3' then '费用结算事件' else '无' end ), "
               +" (case a.ServCaseState when '0' then '未进行启动设置' when '1' then '服务事件已启动' when '2' then '服务事件已完成'  when '3' then '服务事件失败' else '无' end ) , "
               +"  a.ServCaseType , a.ServCaseState "
               +"  from LHServCaseDef a "
               +"  where 1=1 and a.managecom like '"+tempCom+"%%'  "
               +getWherePart("a.ServCaseCode","ServCaseCode")
               +getWherePart("a.ServCaseName","ServCaseName","like" )
               +getWherePart("a.ServCaseType","ServCaseType")
               +getWherePart("a.ServCaseState","ServCaseState")
               +ServItemIN
               +ServItemCode
               +CustomerNo
               +CustomerName
               +sqlContNo
               +LHContStartEnd
               ;
               //fm.all('ContNo').value=strSql;
	 }
	 else
	 {
	 	  strContNO=" select  distinct  conttype from lccont where grpcontno='"+fm.all('ContNo').value+"' "
		  var grpContNo=easyExecSql(strContNO);
		  //alert(grpContNo);
      if(grpContNo=="2")//此保单号为团单号
      {
		      strSql=" select distinct b.ServCaseCode ,a.ServCaseName, "
		    	      +" (case a.ServCaseType when '1' then '个人服务事件' when '2' then '集体服务事件' when '3' then '费用结算事件' else '无' end ),"
		    	      +" (case a.ServCaseState when '0' then '未进行启动设置' when '1' then '服务事件已启动' when '2' then '服务事件已完成'  when '3' then '服务事件失败' else '无' end ) ,"
		    	      +" a.ServCaseType, a.ServCaseState"
		    	      +" from LHServCaseRela b  ,LHServCaseDef a "
        		    +" where a.ServCaseCode=b.ServCaseCode "
        		    +getWherePart("b.CustomerNo","CustomerNo")
        		    +getWherePart("b.CustomerName","CustomerName" )
        		    +getWherePart("b.ServItemCode","ServItemCode")
        		    +getWherePart("b.ServCaseCode","ServCaseCode")
        		    +getWherePart("a.ServCaseName","ServCaseName","like" )
        		    +getWherePart("a.ServCaseType","ServCaseType")
        		    +getWherePart("a.ServCaseState","ServCaseState")
        		    +getWherePart("b.grpcontno","ContNo")
                            +LHContStartEnd
		    	      +" and b.comid = '"+fm.all('MngCom').value+"' "
                            ;
		  }
		  else
		  {
		   	  strSql=" select distinct b.ServCaseCode ,a.ServCaseName, "
		    	      +" (case a.ServCaseType when '1' then '个人服务事件' when '2' then '集体服务事件' when '3' then '费用结算事件' else '无' end ),"
		    	      +" (case a.ServCaseState when '0' then '未进行启动设置' when '1' then '服务事件已启动' when '2' then '服务事件已完成'  when '3' then '服务事件失败' else '无' end ) ,"
		    	      +" a.ServCaseType, a.ServCaseState"
		    	      +" from LHServCaseRela b  ,LHServCaseDef a "
        		    +" where a.ServCaseCode=b.ServCaseCode "
        		    +getWherePart("b.ContNo","ContNo")
        		    +getWherePart("b.CustomerNo","CustomerNo")
        		    +getWherePart("b.CustomerName","CustomerName" )
        		    +getWherePart("b.ServItemCode","ServItemCode")
        		    +getWherePart("b.ServCaseCode","ServCaseCode")
        		    +getWherePart("a.ServCaseName","ServCaseName","like" )
        		    +getWherePart("a.ServCaseType","ServCaseType")
        		    +getWherePart("a.ServCaseState","ServCaseState")
                            +LHContStartEnd
		    	      +" and b.comid = '"+fm.all('MngCom').value+"' "
		    		    ;
		  }
 	}
	if(fm.all('ServCaseType').value == "3")
	{//针对费用结算的处理
		if(easyExecSql(strSql) == null || easyExecSql(strSql) == "")
		{
			strSql = " select distinct a.ServCaseCode ,a.ServCaseName, '费用结算事件', "
					    +" (case a.ServCaseState when '0' then '未进行启动设置' when '1' then '服务事件已启动' when '2' then '服务事件已完成'  when '3' then '服务事件失败' else '无' end ) ,"
					    +" a.ServCaseType, a.ServCaseState"
		      		+" from LHServCaseDef a  where 1=1 "
		      		+getWherePart("a.ServCaseCode","ServCaseCode")
		    	    +getWherePart("a.ServCaseName","ServCaseName","like" )
		    	    +getWherePart("a.ServCaseType","ServCaseType")
		    	    +getWherePart("a.ServCaseState","ServCaseState")
                            +" and a.managecom = '"+fm.all('MngCom').value+"' "
		            ;
		}
	}
  turnPage.queryModal(strSql, LHSettingSlipQueryGrid);
}

function ServQieYueInfo()
{
	   var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数
	   var aa = new Array();
		 var xx = 0;
		 for(var row=0; row < rowNum; row++)
		 {
			   var tChk =LHSettingSlipQueryGrid.getChkNo(row);
					if(tChk == true)
					{
							aa[xx++] = row;
				  }
		 }
	   if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
			{
				alert("请选择要传输的事件信息！");
				return false;
			}
	   if(aa.length >"1")
		  {
		  	alert( "请选择一条事件信息!");
		  	return false;
		  }
	   if (aa.length=="1")
		{
			var ServCaseCode =LHSettingSlipQueryGrid.getRowColData(aa,1);
			//alert(ServCaseCode);
			var EventCode=ServCaseCode;
            //alert(EventCode.toString().substring(8));
            var caseCode=EventCode.toString().substring(8);
            //alert(caseCode.toString().substring(0,1));
            var code=caseCode.toString().substring(0,1);
            //if(code=="2")
            //{
				window.open("./LHServQieYueInfoMain.jsp?ServCaseCode="+ServCaseCode+"&flag=1","服务契约信息管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			//}
		        //if(code=="1")
		        //{
		        //	alert("此事件类型不能进行服务契约信息管理!");
		       // }
		 }

}
function CaseExecManage()
{
	  if (checkStatus() == false)
	  return false;
	  var arrSelected = null;
	  arrSelected = new Array();
	  var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHSettingSlipQueryGrid.getChkNo(row);
	        if(tChk == true)
	        {
	        	   aa[xx] = row;
	            arrSelected[xx++] = LHSettingSlipQueryGrid.getRowColData(aa,1);
	            //alert("AA "+arrSelected);
	        }
	  }
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要传输的事件信息！");
			  return false;
		}
		if(aa.length>="1")
		{
	      window.open("./LHServCaseExecManageMain.jsp?flag="+fm.all('ServCaseState').value+"&arrSelected="+arrSelected,"服务事件实施管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');

		}
}
function checkStatus()
{
	var rowNum2=LHSettingSlipQueryGrid. mulLineCount ; //行数
	for(var row2=0; row2 < rowNum2; row2++)
	{
		 var tChk2 =LHSettingSlipQueryGrid.getChkNo(row2);
	   if(tChk2 == true)
	   {
	  	  //alert(LHSettingSlipQueryGrid.getRowColData(row2,6));
	  	  if(LHSettingSlipQueryGrid.getRowColData(row2,6)=="0")
	  	  {
	  	      alert("未进行事件状态启动设置的信息，不能进行实施管理!");
	    	    return false;
	  	  }
	   }
	 }
  return true;
}
function ServCaseRun()
{
	//事件是否都为未启动的
	if (checkState() == false)
	return false;

	//针对结算事件，其下项目所关联事件是否都已结束
	if (checkFeeCase() == false)
	return false;
	var arrSelected = null;
	arrSelected = new Array();
	var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数
	var aa = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{   
		var flagl = true; 
		var tChk =LHSettingSlipQueryGrid.getChkNo(row);
		if(tChk == true)
	    {
			//若未关联服务信息，不能启动
			if( getRelaInfo(row)==false)
			
			{   								
				 var sqla = "select  a.servcasetype, a.servcasedes from lhservcasedef a  where  "
                             +" a.servcasecode='"+LHSettingSlipQueryGrid.getRowColData(row,1)+"' with ur ";
                 var mm = easyExecSql(sqla);
                 //alert(mm);
                 if(mm=="2,015"||mm=="2,006"){
                 	  flagl=false;  
                 	  if(!confirm(LHSettingSlipQueryGrid.getRowColData(row,1)+"事件未关联服务信息，确定要启动吗？")){
                 	  	 return false;
                 	  }   
	             }else{
	             	  alert(LHSettingSlipQueryGrid.getRowColData(row,1)+"号事件未关联服务信息，无法启动");
				      return false;
			     }
			}
			aa[xx] = row;
	    	//alert("aa  ----"+aa);
			if(flagl == true){
	        arrSelected[xx++] = LHSettingSlipQueryGrid.getRowColData(row,1);
	        //alert("AA "+arrSelected);
	        }
	    }
	}

	if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
	{
		  alert("请选择要传输的事件信息！");
		  return false;
	}
	if(aa.length>="1")
	{
      //alert("准备要启动");
	  updateClick();
	  if(arrSelected !=""){
	     window.open("./LHServCaseExecManageMain.jsp?arrSelected="+arrSelected+"","服务事件实施管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	  }
	}
}

function getRelaInfo(row)
{
	var sql  = " select count(servcasecode) from LHServCaseRela where servcasecode = '"+ LHSettingSlipQueryGrid.getRowColData(row,1)+"' ";
	if(easyExecSql(sql)=="0")
		return false;
}

function checkState()
{
	var rowNum2=LHSettingSlipQueryGrid.mulLineCount ; //行数
	for(var row2=0; row2 < rowNum2; row2++)
	{
		 var tChk2 =LHSettingSlipQueryGrid.getChkNo(row2);
	   if(tChk2 == true)
	   {
	  	  //alert(LHSettingSlipQueryGrid.getRowColData(row2,6));
	  	  if(LHSettingSlipQueryGrid.getRowColData(row2,6)!="0")
	  	  {
	  	      alert("您选择的事件不全为未启动事件!");
	    	    return false;
	  	  }
	   }
	 }
  return true;
}

function checkFeeCase()
{
	if(fm.all('ServCaseType').value == "3")
	{
		for(var row=0; row < LHSettingSlipQueryGrid.mulLineCount; row++)
		{
			var tChk =LHSettingSlipQueryGrid.getChkNo(row);
			if(tChk == true)
			{
				//alert(LHSettingSlipQueryGrid.getRowColData(row2,6));
				if(checkFeeCaseRela(row) == false)
				{return false;}
				if(checkFeeCaseStart(row) == false)
				{return false;}
			}
			//alert("可以启动");return false;
		}
	}
}

function checkFeeCaseRela(  row)
{
	var sql = " select * from LHServCaseRela where ServCaseCode = '"+LHSettingSlipQueryGrid.getRowColData(row,1)+"' ";
	var re = easyExecSql(sql);
	if(re=="null" || re=="" || re==null)
	{
		alert("第"+(row+1)+"行的'"+LHSettingSlipQueryGrid.getRowColData(row,2)+"' 费用结算事件尚未关联服务信息！");
		return false;
	}
	return true;
}

function checkFeeCaseStart(row)
{
	var sql = " select distinct servitemno,ServCaseCode from LHServCaseRela where ServCaseCode = '"+LHSettingSlipQueryGrid.getRowColData(row,1)+"' ";
	var re = easyExecSql(sql);
	for(var i = 0; i < re.length; i++)
	{

		var sql = " SELECT A.ServCaseCode,A.ServCaseName FROM LHSERVCASEDEF A, LHSERVCASERELA B "
				 +" WHERE  A.SERVCASECODE = B.SERVCASECODE AND B.SERVITEMNO = '"+re[i][0]+"'"
				 +" AND    A.SERVCASESTATE IN ('0','1') AND A.SERVCASETYPE <> '3'  fetch first 1 rows only "
				;
		var arrNoCase = easyExecSql(sql);
		if( arrNoCase != "null" && arrNoCase!="" && arrNoCase!= null)
		{
			alert("在第"+(row+1)+"行的'"+LHSettingSlipQueryGrid.getRowColData(row,2)+"' 费用结算事件中，\n它的某个服务项目所在的 '"+arrNoCase[0][0]+"-"+arrNoCase[0][1]+"' 服务事件尚未结束");
			return false;
		}
	}
}

function getQueryResult()
{
	 var arrSelected = null;
	 arrSelected = new Array();
	 var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数
	 var aa = new Array();
	 var xx = 0;
	 for(var row=0; row < rowNum; row++)
	 {
	     var tChk =LHSettingSlipQueryGrid.getChkNo(row);
	     if(tChk == true)
	     {
	     	   aa[xx++] = row;
	         arrSelected = LHSettingSlipQueryGrid.getRowColData(aa,1);
	         var ServCaseCode =LHSettingSlipQueryGrid.getRowColData(aa,1);
	         //alert("AAAAAAAAA  "+arrSelected);
	     }
	 }

}
function updateClick()
{
  //下面增加相应的代码
  //if (confirm("您确实要启动服务事件吗?"))
  //{
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.all('fmtransact').value = "UPDATE||MAIN";
      fm.action="./LHSettingSlipQuerySave.jsp";
      fm.submit(); //提交
      showInfo.close();
  //}
  //else
  //{
    // alert("您取消了启动操作！");
    // return false;
  //}
}
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}
function CaseStepUP()
{
	//alert("AA");
	window.open("./LHEventTypeSettingMain.jsp","事件启动自动设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}
function StandardTask()
{
	 window.open("./LHStandardTaskDefineMain.jsp","标准服务任务定义",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}
function ReturnResult()
{
	var arrReturn = new Array();
	var line = LHSettingSlipQueryGrid.mulLineCount;
	var j = 0;

	for (var i = 0; i < line; i++)
	{
  	var tSel = LHSettingSlipQueryGrid.getChkNo(i);
  	if (tSel == true)
  	{
  	  	j=j+1;
  	  	checkedrow = i;
  	}
  	if (j > 1)
  	{
  	  	alert("只能选择一条记录!")
  	  	return false;
  	}
  }
	//alert(checkedrow);
	if( checkedrow == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{

			try
			{
				//alert(tSel);
				arrReturn = getQueryResult();

				top.opener.afterQuery( arrReturn );


			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();

	}
}
function getQueryResult()
{
	var arrSelected = null;
 // tRow = LHSettingSlipQueryGrid.getSelNo();
//	if( tRow == 0 || tRow == null )
//	    return arrSelected;

	arrSelected = new Array();

	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
  arrSelected[0] = LHSettingSlipQueryGrid.getRowData(checkedrow);

	//arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}


function afterCodeSelect(codeName,Field)
{
	//alert(codeName);
	if(codeName == "eventstate")
	{
		 initLHSettingSlipQueryGrid() ;
		 if(Field.value=="0")
		 {
		 	  divLHServItemInfo.style.display='';
		 }
		 else
		 {
		 	  divLHServItemInfo.style.display='none';
		 }
	}
	if(codeName == "hmhospitalmng")
	{
		if(fm.all('MngCom').value == "8600")
		{
			fm.all('MngCom').value = "86";
		}
	}
}
