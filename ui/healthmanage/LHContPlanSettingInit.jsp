<%
//程序名称：LHContPlanSettingInit.jsp
//程序功能：
//创建日期：2006-06-26  12:47
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     String flag = request.getParameter("flag");
     String  ContNo2 = request.getParameter("ContNo");
     String  CustomerNo2 = request.getParameter("CustomerNo");
     String  prtNo = request.getParameter("prtNo");

%>                            
<script language="JavaScript">
var allflag = "<%=flag%>";
function initInpBox()
{ 
  try
  {                                 
    fm.all('GrpServPlanNo').value = "";
    fm.all('ServPlanNo').value = "";
    fm.all('ContNo').value = "";
    fm.all('CustomerNo').value = "";
    fm.all('Name').value = "";
    fm.all('ServPlanCode').value = "";
    fm.all('ServPlanName').value = "";
    fm.all('ServPlanLevel').value = "";
    fm.all('ServPlayType').value = "";
    fm.all('ComID').value = "";
    fm.all('StartDate').value = "";
    fm.all('EndDate').value = "";
    fm.all('ServPrem').value = "";
    fm.all('ManageCom').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
   
    fm.all('HiddenCustom').value="<%=CustomerNo2%>";
    fm.all('prtNo').value="<%=prtNo%>";
  }
  catch(ex)
  {
    alert("在LHServPlanInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {}
  catch(ex)
  {
    alert("在LHServPlanInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
	try
	{
    	initInpBox();
    	initLHServPlanGrid();
    	initLHItemPriceFactorGrid() ;
    	if(fm.all('HiddenCustom').value!="" && fm.all('HiddenCustom').value!=null && fm.all('HiddenCustom').value!="null")//有客户编号传入时
    	{
			if("<%=flag%>" == "NCONT")//非健康险种保单标志
	    	{
	    		NQuery();
	    	}
	    	else if("<%=flag%>" == "UCONT")//非标业务标志
	    	{
	    		//alert("UCONT");
	    		UQuery();
	    	}
	    	else
	    	{
	    		//alert("passQuery");
	    		passQuery();//传入的客户号不为空时
	    	}
    	}
    	else
    	{
    		 fm.all('ContNo').value="<%=ContNo2%>";//客户号为空时
    	}
		var flag = "<%=flag%>";
    	if(flag!=""&&flag!="null"&&flag!=null&&flag!="NCONT"&&flag!="UCONT")//团体服务
    	{
    	    fm.all('modifyButton').disabled=true;
    	    fm.all('saveButton').disabled=true;
    	    fm.all('ServTrace').disabled=true;
    	}

		var rowNum=LHServPlanGrid.mulLineCount ; //行数 
		var k;	
		for( var i=0; i<rowNum; i++)
		{
			if(LHServPlanGrid.getRowColData(i,5)=="个人服务事件")
			{
		    	k=(LHServPlanGrid.getRowColData(i,7));
		    	if(k!=""&&k!="null"&&k!=null)
		    	{
		    		  fms.all('saveButton').disabled = true;//如果此页的个人事件编号已经生成，刚保存按钮不可用
		    		  break;
		    	}
			}	 
		}
		if(easyExecSql(" select CaseState from LHServPlan where servplanno = '"+fm.all('ServPlanNo').value+"' ") == "3")
		{
			fms.all('QieSuccess').disabled = true;
		} 
		fms.all('QieStatus').disabled=true;
	}
  catch(re)
  {
    alert("LHContPlanSettingInit.jsp-->InitForm函数中发生异常:初始化界面错误AA!");
  }
}

var LHServPlanGrid;
function initLHServPlanGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="40px";         		//列名
	    iArray[0][3]=1;         		//列名
	    iArray[0][4]="station";         		//列名
	    
	    iArray[1]=new Array(); 
	    iArray[1][0]="服务项目代码";   
	    iArray[1][1]="70px";   
	    iArray[1][2]=20;        
	    iArray[1][3]=0;
	    
	    iArray[2]=new Array(); 
	    iArray[2][0]="服务项目名称";   
	    iArray[2][1]="110px";   
	    iArray[2][2]=20;        
	    iArray[2][3]=0;
	    
	    iArray[3]=new Array(); 
	    iArray[3][0]="个人服务项目号码";   
	    iArray[3][1]="0px";   
	    iArray[3][2]=20;        
	    iArray[3][3]=3;
	    
	    iArray[4]=new Array(); 
	    iArray[4][0]="服务项目序号";   
	    iArray[4][1]="70px";   
	    iArray[4][2]=20;        
	    iArray[4][3]=0;
	    
	    iArray[5]=new Array(); 
        iArray[5][0]="服务事件类型";                             
        iArray[5][1]="80px";                     
        iArray[5][2]=20;                                  
        iArray[5][3]=1;      
        //iArray[5][4]="eventtype";
        iArray[5][5]="5|6";     //引用代码对应第几列，'|'为分割符
        iArray[5][6]="1|0";     //上面的列中放置引用代码中第几位值
        iArray[5][14]="个人服务事件";
        iArray[5][17]="1"; 
        iArray[5][18]="160";
        iArray[5][19]="1" ;
	        
	                
        iArray[6]=new Array(); 
	    iArray[6][0]="eventtype";   
	    iArray[6][1]="0px";   
	    iArray[6][2]=20;        
	    iArray[6][3]=3;
	    iArray[6][14]="1";
	    
	    iArray[7]=new Array(); 
	    iArray[7][0]="服务事件编号";   
	    iArray[7][1]="100px";   
	    iArray[7][2]=20;        
	    iArray[7][3]=0;
	    iArray[7][14]="1";
	    
	    
	    iArray[8]=new Array(); 
	    iArray[8][0]="服务事件名称";   
	    iArray[8][1]="280px";   
	    iArray[8][2]=20;        
	    iArray[8][3]=0;
	    iArray[8][14]="1";
	    
	    iArray[9]=new Array(); 
	    iArray[9][0]="服务事件状态";   
	    iArray[9][1]="80px";   
	    iArray[9][2]=20;        
	    iArray[9][3]=1;
	    iArray[9][14]="1";
	                            
        LHServPlanGrid = new MulLineEnter( "fm" , "LHServPlanGrid" );           
        //这些属性必须在loadMulLine前                          
        //alert(fm.all('HiddenCustom').value); 
        if(fm.all('HiddenCustom').value!=""&&fm.all('HiddenCustom').value!="null"&&fm.all('HiddenCustom').value!=null)
        {            
        	LHServPlanGrid.mulLineCount = 0;                             
        	LHServPlanGrid.displayTitle = 1;                          
        	LHServPlanGrid.hiddenPlus = 1;                          
        	LHServPlanGrid.hiddenSubtraction = 1;                          
        	LHServPlanGrid.canSel = 1;                          
        	LHServPlanGrid.canChk = 0;                          
        	LHServPlanGrid.selBoxEventFuncName = "showOne";                       
        } 
        else
        {
    		LHServPlanGrid.mulLineCount = 1; 
    		LHServPlanGrid.displayTitle = 1;                          
            LHServPlanGrid.hiddenPlus = 0;                          
            LHServPlanGrid.hiddenSubtraction = 0;                          
            LHServPlanGrid.canSel = 1;                          
            LHServPlanGrid.canChk = 0;           
		}                                                       
    	LHServPlanGrid.loadMulLine(iArray);                                     
    	//这些操作必须在loadMulLine后面                                         
    	//LHServPlanGrid.setRowColData(1,1,"asdf");                                   
	}
    catch(ex) {alert(ex);}
}
function dispaly()
{
	// eval("submenu1.style.display='';");

}
var LHItemPriceFactorGrid;
function initLHItemPriceFactorGrid() 
{                            
	var iArray = new Array();
	try 
	{   
    	iArray[0]=new Array();                                
    	iArray[0][0]="序号";         		//列名               
    	iArray[0][1]="30px";         		//列名               
    	iArray[0][3]=0;         				//列名                 
    	iArray[0][4]="";         //列名   
    
	    iArray[1]=new Array(); 
		iArray[1][0]="服务事件编号";   
		iArray[1][1]="60px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;
	   
  
	    iArray[2]=new Array(); 
		iArray[2][0]="服务事件名称";   
		iArray[2][1]="60px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="服务事件状态";   
		iArray[3][1]="60px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;
	
	   	LHItemPriceFactorGrid = new MulLineEnter( "fms" , "LHItemPriceFactorGrid" );                         
	    //这些属性必须在loadMulLine前                                                                                              
	                                                                                                              
	    LHItemPriceFactorGrid.mulLineCount = 0;                                          
	    LHItemPriceFactorGrid.displayTitle = 1;                                          
	    LHItemPriceFactorGrid.hiddenPlus = 0;                                            
	    LHItemPriceFactorGrid.hiddenSubtraction = 0;                                     
	    LHItemPriceFactorGrid.canSel = 1;                                                
	                                            
	    //LHItemPriceFactorGrid.selBoxEventFuncName = "showOne";                         
	             ;                                                             
	    LHItemPriceFactorGrid.loadMulLine(iArray);                                       
	    //这些操作必须在loadMulLine后面                                          
	    ////LHItemPriceFactorGrid.setRowColData(1,1,"asdf");                                                                                                
	}                                                                          
  	catch(ex) {alert(ex);}
} 
</script>
	<script>
		//通过综合查询进入此页面的处理
		var turnPage = new turnPageClass(); 
		function passQuery()
		{ //进入此函数
			//alert(fm.all('saveButton').disabled);
			var ContNo = "<%=request.getParameter("ContNo")%>";
			var CustomerNo = "<%=request.getParameter("CustomerNo")%>";

			if(ContNo!='null'||ContNo>0) 
			{
				var strSqlInit = "select grpcontno,contno,insuredno,insuredname,Managecom from lccont "
								+"where  contno = '"+ContNo+"'"
                        		;
              	var arrResultInit = easyExecSql(strSqlInit);

              	fm.all('GrpContNo').value = arrResultInit[0][0];
              	fm.all('ContNo').value = arrResultInit[0][1];
              
              	//针对家庭单的处理
              	if(CustomerNo == arrResultInit[0][2] || CustomerNo == "null")
              	{
              		fm.all('CustomerNo').value = arrResultInit[0][2];
              		fm.all('Name').value = arrResultInit[0][3];
              	}
	            else
	            {
	              	fm.all('CustomerNo').value = CustomerNo;
	              	fm.all('Name').value = easyExecSql("select name from ldperson where CustomerNo = '"+CustomerNo+"'");
	            }
				fm.all('ComID').value = arrResultInit[0][4].substring (0,4);

//              var strSqlInit1 =" select distinct ServPlanName,ServPlanLevel,ServPlanCode,ServPlayType from LHHealthServPlan "
//                              +" where servplancode = ( select riskcode from lcpol where contno = '"+ContNo+"' and insuredno = '"+CustomerNo+"' and riskcode in (select distinct riskcode from lmriskapp where  risktype2 = '5'))"                                                 
//	                          +" and servplanlevel = char((select integer(mult)  from lcpol  where  contno = '"+ContNo+"' and insuredno = '"+CustomerNo+"' and riskcode  in (select distinct riskcode from lmriskapp where  risktype2 = '5')))"                                                           
//                             ;
//             alert(strSqlInit1);
//			   var arrResultInit1 = easyExecSql(strSqlInit1);
//             if((arrResultInit1)==null)
//             {
//             	alert("请先录入该保单下的服务计划");
//             }
               
              var strSqlInit1 = " select distinct ServPlanName,ServPlanLevel,ServPlanCode,ServPlayType from LHServPlan "
              				   +" where  contno = '"+ContNo+"' and CustomerNo = '"+CustomerNo+"'  and ServPlayType <> '1H'"
              				   ;
              var arrResultInit1 = easyExecSql(strSqlInit1);  
              if((arrResultInit1)==null)                      
              {                                                
              	alert("契约数据未保存");           
              }                                                
              
              fm.all('ServPlanName').value = arrResultInit1[0][0];
              fm.all('ServPlanLevel').value = arrResultInit1[0][1];
              fm.all('ServPlanCode').value = arrResultInit1[0][2];
              fm.all('ServPlayType').value = arrResultInit1[0][3];
              
      	      
      	      //看此Contno在LHServPlan是否已保存过
      	      var sql = " select ServPlanno from LHServPlan where ContNo = '"+arrResultInit[0][1]+"' and CustomerNo = '"+CustomerNo+"'  and ServPlayType <> '1H'";
      	      var arr_ex = easyExecSql(sql);
				//alert(sql);alert(easyExecSql(sql));
      	      if(arr_ex == null || arr_ex == "" || arr_ex == "null")
      	      {
				var strSqlInit2 = " select servitemcode, servitemname,'',servitemsn,'个人服务事件','1','','','0'"
							      +" from LHHealthServPlan a"
							      +" where ServPlanCode = '"+arrResultInit1[0][2]+"'"
							      +" and servitemcode not in('006','015')"
							      +" and comid = '"+arrResultInit[0][4].substring (0,4)+"'"
							      +" and ServPlanLevel = '"+arrResultInit1[0][1]+"'";    
					turnPage.pageLineNum = 50;
      	      	  turnPage.queryModal(strSqlInit2, LHServPlanGrid); 

				//结束
      	      }
      	      else
      	      {
      	      	  var sqlSqlInit4 = " select StartDate, EndDate, servprem,GrpServPlanNo,MakeDate,MakeTime from LHServPlan where servplanno = '"+arr_ex[0][0]+"' "
      	      	  var arr_4 = easyExecSql(sqlSqlInit4);
      	      	  fm.all('StartDate').value = arr_4[0][0];
      	      	  fm.all('EndDate').value = arr_4[0][1];
      	      	  fm.all('ServPrem').value = arr_4[0][2];
      	      	  fm.all('ServPlanNo').value = arr_ex;
      	      	  fm.all('GrpServPlanNo').value = arr_4[0][3];
      	      	  fm.all('MakeDate').value = arr_4[0][4];
      	      	  fm.all('MakeTime').value = arr_4[0][5];
      	      	 
      	      	  var sql2 = " select distinct ServPlanno from LHServCaseRela where ServPlanNo = '"+fm.all('ServPlanNo').value+"'";
      	          var arr_ex2 = easyExecSql(sql2);
 					//alert(sql2);alert(arr_ex2);
      	      	  if(arr_ex2 == null || arr_ex2 == "" || arr_ex2 == "null")
      	      	  {
      	      	  	  var strSqlInit6 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
      	      					          +" a.servitemno, a.servitemtype,"
      	      					          +" (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end)"
      	      					          +" ,a.ServCaseType,"
      	      					          +"'',"
      	      					          +"'',"
      	      					          +"'0' "
      	      					          +" from lhservitem a where a.servplanno = '"+arr_ex[0][0]+"'"
      	      					          +" and servitemcode not in('006','015')"
      	      					     ;//alert(strSqlInit6);alert(easyExecSql(strSqlInit6))
      	      		  turnPage.pageLineNum = 50;
      	      	      turnPage.queryModal(strSqlInit6, LHServPlanGrid); 
      	      	  }
      	      	  else
      	      	  {
      	      	         var strSqlInit3 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
      	      					          +" a.servitemno, a.servitemtype,"
      	      					          +" (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end)"
      	      					          +" ,a.ServCaseType,"
      	      					          +"(select b.ServCaseCode from LHServCaseRela  b where b.ServItemNo=a.servitemno  ),"
      	      					          +"(select distinct c.ServCaseName from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ),"
      	      					          +"(select distinct c.ServCaseState from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ) "
      	      					          +" from lhservitem a where a.servplanno = '"+arr_ex[0][0]+"'"
      	      					           + " and servitemcode not in('006','015')"
      	      					     ;//alert(strSqlInit3);alert(easyExecSql(strSqlInit3))
      	      			turnPage.pageLineNum = 50;
      	      	         turnPage.queryModal(strSqlInit3, LHServPlanGrid); 
      	      	  }
      	      }
      	      	
      	      	
      	      
			 } 
			 else 
			 {
			  	  alert("请先到保险合同查询选择未录入保单!");
			 }
	}	
	
	
	//处理核保费用结算
	function NQuery()
	{
		var ContNo = "<%=request.getParameter("ContNo")%>";
		var CustomerNo = "<%=request.getParameter("CustomerNo")%>";
		if(ContNo!='null' && ContNo != null && ContNo != "") 
		{
			//查询保单信息
			var strSqlInit = "select grpcontno,contno,insuredno,insuredname,Managecom from lccont where  contno = '"+ContNo+
											 "' union select grpcontno,contno,insuredno,insuredname,Managecom from LBCont where  contno = '"+ContNo+"'" ;
			var arrResultInit = easyExecSql(strSqlInit);

			fm.all('GrpContNo').value = arrResultInit[0][0];
      fm.all('ContNo').value = arrResultInit[0][1];
      fm.all('ComID').value = arrResultInit[0][4].substring (0,4);
        
      //针对家庭单的处理
      if(CustomerNo == arrResultInit[0][2] || CustomerNo == "null")
      {
      	fm.all('CustomerNo').value = arrResultInit[0][2];
        fm.all('Name').value = arrResultInit[0][3];
      }else
      {
	      fm.all('CustomerNo').value = CustomerNo;
	      fm.all('Name').value = easyExecSql("select name from ldperson where CustomerNo = '"+CustomerNo+"'");
			}
			
			//判断健管契约信息是否已存
			var strSqlInit1 = " select distinct ServPlanName,ServPlanLevel,ServPlanCode,ServPlayType from LHServPlan "
              				   +" where  contno = '"+ContNo+"' and CustomerNo = '"+CustomerNo+"' and ServPlayType='1H'";
			var arrResultInit1 = easyExecSql(strSqlInit1);  
			if((arrResultInit1)==null || (arrResultInit1)== "" || (arrResultInit1)=="null")                      
			{                                                
				alert("契约数据未保存");           
      }                                                
      fm.all('ServPlanName').value = arrResultInit1[0][0];
      fm.all('ServPlanLevel').value = arrResultInit1[0][1];
      fm.all('ServPlanCode').value = arrResultInit1[0][2];
      fm.all('ServPlayType').value = arrResultInit1[0][3];
        
      //看此Contno在LHServPlan是否已保存过
      var sql = " select ServPlanno from LHServPlan where ContNo = '"+arrResultInit[0][1]+"' and CustomerNo = '"+CustomerNo+"' and ServPlayType='1H'";
      var arr_ex = easyExecSql(sql);//alert(arr_ex)
      if(arr_ex == null || arr_ex == "" || arr_ex == "null")
      {
				var strSqlInit2 = " select servitemcode, servitemname,'',servitemsn,'个人服务事件','1','','','0'"
				      			+" from LHHealthServPlan where ServPlanCode = '"+arrResultInit1[0][2]+"'"
										+" and comid = '"+arrResultInit[0][4].substring (0,4)+"'"
						      	+" and ServPlanLevel = '"+arrResultInit1[0][1]+"'"
						      	;   
				turnPage.pageLineNum = 50;  
				turnPage.queryModal(strSqlInit2, LHServPlanGrid); 
		  }else
      {
      	var sqlSqlInit4 = " select StartDate, EndDate, servprem,GrpServPlanNo,MakeDate,MakeTime from LHServPlan where servplanno = '"+arr_ex[0][0]+"' "
      	var arr_4 = easyExecSql(sqlSqlInit4);
      	fm.all('StartDate').value = arr_4[0][0];
      	fm.all('EndDate').value = arr_4[0][1];
      	fm.all('ServPrem').value = arr_4[0][2];
      	fm.all('ServPlanNo').value = arr_ex;
      	fm.all('GrpServPlanNo').value = arr_4[0][3];
      	fm.all('MakeDate').value = arr_4[0][4];
      	fm.all('MakeTime').value = arr_4[0][5];
      	
      	
      	//对事件信息进行关联
      	var sqlCaseRela = " select ServPlanno from LHServCaseRela where ServPlanNo = '"+fm.all('ServPlanNo').value+"'";
      	var arrCaseRela = easyExecSql(sqlCaseRela);
      	if(arrCaseRela == null || arrCaseRela == "" || arrCaseRela == "null")
      	{
      		var strSqlInit6 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
      					     +" a.servitemno, a.servitemtype, (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end)"
      					     +" ,a.ServCaseType,'','','0'  from lhservitem a where a.servplanno = '"+arr_ex[0][0]+"'"
      					     ;
      		turnPage.pageLineNum = 50;
					turnPage.queryModal(strSqlInit6, LHServPlanGrid); 
      	}else
      	{
      	  var strSqlInit3 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
      	            +" a.servitemno, a.servitemtype, (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end)"
      	            +" ,a.ServCaseType,(select b.ServCaseCode from LHServCaseRela  b where b.ServItemNo=a.servitemno  ),"
      	            +"(select distinct c.ServCaseName from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ),"
      	            +"(select distinct c.ServCaseState from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ) "
      	            +" from lhservitem a where a.servplanno = '"+arr_ex[0][0]+"'"
      	  			;
      	  turnPage.pageLineNum = 50;
      	  turnPage.queryModal(strSqlInit3, LHServPlanGrid); 
      	}
			}
    }else 
		{
			alert("请先到保险合同查询选择未录入保单!");
		}
	}	
function UQuery()
{ //进入此函数
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var CustomerNo = "<%=request.getParameter("CustomerNo")%>";
	//alert(ContNo);
	if(ContNo!='null'||ContNo>0) 
	{
		fm.all('ContNo').value = ContNo;
          	fm.all('CustomerNo').value = CustomerNo;
          	fm.all('Name').value = easyExecSql("select name from ldperson where CustomerNo = '"+CustomerNo+"'");
        
		var strSqlInit1 = " select distinct ServPlanName,ServPlanLevel,ServPlanCode,ServPlayType,ComID from LHServPlan "
      				   +" where  contno = '"+ContNo+"' and CustomerNo = '"+CustomerNo+"'  and ServPlayType <> '1H'"
      				   ;
        var arrResultInit1 = easyExecSql(strSqlInit1);  
        if((arrResultInit1)==null)                      
        {                                                
        	alert("契约数据未保存");           
        }                                                
        
        fm.all('ServPlanName').value = arrResultInit1[0][0];
        fm.all('ServPlanLevel').value = arrResultInit1[0][1];
        fm.all('ServPlanCode').value = arrResultInit1[0][2];
        fm.all('ServPlayType').value = arrResultInit1[0][3];
        fm.all('ComID').value = arrResultInit1[0][4];
        
  	    
  	    //看此Contno在LHServPlan是否已保存过
  	    var sql = " select ServPlanno from LHServPlan where ContNo = '"+ContNo+"' and CustomerNo = '"+CustomerNo+"'  and ServPlayType <> '1H'";
  	    var arr_ex = easyExecSql(sql);
  	    if(arr_ex == null || arr_ex == "" || arr_ex == "null")
  	    {
		  var strSqlInit2 = " select servitemcode, servitemname,'',servitemsn,'个人服务事件','1','','','0'"
		  			      +" from LHHealthServPlan"
		  			      +" where ServPlanCode = '"+arrResultInit1[0][2]+"'"
		  			      +" and comid = '"+arrResultInit1[0][4].substring(0,4)+"'"
		  			      +" and ServPlanLevel = '"+arrResultInit1[0][1]+"'"
		  			   ;     
		  	  turnPage.pageLineNum = 50;
  	    	  turnPage.queryModal(strSqlInit2, LHServPlanGrid); 
        
		  //结束
  	    } 
  	    else
  	    {
			var sqlSqlInit4 = " select StartDate, EndDate, servprem,GrpServPlanNo,MakeDate,MakeTime from LHServPlan where servplanno = '"+arr_ex[0][0]+"' "
  	  	    var arr_4 = easyExecSql(sqlSqlInit4);
  	  	    fm.all('StartDate').value = arr_4[0][0];
  	  	    fm.all('EndDate').value = arr_4[0][1];
  	  	    fm.all('ServPrem').value = arr_4[0][2];
  	  	    fm.all('ServPlanNo').value = arr_ex;
  	  	    fm.all('GrpServPlanNo').value = arr_4[0][3];
  	  	    fm.all('MakeDate').value = arr_4[0][4];
  	  	    fm.all('MakeTime').value = arr_4[0][5];
  	  	    
  	  	    var sql2 = " select distinct ServPlanno from LHServCaseRela where ServPlanNo = '"+fm.all('ServPlanNo').value+"'";
  	        var arr_ex2 = easyExecSql(sql2);
			//alert(sql2);alert(arr_ex2);
  	  	    if(arr_ex2 == null || arr_ex2 == "" || arr_ex2 == "null")
  	  	    {  
				var strSqlInit6 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
  	  		  		+" a.servitemno, a.servitemtype,"
  	  		  		+" (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end)"
  	  		  		+" ,a.ServCaseType,'','','0' "
  	  		  		+" from lhservitem a where a.servplanno = '"+arr_ex[0][0]+"'"
  	  		  			   ;//alert(strSqlInit6);alert(easyExecSql(strSqlInit6))
  	  		  	   turnPage.pageLineNum = 50;
  	  	           turnPage.queryModal(strSqlInit6, LHServPlanGrid); 
  	  	    }
  	  	    else
  	  	    {
				var strSqlInit3 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
  	  		  		+" a.servitemno, a.servitemtype,"
  	  		  		+" (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end)"
  	  		  		+" ,a.ServCaseType,"
  	  		  		+"(select b.ServCaseCode from LHServCaseRela  b where b.ServItemNo=a.servitemno  ),"
  	  		  		+"(select distinct c.ServCaseName from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ),"
  	  		  		+"(select distinct c.ServCaseState from LHServCaseDef  c ,LHServCaseRela  b where c.ServCaseCode=b.ServCaseCode and b.servitemno = a.servitemno ) "
  	  		  		+" from lhservitem a where a.servplanno = '"+arr_ex[0][0]+"'"
				;//alert(strSqlInit3);alert(easyExecSql(strSqlInit3))
					turnPage.pageLineNum = 50;
  	  	           turnPage.queryModal(strSqlInit3, LHServPlanGrid); 
  	  	    }
		}
	} 
	else 
	{
		alert("请先到保险合同查询选择未录入保单!");
	}
}
	</script>
