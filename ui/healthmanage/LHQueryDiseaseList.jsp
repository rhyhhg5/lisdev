<%
//程序名称：LHQueryDiseaseList.jsp
//程序功能：功能描述
//创建日期：2005-12-19 14:47:24
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHQueryDiseaseList.js"></SCRIPT> 
    
  <title>查询结果列表</title>
</head>
<%
   	GlobalInput tG = new GlobalInput(); 
    tG = (GlobalInput)session.getValue("GI");
	String itemname = new String(request.getParameter("itemname").getBytes("ISO8859_1"),"GBK");
	String keyword = new String(request.getParameter("keyword").getBytes("ISO8859_1"),"GBK");
	String itemgroup = request.getParameter("itemgroup");
	System.out.println("+++++++++LHQueryDiseaseList.jsp: itemgroup = "+itemgroup+"+++++++++++++");
%>
<script>
  	var comCode = "<%=tG.ComCode%>";
  	var manageCom = "<%=tG.ManageCom%>";
  	var operator = "<%=tG.Operator%>";
  
  	var itemname = "<%=itemname%>";
  	var keyword = "<%=keyword%>";
  	var itemgroup = "<%=itemgroup%>";
  
	window.focus();
</script>
<body onload="initForm();submitForm();">
		<form action="./LHQueryDiseaseSave.jsp" method=post name=fm target="fraSubmit">
		<br>
			  <table>
			  	<tr>
			      <td class=common>
					    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,diseaseList);">
			  		</td>
			  		<td class=titleImg>
			  			 查询结果
			  		</td>
			  	</tr>
			  </table>
			  <!-- 信息（列表） -->
				<Div id="diseaseList" style="display:''">
			    <table class=common>
			    	<tr class=common>
				  		<td text-align:left colSpan=1>
			  				<span id="spanLHDiseaseListGrid">
			  				</span> 
					    </td>
						</tr>
					</table>
				</div>
			  	
			  <Div id= "divPage" align=center style= "display: '' ">
			    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="pageCalculate(1);"> 
			    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="pageCalculate(2);"> 					
			    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="pageCalculate(3);"> 
			    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="pageCalculate(4);">
			  </Div>
			  <input type = hidden name=itemName>
			  <input type = hidden name=keyWord>
			  <input type = hidden name=startIndex>
			  <input type = hidden name=itemGroup>
		</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>


		<script language="javascript">
			function showDetail()
			{
				var selNo = LHDiseaseListGrid.getSelNo();
				var pageName = LHDiseaseListGrid.getRowColData(selNo-1,1);
				var fullPath = "QueryFile/"+itemgroup+"/"+pageName+".txt";

				window.open("LHQueryDiseaseShow.jsp?fullPath="+fullPath+"&pageName="+pageName,pageName);
			}
		</script>
</html>
 
