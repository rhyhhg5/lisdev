//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var selectedSpecSecDiaRow; 
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function generateFlowNo()
{
		var str = "select max(hospitcode) from ldhospital where hospitcode like '"+fm.AreaCode.value+"%%'";
		var shortFlowNo = easyExecSql(str);
		if(fm.AreaCode.value=="")
		{
				alert("请先输入地区代码");
		}
	  else
	  {
	  	if(shortFlowNo == null || shortFlowNo=="")
				{
						fm.FlowNo.value="001";
						fm.HospitCode.value = fm.AreaCode.value+"001";
					//	fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
				}
				else
				{
						fm.HospitCode.value = parseInt(shortFlowNo)+1;
						fm.FlowNo.value= fm.HospitCode.value.substr(4,3);
			//			fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
				}
		}
}

function inputClear()
{
  	fm.all('HospitalType').value = "";
    fm.all('HospitalType_ch').value = "";
    fm.all('HospitCode').value = "";
//    fm.all('HospitCode').readOnly =true;
    fm.all('HospitName').value = "";
    fm.all('AreaCode').value = "";
    fm.all('AreaName').value = "";
    fm.all('LevelCode').value = "";
    fm.all('LevelCode_ch').value = "";
    fm.all('BusiTypeCode').value = "";
    fm.all('BusiTypeCode_ch').value = "";
    fm.all('CommunFixFlag').value = "";
    fm.all('CommunFixFlag_ch').value = "";
    fm.all('adminisortcode').value = "";
    fm.all('adminisortcode_ch').value = "";
    fm.all('Economelemencode').value = "";
    fm.all('Economelemencode_ch').value = "";
    fm.all('AssociateClass').value = "";
    fm.all('AssociateClass_ch').value = "";
    fm.all('MngCom').value = "";
    fm.all('MngCom_ch').value = "";
    fm.all('DutyItemCode').value = "";
    fm.all('DutyItem').value = "";
    fm.all('ContraState_ch').value = "";
    fm.all('ContraState').value = "";
    fm.all('DutyItemName').value = "";
    fm.all('DutyName').value = "";
}

function mainQuery()
{
	if (manageCom.toString().length == 8)
	   var mlen = manageCom.substring(0,4);
	else
	   var mlen = manageCom ;
	var SpecialCode="";
  if(fm.all('SpecialCode').value != "")
  {
  	  SpecialCode =" and a.HospitCode in ( select distinct   b.HospitCode from  LDHospitalInfoIntro b where   b.SpecialName = '"+fm.all('SpecialCode').value+"')";	
  }
  var Riskcode="";
  if(fm.all('Riskcode').value != "")//赔付险种类别不为空
  {
        Riskcode =" and a.HospitCode in ( select distinct b.HospitalCode from llfeemain b, llcase c "
        		 +" where b.caseno = c.caseno and c.rgtstate in ('09','11','12') and b.caseno in "
                 +" (select  distinct  c.Caseno from llclaimpolicy c where  "
                 +" c.riskcode in ('"+fm.all('Riskcode').value+"')))";	
  }
  var ICDCode="";
  if(fm.all('ICDCode').value != "")//诊断疾病类别不为空
  {
        ICDCode =" and a.HospitCode in ( select distinct b.HospitalCode from llfeemain b, llcase c "
        		 +" where b.caseno = c.caseno and c.rgtstate in ('09','11','12') and b.caserelano  in "
                 +" (select  distinct  c.caserelano  from llcasecure c where  "
                 +" c.Diseasecode in ('"+fm.all('ICDCode').value+"')))";
  }
  var rowNum=LHHighPartGrid. mulLineCount ; //行数 	
  var testSignNo=new Array;
  var testValue=new Array;
  var xx = 0;
  var SumFee="";
  var testAll=new Array;
	for(var row=0; row< rowNum; row++)
	{
		testSignNo=LHHighPartGrid.getRowColData(row,2);//逻辑判断符号
		testValue=LHHighPartGrid.getRowColData(row,3);//比较值
	  testAll[xx++]=testSignNo+testValue;
	}
	testAll=testAll.toString().split(",");
	var sqlSignValue = "";
	for(var j=0; j < testAll.length; j++)
	{
		sqlSignValue= sqlSignValue+" and b.SumFee "+testAll[j]+"";//给取得的值加上连接值
	}
	//alert("WWWWWWWWW "+sqlSignValue.substring(1));
	if(testSignNo!=""&&testSignNo!="null"&&testSignNo!=null)
	{
	  SumFee =" and a.HospitCode in   ( select distinct   b.HospitalCode  from llfeemain b where  "
                 +" 1=1 "
                 +" "+ sqlSignValue.substring(1) +") ";
  }
	if (fm.all('ContraState').value== "" && fm.all('DutyName').value== "" && fm.all('DutyItem').value == "" && fm.all('DutyItemState').value =="")
	{
		 
		    var sql=" select distinct a.HospitCode,a.HospitName from LDHospital a where 1=1 "
	             + getWherePart("a.HospitalType","HospitalType")
	             + getWherePart("a.HospitCode","HospitCode")
	             + getWherePart("a.HospitName","HospitName")
	             + getWherePart("a.AreaCode","AreaCode")
	             + getWherePart("a.AreaName","AreaName")
	             + getWherePart("a.ManageCom","MngCom")
	             + getWherePart("a.CommunFixFlag","CommunFixFlag")
	             + getWherePart("a.AdminiSortCode","AdminiSortCode")
	             + getWherePart("a.EconomElemenCode","EconomElemenCode")
	             + getWherePart("a.LevelCode","LevelCode")
	             + getWherePart("a.BusiTypeCode","BusiTypeCode")
	             + getWherePart("a.AssociateClass","AssociateClass")
	             + SpecialCode
	             + Riskcode
	             + ICDCode
	             + SumFee
	             + " and a.Managecom like '" + mlen +"%%'"
	             ;
	     //alert(sql);
	     turnPage.queryModal(sql, HospitalComGrid);
	}
	else
	{
		//alert();
	   var sql=" select distinct a.HospitCode,a.HospitName from LDHospital a ,LHGroupCont b, LHContItem c where 1=1 and "
	          + " a.HospitCode=b.HospitCode and b.ContraNo=c.ContraNo "
	          + getWherePart("a.HospitalType","HospitalType")
	          + getWherePart("a.HospitCode","HospitCode")
	          + getWherePart("a.HospitName","HospitName")
	          + getWherePart("a.AreaCode","AreaCode")
	          + getWherePart("a.AreaName","AreaName")
	          + getWherePart("a.ManageCom","MngCom")
	          + getWherePart("a.CommunFixFlag","CommunFixFlag")
	          + getWherePart("a.AdminiSortCode","AdminiSortCode")
	          + getWherePart("a.EconomElemenCode","EconomElemenCode")
	          + getWherePart("a.LevelCode","LevelCode")
	          + getWherePart("a.BusiTypeCode","BusiTypeCode")
	          + getWherePart("a.AssociateClass","AssociateClass")
	          + getWherePart("c.contratype","DutyItem")
	          + getWherePart("b.ContraState","ContraState")
	          + getWherePart("c.DutyItemCode","DutyName")
	          + getWherePart("c.DutyState","DutyItemState")
	          + " and a.Managecom like '" + mlen +"%%'"
	          +Riskcode
	          +ICDCode
	          +SumFee
	          ;
	    //alert(sql);
	     turnPage.queryModal(sql, HospitalComGrid);
	}
}

function healthQuery()
{
	 if (manageCom.toString().length == 8)
	   var mlen = manageCom.substring(0,4);
	 else
	   var mlen = manageCom ;
   var Riskcode="";
   if(fm.all('Riskcode').value != "")//赔付险种类别不为空
   {
  	       Riskcode =" and a.HospitCode in ( select distinct m.HospitalCode from llfeemain m, llcase c"
  	                +" where m.caseno = c.caseno and c.rgtstate in in ('09','11','12') "
  	                +" and m.caseno in (select  distinct  n.Caseno from llclaimpolicy n  "
  	                +" where  n.riskcode in ('"+fm.all('Riskcode').value+"')))";	
   }  
   var ICDCode="";
   if(fm.all('ICDCode').value != "")//诊断疾病类别不为空
   {
  	      ICDCode =" and a.HospitCode in ( select distinct b.HospitalCode from llfeemain b, llcase c "
  	      		   +" where m.caseno = c.caseno and c.rgtstate in ('09','11','12') and b.caserelano  in "
  	               +" (select distinct c.caserelano from llcasecure c where  "
  	               +" c.Diseasecode in ('"+fm.all('ICDCode').value+"')))";	
   }
   var rowNum=LHHighPartGrid. mulLineCount ; //行数 	
   var testSignNo=new Array;
   var testValue=new Array;
   var xx = 0;
   var SumFee="";
   var testAll=new Array;
	 for(var row=0; row< rowNum; row++)
	 {
	 	testSignNo=LHHighPartGrid.getRowColData(row,2);//逻辑判断符号
	 	testValue=LHHighPartGrid.getRowColData(row,3);//比较值
	   testAll[xx++]=testSignNo+testValue;
	 }
	 testAll=testAll.toString().split(",");
	 var sqlSignValue = "";
	 for(var j=0; j < testAll.length; j++)
	 {
	 	sqlSignValue= sqlSignValue+" and b.SumFee "+testAll[j]+"";//给取得的值加上连接值
	 }
	 //alert("WWWWWWWWW "+sqlSignValue.substring(1));
	 if(testSignNo!=""&&testSignNo!="null"&&testSignNo!=null)
	 {
	   SumFee =" and a.HospitCode in   ( select distinct   b.HospitalCode  from llfeemain b where  "
                  +" 1=1 "
                  +" "+ sqlSignValue.substring(1) +") ";
   }
		var sql="select distinct a.HospitCode,a.HospitName,b.ContraNo from LDHospital a,LHGroupCont b, LHContItem c where 1=1 and "
		     + " a.HospitCode=b.HospitCode and b.ContraNo=c.ContraNo "
	       + getWherePart("a.HospitalType","HospitalType")
	       + getWherePart("a.HospitCode","HospitCode")
	       + getWherePart("a.HospitName","HospitName")
	       + getWherePart("a.AreaCode","AreaCode")
	       + getWherePart("a.AreaName","AreaName")
	       + getWherePart("a.ManageCom","MngCom")
	       + getWherePart("a.CommunFixFlag","CommunFixFlag")
	       + getWherePart("a.AdminiSortCode","AdminiSortCode")
	       + getWherePart("a.EconomElemenCode","EconomElemenCode")
	       + getWherePart("a.LevelCode","LevelCode")
	       + getWherePart("a.BusiTypeCode","BusiTypeCode")
	       + getWherePart("a.AssociateClass","AssociateClass")
	       + getWherePart("c.contratype","DutyItem")
	       + getWherePart("b.ContraState","ContraState")
	       + getWherePart("c.DutyState","DutyItemState")
	       + " and a.Managecom like '" + mlen +"%%'"
	       + Riskcode   
	       + ICDCode
	       + SumFee
	       ;
	       //alert(sql);
	       turnPage.queryModal(sql, HospitalComGrid);
}

function ToHospitalInfo()
{
      if (HospitalComGrid.getSelNo() >= 1)
			{     
				    // alert(HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,1));
					  if(HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,1)=="")
					  {
							  alert("你选择了空的列，请重新选择!");
								return false;
					  }
						else
						{
							   var ContraNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,3);
							  
							   if(ContraNo==""||ContraNo=="null"||ContraNo==null)
							   {
							     	var HospitalNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,1);
							     	var HospitalName = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,2);
							     //	alert(HospitalName);
						        window.open("./LDHospitalInputMain.jsp?HospitalNo="+HospitalNo+"&flag=1","医疗机构信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			          }
			      }
			} 
			else
			{
					alert("请选择一条要传输的记录！");    
			}

}

function ToCompactInfo()
{
	if (HospitalComGrid.getSelNo() >= 1)
	{     
		// alert(HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,1));
		if(HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,3)=="")
		{
			alert("请选择正确的合同编号!");
			return false;
		}
		else
		{
			var ContNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,3);
			if(ContNo!=""||ContNo!="null"||ContNo!=null)
			{
				var HospitalNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,1);
				var ContraNo = HospitalComGrid.getRowColData(HospitalComGrid.getSelNo()-1,3);
				//alert(ContraNo);
				window.open("./LHGroupContInputMain.jsp?HospitalNo="+HospitalNo+"&ContraNo="+ContraNo+"&ContFlag=1","医疗机构信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			}
		}
	} 
	else
	{
		alert("请选择一条要传输的记录！");    
	}
}

function SpeciOfHospit()
{
	 var arrSelected = null;
	 var tSel = HospitalComGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		{
			alert( "请先选择一条记录，再点击返回按钮。" );
			return false;
		}
	else
	{		
			try
			{	
	      var HospitalCode = HospitalComGrid.getRowColData(tSel-1,1);
	  	  window.open("./LHHospitalInfoMain.jsp?HospitalCode="+HospitalCode,"医疗机构详细信息",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			
		
	}
}
function afterCodeSelect(codeName,Field)
{
	//alert(codeName);
	if(codeName=="searchesmode" )
	{
		if(Field.value=="1")//按险种;按疾病
		{
			HighTypeGrid.style.display="";
			fm.ICDName.style.display='';
			fm.Riskname.style.display='';
		}
		//alert(Field.value);
		if(Field.value=="2")//不按险种;按疾病
		{
			fm.ICDName.style.display='';
			fm.Riskname.style.display='none';
		}
		if(Field.value=="3")//按险种;不按疾病
		{
			fm.ICDName.style.display='none';
			fm.Riskname.style.display='';
		}
		if(Field.value=="4")//不按险种/疾病
		{
			HighTypeGrid.style.display="none";
			fm.ICDName.style.display='none';
			fm.Riskname.style.display='none';
		}
	}
}

function ExportHospital()
{
	//if (fm.all('ContraState').value== "" && fm.all('DutyName').value== "" && fm.all('DutyItem').value == "" && fm.all('DutyItemState').value =="")
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./HospitalSearchExp.jsp";
	fm.target = "f1print";
	fm.submit();
	showInfo.close();
}