<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHGrpServPlanSave.jsp
//程序功能：
//创建日期：2006-03-09 11:34:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数

    OLHPersonCaseUI tOLHPersonCaseUI  = new OLHPersonCaseUI();    
    LHServCaseRelaSet tLHServCaseRelaSet = new LHServCaseRelaSet();	
  
	String tServPlanLevel[] = request.getParameterValues("LHGrpServPlanGrid1");					//MulLine的列存储数组
	String tServPlanNo[] = request.getParameterValues("LHGrpServPlanGrid3");					//MulLine的列存储数组
	String tServCaseType[] = request.getParameterValues("LHGrpServItemGrid4");
	String tGrpServItemNo[] = request.getParameterValues("LHGrpServItemGrid5");	
	  
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    
  	int LHGrpServItemCount = tServCaseType.length;  
  	int j = 0;
  	int[] arr = new int[20];
  	arr[0]=-1;
  	Content = " 不存在个人服务事件! ";
	FlagStr = "Success";
	
	//循环检查Mulline每行，记录为“个人服务事件”的行
  	for (int i = 0; i < LHGrpServItemCount; i++)
  	{
  	    String a = "";  
  	    try
		{
			System.out.println("tServCaseType[i]"+tServCaseType[i]);
			a = new String(tServCaseType[i].getBytes("ISO8859_1"), "GBK");//66用
			//a = tServCaseType[i];//正式机用
			System.out.println("a"+a);
		}
		catch(Exception e){} 
  	     
  	    if(a.equals("个人服务事件"))
		arr[j++]=i;                    
  	} 

    j=0;
    for (int i = 0; i < LHGrpServItemCount; i++)
    {
		if (arr[j] == i)
		{
			//根据每个团体服务项目查询其下个人项目的流水号是否在事件表（LHServCaseRela）出现，如果存在未出现的，则有未生成个人事件的
        	LHServItemDB tLHServItem = new LHServItemDB();
        	LHServItemSet mLHServItemSet = new LHServItemSet();
        	String sql = "select ServItemNo from LHServItem where GrpServItemNo ='"+tGrpServItemNo[i]+"' and ServItemNo not in (select ServItemNo from LHServCaseRela)";
			mLHServItemSet = tLHServItem.executeQuery(sql);
			System.out.println("--------mLHServItemSet.size()-------"+mLHServItemSet.size());
			if (mLHServItemSet.size() == 0)
			{
				Content = " 均已生成个人事件! ";
				FlagStr = "Success";
//				   continue;			  
           		j++;
			}
			else
			{
        		TransferData StartEnd = new TransferData();
   	    		StartEnd.setNameAndValue("tGrpServItemNo",tGrpServItemNo[i]);        
//		        LHServCaseRelaSchema tLHServCaseRelaSchema = new LHServCaseRelaSchema();
//				tLHServCaseRelaSchema.setServPlanLevel(tServPlanLevel[0]);					
//              tLHServCaseRelaSchema.setContType("2");
//              tLHServCaseRelaSchema.setGrpContNo(request.getParameter("GrpContNo"));
//              tLHServCaseRelaSchema.setGrpCustomerNo(request.getParameter("GrpCustomerNo"));
//              tLHServCaseRelaSchema.setGrpName(request.getParameter("GrpName"));
         		
         		j++;
				
				try
				{// 准备传输数据 VData
				  	VData tVData = new VData();
				  	tVData.addElement(StartEnd);
//				    tVData.add(tLHServCaseRelaSchema);
			  		tVData.add(tG);
			    	tOLHPersonCaseUI.submitData(tVData,"INSERT||MAIN");
			  	}
				catch(Exception ex)
				{
				    Content = "保存失败，原因是:" + ex.toString();
				    FlagStr = "Fail";
				}
			  
				//如果在Catch中发现异常，则不从错误类中提取错误信息
			  	if (FlagStr=="Success")
			  	{
			    	tError = tOLHPersonCaseUI.mErrors;
				    if (!tError.needDealError())
				    {                          
				    	Content = " 保存成功! ";
				    	FlagStr = "Success";
				    }
				    else                                                                           
				    {
				    	Content = " 保存失败，原因是:" + tError.getFirstError();
				    	FlagStr = "Fail";
				    }
			  	}
			}
		}
	}
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
