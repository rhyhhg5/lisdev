<%
  //程序名称：LHHospitalBasicStat.jsp
  //程序功能：理赔费用医院分布情况统计
  //创建日期：2008-01-17
  //创建人  ：liuli
  //更新人  ：
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
      GlobalInput tGI = new GlobalInput();
      tGI = (GlobalInput)session.getValue("GI");
%>
<script type="">
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

</head>
<body onload="initform();initElementtype();">
<table>
  <tr>
    <td class=titleImg>客户就诊情况</td>
  </tr>
</table>
<form action="./NewContQuerySave.jsp" method=post name=fm target=fraSubmit>
<table class= common align='center'>
	<TR>
  	<TD  class= title8>
     开始日期
    </TD>
  	<TD  class= input8>
      <Input class= 'coolDatePicker'  dateFormat="Short" name=MakeDate1 value="" verify="入机时间|DATE&NOTNULL"  elementtype=nacessary>
    </TD>
     <TD  class= title8>
     截止日期
    </TD>
    <TD class=input8>
      <Input class= 'coolDatePicker'  dateFormat="Short" name=MakeDate2 value="" verify="入机时间|DATE&NOTNULL "  elementtype=nacessary>
    </TD>
    <td>    </td>
    <td>    </td>
   </TR>
   <TR class="common"> 
    <TD class=title8>险种编码</TD>
    <TD class=input8>
    	<input class="fcodeno" name="RiskCode" value='' CodeData="0|4^331201|健康人生个人护理保险(万能型，B款)^331301|健康人生个人护理保险(万能型，C款)^230501|康健无忧个人防癌疾病保险"  ondblclick="return showCodeListEx('RiskCode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKeyEx('feetype',[this,RiskCodeName],[0,1]);"><input class=fcodename name=RiskCodeName value='' >
    </TD>
      <TD class=title8>机构名称</TD>
    <TD>
      <Input class=fcodeno  name=ManageCom verify="机构名称|code:comcode&NOTNULL" value="<%=tGI.ManageCom%>" ondblclick="return showCodeList('comcode4',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=fcodename name=ManageComName >
    </TD>
  </TR>
<tr>
	<td></td>
</tr>
  <tr>
  	<TD class=title8>
  			<Input value="新单查询" style="width:130px"  type=button class=cssbutton onclick="ContQuery();">&nbsp;&nbsp;
  	</TD>
  	<TD ></TD>
  </tr>
  <input type=hidden id="hospital" name="hospital">
</table>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
<script>
	function initform(){ 
    var   str="select Current Date  from dual";  
    fm.all("MakeDate2").value = easyExecSql(str);  
    fm.all("MakeDate1").value = easyExecSql(str); 	 
	}
	
	function ContQuery()
    {
    if( verifyInput2() == false ){
           return false;
    }
	if((fm.MakeDate1.value != null && fm.MakeDate1.value != "")&&(fm.MakeDate2.value != null && fm.MakeDate2.value != "")&& fm.MakeDate1.value>fm.MakeDate2.value)
	{
	      alert ("您输入的统计时间错误!");
	      return false;
	}
    fm.submit();
   }
</script>