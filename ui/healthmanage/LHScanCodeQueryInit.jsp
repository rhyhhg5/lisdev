<%
//程序名称：LHScanCodeQueryInit.jsp
//程序功能：
//创建日期：2006-06-07 15:22:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('CustomerNo').value = "";

  }
  catch(ex)
  {
    alert("LHScanCodeQueryInit.sp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    initLHScanCodeQueryGrid();
  }
  catch(re)
  {
    alert("LHScanCodeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}    
 function initLHScanCodeQueryGrid() 
 {                            
   var iArray = new Array();                               
                                                           
   try 
   {                                                   
     iArray[0]=new Array();                                
     iArray[0][0]="序号";         		//列名               
     iArray[0][1]="30px";         		//列名               
     iArray[0][3]=0;         				//列名                 
     iArray[0][4]="";         //列名   
    
    iArray[1]=new Array(); 
		iArray[1][0]="客户姓名/客户号";   
		iArray[1][1]="110px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
     
		iArray[2]=new Array(); 
		iArray[2][0]="扫描件类型";   
		iArray[2][1]="90px";   
		iArray[2][2]=20;        
		iArray[2][3]=1;

		
		iArray[3]=new Array(); 
		iArray[3][0]="扫描件编号";   
		iArray[3][1]="110px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;   

		
		iArray[4]=new Array(); 
		iArray[4][0]="扫描日期";   
		iArray[4][1]="70px";   
		iArray[4][2]=20;    
		
		iArray[5]=new Array(); 
		iArray[5][0]="扫描时间";   
		iArray[5][1]="60px";   
		iArray[5][2]=20;         
		
		iArray[6]=new Array(); 
		iArray[6][0]="扫描件维护状态";   
		iArray[6][1]="70px";   
		iArray[6][2]=50;        
		iArray[6][3]=1;  
	  
	  iArray[7]=new Array(); 
		iArray[7][0]="ScanType";   
		iArray[7][1]="0px";   
		iArray[7][2]=50;        
		iArray[7][3]=3; 
		
		iArray[8]=new Array(); 
		iArray[8][0]="Comment";   
		iArray[8][1]="0px";   
		iArray[8][2]=50;        
		iArray[8][3]=3;  
		
	  iArray[9]=new Array(); 
		iArray[9][0]="ScanNo";   
		iArray[9][1]="0px";   
		iArray[9][2]=50;        
		iArray[9][3]=3;    
		
		iArray[10]=new Array(); 
		iArray[10][0]="RelationType";   
		iArray[10][1]="0px";   
		iArray[10][2]=50;        
		iArray[10][3]=3;   
		
		iArray[11]=new Array(); 
		iArray[11][0]="InHospitNo";   
		iArray[11][1]="0px";   
		iArray[11][2]=50;        
		iArray[11][3]=3;    
    
    iArray[12]=new Array(); 
		iArray[12][0]="客户号";   
		iArray[12][1]="0px";   
		iArray[12][2]=50;        
		iArray[12][3]=3;    
		
		iArray[13]=new Array(); 
		iArray[13][0]="扫描件名称";   
		iArray[13][1]="90px";   
		iArray[13][2]=50;        
		iArray[13][3]=1;   
     
     
    LHScanCodeQueryGrid = new MulLineEnter( "fm" , "LHScanCodeQueryGrid" );                         
    //这些属性必须在loadMulLine前                                                                                              
                                                                                                               
    LHScanCodeQueryGrid.mulLineCount = 0;                                          
    LHScanCodeQueryGrid.displayTitle = 1;                                          
    LHScanCodeQueryGrid.hiddenPlus = 1;                                            
    LHScanCodeQueryGrid.hiddenSubtraction = 1;                                     
    LHScanCodeQueryGrid.canSel = 1;                                                
//    LHScanCodeQueryGrid.canChk = 1;                                              
    //LHScanCodeQueryGrid.selBoxEventFuncName = "showOne";                         
                                                                          ;  
    LHScanCodeQueryGrid.loadMulLine(iArray);                                       
                             
  }                                                                          
  catch(ex) {                                                                
    alert(ex);                                                               
  }                                                                          
}                                                                        
    
      
</script>
  