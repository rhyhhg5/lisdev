var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
    var i = 0;
    fm.fmtransact.value="INSERT||MAIN";
    
    //在初次添加费用时，将应付费用赋给实付费用
	var rowNum=LHFeeChargeGrid.mulLineCount ; //行数 	
	for(var row=0; row < rowNum; row++)
	{
		LHFeeChargeGrid.setRowColData(row,20,LHFeeChargeGrid.getRowColData(row,19));
	}
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.action="./LHFeeChargeSave.jsp";
    fm.target="fraSubmit";
    fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	initForm();
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHCommTrans.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}                                          
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */              
function updateClick()
{
	  var rowNum=LHFeeChargeGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHFeeChargeGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx] = row;
	        }
	  }
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要修改的信息记录!");
			  return false;
		}	
		if(aa.length>="1")
		{
    	if (confirm("您确实想修改该记录吗?"))
    	{
          var i = 0;
          var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
     
          fm.fmtransact.value = "UPDATE||MAIN";
          fm.action="./LHFeeChargeSave.jsp";
          fm.target="fraSubmit";
          fm.submit(); //提交
          initForm();
         
		  }
      else
      {
        alert("您取消了修改操作！");
      } 
   }
}

function showOne()
{
	var rowNum=LHFeeChargeGrid.mulLineCount ; //行数 	
	var FeeArr = new Array();
	var xx = 0;
	for(var row=0; row < rowNum; row++)
	{
		var tChk =LHFeeChargeGrid.getChkNo(row); 
		if(tChk == true)
		{
			FeeArr[xx++] = row;	
		}  
	}
	//alert(FeeArr.length);
	if(FeeArr.length=="1")
	{
		 var Fee =LHFeeChargeGrid.getRowColData(FeeArr,20); 
		 fm.all('Fee').value=Fee;
		 var TaskExecNo =LHFeeChargeGrid.getRowColData(FeeArr,16); 
		 fm.all('TaskExecNo').value=TaskExecNo;
		 
		 var hServTaskno =LHFeeChargeGrid.getRowColData(FeeArr,18); 
		 //alert(hServTaskno);
		 fm.all('tServTaskno').value=hServTaskno;
		 
		 initCustomTestInfoGrid();
		 LHTestInfo.style.display="none";
		 if(LHFeeChargeGrid.getRowColData(FeeArr,7) == "核保体检结算" && LHFeeChargeGrid.getRowColData(FeeArr,8) == "0")		 
		 {
		 	LHTestInfo.style.display="";
		 	var arrProposalNo = "";
		 	
		 	//对变号的保单作处理
		 	var tmpNo = easyExecSql(" select distinct ProposalContno from lccont where  contno = '"+LHFeeChargeGrid.getRowColData(FeeArr,9)+"' ");
		 	if(tmpNo=="" || tmpNo==null || tmpNo=="null")
		 	{	//存储的不是ContNo，而是ProposalNo
		 		var tmpForPro = easyExecSql(" select distinct ProposalContno from lccont where  ProposalContno = '"+LHFeeChargeGrid.getRowColData(FeeArr,9)+"' ");
			 	//alert(tmpForPro);
			 	if(tmpForPro=="" || tmpForPro==null || tmpForPro=="null")
			 	{	//存储的不是ContNo，而是ProposalNo
			 		alert("存储的合同号码无效！");
			 		return false;
			 	}
			 	else
			 	{	//存储的号码就是ProposalContno
//			 		alert("存储的号码就是ProposalContno: "+tmpForPro)
//			 		alert("ProposalContno[0][0]: "+tmpForPro[0][0])
			 		arrProposalNo = tmpForPro;
			 	}
		 	}
		 	else
		 	{	//存储的号码就是ContNo
//				alert("存储的号码就是ContNo: "+arrProposalNo)
//				alert("ContNo[0][0]: "+arrProposalNo[0][0])
		 		arrProposalNo = tmpNo;
		 	}
		 	
		 	var PrtSeq = easyExecSql(" select distinct prtseq from lcpenotice where  ProposalContno = '"+arrProposalNo[0][0]+"' and customerno = '"+LHFeeChargeGrid.getRowColData(FeeArr,1)+"' ");

			//针对一张保单多次核保体检进行了处理
			if(PrtSeq.length == 1)
			{
				PrtSeq = "'"+PrtSeq+"'"
			}
			else
			{
				var tPrtSeq = "";
				for (var i = 0;i<PrtSeq.length; i++)
				{
					tPrtSeq = tPrtSeq +"','" + PrtSeq[i];
				}
				tPrtSeq = tPrtSeq.substring(2)+"'";
				PrtSeq = tPrtSeq;
			}
			var sql = " select peitemname,(select a.medicaitemprice from ldtestpricemgt a where a.hospitcode = '"+
		  	LHFeeChargeGrid.getRowColData(FeeArr,15)+"' and a.ContraNo = '"+LHFeeChargeGrid.getRowColData(FeeArr,14)
		  	+"' and medicaitemcode = peitemcode ) from lcpenoticeitem where  ProposalContno = '"+arrProposalNo[0][0]+"' and PrtSeq in ("+PrtSeq+") ";
				turnPage1.pageLineNum = 200; 
		  	turnPage1.queryModal(sql,CustomTestInfoGrid);
			var sqlSum = " select value(sum(decimal((a.medicaitemprice),12,2)),0) from lcpenoticeitem p,ldtestpricemgt a "
					+ " where p.ProposalContno = '"+arrProposalNo[0][0]+"' "
					+ "  and  a.hospitcode = '"+LHFeeChargeGrid.getRowColData(FeeArr,15)+"' and a.ContraNo = '"+LHFeeChargeGrid.getRowColData(FeeArr,14)
					+"' and a.medicaitemcode = p.peitemcode   and p.PrtSeq in ( "+PrtSeq+") ";
			var arrSum = easyExecSql(sqlSum);
			LHFeeChargeGrid.setRowColData(FeeArr,19,arrSum[0][0]);
		}
	}
	else
	{
		 fm.all('Fee').value="";
		 LHTestInfo.style.display="none";
	}
}  
function feePrint()
{
	   if (verifyInput2() == false)
     		return false;
	   fm.action="./LHFeeChargeInputPrt.jsp";
	   fm.target="f1print";
	   fm.submit();
}  
function printFeeInfo()
{                          
	fm.all('ContraNo').verify = "";
	fm.all('DutyItemCode').verify = "";
	if (verifyInput2() == false)
  		return false;
	fm.action="./LHFeeChargeNotePrt.jsp";
	fm.target="f1print";
	fm.submit();
}   


function afterCodeSelect(codeName,Field)
{
	if(codeName == "hmexecstate")	
	{
		for(var row=0; row < LHFeeChargeGrid.mulLineCount; row++)
		{
			var tChk =LHFeeChargeGrid.getChkNo(row); 
			if(tChk == true)
			{
				LHFeeChargeGrid.setRowColData(row,21,fm.all('ExecState_ch').value);	
				LHFeeChargeGrid.setRowColData(row,22,fm.all('ExecState').value);
	  		}  
		}	
	}
}