<%
//程序名称：LHServPlanInput.jsp
//程序功能：
//创建日期：2006-11-20 16:42:56
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     String flag = request.getParameter("flag");
     String ContNo2 = request.getParameter("ContNo");
     String CustomerNo2 = request.getParameter("CustomerNo");     
     String prtNo = request.getParameter("prtNo");
	String PersonFee = request.getParameter("PersonFee");

	GlobalInput tG = new GlobalInput(); 
	tG = (GlobalInput)session.getValue("GI");
%>
                    
<script language="JavaScript">
	var comCode = "<%=tG.ComCode%>";
	var manageCom = "<%=tG.ManageCom%>";
	manageCom = manageCom.length==8?manageCom.substring(0,4):manageCom;
	var operator = "<%=tG.Operator%>";

var flag = "<%=flag%>";//标志符，判断按保费、团单、非健康保单等多种标志，支持以后扩充
function initInpBox()
{ 
	try
	{                                   
    	fm.all('GrpServPlanNo').value = "";
    	fm.all('ServPlanNo').value = "";
    	fm.all('ContNo').value = "";
    	fm.all('CustomerNo').value = "";
    	fm.all('CustomerName').value = "";
    	fm.all('ServPlanCode').value = "";
    	fm.all('ServPlanName').value = "";
    	fm.all('ServPlanLevel').value = "0";
    	fm.all('ServPlayType').value = "";
    	fm.all('ComID').value = "";
    	fm.all('StartDate').value = "";
    	fm.all('EndDate').value = "";
    	fm.all('ServPrem').value = "";
    	fm.all('MakeDate').value = "";
    	fm.all('MakeTime').value = "";
    	fm.all('HiddenCustom').value="";
    	fm.all('prtNo').value="";
    	fm.all('CaseState').value = "1";
    	fm.all('CaseStateName').value = "未进行服务信息设置";

		//if(flag == "NCONT")
		//{
		//	fm.all('StartDate').className="common";
		//	fm.all('EndDate').className="common";
		//	fm.all('StartDate').readOnly =true;
    	//	fm.all('EndDate').readOnly =true;
		//}
		//else
		//{
		//	 fm.all('StartDate').className= "CoolDatePicker";
		//	 fm.all('EndDate').className= "CoolDatePicker";
		//}
	}
  	catch(ex)
  	{
    	alert("在LHServPlanInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  	}      
}                                       
function initForm()
{
	try
	{
	    initInpBox();
	    initLHServPlanGrid();
	    initComID();
	    
//	   	if(flag == "NCONT")//非健康险种保单标志
//	   	{
//	   		NQuery();
//	   	}
//	   	else
//	   	{
//	   		passQuery();//传入的客户号不为空时
//	   	}
//	
//    	//服务计划状态的判断
//		var arrResult4=new Array;
//	    var mulSql4 = "select CaseState from LHServPlan a where a.ServPlanNo='"+fm.all('ServPlanNo').value+"'";
//		arrResult4=easyExecSql(mulSql4);//契约状态为2或3时，不可修改服务计划信息
//
//		if(arrResult4=="2"||arrResult4=="3")
//		{
//			fm.all('saveButton').disabled = true;
//			fm.all('modifyButton').disabled = true;
//		}
	}
	catch(re)
	{
	  alert("LHServPlanInputInit.jsp-->InitForm函数中发生异常:初始化界面错误AA!");
	}
}

var LHServPlanGrid;
function initLHServPlanGrid() 
{                               
	var iArray = new Array();

	try 
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         		//列名
    	iArray[0][1]="40px";         		//列名
    	iArray[0][3]=1;         		//列名
    	iArray[0][4]="station";         		//列名
    
    	iArray[1]=new Array(); 
	    iArray[1][0]="服务项目代码";   
	    iArray[1][1]="80px";   
	    iArray[1][2]=20;        
	    iArray[1][3]=0;
	               
		iArray[2]=new Array(); 
		iArray[2][0]="服务项目名称";   
		iArray[2][1]="114px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="hmservitemcode";
        iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
        iArray[2][6]="1|0";     //上面的列中放置引用代码中第几位值
        iArray[2][15]="servitemname";
        iArray[2][17]="2" ;
        iArray[2][19]="1" ;
        
        iArray[4]=new Array(); 
		iArray[4][0]="服务项目序号";   
		iArray[4][1]="80px";   
		iArray[4][2]=20;        
		iArray[4][3]=1;
		iArray[4][14]="1";
		    
		iArray[7]=new Array();                           
	    iArray[7][0]="定价方式代码";                             
	    iArray[7][1]="0px";                             
	    iArray[7][2]=20;                                  
	    iArray[7][3]=3;  
	    iArray[7][4]="lhserveritemcode";
	    iArray[7][5]="7|8";     //引用代码对应第几列，'|'为分割符
	    iArray[7][6]="0|1";     //上面的列中放置引用代码中第几位值
	    iArray[7][9]="定价方式名称|len<=10";   
	    iArray[7][15]="servpricecode";
	    iArray[7][17]="7";  
	    iArray[7][19]="1" ;           //是否允许强制刷新数据源,1表示允许，0表示不允许
		iArray[7][14]="0";  
	    
	    iArray[3]=new Array(); 
	    iArray[3][0]="个人服务项目号码";   
	    iArray[3][1]="0px";   
	    iArray[3][2]=20;        
	    iArray[3][3]=3;
	    
	    iArray[5]=new Array(); 
      	iArray[5][0]="服务事件类型";                             
      	iArray[5][1]="110px";                     
      	iArray[5][2]=20;                                  
      	iArray[5][3]=2;      
      	iArray[5][4]="eventtype";
      	iArray[5][5]="5|9";     //引用代码对应第几列，'|'为分割符
      	iArray[5][6]="1|0";     //上面的列中放置引用代码中第几位值
      	iArray[5][14]="集体服务事件";
      	iArray[5][17]="1"; 
      	iArray[5][18]="160";
      	iArray[5][19]="1" ;
	      
		iArray[6]=new Array(); 
	    iArray[6][0]="服务提供机构标识";                             
	    iArray[6][1]="110px";                     
	    iArray[6][2]=20;                                  
	    iArray[6][3]=2;      
	    iArray[6][4]="hmhospitalmng";
	    iArray[6][5]="6";     //引用代码对应第几列，'|'为分割符
	    iArray[6][6]="1";     //上面的列中放置引用代码中第几位值
	    iArray[6][14]=manageCom;
      
      	iArray[8]=new Array(); 
	    iArray[8][0]="定价方式名称";   
	    iArray[8][1]="0px";   
	    iArray[8][2]=20;        
	    iArray[8][3]=3;   
        iArray[8][4]="lhserveritemcode";
        iArray[8][5]="8|7";     //引用代码对应第几列，'|'为分割符
        iArray[8][6]="1|0";     //上面的列中放置引用代码中第几位值
        iArray[8][9]="定价方式名称|len<=10";   
        iArray[8][15]="servpricename";
        iArray[8][17]="8";  
        iArray[8][19]="1" ;           //是否允许强制刷新数据源,1表示允许，0表示不允许    
	                
        iArray[9]=new Array(); 
	    iArray[9][0]="eventtype";   
	    iArray[9][1]="0px";   
	    iArray[9][2]=20;        
	    iArray[9][3]=3;
	    iArray[9][14]="2";
	                            
    	LHServPlanGrid = new MulLineEnter( "fm" , "LHServPlanGrid" );           
    	//这些属性必须在loadMulLine前                          
    	if(flag == "NCONT")
    	{            
			LHServPlanGrid.mulLineCount = 0;                             
      		LHServPlanGrid.displayTitle = 1;                          
      		LHServPlanGrid.hiddenPlus = 0;                          
      		LHServPlanGrid.hiddenSubtraction = 0;                          
      		LHServPlanGrid.canSel = 1;                          
      		LHServPlanGrid.canChk = 0;                          
      		LHServPlanGrid.selBoxEventFuncName = "";                         	
      	}       	
      	else
    	{
			LHServPlanGrid.mulLineCount = 0;                             
      		LHServPlanGrid.displayTitle = 1;                          
      		LHServPlanGrid.hiddenPlus = 0;                          
      		LHServPlanGrid.hiddenSubtraction = 0;                          
      		LHServPlanGrid.canSel = 1;                          
      		LHServPlanGrid.canChk = 0;                          
      		LHServPlanGrid.selBoxEventFuncName = "";            
    	}                                                       

    	LHServPlanGrid.loadMulLine(iArray);                                     
    	//这些操作必须在loadMulLine后面                                         
	}
	catch(ex) {alert(ex);}
}
</script>
<script>
//通过综合查询进入此页面的处理
var turnPage = new turnPageClass(); 
function passQuery()
{	//进入此函数

	var ContNo = "<%=request.getParameter("ContNo")%>";
	var CustomerNo = "<%=request.getParameter("CustomerNo")%>";
	if(ContNo != 'null' || ContNo>0) 
	{
		//保单基本信息
		var strSqlInit = "select grpcontno,contno,insuredno,insuredname,Managecom from lccont "
						+"where  contno = '"+ContNo+"'"
						;
		var arrResultInit = easyExecSql(strSqlInit);
      	fm.all('GrpContNo').value = arrResultInit[0][0];
      	fm.all('ContNo').value = arrResultInit[0][1];
        fm.all('ComID').value = arrResultInit[0][4].substring (0,4);
      	//针对家庭单的处理
      	if(CustomerNo == arrResultInit[0][2] || CustomerNo == "null")
      	{//传入客户号与LCCont一致则直接处理
          	fm.all('CustomerNo').value = arrResultInit[0][2];
          	fm.all('CustomerName').value = arrResultInit[0][3];
      	}
      	else
      	{//传入客户号与LCCont不一致，则针对这名客户处理
          	fm.all('CustomerNo').value = CustomerNo;
          	fm.all('CustomerName').value = easyExecSql("select name from ldperson where CustomerNo = '"+CustomerNo+"'");
      	}

      	//看此Contno在LHServPlan是否已保存过
      	var sql = " select ServPlanno from LHServPlan where ContNo = '"+arrResultInit[0][1]+"' and CustomerNo = '"+CustomerNo+"' and Servplaytype <> '1H'";
      	var arr_ex = easyExecSql(sql);

      	//此保单此客户未被保存过
      	if(arr_ex == null || arr_ex == "" || arr_ex == "null")
      	{
      		//060901增加按保费查询不同服务计划
      		if("<%=flag%>" == "Fee")
      		{
      			//此查询是针对档次的，看客户所购买的产品在健管系统中是否已定义
				var strSqlInit1 =" ( select p.riskcode, (select distinct m.riskname from lmrisk m where m.riskcode = p.riskcode), integer(p.mult) from lcpol p where p.contno = '"+ContNo+"' and p.insuredno = '"+CustomerNo+"' and p.riskcode in (select distinct riskcode from lmriskapp where  risktype2 = '5')) ";
				var arrRiskCode = easyExecSql(strSqlInit1);
				
				fm.all('ServPlanCode').value = arrRiskCode[0][0];
				fm.all('ServPlanName').value = arrRiskCode[0][1];
				//fm.all('ServPlanLevel').value = arrRiskCode[0][2];
				
				//alert(arrRiskCode.length);
				if(arrRiskCode.length == 2)
				{
					alert("该客户一张保单下有多个健康型产品，系统目前不支持，只能随机取一个进行实施管理");	
				}

      			//取得该保费所对应的档次
      			var sqlFee = " select distinct servplanlevel from lhhealthservplan where  plantype = '2' "
				    +" and    servplancode = '"+arrRiskCode[0][0]+"' and comid = '"+arrResultInit[0][4].substring (0,4)+"' "
				    +" and    feeuplimit <= "+"<%=PersonFee%>"+" and feelowlimit >="+"<%=PersonFee%>"
				    ;
					
				var FeeLevel = easyExecSql(sqlFee);
				if(FeeLevel == "" || FeeLevel == null || FeeLevel == "null")
				{
					alert("请先录入该保单下对应保费区间的健管服务计划");
					return false;	
				}
      			
      			var strSqlInit2 = " select a.servitemcode, a.servitemname,'',a.servitemsn, "
	         		+ " (select case  when  c.ServCaseType='1' then  '个人服务事件' when c.ServCaseType='2' then  '集体服务事件' else '无' end from LHServCaseTypeDef c where c.ContType='1' and c.ComID =  '"+arrResultInit[0][4].substring (0,4)+"'  and c.ServItemCode = a.ServItemCode),"
	         		+"  a.comid,'','',"
	         		+" (select distinct b.ServCaseType as xxx from LHServCaseTypeDef b where b.ContType='1' and b.ComID = '"+arrResultInit[0][4].substring (0,4)+"' and b.ServItemCode = a.ServItemCode) as def "
					+" from LHHealthServPlan a "
					+" where a.ServPlanCode = '"+arrRiskCode[0][0]+"'"
					+" and a.comid = '"+arrResultInit[0][4].substring (0,4)+"'"
					+" and a.ServPlanLevel = '"+FeeLevel[0][0]+"'"
				;

          		turnPage.pageLineNum = 50;
				turnPage.queryModal(strSqlInit2, LHServPlanGrid); 
      		}
      		else
      		{
      			//此查询是针对档次的，看客户所购买的产品在健管系统中是否已定义
				var strSqlInit1 =" select distinct ServPlanName,ServPlanLevel,ServPlanCode,ServPlayType from LHHealthServPlan "
				    +" where servplancode = ( select riskcode from lcpol where contno = '"+ContNo+"' and insuredno = '"+CustomerNo+"' and riskcode in (select distinct riskcode from lmriskapp where  risktype2 = '5'))"                                                 
				    +" and servplanlevel = char((select integer(mult)  from lcpol  where  contno = '"+ContNo+"' and insuredno = '"+CustomerNo+"' and riskcode  in (select distinct riskcode from lmriskapp where  risktype2 = '5')))"                                                           
					;
				var arrResultInitLevel = easyExecSql(strSqlInit1);
				if((arrResultInitLevel)==null)
				{
					alert("请先录入该保单下的健管服务计划");
					return false;
				}
				fm.all('ServPlanName').value = arrResultInitLevel[0][0];
				//fm.all('ServPlanLevel').value = arrResultInitLevel[0][1];
				fm.all('ServPlanCode').value = arrResultInitLevel[0][2];
				fm.all('ServPlayType').value = arrResultInitLevel[0][3];

	         	var strSqlInit2 = " select a.servitemcode, a.servitemname,'',a.servitemsn, "
	         		+ " (select case  when  c.ServCaseType='1' then  '个人服务事件' when c.ServCaseType='2' then  '集体服务事件' else '无' end from LHServCaseTypeDef c where c.ContType='1' and c.ComID =  '"+arrResultInit[0][4].substring (0,4)+"'  and c.ServItemCode = a.ServItemCode),"
	         		+"  a.comid,'','',"
	         		+" (select distinct b.ServCaseType as xxx from LHServCaseTypeDef b where b.ContType='1' and b.ComID = '"+arrResultInit[0][4].substring (0,4)+"' and b.ServItemCode = a.ServItemCode) as def "
					+" from LHHealthServPlan a "
					+" where a.ServPlanCode = '"+arrResultInitLevel[0][2]+"'"
					+" and a.comid = '"+arrResultInit[0][4].substring (0,4)+"'"
					+" and a.ServPlanLevel = '"+arrResultInitLevel[0][1]+"'"
				;
				turnPage.pageLineNum = 50;
				turnPage.queryModal(strSqlInit2, LHServPlanGrid); 
        	}

      	  	
      	  	//060522新增，处理结构非空字段业务为空的情况
		  	var maxRow = LHServPlanGrid.mulLineCount; //最大行
		  	for(var j = 0; j < maxRow; j++)
		  	{ 
		  		LHServPlanGrid.setRowColData(j,7,"0");
		  		LHServPlanGrid.setRowColData(j,8,"待定义");
		  	}
		}
      	else
      	{
      		fm.all('saveButton').disabled = true;
      		var sqlSqlInit4 = " select StartDate, EndDate, servprem,GrpServPlanNo,MakeDate,MakeTime,servplancode,servplanname,servplanlevel from LHServPlan where servplanno = '"+arr_ex[0][0]+"' "
      		var arr_4 = easyExecSql(sqlSqlInit4);
      		fm.all('StartDate').value = arr_4[0][0];
      		fm.all('EndDate').value = arr_4[0][1];
      		fm.all('ServPrem').value = arr_4[0][2];
      		fm.all('ServPlanNo').value = arr_ex[0][0];
      		fm.all('ServPlanCode').value = arr_4[0][6]
      		fm.all('ServPlanName').value = arr_4[0][7]
      		//fm.all('ServPlanLevel').value = arr_4[0][8]
      		fm.all('GrpServPlanNo').value = arr_4[0][3];
      		fm.all('MakeDate').value = arr_4[0][4];
      		fm.all('MakeTime').value = arr_4[0][5];
      		var strSqlInit3 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
      			+" a.servitemno, a.servitemtype,"
      			+" (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end),"
      			+" a.comid, a.servpricecode, "
      			+" (select b.servpricename from lhserveritemprice b where  b.servpricecode = a.servpricecode), "
      			+" a.ServCaseType "
      			+" from lhservitem a where a.servplanno = '"+arr_ex[0][0]+"'"
      			;

			turnPage.pageLineNum = 50;
			turnPage.queryModal(strSqlInit3, LHServPlanGrid); 
		}
	}
	else 
	{
	 	  alert("请先到保险合同查询选择未录入保单!");
	}
}	


//非健康保单的查询
function NQuery()
{
	//此时的ContNo为ProposalContNo,针对契约换号处理--20061026
	var ProposalContNo = "<%=request.getParameter("ContNo")%>";
	var sqlContNo = " select distinct ContNo from lccont where  ProposalContNo = '"+ProposalContNo+"' ";
	var arrContNo = easyExecSql(sqlContNo);
	//alert("arrContNo.length="+arrContNo.length)
	if(arrContNo.length != 1)
	{
		alert("契约数据错误，一个ProposalContNo对应多个ContNo，或者ProposalContNo无对应ContNo");
		return false;
	}
	
	var ContNo = arrContNo[0][0];
	var CustomerNo = "<%=request.getParameter("CustomerNo")%>";
	
	//保单基本信息
	var sqlContInfo = "select grpcontno,contno,insuredno,(select p.name from ldperson p where p.customerno = lcinsured.insuredno),Managecom from lcinsured where  contno = '"+ContNo+"' and insuredno ='"+CustomerNo+"'"
            ;
	var aContInfo = easyExecSql(sqlContInfo);
    fm.all('GrpContNo').value = aContInfo[0][0];
    fm.all('ContNo').value = aContInfo[0][1];
    fm.all('ComID').value = aContInfo[0][4].substring (0,4);
   	fm.all('CustomerNo').value = aContInfo[0][2];
   	fm.all('CustomerName').value = aContInfo[0][3];
   	
   	var sqlStartEndDate = " select cvalidate,cinvalidate from lccont where contno =  '"+ContNo+"'";
   	var aStartEndDate = easyExecSql(sqlStartEndDate);
   	fm.all('StartDate').value = aStartEndDate[0][0]==""?"3000-01-01":aStartEndDate[0][0];
   	fm.all('EndDate').value = aStartEndDate[0][1]==""?"3000-01-01":aStartEndDate[0][1];

    //看此Contno在LHServPlan是否已保存过
    var sql = " select ServPlanno from LHServPlan where ContNo = '"+aContInfo[0][1]
    		 +"' and CustomerNo = '"+CustomerNo+"' and ServPlayType = '1H'";
    var arr_ex = easyExecSql(sql);

    //此保单此客户未被保存过
    if(arr_ex == null || arr_ex == "" || arr_ex == "null")
    {
		var sqlPolInfo =" select (select m.riskname from lmrisk m where m.riskcode = a.riskcode),a.mult,a.riskcode,"
					   +" a.conttype from lcpol a where a.contno = '"+ContNo+"'  fetch first 1 rows only"
						;
		var aPolInfo = easyExecSql(sqlPolInfo);

		fm.all('ServPlanName').value = aPolInfo[0][0];
		//fm.all('ServPlanLevel').value = aPolInfo[0][1];
		fm.all('ServPlanCode').value = aPolInfo[0][2];
		fm.all('ServPlayType').value = aPolInfo[0][3]+"H";//1H,代表核保体检进入
	
		var strSqlInit2 = " select a.servitemcode, a.servitemname,'',a.servitemsn, "
	    	+ " (select case  when  c.ServCaseType='1' then  '个人服务事件' when c.ServCaseType='2' then  '集体服务事件' else '无' end from LHServCaseTypeDef c where c.ContType='1' and c.ComID =  '"+aContInfo[0][4].substring (0,4)+"'  and c.ServItemCode = a.ServItemCode),"
	    	+"  a.comid,'','',"
	    	+" (select distinct b.ServCaseType as xxx from LHServCaseTypeDef b where b.ContType='1' and b.ComID = '"+aContInfo[0][4].substring (0,4)+"' and b.ServItemCode = a.ServItemCode) as def "
			+" from LHHealthServPlan a "
			+" where a.ServPlanCode = '"+aPolInfo[0][2]+"'"
			+" and a.comid = '"+aContInfo[0][4].substring (0,4)+"'"
			+" and a.ServPlanLevel = '"+aPolInfo[0][1]+"'"
		;
		turnPage.pageLineNum = 50;
		turnPage.queryModal(strSqlInit2, LHServPlanGrid)      	
	}	
	
	else
    {
    	fm.all('saveButton').disabled = true;
    	var sqlSqlInit4 = " select StartDate, EndDate, servprem,GrpServPlanNo,MakeDate,MakeTime,servplancode,servplanname,servplanlevel from LHServPlan where servplanno = '"+arr_ex[0][0]+"' "
    	var arr_4 = easyExecSql(sqlSqlInit4);
    	fm.all('StartDate').value = arr_4[0][0];
    	fm.all('EndDate').value = arr_4[0][1];
    	fm.all('ServPrem').value = arr_4[0][2];
    	fm.all('ServPlanNo').value = arr_ex[0][0];
    	fm.all('ServPlanCode').value = arr_4[0][6]
    	fm.all('ServPlanName').value = arr_4[0][7]
    	//fm.all('ServPlanLevel').value = arr_4[0][8]
    	fm.all('GrpServPlanNo').value = arr_4[0][3];
    	fm.all('MakeDate').value = arr_4[0][4];
    	fm.all('MakeTime').value = arr_4[0][5];
    	var strSqlInit3 = " select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
    		+" a.servitemno, a.servitemtype,"
    		+" (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end),"
    		+" a.comid, a.servpricecode, "
    		+" (select b.servpricename from lhserveritemprice b where  b.servpricecode = a.servpricecode), "
    		+" a.ServCaseType "
    		+" from lhservitem a where a.servplanno = '"+arr_ex[0][0]+"'"
    		;

		turnPage.pageLineNum = 50;
		turnPage.queryModal(strSqlInit3, LHServPlanGrid); 
	}
}

function initComID()
{
	 if(manageCom!="86"&&manageCom!="86000000")
	 {
	 	//alert(manageCom);
	 	fm.all('ComID').value = manageCom;
	 	var sqlSql = "select showname from ldcom "+
                 "where  sign = '1' and length(trim(comcode)) = 4 "+ 
                 "and comcode = '"+manageCom+"'";
    var arr = easyExecSql(sqlSql);
    fm.all('ComID_cn').value = arr[0][0];
	 	
	 	}
   
}


</script>

