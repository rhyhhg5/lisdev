<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDSocialInsOrganSave.jsp
//程序功能：
//创建日期：2005-01-15 13:07:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
	  //接收信息，并作校验处理。
	  //输入参数
	  LDSocialInsOrganSchema tLDSocialInsOrganSchema   = new LDSocialInsOrganSchema();
	  LDSocialInsOrganUI tLDSocialInsOrganUI   = new LDSocialInsOrganUI();
	  //输出参数
	  CErrors tError = null;
	  String tRela  = "";                
	  String FlagStr = "";
	  String Content = "";
	  String transact = "";
	  GlobalInput tG = new GlobalInput(); 
	  tG=(GlobalInput)session.getValue("GI");
		
	  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	  transact = request.getParameter("fmtransact");
    tLDSocialInsOrganSchema.setSIOrganCode(request.getParameter("SIOrganCode"));
    tLDSocialInsOrganSchema.setSuperSIOrganCode(request.getParameter("SuperSIOrganCode"));
    tLDSocialInsOrganSchema.setSIOrganName(request.getParameter("SIOrganName"));
    tLDSocialInsOrganSchema.setAreaCode(request.getParameter("AreaCode"));
    tLDSocialInsOrganSchema.setAddress(request.getParameter("Address"));
    tLDSocialInsOrganSchema.setZipCode(request.getParameter("ZipCode"));
    tLDSocialInsOrganSchema.setPhone(request.getParameter("Phone"));
    tLDSocialInsOrganSchema.setWebAddress(request.getParameter("WebAddress"));
    tLDSocialInsOrganSchema.setFax(request.getParameter("Fax"));
    tLDSocialInsOrganSchema.setbankCode(request.getParameter("bankCode"));
    tLDSocialInsOrganSchema.setAccName(request.getParameter("AccName"));
    tLDSocialInsOrganSchema.setbankAccNO(request.getParameter("bankAccNO"));
    tLDSocialInsOrganSchema.setSatrapName(request.getParameter("SatrapName"));
    tLDSocialInsOrganSchema.setLinkman(request.getParameter("Linkman"));
    tLDSocialInsOrganSchema.setLastModiDate(request.getParameter("LastModiDate"));
    tLDSocialInsOrganSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDSocialInsOrganSchema.setMakeTime(request.getParameter("MakeTime"));System.out.println(tLDSocialInsOrganSchema.getSIOrganCode());
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLDSocialInsOrganSchema);
  	tVData.add(tG);
    tLDSocialInsOrganUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDSocialInsOrganUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
