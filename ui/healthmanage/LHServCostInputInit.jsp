<%
//程序名称：LHServCostInput.jsp
//程序功能：
//创建日期：2006-03-29 10:45:21
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {     
  	fm.all('querybutton').disabled=true;                              
    fm.all('ServPriceCode').value = "";
    fm.all('ServPriceName').value = "";
//    fm.all('BalanceObjCode').value = "";
//    fm.all('BalanceObjName').value = "";
//    fm.all('ContraNo').value = "";
//    fm.all('ContraItemNo').value = "";
//    fm.all('ManageCom').value = "";
//    fm.all('Operator').value = "";
//    fm.all('MakeDate').value = "";
//    fm.all('MakeTime').value = "";
//    fm.all('ModifyDate').value = "";
//    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHServCostInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try{}
  catch(ex)
  {
    alert("在LHServCostInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
                                        
function initForm()
{
  try
  {
    initInpBox(); 
    initLHServCostGrid();
    if("<%=PriceCode%>" != null && "<%=PriceCode%>" != "" && "<%=PriceCode%>" != "null")
    {
    	initQuery();
    }
  }
  catch(re)
  {
    alert("LHServCostInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initQuery()
{
	var sql = " select ServPriceCode, ServPriceName, ServItemName from LHServerItemPrice where ServPriceCode = '"+"<%=PriceCode%>"+"'";
	var arr	= easyExecSql(sql);
	fm.all('ServPriceCode').value = arr[0][0];
	fm.all('ServPriceName').value = arr[0][1];
	fm.all('ServItemName').value = arr[0][2];
	
	var sql_grid = " select a.balanceobjcode, a.balanceobjname,  a.contraitemno, "
		 		  +" (select c.dutyitemname  from lhcontitem b, ldcontraitemduty c "
		  		  +" where b.contraitemno = a.contraitemno and b.dutyitemcode = c.dutyitemcode),a.MakeDate,a.MakeTime,a.contrano "
			  	  +" from LHServCost a where ServPriceCode = '"+arr[0][0]+"'"
			  	  ;
	turnPage.queryModal(sql_grid, LHServCostGrid);
	                 
}                    
                     
var LHServCostGrid;  
function initLHServCostGrid() 
{
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";		//列名
	    iArray[0][1]="30px";		//列名
	    iArray[0][3]=0;				//列名
	    
	    iArray[1]=new Array();		
		iArray[1][0]="费用结算对象代码";		//列名    
		iArray[1][1]="80px";		//列名    
		iArray[1][3]=2;				//列名    
		iArray[1][4]="hmcontraincost";
		iArray[1][5]="1|2|7";
		iArray[1][6]="3|1|2";
		iArray[1][17]="2";
		iArray[1][18]="200"; 
		iArray[1][19]="1" ;    
		
		iArray[2]=new Array();                    
		iArray[2][0]="费用结算对象名称";	//列名    
		iArray[2][1]="120px";       //列名    
		iArray[2][3]=2;         	//列名  
		iArray[2][4]="hmcontraincost";
		iArray[2][5]="2|1|7";
		iArray[2][6]="1|3|2";
		iArray[2][17]="2";
		iArray[2][18]="200"; 
		iArray[2][19]="1" ;      
			
		iArray[3]=new Array();                    
		iArray[3][0]="合同责任项目代码";    //列名    
		iArray[3][1]="80px";       //列名    
		iArray[3][3]=2;         	//列名   
		iArray[3][4]="vvvvvvvv"
		iArray[3][5]="3|4"
		iArray[3][6]="2|1"
		iArray[3][15]="DutyItemName";
		iArray[3][17]="7";  
		iArray[3][18]="200";
		iArray[3][19]="1" ; 

		
		iArray[4]=new Array();  
		iArray[4][0]="合同责任项目名称";
		iArray[4][1]="120px";   
		iArray[4][3]=2;         
		iArray[4][4]="vvvvvvvv"
		iArray[4][5]="4|3"
		iArray[4][6]="1|2"
		iArray[4][15]="DutyItemName";
		iArray[4][17]="7";  
		iArray[4][18]="200";
		iArray[4][19]="1" ;       
		
		iArray[5]=new Array();  
		iArray[5][0]="MakeDate";
		iArray[5][1]="0px";   
		iArray[5][3]=3;         
		
		iArray[6]=new Array();  
		iArray[6][0]="MakeTime";
		iArray[6][1]="0px";   
		iArray[6][3]=3;         
			     
		iArray[7]=new Array();  
		iArray[7][0]="ConNo";
		iArray[7][1]="0px";   
		iArray[7][3]=3;  

	    LHServCostGrid = new MulLineEnter( "fm" , "LHServCostGrid" ); 
	    //这些属性必须在loadMulLine前           
	    
		LHServCostGrid.canSel = 1;
		LHServCostGrid.hiddenPlus = 0;
		LHServCostGrid.hiddenSubtraction = 0;
	/*
	    LHServCostGrid.mulLineCount = 0;   
	    LHServCostGrid.displayTitle = 1;

	    LHServCostGrid.canChk = 0;
	*/	    LHServCostGrid.selBoxEventFuncName = "showOne";

	    LHServCostGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //LHServCostGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
		alert(ex);
	}
}

function initTestItemPriceGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="检查项目代码";   
		iArray[1][1]="150px";   
		iArray[1][2]=20;        
		iArray[1][3]=2;													//选‘2’背景色为蓝色
		iArray[1][4]="hmtestgrpquery";           //CodeQueryBL中的引用参数                 
		iArray[1][5]="1|2";     								//引用代码对应第几列，'|'为分割符
		iArray[1][6]="0|1";    									//上面的列中放置引用代码中第几位值
		iArray[1][9]="检查项目代码|LEN<20";                      
		iArray[1][15]="code";         //数据库中字段，用于模糊查询                 
		iArray[1][17]="1";                                       
		iArray[1][19]="1" ;                                      
		
		
		iArray[2]=new Array(); 
		iArray[2][0]="检查项目名称";   
		iArray[2][1]="200px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;   
		iArray[2][4]="hmtestgrpquery";                           
		iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
		iArray[2][6]="1|0";    //上面的列中放置引用代码中第几位值
		iArray[2][15]="name";                          
		iArray[2][17]="2";                                       
		iArray[2][19]="1" ;  
		iArray[2][18]=325 ;                                          
		
		
		
		iArray[3]=new Array(); 
		iArray[3][0]="检查项目单价（元）";   
		iArray[3][1]="130px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="补充说明";
		iArray[4][1]="170px";         
		iArray[4][2]=20;            
		iArray[4][3]=1;
		
		iArray[5]=new Array();
		iArray[5][0]="价格类别";
		iArray[5][1]="0px";         
		iArray[5][2]=20;            
		iArray[5][3]=3;
		iArray[5][14]="JCXM";
		
		iArray[6]=new Array(); 
		iArray[6][0]="SerialNo";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3; 
		             
		
    TestItemPriceGrid = new MulLineEnter( "fm" , "TestItemPriceGrid" ); 
    //这些属性必须在loadMulLine前

    TestItemPriceGrid.mulLineCount = 0;   
    TestItemPriceGrid.displayTitle = 1;
    TestItemPriceGrid.hiddenPlus = 1;
    TestItemPriceGrid.hiddenSubtraction = 0;
    TestItemPriceGrid.canSel = 0;
    TestItemPriceGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    TestItemPriceGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //TestItemPriceGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
//var TestGrpPriceGrid;
function initTestGrpPriceGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
		iArray[1][0]="体检套餐代码";   
		iArray[1][1]="150px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
		iArray[1][9]="体检套餐代码|LEN<60"
		
		iArray[2]=new Array(); 
		iArray[2][0]="体检套餐名称";   
		iArray[2][1]="200px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		if(fm.all('ContraType').value=="01")
		{ 
		 iArray[2][4]="hmchoosetest";  
	  }  
	  if(fm.all('ContraType').value=="02")
		{ 
		 iArray[2][4]="hmchoosetest2";  
	  }                        
		iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
		iArray[2][6]="0|1";    //上面的列中放置引用代码中第几位值
		iArray[2][18]=325;
		
		iArray[3]=new Array(); 
		iArray[3][0]="体检套餐总价格（元）";   
		iArray[3][1]="130px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="补充说明";
		iArray[4][1]="170px";         
		iArray[4][2]=20;            
		iArray[4][3]=1;    
		
		iArray[5]=new Array();  
		iArray[5][0]="价格类别";         
		iArray[5][1]="0px";    
    iArray[5][2]=20;       
    iArray[5][3]=3;         //这些属性必须在loadMulLine前
		iArray[5][14]="TJTC";
		
		iArray[6]=new Array(); 
		iArray[6][0]="SerialNo2";   
		iArray[6][1]="0px";   
		iArray[6][2]=20;        
		iArray[6][3]=3; 

    TestGrpPriceGrid = new MulLineEnter( "fm" , "TestGrpPriceGrid" ); 
    TestGrpPriceGrid.mulLineCount = 0;   
    TestGrpPriceGrid.displayTitle = 1;
    TestGrpPriceGrid.hiddenPlus = 1;
    TestGrpPriceGrid.hiddenSubtraction = 0;
    TestGrpPriceGrid.canSel = 0;
    TestGrpPriceGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    TestGrpPriceGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //TestGrpPriceGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}


</script>
