<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServCaseExecManageUpdate.jsp
//程序功能：
//创建日期：2006-08-02 14:28:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHServiceManageUI tLHServiceManageUI   = new LHServiceManageUI();
  
  LHCaseTaskRelaSet tLHCaseTaskRelaSet = new LHCaseTaskRelaSet();		//服务事件任务关联表
    
    String tServCaseCode[] = request.getParameterValues("LHCaseInfoGrid2");           //事件号码 
    String tServTaskNo[] = request.getParameterValues("LHCaseInfoGrid4");					//任务号码  
    String tServTaskCode[] = request.getParameterValues("LHCaseInfoGrid8");           //任务代码
    String tServTaskAffirm[] = request.getParameterValues("LHCaseInfoGrid9");					//任务确认
    String tExecuteOperator[] = request.getParameterValues("LHCaseInfoGrid10");					//任务实施人员
    String tPlanExeDate[] = request.getParameterValues("LHCaseInfoGrid11");					//计划实施时间
    String tTaskFinishDate[] = request.getParameterValues("LHCaseInfoGrid6");					//任务完成时间
    
    //String tChk[] = request.getParameterValues("InpLHCaseInfoGridChk"); //参数格式=” Inp+MulLine对象名+Chk”     	 
     
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");

   int LHCaseTaskRelaCount = 0;
   System.out.println("R R  "+tServTaskNo.length);
	 if(tServTaskNo != null)
	 {	
		    LHCaseTaskRelaCount = tServTaskNo.length;
	 }	
	  System.out.println(" LHCaseTaskRelaCount is : "+LHCaseTaskRelaCount);
    for(int j = 0; j < LHCaseTaskRelaCount; j++)
	  {
		    LHCaseTaskRelaSchema tLHCaseTaskRelaSchema = new LHCaseTaskRelaSchema();
		    
		     tLHCaseTaskRelaSchema.setServTaskNo(tServTaskNo[j]);	 //流水号
		     System.out.println(":SS ccServCaseCode SS "+j+"  "+tLHCaseTaskRelaSchema.getServTaskNo()); 
		     tLHCaseTaskRelaSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		     System.out.println(":AA ccServCaseCode AA "+j+"  "+tLHCaseTaskRelaSchema.getServCaseCode()); 
		     tLHCaseTaskRelaSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		     System.out.println(":CC ccServTaskCode CC "+j+"  "+tLHCaseTaskRelaSchema.getServTaskCode());  
		     tLHCaseTaskRelaSchema.setServTaskAffirm(tServTaskAffirm[j]);	//任务确认
		     System.out.println(":DD ServTaskAffirm DD "+j+"  "+tLHCaseTaskRelaSchema.getServTaskAffirm());  
		     tLHCaseTaskRelaSchema.setExecuteOperator(tExecuteOperator[j]);	//任务实施人员
		     System.out.println(":FF ExecutOperator FF "+j+"  "+tLHCaseTaskRelaSchema.getExecuteOperator());  
		     tLHCaseTaskRelaSchema.setPlanExeDate(tPlanExeDate[j]);	 //任务实施时间
		     System.out.println(":GG cccPlanExeDate GG "+j+"  "+tLHCaseTaskRelaSchema.getPlanExeDate());  
		    // tLHCaseTaskRelaSchema.setTaskFinishDate(tTaskFinishDate[j]);	//任务完成时间
		     //System.out.println(":HH cccTaskFinishDate "+j+"  "+tLHCaseTaskRelaSchema.getTaskFinishDate());  
		     
		     
		     tLHCaseTaskRelaSchema.setServTaskState(request.getParameter("ServTaskState"));	 //任务状态
		     System.out.println(":EE cServTaskState EE "  +tLHCaseTaskRelaSchema.getServTaskState());  
		     tLHCaseTaskRelaSchema.setTaskFinishDate(request.getParameter("TaskFinishDate"));	 //任务完成时间
		     tLHCaseTaskRelaSchema.setTaskDesc(request.getParameter("ServItemNote"));	//任务描述
		     System.out.println(":KK ccccccTaskDesc LL "   +tLHCaseTaskRelaSchema.getTaskDesc());  
         tLHCaseTaskRelaSet.add(tLHCaseTaskRelaSchema);          
         //System.out.println("QQ "+ tLHCaseTaskRelaSchema.getServCaseCode()+"  " + tChk[j]);  
                              
          //if(tChk[j].equals("1"))  
          //{        
          //       
          //     System.out.println(":BB cccccccc BB "+tServCaseCode[j]);
          //     System.out.println("该行被选中 "+tServCaseCode[j]);     
          //     tLHCaseTaskRelaSet.add(tLHCaseTaskRelaSchema);         
          //     System.out.println("OOOOOOOOOOOOOOOOOOOOO");  
          //}
          //if(tChk[j].equals("0"))  
          //{     System.out.println(":D cccccccc DD "+tServCaseCode[j]);
          //      System.out.println("该行未被选中 "+tServCaseCode[j]);
          //}
      }
                              
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHCaseTaskRelaSet);
  	tVData.add(tG);
  	System.out.println("- DDDDDDDDDD "+transact);
    tLHServiceManageUI.submitData(tVData,transact);
    System.out.println("- SSSSSSSSSSSS "+transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHServiceManageUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");   
</script>
</html>