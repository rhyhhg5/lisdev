<%
//程序名称：LHServiceManagerInputInit.jsp
//程序功能：
//创建日期：2006-08-15 18:29:02
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
        String ServTaskNo = request.getParameter("ServTaskNo");
        String caseCode = request.getParameter("caseCode");
        String flag = request.getParameter("flag");
%>         
           
<script language="JavaScript"> 
function displayInfo()
{
	fm.all('caseCode').value = "<%=caseCode%>";
	var taskNo = null;
	taskNo = new Array();
	var xxxx = "<%=ServTaskNo%>";
	taskNo=xxxx.split(",");
	var tArr = new Array();
	for (var i=0;i<taskNo.length;i++)
	{
		var sqlRela="select h.ServTaskState,h.TaskDesc,h.TaskFinishDate from LHCaseTaskRela h "
		             +" where h.ServCaseCode='"+ fm.all('caseCode').value +"' and h.ServTaskNo='"+  taskNo[i] +"'";

	    var kk=new Array;
	    kk=easyExecSql(sqlRela);
	    
		if(kk=="null"||kk==""||kk==null)
		{
	        var strsql="select "
	                  +" (select case when a.ServCaseType='1' then '个人服务事件' when a.ServCaseType='2' then '集体服务事件' when a.ServCaseType='3' then '费用结算事件' else '无' end from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode ),"
	                  +" b.ServCaseCode ,"
	                  +" (select a.ServCaseName from LHServCaseDef a  where a.ServCaseCode=b.ServCaseCode ),"
	                  +" b.ServTaskNo,"
	                  +" (select c.ServTaskName from LHServTaskDef c where c.ServTaskCode=b.ServTaskCode),"
	                  +" b.MakeDate ,"
	                  +" (select a.ServCaseType  from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode ), "
	                  +" b.ServTaskCode,b.ServTaskAffirm,b.ExecuteOperator,b.PlanExeDate "
	                  +" from LHCaseTaskRela b "
	                  +" where b.ServCaseCode='"+ fm.all('caseCode').value +"'"
	                  +" and b.ServTaskNo='"+ taskNo[i] +"'";
			var tArr1= new Array();
			tArr1 = easyExecSql(strsql);  
	        tArr[i]=tArr1[0];
	     }
	     else
	     {
			if(kk[0][0]=="3"||kk[0][0]=="4"||kk[0][0]=="5")
	     	{
	     	     fm.all('ServTaskState').value=kk[0][0];
	     	     fm.all('ServTaskStateName').value=easyExecSql("select d.codename from ldcode d where d.codetype='taskstatus' and d.code='"+fm.all('ServTaskState').value+"'");
	     	}
			//fm.all('TaskFinishDate').value=kk[0][2];
			fm.all('ServItemNote').value=kk[0][1];
			var strsql="select "
	                  +" (select case when a.ServCaseType='1' then '个人服务事件' when a.ServCaseType='2' then '集体服务事件' when a.ServCaseType='3' then '费用结算事件' else '无' end from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode ),"
	                  +" b.ServCaseCode ,"
	                  +" (select a.ServCaseName from LHServCaseDef a  where a.ServCaseCode=b.ServCaseCode ),"
	                  +" b.ServTaskNo,"
	                  +" (select c.ServTaskName from LHServTaskDef c where c.ServTaskCode=b.ServTaskCode),"
	                  +" b.TaskFinishDate ,"
	                  +" (select a.ServCaseType  from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode ), "
	                  +" b.ServTaskCode,b.ServTaskAffirm,b.ExecuteOperator,b.PlanExeDate "
	                  +" from LHCaseTaskRela b "
	                  +" where b.ServCaseCode='"+ fm.all('caseCode').value +"'"
	                  +" and b.ServTaskNo='"+ taskNo[i] +"'";
			var tArr1= new Array();
		    tArr1 = easyExecSql(strsql);  
	        tArr[i]=tArr1[0];
		}
	}
	if(tArr!="null"&&tArr!=""&&tArr!=null)
	{
		displayMultiline(tArr, LHCaseInfoGrid);
	}
}     

function initForm()
{
	try
	{
	    initLHCaseInfoGrid();
	    var ff= "<%=flag%>";
	    if(ff=="1")//从服务事件实施管理页面进入,一次只能选一个任务
	    {
		    displayInfo(); 
		    var caseCode = "<%=caseCode%>";
		    var ServTaskNo = "<%=ServTaskNo%>";
        	var sqlRela="select h.ServTaskAffirm from LHCaseTaskRela h where h.ServCaseCode='"+ caseCode +"' and h.ServTaskNo='"+ ServTaskNo+"'";
	        var affirm=easyExecSql(sqlRela);
	        if(affirm=="1")//任务未确认
	        {
	      		fm.all('save').disabled=false;
	      	    fm.all('update').disabled=false;
	        }
		    if(affirm=="2")//任务已确认
		    {
				fm.all('save').disabled=true;
				fm.all('update').disabled=true;
		    }
    	}
    	if(ff=="2")//从服务任务查询页面进入，可选择多个任务
    	{
    		displayCode();
    		var caseCode = new Array();
	    	var cCode = "<%=caseCode%>";
	    	caseCode=cCode.split(",");
	    	var ServTaskNo = new Array();
	    	var stNo = "<%=ServTaskNo%>";
	    	ServTaskNo=stNo.split(",");
   
			//判断操作按钮是否有效
			var sqlRela="select h.ServTaskAffirm from LHCaseTaskRela h where h.ServCaseCode='"+ caseCode[0] +"' and h.ServTaskNo='"+ ServTaskNo[0]+"'";
        	var affirm=easyExecSql(sqlRela);
	        if(affirm=="1")//任务未确认
	        {
				fm.all('save').disabled=false;
	        	fm.all('update').disabled=false;
	        }
	        if(affirm=="2")//任务已确认
	        {
	        	fm.all('save').disabled=true;
	        	fm.all('update').disabled=true;
	        }
    	}
    var ServTaskCode=LHCaseInfoGrid.getRowColData(0,8);//任务编号 
    //alert(ServTaskCode);	
	}
  catch(re)
  {
    alert("LHServiceManagerInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function displayCode()
{
	var codeNo = null;
	codeNo = new Array();
	var cCode = "<%=caseCode%>";
	codeNo=cCode.split(",");

	var taskNo = null;
	taskNo = new Array();
	var stNo = "<%=ServTaskNo%>";
	taskNo=stNo.split(",");

	var tArr = new Array();
	for (var i=0;i<codeNo.length;i++)
	{
		var sqlRela="select h.ServTaskState,h.TaskDesc,h.TaskFinishDate from LHCaseTaskRela h "
				   +" where h.ServCaseCode='"+ codeNo[i] +"' and h.ServTaskNo='"+  taskNo[i] +"'";
		var kk=easyExecSql(sqlRela);

		if(kk=="null"||kk==""||kk==null)
		{
	        var strsql="select "
	                  +" (select case when a.ServCaseType='1' then '个人服务事件' when a.ServCaseType='2' then '集体服务事件' when a.ServCaseType='3' then '费用结算事件' else '无' end from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode ),"
	                  +" b.ServCaseCode ,"
	                  +" (select a.ServCaseName from LHServCaseDef a  where a.ServCaseCode=b.ServCaseCode ),"
	                  +" b.ServTaskNo,"
	                  +" (select c.ServTaskName from LHServTaskDef c where c.ServTaskCode=b.ServTaskCode),"
	                  +" b.MakeDate ,"
	                  +" (select a.ServCaseType  from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode ), "
	                  +" b.ServTaskCode,b.ServTaskAffirm,b.ExecuteOperator,b.PlanExeDate "
	                  +" from LHCaseTaskRela b "
	                  +" where b.ServCaseCode='"+ codeNo[i] +"'"
	                  +" and b.ServTaskNo='"+ taskNo[i] +"'";
		      var tArr1= new Array();
		      tArr1 = easyExecSql(strsql);  
	          tArr[i]=tArr1[0];
	     }
	     else
	     {
	     	  if(kk[0][0]=="3"||kk[0][0]=="4"||kk[0][0]=="5")
	     	  {
	     	     fm.all('ServTaskState').value=kk[0][0];
	     	     fm.all('ServTaskStateName').value=easyExecSql("select d.codename from ldcode d where d.codetype='taskstatus' and d.code='"+fm.all('ServTaskState').value+"'");
	     	  }
	     	  //fm.all('TaskFinishDate').value=kk[0][2];
	     	  fm.all('ServItemNote').value=kk[0][1];
	     	  var strsql="select "
	                  +" (select case when a.ServCaseType='1' then '个人服务事件' when a.ServCaseType='2' then '集体服务事件' when a.ServCaseType='3' then '费用结算事件' else '无' end from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode ),"
	                  +" b.ServCaseCode ,"
	                  +" (select a.ServCaseName from LHServCaseDef a  where a.ServCaseCode=b.ServCaseCode ),"
	                  +" b.ServTaskNo,"
	                  +" (select c.ServTaskName from LHServTaskDef c where c.ServTaskCode=b.ServTaskCode),"
	                  +" b.TaskFinishDate ,"
	                  +" (select a.ServCaseType  from LHServCaseDef a where a.ServCaseCode=b.ServCaseCode ), "
	                  +" b.ServTaskCode,b.ServTaskAffirm,b.ExecuteOperator,b.PlanExeDate "
	                  +" from LHCaseTaskRela b "
	                  +" where b.ServCaseCode='"+ codeNo[i]  +"'"
	                  +" and b.ServTaskNo='"+ taskNo[i] +"'";
	          
		      var tArr1= new Array();
		      tArr1 = easyExecSql(strsql);  
	          tArr[i]=tArr1[0];
	     }
	}
	if(tArr!="null"&&tArr!=""&&tArr!=null)
	{
		displayMultiline(tArr, LHCaseInfoGrid);
	}
 
}
var d = new Date();
var h = d.getYear();
var m = d.getMonth(); 
var day = d.getDate();  
var Date2;       
if(h<10){h = "0"+d.getYear();}  
if(m<9){ m++; m = "0"+m;}
else{m++;}
if(day<10){day = "0"+d.getDate();}
Date2 = h+"-"+m+"-"+day;
var LHCaseInfoGrid;
function initLHCaseInfoGrid() 
{                               
  var iArray = new Array();
    
  try
   {
  iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;
     
    iArray[1]=new Array();
    iArray[1][0]="服务事件类型";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][2]=20;        
    iArray[1][3]=0;         		//列名
            		//列名
    
    iArray[2]=new Array(); 
		iArray[2][0]="服务事件编号";   
		iArray[2][1]="100px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
	
		
		iArray[3]=new Array(); 
		iArray[3][0]="服务事件名称";   
		iArray[3][1]="250px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
	
		
		iArray[4]=new Array(); 
		iArray[4][0]="服务任务编号";   
		iArray[4][1]="100px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
	   
		
		iArray[5]=new Array();
		iArray[5][0]="服务任务名称";
		iArray[5][1]="250px";         
		iArray[5][2]=20;            
		iArray[5][3]=0;       
		
		iArray[6]=new Array();
		iArray[6][0]="任务完成时间";
		iArray[6][1]="70px";         
		iArray[6][2]=20;            
		iArray[6][3]=0; 
		iArray[6][14]=Date2;
		
	  iArray[7]=new Array(); 
	  iArray[7][0]="eventtype";   
	  iArray[7][1]="0px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=3;
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="ServTaskCode";   
	  iArray[8][1]="0px";   
	  iArray[8][2]=3;        
	  iArray[8][3]=3;
	  
	  iArray[9]=new Array(); 
	  iArray[9][0]="ServTaskAffirm";   
	  iArray[9][1]="0px";   
	  iArray[9][2]=20;        
	  iArray[9][3]=3;
	  
	  iArray[10]=new Array(); 
	  iArray[10][0]="ExecuteOperator";   
	  iArray[10][1]="0px";   
	  iArray[10][2]=20;        
	  iArray[10][3]=3;
	  
	  iArray[11]=new Array(); 
	  iArray[11][0]="PlanExeDate";   
	  iArray[11][1]="0px";   
	  iArray[11][2]=20;        
	  iArray[11][3]=3;
		 
		            
		
    LHCaseInfoGrid = new MulLineEnter( "fm" , "LHCaseInfoGrid" ); 
    //这些属性必须在loadMulLine前
    
    LHCaseInfoGrid.mulLineCount = 0;   
    LHCaseInfoGrid.displayTitle = 1;
    LHCaseInfoGrid.hiddenPlus = 1;
    LHCaseInfoGrid.hiddenSubtraction = 1;
    var TaskCode = "<%=ServTaskNo%>";
    var taskNo = null;
	  taskNo = new Array();;
	  taskNo=TaskCode.split(",");
	  //alert(taskNo.length);                                                    
     var sqlRela="select h.ServTaskCode from LHCaseTaskRela h "
				   				 +" where  h.ServTaskNo='"+  taskNo[0] +"'";//服务任务名称为问卷录入的类型时
		 //alert(sqlRela);
		 var caseTaskCode=easyExecSql(sqlRela);
		 //alert(caseTaskCode);
		 if(caseTaskCode=="00203"||caseTaskCode=="00401")
		 {
       LHCaseInfoGrid.canSel = 1;
     }
     else
     {
     	LHCaseInfoGrid.canSel = 0;
     }
    LHCaseInfoGrid.canChk = 0;
    //LHCaseInfoGrid.chkBoxEventFuncName = "showTwo";     
    //LHCaseInfoGrid.selBoxEventFuncName = "showOne";            
    
    LHCaseInfoGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCaseInfoGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
  	
    alert(ex);
  }
} 
function afterCodeSelect(codeName,Field)
{
	
	if(codeName == "taskstatus")
	{
		if(Field.value=="3")
		{
			var caseCode = null;
			caseCode = new Array();
			var rowNum2=LHCaseInfoGrid. mulLineCount ; //行数 	
	    	var bb2 = new Array();
	    	var yy2 = 0;
	    	for(var row2=0; row2 < rowNum2; row2++)
	    	{
				bb2[yy2] = row2;
				caseCode[yy2++]= LHCaseInfoGrid.getRowColData(bb2,2);
	    	}
			var arrSelected = null;
	    	arrSelected = new Array();
	    	var rowNum=LHCaseInfoGrid. mulLineCount ; //行数 	
	    	var bb = new Array();
	    	var yy = 0;
	    	for(var row=0; row < rowNum; row++)
	    	{
				bb[yy] = row;
				arrSelected[yy++] = LHCaseInfoGrid.getRowColData(bb,4);
	    	}
			window.open("./LHServiceManageLookMain.jsp?caseCode="+caseCode+"&arrSelected="+arrSelected+"&flag=finish","事件服务信息查看",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
		    fm.all('ServTaskState').value="3";
		    fm.all('ServTaskStateName').value="任务完成";
		}
		if(Field.value=="4"||Field.value=="5")
		{
		    var caseCode = null;
			caseCode = new Array();
			var rowNum2=LHCaseInfoGrid. mulLineCount ; //行数 	
			var bb2 = new Array();
			var yy2 = 0;
			for(var row2=0; row2 < rowNum2; row2++)
	        {
				bb2[yy2] = row2;
				caseCode[yy2++]= LHCaseInfoGrid.getRowColData(bb2,2);
    	    }
		    var arrSelected = null;
	      	arrSelected = new Array();
	      	var rowNum=LHCaseInfoGrid. mulLineCount ; //行数 	
	      	var bb = new Array();
	      	var yy = 0;
	      	for(var row=0; row < rowNum; row++)
	      	{
				bb[yy] = row;
				arrSelected[yy++] = LHCaseInfoGrid.getRowColData(bb,4);
	      	}
	        window.open("./LHServiceManageLookMain.jsp?caseCode="+caseCode+"&arrSelected="+arrSelected+"&flag=finish","事件服务信息查看",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
		    if(Field.value=="4")
		    {
		      fm.all('ServTaskState').value="4";
		      fm.all('ServTaskStateName').value="任务失败";
		    }
		    if(Field.value=="5")
		    {
		      fm.all('ServTaskState').value="5";
		      fm.all('ServTaskStateName').value="任务取消";
		    }
		}
	}
}

</script>