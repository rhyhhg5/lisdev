<%
//程序名称：LHServTaskQueryInit.jsp
//程序功能：
//创建日期：2006-07-24 10:56:23
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类--> 
                          
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('GrpServPlanNo').value = "";
    fm.all('TaskValidate').value = "1";
    fm.all('TaskValidateName').value = "未确认";
    fm.all('ComID').value = manageCom.substring(0, 4);
  }
  catch(ex)
  {
    alert("在LHServTaskQueryInit.jsp->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    initLHSettingSlipQueryGrid() ;
    initLHQueryDetailGrid();
  }
  catch(re)
  {
    alert("LHServTaskQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHSettingSlipQueryGrid;
function initLHSettingSlipQueryGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="服务事件编号 ";   
	  iArray[1][1]="0px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=3;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="服务事件名称";   
	  iArray[2][1]="260px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="服务任务名称";   
	  iArray[3][1]="120px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=1;
	  iArray[3][5]="3|5";     //引用代码对应第几列，'|'为分割符
      iArray[3][6]="1|0";     //上面的列中放置引用代码中第几位
      iArray[3][17]="1"; 
      iArray[3][18]="160";
      iArray[3][19]="1" ;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="任务状态";   
	  iArray[4][1]="60px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=1;
	  iArray[4][5]="4|6";     //引用代码对应第几列，'|'为分割符
      iArray[4][6]="1|0";     //上面的列中放置引用代码中第几位
      iArray[4][17]="1"; 
      iArray[4][18]="160";
      iArray[4][19]="1" ;
    
	  iArray[5]=new Array(); 
	  iArray[5][0]="taskname";   
	  iArray[5][1]="0px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=3;
	  iArray[5][14]="1";
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="taskstatus";   
	  iArray[6][1]="0px";   
	  iArray[6][2]=20;        
	  iArray[6][3]=3;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="ServTaskNo";   
	  iArray[7][1]="0px";   
	  iArray[7][2]=20;        
	  iArray[7][3]=3;
	  
	  iArray[8]=new Array(); 
	  iArray[8][0]="任务确认";   
	  iArray[8][1]="50px";   
	  iArray[8][2]=20;        
	  iArray[8][3]=1;
	  
	  iArray[9]=new Array(); 
	  iArray[9][0]="ServTaskAffirm";   
	  iArray[9][1]="0px";   
	  iArray[9][2]=20;        
	  iArray[9][3]=3;
	  
	  iArray[10]=new Array(); 
	  iArray[10][0]="计划实施时间";   
	  iArray[10][1]="80px";   
	  iArray[10][2]=20;        
	  iArray[10][3]=1;
	  
	  iArray[11]=new Array(); 
	  iArray[11][0]="实际实施时间";   
	  iArray[11][1]="80px";   
	  iArray[11][2]=20;        
	  iArray[11][3]=1;

	                               
    LHSettingSlipQueryGrid = new MulLineEnter( "fm" , "LHSettingSlipQueryGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHSettingSlipQueryGrid.mulLineCount = 0;                             
    LHSettingSlipQueryGrid.displayTitle = 1;                          
    LHSettingSlipQueryGrid.hiddenPlus = 1;                          
    LHSettingSlipQueryGrid.hiddenSubtraction = 1;                          
    LHSettingSlipQueryGrid.canSel = 0;                          
    LHSettingSlipQueryGrid.canChk = 1;                          
   // LHSettingSlipQueryGrid.selBoxEventFuncName = "showOne";                       
                                                             
    LHSettingSlipQueryGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHSettingSlipQueryGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}
var LHQueryDetailGrid;
function initLHQueryDetailGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=1;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="客户号";   
	  iArray[1][1]="150px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array(); 
	  iArray[2][0]="客户姓名";   
	  iArray[2][1]="150px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array(); 
	  iArray[3][0]="服务计划名称";   
	  iArray[3][1]="150px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=0;
	  
	  iArray[4]=new Array(); 
	  iArray[4][0]="服务计划档次";   
	  iArray[4][1]="150px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	                               
    LHQueryDetailGrid = new MulLineEnter( "fm" , "LHQueryDetailGrid" );           
    //这些属性必须在loadMulLine前                          
                          
    LHQueryDetailGrid.mulLineCount = 0;                             
    LHQueryDetailGrid.displayTitle = 1;                          
    LHQueryDetailGrid.hiddenPlus = 1;                          
    LHQueryDetailGrid.hiddenSubtraction = 1;                          
    LHQueryDetailGrid.canSel = 1;                          
    LHQueryDetailGrid.canChk = 0;                          
                    
                                                             
    LHQueryDetailGrid.loadMulLine(iArray);                                     
    //这些操作必须在loadMulLine后面                                         
    //LHQueryDetailGrid.setRowColData(1,1,"asdf");                                   
  }
  catch(ex) {
    alert(ex);
  }
}
 
</script>
