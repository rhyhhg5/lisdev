<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServiceManageLookSave.jsp
//程序功能：
//创建日期：2006-08-16 8:34:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
	//接收信息，并作校验处理。
  	//输入参数
  	TransferData mTransferData = new TransferData();
  	
	LHServiceManageLookUI tLHServiceManageLookUI   = new LHServiceManageLookUI();
	LHTaskCustomerRelaSet tLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();		//服务事件任务关联表
    
    String tTaskExecNo[] = request.getParameterValues("LHCaseInfotwoGrid14");		//任务实施号码
    String tServTaskCode[] = request.getParameterValues("LHCaseInfotwoGrid15");		//任务实施号码
    String tServCaseCode[] = request.getParameterValues("LHCaseInfotwoGrid1");		//服务事件号码  
    String tServTaskNo[] = request.getParameterValues("LHCaseInfotwoGrid3");		//服务任务号码
    String tServItemNo[] = request.getParameterValues("LHCaseInfotwoGrid13");		//服务项目序号
    String tTaskExecState[] = request.getParameterValues("LHCaseInfotwoGrid12");	//任务执行状态
    
    String tChk[] = request.getParameterValues("InpLHCaseInfotwoGridChk"); 		//参数格式=” Inp+MulLine对象名+Chk”     	 
     
    //输出参数
    CErrors tError = null;
    String tRela  = "";
    String FlagStr = "";
    String Content = "";
    String transact = "";
    GlobalInput tG = new GlobalInput(); 
    tG=(GlobalInput)session.getValue("GI");
	
  	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");
  	mTransferData.setNameAndValue("ServCaseCode",request.getParameter("ServCaseCode"));
  	mTransferData.setNameAndValue("ServTaskNo",request.getParameter("ServTaskNo"));
  	mTransferData.setNameAndValue("StateFlag",request.getParameter("StateFlag"));
  	

	int LHTaskCustomerRelaCount = 0;
	if(tTaskExecNo != null)
	{	
		LHTaskCustomerRelaCount = tServTaskNo.length;
	}	
    for(int j = 0; j < LHTaskCustomerRelaCount; j++)
	{
		LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema();
		    
		tLHTaskCustomerRelaSchema.setTaskExecNo(tTaskExecNo[j]);		//流水号
		tLHTaskCustomerRelaSchema.setServTaskCode(tServTaskCode[j]);	//事件号码
		tLHTaskCustomerRelaSchema.setServCaseCode(tServCaseCode[j]);	//事件号码
		tLHTaskCustomerRelaSchema.setServTaskNo(tServTaskNo[j]);		//任务代码 
		tLHTaskCustomerRelaSchema.setServItemNo(tServItemNo[j]);		//项目号码
		tLHTaskCustomerRelaSchema.setTaskExecState(tTaskExecState[j]);	//任务执行状态  
		
		if(transact.equals("INSERT||MAIN"))
        {
			tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);     
        }  
        if(transact.equals("UPDATE||MAIN"))
        {
            if(tChk[j].equals("1"))  
            {
                System.out.println("该行被选中 "+tTaskExecNo[j]);     
        		tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);     
			}
        }                 
    }
                              
                              
                              
	try
	{
		// 准备传输数据 VData
	  	VData tVData = new VData();
	  	tVData.add(tLHTaskCustomerRelaSet);
	  	tVData.addElement(mTransferData);
	  	tVData.add(tG);
    	tLHServiceManageLookUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
	    Content = "操作失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = tLHServiceManageLookUI.mErrors;
	  	if (!tError.needDealError())
	  	{                          
	  		Content = " 操作成功! ";
	  		FlagStr = "Success";
	  	}
	  	else                                                                           
	  	{
	  		Content = " 操作失败，原因是:" + tError.getFirstError();
	  		FlagStr = "Fail";
	  	}
	}
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	  var transact = "<%=transact%>";
	  if(transact=="INSERT||MAIN")
	  {
	      parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
	  } 
	  if(transact=="UPDATE||MAIN")
	  {
	      parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>"); 
	  }  
</script>
</html>