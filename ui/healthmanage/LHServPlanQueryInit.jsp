<%
//程序名称：LHServPlanQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-03-09 10:42:58
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
//    fm.all('GrpServPlanNo').value = "";
    fm.all('ServPlanNo').value = "";
    fm.all('ContNo').value = "";
    fm.all('CustomerNo').value = "";
    fm.all('Name').value = "";
//    fm.all('ServPlanCode').value = "";
    fm.all('ServPlanName').value = "";
    fm.all('ServPlanLevel').value = "";
//    fm.all('ServPlayType').value = "";
//    fm.all('ComID').value = "";
//   fm.all('StartDate').value = "";                                  
//    fm.all('EndDate').value = "";                                  
//    fm.all('ServPrem').value = "";                                  
//    fm.all('ManageCom').value = "";                                  
//    fm.all('Operator').value = "";                                  
//    fm.all('MakeDate').value = "";                                  
//    fm.all('MakeTime').value = "";                                  
//    fm.all('ModifyDate').value = "";                                  
//    fm.all('ModifyTime').value = "";                                  
  }
  catch(ex) {
    alert("在LHServPlanQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHServPlanGrid();  
  }
  catch(re) {
    alert("LHServPlanQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHServPlanGrid;
function initLHServPlanGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
     
    iArray[1]=new Array();                    
    iArray[1][0]="个人服务计划号码";         		//列名    
    iArray[1][1]="55px";         		//列名    
    iArray[1][3]=0;         		//列名        
    
    
    iArray[2]=new Array();                       
    iArray[2][0]="保单号";         		//列名      
    iArray[2][1]="30px";         		//列名      
    iArray[2][3]=0;         		//列名            
    
    iArray[3]=new Array();                       
    iArray[3][0]="客户号";         		//列名      
    iArray[3][1]="30px";         		//列名      
    iArray[3][3]=0;         		//列名            

    iArray[4]=new Array();                         
    iArray[4][0]="姓名";         		//列名         
    iArray[4][1]="30px";         		//列名         
    iArray[4][3]=0;         		//列名             

    iArray[5]=new Array();                         
    iArray[5][0]="服务计划类型";         		//列名         
    iArray[5][1]="30px";         		//列名         
    iArray[5][3]=0;         		//列名     
    
    iArray[6]=new Array();                                                
    iArray[6][0]="服务计划起始时间";         		//列名                    
    iArray[6][1]="40px";         		//列名                                
    iArray[6][3]=0;         		//列名                                    
                                                                          
    iArray[7]=new Array();                                                
    iArray[7][0]="服务计划起始时间";         		//列名                              
    iArray[7][1]="40px";         		//列名                                
    iArray[7][3]=0;         		//列名                                    
                                                                          
    iArray[8]=new Array();                                                
    iArray[8][0]="健管保费（元）";         		//列名                                
    iArray[8][1]="30px";         		//列名                                
    iArray[8][3]=0;         		//列名                                  
                                                                            
    LHServPlanGrid = new MulLineEnter( "fm" , "LHServPlanGrid" );           
    LHServPlanGrid.mulLineCount = 0;   
    LHServPlanGrid.displayTitle = 1;
    LHServPlanGrid.hiddenPlus = 0;
    LHServPlanGrid.hiddenSubtraction = 0;
    LHServPlanGrid.canSel = 1;
    LHServPlanGrid.canChk = 0;
//    LHServPlanGrid.selBoxEventFuncName = "showOne";

    LHServPlanGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHServPlanGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
