<%
//程序名称：LHHmBespeakManageInit.jsp
//程序功能：健管服务预约管理
//创建日期：2006-09-01 15:51:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
	
	  String ServTaskNo = request.getParameter("ServTaskNo");
	  String sqlServTaskNo = "'"+ServTaskNo.replaceAll(",","','")+"'";
    String flag = request.getParameter("flag");
%>       
           
<script language="JavaScript">     
var turnPage = new turnPageClass();    
var aResult=new Array;                              
function initForm()
{
	fm.all('TPageTaskNo').value= "<%=sqlServTaskNo%>";
	fm.all('flag').value= "<%=flag%>";
	try
	{
	    initLHExecFeeBalanceGrid();
	    
		//取得传入任务流水号的所有任务执行信息
		var sqlExecNo  = " select TaskExecNo, ServCaseCode, ServTaskNo,ServItemNo from LHTaskCustomerRela "
						+" where ServTaskNo in ("+"<%=sqlServTaskNo%>"+") order by ServTaskNo";
		aResult = easyExecSql(sqlExecNo);
	    var flag= "<%=flag%>";
	    //alert(flag);
        if(flag=="1")
	    {
	    	 if(aResult!=""&&aResult!=null&&aResult!="null")
	    	 {
	    	     var strsql="  select TaskExecNo from LHExecFeeBalance d where d.TaskExecNo='"+ aResult[0][0]+"'" ;
		         var result=new Array;
		         result=easyExecSql(strsql);
		         //alert(result);
		         if(result=="" || result==null || result=="null")
		         {//未保存过
	    	        displayInfo();
	    	        fm.all('save').disabled=false;
	    	     }
	    	     if(result!=""&&result!="null"&&result!=null)
	    	     {
					displayConetent();
	    	        fm.all('save').disabled=true;
	    	     }
	    	 }
	    }
	}      
    catch(re)
    {      
		alert("LHHmBespeakManageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }      
}        
function displayInfo()
{
	strsql=" select (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo) ,"
		  +" (select a.CustomerName from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo),"
		  +" d.ServCaseCode,"
		  +" (select b.ServCaseName from LHServCaseDef b where b.ServCaseCode=d.ServCaseCode),"
		  +" (select c.ServTaskName from LHServTaskDef c where c.ServTaskCode=d.ServTaskCode),"
		  +" ServTaskCode,"
		  +" (select  d.ServItemName from LHHealthServItem d ,LHServItem a where d.ServItemCode=a.ServItemCode and a.ServItemNo= d.ServItemNo ),"
		  +" (select a.ServItemType from LHServItem a where a.ServItemNo=d.ServItemNo ),"
		  +" (select a.ContNo from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo= d.ServItemNo ),"
		  +" '','','','','','',TaskExecNo,ServItemNo,ServTaskNo,'已执行','2','',''"
		  +" from LHTaskCustomerRela d "
		  +" where d.ServTaskNo in ("+"<%=sqlServTaskNo%>"+") "
          ;
//		alert(easyExecSql(strsql));
		turnPage.pageLineNum = 15;  
		turnPage.queryModal(strsql, LHExecFeeBalanceGrid); 
}
function displayConetent()
{		
	  strsql=" select (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo ) ,"
		      +" (select a.CustomerName from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode  and a.ServItemNo=d.ServItemNo ),"
		      +" d.ServCaseCode,"
		      +" (select b.ServCaseName from LHServCaseDef b where b.ServCaseCode=d.ServCaseCode),"
		      +" (select c.ServTaskName from LHServTaskDef c where c.ServTaskCode=d.ServTaskCode),"
		      +" ServTaskCode,"
		      +" (select  d.ServItemName from LHHealthServItem d ,LHServItem a where d.ServItemCode=a.ServItemCode and a.ServItemNo=d.ServItemNo ),"
		      +" (select a.ServItemType from LHServItem a where a.ServItemNo=d.ServItemNo ),"
		      +" (select a.ContNo from LHServCaseRela a where a.ServCaseCode=d.ServCaseCode and a.ServItemNo=d.ServItemNo ),"
		      +" (select  HospitName from LDHospital where LDHospital.HospitCode=d.HospitCode),"
		      +" (select  ContraName from LHGroupCont where LHGroupCont.ContraNo=d.ContraNo),"
		      +" ContraItemNo,"
		      +" (select b.DutyItemName from LDContraItemDuty b where b.DutyItemCode=d.ContraItemNo),"
		      +" ContraNo,HospitCode,TaskExecNo,ServItemNo, ServTaskNo, "
		      +"(select case  when  a.TaskExecState='1' then '未执行' when a.TaskExecState='2' then '已执行'  else '' end from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo ),"
		      +"(select a.TaskExecState from LHTaskCustomerRela a where a.TaskExecNo=d.TaskExecNo),d.makedate,d.maketime"
		      +" from LHExecFeeBalance d "
		      +" where d.ServTaskNo in ("+"<%=sqlServTaskNo%>"+")"
		      ;
//		alert(easyExecSql(strsql));
		turnPage.pageLineNum = 15;  
		turnPage.queryModal(strsql, LHExecFeeBalanceGrid); 
}
function initLHExecFeeBalanceGrid() 
{                               
	var iArray = new Array();
	try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         		//列名
        iArray[0][1]="30px";         		//列名
        iArray[0][3]=0;
         
        iArray[1]=new Array();
        iArray[1][0]="客户号";         		//列名
        iArray[1][1]="70px";         		//列名
        iArray[1][2]=20;        
        iArray[1][3]=0;         		//列名
                		//列名
        
        iArray[2]=new Array(); 
	    iArray[2][0]="客户姓名";   
	    iArray[2][1]="60px";   
	    iArray[2][2]=20;        
	    iArray[2][3]=0;
	    	
	    iArray[3]=new Array(); 
	    iArray[3][0]="服务事件编号";   
	    iArray[3][1]="0px";   
	    iArray[3][2]=20;        
	    iArray[3][3]=3;
	    	
	    iArray[4]=new Array(); 
	    iArray[4][0]="服务事件名称";   
	    iArray[4][1]="160px";   
	    iArray[4][2]=20;        
	    iArray[4][3]=0;
	    
	    iArray[5]=new Array(); 
	    iArray[5][0]="服务任务名称";   
	    iArray[5][1]="0px";   
	    iArray[5][2]=20;        
	    iArray[5][3]=3;		
	    	
	    iArray[6]=new Array(); 
	    iArray[6][0]="ServTaskCode";   
	    iArray[6][1]="0px";   
	    iArray[6][2]=20;        
	    iArray[6][3]=3;
        
	    
	    iArray[7]=new Array(); 
	    iArray[7][0]="服务项目名称";   
	    iArray[7][1]="100px";   
	    iArray[7][2]=20;        
	    iArray[7][3]=0;
	    
	    iArray[8]=new Array(); 
	    iArray[8][0]="序号";   
	    iArray[8][1]="25px";   
	    iArray[8][2]=20;        
	    iArray[8][3]=0;
	    
	    iArray[9]=new Array(); 
	    iArray[9][0]="保单号";   
	    iArray[9][1]="90px";   
	    iArray[9][2]=20;        
	    iArray[9][3]=1;
	    
	    iArray[10]=new Array(); 
	    iArray[10][0]="费用结算机构";   
	    iArray[10][1]="150px";   
	    iArray[10][2]=20;        
	    iArray[10][3]=0;
	    
	    iArray[11]=new Array(); 
	    iArray[11][0]="合同名称";   
	    iArray[11][1]="150px";   
	    iArray[11][2]=20;        
	    iArray[11][3]=0;
	    
	    iArray[12]=new Array(); 
	    iArray[12][0]="合同责任代码";   
	    iArray[12][1]="0px";   
	    iArray[12][2]=20;        
	    iArray[12][3]=3;
	    
	    
	    iArray[13]=new Array(); 
	    iArray[13][0]="合同责任名称";   
	    iArray[13][1]="90px";   
	    iArray[13][2]=20;        
	    iArray[13][3]=0;
	    
	    iArray[14]=new Array(); 
	    iArray[14][0]="ContraNo";   
	    iArray[14][1]="80px";   
	    iArray[14][2]=20;        
	    iArray[14][3]=3;
	    
	    iArray[15]=new Array(); 
	    iArray[15][0]="HospitCode";   
	    iArray[15][1]="80px";   
	    iArray[15][2]=20;        
	    iArray[15][3]=3;
        
	    
	    iArray[16]=new Array(); 
	    iArray[16][0]="TaskExecNo";   
	    iArray[16][1]="0px";   
	    iArray[16][2]=20;        
	    iArray[16][3]=3;
	    
	    iArray[17]=new Array(); 
	    iArray[17][0]="ServItemNo";   
	    iArray[17][1]="0px";   
	    iArray[17][2]=20;        
	    iArray[17][3]=3;
	    
	    	  
	    iArray[18]=new Array(); 
	    iArray[18][0]="ServTaskNo";   
	    iArray[18][1]="0px";   
	    iArray[18][2]=20;        
	    iArray[18][3]=3;
	    
	    iArray[19]=new Array(); 
	    iArray[19][0]="执行状态";   
	    iArray[19][1]="50px";   
	    iArray[19][2]=20;        
	    iArray[19][3]=2;
	    iArray[19][4]="taskexecstuas";
	    iArray[19][5]="19|20";     //引用代码对应第几列，'|'为分割符
        iArray[19][6]="1|0";     //上面的列中放置引用代码中第几位
        iArray[19][14]="未执行"; 
        iArray[19][17]="1"; 
        iArray[19][18]="120";
        iArray[19][19]="1" ;
        
        iArray[20]=new Array(); 
	    iArray[20][0]="TaskExecState";   
	    iArray[20][1]="0px";   
	    iArray[20][2]=20;        
	    iArray[20][3]=3;
	    
		iArray[21]=new Array(); 
	    iArray[21][0]="MakeDate";   
	    iArray[21][1]="0px";   
	    iArray[21][2]=20;        
	    iArray[21][3]=3;
	    
	    	  
	    iArray[22]=new Array(); 
	    iArray[22][0]="MakeTime";   
	    iArray[22][1]="0px";   
	    iArray[22][2]=20;        
	    iArray[22][3]=3;
	      
        LHExecFeeBalanceGrid = new MulLineEnter( "fm" , "LHExecFeeBalanceGrid" ); 
        //这些属性必须在loadMulLine前
        
        LHExecFeeBalanceGrid.mulLineCount = 0;   
        LHExecFeeBalanceGrid.displayTitle = 1;
        LHExecFeeBalanceGrid.hiddenPlus = 1;
        LHExecFeeBalanceGrid.hiddenSubtraction = 1;
        LHExecFeeBalanceGrid.canSel = 0;
        LHExecFeeBalanceGrid.canChk = 1;
        //LHExecFeeBalanceGrid.selBoxEventFuncName = "showOne";              
        LHExecFeeBalanceGrid.chkBoxEventFuncName = "showOne";  
        LHExecFeeBalanceGrid.loadMulLine(iArray);  
        //这些操作必须在loadMulLine后面
        //LHExecFeeBalanceGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{alert(ex);}
}
</script>