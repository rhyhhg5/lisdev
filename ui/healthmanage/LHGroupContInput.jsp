<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHGroupContInput.jsp
//程序功能：团体合同合作基本信息
//创建日期：2005-03-19 15:05:48
//创建人  ：YangMing
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-03 10:38:48
// 更新原因/内容: 插入新的字段
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHGroupContInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHGroupContInputInit.jsp"%>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>   
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<SCRIPT>	
	
		
	var initWidth = 0;
	//图片的队列数组
	var pic_name = new Array();
	var pic_place = 0;
	var b_img	= 0;  //放大图片的次数
	var s_img = 0;	//缩小图片的次数
	
	function queryScanType() 
	{
    	var strSql = "select code1, codename, codealias from ldcode1 where codetype='scaninput'";
    	var a = easyExecSql(strSql);

    	return a;
  	} 
</SCRIPT>
<SCRIPT language=javascript1.2>
	//查看扫描件按钮对应事件
function showsubmenu()
{
    whichEl = eval('submenu' );
    if (whichEl.style.display == 'none')
    {
         //submenu1.style.display='';
    	   eval("submenu1.style.display='';");
         submenu.style.display='';
    }
    else
    {
    	  submenu.style.display='none';
        eval("submenu1.style.display='';");
    }
    var tWorkNo = fm.all('ContraNo').value;
        //prtNo = easyExecSql(strSql);
    var url="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo="+tWorkNo+"&BussNoType=61&BussType=HM&SubType=HM01";
    document.getElementById("iframe1").src=url;
}
</SCRIPT>
<body  onload="initForm();initElementtype();" >
  <form action="./LHGroupContSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
      <table>
    	<tr>
			<td class=button width="100%" align=left>
				<INPUT class=cssButton VALUE="查看合同扫描件"  TYPE=button onclick=" showsubmenu();">
			</td>
		</tr>	
		<tr>
        <td style='display:none' id='submenu'>
			<div>
				<table class=common cellpadding=0 cellspacing=0 >   
					<tr class=common>
						<td class=input>
	  		       			<iframe scrolling="auto" width="900" height="300" id=iframe1 src=''>
	  		        		</iframe>
						</td>
					</tr>
	  	       </table>
			</div>
			  </td>
		 </tr>
		</table>  
		<div style="display:''" id='submenu1'>
    <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHGroupCont1);">
    		</td>
    		<td class= titleImg>
        		 团体合作合同基本信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHGroupCont1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      合同编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraNo value="123456789" style="width:152px"  verify="合同编号|NOTNULL&len<=50" ondblclick="getNo();" readonly><font color=red> *</font>
    </TD>
    <TD  class= title>
      合同名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraName style="width:152px"  verify="合同名称|NOTNULL&len<=50"><font color=red> *</font>
    </TD>
     <TD  class= title>
      合同当前状态
    </TD>
    <TD  class= input>
      <Input  class=codeno name=ContraState verify="合同当前状态|len<=4" CodeData= "0|^1|有效^2|无效" ondblClick= "showCodeListEx('',[this,ContraState_ch],[0,1],null,null,null,'1',160);" onkeyup=" if(event.keyCode ==13)  return showCodeList('',[this,ContraState_ch],[1,0],null,fm.ContraState_ch.value,'ContraState_ch');"   onkeydown="return showCodeListKey('',[this,ContraState_ch],[1,0],null,fm.ContraState_ch.value,'ContraState_ch'); codeClear(ContraState_ch,ContraState);"><Input class= 'codename' name=ContraState_ch style="width:120px" >	
    </TD>
   
  </TR>
  <TR  class= common>
  	<TD  class= title>
      合同签订时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short"  style="width:152px" name=IdiogrDate verify="签订时间|DATE">
    </TD>
    <TD  class= title>
      合同起始时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" style="width:152px" name=ContraBeginDate verify="合同起始时间|DATE">
    </TD>
    <TD  class= title>
      合同终止时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" style="width:152px" name=ContraEndDate verify="合同终止时间|DATE">
    </TD>
  </TR>
  <TR  class= common>
		 <TD class= title>
    	医疗机构代码
    </TD>
    <TD  class= input>
    	<Input  class=codeno name= HospitCode style="width:152px" verify="医疗机构代码|NOTNULL&len<=50" ondblclick="showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName','',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName');"   onkeydown="return showCodeListKey('lhhospitname',[this,HospitName],[1,0],null,fm.HospitName.value,'HospitName'); codeClear(HospitName,HospitCode);">
    </TD>   
     <TD class= title>
    	医疗机构名称
    </TD>
    <TD class=input >  
    	<Input class= 'codename' name=HospitName style="width:152px" ><font color=red> *</font>  	
    </TD>
   </TR>
</table>
    </Div>
      <table>
		  	<tr>
		      <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupCont);">
		  		</td>
		  		<td class=titleImg>
		  			 信息
		  		</td>
		  		<td style="width:400px;">
		  		</td>
					<TD align ="right">
							<Input type=button class=cssbutton style="width:110px;" name=getServicePrice value="服务价格设置" onclick=" getAllService();">
					</TD>
					<TD align ="right">
							<Input type=button class=cssbutton style="width:110px;" name=getChargeBalance value="结算方式设置" onclick=" getAllCharge();">
					</TD>
					<TD align ="right">
							<Input type=button class=cssbutton style="width:110px;" name=getAssociateSetting value="关联责任设置"  onclick=" getAllAssociate();">
					</TD>
		  	</tr>

		  </table>
	
		  <!-- 信息（列表） -->
		<Div id="divGroupCont" style="display:''">
	     <div id="divDutyExolai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textDutyExolai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backDutyExolai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backDutyExolaiButton()">
			 </div>
			 <div id="divFeeExplai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textFeeExplai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backFeeExplai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backFeeExplaiButton()">
			 </div>
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanGroupContGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
		</div>

		</table>
    </Div>
		<div id="div1" style="display: 'none'">
			  <table class=common>
			  <TR  class= common>
			    <TD  class= title>
			      执行者
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=linkman >
			    </TD>
			    <TD  class= title>
			      责  任
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=Phone >
			    </TD>
			    <TD >
			    </TD>
			  </TR>
			  </table>
		</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type= hidden name ="ContraItemNo">
    <input type = hidden name = "Operator">
    <input type = hidden name = "MakeDate">
    <input type = hidden name = "MakeTime">
    <input type = hidden name = "ModifyDate">
    <input type = hidden name = "ModifyTime">
    <input type= hidden name ="LHFlag">
    <input type = hidden name = HiddenBtn>
    <input type = hidden name = txtFlag>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</DIV>
</body>
</html>
