<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDICDDiseasTypeSave.jsp
//程序功能：
//创建日期：2005-03-03 20:24:53
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDICDDiseasTypeSchema tLDICDDiseasTypeSchema   = new LDICDDiseasTypeSchema();
  TransferData mTransferData = new TransferData();

//	OLDDiseaseUI tOLDDiseaseUI = new OLDDiseaseUI();

  OLDICDDiseasTypeUI tOLDICDDiseasTypeUI   = new OLDICDDiseasTypeUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  		transact = request.getParameter("fmtransact");
  		System.out.println(transact);
	    tLDICDDiseasTypeSchema.setDiseasTypeLevel(request.getParameter("DiseasTypeLevel"));
	    tLDICDDiseasTypeSchema.setICDBeginCode(request.getParameter("ICDBeginCode"));
	    tLDICDDiseasTypeSchema.setICDEndCode(request.getParameter("ICDEndCode"));
	    tLDICDDiseasTypeSchema.setDiseasType(request.getParameter("DiseasType"));
	     
	    mTransferData.setNameAndValue("DiseasTypeLevel_origin",request.getParameter("DiseasTypeLevel_origin"));
	    mTransferData.setNameAndValue("ICDBeginCode_origin",request.getParameter("ICDBeginCode_origin"));
	    mTransferData.setNameAndValue("ICDEndCode_origin",request.getParameter("ICDEndCode_origin"));

  try 
  {   
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLDICDDiseasTypeSchema);
  	tVData.add(tG);
  	tVData.addElement(mTransferData);
    tOLDICDDiseasTypeUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDICDDiseasTypeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
