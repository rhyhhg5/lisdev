<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2006-03-22 16:59:25
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="LHServerItemPriceInput.js"></SCRIPT>
  <%@include file="LHServerItemPriceInputInit.jsp"%>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body  onload="initForm();" >
  <form action="./LHServerItemPriceSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 服务项目定价表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHServerItemPrice1" style= "display: ''">

<br>
  <!-- 信息（列表） -->
  <table  class= common align='center' >
    <TD  class= title style="width:120">
      标准服务项目代码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ServItemCode  verify="标准服务项目代码|len<=50" ondblclick="showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
    </TD>
    <TD  class= title style="width:120">
       标准服务项目名称
    </TD>
        <TD  class= input>
      <Input class= 'code' name=ServItemName  verify="标准服务项目名称|len<=50" ondblclick="showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
    </TD>
    <TD  class=common >
				<INPUT VALUE="信息查询"  style="width:100" TYPE=button   class=cssbutton OnClick="FindInfo();">
		</TD>
  </TR>
</table>
	<Div id="divItemPrice" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanItemGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
  <table  class= common align='center' >
	<TR  class= common>
		<TD  class= title style="width:120">
	      机构代码
	    </TD>
	    <TD  class= input>
	      <Input class= 'codename' readonly style="width:60px" name=ComID verify="机构代码|notnull"><Input readonly class= 'code' style="width:100px" name=ComID_cn ondblclick="showCodeList('hmhospitalmng', [this, ComID], [0,1], null, null, null, 1);">
	    </TD>
	    <TD style="width:"90px"></TD>
	</TR>
</table>  
    
    <Div id="divItemPrice" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanItemPriceGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	<table class=common border=0 width=100%>
		<TR class=common>  
			<TD  class=common>
				<INPUT VALUE="第三方服务成本设置"   TYPE=button   class=cssbutton onclick="toServCost();">
				<INPUT VALUE="服务运营费用设置"   TYPE=button   class=cssbutton onclick="toOperateCost();">
				<INPUT VALUE="服务定价要素设置"   TYPE=button   class=cssbutton onclick="toPriceFactorSetting();">
			</TD>
	    </TR>
	</table>    
    
    </Div>
    
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
