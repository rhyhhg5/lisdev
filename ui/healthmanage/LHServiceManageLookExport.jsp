<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServiceManageLookExport.jsp
//程序功能：
//创建日期：2006-12-04 11:27:51
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 
<%
  //接收信息，并作校验处理。
  //输入参数
  String tTaskNo="";
  LHTaskCustomerRelaSet tLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();//健康服务任务客户关联表
  LHServiceManageLookExportXls tLHServiceManageLookExportXls = new LHServiceManageLookExportXls();//要提交类文件的名称
   tTaskNo= request.getParameter("taskNo");           //服务任务号码
   //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");

  try
  {
  // 准备传输数据 VData
       String sysPath = application.getRealPath("/")+"/" ;
       tLHServiceManageLookExportXls.DownloadSubmit(tTaskNo,sysPath);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  String FullPath ="./"+tLHServiceManageLookExportXls.getResult();
  if (FullPath!="")
  {
  	System.out.println("导出数据成功");
  	System.out.println("FullPathFullPath "+FullPath);
  	Content = "导出数据成功!";
  }
  else
	{
	 System.out.println("导出数据失败");
	 Content = "导出数据失败!";
	}
//如果在Catch中发现异常，则不从错误类中提取错误信息
%>                      
<%=Content%>
<html>
<script language="javascript">
	  var transact = "<%=transact%>";
	  parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>"); 	
	  <%if(Content.equals("导出数据成功!")){%>  
          window.open("<%=FullPath%>");
    <%}else{%>	
	  	alert("导出数据失败！");
    <%}%>	
</script>
</html>
