<%
//程序名称：LHGroupContQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-19 15:05:48
//创建人  ：ctrHTML
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-03 10:38:48
// 更新原因/内容: 插入新的字段  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    //fm.all('ContraNo').value = "";
    //fm.all('ContraName').value = "";
    fm.all('HospitCode').value = "";
    //fm.all('IdiogrDate').value = "";
    //fm.all('ContraBeginDate').value = "";
    //fm.all('ContraEndDate').value = "";

  }
  catch(ex) {
    alert("在LHGroupContQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHGroupContGrid();  
  }
  catch(re) {
    alert("LHGroupContQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }                                                      
}
//领取项信息列表的初始化
var LHGroupContGrid;
function initLHGroupContGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         				//列名
    iArray[0][4]="";         				//列名
    
    iArray[1]=new Array();                
    iArray[1][0]="合同编号";     		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=0;         				//列名
    iArray[1][4]="";         				//列名
    
    iArray[2]=new Array();                
    iArray[2][0]="合同名称";        //列名
    iArray[2][1]="100px";         		//列名
    iArray[2][3]=0;         				//列名
    iArray[2][4]="";         				//列名
    
    iArray[3]=new Array();                
    iArray[3][0]="合作机构名称";      //列名
    iArray[3][1]="100px";         		//列名
    iArray[3][3]=0;         					//列名
    iArray[3][4]="";         					//列名
    
    iArray[4]=new Array();                         
    iArray[4][0]="合同签订时间";      //列名 
    iArray[4][1]="100px";         		//列名       
    iArray[4][3]=0;         					//列名         
    iArray[4][4]="";         					//列名         
    
    iArray[5]=new Array();                
    iArray[5][0]="合同起始时间";      //列名
    iArray[5][1]="100px";         		//列名
    iArray[5][3]=0;         					//列名
    iArray[5][4]="";         					//列名
    
    iArray[6]=new Array();                
    iArray[6][0]="合同终止时间";         		//列名
    iArray[6][1]="100px";         		//列名
    iArray[6][3]=0;         				//列名
    iArray[6][4]="";         //列名
    
    iArray[7]=new Array();                
    iArray[7][0]="合同当前状态";         		//列名
    iArray[7][1]="100px";         		//列名
    iArray[7][3]=0;         				//列名
    iArray[7][4]="";         //列名
    
    iArray[8] = new Array();
    iArray[8][0]="合同创建日期";
    iArray[8][1]="0px";
    iArray[8][3]=3;  
    iArray[8][4]="3";
                
    iArray[9]= new Array();    
    iArray[9][0]="合同创建时间";            
    iArray[9][1]="0px";                        
    iArray[9][3]=3;                         
    iArray[9][4]="3";              
    
    iArray[10]= new Array();      
    iArray[10][0]="ContrItemNo"; 
    iArray[10][1]="0px";  
    iArray[10][3]=3;           
    iArray[10][4]="";                        
                        
    
    LHGroupContGrid = new MulLineEnter( "fm" , "LHGroupContGrid" ); 
    //这些属性必须在loadMulLine前

		LHGroupContGrid.canSel = 1;     
		LHGroupContGrid.canChk = 0;
		LHGroupContGrid.mulLineCount = 0;     
		LHGroupContGrid.displayTitle = 1;     
		LHGroupContGrid.hiddenPlus = 1;       
		LHGroupContGrid.hiddenSubtraction = 1;
/*  
    LHGroupContGrid.mulLineCount = 0;   
    LHGroupContGrid.displayTitle = 1;
    LHGroupContGrid.hiddenPlus = 0;
    LHGroupContGrid.hiddenSubtraction = 0;
    LHGroupContGrid.canSel = 0;
    LHGroupContGrid.canChk = 1;
    LHGroupContGrid.selBoxEventFuncName = "showOne";
*/
    LHGroupContGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHGroupContGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>





