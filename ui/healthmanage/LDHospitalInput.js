//               该文件中包含客户端需要处理的函数和事件
var ImportPath;
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var selectedSpecSecDiaRow; 
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  
  if(fm.all('HospitalType').value == "1")
  {
	  if(fm.CommunFixFlag.value==null || fm.CommunFixFlag.value==""){
		  alert("社保定点属性为必录项，不能为空！");
		  return false;
	  }
	  if(fm.LevelCode.value==null || fm.LevelCode.value==""){
		  alert("医院等级为必录项，不能为空！");
		  return false;
	  }
	  var ComHosGridMaxRow = ComHospitalGrid.mulLineCount;
	  if(ComHosGridMaxRow == "0")
	  {
	  	alert("请填写推荐医院记录信息");	
	  	return false;
	  }
	  else
	  {
	    var ComInfo = ComHospitalGrid.getRowColData(ComHosGridMaxRow-1,1);
	    if(ComInfo == "推荐医院")
	    {
	    	fm.all('AssociateClass_ch').value = ComInfo;
	    	fm.all('AssociateClass').value = "1";	
	    }
	    else if(ComInfo == "指定医院")
	    {
	    	fm.all('AssociateClass_ch').value = ComInfo;
	    	fm.all('AssociateClass').value = "2";	
	    } 
	    else if(ComInfo == "非指定医院")
	    {
	    	fm.all('AssociateClass_ch').value = ComInfo;
	    	fm.all('AssociateClass').value = "3";	
	    } 	
	    else if(ComInfo == "指定口腔医疗机构")
	    {
	    	fm.all('AssociateClass_ch').value = ComInfo;
	    	fm.all('AssociateClass').value = "4";	
	    } 
	    else
	    {
	        alert("请填写正确医院级别信息");	
	        return false;
	    }
	  }
	}
//  add lyc #2239 2014-11-15
	if( manageCom.length == 2)
	{}
	else
	{
		manageCom = manageCom.substring(0,4)

		if(manageCom != fm.all('MngCom').value)
		{
			alert("您不能配置其他管理机构的医院信息，请选择正确的管理机构！")
			return false;
		}
	//  add lyc #2239 2014-11-15
	}
  var i = 0;
  if( verifyInput2() == false ) return false;
  
  fm.fmtransact.value="INSERT||MAIN";
  //alert('here3');
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //alert('here1');
  fm.action='./LDHospitalSave.jsp';
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDHospital.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if(fm.all('HospitalType').value == "1")
  {
	  if(fm.CommunFixFlag.value==null || fm.CommunFixFlag.value==""){
		  alert("社保定点属性为必录项，不能为空！");
		  return false;
	  }
	  if(fm.LevelCode.value==null || fm.LevelCode.value==""){
		  alert("医院等级为必录项，不能为空！");
		  return false;
	  }
	  var ComHosGridMaxRow = ComHospitalGrid.mulLineCount;
	  if(ComHosGridMaxRow == "0")
	  {
	  	alert("请填写推荐医院记录信息");	
	  	return false;
	  }
	  else
	  {
	    var ComInfo = ComHospitalGrid.getRowColData(ComHosGridMaxRow-1,1);
	    if(ComInfo == "推荐医院")
	    {
	    	fm.all('AssociateClass_ch').value = ComInfo;
	    	fm.all('AssociateClass').value = "1";	
	    }
	    else if(ComInfo == "指定医院")
	    {
	    	fm.all('AssociateClass_ch').value = ComInfo;
	    	fm.all('AssociateClass').value = "2";	
	    } 
	    else if(ComInfo == "非指定医院")
	    {
	    	fm.all('AssociateClass_ch').value = ComInfo;
	    	fm.all('AssociateClass').value = "3";	
	    }
	    else if(ComInfo == "指定口腔医疗机构")
	    {
	    	fm.all('AssociateClass_ch').value = ComInfo;
	    	fm.all('AssociateClass').value = "4";	
	    } 	
	    else
	    {
	        alert("请填写正确医院级别信息");	
	        return false;
	    }
	  }
	}

  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  fm.fmtransact.value = "UPDATE||MAIN";
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDHospitalQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	if(fm.all('MakeDate').value == "")
	{
			alert("无法删除，请重新查询一条信息");
			return false;
	}
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();
	initForm();
	if( arrQueryResult != null )
	{
		//以后再添加更多的条件
		//arrResult = easyExecSql("select * from LDHospital where  HospitCode='"+arrQueryResult[0][0]+"'",1,0);
        arrResult = arrQueryResult;
        fm.all('HospitCode').value= arrResult[0][0];
        fm.all('HospitName').value= arrResult[0][1];
        fm.all('AreaCode').value= arrResult[0][2]; 
        fm.all('HospitalType').value = arrResult[0][9];  
        fm.all('LevelCode').value= arrResult[0][4];
        fm.all('Address').value= arrResult[0][5];
        fm.all('MakeDate').value = arrResult[0][6];
		fm.all('MakeTime').value = arrResult[0][7];
		//当医疗服务机构类型为医院时的操作		
		var HospitalTypeTemp = easyExecSql("select HospitalType from LDHospital where HospitCode='"+arrQueryResult[0][0]+"'");
				if(HospitalTypeTemp == 1)
				{
					servPart.style.display='';
					CurrentlyInfo.style.display='';
					divcomHospital1.style.display='';
					hospitalSrcInfo.style.display='';fm.all('HospitalType_ch').value = "医疗机构";
					arrResult = easyExecSql("select HospitalType,HospitName,HospitShortName,AreaCode,AreaName,"+
																	"HospitCode,CommunFixFlag,AdminiSortCode,EconomElemenCode,LevelCode,"+
																	"BusiTypeCode,AssociateClass,HospitalStandardCode,Address,Phone,ZipCode,"+
																	"WebAddress,SuperiorNo,InterNo,ElementaryNo,TotalNo,BedAmount,PatientPerDay,"+
																	"OutHospital, Route,ManageCom, UrbanOption, securityno from LDHospital where  HospitCode='"+arrQueryResult[0][0]+"'");
									
				fm.all('HospitalType').value = 				arrResult[0][0] ;
			    fm.all('HospitName').value =					arrResult[0][1] ;
			    fm.all('HospitShortName').value = 		arrResult[0][2] ;
			    fm.all('AreaCode').value = 						arrResult[0][3] ;
			    fm.all('AreaName').value = 						arrResult[0][4] ;
			    fm.all('HospitCode').value =   				arrResult[0][5] ;
			    fm.all('CommunFixFlag').value = 		  arrResult[0][6] ;if(arrResult[0][6]=="0"){fm.all('CommunFixFlag_ch').value="社保非定点医院";}if(arrResult[0][6]=="1"){fm.all('CommunFixFlag_ch').value="社保定点医院";}
			    fm.all('AdminiSortCode').value =  		arrResult[0][7] ;var tempAdminiSortCode = easyExecSql("select codename from ldcode where codetype='hmadminisortcode' and code = '"+arrResult[0][7]+"'");fm.all('AdminiSortCode_ch').value= tempAdminiSortCode==null?"":tempAdminiSortCode;
			    fm.all('EconomElemenCode').value = 		arrResult[0][8] ;var tempEconomElemenCode=easyExecSql("select codename from ldcode where codetype='hmeconomelemencode' and code = '"+arrResult[0][8]+"'");fm.all('EconomElemenCode_ch').value= tempEconomElemenCode==null?"":tempEconomElemenCode;
			    fm.all('LevelCode').value = 					arrResult[0][9] ;var tmepLevelCode = easyExecSql("select codename from ldcode where codetype='hospitalclass' and code = '"+arrResult[0][9]+"'");fm.all('LevelCode_ch').value= tmepLevelCode==null?"":tmepLevelCode;
			    fm.all('BusiTypeCode').value = 				arrResult[0][10];var tempBusiTypeCode = easyExecSql("select codename from ldcode where codetype='hmbusitype' and code = '"+arrResult[0][10]+"'");fm.all('BusiTypeCode_ch').value= tempBusiTypeCode==null?"":tempBusiTypeCode;
//			    fm.all('AssociateClass').value = 			arrResult[0][11];var tempAssociateClass = easyExecSql("select codename from ldcode where codetype='llhospiflag' and code = '"+arrResult[0][11]+"'");fm.all('AssociateClass_ch').value= tempAssociateClass==null?"":tempAssociateClass;
				var tempAssociateClass_ch = easyExecSql("select distinct Associatclass from ldcomhospital where hospitcode = '"+arrResult[0][5]+"' and startdate = ( select distinct max(startdate) from ldcomhospital where hospitcode = '"+arrResult[0][5]+"')");
				if(tempAssociateClass_ch == "" || tempAssociateClass_ch == "null" || tempAssociateClass_ch == null)
				{
					fm.all('AssociateClass').value = arrResult[0][11];
					var tempAssociateClass = easyExecSql("select codename from ldcode where codetype='llhospiflag' and code = '"+arrResult[0][11]+"'");
					fm.all('AssociateClass_ch').value= tempAssociateClass==null?"":tempAssociateClass;	
				}
				else
				{
					fm.all('AssociateClass_ch').value = tempAssociateClass_ch;
					var tempAssociateClass = easyExecSql("select code from ldcode where codetype='llhospiflag' and codename = '"+tempAssociateClass_ch+"'");
					fm.all('AssociateClass').value= tempAssociateClass==null?"":tempAssociateClass;
				}

			    fm.all('HospitalStandardCode').value =arrResult[0][12];
			    fm.all('Address').value = 						arrResult[0][13];
			    fm.all('Phone').value = 							arrResult[0][14];
			    fm.all('ZipCode').value = 						arrResult[0][15];			    
			    fm.all('WebAddress').value = 					arrResult[0][16];  
			    //fm.all('SuperiorNo').value = 					arrResult[0][17];
			    //fm.all('InterNo').value = 						arrResult[0][18];
			    //fm.all('ElementaryNo').value = 				arrResult[0][19];
			    //fm.all('TotalNo').value = 						arrResult[0][20];
			    fm.all('BedAmount').value = 					arrResult[0][21];
//			   fm.all('FlowNo').value =							arrResult[0][22];
			    fm.all('PatientPerDay').value = 			arrResult[0][22];
			    fm.all('OutHospital').value = 				arrResult[0][23];
			    fm.all('Route').value = 				      arrResult[0][24];
			    fm.all('MngCom').value =              arrResult[0][25];
			    fm.all('MngCom_ch').value = easyExecSql("select showname from ldcom where comcode='"+arrResult[0][25]+"'");
			    fm.all('SecurityNo').value = arrResult[0][27];
			    if( manageCom.substring(0,4)=="8653" || manageCom=="86")
			    {
					    if(arrResult[0][9]=="00" || arrResult[0][9]=="10" || arrResult[0][9]=="11" )
					    {
								UrbanName.style.display='';
								UrbanOpt.style.display='';
							    fm.all('UrbanOption').value =  				arrResult[0][26];
							    if(arrResult[0][26]=="0"){fm.all('UrbanOptionName').value = "非城区"}
							    if(arrResult[0][26]=="1"){fm.all('UrbanOptionName').value = "城区"}
							 }
			    }                           
			    var GridSQL1 = "select (select d.codename from ldcode d where d.code=a.SpecialName and codetype='lhspecname'),"
			                 +" SpecialIntro,SpecialClass,FlowNo,"
			                 +" SpecialName "
			                 +" from LDHospitalInfoIntro a where SpecialClass = 'SpecSecDia' " + getWherePart("HospitCode", "HospitCode");
			    turnPage.pageLineNum = 100;  
			    turnPage.queryModal(GridSQL1, SpecSecDiaGrid);
			    //var arr = easyExecSql(GridSQL1);alert(arr[0][3]);
			    
			    var GridSQL2 = "select SpecialName,SpecialIntro,SpecialClass,FlowNo from LDHospitalInfoIntro where SpecialClass = 'Instrument' " + getWherePart("HospitCode", "HospitCode");
			    turnPage.pageLineNum = 100;  
			    turnPage.queryModal(GridSQL2, InstrumentGrid);
			    
			    var GridSQL3 = "select AssociatClass,StartDate,EndDate,SerialNo,HospitCode from LDComHospital where 1=1 "+ getWherePart("HospitCode", "HospitCode") + " order by startdate ";
					turnPage.pageLineNum = 20;  
					turnPage.queryModal(GridSQL3, ComHospitalGrid);
				}
				
				
				if(HospitalTypeTemp == 2 || HospitalTypeTemp == 3 || HospitalTypeTemp ==4)
				{

					if(HospitalTypeTemp == 2 )fm.all('HospitalType_ch').value = "健康体检机构";
					if(HospitalTypeTemp == 3 )fm.all('HospitalType_ch').value = "健康管理公司";
					if(HospitalTypeTemp == 4 )fm.all('HospitalType_ch').value = "紧急救援公司";
					arrResult = easyExecSql("select HospitalType,HospitName,HospitShortName,AreaCode,AreaName,"+
																	"HospitCode,CommunFixFlag,AdminiSortCode,EconomElemenCode,LevelCode,"+
																	"BusiTypeCode,AssociateClass,HospitalStandardCode,Address,Phone,ZipCode,"+
																	"WebAddress,SuperiorNo,InterNo,ElementaryNo,TotalNo,BedAmount,PatientPerDay,"+
																	"OutHospital,Route, ManageCom from LDHospital where  HospitCode='"+arrQueryResult[0][0]+"'");
									
					fm.all('HospitalType').value = 				arrResult[0][0] ;
				    fm.all('HospitName').value =					arrResult[0][1] ;
				    fm.all('HospitShortName').value = 		arrResult[0][2] ;
				    fm.all('AreaCode').value = 						arrResult[0][3] ;
				    fm.all('AreaName').value = 						arrResult[0][4] ;
				    fm.all('HospitCode').value =   				arrResult[0][5] ;
				    fm.all('CommunFixFlag').value = 		  arrResult[0][6] ;if(arrResult[0][6]=="0"){fm.all('CommunFixFlag_ch').value="社保非定点医院";}if(arrResult[0][6]=="1"){fm.all('CommunFixFlag_ch').value="社保定点医院";}
				    fm.all('AdminiSortCode').value =  		arrResult[0][7] ;fm.all('AdminiSortCode_ch').value=easyExecSql("select codename from ldcode where codetype='hmadminisortcode' and code = '"+arrResult[0][7]+"'");
				    fm.all('EconomElemenCode').value = 		arrResult[0][8] ;fm.all('EconomElemenCode_ch').value=easyExecSql("select codename from ldcode where codetype='hmeconomelemencode' and code = '"+arrResult[0][8]+"'");
				    fm.all('LevelCode').value = 					arrResult[0][9] ;fm.all('LevelCode_ch').value=easyExecSql("select codename from ldcode where codetype='hospitalclass' and code = '"+arrResult[0][9]+"'");
				    fm.all('BusiTypeCode').value = 				arrResult[0][10];fm.all('BusiTypeCode_ch').value=easyExecSql("select codename from ldcode where codetype='hmbusitype' and code = '"+arrResult[0][10]+"'");
				    fm.all('AssociateClass').value = 			arrResult[0][11];fm.all('AssociateClass_ch').value=easyExecSql("select codename from ldcode where codetype='llhospiflag' and code = '"+arrResult[0][11]+"'");
				    fm.all('HospitalStandardCode').value =arrResult[0][12];
				    fm.all('Address').value = 						arrResult[0][13];
				    fm.all('Phone').value = 							arrResult[0][14];
				    fm.all('ZipCode').value = 						arrResult[0][15];			    
				    fm.all('WebAddress').value = 					arrResult[0][16];  
				    //fm.all('SuperiorNo').value = 					arrResult[0][17];
				    //fm.all('InterNo').value = 						arrResult[0][18];
				    //fm.all('ElementaryNo').value = 				arrResult[0][19];
				    //fm.all('TotalNo').value = 						arrResult[0][20];
				    fm.all('BedAmount').value = 					arrResult[0][21];
				    //fm.all('FlowNo').value =							arrResult[0][22];
				    fm.all('PatientPerDay').value = 			arrResult[0][22];
				    fm.all('OutHospital').value = 				arrResult[0][23];
				    fm.all('Route').value = 				      arrResult[0][24];
				    fm.all('MngCom').value =              arrResult[0][25];
				    fm.all('MngCom_ch').value = easyExecSql("select showname from ldcom where comcode='"+arrResult[0][25]+"'");
	 
				}
	}
}               
function getSuperHospitCode()
{
    /*var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;*/
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    /*turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
    if (turnPage.strQueryResult != "")
    {
    	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	m = turnPage.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    	}
    }
    //alert ("tcodedata : " + tCodeData);
    //return tCodeData;*/
    fm.all("SuperHospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}        



//选择类型为医院时的处理
function afterCodeSelect(codeName,Field)
{
	if(codeName=="HospitalType")
	{
		if(fm.HospitalType.value=="1")
		{
			fm.all('AssociateClass').value = '';
			fm.all('AssociateClass_ch').value = '';
			servPart.style.display='';
			hospitalSrcInfo.style.display='';
			CurrentlyInfo.style.display='';
			divcomHospital1.style.display='';
		}
		else
		{
			fm.all('AssociateClass').value = '0';
			fm.all('AssociateClass_ch').value = '0';
			fm.all('CommunFixFlag').value='';
			fm.all('LevelCode').value='';
			fm.all('AdminiSortCode').value='';
			fm.all('EconomElemenCode').value='';
			fm.all('BusiTypeCode').value='';
			servPart.style.display='none';
		 	hospitalSrcInfo.style.display='none';
		 	divcomHospital1.style.display='none';
		}
	}
	if(codeName=="hmareacode" || codeName=="FlowNo" || codeName=="CommunFixFlag" || codeName=="hmadminisortcode" || codeName=="hmeconomelemencode" || codeName=="hospitalclass" || codeName=="hmBusiType")
	{
			fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
	}
	
	if(codeName == "hospitalclass" && manageCom.substring(0,4)=="8653")
	{
		if(fm.LevelCode.value == "00" || fm.LevelCode.value == "10" || fm.LevelCode.value == "11" )
		{
			UrbanName.style.display='';
			UrbanOpt.style.display='';
		}
		else
		{
			UrbanName.style.display='none';
			UrbanOpt.style.display='none';
			fm.all('UrbanOptionName').value = "";
			fm.all('UrbanOption').value = "";
		}
	}
}

//生成医院的小流水号,    08-12-2 放弃不用。

function generateFlowNo()
{
		var str = "select max(hospitcode) from ldhospital where hospitcode like '"+fm.AreaCode.value+"%%'";
		var shortFlowNo = easyExecSql(str);
		if(fm.AreaCode.value=="")
		{
				alert("请先输入地区代码");
		}
	  else
	  {
	  	if(shortFlowNo == null || shortFlowNo=="")
				{
						fm.FlowNo.value="001";
						fm.HospitCode.value = fm.AreaCode.value+"001";
						fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
				}
				else
				{
						fm.HospitCode.value = parseInt(shortFlowNo)+1;
						fm.FlowNo.value= fm.HospitCode.value.substr(4,3);
						fm.HospitalStandardCode.value = fm.HospitCode.value+fm.CommunFixFlag.value+fm.AdminiSortCode.value+fm.EconomElemenCode.value+fm.LevelCode.value+fm.BusiTypeCode.value;
				}
		}
}

function getNoNew()
{
	fm.action="./LDHospitalInputGetCode.jsp?AreaCode="+fm.AreaCode.value;
	fm.submit();
}

function inputSpecSecDia(a)
{ 
	
	divSpecSecDia.style.display='';
	var tempSpecSecDia = fm.all(a).all('SpecSecDiaGrid2').value;
	selectedSpecSecDiaRow = fm.all(a).all('SpecSecDiaGridNo').value-1;
	fm.all('SpecSecDia').value=tempSpecSecDia;
	
}
function backSpecSecDia()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		SpecSecDiaGrid.setRowColData(selectedSpecSecDiaRow,2,fm.SpecSecDia.value);
		fm.SpecSecDia.value="";
		divSpecSecDia.style.display='none';	
	}
} 
function backSpecSecDia2()
{
		SpecSecDiaGrid.setRowColData(selectedSpecSecDiaRow,2,fm.SpecSecDia.value);
		fm.SpecSecDia.value="";
		divSpecSecDia.style.display='none';	
}



function inputInstrument(a)
{ 
	
	divInstrument.style.display='';
	var tempInstrument = fm.all(a).all('InstrumentGrid2').value;
	selectedInstrumentRow = fm.all(a).all('InstrumentGridNo').value-1;
	fm.all('Instrument').value=tempInstrument;
	
}
function backInstrument()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		InstrumentGrid.setRowColData(selectedInstrumentRow,2,fm.Instrument.value);
		fm.Instrument.value="";
		divInstrument.style.display='none';	
	}
} 
function backInstrument2()
{
		InstrumentGrid.setRowColData(selectedInstrumentRow,2,fm.Instrument.value);
		fm.Instrument.value="";
		divInstrument.style.display='none';	
}

//医院信息导入
function HospitalListUpload()
{
 // var i = 0;
  getImportPath();
 // ImportFile = fmImport.all('FileName').value;  
  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmlode.action = "./HospitalInfoSave.jsp?ImportPath="+ImportPath;
  fmlode.submit(); //提交
  
}

function getImportPath () {
  // 书写SQL语句
  var strSQL = "";

  strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未找到上传路径");
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  ImportPath = turnPage.arrDataCacheSet[0][0];

}