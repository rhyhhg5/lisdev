/** 
 * 程序名称：LHContraAssoSettingContQueryInput.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2006-03-14 17:02:48
 * 创建人  ：郭丽颖
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
/*	
	var strSql = "select a.GetNoticeNo, a.otherno, b.name, a.PayDate, a.SumDuePayMoney, a.bankcode, a.bankaccno, a.accname "
	           + " from LJSPay a, ldperson b where a.appntno=b.customerno and OtherNoType='2' "
          	 + getWherePart("a.GetNoticeNo", "GetNoticeNo2")
          	 + getWherePart("a.OtherNo", "OtherNo")
          	 + getWherePart("a.BankCode", "BankCode")
          	 + getWherePart("a.SumDuePayMoney", "SumDuePayMoney");
  
  if (fm.AppntName.value != "") strSql = strSql + " and a.appntno in (select c.customerno from ldperson c where name='" + fm.AppntName.value + "')";
  if (fm.PrtNo.value != "") strSql = strSql + " and a.otherno in (select polno from lcpol where prtno='" + fm.PrtNo.value + "')";
*/  			    
	var strSql = "select ContraNo,ContraName,"
	  +"(select DoctName from lddoctor where lddoctor.doctno=a.doctno),"
	  +"IdiogrDate,ContraBeginDate,ContraEndDate,"
	  +"(case when ContraState='1' then '有效' else '无效' end )"
	  +" from LHUnitContra  a where 1=1 "
    + getWherePart("ContraNo", "ContraNo")
    + getWherePart("ContraName", "ContraName")
    +" and managecom like '"+manageCom+"%%'"
  ;
  //alert(strSql);( case when a.ExpType = '1' then '支票' else '转帐' end)
	turnPage.queryModal(strSql, LHContraAssoSettingContQueryGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LHContraAssoSettingContQueryGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHContraAssoSettingContQueryGrid.getSelNo();
	//alert("111" + tRow);
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = LHContraAssoSettingContQueryGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
function showDetail()
{
	 var strSql = "select ContraNo,ContraName,"
	  +"(select c.Codename from ldcode c where c.codetype = 'hmdeftype' and c.code = e.contratype),"
		+" (select d.DutyItemName from LDContraItemDuty d where d.dutyitemcode = e.dutyitemcode), "
		+" ( case when a.ContraState = '1' then '有效' else '无效' end),	"	 
	  +" from LHUnitContra  a where 1=1 "
    + getWherePart("ContraNo", "ContraNo")
    + getWherePart("ContraName", "ContraName")
    +" and managecom like '"+manageCom+"%%'"
  ;
	turnPage.queryModal(strSql, LHContraAssoSettingContQueryGrid);
}
