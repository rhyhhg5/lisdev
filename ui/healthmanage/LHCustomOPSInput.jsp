<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-17 17:17:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHCustomOPSInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LHCustomOPSInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHCustomOPSSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 客户手术及物理治疗信息基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCustomOPS1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      客户号码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=24" ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerName >
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      手术及物理治疗序号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OPSNo >
    </TD>
    <TD  class= title>
      手术及物理治疗项目
    </TD>
    <TD  class= input>
      <Input class= 'code' name=MedicaItemCode verify="手术及物理治疗项目|len<=12" ondblclick="getMedicaItemCode();return showCodeListEx('GetMedicaItemCode',[this],[0],'', '', '', true);" onkeyup="getMedicaItemCode();return showCodeListKeyEx('GetHospitCode',[this],[0],'', '', '', true);">
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      治疗方案概述
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RxSummar verify="治疗方案概述|len<=2">
    </TD>
    <TD  class= title>
      实施日期
    </TD>
    <TD  class= input>
      <Input name=ExecutDate verify="实施日期|len<=20" class= 'coolDatePicker' dateFormat="short" >
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      医疗机构
    </TD>
    <TD  class= input>
      <Input class= 'code' name=HospitCode verify="医疗机构|len<=20" ondblclick="getHospitCode();return showCodeListEx('GetHospitCode',[this],[0],'', '', '', true);" onkeyup="getHospitCode();return showCodeListKeyEx('GetHospitCode',[this],[0],'', '', '', true);">
    </TD>
    <TD  class= title>
      医师姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Doctor verify="医师姓名|len<=20">
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      告知标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ImpartFlag verify="告知标志|len<=1">
    </TD>
    <TD  class= title>
      告知日期
    </TD>
    <TD  class= input>
      <Input name=ImpartDate verify="告知日期|len<=20" class= 'coolDatePicker' dateFormat="short">
    </TD>
    
  </TR>
  <TR  class= common>
    <TD  class= title>
      就诊及住院序号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InHospitNo verify="就诊及住院序号|num&len<=3">
    </TD>
    <TD  class= title>
      就诊及住院费用
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InHospitFee verify="就诊及住院费用|num&len<=9">
    </TD>

  </TR>

</table>
    </Div>
    <div id="div1" style="display: 'none'">
    <table>
      <TR  class= common>
          <TD  class= title>
      操作员代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Operator elementtype=nacessary verify="操作员代码|len<=10">
    </TD>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeDate >
    </TD>
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=MakeTime >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
  </TR>
  </table>
    </div>
    <hr/>
    <INPUT VALUE="客户健康信息" class=cssButton TYPE=button onclick="CustomHealth();">
    <INPUT VALUE="客户就诊信息" class=cssButton TYPE=button onclick="CustomInHospital();">
    <INPUT VALUE="客户疾病信息" class=cssButton TYPE=button onclick="CustomDiseas();">
    <INPUT VALUE="客户检查信息" class=cssButton TYPE=button onclick="CustomTest();">
    <!--INPUT VALUE="客户治疗信息" class=cssButton TYPE=button onclick="CustomOPS();"-->
    <INPUT VALUE="客户家族病史" class=cssButton TYPE=button onclick="CustomFamily();">
    <INPUT VALUE="客户健身信息" class=cssButton TYPE=button onclick="CustomGym();">
  <hr/>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
