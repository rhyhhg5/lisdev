<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LHScanCodeQuery.jsp
//程序功能：扫描件查询页面
//创建日期：2006-06-07 14:11:12
//创建人  ：郭丽颖
//更新记录：
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHScanCodeQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHScanCodeQueryInit.jsp"%>
</head>
<%
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>".substr(0,4);
  var operator = "<%=tG.Operator%>";
</script>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<SCRIPT>


	var initWidth = 0;
	//图片的队列数组
	var pic_name = new Array();
	var pic_place = 0;
	var b_img	= 0;  //放大图片的次数
	var s_img = 0;	//缩小图片的次数

	function queryScanType()
	{
    	var strSql = "select code1, codename, codealias from ldcode1 where codetype='scaninput'";
    	var a = easyExecSql(strSql);

    	return a;
  	}
</SCRIPT>
<SCRIPT language=javascript1.2>
	//查看扫描件按钮对应事件
function showsubmenu()
{
    whichEl = eval('submenu' );
    if (whichEl.style.display == 'none')
    {
         //submenu1.style.display='';
    	   eval("submenu1.style.display='';");
         submenu.style.display='';
    }
    else
    {
    	  submenu.style.display='none';
        eval("submenu1.style.display='';");
    }
    var tWorkNo = fm.all('CustomerNo').value;
        //prtNo = easyExecSql(strSql);
    var url="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo="+tWorkNo+"&BussNoType=61&BussType=HM&SubType=HM01";
    document.getElementById("iframe1").src=url;
}
</SCRIPT>
<body  onload="initForm();initElementtype();" >
  <form  method=post name=fm target="fraSubmit">
  	 <table>
    	<tr>
			   <td class=button width="100%" align=left style='display:none'>
			   	<INPUT class=cssButton VALUE="查看扫描件"  style="width:120px"  TYPE=button onclick=" showsubmenu();">
			   </td>
		  </tr>
		 <tr>
        <td style='display:none' id='submenu'>
			       <div>
			       	<table class=common cellpadding=0 cellspacing=0 >
			       		<tr class=common>
			       			<td class=input>
	  	       	       			<iframe scrolling="auto" width="820" height="300" id=iframe1 src=''>
	  	       	        		</iframe>
			       			</td>
			       		</tr>
	  	          </table>
			       </div>
			  </td>
		  </tr>
		</table>
		<div style="display:''" id='submenu1'>
     <table class= common  align='center' >
    	<TR  class= common>
          <TD  class= title>
                客户号码
           </TD>
           <TD  class= input>
                <Input class= 'code' name=CustomerNo ondblclick=" showCodeList('hmldperson',[this,CustomerName],[0,1],null,fm.CustomerNo.value,'CustomerNo',1);" verify="客户姓名|len<=24">
            </TD>
            <TD  class= title>
                客户姓名
            </TD>
            <TD  class= input>
                <Input class= 'code' name=CustomerName ondblclick=" return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name');"  onkeyup=" if( event.keyCode==13 ) return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name');" verify="客户姓名|len<=24">
            </TD>
            <TD  class=title>
            	 扫描件类型
            </TD>
            <TD>
              <Input class= 'codename' style="width:50px" name=ScanType><Input class= 'codeno'  style="width:130px" name=ScanTypeName verify="扫描件类型|NOTNULL&len<=20" CodeData= "0|^1|客户就诊资料^2|健康体检资料 ^3|核保体检资料^4|理赔就诊资料"   ondblclick="showCodeListEx('ScanType',[this,ScanType],[1,0],null,null,null,'1',160);"><font color=red> *</font>
            </TD>
     </tr>
      <TR  class= common>
          <TD  class= title>起始日期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' style="width:140px" verify="起始日期|notnull&Date" > </TD>
          <TD  class= title>终止日期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' style="width:140px" verify="终止日期|notnull&Date" > </TD>
            <TD  class=title>
            	 扫描件维护状态
            </TD>
            <TD>
            <Input class= 'codename' style="width:50px" name=ScanStatus value="1"><Input class= 'codeno'  style="width:130px" name=ScanStatusName value="全部录入" verify="扫描件维护状态|NOTNULL&len<=200" CodeData= "0|^1|全部录入^2|部分录入^3|尚未录入"   ondblclick="showCodeListEx('ScanStatus',[this,ScanStatus],[1,0],null,null,null,'1',160);">
            </TD>
     </tr>
     <tr>
     	   <TD  class=title>
            	 扫描件编号
        </TD>
         <TD>
         <Input class= 'common' style="width:160px" name=ScanTestNO >
        </TD>
    </tr>
    </table>
<Div  id= "divLHContTypeGrid" style= "display: 'none'">
<table  class=common  align='center'>
  <TR  class= common>
    <TD  class= title>账单属性 </TD>
    <TD  class= input><input class="fcodeno" style="width:50px;" name="FeeAtti" CodeData="0|3^0|医院^1|社保结算^2|手工报销^3|社保补充" onclick="return showCodeListEx('feeatti',[this,FeeAttiName],[0,1]);" onkeyup="return showCodeListKeyEx('feeatti',[this,FeeAttiName],[0,1]);"><input class=fcodename style="width:110px;"  name=FeeAttiName ></TD>
    <TD  class= title>账单种类 </TD>
    <TD  class= input><input class="fcodeno" style="width:50px;" name="FeeType"  CodeData="0|2^1|门诊^2|住院" onclick="return showCodeListEx('FeeType',[this,FeeTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeType',[this,FeeTypeName],[0,1]);"><input class=fcodename name=FeeTypeName style="width:110px;"></TD>
    <TD  class= title>是否为健管险种</TD>
    <TD  class= input><input class="fcodeno" style="width:50px;" name="HelthRisk"  CodeData="0|2^1|是^2|否" onclick="return showCodeListEx('',[this,HelthRiskName],[0,1]);" onkeyup="return showCodeListKeyEx('',[this,HelthRiskName],[0,1]);"><input class=fcodename name=HelthRiskName style="width:110px;"></TD>
    <td style="width:100px;">
    </td>
  </tr>
 </table>
</div>
    <table>
		<tr class= common>
			<td style="width:4px">
    	    </td>
    	  	<td>
           		<Input value="查    询"   style="width:115px" type=button class=cssbutton onclick="ScanQuery();">
          	</td>
          	<td>
           		<Input value="扫描件录入"   style="width:115px" type=button class=cssbutton onclick="ScanImport();">
          	</td>
		</tr>
    </table>
    <Div  id= "divInsertHeBao" style= "display: 'none'">
		<table>
    		<tr>
    			<td style="width:4px">
    			</td>
	    	    <td>
	            	<Input value="导入核保数据"   style="width:115px" type=button class=cssbutton onclick="InsertHeBao();">
				</td>
			</tr>
		</table>
	</div>
    <Div  id= "divInsertLiPei" style= "display: 'none'">
    	<table>
    		<tr>
    			<td style="width:4px">
    			</td>
    	    <td>
            <Input value="导入理赔数据"   style="width:115px" type=button class=cssbutton onclick="InsertLiPei();">
          </td>
        </tr>
     </table>
   </div>
     <Div  id= "divLHScanCodeQueryGrid" style= "display: ''">

    	<table  class= common align='center'  >
      	<tr class=common>
      		<td>
      		  <span id="spanLHScanCodeQueryGrid"></span>
          </td>
        </tr>

      </table>

    </Div>
       <Div id= "divPage" align=center style= "display: '' ">
        <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
        <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
        <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
        <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>
  </Div>
	<Div id= "divContInput" align=left style= "display: 'none' ">
		<table>
			<tr class= common>
				<td style="width:4px"></td>
				<td>
					<Input value="契约信息录入" name="ContInput" style="width:115px" type=button class=cssbutton onclick="inputCont();">
				</td>
		    </tr>
		</table>
	</Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type = hidden name = "RelationType">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</DIV>
</body>
</html>
