<%
//程序名称：LHMainCustomHealthInput.jsp
//程序功能：功能描述
//创建日期：2005-01-18 09:08:27
//创建人  ：ctrHTML
//更新人  ：  更新人 St.GN    更新日期   2005-6-12 15:03  更新原因/内容  需求更改
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<%
	String tSQL = "";
	
	try
	{
		tSQL = request.getParameter("SQL");
	}
	catch( Exception e )
	{
		tSQL = "";
	}
%>

<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHTestQueryInputOpenInput.js"></SCRIPT> 
  <%@include file="LHTestQueryInputOpenInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();easyQueryClick();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
 
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCustomInHospital1);">
  		</td>
  		<td class=titleImg>
  			 客户体检信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHCustomInHospital1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHMainCustomHealthGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  <hr>
  	<INPUT CLASS=cssbutton VALUE="返  回" TYPE=button onclick="testQuery2();">
    
 
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>

<script>
var turnPage = new turnPageClass(); 
function easyQueryClick()
{
	self.moveTo(0,0);
	self.resizeTo(window.screen.width,window.screen.height-25);
	
	var SQL = "<%=tSQL%>";
//    alert(SQL);
	turnPage.queryModal(SQL, LHMainCustomHealthGrid);
}

//返回到体检信息页面
function testQuery2()
{
	var arrReturn = new Array();
	var tSel = LHMainCustomHealthGrid.getSelNo();
			
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{	
			try
			{	
//				alert(tSel);
//				top.opener.afterQuery0( arrReturn );
//				alert("LHCustomTestInput.jsp?Customerno="+LHMainCustomHealthGrid.getRowColData(tSel-1,1)+"&Healthno="+LHMainCustomHealthGrid.getRowColData(tSel-1,7));
				parent.opener.window.location="LHCustomTestInput.jsp?Customerno="+LHMainCustomHealthGrid.getRowColData(tSel-1,1)+"&Healthno="+LHMainCustomHealthGrid.getRowColData(tSel-1,7);
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery0接口。" + ex );
			}
			//self.close();		
			parent.opener.window.focus();
	}
}

</script>

</html>
 
