<%
//程序名称：LHBusinCostQueryInit.jsp
//程序功能：功能描述
//创建日期：2006-04-06 09:56:15
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() 
{ 
	try 
	{     
		fm.all('ServPriceCode').value = "";
		fm.all('ServPriceName').value = "";
	}
  	catch(ex) 
  	{
    	alert("在LHBusinCostQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  	}      
}                                    
function initForm() 
{
	try 
	{
		initInpBox();
		initLHBusinCostGrid();  
  	}
  	catch(re)
	{
		alert("LHBusinCostQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  	}
}
//领取项信息列表的初始化
var LHBusinCostGrid;
function initLHBusinCostGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         		//列名
	    iArray[0][4]="station";         		//列名
	    
	    iArray[1]=new Array();                            
	    iArray[1][0]="服务项目名称";         		//列名        
	    iArray[1][1]="100px";         		//列名    
	    iArray[1][3]=0;         		//列名        
	    	    
	    iArray[2]=new Array();                        
	    iArray[2][0]="定价方式代码";         		//列名    
	    iArray[2][1]="80px";         		//列名    
	    iArray[2][3]=0;         		//列名        
	    	    
	    iArray[3]=new Array();                        
	    iArray[3][0]="定价方式名称";         		//列名    
	    iArray[3][1]="100px";         		//列名    
	    iArray[3][3]=0;         		//列名        
	    
	    
	    LHBusinCostGrid = new MulLineEnter( "fm" , "LHBusinCostGrid" ); 
	    LHBusinCostGrid.canSel = 1;
	    //这些属性必须在loadMulLine前
	/*
	    LHBusinCostGrid.mulLineCount = 0;   
	    LHBusinCostGrid.displayTitle = 1;
	    LHBusinCostGrid.hiddenPlus = 1;
	    LHBusinCostGrid.hiddenSubtraction = 1;
	    LHBusinCostGrid.canSel = 1;
	    LHBusinCostGrid.canChk = 0;
	    LHBusinCostGrid.selBoxEventFuncName = "showOne";
	*/
	    LHBusinCostGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //LHBusinCostGrid.setRowColData(1,1,"asdf");
	}
	catch(ex)
	{
		alert(x);
  	}
}
</script>
