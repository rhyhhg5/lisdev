/** 
 * 程序名称：LHHealthQueryInputOpenInput.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-05-18 09:08:27
 * 创建人  ：hm
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     

//  var strSql="select distinct a.CustomerNo,b.name, a.InHospitNo, d.HospitName,a.InHospitDate,"
//  +" e.DoctName,a.InHospitMode,c.MainCureMode"
//  +" from LHCustomInHospital a, ldperson b,LHDiagno c,LDHospital d,LDDoctor e where   "
//  +" a.CustomerNo=b.CustomerNo and b.CustomerNo=c.CustomerNo and a.InHospitNo = c.InHospitNo and a.HospitCode=d.HospitCode"
//  +" and c.DoctNo=e.DoctNo "
//  +getWherePart("a.CustomerNo","CustomerNo") 
//  +getWherePart("c.CustomerNo","CustomerNo")
//  +getWherePart("b.CustomerNo","CustomerNo")
//  ;

//	turnPage.queryModal(strSql,  LHCustomInHospitalGrid);
}
function showOne(parm1, parm2) {	

}
function returnParent()
{
        var arrReturn = new Array();
	      var tSel = LHMainCustomHealthGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery0接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHMainCustomHealthGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LHMainCustomHealthGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}


function healthQuery2()
{
	var arrReturn = new Array();
	var tSel = LHMainCustomHealthGrid.getSelNo();
			
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{	
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
//				top.opener.afterQuery0( arrReturn );
//				alert("LHMainCustomerHealth.jsp?Customerno="+arrReturn[0][0]+"&Healthno="+arrReturn[0][8]);
				parent.opener.window.location="LHMainCustomerHealth.jsp?Customerno="+arrReturn[0][0]+"&Healthno="+arrReturn[0][9];
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery0接口。" + ex );
			}
//			self.close();		
				parent.opener.window.focus();
	}
}

//健康档案综合查询---点击基本健康信息---再选择结果中的就诊信息
function diagnoseQuery2()
{
		var arrReturn = new Array();
		var tSel = LHMainCustomHealthGrid.getSelNo();//得到基础健康信息Mulline的行号	
		arrReturn = getQueryResult();//这个结果是基础健康信息结果，只需要它的客户号，即arrReturn[0][0].
			
		if( tSel == 0 || tSel == null )
			//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
		else
		{	
			  tSel_diag = LHSelecDiagInfoGrid.getSelNo();	//得到基础健康信息客户对应的就诊信息Mulline的行号
			  if( tSel_diag == 0 || tSel_diag == null  || LHMainCustomHealthGrid.getRowColData(tSel-1,1) != LHSelecDiagInfoGrid.getRowColData(tSel_diag-1,1))
			  {	  //选择基础信息后，选择其中某一客户并点击就诊信息查询按钮。
					  initLHSelecDiagInfoGrid();
							//对应的LHSelecDiagInfoGrid的SQL语句
							var strSql="select distinct a.CustomerNo,b.name, a.InHospitNo,d.HospitName,a.InHospitDate,"
				      					+" e.DoctName,a.InHospitMode"
						      			+" from LHCustomInHospital a, ldperson b,LHDiagno c,LDHospital d,LDDoctor e where 1=1  "
									      +" and a.CustomerNo=b.CustomerNo and a.InHospitNo = c.InHospitNo and a.HospitCode=d.HospitCode and c.DoctNo=e.DoctNo"
									      +" and a.Customerno = c.Customerno and a.CustomerNo='"+arrReturn[0][0]+"' order by a.CustomerNo, a.InHospitNo"
				      					;
				      
				      var arr = easyExecSql(strSql);
				      if (arr != null)
				      {
				      	SelectDiagnose.style.display='';			//结果不为空时，显示Mulline
				      	turnPage.queryModal(strSql,  LHSelecDiagInfoGrid);
				      }
	    	}
	
	      else
	      {//选择基础信息后，又选择其Mulline某一客户的就诊信息，再点击就诊信息查询按钮。
		      		try
							{	 
									parent.opener.window.location="LHCustomInHospitalInput.jsp?Customerno="+LHSelecDiagInfoGrid.getRowColData(tSel_diag-1,1)+"&InhospitNo="+LHSelecDiagInfoGrid.getRowColData(tSel_diag-1,3);
							}
							catch(ex)
							{
									alert( "没有发现父窗口的afterQuery0接口。" + ex );
							}
//							self.close();
						  parent.opener.window.focus();
	      }
		}
}