<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLMainAskInput.js"></SCRIPT>
   <%@include file="LLMainAskInputInit.jsp"%>
 </head>

 <body  onload="initForm();initElementtype();" >
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         登记信息
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput1" style= "display: ''">
       <table  class= common>
<TR  class= common8>
<TD  class= title8>客户号码</TD><TD  class= input8><input class= common name="LogerNo" elementtype=nacessary verify="客户号码|notnull&len<=20" onkeydown="QueryOnKeyDown()"></TD>
<TD  class= title8>客户名称</TD><TD  class= input8><input class= common name="LogName" verify="客户姓名|len<=20"></TD>
<TD  class= title8>身份证号</TD><TD  class= input8><input class= common name="IDNo" verify="身份证号|len<=20"></TD>
</TR><TR  class= common8>
<TD  class= title8>登记号</TD><TD  class= input8><input  readonly class=readonly name="LogNo" ></TD>
<TD  class= title8>登记类型</TD><TD  class= input8><input class= code8 name="AskType" CodeData= "0|^0|咨询^1|通知^2|咨询通知" ondblClick="showCodeListEx('AskType',[this,''],[0,1],'', '', '', 1);" onkeyup="showCodeListKeyEx('AskType',[this,''],[0,1],'', '', '', 1);" elementtype=nacessary verify="登陆类型|notnull&len<=1"></TD>
<TD  class= title8>登记方式</TD><TD  class= input8><input class= code8 name="AskMode"  ondblClick="showCodeList('LLAskMode',[this],[0]);" onkeyup="showCodeListKeyEx('LLAskMode',[this],[0]);"></TD>
</TR><TR  class= common8>
<TD  class= title8>电话号码</TD><TD  class= input8><input class= common name="Phone" verify="电话号码|len<=20"></TD>
<TD  class= title8>手机号码</TD><TD  class= input8><input class= common name="Mobile" verify="手机号码|len<=20"></TD>
<TD  class= title8>电子邮箱</TD><TD  class= input8><input class= common name="Email" verify="电子邮箱|len<=20"></TD>
</TR><TR  class= common8>
<TD  class= title8>邮政编码</TD><TD  class= input8><input class= common name="PostCode" verify="邮政编码|len<=20"></TD>
<TD  class= title8>通讯地址</TD><TD  class= input8><input class= common name="AskAddress" verify="通信地址|len<=20"></TD>
<TD  class= title8>单位号码</TD><TD  class= input8><input class= common name="LogCompNo" verify="单位号码|len<=20"></TD>
</TR><TR  class= common8>
<TD  class= title8>单位名称</TD><TD  class= input8><input class= common name="LogComp" verify="单位名称|len<=20"></TD>

</TR>
</table>
  <div align='right'>
     <input  class=cssButton type=button value="保 存" onclick="submitForm()">
    </div>
</DIV>
  

<Div  id= "divCustomerInfo" style= "display: 'none'">
            
      <Table>
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,MulLineDiv);">
          </TD>
          <TD class= titleImg>
            咨询列表
          </TD>
        </TR>
      </Table>
     <div id="MulLineDiv">      
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCustomerGrid" >
            </span>
          </TD>
        </TR>
		  </table>
		</div>
	
</Div>
        
<br>

<input  style="display:'none'"  class=cssButton type=button value="咨询录入" onclick="showButton(0)">
            <input style="display:'none'" class=cssButton type=button value="通知录入" onclick="showButton(1)">
            
            
            <!--隐藏域-->
            <input name ="fmtransact" type="hidden">
            
            
		 	<Input type='hidden'  name=NotAvaliReason >
       	    <Input type='hidden'  name=LogTime >
       	    <Input type='hidden'  name=LogDate >
       	    <Input type='hidden'  name=AnswerMode >		
			<Input type='hidden'  name=Operator >
			<Input type='hidden'  name=MngCom >
			<Input type='hidden'  name=MakeDate >
			<Input type='hidden'  name=MakeTime >
			<Input type='hidden'  name=ModifyDate >
			<Input type='hidden'  name=ModifyTime >
			<Input type='hidden'  name=OtherNo >
			<Input type='hidden'  name=OtherNoType >
			<Input type='hidden'  name=LogState >
			<Input type='hidden'  name=SwitchDate >
			<Input type='hidden'  name=SwitchTime >
			<Input type='hidden'  name=ReplyFDate >
			<Input type='hidden'  name=DealFDate >

<div id="DivComCusInfo" style="display:'none'">
    <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,CustomerInfo);">
         </TD>
         <TD class= titleImg>
         客户信息<input  style="display:''" class=cssButton type=hidden value="客户查询" onclick="ClientQuery()">
         </TD>
       </TR>
      </table>
 <div id="CustomerInfo" style="display: 'n'">
	 <table  class= common>
	<TR  class= common8>
	<!--<TD  class= title8>客户号码</TD><TD  class= input8><Input class= common name="CustomerNo"></TD>
	<TD  class= title8>客户名称</TD><TD  class= input8><Input class= common name="CustomerName"></TD>
	<TD  class= title8>客户类型</TD><TD  class= input8><Input class= code8 name="CustomerType"></TD>-->
	</TR>
	<tr class= common8>
	<TD  class= title8>客户号码</TD><TD  class= input8><Input class= common name="CustomerNo" onkeydown="QueryOnKeyDown1();"></TD>
	<TD  class= title8>客户名称</TD><TD  class= input8><Input class= common name="CustomerName"></TD>
	<TD  class= title8>客户现状评价</TD><TD  class= input8><Input class= code8 name="CustStatus" ondblClick="showCodeList('llcuststaest',[this],[0]);" onkeyup="showCodeListKeyEx('llcuststaest',[this],[0]);" ></TD>
	</tr>
	    </table>	
</div>
</div>
	<!--客户咨询信息-->
<div id="DivCusInfo" style="display:'none'">
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,ConsultInfo);">
         </TD>
         <TD class= titleImg>
         咨询信息
         </TD>
       </TR>
      </table>
 
	<div id="ConsultInfo" style="display:''">
		<table  class= common>
			
			<TR  class= common>
				<TD  class= title colspan="6">咨询内容</TD>
			</TR>
			<TR  class= common>
				<TD  class= input colspan="6">
				    <textarea name="CContent" cols="100%" rows="3" witdh=25% class="common"></textarea>
				</TD>
			</TR>
			<TR  class= common8>
				<Input type=hidden class= common name="ReplyState"></TD>
			</TR>
			
		</table>
	</div>
</DIV>
<div align='right' id="Consultbutton" style="display :'none'">
	<INPUT VALUE="保 存" class=cssbutton TYPE=button  onclick="ConsultSave();">
	<input value="删 除" class=cssButton type=button  onclick="deleteCust()">
</div>

<div id ="DivNoticeInfo" style="display:'none'">
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,NC);">
         </TD>
         <TD class= titleImg>
         客户通知信息
         </TD>
       </TR>
      </table>
   <div id="NC" style="display: ''">
	<table  class= common>
		<TR  class= common>
			<TD  class= title colspan="6">通知内容</TD>
		</TR>
		<TR  class= common>
			<TD  class= input colspan="6">
			    <textarea name="NContent" cols="100%" rows="3" witdh=25% class="common"></textarea>
			</TD>
		</TR>
		
		</table>
		<div align='right'id="Noticebutton">
		 <input class=cssButton type=button value="通知保存"  onclick="NoticeSave()">
		 <input class=cssButton type=button value="删除"  onclick="DelNotice()">
		 <!--input class=cssButton type=button value="提起调查"  onclick="alter()"-->
		</div>
		<div align='right'id="CNSave"> 
		  <input  class=cssButton type=button value="咨询/通知保存"  onclick="CNSave1();">
		  <input  class=cssButton type=button value="删除"  onclick="alert(123);delcnsave();">
		</div>
	</div>

</div> <!-- DivNoticeInfo-->

 <div id="divEventInfo" style="display:'none'">
  <Table>
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecondUWInfo);">
          </TD>
          <TD class= titleImg>
            关联事件
          </TD>
        </TR>
      </Table>

    <Div  id= "divSecondUWInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanSubReportGrid">
            </span>
          </TD>
        </TR>
		  </table>
	<input class=cssButton type=button value="关联查询" onclick="RelaQuery()">
	<input class=cssButton type=button value="生成事件" onclick="showPage(this,divLLLLEventInput1);">


    </Div>
    
       

  <!--隐藏域-->
            
            
  <input name ="ConsultNo" type="hidden">   
  <input name ="ConsultNotmp" type="hidden">   <!--用来删除备用,暂时没用到-->
  <input name ="NoticeNo" type="hidden">       <!--先记下来备用-->
  <input name ="CNNo" type="hidden">     <!--用来记录咨询通知号-->
             
             
  
 
 <Div  id= "divLLLLEventInput1" style= "display: 'none'">
     <table  class= common>
<TR  class= common8>
<TD  class= title8>重大事件标志</TD><TD  class= input8><Input class= code8 name="SeriousGrade1" CodeData= "0|^1|重大事件^2|一般事件" ondblClick="showCodeListEx('SeriousGrade',[this],[0]);" onkeyup="showCodeListKeyEx('SeriousGrade',[this],[0]);"></TD>
<TD  class= title8>发生日期</TD><TD  class= input8><Input class= coolDatePicker name="AccDate1"></TD>
<TD  class= title8>终止日期</TD><TD  class= input8><Input class= coolDatePicker name="AccEndDate1"></TD>
</TR>
<TR  class= common8>
<TD  class= title8>事故地点</TD><TD  class= input8><Input class= common name="AccPlace1"></TD>
<TD  class= title8>客户现状</TD><TD  class= input8><Input class= code name="CustSituation1"  ondblClick="showCodeList('llcuststatus',[this],[0]);" onkeyup="showCodeListKeyEx('llcuststatus',[this],[0]);" ></TD>
<TD  class= title8>医院代码</TD><TD  class= input8><Input class= code name="HospitalCode1" ondblclick="getHospitCode();return showCodeListEx('GetHospitCode',[this,HospitalName1],[0,1],'', '', '', true);" onkeyup="getHospitCode();return showCodeListKeyEx('GetHospitCode',[this,HospitalName1],[0,1],'', '', '', true);"></TD>
</TR><TR  class= common8>
<TD  class= title8>医院名称</TD><TD  class= input8><Input class= common name="HospitalName1" ></TD>

<TD  class= title8>入院日期</TD><TD  class= common8><Input class= 'coolDatePicker' dateFormat="Short" name="InHospitalDate1"></TD>
<TD  class= title8>出院日期</TD><TD  class= common8><Input class= 'coolDatePicker' dateFormat="Short" name="OutHospitalDate1"></TD>
</TR>


<TR  class= common>
	<TD  class= title colspan="6">事故描述</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">
	    <textarea name="AccDesc1" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>


</table>
 <div align='right'>
	<input class=cssButton type=button value="事件保存" onclick="EventSave()">
 </div> 

</DIV>
</div> <!--event-->
	<div id= "AnswerInfo" style= "display: 'none'">
		<Table>
		        <TR class=common>
		          <TD class=common>
		            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAnswerInput1);">
		          </TD>
		          <TD class= titleImg>
		            回复信息
		          </TD>
		        </TR>
		      </Table>
		<div id="divAnswerInput1">
			<Table class=common>
				<TR  class= common>
					<TD  class= input colspan="6">
					    <textarea name="Answer" cols="100%" rows="3" witdh=25% class="common"></textarea>
					    <input type=hidden name="SerialNo">
					</TD>
				</TR>    
			</table>
			<div align='right'>
				<input class=cssButton type=button value="保存回复" onclick="ReplySave()">
			</div>
		</Div>
	</div>

 <! CustInfo-->
     <!--隐藏域-->
     <Input type="hidden" class= common name="SubRptNo" >
     <div id="div1" style="display: 'none'">
     	<table>
     		<TR  class= common>
	<TD  class= input colspan="6">事件主题</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">	
		<textarea name="AccSubject1" cols="100%" rows="2" witdh=25% class="common"></textarea>
	</TD>
</TR>
     		<TD  class= title8>事故类型</TD><TD  class= input8><Input class= common name="AccidentType1"></TD>
     		<TR  class= common8>
			<TD  class= title8>疾病代码</TD><TD  class= input8><Input class= code8 name="DiseaseCode" ondblclick="showCodeList('diseascode',[this],[0]);" onkeyup="showCodeListEx('diseascode',[this],[0]);"></TD>
			<TD  class= title8>疾病描述</TD><TD  class= input8><input class= common name="DiseaseDesc"></TD>
			<TD  class= title8>意外代码</TD><TD  class= input8><input class= common name="AccCode"></TD>
			</TR><TR  class= common8>
			<TD  class= title8>意外描述</TD><TD  class= input8><input class= common name="AccDesc"></TD>
			<TD  class= title8>医院代码</TD><TD  class= input8><input class= common name="HospitalCode"></TD>
			<TD  class= title8>医院名称</TD><TD  class= input8><input class= common name="HospitalName"></TD>
			</TR><TR  class= common8>
			<TD  class= title8>入院日期</TD><TD  class= common8><input class= 'coolDatePicker' dateFormat="Short" name="InHospitalDate"></TD>
			<TD  class= title8>入院日期</TD><TD  class= common8><input class= 'coolDatePicker' dateFormat="Short" name="AnswerType"></TD>
			<TD  class= title8>出院日期</TD><TD  class= common8><input class= 'coolDatePicker' dateFormat="Short" name="OutHospitalDate"></TD>
			<TD  class= title8>客户现状</TD><TD  class= input8><Input class= code8 name="" verify="客户状态|len<=2"  ondblClick="showCodeList('llcuststatus',[this],[0]);" onkeyup="showCodeList('llcuststatus',[this],[0]);"></TD>
			</tr>
		</table>
			<TR  class= common8>
				<TD  class= title8>是否咨询专家</TD><TD  class= input8><Input class= code name="ExpertFlag" CodeData= "0|^0|不咨询专家^1|咨询专家" ondblClick="showCodeListEx('AskType',[this,''],[0,1],'', '', '', 1);" onkeyup="showCodeListKeyEx('AskType',[this,''],[0,1],'', '', '', 1);"></TD>
				<TD  class= title8>专家编号</TD><TD  class= input8><Input class= common name="ExpertNo"></TD>
				<TD  class= title8>专家姓名</TD><TD  class= input8><Input class= common name="ExpertName"></TD>
			</TR>
			<TR  class= common8>	
				<TD  class= title8>	期待回复日期</TD><TD  class= input8><Input class= 'coolDatePicker' dateFormat="Short" name="ExpRDate" ></TD>
				<TD  class= title8>咨询级别</TD><TD  class= input8><Input class= code name="AskGrade" CodeData= "0|^1|一级^2|二级^3|三级" ondblClick="showCodeListEx('AskType',[this,''],[0,1],'', '', '', 1);" onkeyup="showCodeListKeyEx('AskType',[this,''],[0,1],'', '', '', 1);"></TD>
				<TD  class= title8>	咨询有效标志</TD><TD  class= input8><Input class= code name="AvaiFlag" CodeData= "0|^0|无效^1|有效" ondblClick="showCodeListEx('AskType',[this,''],[0,1],'', '', '', 1);" onkeyup="showCodeListKeyEx('AskType',[this,''],[0,1],'', '', '', 1);"></TD>	
					<TD  class= title8>客户类型</TD><TD  class= input8><Input class= code8 name="CustomerType"></TD>
			</TR>
			<TR  class= common>
				<TD  class= input colspan="6">咨询主题</TD>
			</TR>
			<TR  class= common>
				<TD  class= input colspan="6">	
					<textarea name="CSubject" cols="100%" rows="2" witdh=25% class="common"></textarea>
				</TD>
				
				<TD  class= title8>是否邮寄资料</TD><TD  class= input8><input class= code name="SendFlag" verify="是否邮件资料|len<=20" CodeData= "0|^1|是^0|否" ondblClick="showCodeListEx('SendFlag',[this],[0]);" onkeyup="showCodeListKeyEx('SendFlag',[this],[0]);"></TD>
<TD  class= title8>转入部门</TD><TD  class= input8><input class= common name="SwitchCom" verify="转入部门|len<=20"></TD>
			</TR>
    <input class=cssButton type=button value="查询事件" onclick="EventQuery();">
	<input class=cssButton type=button value="删除关联" onclick="DelRela()">
	<TR  class= common>
			<TD  class= title colspan="6">通知主题</TD>
		</TR>
		<TR  class= common>
			<TD  class= input colspan="6">
			    <textarea name="NSubject" cols="100%" rows="3" witdh=25% class="common"></textarea>
			</TD>
	</TR>
	</div>
     	
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
