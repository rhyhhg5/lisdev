<%
//程序名称：LDICDOPSQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-08 17:08:58
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ICDOPSCode').value = "";
    fm.all('ICDOPSName').value = "";
    fm.all('OpsGrag').value = "";
    fm.all('FrequencyFlag').value = "";

  }
  catch(ex) {
    alert("在LDICDOPSQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDICDOPSGrid();  
  }
  catch(re) {
    alert("LDICDOPSQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDICDOPSGrid;
function initLDICDOPSGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="手术分类代码";         		//列名
    iArray[1][1]="70px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="手术分类名称";         		//列名
    iArray[2][1]="200px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="代码等级";         		//列名
    iArray[3][1]="80px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="说明";         		//列名
    iArray[4][1]="0px";         		//列名
    iArray[4][3]=3;         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="手术风险等级";         		//列名
    iArray[5][1]="50px";         			  //列名
    iArray[5][3]=0;         				    //列名
    
    
    LDICDOPSGrid = new MulLineEnter( "fm" , "LDICDOPSGrid" ); 
    //这些属性必须在loadMulLine前

    LDICDOPSGrid.mulLineCount = 0;   
    LDICDOPSGrid.displayTitle = 1;
    LDICDOPSGrid.hiddenPlus = 1;
    LDICDOPSGrid.hiddenSubtraction = 1;
    LDICDOPSGrid.canSel = 1;
    LDICDOPSGrid.canChk = 0;
    //LDICDOPSGrid.selBoxEventFuncName = "showOne";

    LDICDOPSGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDICDOPSGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
