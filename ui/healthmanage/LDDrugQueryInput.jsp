<%
//程序名称：LDDrugInput.jsp
//程序功能：功能描述
//创建日期：2005-01-19 13:58:55
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LDDrugQueryInput.js"></SCRIPT> 
  <%@include file="LDDrugQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();initElementtype();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLDDrugGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      药品编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugCode >
    </TD>
    <TD  class= title>
      通用名
    </TD>
    <TD  class= input>
      <Input class= 'code' name=DrugName ondblClick="showCodeList('hmdrug',[this,DrugCode],[0,1],null,fm.DrugName.value,'DrugName',1,350);" onkeyup=" if(event.keyCode==13) showCodeList('hmdrug',[this,DrugCode],[0,1],null,fm.DrugName.value,'DrugName',1,350);">
    </TD>
    
		 <TD  class= title>
      类别等级
    </TD>
    <TD  class= input>
  
    	<Input class= common type=hidden name= ClassLevel>
      <Input class= 'code	' name=ClassLevel_ch elementtype=nacessary verify="|len<=2" CodeData= "0|^1|1级^2|2级^3|3级^4|4级" ondblClick= "showCodeListEx('DrugClassLevel1',[this,ClassLevel],[1,0],null,null,null,1);" >
    </TD>
  </TR>  
</Table>
</Div>

<Div  id= "divLDDrugGrid2" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      英文名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugEnName >
    </TD>
    <TD  class= title>
      药品剂型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugDoseCode >
    </TD>
    <TD class= title>
    </TD>
    <TD class= input>
    </TD>
  </TR>
<!--
  <TR  class= common>
    <TD  class= title>
      拉丁名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugLtName >
    </TD>
    <TD  class= title>
      中文简拼
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugNameAbbr >
    </TD>
  </TR>
-->  
</table>
  </Div>
  <div id="div1" style="display: 'none'">
  <table>
    <TR  class= common>

    <TD  class= title>
      药品剂型代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugDoseCode >
    </TD>
    <TD  class= title>
      处方药标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RecipeFlag >
    </TD>
  </TR>

  <TR  class= common>
    <TD  class= title>
      新特药标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SpecialFLag >
    </TD>
  </TR>
    <TD  class= title>
      标准价格
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrgStdPrice >
    </TD>
    <TD  class= title>
      药品上级目录编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SuperDrugCode >
    </TD>
    <TD  class= title>
      目录标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IndexFlag >
    </TD>
    <TD  class= title>
      药品规格代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugSpecCode >
    </TD>
    <TD  class= title>
      计量单位代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrugMetrCode >
    </TD>
    <TD  class= title>
      使用说明
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DrgUsage >
    </TD>
  </table>
  </div>  
    
<Div id="divLDDrug2" style="display:''">    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
<Div>
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDDrug1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLDDrug1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLDDrugGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
   <Div id="divLDDrug3" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLDDrugClassGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div> 	
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
