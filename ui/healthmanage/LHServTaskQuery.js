//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var StrSql;
var FeeAllSql="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHSettingSlipQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    //showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
          
//Click事件，当点击增加图片时触发该函数
  

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
            
        
function checkDate()
{
	if(fm.all('StartDate').value != "" && fm.all('EndDate').value != "")
	{
			if(compareDate(fm.all('StartDate').value,fm.all('EndDate').value) == true)
			{
				alert("保单起始时间应早于保单终止时间 ");
				return false;
			}
	}
}
function showQueryInfo()
{ 
	fm.all('FeeFlag').value="0";
	var tempCom = manageCom.length==8?manageCom.substring(0,4):manageCom;

	var ServCaseType="";
	if(fm.all('ServCaseType').value != "")
	{
		  ServCaseType =" and b.ServCaseCode in ( select distinct ServCaseCode from  LHServCaseDef  where  ServCaseType = '"+fm.all('ServCaseType').value+"')";	
	}
	var ServCaseState="";
	if(fm.all('ServCaseState').value != "")
	{
		  ServCaseType =" and b.ServCaseCode in ( select distinct ServCaseCode from  LHServCaseDef  where  ServCaseState = '"+fm.all('ServCaseState').value+"')";	
	}
	var ServCaseName="";
	if(fm.all('ServCaseName').value != "")
	{
		  ServCaseName =" and b.ServCaseCode in ( select distinct ServCaseCode from  LHServCaseDef  where  ServCaseName = '"+fm.all('ServCaseName').value+"')";	
	}
	var ServTaskName="";
	if(fm.all('ServTaskName').value != "")
	{
		  ServTaskName =" and b.ServTaskCode in ( select distinct ServTaskCode from  LHServTaskDef  where  ServTaskName = '"+fm.all('ServTaskName').value+"')";	
	}
	var strSql=" select distinct b.ServCaseCode ,"
	 		  +" (select ServCaseName from LHServCaseDef d where d.ServCaseCode=b.ServCaseCode),  "
	 		  +" (select ServTaskName from LHServTaskDef t where t.ServTaskCode=b.ServTaskCode),"
	 		  +" (select codename from ldcode c where c.codetype='taskstatus' and c.code=b.ServTaskState),"
	 		  +" b.ServTaskCode, b.ServTaskState,b.ServTaskNo,"
	 		  +" (case b. ServTaskAffirm when '1' then '未确认' when '2' then '已确认' else '无' end ), "
	 		  +" b.ServTaskAffirm ,b.PlanExeDate,b.TaskFinishDate"
	 		  +" from LHCaseTaskRela b,lhservcaserela r where b.servcasecode = r.servcasecode and r.comid like '"+tempCom+"%%' "
     		+getWherePart("b.ServCaseCode","ServCaseCode")  
     		+getWherePart("b.ServTaskState","ServTaskState")
     		+getWherePart("b.ServTaskAffirm","TaskValidate")
     		+getWherePart("b.ServTaskCode","ServTaskCode")
     		+getWherePart("b.PlanExeDate","StartDate",">") 
	 		  +getWherePart("b.PlanExeDate","EndDate","<")
	 		  +getWherePart("b.TaskFinishDate","FinStartDate",">") 
	 		  +getWherePart("b.TaskFinishDate","FinEndDate","<")
//	 		  +getWherePart("b.Managecom","ComID", "like")	 		  
     		+ServCaseType
     		+ServCaseState
     		+ServCaseName
     		+ServTaskName
     		;
     //    alert(strSql);
     //   alert(easyExecSql(strSql));
	turnPage.queryModal(strSql, LHSettingSlipQueryGrid);
}
function showOne()
{
	var getSelNo = LHSettingSlipQueryGrid.getSelNo();
	var ContNo = LHSettingSlipQueryGrid.getRowColData(getSelNo-1, 1);

	
	divLHQueryDetailGrid.style.display='';
	divPage2.style.display='';
	try
	{ 
		var strSql=" select InsuredNo,InsuredName,(select ServPlanName from LHServPlan b  where b.ContNo=a.ContNo), "
	            +"(select case when ServPlanLevel='1' then  '一档' when ServPlanLevel='2' then  '二档'when ServPlanLevel='3' then  '三档' when  ServPlanLevel='4' then  '四档' when ServPlanLevel='5' then  '五档' when ServPlanLevel='6' then  '六档' when ServPlanLevel='7' then  '七档'  else '无' end  from LHServPlan c  where c.ContNo=a.ContNo) "
                +" from lccont a where ContNo='"+ContNo+"' "
         //alert(strSql);
         //alert(easyExecSql(strSql));
		turnPage2.queryModal(strSql, LHQueryDetailGrid);
	}
	catch(ex)
	{
		alert("LHItemPriceFactorInputInit.jsp-->传输入信息Info中发生异常:初始化界面错误!");
	}
}
function showContPlan()
{
	if(fm.all('ContType').value==""||fm.all('ContType').value=="null"||fm.all('ContType').value==null)
	{
		alert("请选择保单类型!");
	}
	if(fm.all('ContType').value=="1")
	{
		if(LHSettingSlipQueryGrid.getSelNo() >= 1)
		{
		   if(LHQueryDetailGrid.getSelNo() >= 1)
		   {
		      var ContNo = LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1);//保单号
		      var strSql = "select distinct PrtNo from lccont where ContNo='"+ContNo+"'";
		      var prtNo = easyExecSql(strSql);//印刷号
		      var CustomerNo = LHQueryDetailGrid.getRowColData(LHQueryDetailGrid.getSelNo()-1,1);//客户号
	        window.open("./LHContPlanSetting.jsp?ContNo="+ContNo+"&prtNo="+prtNo+"&CustomerNo="+CustomerNo+"","个人服务计划设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
       }
       else
       {
       	 alert("请选择一条客户信息!");
       }
    }
    else
    {
    	alert("请选择一条保单信息!");
    }
  }
  if(fm.all('ContType').value=="2")
	{
		if(LHSettingSlipQueryGrid.getSelNo() >= 1)
		{
		   var ContNo = LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1);//保单号
	    window.open("./LHGroupPlanSetting.jsp?ContNo="+ContNo+"","团体服务计划设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
    }
    else
    {
    	alert("请选择一条保单信息!");
    }
  }
}
function ServQieYueInfo()
{
	  
	   if (LHSettingSlipQueryGrid.getSelNo() >= 1)
			{     // alert(LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1));
					  if(LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1)=="")
					  {
							  alert("你选择了空的列，请重新选择!");
								return false;
					  }
						else
						{
							   //var ContraNo=fm.ContraNo.value;
							   var ServCaseCode = LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1);
							   //alert(ServCaseCode);
							    var EventCode=ServCaseCode;
                  //alert(EventCode.toString().substring(8));
                  var caseCode=EventCode.toString().substring(8);
                  //alert(caseCode.toString().substring(0,1));
                  var code=caseCode.toString().substring(0,1);
                  if(code=="2")
                  {
						         window.open("./LHServQieYueInfoMain.jsp?ServCaseCode="+ServCaseCode+"&flag=1","服务契约信息管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			            }
			            if(code=="1")
			            {
			            	alert("此事件类型不能进行服务契约信息管理!");
			            }
			      }
			} 
			else
			{
					alert("请选择一条要传输的记录！");    
			}
}
function CaseExecManage()
{
	if (LHSettingSlipQueryGrid.getSelNo() >= 1)
	{
		    var ServCaseCode = LHSettingSlipQueryGrid.getRowColData(LHSettingSlipQueryGrid.getSelNo()-1,1);
			  //alert(ServCaseCode);
	      window.open("./LHServCaseExecManageMain.jsp","服务事件实施管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
	}
	else
	{
					alert("请选择一条要传输的记录！");    
	}
}
function CaseExecManage()
{
	  var arrSelected = null;
	  arrSelected = new Array();
	  var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx] = row;
	            arrSelected[xx++] = LHSettingSlipQueryGrid.getRowColData(aa,1);
	            //alert("AA "+arrSelected);
	        }
	  }
		if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		{
			  alert("请选择要传输的事件信息！");
			  return false;
		}	
		if(aa.length>="1")
		{
	      window.open("./LHServCaseExecManageMain.jsp?arrSelected="+arrSelected+"","服务事件实施管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		    
		} 
}
function TaskExecManage()
{
	if(fm.all('ServTaskName').value==""||fm.all('ServTaskName').value=="null"||fm.all('ServTaskName').value==null)
	{
		alert("请先选择查询要实施管理的'服务任务名称'!");
	  	fm.all('ServTaskName').focus();
	  	if( verifyInput2() == false ) return false;
	  	return false;
	}
	else
	{
//		var sqlModel="select h.ModuleURL from LHServTaskDef h where h.ServTaskCode='"+ fm.all('ServTaskCode').value +"'";
//		var ServTaskModule=new Array;
//	    ServTaskModule=easyExecSql(sqlModel);

//	    if(ServTaskModule==""||ServTaskModule=="null"||ServTaskModule==null)//服务名称模块地址不存在
//	    {
			var caseCode =  new Array();
			var servTaskCode = new Array();
	        var rowNum=LHSettingSlipQueryGrid. mulLineCount ; //行数 	
	        var aa = new Array();
	        var xx = 0;
	        for(var row=0; row < rowNum; row++)
	        {
				var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
	            if(tChk == true)
	            {
					aa[xx] = row;//得到选中的行号
					caseCode[xx] = LHSettingSlipQueryGrid.getRowColData(row,1);//得到选中事件编号
					servTaskCode[xx++] = LHSettingSlipQueryGrid.getRowColData(row,7);//得到选中事件编号
	            }
			}

		    if(aa.length=="0"||aa.length==""||aa.length=="null"||aa.length==null)
		    {
		    	  alert("请选择要传输的事件信息！");
		    	  return false;
		    }	

		    if(aa.length>="1")
		    {
	          window.open("./LHServiceManageMain.jsp?caseCode="+caseCode+"&arrSelected="+servTaskCode+"&flag=2","一般服务任务管理",'width=1024,height=748,top=0,left=0,status=yes,menubar=no,location=yes,directories=no,resizable=yes,scrollbars=auto,toolbar=no');
		    } 
	}	
}

function checkState()
{
	
	var rowNum2=LHSettingSlipQueryGrid. mulLineCount ; //行数 
	var aa = new Array();	
	var xx = 0;
	var tArr = new Array();
	for(var row2=0; row2 < rowNum2; row2++)
	{
		 var tChk2 =LHSettingSlipQueryGrid.getChkNo(row2); 
	   if(tChk2 == true)
	   {
	  	  
	  	   aa[xx] = row2;
	  	   var tArr1= new Array();
         tArr1[xx++] = LHSettingSlipQueryGrid.getRowColData(aa,3); 
         //alert("cC "+tArr1);
         tArr[row2]=tArr1;
        
	   }
	 }
	 //alert(tArr);
  return true;
}
function ServAffirm()
{
		//对任务状态及确认状态的校验
	  if(checkAffirm()==false)
	  return false;
	  if (confirm("您确实想确认这些服务任务吗?"))
    {
          var i = 0;
          var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    
          fm.fmtransact.value = "UPDATE||MAIN";
          fm.action="./LHServTaskAffirmSave.jsp";
          fm.submit(); //提交
		 }
     else
     {
          alert("您取消了确认服务任务的操作！");
     }  
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
}
function checkAffirm()
{
	var rowNum=LHSettingSlipQueryGrid.mulLineCount ; //行数 
	var aa = new Array();
	var xx = 0;	
	for (var row = 0; row < rowNum; row++)
	{
		 var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
	   if(tChk == true)
	   {
	       aa[xx++] = row;	
	       //alert("DD"+aa);
		     //alert(LHSettingSlipQueryGrid.getRowColData(row,6));
		     if(LHSettingSlipQueryGrid.getRowColData(row,6) == "1" || LHSettingSlipQueryGrid.getRowColData(row,6) == "2")	
		     {
		     		alert("服务任务状态为 ‘等待执行’ 或 ‘正在执行’ 时，不能进行确认操作!");	
		     		return false;
		     		
		     }
		  }
	}
}

function afterCodeSelect(codeName,Field)
{
	if(codeName=="taskstatus")
	{
		if(Field.value=="3" && fm.all('ServTaskCode').value=="01602" && fm.all('TaskValidate').value=="1")
		{
			iFeePay.style.display="";
			return true;
		}
		else
		{
			iFeePay.style.display="none";
			return true;		
		}
	}
	if(codeName=="lhservtaskcode")
	{
		if(Field.value=="01602" && fm.all('ServTaskState').value=="3" && fm.all('TaskValidate').value=="1")
		{
			iFeePay.style.display="";
			return true;
		}
	    else
		{
			iFeePay.style.display="none";
			return true;		
		}
	}
	if(codeName=="taskvalidate")
	{
		if(Field.value=="1" && fm.all('ServTaskState').value=="3" && fm.all('ServTaskCode').value=="01602")
		{
			iFeePay.style.display="";
			return true;
		}
		else
		{
			iFeePay.style.display="none";
			return true;		
		}
	}
	if(codeName=="lhhospitname")
	{
		initLHSettingSlipQueryGrid();
	}
}

function feeQuery()
{
	if(fm.all('HospitCode').value == "")	
	{
		alert("请先选择医院代码");
		return false;
	}
	feeQueryDetail();
}

function afterFeeSubmit( FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
    	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
 	}
    else
	{ 
    	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

function feeAllSubmit()
{
	var arrFeeAll = new Array(); 
	
	if(FeeAllSql=="")
	{alert("请先进行财务数据查询"); return false;}
	
	if(fm.all('FeeFlag').value != "1")
	{//校验查询结果是否为财务查询
		alert("请先点击〖查询财务数据〗按钮，进行财务数据查询！");
		return false;
	}
	
	arrFeeAll = easyExecSql(FeeAllSql);
	if(arrFeeAll==null || arrFeeAll=="")
	{
		alert("没有财务数据，请确认");
		return false;		
	}
	if(checkAllHospital(arrFeeAll)==false) return false;
	
	fm.all('FeeAllSql').value = FeeAllSql;
	//提交数据
	var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "INSERT||MAIN";
	fm.action="./LHServTaskFeeAllSave.jsp";
	fm.submit();
}

function checkAllHospital(arrFeeAll)
{
	var ServTaskNo = "";
	var len = arrFeeAll.length
	for(var i=0; i< len; i++ )
	{
		ServTaskNo = arrFeeAll[i][6];
		var sql = " select count( distinct b.hospitcode) from lhfeecharge f, lhexecfeebalance b "
				 +" where f.feetaskexecno = b.taskexecno and f.servtaskno='"+ServTaskNo+"' ";
		if(easyExecSql(sql) != "1")
		{
			alert("‘"+arrFeeAll[i][1]+"’ 服务事件的 ‘"+arrFeeAll[i][2]+"’ 服务任务有多个结算对象，请确认！");	
			return false;
		}
	}
}


function feeSubmit()
{
	var checkRowNo = 0;
	var checkArr = new Array();
	if(fm.all('FeeFlag').value != "1")
	{//校验查询结果是否为财务查询
		alert("请先点击〖查询财务数据〗按钮，进行财务数据查询！");
		return false;
	}
	
	for(var row=0; row < LHSettingSlipQueryGrid.mulLineCount; row++)
	{//校验是否选中
		var tChk =LHSettingSlipQueryGrid.getChkNo(row); 
		if(tChk == true)
		{
			checkArr[checkRowNo++] = row;
		}
	}
	if(checkArr.length == 0)
	{
		alert("请先选择要提交的财务数据");	
		return false;
	}
	
	//校验一个任务下是否多个结算对象
	if(checkHospital(checkArr)==false) return false;
	
	//提交数据
	var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.fmtransact.value = "INSERT||MAIN";
	fm.action="./LHServTaskFeeSave.jsp";
	fm.submit();
}


function checkHospital(checkArr)
{
	var ServTaskNo = "";
//	var strServTaskNo = "";
	for(var i=0; i< checkArr.length; i++ )
	{
		ServTaskNo = LHSettingSlipQueryGrid.getRowColData(checkArr[i],7);
		var sql = "select count( distinct b.hospitcode) from lhfeecharge f, lhexecfeebalance b where f.feetaskexecno = b.taskexecno and f.servtaskno='"+ServTaskNo+"' ";
		if(easyExecSql(sql) != "1")
		{
			alert("‘"+LHSettingSlipQueryGrid.getRowColData(checkArr[i],2)+"’ 服务事件的 ‘"+
					LHSettingSlipQueryGrid.getRowColData(checkArr[i],3)+"’ 服务任务有多个结算对象，请确认！");	
			return false;
		}
//		strServTaskNo = ",'"+ServTaskNo+"'"+strServTaskNo;
	}
//	strServTaskNo = strServTaskNo.substring(1);
//	alert(strServTaskNo);
//	var sqlAll = "select count( distinct b.hospitcode) from lhfeecharge f, lhexecfeebalance b where f.feetaskexecno = b.taskexecno and f.servtaskno in ("+ServTaskNo+") ";
//	if(easyExecSql(sql) != "1")
//	{
//		alert("所选择的多个服务任务有多个结算对象，请确认！");	
//		return false;
//	}
}


function feeQueryDetail()
{ 
	fm.all('FeeFlag').value="1";
	var tempCom = manageCom.length==8?manageCom.substring(0,4):manageCom;

	var ServCaseType="";
	if(fm.all('ServCaseType').value != "")
	{
		  ServCaseType =" and b.ServCaseCode in ( select distinct ServCaseCode from  LHServCaseDef  where  ServCaseType = '"+fm.all('ServCaseType').value+"')";	
	}
	var ServCaseState="";
	if(fm.all('ServCaseState').value != "")
	{
		  ServCaseType =" and b.ServCaseCode in ( select distinct ServCaseCode from  LHServCaseDef  where  ServCaseState = '"+fm.all('ServCaseState').value+"')";	
	}
	var ServCaseName="";
	if(fm.all('ServCaseName').value != "")
	{
		  ServCaseName =" and b.ServCaseCode in ( select distinct ServCaseCode from  LHServCaseDef  where  ServCaseName = '"+fm.all('ServCaseName').value+"')";	
	}
	var ServTaskName="";
	if(fm.all('ServTaskName').value != "")
	{
		  ServTaskName =" and b.ServTaskCode in ( select distinct ServTaskCode from  LHServTaskDef  where  ServTaskName = '"+fm.all('ServTaskName').value+"')";	
	}
	var strSql=" select distinct b.ServCaseCode ,"
	 		  +" (select ServCaseName from LHServCaseDef d where d.ServCaseCode=b.ServCaseCode),  "
	 		  +" (select ServTaskName from LHServTaskDef t where t.ServTaskCode=b.ServTaskCode),"
	 		  +" (select codename from ldcode c where c.codetype='taskstatus' and c.code=b.ServTaskState),"
	 		  +" b.ServTaskCode, b.ServTaskState,b.ServTaskNo,'未确认',b.ServTaskAffirm ,b.PlanExeDate,b.TaskFinishDate"
	 		  +" from LHCaseTaskRela b,lhservcaserela r "
	 		  +"where b.servcasecode = r.servcasecode and r.comid like '"+tempCom+"%%' "
	 		  +" and  b.servtaskno in (select distinct f.servtaskno from lhfeecharge f, lhexecfeebalance b where f.feetaskexecno = b.taskexecno and b.HospitCode='"+fm.all('HospitCode').value+"') "
     		  +getWherePart("b.ServCaseCode","ServCaseCode")  
     		  +getWherePart("b.ServTaskState","ServTaskState")
     		  +getWherePart("b.ServTaskAffirm","TaskValidate")
     		  +getWherePart("b.ServTaskCode","ServTaskCode")
     		  +getWherePart("b.PlanExeDate","StartDate",">") 
	 		  +getWherePart("b.PlanExeDate","EndDate","<")
	 		  +getWherePart("b.TaskFinishDate","FinStartDate",">") 
	 		  +getWherePart("b.TaskFinishDate","FinEndDate","<")
//	 		  +getWherePart("b.Managecom","ComID", "like")	 		  
     		  +ServCaseType
     		  +ServCaseState
     		  +ServCaseName
     		  +ServTaskName
     		;
        // alert(strSql);
        //alert(easyExecSql(strSql));
	turnPage.queryModal(strSql, LHSettingSlipQueryGrid);
	FeeAllSql = strSql;
}

