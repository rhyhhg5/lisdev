/** 
 * 程序名称：LHAddNewHmPlanQuery.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2006-07-6 10:50:44
 * 创建人  ：郭丽颖
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
  var LEN;
	if( manageCom.length == 8)  LEN = 4;
	else{LEN = manageCom.length;}
	var strSql = "select distinct  case a.ServCaseType when '1' then '个人服务事件' when '2' then '集体服务事件'  else '无' end, "
	           +" a.ServCaseCode,a.ServCaseName,"
	           +" (select d.codename from ldcode d where d.codetype='eventstate' and d.code=a.ServCaseState),"
	           +"a.CaseLaunchState,a.ServCaseDes,a.ServCaseType  ,"
	           +" '' ,ServCaseState "
//	           +" from LHServCaseDef  a, LHServCaseRela  b "
			   +" from LHServCaseDef  a "
//	           +" where a.ServCaseCode=b.ServCaseCode  and  servcasestate = '0'  "
			   +" where  servcasestate = '0'  "
             + getWherePart("a.ServCaseType", "ServEventType","like")
             + getWherePart("a.ServCaseCode", "ServCaseCode" ,"like")
             + getWherePart("a.ServCaseName", "ServCaseName" ,"like")
             +" and ManageCom like '"+manageCom.substring(0,LEN)+"%%'";
  //alert(strSql);
    turnPage.pageLineNum = 12;
	turnPage.queryModal(strSql, LHHealthServItemGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LHHealthServItemGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
	{
		top.close();
		alert("请选择一条数据，再点击返回按钮！");
	}
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
                //alert(arrReturn);
             top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHHealthServItemGrid.getSelNo();
	//alert("111" + tRow);
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	
	arrSelected[0] = new Array();
	arrSelected[0] = LHHealthServItemGrid.getRowData(tRow-1);

    //arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
function showOne()
{
	  var getSelNo = LHHealthServItemGrid.getSelNo();
	 	  //alert(getSelNo);
	    var ServCaseCode = LHHealthServItemGrid.getRowColData(getSelNo-1, 2);
	   
	    fm.all('ServCaseCode2').value=ServCaseCode;
	    //alert(fm.all('ServCaseCode2').value);
}
function AddNewSetting()
{
	 tRow = LHHealthServItemGrid.getSelNo();
	 if( tRow == 0 || tRow == null )
	 {
	 	 alert("请先查询选择一条要关联的数据！");
	 }
	 else
	 {
	 	  var getSelNo = LHHealthServItemGrid.getSelNo();
	 	  //alert(getSelNo);
	    var ServCaseCode = LHHealthServItemGrid.getRowColData(getSelNo-1, 2);
//	   alert(ServCaseCode);
	    //fm.all('ServCaseCode').value=ServCaseCode;
	     var ServCaseState = LHHealthServItemGrid.getRowColData(getSelNo-1, 9);
	     //alert(ServCaseState);
	     if(ServCaseState=="0"||ServCaseState=="null"||ServCaseState==null||ServCaseState=="")
	     {
	         var ServPlanNo=fm.all('ServPlanNo').value;
	         
	         var ComID1 = easyExecSql("select comid from lhservplan where servplanno = '"+ServPlanNo+"' with ur");
	         var ComID2 = easyExecSql("select managecom from lhservcasedef where servcasecode = '"+ServCaseCode+"'  with ur");
			 if (ComID1 == null||ComID2 == null)
			 	{
			 		alert("计划或者集体服务事件对应的管理机构为空");
			 	}
			 	else
	         		{
	         		if(ComID1[0][0]==ComID2[0][0])
	        			{
	       				 submitForm();
	       				 }
	       			 else
	        			{
	        			alert("关联计划的机构代码和所要关联事件的机构代码必须一致！"); //xiepan 2008-8-7 校验管理机构一致性
	        			}
	        		}	
	     }
	     else
	     {
	     	  alert("您只能对未进行启动设置的服务事件状态进行关联!");
	     	  return false;
	     }
	 }
}
//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput2() == false ) return false;

  var i = 0;
  var showStr="正在关联数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.action="./LHAddNewHmPlanQuerySave.jsp";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    return false;
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
     window.close();
	   top.opener.focus(); 		
	   parent.top.opener.initServCase();
	   top.close();
    //执行下一步操作
  }
}
