/** 
 * 程序名称：LHServiceRecordQueryInput.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-06-18 09:08:27
 * 创建人  ：ctrHTML
 * 更新人  ：更新人    更新日期    更新原因/内容  需求更改
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
/*	
	var strSql = "select a.GetNoticeNo, a.otherno, b.name, a.PayDate, a.SumDuePayMoney, a.bankcode, a.bankaccno, a.accname "
	           + " from LJSPay a, ldperson b where a.appntno=b.customerno and OtherNoType='2' "
          	 + getWherePart("a.GetNoticeNo", "GetNoticeNo2")
          	 + getWherePart("a.OtherNo", "OtherNo")
          	 + getWherePart("a.BankCode", "BankCode")
          	 + getWherePart("a.SumDuePayMoney", "SumDuePayMoney");
  
  if (fm.AppntName.value != "") strSql = strSql + " and a.appntno in (select c.customerno from ldperson c where name='" + fm.AppntName.value + "')";
  if (fm.PrtNo.value != "") strSql = strSql + " and a.otherno in (select polno from lcpol where prtno='" + fm.PrtNo.value + "')";
*/  			    
//	var strSql = "select a.CustomerNo,b.name,a.InHospitNo,a.InHospitDate,a.HospitCode,a.DiagnoCircs,a.CureEffect from LHCustomInHospital a,LDPerson b where 1=1 and trim(a.CustomerNo)=trim(b.CustomerNo)"
//    + getWherePart("a.CustomerNo", "CustomerNo")
//    + getWherePart("a.InHospitNo", "InHospitNo")
//    + getWherePart("a.InHospitMode", "InHospitMode")
//    + getWherePart("a.InHospitDate", "InHospitDate")
//    + getWherePart("a.HospitCode", "HospitCode")
//    + getWherePart("a.HospitSectio", "HospitSectio")
//    + getWherePart("a.Doctor", "Doctor")
//    + getWherePart("a.DiseasCode", "DiseasCode")
//    + getWherePart("a.DiagnoCircs", "DiagnoCircs")
//    + getWherePart("a.CureEffectCode", "CureEffectCode")
//    + getWherePart("a.CureEffect", "CureEffect")
//    + getWherePart("a.TotalFee", "TotalFee")
//    + getWherePart("a.TestFee", "TestFee")
//    + getWherePart("a.OPSFee", "OPSFee")
//    + getWherePart("a.MedicaFee", "MedicaFee")
//    + getWherePart("a.BedFee", "BedFee")
//    + getWherePart("a.OtherFee", "OtherFee")
//    + getWherePart("a.Operator", "Operator")
//    + getWherePart("a.MakeDate", "MakeDate")
//    + getWherePart("a.MakeTime", "MakeTime")
//    + getWherePart("a.ModifyDate", "ModifyDate")
//    + getWherePart("a.ModifyTime", "ModifyTime")
//  ;
//  //alert(strSql);
  
  alert(easyExecSql(SQL));
  
	turnPage.queryModal(SQL, LHServiceRecordGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
  var arrReturn = new Array();
	var tSel = LHServiceRecordGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				arrReturn = getQueryResult();
//				top.opener.afterQuery0( arrReturn );
				parent.opener.window.location="LHMedicalServiceRecordInput.jsp?recordno="+arrReturn[0][9];
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery1接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHServiceRecordGrid.getSelNo();
	
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LHServiceRecordGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("HospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}


function afterQuery00(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.DiseasCode.value = arrResult[0][0];
  }
}


function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.jsp");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql);
	       //alert(arrResult);
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}
function queryCustomerNo2()
{
	
	if(fm.all('CustomerNo').value != ""&& fm.all('CustomerNo').value.length==24)	 
	{
	var cCustomerNo = fm.CustomerNo.value;  //客户号码	
	var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
      // alert(arrResult);
    if (arrResult != null) {
      fm.CustomerNo.value = arrResult[0][0];
      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户名称:["+arrResult[0][1]+"]");
    }
    else{
     //fm.DiseasCode.value="";
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}
function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  	//var strSql="select name from ldperson  where 1=1 "+getWherePart("CustomerNo","CustomerNo");
	  
  }
}


function queryBack()
{
	var arrReturn = new Array();
	var tSel = LHServiceRecordGrid.getSelNo();
	alert();
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				arrReturn = getQueryResult();
				top.opener.afterQuery1( arrReturn );
//				parent.window.open("../healthmanage/LHMedicalServiceRecordInput.jsp");
				//="../healthmanage/LHMedicalServiceRecordInput.jsp";
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery1接口。" + ex );
			}
			top.close();
		
	}
}