<%
//程序名称: LHHosptalSearchInit.jsp
//程序功能:
//创建日期: 2006-05-24 14:25:18
//创建人  : CrtHtml程序创建
//更新记录:
//更新人  : 
//更新日期:   2006-06-13 11:01:18
//更新原因/内容
%>
<!--用户校验类-->
<%
	String LoadFlag = "";
	if(request.getParameter("LoadFlag")!=null)
	{
		LoadFlag = request.getParameter("LoadFlag");
	}
%>
<script language="JavaScript">
	var d = new Date();
	var h = d.getYear();
	var m = d.getMonth(); 
	var day = d.getDate();  
	var Date1;       
	if(h<10){h = "0"+d.getYear();}  
	if(m<9){ m++; m = "0"+m;}
	else{m++;}
	if(day<10){day = "0"+d.getDate();}
	Date1 = h+"-"+m+"-"+day;
	
function initInpBox()
{ 
	try
	{                                   
	    fm.all('AreaCode').value = "";
	    fm.all('AreaName').value = "";
	    fm.all('LevelCode').value = "";
	    fm.all('LevelCode_ch').value = "";
	    fm.all('BusiTypeCode').value = "";
	    fm.all('BusiTypeCode_ch').value = ""; 
	    fm.all('CommunFixFlag').value = "";
	    fm.all('CommunFixFlag_ch').value = "";
	    fm.all('adminisortcode').value = "";
	    fm.all('adminisortcode_ch').value = "";
	    fm.all('Economelemencode').value = "";
	    fm.all('Economelemencode_ch').value = "";
	    fm.all('AssociateClass').value = "";
	    fm.all('AssociateClass_ch').value = "";
	    fm.all('MngCom').value = "";
	    fm.all('MngCom_ch').value = "";    
	}
	catch(ex)
	{
		alert("在LHHospitalSearchInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}
function initSelBox()
{  
	try{}
	catch(ex)
	{
		alert("在LHospitalSearchInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
	}
}                                        
function initForm()
{
	try
  	{
	    //initInpBox();
	    initSelBox();  
	    initServerGrid() ;
	    initDiseaseItemGrid();
	    initHealthQuesGrid();
	    initHealtItemGrid() ;
	    initLHSettingSlipQueryGrid() ;
	}
	catch(re)
	{
    	alert("LHospitalSearchInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initServerGrid() 
{
	var iArray = new Array();
    try 
    {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         				//列名
	    iArray[0][4]="station";         //列名
	    
	    iArray[1]=new Array(); 
		iArray[1][0]="服务事件编号";   
		iArray[1][1]="120px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
		iArray[1][5]="1|4"; 
		iArray[1][6]="1|0"; 
		iArray[1][18]="320";     //下拉框的宽度 
		
		iArray[2]=new Array(); 
		iArray[2][0]="服务事件名称";   
		iArray[2][1]="420px";   
		iArray[2][2]=500;        
		iArray[2][3]=1;
		
	    ServerGrid = new MulLineEnter( "fm" , "ServerGrid" ); 
	    //这些属性必须在loadMulLine前
	
	    ServerGrid.mulLineCount = 0;   
	    ServerGrid.displayTitle = 1;
	    ServerGrid.hiddenPlus = 0;
	    ServerGrid.hiddenSubtraction = 0;
	    ServerGrid.canSel = 0;
	    ServerGrid.canChk = 0;
	    //HealtItemGrid.selBoxEventFuncName = "showOne";
	
	    ServerGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{
    	alert(ex);
	}
}


function initDiseaseItemGrid() 
{
	var iArray = new Array();
	try 
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         		//列名
    	iArray[0][1]="30px";         		//列名
    	iArray[0][3]=0;         		//列名
    	 
    	iArray[1]=new Array(); 
		iArray[1][0]="ICD代码（起始）";   
		iArray[1][1]="200px";   
		iArray[1][2]=200;        
		iArray[1][3]=1;
		
		iArray[2]=new Array(); 
		iArray[2][0]="ICD代码（终止）";   
		iArray[2][1]="200px";   
		iArray[2][2]=200;        
		iArray[2][3]=1;
		        
		iArray[3]=new Array(); 
		iArray[3][0]="主要诊断标识";   
		iArray[3][1]="200px";   
		iArray[3][2]=400;        
		iArray[3][3]=2; 
		iArray[3][4]="IsMainDiagno";
        iArray[3][5]="3";     //引用代码对应第几列，'|'为分割符
        iArray[3][6]="1";     //上面的列中放置引用代码中第几位值
        //iArray[3][7]="aaa";	
        iArray[3][9]="诊断标志|len<=120";
        iArray[3][18]="170";
	   
    	DiseaseItemGrid	 = new MulLineEnter( "fm" , "DiseaseItemGrid" ); 
    	//这些属性必须在loadMulLine前
	    DiseaseItemGrid.mulLineCount = 0;   
	    DiseaseItemGrid.displayTitle = 1;
	    DiseaseItemGrid.hiddenPlus = 0;  			// +
	    DiseaseItemGrid.hiddenSubtraction = 0;// -
	    DiseaseItemGrid.canSel = 0;						// 单选框
	    DiseaseItemGrid.canChk = 0;						// 复选框
	    //DiseaseItemGrid.selBoxEventFuncName = "showOne";
	
	    DiseaseItemGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{
    	alert(ex);
  	}
}
function initHealtItemGrid() 
{
	var iArray = new Array();
    try 
    {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名 显示的名字
	    iArray[0][1]="30px";         		//列名 长度
	    iArray[0][3]=1;         				//列名 属性 0--只读  1--可写  2--背景可写  3--隐藏 
	    			
		iArray[1]=new Array();                                                    
		iArray[1][0]="诊疗项目代码";	//列名（第2列）
		iArray[1][1]="60px";				//列宽
		iArray[1][2]=10;       		    //列最大值
		iArray[1][3]=1;					//是否允许输入,1表示允许，0表示不允许
			
		iArray[2]=new Array();
		iArray[2][0]="诊疗项目名称";	//列名（第2列）
		iArray[2][1]="160px";  	  		//列宽
		iArray[2][2]=10;				//列最大值
		iArray[2][3]=2;					//是否允许输入,1表示允许，0表示不允许
		iArray[2][4]="lhmedicaitemname";
		iArray[2][5]="2|1|5";     		//引用代码对应第几列，'|'为分割符
		iArray[2][6]="0|1|2";			//上面的列中放置引用代码中第几位值
		//iArray[2][9]="诊疗项目名称<=120";
		iArray[2][15]="MedicaItemName";
		iArray[2][17]="1";  
		iArray[2][18]=400;
		iArray[2][19]="2" ;

		iArray[3]=new Array(); 
		iArray[3][0]="符号";   
		iArray[3][1]="20px";   
		iArray[3][2]=20;        
		iArray[3][3]=2;
		iArray[3][4]='loginsign';
		iArray[3][5]="3";     		//引用代码对应第几列，'|'为分割符
		iArray[3][6]="1";			//上面的列中放置引用代码中第几位值
		iArray[3][18]=80;			//上面的列中放置引用代码中第几位值				
		
		iArray[4]=new Array();
		iArray[4][0]="比较值";
		iArray[4][1]="80px";         
		iArray[4][2]=20;            
		iArray[4][3]=1;  
		
		iArray[5]=new Array();
		iArray[5][0]="标准单位";
		iArray[5][1]="0px";         
		iArray[5][2]=20;            
		iArray[5][3]=3; 
		
		iArray[6]=new Array();
		iArray[6][0]="正常值";
		iArray[6][1]="80px";         
		iArray[6][2]=20;            
		iArray[6][3]=1; 
		
		iArray[7]=new Array();
		iArray[7][0]="是否正常";
		iArray[7][1]="40px";         
		iArray[7][2]=20;            
		iArray[7][3]=2; 
		iArray[7][10]="Isnormal";
		iArray[7][11]= "0|^是|^否|";  	//虚拟数据源
    	iArray[7][14]= "是";  				//虚拟数据源
    	iArray[7][19]=1;    
		
	    HealtItemGrid = new MulLineEnter( "fm" , "HealtItemGrid" ); 
	    //这些属性必须在loadMulLine前
	
	    HealtItemGrid.mulLineCount = 0;   
	    HealtItemGrid.displayTitle = 1;
	    HealtItemGrid.hiddenPlus = 0;
	    HealtItemGrid.hiddenSubtraction = 0;
	    HealtItemGrid.canSel = 0;
	    HealtItemGrid.canChk = 0;
	    //HealthQuesGrid.selBoxEventFuncName = "showOne";
	
	    HealtItemGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{
    	alert(ex);
	}
}


function initLHSettingSlipQueryGrid() 
{
	var iArray = new Array();
    try 
    {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=1;         				//列名
	    iArray[0][4]="station";         //列名
	    
	    iArray[1]=new Array(); 
		iArray[1][0]="客户号";   
		iArray[1][1]="60px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
		
		iArray[2]=new Array(); 
		iArray[2][0]="客户姓名";   
		iArray[2][1]="60px";   
		iArray[2][2]=20;        
		iArray[2][3]=1;
			
		iArray[3]=new Array(); 
		iArray[3][0]="序号";   
		iArray[3][1]="60px";   
		iArray[3][2]=20;        
		iArray[3][3]=1;
				
		iArray[4]=new Array();
		iArray[4][0]="服务项目名称";
		iArray[4][1]="110px";         
		iArray[4][2]=20;            
		iArray[4][3]=1;  
		
		iArray[5]=new Array();
		iArray[5][0]="契约事件类型";
		iArray[5][1]="70px";         
		iArray[5][2]=20;            
		iArray[5][3]=1; 
		
		iArray[6]=new Array();
		iArray[6][0]="事件原始编号";
		iArray[6][1]="90px";         
		iArray[6][2]=20;            
		iArray[6][3]=1; 
		
		
		iArray[7]=new Array();
		iArray[7][0]="服务结束状态";
		iArray[7][1]="200px";         
		iArray[7][2]=20;            
		iArray[7][3]=1; 
		
		iArray[8]=new Array();
		iArray[8][0]="comid";
		iArray[8][1]="0px";         
		iArray[8][2]=20;            
		iArray[8][3]=3; 
		
		iArray[9]=new Array();
		iArray[9][0]="servitemno";
		iArray[9][1]="0px";
		iArray[9][2]=20;
		iArray[9][3]=3; 
		
		iArray[10]=new Array();
		iArray[10][0]="contno";
		iArray[10][1]="0px";
		iArray[10][2]=20;
		iArray[10][3]=3;
		
		iArray[11]=new Array();
		iArray[11][0]="servplanno";
		iArray[11][1]="0px";
		iArray[11][2]=20;
		iArray[11][3]=3;
		
		iArray[12]=new Array();
		iArray[12][0]="healthitemcode";
		iArray[12][1]="0px";
		iArray[12][2]=20;
		iArray[12][3]=3;
		
	    LHSettingSlipQueryGrid = new MulLineEnter( "fm" , "LHSettingSlipQueryGrid" ); 
	    //这些属性必须在loadMulLine前
	
	    LHSettingSlipQueryGrid.mulLineCount = 0;   
	    LHSettingSlipQueryGrid.displayTitle = 1;
	    LHSettingSlipQueryGrid.hiddenPlus = 1;
	    LHSettingSlipQueryGrid.hiddenSubtraction = 1;
	    LHSettingSlipQueryGrid.canSel = 0;
	    LHSettingSlipQueryGrid.canChk = 1;
	    //HealtItemGrid.selBoxEventFuncName = "showOne";
	
	    LHSettingSlipQueryGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{
    	alert(ex);
	}
}


function initHealthQuesGrid() 
{
	var iArray = new Array();
    try 
    {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=1;         				//列名
	    iArray[0][4]="station";         //列名
	    
	    iArray[1]=new Array(); 
		iArray[1][0]="健康问题代码";   
		iArray[1][1]="70px";   
		iArray[1][2]=20;        
		iArray[1][3]=1;
		iArray[1][5]="1|4"; 
		iArray[1][6]="1|0"; 
		iArray[1][18]="320";     //下拉框的宽度 
		
		iArray[2]=new Array(); 
		iArray[2][0]="健康问题内容";   
		iArray[2][1]="300px";   
		iArray[2][2]=20;        
		iArray[2][3]=1;

		iArray[3]=new Array();
		iArray[3][0]="符号";   
		iArray[3][1]="22px";   
		iArray[3][2]=20;        
		iArray[3][3]=2;
		iArray[3][4]='loginsign';
		iArray[3][5]="3";     		//引用代码对应第几列，'|'为分割符
		iArray[3][6]="1";			//上面的列中放置引用代码中第几位值
		iArray[3][14]="=";			//上面的列中放置引用代码中第几位值	
		iArray[3][18]=80;			//上面的列中放置引用代码中第几位值	
		       
		iArray[4]=new Array();
		iArray[4][0]="填写答案";
		iArray[4][1]="80px";         
		iArray[4][2]=20;            
		iArray[4][3]=1; 
		
		iArray[5]=new Array();
		iArray[5][0]="标准单位";
		iArray[5][1]="50px";         
		iArray[5][2]=20;            
		iArray[5][3]=1; 
		
	    HealthQuesGrid = new MulLineEnter( "fm" , "HealthQuesGrid" ); 
	    //这些属性必须在loadMulLine前
	
	    HealthQuesGrid.mulLineCount = 0;   
	    HealthQuesGrid.displayTitle = 1;
	    HealthQuesGrid.hiddenPlus = 0;
	    HealthQuesGrid.hiddenSubtraction = 0;
	    HealthQuesGrid.canSel = 0;
	    HealthQuesGrid.canChk = 0;
	    //HealtItemGrid.selBoxEventFuncName = "showOne";
	
	    HealthQuesGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{
    	alert(ex);
	}
}
</script>
