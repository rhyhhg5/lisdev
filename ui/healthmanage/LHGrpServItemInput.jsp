<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHGrpServItemInput.jsp
//程序功能：
//创建日期：2006-03-09 15:20:05
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
	<SCRIPT src="LHGrpServItemInput.js"></SCRIPT>
	<%@include file="LHGrpServItemInputInit.jsp"%>
</head>
<body  onload="initForm();passQuery();" >
  <form action="LHGrpServItemSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 团体服务项目表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHGrpServItem1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      团体客户号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GrpCustomerNo readonly>
    </TD>
    <TD  class= title>
      团体客户名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GrpName readonly>
    </TD>
    <TD  class= title>
      团体保单号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GrpContNo readonly>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      团体客户分级标识
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerClass readonly>
    </TD>
    <TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
	<TD  class= title>
    </TD>
    <TD  class= input>
    </TD>
  </TR>


</table>

    <table>
    	<tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHGrpItemGrid);">
    		</td>
			<td class= titleImg>
				团体服务计划信息列表
			</td>   
    </table>
    
	<Div id="divLHGrpItemGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHGrpItemGrid">
					</span> 
		    	</td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>


    </Div>
      <input type=hidden id="fmtransact" name="fmtransact">
      <input type=hidden id="fmAction" name="fmAction">
   
   	  <Input type=hidden name=ManageCom >
      <Input type=hidden name=Operator >
      <Input type=hidden name=MakeDate >
      <Input type=hidden name=MakeTime >
      <Input type=hidden name=ModifyDate >
      <Input type=hidden name=ModifyTime > 
             
      <Input  type=hidden name=GrpServPlanNo ><!--团体服务计划号码-->
      <Input type=hidden name=GrpServItemNo ><!--团体服务项目号码-->
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
		<script>
		//通过综合查询进入此页面的处理
		var turnPage2 = new turnPageClass(); 
		function passQuery()
		{ 	
			//进入此函数
			var GrpServPlanNo = "<%=request.getParameter("GrpServPlanNo")%>";	
			fm.all('queryButton').disabled = true;
			if(GrpServPlanNo!=null || GrpServPlanNo>0) 
			{
				var strSql = " select distinct GrpCustomerNo,GrpName,GrpContNo,ServPlanLevel,GrpServPlanNo"
										+" from LHGrpServPlan "
										+" where GrpServPlanNo = '"+GrpServPlanNo+"'"
										;
				var arrResult = easyExecSql(strSql);
				fm.all('GrpCustomerNo').value = arrResult[0][0];
				fm.all('GrpName').value = arrResult[0][1];
				fm.all('GrpContNo').value = arrResult[0][2];
				fm.all('CustomerClass').value = arrResult[0][3];
				fm.all('GrpServPlanNo').value = arrResult[0][4];
				
				var strSql2 = " select ( select distinct b.servitemname from LHHealthServItem b where b.ServItemCode = a.ServItemCode), "
							 +" a.ServItemSerial,a.OrgType,a.AttribFlag,a.ComID, "
							 +" case when a.ServPriceCode = '0' then '未设置' else (select distinct c.servpricename from LHServerItemPrice c where c.ServPriceCode=a.ServPriceCode) end, "
							 +" a.MakeDate,a.MakeTime,a.ServItemCode,a.GrpServItemNo,a.ServPriceCode,"
							 +" case a.OrgType when  '1' then '个人服务事件' when '2' then '集体服务事件' when '3'  then '费用结算事件' else '' end "
      				 +" from LHGrpServItem a"
							 +" where a.GrpServPlanNo  = '"+GrpServPlanNo+"'"
      						;
				turnPage2.queryModal(strSql2,LHGrpItemGrid); 

				if(LHGrpItemGrid.mulLineCount >0)
				{//如果设置了服务项目，则不能保存
					fm.all('saveButton').disabled = true;
				}
				
				var  sql_caseNoOper = " select count(*) from lhservitem where GrpServPlanNo = '"+GrpServPlanNo+"'" ;
				if(easyExecSql(sql_caseNoOper) > 0)
				{
					fm.all('saveButton').disabled = true;	
					fm.all('modifyButton').disabled = true;
					fm.all('querybutton').disabled = true;
					fm.all('deleteButton').disabled = true;
				}
			} 
			else 
			{
			  	  alert("LHMainCustomerHealth.js->没有此个人服务");
			}
		    
	}
		
	</script>
</html>
