<%
//程序名称：LHCustomInHospitalQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-18 09:08:27
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CustomerNo').value = "";
    fm.all('CustomerName').value = "";
   
  }
  catch(ex) {
    alert("在LHCustomInHospitalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHMainCustomHealthGrid();  
  }
  catch(re) {
    alert("LHCustomInHospitalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHCustomInHospitalGrid;
function initLHMainCustomHealthGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array(); 
	  iArray[1][0]="客户号码";   
	  iArray[1][1]="80px";   
	  iArray[1][2]=20;        
	  iArray[1][3]=0;
    
    iArray[2]=new Array(); 
	  iArray[2][0]="客户姓名";   
	  iArray[2][1]="40px";   
	  iArray[2][2]=20;        
	  iArray[2][3]=0;
    
    iArray[3]=new Array(); 
	  iArray[3][0]="记录时间";   
	  iArray[3][1]="60px";   
	  iArray[3][2]=20;        
	  iArray[3][3]=0;
	         
	  iArray[4]=new Array(); 
	  iArray[4][0]="血压舒张压";   
	  iArray[4][1]="50px";   
	  iArray[4][2]=20;        
	  iArray[4][3]=0;
	
	  iArray[5]=new Array(); 
	  iArray[5][0]="血压收缩压";   
	  iArray[5][1]="50px";   
	  iArray[5][2]=20;        
	  iArray[5][3]=0;
	  
	  iArray[6]=new Array(); 
	  iArray[6][0]="身高（m）";   
	  iArray[6][1]="120px";   
	  iArray[6][3]=0;
	  
	  iArray[7]=new Array(); 
	  iArray[7][0]="体重（kg）";   
	  iArray[7][1]="120px";   
    iArray[7][3]=0;
    
    iArray[8]=new Array(); 
	  iArray[8][0]="记录次数";   
	  iArray[8][1]="30px";   
    iArray[8][3]=1;
    
    iArray[9]=new Array(); 
	  iArray[9][0]="MakeDate";   
	  iArray[9][1]="0px";   
    iArray[9][3]=3;
    
    iArray[10]=new Array(); 
	  iArray[10][0]="MakeTime";   
	  iArray[10][1]="0px";   
    iArray[10][3]=3;
    
    
            
    LHMainCustomHealthGrid = new MulLineEnter( "fm" , "LHMainCustomHealthGrid" ); 
    //这些属性必须在loadMulLine前

    LHMainCustomHealthGrid.mulLineCount = 0;   
    LHMainCustomHealthGrid.displayTitle = 1;
    LHMainCustomHealthGrid.hiddenPlus = 1;
    LHMainCustomHealthGrid.hiddenSubtraction = 1;
    LHMainCustomHealthGrid.canSel = 1;
    LHMainCustomHealthGrid.canChk = 0;
    //LHCustomInHospitalGrid.selBoxEventFuncName = "showOne";

    LHMainCustomHealthGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomInHospitalGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
