<%
//程序名称：LHServCaseExecManageInit.jsp 
//程序功能：
//创建日期：2006-07-19 9:40:56
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类--> 
                          
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
	fm.all('ServCaseType').value = "";
	fm.all('ServCaseTypeName').value = "";
	fm.all('ComID').value = "";
	fm.all('ComIDName').value = "";
	fm.all('ServItemCode').value = "";
	fm.all('ServItemName').value = "";
  }
  catch(ex)
  {
    alert("在LHServCaseExecManageInit.jsp ->InitInpBox函数中发生异常:初始化界面错误!Qq");
  }      
}   
function initForm()
{
	try
	{
		initInpBox();
    	initLHCaseLaunchGrid();
	}
	catch(re)
	{
		alert("LHServCaseExecManageInit.jsp -->InitForm函数中发生异常:初始化界面错误!");
	}
}

var LHCaseLaunchGrid;
function initLHCaseLaunchGrid() 
{                               
	var iArray = new Array();
    
	try 
	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="40px";         		//列名
	    iArray[0][3]=1;         		//列名
	    iArray[0][4]="station";         		//列名
	    
	    iArray[1]=new Array(); 
		iArray[1][0]="服务任务编号";   
		iArray[1][1]="60px";   
		iArray[1][2]=20;        
		iArray[1][3]=0;	  
	    
	    iArray[2]=new Array(); 
		iArray[2][0]="服务任务名称";   
		iArray[2][1]="150px";   
		iArray[2][2]=20;        
		iArray[2][3]=2;
		iArray[2][4]="lhservtaskcode";
		iArray[2][5]="1|2";     //引用代码对应第几列，'|'为分割符
	    iArray[2][6]="0|1";     //上面的列中放置引用代码中第几位
		iArray[2][15]="ServTaskName";	    
	    iArray[2][17]="2"; 
	    iArray[2][19]="1" ;
		  
		iArray[3]=new Array(); 
		iArray[3][0]="服务任务状态";   
		iArray[3][1]="60px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		iArray[3][14]="0";
		
		iArray[4]=new Array(); 
		iArray[4][0]="操作员名称";   
		iArray[4][1]="50px";   
		iArray[4][2]=20;        
		iArray[4][3]=1;

		
		iArray[5]=new Array(); 
		iArray[5][0]="计划实施时间(天)";   
		iArray[5][1]="80px";   
		iArray[5][2]=20;        
		iArray[5][3]=1;
	
		iArray[6]=new Array(); 
		iArray[6][0]="服务任务说明";   
		iArray[6][1]="150px";   
		iArray[6][2]=20;        
		iArray[6][3]=1;
		
		iArray[7]=new Array(); 
		iArray[7][0]="MakeDate";   
		iArray[7][1]="0px";   
		iArray[7][2]=20;        
		iArray[7][3]=3;
		
		iArray[8]=new Array(); 
		iArray[8][0]="MakeTime";   
		iArray[8][1]="0px";   
		iArray[8][2]=20;        
		iArray[8][3]=3;
		                           
		iArray[9]=new Array(); 
		iArray[9][0]="SerialNo";   
		iArray[9][1]="0px";   
		iArray[9][2]=20;        
		iArray[9][3]=3;
		                       
		LHCaseLaunchGrid = new MulLineEnter( "fm" , "LHCaseLaunchGrid" );           
	    //这些属性必须在loadMulLine前                          
	                          
	    LHCaseLaunchGrid.mulLineCount = 0;                             
	    LHCaseLaunchGrid.displayTitle = 1;                          
	    LHCaseLaunchGrid.hiddenPlus = 0;                          
	    LHCaseLaunchGrid.hiddenSubtraction = 0;                          
	    LHCaseLaunchGrid.canSel = 0;                          
	    LHCaseLaunchGrid.canChk = 0;                          
	//  LHCaseLaunchGrid.selBoxEventFuncName = "showOne";                       
	//  LHCaseLaunchGrid.chkBoxEventFuncName = "showOne";                                                            
	    LHCaseLaunchGrid.loadMulLine(iArray);                                     
	    //这些操作必须在loadMulLine后面                                         
	    //LHCaseLaunchGrid.setRowColData(1,1,"asdf");                                   
  	}
	catch(ex) 
	{
		alert(ex);
	}
}
  
 
</script>
