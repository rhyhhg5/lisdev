<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServPlanUpSave.jsp
//程序功能：
//创建日期：2006-07-04 18:26:57
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  String inServPlanNo="";
  LHServPlanSchema tLHServPlanSchema   = new LHServPlanSchema();
  LHServPlanUpUI tLHServPlanUpUI   = new LHServPlanUpUI();
   
    
    
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");

  
    tLHServPlanSchema.setGrpServPlanNo(request.getParameter("GrpServPlanNo"));
    tLHServPlanSchema.setServPlanNo(request.getParameter("ServPlanNo"));                                                                    
    tLHServPlanSchema.setContNo(request.getParameter("ContNo"));                                                                    
    tLHServPlanSchema.setCustomerNo(request.getParameter("CustomerNo"));                                                                    
    tLHServPlanSchema.setName(request.getParameter("Name"));                                                                    
    tLHServPlanSchema.setServPlanCode(request.getParameter("ServPlanCode"));                                                                
    tLHServPlanSchema.setServPlanName(request.getParameter("ServPlanName"));                                                                
    tLHServPlanSchema.setServPlanLevel(request.getParameter("ServPlanLevel"));                                                              
    tLHServPlanSchema.setServPlayType(request.getParameter("ServPlayType"));                                                                
    tLHServPlanSchema.setComID(request.getParameter("ComID"));                                                                    
    tLHServPlanSchema.setStartDate(request.getParameter("StartDate"));                                                                    
    tLHServPlanSchema.setEndDate(request.getParameter("EndDate"));                                                                    
    tLHServPlanSchema.setServPrem(request.getParameter("ServPrem"));  
    tLHServPlanSchema.setCaseState(request.getParameter("CaseState"));					//MulLine的列存储数组
       
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLHServPlanSchema);  
    tVData.add(tG);
    tLHServPlanUpUI.submitData(tVData,transact);
  }
    catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
 
  if (FlagStr=="")
  {
    tError = tLHServPlanUpUI.mErrors;
    //System.out.println("FlagStrFlagStrFlagStrF  "+tError);
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
  
  if ( FlagStr.equals("Success") && "INSERT||MAIN".equals(transact) )
  {
    
        VData res = tLHServPlanUpUI.getResult();
        LHServPlanSchema mLHServPlanSchema = new LHServPlanSchema();
        mLHServPlanSchema.setSchema((LHServPlanSchema) res.getObjectByObjectName("LHServPlanSchema",0));
        inServPlanNo = (String)mLHServPlanSchema.getServPlanNo();
  }
  
%>                      
<%=Content%>
<html>
<script language="javascript">
		<%if(inServPlanNo.equals(""))
		{%>
			//parent.fraInterface.fm.all("ServPlanNo").value="";
       <%}
       else
       {%>	
	 		parent.fraInterface.fm.all("ServPlanNo").value = "<%=inServPlanNo%>";
	  <%}%>	
		parent.fraInterface.afterSubmitUp("<%=FlagStr%>","<%=Content%>");
</script>
</html>
