var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
 	var ServCaseType="";//契约事件类型 
	if(fm.all('ServCaseType').value != "")
  {
			ServCaseType =" and a.ServPlanNo in ( select distinct ServPlanNo from  lhservplan  where  ServPlayType = '"+fm.all('ServCaseType').value+"')";	
  }	 
 	var Casestate="";//契约事件状态 
	if(fm.all('CaseStateName').value != "")
  {
			Casestate =" and a.ServPlanNo in ( select distinct ServPlanNo from  lhservplan  where  Casestate = '"+fm.all('CaseStateName').value+"')";	
  }	
	var LEN = manageCom.length;
	if(LEN == 8){LEN = 4; }   
	var strSql = "select a.servitemcode, (select c.servitemname from LHHealthServItem c where a.servitemcode = c.servitemcode), "
	          +" a.servitemno, a.servitemtype,"
	          +" (case a.ServCaseType when  '1' then  '个人服务事件' when '2' then  '集体服务事件' else '无' end),"
	          +" a.comid, a.servpricecode,  "
	          +" (select b.servpricename from lhserveritemprice b where  b.servpricecode = a.servpricecode), "
	          +" a.ServCaseType,a.ServPlanNo,a.contno "
	          +" from lhservitem a "
			  +" where a.managecom like '"+manageCom.substring(0,LEN)+"%%'"
			  + getWherePart("a.ContNo", "ContNo", "like")
			  + getWherePart("a.CustomerNo", "CustomerNo" )
			  + getWherePart("a.ComID", "ComID" )
			  + ServCaseType
			  + Casestate
  			;
 //alert(strSql);
//  alert(easyExecSql(strSql));
	turnPage.pageLineNum = 20;
	turnPage.queryModal(strSql, LHServPlanGrid);
}
function returnParent()
{
	var arrReturn = new Array();
	var tSel = LHServPlanGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}
	else
	{
		try
		{	
			arrReturn = getQueryResult();
			top.opener.afterQuery0( arrReturn[0][9] );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHServPlanGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	arrSelected = new Array();
	
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = LHServPlanGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	return arrSelected;
}
function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	
	{  
	  var newWindow = window.open("../sys/LDPersonQuery.html");	  
	}
	if(fm.all('CustomerNo').value != "")	 
	{
		    var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		    var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	      var arrResult = easyExecSql(strSql);
	      if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      fm.CustomerName.value = arrResult[0][1];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
   }
   else
   {
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
   }
	}	
}

function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}