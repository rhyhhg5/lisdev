<%
//程序名称：LHHosptalSearchInit.jsp
//程序功能：
//创建日期：2006-05-24 14:25:18
//创建人  ：CrtHtml程序创建
//更新记录：
//更新人 :郭丽颖
//更新日期 :   2006-06-13 11:01:18
//更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<%
		String LoadFlag = "";
		if(request.getParameter("LoadFlag")!=null)
		{
			LoadFlag = request.getParameter("LoadFlag");
		}
%>
<script language="JavaScript">
	
	var d = new Date();
	var h = d.getYear();
	var m = d.getMonth(); 
	var day = d.getDate();  
	var Date1;       
	if(h<10){h = "0"+d.getYear();}  
	if(m<9){ m++; m = "0"+m;}
	else{m++;}
	if(day<10){day = "0"+d.getDate();}
	Date1 = h+"-"+m+"-"+day;
	
function initInpBox()
{ 
  try
  {                                   
    fm.all('HospitalType').value = "";
    fm.all('HospitalType_ch').value = "";
    fm.all('HospitCode').value = "";
    fm.all('HospitCode').readOnly =true;
    fm.all('HospitName').value = "";
    fm.all('AreaCode').value = "";
    fm.all('AreaName').value = "";
    fm.all('LevelCode').value = "";
    fm.all('LevelCode_ch').value = "";
    fm.all('BusiTypeCode').value = "";
    fm.all('BusiTypeCode_ch').value = ""; 
    fm.all('CommunFixFlag').value = "";
    fm.all('CommunFixFlag_ch').value = "";
    fm.all('adminisortcode').value = "";
    fm.all('adminisortcode_ch').value = "";
    fm.all('Economelemencode').value = "";
    fm.all('Economelemencode_ch').value = "";
    fm.all('AssociateClass').value = "";
    fm.all('AssociateClass_ch').value = "";
    fm.all('MngCom').value = "";
    fm.all('MngCom_ch').value = "";    
  }
  catch(ex)
  {
    alert("在LHHospitalSearchInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LHospitalSearchInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{

  try
  {
    initInpBox();
    initSelBox();  
    initHospitalComGrid();
    initLHHighPartGrid();
  }
  catch(re)
  {
    alert("LHospitalSearchInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initHospitalComGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="50px";         		//列名
    iArray[0][3]=0;         		//列名
     
    iArray[1]=new Array(); 
		iArray[1][0]="医疗机构代码";   
		iArray[1][1]="110px";   
		iArray[1][2]=60;        
		iArray[1][3]=1;
		
		iArray[2]=new Array(); 
		iArray[2][0]="医疗机构名称";   
		iArray[2][1]="250px";   
		iArray[2][2]=500;        
		iArray[2][3]=1;
	            
	  iArray[3]=new Array(); 
		iArray[3][0]="合同编号/名称";   
		iArray[3][1]="150px";   
		iArray[3][2]=500;        
		iArray[3][3]=1; 
	            
		
    HospitalComGrid = new MulLineEnter( "fm" , "HospitalComGrid" ); 
    //这些属性必须在loadMulLine前

    HospitalComGrid.mulLineCount = 0;   
    HospitalComGrid.displayTitle = 1;
    HospitalComGrid.hiddenPlus = 1;
    HospitalComGrid.hiddenSubtraction = 0;
    HospitalComGrid.canSel = 1;
    HospitalComGrid.canChk = 0;
    //LDDiseaseGrid.selBoxEventFuncName = "showOne";

    HospitalComGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
  }
  catch(ex) {
    alert(ex);
  }
}
function initLHHighPartGrid() 
{                               
	var iArray = new Array();
    
    try 
    {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         				//列名
	    iArray[0][4]="station";         //列名
	    
	    iArray[1]=new Array(); 
		  iArray[1][0]="检索项目";   
		  iArray[1][1]="220px";   
		  iArray[1][2]=20;        
		  iArray[1][3]=1;
		  //iArray[1][4]='searchesitem';
		  iArray[1][14]="费用总额(元)"; 
		  iArray[1][5]="1|4"; 
		  iArray[1][6]="1|0"; 
		  iArray[1][18]="320";     //下拉框的宽度 
		  
		  iArray[2]=new Array(); 
		  iArray[2][0]="逻辑判断符号";   
		  iArray[2][1]="160px";   
		  iArray[2][2]=20;        
		  iArray[2][3]=2;
		  iArray[2][4]='loginsign';
		  iArray[2][5]="2|5"; 
		  iArray[2][6]="1|0"; 
				
		  iArray[3]=new Array(); 
		  iArray[3][0]="比较值";   
		  iArray[3][1]="110px";   
		  iArray[3][2]=20;        
		  iArray[3][3]=1;
		  iArray[3][14]="0";
		  		
		  iArray[4]=new Array();
		  iArray[4][0]="searchesitem";
		  iArray[4][1]="0px";         
		  iArray[4][2]=20;            
		  iArray[4][3]=3;  
		  iArray[4][14]="01";  
		  
		  iArray[5]=new Array();
		  iArray[5][0]="loginsign";
		  iArray[5][1]="0px";         
		  iArray[5][2]=20;            
		  iArray[5][3]=3; 
		
	    LHHighPartGrid = new MulLineEnter( "fm" , "LHHighPartGrid" ); 
	    //这些属性必须在loadMulLine前
	
	    LHHighPartGrid.mulLineCount = 0;   
	    LHHighPartGrid.displayTitle = 1;
	    LHHighPartGrid.hiddenPlus = 0;
	    LHHighPartGrid.hiddenSubtraction = 0;
	    LHHighPartGrid.canSel = 0;
	    LHHighPartGrid.canChk = 0;
	    //LHHighPartGrid.selBoxEventFuncName = "showOne";
	
	    LHHighPartGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{
    	alert(ex);
  }
}
</script>
