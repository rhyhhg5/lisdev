<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-05-19 15:05:48
//创建人  St.GN
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-01 10:38:48
// 更新原因/内容: 插入新的字段
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHRespDefiInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHRespDefiInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHRespDefiSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHRespDefi1);">
    		</td>
    		<td class= titleImg>
        	 合同责任项目定义
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHRespDefi1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      合同责任项目代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DutyItemCode elementtype=nacessary verify = "责任项目代码|notnull&len<=20">
    </TD>
    <TD  class= title>
      合同责任项目名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DutyItemName verify = "责任项目名称|len<=50">
    </TD>
    <TD  class= title>
      合同项目责任类型
    </TD>
    <TD  class= input>
      <Input class=codeno name=DutyItemTye verify="项目责任类型|len<=10" ondblClick="showCodeList('hmdeftype',[this,DutyItemTye_ch],[0,1]);" onkeyup="if(event.keyCode ==13) return showCodeList('hmdeftype',[this,DutyItemTye_ch],[1,0],null,fm.DutyItemTye_ch.value,'DutyItemTye_ch')";  onkeydown="return showCodeListKey('hmdeftype',[this,DutyItemTye_ch],[1,0],null,fm.DutyItemTye_ch.value,'DutyItemTye_ch'); codeClear(DutyItemTye_ch,DutyItemTye);" ><Input class= 'codename' name=DutyItemTye_ch >
    </TD>
  </TR>
  <TR  class= common>
  	<TD  class= title>
      合同责任项目一般性释义
    </TD>
    <TD  class= input colSpan=5>
      <textarea class= 'common'  name=DutyItemExplain verify="责任项目一般性释义|len<=5000" style="width:683px;height:50px" ></textarea>    
    </TD>
  </TR>
  
</table>
    </Div>
    
    
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction"   name="fmAction">
    <input type=hidden name="Operator" value="hm">
    <input type=hidden name="MakeDate">
    <input type=hidden name="MakeTime">
    <input type=hidden name="ModifyDate">
    <input type=hidden name="ModifyTime">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
