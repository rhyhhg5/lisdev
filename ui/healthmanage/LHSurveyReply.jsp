<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLSurveyReply.js"></SCRIPT>
   <%@include file="LLSurveyReplyInit.jsp"%>
 </head>

 <body  onload="initForm();" >
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">
    
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         调查工作
         </TD>
       </TR>
      </table>

  
   <table  class= common>
   <TR  class= common8>
<TD  class= title8>出险客户号</TD><TD  class= input8><input class= common name="CustomerNo"></TD>
<TD  class= title8>客户名称</TD><TD  class= input8><input class= common name="CustomerName"></TD>
<TD  class= title8>客户性别</TD><TD  class= input8><input class= common name="Sex"></TD>
</TR>
   <TR  class= common8>
<TD  class= title8>调查类型</TD><TD  class= input8><input class= common name="SurveyType"></TD>
<TD  class= title8>调查类别</TD><TD  class= input8><input class= common name="SurveyClass"></TD>
<TD  class= title8>提起日期</TD><TD  class= input8><input class= common name="SurveyStartDate"></TD>
</TR>
<TR  class= common8>
<TD  class= title8>提起原因</TD><TD  class= input8><input class= common name="SurveyRCode"></TD>
<TD  class= title8>调查地点</TD><TD  class= input8><input class= common name="SurveySite"></TD>
<TD  class= title8>事件号码</TD><TD  class= input8><input class= common name="SubRptNo"></TD>
</TR>


<TR  class= common>
	<TD  class= title colspan="6">调查内容</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">
	    <textarea name="Content" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>
</table>
<div id="div1" style="display: 'none'">
	<table class=common>
		<TR  class= common8>
			<TD  class= title8>
				审核人
				</TD>
			<TD  class= input8>
				<input class= common name="Confer">
			</TD>
			<TD  class= title8>
				审核时间
			</TD>
			<TD  class= input8>
				<input class= 'coolDatePicker' dateFormat="short" name="ConfDate">
			</TD>
		</TR>
		<TR  class= common>
			<TD  class= title colspan="6">
				审核意见
			</TD>
		</TR>
		<TR  class= common>
			<TD  class= input colspan="6">
			    <textarea name="ConfNote" cols="100%" rows="3"  class="common"></textarea>
			</TD>
		</TR>
	</table>
</div>
<table class=common>
<TR  class= common>
	<TD  class= title colspan="6">回复内容</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">
	    <textarea name="result" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>

</table>
 <Div align="left">
  <input name="AskIn" style="display:''"  class=cssButton type=button value="回复保存" onclick="SurveyReplySave()">
  	<input name="Return" style="display:''"  class=cssButton type=button value="返 回" onclick="CloseWindows()">    
  </div>
  
          
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="SurveyNo" >
     <Input type="hidden" class= common name="OtherNo" >
     <Input type="hidden" class= common name="OtherNoType" >
     <Input type="hidden" class= common name="StartPhase" >
     <Input type="hidden" class= common name="Name" >
     <Input type="hidden" class= common name="SurveyRDesc" >
     <Input type="hidden" class= common name="SurveyEndDate" >
     <Input type="hidden" class= common name="SurveyFlag" >
     <Input type="hidden" class= common name="SurveyOperator" >
     <Input type="hidden" class= common name="StartMan" >
     <Input type="hidden" class= common name="MngCom" >
     <Input type="hidden" class= common name="Operator" >
     <Input type="hidden" class= common name="MakeDate" >
     <Input type="hidden" class= common name="MakeTime" >
     <Input type="hidden" class= common name="ModifyDate" >
     <Input type="hidden" class= common name="ModifyTime" >
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
