<%
//程序名称：LHCustomInHospitalInput.jsp
//程序功能：
//创建日期：2005-01-18 09:08:27
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     String CustomerNo = request.getParameter("CustomerNo");
     String InHospitNo = request.getParameter("InHospitNo");
     System.out.println("#######"+CustomerNo);
     System.out.println("###########"+InHospitNo);
%>  
<!--用户校验类-->

  <script language="JavaScript">
  var d = new Date();
	var h = d.getYear();
	var m = d.getMonth(); 
	var day = d.getDate();  
	var Date1;       
	if(h<10){h = "0"+d.getYear();}  
	if(m<9){ m++; m = "0"+m;}
	else{m++;}
	if(day<10){day = "0"+d.getDate();}
	Date1 = h+"-"+m+"-"+day;
  
  function initTestInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;        //列最大值
      iArray[0][3]=0;   			//1表示允许该列输入，0表示只读且不响应Tab键
                    				  // 2 表示为容许输入且颜色加深.
      iArray[1]=new Array();
      iArray[1][0]="检查项目名称";  //列名（第2列）
      iArray[1][1]="80px";  	  		//列宽
      iArray[1][2]=10;        			//列最大值
      iArray[1][3]=2;          			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="lhmedicaitemname";
      iArray[1][5]="1|2|5";     		//引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1|2";    		  //上面的列中放置引用代码中第几位值
      iArray[1][9]="检查项目名称|len<=120";
      iArray[1][15]="MedicaItemName";
      iArray[1][17]="1";  
      iArray[1][19]="1" ;
      
      iArray[2]=new Array();                                                    
      iArray[2][0]="检查项目代码";  //列名（第2列）
      iArray[2][1]="0px";  	  		  //列宽
      iArray[2][2]=10;       		    //列最大值
      iArray[2][3]=3;         			//是否允许输入,1表示允许，0表示不允许
              
             
      iArray[3]=new Array();
      iArray[3][0]="检查时间";  //列名（第2列）
      iArray[3][1]="40px";  	  //列宽
      iArray[3][2]=10;        	//列最大值
      iArray[3][3]=1;           //是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="检查时间|len<=120";
      iArray[3][14]=Date1;
      
      iArray[4]=new Array();
      iArray[4][0]="检查结果";  //列名（第2列）
      iArray[4][1]="110px";  	  //列宽
      iArray[4][2]=130;        	//列最大值
      iArray[4][3]=1;     
      iArray[4][7]="inputTestResult";//你写的JS函数名，不加扩号
      
      iArray[5]=new Array();
      iArray[5][0]="检查单位";  //列名（第2列）
      iArray[5][1]="30px";  	  //列宽
      iArray[5][2]=40;        	//列最大值
      iArray[5][3]=0;     
      iArray[5][9]="检查单位|len<=30";
      
      iArray[6]=new Array();
      iArray[6][0]="流水号";  				//列名（第2列）
      iArray[6][1]="0px";  	  				//列宽
      iArray[6][2]=10;        				//列最大值
      iArray[6][3]=3; 
      
      iArray[7]=new Array();                   			
		  iArray[7][0]="是否异常";  			//列名（第2列）                                                 
    	iArray[7][1]="40px";  	  		  		//列宽                                                    
    	iArray[7][2]=1;        					//列最大值                                                  
    	iArray[7][3]=2;
    	iArray[7][10]="Isnormal";
    	iArray[7][11]= "0|^正常|1^异常|2";  	//虚拟数据源
    	iArray[7][14]= "正常";  				//虚拟数据源
    	iArray[7][19]=1;               			//强制刷新
     
     
     
     
      
           	//生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
      			TestInfoGrid= new MulLineEnter( "fm" , "TestInfoGrid" ); 
      			//设置属性区 (需要其它特性，在此设置其它属性)
            TestInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            TestInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
           	// TestInfoGrid.canSel =1; 
          	//对象初始化区：调用对象初始化方法，属性必须在此前设置
      TestInfoGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
    
</script>
 <script language="JavaScript">
  function initOPSInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;      //列最大值
      iArray[0][3]=0;   //1表示允许该列输入，0表示只读且不响应Tab键
                     // 2 表示为容许输入且颜色加深.
      
      iArray[1]=new Array();
      iArray[1][0]="手术名称";  //列名（第2列）
      iArray[1][1]="80px";  	  //列宽
      iArray[1][2]=10;        //列最大值
      iArray[1][3]=2;          //是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="lhICDOPSName";
      iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[1][9]="手术名称|len<=100";
      iArray[1][15]="ICDOPSName";
      iArray[1][17]="1";  
      iArray[1][19]="1" ;
      
      iArray[2]=new Array();
      iArray[2][0]="手术代码";  //列名（第2列）
      iArray[2][1]="0px";  	  //列宽
      iArray[2][2]=10;        //列最大值
      iArray[2][3]=3;          //是否允许输入,1表示允许，0表示不允许
         //后续可以添加N列，如上设置
      iArray[3]=new Array();
      iArray[3][0]="手术医师代码";  //列名（第2列）
      iArray[3][1]="0px";  	  //列宽
      iArray[3][2]=10;        //列最大值
      iArray[3][3]=3;   
             //是否允许输入,1表示允许，0表示不允许
      iArray[4]=new Array();
      iArray[4][0]="医师姓名";  //列名（第2列）
      iArray[4][1]="70px";  	  //列宽
      iArray[4][2]=10;        //列最大值
      iArray[4][3]=2;          //是否允许输入,1表示允许，0表示不允许
      iArray[4][4]="lhDoctName";
      iArray[4][5]="4|3";     //引用代码对应第几列，'|'为分割符
      iArray[4][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[4][9]="手术医师姓名|len<=20";
      iArray[4][15]="DoctName";
      iArray[4][17]="4";  
      iArray[4][19]="1" ;         //是否允许输入,1表示允许，0表示不允许
             //是否允许输入,1表示允许，0表示不允许
      
         
      iArray[5]=new Array();
      iArray[5][0]="手术时间";  //列名（第2列）
      iArray[5][1]="50px";  	  //列宽
      iArray[5][2]=10;        //列最大值
      iArray[5][3]=1;          //是否允许输入,1表示允许，0表示不允许
//      iArray[5][7]="getCurrentTime2";
      iArray[5][9]="手术时间|len<=120";
      iArray[5][14]=Date1;
      
      iArray[6]=new Array();
      iArray[6][0]="手术情况";  //列名（第2列）
      iArray[6][1]="100px";  	  //列宽
      iArray[6][2]=10;        //列最大值
      iArray[6][3]=1;          //是否允许输入,1表示允许，0表示不允许
      iArray[6][7]="inputOPSResult";//你写的JS函数名，不加扩号
      
      iArray[7]=new Array();
      iArray[7][0]="流水号";  //列名（第2列）
      iArray[7][1]="0px";  	  //列宽
      iArray[7][2]=10;        //列最大值
      iArray[7][3]=3;
      
      //iArray[7]=new Array();
      //iArray[7][0]="手术费用(元)";  //列名（第2列）
      //iArray[7][1]="25px";  	  //列宽
      //iArray[7][2]=10;        //列最大值
      //iArray[7][3]=1;
      //iArray[7][9]="手术费用(元)|num&len<=12";
      
     
      

      
               //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
      OPSInfoGrid= new MulLineEnter( "fm" , "OPSInfoGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            OPSInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            OPSInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //OPSInfoGrid.canSel =1; 
           
            
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      OPSInfoGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script>
 <script language="JavaScript">
  function initOtherCureInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;      //列最大值
      iArray[0][3]=0;   //1表示允许该列输入，0表示只读且不响应Tab键
                     // 2 表示为容许输入且颜色加深.
      iArray[1]=new Array();
      iArray[1][0]="治疗项目名称";  //列名（第2列）
      iArray[1][1]="90px";  	  //列宽
      iArray[1][2]=10;        //列最大值
      iArray[1][3]=2;          //是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="lhmedicaitemname";
      iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[1][9]="治疗项目名称|len<=120";
      iArray[1][15]="MedicaItemName";
      iArray[1][17]="1";  
      iArray[1][19]="1" ;
        
      iArray[2]=new Array();
      iArray[2][0]="治疗项目代码";  //列名（第2列）
      iArray[2][1]="0px";  	  //列宽
      iArray[2][2]=10;        //列最大值
      iArray[2][3]=3;          //是否允许输入,1表示允许，0表示不允许
         //后续可以添加N列，如上设置  
     
      
      iArray[3]=new Array();                                                    
      iArray[3][0]="治疗医师代码";  //列名（第2列）                                 
      iArray[3][1]="0px";  	  //列宽                                          
      iArray[3][2]=10;        //列最大值                                        
      iArray[3][3]=3;          //是否允许输入,1表示允许，0表示不允许            
             
      iArray[4]=new Array();
      iArray[4][0]="医师姓名";  //列名（第2列）
      iArray[4][1]="50px";  	  //列宽
      iArray[4][2]=10;        //列最大值
      iArray[4][3]=2;          //是否允许输入,1表示允许，0表示不允许
      iArray[4][4]="lhDoctName";
      iArray[4][5]="4|3";     //引用代码对应第几列，'|'为分割符
      iArray[4][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[4][9]="主检医师姓名|len<=20";
      iArray[4][15]="DoctName";
      iArray[4][17]="4";  
      iArray[4][19]="1" ;         //是否允许输入,1表示允许，0表示不允许
      
         
          
      iArray[5]=new Array();
      iArray[5][0]="治疗时间";  //列名（第2列）
      iArray[5][1]="30px";  	  //列宽
      iArray[5][2]=10;        //列最大值
      iArray[5][3]=1;          //是否允许输入,1表示允许，0表示不允许
//      iArray[5][7]="getCurrentTime3";
      iArray[5][9]="治疗时间|len<=120";
      iArray[5][14]=Date1;
     
      iArray[6]=new Array();
      iArray[6][0]="治疗结果";  //列名（第2列）
      iArray[6][1]="100px";  	  //列宽
      iArray[6][2]=10;        //列最大值
      iArray[6][3]=1;     
      iArray[6][7]="inputCureResult";//你写的JS函数名，不加扩号
      
      iArray[7]=new Array();
      iArray[7][0]="治疗费用(元)";  //列名（第2列）
      iArray[7][1]="30px";  	  //列宽
      iArray[7][2]=10;        //列最大值
      iArray[7][3]=1;
      
      iArray[8]=new Array();
      iArray[8][0]="流水号";  //列名（第2列）
      iArray[8][1]="0px";  	  //列宽
      iArray[8][2]=10;        //列最大值
      iArray[8][3]=3;
      
               //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
      OtherCureInfoGrid= new MulLineEnter( "fm" , "OtherCureInfoGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            OtherCureInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            OtherCureInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //OtherCureInfoGrid.canSel =1; 
           
            
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      OtherCureInfoGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script>          
<script language="JavaScript">
  function initDiagnoInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;      //列最大值
      iArray[0][3]=0;   //1表示允许该列输入，0表示只读且不响应Tab键
                     // 2 表示为容许输入且颜色加深.
                     
      iArray[1]=new Array();
      iArray[1][0]="诊断疾病代码";  //列名（第2列）
      iArray[1][1]="70px";  	  //列宽
      iArray[1][2]=10;        //列最大值
      iArray[1][3]=0;          //是否允许输入,1表示允许，0表示不允许
   
      iArray[2]=new Array();
      iArray[2][0]="诊断疾病名称";  //列名（第2列）
      iArray[2][1]="180px";  	  //列宽
      iArray[2][2]=10;        //列最大值
      iArray[2][3]=2;    
      iArray[2][4]="lhdisease";      //是否允许输入,1表示允许，0表示不允许            
      iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
      iArray[2][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[2][9]="疾病名称|len<=120";
      iArray[2][15]="ICDName";
      iArray[2][17]="2";     
      iArray[2][18]="430";
      iArray[2][19]="1" ;

      
      iArray[3]=new Array();
      iArray[3][0]="诊断标志代码";  	
      iArray[3][1]="0px";  	
      iArray[3][2]=10;      
      iArray[3][3]=3;  
      iArray[3][14]="2";     
      
      iArray[4]=new Array();
      iArray[4][0]="诊断标志";  	//列名（序号列，第1列）
      iArray[4][1]="50px";  	//列宽
      iArray[4][2]=10;      //列最大值
      iArray[4][3]=2;
      iArray[4][4]="IsMainDiagno";
      iArray[4][5]="4|3";     //引用代码对应第几列，'|'为分割符
      iArray[4][6]="0|1";     //上面的列中放置引用代码中第几位值
      iArray[4][7]="aaa";	
      iArray[4][9]="诊断标志|len<=120";
      iArray[4][18]="170";
      
      iArray[5]=new Array();
      iArray[5][0]="流水号";  	//列名（序号列，第1列）
      iArray[5][1]="0px";  	//列宽
      iArray[5][2]=10;      //列最大值
      iArray[5][3]=3;
     	
      
               //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
      DiagnoInfoGrid= new MulLineEnter( "fm" , "DiagnoInfoGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            DiagnoInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            DiagnoInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //DiagnoInfoGrid.canSel =1; 
           
            
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      DiagnoInfoGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script> 
<script language="JavaScript">
 var strsqlinit = null;
 function afterCodeSelect (strCodeName,Field)
 {
 		 if( strCodeName=="hminhospitalmode")
		 { 
	 		 if(fm.all('InHospitModeCode').value=="2") 
	     {
	     		  fm.CureEffect.style.display='';     
	     		  fm.InHospitalDays.style.display=''; 
	     		  strsqlinit=" code like #2%# and ";initFeeInfoGrid();return;
	     }
	     else(fm.all('InHospitModeCode').value=="1" )
	     { 
	     			fm.CureEffect.style.display='none';    
	          fm.InHospitalDays.style.display='none';
	     			strsqlinit=" code like #1%# and ";initFeeInfoGrid();
	     }
  	 }                                           
}
 
  
  
  
  
  
  function initFeeInfoGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
 
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;      //列最大值
      iArray[0][3]=0;   //1表示允许该列输入，0表示只读且不响应Tab键
                     // 2 表示为容许输入且颜色加深.
      	
      iArray[1]=new Array();
      iArray[1][0]="分费用项目名称";  	//列名（序号列，第1列）
      iArray[1][1]="120px";  	//列宽
      iArray[1][2]=10;      //列最大值
      iArray[1][3]=2;
      iArray[1][4]="lhfeeitemtype";
      iArray[1][5]="1|2";     //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";    //上面的列中放置引用代码中第几位值
      iArray[1][9]="分费用项目名称|len<=120";     
      iArray[1][15]="1";       
      iArray[1][16]=strsqlinit;
      iArray[1][18]="300";
     
      iArray[2]=new Array();
      iArray[2][0]="分费用项目代码";  	
      iArray[2][1]="0px";  	
      iArray[2][2]=10;      
      iArray[2][3]=3; 
                           
      iArray[3]=new Array();
      iArray[3][0]="分项目费用（元）";  //列名（第2列）
      iArray[3][1]="90px";  	  //列宽
      iArray[3][2]=10;        //列最大值
      iArray[3][3]=1;          //是否允许输入,1表示允许，0表示不允许
      iArray[3][9]="分项目费用（元）|num&len<=12";
      
      iArray[4]=new Array();
      iArray[4][0]="分费用比例";  //列名（第2列）
      iArray[4][1]="90px";  	  //列宽
      iArray[4][2]=10;        //列最大值
      iArray[4][3]=0;          //是否允许输入,1表示允许，0表示不允许
      iArray[4][7]="countProportion"; 
      
      iArray[5]=new Array();
      iArray[5][0]="流水号";  //列名（第2列）
      iArray[5][1]="0px";  	  //列宽
      iArray[5][2]=10;        //列最大值
      iArray[5][3]=3;          //是否允许输入,1表示允许，0表示不允许
     
     
      
               //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
      FeeInfoGrid= new MulLineEnter( "fm" , "FeeInfoGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            FeeInfoGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            FeeInfoGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //FeeInfoGrid.canSel =1; 
           
            
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      FeeInfoGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script>                  
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('CustomerNo').value = "";
    fm.all('CustomerName').readOnly=true;
    fm.all('CustomerName').value = ""
    
    fm.all('InHospitNo').value = "";
//    fm.all('InHospitNo').readOnly=true;MainItem
    fm.all('MainItem').value = "";
    fm.all('InHospitMode').value = "";
    fm.all('InHospitDate').value = "";
    fm.CureEffect.style.display='none';
		fm.InHospitalDays.style.display='none';
		fm.all('CureEffect').value="";
		
		fm.all('InHospitalDays').value="";
		fm.all('iscomefromquery').value="0";
		
    fm.all('HospitCode').value = "";
//    fm.all('HospitName').readOnly = true;
    fm.all('HospitName').value = "";
    fm.all('MainCureModeCode').value = "";
    fm.all('MainCureMode').value="";
    fm.all('DoctName').value = "";
    //fm.all('DiseasCode').value = "";
    //fm.all('DiseasName').readOnly =true ;
    //fm.all('DiagnoCircs').value = "";
    //fm.all('CureEffectCode').value = "";
    //fm.all('CureEffect').value = "";
    fm.all('TotalFee').value = "";
   // fm.all('TestFee').value = "";
   // fm.all('OPSFee').value = "";
   // fm.all('MedicaFee').value = "";
  //  fm.all('BedFee').value = "";
   // fm.all('OtherFee').value = "";
   

			
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHCustomInHospitalInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LHCustomInHospitalInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();  
    initTestInfoGrid();
    initOPSInfoGrid();
    initOtherCureInfoGrid();
    initDiagnoInfoGrid();
    initFeeInfoGrid();
    var flag = "<%=CustomerNo%>";
    //alert(flag);
    if(flag!=""&&flag!="null"&&flag!=null)//团体服务
    {
    	  fm.all('querybutton').disabled=true;
        fm.all('deleteButton').disabled=true;
        fm.all('modifyButton').disabled=true;
        fm.all('saveButton').disabled=true;
        getCustomerNo();
    }    
  }
  catch(re)
  {
    alert("LHCustomInHospitalInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function getCustomerNo()
{
	//var arrResult = new Array();

	//if( arrQueryResult != null )
	//{
	
		try
		{
			var strSql=" select a.CustomerNo,b.name, a.InHospitNo, a.HospitCode,'',a.InHospitDate,"
      					+" (select distinct c.DoctNo from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"
      					+" '',a.InHospitMode,(select distinct c.MainCureMode from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),'',"
//      					+" (select distinct c.DiagnoseNo from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"     					
      					+" (select distinct c.CureEffect from LHDiagno c where c.CustomerNo = a.CustomerNo and a.InHospitNo = c.InHospitNo),"
      					+" a.InHospitalDays,a.MakeDate,a.MakeTime ,a.MainItem "
      					+" from LHCustomInHospital a, ldperson b "     					
      					+" where a.CustomerNo=b.CustomerNo and a.CustomerNo='"+ "<%=CustomerNo%>" +"' "
      					+" and a.InHospitNo='"+ "<%=InHospitNo%>" +"'"      
      					;
      					//alert(strSql);
      arrResult = easyExecSql(strSql); //alert(arrResult);
		
	  }
	  catch(ex){  alert("LHCustomInHospitalInput.js->afterquery0出错");}
      
      
      fm.all('CustomerNo').value= arrResult[0][0];fm.all('CustomerNo').readOnly = true;
      fm.all('CustomerName').value= arrResult[0][1];
      fm.all('InHospitNo').value= arrResult[0][2];
      fm.all('HospitCode').value= arrResult[0][3];
      if(arrResult[0][3] != "")
      {  fm.all('HospitName').value= easyExecSql("select HospitName from LDHospital where hospitcode = '"+arrResult[0][3]+"'");}
      fm.all('InHospitDate').value= arrResult[0][5];
      fm.all('DoctNo').value= arrResult[0][6];
      if(arrResult[0][6] != "")
      {  fm.all('DoctName').value= easyExecSql("select DoctName from LDDoctor where DoctNo = '"+arrResult[0][6]+"'");}
      fm.all('InHospitModeCode').value= arrResult[0][8];if(arrResult[0][8]=="1"){fm.all('InHospitMode').value="门诊";strsqlinit=" code like #1%# and ";initFeeInfoGrid();};if(arrResult[0][8]=="2"){fm.all('InHospitMode').value="住院";strsqlinit=" code like #2%# and ";initFeeInfoGrid();}if(arrResult[0][8]=="3"){fm.all('InHospitMode').value="急诊";}
      fm.all('MainCureModeCode').value= arrResult[0][9];
      var tempsql="select codename from ldcode where codetype='maincuremode' and code='"+arrResult[0][9]+"'";
      var curemodeResult=easyExecSql(tempsql);
      if(curemodeResult!=null)
      {
      	fm.all('MainCureMode').value= curemodeResult[0][0];
      }
      //fm.all('MainCureModeCode').value= arrResult[0][10];
      //fm.all('DiagnoseNo').value= arrResult[0][10];
      fm.all('CureEffectCode').value= arrResult[0][11];fm.all('CureEffect').value= easyExecSql("select codename from ldcode where codetype='diagnosecureeffect' and code ='"+arrResult[0][11]+"'");
      fm.all('InHospitalDays').value= arrResult[0][12];if(arrResult[0][8]=="2"){fm.InHospitalDays.style.display='';fm.CureEffect.style.display='';}else{fm.CureEffect.style.display='none';fm.InHospitalDays.style.display='none';}
      fm.all('MakeDate').value= arrResult[0][13];
      fm.all('MakeTime').value= arrResult[0][14];
      fm.all('MainItem').value= arrResult[0][15];
       
      strSql="select a.ICDCode,b.ICDName, a.IsMainDiagno,c.CodeName,a.DiagnoseNo "         
     +" from LHDiagno a, LDDisease b,LDCode c where 1=1  "            
     +" and a.ICDCode=b.ICDCode and a.IsMainDiagno=c.Code and c.codetype='ismaindiagno'"
     +getWherePart("a.CustomerNo","CustomerNo")                       
     +getWherePart("a.InHospitNo","InHospitNo");                      
		  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
		  turnPage.queryModal(strSql, DiagnoInfoGrid);
		  
		   strSql=" select b.MedicaItemName,a.MedicaItemCode,"
						 +" a.TestDate,a.TestResult,a.ResultUnitNum,"
				     +" a.TestNo from LHCustomTest a, LHCountrMedicaItem b where 1=1  "
				     +" and a.MedicaItemCode=b.MedicaItemCode "
				     + getWherePart("a.CustomerNo","CustomerNo") 
				     + getWherePart("a.InHospitNo","InHospitNo");
				     //  a.DoctNo,(select c.DoctName from LDDoctor c where a.DoctNo=c.DoctNo ),a.TestFeeAmount,
			//alert(strSql);
					  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
	     //alert(easyExecSql(strSql));
	  turnPage.queryModal(strSql, TestInfoGrid);
	  
	   strSql="select b.ICDOPSName,a.ICDOPSCode, a.DoctNo,(select c.DoctName from LDDoctor c where a.DoctNo=c.DoctNo ),a.ExecutDate,a.CureEffect,"
     +" a.OPSNo from LHCustomOPS a, LDICDOPS b where 1=1  "
     +" and a.ICDOPSCode=b.ICDOPSCode   "
     +getWherePart("a.CustomerNo","CustomerNo") 
     +getWherePart("a.InHospitNo","InHospitNo");
	  //var   arrLHDiagnoResult = easyExecSql(strSQL); a.OPSFeeAmount,
	  
	  turnPage.queryModal(strSql, OPSInfoGrid);
	  
	    
	   strSql="select b.MedicaItemName,a.MedicaItemCode, a.DoctNo,(select c.DoctName from LDDoctor c where a.DoctNo=c.DoctNo ),a.ExecutDate,a.CureEffect,"
     +" a.OtherFeeAmount,a.OtherCureNo from LHCustomOtherCure a, LHCountrMedicaItem b where 1=1  "
     +" and a.MedicaItemCode=b.MedicaItemCode "
     +getWherePart("a.CustomerNo","CustomerNo") 
     +getWherePart("a.InHospitNo","InHospitNo");
     //alert(strSql);
     //alert(easyExecSql(strSql));
	  //var   arrLHDiagnoResult = easyExecSql(strSQL); 
	  
	  turnPage.queryModal(strSql,OtherCureInfoGrid);
	  
	    strSql="select b.codename,a.FeeCode, a.FeeAmount,'',a.FeeNo "
     +"from LHFeeInfo a, ldcode b where 1=1  "
     +" and a.FeeCode=b.code and b.codetype='llfeeitemtype' "
     +getWherePart("a.CustomerNo","CustomerNo") 
     +getWherePart("a.InHospitNo","InHospitNo");
   
	  turnPage.queryModal(strSql, FeeInfoGrid);
	  
//计算分费用比例	   
	    var totalfee=0;
			var tempProportion;
			var rowcount=FeeInfoGrid.mulLineCount;
		
			for (i=0;i<=rowcount-1; i++)
			{
		   tempProportion=FeeInfoGrid.getRowColData(i,3)/1;
		   totalfee+= tempProportion     ;
			}

			for(i=0;i<=rowcount-1; i++)
			{   			
				tempProportion=FeeInfoGrid.getRowColData(i,3)/totalfee*100;
				tempProportion=Math.round(tempProportion*100)/100;  
				tempProportion=tempProportion+"%";
				FeeInfoGrid.setRowColData(i,4,tempProportion);
			}
			fm.all('TotalFee').value=totalfee;
   
//	  fm.all('iscomefromquery').value="1";
	//}
}               

</script>
