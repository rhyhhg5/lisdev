<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHServQieYueInfo.jsp
//程序功能：
//创建日期：2006-07-12 17:29:55
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<SCRIPT src="LHServQieYueInfo.js"></SCRIPT>
	<%@include file="LHServQieYueInfoInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
		<div style="display:''">
    		<table>
    			<tr>
		    		<td>
		    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHSettingSlipQueryGrid1);">
		    	 	</td>
    	 			<td class= titleImg>
        				 服务事件信息
       				</td>   		 
    			</tr>
    		</table>
	<Div  id= "divLHSettingSlipQueryGrid1" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>  
     
		        <TD class= title >
		        		 服务事件类型
		       	</TD>   		 
				<TD  class= input>
				 <Input class= 'codename' style="width:40px" name=ServCaseType ><Input class= 'codeno' style="width:120px" verify="服务事件类型|NOTNULL"  name=ServCaseTypeName ondblclick="showCodeList('eventtype',[this,ServCaseType],[1,0],null,null,null,'1',160);" codeClear(ServCaseTypeName,ServCaseType);">
				 <!--<Input class= 'common' style="width:40px" name=ServCaseType ><Input class= 'common' style="width:120px"  name=ServCaseTypeName >-->
				</TD>
						
		        <TD  class= title>
				  服务事件编号
				</TD>
				<TD  class= input>
				  <Input class= 'common' name=ServCaseCode style="width:160px" verify="服务事件编号|NOTNULL"  >
				</TD>
				<TD  class= title>
				  服务事件名称
				</TD>
				<TD  class= input>
				  <Input class= 'common' name=ServCaseName  style="width:160px"  verify="服务事件名称|NOTNULL" >
				</TD>
			</TR> 
			<TR  class= common>
				<TD class= title >
		    		服务事件状态
		       	</TD>   		 
				<TD  class= input>
				 <!--<Input class= 'codename' style="width:40px" name=ServCaseState value=2><Input class= 'codeno' style="width:120px"  name=ServCaseStateName value="服务事件已启动" ondblclick="showCodeList('eventstate',[this,ServCaseState],[1,0],null,null,null,'1',160);" codeClear(ServCaseState,ServCaseStateName);">
					<Input class= 'common' style="width:40px" name=ServCaseState><Input class= 'common' style="width:120px"  name=ServCaseStateName >-->
					<Input class= 'codename' readonly style="width:40px" name=ServCaseState><Input class= 'codeno' verify="服务事件状态|NOTNULL"  style="width:120px"  name=ServCaseStateName  ondblclick="showCodeList('eventstate',[this,ServCaseState],[1,0],null,null,null,'1',160);" codeClear(ServCaseState,ServCaseStateName);">
				</TD>
			</tr>
			<tr>
		      <td>
		         <input type= button class=cssButton  style="width:130px" name=QueryServEvent value="查询服务事件" OnClick="QueryServCase();">
		      </td>
		     <td>
					<input type= hidden class=cssButton  style="width:130px" name=AddNewServEvent value="新增服务事件" OnClick="AddServCase();">
		     </td>  
      </tr>
      	</table>
	</div>
    <div style="display:''">
    <table>
    	<tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServCaseRela);">
    	 	</td>
    	 	<td class= titleImg>
        	 服务事件关联信息
       		</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHServCaseRela" style= "display: ''">
		<table  class= common align='center' >  
			<TR class=common>     	
				<TD class= title >
		      	   是否未结束
		      	</TD>   		 
				<TD  class= input>
				    <Input class= 'codename' style="width:40px" name=ServCaseStutes value=2><Input class= 'codeno' style="width:120px"  name=ServCaseStutesName ondblclick="showCodeList('caseassociate',[this,ServCaseStutes],[1,0],null,null,null,'1',160);" value="未结束">
				</TD>
			</TR>
			<TR  class= common>
				<TD  class= title>
                	客户号码
              	</TD>
            	<TD  class= input>
            	  <Input class= 'code' name=CustomerNo ondblclick=" showCodeList('hmldpersonname',[this,CustomerName],[1,0],null,fm.CustomerName.value,'Name',1);">
            	</TD>
            	<TD  class= title>
            	  客户姓名
            	</TD>
            	<TD  class= input>
            	  <Input class= 'code' name=CustomerName ondblclick=" return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name',1);"  onkeyup=" if( event.keyCode==13 ) return showCodeList('hmldpersonname',[this,CustomerNo],[0,1],null,fm.CustomerName.value,'Name',1);" >
            	</TD>
            	<TD  class= title>
			      保单号
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=ContNo  >
			    </TD>  
			</TR> 
			<tr>
				<TD  class= title style="width:120">
         		   服务项目代码
         		</TD>
         		<TD  class= input>
         		  <Input class= 'code' name=ServItemCode  ondblclick="showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
         		</TD>
				 <TD  class= title style="width:120">
           			 服务项目名称
         		</TD>
             	<TD  class= input>
           			<Input class= 'code' name=ServItemName   ondblclick="showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
         		</TD>
				<TD class= title >保单机构</TD>   		 
				<TD  class= input>
					<Input class= "codeno"  name=MngCom  ondblclick="return showCodeList('hmhospitalmng',[this,MngCom_ch],[1,0],null,null,null,1,180);" onkeyup="return showCodeListKey('hmhospitalmng',[this,MngCom_ch],[1,0],null,null,null,1,180);" ><Input class=codename  name=MngCom_ch>	
				</TD>
			</TR>   
			<TR>
    			<TD  class= title>
    			  服务计划代码
    			</TD>
    			<TD  class= input>
					<Input class= 'code' name=Riskcode   ondblclick="showCodeList('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode');"   onkeydown="return showCodeListKey('hmriskcode',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode'); codeClear(Riskname,Riskcode);">
    			</TD>
    			<TD  class= title>
    			  服务计划名称
    			</TD>
    			<TD  class= input>
    			  <Input class= 'code' name=Riskname   ondblclick="showCodeList('hmriskcode',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskcode','1',300);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmriskcode',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskcode');"   onkeydown="return showCodeListKey('hmriskcode',[this,Riskcode],[0,1],null,fm.Riskcode.value,'Riskcode'); codeClear(Riskname,Riskcode);">
    			</TD>
    			<TD  class= title></TD>
             	<TD  class= input></TD> 
			</TR> 
			<TR  class= common>
				<TD  class= title style="width:120">
	            	服务事件编号
	         	</TD>
				<TD  class= input>
		           <Input class= 'code' name=tServCaseCode  ondblclick="showCodeList('lhservcasecode',[this,tServCaseName],[0,1],null,fm.tServCaseCode.value,'ServCaseCode','1',300);" >
				</TD>
				<TD  class= title style="width:120">
		            服务事件名称
				</TD>
				<TD  class= input>
		           <Input class= 'code' name=tServCaseName   ondblclick="showCodeList('lhservcasecode',[this,tServCaseCode],[1,0],null,fm.tServCaseName.value,'ServCaseName','1',300);" >
				</TD>
				<TD  class= title></TD>
        		<TD  class= input></TD> 
			</TR> 
			<tr>
				<td>
          			<input type= button class=cssButton  style="width:130px" name=ServTrace value="查 询" OnClick="QueryQieYueInfo();">
       			</td>  
       		</tr>
		</table>
    </Div>    
	<Div id="divLHSettingSlipQueryGrid" style="display:''">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHSettingSlipQueryGrid">
					</span> 
		    	</td>
			</tr>
		</table>
		<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	</div>

	<br><br>
	
  <Div id="divLHQueryDetailGrid" style="display:'none'">
		<table class=common>
			<tr class=common>
				<td text-align:left colSpan=1>
					<span id="spanLHQueryDetailGrid">
					</span> 
		    	</td>
			</tr>
		</table>
		<Div id= "divPage2" align=center style= "display: 'none' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	  </Div>
	</div>
  	
	<table>
     <tr style="width:50px" > 
     	<td >
     		<input type= button class=cssButton name=CaseShiShi   style="width:120px" value="添加到服务事件" OnClick="AddServCaseOld();">
       </td>
       <td >
     		<input type= button class=cssButton name=AddItemAll   style="width:120px" value="添加全部服务项目" OnClick="AddAllCaseInfo();">
       </td>
		   </tr> 
  </table>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmAction" name="fmAction">
		<!--团体服务计划号码-->
		<Input class= 'common' type=hidden  name=GrpServPlanNo >
		<!--个人服务计划号码-->
		<Input class= 'common' type=hidden   name=ServPlanNo >
		<!--服务计划类型-->
		<Input class= 'common' type=hidden name=ServPlayType >
		<Input class= 'common' type=hidden name=ManageCom >
	    <Input class= 'common' type=hidden name=Operator >
	    <Input class= 'common' type=hidden name=MakeDate >
	    <Input class= 'common' type=hidden name=MakeTime >
	    <Input class= 'common' type=hidden name=flag >
	    <Input class= 'common' type=hidden name=ServPlanNo2 >
	    <Input class= 'common' type=hidden name=ServPlanName2 >
	    <Input class= 'common' type=hidden name=ComID2 >
	    <Input class= 'common' type=hidden name=ServPlanLevel2 >
	    <Input class= 'common' type=hidden name=ServItemNo2 >
	    <Input class= 'common' type=hidden name=ServItemCode2 >
	    <Input class= 'common' type=hidden name=ContNo2 >
	    <Input class= 'common' type=hidden name=CustomerNo2 >
	    <Input class= 'common' type=hidden name=CustomerName2 >
	    <Input class= 'common' type=hidden name=ResultSql>
  	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
</html>
