<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-01-15 13:07:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDSocialInsOrganInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDSocialInsOrganInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDSocialInsOrganSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
		<table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDSocialInsOrgan1);">
    		</td>
    		 <td class= titleImg>
        		 社保机构信息
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDSocialInsOrgan1" style= "display: ''">
<table  class= common align='center' >
	<TR  class= common>
    <TD  class= title>
      社保机构代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SIOrganCode elementtype=nacessary verify="社保机构代码|notnull&len<=20" >
    </TD>
    <TD  class= title>
      社保机构名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SIOrganName verify="社保机构名称|len<=60">
    </TD>
    <TD  class= title>
      所属地区
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AreaCode verify="所属地区|len<=6">
    </TD>
  </TR>	
  <TR  class= common>
    <TD  class= title>
      详细地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Address verify="详细地址|len<=80">
    </TD>
    <TD  class= title>
      联系电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Phone verify="联系电话|len<=18">
    </TD>
    <TD  class= title>
      邮编
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ZipCode verify="邮编|len<=6&INT">
    </TD>
  </TR>
</table>    
  </div>
<!--
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDSocialInsOrgan1);">
    		</td>
    		 <td class= titleImg>
        		 医保经办机构基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDSocialInsOrgan1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      社会医疗保险机构代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SIOrganCode elementtype=nacessary verify="社会医疗保险机构代码|notnull&len<=20" >
    </TD>
    <TD  class= title>
      上级机构
    </TD>
    <TD  class= input>
      <Input class= 'code' name=SuperSIOrganCode elementtype=nacessary verify="上级机构|notnull" ondblclick="getSuperSIOrganCode();return showCodeListEx('SuperSIOrganCode',[this],[0],'', '', '', true);"  onkeyup="getSuperSIOrganCode();return showCodeListKeyEx('SIOrganName',[this],[0],'', '', '', true);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      社会医疗保险机构名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SIOrganName verify="社会医疗保险机构名称|len<=20">
    </TD>
    <TD  class= title>
      所在地区代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AreaCode verify="所在地区代码|len<=6">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Address verify="地址|len<=80">
    </TD>
    <TD  class= title>
      邮编
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ZipCode verify="邮编|len<=6">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      联系电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Phone verify="联系电话|len<=18">
    </TD>
    <TD  class= title>
      网址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WebAddress verify="网址|len<=100">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      传真
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Fax verify="传真|len<=18">
    </TD>
    <TD  class= title>
      开户银行
    </TD>
    <TD  class= input>
      <Input class= 'code' name=bankCode verify="开户银行|len<=10" ondblclick="showCodeList('bank',[this]);" onkeyup="showCodeListKey('bank',[this]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      户名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccName verify="户名|len<=50">
    </TD>
    <TD  class= title>
      银行帐号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=bankAccNO verify="银行帐号|len<=50">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      负责人
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SatrapName verify="负责人|len<=20">
    </TD>
    <TD  class= title>
      联系人
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Linkman verify="联系人|len<=20">
    </TD>
  </TR>
  

  
</table>
    </Div>
-->    
      <Div id= "div1" style= "display: 'none'">
  <table>
  <TR  class= common>
    <TD  class= title>
      最近修改日期
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=LastModiDate dateFormat="short">
    </TD>
  </TR>
  </table>
  </Div>
  
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type = hidden name = SuperSIOrganCode value = '1'>
    <input type = hidden name = MakeDate >
    <input type = hidden name = MakeTime >
    
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
