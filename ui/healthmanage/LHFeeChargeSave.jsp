<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHFeeChargeSave.jsp
//程序功能：
//创建日期：2006-09-21 15:08:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.health.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	TransferData mTransferData = new TransferData();
	LHFeeChargeUI tLHFeeChargeUI   = new LHFeeChargeUI();
	LHFeeChargeSet tLHFeeChargeSet = new LHFeeChargeSet();		//服务预约管理表
	LHTaskCustomerRelaSet tLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();		//服务预约管理表
    
    String tTaskExecNo[] = request.getParameterValues("LHFeeChargeGrid16");           //任务实施号码流水号
    String tServTaskNo[] = request.getParameterValues("LHFeeChargeGrid18");           //服务任务编号
    String tServCaseCode[] = request.getParameterValues("LHFeeChargeGrid3");					//服务事件号  
    String tServTaskCode[] = request.getParameterValues("LHFeeChargeGrid6");           //服务任务代码
    String tServItemNo[] = request.getParameterValues("LHFeeChargeGrid17");					//服务项目序号
    String tContNo[] = request.getParameterValues("LHFeeChargeGrid9");					//客户号  
    String tCustomerNo[] = request.getParameterValues("LHFeeChargeGrid1");					//客户号  	 
    String tFeeNormal[] = request.getParameterValues("LHFeeChargeGrid19");
    String tFeePayed[] = request.getParameterValues("LHFeeChargeGrid20");    
    String tTaskExecState[] = request.getParameterValues("LHFeeChargeGrid22");					//服务执行状态		 
    String tFeeTaskExecNo[] = request.getParameterValues("LHFeeChargeGrid23");
    String tFeeType[] = request.getParameterValues("LHFeeChargeGrid24");
    String tMakeDate[] = request.getParameterValues("LHFeeChargeGrid25");
    String tMakeTime[] = request.getParameterValues("LHFeeChargeGrid26");
    
    String tChk[] = request.getParameterValues("InpLHFeeChargeGridChk"); //参数格式=” Inp+MulLine对象名+Chk”     	 
     
	mTransferData.setNameAndValue("ServTaskNo",request.getParameter("ServTaskNo"));     
	mTransferData.setNameAndValue("Fee",request.getParameter("Fee"));     
	mTransferData.setNameAndValue("ExecState",request.getParameter("ExecState"));   
     
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");
	int LHFeeChargeCount = 0;
	if(tTaskExecNo != null)
	{	
		LHFeeChargeCount = tTaskExecNo.length;
	}	

    for(int j = 0; j < LHFeeChargeCount; j++)
	{ 
		LHFeeChargeSchema tLHFeeChargeSchema = new LHFeeChargeSchema();
		      
		tLHFeeChargeSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
		tLHFeeChargeSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
		tLHFeeChargeSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
		tLHFeeChargeSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
		tLHFeeChargeSchema.setServItemNo(tServItemNo[j]);	//项目号码
		tLHFeeChargeSchema.setCustomerNo(tCustomerNo[j]);	//客户号
		tLHFeeChargeSchema.setContNo(tContNo[j]);	//保单号 
		tLHFeeChargeSchema.setFeeNormal(tFeeNormal[j]);	//保单号     
		tLHFeeChargeSchema.setMakeDate(tMakeDate[j]);   
		tLHFeeChargeSchema.setMakeTime(tMakeTime[j]);
		      
//		if(tChk[j].equals("1"))  
//		{   
			tLHFeeChargeSchema.setFeePay(request.getParameter("Fee"));//实付
//		}
//		else
//		{
//			tLHFeeChargeSchema.setFeePay(tFeePayed[j]);
//		}
		tLHFeeChargeSchema.setFeeTaskExecNo(tFeeTaskExecNo[j]);
		tLHFeeChargeSchema.setFeeType("1");
		
//		LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema();
//		tLHTaskCustomerRelaSchema.setTaskExecNo(tTaskExecNo[j]);	 //流水号
//		tLHTaskCustomerRelaSchema.setServTaskNo(tServTaskNo[j]);	 //任务编号
//		tLHTaskCustomerRelaSchema.setServCaseCode(tServCaseCode[j]);	 //事件号码
//		tLHTaskCustomerRelaSchema.setServTaskCode(tServTaskCode[j]);	 //任务代码 
//		tLHTaskCustomerRelaSchema.setServItemNo(tServItemNo[j]);	//项目号码
//		tLHTaskCustomerRelaSchema.setTaskExecState(tTaskExecState[j]);	//服务执行状态  
//		
		if(transact.equals("INSERT||MAIN"))
		{
			tLHFeeChargeSet.add(tLHFeeChargeSchema);   
//			tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);     
		}  
		if(transact.equals("UPDATE||MAIN"))
		{
			if(tChk[j].equals("1"))  
            {
                tLHFeeChargeSet.add(tLHFeeChargeSchema);      
//	 			tLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);      
	 		}
        }
	}
	
	try
	{
	// 准备传输数据 VData
	  	VData tVData = new VData();
	  	System.out.println("=-----------------=");
	  	tVData.add(tLHFeeChargeSet);
//	  	tVData.add(tLHTaskCustomerRelaSet);
	  	tVData.add(tG);
	  	tVData.addElement(mTransferData);
	    tLHFeeChargeUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tLHFeeChargeUI.mErrors;
	if (!tError.needDealError())
	{                          
		Content = " 操作成功! ";
		FlagStr = "Success";
	}
	else                                                                           
	{
		Content = " 操作失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
  
	//添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	var transact = "<%=transact%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 
</script>
</html>