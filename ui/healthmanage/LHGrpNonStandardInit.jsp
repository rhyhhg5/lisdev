<%
//程序名称：LHGrpNonStandardInit.jsp
//程序功能：
//创建日期：2006-12-14 14:34:17
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('GrpServPlanNo').value = "";
    fm.all('GrpCustomerNo').value = "";
    fm.all('GrpName').value = "";
    fm.all('ServPlanCode').value = "";
    fm.all('ServPlanName').value = "";
    fm.all('ServPlanLevel').value = "";
    fm.all('ComID').value = "";
    fm.all('StartDate').value = "";
    fm.all('EndDate').value = "";
    fm.all('ServPrem').value = "";
    fm.all('ManageCom').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHGrpServPlanInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {}
  catch(ex)
  {
    alert("在LHGrpServPlanInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initLHGrpServPlanGrid();
	fm.all('ServSet').disabled = true; 
  }
  catch(re)
  {
    alert("LHGrpServPlanInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHGrpServPlanGrid;
function initLHGrpServPlanGrid()
{
	var iArray = new Array();
    
	try 
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         		//列名
    	iArray[0][1]="30px";         		//列名
    	iArray[0][3]=0;         		//列名
    	iArray[0][4]="station";			//列名
    
    	iArray[1]=new Array(); 
	    iArray[1][0]="客户分档";   
	    iArray[1][1]="60px";   
	    iArray[1][2]=20;        
	    iArray[1][3]=2;
		iArray[1][4]="grpbaddish";
		iArray[1][5]="1|5";
		iArray[1][6]="1|0";
		iArray[1][17]="1";
		iArray[1][18]="200"; 
		iArray[1][19]="1" ;

	    iArray[2]=new Array(); 
	    iArray[2][0]="约定人数";   
	    iArray[2][1]="60px";   
	    iArray[2][2]=20;        
	    iArray[2][3]=1;

		iArray[3]=new Array();                               
    	iArray[3][0]="GrpServPlanNo";         		//列名  
    	iArray[3][1]="0px";         		//列名             
    	iArray[3][3]=3; 

		iArray[4]=new Array();                               
    	iArray[4][0]="实际人数";         		//列名  
    	iArray[4][1]="60px";         		//列名   
    	iArray[4][2]=20;              
    	iArray[4][3]=1;         		//列名         

    	iArray[5]=new Array();                               
    	iArray[5][0]="grpbaddish";         		//列名  
    	iArray[5][1]="0px";         		//列名             
    	iArray[5][3]=3;       
    	
    	iArray[6]=new Array();                               
    	iArray[6][0]="MakeDate";         		//列名  
    	iArray[6][1]="0px";         		//列名             
    	iArray[6][3]=3;    
    	
    	iArray[7]=new Array();                               
    	iArray[7][0]="MakeTime";         		//列名  
    	iArray[7][1]="0px";         		//列名             
    	iArray[7][3]=3;       
    	
		iArray[8]=new Array();                               
    	iArray[8][0]="档次说明";         		//列名  
    	iArray[8][1]="380px";         		//列名   
    	iArray[8][2]=1000;              
    	iArray[8][3]=1;        
    	
    	
    	LHGrpServPlanGrid = new MulLineEnter( "fm" , "LHGrpServPlanGrid" ); 
    	//这些属性必须在loadMulLine前
    	
    	LHGrpServPlanGrid.mulLineCount = 0;   
    	LHGrpServPlanGrid.displayTitle = 1;
    	LHGrpServPlanGrid.hiddenPlus = 0;
    	LHGrpServPlanGrid.hiddenSubtraction = 0;
    	LHGrpServPlanGrid.canSel = 1;
    	LHGrpServPlanGrid.canChk = 0;
    	//LHGrpServPlanGrid.selBoxEventFuncName = "showOne";
    	
    	LHGrpServPlanGrid.loadMulLine(iArray);  
    	//这些操作必须在loadMulLine后面
    	//LHGrpServPlanGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
