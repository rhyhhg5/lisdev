<%
//程序名称：LHNonStandardPlanQueryInput.jsp
//程序功能：功能描述
//创建日期：2006-11-21 09:56:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期    : 
// 更新原因/内容: 插入新的字段
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHNonStandardPlanQuery.js"></SCRIPT> 
  <%@include file="LHNonStandardPlanQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLHServPlanGrid1" style= "display: ''">    
<table  class= common align='center' >
      <tr class=common>
				  <TD class= title >
		          契约事件类型
		       </TD>   		 
					<TD  class= input> 
					<input class=codeno CodeData="0|3^1|个人保单^2|团体保单^3|非标业务"  name=ServCaseType 
					ondblclick="return showCodeListEx('DealWith',[this,ServCaseTypeName],[0,1],null,null,null,1);" 
					onkeyup="return showCodeListKeyEx('DealWith',[this,ServCaseTypeName],[0,1],null,null,null,1);" verify="契约事件类型|notnull&len=1" ><input class=codename name=ServCaseTypeName>
					</TD>				  
				  <TD  class= title style="width:120">
	             事件原始编号
	        </TD>
				  <TD  class= input>
		          <Input class= 'common' name=ContNo>
				  </TD>
				  <TD id=aa1 class= title>
				  	契约事件状态
				  </TD>
	        <TD id=aa2 class= input>
	         	   <Input class= 'codename' style="width:40px" readonly name=CaseStateName><Input class= 'codeno'  style="width:120px" name=CaseState ondblclick="return showCodeList('lhcasestate',[this,CaseStateName],[1,0],null,null,null,null,160);" >
		     </TD>
		  </tr>
			<TR  class= common>  
			    <TD  class= title>
             客户号码
          </TD>
          <TD  class= input>
            <Input class= 'code' name=CustomerNo elementtype=nacessary ondblclick="return queryCustomerNo();" onkeyup="return queryCustomerNo2();">
          </TD>
          <TD  class= title>
            客户姓名
          </TD>
			    <TD  class= input>
			      <Input class= 'code' name=CustomerName   ondblclick="return queryCustomerNo();">
			    </TD>
			    <TD  class= title style="width:120">
	          契约机构标识
	        </TD>
	        <TD  class= input>
	            <Input class= 'codename' readonly style="width:40px" name=ComID><Input class= 'code' style="width:120px" name=ComID_cn ondblclick="showCodeList('hmhospitalmng', [this, ComID], [0,1], null, null, null, 1,150);">
	        </TD>
			</TR>
</table>
  </Div>
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT class =cssbutton VALUE="查询"   TYPE=button   class=common onclick="easyQueryClick();">
        <INPUT class =cssbutton VALUE="返回"   TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>     
  <hr>   
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHServPlanGrid);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHServPlanGrid" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHServPlanGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>  
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
