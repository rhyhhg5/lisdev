<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHStandardTaskDefineSave.jsp
//程序功能：
//创建日期：2006-08-02 15:45:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHTaskServItemRelaUI tLHTaskServItemRelaUI   = new LHTaskServItemRelaUI();
   LHServTaskDefSchema tLHServTaskDefSchema = new LHServTaskDefSchema();
  LHTaskServItemRelaSet tLHTaskServItemRelaSet = new LHTaskServItemRelaSet();		//服务事件任务关联表
  
    
    String tSerialNo[] = request.getParameterValues("LHQueryDetailGrid3");           //流水号 
    String tServItemCode[] = request.getParameterValues("LHQueryDetailGrid1");					//标准服务项目代码  
   
			    tLHServTaskDefSchema.setServTaskCode(request.getParameter("TaskNo"));
			    tLHServTaskDefSchema.setServTaskName(request.getParameter("TaskName"));
			    tLHServTaskDefSchema.setServTaskModule(request.getParameter("ServTaskModule"));
			    tLHServTaskDefSchema.setServTaskDes(request.getParameter("ServTaskDes"));  	 
     
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");

   int LHServTaskDefCount = 0;
	 if(tSerialNo != null)
	 {	
		   LHServTaskDefCount = tSerialNo.length;
	 }	
	 System.out.println(" LHServTaskDefCount is : "+LHServTaskDefCount);

			  System.out.println(" VVVVVVVVVVVVVVVvvv");                    
    for(int i = 0; i < LHServTaskDefCount; i++)
    {
           LHTaskServItemRelaSchema tLHTaskServItemRelaSchema = new LHTaskServItemRelaSchema();
			
				  tLHTaskServItemRelaSchema.setSerialNo(tSerialNo[i]);	 //流水号
				  tLHTaskServItemRelaSchema.setServTaskCode(request.getParameter("TaskNo"));//任务号
				  tLHTaskServItemRelaSchema.setServItemCode(tServItemCode[i]);	//项目号
				 
				   tLHTaskServItemRelaSet.add(tLHTaskServItemRelaSchema);     
    }
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHServTaskDefSchema);
  	tVData.add(tLHTaskServItemRelaSet);
  	tVData.add(tG);
  	System.out.println("- DDDDDDDDDD "+transact);
    tLHTaskServItemRelaUI.submitData(tVData,transact);
    System.out.println("- SSSSSSSSSSSS "+transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHTaskServItemRelaUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");   
	 
</script>
</html>