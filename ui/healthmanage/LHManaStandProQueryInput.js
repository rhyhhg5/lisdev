/** 
 * 程序名称：LHHealthServPlanQueryInput.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2006-09-11 17:20:48
 * 创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
	var strSql = " select distinct  StandCode,StandContent,"
	           +" (select b.codename from ldcode b where codetype='lhcodelabelgrade' and b.code=a.StandLevel), "
	           +"  StandUnit,"
	           +"(select b.codename from ldcode b where codetype='lhquestand' and b.code=a.QuesStauts),"
	           +" (select b.codename from ldcode b where codetype='lhincludeques' and b.code=a.QuerAffiliated), "
	           +" (select b.codename from ldcode b where codetype='lhqueresultype' and b.code=a.QuerResType) ,"
	           +" ResuAffiInfo,"
	           +" (select b.codename from ldcode b where codetype='lhquesrestype' and b.code=a.StandQuerType) "
	           +"  from  LHManaStandPro a  "
			       +"  where 1=1 "
			       +getWherePart("a.StandCode", "StandCode", "like")
			       +getWherePart("a.StandContent", "StandContent", "like")
			       +getWherePart("a.StandLevel", "StandLevel")
			       +getWherePart("a.StandUnit", "StandUnit", "like")
			       +getWherePart("a.QuerAffiliated", "QuerAffiliated")
			       +getWherePart("a.QuesStauts", "QuesStauts")
			       +getWherePart("a.QuerResType", "QuerResType")
			       +getWherePart("a.ResuAffiInfo", "ResuAffiInfo", "like")
			       +getWherePart("a.StandQuerType", "StandQuerType")
  			     ;
  //alert(strSql);
  //alert(easyExecSql(strSql));
	turnPage.queryModal(strSql, LHStaQuOptionGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");

}
function returnParent()
{
  var arrReturn = new Array();
	var tSel = LHStaQuOptionGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHStaQuOptionGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = LHStaQuOptionGrid.getRowData(tRow-1);	
	return arrSelected;
}
