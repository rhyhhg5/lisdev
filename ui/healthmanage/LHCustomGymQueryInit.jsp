<%
//程序名称：LHCustomGymQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-01-17 15:20:33
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('CustomerNo').value = "";
    fm.all('GymItemCode').value = "";
    fm.all('GymFreque').value = "";
    //fm.all('Operator').value = "";

  }
  catch(ex) {
    alert("在LHCustomGymQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLHCustomGymGrid();  
  }
  catch(re) {
    alert("LHCustomGymQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LHCustomGymGrid;
function initLHCustomGymGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		        //列名
    iArray[0][4]="station";         		//列名
    
                iArray[1]=new Array(); 
		iArray[1][0]="客户号码";   
		iArray[1][1]="30px";   
		iArray[1][2]=24;        
		iArray[1][3]=0;
		
                iArray[2]=new Array();  
		iArray[2][0]="客户姓名";
		iArray[2][1]="30px";    
		iArray[2][2]=20;         
		iArray[2][3]=0;            
		
		iArray[3]=new Array();  
		iArray[3][0]="健身项目代码";
		iArray[3][1]="30px";    
		iArray[3][2]=20;         
		iArray[3][3]=0;
		
		iArray[4]=new Array();  
		iArray[4][0]="健身频率";
		iArray[4][1]="30px";    
		iArray[4][2]=20;         
		iArray[4][3]=0; 
		
		iArray[5]=new Array();  
		iArray[5][0]="每次健身时间";
		iArray[5][1]="30px";    
		iArray[5][2]=20;         
		iArray[5][3]=0;   
		
    
    
    LHCustomGymGrid = new MulLineEnter( "fm" , "LHCustomGymGrid" ); 
    //这些属性必须在loadMulLine前
    
    LHCustomGymGrid.mulLineCount = 0;   
    LHCustomGymGrid.displayTitle = 1;
    LHCustomGymGrid.hiddenPlus = 1;
    LHCustomGymGrid.hiddenSubtraction = 1;
    LHCustomGymGrid.canSel = 1;
    LHCustomGymGrid.canChk = 0;


    LHCustomGymGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHCustomGymGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
