<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLRegisterSave.jsp
//程序功能：
//创建日期：2005-02-06 11:49:38
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLRegisterSchema tLLRegisterSchema   = new LLRegisterSchema();
  OLLRegisterUI tOLLRegisterUI   = new OLLRegisterUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    //tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
    tLLRegisterSchema.setRgtState(request.getParameter("RgtState"));
    tLLRegisterSchema.setRgtObj(request.getParameter("RgtObj"));
    tLLRegisterSchema.setRgtObjNo(request.getParameter("RgtObjNo"));
    tLLRegisterSchema.setRgtType(request.getParameter("RgtType"));
    tLLRegisterSchema.setRgtClass(request.getParameter("RgtClass"));
    tLLRegisterSchema.setAgentCode(request.getParameter("AgentCode"));
    tLLRegisterSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLLRegisterSchema.setApplyerType(request.getParameter("ApplyerType"));
    tLLRegisterSchema.setRgtantName(request.getParameter("RgtantName"));
    tLLRegisterSchema.setRgtantSex(request.getParameter("RgtantSex"));
    tLLRegisterSchema.setRelation(request.getParameter("Relation"));
    tLLRegisterSchema.setRgtantAddress(request.getParameter("RgtantAddress"));
    tLLRegisterSchema.setRgtantPhone(request.getParameter("RgtantPhone"));
    tLLRegisterSchema.setRgtantMobile(request.getParameter("RgtantMobile"));
    tLLRegisterSchema.setEmail(request.getParameter("Email"));
    tLLRegisterSchema.setPostCode(request.getParameter("PostCode"));
    tLLRegisterSchema.setRgtDate(request.getParameter("RgtDate"));
    tLLRegisterSchema.setAccidentSite(request.getParameter("AccidentSite"));
    tLLRegisterSchema.setAccidentReason(request.getParameter("AccidentReason"));
    tLLRegisterSchema.setAccidentCourse(request.getParameter("AccidentCourse"));
    tLLRegisterSchema.setAccStartDate(request.getParameter("AccStartDate"));
    tLLRegisterSchema.setAccidentDate(request.getParameter("AccidentDate"));
    tLLRegisterSchema.setRgtReason(request.getParameter("RgtReason"));
    tLLRegisterSchema.setAppPeoples(request.getParameter("AppPeoples"));
    tLLRegisterSchema.setAppAmnt(request.getParameter("AppAmnt"));
    tLLRegisterSchema.setGetMode(request.getParameter("GetMode"));
    tLLRegisterSchema.setGetIntv(request.getParameter("GetIntv"));
    tLLRegisterSchema.setCaseGetMode(request.getParameter("CaseGetMode"));
    tLLRegisterSchema.setReturnMode(request.getParameter("ReturnMode"));
    tLLRegisterSchema.setRemark(request.getParameter("Remark"));
    tLLRegisterSchema.setHandler(request.getParameter("Handler"));
    tLLRegisterSchema.setTogetherFlag(request.getParameter("TogetherFlag"));
    tLLRegisterSchema.setRptFlag(request.getParameter("RptFlag"));
    tLLRegisterSchema.setCalFlag(request.getParameter("CalFlag"));
    tLLRegisterSchema.setUWFlag(request.getParameter("UWFlag"));
    tLLRegisterSchema.setDeclineFlag(request.getParameter("DeclineFlag"));
    tLLRegisterSchema.setEndCaseFlag(request.getParameter("EndCaseFlag"));
    tLLRegisterSchema.setEndCaseDate(request.getParameter("EndCaseDate"));
    tLLRegisterSchema.setMngCom(request.getParameter("MngCom"));
    tLLRegisterSchema.setClmState(request.getParameter("ClmState"));
    tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));
    tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLLRegisterSchema.setAccName(request.getParameter("AccName"));
    tLLRegisterSchema.setIDType(request.getParameter("IDType"));
    tLLRegisterSchema.setIDNo(request.getParameter("IDNo"));
    tLLRegisterSchema.setHandler1(request.getParameter("Handler1"));
    tLLRegisterSchema.setHandler1Phone(request.getParameter("Handler1Phone"));
    tLLRegisterSchema.setOperator(request.getParameter("Operator"));
    tLLRegisterSchema.setMakeDate(request.getParameter("MakeDate"));
    tLLRegisterSchema.setMakeTime(request.getParameter("MakeTime"));
    tLLRegisterSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLLRegisterSchema.setModifyTime(request.getParameter("ModifyTime"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLLRegisterSchema);
  	tVData.add(tG);
    tOLLRegisterUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLLRegisterUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
