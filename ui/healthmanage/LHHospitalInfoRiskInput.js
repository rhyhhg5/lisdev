//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initLHRiskInfoGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.RiskCode, a.RiskName from LMRisk  a,LMRiskApp b where a.RiskCode=b.RiskCode and 1=1 "
				 + getWherePart("a.RiskCode","Riskcode","like")
				 + getWherePart("a.RiskName","Riskname","like")
  //alert(strSQL);
  turnPage.pageLineNum = 100;  
	turnPage.queryModal(strSQL, LHRiskInfoGrid);        
}

// 数据返回父窗口
function returnParent()
{
    var arrSelected = null;
	  arrSelected = new Array();
	  var rowNum=LHRiskInfoGrid. mulLineCount ; //行数 	
	  var aa = new Array();
	  var xx = 0;
	  for(var row=0; row < rowNum; row++)
	  {
	        var tChk =LHRiskInfoGrid.getChkNo(row); 
	        if(tChk == true)
	        {
	        	   aa[xx] = row;
	             arrSelected[xx++] = LHRiskInfoGrid.getRowColData(aa,1);
	             //alert("AA "+arrSelected);
	        }
	  }
	  //alert(arrSelected);
	  top.opener.afterQuery(arrSelected);
		top.close();
}
