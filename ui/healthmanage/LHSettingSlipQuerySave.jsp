<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHSettingSlipQuerySave.jsp
//程序功能：
//创建日期：2006-07-26 14:14:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期: 
// 更新原因/内容: 
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LHSettingSlipQueryUI tLHSettingSlipQueryUI   = new LHSettingSlipQueryUI();
  
  LHServCaseDefSet tLHServCaseDefSet = new LHServCaseDefSet();		//合同责任信息
  
    String tServCaseCode[] = request.getParameterValues("LHSettingSlipQueryGrid1");           //事件编号   
    String tServCaseState[] = request.getParameterValues("LHSettingSlipQueryGrid6");					//事件状态     
    String tChk[] = request.getParameterValues("InpLHSettingSlipQueryGridChk"); //参数格式=” Inp+MulLine对象名+Chk”     	 
     
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	transact = request.getParameter("fmtransact");

    int LHServCaseDefCount = 0;
		if(tServCaseCode != null)
		{	
			   LHServCaseDefCount = tServCaseCode.length;
		}	
		System.out.println(" LHServCaseDefCount is : "+LHServCaseDefCount);
		
		for(int i = 0; i < LHServCaseDefCount; i++)
		{
			
				LHServCaseDefSchema tLHServCaseDefSchema = new LHServCaseDefSchema();
			
				 
				  
				  tLHServCaseDefSchema.setServCaseCode(tServCaseCode[i]);	 //事件号
				  tLHServCaseDefSchema.setServCaseState(tServCaseState[i]);	//事件状态
				  
				tLHServCaseDefSchema.setOperator(request.getParameter("Operator"));
				//liuli 08/05/07 不更改lhservcasedef表中的make时间
				//tLHServCaseDefSchema.setMakeDate(request.getParameter("MakeDate"));
				//tLHServCaseDefSchema.setMakeTime(request.getParameter("MakeTime"));                                               
            if(tChk[i].equals("1"))  
            {         
                 System.out.println("该行被选中 "+tServCaseCode[i]);
                 tLHServCaseDefSet.add(tLHServCaseDefSchema);         
                 
             }
            if(tChk[i].equals("0"))  
            {     
                  System.out.println("该行未被选中 "+tServCaseCode[i]);
            } 
        
                 
			                        
		}
                              
                              
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLHServCaseDefSet);
  	tVData.add(tG);
  	System.out.println("- DDDDDDDDDD "+transact);
    tLHSettingSlipQueryUI.submitData(tVData,transact);
    System.out.println("- SSSSSSSSSSSS "+transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHSettingSlipQueryUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");   
</script>
</html>