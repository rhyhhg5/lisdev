<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHServQieYueAddEventSave.jsp
//程序功能：
//创建日期：2006-10-10 15:00:20
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  
  String Healthno="";
  
  LHServCaseDefSchema tLHServCaseDefSchema=new LHServCaseDefSchema();
   OLHServCaseDefUI tOLHServCaseDefUI   = new OLHServCaseDefUI();
  
  //输出参数
  CErrors tError = null;
             
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLHServCaseDefSchema.setServCaseType(request.getParameter("ServCaseType"));
    tLHServCaseDefSchema.setServCaseCode(request.getParameter("ServCaseCode"));
    tLHServCaseDefSchema.setServCaseName(request.getParameter("ServCaseName"));
    tLHServCaseDefSchema.setServCaseState(request.getParameter("ServCaseState"));
    tLHServCaseDefSchema.setOperator(request.getParameter("Operator"));
    tLHServCaseDefSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHServCaseDefSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHServCaseDefSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHServCaseDefSchema.setModifyTime(request.getParameter("ModifyTime"));
    
    System.out.println("--------------tG.ManageCom"+tG.ManageCom);
    tLHServCaseDefSchema.setManageCom(request.getParameter(tG.ManageCom));
    
     
  try
  {
  // 准备传输数据 VData
    System.out.println("--------------Start to Submit---------");
  	VData tVData = new VData();
  	tVData.add(tLHServCaseDefSchema);
  	tVData.add(tG);
    System.out.println("aaaaaaaaaaaaaaaaaa");
    tOLHServCaseDefUI.submitData(tVData,transact);

  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLHServCaseDefUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
