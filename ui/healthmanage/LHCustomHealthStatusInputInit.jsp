<%
//程序名称：LHCustomHealthStatusInput.jsp
//程序功能：
//创建日期：2005-01-18 10:11:20
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <script language="JavaScript">
  function initLHCustomGymGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	
      iArray[0][1]="25px";  	
      iArray[0][2]=10;     
      iArray[0][3]=2;
      
      iArray[1]=new Array();
      iArray[1][0]="健身项目代码";
      iArray[1][1]="0px";
      iArray[1][2]=10;
      iArray[1][3]=0; 
      iArray[1][9]="健身项目代码|notnull"; 
      
      iArray[2]=new Array();
      iArray[2][0]="健身项目名称";  	
      iArray[2][1]="75px";  	
      iArray[2][2]=10;      
      iArray[2][3]=2;  
      iArray[2][4]="lhgymitem";
      iArray[2][5]="2|1";     //引用代码对应第几列，'|'为分割符
      iArray[2][6]="0|1";     //上面的列中放置引用代码中第几位值
      iArray[2][9]="健身项目名称|notnull&len<=120";
      iArray[2][15]="codename";
      iArray[2][17]="2";  		//Mulline中进行模糊查询时，查询输入框中内容的列数
      iArray[2][19]="1" ;
 
      iArray[3]=new Array();
      iArray[3][0]="每次健身时间（小时）";  
      iArray[3][1]="75px";  	  
      iArray[3][2]=10;        
      iArray[3][3]=1;     
      iArray[3][9]="每次健身时间（小时）|INT&len<=2";     
      
      iArray[4]=new Array();
      iArray[4][0]="健身频率（次/月）";  
      iArray[4][1]="75px";  	 
      iArray[4][2]=10;
      iArray[4][3]=1; 
      iArray[4][9]="健身频率（次/月）|INT&len<=3";         
  
      iArray[5]=new Array();
      iArray[5][0]="CustomerGymNo";  
      iArray[5][1]="0px";  	 
      iArray[5][2]=10;
      iArray[5][3]=3; 

     
      LHCustomGymGrid= new MulLineEnter( "fm" , "LHCustomGymGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            LHCustomGymGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            LHCustomGymGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
            //LHCustomGymGrid.canSel =1; 
           
         
              //对象初始化区：调用对象初始化方法，属性必须在此前设置
      LHCustomGymGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script>     
<script language="JavaScript">
  function initLHCustomFamilyDiseasGrid()    //函数名为init+MulLine的对象名ObjGrid
   {
     var iArray = new Array(); //数组放置各个列
     try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";  	//列名（序号列，第1列）
      iArray[0][1]="25px";  	//列宽
      iArray[0][2]=10;     	  //列最大值
      iArray[0][3]=0;  			  //1表示允许该列输入，0表示只读且不响应Tab键
                     					// 2 表示为容许输入且颜色加深. 	
      iArray[1]=new Array();
      iArray[1][0]="亲属关系名称";  	//列名（序号列，第1列）
      iArray[1][1]="75px";  					//列宽
      iArray[1][2]=10;      					//列最大值
      iArray[1][3]=2;
      iArray[1][4]="FamilyCode";
      iArray[1][5]="1|2";     				//引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";    					//上面的列中放置引用代码中第几位值
      iArray[1][9]="亲属关系名称|len<=120";
      
      
      
      
      iArray[2]=new Array();
      iArray[2][0]="亲属关系代码";  	
      iArray[2][1]="0px";  	
      iArray[2][2]=10;      
      iArray[2][3]=3; 
     
      
      iArray[3]=new Array();
      iArray[3][0]="疾病代码";  	
      iArray[3][1]="75px";  	
      iArray[3][2]=10;      
      iArray[3][3]=0;  
                           
      iArray[4]=new Array();
      iArray[4][0]="疾病名称";  				//列名（第2列）
      iArray[4][1]="75px";  	  				//列宽
      iArray[4][2]=10;        					//列最大值
         																//后续可以添加N列，如上设置
      iArray[4][3]=2;          					//是否允许输入,1表示允许，0表示不允许
      iArray[4][4]="lhdisease";
      iArray[4][5]="4|3";     					//引用代码对应第几列，'|'为分割符
      iArray[4][6]="0|1";    						//上面的列中放置引用代码中第几位值
      iArray[4][9]="疾病名称|len<=120";
      iArray[4][15]="ICDName";
      iArray[4][17]="4";  
      iArray[4][19]="1" ;
      
      iArray[5]=new Array();
      iArray[5][0]="流水号";  	
      iArray[5][1]="0px";  	
      iArray[5][2]=10;      
      iArray[5][3]=3;  
     
      
    
      LHCustomFamilyDiseasGrid= new MulLineEnter( "fm" , "LHCustomFamilyDiseasGrid" ); 
      //设置属性区 (需要其它特性，在此设置其它属性)
            LHCustomFamilyDiseasGrid.mulLineCount = 0 ; //行属性：设置行数=3    
            LHCustomFamilyDiseasGrid.displayTitle = 1;   //标题属性：1显示标题 (缺省值) ,0隐藏标题       
				    LHCustomFamilyDiseasGrid.hiddenPlus =0;
				    LHCustomFamilyDiseasGrid.hiddenSubtraction = 0;
				    //LHCustomFamilyDiseasGrid.canSel = 1;
				    LHCustomFamilyDiseasGrid.canChk = 0;         
           // LHCustomFamilyDiseasGrid.canSel =1; 
           //对象初始化区：调用对象初始化方法，属性必须在此前设置
       
      LHCustomFamilyDiseasGrid.loadMulLine(iArray); 
      }
      catch(ex)
      { alert(ex); }
    }
</script>                       
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    //fm
    fm.all('CustomerNo').value = "";
    fm.all('CustomerName').readOnly=true;
    fm.all('CustomerName').value = ""
    fm.all('AddDate').value = "";
    fm.all('Stature').value="";
    fm.all('Avoirdupois').value=""
    fm.all('AvoirdIndex').readOnly=true;
    fm.all('BloodPressHigh').value="";
    fm.all('BloodPressLow').value="";
    fm.all('Smoke').value="";
    fm.all('KissCup').value="";
    fm.all('SitUp').value="";
    fm.all('DiningNoRule').value="";
    fm.all('BadHobby').value="";
    fm.all('iscomefromquery').value="0";
 
  
    /*form2
    form2.all('CustomerNo').value = "";
    form2.all('CustomerName').readOnly=true;
    form2.all('CustomerName').value = ""
    form2.all('AddDate').value = "";
    form2.all('Stature').value="";
    form2.all('Avoirdupois').value=""
    form2.all('AvoirdIndex').readOnly=true;
    form2.all('BloodPressHigh').value="";
    form2.all('BloodPressLow').value="";
    form2.all('Smoke').value="";
    form2.all('KissCup').value="";
    form2.all('SitUp').value="";
    form2.all('DiningNoRule').value="";
    form2.all('BadHobby').value="";
    //form3
    form3.all('CustomerNo').value="";    
    form3.all('CustomerName').readOnly=true;    
    form3.all('CustomerName').value="";  
    form3.all('AddDate').value="";              
    form3.all('GymItemCode').value = "";
    form3.all('GymItemName').value="";
    form3.all('GymItemName').readOnly=true;
    form3.all('GymFreque').value = "";
    form3.all('GymTime').value = "";
    //form4
    form4.all('CustomerNo').value="";    
    form4.all('CustomerName').readOnly=true;    
    form4.all('CustomerName').value="";  
    form4.all('AddDate').value=""; 
    form4.all('FamilyCode').value="";
    form4.all('DiseasCode').value="";
    */
   
  }
  catch(ex)
  {
    alert("在LHCustomHealthStatusInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LHCustomHealthStatusInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initLHCustomGymGrid();
    initLHCustomFamilyDiseasGrid();

    //initSelBox();    
  }
  catch(re)
  {
    alert("LHCustomHealthStatusInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
