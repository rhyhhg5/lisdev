<%
//程序名称：LHHmBespeakManageInit.jsp
//程序功能：健管服务预约管理
//创建日期：2006-09-01 15:51:32
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
	
	String ServTaskNo = request.getParameter("ServTaskNo");
	String sqlServTaskNo = "'"+ServTaskNo.replaceAll(",","','")+"'";
	String flag = request.getParameter("flag");
%>       
           
<script language="JavaScript">     
var ServTaskNo = "<%=ServTaskNo%>";			//传入的一个或多个ServTaskNo
var sqlServTaskNo = "<%=sqlServTaskNo%>";   //拼接好的可在SQL中使用的一个或多个ServTaskNo
var aResult=new Array;                              
var turnPage = new turnPageClass(); 
function initForm()
{
	fm.all('TPageTaskNo').value= "<%=sqlServTaskNo%>";
	fm.all('flag').value= "<%=flag%>";
	try
	{
	    initLHMessSendMgGrid();
	    
		//取得传入任务流水号的所有任务执行信息
		var sqlExecNo  = " select TaskExecNo, ServCaseCode, ServTaskNo,ServItemNo from LHTaskCustomerRela "
						+" where ServTaskNo in ("+"<%=sqlServTaskNo%>"+") order by ServTaskNo";
		aResult = easyExecSql(sqlExecNo);

	  var flag= "<%=flag%>";
		if(flag=="1")
	    {
	    	 if(aResult!=""&&aResult!=null&&aResult!="null")
	    	 {
	    	     var strsql="  select TaskExecNo from LHMessSendMg d where d.TaskExecNo='"+ aResult[0][0]+"'" ;
		         var result=new Array;
		         result=easyExecSql(strsql);
		         if(result=="" || result==null || result=="null")
		         {//未保存过
	    	        displayInfo();
	    	        fm.all('save').disabled=false;
	    	     }
	    	     if(result!=""&&result!="null"&&result!=null)
	    	     {
	    	     	  displayConetent();
	    	        fm.all('save').disabled=true;
	    	     }
	    	 }
	    }
	}      
  catch(re)
  {      
    alert("LHHmBespeakManageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }      
}        
function displayInfo()
{
		strsql="select w.CustomerNo,w.CustomerName,"
			  	+" (select case when a.Sex='1' then '女' else '男' end  from LDPerson a where a.CustomerNo=w.CustomerNo),"
		      +" (select a.Birthday  from LDPerson a where a.CustomerNo=w.CustomerNo),"
		      +" (select a.Mobile  from LCAddress a where a.CustomerNo = w.CustomerNo and to_number(a.addressno)"
		      +"  = (select max(to_number(addressno)) from lcaddress where customerno = w.CustomerNo)),"
		      +" (select a.PostalAddress  from LCAddress a where a.CustomerNo = w.CustomerNo and to_number(a.addressno) = "
		      +" (select max(to_number(addressno)) from lcaddress where customerno = w.CustomerNo)),"
		      +" (select  d.ServItemName from LHHealthServItem d ,LHServItem a where d.ServItemCode=a.ServItemCode and a.ServItemNo=v.ServItemNo),"
		     	+" (select a.ServItemType from LHServItem a where a.ServItemNo=v.ServItemNo),"
		      +" '','','',ServTaskNo,v.ServCaseCode,TaskExecNo,v.ServItemNo, ServTaskCode,'','','已执行','2',"   
					+" (select a.HmMessName from LHMessManage a ,lhmesssendmg b"  
					+" where a.HmMessCode = b.HmMessCode and b.servitemno = v.servitemno and b.ServTaskNo =" 
					+" (select d.ServTaskNo from LHTaskCustomerRela c , lhcasetaskrela d"
					+" where c.servitemno = v.servitemno and c.ServTaskNo = d.ServTaskNo and d.servtaskaffirm = '2'"
					+" order by d.taskfinishdate desc fetch first 1 row only))"
				  +" from LHTaskCustomerRela v,LHServCaseRela w where v.ServTaskNo in ("+ sqlServTaskNo +")"
				  +" and w.ServCaseCode=v.ServCaseCode and w.ServItemNo=v.ServItemNo with ur"
		      ;
		turnPage.pageLineNum = 15;  
		turnPage.queryModal(strsql, LHMessSendMgGrid);
}
function displayConetent()
{		
		strsql=" select v.CustomerNo ,"
		      +" (select a.Name from LDPerson a where a.CustomerNo=v.CustomerNo),"
		      +" (select case when a.Sex='1' then '女' else '男' end  from LDPerson a  where a.CustomerNo=v.CustomerNo),"
		      +" (select a.Birthday  from LDPerson a where a.CustomerNo=v.CustomerNo),"
		      +" (select a.Mobile  from LCAddress a where a.CustomerNo = v.CustomerNo and to_number(a.addressno) = (select max(to_number(addressno)) from lcaddress where customerno = v.CustomerNo)),"
		      +" (select a.PostalAddress  from LCAddress a where a.CustomerNo = v.CustomerNo and to_number(a.addressno) = (select max(to_number(addressno)) from lcaddress where customerno = v.CustomerNo)),"
		      +" (select  d.ServItemName from LHHealthServItem d ,LHServItem a where d.ServItemCode=a.ServItemCode and a.ServItemNo=v.ServItemNo),"
		      +" (select a.ServItemType from LHServItem a where a.ServItemNo=v.ServItemNo),"
		      +" v.HmMessCode,(select a.HmMessName from LHMessManage a where a.HmMessCode=v.HmMessCode),"
		      +" v.ReMarkExp, v.ServTaskNo,v.ServCaseCode,v.TaskExecNo,v.ServItemNo,v.ServTaskCode,v.makedate,v.maketime, "
		      +" (select case  when  a.TaskExecState='1' then '未执行' when a.TaskExecState='2' then '已执行'  else '' end from LHTaskCustomerRela a where a.TaskExecNo=v.TaskExecNo ),"
		      +" (select a.TaskExecState from LHTaskCustomerRela a where a.TaskExecNo=v.TaskExecNo), "
					+" (select a.HmMessName from LHMessManage a ,lhmesssendmg b"  
					+" where a.HmMessCode = b.HmMessCode and b.servitemno = v.servitemno and b.ServTaskNo =" 
					+" (select d.ServTaskNo from LHTaskCustomerRela c , lhcasetaskrela d"
					+" where c.servitemno = v.servitemno and c.ServTaskNo = d.ServTaskNo and d.servtaskaffirm = '2'"
					+" order by d.taskfinishdate desc fetch first 1 row only))"
					+"  from   LHMessSendMg v  where v.ServTaskNo in ("+ sqlServTaskNo +") with ur"
		      ;
		turnPage.pageLineNum = 15;  
		turnPage.queryModal(strsql, LHMessSendMgGrid);

}
function initLHMessSendMgGrid() 
{                               
	var iArray = new Array();
    
	try
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         		//列名
    	iArray[0][1]="30px";         		//列名
    	iArray[0][3]=0;
    	 
    	iArray[1]=new Array();
    	iArray[1][0]="客户号";         		//列名
    	iArray[1][1]="75px";         		//列名
    	iArray[1][2]=20;        
    	iArray[1][3]=0;         		//列名
    	
    	iArray[2]=new Array(); 
		iArray[2][0]="客户姓名";   
		iArray[2][1]="70px";   
		iArray[2][2]=20;        
		iArray[2][3]=0;
		
		iArray[3]=new Array(); 
		iArray[3][0]="性别";   
		iArray[3][1]="40px";   
		iArray[3][2]=20;        
		iArray[3][3]=0;
		
		iArray[4]=new Array(); 
		iArray[4][0]="出生日期";   
		iArray[4][1]="85px";   
		iArray[4][2]=20;        
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="手机号";
		iArray[5][1]="90px";         
		iArray[5][2]=20;            
		iArray[5][3]=0;       
		
		iArray[6]=new Array();
		iArray[6][0]="邮寄地址";
		iArray[6][1]="0px";         
		iArray[6][2]=20;            
		iArray[6][3]=3; 
		
	    iArray[7]=new Array(); 
	    iArray[7][0]="服务项目名称";   
	    iArray[7][1]="150px";   
	    iArray[7][2]=20;        
	    iArray[7][3]=0;
	    
	    iArray[8]=new Array(); 
	    iArray[8][0]="序号";   
	    iArray[8][1]="35px";   
	    iArray[8][2]=20;        
	    iArray[8][3]=0;
	    
	    iArray[9]=new Array(); 
	    iArray[9][0]="健康通讯代码";   
	    iArray[9][1]="0px";   
	    iArray[9][2]=20;        
	    iArray[9][3]=3;
	    
	    iArray[10]=new Array(); 
	    iArray[10][0]="健康通讯名称";   
	    iArray[10][1]="160px";   
	    iArray[10][2]=20;        
	    iArray[10][3]=0;
	    
	    iArray[11]=new Array(); 
	    iArray[11][0]="备注说明";   
	    iArray[11][1]="0px";   
	    iArray[11][2]=20;        
	    iArray[11][3]=3;
	    
	    iArray[12]=new Array(); 
	    iArray[12][0]="ServTaskNo";   
	    iArray[12][1]="0px";   
	    iArray[12][2]=20;        
	    iArray[12][3]=3;
	    
	    iArray[13]=new Array(); 
	    iArray[13][0]="ServCaseCode";   
	    iArray[13][1]="0px";   
	    iArray[13][2]=20;        
	    iArray[13][3]=3;
	    
	    iArray[14]=new Array(); 
	    iArray[14][0]="TaskExecNo";   
	    iArray[14][1]="0px";   
	    iArray[14][2]=20;        
	    iArray[14][3]=3;
	    
	    iArray[15]=new Array(); 
	    iArray[15][0]="ServItemNo";   
	    iArray[15][1]="0px";   
	    iArray[15][2]=20;        
	    iArray[15][3]=3;
	    
	    	  
	    iArray[16]=new Array(); 
	    iArray[16][0]="ServTaskCode";   
	    iArray[16][1]="0px";   
	    iArray[16][2]=20;        
	    iArray[16][3]=3;
	  
		  iArray[17]=new Array(); 
	    iArray[17][0]="MakeDate";   
	    iArray[17][1]="60px";   
	    iArray[17][2]=20;        
	    iArray[17][3]=3;
	    
	    	  
	    iArray[18]=new Array(); 
	    iArray[18][0]="MakeTime";   
	    iArray[18][1]="60px";   
	    iArray[18][2]=20;        
	    iArray[18][3]=3;
	  
	  	iArray[19]=new Array(); 
		  iArray[19][0]="执行状态";   
		  iArray[19][1]="55px";   
		  iArray[19][2]=20;        
		  iArray[19][3]=2;
		  iArray[19][4]="taskexecstuas";
		  iArray[19][5]="19|20";     //引用代码对应第几列，'|'为分割符
    	iArray[19][6]="1|0";     //上面的列中放置引用代码中第几位
    	iArray[19][14]="已执行"; 
    	iArray[19][17]="1"; 
    	iArray[19][18]="120";
    	iArray[19][19]="1" ;
    
    	iArray[20]=new Array(); 
	    iArray[20][0]="TaskExecState";   
	    iArray[20][1]="0px";   
	    iArray[20][2]=20;        
	    iArray[20][3]=3;
	    
	    iArray[21]=new Array(); 
	    iArray[21][0]="最近发送通讯名称";   
	    iArray[21][1]="160px";   
	    iArray[21][2]=20;        
	    iArray[21][3]=0;
	  
		LHMessSendMgGrid = new MulLineEnter( "fm" , "LHMessSendMgGrid" ); 
    	//这些属性必须在loadMulLine前
    	LHMessSendMgGrid.mulLineCount = 0;   
    	LHMessSendMgGrid.displayTitle = 1;
    	LHMessSendMgGrid.hiddenPlus = 1;
    	LHMessSendMgGrid.hiddenSubtraction = 1;
    	LHMessSendMgGrid.canSel = 0;
    	LHMessSendMgGrid.canChk = 1;
    	LHMessSendMgGrid.chkBoxEventFuncName = "showOne";  
    	LHMessSendMgGrid.loadMulLine(iArray);  
    	//这些操作必须在loadMulLine后面
	}
	catch(ex) 
	{ alert(ex);}
} 

</script>