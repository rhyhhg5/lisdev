<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-03-03 20:24:53
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LDICDDiseasTypeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LDICDDiseasTypeInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LDICDDiseasTypeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDICDDiseasType1);">
    		</td>
    		 <td class= titleImg>
        		 ICD疾病分类基本信息
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLDICDDiseasType1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      代码分类等级
    </TD>
    <TD  class= input>
      <Input class= 'codeno' name=DiseasTypeLevel  verify="代码分类等级|notnull&len<=1" CodeData= "0|^1|1级代码^2|2级代码" ondblClick= "showCodeListEx('DiseasTypeLevel',[this,DiseasTypeLevel_origin],[0,1],null,null,null,1);"  onkeyup= "showCodeListKeyEx('DiseasTypeLevel',[this,DiseasTypeLevel_origin],[0,1],null,null,null,1);"><Input class=codename name=DiseasTypeLevel_origin elementtype=nacessary>
    </TD>
    <TD  class= title>
      分类起始代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICDBeginCode elementtype=nacessary verify="分类起始代码|notnull&len<=3">
      <Input type=hidden name=ICDBeginCode_origin >
    </TD>
    <TD  class= title>
      分类终止代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ICDEndCode elementtype=nacessary verify="分类终止代码|notnull&len<=3">
      <Input type=hidden name=ICDEndCode_origin >
    </TD>
  <TR  class= common>  
    <TD  class= title>
      疾病分类名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DiseasType elementtype=nacessary verify="疾病分类名称|notnull&len<=50">
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
