<%
//程序名称：LDClassInfoQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-05-30 18:29:02
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ClassInfoCode').value = "";
    fm.all('ClassCode').value = "";
    fm.all('ClassName').value = "";
    fm.all('ClassLevel').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LDClassInfoQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLDClassInfoGrid();  
  }
  catch(re) {
    alert("LDClassInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LDClassInfoGrid;
function initLDClassInfoGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();                    
		iArray[1][0]="类别代码";         		//列名    
		iArray[1][1]="100px";         		//列名    
		iArray[1][3]=0;         		//列名        
		
		iArray[2]=new Array();                    
		iArray[2][0]="类别名称";         		//列名    
		iArray[2][1]="100px";         		//列名    
		iArray[2][3]=0;         		//列名        
		
		iArray[3]=new Array();                    
		iArray[3][0]="类别等级";         		//列名    
		iArray[3][1]="100px";         		//列名    
		iArray[3][3]=0;         		//列名   
		
		iArray[4]=new Array();  
		iArray[4][0]="Operator";
		iArray[4][1]="0px";   
		iArray[4][3]=3;         
		
		iArray[5]=new Array();  
		iArray[5][0]="MakeDate";
		iArray[5][1]="0px";   
		iArray[5][3]=3;         
		
		iArray[6]=new Array();  
		iArray[6][0]="MakeTime";
		iArray[6][1]="0px";   
		iArray[6][3]=3;         
		     
    
    LDClassInfoGrid = new MulLineEnter( "fm" , "LDClassInfoGrid" ); 
    //这些属性必须在loadMulLine前           
    
        LDClassInfoGrid.canSel = 1;
				LDClassInfoGrid.hiddenPlus = 1;
  		  LDClassInfoGrid.hiddenSubtraction = 1;
/*
    LDClassInfoGrid.mulLineCount = 0;   
    LDClassInfoGrid.displayTitle = 1;
    LDClassInfoGrid.canSel = 1;
    LDClassInfoGrid.canChk = 0;
    LDClassInfoGrid.selBoxEventFuncName = "showOne";
*/
    LDClassInfoGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LDClassInfoGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
