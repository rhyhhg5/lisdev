<%
//程序名称：LDICDOPSInput.jsp
//程序功能：功能描述
//创建日期：2005-03-08 17:08:58
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LDICDOPSQueryInput.js"></SCRIPT> 
  <%@include file="LDICDOPSQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLDICDOPSGrid1" style= "display: ''">    
<table  class= common align='center' >
   <TR  class= common>
    <TD  class= title>
      手术分类代码
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ICDOPSCode ondblclick=" return showCodeList('ldopsname',[this,ICDOPSName],[0,1],null,fm.ICDOPSCode.value,'ICDOPSCode',1,350);" onkeyup=" if(event.keyCode == 13) return showCodeList('ldopsname',[this,ICDOPSName],[0,1],null,fm.ICDOPSCode.value,'ICDOPSCode',1,350);" verify="手术ICD代码|len<=18" >
    </TD>
    <TD  class= title>
      手术分类名称
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ICDOPSName ondblclick=" return showCodeList('ldopsname',[this,ICDOPSCode],[1,0],null,fm.ICDOPSName.value,'ICDOPSName',1,350);" onkeyup=" if(event.keyCode == 13) return showCodeList('ldopsname',[this,ICDOPSCode],[1,0],null,fm.ICDOPSName.value,'ICDOPSName',1,350);" verify="手术标准名称|len<=30">
    </TD>
    <TD  class= title>
      代码等级
    </TD>
    <TD  class= input>
      <Input class= 'code' name=OpsGrag elementtype=nacessary verify="|len<=2" CodeData= "0|^2|2级代码^3|3级代码^4|4级代码^5|5级代码" ondblClick= "showCodeListEx('OpsGrag',[this,''],[0,1],null,null,null,1);"  >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      手术风险等级
    </TD>
    <TD  class= input>
      <Input class= 'code' name=FrequencyFlag verify="|len<=2" CodeData= "0|^0|未分类^1|1类手术^2|2类手术^3|3类手术^4|4类手术" ondblClick= "showCodeListEx('',[this,''],[0,1],null,null,null,1);" >
    </TD>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssButton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssButton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDICDOPS1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLDICDOPS1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLDICDOPSGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
