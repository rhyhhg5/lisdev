<%
//程序名称：LHGrpServPlanInput.jsp
//程序功能：
//创建日期：2006-03-09 11:34:17
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('GrpServPlanNo').value = "";
    fm.all('GrpContNo').value = "";
    fm.all('GrpCustomerNo').value = "";
    fm.all('GrpName').value = "";
    fm.all('ServPlanCode').value = "";
    fm.all('ServPlanName').value = "";
    fm.all('ServPlanLevel').value = "";
    fm.all('ServPlayType').value = "";
    fm.all('ComID').value = "";
    fm.all('StartDate').value = "";
    fm.all('EndDate').value = "";
    fm.all('ServPrem').value = "";
    fm.all('ManageCom').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
  }
  catch(ex)
  {
    alert("在LHGrpServPlanInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {}
  catch(ex)
  {
    alert("在LHGrpServPlanInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initLHGrpServPlanGrid();
  }
  catch(re)
  {
    alert("LHGrpServPlanInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHGrpServPlanGrid;
function initLHGrpServPlanGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";			//列名
    
    iArray[1]=new Array(); 
	iArray[1][0]="团体内客户分级标识";   
	iArray[1][1]="50px";   
	iArray[1][2]=20;        
	iArray[1][3]=1;
	
	iArray[2]=new Array(); 
	iArray[2][0]="健管分保费（元）";   
	iArray[2][1]="50px";   
	iArray[2][2]=20;        
	iArray[2][3]=1;
	
	iArray[3]=new Array();                               
  iArray[3][0]="团体服务计划号码";         		//列名  
  iArray[3][1]="0px";         		//列名   
  iArray[2][2]=20;              
  iArray[3][3]=3;         		//列名         
  
  iArray[4]=new Array();                               
  iArray[4][0]="加入日期";         		//列名  
  iArray[4][1]="0px";         		//列名   
  iArray[4][2]=20;              
  iArray[4][3]=3;         		//列名   
  
  iArray[5]=new Array();                               
  iArray[5][0]="加入时间";         		//列名  
  iArray[5][1]="0px";         		//列名   
  iArray[5][2]=20;              
  iArray[5][3]=3;         		//列名  
  
  iArray[6]=new Array();                               
  iArray[6][0]="约定客户人数";         		//列名  
  iArray[6][1]="40px";         		//列名   
  iArray[6][2]=20;              
  iArray[6][3]=1;
  
  iArray[7]=new Array();                               
  iArray[7][0]="实际客户人数";         		//列名  
  iArray[7][1]="0px";         		//列名   
  iArray[7][2]=20;              
  iArray[7][3]=3;
  
  iArray[8]=new Array();                               
  iArray[8][0]="档次说明";         		//列名  
  iArray[8][1]="180px";         		//列名   
  iArray[8][2]=1000;              
  iArray[8][3]=1;           
    
    LHGrpServPlanGrid = new MulLineEnter( "fm" , "LHGrpServPlanGrid" ); 
    //这些属性必须在loadMulLine前
  
    LHGrpServPlanGrid.mulLineCount = 0;   
    LHGrpServPlanGrid.displayTitle = 1;
    LHGrpServPlanGrid.hiddenPlus = 0;
    LHGrpServPlanGrid.hiddenSubtraction = 0;
    LHGrpServPlanGrid.canSel = 1;
    LHGrpServPlanGrid.canChk = 0;
    //LHGrpServPlanGrid.selBoxEventFuncName = "showOne";

    LHGrpServPlanGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHGrpServPlanGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
