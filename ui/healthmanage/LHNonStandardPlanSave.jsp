<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHNonStandardPlanSave.jsp
//程序功能：
//创建日期：2006-11-21 9:30:57
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
    //接收信息，并作校验处理。
    //输入参数
    String inServPlanNo="";

    LHServPlanSchema tLHServPlanSchema   = new LHServPlanSchema();
    OLHServPlanUI tOLHServPlanUI   = new OLHServPlanUI();
    LHServItemSet tLHServItemSet = new LHServItemSet();
    String tServItemCode[] = request.getParameterValues("LHServPlanGrid1");		//MulLine的列存储数组
    String tServItemNo[] = request.getParameterValues("LHServPlanGrid3");		//MulLine的列存储数组
    String tServItemType[] = request.getParameterValues("LHServPlanGrid4");
    String tComID[] = request.getParameterValues("LHServPlanGrid6");			//MulLine的列存储数组
    String tServPriceCode[] = request.getParameterValues("LHServPlanGrid7");	//定价方式代码
    String tServCaseType[] = request.getParameterValues("LHServPlanGrid9");		//服务事件类型
    LHServExeTraceSet tLHServExeTraceSet = new LHServExeTraceSet();	

    //输出参数
    CErrors tError = null;
    String tRela  = "";                
    String FlagStr = "";
    String Content = "";
    String transact = "";
    GlobalInput tG = new GlobalInput(); 
    tG=(GlobalInput)session.getValue("GI");

    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    tLHServPlanSchema.setGrpServPlanNo(request.getParameter("GrpServPlanNo"));
    tLHServPlanSchema.setServPlanNo(request.getParameter("ServPlanNo"));
    tLHServPlanSchema.setContNo(request.getParameter("ContNo"));
    tLHServPlanSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLHServPlanSchema.setName(request.getParameter("CustomerName"));
    tLHServPlanSchema.setServPlanCode(request.getParameter("ServPlanCode"));
    tLHServPlanSchema.setServPlanName(request.getParameter("ServPlanName"));
    tLHServPlanSchema.setServPlanLevel(request.getParameter("ServPlanLevel"));	//服务计划档次
    tLHServPlanSchema.setServPlayType(request.getParameter("ServPlayType"));	//服务计划类型
    tLHServPlanSchema.setComID(request.getParameter("ComID"));
    tLHServPlanSchema.setStartDate(request.getParameter("StartDate"));
    tLHServPlanSchema.setEndDate(request.getParameter("EndDate"));
    tLHServPlanSchema.setServPrem(request.getParameter("ServPrem"));			//健管保费 
    tLHServPlanSchema.setCaseState(request.getParameter("CaseState"));		//契约事件状态
    tLHServPlanSchema.setManageCom(request.getParameter("ManageCom"));
    tLHServPlanSchema.setOperator(request.getParameter("Operator"));
    tLHServPlanSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHServPlanSchema.setMakeTime(request.getParameter("MakeTime"));

    String CaseState=request.getParameter("CaseStateName");    //契约事件状态为1
	int LHServItemCount = 0;
	if(tServItemCode != null)
	{	
		LHServItemCount = tServItemCode.length;
	}	
	
	for(int i = 0; i < LHServItemCount; i++)
	{
		LHServItemSchema tLHServItemSchema = new LHServItemSchema();
		
		tLHServItemSchema.setServItemCode(tServItemCode[i]);	
		tLHServItemSchema.setServItemNo(tServItemNo[i]);	
		tLHServItemSchema.setServItemType(tServItemType[i]);
		tLHServItemSchema.setComID(tComID[i]);	
		tLHServItemSchema.setServPriceCode(tServPriceCode[i]);
		tLHServItemSchema.setServCaseType(tServCaseType[i]);
		tLHServItemSchema.setOperator(request.getParameter("Operator"));
		tLHServItemSchema.setMakeDate(request.getParameter("MakeDate"));
		tLHServItemSchema.setMakeTime(request.getParameter("MakeTime"));
		tLHServPlanSchema.setManageCom(request.getParameter("ManageCom")); 
    	                       
		tLHServItemSet.add(tLHServItemSchema);             
	}
    
	//for(int i = 0; i < LHServItemCount; i++)
	//{
	//	LHServExeTraceSchema tLHServExeTraceSchema = new LHServExeTraceSchema();
	//			
	//	tLHServExeTraceSchema.setServItemNo(tServItemNo[i]);	
	//	tLHServExeTraceSchema.setOperator(request.getParameter("Operator"));
	//	tLHServExeTraceSchema.setMakeDate(request.getParameter("MakeDate"));
	//	tLHServExeTraceSchema.setMakeTime(request.getParameter("MakeTime"));
	//	tLHServExeTraceSchema.setManageCom(request.getParameter("ManageCom"));                            
	//	                    
	//	tLHServExeTraceSet.add(tLHServExeTraceSchema);                    
	//}  
    try
    {
    	// 准备传输数据 VData
    	VData tVData = new VData();
	    tVData.add(tLHServPlanSchema);
	    tVData.add(tLHServItemSet);
	    //tVData.add(tLHServExeTraceSet);
		tVData.add(tG);
		tOLHServPlanUI.submitData(tVData,transact);	     
    }
    catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tOLHServPlanUI.mErrors;
	if (!tError.needDealError())
	{                          
		Content = " 操作成功! ";
		FlagStr = "Success";
	}
	else                                                                           
	{
		Content = " 操作失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
  System.out.println("=================FlagStr"+FlagStr); 
  //添加各种预处理
  
if ( FlagStr.equals("Success") && ("INSERT||MAIN".equals(transact) || "UPDATE||MAIN".equals(transact)))
{
    VData res = tOLHServPlanUI.getResult();
    LHServPlanSchema mLHServPlanSchema = new LHServPlanSchema();

    mLHServPlanSchema.setSchema((LHServPlanSchema)res.getObjectByObjectName("LHServPlanSchema",0));
    inServPlanNo = (String)mLHServPlanSchema.getServPlanNo();
}

%>                      
<%=Content%>
<html>
<script language="javascript">
		<%if(inServPlanNo.equals(""))
		{%>
			parent.fraInterface.fm.all("ServPlanNo").value="";
			parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=inServPlanNo%>");
       <%}
       else
       {%>	
	 		parent.fraInterface.fm.all("ServPlanNo").value = "<%=inServPlanNo%>";
			parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=inServPlanNo%>");
	  <%}%>	
	  
</script>
</html>
