<%
//程序名称：LHGrpNonStandardUnitInit.jsp
//程序功能：
//创建日期：2006-12-14 15:52:50
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                        
<script language="JavaScript">
function initInpBox()
{ 
  try
  {
  }
  catch(ex)
  {
    alert("在LHGrpNonStandardUnitInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {}
  catch(ex)
  {
    alert("在LHGrpNonStandardUnitInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    //initInpBox();
    initLHGrpCustomerGrid();
  }
  catch(re)
  {
    alert("LHGrpNonStandardUnitInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var LHGrpCustomerGrid;
function initLHGrpCustomerGrid() 
{                               
  var iArray = new Array();

  try {
         iArray[0]=new Array();
         iArray[0][0]="序号";         		//列名
         iArray[0][1]="40px";         		//列名
         iArray[0][3]=0;         		//列名
         iArray[0][4]="station";         		//列名
         
         iArray[1]=new Array(); 
	       iArray[1][0]="客户号";   
	       iArray[1][1]="60px";   
	       iArray[1][2]=20;        
	       iArray[1][3]=1;
	       
	       iArray[2]=new Array(); 
	       iArray[2][0]="客户姓名";   
	       iArray[2][1]="120px";   
	       iArray[2][2]=20;        
	       iArray[2][3]=1;
	       
	       iArray[3]=new Array(); 
	       iArray[3][0]="团体内客户分档";   
	       iArray[3][1]="80px";   
	       iArray[3][2]=20;        
	       iArray[3][3]=0;
	       iArray[3][14]="<%=request.getParameter("Level")%>";
	       
         iArray[4]=new Array(); 
         iArray[4][0]="入机日期";   
         iArray[4][1]="0px";   
         iArray[4][2]=20;        
         iArray[4][3]=3;
         
         iArray[5]=new Array(); 
         iArray[5][0]="入机时间";   
         iArray[5][1]="0px";   
         iArray[5][2]=20;        
         iArray[5][3]=3;
         LHGrpCustomerGrid = new MulLineEnter( "fm" , "LHGrpCustomerGrid" ); 
    //这些属性必须在loadMulLine前

    LHGrpCustomerGrid.mulLineCount = 0;   
    LHGrpCustomerGrid.displayTitle = 1;
    LHGrpCustomerGrid.hiddenPlus = 1;
    LHGrpCustomerGrid.hiddenSubtraction = 1;
    LHGrpCustomerGrid.canSel = 0;
    LHGrpCustomerGrid.canChk = 0;
    //LHGrpCustomerGrid.selBoxEventFuncName = "showOne";

    LHGrpCustomerGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LHGrpCustomerGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
