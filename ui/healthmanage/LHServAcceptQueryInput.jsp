<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHServAcceptQueryInput.jsp
//程序功能：
//创建日期：2006-08-26 15:54:55
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="LHServAcceptQueryInput.js"></SCRIPT>
	<%@include file="LHServAcceptQueryInit.jsp"%>
</head>
<body  onload="initForm();" >
<form action="" method=post name=fm target="fraSubmit">
<%@include file="../common/jsp/InputButton.jsp"%>
<div id= "divLHBaseInfoGrid" style="display:''">
    <table>
    	<tr>
    		 <td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHBaseInfo);">
    	 	 </td>
    	   <td class= titleImg>
        	 基本信息
       	 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHBaseInfo" style= "display: ''">
		<table  class= common align='center' >
			<TR  class= common>
				<TD class= title >
          		客户类型
       	  </TD>   		 
				  <TD  class= input>
				    <Input class= 'codename' style="width:40px" name=CustomType value='1'><Input class= 'codeno' style="width:120px"  name=CustomTypeName value='个人客户' ondblclick="showCodeList('lhcustomtype',[this,CustomType],[1,0],null,null,null,'1',160);" codeClear(CustomType,CustomTypeName);">
				  </TD>
    		  <TD  class= title>
            客户号码
          </TD>
          <TD  class= input>
             <Input class= 'code' name=CustomerNo ondblclick=" return showCodeList('hmldpersonname',[this,CustomerName],[1,0],null,fm.CustomerNo.value,'CustomerNo');"  onkeyup=" if( event.keyCode==13 ) return showCodeList('hmldperson',[this,CustomerName],[0,1],null,fm.CustomerNo.value,'CustomerNo');" verify="客户姓名|len<=24">
          </TD>
          <TD  class= title>
            客户姓名
          </TD>
          <TD  class= input>
             <Input class= 'common' name=CustomerName readonly>
          </TD>

			</TR> 
			<TR  class= common>
					<TD class= title >
          		 服务受理渠道
       	  </TD>   		 
				  <TD  class= input>
				    <Input class= 'codename' style="width:40px" name=ServAccepChannel><Input class= 'codeno' style="width:120px"  name=ServAccepChannelName  ondblclick="showCodeList('lhacceptchannel',[this,ServAccepChannel],[1,0],null,null,null,'1',160);" codeClear(ServAccepChannel,ServAccepChannelName);">
				  </TD>
			  <TD  class= title style="width:120">
           服务项目代码
        </TD>
        <TD  class= input>
           <Input class= 'code' name=ServItemCode   ondblclick="showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode','1',220);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemName],[0,1],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
        </TD>
         <TD  class= title style="width:120">
           服务项目名称
        </TD>
        <TD  class= input>
           <Input class= 'code' name=ServItemName   ondblclick="showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode','1',220);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode');"   onkeydown="return showCodeListKey('hmservitemcode',[this,ServItemCode],[1,0],null,fm.ServItemCode.value,'ServItemCode'); codeClear(ServItemName,ServItemCode);">
        </TD>
			</TR> 
				<TR  class= common>  
				<TD  class= title>
					服务受理起始时间
				</TD>
        <TD  class= input>
           <Input name=ServAceptStartDate class='coolDatePicker' dateFormat='short' style="width:142px" > 
        </TD> 
				<TD  class= title>
					服务受理终止时间
				</TD>
        <TD  class= input>
           <Input name=ServAceptEndDate class='coolDatePicker' dateFormat='short' style="width:142px" > 
        </TD> 
	   	  <TD class= title  type=hidden >
          		<!-- 服务执行情况-->
        </TD>   		 
			  <TD  class= input>
				    <Input class= 'codename'  type=hidden   style="width:40px" name=ServExecCircs><Input class= 'codeno' style="width:120px"   type=hidden  name=ServExecCircsName  ondblclick="showCodeList('lhservexeccircs',[this,ServExecCircs],[1,0],null,null,null,'1',160);" codeClear(ServExecCircs,ServExecCircsName);">
			  </TD>
			  </TR> 
		</table>
</Div> 
    <Div id="divLHServExecInfo" style="display:''"> 
	<table  class= common align='center' >
	   <TR  class= common>
			<td>
           <input type= button class=cssButton name=ExecServItem  value="查 询" OnClick="ServAcceptQuery();">
       </td> 
       <td>
           <input type= button class=cssButton name=IndiInfoLook  value="返 回" OnClick="returnParent();">
       </td> 
       <td style="width:4700px;">
		  </td>
	  </tr>
	</table>
</div>
<Div id="divServAcceptGrid" style="display:''">
	 <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanServAcceptGrid">
	  				</span> 
			    </td>
				</tr>
	 </table>
</div>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="fmAction" name="fmAction">
		<Input class= 'common' type=hidden name=ManageCom >
	  <Input class= 'common' type=hidden name=Operator >
	  <Input class= 'common' type=hidden name=MakeDate >
	  <Input class= 'common' type=hidden name=MakeTime >
	  <Input class= 'common' type=hidden name=ServTaskNo >
	  <Input class= 'common' type=hidden name=ServCaseCode >
	  <Input class= 'common' type=hidden name=ServItemNO >
	  <Input class= 'common' type=hidden name=HidCusNo >
	  <Input class= 'common' type=hidden name=HidItemCode >
	  <Input class= 'common' type=hidden name=accFlag >
  	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
</html>
