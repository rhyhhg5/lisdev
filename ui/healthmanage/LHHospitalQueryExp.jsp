<%
//程序名称：LLWorkEfficientStaSub.jsp
//程序功能：F1报表生成
//创建日期：2004-04-16
//创建人  ：DX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
	<title>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	</title>
	<head>
			<SCRIPT src="LHHospitalInfoQuery.js"></SCRIPT>
	</head>
<body>
<%

String flag = "0";
String FlagStr = "";
String Content = "";

//设置模板名称
String StartDate = "";
String EndDate   = "";
String RiskCodeInfo = "";
String HospitalCode = "";

String FileName ="LHHospitalQueryExp";
StartDate = request.getParameter("InHospitStartDate");
EndDate   = request.getParameter("InHospitEndDate");
RiskCodeInfo = request.getParameter("RiskcodeInfo");
HospitalCode = request.getParameter("HospitCode");


if(!StartDate.equals("") && !EndDate.equals(""))
{
	String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
	String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");
	
	if( !sd.equals("") && !ed.equals(""))
	{
		if(sd.compareTo(ed) > 0)
		{
			flag = "1";
			FlagStr = "Fail";
			Content = "操作失败，原因是:统计止期比统计起期早";
		}
	}
}

JRptList t_Rpt = new JRptList();
String tOutFileName = "";
String sqlStartDate = " ";
String sqlEndDate = " ";
String sqlRiskCodeInfo = " ";
String sqlHospitalCode = " ";
String YYMMDD = "";
String hospitalName = "";

if(flag.equals("0"))
{
	t_Rpt.m_NeedProcess = false;
	t_Rpt.m_Need_Preview = false;
	t_Rpt.mNeedExcel = true;
	
	if(!HospitalCode.equals(""))
	{
		hospitalName = new ExeSQL().getOneValue(" select hospitname from ldhospital where hospitcode = '"+HospitalCode+"' ");
		sqlHospitalCode = " and m.hospitalcode = '"+HospitalCode+"' ";
	}
	t_Rpt.AddVar("sqlHospitalCode", sqlHospitalCode);
	t_Rpt.AddVar("HospitalName", hospitalName);
	
	if(!RiskCodeInfo.equals(""))
	{
		sqlRiskCodeInfo = " and p.riskcode in ("+RiskCodeInfo+") ";
	}
	t_Rpt.AddVar("sqlRiskInfo", sqlRiskCodeInfo);
	t_Rpt.AddVar("RiskInfo", RiskCodeInfo);
	
	if(!StartDate.equals(""))
	{
		//起始日期
		StartDate = AgentPubFun.formatDate(StartDate, "yyyy-MM-dd");
		t_Rpt.AddVar("StartDate", StartDate);
		
		YYMMDD = StartDate.substring(0, StartDate.indexOf("-")) + "年"
		       + StartDate.substring(StartDate.indexOf("-") + 1,StartDate.lastIndexOf("-")) + "月"
		       + StartDate.substring(StartDate.lastIndexOf("-") + 1) + "日";
		t_Rpt.AddVar("StartDateN", YYMMDD);
		
		sqlStartDate = " and m.Feedate >= '"+StartDate+"' ";
	}
	t_Rpt.AddVar("sqlStartDate", sqlStartDate);
	
	//终止日期
	if(!EndDate.equals(""))
	{
		EndDate = AgentPubFun.formatDate(EndDate, "yyyy-MM-dd");
		t_Rpt.AddVar("EndDate", EndDate);
		YYMMDD = "";
		YYMMDD = EndDate.substring(0, EndDate.indexOf("-")) + "年"
		       + EndDate.substring(EndDate.indexOf("-") + 1,EndDate.lastIndexOf("-")) + "月"
		       + EndDate.substring(EndDate.lastIndexOf("-") + 1) + "日";
		t_Rpt.AddVar("EndDateN", YYMMDD);
		sqlEndDate = " and m.Feedate <= '"+EndDate+"' ";
	}
	t_Rpt.AddVar("sqlEndDate", sqlEndDate);	
	
	//当前日期
	String CurrentDate = PubFun.getCurrentDate();
	CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
	YYMMDD = "";
	YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
	       + CurrentDate.substring(CurrentDate.indexOf("-") + 1,CurrentDate.lastIndexOf("-")) + "月"
	       + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) + "日";
	t_Rpt.AddVar("MakeDate", YYMMDD);	
//	t_Rpt.AddVar("MngComName", ReportPubFun.getMngName(magcom));

	t_Rpt.Prt_RptList(pageContext,FileName);
	tOutFileName = t_Rpt.mOutWebReportURL;
	String strVFFileName = FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
	String strRealPath = application.getRealPath("/web/Generated").replace('\\','/');
	String strVFPathName = strRealPath +"/"+ strVFFileName;
	System.out.println("strVFPathName : "+ strVFPathName);
	System.out.println("=======Finshed in JSP===========");
	System.out.println(tOutFileName);
	
	response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+"&RealPath="+strVFPathName);
}
%>
</body>
<script language="javascript">
	//parent.fraInterface.afterSubmit2();
</script>
</html>

