//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var StrSql;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHIndivServAcceptInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
    //showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
function ServAcceptQuery()
{
	if(fm.all('CustomType').value=="1")//个人客户
	{
	   var ServItemName="";
  	 if(fm.all('ServItemName').value != "")
  	 {
  	 	  ServItemName =" and b.ServItemCode in ( select distinct m.ServItemCode from  LHHealthServItem m  where  m.ServItemName ='"+ fm.all('ServItemName').value +"')";	
  	 }
  	 	var CustomerName="";
  	 if(fm.all('CustomerName').value != "")
  	 {
  	 	  CustomerName =" and a.CustomerNo in (select distinct m.CustomerNo from ldperson m where  m.Name ='"+ fm.all('CustomerName').value +"')";	
  	 }
	   var mulSql = "select "
	        +" distinct a.CustomerNo,"
				  +" (select d.Name from ldperson d where d.CustomerNo=a.CustomerNo),"
	        +"  b.ServItemCode, "
	        +"  ( select distinct c.ServItemName from   LHHealthServItem c  where  c.ServItemCode = b.ServItemCode),"
	        +" a.ServAceptDate, "
	        +" '',"
	        +" a.ServAccepChannel ,"
	        +" a.ServAccepNo,a.ServCaseCode,a.ModelTypeNo "
	        + " from LHIndiServAccept a ,LHServCaseRela b "
	        + " where  b.ServCaseCode = a.ServCaseCode "
	        + " and b.ServCaseCode in a.ServCaseCode   "
	        + " and b.ServItemCode = a.ServItemCode   "
	        +" and 1=1 "
	        + getWherePart("a.CustomerNo","CustomerNo") 
	        //+ getWherePart("b.CustomerName","CustomerName") 
	        + getWherePart("b.ServItemCode","ServItemCode") 
	        + getWherePart("a.ServAceptDate","ServAceptStartDate",">") 
	        + getWherePart("a.ServAceptDate","ServAceptEndDate","<")
	        + getWherePart("a.ServAccepChannel","ServAccepChannel")  
	        + ServItemName
	        //+ CustomerName
	        ;
			//alert(mulSql);
			//alert(easyExecSql(mulSql));
			turnPage.queryModal(mulSql, ServAcceptGrid);   
	} 
	if(fm.all('CustomType').value=="2")//团体客户
	{
		 initServAcceptGrid();
	}  
}
function returnAccept()
{
	  
}
function returnParent()
{
  var arrReturn = new Array();
	var tSel = ServAcceptGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				//alert(fm.all('accFlag').value);
				//alert(arrReturn);
				if(fm.all('accFlag').value=="1")
				{
					 var ServAccepNo = ServAcceptGrid.getRowColData(ServAcceptGrid.getSelNo()-1,8);
					 //alert(ServAccepNo);
					 window.open("./LHIndivServAcceptInput.jsp?ServAccepNo="+ServAccepNo+"&test=1","返回受理页面",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
				}
				else
				{
				    top.opener.afterQuery2( arrReturn );
				}
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = ServAcceptGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	return arrSelected;
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = ServAcceptGrid.getRowData(tRow-1);
	//alert(arrSelected);
	return arrSelected;
}
 
            
        
