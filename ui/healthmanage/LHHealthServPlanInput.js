//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass(); 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	var i = 0;
	if( verifyInput2() == false ) return false;
	fm.fmtransact.value="INSERT||MAIN";
	var mulSql = "select distinct ServPlanCode,ServPlanLevel,ComID from  LHHealthServPlan ";		    
	var arrResult=easyExecSql(mulSql);
    if(arrResult!="" && arrResult!=null && arrResult!=" ")
    {
		var reslut=new Array;
		var reslut=arrResult; 
		for(var i=0;i<arrResult.length;i++)
		{
			if(fm.all('Riskcode').value==reslut[i][0] && fm.all('ServPlanLevelCode').value==reslut[i][1] && fm.all('ComIDCode').value==reslut[i][2])
			{
             	alert("对不起,一个地区固定产品的固定档次只能有一种机构属性标识!");
             	return false;
           	}
	    }
	}
	   
	//如果计划类型按保费，则进行相应处理
	if(fm.all('PlanType').value == "2")
	{ 
		var FeeSelNo = LHFeeGrid.getSelNo();
		if(FeeSelNo == 0)
		{
			alert("请选择一个保费区间");
			return false;
		}
		
		fm.all('FeeUpLimit').value = LHFeeGrid.getRowColData(FeeSelNo-1,1);
		fm.all('FeeLowLimit').value = LHFeeGrid.getRowColData(FeeSelNo-1,2);
//		fm.all('ServPlanLevelCode').value = LHFeeGrid.getRowColData(FeeSelNo-1,3)==""?"0":LHFeeGrid.getRowColData(FeeSelNo-1,3);
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit(); //提交
}    
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
  }
}


function afterSubmitFee( FlagStr, content, rLevel )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
    if(fm.all('PlanType').value == "2")
    {
    	LHFeeGrid.setRowColData(LHFeeGrid.getSelNo()-1,3,rLevel);
    	showOneFee();	
    }
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHHealthServPlanInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	//下面增加相应的代码
	if (confirm("您确实想修改该记录吗?"))
	{
		if( verifyInput2() == false ) return false;
		//如果计划类型按保费，则进行相应处理
		if(fm.all('PlanType').value == "2")
		{ 
			var FeeSelNo = LHFeeGrid.getSelNo();
			if(FeeSelNo == 0)
			{
				alert("请选择一个保费区间");
				return false;
			}
			
			fm.all('FeeUpLimit').value = LHFeeGrid.getRowColData(FeeSelNo-1,1);
			fm.all('FeeLowLimit').value = LHFeeGrid.getRowColData(FeeSelNo-1,2);
//			fm.all('ServPlanLevelCode').value = LHFeeGrid.getRowColData(FeeSelNo-1,3)==""?"0":LHFeeGrid.getRowColData(FeeSelNo-1,3);
		}

  		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  		fm.fmtransact.value = "UPDATE||MAIN";
  		fm.submit(); //提交
	}
	else
	{
    	//mOperate="";
    	alert("您取消了修改操作！");
	}
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  initForm();
  showInfo=window.open("./LHHealthServPlanQueryInput.html","信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  	if(fm.all('PlanType').value == "2")
  	{
  		if(LHFeeGrid.getSelNo() == 0)
  		{
  			alert("请先选择一个保费区间");	
  			return false;
  		}
  	}
  	var i = 0;
  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  	
  	fm.fmtransact.value = "DELETE||MAIN";
  	fm.submit(); //提交
  	initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function afterQuery0( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('PlanType').value = arrResult[0][6];
		//alert("arrResult[0][0]="+arrResult[0][0]);

		if(arrResult[0][6] == "1")
		{
			divLHFeeGrid.style.display = "none";
			var mulSql = "select distinct ServPlanCode,ServPlanName,"
					+"ServPlanLevel,ComID,"                                  
					+"(select  CodeName from LDCode where codetype = 'servplanlevel' and ldcode.code=a.ServPlanLevel),"
					+"(select showname from ldcom  where  ldcom.comcode=a.ComID),"
					+" ServPlayType,ServItemCode, PlanType "
					+" from  LHHealthServPlan a  "
					+" where a.ServPlanCode ='"+arrResult[0][0]+"'and a.ServPlanLevel='"+arrQueryResult[0][4]+"'"
					+" and ComID='"+ arrQueryResult[0][5]+"'"
					;		 
			arrResult=easyExecSql(mulSql);
        	fm.all('Riskcode').value		= arrResult[0][0];
        	fm.all('Riskname').value		= arrResult[0][1];
        	fm.all('ServPlanLevelCode').value   = arrResult[0][2];
        	fm.all('ComIDCode').value		= arrResult[0][3];
        	fm.all('ServPlanLevelName').value	= arrResult[0][4];
        	fm.all('ComIDName').value		= arrResult[0][5];
        	fm.all('ServPlayType').value	= arrResult[0][6];
        	fm.all('PlanTypeName').value    = "按档次";
        	
			var strSql = "select ServItemCode,ServItemName, "
				+" ServItemSN "                                  
				+" from  LHHealthServPlan a  "
				+" where a.ServPlanCode ='"+arrResult[0][0]+"'and a.ServPlanLevel='"+arrQueryResult[0][4]+"'"
				+" and ComID='"+ arrQueryResult[0][5]+"'";
				turnPage.pageLineNum = 50;
				turnPage.queryModal(strSql, LHHealthServPlanGrid);
		}
		
		if(arrResult[0][6] == "2")
		{
			fm.all('Riskcode').value			= arrResult[0][0];  // alert(arrResult[0][0]);  
        	fm.all('Riskname').value			= arrResult[0][1];  // alert(arrResult[0][1]);  
        	fm.all('ServPlanLevelName').value   = arrResult[0][2];  // alert(arrResult[0][2]);
        	fm.all('ComIDName').value			= arrResult[0][3];  // alert(arrResult[0][3]);  
        	fm.all('ServPlanLevelCode').value	= arrResult[0][4];  // alert(arrResult[0][4]);
        	fm.all('ComIDCode').value			= arrResult[0][5];  // alert(arrResult[0][5]);  
        	fm.all('PlanTypeName').value    = "按保费";
        	
        	divLHFeeGrid.style.display = "";
        	tdServPlanLevelCode.style.display = "none";
        	var sql = " select distinct FeeUpLimit, FeeLowLimit, ServPlanLevel from LHHealthServPlan where "
        			 +" ServPlanCode ='"+arrResult[0][0]+"'and ComID='"+ arrQueryResult[0][5]+"' and PlanType = '2' " 
        			 ;   
        			 
        	turnPage.queryModal(sql, LHFeeGrid);   
		}
	
	}     
}      
                
function showOneFee()
{
	var FeeSelNo = LHFeeGrid.getSelNo();
	//alert(FeeSelNo);
	var rowNum=LHFeeGrid.mulLineCount ; 
	//alert(rowNum);
	
	 var strSql = "select ServItemCode,ServItemName, ServItemSN  from  LHHealthServPlan a  "
		              +" where a.ServPlanCode ='"+fm.all('Riskcode').value+"' and ComID='"+ fm.all('ComIDCode').value+"'"
		              +" and a.ServPlanLevel='"+LHFeeGrid.getRowColData(LHFeeGrid.getSelNo()-1,3)+"'"
		              ;
		turnPage.pageLineNum = 50;
		turnPage.queryModal(strSql, LHHealthServPlanGrid);	
		fm.all('ServPlanLevelCode').value = LHFeeGrid.getRowColData(FeeSelNo-1,3)==""?"0":LHFeeGrid.getRowColData(FeeSelNo-1,3);
  
  if(FeeSelNo > 1&&FeeSelNo<rowNum)
  {   
  	   var minNo=LHFeeGrid.getRowColData(FeeSelNo-1,1);//选中的保费用下限
       //alert(minNo);
       var maxNo=LHFeeGrid.getRowColData(FeeSelNo-1,2);//选中保费的上限
       //alert(maxNo);
       //alert(maxNo < minNo);
       var minNoM=LHFeeGrid.getRowColData(FeeSelNo-2,2);//选中保费的上次保费用上限
       //alert(minNoM);
       var minNoB=LHFeeGrid.getRowColData(FeeSelNo,1);//选中保费的下次保费的下限
       //alert(minNoB);
       if(parseInt(maxNo) < parseInt(minNo))
       {
       	alert("您输入的保费上限应大于等于保费下限!");
       	return false;
       }
       if(parseInt(minNo) < parseInt(minNoM))
       {
       	alert("您选择的保费下限应大于上次保费的上限!");
       	return false;
       }
       if(parseInt(minNoB) < parseInt(maxNo))
       {
       	alert("您输入的下次保费用下限应该大于选择保费的上限!");
       	return false;
       } 
   }
   if(FeeSelNo =1)
   {
   	   var minNo=LHFeeGrid.getRowColData(FeeSelNo-1,1);//选中的保费用下限
       //alert(minNo);
       var maxNo=LHFeeGrid.getRowColData(FeeSelNo-1,2);//选中保费的上限
       //alert(maxNo);
       //alert(maxNo < minNo);
       if(parseInt(maxNo) < parseInt(minNo))
       {
       	alert("您输入的保费上限应大于等于保费下限!");
       	return false;
       }
   }
}
                
function afterCodeSelect(codeName,Field)
{ 
	if(codeName == "hmPlanType")
	{
		if(Field.value == "1")
		{
			initLHFeeGrid();
			divLHFeeGrid.style.display = "none";
			fm.all('ServPlanLevelCode').value = "";
			tdServPlanLevelCode.style.display = "";
			
		}
		if(Field.value == "2")
		{
			divLHFeeGrid.style.display = "";
			fm.all('ServPlanLevelCode').value = 0;
			tdServPlanLevelCode.style.display = "none";
		}
	}	
}

 