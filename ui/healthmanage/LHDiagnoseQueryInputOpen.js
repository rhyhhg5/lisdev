/** 
 * 程序名称：LHMainCustomHealthQueryInput.js
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-06-18 09:08:27
 * 创建人  ：St.GN
 * 更新人  ：更新人    更新日期     更新原因/内容  
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			      
  alert(easyExecSql(SQL));
  
	turnPage.queryModal(SQL, LHMainCustomHealthGrid);
}

function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}

function returnParent()
{
  var arrReturn = new Array();
	var tSel = LHMainCustomHealthGrid.getSelNo();
		
		
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
			try
			{	 
				arrReturn = getQueryResult(); 
				top.opener.afterQuery1( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery1接口。" + ex );
			}
			top.close(); 		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = LHMainCustomHealthGrid.getSelNo();
	
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组 
	arrSelected[0] = new Array();
	arrSelected[0] = LHMainCustomHealthGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}


function afterQuery00(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.DiseasCode.value = arrResult[0][0];
  }
}

function queryCustomerNo()
{
	if(fm.all('CustomerNo').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LDPersonQuery.jsp");	  
	  }
	if(fm.all('CustomerNo').value != "")	 
	{
		var cCustomerNo = fm.CustomerNo.value;  //客户代码	
		var strSql = "select CustomerNo,Name from LDPerson where CustomerNo='" + cCustomerNo +"'";
	    var arrResult = easyExecSql(strSql); 
	    if (arrResult != null) {
	      fm.CustomerNo.value = arrResult[0][0];
	      alert("查询结果:  客户代码:["+arrResult[0][0]+"] 客户姓名:["+arrResult[0][1]+"]");
    }
    else{ 
     alert("客户代码为:["+fm.all('CustomerNo').value+"]的客户不存在，请确认!");
     }
	}	
}
function afterQuery(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.CustomerNo.value = arrResult[0][0];
  	fm.CustomerName.value = arrResult[0][1];
  }
}


//function healthQuery2()
//{
//	var arrReturn = new Array();
//	var tSel = LHMainCustomHealthGrid.getSelNo();
//			
//	if( tSel == 0 || tSel == null )
//		//top.close();
//	alert( "请先选择一条记录，再点击返回按钮。" );
//	else
//	{	
//			try
//			{	
//				//alert(tSel);
//				arrReturn = getQueryResult();
////				top.opener.afterQuery0( arrReturn );
////				alert("LHMainCustomerHealth.jsp?Customerno="+arrReturn[0][0]+"&Healthno="+arrReturn[0][8]);
//				parent.opener.window.location="LHMainCustomerHealth.jsp?Customerno="+arrReturn[0][0]+"&Healthno="+arrReturn[0][8];
//			}
//			catch(ex)
//			{
//				alert( "没有发现父窗口的afterQuery0接口。" + ex );
//			}
//			self.close();		
//	}
//}


function diagnoseQuery2()
{
	var arrReturn = new Array();
	var tSel = LHMainCustomHealthGrid.getSelNo();
			
	if( tSel == 0 || tSel == null )
		//top.close();
	alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{	
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
//				top.opener.afterQuery0( arrReturn );
//				alert("LHMainCustomerHealth.jsp?Customerno="+arrReturn[0][0]+"&Healthno="+arrReturn[0][8]);
				parent.opener.window.location="LHCustomInHospitalInput.jsp?Customerno="+arrReturn[0][0]+"&InhospitNo="+arrReturn[0][13];
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery0接口。" + ex );
			}
//			self.close();		
			parent.opener.window.focus();
	}
}

//健康档案综合查询--在就诊信息页面--再点击某人基础健康信息

function healthQuery2()
{
		var arrReturn = new Array();
		var tSel = LHMainCustomHealthGrid.getSelNo();//得到基础健康信息Mulline的行号	
		arrReturn = getQueryResult();//这个结果是基础健康信息结果，只需要它的客户号，即arrReturn[0][0].
//		alert(getQueryResult());	
		if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
		
		else
		{	
			  tSel_main = LHMainCustomHealth2Grid.getSelNo();	//得到基础健康信息客户对应的就诊信息Mulline的行号

			  if( tSel_main == 0 || tSel_main == null || LHMainCustomHealthGrid.getRowColData(tSel-1,1) != LHMainCustomHealth2Grid.getRowColData(tSel_main-1,1))
			  {	  //选择了就诊信息的Mulline一行的信息时
					  	initLHMainCustomHealth2Grid();
							//对应的LHMainCustomHealth2Grid的SQL语句
							var strSql="select distinct a.CustomerNo,b.name, a.HealthNo,a.AddDate,a.Stature,"
				      +" a.Avoirdupois,a.AvoirdIndex,a.BloodPressLow,a.BloodPressHigh,a.Smoke,a.Kisscup,a.Situp,a.Diningnorule "
						      +" from LHCustomHealthStatus a, ldperson b where 1=1  "
				      +" and a.CustomerNo=b.CustomerNo and "
				      +" a.CustomerNo='"+arrReturn[0][0]+"'"
				      ;
				      
				      var arr = easyExecSql(strSql);
				      if (arr != null)
				      {
				      	CustomMainHealth.style.display='';			//结果不为空时，显示Mulline_1中被选客户的基本健康信息，在Mulline2中
				      	turnPage.queryModal(strSql,  LHMainCustomHealth2Grid);
				      }
	    	}
	
	      else
	      {//选择就诊信息Mulline后，再选择某一客户的基础健康信息后，点击基础健康信息查询按钮
		      		try
							{	
									parent.opener.window.location="LHMainCustomerHealth.jsp?Customerno="+LHMainCustomHealth2Grid.getRowColData(tSel_main-1,1)+"&Healthno="+LHMainCustomHealth2Grid.getRowColData(tSel_main-1,3);
//									alert("LHMainCustomerHealth.jsp?Customerno="+LHMainCustomHealth2Grid.getRowColData(tSel_main-1,1)+"&Healthno="+LHMainCustomHealth2Grid.getRowColData(tSel_main-1,3));
							}
							catch(ex)
							{
									alert( "没有发现父窗口的afterQuery0接口。" + ex );
							}
//							self.close();
							parent.opener.window.focus();
	      }
		}
}