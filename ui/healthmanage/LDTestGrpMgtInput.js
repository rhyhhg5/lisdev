//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{//alert(fm.all('TestGrpType').value);
	if(fm.all('TestGrpType').value == "2" || fm.all('TestGrpType').value == "5")
	{
		
		var re = easyExecSql("select * from ldtestgrpmgt where TestGrpType = '"+fm.all('TestGrpType').value+"' and testgrpcode = '"+fm.all('TestGrpCode').value+"'");	
		
		if(re != null && re != "null")
		{
			alert("体检套餐代码重复，请更换一个体检套餐代码");	
			return false;
		}
	}
	
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LDTestGrpMgt.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if( verifyInput2() == false ) return false;
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LDTestGrpMgtQuery.html");
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
				arrResult = arrQueryResult;
        fm.all('TestGrpCode').value= arrResult[0][0];
        fm.all('TestGrpName').value= arrResult[0][1];
        fm.all('TestGrpType').value= arrResult[0][2];fm.all('TestGrpType_ch').value = easyExecSql("select codename from ldcode where codetype = 'hmtestgrptype' and code ='"+arrResult[0][2]+"'");
        fm.all('MakeDate').value= arrResult[0][3];
        fm.all('MakeTime').value= arrResult[0][4];
   		 
   		  if(arrResult[0][2] == "4")
   		  {
   		  		StrSQL = " select a.TestItemCode, (select distinct b.TestGrpName from ldtestgrpmgt b where  b.TestGrpCode = a.TestItemCode ), a.othersign "
   		  						+" from LDTestGrpMgt a where a.TestGrpType ='4' and a.TestGrpCode = '"+arrResult[0][0]+"' order by a.TestItemCode ";
		    		turnPage.pageLineNum = 100; 
		    		turnPage.queryModal(StrSQL, TestGrpCombineGrid);
		    		mulline1.style.display='none';
		    		mulline2.style.display='';
		    		fm.all('GrpInBelly').value = "";
		    		HosptialInfo.style.display="none";
		    		belly.style.display="none";	
		    		
   		  }
   		  else
   		  {
		   		  StrSQL = " select a.TestItemCode ,b.MedicaItemName from LDTestGrpMgt a,LHCountrMedicaItem b where a.TestItemCode=b.MedicaItemCode and a.TestGrpCode = '"+arrResult[0][0]+"'  order by a.TestItemCode ";
		    		turnPage.pageLineNum = 100; 
		    		turnPage.queryModal(StrSQL, TestGrpItemGrid);
		    		mulline1.style.display='';
		    		mulline2.style.display='none';
		    		fm.all('GrpInBelly').value = "";
		    }
		    if(arrResult[0][2]=="1")
			 {
			 		belly.style.display="none";	
			 		HosptialInfo.style.display="none";
		    }
			 if(arrResult[0][2]=="3")
			 {
			 		belly.style.display="";	
			 		HosptialInfo.style.display="none";
	//			 	GrpInBelly.verify="是否空腹|NOTNULL";
			 	  var GrpInBelly = easyExecSql("select distinct case when othersign='Y' then '是' else '否' end from LDTestGrpMgt where TestGrpType = '3' and TestGrpCode = '"+arrResult[0][0]+"'");
			 		fm.all('GrpInBelly').value = GrpInBelly=="null"?"":GrpInBelly;
			 }
			 //else
			 //{
			 //		belly.style.display="none";	
			 //		fm.all('GrpInBelly').value = "";
			// }
			if(arrResult[0][2]=="2")
			 {
			 		belly.style.display="none";	
			 		HosptialInfo.style.display="";
			 	  var HospitCode = arrResult[0][5];//easyExecSql("select distinct Explain from LDTestGrpMgt where TestGrpType = '2' and TestGrpCode = '"+arrResult[0][0]+"'");
			 		if(HospitCode!=""&&HospitCode!="null"&&HospitCode!=null)
			 		{
			 		  fm.all('HospitCode').value =HospitCode;
			 		  var HospitName=easyExecSql("select  HospitName from LDHospital where LDHospital.HospitCode='"+fm.all('HospitCode').value+"'");
			      fm.all('HospitName').value =HospitName;
			      
			      StrSQL = " select a.TestItemCode ,b.MedicaItemName from LDTestGrpMgt a,LHCountrMedicaItem b where a.TestItemCode=b.MedicaItemCode and a.TestGrpCode = '"+arrResult[0][0]+"' and Explain = '"+arrResult[0][5]+"'  order by a.TestItemCode ";
		    		turnPage.pageLineNum = 100; 
		    		turnPage.queryModal(StrSQL, TestGrpItemGrid);
			 		}
			 		else 
			 		{
			 			fm.all('HospitCode').value="";
			 			fm.all('HospitName').value="";
			 			
			 		}
			 		
			 }
			 if(arrResult[0][2]=="5")
			 {
					StrSQL = " select a.TestItemCode, (select distinct b.TestGrpName from ldtestgrpmgt b where  b.TestGrpCode = a.TestItemCode ), a.othersign "
   		  				+" from LDTestGrpMgt a where a.TestGrpType ='5' and a.TestGrpCode = '"+arrResult[0][0]+"' and Explain = '"+arrResult[0][5]+"'  order by a.TestItemCode ";
		    		turnPage.pageLineNum = 100;
		    		turnPage.queryModal(StrSQL, TestGrpCombineGrid);
					mulline1.style.display='none';
		    		mulline2.style.display='';
			 		belly.style.display="none";	
			 		HosptialInfo.style.display="";
//			 	  	var HospitCode = easyExecSql("select distinct Explain from LDTestGrpMgt where TestGrpType = '5' and TestGrpCode = '"+arrResult[0][0]+"'");
			 		if(arrResult[0][5]!="" && arrResult[0][5] != "null" && arrResult[0][5]!= null)
			 		{
						fm.all('HospitCode').value =arrResult[0][5];
			 		    var HospitName=easyExecSql("select  HospitName from LDHospital where LDHospital.HospitCode='"+fm.all('HospitCode').value+"'");
						fm.all('HospitName').value =HospitName;
				    }
			 		else 
			 		{
			 			fm.all('HospitCode').value="";
			 			fm.all('HospitName').value="";
			 			
			 		}
			 }
	}
}               
     
function afterCodeSelect(codeName,Field)
{
  //alert(Field.value);
	if(codeName == "hmtestgrptype")
	{
		fm.all('GrpInBelly').value = "";
		if(Field.value == "4")
		{ 
			 mulline1.style.display="none";
			 mulline2.style.display="";
			 belly.style.display="none";
			 HosptialInfo.style.display="none";
//			 GrpInBelly.verify="";
		}
		else
		{	
			
				mulline1.style.display="";
        mulline2.style.display="none";
        if(Field.value == "1")
			  {
			 		belly.style.display="none";	
			 		HosptialInfo.style.display="none";
			  }
			  if(Field.value == "3")
			  {
			 		belly.style.display="";	
			 		HosptialInfo.style.display="none";
			  }
			  //else{belly.style.display="none";	GrpInBelly.verify="";}		 

		   if(Field.value == "2")
		   { 
			   mulline1.style.display="";
			   mulline2.style.display="none";
			   HosptialInfo.style.display="";
			   belly.style.display="none";	
		   }
		   if(Field.value == "5")
		   { 
			   mulline1.style.display="none";
			   mulline2.style.display="";
			   HosptialInfo.style.display="";
			   belly.style.display="none";	
		   }
		}
		
	}
}
 
