<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHHealthServPlanInput.jsp
//程序功能：个人服务计划内容定义(页面显示)
//创建日期：2006-09-11 11:20:30
//创建人  ：郭丽颖
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHManaStandProInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHManaStandProInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LHManaStandProSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHStaQuOptionGrid1);">
    		</td>
    		<td class= titleImg>
        		 健康问题标准化基本信息
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHStaQuOptionGrid1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      健康问题(分类)代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StandCode style="width:150px"   verify="健康问题(分类)代码|notnull&len<=20"><font color=red> *</font>
    </TD>
    <TD  class= title>
      健康问题(分类)名称(问题内容)
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StandContent >
    </TD>
    <TD class= title>
    	代码分类等级
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=StandLevel><Input class= 'codeno' style="width:120px"  name=StandLevelName  ondblclick="showCodeList('lhcodelabelgrade',[this,StandLevel],[1,0],null,null,null,'1',130);" codeClear(StandLevel,StandLevelName);">
		</TD>   
  </TR>
  <TR  class= common>
  	 <TD  class= title>
      标准单位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=StandUnit >
    </TD>
  
     <TD  class= title>
      问题所属问卷
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=QuerAffiliated><Input class= 'codeno' style="width:120px"  name=QuerAffiliatedName  ondblclick="showCodeList('lhincludeques',[this,QuerAffiliated],[1,0],null,null,null,'1',200);" codeClear(QuerAffiliated,QuerAffiliatedName);">
		</TD> 
		<TD class= title>
    	问题状态
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=QuesStauts><Input class= 'codeno' style="width:120px"  name=QuesStautsName  ondblclick="showCodeList('lhquestand',[this,QuesStauts],[1,0],null,null,null,'1',130);" codeClear(QuesStauts,QuesStautsName);">
		</TD>  
   </TR>
    <TR  class= common>
    	<TD class= title>
    	问题答案类型
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=QuerResType><Input class= 'codeno' style="width:120px"  name=QuerResTypeName  ondblclick="showCodeList('lhqueresultype',[this,QuerResType],[1,0],null,null,null,'1',130);" codeClear(QuerResType,QuerResTypeName);">
		</TD>   
   	<TD class= title>
    	问题附属信息
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ResuAffiInfo >
    </TD> 
    <TD  class= title>
      问题类型
    </TD>
     <TD  class= input>
				 <Input class= 'codename' style="width:40px" name=StandQuerType><Input class= 'codeno' style="width:120px"  name=StandQuerTypeName  ondblclick="showCodeList('lhquesrestype',[this,StandQuerType],[1,0],null,null,null,'1',130);" codeClear(StandQuerType,StandQuerTypeName);">
		</TD> 
    </tr>
</table>
    </Div>

<Div id="divLHStaQuOptionInfo" style="display:''">      
	<table>
		<tr>
			<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHStaQuOptionGrid);">
			</td>
			<td class=titleImg>
		  			 信息
			</td>
		</tr>
	</table>
		  <!-- 信息（列表） -->
	<Div id="divLHStaQuOptionGrid" style="display:''">   
		<Div style="display:''">
			<table class=common>
				<tr class=common>
					<td text-align:left colSpan=1>
		  					<span id="spanLHStaQuOptionGrid">
		  					</span> 
					</td>
				</tr>
			</table>
		</div>
	</Div>
</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type = hidden name = "Operator">
    <input type = hidden name = "MakeDate">
    <input type = hidden name = "MakeTime">
    <input type = hidden name = "ModifyDate">
    <input type = hidden name = "ModifyTime">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
