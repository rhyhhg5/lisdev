<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
	function initForm() {
		try {
			initInpBox();
			initEnd010Grid();
			showAllCodeName();
		} catch (re) {
			alert("初始化界面错误!");
		}
	}
	function initInpBox() {
	}

	var End010Grid;
	function initEnd010Grid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名
			iArray[0][1] = "30px"; //列名    
			iArray[0][2] = 0; //列名

			iArray[1] = new Array();
			iArray[1][0] = "转出保单号"; //列名
			iArray[1][1] = "50px"; //列宽
			iArray[1][2] = 200; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许 

			iArray[2] = new Array();
			iArray[2][0] = "转出分单号"; //2
			iArray[2][1] = "60px";
			iArray[2][2] = 200;
			iArray[2][3] = 0;

			iArray[3] = new Array();
			iArray[3][0] = "转出公司联系人"; //列名3
			iArray[3][1] = "60px"; //列宽
			iArray[3][2] = 200; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array(); //列名4
			iArray[4][0] = "电话";
			iArray[4][1] = "60px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[5] = new Array();
			iArray[5][0] = "Email"; //列名5
			iArray[5][1] = "60px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 0;
			
			iArray[6] = new Array();
			iArray[6][0] = "余额转出平台接收日期"; //列名5
			iArray[6][1] = "60px"; //列宽
			iArray[6][2] = 200; //列最大值
			iArray[6][3] = 0;
			
			iArray[7] = new Array();
			iArray[7][0] = "转出金额"; //列名5
			iArray[7][1] = "60px"; //列宽
			iArray[7][2] = 200; //列最大值
			iArray[7][3] = 0;
			
			

			End010Grid = new MulLineEnter("fm", "End010Grid");
			//设置Grid属性
			End010Grid.mulLineCount = 0;
			End010Grid.displayTitle = 1;
			End010Grid.locked = 1;
			End010Grid.canSel = 0;
			End010Grid.canChk = 1;
			End010Grid.hiddenSubtraction = 1;
			End010Grid.hiddenPlus = 1;
			End010Grid.loadMulLine(iArray);
		} catch (ex) {
			alert(ex);
		}
	}
</script>
