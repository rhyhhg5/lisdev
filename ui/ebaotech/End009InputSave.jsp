
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.httpclient.dto.*"%>
<%@page import="com.sinosoft.httpclient.inf.*"%>

<%
//输入参数
System.out.println("save开始...");
	String sSerNo[] = request.getParameterValues("End009GridNo");
	String tChk[] = request.getParameterValues("InpEnd009GridChk");//判断是否被选中InpXxxxChk
	//获取当前页每一个 AgentCom  ManageCom SalechnlCode AcTypeCode
	String tManageComs[] = request.getParameterValues("End009Grid1");  //管理机构代码
	String tAgentCom[] = request.getParameterValues("End009Grid5");//中介机构编码
	String tSalechnlCode[] = request.getParameterValues("End009Grid10");//中介所属销售渠道编码
	String tAcTypeCode[] = request.getParameterValues("End009Grid11");//中介机构类型编码
	
	//LAComECTypeSet tLAComECTypeSet = new LAComECTypeSet();
	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String strManageCom = tG.AgentCom;
	//执行动作：insert 添加纪录       delete 删除记录 
	transact = request.getParameter("fmtransact");
	System.out.println(transact);
	
	
	String queryStartDate = request.getParameter("StartDate");//起始日期
	String queryEndDate = request.getParameter("EndDate");//终止日期
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("QueryStartDate", queryStartDate);
	tTransferData.setNameAndValue("QueryEndDate", queryEndDate);
	
	// 准备向后台传输数据 VData
	VData tVData = new VData();
	FlagStr = "";
	tVData.add(tG);
	tVData.add(tTransferData);
//	End009UI tEnd009UI = new End009UI();
	PolicyTransOutRegisterQuery objEnd009 = new PolicyTransOutRegisterQuery();
	try {
	
		objEnd009.submitData(tVData, "ZBXPT");
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	if (!FlagStr.equals("Fail")) {
		tError = objEnd009.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Succ";
		} else {
			Content = tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
