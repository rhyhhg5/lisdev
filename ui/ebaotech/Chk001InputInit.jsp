<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
	function initForm() {
		try {
			initInpBox();
			initChk001Grid();
			showAllCodeName();
		} catch (re) {
			alert("初始化界面错误!");
		}
	}
	function initInpBox() {
	}

	var Chk001Grid;
	function initChk001Grid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名
			iArray[0][1] = "30px"; //列名    
			iArray[0][2] = 0; //列名

			iArray[1] = new Array();
			iArray[1][0] = "保险公司代码"; //列名
			iArray[1][1] = "60px"; //列宽
			iArray[1][2] = 200; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许 

			iArray[2] = new Array();
			iArray[2][0] = "指定核对日期"; //2
			iArray[2][1] = "60px";
			iArray[2][2] = 200;
			iArray[2][3] = 0;

			iArray[3] = new Array();
			iArray[3][0] = "总记录数"; //列名3
			iArray[3][1] = "60px"; //列宽
			iArray[3][2] = 200; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array(); //列名4
			iArray[4][0] = "成功记录数";
			iArray[4][1] = "80px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[5] = new Array();
			iArray[5][0] = "失败记录数"; //列名5
			iArray[5][1] = "50px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 0;

			iArray[6] = new Array();
			iArray[6][0] = "核对请求类型"; //列名
			iArray[6][1] = "60px"; //列宽
			iArray[6][2] = 200; //列最大值
			iArray[6][3] = 0;

			iArray[7] = new Array();
			iArray[7][0] = "交易时间"; //列名
			iArray[7][1] = "40px"; //列宽
			iArray[7][2] = 200; //列最大值
			iArray[7][3] = 0;

			iArray[8] = new Array();
			iArray[8][0] = "保单号码"; //列名
			iArray[8][1] = "60px"; //列宽
			iArray[8][2] = 200; //列最大值
			iArray[8][3] = 0;
			
			iArray[9] = new Array();
			iArray[9][0] = "分单号码"; //列名
			iArray[9][1] = "60px"; //列宽
			iArray[9][2] = 200; //列最大值
			iArray[9][3] = 0;
			
			iArray[10] = new Array();
			iArray[10][0] = "保全批单号"; //列名
			iArray[10][1] = "60px"; //列宽
			iArray[10][2] = 200; //列最大值
			iArray[10][3] = 0;
			
			iArray[11] = new Array();
			iArray[11][0] = "理赔赔案号"; //列名
			iArray[11][1] = "60px"; //列宽
			iArray[11][2] = 200; //列最大值
			iArray[11][3] = 0;
			
			iArray[12] = new Array();
			iArray[12][0] = "费用编码"; //列名
			iArray[12][1] = "60px"; //列宽
			iArray[12][2] = 200; //列最大值
			iArray[12][3] = 0;
			
			iArray[13] = new Array();
			iArray[13][0] = "保费状态"; //列名
			iArray[13][1] = "60px"; //列宽
			iArray[13][2] = 200; //列最大值
			iArray[13][3] = 0;
			
			iArray[14] = new Array();
			iArray[14][0] = "续保批单号"; //列名
			iArray[14][1] = "60px"; //列宽
			iArray[14][2] = 200; //列最大值
			iArray[14][3] = 0;
			
			iArray[15] = new Array();
			iArray[15][0] = "公司费用ID"; //列名
			iArray[15][1] = "60px"; //列宽
			iArray[15][2] = 200; //列最大值
			iArray[15][3] = 0;
			
			iArray[16] = new Array();
			iArray[16][0] = "返回结果"; //列名
			iArray[16][1] = "60px"; //列宽
			iArray[16][2] = 200; //列最大值
			iArray[16][3] = 0;
			
			iArray[17] = new Array();
			iArray[17][0] = "失败原因"; //列名
			iArray[17][1] = "60px"; //列宽
			iArray[17][2] = 200; //列最大值
			iArray[17][3] = 0;

			Chk001Grid = new MulLineEnter("fm", "Chk001Grid");
			//设置Grid属性
			Chk001Grid.mulLineCount = 0;
			Chk001Grid.displayTitle = 1;
			Chk001Grid.locked = 1;
			Chk001Grid.canSel = 0;
			Chk001Grid.canChk = 1;
			Chk001Grid.hiddenSubtraction = 1;
			Chk001Grid.hiddenPlus = 1;
			Chk001Grid.loadMulLine(iArray);
		} catch (ex) {
			alert(ex);
		}
	}
</script>
