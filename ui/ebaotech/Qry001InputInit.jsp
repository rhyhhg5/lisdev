<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
	function initForm() {
		try {
			initInpBox();
			initPolicyGrid();//保单信息
			initCoverageGrid();//险种信息
			initClaimGrid();//赔案信息
			showAllCodeName();
		} catch (re) {
			alert("初始化界面错误!");
		}
	}
	function initInpBox() {
	}
	
	//1、inital保单信息
	var PolicyGrid;//保单信息
	function initPolicyGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名
			iArray[0][1] = "30px"; //列名    
			iArray[0][2] = 0; //列名

			iArray[1] = new Array();
			iArray[1][0] = "平台客户编码"; //列名
			iArray[1][1] = "60px"; //列宽
			iArray[1][2] = 200; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许 

			iArray[2] = new Array();
			iArray[2][0] = "国籍"; //2
			iArray[2][1] = "40px";
			iArray[2][2] = 200;
			iArray[2][3] = 0;

			iArray[3] = new Array();
			iArray[3][0] = "保险公司代码"; //列名3
			iArray[3][1] = "60px"; //列宽
			iArray[3][2] = 200; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array(); //列名4
			iArray[4][0] = "保单类型";
			iArray[4][1] = "50px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[5] = new Array();
			iArray[5][0] = "保单号码"; //列名5
			iArray[5][1] = "50px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 0;

			iArray[6] = new Array();
			iArray[6][0] = "分单号"; //列名
			iArray[6][1] = "50px"; //列宽
			iArray[6][2] = 200; //列最大值
			iArray[6][3] = 0;

			iArray[7] = new Array();
			iArray[7][0] = "保单投保来源"; //列名
			iArray[7][1] = "60px"; //列宽
			iArray[7][2] = 200; //列最大值
			iArray[7][3] = 0;

			iArray[8] = new Array();
			iArray[8][0] = "保单状态"; //列名8
			iArray[8][1] = "50px"; //列宽
			iArray[8][2] = 200; //列最大值
			iArray[8][3] = 0;
			
			iArray[9] = new Array();
			iArray[9][0] = "保单生效日期"; //列名9
			iArray[9][1] = "60px"; //列宽
			iArray[9][2] = 200; //列最大值
			iArray[9][3] = 0;
			
			iArray[10] = new Array();
			iArray[10][0] = "保单满期日期"; //列名10
			iArray[10][1] = "60px"; //列宽
			iArray[10][2] = 200; //列最大值
			iArray[10][3] = 0;
			
			iArray[11] = new Array();
			iArray[11][0] = "保单终止日期"; //列名11
			iArray[11][1] = "60px"; //列宽
			iArray[11][2] = 200; //列最大值
			iArray[11][3] = 0;
			
			iArray[12] = new Array();
			iArray[12][0] = "保单终止原因"; //列名12
			iArray[12][1] = "60px"; //列宽
			iArray[12][2] = 200; //列最大值
			iArray[12][3] = 0;
			
			iArray[13] = new Array();
			iArray[13][0] = "是否有有效的税优识别码"; //列名13
			iArray[13][1] = "110px"; //列宽
			iArray[13][2] = 200; //列最大值
			iArray[13][3] = 0;
			
			iArray[14] = new Array();
			iArray[14][0] = "当前保单年度内赔付金额"; //列名14
			iArray[14][1] = "110px"; //列宽
			iArray[14][2] = 200; //列最大值
			iArray[14][3] = 0;
			
			iArray[15] = new Array();
			iArray[15][0] = "保单累计赔付金额"; //列名15
			iArray[15][1] = "80px"; //列宽
			iArray[15][2] = 200; //列最大值
			iArray[15][3] = 0;

			PolicyGrid = new MulLineEnter("fm", "PolicyGrid");
			//设置Grid属性
			PolicyGrid.mulLineCount = 0;
			PolicyGrid.displayTitle = 1;
			PolicyGrid.locked = 1;
			PolicyGrid.canSel = 1;
			PolicyGrid.canChk = 0;
			PolicyGrid.hiddenSubtraction = 1;
			PolicyGrid.hiddenPlus = 1;
			//PolicyGrid.selBoxEventFuncName="setRiskValue";
			
			PolicyGrid.loadMulLine(iArray);
		} catch (ex) {
			alert(ex);
		}
	}
	//2、inital险种信息
	var CoverageGrid;//险种信息
	function initCoverageGrid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0]="序号";
			iArray[0][1] ="30px";
			iArray[0][2] =200;
			iArray[0][3] =0;
			
			iArray[1] = new Array();
			iArray[1][0]="公司险种名称";
			iArray[1][1] ="60px";
			iArray[1][2] =200;
			iArray[1][3] =0;
			
			iArray[2] = new Array();
			iArray[2][0]="险种保额";
			iArray[2][1] ="60px";
			iArray[2][2] =200;
			iArray[2][3] =0;
			
			iArray[3] = new Array();
			iArray[3][0]="核保决定";
			iArray[3][1] ="60px";
			iArray[3][2] =200;
			iArray[3][3] =0;
			
			CoverageGrid = new MulLineEnter("fm", "CoverageGrid");
		      //这些属性必须在loadMulLine前
			CoverageGrid.mulLineCount = 0;
			CoverageGrid.displayTitle = 1;
			CoverageGrid.locked = 1;
			CoverageGrid.canSel = 0;
			CoverageGrid.canChk = 1;
			CoverageGrid.hiddenSubtraction = 1;
			CoverageGrid.hiddenPlus = 1;
			//CoverageGrid.selBoxEventFuncName="fromListReturn";
			CoverageGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
	//3、inital赔案信息
	var ClaimGrid;//赔案信息
	function initClaimGrid() {
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0]="序号";
			iArray[0][1] ="30px";
			iArray[0][2] =200;
			iArray[0][3] =0;
			
			iArray[1] = new Array();
			iArray[1][0]="出险日期";
			iArray[1][1] ="50px";
			iArray[1][2] =200;
			iArray[1][3] =0;
			
			iArray[2] = new Array();
			iArray[2][0]="结案时间";
			iArray[2][1] ="50px";
			iArray[2][2] =200;
			iArray[2][3] =0;  

			iArray[3] = new Array();
			iArray[3][0]="理赔结论代码";
			iArray[3][1] ="50px";
			iArray[3][2] =200;
			iArray[3][3] =0;
			
			iArray[4] = new Array();
			iArray[4][0]="警示标识";
			iArray[4][1] ="50px";
			iArray[4][2] =200;
			iArray[4][3] =0;
			
			iArray[5] = new Array();
			iArray[5][0]="警示原因描述";
			iArray[5][1] ="80px";
			iArray[5][2] =200;
			iArray[5][3] =0;
			
			ClaimGrid = new MulLineEnter("fm", "ClaimGrid");
			//设置Grid属性
			ClaimGrid.mulLineCount = 0;
			ClaimGrid.displayTitle = 1;
			ClaimGrid.locked = 1;
			ClaimGrid.canSel = 0;
			ClaimGrid.canChk = 1;
			ClaimGrid.hiddenSubtraction = 1;
			ClaimGrid.hiddenPlus = 1;
			ClaimGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>
