var turnPage = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;// 使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		} catch (ex) {
			showInfo = null;
		}
	}
}

function beforeSubmit() {
	if(!checkTime009()){
		return false;
	}
	
	var showStr = "\u6b63\u5728\u6267\u884c\u63d0\u53d6\u6570\u636e\u64cd\u4f5c\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.submit();
}


function checkTime009() {
	var ccheckDate = fm.StartDate.value;
	if (ccheckDate == null || ccheckDate == "") {
		alert("请输入指定核对日期");
		return false;
	} else {
		if (!checkDateFormat("指定核对日期", ccheckDate)) {
			fm.all('ccheckDate').focus();
			return false;
		}
		
	}
	var myDate = new Date();//.toLocaleDateString();
	
	var	nian=myDate.getFullYear();    //获取完整的年份(4位,1970-????)
	var	 month=myDate.getMonth()+1;       //获取当前月份(0-11,0代表1月)
	var	 day=myDate.getDate(); //获取当前日
	var jt=nian+"-"+month+"-"+day;
		if(ccheckDate>=jt){//开始大于当前
			alert("核对日期必须小于今天！");
			return false;
		}
	return true;
}
// 日期格式校验
function checkDateFormat(tName, strValue) {
	if (!isDate(strValue)) {
		alert("输入的[" + tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}

// 点击查询 将符合要求的险种显示出来
function queryClick() {
	initChk001Grid();
	var ccheckDate = fm.checkDate.value;
	if (ccheckDate == null || ccheckDate == "") {
		alert("请输入指定核对日期");
		return false;
	} else {
		if (!checkDateFormat("指定核对日期", ccheckDate)) {
			fm.all('ccheckDate').focus();
			return false;
		}
		
	}
	var strSql = "select CompanyCode,CheckDate,TotalNum,SuccessNum,FailNum,CheckTransType, TransDate,PolicyNo,SequenceNo,EndorsementNo," 
			+ "ClaimNo,FeeId,FeeStatus,RenewalEndorsementNo,ComFeeId,ReturnCode,ErrorMessage"
			+ " from BusinessCheck where 1=1 "
			+ getWherePart('CheckDate', 'checkDate')// 指定核对日期
	var strResult = easyExecSql(strSql);
	if (!strResult) {
		alert("未查询到交易核对的相关信息！");
		return false;
	}
	turnPage.queryModal(strSql, Chk001Grid);
}