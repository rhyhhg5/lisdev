<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
	function initForm() {
		try {
			initInpBox();
			initEnd009Grid();
			showAllCodeName();
		} catch (re) {
			alert("初始化界面错误!");
		}
	}
	function initInpBox() {
	}

	var End009Grid;
	function initEnd009Grid() {
		var iArray = new Array();
		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名
			iArray[0][1] = "30px"; //列名    
			iArray[0][2] = 0; //列名
			
			iArray[1] = new Array();
			iArray[1][0] = "转出保单号"; //列名
			iArray[1][1] = "50px"; //列宽
			iArray[1][2] = 200; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许 

			iArray[2] = new Array();
			iArray[2][0] = "转出分单号"; //列名
			iArray[2][1] = "50px"; //列宽
			iArray[2][2] = 200; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许 

			iArray[3] = new Array();
			iArray[3][0] = "转出登记日期"; //2
			iArray[3][1] = "60px";
			iArray[3][2] = 200;
			iArray[3][3] = 0;

			iArray[4] = new Array();
			iArray[4][0] = "预计终止日期"; //列名3
			iArray[4][1] = "60px"; //列宽
			iArray[4][2] = 200; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[5] = new Array(); //列名4
			iArray[5][0] = "无法转出的原因";
			iArray[5][1] = "80px"; //列宽
			iArray[5][2] = 200; //列最大值
			iArray[5][3] = 1; //是否允许输入,1表示允许，0表示不允许

			iArray[6] = new Array();
			iArray[6][0] = "转出公司联系人"; //列名5
			iArray[6][1] = "50px"; //列宽
			iArray[6][2] = 200; //列最大值
			iArray[6][3] = 0;

			iArray[7] = new Array();
			iArray[7][0] = "电话"; //列名
			iArray[7][1] = "60px"; //列宽
			iArray[7][2] = 200; //列最大值
			iArray[7][3] = 0;

			iArray[8] = new Array();
			iArray[8][0] = "Email"; //列名
			iArray[8][1] = "40px"; //列宽
			iArray[8][2] = 200; //列最大值
			iArray[8][3] = 0;

			iArray[9] = new Array();
			iArray[9][0] = "保单转出登记平台接收日期"; //列名8
			iArray[9][1] = "60px"; //列宽
			iArray[9][2] = 200; //列最大值
			iArray[9][3] = 0;

			End009Grid = new MulLineEnter("fm", "End009Grid");
			//设置Grid属性
			End009Grid.mulLineCount = 0;
			End009Grid.displayTitle = 1;
			End009Grid.locked = 1;
			End009Grid.canSel = 0;
			End009Grid.canChk = 1;
			End009Grid.hiddenSubtraction = 1;
			End009Grid.hiddenPlus = 1;
			End009Grid.loadMulLine(iArray);
		} catch (ex) {
			alert(ex);
		}
	}
</script>
