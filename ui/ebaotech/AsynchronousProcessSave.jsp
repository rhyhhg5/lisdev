
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.httpclient.dto.*"%>
<%@page import="com.sinosoft.httpclient.inf.*"%>

<%!

%>
<%
	System.out.println("控制台提示数据：开始执行Save页面...");
	//接口类型
	String cBusinessType = request.getParameter("BusinessType");
	//异步预约码
	String cReverveCode = request.getParameter("ReverveCode");
	System.out.println("控制台提示数据：" + cBusinessType + "*************"
			+ cReverveCode);
	TransferData tTransferData = new TransferData();

	//设置预约码
	tTransferData.setNameAndValue("BookingSequenceNo", cReverveCode);
	
	String transact = "";
	String Content = "";
	String FlagStr = "";
	CErrors tError = null;

	transact = request.getParameter("fmtransact");
	System.out.println("操作的类型是" + transact);
	System.out.println("控制台提示数据：开始进行获取数据的操作...");
	VData tVData = new VData();
	tVData.add(tTransferData);
	//有一个变量定义类型
	
	if ("PTY002".equals(cBusinessType)) {
		GetPlatformCustomerNoPTY002 objPTY002= new GetPlatformCustomerNoPTY002();
		try {
			//建立接口PTY002
			objPTY002.submitData(tVData, "ZBXPT");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = objPTY002.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	
	if ("NBU002".equals(cBusinessType)) {
		GetTaxCodeNBU002 objNBU002= new GetTaxCodeNBU002();
		try {
			//建立接口NBU002
			objNBU002.submitData(tVData, "ZBXPT");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = objNBU002.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	if ("END008".equals(cBusinessType)) {
		GetXBInfoAsynEND008 objEND008 = new GetXBInfoAsynEND008();
		try {
			//建立接口END008
			objEND008.submitData(tVData, "ZBXPT");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = objEND008.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	if ("PRM002".equals(cBusinessType)) {
		XqPremUpLoadPRM002 objPRM002 = new XqPremUpLoadPRM002();
		try {
			//建立接口PRM002
			objPRM002.submitData(tVData, "ZBXPT");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = objPRM002.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	if ("RNW002".equals(cBusinessType)) {
		XbAsynDealResultRNW002 objRNW002 = new XbAsynDealResultRNW002();
		try {
			//建立接口RNW002
			objRNW002.submitData(tVData, "ZBXPT");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = objRNW002.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	if ("ACT002".equals(cBusinessType)) {
		WnAccChangeUpLoadACT002 objACT002  = new WnAccChangeUpLoadACT002();
		try {
			//建立接口ACT002
			objACT002.submitData(tVData, "ZBXPT");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = objACT002.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	
	if ("CLM002".equals(cBusinessType)) {
		UploadClaimCLM002 objCLM002  = new UploadClaimCLM002();
		try {
			//建立接口CLM002
			objCLM002.submitData(tVData, "ZBXPT");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = objCLM002.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	
	if ("END004".equals(cBusinessType)) {
		BqStatusUpLoadEND004 objEND004  = new BqStatusUpLoadEND004();
		try {
			//建立接口objEND004
			objEND004.submitData(tVData, "ZBXPT");
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = objEND004.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	
	if ("".equals(cBusinessType)||cBusinessType==null) {
		if (!FlagStr.equals("Fail")) {
			
				Content = "类型为空！";
				FlagStr = "Fail";
			
		}
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>