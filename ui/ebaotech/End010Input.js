var turnPage = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;// 使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		} catch (ex) {
			showInfo = null;
		}
	}
}

function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}
//日期格式校验
function checkDateFormat(tName,strValue) {
	if (!isDate(strValue))
	{
		alert("输入的["+tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}
	
// 点击查询 将符合要求的险种显示出来
function queryEnd010() {//保单转入余额信息查询
	initEnd010Grid();
	var startDate = fm.all('queryStartDate').value;
	var endDate = fm.all('queryEndDate').value;
	//应收时间起期校验
	if(startDate == "" || startDate == null)
	{
		alert("请选择查询起期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("查询起期",startDate))
		{
			fm.all('queryStartDate').focus();
			return false;
		}
	}
	//应收时间止期校验
	if(endDate == "" || endDate == null)
	{
		alert("请选择查询止期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("查询止期",endDate)){
			fm.all('queryEndDate').focus();
			return false;
		}
	}
	if(startDate>endDate){
		alert("查询起期大于查询止期，请重新选择！");
		return false;
	}
	
	var strSql ="select PolicyNo, SequenceNo,ContactName,ContactTele,ContactEmail,TransReceivDate,TransAmount from PolicyTransformRemainderQuery where 1=1 "
			+ getWherePart('TransReceivDate', 'queryStartDate', '>=','0') 
			+ getWherePart('TransReceivDate', 'queryEndDate', '<=','0') 
	;
	var strResult = easyExecSql(strSql);
	if (!strResult) {
		alert("未查询到符合条件的保单转入信息！");
		return false;
	}
	turnPage.queryModal(strSql, End010Grid);
}
function b_Submit() {
	if(!checkTime009()){
		return false;
	}
	var showStr = "\u6b63\u5728\u6267\u884c\u63d0\u53d6\u6570\u636e\u64cd\u4f5c\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function checkTime009(){
	var startDate = fm.StartDate.value;
	var endDate = fm.EndDate.value;
	//应收时间起期校验
	if(startDate == "" || startDate == null)
	{
		alert("请选择查询起期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("查询起期",startDate))
		{
			fm.all('queryStartDate').focus();
			return false;
		}
	}
	//应收时间止期校验
	if(endDate == "" || endDate == null)
	{
		alert("请选择查询止期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("查询止期",endDate)){
			fm.all('queryEndDate').focus();
			return false;
		}
	}
	var myDate = new Date();//.toLocaleDateString();
	var	nian=myDate.getFullYear();    //获取完整的年份(4位,1970-????)
	var	 month=myDate.getMonth()+1;       //获取当前月份(0-11,0代表1月)
	var	 day=myDate.getDate(); //获取当前日
//		alert("1："+n);
//		 alert("2："+y);
//		 alert("3："+r);
//		var dateD=n+"-"+y+"-"r;
	var jt=nian+"-"+month+"-"+day;
//	alert("当前日期："+jt);
//	if(jt.length()!=10){
//		alert("获取当前日期时异常");
//		return false;
//	}
		if(startDate>jt){//开始大于当前
			alert("查询起期不能大于当前日期！");
			return false;
		}
		
		if(endDate>jt){//终止大于当前
			alert("查询止期不能大于当前日期！");
			return false;
		}
	if(startDate>endDate){
		alert("查询起期大于查询止期，请重新选择！");
		return false;
	}
	
	
	
	return true;
}
