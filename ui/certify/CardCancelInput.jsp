<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head > 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="CertifyNumberCommon.js"></SCRIPT>
<SCRIPT src="CardCancelInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CardCancelInit.jsp"%>
</head>
<body  onload="initForm()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./CardCancelSave.jsp" method=post name="fm" target="fraSubmit"> 	
    
    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>特殊单证处理</td></tr>
    </table>
    
	<Div  id= "divCertifyList" style= "display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanCertifyList" ></span>
         	</td>
       	</tr>
	  	</table>
	  	<br>
	  <input class="cssButton" type="button" value="人工处理" onclick="submitForm()" >
	</div>
	
	<br>
	
	<table>
   	  <tr>
        <td class=common>
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyStroe);">
        </td>
    	<td class= titleImg>现有单证信息</td></tr>
    </table>
		
		<div id="divShowStore" style="display:''">
			<div id="divCertifyStroe">
	      <table class="common">
	        <tr class="common">
	          <td text-align: left colSpan=1><span id="spanCertifyStoreList"></span></td></tr>
		  	</table>
			</div>
			<Div id= "divPage" align=center style= "display:''">
			  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage.firstPage();"> 
			  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage.lastPage();">
	  	</Div>
  	</div>
    <p align="right">单证总数量: <input class="common" name="CertifyCount"></p>

    <input type="hidden" name="certifyLength" value="">
    <input type="hidden" name="ManageCom" value="">
    <input type="hidden" name="Operator" value="">
    <input type="hidden" name="AuditKind" value="">
    <input type="hidden" name="StrComCode" value="<%=strComCode%>">
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
