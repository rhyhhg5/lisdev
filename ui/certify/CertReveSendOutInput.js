//               该文件中包含客户端需要处理的函数和事件
var turnPage=new turnPageClass();
var showInfo;
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput() == true && CertifyList.checkValue("CertifyList") ) {
		if(veriryInput3()==true)
		{
			receiveHandle();
		  var i = 0;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		  fm.submit(); //提交
		}
	}
}

function receiveHandle()
{
	var sendOutC = fm.SendOutCom.value;
	if (sendOutC.substring(0,2)=="86")
	{
		fm.ReceiveType.value="COM";
	}
	else if(fm.ReceiveType.value!="AGE" && fm.ReceiveType.value!="AGECOM")
	{
		fm.ReceiveType.value="USR";
	}
}

function veriryInput3()
{
	if (fm.ReceiveType.value=="COM"||fm.ReceiveType.value=="USR")
	{
		if(fm.SendOutCom.value==''||fm.SendOutCom.value==null)
		{
			alert("回退者不能为空！");
			return false;
		}
	}
	else
	{
		if(fm.AgentCode.value==''||fm.AgentCode.value==null)
		{
			alert("回退者不能为空！");
			return false;
		}
	}
	
	if (fm.ReceiveCom.value==''||fm.ReceiveCom.value==null)
	{
		alert("接收者不能为空！");
		return false;
	}
	
	var rowNum = CertifyList.mulLineCount ;
	if (rowNum==0)
	{
		alert("请在单证列表中录入发放信息！");
		return false;
	}   
	
	for(var i=1;i<=rowNum;i++)
	{
		var strSQL = "select havenumber from LMCertifyDes where certifycode='"+CertifyList.getRowColData(i-1,1)+"'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
		  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,1)+"的单证");
		  return false;
		}
		else
		{
			if (arrResult[0][0]=="Y")
			{
				if (CertifyList.getRowColData(i-1,5)!=""&&CertifyList.getRowColData(i-1,5)!=null)
				{
					var lineNum=CertifyList.getRowColData(i-1,4)-CertifyList.getRowColData(i-1,3)+1; 
					if (lineNum+""!=CertifyList.getRowColData(i-1,5))
					{
						alert("第"+i+"行数量与起始单号终止单号不符！");
						return false;
					}
				}
			}	
		}
		if (CertifyList.getRowColData(i-1,5)==null||CertifyList.getRowColData(i-1,5)=="")
		{
			alert("发放数量不能为空！");
			return false;
		}
		
	}
	     
	return true;
}

function countNum(parm1,parm2)
{
	var startN=CertifyList.getRowColData(parm1,3);
	var endN=CertifyList.getRowColData(parm1,4);
	
	if(startN==""||startN==null)
	{
		CertifyList.setRowColData(parm1,5,"");
		return;
	}
	if (endN==""||endN==null)
	{
		CertifyList.setRowColData(parm1,5,"");
		return;
	}
	if (!isNumeric(startN))
	{
		alert("起始单号有非数字字符！");
		return false;
	}
	if (!isNumeric(endN)&&endN!=null)
	{
		alert("终止单号有非数字字符！");
		return false;
	}
	
	var num=parseFloat(endN)-parseFloat(startN)+1;
	if (num<0)
	{
		alert("起始单号大于终止单号！");
		return false;
	}
	CertifyList.setRowColData(parm1,5,num+"");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{		
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //resetPage();
  }
  else
  { 
  	if( fm.chkPrt.checked == true ) {
	    var urlStr = "CertifyListPrint.jsp";
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:" + 
	      oClientCaps.availWidth + "px;dialogHeight:" + oClientCaps.availHeight + "px");
	  } else {
	    content="保存成功！";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	    CertifyList.addOne("CertifyList");
	    resetPage();
	  }
  }
}

function resetPage()
{
	fm.reset();
	CertifyList.clearData("CertifyList");
	initForm();
}

function afterQuery2(arrQueryResult)
{
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
    {
	  	arrResult = arrQueryResult;
	  	fm.AgentCode.value=arrResult[0][0];
	  	if(arrResult[0].length <95){
 			fm.GroupAgentCode.value=arrResult[0][0];
 		}else{
 			fm.GroupAgentCode.value=arrResult[0][95];
 		}
	}
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertReveSendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
	parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

/*
function receiveType()
{
	var typeRadio="";
  for(i = 0; i <fm.TypeRadio.length; i++){
    if(fm.TypeRadio[i].checked){
      typeRadio=fm.TypeRadio[i].value;
      break;
    }
  }
  if (typeRadio=="0")
	{
  	fm.SendOutCom.style.display="";
  	fm.SendOutName.style.display="";
  	fm.AgentCode.style.display="none";
  	fm.AgentQuery.style.display="none";
  	
  	fm.AgentCode.value="";
  	fm.ReceiveType.value="COM";
  }
  else
  {
  	fm.SendOutCom.style.display="none";
  	fm.SendOutName.style.display="none";
  	fm.AgentCode.style.display="";
  	//fm.ReceiveType.style.display="";
  	fm.AgentQuery.style.display="";
  	fm.ReceiveType.value="AGE";
  	
  	fm.SendOutCom.value="";
  	fm.SendOutName.value="";
  }
}
*/

function queryAgent()
{
	fm.ReceiveType.value="AGE";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
    if(fm.all('AgentCode').value == "")	
    {  
		  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+ "&SaleChnl=all","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	  }
	if(fm.all('AgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select AgentCode,Name,ManageCom from LAAgent where AgentCode='" + cAgentCode +"'";
        var arrResult = easyExecSql(strSql);
        if (arrResult != null) 
        {
          alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
        }
        else
        {
        	alert("查询结果:  无此业务员！");
        } 
	}
}

//查询代理机构
function queryCom()
{
    if(fm.all('ManageCom').value==""){
        alert("请先录入管理机构信息！"); 
        return;
    }
    fm.ReceiveType.value="AGECOM";
    showInfo = window.open("./LAComMain.jsp?ManageCom=" + fm.all('ManageCom').value.substr(0, 4) + "&BranchType=3&BranchType2=01&BankType=01");
}

function afterCodeSelect(codeName,Field)
{
	
    if(codeName == "CertifyCodeReceive1")
    {
        initCertifyStoreList();
        var certifyCode = CertifyList.getRowColDataByName(0, "CertifyCode");
        var strSQL = "";
        if(fm.ManageCom.value.length <= 4)
        {
            strSQL = "select CertifyCode, StartNo, EndNo, SumCount, ReceiveCom "
                + "from LZCard where CertifyCode = '" + certifyCode 
                //+ "' and SendOutCom = 'A" + fm.ManageCom.value 
                + "' and ReceiveCom in (select ReceiveCom from LZAccess where SendOutCom = 'A" 
                + fm.ManageCom.value + "') and StateFlag in ('0','7','8') "
                + "order by StartNo";
        }
        else
        {
            strSQL = "select CertifyCode, StartNo, EndNo, SumCount, ReceiveCom "
                + "from LZCard a where CertifyCode = '" + certifyCode 
                + "' and SendOutCom = 'B" + fm.Operator.value 
                + "' and (ReceiveCom like 'D%' or ReceiveCom like 'E%') and StateFlag in ('0','7','8') "
                + "order by StartNo";
        }
        turnPage.queryModal(strSQL, CertifyStoreList);
    }
}