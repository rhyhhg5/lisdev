
<%
	//程序名称：LLReportInput.jsp
	//程序功能：
	//创建日期：2002-07-19 16:49:22
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page contentType="text/html;charset=GBK"%>


<%
	System.out.println("开始执行CertifyMaxNoSave页面");
	String FlagStr = "Succ";
	String Content = "操作成功";

	GlobalInput tGI = new GlobalInput(); //repair:
	tGI = (GlobalInput) session.getValue("GI"); //参见loginSubmit.jsp
	if (tGI == null) {
		System.out.println("页面失效,请重新登陆");
		FlagStr = "Fail";
		Content = "页面失效,请重新登陆";
	} else {
		String CurrentDate = PubFun.getCurrentDate();
		String CurrentTime = PubFun.getCurrentTime();
		String tOperator = tGI.Operator;
		LMCertifyDesSet tLMCertifyDesSet = new LMCertifyDesSet();
		LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
		String tChk[] = request.getParameterValues("InpCertifyMaxNoGridChk");
		String tCertifyCodes[] = request.getParameterValues("CertifyMaxNoGrid2");
		String tCertifyMaxNos[] = request.getParameterValues("CertifyMaxNoGrid5");
		for (int i = 0; i < tChk.length; i++) {
			if ("1".equals(tChk[i])) {
				String tCertifyCode = tCertifyCodes[i];
				String tSQL = "select * from LMCertifyDes where certifycode = '"+tCertifyCode+"' ";
				LMCertifyDesSet tempLMCertifyDesSet = new LMCertifyDesSet();
				tempLMCertifyDesSet = tLMCertifyDesDB.executeQuery(tSQL);
				if(tempLMCertifyDesSet == null || tempLMCertifyDesSet.size() != 1){
					Content = " 失败，原因是:获取单证"+tCertifyCode+"数据失败！";
					FlagStr = "Fail";
				}else{
					LMCertifyDesSchema tLMCertifyDesSchema = tempLMCertifyDesSet.get(1);
					tLMCertifyDesSchema.setCertifyMaxNo(tCertifyMaxNos[i]);
					tLMCertifyDesSchema.setMaxNoOperator(tOperator);
					if(tLMCertifyDesSchema.getMaxNoMakeDate() == null || "".equals(tLMCertifyDesSchema.getMaxNoMakeDate())){
						tLMCertifyDesSchema.setMaxNoMakeDate(CurrentDate);
						tLMCertifyDesSchema.setMaxNoMakeTime(CurrentTime);
					}
					tLMCertifyDesSchema.setMaxNoModifyDate(CurrentDate);
					tLMCertifyDesSchema.setMaxNoModifyTime(CurrentTime);
					
					tLMCertifyDesSet.add(tLMCertifyDesSchema);
				}
			}
		}
		if(!"Fail".equals(FlagStr)){
			try {
				VData tVData = new VData();
				tVData.addElement(tGI);
				tVData.addElement(tLMCertifyDesSet);
				CertifyMaxNoUI mCertifyMaxNoUI = new CertifyMaxNoUI();
				mCertifyMaxNoUI.submitData(tVData, "");
				if (!mCertifyMaxNoUI.mErrors.needDealError()) {
					Content = " 操作成功";
					FlagStr = "Succ";
				} else {
					Content = " 失败，原因是:"+ mCertifyMaxNoUI.mErrors.getFirstError();
					FlagStr = "Fail";
				}
			} catch (Exception ex) {
				Content = "失败，原因是:" + ex.toString();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>