<%
//程序名称：CertifyInputSave.jsp
//程序功能：
//创建日期：2002-08-08
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=gb2312" %>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  CertifyInputUI tCertifyInputUI = new CertifyInputUI();

  GlobalInput tG = new GlobalInput();

  tG.Operator = "Admin";
  tG.ComCode  = "001";

  session.putValue("GI",tG);

  //tG=(GlobalInput)session.getValue("GI");

  //校验处理
  //内容待填充

  //接收信息
  System.out.println("Start Save...");

  // 保单信息部分
  String szCertifyCode = request.getParameter("CertifyCode");

  System.out.println("get data ready!");

  // 准备传输数据 VData
  VData tVData = new VData();

  tVData.addElement(szPolNo);
  tVData.addElement(tG);

  // 数据传输
  if (!tCertifyInputUI.submitData(tVData,"INSERT")) {
    Content = " 保存失败，原因是: " + tCertifyInputUI.mErrors.getError(0).errorMessage;
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "") {
    tError = tCertifyInputUI.mErrors;
    if (!tError.needDealError()) {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    } else {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
  System.out.println(Content);
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>

