<%
//程序名称：CertifyPrintInput.jsp
//程序功能：
//创建日期：2006-06-30
//创建人  ：Zhang Bin程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="java.util.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%
		String FlagStr = "";
		String Content = "";
		
		LZTempReceptSchema schemaLZTempRecept = new LZTempReceptSchema();
System.out.println("================================================================================================");	
		CErrors tError = null;
		String strOperate = request.getParameter("Operator").trim();
		
		String strReceptHeading = request.getParameter("ReceptHeading").trim(); 
		String strContNo = request.getParameter("ContNo"); 
		String strPayMoney = request.getParameter("PayMoney"); 
		String strAgentCode = request.getParameter("AgentCode"); 
		String strTempFeeNo = request.getParameter("TempFeeNo"); 
		String strOpenDate = request.getParameter("OpenDate"); 
		String strManageCom = request.getParameter("ManageCom"); 
		
		VData vData = new VData();
		
		GlobalInput globalInput = new GlobalInput();
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
		schemaLZTempRecept.setReceptHeading(strReceptHeading);
		schemaLZTempRecept.setContNo(strContNo);
		schemaLZTempRecept.setTempFeeNo(strTempFeeNo);
		schemaLZTempRecept.setPayMoney(strPayMoney);
		schemaLZTempRecept.setAgentCode(strAgentCode);
		schemaLZTempRecept.setOpenTime(strOpenDate);
		schemaLZTempRecept.setManageCom(strManageCom);
		
		vData.addElement(schemaLZTempRecept);
		vData.add(globalInput);
		
		CertTempFeeAuditUI tCertTempFeeAuditUI = new CertTempFeeAuditUI();
		
	 	try {
	 	  if ( !tCertTempFeeAuditUI.submitData(vData, "RecordInform") ) 
	 	  {
		   	if( tCertTempFeeAuditUI.mErrors.needDealError() ) 
		   	{
		   		FlagStr = "Fail";
		   		tError = tCertTempFeeAuditUI.mErrors;
					Content = "操作失败，原因是："+tError.getFirstError();
					
			  } else {
			  	FlagStr = "Fail";
			  	Content = "保存失败，但是没有详细的原因";
			  }
			}
			else
			{
				FlagStr = "SUCC";
			}
			
			if (FlagStr.equals("SUCC"))
			{
				 LZCardSet setLZCard = new LZCardSet( );
         LZCardSchema schemaLZCard = new LZCardSchema( );

         schemaLZCard.setCertifyCode("HT07/05B");
         schemaLZCard.setStartNo(strTempFeeNo);
         schemaLZCard.setEndNo(strTempFeeNo);
         schemaLZCard.setStateFlag("5");

         if (globalInput.ComCode.length()<=4)
         {
             schemaLZCard.setSendOutCom("A"+globalInput.ComCode);
         }
         else
         {
             schemaLZCard.setSendOutCom("B"+globalInput.Operator);
         }
         schemaLZCard.setReceiveCom("SYS");
         schemaLZCard.setSumCount("1");
         schemaLZCard.setHandler(globalInput.Operator);
         schemaLZCard.setHandleDate(PubFun.getCurrentDate());

         setLZCard.add(schemaLZCard);

         // 准备传输数据 VData
         vData.clear();

         vData.addElement(globalInput);
         vData.addElement(setLZCard);
         vData.addElement("SINGLE");
         vData.addElement("1");

         Hashtable hashParams = new Hashtable();
         hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
         vData.addElement(hashParams);

         // 数据传输
         AuditCancelBL tAudiCancelUI = new AuditCancelBL();
         
         if (!tAudiCancelUI.submitData(vData, "INSERT"))  //核销单证
         {
            if( tAudiCancelUI.mErrors.needDealError() ) 
				   	{
				   	System.out.println("HOIHIHHOHO(HPHPHPPH: 核销单证失败！！！！");
				   		FlagStr = "Fail";
				   		tError = tAudiCancelUI.mErrors;
							Content = "操作失败，原因是："+tError.getFirstError();
							
					  } else {
					  	FlagStr = "Fail";
					  	Content = "保存失败，但是没有详细的原因";
					  }
					  
					  vData.clear();
						vData.addElement(schemaLZTempRecept);
						vData.add(globalInput);
						tCertTempFeeAuditUI = new CertTempFeeAuditUI();
					  if (!tCertTempFeeAuditUI.submitData(vData, "DELETERECORD")) //如果核销失败删除核销信息记录
		 	  		{
		 	  			FlagStr = "Fail";
				   		tError = tCertTempFeeAuditUI.mErrors;
							Content = "操作失败，原因是："+tError.getFirstError();
		 	  		}
         }
         else
         {
             FlagStr = "Succ";
         }
			}
			
			if (FlagStr.equals("Succ")) //如果核销成功修改记录表状态
			{
				vData.clear();
				vData.addElement(schemaLZTempRecept);
				vData.add(globalInput);
				
				tCertTempFeeAuditUI = new CertTempFeeAuditUI();
				
				if (!tCertTempFeeAuditUI.submitData(vData, "AuditCansel")) 
	 	  	{
	 	  		FlagStr = "Fail";
		   		tError = tCertTempFeeAuditUI.mErrors;
					Content = "操作失败，原因是："+tError.getFirstError();
					
	 	  	}
	 	  	else
	 	  	{
	 	  		FlagStr = "Succ";
	 	  	}
			}
	 	} catch(Exception ex) {
	 		ex.printStackTrace();
			Content = "保存失败，原因是:" + ex.toString();	  	
	 	}
	 	
%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
