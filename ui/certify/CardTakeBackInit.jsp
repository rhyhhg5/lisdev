<%
//程序名称：CardTakeBackInit.jsp
//程序功能：
//创建日期：2002-08-07
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	String strOperator = globalInput.Operator;
	String strCurTime = PubFun.getCurrentDate();
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
    fm.Operator.value = '<%= strOperator %>';
    fm.curTime.value = '<%= strCurTime %>';
  }
  catch(ex)
  {
    alert("在CardTakeBackInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initCertifyList();
  }
  catch(re)
  {
    alert("CardTakeBackInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 单证列表的初始化
function initCertifyList()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		    //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40";        			//列宽
      iArray[0][2]=40;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="起始单号";    	        //列名
      iArray[1][1]="180";            		//列宽
      iArray[1][2]=180;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][9]="起始单号|NUM";

      iArray[2]=new Array();
      iArray[2][0]="终止单号";    	        //列名
      iArray[2][1]="180";            		//列宽
      iArray[2][2]=180;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][9]="终止单号|NUM";

      iArray[3]=new Array();
      iArray[3][0]="单张保费";    	        //列名
      iArray[3][1]="100";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="单张保额";    	        //列名
      iArray[4][1]="100";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="回收状态";    	        //列名
      iArray[5][1]="50";            		//列宽
      iArray[5][2]=50;            			//列最大值
      iArray[5][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][10]="TakeBackFlag";			//回收状态
      iArray[5][11]="0|^1|正常回收|^2|作废|^3|挂失|^4|销毁";

      CertifyList = new MulLineEnter( "fm" , "CertifyList" ); 
      //这些属性必须在loadMulLine前
      CertifyList.displayTitle = 1;
      CertifyList.loadMulLine(iArray);  
      
    } catch(ex) {
      alert(ex);
    }
}

</script>