var showInfo;

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true && CertifyTypeGrid.checkValue("CertifyTypeGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./CardOrderSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3() //提交校验
{
	var numFlag = false; 		//通过订单数量校验是否订单
	var numFormat = false; 	//校验订单数格式
	var lineAmnt = 0;				//录入错误格式的行数
	
	var strSQL = "";
	strSQL = "select state from LZOrder where Serialno='"+fm.SerialNo.value+"'";
	var arrReslt = easyExecSql(strSQL);
	if (arrReslt[0]==1)
	{
		alert("该批次已经是批准状态，不能提交！");
		return false;
	}
	
	for(i=0;i<CertifyTypeGrid.mulLineCount;i++)
	{
		var Amnt=CertifyTypeGrid.getRowColData(i,4);
		if(Amnt!="0")
		{
			numFlag = true;
		}
		if (!isInteger(Amnt))
		{
			numFormat = true;
			lineAmnt = i+1;
			break;
		}
	}
	if (numFlag==false)
	{
		alert("你没有订任何单证！");
		return false;
	}
	if (numFormat==true)
	{
		alert("第"+lineAmnt+"行录入数量有误！");
		return false;
	}
	return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, SerialNo, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
  	
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  resetForm();
	  fm.SerialNo.value = "";
	  if (SerialGrid.mulLineCount!=0)
	  {
	  	queryClick();
	  }
  }
}

function updateForm()
{
	fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true && CertifyTypeGrid.checkValue("CertifyTypeGrid")) 
		{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./CardOrderSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //修改校验
{
	var numFlag = false; 		//通过订单数量校验是否订单
	var numFormat = false; 	//校验订单数格式
	var updateErrFlag = false;
	var lineAmnt = 0;				//录入错误格式的行数
	
	
	var strSQL = "";
	strSQL = "select state from LZOrder where Serialno='"+fm.SerialNo.value+"'";
	var arrReslt = easyExecSql(strSQL);
	if (arrReslt[0]==1)
	{
		alert("该批次已经是批准状态，不能修改！");
		return false;
	}
	
	for(i=0;i<CertifyTypeGrid.mulLineCount;i++)
	{
		var Amnt=CertifyTypeGrid.getRowColData(i,4);
		if(Amnt!="0")
		{
			numFlag = true;
		}
		if (!isInteger(Amnt))
		{
			numFormat = true;
			lineAmnt = i+1;
			break;
		}
		if (CertifyTypeGrid.getRowColData(i,5)=="3")
		{
			if (CertifyTypeGrid.getRowColData(i,4)!=CertifyTypeGrid.getRowColData(i,7))
			{
				updateErrFlag=true;
				lineAmnt = i+1;
				break;
			}
		}
	}
	if (updateErrFlag==true)
	{
		alert("第"+lineAmnt+"行记录已被批准不能修改！原数量为："+CertifyTypeGrid.getRowColData(lineAmnt-1,7));
		return false;
	}
	if (numFormat==true)
	{
		alert("第"+lineAmnt+"行录入数量有误！");
		return false;
	}
	if (numFlag==false)
	{
		if(!confirm("你取消了所有订单！"))
		{
			return false;
		}
	}
	return true;
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick() //查询批次
{
	var strSQL = "";
	
	strSQL = "select SerialNo,AttachDate,"
		+ " case when State='0' then '征订' else '批准' end ,State from lzorder where 1=1"
	 	+ getWherePart("State");
	;
	var quart = fm.QuarterValue.value;
	if(fm.QuarterValue.value==""||fm.QuarterValue.value==null)
	{
	}
	else
	{
		var staYear = quart.substring(0,4);
		
		//alert(quart.substr(0,4));
		//alert(quart.substr(4,1));
		
		var month;
		
		if (quart.substr(4,1)=="1")
		{
			month = new Array("01","02","03");
		}
		if (quart.substr(4,1)=="2")
		{
			month = new Array("04","05","06");
		}
		if (quart.substr(4,1)=="3")
		{
			month = new Array("07","08","09");
		}
		if (quart.substr(4,1)=="4")
		{
			month = new Array("10","11","12");
		}
				
		strSQL = strSQL 
			+ " and (cast(AttachDate as char(10)) like '%%"+staYear+"-"+month[0]+"%%'"
			+ " or cast(AttachDate as char(10)) like '%%"+staYear+"-"+month[1]+"%%'"
			+ " or cast(AttachDate as char(10)) like '%%"+staYear+"-"+month[2]+"%%')"
	}
	strSQL = strSQL + " order by SerialNo desc";
	turnPage.queryModal(strSQL, SerialGrid);
}

function resetClick()
{
	fm.OrderSta.value ="";
	fm.QuarterValue.value = "";
	fm.StateName.value="";
	fm.State.value="";
	resetForm();
	
}

function initOrder() //页面初始化是显示分公司查询批次中的订单
{
	var strSQL = "select max(Serialno) from LZOrder where state='0'";
	var arrResult = easyExecSql(strSQL);
	fm.SerialNo.value = arrResult[0][0];
	
	strSQL = "select serialNo from LZOrder";
	
	strSQL = "(select a.Certifycode A,b.CertifyName B,"
		+ " a.ordermoney C, 0 D,'' E,'' F,0 G"
		+ " from LZOrderCard a,LMCertifyDes b where a.serialno = (select max(Serialno) from LZOrder where state='0') and a.CertifyCode=b.CertifyCode "
		+ " and a.certifycode not in (select certifycode from LZOrderDetail where serialno = (select max(Serialno) from LZOrder where state='0') and OrderCom='"+fm.ComCode.value+"') "
		+ " union "
		+ " select a.Certifycode A,b.CertifyName B," 
		+ " (select ordermoney from LZOrderCard where Serialno=a.Serialno and CertifyCode=a.CertifyCode) C,"
		+ " a.Amnt D, a.OrderState E, case a.OrderState when '1' then '未阅' when '2' then '打回' else '批准' end F,a.Amnt G"
		+ " from LZOrderDetail a,LMCertifyDes b "
		+ " where a.serialno = (select max(Serialno) from LZOrder where state='0') and a.CertifyCode=b.CertifyCode and OrderCom='"+fm.ComCode.value+"' ) order by E"
		;
	arrResult = easyExecSql(strSQL);
	
	if (arrResult==null)
	{
		return false;
	}else
	{
		displayMultiline(arrResult,CertifyTypeGrid);	
	}
	
	strSQL = "select Note from LZOrder where SerialNo = (select max(Serialno) from LZOrder where state='0')"
	;
	arrResult = easyExecSql(strSQL);
	fm.Note.value = arrResult[0];
}

function SearchOrder() //分公司查询批次中的订单
{
	var tSel = SerialGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return false;
  }
	var serialNo = SerialGrid.getRowColData(tSel-1,1); 
	fm.SerialNo.value = serialNo;
	
	var strSQL = "";
	
	strSQL = "(select a.Certifycode A,b.CertifyName B,"
		+ " a.ordermoney C, 0 D,'' E,'' F,0 G "
		+ " from LZOrderCard a,LMCertifyDes b where a.serialno = '"+serialNo+"' and a.CertifyCode=b.CertifyCode "
		+ " and a.certifycode not in (select certifycode from LZOrderDetail where serialno = '"+serialNo+"' and OrderCom='"+fm.ComCode.value+"') "
		+ " union "
		+ " select a.Certifycode A,b.CertifyName B," 
		+ " (select ordermoney from LZOrderCard where Serialno=a.Serialno and CertifyCode=a.CertifyCode) C,"
		+ " a.Amnt D, a.OrderState E, case a.OrderState when '1' then '未阅' when '2' then '打回' else '批准' end F,a.Amnt G"
		+ " from LZOrderDetail a,LMCertifyDes b "
		+ " where a.serialno = '"+serialNo+"' and a.CertifyCode=b.CertifyCode and OrderCom='"+fm.ComCode.value+"') order by E"
		;
		
	CertifyTypeGrid.clearData();
	arrResult = easyExecSql(strSQL);
	
	if (arrResult)
	{
	  displayMultiline(arrResult,CertifyTypeGrid);	
	}
	
	strSQL = "select Note from LZOrder where SerialNo = '"+serialNo+"'"
	;
	arrResult = easyExecSql(strSQL);
	fm.Note.value = arrResult[0];
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
  	CertifyTypeGrid.clearData();
  	SerialGrid.clearData();
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function requestClick()
{
  showInfo = window.open("./CertifyTypeQuery.jsp");
}