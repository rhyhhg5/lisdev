<%@page contentType="text/html;charset=GBK" %>
<%
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
<%
System.out.println("开始执行UPDATE操作！！！");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  String transact = "";
  String mOperateType = request.getParameter("OperateType");
  String mUpdateType = request.getParameter("UpdateType");
  String mAgentCode = request.getParameter("AgentCode");
  String mManageCom = request.getParameter("ManageCom");
  String mAgentGrade = request.getParameter("AgentGrad");
  String mAgentCom = request.getParameter("AgentCom");		
  System.out.println("操作类型是"+mAgentCode);
  System.out.println("修改的类型是"+mUpdateType);

  LMCertifyDesSet mLMCertifyDesSet = new LMCertifyDesSet();//单证描述表
  LDAgentCardCountSet mLDAgentCardCountSet = new LDAgentCardCountSet();//代理人最大发放表
  CertifyMaxSaveUI mCertifyMaxSaveUI = new CertifyMaxSaveUI();
  VData tVData = new VData();
  if(mOperateType.equals("UPDATE"))
  {
  	System.out.println("操作的符号是"+mOperateType);
  	transact = "UPDATE";
    //对应的操作是按照“代理人编码”进行修改的
  
      String[] strNumber=request.getParameterValues("CertifyMaxGridNo");
      String[] strCertifyCode= request.getParameterValues("CertifyMaxGrid1");
      String[] strCertifyName= request.getParameterValues("CertifyMaxGrid2");
      String[] strMaxCount = request.getParameterValues("CertifyMaxGrid3");
      String[] strPeriod = request.getParameterValues("CertifyMaxGrid4");

      int intLength=0;
      if(strNumber!=null)
      intLength=strNumber.length;
      for(int i=0;i<intLength;i++)
      {
        LDAgentCardCountSchema tLDAgentCardCountSchema = new LDAgentCardCountSchema();
        //代理人最大领取表
        tLDAgentCardCountSchema.setAgentCode(mAgentCode);
        tLDAgentCardCountSchema.setManageCom(mManageCom); 
        tLDAgentCardCountSchema.setAgentCom(mAgentCom);        
        tLDAgentCardCountSchema.setAgentGrade(mAgentGrade);
        tLDAgentCardCountSchema.setCertifyCode(strCertifyCode[i]);
        tLDAgentCardCountSchema.setMaxCount(strMaxCount[i]);
        tLDAgentCardCountSchema.setVerPeriod(strPeriod[i]);
        mLDAgentCardCountSet.add(tLDAgentCardCountSchema);
        tVData.clear();
        tVData.addElement(mOperateType);
        tVData.addElement(mUpdateType);
        tVData.addElement(mLDAgentCardCountSet);        
      }
     
		try
		{
			mCertifyMaxSaveUI.submitData(tVData,"UPDATE");
		}
		catch(Exception ex)
  	{
    	Content = transact+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
    	System.out.println(Content);
  	}
  	//如果在Catch中发现异常，则不从错误类中提取错误信息
  	if (FlagStr=="")
  	{
    	tError = mCertifyMaxSaveUI.mErrors;
    	if (!tError.needDealError())
    	{
      	Content = " 保存成功";
    		FlagStr = "Succ";
    	}
    	else
    	{
    		Content = " 保存失败，原因是:" + tError.getFirstError();
    		FlagStr = "Fail";
    	}
  	}
  }

  if (FlagStr == "Fail")
  {
    tError = mCertifyMaxSaveUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 操作成功! ";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 操作失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
  System.out.println("------end------");
  System.out.println(FlagStr);
  System.out.println(Content);
  System.out.println("------end------");
  System.out.println(FlagStr);
  System.out.println(Content);
  out.println("<script language=javascript>");
  out.println("</script>");
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
        </script>
</html>