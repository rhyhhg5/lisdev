/**
 * <p>程序名称: Certigy.js</p>
 * <p>程序功能: 公用函数变量定义 </p>
 * <p>注释更新人: 王珑</p>
 * <p>最近更新日期: 2006-9-2</p>
 * <p>注意：所有的变量类型为VAR，在JAVA中表示为STRING<p>
 */

//公用变量
/** 日期分隔符,初始值=":" */
var DATEVALUEDELIMITER=":";
/** 域名与域值的分隔符,初始值=":" */
var NAMEVALUEDELIMITER=":";
/** 初始值=":" */
var SBCCASECOLON="：";
/** 域之间的分隔符,初始值="|" */
var FIELDDELIMITER="|";
/** 初始值="｜" */
var SBCCASEVERTICAL="｜";
/** 记录之间的分隔符,初始值="^" */
var RECORDDELIMITER="^";
/** 每一页最大显示的行数,初始值="10" */
var MAXSCREENLINES=10;
/** 内存中存储的最大的页数,初始值="20" */
var MAXMEMORYPAGES=20;
/** 修改(颜色),初始值="FFFF00" */
var BGCOLORU="FFFF00";
/** 添加(颜色),初始值="#00F0F0" */
var BGCOLORI="#00F0F0";
/** 删除(颜色),初始值="#778899" */
var BGCOLORD="#778899";
/** 快捷菜单最大项数 */
var MAXMENUSHORTNUM = 3;


  /**
   * 更换图片
   * <p><b>Example: </b><p>
   * <p>function changeImage(image,gif)<p>
   * @param image 存放图片的对象或框架或页面
   * @param gif 图片的全路径
   */
function changeImage(image,gif)
{
	//image.src='/Images/piccSh/'+gif;
	image.src=gif;  //Modify by yt 2002-05-30
}

  /**
   * 替换字符串函数
   * <p><b>Example: </b><p>
   * <p>replace("Minim123Minim", "123", "Minim") returns "MinimMinimMinim"<p>
   * @param strExpression 字符串表达式
   * @param strFind 被替换的子字符串
   * @param strReplaceWith 替换的目标字符串，即用strReplaceWith字符串替换掉strFind
   * @return 返回替换后的字符串表达式
   */
function replace(strExpression,strFind,strReplaceWith)
{
	var strReturn;
	strReturn =(strExpression==null?"":strExpression);
	//利用正则表达式进行全局替换
	strReturn = strExpression.replace(eval("/"+strFind+"/g"),strReplaceWith);
	return strReturn;
}

  /**
   * 去掉字符串头尾空格
   * <p><b>Example: </b><p>
   * <p>trim(" Minim ") returns "Minim"<p>
   * @param strValue 字符串表达式
   * @return 头尾无空格的字符串表达式
   */
function trim(s)
{
	//使用正则表达式进行全局替换
	return s.replace(/(^\s*)|(\s*$)/g,"");
}

  /**
   * 对输入域是否是整数的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isInteger(strValue)
{
	//使用正则表达式进行判断
	var chkExp=/^\d+$/;
	return (chkExp.test(strValue));
}

  /**
   * 对输入域是否是数字的校验
   * <p><b>Example: </b><p>
   * <p>isNumeric("Minim") returns false<p>
   * <p>isNumeric("123.1") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是数字, false--不是数字）
   */
function isNumeric(strValue)
{
	//使用正则表达式进行判断
	var chkExp=/^\d+(\.\d+)?$/;
	return (chkExp.test(strValue));
}

  /**
   * 离开域时的数字校验
   * <p><b>Example: </b><p>
   * <p>checkNumber(HTML.Form.Object.Name)<p>
   * @param Field HTML页面的对象名称
   * @return true或产生一个“errorMessage("请输入合法的数字")”
   */
function checkNumber(Field)
{
	var strValue=Field.value;
	if( trim(strValue)!="" && !isNumeric(strValue) )
	{
	  errorMessage("请输入合法的数字");
		Field.focus();
		Field.select();
		return false;
	}
	return true;
}

 