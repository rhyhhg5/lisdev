//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{   
    if(!chkMulLine()){
     	return false;
    }   
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

//单证类型码变更时
function Query()
{  
   var strSQL  = "select prtno,certifycode ,subcode,startno,endno,activedate  "
            + "from lzcardprint "
            + "where 1=1 "
            + getWherePart('CertifyCode', 'CertifyCode')
            + getWherePart('SubCode', 'CardType')
            + getWherePart('EndNo', 'StartNo','>=')
            + getWherePart('StartNo', 'EndNo','<=')   
            + "order by startno ";
	turnPage.queryModal(strSQL, CertifyActiveGrid);
	
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
}

function chkMulLine()
{
	var i;
	var iCount = 0;
	var rowNum = CertifyActiveGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(CertifyActiveGrid.getChkNo(i))
		{
			iCount++;
		}
	}
	if(iCount == 0)
	{
		alert("请选择要保存的记录!");
		return false
	}
	return true;
}
