var showInfo;
var mDebug="0";
var tSaveFlag = "0";

/*******************************************************************************
 * Name        :main_init
 * Author      :Zhangbin
 * CreateDate  :2006-04-17
 * ModifyDate  :
 * Function    :初始化主程序
 * Parameter   :A_jsp---fm.action;Target_Flag (0--无target，1有target),T_jsp--target 对应的页面 ;Info---提示信息;Operate---操作标识
 *
*/
function main_init(A_jsp,Target_Flag,T_jsp,Info,Operate)
{
  var i = 0;
  var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action ="./"+A_jsp;
  if(Operate=="INSERT")
  {
    fm.fmtransact.value="INSERT";
    
  }
  if(Operate=="QUERY")
  {
    fm.fmtransact.value="QUERY";
  }
  if(Operate=="UPDATE")
  {
    fm.fmtransact.value="UPDATE";
  }
  if(Operate=="DELETE")
  {
    fm.fmtransact.value="DELETE";
  }
  if(Target_Flag=="1")
  {
    fm.target = "./"+T_jsp;
  }
  else
  {
    fm.target = "fraSubmit";
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


function submitForm()
{
	if( verifyInput3() == false ) return false;
	
	if (fm.UserName.value.match(/[^\u4e00-\u9fa5]/g))
	{
	alert("[用户名称]只能录入中文或者为空");
	return false;
	}
	else
	{
  		if(fm.UserName.value.length>6)
  		{
 		alert("[用户名称]不能超过6个汉字,请修改后重新保存");
 		return false;
 		}
    }
  
  	
    if (confirm("您确实要保存该记录吗?"))
    {
      main_init("LZCertifyUserSave.jsp","0","_blank","保存","INSERT");
    }
}
                 
function updateClick()
{
	if( verifyInput3() == false ) return false;
	var strSQL = "select * from LZCertifyUser where UserCode = '"+fm.UserCode.value+"'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult==null)
	{
		alert("没有此单证管理员的信息，不能进行修改！");
		return false;
	}
  
  if (confirm("您确实要修改该记录吗?"))
  {
    main_init("LZCertifyUserSave.jsp","0","_blank","保存","UPDATE");
  }
}
               
function queryClick()
{
	//下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./UserQuery.html");
}
               
function deleteClick()
{
	var strSQL = "select * from LZCertifyUser where UserCode = '"+fm.UserCode.value+"'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult==null)
	{
		alert("没有此单证管理员的信息，不能进行删除！");
		return false;
	}
	
	strSQL = "select sum(Sumcount) from lzcard where receivecom ='B" + fm.all('UserCode').value +"'" ;
	arrResult = easyExecSql(strSQL);
	if (arrResult[0]!='0')
	{
		alert("此单证管理员名下还有未发放的单证，不能删除！");
		return false;
	}
	
	strSQL= "select * from LZAccess where sendoutcom='B"+fm.UserCode.value+"' or receivecom='B"+fm.UserCode.value+"'";
	arrResult = easyExecSql(strSQL);
	
	if (arrResult!=null)
	{
		alert("删除此单证管理员之前请先删除相关发放关系！");
		return false;
	}
	
	if (confirm("您确实要删除该记录吗?"))
  {
    main_init("LZCertifyUserSave.jsp","0","_blank","保存","DELETE");
  }
  
	return false;
}

function verifyInput3()
{
	if(fm.ComCode.value==""||fm.ComCode.value==null)
	{
		tSaveFlag = "0";
		alert("用户机构代码不能为空!");
		return false;
	}
	if(fm.UserCode.value==""||fm.UserCode.value==null)
	{
		tSaveFlag = "0";
		alert("用户编码不能为空!");
		return false;
	}
	if(fm.StateFlag.value==""||fm.StateFlag.value==null)
	{
		tSaveFlag = "0";
		alert("用户有效状态不能为空!");
		return false;
	}
	var strSQL = "select UserCode,Comcode from LDUser where UserCode ='" + fm.all('UserCode').value +"'" ;
	var arrResult = easyExecSql(strSQL);
	if (arrResult==null)
	{
		alert("无效用户名！");
		return false;
	}
	if (fm.ComCode.value!=arrResult[0][1])
	{
		alert("录入机构与用户所属机构不符！");
		return false;
	}
	
	return true;
}

function afterCodeSelect( cCodeName, Field )
{
	//选择了处理
	if( cCodeName == "UserCode")
	{
		var strSQL = "select ComCode from LDUser where UserCode ='" + fm.all('UserCode').value +"'" ;
		var arrResult = easyExecSql(strSQL);
		if (arrResult)
		{
			fm.all("ComCode").value=arrResult[0];
		}				
		
	}
}

function afterQuery(arrQueryResult)
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all( 'UserCode' ).value = arrResult[0][0];
		fm.all('UserName').value = arrResult[0][1];
		fm.all('ComCode').value = arrResult[0][2];
		fm.all('ComName').value = arrResult[0][3];
		fm.all('StateFlag').value = arrResult[0][4];
		fm.all('StateFlagName').value = arrResult[0][5];
		fm.all('UserDescription').value = arrResult[0][6];
	}
}

function afterSubmit(FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 	  
	  //fm.ComCode.value = "";
		//fm.ComName.value = "";
		//fm.SendOutName.value = "";
		fm.reset();
		//fm..value = "";
		//fm..value = "";
		//fm..value = "";
		
		
	  content="操作成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  fm.reset();
	  initForm();
	}
}