<%
//Name：LZCertifyUserInit.jsp
//Function：单证管理员的初始化程序
//Date：2006-04-14
//Author  ：zhangbin
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
  //添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	String strOperator = globalInput.Operator;
	String strComCode = globalInput.ComCode;
	String strCurDate = PubFun.getCurrentDate();
%>

<script language="JavaScript">

function initInpBox( )
{
  try 
  {
		fm.ComCode.value="<%=strComCode%>";
		fm.UserCode.value="";
		fm.StateFlag.value="0";
		fm.UserDescription.value="";
		fm.ComName.value=getComName(<%=strComCode%>);
  } catch(ex) {
    alert("在RegisterInputInit.jsp-->initInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
        
  }
  catch(re)
  {
    alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function getComName(comC)
{
	var strSQL="select name from LDCom where comcode= '"+ comC +"' ";
	var comN = easyExecSql(strSQL);	
	return comN[0][0];
	//if (comN)
	//{	
	//	fm.all("ManageComName").value= comN[0][0];
	//}
}

//立案附件信息
</script>