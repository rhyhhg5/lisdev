<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; 	//记录管理机构
    var comcode = "<%=tGI.ComCode%>";		//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="CertifyStateModifyInput.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifyStateModifyInit.jsp" %>
<title>维护单证状态</title>
</head>

<body onload="initForm();">
	<form action="./CertifyStateModifySave.jsp" method=post name=fm target="fraSubmit">
	<!-- 单证信息部分 -->
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>
		<table class="common" align=center>
			<tr class="common">
				<td class="title">单证编码</td>
				<td class="input"><Input class=codeNo readonly=true name=CertifyCode  verify="单证编码|code:certifycodedes" ondblclick="return showCodeList('certifycodedes',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('certifycodedes',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true ></td>
				<td class="title">起始号码</td>
				<td class="input"><input class="common" name="StartNo" /></td>
				<td class="title">终止号码</td>
				<td class="input"><input class="common" name="EndNo" /></td>
			</tr>

		</table>
		<input class="cssButton" type="button" value="查询" onclick="easyQueryClick();" />
		<INPUT  type="hidden" class=Common name=querySql >
		
		<table>
			<tr>
				<td class=common>
					<IMG src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divLCPol1);">
				</td>
				<td class=titleImg>单证状态信息</td>
			</tr>
		</table>
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid" ></span>
					</td>
				</tr>
			</table>

			<table align=center>
			<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
		 </table>
		</div>
		<table>
			<tr>
				<td class= titleImg>单证信息</td>
			</tr>
		</table>
		<table class= common align=center>

			<TR class= common>
				<TD class= title>定额状态</TD>
				<TD class= input>
					<Input class= 'codeno' name=upStateflagModify verify="定额状态|code:lzcardstate" ondblclick="return showCodeList('lzcardstate',[this,UserCodeName],[0,1]);" onkeyup="return showCodeListKey('lzcardstate', [this,UserCodeName],[0,1]);"><input  class="codename" name="UserCodeName" readonly=true > 
				</TD>
				<TD class= title>普通状态</TD>
				     <TD>
				    <Input class='codeno' name=upStateModify verify="普通状态|code:lzcardstateflag" ondblclick="return showCodeList('lzcardstateflag',[this,UserCodeName2],[0,1]);" onkeyup="return showCodeListKey('lzcardstateflag',[this,UserCodeName2],[0,1]);"><input class="codename" name="UserCodeName2" readonly=true> 
				    </TD>
			</TR>
		</table>
		<INPUT VALUE="修改单证状态" class="cssButton" TYPE=button onclick="modifyCertify();">
		<input type=hidden id="fmtransact" name="fmtransact">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>