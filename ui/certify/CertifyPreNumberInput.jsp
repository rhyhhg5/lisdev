<html>
<%
//程序名称：银行代收对帐清单
//程序功能：
//创建日期：2003-3-25
//创建人  ：刘岩松程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.bank.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%    
    String CurrentDate= PubFun.getCurrentDate(); 
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);           
%>
  <SCRIPT>
	var CurrentYear=<%=tCurrentYear%>;  
	var CurrentMonth=<%=tCurrentMonth%>;  
	var CurrentDate=<%=tCurrentDate%>;
	var CurrentTime=CurrentYear+""+CurrentMonth+CurrentDate;
  </SCRIPT> 
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="CertifyPreNumberInput.js"></SCRIPT>
<SCRIPT src="CertifyNumberCommon.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifyPreNumberInputInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >
<form action="./CertifyDescQuerySave.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->

   <Div id= "divLLReport1" style= "display: ''">

   	<Table class= common>   		

      <TR class= common>
         <TD class= title>
           单证类型码：
         </TD>
         <TD class= input>
           <Input class= common name= CardType verify="单证类型|NOTNULL" elementtype=nacessary>           
         </TD>
        <TD  class= title> 
        批次号：
        </TD>
        <TD class= input>
           <Input class= common name= PrtNo1 >           
         </TD>
       </TR> 
       <Input class= common type= hidden name= PageType value="1">         
       <Input class= common type= hidden name= Operate> 
       <Input class= common type= hidden name= PrtNo> 
    </Table>
    <input class="cssButton" type=button value="查  询" onclick="easyQueryClick();">
		<Div  id= "divCard" style= "display: ''">
		      <table  class= common>
		          <tr  class= common>
		            <td text-align: left colSpan=1>
		          <span id="spanCertifyList" >
		          </span>
		        </td>
		      </tr>
		    </table>
			</div>
		<input class="cssButton" type=button style="display: none" value="生成单证号码" onclick="submitForm();">
		<input class="cssButton" type=button value="下载清单" onclick="printData();">
		<input class="cssButton" type=button value="下载成功" onclick="confirmClike();">
		<input class="cssButton" type=button style="display: none" value="删  除" onclick="deleteClike();">
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
