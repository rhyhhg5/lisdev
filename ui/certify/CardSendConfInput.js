var turnPage=new turnPageClass(); 
var turnPage1=new turnPageClass(); 

var showInfo;
window.onfocus=myonfocus; 


//提交，保存按钮对应操作
function submitForm()
{
	if(fm.ComCode.value.length >=8)
	{
		alert("您无权配置单证发放权限！");
		return false;
	}
	var handleCount=CertifyList.mulLineCount; //得到MulLine的行数
	if (handleCount==0)
	{
		alert("请先输入单证信息!");
		return false;
	}
	for(var i=0; i<handleCount; i++)
	{
		if(CertifyList.getRowColData(i,1)==""||CertifyList.getRowColData(i,2)==null||CertifyList.getRowColData(i,3)==""||CertifyList.getRowColData(i,3)==null)
		{
			alert("发放与被发放者不能为空!");
			return false;
		}
	}

	fm.all("OperateFlag").value="INSERT||CONFIG";
	if(1==1)// verifyInput() == true && CertifyList.checkValue("CertifyList") ) 
	{
		  var i = 0;
		  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			
		  fm.submit(); //提交
	}
}

function afterSubmit( FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
	  CertifyList.clearData("CertifyList");
	  CertifyShowGrid.clearData("CertifyShowGrid");
	  CertifyList.addOne("CertifyList");
	  
	  fm.SendOutCode.value = "";
		fm.ReceiveCode.value = "";
		fm.SendOutName.value = "";
		fm.ReceiveName.value = "";
		
	  content="操作成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

function queryClick()
{
	//var comCodeEx = 'A'+fm.ComCode.value;
	
	var receiveC = fm.ReceiveCode.value;
	var sendOutC = fm.SendOutCode.value;
	var constr = "";	
  if (sendOutC != null && sendOutC != "") {
  	constr = " and  sendoutcom = '" + "A" + sendOutC + "'";
  }
  if (receiveC != null && receiveC != "") {
  	constr = constr + " and  receivecom = '" + "B" + receiveC + "'";
  }
  
	var strSQL = "";
	strSQL = "select substr(sendoutcom,2),substr(receivecom,2) from LZAccess where 1=1" 
		+ constr
		+ " and substr(sendoutcom,2) in (select usercode from LZCertifyUser where 1=1" 
		+ getWherePart( 'ComCode' ,'ComCode','like')	
		+ ")"
		+ " order by sendoutcom,receivecom "
		;	

	turnPage1.queryModal(strSQL, CertifyShowGrid); 
/***************************
turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
//判断是否查询成功
if (!turnPage.strQueryResult)
{
	alert("未查询到满足条件的数据！");
	return false;
}

//设置查询起始位置
turnPage.pageIndex = 0;
//在查询结果数组中取出符合页面显示大小设置的数组
turnPage.pageLineNum = 10 ;
//查询成功则拆分字符串，返回二维数组
turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
//设置初始化过的MULTILINE对象
turnPage.pageDisplayGrid = QCerGrid;
//保存SQL语句
turnPage.strQuerySql = strSQL ;
arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
//调用MULTILINE对象显示查询结果
displayMultiline(arrDataSet, turnPage.CertifyShowGrid);
**************************/	
}

function resetCon()
{
	fm.SendOutCode.value = "";
	fm.ReceiveCode.value = "";
	fm.SendOutName.value = "";
	fm.ReceiveName.value = "";
	CertifyShowGrid.clearData();
}

function deleteData()
{
	var tSel = CertifyShowGrid.getSelNo();	
	
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return false;
  }
	if(confirm("确定要删除所选发放关系吗?"))
	{
		fm.all("OperateFlag").value="DELETE||CONFIG";
		
		var i = 0;
	  var showStr="正在核销数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.submit();
	}	
	
}
