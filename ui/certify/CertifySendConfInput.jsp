<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：核销管理
//程序功能：
//创建日期：2006-04-17
//创建人  ：Javabean
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="CertifySendConfInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifySendConfInit.jsp"%>
</head>
<body  onload="initForm()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./CertifySendConfSave.jsp" method=post name="fm" target="fraSubmit">
	
    <!-- 发放权限配置 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>单证发放权限配置</td></tr>
    </table>
    
	<Div  id= "divCertifyList" style= "display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanCertifyList" ></span>
         	</td>
       	</tr>
	  	</table>
	  	<br>
	  <input class="cssButton" type="button" value="保存" onclick="submitForm()" >
	</div>
	
	<br><hr><br>
	
	<table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divCertifyShow);"></td>
    	<td class= titleImg>单证发放权限查询</td></tr>
  </table>
  <strong>查询条件</strong>
  
  <table class="common">
        <tr class="common">
          <td class="title">发放者</td>
          <td class="input">
          	<Input class="codeno" name=SendOutCode  
             ondblclick="return showCodeList('searchsendoutcode', [this,SendOutName],[0,1],null,fm.ComCode.value,'ComCode',1);"
             onkeyup="return showCodeListKey('searchsendoutcode', [this,SendOutName],[0,1],null,fm.ComCode.value,'ComCode',1);"><Input class=codename name=SendOutName type="text" >
          </td>
          
          <TD class= title>接受者</TD>
         <TD class= input>
          <input class="codeno" name="ReceiveCode" 
            ondblclick="return showCodeList('searchreceivecode', [this,ReceiveName],[0,1],null,fm.SendOutCode.value,'ComCode',1);"
            onkeyup="return showCodeListKey('searchreceivecode', [this,ReceiveName],[0,1],null,fm.SendOutCode.value,'ComCode',1);"><input name = "ReceiveName" class=codename type="text">
         </TD>
         <td class="title"></td>
         <td class="input"> </td>
        </tr>
        <!-- 用"总保额"来表示"最大金额" -->
   </table>
  
	<input class="cssButton" type="button" value="查询" onclick="queryClick()" >&nbsp;&nbsp;
	<input class="cssButton" type="button" value="重置" onclick="resetCon()" >
	
	<Div  id= "divCertifyShow" style= "display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanCertifyShowGrid" ></span>
         	</td>
       	</tr>
	  	</table>
	  	<Div id= "divPage" align=center style= "display:''">
		    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage1.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage1.lastPage();">
  		</Div>
	  	<br>
	  <input class="cssButton" type="button" value="删除" onclick="deleteData()" >
	</div>
	
	<input type="hidden" name="OperateFlag" value="">
	<input type="hidden" name="ComCode" value="<%=strComCode%>">
	<input type="hidden" name="CurrentDate" value="">
	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
   
  </form>
</body>
</html>
