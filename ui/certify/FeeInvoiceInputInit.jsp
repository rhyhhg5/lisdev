<%
//程序名称：CertifyTakeBackInit.jsp
//程序功能：
//创建日期：2002-10-08
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%> 
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
  String Operator=globalInput.Operator;
  String Comcode=globalInput.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
  String AheadDays="-90";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
  String ManageCom = "";
  String CertifyCode = "";
  String CertifyName = "";
  if (Comcode.length()==8) {
    ManageCom = Comcode;
    ExeSQL tExeSQL = new ExeSQL();
    String tSQL = "select a.codename,b.certifyname from ldcode a,lmcertifydes b where a.codetype='invoicetype' and a.code='"
                + ManageCom
                + "' and a.codename=b.certifycode with ur";
    SSRS tSSRS = tExeSQL.execSQL(tSQL);
    CertifyCode = tSSRS.GetText(1, 1);
    CertifyName = tSSRS.GetText(1, 2);
  }
%>
<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
  	fm.reset();
  }
  catch(ex)
  {
    alert("在CertifyCancelInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initCertifyList();
  }
  catch(re)
  {
    alert("CertifyCancelInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 单证列表的初始化
function initCertifyList()
{                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		  //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30";        				//列宽
      iArray[0][2]=50;          				//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      if (fm.typeRadio[0].checked) {
      	iArray[1][0]="保单号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      } else if (fm.typeRadio[1].checked) {
      	iArray[1][0]="批单号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      } else {
      	iArray[1][0]="结算单号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      }
      iArray[1][1]="80";        			//列宽
      iArray[1][2]=80;          			//列最大值
      iArray[1][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="实收号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="80";        			//列宽
      iArray[2][2]=80;          			//列最大值
      iArray[2][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="发票号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[3][1]="80";        		  //列宽
      iArray[3][2]=80;          			//列最大值
      iArray[3][3]=3;              		//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="发票号";    	    //列名
      iArray[4][1]="80";            		//列宽
      iArray[4][2]=80;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="发票状态";    	    //列名
      iArray[5][1]="80";            		//列宽
      iArray[5][2]=80;            			//列最大值
      iArray[5][3]=30;              			//是否允许输入,1表示允许，0表示不允许
      
      CertifyList = new MulLineEnter( "fm" , "CertifyList" ); 
      //这些属性必须在loadMulLine前
      CertifyList.mulLineCount = 1;     
      CertifyList.displayTitle = 1;
      CertifyList.hiddenPlus = 1;     
      CertifyList.hiddenSubtraction = 1;
      CertifyList.loadMulLine(iArray);  
      
    } catch(ex) {
      alert(ex);
    }
}

function getManageName(cCode)
{
	var strSQL = "select Name from LDCom where ComCode = '"+cCode+"'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult!=null)
	{
		var cName = arrResult[0];
		return cName;
	}
	else
	{
		return '';
	}
}

</script>