
<%
	//Creator :xp
	//Date :2011-06-07
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<script>

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在CardUpActiveDateInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initSelBox();
    initCertifyActiveGrid();
  }
  catch(re)
  {
    alert("CardUpActiveDateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCertifyActiveGrid()
{
	var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";    			    //列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="单证编码";         			//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
 
      iArray[3]=new Array();
      iArray[3][0]="单证类型码";         			//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="单证起始号";         			//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="单证终止号";         		    //列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      
	  iArray[6]=new Array();
      iArray[6][0]="最晚激活日期";          //列名
      iArray[6][1]="80px";                 //列宽
      iArray[6][2]=100;                    //列最大值
      iArray[6][3]=1;                      //是否允许输入,1表示允许,0表示不允许    
      iArray[6][9]="Date";
      
    
    CertifyActiveGrid = new MulLineEnter( "fm" , "CertifyActiveGrid" ); 
    CertifyActiveGrid.canChk = 1;
    CertifyActiveGrid.mulLineCount = 0;       
    CertifyActiveGrid.displayTitle = 1;

    CertifyActiveGrid.locked = 1;	
	CertifyActiveGrid.hiddenPlus = 1;
	CertifyActiveGrid.hiddenSubtraction = 1;
	CertifyActiveGrid.loadMulLine(iArray);  
    CertifyActiveGrid.detailInfo="单击显示详细信息";
    CertifyActiveGrid.selBoxEventFuncName = "SetModifyInfo";
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
