<html>
<%@page import="com.sinosoft.lis.certify.*"%>
<%
//程序名称：
//程序功能：
//创建日期：2002-10-14 10:20:43
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容

	String strCertifyClass = (String)session.getAttribute("CertifyClass");
	
	session.removeAttribute("CertifyClass");
	
	if( strCertifyClass == null || strCertifyClass.equals("") ) {
		strCertifyClass = CertifyFunc.CERTIFY_CLASS_CERTIFY;
	}
	
	String strCertifyCode = "";

	if( strCertifyClass.equals(	CertifyFunc.CERTIFY_CLASS_CERTIFY ) ) {
		strCertifyCode = "<input class=\"code\" name=\"CertifyCode\" " +
											" ondblclick=\"return showCodeList('CertifyCode', [this,CertifyName],[0,1],null,null,null,1);\" " +
											" onkeyup=\"return showCodeList('CertifyCode', [this,CertifyName],[0,1],null,null,null,1);\" " +
											" verify=\"单证编码|NOTNULL\" elementtype=nacessary>";
	} else {
		strCertifyCode = "<input class=\"code\" name=\"CertifyCode\" " +
											" ondblclick=\"return showCodeList('CardCode', [this]);\" " +
											" onkeyup=\"return showCodeList('CardCode', [this]);\"" +
											" verify=\"单证编码|NOTNULL\" elementtype=nacessary>";
	}
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="CertifyNumberCommon.js"></SCRIPT>

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>


<SCRIPT src="CertifyPrintInput.js"></SCRIPT>
<%@include file="CertifyPrintInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./CertifyPrintSave.jsp" method=post name=fm target="fraSubmit">
		<input class="cssButton" type="button" value="定单入库" onclick="requestClick()" >
    	<input class="cssButton" type="button" value="提&nbsp;&nbsp;&nbsp;单" onclick="confirmClick()" style= "display: 'none'">
    	<input class="cssButton" type="button" value="定单查询" onclick="query()" style= "display: 'none'">
    	<input class="cssButton" type="button" value="单证查询" onclick="queryClick()" style= "display: 'none'">
    	<br>
    <div style="width:120">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyPrintList);"></td>
          <td class="titleImg">印刷管理</td></tr></table></div>
    
    <div id= "divCertifyPrintList" style= "display: ''">
      <table class="common">
        <tr class="common">
          <td class="title">印刷批次号</td>
          <td class="input" title="请查询出要提单的印刷批次号">
          	<input class="readonly" readonly name="PrtNo">
          </td>

          
          <td class="title">单证类型</td>
          <td class="input"><input class="readonly" name="CardType" maxlength=2 verify="单证类型|NOTNULL" ReadOnly></td>
          
        <tr class="common">
          <td class="title">单证编码</td>
          <td class="input">
          	<input class="code" name="CertifyCode" 
	            ondblclick="return showCodeList('CardCode', [this,CertifyName],[0,1],null,null,null,1,350);"
	            onkeyup="return showCodeListKey('CardCode', [this,CertifyName],[0,1],null,null,null,1,350);">
          	<!-- <%= strCertifyCode %> -->
          </td>
          
          
          <td class="title">单证名称</td>
          <td class="input"><input class="ReadOnly" name="CertifyName" style="width:100%" readonly></td></tr>
          
        <tr class="common">
          <td class="title">单证价格</td>
          <td class="input"><input class="common" name="CertifyPrice" verify="单证价格|NUM&NOTNULL" elementtype=nacessary></td>
          
          <td class="title">管理机构</td>
          <td class="input"><input class="readonly" readonly name="ManageCom"></td></tr>
          	
        <tr class="common">
          <td class="title">数量</td>
          <td class="input"><input class="common" name="SumCount" onblur="calEndNo()" verify="数量|NUM&NOTNULL" elementtype=nacessary></td>
          
           <TD class= title>
           最终激活日期
         </TD>
         <TD class= Input id='Active1' >
           <input class="coolDatePicker" dateFormat="short" name= ActiveDate verify="最终激活日期|DATE" elementtype=nacessary>
         </TD>
         <TD class= Input id='Active2' >
           <input class="coolDatePicker" dateFormat="short" name= ActiveDate1 verify="最终激活日期|DATE" >
         </TD>
          
        <tr class="common">
          <td class="title">起始号</td>
          <td class="input"><input class="common" name="StartNo" verify="起始号|NUM&NOTNULL" onblur="calEndNo()"></td>
          
          <td class="title">终止号</td>
          <td class="input"><input class="common" name="EndNo" verify="终止号|NUM&NOTNULL" onfocus="calEndNo()"></td></tr>

        <tr class="common" height=20><td></td></tr>
          
        <tr class="common">
          <td class="title">厂商编码</td>
          <td class="input"><input class="common" name="ComCode"></td>
          
          <td class="title">电话</td>
          <td class="input"><input class="common" name="Phone"></td></tr>

        <tr class="common">
          <td class="title">定单操作员</td>
          <td class="input"><input class="readonly" readonly name="OperatorInput"></td>
          
          <td class="title">联系人</td>
          <td class="input"><input class="common" name="LinkMan"></td></tr>
          
        <tr class="common">
          <td class="title">定单日期</td>
          <td class="input"><input class="coolDatePicker" name="InputDate" verify="定单日期|DATE&NOTNULL" elementtype=nacessary></td>

          <td class="title">定单操作日期</td>
          <td class="input"><input class="readonly" readonly name="InputMakeDate"></td></tr>
          
        <tr class="common" height=20><td></td></tr>
          
        <tr class="common">
          <td class="title">提单操作员</td>
          <td class="input"><input class="readonly" readonly name="OperatorGet"></td>
          
          <td class="title">提单人</td>
          <td class="input"><input class="common" name="GetMan"></td></tr>
          
        <tr class="common">
          <td class="title">提单日期</td>
          <td class="input"><input class="coolDatePicker" name="GetDate"></td>
          
          <td class="title">提单操作日期</td>
          <td class="input"><input class="readonly" readonly name="GetMakeDate"></td></tr>
          
      </table>
    </div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=prtNo>
    <input type=hidden name=sql_where value=''>
    <input type=hidden name=CertifyClass value='<%= strCertifyClass %>'>
    <input type=hidden name=OperateType value=''> <!-- 1:定单 2:提单 -->
    <input type=hidden name=OptType > <!-- 0:卡单 1:撕单 -->
    <input type=hidden name=checkRule > <!-- 1:有校验 2:无校验 -->
    <input type=hidden name=certifyLength >
    <input type=hidden name="LimitFlag" >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
