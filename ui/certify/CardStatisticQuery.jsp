<html>
<%
//name :CertifyMaxInput.jsp
//function :Query Certify Info and Show Info Based On ManageCom and AgentGrade
//Creator :刘岩松
//date :2003-04-18
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	
    GlobalInput tGI = new GlobalInput();        
    tGI = (GlobalInput)session.getValue("GI");
 	
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
 	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
 	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
 	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
 	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
 	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
 	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
 	<SCRIPT src="CardStatisticQuery.js"></SCRIPT>
 	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 	<%@include file="CardStatisticQueryInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm target="fraSubmit">	
  <Div id= "divLLReport1" style= "display: ''">

   	<table class= common>
   		<tr class= common>
				<td class="title"> 统计机构：</td>
				<td class="input"><input class="codeno" name="companyCode" value='<%= tGI.ComCode %>' verify="统计机构|NOTNULL"
          	ondblclick="return showCodeList('cardsendoutcode',[this,companyCodeName],[0,1]);"
            onkeyup="return showCodeListKey('cardsendoutcode',[this,companyCodeName],[0,1]);"
          	><input name = "companyCodeName" class=codename type="text" elementtype=nacessary ></td>
				<td class="title">是否含下级机构：</td>
				<td class="input"> <select name="include" elementtype=nacessary ><option value="0">不包含</option><option value="1">包含</option></select></td>
   			<td class="title"> 单证类型：</td>
				<td class="input">
					<select name="cardType" elementtype=nacessary >
						<option value="D">定额单证</option>
						<option value="P">普通单证</option>
						<option value="A">全部单证</option>
					</select>
				</td>
   		</tr>
   		<tr class= common>
				<td class="title"> 单证编码：</td>
				<td class="input"><input class="codeno" name="certifyCode"	ondblclick="return showCodeList('cardcode', [this,certifyCodeName],[0,1],null,null,null,1,350);" onkeyup="return showCodeListKey('cardcode', [this,certifyCodeName],[0,1],null,null,null,1,350);"><Input class= codename name="certifyCodeName" readonly></td>
				<td class= title> 单证起始号：</TD>
				<td class="input"><Input class= common name=startNo></td>
				<td class= title> 单证终止号：</TD>
				<td class="input"><Input class= common name=endNo></td>
			</tr>
   		<tr class= common>
				<td class="title"> 发放人：</td>
				<td class="input"><input class="codeno" name="sendOut"	ondblclick="return showCodeList('cardsendoutcode', [this,sendOutName],[0,1]);" onkeyup="return showCodeListKey('cardsendoutcode', [this,sendOutName],[0,1]);"><Input class= codename name="sendOutName" readonly></td>
				<td class="title"> 领用人：</td>
				<td class="input"><input class="codeno" name="reveive"	ondblclick="return showCodeList('cardsendoutcode', [this,reveiveName],[0,1]);" onkeyup="return showCodeListKey('cardsendoutcode', [this,reveiveName],[0,1]);"><Input class= codename name="reveiveName" readonly></td>
				<td class="title"> 回销人：</td>
				<td class="input"><input class="codeno" name="takeBack"	ondblclick="return showCodeList('cardsendoutcode', [this,takeBackName],[0,1],null,'<%=tGI.Operator%>',null,1);" onkeyup="return showCodeListKey('cardsendoutcode', [this,takeBackName],[0,1],null,'<%=tGI.Operator%>',null,1);"><Input class= codename name="takeBackName" readonly></td>
			</tr>
			<tr class= common>   			
				<td class= title> 领用起始日期：</td>
				<td class= input><Input class="coolDatePicker" dateFormat="short" name=sendStartDate ></td>
				<td class= title> 领用终止日期：</td>
				<td class= input><Input class="coolDatePicker" dateFormat="short" name=sendEndDate ></td>                     
   			<td class= title> 状态类型：</TD>
				<td class= input><!--input class="codeno" name="stateType"	ondblclick="return showCodeList('certifystate', [this,stateTypeName],[0,1]);" onkeyup="return showCodeListKey('takebackstate', [this,stateTypeName],[0,1]);"><Input class= codename name="stateTypeName" readonly-->
					<select name="stateType" >
						<option></option>
						<option value="11">未领用</option>
						<option value="12">已领用</option>
						<option value="13">已录入</option>
						<option value="2">正常回销</option>
						<option value="3">空白回销</option>
						<option value="4">遗失</option>
						<option value="5">损毁</option>
						<option value="6">作废</option>
					</select>
				</td>
			</tr>
   		<tr class= common>   			
				<td class= title> 回销起始日期：</td>
				<td class= input><Input class="coolDatePicker" dateFormat="short" name=backStartDate ></td>
				<td class= title> 回销终止日期：</td>
				<td class= input><Input class="coolDatePicker" dateFormat="short" name=backEndDate ></td>                     
   			<td class= title> 保单号：</TD>
				<td class= input><Input class= common name=contNo></td>
			</tr>
 		</table>
    <Input type= "hidden" name= querySql>
 		<input class="cssButton" type= button value="打印报表" onclick="printInfo();">

</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>