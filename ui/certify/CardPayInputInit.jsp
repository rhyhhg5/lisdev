<%
//Creator :刘岩松
//Date :2003-04-18
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
 		String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('PayNo').value = '';   
    fm.all('ManageCom').value = <%=Comcode%>;
    fm.all('AgentCode').value ='';
    fm.all('AgentCom').value ='';
    fm.all('AgentName').value = '';  
    fm.all('AgeComName').value = '';
    fm.all('CardType').value = '';
    fm.all('PayMoney').value = '';  
    
    
    divCardRisk.style.display="";

  }
  catch(ex)
  {
    alert("进行初始化时出现错误！！！！");
  }
}
function RegisterDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divDetailInfo.style.left=ex;
  	divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCardRiskGrid();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCardRiskGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="起始单号";    	  //列名
    iArray[1][1]="180";            	//列宽
    iArray[1][2]=180;            		//列最大值
    iArray[1][3]=1;              		//是否允许输入,1表示允许，0表示不允许
    iArray[1][9]="起始单号|INT";

    iArray[2]=new Array();
    iArray[2][0]="终止单号";    	  //列名
    iArray[2][1]="180";            	//列宽
    iArray[2][2]=180;            		//列最大值
    iArray[2][3]=1;              		//是否允许输入,1表示允许，0表示不允许   
    iArray[2][9]="终止单号|INT";

    iArray[3]=new Array();
    iArray[3][0]="数量";    	      //列名
    iArray[3][1]="50";            	//列宽
    iArray[3][2]=50;            		//列最大值
    iArray[3][3]=1;              		//是否允许输入,1表示允许，0表示不允许
    
    iArray[3][9]="数量|INT";

    CardRiskGrid = new MulLineEnter( "fm" , "CardRiskGrid" );
    CardRiskGrid.mulLineCount = 0;
    CardRiskGrid.displayTitle = 1;
    //CardRiskGrid.canSel=1;
    CardRiskGrid.loadMulLine(iArray);
    CardRiskGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("初始化时出错2003-05-21"+ex);
  }
}

function getComName(comC)
{
	var strSQL="select name from LDCom where comcode= '"+ comC +"' ";
	var comN = easyExecSql(strSQL);	
	
	if (comN)
	{	
		fm.all("ManageCom").value= comN[0][0];	
		
	}
}

function showCom()
{
	var typeRadio="";
	for(var i=0;fm.StateRadio.length;i++)
	{
		if(fm.StateRadio[i].checked)
		{
			typeRadio=fm.StateRadio[i].value;
			break;
		}
	}
	if (typeRadio=="0")
	{
		divComName.style.display='none';
		divComCode.style.display='none';
		fm.ManageCom.value = 'A';
	}
	else
	{
		divComName.style.display='';
		divComCode.style.display='';
		fm.ManageCom.value = <%=Comcode%>;
	}
}

</script>


