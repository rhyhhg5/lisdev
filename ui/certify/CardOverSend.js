var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
			parent.fraMain.rows = "0,0,0,0,*";
    }
    else
    {
  		parent.fraMain.rows = "0,0,0,0,*";
    }
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
    if (cShow=="true")
    {
    	cDiv.style.display="";
    }
    else
    {
    	cDiv.style.display="none";
    }
}


//根据管理机构和代理人级别进行查询的函数
function queryInfo()
{  
	var agePeriod=0;//业务员回销期
	var comPeriod=0;//代理机构回销期
	
	// 初始化表格
	initCertifyMaxGrid();
 
   var strSQL = "select b.HandleDate, case when substr(char(b.Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(b.Receivecom),2)) else substr(b.receivecom,2) end,(select name from laagent where agentcode=substr(b.receivecom,2) "  
   +" union all select name from lacom where agentcom=substr(b.receivecom,2)), b.SubCode,sum(b.MaxCount),sum(b.OverCount),b.authorizeno"
   + " from LMCertifyDes a,LZCardTrack b"	   
   + " where a.SubCode=b.SubCode and a.CertifyCode=b.CertifyCode"
   +" and a.CertifyClass='D'" 
   +" and b.state in ('10','11')"
   +" and b.MaxCount>0 and b.OverCount<>0"
   + " and b.AuthorizeNo is not null"
   + getWherePart( 'b.HandleDate','StartDate','>=' )
   + getWherePart( 'b.HandleDate', 'EndDate','<=')
   + " and  (SELECT count(*) FROM LDUSER a WHERE a.USERCODE in (select USERCODE from LDUSER d where d.USERCODE =b.operator and d.comcode like '"+managecom +"%%') and a.comcode like '"+managecom +"%%')>0"
   + " group by b.SubCode,a.CertifyName,b.CertifyCode,b.receivecom,b.HandleDate,b.authorizeno"
  ;	  

	turnPage.queryModal(strSQL, CertifyMaxGrid); 
	fm.SearchSQL.value=strSQL;
 
}



function showalert(){
	alert("没有符合条件的数据！");
}

function printList()
{
	if (fm.SearchSQL.value=="") {
		alert("请先进行查询");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "CardOverSendPrint";
  fm.action = "CardOverSendPrintSave.jsp";
	fm.submit();
  showInfo.close();
}