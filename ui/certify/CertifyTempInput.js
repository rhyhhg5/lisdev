//               该文件中包含客户端需要处理的函数和事件
var turnPage=new turnPageClass(); 

var showInfo;
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	try
	{
		if(veriryInput3()==true)
		{
			if( verifyInput() == true )//&& CertifyList.checkValue("CertifyList") ) 
			{
				  var i = 0;
				  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
					showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
					
				  fm.submit(); //提交
			}
		}
	}
	catch(ex) 
	{
		showInfo.close( );
		alert(ex);
	}
}

function auditCancel()
{
	var strSQL = "select Receptheading,Contno,PayMoney,TempFeeNo,AgentCode,Opentime from LZTempRecept where state='1'"
		+ getWherePart( 'Receptheading','ReceptUserName1')
		+ getWherePart( 'Contno','ContNo1')
		+ getWherePart( 'TempFeeNo','TempFeeNo1')
		+	getWherePart(	'AgentCode','AgentCode1')
		+ getWherePart(	'Opentime','StartDate','>=') 
		+ getWherePart(	'Opentime','EndDate','<=')
		+ getWherePart(	'ManageCom','ManageCom','like')
		;
	turnPage.queryModal(strSQL, TempFeeGrid);   
}

function veriryInput3()
{
	if (fm.ReceptHeading.value==null||fm.ReceptHeading.value=="")
	{
		alert("收据方名称不能为空！");
		return false;
	}
	
	if (fm.ContNo.value==null||fm.ContNo.value=="")
	{
		alert("投保单号不能为空！");
		return false;
	}
	if(!isInteger(fm.ContNo.value))
	{
		alert("投保单号录入有误！");
		return false;
	}
	if (fm.PayMoney.value==null||fm.PayMoney.value=="")
	{
		alert("金额不能为空！");
		return false;
	}
	if (fm.AgentCode.value==null||fm.AgentCode.value=="")
	{
		alert("领用人不能为空！");
		return false;
	}
	if(!isNumeric(fm.PayMoney.value))
	{
		alert("金额录入有误！");
		return false;
	}
	
	if(fm.TempFeeNo.value==null||fm.TempFeeNo.value=="")
	{
		alert("暂收据号不能为空！");
		return false;
	}
	if(!isInteger(fm.TempFeeNo.value))
	{
		alert("暂收据号录入有误！");
		return false;
	}
	
	return true;
} 
  
  
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
	  content="操作成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  initInpBox();
  }
} 
  
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{ 
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifyCancelInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
  
function resetSearch()
{ 
	fm.ReceptUserName1.value="";
	fm.ContNo1.value="";
	fm.TempFeeNo1.value="";
	fm.AgentCode1.value="";
	fm.StartDate.value="";
	fm.EndDate.value="";
	
	TempFeeGrid.clearData();
	TempFeeGrid.addOne();
} 
  
//提交前的校验、计算  
function beforeSubmit()
{ 
  //添加操作	
}           
  
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{ 
  if(cDebug=="1") {
		parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
} 
  
/******************************
*	领用人查询 
* 
******************************/
function afterQuery2(arrQueryResult)
{ 
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
  {
  	arrResult = arrQueryResult;
  	fm.AgentCode.value=arrResult[0][0];
	}
} 
  
function auditSubmit()
{ 
	
} 
  
function queryAgent() //查询业务员
{ 
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('AgentCode').value == "")	
  {  
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=2","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('AgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select AgentCode,Name,ManageCom from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) 
    {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
    }
    else
    {
    	alert("查询结果:  无此业务员！");
    } 
	}
}

function queryAgent1() //团险暂收收据核销查询
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('AgentCode1').value == "")	
  {  
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('AgentCode1').value != "") 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select AgentCode,Name,ManageCom from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) 
    {
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
    }
    else
    {
    	alert("查询结果:  无此业务员！");
    } 
	}
}	
