<html>


<%
//name :CardPayInput.jsp
//function :Manage LMCertifyDes
//Creator :刘岩松
//date :2003-05-16
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<SCRIPT src = "CardPayInput.js"></SCRIPT>      
<%@include file="CardPayInputInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./CardPaySave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  <Div id= "divLLReport1" style= "display: ''">

   	<Table class= common>
   		<TR class= common>
   			<td class="title">单证编码</td>
        <td class="input">
          <input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);" onkeyup="return showCodeListKey('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);"><Input class= codename name="CertifyCodeName" readonly></td>
        <td class="title">单证类型码</td>
        <td class="input"><input class="common" name="CardType"></td>
      </TR>     

      <TR class= common>
         <TD class= title>
           回销人代码：
         </TD>
          <TD class= input>
           <Input NAME=AgentCode VALUE=""  CLASS=code8  type=hidden>
           <Input NAME=GroupAgentCode VALUE=""  CLASS=code8  ondblclick="return queryAgent();" elementtype=nacessary verify="回销人代码|NOTNULL" >
            </TD>         
         <TD class= title>
           回销人名称：
         </TD>
         <TD class= input>
           <Input class= common name= 'AgentName' ReadOnly >
         </TD>
      </TR>
       <TR class= common>
         <TD class= title>
           代理机构编码：
         </TD>
         <TD class= input>
           <input name="AgentCom" CLASS=code8  value="" ondblclick="return queryCom();" onblur="return queryComBlur();" >
         <TD class= title>
           代理机构名称：
         </TD>
         <TD class= input>
           <Input class= common name= 'AgeComName' ReadOnly >
         </TD>
      </TR>
      <TR class= common>
   			<TD class= title>
   		 		结算单号
   			</TD>
   			<TD class= input>
   				<Input class= common name= PayNo id="CertifyCodeId" elementtype=nacessary verify="结算单号|NOTNULL">
   			</TD>   			
        <TD class= title>
   		 		结算总金额：
   			</TD>
   			<TD class= input>
   				<Input class= common name= PayMoney  elementtype=nacessary verify="结算金额|NUM&NOTNULL">
   			</TD>   			
        <TD class= title></TD>        
          <td class= title></td>
   		</TR>
      <Input type=hidden name= ManageCom  >
      <Input type=hidden name= OperateType  >
      <Input type=hidden name= OperateObj  >
    </Table>

   <!-- <Table class= common>
      <TR>   
      	<TD class= title>
      		<div id="divComName" style="display:''">
          	管理机构
          </div>
        </TD>
        <TD class= input>
        	<div id="divComCode" style="display:''">
	          <Input class="codeno" name= ManageCom  ReadOnly style="display:''"
	            ondblclick="return showCodeList('Station', [this,ManageComName],[0,1],null,null,null,1);"
	            onkeyup="return showCodeListKey('Station', [this,ManageComName],[0,1],null,null,null,1);"
	            ><Input name=ManageComName class= codename verify="管理机构|NOTNULL" elementtype=nacessary>
	        </div>
        </TD>
    </TR>
 		</table>
 		-->
 		
    <Div  id= "divShow" style= "display: 'none'">
    <table>
      <tr>
      <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCardRisk);">
    		</td>
        <td class= titleImg>
           定额单险种信息
        </td>
      </tr>
    </table>
    </Div>

<Input type='hidden' name= Prem >
<Input type='hidden' name= Amnt >

     <Div  id= "divCardRisk" style= "display: 'none'">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          <span id="spanCardRiskGrid" >
          </span>
        </td>
      </tr>
    </table>
	</div>
<input class="cssButton" type=button value="计算数量" onclick="getCount();">
</form>
 <form action="./CardPayDiskSave.jsp" method=post name=fmImport target="fraSubmit" ENCTYPE="multipart/form-data">
<div id="divDiskInput" style="display:''">
    <table class = common >    	
    <TR  >
      <TD  width='8%' style="font:9pt">
        文件名：
      </TD>
      
      <TD  width='80%'>
        <Input  type="file"　width="100%" name=FileName class= common>
        <INPUT  VALUE="上载结算单" class="cssbutton" TYPE=button onclick = "InsuredListUpload();" >
        
        <Input  type="hidden"　width="100%" name="insuredimport" value ="1">
      </TD>
    </TR>
    
	    <input type=hidden name=ImportFile>
	</table>
	<div id="divInsuredInfo" style="display: ''">
</form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>