<html>
	<%
	//Name:RegisterInput.jsp
	//Function：单证管理员初始化程序
	//Date：2006-04-17 
	//Author ：zhangbin
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="LZCertifyUserInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LZCertifyUserInit.jsp"%>
	</head>
	<body  onload="initForm();initElementtype();">
		<form action="LZCertifyUserSave.jsp" method=post name=fm target="fraSubmit">
			<%@include file="../common/jsp/OperateButton.jsp"%>
			<%@include file="../common/jsp/InputButton.jsp"%>

			<table class=common>
				<tr class= titleImg>
					<TD class= titleImg > 单证管理用户维护信息 </TD>
				</tr>
			</table>

			<Div id= "divCertifyUserInfo" style= "display: ''">
				<table class=common>
					<TR>
						<TD  class= title>
							用户机构代码
						</TD>
						<TD  class= input>
							<Input class="code" name=ComCode verify="机构代码|notnull&code:ComCode" 
							ondblclick="return showCodeList('ComCode',[this, ComName], [0, 1],null,null,null,1);" 
							onkeyup="return showCodeListKey('ComCode', [this, ComName], [0, 1],null,null,null,1);" elementtype=nacessary>
						</TD>
						<TD  class= title>
							用户机构名称
						</TD>
						<TD  class= input>
							<Input class= "readonly" readonly name= ComName  >
						</TD>
					</TR>

					<TR class=common >
						<TD  class= title >
							用户编码
						</TD>
						<TD  class= input>
							<Input class= "code"  name=UserCode verify="用户编码|notnull&code:UserCode" 
								ondblclick="return showCodeList('UserCode',[this, UserName], [0, 1],null,fm.ComCode.value,'ComCode',1);" 
								onkeyup="return showCodeListKey('CertifyUserCode', [this, UserName], [0, 1],null,fm.ComCode.value,'ComCode',1);" elementtype=nacessary>
						</TD>
						<TD  class= title >
							用户名称
						</TD>
						<TD  class= input>
							<Input class=codename name=UserName >
						</TD>
					</TR>

					<TR  class= common>
						
						<TD  class= title>
							用户有效状态
						</TD>
						<TD  class= input>
							<Input class="codeno" name=StateFlag verify="用户有效状态|NOTNULL"
								CodeData="0|^0|有效^1|无效" 
								ondblClick="showCodeListEx('StateFlag_1',[this,StateFlagName],[0,1],null,null,null,1);"
								onkeyup="showCodeListKeyEx('StateFlag_1',[this,StateFlagName],[0,1],null,null,null,1);"><Input class=codename name=StateFlagName elementtype=nacessary>
						</TD>
						<TD  class= title >
							用户代码操作人姓名
						</TD>
						<TD  class= input>
							<input class="common" name="UserDescription" title="">
						</TD>
					</TR>
				</table>


		</Div>
		<input type=hidden id="fmtransact" name="fmtransact">
	</form>

	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>