//               该文件中包含客户端需要处理的函数和事件
var turnPage=new turnPageClass(); 

var showInfo;
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{	
	var handleCount=CertifyList.mulLineCount; //得到MulLine的行数	
	if (handleCount==0)
	{
		alert("请先输入单证信息!");
		return false;
	}
	for(var i=0; i<handleCount; i++)
	{
		//var strSQL = "select HaveNumber from LMCertifyDes where CertifyCode='"+CertifyList.getRowColData(i,1)+"'";
		//var arrResult = easyExecSql(strSQL);
		//if (arrResult!=null)
		//{
		//	if(arrResult[0][0]=="N")
		//	{
		//		alert("对不起，暂时不能对无号单证进行人工处理！");
		//		return false;
		//	}
		//}
		
		if(CertifyList.getRowColData(i,6)==null||CertifyList.getRowColData(i,6)=="")
		{
			alert("特殊单证处理数量不能为空!");
			return false;
		}
		else if(!isNumeric(CertifyList.getRowColData(i,6)))
		{
			alert("特殊单证处理数量有非数字字符!");
			return false;
		}		
		
		if(CertifyList.getRowColData(i,1)==""||CertifyList.getRowColData(i,1)==null||CertifyList.getRowColData(i,7)==""||CertifyList.getRowColData(i,7)==null)
		{
			alert("处理状态不能为空!");
			return false;
		}
		
		if((CertifyList.getRowColData(i,7)=="3"&&trim(CertifyList.getRowColData(i,1))!="空白回销")
			||(CertifyList.getRowColData(i,7)=="4"&&trim(CertifyList.getRowColData(i,1))!="遗失")      
			||(CertifyList.getRowColData(i,7)=="5"&&trim(CertifyList.getRowColData(i,1))!="销毁")
			||(CertifyList.getRowColData(i,7)=="6"&&trim(CertifyList.getRowColData(i,1))!="作废"))
		{
			alert("请通过鼠标双击后选择处理状态!");
			return false;
		}
	}

	fm.all("AuditKind").value="SINGLE";
	try
	{
		if(veriryInput3()==true)
		{
			if( verifyInput() == true )//&& CertifyList.checkValue("CertifyList") ) 
			{
				  var i = 0;
				  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
				  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
				  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
					
				  fm.submit(); //提交
			}
		}
	}
	catch(ex) 
	{
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	var rowNum = CertifyList.mulLineCount ;
	if (rowNum==0)
	{
		alert("请在单证列表中录入发放信息！");
		return false;
	} 
	for(var i=1;i<=rowNum;i++)
	{
		var strSQL = "select havenumber from LMCertifyDes where certifycode='"+CertifyList.getRowColData(i-1,2)+"'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
		  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,2)+"的单证");
		  return false;
		}
		else
		{
			if (arrResult[0][0]=="Y")
			{
				if (CertifyList.getRowColData(i-1,6)!=""&&CertifyList.getRowColData(i-1,6)!=null)
				{
					var lineNum=CertifyList.getRowColData(i-1,5)-CertifyList.getRowColData(i-1,4)+1; 
					if (lineNum+""!=CertifyList.getRowColData(i-1,6))
					{
						alert("第"+i+"行数量与起始单号终止单号不符！");
						return false;
					}
					if(lineNum > 3000){
						alert("单次处理最大数量3000个，数量超过3000请分批处理！");
						return false;
					}
				}
			}	
		}
	}  
	return true;
}



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  
	  CertifyList.clearData("CertifyList");
	  CertifyStoreList.clearData("CertifyStoreList");
	  CertifyList.addOne("CertifyList");	  
	
	  
	  content="操作成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifyCancelInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
		parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

function afterCodeSelect(codeName, Field){
    if(codeName == "TakeBackFlag")
    {
        CertifyList.setRowColData(0, 2, "");
        CertifyList.setRowColData(0, 3, "");
        CertifyList.setRowColData(0, 4, "");
        CertifyList.setRowColData(0, 5, "");
        CertifyList.setRowColData(0, 6, "");
        initCertifyStoreList();
        fm.CertifyCount.value = "";
    }

    if(codeName == "certifycoded")
    {
        var rowNum = CertifyList.mulLineCount ; //行数
        for(var i=0;i<rowNum;i++) //清空mulLine数据
        {
            CertifyList.setRowColData(i,4,"");
            CertifyList.setRowColData(i,5,"");
            CertifyList.setRowColData(i,6,"");
        }
        var dealState = CertifyList.getRowColDataByName(0, "DealState");
        if(dealState == null || dealState == "")
        {
            CertifyList.setRowColData(0, 2, "");
            CertifyList.setRowColData(0, 3, "");
            alert("请先选择处理状态！");
            return false;
        }
        
        var manageCom = fm.ManageCom.value;
        var operator = fm.Operator.value;
        var certifyCode = Field.value;//单证类型
        
        //查询单证号码长度
        var certifyLengthSQL = "select CertifyLength from LMCertifyDes where CertifyCode = '" + certifyCode + "'";
        var arrResult = easyExecSql(certifyLengthSQL);
        if(arrResult)
            fm.certifyLength.value = arrResult[0][0];
        
        var sumSQL;//查询单证数量
        var strSQL;//查询可操作单证
        if (manageCom.length <= 4)
        {
            if(dealState == "3")
            {
                sumSQL = "select sum(SumCount) from LZCard "
                    + "where ReceiveCom in (select ReceiveCom from LZAccess where SendOutCom = 'A" + manageCom + "') "
                    + "and State in ('0', '3', '8', '9') and CertifyCode='" + certifyCode + "'";
                
                strSQL = "select CertifyCode, StartNo, EndNo, SumCount, "
                    + "(select case when b.HaveNumber='Y' then '有号单证' else '无号单证' end from LMCertifyDes b where a.CertifyCode = b.CertifyCode), "
                    + " case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2,length(char(Receivecom))-1)) else Receivecom end "
                    + "from LZCard a "
                    + "where ReceiveCom in (select ReceiveCom from LZAccess where SendOutCom = 'A" + manageCom + "') "
                    + "and State in ('0', '3', '8', '9') and CertifyCode='" + certifyCode + "' "
                    + "order by ModifyDate";
            }
            else if (dealState == "5") {
            	sumSQL = "select sum(SumCount) from LZCard "
                    + "where (ReceiveCom in (select ReceiveCom from LZAccess where SendOutCom = 'A" + manageCom + "') "
                    + " or ReceiveCom = 'A" + manageCom + "') "
                    + "and State in ('0', '3', '8', '9') and CertifyCode='" + certifyCode + "'";
                
              strSQL = "select CertifyCode, StartNo, EndNo, SumCount, "
                    + "(select case when b.HaveNumber='Y' then '有号单证' else '无号单证' end from LMCertifyDes b where a.CertifyCode = b.CertifyCode), "
                    + " case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2,length(char(Receivecom))-1)) else Receivecom end "
                    + "from LZCard a "
                    + "where (ReceiveCom in (select ReceiveCom from LZAccess where SendOutCom = 'A" + manageCom + "') "
                    + " or ReceiveCom = 'A" + manageCom + "') "
                    + "and State in ('0', '3', '8', '9') and CertifyCode='" + certifyCode + "' "
                    + "order by ModifyDate";
            } else {
                sumSQL = "select sum(SumCount) from LZCard "
                    + "where ReceiveCom = 'A" + manageCom + "' "
                    + "and State in ('0', '3', '8', '9') and CertifyCode='" + certifyCode + "'";
                
                strSQL = "select CertifyCode, StartNo, EndNo, SumCount, "
                    + "(select case when b.HaveNumber='Y' then '有号单证' else '无号单证' end from LMCertifyDes b where a.CertifyCode = b.CertifyCode), "
                    + " case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2,length(char(Receivecom))-1)) else Receivecom end "
                    + "from LZCard a "
                    + "where ReceiveCom = 'A" + manageCom + "' "
                    + "and State in ('0', '3', '8', '9') and CertifyCode='" + certifyCode + "' "
                    + "order by ModifyDate";
            }
        }
        else
        {
            sumSQL = "select sum(SumCount) from LZCard "
                + "where (ReceiveCom = 'B" + operator + "' or SendOutcom=  'B" + operator + "') "
                + "and State in ('10', '11') and CertifyCode='" + certifyCode + "'";
            
            strSQL = "select CertifyCode, StartNo, EndNo, SumCount, "
                + "(select case when b.HaveNumber='Y' then '有号单证' else '无号单证' end from LMCertifyDes b where a.CertifyCode = b.CertifyCode), "
                + " case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2,length(char(Receivecom))-1)) else Receivecom end "
                + "from LZCard a "
                + "where (ReceiveCom = 'B" + operator + "' or SendOutcom=  'B" + operator + "') "
                + "and State in ('10', '11') and CertifyCode='" + certifyCode + "' "
                + "order by ModifyDate";
        }
        arrResult = easyExecSql(sumSQL);
        if(arrResult)
            fm.CertifyCount.value = arrResult[0][0];
        turnPage.queryModal(strSQL, CertifyStoreList);
    }
}

function resetSearch()
{      
	//fm.all("ManageName").value="";
	//fm.all("ManageCom").value="";
	fm.all("CertifyName").value="";
	fm.all("CertifyCode").value="";
	fm.all("StartDate").value="";
	fm.all("EndDate").value="";
	fm.all("ErrorInfoName").value="";
	fm.all("ErrorInfoCode").value="";
	ErrorLogGrid.clearData();
}

function veriryInput3()
{
	var rowNum = CertifyList.mulLineCount ;
	if (rowNum==0)
	{
		alert("请在单证列表中录入发放信息！");
		return false;
	} 
		for(var i=1;i<=rowNum;i++)
		{
			var strSQL = "select havenumber from LMCertifyDes where certifycode='"+CertifyList.getRowColData(i-1,2)+"'";
			var arrResult = easyExecSql(strSQL);
			if(arrResult == null)
			{
			  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,2)+"的单证");
			  return false;
			}
			else
			{
				if (arrResult[0][0]=="Y")
				{
					if (CertifyList.getRowColData(i-1,6)!=""&&CertifyList.getRowColData(i-1,6)!=null)
					{
						var lineNum=CertifyList.getRowColData(i-1,5)-CertifyList.getRowColData(i-1,4)+1; 
						if (lineNum+""!=CertifyList.getRowColData(i-1,6))
						{
							alert("第"+i+"行数量与起始单号终止单号不符！");
							return false;
						}
						if(lineNum > 3000){
							alert("单次处理最大数量3000个，数量超过3000请分批处理！");
							return false;
						}
					}
				}	
			}
			if (CertifyList.getRowColData(i-1,6)==null||CertifyList.getRowColData(i-1,6)=="")
			{
				alert("发放数量不能为空！");
				return false;
			}
		}  
	return true;
}

/*function countNum(parm1,parm2)
{
	
	var startN=CertifyList.getRowColData(parm1,3);
	var endN=CertifyList.getRowColData(parm1,4);
	var numN=CertifyList.getRowColData(parm1,5);
	
	if (CertifyList.getRowColData(parm1,1)!=""&&CertifyList.getRowColData(parm1,3)!=null)
	{
		var strSQL = "select havenumber from LMCertifyDes where certifycode='"+CertifyList.getRowColData(parm1,1)+"'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
		  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,1)+"的单证");
		  return false;
		}
		else
		{
			if(startN==""||startN==null)
			{
				CertifyList.setRowColData(parm1,5,"");
				return;
			}
			if (endN==""||endN==null)
			{
				CertifyList.setRowColData(parm1,5,"");
				return;
			}
			
			if (!isNumeric(startN))
			{
				alert("起始单号有非数字字符！");
				return false;
			}
			if (!isNumeric(endN))
			{
				alert("终止单号有非数字字符！");
				return false;
			}
			
			var num=parseFloat(endN)-parseFloat(startN)+1;
			if (num<0)
			{
				alert("起始单号大于终止单号！");
				return false;
			}
			CertifyList.setRowColData(parm1,5,num+"");
		}
	}
}*/
function countNum(parm1,parm2)
{
	var startN=CertifyList.getRowColData(parm1,4);
	var endN=CertifyList.getRowColData(parm1,5);
	var numN=CertifyList.getRowColData(parm1,6);
	
	if (CertifyList.getRowColData(parm1,2)!=""&&CertifyList.getRowColData(parm1,4)!=null)
	{
		var strSQL = "select havenumber from LMCertifyDes where certifycode='"+CertifyList.getRowColData(parm1,2)+"'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
		  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,2)+"的单证");
		  return false;
		}
		else
		{
			var numType = arrResult[0][0];
			
			if (numN==""||numN==null)
			{
				if(startN==""||startN==null)
				{
					CertifyList.setRowColData(parm1,6,"");
					return;
				}
				if (endN==""||endN==null)
				{
					CertifyList.setRowColData(parm1,6,"");
					return;
				}
				
				if (!isNumeric(startN))
				{
					alert("起始单号有非数字字符！");
					return false;
				}
				if (!isNumeric(endN))
				{
					alert("终止单号有非数字字符！");
					return false;
				}
				
				var num=parseFloat(endN)-parseFloat(startN)+1;
				if (num<0)
				{
					alert("起始单号大于终止单号！");
					return false;
				}
				CertifyList.setRowColData(parm1,6,num+"");
			}
			else
			{
					if (numType=="Y")
					{
						if(startN==""||startN==null)
						{
							CertifyList.setRowColData(parm1,6,"");
							return;
						}
						if (numN==""||numN==null)
						{
							CertifyList.setRowColData(parm1,6,"");
							return;
						}
						
						if (!isNumeric(startN))
						{
							alert("起始单号有非数字字符！");
							return false;
						}
						if (!isNumeric(numN))
						{
							alert("数量栏有非数字字符！");
							return false;
						}
						
						var num=parseFloat(numN)+parseFloat(startN)-1;
						num = LCh(num, "0", startN.length);
						CertifyList.setRowColData(parm1,5,num);
					}
			}
		}
	}
}
