var turnPage=new turnPageClass(); 
var turnPage1=new turnPageClass(); 


// 该文件中包含客户端需要处理的函数和事件
// 发放管理


var showInfo;
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	try {
		if( verifyInput() == true && CertifyList.checkValue("CertifyList")) {
			receiveHandle();
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.SendOutCom.value = fm.SendOutComEx.value;
				fm.PrtNo.value = fm.PrtNoEx.value;
				fm.action="./CertifySendOutSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) {
  		showInfo.close( );
  	alert(ex);
  }
}

function receiveHandle() //置接收者状态
{
	var receiveC = fm.ReceiveCom.value;
	if (receiveC!=""&&receiveC!=null)
	{
		if (receiveC.substring(0,2)=="86")
		{
			fm.ReceiveType.value="COM";
		}
		else if (fm.ReceiveType.value!="AGE")
		{
			fm.ReceiveType.value="USR";
		}
	}
}

function veriryInput3()
{
	/*
         * 优化 #281: 单证下发至中介机构时对于（共保机构 05）应排除在外 定额单证部分
         * 【OoO？】杨天政 2011-12-12
         */
	 var receiveSuper = fm.AgentCode.value;
	 if (receiveSuper!=""&&receiveSuper!=null)
	 {
       var check_sql="select name from lacom where actype='05' " +
					"and agentcom ='"+receiveSuper+"' with ur";
	   var arrResult = easyExecSql(check_sql);
		if(receiveSuper!=""&&arrResult!=null)
		  {
		   alert("该机构为共保机构，不可以下发单证");
		   return false;
		  }
	 }

	/*
	 * ---------------THE END---------------------
	 */
	
	if (fm.ReceiveType.value=="COM"||fm.ReceiveType.value=="USR")
	{
		if(fm.ReceiveCom.value==''||fm.ReceiveCom.value==null)
		{
			alert("接收者不能为空！");
			return false;
		}
	}
	else
	{
		if(fm.AgentCode.value==''||fm.AgentCode.value==null)
		{
			alert("接收者不能为空！");
			return false;
		}
	}
	var rowNum = CertifyList.mulLineCount ;
	if (rowNum==0)
	{
		alert("请在单证列表中录入发放信息！");
		return false;
	} 
	
	
		for(var i=1;i<=rowNum;i++)
		{
			var strSQL = "select havenumber ,certifylength from LMCertifyDes where certifycode='"+CertifyList.getRowColData(i-1,1)+"'";
			var arrResult = easyExecSql(strSQL);
			if(arrResult == null)
			{
			  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,1)+"的单证");
			  return false;
			}
			else
			{
			
			    var length=parseInt(arrResult[0][1]);  
			    if(CertifyList.getRowColData(i-1,3).length!=length)
			    {
			       alert("起始保单号的位数不符合规定！");
			       return false;
			    }
			    if(CertifyList.getRowColData(i-1,4).length!=length)
			    {
			       alert("终止保单号的位数不符合规定！");
			       return false;
			    }
				if (arrResult[0][0]=="Y")
				{
					if (CertifyList.getRowColData(i-1,5)!=""&&CertifyList.getRowColData(i-1,5)!=null)
					{
						var lineNum=CertifyList.getRowColData(i-1,4)-CertifyList.getRowColData(i-1,3)+1; 
						if (lineNum+""!=CertifyList.getRowColData(i-1,5))
						{
							alert("第"+i+"行数量与起始单号终止单号不符！");
							return false;
						}
					}
				}	
			}
			if (CertifyList.getRowColData(i-1,5)==null||CertifyList.getRowColData(i-1,5)=="")
			{
				alert("发放数量不能为空！");
				return false;
			}
				
		}  
	
	//alert(rowNum);
	
	return true;
}



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, TakeBackNo, CertifyCode )
{
  showInfo.close();

	if( fm.chkModeBatch.checked == true ) {
		var urlStr = "CertBatchLogQuery.html";
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:" + 
      oClientCaps.availWidth + "px;dialogHeight:" + oClientCaps.availHeight + "px");
	}
	
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
  	if( fm.chkPrt.checked == true ) {
	    var urlStr = "CertifyListPrint.jsp";
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:" + 
	      oClientCaps.availWidth + "px;dialogHeight:" + oClientCaps.availHeight + "px");
	  } else {
	    content="保存成功！";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	    
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	     
	    
	    if (CertifyCode == null || CertifyCode == '')
	    {
		    CertifyList.clearData("CertifyList");
	    	CertifyList.addOne("CertifyList");
	    	initForm();
	  	}
	  	else
	  	{
	  		CertifyList.setRowColData(0,3,"");
		    CertifyList.setRowColData(0,4,"");
		    CertifyList.setRowColData(0,5,"");
		    afterStore("CertifyCode",CertifyCode);
	  	}
	  }
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
	parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

// 单证入库操作的切换函数
function inputCertify(objCheck)
{
	CertifyList.clearData();
	
	CertifyStoreList.clearData(); //清除现有单证列表数据
	fm.certifysumcount.value="";
	    
	if(objCheck.checked == true) {
		fm.SendOutComEx.value = "00";
		fm.SendOutComEx.disabled = true;
		fm.SendOutName.disabled = true;
		
		fm.PrtNoEx.value = "";
		fm.PrtNoEx.disabled = false;
		fm.btnPrtNo.style.display = "";
		fm.btnOp.value = "单证入库";
		
		fm.chkPrt.disabled = true;
		fm.chkPrt.checked = false;
		
		fm.chkModeBatch.disabled = true;
		fm.chkModeBatch.checked = false;
		
		fm.SendOutCom.value = "";
		fm.SendOutName.value = "";
		fm.ReceiveCom.value = "86";
		fm.ReceiveName.value = "总公司";
		fm.HaveNum.value = "";
		
		//divReceiveType.style.display = "none";
		divReceiveTitle.style.display = "none";
		fm.HaveNum.style.display="none";
		
	} else {
		fm.SendOutComEx.value = "";
		fm.SendOutComEx.disabled = false;

		fm.PrtNoEx.value = "";
		fm.PrtNoEx.disabled = true;
		fm.btnPrtNo.style.display = "none";
		fm.btnOp.value = "发放单证";
		fm.chkPrt.disabled = false;
		fm.chkModeBatch.disabled = false;
		
		fm.SendOutCom.value = "";
		fm.ReceiveCom.value = "";
		fm.ReceiveName.value = "";
		fm.SendOutName.value = "";
		fm.HaveNum.value = "";
		
		CertifyList.addOne("CertifyList");
		
		//divReceiveType.style.display = "";
		divReceiveTitle.style.display = "";
		fm.HaveNum.style.display="";
	}
}

function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

// 查询印刷号的功能
function queryPrtNo()
{
	fm.sql_where.value = " State = '1' ";
  showInfo = window.open("./CertifyPrintQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
  if(arrResult!=null)
  {
  	if( fm.chkModeBatch.checked == false ) {
	    fm.PrtNoEx.value = arrResult[0][0];
	    fm.ReceiveCom.value = 'A' + arrResult[0][11];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
	    
			var rowCount = 0;

	    CertifyList.setRowColData(rowCount, 1, arrResult[0][1]);
	    CertifyList.setRowColData(rowCount, 2, arrResult[0][25]);
	    CertifyList.setRowColData(rowCount, 3, arrResult[0][18]);
	    CertifyList.setRowColData(rowCount, 4, arrResult[0][19]);
	    
	    var cNum = parseFloat(arrResult[0][19])-parseFloat(arrResult[0][18])+1;
	    CertifyList.setRowColData(rowCount, 5, cNum+"");
	    
	  } else {
	    fm.ReceiveCom.value = arrResult[0][3];
	    fm.ReceiveCom.title = arrResult[0][1];
	    
	    CertifyList.clearData();
	    CertifyList.addOne();
		}
  }
}

function afterQuery2(arrQueryResult)
{
	var arrResult = new Array();
	
	if(arrQueryResult!=null)
    {
	  	arrResult = arrQueryResult;
	  	fm.AgentCode.value=arrResult[0][0];
	  	if(arrResult[0].length <95){
			fm.GroupAgentCode.value=arrResult[0][0];
		}else{
			fm.GroupAgentCode.value=arrResult[0][95];
		}
	}
}

//批量发放模式的切换函数
function changeMode(objCheck)
{
	CertifyList.clearData();
	
	if(objCheck.checked == true) {
		fm.btnOp.value = "批量发放";
		
		fm.btnQueryCom.style.display = "";
		
		fm.chkPrtNo.disabled = true;
		
	} else {
		fm.btnOp.value = "发放单证";
		
		fm.btnQueryCom.style.display = "none";
		
		fm.chkPrtNo.disabled = false;
	}
}

//批量发放。查询代理人区部组的函数。
function queryCom()
{
    fm.ReceiveType.value="AGECOM";
    fm.sql_where.value = "";
    if(fm.all("GroupAgentCode").value==""){
    	 showInfo = window.open("./LAComMain.jsp?ManageCom=" + fm.all('ManageCom').value.substr(0, 4) + "");
    }
    if(fm.all('GroupAgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select a.AgentCom,a.Name,a.ManageCom"	       
	         +" from LACom a where a.AgentCom='" + cAgentCode +"'";
  var arrResult = easyExecSql(strSql);
     //alert(arrResult);
  if (arrResult != null) 
  {
    alert("查询结果:  代理机构编码:["+arrResult[0][0]+"] 代理机构名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
  }
  else
  {
  	alert("查询结果:  无此代理机构！");
  } 
	}
}


function queryCertify(){
  showInfo = window.open("./CertifyInfoQuery.jsp?certifyCode=certifycode1");
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('GroupAgentCode').value == "")	
  {  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&SaleChnl=all","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}
	if(fm.all('GroupAgentCode').value != "")	 
	{
		var cAgentCode = fm.AgentCode.value;  //保单号码	
		var strSql = "select AgentCode,Name,ManageCom,groupagentcode from LAAgent where AgentCode='" + cAgentCode +"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) 
    {
      alert("查询结果:  业务员编码:["+arrResult[0][3]+"] 业务员名称为:["+arrResult[0][1]+"] 管理机构为:["+arrResult[0][2]+"]");
    }
    else
    {
    	alert("查询结果:  无此业务员！");
    } 
	}
}

function searchSendOut()
{
		var strSQL = "";
		if (fm.ManageCom.value.length<=4)
		{
			strSQL = "select distinct a.CertifyCode,a.StartNo,a.EndNo,a.SumCount,a.Receivecom,a.MakeDate,a.MakeTime "
			+	" from LZCARD a, LZCARDTRACK b where a.certifycode=b.certifycode "
			+ " and a.startno=b.startno and a.endno=b.endno and a.stateflag in ('0','7','8') and a.operateflag='0' "
			+	" and a.sendoutcom='A" + fm.ManageCom.value + "' "
			+ getWherePart( 'a.CertifyCode','CertifyType')
			+ getWherePart( 'a.StartNo','StartNo')
			+ getWherePart( 'a.EndNo','EndNo')
			+ getWherePart(	'a.ReceiveCom','CertifyOwner')
			+ " order by a.MakeDate Desc, a.MakeTime Desc"
			;
		}
		else
		{
			strSQL = "select distinct a.CertifyCode,a.StartNo,a.EndNo,a.SumCount,a.Receivecom,a.MakeDate,a.MakeTime "
			+	" from LZCARD a, LZCARDTRACK b where a.certifycode=b.certifycode "
			+ " and a.startno=b.startno and a.endno=b.endno and a.stateflag in ('0','7','8') and a.operateflag='0' "
			+	" and a.sendoutcom='B"+fm.Operator.value+"' "
			+ getWherePart( 'a.CertifyCode','CertifyType')
			+ getWherePart( 'a.StartNo','StartNo')
			+ getWherePart( 'a.EndNo','EndNo')
			+ getWherePart(	'a.ReceiveCom','CertifyOwner')
			+ " order by a.MakeDate Desc,a.MakeTime Desc"
			;
		}
		
	turnPage1.queryModal(strSQL, CertifyBackList);   
}

function resetSendOut()
{
	fm.CertifyType.value="";
	fm.CertifyName.value="";
	fm.StartNo.value="";
	fm.EndNo.value="";
	CertifyBackList.clearData("CertifyBackList");
}

function deleteSendOut()
{
	if(confirm("确定要删除发放吗?"))
	{
		if( verifyInput() == true && CertifyBackList.checkValue("CertifyBackList") ) 
		{
			var tSel = CertifyBackList.getSelNo();
			if (tSel==0||tSel == null)
			{
				alert( "请先选择一条记录，再删除发放。" );
				return false;
			}
			
			fm.action = "./CertReveSendOutSave.jsp";
			
			var i = 0;
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			
			fm.submit(); //提交
		}
	}
}

function countNum(parm1,parm2)
{
	var startN=CertifyList.getRowColData(parm1,3);
	var endN=CertifyList.getRowColData(parm1,4);
	var numN=CertifyList.getRowColData(parm1,5);
	
	if (CertifyList.getRowColData(parm1,1)!=""&&CertifyList.getRowColData(parm1,3)!=null)
	{
		var strSQL = "select havenumber from LMCertifyDes where certifycode='"+CertifyList.getRowColData(parm1,1)+"'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
		  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,1)+"的单证");
		  return false;
		}
		else
		{
			var numType = arrResult[0][0];
			
			if (numN==""||numN==null)
			{
				if(startN==""||startN==null)
				{
					CertifyList.setRowColData(parm1,5,"");
					return;
				}
				if (endN==""||endN==null)
				{
					CertifyList.setRowColData(parm1,5,"");
					return;
				}
				
				if (!isNumeric(startN))
				{
					alert("起始单号有非数字字符！");
					return false;
				}
				if (!isNumeric(endN))
				{
					alert("终止单号有非数字字符！");
					return false;
				}
				
				var num=parseFloat(endN)-parseFloat(startN)+1;
				if (num<0)
				{
					alert("起始单号大于终止单号！");
					return false;
				}
				CertifyList.setRowColData(parm1,5,num+"");
			}
			else
			{
					if (numType=="Y")
					{
						if(startN==""||startN==null)
						{
							CertifyList.setRowColData(parm1,5,"");
							return;
						}
						if (numN==""||numN==null)
						{
							CertifyList.setRowColData(parm1,5,"");
							return;
						}
						
						if (!isNumeric(startN))
						{
							alert("起始单号有非数字字符！");
							return false;
						}
						if (!isNumeric(numN))
						{
							alert("数量栏有非数字字符！");
							return false;
						}
						
						var num=parseFloat(numN)+parseFloat(startN)-1;
						
						CertifyList.setRowColData(parm1,4,num+"");
					}
			}
		}
	}
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == "CertifyCodeSend")
	{
		CertifyList.setRowColData(0,3,"");
		CertifyList.setRowColData(0,4,"");
		CertifyList.setRowColData(0,5,"");
		
		var strSQL = "select havenumber from LMCertifyDes where CertifyCode='"+ Field.value +"'";
		
		var arrResult = easyExecSql(strSQL);
		var numType = "";
		if(arrResult != null)
		{
			numType = arrResult[0][0];
		}
		
		if (numType=="Y")
		{
			fm.HaveNum.value="有号单证";
		}
		else if(numType=="N")
		{
			fm.HaveNum.value="无号单证";
		}
		else
		{
			fm.HaveNum.value="无有号标记";
		}
		
		if (fm.ManageCom.value.length<=4)
		{
			strSQL = "select sum(SumCount) from LZCard "
				+ " where receivecom = 'A"+fm.ManageCom.value+"' and CertifyCode='"+Field.value+"' and  StateFlag in ('0','7','8','9') " ; 
		}
		else
		{
			strSQL = "select sum(SumCount) from LZCard "
				+ " where receivecom = 'B"+fm.Operator.value+"' and CertifyCode='"+Field.value+"' and  StateFlag in ('0','7','8','9') " ; 
		}
		
		var arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
			fm.certifysumcount.value = arrResult[0][0];
		}
		else
		{
		  fm.certifysumcount.value = "0";
		}
		
		var strSQL = "";
		if (fm.ManageCom.value.length<=4)
		{
			strSQL = "select CertifyCode,StartNo,EndNo,SumCount,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2,length(char(Receivecom))-1)) else Receivecom end "
			 + " from LZCard where receivecom = 'A"+fm.ManageCom.value+"' and CertifyCode='"+Field.value+"' and StateFlag in ('0','7','8','9') "
			 + " order by StartNo";      
		}
		else
		{
			strSQL = "select CertifyCode,StartNo,EndNo,SumCount,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2,length(char(Receivecom))-1)) else Receivecom end "
			 + " from LZCard where receivecom = 'B"+fm.Operator.value+"' and CertifyCode='"+Field.value+"' and StateFlag in ('0','7','8','9')"
			 + " order by StartNo";            
		}
		turnPage.queryModal(strSQL, CertifyStoreList);   
		
	}
}


	/**
   * 发放结束后重新显示现有单证信息
   * <p><b>Example: </b><p>
   * @param codeName 
   * @param Field 单证类型
   * @return 
   */
function afterStore(codeName,Field)
{
	if(codeName == "CertifyCode")
	{
		CertifyList.setRowColData(0,3,"");
		CertifyList.setRowColData(0,4,"");
		CertifyList.setRowColData(0,5,"");
		
		var strSQL = "select havenumber from LMCertifyDes where CertifyCode='"+ Field +"'";
		var arrResult = easyExecSql(strSQL);
		
		var numType = "";
		if(arrResult != null)
		{
			numType = arrResult[0][0];
		}
		
		if (numType=="Y")
		{
			fm.HaveNum.value="有号单证";
		}
		else if(numType=="N")
		{
			fm.HaveNum.value="无号单证";
		}
		else
		{
			fm.HaveNum.value="无有号标记";
		}
		
		var strSQL = "";
		if (fm.ManageCom.value.length<=4)
		{
		strSQL = "select CertifyCode,StartNo,EndNo,SumCount,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2,length(char(Receivecom))-1)) else Receivecom end "
		 + " from LZCard where receivecom = 'A"+fm.ManageCom.value+"' and CertifyCode='"+Field+"' and StateFlag in ('0','7','8','9') "
		 + " order by StartNo";      
		}
		else
		{
			strSQL = "select CertifyCode,StartNo,EndNo,SumCount,case when substr(char(Receivecom),1,1)='D'  then (select groupagentcode from laagent where agentcode=substr(char(Receivecom),2,length(char(Receivecom))-1)) else Receivecom end "
			 + " from LZCard where receivecom = 'B"+fm.Operator.value+"' and CertifyCode='"+Field+"' and StateFlag in ('0','7','8','9') "
			 + " order by StartNo";            
		}
		turnPage.queryModal(strSQL, CertifyStoreList); 

		if (fm.ManageCom.value.length<=4)
		{
			strSQL = "select sum(SumCount) from LZCard "
				+ " where receivecom = 'A"+fm.ManageCom.value+"' and CertifyCode='"+Field+"' and StateFlag in ('0','7','8','9')" ; 
		}
		else
		{
			strSQL = "select sum(SumCount) from LZCard "
				+ " where receivecom = 'B"+fm.Operator.value+"' and CertifyCode='"+Field+"' and StateFlag in ('0','7','8','9')" ; 
		}
		var arrResult = easyExecSql(strSQL);
    
		if(arrResult != null)
		{
			fm.certifysumcount.value = arrResult[0][0];
		}
		else
		{
		  fm.certifysumcount.value = "0";
		}
	}
}

function computeNum(parm1,parm2){
    var nStart=CertifyList.getRowColData(parm1,3);
	var nCount=CertifyList.getRowColData(parm1,5)

	if(nStart==""||nStart==null){
		CertifyList.setRowColData(parm1,4,"");
		return
	}
	
	if(nCount==""||nCount==null){
		CertifyList.setRowColData(parm1,4,"");
		return
	}
	
	if(!isNumeric(nStart)){
		alert("起始单号有非数字字符！");
		return false
	}
	
	if(!isNumeric(nCount)){
		alert("数量有非数字字符！");
		return false
	}
	
	var num=parseFloat(nStart)+parseFloat(nCount)-1;
	num+="";
	while(num.length<nStart.length){
		num="0"+num;
	}
	CertifyList.setRowColData(parm1,4,num+"");
	

}
//add by zjd 统一工号
function setAgentcode(){
	if(fm.GroupAgentCode.value !=""){
		var strSQL=" select agentcode from laagent where groupagentcode='"+fm.GroupAgentCode.value+"' union select Agentcom from LAcom where AgentCom='"+fm.GroupAgentCode.value+"' ";
		var arrResult=easyExecSql(strSQL);
		if(arrResult != null)
		{
			fm.AgentCode.value = arrResult[0][0];
		}else{
			alert("接收者查询失败，系统中不存在该接受者！");
			return ;
		}
	}else{
		fm.AgentCode.value="";
	}
	
}