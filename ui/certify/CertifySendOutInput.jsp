<%
	// 防止IE缓存页面
	response.setHeader("Pragma","No-cache"); 
	response.setHeader("Cache-Control","no-cache"); 
	response.setDateHeader("Expires", 0); 
%>

<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-07
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="CertifySendOutInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifySendOutInit.jsp"%>
</head>
<body onload="initForm()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./CertifySendOutSave.jsp" method="post" name=fm target="fraSubmit">
    <table class="common">
    	<tr class="common">
    		<td class="input">
    			<input name="btnOp" class="cssButton" type="button" value="发放单证" onclick="submitForm()">
    			<input type="button" name="btnCertify" class="cssButton" value="单证信息查询" onclick="queryCertify()">
    		</td>

    		<td class="input">
    			<div id="divAffix" style="display:none">
	    			<input name="chkPrt" type="checkbox" >打印清单
	    			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    			<input name="chkModeBatch" type="checkbox" onclick="changeMode(this)">批量发放
					</div>
					<div id="divSendStore" style = "display:''">
						
					</div>
    		</td>
    	</tr>
    
    </table>

    <!-- 发放的信息 -->
    <div style="width:120"><!-- this div is used to change output effect. zhouping 2002-08-07 -->
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSendOutInfo);"></td>
          <td class="titleImg">发放信息</td></tr></table></div>

    <div id="divSendOutInfo">
      <table class="common">
        <tr class="common">
          <td class="title">印刷批次号</td>
          <td class="input" nowrap=true>
          	<input class="common" name="PrtNoEx" title="入库操作时使用" disabled>
          	<input type="hidden" name="PrtNo" value="PrtNo">
          	<input type="checkbox" name="chkPrtNo" onclick="inputCertify(this)" style="display:'none'"></td>

          <td class="title">
          	<input type="button" name="btnPrtNo" class="cssButton" value="入库查询" onclick="queryPrtNo()" style="display:none">
          	<div id="divReceiveTitle" style="display:''">
          		单证有号类型
          	</div>
          </td>
          <td class="input">
          	<!--<div id="divReceiveType" style="display:''">
	          	<input type="radio" name="TypeRadio"  value="0" checked onclick="receiveType()">机构 
	          	&nbsp;&nbsp;
							<input type="radio" name="TypeRadio"  value="1" onclick="receiveType()">业务员
						</div>
						-->
						<input class="readonly" name="HaveNum" style="display:''" ReadOnly >
          </td>
        </tr>

        <tr class="common">
          <td class="title">发放者</td>
          <td class="input">
          	<input class="codeno" name="SendOutComEx" verify="发放者|NOTNULL"
          	ondblclick="return showCodeList('sendoutcode', [this,SendOutName],[0,1],null,null,null,1);"
            onkeyup="return showCodeListKey('sendoutcode', [this,SendOutName],[0,1],null,null,null,1);"
          	><input name = "SendOutName" class=codename type="text">
          	<input type="hidden" name="SendOutCom">
          </td>

          <td class="title">接收者</td>
          <td class="input" nowrap=true>
	          	<input class="codeno" name="ReceiveCom"  
	          	ondblclick="return showCodeList('receivecode', [this,ReceiveName],[0,1],null,fm.SendOutComEx.value,'ComCode',1);"
	            onkeyup="return showCodeListKey('receivecode', [this,ReceiveName],[0,1],null,fm.SendOutComEx.value,'ComCode',1);"
	          	><input name = "ReceiveName" class=codename type="text">
          		<input class="codename" name=AgentCode type="hidden">
          		<input type="text" class="codename" name=GroupAgentCode style="display:'none'" onblur="setAgentcode()">
          		<input type="button" class="button" name=AgentQuery value="业务员查询" onclick="queryAgent()" style="display:'none'">
          	    <input type="button" class="button" name="btnQueryCom" value="代理机构查询" onclick="queryCom()" style="display:none"></td></tr>

        <!--<tr class="common">
          <td class="title">失效日期</td>
          <td class="input"><input class="readonly" readonly name="InvalidDate"></td>

          <td class="title">最大金额</td>
          <td class="input"><input class="readonly" readonly name="Amnt"></td></tr>
         -->
          <!-- 用“总保额”来表示“最大金额” -->

        <tr class="common">
          <td class="title">经办人</td>
          <td class="input"><input class="common" name="Handler"></td>

          <td class="title">经办日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDate"></td></tr>

        <tr class="common">
          <td class="title">操作员</td>
          <td class="input"><input class="readonly" readonly name="Operator"></td>

          <td class="title">当前时间</td>
          <td class="input"><input class="readonly" readonly name="curTime"></td></tr>
      </table>
    </div>

    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>单证列表</td></tr>
    </table>

		<div id="divCertifyList">
      <table class="common">
        <tr class="common">
          <td text-align: left colSpan=1><span id="spanCertifyList"></span></td></tr>
	  	</table>
		</div>
		
		<br><hr><br>
		
		<table>
   	  <tr>
        <td class=common>
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divShowStore);">
        </td>
    	<td class= titleImg>现有单证信息</td></tr>
    </table>
		
		<div id="divShowStore" style="display:''">
	    <table class="common">
	      <tr class="common">
	        <td text-align: left colSpan=1><span id="spanCertifyStoreList"></span></td></tr>
			</table>
		</div>
		<Div id= "divPage" align=center style= "display:''">
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage.lastPage();">
	  </Div>
	  
			<table class="common">
		    <tr class="common">
		    	<td class="title"></td><td class="input"></td><td class="title"></td>
		      <td class="input"></td>
		      <td class="title">单证总数量</td>
		      <td class="input"><input class="common" name="certifysumcount"></td>
		      <td class="title"></td>
		      <td class="input"></td>
		    </tr>
		  </table>
	  </div>
	  <div style="display:'none'">
	  <br><hr><br>
	  
	  <table>
   	  <tr>
        <td class=common>
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divShowCertifyBack);">
        </td>
    	<td class= titleImg>发放删除</td></tr>
    </table>
    </div>
    <div id="divShowCertifyBack" style="display:'none'">
	    <table class="common">
	        <tr class="common">
	          <td class="title">单证类型</td>
	          <td class="input">
	          	<input name = "CertifyType" class="codeno" 
	          	onclick="return showCodeList('CertifyCode1', [this,CertifyName],[0,1],null,null,null,1,300);"
		          onkeyup="return showCodeListKey('CertifyCode1', [this,CertifyName],[0,1],null,null,null,1,300);"
	          	><input class="codename" name="CertifyName">         	
	          </td>	
	          <td class="title">单证接收人</td>
	          <td class="input">
							<input class="common" name="CertifyOwner">
						</td>
	        </tr>
	        <tr class="common">
	          <td class="title">起始号</td>
	          <td class="input">
	          	<input class="common" name="StartNo">
	          </td>	
	          <td class="title">终止号</td>
	          <td class="input">
							<input class="common" name="EndNo">
						</td>
	        </tr>
	        <tr class="common">
	          <td class="title">
	          	<input name="btnOp" class="cssButton" type="button" value="已发放单证查询" Onclick="searchSendOut();">
	          </td>
	          <td class="input">
	          	<input name="btnOp" class="cssButton" type="button" value="重&nbsp;&nbsp;&nbsp;&nbsp;置" Onclick="resetSendOut();">
	          </td>	
	          <td class="title"></td>
	          <td class="input"></td>
	        </tr>
	    </table>
	    
			<div id="divCertifyBack">
		    <table class="common">
		      <tr class="common">
		        <td text-align: left colSpan=1><span id="spanCertifyBackList"></span></td></tr>
				</table>
			</div>
			<Div id= "divPage1" align=center style= "display:''">
			  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage1.firstPage();"> 
			  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
			  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
			  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage1.lastPage();">
		  </Div>
		  
		  <tr class="common">
	      <td class="title">
	      	<input name="btnOp" class="cssButton" type="button" value="发放删除" Onclick="deleteSendOut();">
	      </td>
	      <td class="input"></td>	
	      <td class="title"></td>
	      <td class="input"></td>
	    </tr>
    </div>
	  
		<input type=hidden name="sql_where">
		<input type=hidden name="LimitFlag">
		<input type=hidden name="ReceiveType" value="COM">
		<input type=hidden name="ManageCom" value="<%=strCom%>">
		<input type='hidden' name='ReceivePage'  value='SENDDEL'>
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
