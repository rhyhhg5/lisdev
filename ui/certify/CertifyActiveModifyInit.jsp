
<%
	//Creator :xp
	//Date :2011-06-07
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strOperator = globalInput.Operator;
	String strCurTime = PubFun.getCurrentDate();
	String strCom = globalInput.ComCode;
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<script>
function initInpBox()
{
	var operator='<%= strOperator %>';
	var com='<%= strCom %>';
  try
  {
  	if(com.length==8){
    fm.all('SendOutCom').value = 'B'+operator;
  	}else{
  	document.all.query.disabled=true;
  	document.all.modify.disabled=true;
  	alert("请使用八位机构单证下发操作用户进行修改!");
  	}
  }
  catch(ex)
  {
    alert("进行初始化时出现错误！");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在CertifyActiveModifyInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCertifyActiveGrid();
  }
  catch(re)
  {
    alert("CertifyActiveModifyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCertifyActiveGrid()
{
	var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="单证编码";    			//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="单证名称";         			//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
 
      iArray[3]=new Array();
      iArray[3][0]="当前发放机构";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="单证起始号";         			//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="单证终止号";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      
	  iArray[6]=new Array();
      iArray[6][0]="单证类型吗";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      
    CertifyActiveGrid = new MulLineEnter( "fm" , "CertifyActiveGrid" ); 
    CertifyActiveGrid.mulLineCount = 0;       
    CertifyActiveGrid.displayTitle = 1;
    CertifyActiveGrid.canSel=1;
    CertifyActiveGrid.locked = 1;	
	CertifyActiveGrid.hiddenPlus = 1;
	CertifyActiveGrid.hiddenSubtraction = 1;
	CertifyActiveGrid.loadMulLine(iArray);  
    CertifyActiveGrid.detailInfo="单击显示详细信息";
    CertifyActiveGrid.selBoxEventFuncName = "SetModifyInfo";
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>
