<%
//程序名称：CertifyPrintInput.jsp
//程序功能：
//创建日期：2002-10-14 10:20:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.Hashtable"%>
  
<%@page contentType="text/html;charset=GBK" %>

<%!
		String handleFunction(HttpSession session, HttpServletRequest request) {
	  LZCardPrintSchema schemaLZCardPrint = new LZCardPrintSchema();
    System.out.println("================================================================================================");	
	  //输出参数
	  CErrors tError = null;
	  String strOperate = request.getParameter("hideOperate").trim();
	  
		GlobalInput globalInput = new GlobalInput();
		
		globalInput.setSchema((GlobalInput)session.getValue("GI"));
		
		String optType = request.getParameter("OptType");
		String checkRule = request.getParameter("checkRule");
	  if((optType.equals("1") && checkRule.equals("2"))||(optType.equals("3") && checkRule.equals("2")))
	  {
	  System.out.println("in 无校验四单111"+request.getParameter("prtNo"));
	  System.out.println("in 无校验四单");
//add by xp 090521 针对lzcardnumber会产生多次的情况增加对于操作员的校验------------
	  	//查询单证描述
        LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
        tLMCertifyDesDB.setSubCode(request.getParameter("CardType"));
        LMCertifyDesSet tLMCertifyDesSet = tLMCertifyDesDB.query();
        if(tLMCertifyDesSet.size() != 1)
        {
            return " 失败，原因是: 单证描述不唯一";
        }
	  	if (!tLMCertifyDesSet.get(1).getManageCom().equals("A"))
         {
                if(!tLMCertifyDesSet.get(1).getManageCom().equals(globalInput.ComCode))
                {
                    return " 失败，原因是: 当前操作员不能操作此种单证,因为管理机构不一致";
                }
          }
//校验添加完毕--------------------------------------------------------------------
	  	
	    TransferData transferData=new TransferData();	
	    transferData.setNameAndValue("CardType", request.getParameter("CardType"));
	    transferData.setNameAndValue("CardSerNo", request.getParameter("StartNo"));
	    transferData.setNameAndValue("CardNum", request.getParameter("SumCount"));
	    transferData.setNameAndValue("OperateType", optType);  
	    transferData.setNameAndValue("PrtNo", request.getParameter("prtNo"));
	    transferData.setNameAndValue("PageType", "0");	
	    transferData.setNameAndValue("Operate", "new");   
	    transferData.setNameAndValue("CheckRule", checkRule);
	    
	    String tCardType=request.getParameter("CardType");
	    String tPrtNo=request.getParameter("prtNo");
	    
	    VData vData = new VData();
	    vData.addElement(transferData);
	    vData.addElement(globalInput);
	    
	    CertifyNumberUI mCertifyNumberUI = new CertifyNumberUI();
	    try{
	    	mCertifyNumberUI.submitData(vData,"new");
	    }catch(Exception ex){
	    	ex.printStackTrace();
	    	String tSQL="delete from lzcardnumber where cardtype='"+tCardType
	    	+"' and prtno='"+tPrtNo+"' ";
	    	ExeSQL tExeSQL=new ExeSQL();
	    	tExeSQL.execUpdateSQL(tSQL);
	    	return "操作失败，原因是：生成单证编码时失败，并对已经生成的单证编码进行了回滚。请重新尝试操作。" ;
	    }
			if(mCertifyNumberUI.mErrors.needDealError())
	    {       
	    	String tSQL="delete from lzcardnumber where cardtype='"+tCardType
	    	+"' and prtno='"+tPrtNo+"' ";
	    	ExeSQL tExeSQL=new ExeSQL();
	    	tExeSQL.execUpdateSQL(tSQL);               
	       return " 失败，原因是:" + mCertifyNumberUI.mErrors.getFirstError();
	    }       
	  }
	  
	
		schemaLZCardPrint.setPrtNo(request.getParameter("PrtNo"));
		schemaLZCardPrint.setCertifyCode(request.getParameter("CertifyCode"));
		schemaLZCardPrint.setRiskCode(request.getParameter("RiskCode"));
		schemaLZCardPrint.setRiskVersion(request.getParameter("RiskVersion"));
		schemaLZCardPrint.setSubCode(request.getParameter("CardType"));
		System.out.println(schemaLZCardPrint.getSubCode());
		schemaLZCardPrint.setMaxMoney(request.getParameter("CertifyPrice"));
		schemaLZCardPrint.setMaxDate(request.getParameter("MaxDate"));
		schemaLZCardPrint.setComCode(request.getParameter("ComCode"));
		schemaLZCardPrint.setPhone(request.getParameter("Phone"));
		schemaLZCardPrint.setLinkMan(request.getParameter("LinkMan"));
		schemaLZCardPrint.setCertifyPrice(request.getParameter("CertifyPrice"));
		schemaLZCardPrint.setManageCom(request.getParameter("ManageCom"));
		schemaLZCardPrint.setOperatorInput(request.getParameter("OperatorInput"));
		schemaLZCardPrint.setInputDate(request.getParameter("InputDate"));
		schemaLZCardPrint.setInputMakeDate(request.getParameter("InputMakeDate"));
		schemaLZCardPrint.setGetMan(request.getParameter("GetMan"));
		schemaLZCardPrint.setGetDate(request.getParameter("GetDate"));
		schemaLZCardPrint.setOperatorGet(request.getParameter("OperatorGet"));
		schemaLZCardPrint.setStartNo(request.getParameter("StartNo"));
		schemaLZCardPrint.setEndNo(request.getParameter("EndNo"));
		schemaLZCardPrint.setGetMakeDate(request.getParameter("GetMakeDate"));
		schemaLZCardPrint.setSumCount(request.getParameter("SumCount"));
		schemaLZCardPrint.setState(request.getParameter("State"));
	    schemaLZCardPrint.setActiveDate(request.getParameter("ActiveDate"));
		System.out.println("ActiveDate:------------- "+request.getParameter("ActiveDate"));
		String strCertifyClass = request.getParameter("CertifyClass");
			
	  // 准备传输数据 VData
	  VData vData = new VData();

		vData.addElement(schemaLZCardPrint);
		vData.add(globalInput);
		
		Hashtable hashParams = new Hashtable();
		
		hashParams.put("CertifyClass", strCertifyClass);
		
		vData.add( hashParams );
	
		CardPrintUI tCardPrintUI = new CardPrintUI();
	  try {
	    if ( !tCardPrintUI.submitData(vData, strOperate) ) {
	    
		     if((optType.equals("1") && checkRule.equals("2"))||(optType.equals("3") && checkRule.equals("2")))
		     {
		        String strSql="delete from LZCardNumber where cardtype='"+schemaLZCardPrint.getSubCode()+"' and prtno='"+request.getParameter("prtNo")+"'  ";
		  	    if (!(new ExeSQL().execUpdateSQL(strSql)))
		        {
		           return " 保存失败，原因是: 删除LZCardNumber中数据失败！";
		        }
		     }
	    
		   	if( tCardPrintUI.mErrors.needDealError() ) {
		   		return tCardPrintUI.mErrors.getFirstError();
			  } else {
			  	return "保存失败，但是没有详细的原因";
			  }
			}
	    
	  } catch(Exception ex) {
	  	ex.printStackTrace();
			return "保存失败，原因是:" + ex.toString();	  	
	  }
		VData vData1 = new VData();
	  vData1 = tCardPrintUI.getResult();
		schemaLZCardPrint = null;
		schemaLZCardPrint = (LZCardPrintSchema)vData1.getObjectByObjectName("LZCardPrintSchema", 0);
		String prtNo = schemaLZCardPrint.getPrtNo();	
	  String certifyCode = schemaLZCardPrint.getCertifyCode();	
	  String startNo = schemaLZCardPrint.getStartNo();
		String endNo = schemaLZCardPrint.getEndNo();
    
    vData.clear();
    
    schemaLZCardPrint.setPrtNo(prtNo);
    schemaLZCardPrint.setCertifyCode(request.getParameter("CertifyCode"));
		schemaLZCardPrint.setRiskCode(request.getParameter("RiskCode"));
		schemaLZCardPrint.setRiskVersion(request.getParameter("RiskVersion"));
		schemaLZCardPrint.setSubCode(request.getParameter("CardType"));
		schemaLZCardPrint.setMaxMoney("1");
		schemaLZCardPrint.setMaxDate(request.getParameter("MaxDate"));
		schemaLZCardPrint.setComCode(request.getParameter("ComCode"));
		schemaLZCardPrint.setPhone(request.getParameter("Phone"));
		schemaLZCardPrint.setLinkMan(request.getParameter("LinkMan"));
		schemaLZCardPrint.setCertifyPrice(request.getParameter("CertifyPrice"));
		schemaLZCardPrint.setManageCom(request.getParameter("ManageCom"));
		schemaLZCardPrint.setOperatorInput(request.getParameter("OperatorInput"));
		schemaLZCardPrint.setInputDate(request.getParameter("InputDate"));
		schemaLZCardPrint.setInputMakeDate(request.getParameter("InputMakeDate"));
		schemaLZCardPrint.setGetMan(request.getParameter("GetMan"));
		schemaLZCardPrint.setGetDate(request.getParameter("GetDate"));
		schemaLZCardPrint.setOperatorGet(request.getParameter("OperatorGet"));
		schemaLZCardPrint.setStartNo(startNo);
		schemaLZCardPrint.setEndNo(endNo);
		schemaLZCardPrint.setGetMakeDate(request.getParameter("GetMakeDate"));
		schemaLZCardPrint.setSumCount(request.getParameter("SumCount"));
    System.out.println("sumcount: "+request.getParameter("SumCount"));		
		schemaLZCardPrint.setState(request.getParameter("State"));
		schemaLZCardPrint.setActiveDate(request.getParameter("ActiveDate"));
		System.out.println("ActiveDate:========= "+request.getParameter("ActiveDate"));
		
    vData.addElement(schemaLZCardPrint);
		vData.add(globalInput);
	  
	  strOperate="INSERT||CONFIRM";
	  
	  tCardPrintUI = null;
	  try 
	  {
	  	tCardPrintUI = new CardPrintUI();
	 	  
	    if ( !tCardPrintUI.submitData(vData, strOperate) ) 
	    {
	    	ExeSQL tExeSQL = new ExeSQL();
		  	String strSql="delete from LZCardPrint where certifycode='"+schemaLZCardPrint.getCertifyCode()
		  		+"' and startno='"+schemaLZCardPrint.getStartNo()+"' and endno='"+schemaLZCardPrint.getEndNo()+"'"
		  		;
		  	if (!tExeSQL.execUpdateSQL(strSql))
	      {
	        return " 保存失败，原因是:提单操作失败,且不能消除定单数据！";
	      }
	    	
		   	if( tCardPrintUI.mErrors.needDealError() ) 
		   	{
		   		return tCardPrintUI.mErrors.getFirstError();
			  } else 
			  {
			  	return "保存失败，但是没有详细的原因";
			  }
			}
	  } catch(Exception ex) {
	  	ex.printStackTrace();
			return "保存失败，原因是:" + ex.toString();	  	
	  }
		/*-----------------入库---------------*/	  
		
		LZCardSet setLZCard = new LZCardSet( );
		LZCardPrintSet tsetLZCardPrint = new LZCardPrintSet( );
		System.out.println("sumcount: "+schemaLZCardPrint.getCertifyCode());		
		try
		{
			// 加入打印表信息
			tsetLZCardPrint.add(schemaLZCardPrint);

  		LZCardSchema schemaLZCard = new LZCardSchema( );

	    schemaLZCard.setCertifyCode(certifyCode);
			schemaLZCard.setSubCode(schemaLZCardPrint.getSubCode());
			schemaLZCard.setRiskCode("");
			schemaLZCard.setRiskVersion("");

	    schemaLZCard.setSendOutCom("00");
	    schemaLZCard.setReceiveCom("A"+globalInput.ComCode);

			schemaLZCard.setSumCount(0);
			schemaLZCard.setPrem("");
	    schemaLZCard.setAmnt("");
	    schemaLZCard.setHandler(globalInput.Operator);
	    schemaLZCard.setHandleDate(request.getParameter("GetDate"));
	    schemaLZCard.setInvaliDate(request.getParameter("GetDate"));

			schemaLZCard.setTakeBackNo("");
			schemaLZCard.setSaleChnl("");
			schemaLZCard.setStateFlag("0");
			schemaLZCard.setOperateFlag("0");
			schemaLZCard.setPayFlag("");
			schemaLZCard.setEnterAccFlag("");
			schemaLZCard.setReason("");
			schemaLZCard.setState("0");
			schemaLZCard.setOperator("");
			schemaLZCard.setMakeDate("");
			schemaLZCard.setMakeTime("");
			schemaLZCard.setModifyDate("");
			schemaLZCard.setModifyTime("");

			// 加入单证信息
	    setLZCard.add(schemaLZCard);
		
		  String szLimitFlag = request.getParameter("LimitFlag");
		  vData.clear();
		  vData.addElement(globalInput);
		  vData.addElement(setLZCard);
		  vData.addElement(tsetLZCardPrint);
		  vData.addElement(szLimitFlag);
		  
		  hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
		  vData.addElement(hashParams);
	
			// 设置操作字符串
			String szOperator = "INSERT";
			
			// 数据传输
		  CertSendOutUI tCertSendOutUI = new CertSendOutUI();
			
		  if (!tCertSendOutUI.submitData(vData, szOperator)) 
		  {
		  	ExeSQL tExeSQL = new ExeSQL();
		  	String strSql="delete from LZCardPrint where certifycode='"+schemaLZCardPrint.getCertifyCode()
		  		+"' and startno='"+schemaLZCardPrint.getStartNo()+"' and endno='"+schemaLZCardPrint.getEndNo()+"'"
		  		;
		  	if (!tExeSQL.execUpdateSQL(strSql))
		    {
		        return " 保存失败，原因是: 入库操作失败,且不能消除定单数据！";
		    }
	        if((optType.equals("1") && checkRule.equals("2"))||(optType.equals("3") && checkRule.equals("2")))
		    {
		        String strSql1="delete from LZCardNumber where cardtype='"+schemaLZCardPrint.getSubCode()+"' and prtno='"+request.getParameter("prtNo")+"'  ";
		  	    if (!(new ExeSQL().execUpdateSQL(strSql1)))
		        {
		           return " 保存失败，原因是:在tCertSendOutUI后 删除LZCardNumber中数据失败！";
		        }
		    }
		  	
		    return " 保存失败，原因是: 入库操作失败！";
		  }
	  	
	  	String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0); 
			System.out.println("abc: "+strTakeBackNo);	  
	  	
	  }
		catch(Exception ex) 
		{
	  	ex.printStackTrace();
			return "保存失败，原因是:" + ex.toString();	  	
	  }
	  
	  return "";
	}
%>

<%
	String FlagStr = "";
	String Content = "";
	
	try {
		Content = handleFunction(session, request);
		
		if( Content.equals("") ) {
			FlagStr = "Succ";
			Content = "操作成功";
		} else {
			FlagStr = "Fail";
		}
	} catch (Exception ex) {
		ex.printStackTrace();
	}
%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

