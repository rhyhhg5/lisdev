var arrDataSet
var showInfo;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

// 查询按钮
function easyQueryClick()
{
    // 初始化表格
    initPolGrid();
    try{
    
  if(!verifyInput2())
    {
		return false;
    }
    if(fm.CertifyCode.value == ""||fm.CertifyCode.value == null || fm.CertifyCode.value == "null")
    {
        alert("单证编码不能为空！");
        return false;
    }
    // 书写SQL语句
    var strSQL ="select lz.certifycode,lz.startno,lz.endno,lz.state,lz.stateflag " 
   			     +"from lzcard lz "
   			     +"where 1=1"
    			 +getWherePart('lz.certifycode','CertifyCode') 
    			 +getWherePart('lz.startno','StartNo')
    			 +getWherePart('lz.endno','EndNo')
    			 +" with ur ";   
    turnPage1.pageLineNum = 20;
    turnPage1.strQueryResult  = easyQueryVer3(strSQL);
    
    //判断是否查询成功
    if (!turnPage1.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
    
    turnPage1.queryModal(strSQL, PolGrid);
    }catch(e){
    alert(e.message);
    }
}
//修改单证状态
function modifyCertify()
{
	var i = 0;
	var checkFlag = 0;
	
	for(i = 0; i < PolGrid.mulLineCount; i++)
	{
		if(PolGrid.getSelNo(i))
		{
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	
	if(checkFlag == 0)
	{
		alert("请先选择一条信息！");
		return false;
	}
	/*if(fm.upStateflagModify.value == "" || fm.upStateModify.value == ""){
		alert("不能为空！");
		fm.upStateflagModify.focus();
		return false;
	}
	/*
	var tContNo=PolGrid.getRowColData(checkFlag-1,2);
	var tContPlanCode=PolGrid.getRowColData(checkFlag-1,5);
	var tRiskCode=PolGrid.getRowColData(checkFlag-1,6);
	var tDutyCode=PolGrid.getRowColData(checkFlag-1,7);
	var tOldAmnt=PolGrid.getRowColData(checkFlag-1,8);
	var tContType=PolGrid.getRowColData(checkFlag-1,11);
	
	var tWhere = "";
	var tSQLBQ = "";
	var tLPSQL = "";
	if("1" == tContType){
		tWhere = " where contno = '"+tContNo+"' and riskcode = '"+tRiskCode+"' ";
		tSQLBQ = "select 1 from lpedoritem where contno = '"+tContNo+"' ";
		tLPSQL = "select 1 from LLClaimDetail where contno = '"+tContNo+"' union select 1 from LLClaimPolicy where contno = '"+tContNo+"' ";
	}else{
		tWhere = " where grpcontno = '"+tContNo+"' and contplancode = '"+tContPlanCode+"' and riskcode = '"+tRiskCode+"' ";
		tSQLBQ = "select 1 from lpgrpedoritem where grpcontno = '"+tContNo+"' ";
		tLPSQL = "select 1 from LLClaimDetail where grpcontno = '"+tContNo+"' union select 1 from LLClaimPolicy where grpcontno = '"+tContNo+"' ";
	}
    */
	
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.submit(); //提交
}
function afterSubmit(FlagStr,content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
			
}