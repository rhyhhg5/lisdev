<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ReportQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>

<%
    //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";

    //保单信息部分
    String mOperateType="RETURNDATA";

    System.out.println("现在的操作标志是"+mOperateType);
    LMCertifyDesSchema tLMCertifyDesSchema = new LMCertifyDesSchema();
    tLMCertifyDesSchema.setCertifyCode(request.getParameter("CertifyCode"));
    String mCertifyClass = request.getParameter("CertifyClass");
    LMCertifyDesSet tLMCertifyDesSet = new LMCertifyDesSet();
    tLMCertifyDesSet.add(tLMCertifyDesSchema);
    System.out.println("单证类型是"+mCertifyClass);
    System.out.println("得到的单证号码是"+tLMCertifyDesSchema.getCertifyCode());
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.addElement(mOperateType);
    tVData.addElement(mCertifyClass);
    tVData.addElement(tLMCertifyDesSet);
    // 数据传输
    CertifyDescUI tCertifyDescUI = new CertifyDescUI();
    if (!tCertifyDescUI.submitData(tVData,mOperateType))
    {
    	Content = " 查询失败，原因是: " + tCertifyDescUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
    }
    else
    {
			tVData.clear();
			tVData = tCertifyDescUI.getResult();
			LMCertifyDesSchema yLMCertifyDesSchema = new LMCertifyDesSchema();
			LMCertifyDesSet yLMCertifyDesSet = new LMCertifyDesSet();
			yLMCertifyDesSet = (LMCertifyDesSet)tVData.getObjectByObjectName("LMCertifyDesSet",0);
			yLMCertifyDesSchema=yLMCertifyDesSet.get(1);
			System.out.println("接收的记录共有"+yLMCertifyDesSet.size());
		%>
    	<script language="javascript">
    		top.opener.document.all.CertifyCodeId.readOnly= "true";
            var SubCode = "<%=yLMCertifyDesSchema.getSubCode()%>";
            if(SubCode == null || SubCode == "null" || SubCode == "")
            {
                SubCode = "";
	        }
    		
	      top.opener.fm.all("CertifyCode").value 			= "<%=yLMCertifyDesSchema.getCertifyCode()%>";
	      top.opener.fm.all("SubCode").value 				= SubCode;
	      top.opener.fm.all("SubCodeBackup").value 		= "<%=yLMCertifyDesSchema.getSubCode()%>";//备份类型码    2007-9-13 13:38
	      
	      top.opener.fm.all("CertifyName").value 			= "<%=yLMCertifyDesSchema.getCertifyName()%>";
	      top.opener.fm.all("Prem").value 						= "<%=yLMCertifyDesSchema.getPrem()%>";
	      top.opener.fm.all("Amnt").value 						= "<%=yLMCertifyDesSchema.getAmnt()%>";
	      top.opener.fm.all("CertifyClass").value 		= "<%=yLMCertifyDesSchema.getCertifyClass()%>";   
	      top.opener.fm.all("DutyPer").value 		= "<%=yLMCertifyDesSchema.getPolPeriod()%>";   
	      top.opener.fm.all("DutyUinit").value 		= "<%=yLMCertifyDesSchema.getPolPeriodFlag()%>";
	      if(top.opener.fm.all("DutyUinit").value=='Y'){
	      	top.opener.fm.all("DutyUinitName").value 		= "年";
	      	top.opener.fm.DutyPer.disabled = false;
	      } else if(top.opener.fm.all("DutyUinit").value=='M'){
	      	top.opener.fm.all("DutyUinitName").value 		= "月";
	      	top.opener.fm.DutyPer.disabled = false;
	      } else if(top.opener.fm.all("DutyUinit").value=='D'){
	      	top.opener.fm.all("DutyUinitName").value 		= "日";
	      	top.opener.fm.DutyPer.disabled = false;
	      } else if(top.opener.fm.all("DutyUinit").value=='B'){
	      	top.opener.fm.all("DutyUinitName").value 		= "不定期";
	      	top.opener.fm.DutyPer.disabled = true;
	      }   
	      	 
	      var certifyClass = "<%=yLMCertifyDesSchema.getCertifyClass()%>";
	      if (certifyClass=="P")
	      {
	      	top.opener.fm.all("CertifyClassName").value = "普通单证";
	      	top.opener.CardRiskGrid.clearData();
	      	top.opener.fm.all("divCardRisk").style.display="none";
		    }
		    else
		    {
		    	top.opener.fm.all("CertifyClassName").value = "定额单证";
		    	if(<%=yLMCertifyDesSchema.getOperateType()%> != null && <%=yLMCertifyDesSchema.getOperateType()%> == '0'){
		    		top.opener.fm.all("divCardRisk").style.display="";
		    	}
		    	top.opener.fm.all("divShow").style.display="";
		    }      	
       	top.opener.fm.all("Note").value 							= "<%=yLMCertifyDesSchema.getNote()%>";
       	 	
       	top.opener.fm.all("State").value 							= "<%=yLMCertifyDesSchema.getState()%>";
       	var state="<%=yLMCertifyDesSchema.getState()%>";
       	if(state=="0")
       	{
       		top.opener.fm.all("StateName").value = "有效";
       	}else
       	{
       		top.opener.fm.all("StateName").value = "无效";
       	}
       	top.opener.fm.all("ManageCom").value = "<%=yLMCertifyDesSchema.getManageCom()%>" ;

       	if ("<%=yLMCertifyDesSchema.getManageCom()%>"!='A')
       	{
	       	var strSQL = "select Name from LDCOM where ComCode='"+<%=yLMCertifyDesSchema.getManageCom()%>+"'" ;
	       	var arrResult =easyExecSql(strSQL);
	       	if (arrResult==null)
	       	{
	       		alert("管理机构有误！");
	       	}
	       	top.opener.fm.all("ManageComName").value 		= arrResult ; 
	       	
	       	top.opener.fm.StateRadio[0].checked = true;
       		top.opener.divComName.style.display='';
					top.opener.divComCode.style.display='';
	      }
	      else
	      {
	      	top.opener.fm.all("ManageCom").value 		= 'A' ; 
	      	top.opener.fm.StateRadio[1].checked = true;
	      	
       		top.opener.divComName.style.display='none';
					top.opener.divComCode.style.display='none';
	      }	      
       	
       	top.opener.fm.all("CertifyLength").value 		= parseInt("<%=yLMCertifyDesSchema.getCertifyLength()%>") + SubCode.length;
       	
       	top.opener.fm.all("HaveNumber").value 			= "<%=yLMCertifyDesSchema.getHaveNumber()%>";
       	var haveNumber="<%=yLMCertifyDesSchema.getHaveNumber()%>"
       	if(haveNumber=="Y")
       	{
       		top.opener.fm.all("HaveNumberName").value = "有号单证";
       	}else
       	{
       		top.opener.fm.all("HaveNumberName").value = "无号单证";
       	}
       	
       	top.opener.fm.all("CertifyCode_1").value 		= "<%=yLMCertifyDesSchema.getCertifyCode()%>";
       	top.opener.fm.all("Unit").value 						= "<%=yLMCertifyDesSchema.getUnit()%>";
       	
       	top.opener.fm.all("VerifyFlag").value 			= "<%=yLMCertifyDesSchema.getVerifyFlag()%>";

       	var verifyFlag="<%=yLMCertifyDesSchema.getVerifyFlag()%>";//业务员校验标志    2007-9-13 12:01
       	if (verifyFlag=="Y")
       	{
       			top.opener.fm.all("VerifyFlagName").value = "校验";      		
       	} else if(verifyFlag=="N")
       	{
       			top.opener.fm.all("VerifyFlagName").value = "不校验";       	
       	} else
       	{
       			top.opener.fm.all("VerifyFlagName").value = "";
       	}
       	
       	var CheckRule="<%=yLMCertifyDesSchema.getCheckRule()%>";//校验规则    2007-9-13 12:01
       	if (CheckRule != null && CheckRule != "") {
       	  if (CheckRule=="1")
       	  {
       	  		top.opener.fm.all("CheckRule").value = "1"; 
       	  		top.opener.fm.all("ChkRuleName").value = "有校验";      		
       	  } else if(CheckRule=="2")
       	  {
       	  		top.opener.fm.all("CheckRule").value = "2"; 
       	  		top.opener.fm.all("ChkRuleName").value = "无校验";       	
       	  } else
       	  {
       	  		top.opener.fm.all("CheckRule").value = ""; 
       	  		top.opener.fm.all("ChkRuleName").value = "";
       	  }
       	}
       	
       	var Operate="<%=yLMCertifyDesSchema.getOperateType()%>";//业务类型  0.卡单;1.撕单.    2007-9-13 16:56
       	if (Operate != null && Operate.equals != "") {
       	  if (Operate=="0")
       	  {
       	  		top.opener.fm.all("CardTypeName").value = "卡单业务";      		
       	  		top.opener.fm.all("Operate").value = "0";
         	} else if(Operate=="1")
         	{
         			top.opener.fm.all("CardTypeName").value = "撕单业务";  
         			top.opener.fm.all("Operate").value = "1";     	
         	} else
         	{
         			top.opener.fm.all("CardTypeName").value = "";
         			top.opener.fm.all("Operate").value = "";
         	}
        }
       	
       	top.opener.emptyUndefined();

    	</script>
		<%
    System.out.println("现在是定额单证！！！");
		if(mCertifyClass.equals("D"))
		{
			//若是定额单证，则要显示出LMCardRisk（定额单证险种描述表）的信息。
      LMCardRiskSet tLMCardRiskSet = new LMCardRiskSet();
      System.out.println("开始接收定额单证险种描述表的信息！！！");
      tLMCardRiskSet = (LMCardRiskSet)tVData.getObjectByObjectName("LMCardRiskSet",0);
      System.out.println("%%%%%%%%tLMCardRiskSet.size()是"+tLMCardRiskSet.size());
      %>
      <script language="javascript">
      top.opener.CardRiskGrid.clearData();
      </script>
      <%
      if(tLMCardRiskSet.size()!=0)
      {
        System.out.println("显示MulLine信息！！！11");
        for(int i = 1;i <= tLMCardRiskSet.size();i++)
        {
          LMCardRiskSchema tLMCardRiskSchema = new LMCardRiskSchema();
          tLMCardRiskSchema.setSchema(tLMCardRiskSet.get(i));
          %>
          <script language="javascript">
            top.opener.CardRiskGrid.addOne("CardRiskGrid");
            top.opener.CardRiskGrid.setRowColData(<%=i-1%>,1,"<%=tLMCardRiskSchema.getRiskCode()%>");
            top.opener.CardRiskGrid.setRowColData(<%=i-1%>,2,"<%=tLMCardRiskSchema.getPrem()%>");
            top.opener.CardRiskGrid.setRowColData(<%=i-1%>,3,"<%=tLMCardRiskSchema.getPremLot()%>");
            top.opener.CardRiskGrid.setRowColData(<%=i-1%>,4,"<%=tLMCardRiskSchema.getMult()%>");
            top.opener.CardRiskGrid.setRowColData(<%=i-1%>,5,"<%=tLMCardRiskSchema.getRiskType()%>");
            top.opener.emptyUndefined();
          </script>
          <%
        }

      }
		}
	}
  if (FlagStr == "Fail")
  {
    tError = tCertifyDescUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  out.println("top.close();");
  out.println("</script>");
%>