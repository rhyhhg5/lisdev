<%
//程序名称：LLReportInput.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.certify.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  String CurrentDate = PubFun.getCurrentDate();
  String CurrentTime = PubFun.getCurrentTime();
  String FlagStr = "";
	String Content = "";  
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }else
  {
	 
	  CertifyPayUI mCertifyPayUI = new CertifyPayUI();
	  CErrors tError = null;
	  String mOperateType = request.getParameter("OperateType");
	  String mPayNo = request.getParameter("PayNo");
	  System.out.println("操作的类型是"+mOperateType);
	  String tRela  = "";	  
	  String mDescType = "";//将操作标志的英文转换成汉字的形式
	
	  System.out.println("开始进行获取数据的操作！！！");	     
    LZCardPayDB mLZCardPayDB = new LZCardPayDB(); 
    LZCardPaySet mLZCardPaySet = new LZCardPaySet();   
    mLZCardPayDB.setPayNo(mPayNo);
    mLZCardPayDB.setState("0");
    mLZCardPaySet=mLZCardPayDB.query();
	  if(mOperateType.equals("AFFIRM"))
	  {
	    mDescType = "确认";
	  }	 
	  VData tVData = new VData();
	  try
	  {
	    tVData.addElement(mOperateType);   
	    tVData.addElement(mLZCardPaySet);   
	    tVData.addElement(tGI);    
	    
	    mCertifyPayUI.submitData(tVData,mOperateType);
	  }
	  catch(Exception ex)
	  {
	    Content = mDescType+"失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	  }
	
	  //如果在Catch中发现异常，则不从错误类中提取错误信息
	  if (FlagStr=="")
	  {
	    tError = mCertifyPayUI.mErrors;
	    if (!tError.needDealError())
	    {
	      Content = mDescType+" 成功";
	    	FlagStr = "Succ";
	    }
	    else
	    {
	    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	  }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>