<%
	// 防止IE缓存页面
	response.setHeader("Pragma","No-cache"); 
	response.setHeader("Cache-Control","no-cache"); 
	response.setDateHeader("Expires", 0); 
%>

<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-05-15
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="CertReveTakeBackInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertReveTakeBackInit.jsp"%>
</head>
<body onload="initForm()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./CertReveTakeBackSave.jsp" method="post" name=fm target="fraSubmit">
    <table class="common">
    	<tr class="common">
    		<td class="input">
    			<input name="btnOp" class="cssButton" type="button" value="回收回退" onclick="submitForm()">
    			<input name="chkPrt" type="checkbox" checked>打印清单</td>

    		<td class="input"></td>
    	</tr>
    
    </table>

    <!-- 回收回退的信息 -->
    <div style="width:120">
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSendOutInfo);"></td>
          <td class="titleImg">回收回退信息</td></tr></table></div>

    <div id="divSendOutInfo">
      <table class="common">
        <tr class="common">
          <td class="title">发放者</td>
          <td class="input"><input class="common" name="SendOutCom" verify="发放者|NOTNULL"><input type="hidden" name="SendOutCom"></td>

          <td class="title">接收者</td>
          <td class="input"><input class="common" name="ReceiveCom" verify="接收者|NOTNULL"></td></tr>

        <tr class="common">
          <td class="title">失效日期</td>
          <td class="input"><input class="readonly" readonly name="InvalidDate"></td>

          <td class="title">最大金额</td>
          <td class="input"><input class="readonly" readonly name="Amnt"></td></tr>
          <!-- 用“总保额”来表示“最大金额” -->

        <tr class="common">
          <td class="title">经办人</td>
          <td class="input"><input class="common" name="Handler"></td>

          <td class="title">经办日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDate"></td></tr>

        <tr class="common">
          <td class="title">操作员</td>
          <td class="input"><input class="readonly" readonly name="Operator"></td>

          <td class="title">当前时间</td>
          <td class="input"><input class="readonly" readonly name="curTime"></td></tr>
      </table>
    </div>

    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>单证列表</td></tr>
    </table>

		<div id="divCertifyList">
      <table class="common">
        <tr class="common">
          <td text-align: left colSpan=1><span id="spanCertifyList"></span></td></tr>
	  	</table>
		</div>
		
		<input type=hidden name="sql_where">
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
