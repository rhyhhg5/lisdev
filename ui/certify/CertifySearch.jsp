<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
%>
<html>
<head>
<script>
	var tManageCom = <%=tG1.ManageCom%>;
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="CertifySearch.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifySearchInit.jsp"%>
<title>单证查询打印</title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./CertifySearchOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 单证信息部分 -->
    <table class="common" border=0 width=100%>
      <tr><td class="titleImg" align="center">请输入查询条件：</td></tr>
	  </table>
    <table class="common" align="center" id="tbInfo" name="tbInfo">
      <tr class="common">
        <td class="title">单证编码</td>
        <td class="input">
          <input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('certifycode1', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,300);" onkeyup="return showCodeListKey('certifycode1', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,300);" verify="单证编码|notnull"  ><Input class= codename name="CertifyCodeName"  elementtype="nacessary" readonly></td>
        <td class="title">单证类型码</td>
        <td class="input"><input class="common" name="CardType"></td>
      </tr>

      <tr class="common">
        <td class="title">发放者</td>
        <td class="input">
          	<input class="codeno" name="SendOutCom" 
          	ondblclick="return showCodeList('sendoutcodesearch', [this,SendOutName],[0,1],null,null,null,1);"
            onkeyup="return showCodeListKey('sendoutcodesearch', [this,SendOutName],[0,1],null,null,null,1);"
          	><input name = "SendOutName" class=codename type="text">
        </td>

        <td class="title">接收者</td>
        <td class="input" nowrap=true>
           	<input class="codeno" name="ReceiveCom"  
	          	ondblclick="return showCodeList('receivecodesearch', [this,ReceiveName],[0,1],null,fm.ManageCom.value,'ComCode',1);"
	            onkeyup="return showCodeListKey('receivecodesearch', [this,ReceiveName],[0,1],null,fm.ManageCom.value,'ComCode',1);"
	          ><input name = "ReceiveName" class=codename type="text">
          	<input class="codeno" name="AgentCode" 
          	ondblclick="return showCodeList('sendoutcodesearch', [this],[0],null,null,null,1);"
            onkeyup="return showCodeListKey('sendoutcodesearch', [this],[0],null,null,null,1);"
          	style="display:'none'">
          	<input class="common" name="GroupAgentCode" 
          	ondblclick="return showCodeList('sendoutcodesearch', [this],[0],null,null,null,1);"
            onkeyup="return showCodeListKey('sendoutcodesearch', [this],[0],null,null,null,1);"
          	style="display:'none'">
          	<input type="button" class="button" name="AgentQuery" value="业务员查询" onclick="queryAgent()" style="display:'none'">
          	<input type="button" class="button" name="btnQueryCom" value="代理机构查询" onclick="queryCom()" style="display:none"></td>
      </tr>

      <tr class="common">
        <td class="title">经办日期（开始）</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDateB"></td>

        <td class="title">经办日期（结束）</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDateE"></td>
      </tr>
        
       <tr class="common" >
         <TD  class= title8>
      		管理机构
    	</TD>
    	<TD  class= input8>
      		<Input class=codeNo name=ManageComC   ondblclick="return showCodeList('comcode',[this,ManageComCName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComCName],[0,1],null,null,null,1);"><input class=codename name=ManageComCName >
    	</TD>
      </tr>
       
      <tr class="common" style="display:'none'">
        <td class="title">经办人</td>
        <td class="input"><input class="common" name="Handler"></td>
      </tr>
	</table>
	
    <table class="common" border=0 width=100%>
      <tr><td class="titleImg" align="center">记录明细：</td></tr>
	  </table>
    <table class="common" align="center" id="tbInfo" name="tbInfo">
      <tr class="common">
        <td class="title">清算单号</td>
        <td class="input"><input class="common" name="TakeBackNo"></td>

        <td class="title">数量</td>
        <td class="input"><input class="common" name="SumCount"></td></tr>

      <tr class="common">
        <td class="title">起始号</td>
        <td class="input"><input class="common" name="StartNo"></td>

        <td class="title">终止号</td>
        <td class="input"><input class="common" name="EndNo"></td></tr>

      <tr class="common">
        <td class="title">单证状态</td>
        <td class="input"><input class="codeNo" name="CertifyState" CodeData="0|^0|未领用|^2|作废|^3|遗失|^4|销毁|^5|正常回销|^6|总部领用|^7|已发放|^8|已领用|^9|空白回销" ondblclick="return showCodeListEx('CertifyState',[this,CertifyStateName],[0,1],null,null,null,1);"><input class=codename name=CertifyStateName readonly=true ></td>

        <td class="title">操作标志</td>
        <td class="input"><input class="common" name="OperateFlag"></td></tr>

    </table>

    <input value="查询状态" type="button" class="cssButton" onclick="searchState();">
    <input value="查询轨迹" type="button" class="cssButton" onclick="searchTrack();">
    <input value="打  印" type="button" class="cssButton" onclick="printList();">
    <br>
    
    <div style="display:none">
	    <input value="打  印" type="button" class="cssButton" onclick="printList();">
	    <input value="重  置" type="button" class="cssButton" onclick="initInpBox(); initCardInfo();">
		</div>
    <input type="hidden" name="State">

    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCardInfo);"></td>
    	<td class= titleImg>单证信息</td></tr>
    </table>

  	<div id="divCardInfo" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCardInfo"></span></td></tr>
      </table>
      
      <div id="divSumCount" style="display:'none'">
	  	<table class="common" align="center" id="tbInfo" name="tbInfo">
	      <tr class="common">
	      	<td class="title"></td><td class="input"></td><td class="title"></td>
	        <td class="input"></td>
	        <td class="title">统计总数量</td>
	        <td class="input"><input class="common" name="certifysumcount"></td>
	        <td class="title"></td>
	        <td class="input"></td>
	      </tr>
	    </table>
		</div>

      <!--  INPUT VALUE="首  页" Class=cssbutton TYPE=button onclick="getFirstPage();">
      <INPUT VALUE="上一页" Class=cssbutton TYPE=button onclick="getPreviousPage();">
      <INPUT VALUE="下一页" Class=cssbutton TYPE=button onclick="getNextPage();">
      <INPUT VALUE="尾  页" Class=cssbutton TYPE=button onclick="getLastPage();"-->
      <input VALUE="首  页" TYPE="button" class="cssButton" onclick="turnPage2.firstPage();">
      <input VALUE="上一页" TYPE="button" class="cssButton" onclick="turnPage2.previousPage();">
      <input VALUE="下一页" TYPE="button" class="cssButton" onclick="turnPage2.nextPage();">
      <input VALUE="尾  页" TYPE="button" class="cssButton" onclick="turnPage2.lastPage();">
  	</div>
  	
  	<table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCardState);"></td>
    	<td class= titleImg>单证数量统计</td></tr>
    </table>

  	<div id="divCardState" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCardState"></span></td></tr>
    </table>
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
    </div>
  	
    <br>
    
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	  
	  <input  name="TempComCode" type="hidden" value="<%=tG1.ComCode%>">
	  <input  name="TempOpe" type="hidden" value="<%=tG1.Operator%>">
	  <input type=hidden name="ReceiveType" value="COM">
		<input type=hidden name="ManageCom" value="<%=tG1.ManageCom%>">
		<input type=hidden name="AManageCom" value="">
		<input type=hidden name="SearchSQL" value="">
  </form>
</body>
</html>
