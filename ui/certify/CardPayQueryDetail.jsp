<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ReportQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>

<%
    //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    GlobalInput tGI = new GlobalInput(); //repair:
    tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp  
  
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }else
  {

    //保单信息部分
    String mOperateType="QUERY";

    System.out.println("现在的操作标志是"+mOperateType);
    LZCardPaySchema tLZCardPaySchema = new LZCardPaySchema();
    LZCardPaySet tLZCardPaySet = new LZCardPaySet();
    tLZCardPaySchema.setPayNo(request.getParameter("PayNo"));
    tLZCardPaySchema.setCardType(request.getParameter("CardType"));
    tLZCardPaySchema.setStartNo(request.getParameter("StartNo"));
    tLZCardPaySchema.setEndNo(request.getParameter("EndNo"));

    tLZCardPaySet.add(tLZCardPaySchema);
    System.out.println("得到的操作表示是"+mOperateType);    
    System.out.println("得到的结算单号是"+tLZCardPaySchema.getPayNo());
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.addElement(mOperateType);    
    tVData.addElement(tLZCardPaySet);
    tVData.addElement(tGI);
    // 数据传输
    CertifyPayUI tCertifyPayUI = new CertifyPayUI();
    if (!tCertifyPayUI.submitData(tVData,mOperateType))
    {
    	Content = " 查询失败，原因是: " + tCertifyPayUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
    }
    else
    {
			tVData.clear();
			tVData = tCertifyPayUI.getResult();
			LZCardPaySchema yLZCardPaySchema = new LZCardPaySchema();
			LZCardPaySet yLZCardPaySet = new LZCardPaySet();
			yLZCardPaySet = (LZCardPaySet)tVData.getObjectByObjectName("LZCardPaySet",0);
			yLZCardPaySchema=yLZCardPaySet.get(1);
			System.out.println("接收的记录共有"+yLZCardPaySet.size());
		%>
    	<script language="javascript">
    		top.opener.fm.all("PayNo").value 			= "<%=yLZCardPaySchema.getPayNo()%>";
    		top.opener.fm.all("CardType").value 			= "<%=yLZCardPaySchema.getCardType()%>";
    		top.opener.fm.all("AgentCode").value 			= "<%=yLZCardPaySchema.getHandlerCode()%>";
    		top.opener.fm.all("AgentName").value 			= "<%=yLZCardPaySchema.getHandler()%>";
    		top.opener.fm.all("AgentCom").value 			= "<%=yLZCardPaySchema.getAgentCom()%>";
    		top.opener.fm.all("AgeComName").value 			= "<%=yLZCardPaySchema.getAgentComName()%>";
    		top.opener.fm.all("PayMoney").value 			= "<%=yLZCardPaySchema.getDuePayMoney()%>";
    		top.opener.CardRiskGrid.clearData();    
    	</script>
    	<%
    	 for(int i =0 ; i<yLZCardPaySet.size();i++){
    	   yLZCardPaySchema=yLZCardPaySet.get(i+1);
    	%>
	    	<script language="javascript">
	    	   	
	    		top.opener.CardRiskGrid.addOne("CardRiskGrid");
	        top.opener.CardRiskGrid.setRowColData(<%=i%>,1,"<%=yLZCardPaySchema.getStartNo()%>");
	        top.opener.CardRiskGrid.setRowColData(<%=i%>,2,"<%=yLZCardPaySchema.getEndNo()%>");
	        top.opener.CardRiskGrid.setRowColData(<%=i%>,3,"<%=yLZCardPaySchema.getSumCount()%>");     
	        top.opener.emptyUndefined();
		     
	    	</script>
		 <%
		  }

		}
	  if (FlagStr == "Fail")
	  {
	    tError = tCertifyPayUI.mErrors;
	    if (!tError.needDealError())
	    {
	    	Content = " 查询成功! ";
	    	FlagStr = "Succ";
	    }
	    else
	    {
	    	Content = " 查询失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
    }
   System.out.println("------end------");
   System.out.println(FlagStr);
   System.out.println(Content);
  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  out.println("top.close();");
  out.println("</script>");
 }
%>