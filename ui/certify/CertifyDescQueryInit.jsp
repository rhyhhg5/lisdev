<%
//Creator :刘岩松
//Date :2003-04-18
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('CertifyCode').value = '';
    fm.all('SubCode').value = '';
    fm.all('RiskCode').value = '';
    fm.all('CertifyName').value = '';
    fm.all('Prem').value = '';
    fm.all('Amnt').value = '';
    fm.all('CertifyClass').value = '';
    fm.all('ImportantLevel').value = '';
    fm.all('State').value = '';
    fm.all('ManageCom').value = '';
    fm.all('InnerCertifyCode').value = '';
    fm.all('CertifyLength').value = '';
    fm.all('HaveNumber').value = '';
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}


// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCertifyDescGrid();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCertifyDescGrid()
{
	var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="单证号码";    	//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="单证名称";         			//列名
      iArray[2][1]="250px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
 
      iArray[3]=new Array();
      iArray[3][0]="险种编码";         			//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      
      iArray[4]=new Array();
      iArray[4][0]="单证类型";         			//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="重要级别";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[6]=new Array();
      iArray[6][0]="总保费";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="总保额";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      CertifyDescGrid = new MulLineEnter( "fm" , "CertifyDescGrid" ); 
      CertifyDescGrid.mulLineCount = 0;   
      CertifyDescGrid.displayTitle = 1;
      CertifyDescGrid.canSel=1;
      CertifyDescGrid.locked = 1;	
	CertifyDescGrid.hiddenPlus = 1;
	CertifyDescGrid.hiddenSubtraction = 1;
      CertifyDescGrid.loadMulLine(iArray);  
      CertifyDescGrid.detailInfo="单击显示详细信息";
      //CertifyDescGrid.detailClick=reportDetailClick;
     
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>