<%
//程序名称：OrderDescSave.jsp
//程序功能：
//创建日期：2006-07-29
//创建人  ：Zhang Bin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.certify.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  LZOrderSchema mLZOrderSchema = new LZOrderSchema();
  //LZOrderCardSchema mLZOrderCardSchema = new LZOrderCardSchema();
  
  LZOrderSet mLZOrderSet = new LZOrderSet();
  LZOrderCardSet mLZOrderCardSet = new LZOrderCardSet();
    
  OrderDescUI mOrderDescUI = new OrderDescUI();
  
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String mDescType = "";//将操作标志的英文转换成汉字的形式
  
  System.out.println("开始进行获取数据的操作！！！");
  mLZOrderSchema.setSerialNo(request.getParameter("SerialNo"));
  mLZOrderSchema.setDescPerson(request.getParameter("OrderCreator"));
  mLZOrderSchema.setAttachDate(request.getParameter("AttachDate"));
  mLZOrderSchema.setNote(request.getParameter("Note"));
  
System.out.println("in jsp0 ->before submitData..........");    
  /****************************************************************************
   * 判断单证的类型，若是定额单证则要对定额单险种信息表进行描述
   ***************************************************************************/

  String[] strNumber 			= request.getParameterValues("CertifyTypeGridNo");
  String[] strCertifyCode = request.getParameterValues("CertifyTypeGrid1");
  String[] strOrderMoney 	= request.getParameterValues("CertifyTypeGrid3");


  int tLength = strNumber.length;
 
System.out.println("in jsp1 ->before submitData..........");    
  if(strNumber!=null)
  {
    for(int i = 0 ;i < tLength ;i++)
    {
      LZOrderCardSchema tLZOrderCardSchema = new LZOrderCardSchema();
      tLZOrderCardSchema.setCertifyCode(strCertifyCode[i]);
      tLZOrderCardSchema.setOrderMoney(strOrderMoney[i]);
      
      mLZOrderCardSet.add(tLZOrderCardSchema);
    }
  }

  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增批次";
  }
  if(mOperateType.equals("UPDATE"))
  {
    mDescType = "修改注释";
  }
  if(mOperateType.equals("DELETE"))
  {
    mDescType = "删除批次";
  }
  if(mOperateType.equals("QUERY"))
  {
    mDescType = "查询批次";
  }

  VData tVData = new VData();
  try
  {
  	tVData.addElement(globalInput);
    tVData.addElement(mLZOrderSchema);
    tVData.addElement(mLZOrderCardSet);
    mOrderDescUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mOrderDescUI.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+"成功，"+" 批次号："+mOrderDescUI.getResult();
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mOrderDescUI.getResult()%>");
</script>
</html>