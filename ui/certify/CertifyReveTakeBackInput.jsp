<% 
//清空缓存
//response.setHeader("Pragma","No-cache"); 
//response.setHeader("Cache-Control","no-cache"); 
//response.setDateHeader("Expires", 0); 
%>

<%@page contentType="text/html;charset=gb2312" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-07
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="CertifyReveTakeBackInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CertifyReveTakeBackInit.jsp"%>
</head>
<body  onload="initForm()" >
  <form action="./CertifyReveTakeBackSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 单证类型 -->
    <table class="common">
      <tr class="common">
        <td class="title">单证类型</td>
        <td><input class="code" id="CertifyType" onclick="return showCodeList('CertifyType', this);"></td></tr></table>
    
    <!-- 反回收的信息 -->    
    <div style="width:120">
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divReveTakeBack);"></td>
          <td class="titleImg">反回收信息</td></tr></table></div>

    <div id="divReveTakeBack">
      <table class="common">
        <tr class="common">
          <td class="title">回收者</td>
          <td class="input"><input class="common" id="Taker"></td>
          
          <td class="title">领用者</td>
          <td class="input"><input class="common" id="Receiver"></td></tr>
          
        <tr class="common">
          <td class="title">失效日期</td>
          <td class="input"><input class="common" id="InvalidDate"></td>
          
          <td class="title">最大金额</td>
          <td class="input"><input class="common" id="MaxMount"></td></tr>
          
        <tr class="common">
          <td class="title">提货人</td>
          <td class="input"><input class="common" id="Picker"></td>
        
          <td class="title">提货日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" id="PickDate"></td></tr>
          
        <tr class="common">
          <td class="title">操作员</td>
          <td class="input"><input class="common" id="Operator"></td>
          
          <td class="title">当前时间</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" id="curTime"></td></tr>
      </table>
    </div>
    
    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>单证列表</td></tr>
    </table>
    
	<Div  id= "divCertifyList" style= "display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanCertifyList" ></span></td></tr>
	  </table>
	</div>
  
  </form>
</body>
</html>
