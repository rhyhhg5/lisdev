<%
//程序名称：LZCardBugetInput.jsp
//程序功能：功能描述
//创建日期：2004-04-05 16:08:27
//创建人  ：yangtao
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");

%>
<html> 
<head> 
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LZCardBugetQueryInput.js"></SCRIPT> 
  <%@include file="LZCardBugetQueryInit.jsp"%>
  <title></title>
</head>


<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLZCardBugetGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common >
    <TD  class= title>
      单证编码
    </TD>
    <TD  class= input >
      <Input class="readonly" readonly name=CertifyCode>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      管理机构
    </TD>
    <TD  class= input>
       <input type="text" class="code" name=ManageCom onDblClick="showCodeList('station',[this],null,null,codeSql,'1',null,250);"  onKeyUp="return showCodeListKey('station',[this],null,null,codeSql,'1',null,250);">
   </TD>
    <TD  class= title>
      预算开始日期
    </TD>
    <TD  class= input>
       <Input class="coolDatePicker" dateFormat="short" verify="起始时间|NOTNULL" name=SDate> 
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      预算结束日期
    </TD>
    <TD  class= input>
      <Input class="coolDatePicker" dateFormat="short" verify="结束时间|NOTNULL" name=EDate>
    </TD>
    <TD  class= title>
      预算费用
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Buget >
    </TD>
  </TR>
 
  <TR  class= common>
    <TD  class= title>
      入机日期
    </TD>
    <TD  class= input>
      <Input class="coolDatePicker" dateFormat="short" verify="结束时间|NOTNULL"   name=MakeDate >
    </TD> 
 </TR>
      <!--
    <TD  class= title>
      入机时间
    </TD>
    <TD  class= input>
      <Input class="readonly" readonly name=MakeTime >
    </TD>


  <TR  class= common>
    <TD  class= title>
      最后一次修改日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyDate >
    </TD>
    <TD  class= title>
      最后一次修改时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ModifyTime >
    </TD>
  </TR>
  -->
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="打印" TYPE=button class=common onclick="printPlan();">
        <INPUT VALUE="查询" TYPE=button class=common onclick="easyQueryClick();">
        <INPUT VALUE="返回" TYPE=button class=common name=Return   onclick="returnParent();"> 	
        				
    </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
        <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLZCardBuget1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLZCardBuget1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLZCardBugetGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden name="Strsql">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
<script>
var codeSql = "1  and code like #"+comCode+"%#";
</script>