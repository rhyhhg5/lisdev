
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function submitForm() {
	if(!chkMulLine()){
     	return false;
    }  
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./CertifyMaxNoSave.jsp";
	fm.submit();
	fm.querySql.value = "";
	return true;
}
function cancel() {
	top.close();
}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	initForm();
}
function queryData(){
	var tSQL = " select managecom,certifycode,subcode,certifyname,CertifyMaxNo "
	         + " from LMCertifyDes "
	         + " where length(trim(managecom)) >=4 "
	         + " and operatetype = '0' "
	         + getWherePart('ManageCom', 'ManageCom','like')
	         + getWherePart('CertifyCode', 'CertifyCode')
	         + getWherePart('SubCode', 'SubCode','like')
	         + " order by managecom,certifycode with ur ";
	turnPage1.queryModal(tSQL,CertifyMaxNoGrid);
	if (!turnPage1.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
	fm.querySql.value = tSQL;
}
function chkMulLine()
{
	var i;
	var iCount = 0;
	var rowNum = CertifyMaxNoGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(CertifyMaxNoGrid.getChkNo(i))
		{
			iCount++;
			var tCertifyCode = CertifyMaxNoGrid.getRowColData(i,2);
			var tCertifyMaxNo = CertifyMaxNoGrid.getRowColData(i,5);
			if(!isInteger(tCertifyMaxNo)){
				alert("单证"+tCertifyCode+"配置的最大数量必须为整数！");
				return false;
			}
		}
	}
	if(iCount == 0)
	{
		alert("请选择要处理的单证数据!");
		return false
	}
	return true;
}
//下载
function downLoad() {
	if(fm.querySql.value != null && fm.querySql.value != "" && CertifyMaxNoGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "CertifyMaxNoDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}