//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput() == true && CertifyList.checkValue("CertifyList") ) {
	  var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	  fm.submit(); //提交
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	if( fm.chkPrt.checked == true ) {
	    var urlStr = "CertifyListPrint.jsp";
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:" + 
	      oClientCaps.availWidth + "px;dialogHeight:" + oClientCaps.availHeight + "px");
	  } else {
	    content="保存成功！";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  }
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
	parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

// 查询单证的信息
function queryCertify(certifyCode)
{
	if( certifyCode == "" ) {
		fm.SubCode.value = "";
		fm.RiskCode.value = "";
		fm.RiskVersion.value = "";
		fm.Prem.value = "";
		fm.Amnt.value = "";
	} else {
		var strSql = "SELECT SubCode, RiskCode, RiskVersion, Prem, Amnt FROM LMCertifyDes WHERE CertifyCode = '"
								+ certifyCode + "' AND CertifyClass = 'D'";

		var strRS = easyQueryVer3(strSql, 1, 0, 1);

		if( !strRS ) {
			fm.SubCode.value = "";
			fm.RiskCode.value = "";
			fm.RiskVersion.value = "";
			fm.Prem.value = "";
			fm.Amnt.value = "";
			return;
		}
		
		var arrRS = decodeEasyQueryResult(strRS);
		
		fm.SubCode.value = arrRS[0][0];
		fm.RiskCode.value = arrRS[0][1];
		fm.RiskVersion.value = arrRS[0][2];
		fm.Prem.value = arrRS[0][3];
		fm.Amnt.value = arrRS[0][4];
	}
}


function afterCodeSelect(cCodeName, Field)
{
	try {
		if( cCodeName == 'CardCode' ) {
			queryCertify(Field.value);
		}
	} catch(ex) {
		alert("在afterCodeSelect中发生异常");
	}
}