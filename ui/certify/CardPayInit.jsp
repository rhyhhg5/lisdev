<%
//Creator :刘岩松
//Date :2003-04-18
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
 		String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('CertifyCode').value = '';
    fm.all('VerifyFlag').value = '';
    fm.all('RiskCode').value = '0';
    fm.all('CertifyName').value = '';
    fm.all('Prem').value = '0';
    fm.all('Amnt').value = '0';
    fm.all('CertifyClass').value = 'P';
    fm.all('CertifyClassName').value = '普通单证';
    fm.all('CertifyClass2').value = '';
    fm.all('CertifyClass2Name').value = '';
    fm.all('Note').value = '';
    fm.all('ImportantLevel').value = '0';
    
    fm.all('State').value = '0';
    fm.all('StateName').value = '有效';
    
    fm.all('ManageCom').value = <%=Comcode%>;
    fm.all('InnerCertifyCode').value = '0';
    fm.all('CertifyLength').value = '';
    fm.all('HaveNumber').value = '';
    fm.all('HaveNumberName').value = '';
    fm.all('CertifyCode_1').value = '';
    fm.all('Unit').value = '';
    fm.all('VerifyFlag').value = '';
    fm.all('VerifyFlagName').value = '';
    fm.all('DutyPer').value='';
    fm.all('DutyUinit').value='D';  
    fm.all('DutyUinitName').value='日';  
    
    
    divCardRisk.style.display="none";
    
    getComName(<%=Comcode%>);
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;
function RegisterDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divDetailInfo.style.left=ex;
  	divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCardRiskGrid();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCardRiskGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="险种代码";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=2;
    iArray[1][4]="RiskCode";

    iArray[2]=new Array();
    iArray[2][0]="保费";         			//列名
    iArray[2][1]="200px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许  

    iArray[3]=new Array();
    iArray[3][0]="保费份额";         	//列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="档次";         	//列名
    iArray[4][1]="100px";            	//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    CardRiskGrid = new MulLineEnter( "fm" , "CardRiskGrid" );
    CardRiskGrid.mulLineCount = 0;
    CardRiskGrid.displayTitle = 1;
    //CardRiskGrid.canSel=1;
    CardRiskGrid.loadMulLine(iArray);
    CardRiskGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("初始化时出错2003-05-21"+ex);
  }
}

function getComName(comC)
{
	var strSQL="select name from LDCom where comcode= '"+ comC +"' ";
	var comN = easyExecSql(strSQL);	
	
	if (comN)
	{	
		fm.all("ManageComName").value= comN[0][0];
		//alert();
		
	}
}

function showCom()
{
	var typeRadio="";
	for(var i=0;fm.StateRadio.length;i++)
	{
		if(fm.StateRadio[i].checked)
		{
			typeRadio=fm.StateRadio[i].value;
			break;
		}
	}
	if (typeRadio=="0")
	{
		divComName.style.display='none';
		divComCode.style.display='none';
		fm.ManageCom.value = 'A';
	}
	else
	{
		divComName.style.display='';
		divComCode.style.display='';
		fm.ManageCom.value = <%=Comcode%>;
	}
}

</script>


