<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
    //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    GlobalInput globalInput = new GlobalInput();
    globalInput.setSchema( (GlobalInput)session.getValue("GI") );
	try
	{
	    String szNo[] = request.getParameterValues("CertifyListNo");
	    String szCertifyCode[] = request.getParameterValues("CertifyList1");
	    String szStartNo[] = request.getParameterValues("CertifyList3");
	    String szEndNo[] = request.getParameterValues("CertifyList4");
	    String szSumCount[] = request.getParameterValues("CertifyList5");

	    if( szNo == null ) {
	    	throw new Exception("没有输入要回收的起始单证号和终止单证号");
	    }

	    LZCardSet setLZCard = new LZCardSet();
	    for(int nIndex = 0; nIndex < szNo.length; nIndex ++ ) {
	        LZCardSchema schemaLZCard = new LZCardSchema();

	        schemaLZCard.setCertifyCode(szCertifyCode[nIndex]);
	        schemaLZCard.setStartNo(szStartNo[nIndex].trim());
	        schemaLZCard.setEndNo(szEndNo[nIndex].trim());
			schemaLZCard.setSumCount(szSumCount[nIndex].trim());
	        setLZCard.add(schemaLZCard);
        }

	    // 准备传输数据 VData
	    VData vData = new VData();
	    vData.addElement(globalInput);
	    vData.addElement(setLZCard);

	    // 数据传输
	    CertifyResendBL tCertifyResendBL = new CertifyResendBL();

	    if (!tCertifyResendBL.submitData(vData, "INSERT")) {
	        Content = " 保存失败，原因是: " + tCertifyResendBL.mErrors.getFirstError();
	        FlagStr = "Fail";
        } else {
	  	    Content = " 保存成功 ";
	  	    FlagStr = "Succ";
	    }
	}
	catch(Exception ex)
	{
		ex.printStackTrace();
   	    Content = FlagStr + " 保存失败，原因是:" + ex.getMessage();
   	    FlagStr = "Fail";
	}
%>
<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>