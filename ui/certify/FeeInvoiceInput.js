//               该文件中包含客户端需要处理的函数和事件
var turnPage=new turnPageClass(); 
var turnPage1=new turnPageClass(); 

var showInfo;
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{	
  var CertifyCount=CertifyList.mulLineCount;
  if (CertifyCount<=0) {
  	alert("没有打印发票信息！");
  	return false;
  }
  for (var rowNum = 0;rowNum < CertifyCount;rowNum++) {
  	var InvoiceNo = CertifyList.getRowColData(rowNum,4);
  	if (InvoiceNo==""||InvoiceNo==null) {
  		alert("请输入发票号！");
  		return false;
  	}
  }
  var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
	fm.submit(); //提交
  showInfo.close( );
}

function veriryInput3()
{
	var rowNum = CertifyList.mulLineCount ;
	if (rowNum==0)
	{
		alert("请在单证列表中录入发放信息！");
		return false;
	} 
	for(var i=1;i<=rowNum;i++)
	{
		var strSQL = "select havenumber from LMCertifyDes where certifycode='"+CertifyList.getRowColData(i-1,2)+"'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult == null)
		{
		  alert("没有单证编码为:"+CertifyList.getRowColData(i-1,2)+"的单证");
		  return false;
		}
		else
		{
			if (arrResult[0][0]=="Y")
			{
				if (CertifyList.getRowColData(i-1,6)!=""&&CertifyList.getRowColData(i-1,6)!=null)
				{
					var lineNum=CertifyList.getRowColData(i-1,5)-CertifyList.getRowColData(i-1,4)+1; 
					if (lineNum+""!=CertifyList.getRowColData(i-1,6))
					{
						alert("第"+i+"行数量与起始单号终止单号不符！");
						return false;
					}
				}
			}	
		}
	}  
	return true;
}

function batchSubmit()
{
	if (!confirm("手工批量核销仅限于核销昨日自动核销时点至此时点尚未核销单证，您确信要手动核销?"))
	{
		return false;
	}
	
	fm.all("AuditKind").value="BATCH";
	  var i = 0;
	  var showStr="正在核销数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	//if( fm.chkPrt.checked == true ) {
	  //  var urlStr = "CertifyListPrint.jsp";
	  //  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:" + 
	  //    oClientCaps.availWidth + "px;dialogHeight:" + oClientCaps.availHeight + "px");
	  //} else {
	  content="操作成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  //}
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifyCancelInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
		parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";  
}

function afterCodeSelect(codeName,Field)
{ 

}

	/**
	 *
	 *
	**/
function errorLogQuery()
{
	if (!isDate(fm.StartDate.value)&&fm.StartDate.value!="")
	{
		alert("录入日期格式不正确!");
		ErrorLogGrid.clearData();
		return false;
	}
	if (!isDate(fm.EndDate.value)&&fm.EndDate.value!="")
	{
		alert("录入日期格式不正确!");
		ErrorLogGrid.clearData();
		return false;
	}
	if (fm.EndDate.value!=""&&fm.StartDate.value!="")
	{
		if (compareDate(fm.StartDate.value,fm.EndDate.value)==1)
		{
			alert("录入日期不正确，起始日期大于终止日期!");
			ErrorLogGrid.clearData();
			return false;
		}
	}
	var strSQL = "select a.CertifyCode,b.CertifyName,a.StartNo,c.CodeName,a.PossessCom,a.CertifyCom, a.CertifymakeData, a.makedate, a.State "
	+ " from LZCertifyErrorLog a , LMCertifyDes b, LDCode c where 1=1 and a.certifycode = b.certifycode and c.codetype='certifyerror' and a.ErrorInfo=c.code" 
	+ getWherePart( 'a.CertifyCode','CertifyCode')
	+ getWherePart( 'a.CertifyCom','ManageCom','like')
	+ getWherePart('a.CertifymakeData','StartDate','>=') 
	+ getWherePart('a.CertifymakeData','EndDate','<=')
	+ getWherePart( 'a.StartNo','CertifyNo')
	+	getWherePart('a.ErrorInfo','ErrorInfoCode')
	;
	
	strSQL =strSQL + " order by a.CertifyCode" ;
	var arrResult = easyExecSql(strSQL);
	
	if(arrResult == null)
	{
		alert("查询结果为空!");
		ErrorLogGrid.clearData();
		return false;
	}
	turnPage.queryModal(strSQL, ErrorLogGrid);   
}


	/**
	 *
	 *
	**/
function handAudit()
{
	if (fm.ComCode.value!="86")
	{
		alert("您无权核销日志中的单证!"); 
		return false;
	}
	var handAuditCount=ErrorLogGrid.mulLineCount; //得到MulLine的行数	
	
	var chkFlag=false; //用于标记是否有事件被选中	
	
	for (i=0; i<handAuditCount; i++) //根据mulLineCount数循环
	{
		if(ErrorLogGrid.getChkNo(i)==true) //得到被选中的事件,就将chkFlag置为true
		{
			chkFlag=true;
			if (ErrorLogGrid.getRowColData(i,9)!='0')
			{
				alert("要核销的单证中有无效或已作废的单证(单证号:"+ErrorLogGrid.getRowColData(i,3)+")，不能进行核销!");
				return false;
			}
			//if (ErrorLogGrid.getRowColData(i,4)=='该单证没有下发给业务员')
			//{
			//	alert("要核销的单证中有未下发给业务员的单证(单证号:"+ErrorLogGrid.getRowColData(i,3)+")，\n"
			//	+"需发放到业务员后进行核销!");
			//	return false;
			//}
		}
	}
	if (ErrorLogGrid.mulLineCount==0)
	{
		alert("请先查询错误日志，选中后再进行核销!");
		return flase;
	}
	if (chkFlag==false)
	{
		alert("请选中要核销的单证!");
		return false;
	}
	
	if(confirm("所选单证未通过核销校验，确定要核销吗?"))
	{
		fm.all("AuditKind").value="HANDAUDIT";
		
		var i = 0;
	  var showStr="正在核销数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.submit();
	}	
}

	/**
	 *
	 *
	**/
function resetSearch()
{
	//fm.all("ManageName").value="";
	//fm.all("ManageCom").value="";
	fm.all("CertifyName").value="";
	fm.all("CertifyCode").value="";
	fm.all("StartDate").value="";
	fm.all("EndDate").value="";
	fm.all("ErrorInfoName").value="";
	fm.all("ErrorInfoCode").value="";
	ErrorLogGrid.clearData();
}

function queryList() {
	if ((fm.OtherNo.value==""||fm.OtherNo.value==null)
	    &&(fm.PayNo.value==""||fm.PayNo.value==null)
	    &&(fm.InvoiceNo.value==""||fm.InvoiceNo.value==null)) {
		alert("请输入查询条件!");
		return false;
	}
	var tType = "";
	if (fm.typeRadio[0].checked) {
		tType = " and b.incometype in ('1','2') ";
	} else if (fm.typeRadio[1].checked) {
		tType = " and b.incometype in ('3','10','13') ";
	} else {
		tType = " and b.incometype = '15' ";
	}
	var strSQL = "select b.incomeno,b.payno,a.standbyflag4,a.standbyflag4,"
	           + " case when c.stateflag='5' then '正常回销' when c.stateflag='2' then '作废' "
	           + " when c.stateflag='3' then '遗失' when c.stateflag='4' then '销毁' else '未发放' end"
	           + " From loprtmanager2 a,ljapay b,lzcard c "
	           + " where a.otherno=b.payno and a.standbyflag4>=c.startno "
	           + " and a.standbyflag4<=c.endno "
	           + " and a.othernotype='05' and a.code='35' "
	           + " and c.certifycode='"
	           + fm.CertifyCode.value +"' and c.sendoutcom='B"
	           + fm.Operator.value+"'"
	           + tType
	           + getWherePart("b.incomeno","OtherNo")
	           + getWherePart("b.PayNo","PayNo")
	           + getWherePart("a.standbyflag4","InvoiceNo");
	turnPage1.queryModal(strSQL, CertifyList);   
}

function ChangeType() {
	showDiv(ContNoID,"false");
	showDiv(EdorNoID,"false");
	showDiv(PrtNoID,"false");
	if (fm.typeRadio[0].checked) {
		showDiv(ContNoID,"true");
	} else if (fm.typeRadio[1].checked) {
		showDiv(EdorNoID,"true");
	} else if (fm.typeRadio[2].checked) {
		showDiv(PrtNoID,"true");
	}
	initCertifyList();
}