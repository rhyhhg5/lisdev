<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<script language="JavaScript">

    function RegisterDetailClick(cObj)
    {
        var ex,ey;
        ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
        ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
        divDetailInfo.style.left=ex;
        divDetailInfo.style.top =ey;
        divDetailInfo.style.display ='';
    }

    // 输入框的初始化（单记录部分）
    function initInputBox()
    {
        try
        {
	        fmSave.MaxCount.value = "";
	        fmSave.VerPeriod.value = "";
	        fmSave.CertifyCode.value = "";
	        fmSave.CertifyName.value = "";
	        fmSave.AgentGrade.value = "";
	        fmSave.AgentGradeName.value = "";
	        fmSave.CertifyClass.value = "";
	        fmSave.CertifyClassName.value = "";
        }
        catch(ex)
        {
            alert("在CardMaxInit.jsp-->initInputBox 函数中发生异常:初始化界面错误!");
        }
    }

    // 下拉框的初始化
    function initSelectBox()
    {
        try
        {
	        //fm.CertifyClass.value = "D";
	        //fm.CertifyClassName.value = "定额单证";
        }
        catch(ex)
        {
            alert("在CardMaxInit.jsp-->initSelectBox 函数中发生异常:初始化界面错误!");
        }
    }

    function initForm()
    {
        try
        {
            initInputBox();
            initSelectBox();
            initCertifyMaxGrid();
        }
        catch(re)
        {
            alert("CertifyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
        }
    }

    function initCertifyMaxGrid()
    {
        var iArray = new Array();
        try
        {
            iArray[0]=new Array();
            iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
            iArray[0][1]="30px";         			//列宽
            iArray[0][2]=10;          			//列最大值
            iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

            iArray[1]=new Array();
            iArray[1][0]="单证编码";    	//列名
            iArray[1][1]="150px";            		//列宽
            iArray[1][2]=150;            			//列最大值
            iArray[1][3]=1;
            iArray[1][21]="CertifyCode";

            iArray[2]=new Array();
            iArray[2][0]="单证名称";         			//列名
            iArray[2][1]="250px";            		//列宽
            iArray[2][2]=250;            			//列最大值
            iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
            iArray[2][21]="CertifyName";

            iArray[3]=new Array();
            iArray[3][0]="最大数量";         			//列名
            iArray[3][1]="90px";            		//列宽
            iArray[3][2]=60;            			//列最大值
            iArray[3][3]=1;
            iArray[3][21]="MaxCount";

            iArray[4]=new Array();
            iArray[4][0]="回销期";         			//列名
            iArray[4][1]="90px";            		//列宽
            iArray[4][2]=60;            			//列最大值
            iArray[4][3]=1;
            iArray[4][21]="VerPeriod";


            CertifyMaxGrid = new MulLineEnter( "fm" , "CertifyMaxGrid" );
            //这些属性必须在loadMulLine前
            CertifyMaxGrid.mulLineCount = 1;   
            CertifyMaxGrid.displayTitle = 1;
            CertifyMaxGrid.locked = 1;
            CertifyMaxGrid.canSel = 1;
            CertifyMaxGrid.canChk = 0;
            CertifyMaxGrid.hiddenPlus=1;
            CertifyMaxGrid.hiddenSubtraction=1;
            CertifyMaxGrid.loadMulLine(iArray);
            CertifyMaxGrid.selBoxEventFuncName = "showCertifyMaxInfo";
        }
        catch(ex)
        {
            alert(ex);
        }
    }

</script>