<%
//程序名称：CardPlanQuerySubmit.jsp
//程序功能：
//创建日期：2003-10-16 10:20:50
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.Hashtable"%>
  
<%@include file="../common/jsp/UsrCheck.jsp"%>
  
<%!
	String buildMsg(boolean bFlag, String strMsg) {
		String strReturn = "";
		
		strReturn += "<script language=\"javascript\">";
		
		if( bFlag ) {
			strReturn += "  parent.fraInterface.afterSubmit('Succ', '操作成功完成');";
		} else {
			strReturn += "  parent.fraInterface.afterSubmit('Fail','" + strMsg + "');";
		}
		strReturn += "</script>";
		
		return strReturn;
	}
%>

<html>

<%
  CardPlanUI CardPlanUI = new CardPlanUI();

	try {
		GlobalInput globalInput = (GlobalInput)session.getValue("GI");

	  LZCardPlanSchema tLZCardPlanSchema = new LZCardPlanSchema();
	
	  tLZCardPlanSchema.setPlanID(request.getParameter("PlanID"));
	  tLZCardPlanSchema.setCertifyCode(request.getParameter("CertifyCode"));
	  tLZCardPlanSchema.setAppCount(request.getParameter("AppCount"));
	  tLZCardPlanSchema.setRetCom(request.getParameter("RetCom"));
	  tLZCardPlanSchema.setRelaPlan(request.getParameter("RelaPlan"));
	  tLZCardPlanSchema.setRetState(request.getParameter("RetState"));
	  tLZCardPlanSchema.setPlanState(request.getParameter("PlanState"));
	  
	  String strWhere = request.getParameter("sql_where");
	
	  // 准备传输数据 VData
	  VData tVData = new VData();
	
		tVData.addElement(tLZCardPlanSchema);
		tVData.add(globalInput);
		tVData.add(strWhere);
		
		Hashtable hashParams = new Hashtable();
		
		hashParams.put("MakeDateB", request.getParameter("MakeDateB"));
		hashParams.put("MakeDateE", request.getParameter("MakeDateE"));
		hashParams.put("CurrentPage", request.getParameter("cur_page"));
		hashParams.put("RecordNum", request.getParameter("record_num"));
		
		tVData.add( hashParams );

	
	  // 数据传输
		if (!CardPlanUI.submitData(tVData, "QUERY||MAIN")) {
			out.println( buildMsg(false, "查询失败，原因是:" + CardPlanUI.mErrors.getFirstError()));
			return;
		
		} else {
			tVData.clear();
			tVData = CardPlanUI.getResult();
			
			// 显示
			String strPageCount = (String)tVData.getObjectByObjectName("String", 0);
			SSRS ssrs = (SSRS)tVData.getObjectByObjectName("SSRS", 0);
			String strResult = ssrs.encode();
			if( strResult.equals("") ) {
				strResult = "0|0^";
			}
			
			// LZCardPlanSet mLZCardPlanSet = new LZCardPlanSet();
			// mLZCardPlanSet.set((LZCardPlanSet)tVData.getObjectByObjectName("LZCardPlanSet",0));
	    out.println("<script language=javascript>");
	    out.println("function getGridResult()");
	    out.println("{");
	    // out.println("parent.fraInterface.arrStrReturn[0]=\"0|" + String.valueOf(mLZCardPlanSet.size()) + "^" + mLZCardPlanSet.encode()+"\";");
	    out.println("parent.fraInterface.arrStrReturn[0]=\"" + strResult +"\";");
	    out.println("parent.fraInterface.arrStrReturn[1]=\"" + strPageCount + "\";");
	    out.println("}");
	    out.println("</script>");
		} // end of if
		
	} catch (Exception ex) {
		ex.printStackTrace();
		out.println( buildMsg(false, "发生异常") );
		return;
	}
	
	out.println( buildMsg(true, "查询成功") );
%>

</html>