<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：CertBalancePrint.jsp
//程序功能：
//创建日期：2003-08-19
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>

<%
  //输出参数
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;

  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
	try {
		LZCardSet tLZCardSet = new LZCardSet();

		// 设置第一个Schema的数据
	  LZCardSchema tLZCardSchema = new LZCardSchema();
		
		tLZCardSchema.setSendOutCom(request.getParameter("SendOutCom"));
		tLZCardSchema.setReceiveCom(request.getParameter("ReceiveCom"));
		tLZCardSchema.setMakeDate(request.getParameter("MakeDateB"));
		
		tLZCardSet.add( tLZCardSchema );
		
		// 设置第二个Schema的数据
		tLZCardSchema = new LZCardSchema();
		
		tLZCardSchema.setMakeDate(request.getParameter("MakeDateE"));
		
		tLZCardSet.add( tLZCardSchema );

	  // 准备传输数据 VData
	  VData vData = new VData();
	
	  vData.addElement(globalInput);
	  vData.addElement(tLZCardSet);
	  
	  // 数据传输
	  CertBalanceUI tCertBalanceUI = new CertBalanceUI();

	  if (!tCertBalanceUI.submitData(vData, "QUERY||MAIN")) {
	    Content = " 保存失败，原因是: " + tCertBalanceUI.mErrors.getFirstError();
	    FlagStr = "Fail";
%>
		  <script language="javascript">
			  alert('<%= Content %>');
			  window.opener = null;
			  window.close();
		  </script>
<%
		  return;

	  } else {
	  	Content = " 保存成功 ";
	  	FlagStr = "Succ";

		  vData = tCertBalanceUI.getResult();

			XmlExport xe = (XmlExport)vData.getObjectByObjectName("XmlExport",0);
			
			session.setAttribute("PrintStream", xe.getInputStream());
			System.out.println("put session value");
			response.sendRedirect("../f1print/GetF1Print.jsp");
	  }
	  
	} catch(Exception ex) {
		ex.printStackTrace( );
   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   	FlagStr = "Fail";
	}
%>