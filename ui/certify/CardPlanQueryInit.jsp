<%
//程序名称：CardPlanQueryInit.jsp
//程序功能：
//创建日期：2003-10-16 10:20:50
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	//添加页面控件的初始化。
%>                            

<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
  	fm.reset();
  }
  catch(ex)
  {
    alert("CardPlanQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initForm()
{
  try
  {
	  initInpBox();
	  initCardPlanGrid();
	  
	  //
	  // retrieve the extra where condition of sql statement
	  //
		try {
			fm.sql_where.value = eval("top.opener.fm.sql_where.value");
		} catch (ex) {
			fm.sql_where.value = "";
		}
		
		// set value of fm.record_num
		fm.record_num.value = turnPage.pageLineNum;
  }
  catch(re)
  {
    alert("CardPlanQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化CardPlanGrid
 ************************************************************
 */
function initCardPlanGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";			//列名
    iArray[0][1]="30px";     //列名
    iArray[0][2]=60;	        //列名
    iArray[0][3]=0; 	        //列名

    iArray[1]=new Array();
    iArray[1][0]="计划标识";		//列名
    iArray[1][1]="160px";         //宽度
    iArray[1][2]=100;         		//最大长度
    iArray[1][3]=0;         			//是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="单证编码";                            //列名
    iArray[2][1]="80px";                                //宽度
    iArray[2][2]=100;         		                //最大长度
    iArray[2][3]=2;                                     //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[2][4]="CertifyCode";              	        //是否引用代码:null||""为不引用
    iArray[2][5]="2|3";              	                //引用代码对应第几列，'|'为分割符
    iArray[2][9]="单证编码|code:CertifyCode&NOTNULL";
    iArray[2][18]=200;

    iArray[3]=new Array();
    iArray[3][0]="申请机构";	    //列名
    iArray[3][1]="80px";         //宽度
    iArray[3][2]=100;         		//最大长度
    iArray[3][3]=0;         			//是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="申请数量";    //列名
    iArray[4][1]="60px";         //宽度
    iArray[4][2]=60;         		//最大长度
    iArray[4][3]=0;         			//是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="关联计划";      //列名
    iArray[5][1]="160px";         //宽度
    iArray[5][2]=100;         		//最大长度
    iArray[5][3]=0;         			//是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="批复情况";        //列名
    iArray[6][1]="60px";         //宽度
    iArray[6][2]=60;         		//最大长度
    iArray[6][3]=0;         			//是否允许录入，0--不能，1--允许

    iArray[7]=new Array();
    iArray[7][0]="批复数量";        //列名
    iArray[7][1]="60px";         //宽度
    iArray[7][2]=60;         		//最大长度
    iArray[7][3]=0;         			//是否允许录入，0--不能，1--允许

    iArray[8]=new Array();
    iArray[8][0]="计划状态";        //列名
    iArray[8][1]="60px";         //宽度
    iArray[8][2]=60;         		//最大长度
    iArray[8][3]=0;         			//是否允许录入，0--不能，1--允许

    CardPlanGrid = new MulLineEnter("fm", "CardPlanGrid"); 

    //这些属性必须在loadMulLine前
    CardPlanGrid.mulLineCount = 0;   
    CardPlanGrid.displayTitle = 1;
    CardPlanGrid.canSel=1;
    CardPlanGrid.hiddenPlus = 1;
    CardPlanGrid.hiddenSubtraction = 1;
    CardPlanGrid.loadMulLine(iArray);  

  }
  catch(ex)
  {
    alert("初始化CardPlanGrid时出错："+ ex);
  }
}
</script>