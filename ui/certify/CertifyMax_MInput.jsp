<html>
<%
//name :CertifyMaxInput.jsp
//function :Query Certify Info and Show Info Based On ManageCom and AgentGrade
//Creator :刘岩松
//date :2003-04-18
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.certify.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="CertifyMaxInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="CertifyMax_MInit.jsp"%>
	</head>

<body  onload="initForm();" >
  <form action="./CertifyMaxSave.jsp" method=post name=fm target="fraSubmit">
	<%@include file="../common/jsp/InputButton.jsp"%>
  <Div id= "divLLReport1" style= "display: ''">

   	<table class= common>
   		<tr class= common>
   			<TD class= title>
   		 		管理机构
   			</TD>
   			<TD class= input><Input class="codeno" name= ManageCom  ReadOnly   ondblclick="return showCodeList('comcode', [this,ManageName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode', [this,ManageName],[0,1],null,null,null,1);" verify="管理机构|NOTNULL" ><Input name=ManageName class="codename" ></td>
   				<!--Input class= code name= ManageCom ondblclick="return showCodeList('Station', [this]);"
           	onkeyup="return showCodeListKey('Station', [this]);"
           	-->
   			</TD>

   			<TD class= title>
   				代理人级别
   			</TD>
   			<TD class= input><Input class="codeno" name= AgentGrade  ReadOnly   ondblclick="return showCodeList('AgentGrade', [this,AgentGradeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AgentGrade', [this,AgentGradeName],[0,1],null,null,null,1);"  ><Input name=AgentGradeName class="codename" ></td>
   				<!--Input class= code name= AgentGrade ondblclick="return showCodeList('AgentGrade', [this]);"
           	onkeyup="return showCodeListKey('AgentGrade', [this]);"-->

   			<Input type= "hidden" name= OperateType>
   			<Input type= "hidden" name= QueryType>
        <Input type= "hidden" name= UpdateType>
   		</TR>

 		</table>
       	<input class="cssButton" type= button value="查  询" onclick="queryInfo_ManageCom()">
       	<input class="cssButton" type= button value="保  存" onclick="updateInfo_ManageCom()">
    <table>
    	<tr>
        <td class= titleImg>
    			 信息列表
    		</td>
    	</tr>
    </table>

	<Div  id= "divLLReport2" style= "display: ''">
    <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1>
  				<span id="spanCertifyMaxGrid" >
  				</span>
  			</td>
  		</tr>
    </table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="showInfo1()">
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="showInfo2()">
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="showInfo3();">
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="showInfo4();">
  	</Div>
 	</div>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>