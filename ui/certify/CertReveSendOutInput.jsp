<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2004-04-14
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="CertReveSendOutInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertReveSendOutInit.jsp"%>
</head>
<body  onload="initForm()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./CertReveSendOutSave.jsp" method=post name=fm target="fraSubmit">
    <table class="common">
    	<tr class="common">
    		<td class="input">
    		  <input class="cssButton" type="button" value="发放回退" onclick="submitForm()" >
    			<input name="chkPrt" type="checkbox" style="display:none"><!--打印清单--></td>
    			
    		<td class="input"></td>
    	</tr>
    
    </table>

    <!-- 发放回退的信息 -->    
    <div style="width:120">
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divOtherInfo);"></td>
          <td class="titleImg">发放回退</td></tr></table></div>

    <div id="divOtherInfo">
      <table class="common">
      	<tr class="common">
          <td class="title">
          	<!--回退者类型	-->
          </td>
          <td class="input">
          	<!--
          	<div id="divReceiveType" style="display:''">
	          	<input type="radio" name="TypeRadio"  value="0" checked onclick="receiveType()">机构 
	          	&nbsp;&nbsp;
							<input type="radio" name="TypeRadio"  value="1" onclick="receiveType()">业务员
						</div>
						-->
          </td>

          <td class="title">
          </td>
          <td class="input">
          </td>
        </tr>
      	
        <tr class="common">
          <td class="title">回退者</td>
          <td class="input" nowrap=true>
          	
          	<input class="codeno" name="SendOutCom" 
	          	ondblclick="return showCodeList('receivecode', [this,SendOutName],[0,1],null,fm.ReceiveCom.value,'ComCode',1);"
	            onkeyup="return showCodeListKey('receivecode', [this,SendOutName],[0,1],null,fm.ReceiveCom.value,'ComCode',1);"
	          ><input name = "SendOutName" class=codename type="text">
          	<input type="text" class="codename" name=AgentCode style="display:'none'" readonly>
          	<input type="text" class="codename" name=GroupAgentCode style="display:'none'" readonly>
          	<input type="button" class="button" name=AgentQuery value="业务员查询" onclick="queryAgent()" style="display:'none'">
          	<input type="button" class="button" name="QueryCom" value="代理机构查询" onclick="queryCom()" style="display:none">
          </td>
          
          <td class="title">接收者</td>
          <td class="input">
          	<input class="codeno" name="ReceiveCom" 
	          	ondblclick="return showCodeList('sendoutcode', [this,ReceiveName],[0,1],null,null,null,1);"
	            onkeyup="return showCodeListKey('sendoutcode', [this,ReceiveName],[0,1],null,null,null,1);"
	          ><input name = "ReceiveName" class=codename type="text">
          </td>
         </tr>
          
        <tr class="common">
          <td class="title">经办人</td>
          <td class="input"><input class="common" name="Handler"></td>

          <td class="title">经办日期</td>
          <td class="input"><input class="coolDatePicker" dateFormat="short" name="HandleDate"></td></tr>

        <tr class="common">
          <td class="title">操作员</td>
          <td class="input"><input class="readonly" readonly name="Operator"></td>
          
          <td class="title">当前时间</td>
          <td class="input"><input class="readonly" readonly name="curTime"></td></tr>

        <tr class="common">
          <td class="title">回收清算单号</td>
          <td class="input" cols=3><input class="readonly" readonly name="TakeBackNo"></td></tr>
          
      </table>
    </div>
    
    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>单证列表</td></tr>
    </table>
	<Div  id= "divCertifyList" style= "display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanCertifyList" ></span></td></tr>
	  </table>
	</div>
    </br>
    <table>
        <tr>
            <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyStoreList);"></td>
            <td class= titleImg>可发放回退单证列表：</td>
        </tr>
    </table>
    <Div  id= "divCertifyStoreList" style= "display: ''">
        <table  class= common>
            <tr  class= common>
                <td text-align: left colSpan=1><span id="spanCertifyStoreList" ></span></td>
            </tr>
        </table>
        <div id= "divPage" align=center style= "display: '' ">
            <input value="首 页" class="cssButton" type=button onclick="turnPage.firstPage();">
            <input value="上一页" class="cssButton" type=button onclick="turnPage.previousPage();">
            <input value="下一页" class="cssButton" type=button onclick="turnPage.nextPage();">
            <input value="尾 页" class="cssButton" type=button onclick="turnPage.lastPage();">
        </div>
    </div>
	<input type='hidden' name='ManageCom'  value='<%=strComCode%>'>
	<input type='hidden' name='ReceiveType'  value='COM'>
	<input type='hidden' name='ReceivePage'  value='NORMAL'>
	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
   
  </form>
</body>
</html>