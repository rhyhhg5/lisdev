<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;
  String szFailSet = "";

  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );

  //校验处理
  //内容待填充
  String auditType = request.getParameter("AuditKind");
  if (auditType.equals("SINGLE")) //人工单证处理
  {
    try
    {
      // 单证信息部分
      int certifyLength = Integer.parseInt(request.getParameter("certifyLength"));//单证流水号长度
      String szCertifyCode[] = request.getParameterValues("CertifyList2");
      String szStartNo[] = request.getParameterValues("CertifyList4");
      String szEndNo[] = request.getParameterValues("CertifyList5");
      String szSumCount[] = request.getParameterValues("CertifyList6");
      String szStateFlag[] = request.getParameterValues("CertifyList7");

      if( szCertifyCode == null ) {
        throw new Exception("没有输入要回收的起始单证号和终止单证号");
      }
      
      if("3".equals(szStateFlag[0].trim()))
      {
        LZCardSet tLZCardset = new LZCardSet();
        LZCardSchema tLZCardschema = new LZCardSchema();
        for(int nIndex = 0; nIndex < szCertifyCode.length; nIndex ++ ) {
          tLZCardschema.setCertifyCode(szCertifyCode[nIndex]);
          tLZCardschema.setStartNo(szStartNo[nIndex].trim());
          tLZCardschema.setEndNo(szEndNo[nIndex].trim());
          tLZCardschema.setSumCount(szSumCount[nIndex].trim());
          tLZCardschema.setSumCount(szSumCount[nIndex].trim());
          tLZCardschema.setState(szStateFlag[nIndex].trim());
          tLZCardset.add(tLZCardschema);
        }
        
        // 准备传输数据 VData
        VData vData = new VData();
        vData.addElement(globalInput);
        vData.addElement(tLZCardset);
        
        // 数据传输
        CardResendBL tCardResendBL = new CardResendBL();
        
        if (!tCardResendBL.submitData(vData, "TAKEBACK")) {
          Content = " 保存失败，原因是: " + tCardResendBL.mErrors.getFirstError();
          FlagStr = "Fail";
        } else {
          Content = " 保存成功 ";
          FlagStr = "Succ";
        }
      }
      else
      {
        LZCardSet setLZCard = new LZCardSet();
        int intStartNo = Integer.parseInt(szStartNo[0].trim());
        int intEndNo = Integer.parseInt(szEndNo[0].trim());
        for(int nIndex = intStartNo; nIndex <= intEndNo; nIndex ++ )
        {
          String strCurSerNo = String.valueOf(nIndex);
          strCurSerNo = PubFun.LCh(strCurSerNo, "0", certifyLength);
          LZCardSchema schemaLZCard = new LZCardSchema();
          schemaLZCard.setCertifyCode(szCertifyCode[0]);
          schemaLZCard.setStartNo(strCurSerNo);
          schemaLZCard.setEndNo(strCurSerNo);
          schemaLZCard.setStateFlag(szStateFlag[0].trim());
          schemaLZCard.setState(szStateFlag[0].trim());
          if (globalInput.ComCode.length()==2)
          {
            schemaLZCard.setSendOutCom("00");
            schemaLZCard.setReceiveCom("A"+globalInput.ComCode);
          }
          else if(globalInput.ComCode.length()==4)
          {
            schemaLZCard.setSendOutCom("A"+globalInput.ComCode.substring(0,2));
            schemaLZCard.setReceiveCom("A"+globalInput.ComCode);
          }
          else
          {
            schemaLZCard.setSendOutCom("A"+globalInput.ComCode.substring(0,4));
            schemaLZCard.setReceiveCom("B"+globalInput.Operator);
          }
          schemaLZCard.setSumCount(1);
          schemaLZCard.setHandler(globalInput.Operator);
          schemaLZCard.setHandleDate(PubFun.getCurrentDate());
          System.out.println("in save() -> ReceiveCom: "+schemaLZCard.getReceiveCom());
          System.out.println("in save() -> SendOutCom: "+schemaLZCard.getSendOutCom());
          setLZCard.add(schemaLZCard);
        }
        // 准备传输数据 VData
        VData vData = new VData();
        vData.addElement(globalInput);
        vData.addElement(setLZCard);
        vData.addElement(auditType);
        
        Hashtable hashParams = new Hashtable();
        hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CARD);
        vData.addElement(hashParams);
        
        // 数据传输
        AuditCancelBL tCertTakeBackUI = new AuditCancelBL();
        
        if (!tCertTakeBackUI.submitData(vData, "INSERT")) {
          Content = " 保存失败，原因是: " + tCertTakeBackUI.mErrors.getFirstError();
          FlagStr = "Fail";
        }
        else
        {
          Content = " 保存成功 ";
          FlagStr = "Succ";
          
          vData = tCertTakeBackUI.getResult();
          String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
          session.setAttribute("TakeBackNo", strTakeBackNo);
          session.setAttribute("State", CertStatBL.PRT_STATE);
        }
      }
    }
    catch(Exception ex)
    {
      ex.printStackTrace( );
      Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
      FlagStr = "Fail";
    }
  }	
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  </script>
<body>
</body>
</html>

