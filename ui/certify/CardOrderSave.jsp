<%
//程序名称：CardOrderSave.jsp
//程序功能：
//创建日期：2006-08-01
//创建人  ：Zhang Bin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.certify.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  LZOrderSchema mLZOrderSchema = new LZOrderSchema();
  //LZOrderCardSchema mLZOrderCardSchema = new LZOrderCardSchema();
  
  LZOrderSet mLZOrderSet = new LZOrderSet();
  LZOrderDetailSet mLZOrderDetailSet = new LZOrderDetailSet();
  
  CardOrderUI mCardOrderUI = new CardOrderUI();
  
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String mDescType = "";//将操作标志的英文转换成汉字的形式
  
  System.out.println("开始进行获取数据的操作！！！");
  
  String serialno 	= request.getParameter("SerialNo");
  String comCode 		= request.getParameter("ComCode");
  String descPerson = request.getParameter("DescPerson");
  String orderDate 	= request.getParameter("OrderDate");
  
  
	System.out.println("in jsp0 ->before submitData..........");  
	
	mLZOrderSchema.setSerialNo(serialno);  
	mLZOrderSchema.setDescCom(comCode);

  String[] strNumber 				= request.getParameterValues("CertifyTypeGridNo");
  String[] strCertifyCode 	= request.getParameterValues("CertifyTypeGrid1");
  String[] strCertifyName 	= request.getParameterValues("CertifyTypeGrid2");
  String[] strCertifyPrice 	= request.getParameterValues("CertifyTypeGrid3");
  String[] strCertifyAmn 		= request.getParameterValues("CertifyTypeGrid4");
  String[] strState 				= request.getParameterValues("CertifyTypeGrid5");

  int tLength = strNumber.length;
 
	System.out.println("in jsp1 ->before submitData..........");    
	
  if(strNumber!=null)
  {
    for(int i = 0 ;i < tLength ;i++)
    {
    	if(strCertifyAmn[i]==null||strCertifyAmn[i].equals("")||strCertifyAmn[i].equals("0"))
    	{
    		continue;
    	}
    	else
    	{
	    	LZOrderDetailSchema tLZOrderDetailSchema = new LZOrderDetailSchema();
	    	
	    	tLZOrderDetailSchema.setSerialNo(serialno);
	    	tLZOrderDetailSchema.setCertifyCode(strCertifyCode[i]);
	    	tLZOrderDetailSchema.setOrderCom(comCode);
	    	tLZOrderDetailSchema.setAppendTime(0);
	    	tLZOrderDetailSchema.setAmnt(strCertifyAmn[i]);
	    	tLZOrderDetailSchema.setOrderPerson(descPerson);
	    	
	    	if (strState[i]==null||strState[i].equals(""))
	    	{
	    		tLZOrderDetailSchema.setOrderState("1");
	    	}
	    	else
	    	{
	    		tLZOrderDetailSchema.setOrderState(strState[i]);
	    	}
	    	
      	mLZOrderDetailSet.add(tLZOrderDetailSchema);
    	}
    }
  }

  if(mOperateType.equals("INSERT"))
  {
    mDescType = "订单";
  }
  if(mOperateType.equals("UPDATE"))
  {
    mDescType = "修改订单";
  }
  if(mOperateType.equals("DELETE"))
  {
    mDescType = "删除订单";
  }
  if(mOperateType.equals("QUERY"))
  {
    mDescType = "查询订单";
  }

  VData tVData = new VData();
  try
  {
  	tVData.addElement(globalInput);
  	tVData.addElement(mLZOrderSchema);
    tVData.addElement(mLZOrderDetailSet);
    mCardOrderUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mCardOrderUI.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+"成功，"+" 批次号："+mCardOrderUI.getResult();
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+"失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mCardOrderUI.getResult()%>");
</script>
</html>