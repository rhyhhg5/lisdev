<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：核销管理
//程序功能：
//创建日期：2002-10-08
//创建人  ：Javabean
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="CertifyCancelInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CertifyCancelInit.jsp"%>
</head>
<body  onload="initForm()" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./CertifyCancelSave.jsp" method=post name="fm" target="fraSubmit">
    <div id="divHidden" style="display:'none'">
  	<div style="width:120" ><!-- this div is used to change output effect. zhouping 2002-08-07 -->
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivButton);"></td>
          <td class="titleImg">手工批量核销</td></tr>
    	</table>
    </div>

  	<div id="DivButton" style="display:''">
	    <table class="common">
	    	<tr class="common">
          <td class="title">
          	<input class="cssButton" type="button" value="批量核销" onclick="batchSubmit()" >
          </td>
          <td class="input"></td>
          <td class="title"></td>
          <td class="input"></td>
          <td class="title"></td>
          <td class="input"></td>
        </tr>  
	    	
	    </table>
		</div>
	
	<br> <hr> <br>
	
	<div style="width:120"><!-- this div is used to change output effect. zhouping 2002-08-07 -->
      <table class="common">
        <tr class="common">
          <td class="common">
          	<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divErrorLogInfo);">
          </td>
          <td class="titleImg">
          	核销错误处理
          </td>
        </tr>
     	</table>
	</div>
	<div id="divErrorLogInfo">
		<strong>查询条件</strong>
		<table class="common">
        <tr class="common">
          <td class="title">管理机构</td>
          <td class="input">
          	<Input class="codeno" name= ManageCom  ReadOnly   ondblclick="return showCodeList('comcode', [this,ManageName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode', [this,ManageName],[0,1],null,null,null,1);"><Input name=ManageName class="codename" ></td>
          <TD class= title>
           单证类型
         	</TD>
         <TD class= input>
          <input class="codeno" name="CertifyCode" ReadOnly	ondblclick="return showCodeList('CertifyCode', [this,CertifyName],[0,1],null,null,null,1,300);" onkeyup="return showCodeListKey('CertifyCode', [this,CertifyName],[0,1],null,null,null,1,300);"><input name = "CertifyName" class="codename">
         </TD>
         <td class="title">单证号</td>
         <td class="input"><input class="common" name="CertifyNo" </td>
        </tr>
          <!-- 用"总保额"来表示"最大金额" -->
          
        <tr class="common">
          <td class="title">业务处理<br>起始时间</td>
          <td class="input">
          	<input class="coolDatePicker" dateFormat="short" name="StartDate">
          </td>

          <td class="title">业务处理<br>截止时间</td>
          <td class="input">
          	<input class="coolDatePicker" dateFormat="short" name="EndDate">
          </td>
          <td class="title">错误信息</td>
          <td class="input">
          	<Input 
          	class="codeno" name= "ErrorInfoCode" readOnly onClick="return showCodeList('certifyerrortype',[this,ErrorInfoName],[0,1],null,null,null,1,200);" onkeyup="return showCodeListKey('certifyerrortype',[this,ErrorInfoName],[0,1],null,null,null,1,200);" ><Input class= codename name="ErrorInfoName" ></td>
       </tr>
   </table>

		<br>
		<input class="cssButton" type="button" value="查&nbsp;&nbsp;&nbsp;&nbsp;询" onclick="errorLogQuery()" >
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input class="cssButton" type="button" value="重&nbsp;&nbsp;&nbsp;&nbsp;置" onclick="resetSearch()" >
		
		<Div  id= "divErrorLog" style= "display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanErrorLogGrid" ></span></td></tr>
	  	</table>
		</div>
		<Div id= "divPage" align=center style= "display: '' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  	</Div>
		
		<input class="cssButton" type="button" value="人工核销" onclick="handAudit()" >
		
	</div>
	<br><hr><br>
	<div id="divCertifyInfo" style='display:none'>
    <div style="width:120" ><!-- this div is used to change output effect. zhouping 2002-08-07 -->
      <table class="common">
        <tr class="common">
          <td class="common"><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divOtherInfo);"></td>
          <td class="titleImg">单证信息</td></tr>
    	</table>
    </div>

    <div id="divOtherInfo" >
      <table class="common">
        <tr class="common">
          <td class="title">有效单证数量</td>
          <td class="input"><input class="readonly" name="AvailNum"></td>
          
          <td class="title">单证号码长度</td>
          <td class="input"><input class="readonly" name="NOLen"></td>
          <!-- 用“总保额”来表示“最大金额” -->
          
          <td class="title"></td>
          <td class="input"><input class="readonly" name="Handler" ></td>
        </tr>  
        <tr class="common">
          

          <td class="title">管理机构</td>
          <td class="input"><Input class="readonly" readOnly name= "ComCode"></td>
          	
          <td class="title">操作员</td>
          <td class="input"><input class="readonly" readonly name="Operator"></td>
          
          <td class="title">当前日期</td>
          <td class="input"><input class="readonly" readonly name="CurrentDate"></td>
       </tr>

      </table>
    </div>
   </div>
  </div>
    
    <!-- 单证列表 -->
    <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyList);"></td>
    	<td class= titleImg>特殊单证处理</td></tr>
    </table>
    
	<Div  id= "divCertifyList" style= "display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1><span id="spanCertifyList" ></span>
         	</td>
       	</tr>
	  	</table>
	  	<br>
	  <input class="cssButton" type="button" value="人工处理" onclick="submitForm()" >
	</div>
	
	<br>
	
	<table>
   	  <tr>
        <td class=common>
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertifyStroe);">
        </td>
    	<td class= titleImg>现有单证信息</td></tr>
    </table>
		
		<div id="divShowStore" style="display:''">
			<div id="divCertifyStroe">
	      <table class="common">
	        <tr class="common">
	          <td text-align: left colSpan=1><span id="spanCertifyStoreList"></span></td></tr>
		  	</table>
			</div>
			<Div id= "divPage" align=center style= "display:''">
			  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button 	onclick="turnPage1.firstPage();"> 
			  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
			  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
			  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button 	onclick="turnPage1.lastPage();">
	  	</Div>
  	</div>
		
		<table class="common">
	    <tr class="common">
	    	<td class="title"></td><td class="input"></td><td class="title"></td>
	      <td class="input"></td>
	      <td class="title">单证总数量</td>
	      <td class="input"><input class="common" name="certifysumcount"></td>
	      <td class="title"></td>
	      <td class="input"></td>
	    </tr>
	  </table>
	
	
	
	<input type="hidden" name="AuditKind" value="">
	<input type="hidden" name="StrComCode" value="<%=strComCode%>">
	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
   
  </form>
</body>
</html>
