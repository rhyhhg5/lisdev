<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CardStatisticCancel.jsp
//程序功能：
//创建日期：2007-10-25
//创建人  ：张建宝
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.ArrayList"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>

<%		
    System.out.println("in to CardStatisticCancel.jsp");	
    boolean operFlag = true;
		String FlagStr = "";
		String Content = "";
		String[] title = {"单证编码", "单证名称", "领用部门", "领用人", "领用日期", "领用数量", 
			"已领用天数", "超期天数", "回销数量", "未回销数"};
		XmlExport xmlExport = null;   
		GlobalInput tG = (GlobalInput)session.getValue("GI");

		String sql = request.getParameter("querySql");
		//System.out.println(request.getParameter("querySql"));
		
		ArrayList arrayList = new ArrayList();
		arrayList.add("ManageCode");
		arrayList.add(request.getParameter("companyCode"));
		arrayList.add("startDate");
		arrayList.add(request.getParameter("startDate"));
		arrayList.add("endDate");
		arrayList.add(request.getParameter("endDate"));
		
    TransferData tTransferData= new TransferData();
		tTransferData.setNameAndValue("sql", sql);	//查询的 SQL 
		tTransferData.setNameAndValue("vtsName", "CardCancel.vts");//模板名
		tTransferData.setNameAndValue("printerName", "printer");//打印机名
		tTransferData.setNameAndValue("title", title);//表头
		tTransferData.setNameAndValue("tableName", "BB");//表名
		tTransferData.setNameAndValue("arrayList", arrayList);
		
		VData tVData = new VData();
    tVData.addElement(tG);
		tVData.addElement(tTransferData);
    
    PrintList printList = new PrintList(); 
    if(!printList.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = printList.mErrors.getError(0).errorMessage;
    }
    else
    {    
			VData result = printList.getResult();			
	  	xmlExport=(XmlExport)result.getObjectByObjectName("XmlExport",0);

	  	if(xmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
		}
		//System.out.println(operFlag);
		if (operFlag==true)
		{
  		ExeSQL exeSQL = new ExeSQL();
      //获取临时文件名
      String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
      String strFilePath = exeSQL.getOneValue(strSql);
      String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
      //获取存放临时文件的路径
      //strSql = "select SysVarValue from ldsysvar where Sysvar='VTSRealPath'";
      //String strRealPath = exeSQL.getOneValue(strSql);
      String strRealPath = application.getRealPath("/").replace('\\','/');
      String strVFPathName = strRealPath + "//" +strVFFileName;
      
      CombineVts tcombineVts = null;	
      
      String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
    	tcombineVts = new CombineVts(xmlExport.getInputStream(),strTemplatePath);
    
    	ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    	tcombineVts.output(dataStream);
        	
    	//把dataStream存储到磁盘文件
    	System.out.println("存储文件到"+strVFPathName);
    	AccessVtsFile.saveToFile(dataStream,strVFPathName);
	    System.out.println("==> Write VTS file to disk ");
            
			response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=31&RealPath="+strVFPathName);
		}
		else
		{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>