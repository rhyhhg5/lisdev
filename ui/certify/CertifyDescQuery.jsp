<html>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.bank.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="CertifyDescQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CertifyDescQueryInit.jsp"%>
</head>

<body onload="initForm();" >
  <form action="./CertifyDescQuerySave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 显示或隐藏LLReport1的信息 -->

    <div id="divLLReport1" style= "display: ''">
      <table class=common>
        <tr class=common>
          <td class="title">单证编码</td>
          <td class="input"><input class="codeno" name="CertifyCode" 
            ondblclick="return showCodeList('certifycodedes', [this,CertifyCodeName,SubCode],[0,1,2],null,null,null,1,350);" 
            ><input class=codename name="CertifyCodeName" readonly></td>
          <td class=title>单证类型码</td>
          <td class=input><input class="common" dateFormat="short" name=SubCode ></td>
        </tr>
        <tr class=common>
          <td class=title>单证类型</td>
          <td class=input><input class=code name=CertifyClassName verify="单证类型|NOTNULL"
            CodeData="0|^P|普通单证^D|定额单证"
            ondblClick="showCodeListEx('CertifyClassListNew',[this,CertifyClass],[1,0],null,null,null,1);">
          </td>
          <td class=title>单证名称</td>
          <td class=input><input class=common name=CertifyName ></td>
        </tr>
        <tr class=common>
          <td class=title>单证内部编码</td>
          <td class=input><input class=common name=InnerCertifyCode ></td>
          <td class=title>单证号码长度</td>
          <td class=input><input class=common name=CertifyLength ></td>
        </tr>
        <tr class=common>
          <td class=title>险种编码</td>
          <td class=input><input class=common name=RiskCode ></td>
          <td class=title>状态</td>
          <td class=input><input class=code name=StateName verify="状态|NOTNULL"
            CodeData="0|^0|有效单证^1|无效单证"
            ondblClick="showCodeListEx('CertifyStateList',[this,State],[1,0],null,null,null,1);">
          </td>
        </tr>
        <tr class=common>
          <td class=title>重要级别</td>
          <td class=input><input class=common name=ImportantLevel ></td>
          <td class=title>是否是有号单证</td>
          <td class=input><input class=code name=HaveNumberName verify="单证类型|NOTNULL"
            CodeData="0|^Y|有号单证^N|无号单证"
            ondblClick="showCodeListEx('CertifyHaveNumber',[this,HaveNumber],[1,0],null,null,null,1);">
          </td>
        </tr>
        <tr class=common>
          <td class=title>总保费</td>
          <td class=input><input class=common name=Prem ></td>
          <td class=title>总保额</td>
          <td class=input><input class=common name=Amnt ></td>
        </tr>
        <tr class=common>
          <td class=title>管理机构</td>
          <td class=input><input class="code" name=ManageComName
            ondblclick="return showCodeList('Station', [this,ManageCom],[1,0],null,null,null,1);"
            verify="管理机构|NOTNULL">
          </td>
        </tr>
      </table>

      <input class=common name="CertifyClass" type="hidden">
      <input class=common name="State" type="hidden">
      <input class=common name="HaveNumber" type="hidden">
      <input class=common name="OperateType" type="hidden" >
      <input class=common name="ManageCom" type="hidden">
      <input class="cssButton" type=button value="查  询" onclick="submitForm()">
      <input class="cssButton" type=button value="返  回" onclick="ReturnData()">

      <table>
        <tr>
          <td class=common>
            <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
          </td>
          <td class=titleImg>清单列表</td>
        </tr>
      </table>
      <div id="divLLReport2" style= "display: ''">
        <table class=common>
          <tr class=common>
            <td text-align: left colSpan=1>
              <span id="spanCertifyDescGrid" >
              </span>
            </td>
          </tr>
        </table>
        <div id="divPage" align=center style= "display: 'none' ">
          <input class=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
          <input class=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
          <input class=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
          <input class=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
        </div>
      </div>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
  </body>
</html>
