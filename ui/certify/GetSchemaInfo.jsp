<%@page contentType="text/html;charset=GBK" %>

<%@page import="java.lang.*"%>
<%@page import="java.lang.reflect.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.utility.*"%>

<%
	// 用这个jsp文件来简化代码的编写
	String strTableName = request.getParameter("TableName");
	String strFormat = request.getParameter("FormatString");
	
	if( strTableName == null ) {
		strTableName = "";
	}

	if( strFormat == null ) {
		strFormat = "";
	}
%>

<html> 
<head >
</head>
<body>
  <form action="GetSchemaInfo.jsp" method="post">
  	表名：<input type="text" name="TableName" value="<%= strTableName %>">
  	<br>
  	模式：<input type="text" name="FormatString" value="<%= strFormat %>">%将被字段名所替换
  	<br>
  	<input type="submit" value="提交">
  </form>
<%
	if( !strTableName.equals("") ) {
		strTableName = "com.sinosoft.lis.schema." + strTableName + "Schema";
		
	  try {
	    Field fields[] = Class.forName(strTableName).getDeclaredFields();
	    for(int nIndex = 0; nIndex < fields.length; nIndex ++) {
	    	if( !fields[nIndex].getName().equals("FIELDNUM")
	    		&& !fields[nIndex].getName().equals("PK")
	    		&& !fields[nIndex].getName().equals("fDate")
	    		&& !fields[nIndex].getName().equals("mErrors") ) {

					String strOutput = strFormat;
					
					if( strOutput.equals("") ) {
						strOutput = fields[nIndex].getName();
					} else {
						strOutput = strFormat.replaceAll("[%]", fields[nIndex].getName());
					}
					
		    	out.println(fields[nIndex].getName());
		    	out.println("<p>");
		    }
	    }
	  } catch (Exception ex) {
	  	ex.printStackTrace();
	  }
	}
%>  
</body>
</html>
