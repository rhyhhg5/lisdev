<%
	// 防止IE缓存页面
	// response.setHeader("Pragma","No-cache"); 
	// response.setHeader("Cache-Control","no-cache"); 
	// response.setDateHeader("Expires", 0); 
%>

<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：单证批量发放的日志信息
//创建日期：2003-06-11
//创建人  ：周平
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="CertBatchLogInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CertBatchLogInit.jsp"%>
</head>
<body onload="initForm()">

	<form action="" method="post" name="fm">
	  <input type="hidden" name="hideTakeBackNo">
	</form>
	
	<input value="查询" type="button" onclick="easyQueryClick();">
	<input value="关闭" type="button" onclick="window.close();">

	<form action="" method=post name="fmsave" target="fraSubmit">
	  <table>
	 	  <tr>
	      <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCertLog);"></td>
	  	<td class= titleImg>单证批量发放错误日志</td></tr>
	  </table>
	
		<div id="divCertLog">
	    <table class="common">
	      <tr class="common">
	        <td text-align: left colSpan=1><span id="spanCertLog"></span></td></tr>
	  	</table>
		</div>

	</form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
  <input value="首　页" type="button" onclick="getFirstPage();">
  <input value="上一页" type="button" onclick="getPreviousPage();">
  <input value="下一页" type="button" onclick="getNextPage();">
  <input value="尾　页" type="button" onclick="getLastPage();">
  
</body>
</html>
