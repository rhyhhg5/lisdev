<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupPolInput.jsp
//程序功能：
//创建日期：2002-08-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.certify.ECCardActiveUI"%>
  
<%
	//输出参数               
	String FlagStr="";
	String Content = "";
	GlobalInput globalInput = new GlobalInput( );
  	globalInput.setSchema((GlobalInput)session.getValue("GI"));
  	TransferData mTransferData = new TransferData();
  
  //校验处理
  //内容待填充
	try {
	  	String cardNum = request.getParameter("cardNum");
		String cardPassword = request.getParameter("cardPassword");
		String cvalidate = request.getParameter("cvalidate");
		String mult = request.getParameter("mult");
		
		//投保人
		String appName = request.getParameter("appName");
		String appIdType = request.getParameter("appIdType");
		String appSex = request.getParameter("appSex");
		String appIdNo = request.getParameter("appIdNo");
		String appBirthday = request.getParameter("appBirthday");
		//被保人
		String[] insuNumber=request.getParameterValues("CertifyMaxGridNo");
	  	String[] insuName = request.getParameterValues("CertifyMaxGrid1");
     	String[] insuSex = request.getParameterValues("CertifyMaxGrid2");
      	String[] insuIdtype=request.getParameterValues("CertifyMaxGrid3");
      	String[] insuIdno=request.getParameterValues("CertifyMaxGrid4");
      	String[] insuBirth=request.getParameterValues("CertifyMaxGrid5");
      	//String[] insuAge=request.getParameterValues("CertifyMaxGrid6");
      	//String[] insuCopy=request.getParameterValues("CertifyMaxGrid7");
      	String[] insuOccupation=request.getParameterValues("CertifyMaxGrid8");
	  // 准备传输数据 VData
	  	VData vData = new VData();
	  	
		mTransferData.setNameAndValue("cardNum",cardNum);
		mTransferData.setNameAndValue("cardPassword",cardPassword);
		mTransferData.setNameAndValue("cvalidate",cvalidate);
		mTransferData.setNameAndValue("mult",mult);
		
		mTransferData.setNameAndValue("appName",appName);
		mTransferData.setNameAndValue("appIdType",appIdType);
		mTransferData.setNameAndValue("appSex",appSex);
		mTransferData.setNameAndValue("appIdNo",appIdNo);
		mTransferData.setNameAndValue("appBirthday",appBirthday);
		
		mTransferData.setNameAndValue("insuNumber",insuNumber);
		mTransferData.setNameAndValue("insuName",insuName);
		mTransferData.setNameAndValue("insuSex",insuSex);
		mTransferData.setNameAndValue("insuIdtype",insuIdtype);
		mTransferData.setNameAndValue("insuIdno",insuIdno);
		mTransferData.setNameAndValue("insuBirth",insuBirth);
		//mTransferData.setNameAndValue("insuAge",insuAge);
		//mTransferData.setNameAndValue("insuCopy",insuCopy);
		mTransferData.setNameAndValue("insuOccupation",insuOccupation);
	  	vData.add(mTransferData);
	  
	  // 数据传输
	  ECCardActiveUI tECCardActiveUI = new ECCardActiveUI();

	  if (!tECCardActiveUI.submitData(vData,null)) {
	    Content = tECCardActiveUI.mErrors.getErrContent();
	  	FlagStr = "Fail";
	  } else {
	  	Content = " 保存成功 ";
	  	FlagStr = "Succ";
	  }
	  
	} catch(Exception ex) {
		ex.printStackTrace( );
   		Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
   		FlagStr = "Fail";
	}
	
	Content = PubFun.changForHTML(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

