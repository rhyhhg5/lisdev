<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：CertifyQueryOut.jsp
//程序功能：
//创建日期：2003-10-23
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
  String FlagStr = "Fail";
  String Content = "";
  String strResult = "";
  String strSum = "";
  String Message="";
  
  try {
	    String strCertifyCode = request.getParameter("CertifyCode");
        String strPlanID=request.getParameter("PlanID");
		
		
		LZCardPlanDB tLZCardPlanDB = new LZCardPlanDB();
        tLZCardPlanDB.setPlanID(strPlanID);
        tLZCardPlanDB.getInfo();
        LZCardPlanSchema tLZCardPlanSchema = new LZCardPlanSchema();
        tLZCardPlanSchema.setSchema(tLZCardPlanDB);
        LZCardPlanSet tLZCardPlanSet = new LZCardPlanSet();
	    GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	    
	  // 准备传输数据 VData
	  VData tVData = new VData();
	
		tVData.add( globalInput );
		tVData.add( tLZCardPlanSchema );
		tVData.add( tLZCardPlanSet );
		tVData.add( new Hashtable() );
	  
	  
	  // 数据传输
	  CardPlanUI tCardPlanUI = new CardPlanUI();
	  System.out.println("ui start..");
	  if( !tCardPlanUI.submitData(tVData, "QUERY||BUGET") ) {
	    if( tCardPlanUI.mErrors.needDealError() ) {
	    	throw new Exception( tCardPlanUI.mErrors.getFirstError() );
	    } else {
	    	throw new Exception("CardPlanUI查询失败，但是没有提供详细的信息");
	    }
	  } else {
	     tVData.clear();
	     tVData = tCardPlanUI.getResult();
	     Message = (String) tVData.getObjectByObjectName("String", 0);
	     FlagStr = "Succ";
	     Content=Message+"\n本操作成功！";
      }
     } catch (Exception ex) {
	  ex.printStackTrace();
	  FlagStr = "Fail";
	  Content = "保存失败，原因是:" + ex.getMessage();
	}
%>


<html>
<%=PubFun.changForHTML(Content)%>;
<body>
</body>
</html>


