var showInfo;

var turnPage = new turnPageClass(); 

window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true && CertifyTypeGrid.checkValue("CertifyTypeGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./OrderDescSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	if (fm.OrderCreator.value==""||fm.OrderCreator.value==null)
	{
		alert("制定人不能为空！");
		return false;
	}
	
	if (fm.AttachDate.value==""||fm.AttachDate.value==null)
	{
		alert("归属日期不能为空！");
		return false;
	}
	if (fm.AttachDateBak.value!=""&&fm.AttachDateBak.value!=null)
	{
		if (fm.AttachDate.value!=fm.AttachDateBak.value)
		{
			if (!confirm("你修改了归属日期，征订数据将做为一个新批次被保存，请确认？"))
			{
				return false;
			}
			fm.SerialNo.value="";
		}
	}
	var rowNum=CertifyTypeGrid. mulLineCount ; //行数 
	if (rowNum==0)
	{
		alert("请录入要征订的单证类型！");
		return false;
	}
	var price;
	for(var i=0;i<rowNum;i++)
	{
		price = CertifyTypeGrid.getRowColData(i,3);
		k=i+1;
		if(price!=""&&price!=null)
		{
			if(!isNumeric(price))
			{
				alert("第"+k+"行单价录入格式有误！");
				return false;
			}
		}
	}
	
	var strSQL = "select max(AttachDate) from lzorder";
	var arrResult = easyExecSql(strSQL);
	var rslt = arrResult[0][0]+"";
	
	if (rslt==""||rslt==null)
	{
	}
	else
	{
		if (compareDate(fm.AttachDate.value,arrResult[0][0])==2)
		{
			alert("新增征订批次归属日期不能晚于以前批次的归属日期！");
			return false;
		}
	}
	
	return true;
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  fm.OperateType.value="QUERY";
  
  window.open("./FrameOrderDescQuery.jsp?Serial="+fm.SerialNo.value);
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, SerialNo, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
  	
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  resetForm();
	  fm.SerialNo.value = "";
  }
}

function updateClick()
{
	fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true && CertifyTypeGrid.checkValue("CertifyTypeGrid")) 
		{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./OrderDescSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	if (fm.SerialNo.value==""||fm.SerialNo.value==null)
	{
		alert("批次号不能为空！");
		return false;
	}
	var strSQL = "select * from LZOrder where SerialNo='"+fm.SerialNo.value+"'";
	var strResult = easyExecSql(strSQL);
	
	if (strResult == null)
	{
		alert("该批次不存在！");
		return false;
	}
	
	if (fm.SerialNoBak.value==""||fm.SerialNoBak.value==null)
	{
		alert("请先查询再修改！");
		return false;
	}
	if (fm.SerialNo.value!=fm.SerialNoBak.value)
	{
		alert("不能对批次号进行修改！");
		return false;
	}
	return true;
	
}

function deleteClick()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该批次吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true && CertifyTypeGrid.checkValue("CertifyTypeGrid")) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./OrderDescSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	if (fm.SerialNo.value==""||fm.SerialNo.value==null)
	{
		alert("批次号不能为空！");
		return false;
	}
	var strSQL = "select * from LZOrder where SerialNo='"+fm.SerialNo.value+"'";
	var strResult = easyExecSql(strSQL);
	
	if (strResult == null)
	{
		alert("该批次不存在！");
		return false;
	}
	
	if (fm.SerialNoBak.value==""||fm.SerialNoBak.value==null)
	{
		alert("请先查询再删除！");
		return false;
	}
	if (fm.SerialNo.value!=fm.SerialNoBak.value)
	{
		alert("不能对批次号进行修改！");
		return false;
	}
	return true;
	
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
  	CertifyTypeGrid.clearData();
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function requestClick()
{
  showInfo = window.open("./CertifyTypeQuery.jsp");
}