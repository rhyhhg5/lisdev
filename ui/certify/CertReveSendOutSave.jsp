<%
//程序名称：CertReveSendOutSave.jsp
//程序功能：
//创建日期：2003-04-18
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
    //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    boolean bContinue = true;
    String szFailSet = "";

    GlobalInput globalInput = new GlobalInput( );
    globalInput.setSchema( (GlobalInput)session.getValue("GI") );

    String receivePage = request.getParameter("ReceivePage"); //回退页面

    //校验处理
    //内容待填充
    if (receivePage.equals("NORMAL")) //来自回退页面
    {
		try
		{
		    // 单证信息部分
		    String szPrtNo		 = request.getParameter("PrtNo");
		    String szReceiveType = request.getParameter("ReceiveType"); //接收者类型
		    String szSendOutCom  = "";
		    if (szReceiveType.equals("AGE") || szReceiveType.equals("AGECOM"))
		    {
		    	szSendOutCom = request.getParameter("AgentCode");
		    }
			else
			{
				szSendOutCom = request.getParameter("SendOutCom");
			}
	        System.out.println("szSendOutCom: "+szSendOutCom+"    szReceiveType: "+szReceiveType);
		    String szReceiveCom		= request.getParameter("ReceiveCom");
		    String szInvaliDate 	= request.getParameter("InvalidDate");
		    String szAmnt 			= request.getParameter("Amnt");
		    String szHandler		= request.getParameter("Handler");
		    String szHandleDate		= request.getParameter("HandleDate");

		    String szNo[]			= request.getParameterValues("CertifyListNo");
		    String szCertifyCode[]  = request.getParameterValues("CertifyList1");
		    String szStartNo[]		= request.getParameterValues("CertifyList3");
		    String szEndNo[]		= request.getParameterValues("CertifyList4");
		    String szSumCount[]     = request.getParameterValues("CertifyList5");
		    int nIndex;

		    LZCardSet setLZCard = new LZCardSet( );

		    if( szNo == null ) {
		    	throw new Exception("没有输入要回收的起始单证号和终止单证号");
		    }

		    for( nIndex = 0; nIndex < szNo.length; nIndex ++ ) {
		        LZCardSchema schemaLZCard = new LZCardSchema( );

		        schemaLZCard.setCertifyCode(szCertifyCode[nIndex]);
				schemaLZCard.setSubCode("");
				schemaLZCard.setRiskCode("");
				schemaLZCard.setRiskVersion("");

		        schemaLZCard.setStartNo(szStartNo[nIndex].trim());
		        schemaLZCard.setEndNo(szEndNo[nIndex].trim());

		        if (szReceiveType.equals("COM"))
			    {
			    	schemaLZCard.setSendOutCom("A"+szSendOutCom);
			    }else if (szReceiveType.equals("USR"))
			    {
			    	schemaLZCard.setSendOutCom("B"+szSendOutCom);
			    }else if (szReceiveType.equals("AGECOM"))
			    {
			    	schemaLZCard.setSendOutCom("E"+szSendOutCom);
			    }else
			    {
			    	schemaLZCard.setSendOutCom("D"+szSendOutCom);
			    }

		        if (szReceiveCom.substring(0,2).equals("86"))
		        {
		        	schemaLZCard.setReceiveCom("A"+szReceiveCom);
		        }
		  	    else
		  	    {
		  	    	schemaLZCard.setReceiveCom("B"+szReceiveCom);
		  	    }

				schemaLZCard.setSumCount(szSumCount[nIndex].trim());
				schemaLZCard.setPrem("");
		        schemaLZCard.setAmnt(szAmnt);
		        schemaLZCard.setHandler(szHandler);
		        schemaLZCard.setHandleDate(szHandleDate);
		        schemaLZCard.setInvaliDate(szInvaliDate);

				schemaLZCard.setTakeBackNo("");
				schemaLZCard.setSaleChnl("");
				schemaLZCard.setOperateFlag("");
				schemaLZCard.setPayFlag("");
				schemaLZCard.setEnterAccFlag("");
				schemaLZCard.setReason("");
				schemaLZCard.setState("");
				schemaLZCard.setOperator("");
				schemaLZCard.setMakeDate("");
				schemaLZCard.setMakeTime("");
				schemaLZCard.setModifyDate("");
				schemaLZCard.setModifyTime("");

		        setLZCard.add(schemaLZCard);
            }

		    // 准备传输数据 VData
		    VData vData = new VData();

		    vData.addElement(globalInput);
		    vData.addElement(setLZCard);

		    Hashtable hashParams = new Hashtable();
		    hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
		    vData.addElement(hashParams);

		    // 数据传输
		    CertReveSendOutUI tCertReveSendOutUI = new CertReveSendOutUI();

		    if (!tCertReveSendOutUI.submitData(vData, "INSERT")) {
		        Content = " 保存失败，原因是: " + tCertReveSendOutUI.mErrors.getFirstError( );
		        FlagStr = "Fail";

		        vData = tCertReveSendOutUI.getResult();
		        setLZCard = (LZCardSet)vData.getObjectByObjectName("LZCardSet", 0);

			    szFailSet = "parent.fraInterface.CertifyList.clearData();\r\n";
		        for(nIndex = 0; nIndex < setLZCard.size(); nIndex ++) {
		    	    LZCardSchema tLZCardSchema = setLZCard.get(nIndex + 1);

		    	    szFailSet += "parent.fraInterface.CertifyList.addOne();\r\n";
		    	    szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
		    	      ", 1, '" + tLZCardSchema.getCertifyCode() + "');\r\n";
		    	    szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
		    	      ", 3, '" + tLZCardSchema.getStartNo() + "');\r\n";
		    	    szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
		    	      ", 4, '" + tLZCardSchema.getEndNo() + "');\r\n";
		    	    szFailSet += "parent.fraInterface.CertifyList.setRowColData(" + String.valueOf(nIndex) +
		    	      ", 5, '');\r\n";
		        }
            } else {
		  	    Content = " 保存成功 ";
		  	    FlagStr = "Succ";

			    vData = tCertReveSendOutUI.getResult();
				String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
				session.setAttribute("TakeBackNo", strTakeBackNo);
				session.setAttribute("State", CertStatBL.PRT_STATE);
		    }
		}
		catch(Exception ex)
		{
			ex.printStackTrace( );
	   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
	   	FlagStr = "Fail";
		}
	}
	else  //来自发放页面
	{
		try
		{
		    // 单证信息部分
		    //String szReceiveType	= request.getParameter("ReceiveType"); //接收者类型
		    //String szSendOutCom 	= request.getParameter("AgentCode"); //发放者
		    //String szReceiveCom		= request.getParameter("ReceiveCom");//接受者
		    String szInvaliDate 	= request.getParameter("InvalidDate");
		    String szAmnt 				= request.getParameter("Amnt");
		    String szHandler			= request.getParameter("Handler");
		    String szHandleDate		= request.getParameter("HandleDate");

		    String szNo[]					= request.getParameterValues("CertifyBackListNo");
		    String szCertifyCode[]= request.getParameterValues("CertifyBackList1");
		    String szStartNo[]		= request.getParameterValues("CertifyBackList2");
		    String szEndNo[]			= request.getParameterValues("CertifyBackList3");
		    String szSumCount[]   = request.getParameterValues("CertifyBackList4");
		    int nIndex;
		    System.out.println("ssss");

		    LZCardSet setLZCard = new LZCardSet( );

		    if( szNo == null )
		    {
		    	throw new Exception("没有选择要删除的发放！");
		    }

		    String tRadio[] = request.getParameterValues("InpCertifyBackListSel");

            System.out.println("aaaaaa: tRadio.length: "+tRadio.length);
	   	    for( nIndex = 0; nIndex < tRadio.length; nIndex ++ )
	   	    {
	   		    if(tRadio[nIndex].equals("1"))
	            {
			        LZCardSchema schemaLZCard = new LZCardSchema();

			        schemaLZCard.setCertifyCode(szCertifyCode[nIndex]);
					schemaLZCard.setSubCode("");
					schemaLZCard.setRiskCode("");
					schemaLZCard.setRiskVersion("");

			        schemaLZCard.setStartNo(szStartNo[nIndex].trim());
			        schemaLZCard.setEndNo(szEndNo[nIndex].trim());

			        LZCardSet setLZCard1 = new LZCardSet();
			        String strSQL = "select * from LZCARD where CertifyCode='"+szCertifyCode[nIndex]+"' "
				      	+ " and StartNo='"+szStartNo[nIndex].trim()+"' and EndNo='"+szEndNo[nIndex].trim()+"'"
				      	  ;
				 	LZCardDB tLZCardDB = new LZCardDB();
				 	setLZCard1 = tLZCardDB.executeQuery(strSQL);

				    if (setLZCard.size()==0)
			        {
			            Content = "没有找到该单证！";
			  	    	FlagStr = "Fail";
			        }

				    schemaLZCard.setSendOutCom(setLZCard1.get(1).getReceiveCom());
			        schemaLZCard.setReceiveCom(setLZCard1.get(1).getSendOutCom());


					schemaLZCard.setSumCount(szSumCount[nIndex].trim());
					schemaLZCard.setPrem("");
			        schemaLZCard.setAmnt(szAmnt);
			        schemaLZCard.setHandler(szHandler);
			        schemaLZCard.setHandleDate(szHandleDate);
			        schemaLZCard.setInvaliDate(szInvaliDate);

					schemaLZCard.setTakeBackNo("");
					schemaLZCard.setSaleChnl("");
					schemaLZCard.setOperateFlag("");
					schemaLZCard.setPayFlag("");
					schemaLZCard.setEnterAccFlag("");
					schemaLZCard.setReason("");
					schemaLZCard.setState("");
					schemaLZCard.setOperator("");
					schemaLZCard.setMakeDate("");
					schemaLZCard.setMakeTime("");
					schemaLZCard.setModifyDate("");
					schemaLZCard.setModifyTime("");
		    	    setLZCard.add(schemaLZCard);
                }
            }

		    // 准备传输数据 VData
		    VData vData = new VData();

		    vData.addElement(globalInput);
		    vData.addElement(setLZCard);

		    Hashtable hashParams = new Hashtable();
		    hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
		    vData.addElement(hashParams);

		    // 数据传输
		    CertReveSendOutUI tCertReveSendOutUI = new CertReveSendOutUI();

		    if (!tCertReveSendOutUI.submitData(vData, "INSERT")) {
		        Content = " 保存失败，原因是: " + tCertReveSendOutUI.mErrors.getFirstError( );
		        FlagStr = "Fail";

		        vData = tCertReveSendOutUI.getResult();
		        setLZCard = (LZCardSet)vData.getObjectByObjectName("LZCardSet", 0);

			} else {
		  	    Content = " 保存成功 ";
		  	    FlagStr = "Succ";

			    vData = tCertReveSendOutUI.getResult();
				String strTakeBackNo = (String)vData.getObjectByObjectName("String", 0);
				session.setAttribute("TakeBackNo", strTakeBackNo);
				session.setAttribute("State", CertStatBL.PRT_STATE);
	  	    }
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
	   	    Content = FlagStr + " 保存失败，原因是:" + ex.getMessage();
	   	    FlagStr = "Fail";
		}
    }
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  </script>
<body>
</body>
</html>