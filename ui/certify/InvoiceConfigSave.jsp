<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");

  //获得发票配置信息
  String ManageCom = request.getParameter("ManageCom");
  String CertifyCode = request.getParameter("CertifyCode");
  String CertifyName = request.getParameter("CertifyName");
  System.out.println("ManageCom : " + ManageCom + "  CertifyCode : " + CertifyCode + "  CertifyName : " + CertifyName);

  InvoiceConfigBL tInvoiceConfigBL = new InvoiceConfigBL();
  try
  {
    TransferData mTransferData = new TransferData();
    mTransferData.setNameAndValue("ManageCom", ManageCom);
    mTransferData.setNameAndValue("CertifyCode", CertifyCode);
    mTransferData.setNameAndValue("CertifyName", CertifyName);
    
    VData vData =  new VData();
    vData.add(tG);
    vData.add(mTransferData);

    tInvoiceConfigBL.submitData(vData, request.getParameter("Operate"));
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("after submit!");
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr.equals(""))
  {
    tError = tInvoiceConfigBL.mErrors;
    if (!tError.needDealError())
    {
      Content = "操作成功! ";
      FlagStr = "Success";
    }
    else
    {
      Content = "操作失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  </script>
</html>
