<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：CertifyQueryInit.jsp
//程序功能：
//创建日期：2002-08-15
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	
	String strManageCom = globalInput.ManageCom;
	
	String strCertifyCode = request.getParameter("CertifyCode");
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
  	fm.reset();
  	fm.cur_com.value = '<%= strManageCom %>';
  }
  catch(ex)
  {
    alert("在CardPlanInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在CardPlanInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
		initCardPlanInfo();
		initContent();
  }
  catch(re)
  {
    alert("在CardPlanInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCardPlanInfo()
{                               
	var iArray = new Array();
      
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="计划标识";         		//列名
		iArray[1][1]="125px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="申请机构";          	//列名
		iArray[2][1]="80px";            		//列宽
		iArray[2][2]=100;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="单证编码";        		//列名
		iArray[3][1]="80px";            		//列宽
		iArray[3][2]=100;            			  //列最大值
		iArray[3][3]=2;              			  //是否允许输入,1表示允许，0表示不允许
	        iArray[3][4]="CertifyCode";              	        //是否引用代码:null||""为不引用
                iArray[3][5]="3|4";              	                //引用代码对应第几列，'|'为分割符
                iArray[3][9]="单证编码|code:CertifyCode&NOTNULL";
                iArray[3][18]=200;
		
		iArray[4]=new Array();
		iArray[4][0]="申请数量";          	//列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            		 	  //列最大值
		iArray[4][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		CardPlanInfo = new MulLineEnter("fm" , "CardPlanInfo"); 

    CardPlanInfo.mulLineCount = 0;   
    CardPlanInfo.displayTitle = 1;
    CardPlanInfo.hiddenPlus = 1;
		CardPlanInfo.loadMulLine(iArray);  
 
	}
	catch(ex)
	{
		alert(ex);
	}
}

function initContent()
{
  try
  {
		var vCertifyCode = '<%= strCertifyCode %>';
		
		if( vCertifyCode == null || vCertifyCode == "" ) {
			return;
		}
	
		var vSQL = "select * from lzcardplan where retcom = '<%= strManageCom %>'"
								+ " and planstate = 'A' and certifycode = '" + vCertifyCode + "'";
		
		//using my docode function
		var myResult = myDecodeEasyQueryResult(easyQueryVer3(vSQL));
		
		if( myResult == null ) {
			return;
		}

		var vRow;
		var vAllCount = 0;

		for( vRow = 0; vRow < myResult.length && vRow < 10; vRow ++ ) {
			CardPlanInfo.addOne();
			
			CardPlanInfo.setRowColData(vRow, 1, myResult[vRow][0]);
			CardPlanInfo.setRowColData(vRow, 2, myResult[vRow][4]);
			CardPlanInfo.setRowColData(vRow, 3, myResult[vRow][1]);
			CardPlanInfo.setRowColData(vRow, 4, myResult[vRow][2]);
			
			vAllCount += parseInt(myResult[vRow][2]);
		}
		
		fm.CertifyCode.value = vCertifyCode;
		fm.AppCount.value = vAllCount;
  }
  catch(re)
  {
    alert("在CardPlanInit.jsp-->InitContent函数中发生异常:初始化界面错误!");
  }
}

</script>