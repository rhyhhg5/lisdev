<%@page import="com.sinosoft.lis.pubfun.*"%>
<script language="JavaScript">

//初始化页面
function initForm()
{
    try
    {
        initInputBox();
        initCardGrid();
        initElementtype();
    }
    catch(ex)
    {
        alert("初始化界面错误，" + ex.message);
    }
}

//初始化输入框
function initInputBox()
{
	fmSave.ManageCom.value = "";
	fmSave.ManageName.value = "";
	fmSave.CertifyCode.value = "";
	fmSave.CertifyName.value = "";
}

//初始化发票配置列表
function initCardGrid()
{
    var iArray = new Array();
        
    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=20;            			//列最大值
        iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="机构编码";         		//列名
        iArray[1][1]="80px";            		//列宽
        iArray[1][2]=100;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[1][21]="ManageCom";
        
        iArray[2]=new Array();
        iArray[2][0]="机构名称";         		//列名
        iArray[2][1]="100px";            		//列宽
        iArray[2][2]=80;            			//列最大值
        iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[2][21]="ManageName"; 
        
        iArray[3]=new Array();
        iArray[3][0]="单证类型码";         		//列名
        iArray[3][1]="80px";            		//列宽
        iArray[3][2]=80;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        iArray[3][21]="CertifyCode"; 
        
        iArray[4]=new Array();
        iArray[4][0]="单证描述";         		//列名
        iArray[4][1]="150px";            		//列宽
        iArray[4][2]=100;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        iArray[4][21]="CertifyName";
        
        CardGrid = new MulLineEnter("fm", "CardGrid"); 
        //这些属性必须在loadMulLine前
        CardGrid.mulLineCount = 0;   
        CardGrid.displayTitle = 1;
        CardGrid.locked = 1;
        CardGrid.canSel = 1;
        CardGrid.canChk = 0;
        CardGrid.hiddenPlus=1;
        CardGrid.hiddenSubtraction=1;
        CardGrid.loadMulLine(iArray);
        CardGrid.selBoxEventFuncName = "showCardInfo";
    }
    catch(ex)
    {
        alert("初始化" + ex.message);
    }
}

</script>