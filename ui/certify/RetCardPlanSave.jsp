<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：RetCardPlanSave.jsp
//程序功能：
//创建日期：2003-11-17
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
  String FlagStr = "Fail";
  String Content = "";
  String strResult = "";
  String strSum = "";
  
  try {
	  String szNo[]					= request.getParameterValues("CardPlanInfoNo");
	  String szPlanID[]			= request.getParameterValues("CardPlanInfo1");
	  String szCertifyCode[]= request.getParameterValues("CardPlanInfo3");
	  String szRetCount[]		= request.getParameterValues("CardPlanInfo5");
	  String szRetState[]		= request.getParameterValues("CardPlanInfo6");
	  
	  //
	  // add info of plan list
	  //
	  LZCardPlanSet tLZCardPlanSet = new LZCardPlanSet();
	  LZCardPlanSchema tLZCardPlanSchema = null;
	  
	  if( szNo != null ) {
		  for(int nIndex = 0; nIndex < szNo.length; nIndex ++) {
		  	tLZCardPlanSchema = new LZCardPlanSchema();
		  	
		  	tLZCardPlanSchema.setPlanID( szPlanID[nIndex] );
		  	tLZCardPlanSchema.setCertifyCode( szCertifyCode[nIndex] );
		  	tLZCardPlanSchema.setRetCount( szRetCount[nIndex] );
		  	tLZCardPlanSchema.setRetState( szRetState[nIndex] );
		  	
		  	System.out.println( szRetState[nIndex] );
		  	
		  	tLZCardPlanSet.add( tLZCardPlanSchema );
		  }
		}
		
		//
		// add info of main lzcardplan
		//
		tLZCardPlanSchema = new LZCardPlanSchema();

		tLZCardPlanSchema.setPlanID( request.getParameter("PlanID") );		
		tLZCardPlanSchema.setCertifyCode( request.getParameter("CertifyCode") );

		tLZCardPlanSchema.setAppCom( request.getParameter("AppCom") );
		tLZCardPlanSchema.setAppCount( request.getParameter("AppCount") );

		tLZCardPlanSchema.setRetCom( request.getParameter("RetCom") );
		tLZCardPlanSchema.setRelaPrint( request.getParameter("RelaPrint") );

		tLZCardPlanSchema.setMakeDate( request.getParameter("MakeDate") );		
		tLZCardPlanSchema.setMakeTime( request.getParameter("MakeTime") );		

		tLZCardPlanSchema.setRetState( request.getParameter("RetState") );		
		tLZCardPlanSchema.setPlanState( request.getParameter("PlanState") );		
		
		GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	    
	  // 准备传输数据 VData
	  VData tVData = new VData();
	
		tVData.add( globalInput );
		tVData.add( tLZCardPlanSchema );
		tVData.add( tLZCardPlanSet );
		tVData.add( new Hashtable() );
	
	  // 数据传输
	  CardPlanUI tCardPlanUI = new CardPlanUI();
	  
	  if( !tCardPlanUI.submitData(tVData, request.getParameter("oper_flag")) ) {
	    if( tCardPlanUI.mErrors.needDealError() ) {
	    	throw new Exception( tCardPlanUI.mErrors.getFirstError() );
	    } else {
	    	throw new Exception("CardPlanUI查询失败，但是没有提供详细的信息");
	    }
	  } else {
	    // tVData.clear();
	    // tVData = tCardPlanUI.getResult();
	    
	    // tLZCardPlanSet = (LZCardPlanSet)tVData.getObjectByObjectName("LZCardPlanSet", 0);
	    
	    // strResult = "0|" + String.valueOf(tLZCardPlanSet.size());
	    // strResult += "^" + tLZCardPlanSet.encode();
	  }
	  
	  FlagStr = "Succ";
	  
	} catch (Exception ex) {
	  ex.printStackTrace();
	  FlagStr = "Fail";
	  Content = ex.getMessage();
	}
%>

<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
<% if( FlagStr.equals("Succ") ) { %>
		 var varResult = new Array();
		 
		 varResult[0] = "<%= strResult %>";
		 varResult[1] = "<%= strSum %>";
		 
		 // parent.fraInterface.onShowResult(varResult);
<% } else { %>
<% } %>
	</script>
</html>
