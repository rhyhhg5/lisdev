<%
//程序名称：CardPlanQueryInput.jsp
//程序功能：
//创建日期：2003-10-16 10:20:49
//创建人  ：kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="CardPlanQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./CardPlanQueryInit.jsp"%>
  <title>单证计划表 </title>
</head>
<body  onload="initForm();" >
  <form action="./CardPlanQuerySubmit.jsp" method=post name=fm target="fraSubmit">
      <table class="common">
        <tr class="common">
          <td class="title">计划标识</td>
          <td class="input"><input class="common" name="PlanID"></td>
          
          <td class="title">单证编码</td>
          <td class="input"><input class="code" name="CertifyCode" ondblclick="return showCodeList('CertifyCode', [this]);" onkeyup="return showCodeList('CardCode', [this]);"></td>
          
        <tr class="common">
          <td class="title">申请机构</td>
          <td class="input"><input class="common" name="AppCom"></td>
          
          <td class="title">申请数量</td>
          <td class="input"><input class="common" name="AppCount"></td></tr>
          
        <tr class="common">
          <td class="title">批复机构</td>
          <td class="input"><input class="common" name="RetCom"></td>
          
          <td class="title">关联计划</td>
          <td class="input"><input class="code" name="RelaPlan" CodeData="0|^00000000000000000000|未关联计划|"
            ondblclick="return showCodeListEx('RelaPlan', [this]);"
            onkeyup="return showCodeListKeyEx('RelaPlan', [this]);"></td></tr>
          
        <tr class="common">
          <td class="title">入机日期（开始）</td>
          <td class="input"><input class="coolDatePicker"  dateFormat="short" name="MakeDateB" verify="起始时间|NOTNULL"></td>
          <td class="title">入机日期（结束）</td>                                                      
          <td class="input"><input class="coolDatePicker"  dateFormat="short" name="MakeDateE" verify="结束时间|NOTNULL"></td></tr>
          
        <tr class="common">
          <td class="title">批复状态</td>
          <td class="input"><input class="code" name="RetState" CodeData="0|^Y|同意|^N|不同意|"
            ondblclick="return showCodeListEx('RetState', [this]);"
            onkeyup="return showCodeListKeyEx('RetState', [this]);"></td>
          
          <td class="title">计划状态</td>
          <td class="input"><input class="code" name="PlanState" CodeData="0|^A|申请|^C|确认|^R|批复|^P|归档|"
            ondblclick="return showCodeListEx('PlanState', [this]);"
            onkeyup="return showCodeListKeyEx('PlanState', [this]);"></td></tr>

      </table>

    </table>
    <table>
	      <tr class="common">
	        <td><input value=" 查  询"              class="common" type="button" onclick="submitForm();return false;"></td>
    		<td><input value="对计划进行预算查询"   class="common" type="button" onclick="queryBuget();"></td>
    		<td><input value=" 返  回"              class="common" type="button" onclick="returnParent();"></td>
    	 </tr>
	      <tr class="common">
	        <td><input value=" 打  印"              class="common" type="button" onclick="printPlan();"></td></td>
    		<td><input value=" 重  置"              class="common" type="button" onclick="initForm();"></td>
    	    <td><input value=" 关  闭"              class="common" type="button" onclick="parent.close();"></td>
    	  </tr>
	      
		  
		  
    	  
    </table>
    
    
    
    
    
    
    <table>
    	<tr>
        <td class=common>
			    <img src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCardPlanGrid);">
    		</td>
    		<td class= titleImg>
    			 单证计划列表
    		</td>
    	</tr>
    </table>
  	<div id="divCardPlanGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCardPlanGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <input value=" 首页 " class="common" type="button" onclick="firstPage();"> 
      <input value="上一页" class="common" type="button" onclick="prevPage();"> 					
      <input value="下一页" class="common" type="button" onclick="nextPage();"> 
      <input value=" 尾页 " class="common" type="button" onclick="lastPage();"> 					
  	</div>
  	
  	<input type=hidden name="sql_where">
  	<input type=hidden name="cur_page" value="0">
  	<input type=hidden name="page_count" value="-1">
  	<!-- this value will be set in *init.jsp -->
  	<input type=hidden name="record_num">
  	<!-- add by guoxiang at 2004-02-04-->
  	<input type=hidden name="Strsql">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
