<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
    //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    GlobalInput globalInput = new GlobalInput();
    globalInput.setSchema((GlobalInput)session.getValue("GI"));
	try
	{
	    //String check[] = request.getParameterValues("InpCertifyListGridChk");
	    //String certifyCodes[] = request.getParameterValues("CertifyListGrid1");
	    //String startNos[] = request.getParameterValues("CertifyListGrid2");
	    //String endNos[] = request.getParameterValues("CertifyListGrid3");
        //
	    //LZCardSet tLZCardSet = new LZCardSet();
	    //for(int i = 0; i < check.length; i++) {
	    //    if(check[i].equals("0"))
	    //        continue;
	    //    System.out.println("单证类型：" + certifyCodes[i] + "  单证起始号：" 
	    //        + startNos[i] + "  单证终止号：" + endNos[i]);
	    //    LZCardSchema tLZCardschema = new LZCardSchema();
	    //    tLZCardschema.setCertifyCode(certifyCodes[i]);
	    //    tLZCardschema.setStartNo(startNos[i]);
	    //    tLZCardschema.setEndNo(endNos[i]);
	    //    tLZCardSet.add(tLZCardschema);
        //}
        
	    LZCardSchema tLZCardschema = new LZCardSchema();
	    tLZCardschema.setCertifyCode(request.getParameter("certifyCode"));
	    tLZCardschema.setStartNo(request.getParameter("startNo"));
	    tLZCardschema.setEndNo(request.getParameter("endNo"));
	    tLZCardschema.setSumCount(request.getParameter("sumCount"));
	    
	    TransferData tTransferData = new TransferData();
	    tTransferData.setNameAndValue("oldStateFlag", request.getParameter("oldStateFlag"));
	    tTransferData.setNameAndValue("newStateFlag", request.getParameter("newStateFlag"));

	    // 准备传输数据 VData
	    VData vData = new VData();
	    vData.addElement(globalInput);
	    vData.addElement(tLZCardschema);
	    vData.addElement(tTransferData);

	    // 数据传输
	    CertifyReTakeBackBL tCertifyReTakeBackBL = new CertifyReTakeBackBL();
        
	    if (!tCertifyReTakeBackBL.submitData(vData, "INSERT")) {
	        Content = " 保存失败，原因是: " + tCertifyReTakeBackBL.mErrors.getFirstError();
	        FlagStr = "Fail";
        } else {
	  	    Content = " 保存成功 ";
	  	    FlagStr = "Succ";
	    }
	}
	catch(Exception ex)
	{
		ex.printStackTrace();
   	    Content = FlagStr + " 保存失败，原因是:" + ex.getMessage();
   	    FlagStr = "Fail";
	}
%>
<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>