<%
//程序名称：CertifyTakeBackSave.jsp
//程序功能：
//创建日期：2002-10-08
//创建人  ：JavaBean
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.lis.taskservice.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;
  String szFailSet = "";

  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );

  //校验处理
  //内容待填充
  
  String auditType = request.getParameter("AuditKind");
  
  if (auditType.equals("BATCH")) //手动批量核销
  {
  	
  	AuditCancelTask auditCancelTask = new AuditCancelTask();
  	VData tVData = new VData();
  	tVData.addElement(globalInput);
  	
  	if(!auditCancelTask.auditOperate(tVData,"HANDLE"))
  	{
  		Content = " 批量核销未全部完成,请在系统不忙时再试！";
		  FlagStr = "Fail";
  	}
  	else
  	{
  		Content = " 批量核销成功 ";
		  FlagStr = "Succ";
  	}
  } 
	else if (auditType.equals("SINGLE")) //人工单证处理
	{
		try {
		  // 单证信息部分
		  //String szPrtNo				= request.getParameter("PrtNo");
		  //String szSendOutCom 	= request.getParameter("SendOutCom");
		  //String szReceiveCom		= request.getParameter("ReceiveCom");
		  //String szInvaliDate 	= request.getParameter("InvalidDate");
		  //String szAmnt 				= request.getParameter("Amnt");
		  //String szHandler			= request.getParameter("Handler");
		  //String szHandleDate		= request.getParameter("HandleDate");
		  
		  //String szNo[]					= request.getParameterValues("CertifyListNo");
		  String szCertifyCode[]	= request.getParameterValues("CertifyList2");
		  String szStartNo[]			= request.getParameterValues("CertifyList4");
		  String szEndNo[]				= request.getParameterValues("CertifyList5");
		  String szSumCount[]  		= request.getParameterValues("CertifyList6");
		  String szStateFlag[]		= request.getParameterValues("CertifyList8");
			System.out.println("in save() -> StateFlag: "+szStateFlag[0]); 
		  int nIndex;
		  
		  LZCardSet setLZCard = new LZCardSet( );
		  if( szCertifyCode == null ) {
		  	throw new Exception("没有输入要回收的起始单证号和终止单证号");
		  }
	
		  for( nIndex = 0; nIndex < szCertifyCode.length; nIndex ++ ) 
		  {
		    //if( szStateFlag == null || szStateFlag[nIndex] == null || szStateFlag[nIndex].equals("") ) {
		    //	throw new Exception("没有输入单证回收状态................");
		    //}
		    
		    LZCardSchema schemaLZCard = new LZCardSchema( );
		    
		    schemaLZCard.setCertifyCode(szCertifyCode[nIndex]);
				//schemaLZCard.setSubCode("");
				//schemaLZCard.setRiskCode("");
				//schemaLZCard.setRiskVersion("");
	      
		    schemaLZCard.setStartNo(szStartNo[nIndex].trim());
		    schemaLZCard.setEndNo(szEndNo[nIndex].trim());
				schemaLZCard.setStateFlag(szStateFlag[nIndex].trim());
				
		    if (globalInput.ComCode.length()<=4)
		    {
		    	schemaLZCard.setSendOutCom("A"+globalInput.ComCode);
		    }
		  	else
		  	{
		  		schemaLZCard.setSendOutCom("B"+globalInput.Operator);
		  	}
		    schemaLZCard.setReceiveCom("SYS");
		    
				schemaLZCard.setSumCount(szSumCount[nIndex]);
				//schemaLZCard.setPrem("");
		    //schemaLZCard.setAmnt(szAmnt);
		    schemaLZCard.setHandler(globalInput.Operator);
		    schemaLZCard.setHandleDate(PubFun.getCurrentDate());
		    //schemaLZCard.setInvaliDate(szInvaliDate);
	
				//schemaLZCard.setTakeBackNo("");
				//schemaLZCard.setSaleChnl("");
				//schemaLZCard.setOperateFlag("");
				//schemaLZCard.setPayFlag("");
				//schemaLZCard.setEnterAccFlag("");
				//schemaLZCard.setReason("");
				//schemaLZCard.setState("");
				//schemaLZCard.setOperator("");
				//schemaLZCard.setMakeDate("");
				//schemaLZCard.setMakeTime("");
				//schemaLZCard.setModifyDate("");
				//schemaLZCard.setModifyTime("");
					    
		    setLZCard.add(schemaLZCard);
		  }
			
		  // 准备传输数据 VData
		  VData vData = new VData();
		
		  vData.addElement(globalInput);
		  vData.addElement(setLZCard);
		  vData.addElement(auditType);
		  
		  Hashtable hashParams = new Hashtable();
		  hashParams.put("CertifyClass", CertifyFunc.CERTIFY_CLASS_CERTIFY);
		  vData.addElement(hashParams);
	
		  // 数据传输
		  //AuditCancelBL tCertTakeBackUI = new AuditCancelBL();
		  CertifyModifyBL tCertifyResendBL = new CertifyModifyBL();

	    if (!tCertifyResendBL.submitData(vData, "INSERT")) {
	        Content = " 保存失败，原因是: " + tCertifyResendBL.mErrors.getFirstError();
	        FlagStr = "Fail";
        } else {
	  	    Content = " 保存成功 ";
	  	    FlagStr = "Succ";
	    }
	
		} catch(Exception ex) {
			ex.printStackTrace( );
	   	Content = FlagStr + " 保存失败，原因是:" + ex.getMessage( );
	   	FlagStr = "Fail";
		}
	}
%>
<html>
  <script language="javascript">
  	<%= szFailSet %>
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
<body>
</body>
</html>

