<html>
<%
//name :CardStatisticCancel.jsp
//function :单证回销情况统计表
//Creator :张建宝
//date :2007-10-23
%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
<%
    GlobalInput tGI = new GlobalInput();        
    tGI = (GlobalInput)session.getValue("GI");
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="CardStatisticCancel.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 	<%@include file="CardStatisticCancelInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm target="fraSubmit">	
   	<table class= common>
   		<tr class= common>
          <td class="title">统计机构</td>
          <td class="input">
          	<input class="codeno" name="companyCode" verify="统计机构|NOTNULL"
          	ondblclick="return showCodeList('cardsendoutcode',[this,companyCodeName],[0,1]);"
            onkeyup="return showCodeListKey('cardsendoutcode',[this,companyCodeName],[0,1]);"
          	><input name = "companyCodeName" class=codename type="text" elementtype=nacessary >
   		</TR>
   		<tr class= common>   			
          <TD class= title> 统计起期 </TD>
          <TD  class= input> <Input class="coolDatePicker" dateFormat="short" name=startDate elementtype=nacessary > </TD>
          <TD  class= title> 统计止期 </TD>
          <TD  class= input> <Input class="coolDatePicker" dateFormat="short" name=endDate elementtype=nacessary > </TD>
   		</TR>
		</table>
    <Input type= "hidden" name= querySql>
   	<input class="cssButton" type= button value="打印报表" onclick="printList();">

</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>