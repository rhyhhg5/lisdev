<%
//Creator :刘岩松
//Date :2003-04-18
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%

  String strCertifyCode = "Select CertifyCode,CertifyName From LMCertifyDes "
                        +"Where CertifyClass = 'D'";
  ExeSQL exesql = new ExeSQL();
  SSRS ssrs =exesql.execSQL(strCertifyCode);
  String CertifyCode = ssrs.encode();
  //System.out.println(CertifyCode);
%>
<script language="JavaScript">
function RegisterDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divDetailInfo.style.left=ex;
  	divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
}
// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
      fm.ReceiveCom.value='';
      fm.CardType.value='';
      fm.StateType.value='1';
      fm.StateName.value='未回销';
      fm.StartNo.value='';
      fm.EndNo.value='';
     
  }
  catch(ex)
  {
    alert("1在CardMaxInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CardMaxInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox();
    initCardState();
		initCertifyMaxGrid();
  }
  catch(re)
  {
    alert("3CertifyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCertifyMaxGrid()
{
	var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许      

      iArray[1]=new Array();
      iArray[1][0]="单证号码";    	//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=150;            			//列最大值
      iArray[1][3]=0;    

      iArray[2]=new Array();
      iArray[2][0]="回销状态";         			//列名
      iArray[2][1]="70px";            		//列宽
      iArray[2][2]=250;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="回销人";         			//列名
      iArray[3][1]="90px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;

      iArray[4]=new Array();
      iArray[4][0]="回销时间";         			//列名
      iArray[4][1]="90px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;
      
      
     
      CertifyMaxGrid = new MulLineEnter( "fm" , "CertifyMaxGrid" );
      CertifyMaxGrid.mulLineCount = 0;
      CertifyMaxGrid.displayTitle = 1; 
      CertifyMaxGrid.locked = 1;
      CertifyMaxGrid.hiddenPlus = 1; 
      CertifyMaxGrid.hiddenSubtraction = 1;  
      CertifyMaxGrid.loadMulLine(iArray);
      //CertifyMaxGrid.detailInfo="单击显示详细信息";
      //CertifyMaxGrid.detailClick=RegisterDetailClick;
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initCardState()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="单证类型";         		//列名
		iArray[1][1]="60px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="单证状态";          		  //列名
		iArray[2][1]="60px";            		//列宽
		iArray[2][2]=60;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许	

		iArray[3]=new Array();
		iArray[3][0]="数量";          		  //列名
		iArray[3][1]="60px";            		//列宽
		iArray[3][2]=60;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		CardState = new MulLineEnter("fm" , "CardState");
		// 这些属性必须在loadMulLine前
		// CardInfo.mulLineCount = 2;
		CardState.displayTitle = 1;
		CardState.locked = 1;
    CardState.hiddenPlus = 1;
    CardState.hiddenSubtraction = 1;
		CardState.loadMulLine(iArray);
		//这些操作必须在loadMulLine后面
		//CardInfo.setRowColData(1,1,"asdf");
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>