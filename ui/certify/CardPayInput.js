var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var mDebug="0";
var mOperate="";
var arrDataSet;
var ImportPath;
var ImportState = "no";
var cntButton = "";
var GrpContNo="1400002649";
function afterCodeSelect( cName, Filed)
{
  if(cName=='CertifyClassListNew')
  {
    displayMulLine();
  
	} else if( cName == 'CertifyHaveNumber' ) {
		displayNumberInfo();		
  }
}

function displayNumberInfo()
{
	var vHaveNumber = fm.HaveNumber.value;
	if( vHaveNumber == 'Y' ) {
		fm.CertifyLength.disabled = false;
	} else {
		fm.CertifyLength.value = 18;
		fm.CertifyLength.disabled = true;
	}
}

function displayMulLine()
{
  var displayType = fm.CertifyClass.value;
  if(displayType=="D")
  {
    fm.all('divShow').style.display="";
    fm.all('divCardRisk').style.display="";
  }
  if(displayType=="P")
  {
    fm.all('divShow').style.display="none";
    fm.all('divCardRisk').style.display="none";
  }
}

//提交，保存按钮对应操作
function submitForm()
{
	if(!chkValiValue()){return;}
	if(!isExistPay()){return;}
	
	if (CardRiskGrid.mulLineCount==0){
		alert("请输入卡单起始、终止号");
		return;
	}	
  for( i =0 ;i<CardRiskGrid.mulLineCount;i++){
  	 	
  	 var startNo = CardRiskGrid.getRowColData(i,1);
  	 var endNo = CardRiskGrid.getRowColData(i,2);   	 
  	 if(!IsSendOut(startNo,endNo,i)){
  		  return;
   	}  	  	
  }

  fm.OperateType.value = "INSERT"; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = './CardPaySave.jsp';
  fm.submit(); //提交
}
//校验
function chkValiValue()
{
	var strState="";
	if (!verifyInput() == true) {
		return false;
	}
	if( fm.OperateType.value == "UPDATE" ){
		strState = ",'12'";
	}
	var strSQL = "select distinct SubCode from LZCard where SubCode='"+fm.CardType.value+"'"
	             + " and State in ('10','11'" + strState +")";
	var arrResult = easyExecSql(strSQL);
	if(arrResult == null)
	{
	  alert("单证类型为:"+fm.CardType.value+"的单证不存在");
    return false;
	}
	if((fm.AgentCode.value=="")&&(fm.AgentCom.value==""))
  {
    alert("请您录入回销人代码或代理机构编码！！！");
    return false;
  }
  if(parseFloat(fm.PayMoney.value)<=0)
  {
    alert("请您录入正确的结算总金额！！！");
    return false;
  }  
  return true;
}

//校验是否已发放
function IsSendOut(startN,endN,RowNum)
{		
	var strState = "";
	if(!chkListValue()){return false;	}
	
	startN = LCh(startN,"0",7);
  endN = LCh(endN,"0",7);
	var numC = CardRiskGrid.getRowColData(RowNum,3)
	if(numC==null || numC==""){
		alert("在第"+RowNum +"行请输入数量");
    return false;
	}	
	if( fm.OperateType.value == "UPDATE" ){
		strState = ",'12'";
	}
	var strSQL = "select count(*) from LZCard where SubCode='"+fm.CardType.value+"'"
	             + " and State in ('10','11'"+ strState +") and (StartNo <='"+ endN + "'"
	             + " and StartNo >='"+ startN + "')"
	
	var arrResult = easyExecSql(strSQL);

	RowNum = RowNum + 1;	
	if(arrResult == null)
	{ 		
	  alert("第"+RowNum +"行不存在下发给业务员或代理机构类型为:"+fm.CardType.value+"的单证");
    return false;
	}else if (numC != arrResult[0][0] && cntButton!="yes")
	{
		alert("第"+RowNum +"行部分单证没有下发，或已录入或已确认,或数量录入不正确");
    return false;
	} 
	strSQL = "select distinct PayNo from  LZCardPay where CardType='"+fm.CardType.value+"'"
	             + " and (StartNo <='"+ startN + "'"
	             + " and EndNo >='"+ startN + "' or "
	             + " StartNo >='"+ startN + "'"
	             + " and EndNo <='"+ endN + "' or "
	             + " StartNo <='"+ endN + "'"
	             + " and EndNo >='"+ endN + "' "
	            
	             +")"
	var arrResult1 = easyExecSql(strSQL);
	
	if (arrResult1!=null && arrResult1.length > 1 ){
		alert("第"+RowNum +"行部分单证已录入或已确认");
		return false;
	}
	if (arrResult1!=null && arrResult1.length ==1 && arrResult1[0][0] !=fm.all('PayNo').value ){
		alert("第"+RowNum +"行部分单证已录入或已确认");
		return false;
	}
	 return true;
}
/**
 * 将字符串补数,将sourString的<br>前面</br>用cChar补足cLen长度的字符串,如果字符串超长，则不做处理
 * <p><b>Example: </b><p>
 * <p>LCh("Minim", "0", 10) returns "00000Minim"<p>
 * @param sourString 源字符串
 * @param cChar 补数用的字符
 * @param cLen 字符串的目标长度
 * @return 字符串
 */
function LCh(sourString, cChar,cLen) {
	
	 var sourString = sourString+ "";
   var tLen = sourString.length;    
   
   var i=0;   
   var iMax=0;
   var tReturn = "";
   if (tLen >= cLen) {
       return sourString;
   }
   iMax = cLen - tLen;
   for (i = 0; i < iMax; i++) {
       tReturn = cChar + tReturn;
   }
   tReturn = trim(tReturn) + trim(sourString);   
   return tReturn;
}

//校验是否已发放
function chkListValue(startN,endN,RowNum)
{	
	if (startN!="" && endN!=null)
	{
			var RowNumR = RowNum + 1;
			if(startN==""||startN==null)
			{				
				alert("在第"+ RowNumR  + "行请输入起始号");
				return;
			}
			if (endN==""||endN==null)
			{
				alert("在第"+ RowNumR + "行请输入起始号");
				return;
			}
			
			if (!isNumeric(startN))
			{
				alert("在第"+ RowNumR + "行起始单号有非数字字符！");
				return false;
			}
			if (!isNumeric(endN))
			{
				alert("在第"+ RowNumR + "行终止单号有非数字字符！");
				return false;
			}
			
			var num=parseFloat(endN)-parseFloat(startN)+1;
			if (num<=0)
			{
				alert("在第"+ RowNumR + "行起始单号大于终止单号！");
				return false;
			}							
	}
	return true;
}

//计算数量
function getCount()
{
	cntButton ="yes";
	if(!chkValiValue()){return false;}
  var i = 0;    
  for( i =0 ;i<CardRiskGrid.mulLineCount;i++){
  	 	
  	 var startNo = CardRiskGrid.getRowColData(i,1);
  	 var endNo = CardRiskGrid.getRowColData(i,2);   	 
  	if(!countNum(CardRiskGrid.getRowColData(i,1),CardRiskGrid.getRowColData(i,2),i))
  	{
  		return;
  	}
  	
  }
}
//具体执行计算数量
function countNum(startN,endN,RowNum)
{	   
	if (!chkListValue(startN,endN,RowNum))
	{	
		return false;
	}
	var num=parseFloat(endN)-parseFloat(startN)+1;
	CardRiskGrid.setRowColData(RowNum ,3,num+"");	
	if(!IsSendOut(startN,endN,RowNum)){return false;}	
	return true;
}



//校验单证类型码在表LZCardNumber中存在
function verifyCardType()
{
	if(fm.CertifyClass.value=="P")   {
  	 	
      return true ;
  }
	var strSQL="select distinct CardType from LZCardNumber where CardType='"+fm.SubCode.value+"'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult!=null)
	{
		if(fm.SubCode.value!=arrResult[0][0])
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
}


function verifyHaveNum()
{
	var strSQL="select havenumber from LMCertifyDes where CertifyCode='"+fm.CertifyCode.value+"'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult!=null)
	{
		if(fm.HaveNumber.value!=arrResult[0][0])
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();   
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    initForm();
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initCardRiskGrid();
  }
  catch(re)
  {
    alert("初始化页面错误，重置出错");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
      parent.fraMain.rows = "0,0,0,0,*";
  }
   else
   {
      parent.fraMain.rows = "0,0,0,0,*";
   }
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	var upStr = "";//修改提醒
	fm.OperateType.value = "UPDATE";
	if(!chkValiValue()){return;}
	if (CardRiskGrid.mulLineCount==0){
		alert("请输入卡单起始、终止号");
		return;
	}
	//判断是否已下发
  for( i =0 ;i<CardRiskGrid.mulLineCount;i++){
  	 	
  	 var startNo = CardRiskGrid.getRowColData(i,1);
  	 var endNo = CardRiskGrid.getRowColData(i,2); 
  	 if(!IsSendOut(startNo,endNo,i)){
  		  return;
   	}    	 
  	 	  	
  }
	//判断是否已确认	
	var strSQL = "select count(*) from LZCardPay where PayNo='" + fm.PayNo.value+ "' and CardType='"+fm.CardType.value+"'"
             + " and state = '1'"
             ;

  var arrResult1 = easyExecSql(strSQL);
	 
	if(arrResult1 != null && arrResult1[0][0]!=0){
		alert("您不能修改，该结算单已被财务管理员确认！！！");
    return false;
	}
	
	//判断是否已存在结算单	
	strSQL = "select count(*) from LZCardPay where PayNo='" + fm.PayNo.value+ "'" ;
  var arrResult2 = easyExecSql(strSQL);
	upStr = "您确实想修改该记录吗?";
	if(arrResult2 != null && arrResult2[0][0]!=0){
		upStr = "该结算单已存在，您确实想修改该记录吗?";
	}
  if (confirm(upStr))
   {
   
    var i = 0;
    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    showSubmitFrame(mDebug);
    fm.action = './CardPaySave.jsp';

    fm.submit(); //提交
  }
  else
  {
    fm.OperateType.value = "";
    //alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  fm.OperateType.value="QUERY";
  window.open("./CardPayQuery.jsp");
}

function deleteClick()
{
	if( fm.PayNo.value == "" ){
		alert("删除失败，请输入结算单号！！！");
    return false;
	}
	//判断是否已确认	
	var strSQL = "select count(*) from LZCardPay where PayNo='" + fm.PayNo.value+ "' "
             + " and state = '1'"    ;

  var arrResult1 = easyExecSql(strSQL);
	 
	if(arrResult1 != null && arrResult1[0][0]!=0){
		alert("删除失败，该结算单已被财务管理员确认！！！");
    return false;
	}
  
   if(confirm("您确实要删除该记录吗？"))
  {
    
    fm.OperateType.value = "DELETE";
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或连接其他的界面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    showSubmitFrame(mDebug);
    fm.action = './CardPaySave.jsp';

    fm.submit();//提交
    resetForm();
  }
  else
  {
    fm.OperateType.value = "";
    alert("您已经取消了修改操作！");
  }
}

function queryAgent() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
  }
  fm.all('OperateObj').value="AgentCode";
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+ "&SaleChnl=all","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  
}
function queryAgentBlur() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
  }
  fm.all('OperateObj').value="AgentCode";
  if(fm.all('AgentCode').value == "")	{
    //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
    var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value,"AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  }
  if(fm.all('AgentCode').value != "")	 {

    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom like '"+fm.all('ManageCom').value+"%'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];     
    } else {    
      alert("代码为:["+fm.all('AgentCode').value+"]的业务员不存在，请确认!");
    }
  }
}

function queryAgent2() {
  if(fm.all('ManageCom').value=="") {
    alert("请先录入管理机构信息！");
    return;
  }
  if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==8 )	 {
    var cAgentCode = fm.AgentCode.value;  //保单号码
    var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom like '"+fm.all('ManageCom').value+"%'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];     
      alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    } else {     
      alert("代码为:["+fm.all('AgentCode').value+"]的业务员不存在，请确认!");
      return false;
    }
  }
}
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult) {
  if(arrResult!=null) {
  	if(fm.all('OperateObj').value =="AgentCode"){
	    fm.AgentCode.value = arrResult[0][0];
	    fm.AgentName.value = arrResult[0][5];
	    fm.GroupAgentCode.value = arrResult[0][95];
    }else if(fm.all('OperateObj').value =="AgentCom")
    {
    	 fm.AgentCom.value = arrResult[0][0];
	     fm.AgeComName.value = arrResult[0][1];
    }
    
  }
}

//批量发放。查询代理机构的函数。
function queryCom()
{
	//下发对象：代理机构
	fm.all('OperateObj').value="AgentCom";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('AgentCom').value == "")	
  {  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/LAComQueryInput.jsp?ManageCom="+fm.all('ManageCom').value,"LAComQueryInput",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	}	
}

//批量发放。查询代理机构的函数。
function queryComBlur()
{
	//下发对象：代理机构
	fm.all('OperateObj').value="AgentCom";
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
  if(fm.all('AgentCom').value != ""  && fm.all('AgentCom').value != null)	
  {  
  	 var cAgentCom = fm.AgentCom.value;  //代理机构代码
  	 var strSql = "select a.AgentCom,a.Name"	       
	         +" from LACom a where 1=1 "	        
	         + getWherePart('a.AgentCom','AgentCom')	
	         + getWherePart('a.ManageCom','ManageCom','like')
	         +" order by AgentCom";	
	    var arrResult = easyExecSql(strSql);
	   
	    if (arrResult != null) {
	      fm.AgeComName.value = arrResult[0][1];     
	     } else {     
	      alert("代码为:["+fm.all('AgentCom').value+"]的代理机构不存在，请确认!");
	      return false;
	    }    	         
	}	
}


function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function InsuredListUpload() {
  
  var i = 0;
  getImportPath();
  ImportFile = fmImport.all('FileName').value;  
  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmImport.action = "./CardPayDiskSave.jsp?ImportPath="+ImportPath;
  fmImport.submit(); //提交
  
}

function getImportPath () {
  // 书写SQL语句
  var strSQL = "";

  strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未找到上传路径");
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  ImportPath = turnPage.arrDataCacheSet[0][0];

}
function getPrtNo () {
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select PrtNo from lcgrpcont where GrpContNo ='"+ GrpContNo +"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未查找到保单:" + GrpContNo);
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  return turnPage.arrDataCacheSet[0][0];
}
//判断是否已录入结算单号	
function isExistPay()
{
	
	//判断是否已录入结算单号	
	var strSQL = "select count(*) from LZCardPay where PayNo='" + fm.PayNo.value+"'";

  var arrResult1 = easyExecSql(strSQL);
	 
	if(arrResult1 != null && arrResult1[0][0]!=0){
		alert("您不能保存，该结算单已录入过！！！");
    return false;
	}
  return true;
}