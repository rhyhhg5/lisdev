
<%
	//程序名称：CertifyActiveModifySave.jsp
	//程序功能：
	//创建日期：2011-6-13
	//创建人  ：JavaBean
	//更新记录：  更新人    更新日期     更新原因/内容
	//
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certify.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK"%>
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput globalInput = new GlobalInput();
	globalInput.setSchema((GlobalInput) session.getValue("GI"));

	try {
		
		VData tVData = new VData();
		tVData.addElement(globalInput);
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("CertifyCodeModify",request.getParameter("CertifyCodeModify"));
		tTransferData.setNameAndValue("StartNoModify",request.getParameter("StartNoModify"));
		tTransferData.setNameAndValue("EndNoModify",request.getParameter("EndNoModify"));
		tTransferData.setNameAndValue("CardTypeModify",request.getParameter("CardTypeModify"));
		tTransferData.setNameAndValue("AgentCode",request.getParameter("AgentCode"));
		tTransferData.setNameAndValue("AgentType",request.getParameter("AgentType"));
		tVData.addElement(tTransferData);
		CertifyActiveModifyBL tCertifyActiveModifyBL = new CertifyActiveModifyBL();
		
		if (!tCertifyActiveModifyBL.submitData(tVData)) {
			Content = " 保存失败，原因是: " + tCertifyActiveModifyBL.mErrors.getFirstError();
			FlagStr = "Fail";
		} else {
			Content = " 变更成功 ";
			FlagStr = "Succ";
		}

	} catch (Exception ex) {
		ex.printStackTrace();
		Content = " 保存失败，原因是:" + ex.getMessage();
		FlagStr = "Fail";
	}
%>
<html>
	<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
	<body>
	</body>
</html>

