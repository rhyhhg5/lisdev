<%@page contentType="text/html;charset=GBK" %>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.certify.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head >
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src = "CertifyDescRiskInput.js"></SCRIPT>
    <%@include file="CertifyDescRiskInit.jsp"%>
</head>
<body onload="initForm();initElementtype();" >
    <form action="./CertifyDescSave.jsp" method=post name=fm target="fraSubmit">
        <table class=common align=center>
        <tr align=right>
          <td class=button >&nbsp;&nbsp;</td>
          <td class=button width="10%" align=right>
			    	<INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="return queryClick();">
			    </td>
          <td class=button width="10%" align=right>
          	<INPUT class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
			    </td>
			  </tr>
			  </table>
        
        <table class=common>
            <tr class=common>
                <td class=title>单证编码</td>
                <td class=input><input class=common name=CertifyCode id=CertifyCodeId elementtype=nacessary readonly ></td>
                <td class=title>单证名称</td>
                <td class=input><input class=common name=CertifyName elementtype=nacessary readonly ></td>
            </tr>
            <tr class=common>
                <td class=title>单证类型</td>
                <td class=input><input class=codeno name=CertifyClass readonly ><input class=codename name='CertifyClassName' readonly elementtype=nacessary></td>
                <td class=title>业务员校验标志</td>
                <td class=input><input class=codeno readonly name="VerifyFlag" ><input class=codename name= 'VerifyFlagName'  verify="业务员校验标志|NOTNULL" elementtype=nacessary readonly >
                </td>
            </tr>
            <tr class=common>
                <td class=title>单证号码长度</td>
                <td class=input><input class=common name=CertifyLength elementtype=nacessary readonly ></td>
                <td class=title>状态</td>
                <td class=input><input class=codeno name=State readonly ><input name=StateName class=codename verify="状态|NOTNULL" elementtype=nacessary readonly >
                </td>
            </tr>
            <tr class=common>
                <td class=title>是否是有号单证</td>
                <td class=input><input class=codeno name=HaveNumber readonly ><input name=HaveNumberName class=codename  verify="是否是有号单证|NOTNULL" elementtype=nacessary readonly>
                </td>
                <td class="title">单证单位</td>
                <td class="input"><input class=common name="Unit" readonly ></td>
            </tr>
        </table>

        <table class=common>
            <tr>
                <td class=title>是否选定具体管理机构</td>
                <td class=input>
                    <input type="radio" name="StateRadio" value="1" checked onclick="showCom()" disabled > 是 &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="StateRadio" value="0" onclick="showCom()" disabled > 否
                </td>
                <td class=title>
                    <div id="divComName" style="display:''">
                        管理机构
                    </div>
                </td>
                <td class=input>
                    <div id="divComCode" style="display:''">
                        <input class=codeno name= ManageCom readonly ><input name=ManageComName class=codename verify="管理机构|NOTNULL" elementtype=nacessary readonly >
                    </div>
                </td>
            </tr>
            <tr>
                <td class=title>备注</td>
                <td class=input><input class=common name=Note type="text" readonly ></td>
                <td class=title> </td>
                <td class=title> </td>
            </tr>
        </table>
        <div id="divShow" style="display: ''">
            <table class=common>
                <tr>
                    <td class=title>校验规则</td>
                    <td class=input><input class=codeno name=CheckRule readonly ><input name=ChkRuleName class=codename  verify="是否是有号单证|NOTNULL" elementtype=nacessary readonly ></td>
                    <td class=title>单证类型码</td>
                    <td class="input"><input class=common name=SubCode maxlength=2 verify="单证类型|NUM&NOTNULL"  elementtype=nacessary readonly >
                    <input class=common type= hidden name="SubCodeBackup" value=""></td>
                </tr>
                <tr>
                    <td class=title>业务类型</td>
                    <td class=input><input class=codeno name=Operate verify="类型|NOTNULL" readonly ><input class=codename name='CardTypeName' readonly elementtype=nacessary readonly ></td>
                </tr>
            </table>
        </div>
        <div id= "divCardRisk" style="display: ''">
            <table>
                <tr>
                    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCardRisk);"></td>
                    <td class=titleImg>定额单险种信息</td>
                </tr>
            </table>
            <table class=common>
                <tr class=common>
                    <td text-align: left colSpan=1>
                        <span id="spanCardRiskGrid" >
                        </span>
                    </td>
                </tr>
            </table>
            <table class=common>
                <tr class=common>
                    <td class=title>保险期限</td>
                    <td class=input><input class=common name=DutyPer elementtype=nacessary ></td>
                    <td class=title>单位</td>
                    <td class=input><input class=codeno name=DutyUinit CodeData="0|^Y|年^M|月^D|日^B|不定期"  readonly
                        ondblClick="showCodeListEx('DutyUinit',[this,DutyUinitName],[0,1],null,null,null,1);" ><input name=DutyUinitName class=codename  verify="是否是有号单证|NOTNULL" elementtype=nacessary >
                    </td>
                </tr>
            </table>
        </div>
        <input type="hidden" name=Prem >
        <input type="hidden" name=Amnt >
        <input type="hidden" name=OperateType >
        <input type="hidden" name=CertifyCode_1 >
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>