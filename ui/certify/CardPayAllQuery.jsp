<html>
<%
//程序名称：银行代收对帐清单
//程序功能：
//创建日期：2003-3-25
//创建人  ：刘岩松程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.bank.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  
  GlobalInput tGI = new GlobalInput();        
    tGI = (GlobalInput)session.getValue("GI"); 
    String strOperator = tGI.Operator;
%>
<script>
	var tOperater = "<%=strOperator%>";  //结算单录入确认
	
</script>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="CardPayAllQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="CardPayAllQueryInit.jsp"%>
</head>
<body  onload="initForm();" >
<form action="" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->

   <Div id= "divLLReport1" style= "display: ''">

   	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		结算单号
   			</TD>
   			<TD class= input>
   				<Input class= common name= PayNo id="CertifyCodeId" elementtype=nacessary>
   			</TD>   			
      </TR>
   		<TR class= common>
   			<td class="title">单证编码</td>
        <td class="input">
          <input class="codeno" name="CertifyCode"	ondblclick="return showCodeList('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);" onkeyup="return showCodeListKey('cardcode', [this,CertifyCodeName,CardType],[0,1,2],null,null,null,1,350);"><Input class= codename name="CertifyCodeName" readonly></td>
        <TD class= title>单证类型码：</TD>        
          <td class="input"><input  class=common name="CardType" maxlength=2 verify="单证类型|NUM&NOTNULL"  elementtype=nacessary" ></td>
   		</TR> 

            <TR class= common>
         <TD class= title>
           回销人代码：
         </TD>
          <TD class= input>
           <Input NAME=AgentCode VALUE=""  CLASS=code8  type=hidden>
           <Input NAME=GroupAgentCode VALUE=""  CLASS=code8  ondblclick="return queryAgent();"onkeyup="return queryAgent2();" >
            </TD>         
         <TD class= title>
           回销人名称：
         </TD>
         <TD class= input>
           <Input class= common name= 'AgentName' ReadOnly >
         </TD>
      </TR>
       <TR class= common>
         <TD class= title>
           代理机构编码：
         </TD>
         <TD class= input>
           <input name="AgentCom" CLASS=code8  value="" ondblclick="return queryCom();" >
         <TD class= title>
           代理机构名称：
         </TD>
         <TD class= input>
           <Input class= common name= 'AgeComName' ReadOnly >
         </TD>
      </TR>
      <Input type=hidden name= OperateType  >
      <Input type=hidden name= OperateObj  >
       <Input type=hidden name= ManageCom  >
    </Table>

		<input class="cssButton" type=button value="查  询" onclick="submitForm()">
		<input value="打印报表" type="button" onclick="printList();" class="cssButton">
		    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanCardPayGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
      <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>
  	</div>
  	<input type=hidden name="SearchSQL" >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
