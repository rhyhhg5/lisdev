var turnPage = new turnPageClass();

//提交后触发事件
function afterSubmit(flag, content)
{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    initInputBox();
	queryCardInfo();
}

//查询发票配置列表
function queryCardInfo()
{
	var sql = "select Code, (select Name from LDCom where ComCode = Code), CodeName, CodeAlias "
	    + "from LDCode where CodeType = 'invoicetype' "
	    + "and Code like '" + tManageCom + "%' "
	    + "and Code like '" + fm.ManageCom.value + "%' "
	    + getWherePart("CodeName", "CertifyCode")
	    + "order by Code";
	
	turnPage.strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
    turnPage.queryModal(sql,CardGrid);
	
    return true;
}

//显示发票配置，用于修改/删除
function showCardInfo()
{
    var sel = CardGrid.getSelNo();
    if(sel == 0 || sel == null)
    {
        alert( "请先选择一条记录。" );
        return false;
    }
	fmSave.ManageCom.value = CardGrid.getRowColDataByName(sel - 1, "ManageCom");
	fmSave.ManageName.value = CardGrid.getRowColDataByName(sel - 1, "ManageName");
	fmSave.CertifyCode.value = CardGrid.getRowColDataByName(sel - 1, "CertifyCode");
	fmSave.CertifyName.value = CardGrid.getRowColDataByName(sel - 1, "CertifyName");
	
    return true;
}

//新增发票配置
function insertForm()
{
    if(!checkDate())
    {
        return ;
    }
    var strSql = "select 1 from LDCode where CodeType = 'invoicetype' and Code = '" + fmSave.ManageCom.value + "'";
    var arrResult = easyExecSql(strSql);
    if (arrResult) 
    {
        alert("已有该管理机构的发票配置信息，请点击修改！");
        return false;
    }
    //alert();return false;
    fmSave.Operate.value = "INSERT";
    fmSave.action = "./InvoiceConfigSave.jsp";
    fmSave.submit();
}

//修改发票配置
function modifyForm()
{
    if(!checkDate())
    {
        return ;
    }
    var strSql = "select 1 from LDCode where CodeType = 'invoicetype' and Code = '" + fmSave.ManageCom.value + "'";
    var arrResult = easyExecSql(strSql);
    if (!arrResult) 
    {
        alert("没有该管理机构的发票配置信息，请点击保存！");
        return false;
    }
    //alert();return false;
    fmSave.Operate.value = "UPDATE";
    fmSave.action = "./InvoiceConfigSave.jsp";
    fmSave.submit();
}

//删除发票配置
function deleteForm()
{
    if(!checkDate())
    {
        return ;
    }
    var strSql = "select 1 from LDCode where CodeType = 'invoicetype' and Code = '" + fmSave.ManageCom.value + "'";
    var arrResult = easyExecSql(strSql);
    if (!arrResult) 
    {
        alert("没有该管理机构的发票配置信息，请确认！");
        return false;
    }
    //alert();return false;
    fmSave.Operate.value = "DELETE";
    fmSave.action = "./InvoiceConfigSave.jsp";
    fmSave.submit();
}

//校验数据
function checkDate()
{
    if(tManageCom.length > 4)
    {
        alert("只有总公司和分公司有发票配置权限！");
        return false;
    }
    if(fmSave.ManageCom.value == null || fmSave.ManageCom.value == "")
    {
        alert("机构代码为空！");
        return false;
    }
    if(fmSave.CertifyCode.value == null || fmSave.CertifyCode.value == "")
    {
        alert("单证类型为空！");
        return false;
    }
    return true;
}