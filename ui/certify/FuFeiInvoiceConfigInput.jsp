<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="FuFeiInvoiceConfig.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="FuFeiInvoiceConfigInit.jsp"%>
  <title>分公司发票配置</title>
</head>
<%
  GlobalInput globalInput = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">
  var tManageCom = "<%=globalInput.ManageCom%>"
</script>
<body onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <table class="common">
      <tr class="common">
        <td class="title">管理机构</td>
        <td class="input"><input class="codeno" name=ManageCom value="<%=globalInput.ManageCom%>" ondblclick="return showCodeList('comcode',[this,ManageName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageName],[0,1],null,null,null,1);"><input class=codename name=ManageName readonly></td> 
        <td class="title">单证类型</td>
        <td class="input"><input class="codeno" name="CertifyCode" ondblclick="return showCodeList('certifycode1', [this,CertifyName],[0,1],null,null,null,1,300);" onkeyup="return showCodeListKey('certifycode1', [this,CertifyName],[0,1],null,null,null,1,300);"><input class=codename name=CertifyName readonly></td>  		
      </tr>
    </table>
    <input value="查  询" class=cssButton type=button onclick="queryCardInfo();">
    <br> </br>
    <table>
      <tr>
        <td class=common><img src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCardGrid);"></td>
        <td class= titleImg>分公司发票配置列表</td>
      </tr>
    </table>
    <div id= "divCardGrid" style="display: ''" align=center>
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1><span id="spanCardGrid" ></span></td>
        </tr>
      </table>
    </div>
  	<div id= "divPage" align=center style="display: ''">
      <input value="首  页" class=cssButton type=button onclick="turnPage.firstPage();"> 
      <input value="上一页" class=cssButton type=button onclick="turnPage.previousPage();"> 					
      <input value="下一页" class=cssButton type=button onclick="turnPage.nextPage();"> 
      <input value="尾  页" class=cssButton type=button onclick="turnPage.lastPage();"> 
    </div>
  </form>
  <form action="" method=post name=fmSave target="fraSubmit">
    <table class="common">
      <tr class="common">
        <td class="title">管理机构</td>
        <td class="input"><input class="codeno" name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageName],[0,1],null,null,null,1);" readonly><input class=codename name=ManageName readonly></td>
        <td class="title">单证类型</td>
        <td class="input"><input class="code" name="CertifyCode" ondblclick="return showCodeList('certifycode1', [this,CertifyName],[0,1],null,null,null,1,300);" readonly></td>
        <td class="title">单证名称</td>
        <td class="input"><input class=common name=CertifyName ></td>		
      </tr>
    </table>
    <br> </br>
    <input type="hidden" name="Operate">
    <input value="保  存" class=cssButton type=button onclick="insertForm();">
    <input value="修  改" class=cssButton type=button onclick="modifyForm();">
    <input value="删  除" class=cssButton type=button onclick="deleteForm();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
