var showInfo;

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//确定按钮对应操作
function submitForm()
{
	fm.OperateType.value="RETIFY";
	try 
	{
		if( verifyInput() == true && CertifyTypeGrid.checkValue("CertifyTypeGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./OrderCheAppSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3() //确定按钮对应校验
{
	var CerCount=CertifyTypeGrid.mulLineCount; //得到MulLine的行数	
	
	var chkFlag=false; //用于标记是否有事件被选中
	
	var k=0;
	
	for (i=0;i<CerCount;i++) //根据mulLineCount数循环
	{
		if(CertifyTypeGrid.getChkNo(i)==true) //得到被选中的事件,就将chkFlag置为true
		{	
			chkFlag=true;
		}
	}
	
	if (chkFlag==false)
	{
		alert("请选中批准的订单！");
		return false;
	}
	
	var strSQL = "select State from LZOrder where SerialNo = '"+fm.SerialNo.value+"'";
	var arrResult=easyExecSql(strSQL);
	if (arrResult)
	{
		if (arrResult[0][0]=="1")
		{
			alert("该征订批次已被批准，不能再修改订单状态！");
			return false;
		}
	}else
	{
		alert("批次号为空！");
		return false;
	}
	
	return true;
}

function allSubmitForm()
{
	fm.OperateType.value="ALLRETIFY";
	
	if (!confirm("确定要批准该征订批次下所有机构的征订单吗?"))
	{
		return false;
	}
	try 
	{
		if (veriryInput4()==true)
		{
			var i = 0;
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			
			fm.action="./OrderCheAppSave.jsp";
			fm.submit(); //提交
	  }
	  else
	  {
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4()
{
	//if (fm.SerialNo.value==""||fm.SerialNo.value==null)
	//{
	//	alert("请先录入批次号!");
	//	return false;
	//}
	var strSQL = "";
	strSQL = "SELECT STATE FROM LZORDER WHERE SERIALNO='"+fm.SerialNo.value+"'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult[0]=="1")
	{
		alert("该批次已经是批准状态!");
		return false;
	}
	
	strSQL = "select SERIALNO from LZORDERDETAIL WHERE 1=1 and "
		+ " SERIALNO='"+fm.SerialNo.value+"'";
	arrResult=easyExecSql(strSQL);
	
	if (arrResult)
	{
	}
	else
	{
		alert("该批次中还没有订单");
		return false;
	}
	
	strSQL = "select SERIALNO from LZORDERDETAIL WHERE 1=1 and "
		+ " SERIALNO='"+fm.SerialNo.value+"'  AND ORDERSTATE<>'3'";
	arrResult=easyExecSql(strSQL);
	if (arrResult)
	{
		alert("该批次中还有未批准的订单");
		return false;
	}
	return true;
}

function printLoadForm() //打印下载
{
	try 
	{
		if (veriryInput6()==true)
		{
		  var i = 0;
		  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		  //showSubmitFrame(mDebug); 
			//fm.fmtransact.value = "PRINT";
			fm.target = "OrderCollect";
			fm.action = "../f1print/OrderCollectSta.jsp"
			fm.submit();
			showInfo.close();
		}else
		{
		}
	}
	catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput6() //打印下载校验
{
	if (fm.SerialNo.value==""||fm.SerialNo.value==null)
	{
		alert("请先录入批次号!");
		return false;
	}
	var strSQL = "";
	strSQL = "SELECT STATE FROM LZORDER WHERE SERIALNO='"+fm.SerialNo.value+"'";
	var arrResult = easyExecSql(strSQL);
	
	//strSQL = "select SERIALNO from LZORDERDETAIL WHERE 1=1 and "
	//	+ " SERIALNO='"+fm.SerialNo.value+"' AND ORDERCOM='"+fm.OrderCom.value+"' AND ORDERSTATE<>'3'";
	//arrResult=easyExecSql(strSQL);
	if(arrResult[0]=="0")
	{
		alert("该批次未批准,不能打印汇总表!");
		return false;
	}
	return true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, SerialNo, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
  	
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  resetForm();
	  //SearchOrder();
	  //fm.SerialNo.value = "";
  }
}

function updateForm() //打回操作
{
	fm.OperateType.value="BACK";
	try 
	{
		if( verifyInput() == true && CertifyTypeGrid.checkValue("CertifyTypeGrid")) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./OrderCheAppSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5() //打回操作校验
{
	var CerCount=CertifyTypeGrid.mulLineCount; //得到MulLine的行数	
	
	var chkFlag=false; //用于标记是否有事件被选中
	
	var k=0;
	
	for (i=0;i<CerCount;i++) //根据mulLineCount数循环
	{
		if(CertifyTypeGrid.getChkNo(i)==true) //得到被选中的事件,就将chkFlag置为true
		{	
			chkFlag=true;
		}
	}
	
	if (chkFlag==false)
	{
		alert("请选中批准的订单！");
		return false;
	}
	
	var strSQL = "select State from LZOrder where SerialNo = '"+fm.SerialNo.value+"'";
	var arrResult=easyExecSql(strSQL);
	if (arrResult[0][0]=="1")
	{
		alert("该征订批次已被批准，不能再修改订单状态！");
		return false;
	}
	
	return true;
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick() //查询批次
{
	var strSQL = "";
	
	strSQL = "select SerialNo,AttachDate,"
		+ " case when State='0' then '征订' else '批准' end ,State from lzorder where 1=1"
	 	+ getWherePart("State");
	;
	var quart = fm.QuarterValue.value;
	if(fm.QuarterValue.value==""||fm.QuarterValue.value==null)
	{
	}
	else
	{
		var staYear = quart.substring(0,4);
		
		//alert(quart.substr(0,4));
		//alert(quart.substr(4,1));
		
		var month;
		
		if (quart.substr(4,1)=="1")
		{
			month = new Array("01","02","03");
		}
		if (quart.substr(4,1)=="2")
		{
			month = new Array("04","05","06");
		}
		if (quart.substr(4,1)=="3")
		{
			month = new Array("07","08","09");
		}
		if (quart.substr(4,1)=="4")
		{
			month = new Array("10","11","12");
		}
				
		strSQL = strSQL 
			+ " and (cast(AttachDate as char(10)) like '%%"+staYear+"-"+month[0]+"%%'"
			+ " or cast(AttachDate as char(10)) like '%%"+staYear+"-"+month[1]+"%%'"
			+ " or cast(AttachDate as char(10)) like '%%"+staYear+"-"+month[2]+"%%')"
	}
	strSQL = strSQL + " order by SerialNo Desc";
	turnPage.queryModal(strSQL, SerialGrid);
}

function resetClick()
{
	fm.OrderSta.value ="";
	fm.QuarterValue.value = "";
	fm.StateName.value="";
	fm.State.value="";
	resetForm();
	
}

function initOrder() //页面初始化是显示分公司查询批次中的订单
{
	var strSQL = "select max(Serialno) from LZOrder where state='0'";
	var arrResult = easyExecSql(strSQL);
	fm.SerialNo.value = arrResult[0][0];
	
	strSQL = "select serialNo from LZOrder";
	
	strSQL = "(select a.Certifycode A,b.CertifyName B,"
		+ " a.ordermoney C, 0 D,'' E,'' F,0 G"
		+ " from LZOrderCard a,LMCertifyDes b where a.serialno = (select max(Serialno) from LZOrder where state='0') and a.CertifyCode=b.CertifyCode "
		+ " and a.certifycode not in (select certifycode from LZOrderDetail where serialno = (select max(Serialno) from LZOrder where state='0') and OrderCom='"+fm.OrderCom.value+"') "
		+ " union "
		+ " select a.Certifycode A,b.CertifyName B," 
		+ " (select ordermoney from LZOrderCard where Serialno=a.Serialno and CertifyCode=a.CertifyCode) C,"
		+ " a.Amnt D, a.OrderState E, case a.OrderState when '1' then '未阅' when '2' then '打回' else '批准' end F,a.Amnt G"
		+ " from LZOrderDetail a,LMCertifyDes b "
		+ " where a.serialno = (select max(Serialno) from LZOrder where state='0') and a.CertifyCode=b.CertifyCode and OrderCom='"+fm.OrderCom.value+"' ) order by E"
		;
	arrResult = easyExecSql(strSQL);
	
	if (arrResult)
	{
	  displayMultiline(arrResult,CertifyTypeGrid);	
	}
	
	strSQL = "select Note from LZOrder where SerialNo = (select max(Serialno) from LZOrder where state='0')"
	;
	arrResult = easyExecSql(strSQL);
	fm.Note.value = arrResult[0];
}

function selSerial()
{
	var tSel = SerialGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return false;
  }
	var serialNo = SerialGrid.getRowColData(tSel-1,1); 
	fm.SerialNo.value = serialNo;
	CertifyTypeGrid.clearData();
}

function SearchOrder() //分公司查询批次中的订单
{
  if(fm.SerialNo.value=='' ||fm.SerialNo.value==null)
  {
		alert( "请先录入征订批次号！" );
		return false;
  }
  
  if (fm.OrderCom.value==""||fm.OrderCom.value==null)
	{
		alert("请先选择机构代码！");
		return false;
	}
	var serialNo = fm.SerialNo.value;
	fm.SerialNo.value = serialNo;
	
	var strSQL = "";
	strSQL = " select a.Certifycode A,b.CertifyName B," 
		+ " (select ordermoney from LZOrderCard where Serialno=a.Serialno and CertifyCode=a.CertifyCode) C,"
		+ " a.Amnt D, a.OrderState E, case a.OrderState when '1' then '未阅' when '2' then '打回' else '批准' end F,a.Amnt G"
		+ " from LZOrderDetail a,LMCertifyDes b "
		+ " where a.serialno = '"+serialNo+"' and a.CertifyCode=b.CertifyCode and OrderCom='"+fm.OrderCom.value+"' order by E"
		;
		
	CertifyTypeGrid.clearData();
	arrResult = easyExecSql(strSQL);
	
	if (arrResult)
	{
	  displayMultiline(arrResult,CertifyTypeGrid);	
	}
	else
	{
		alert("该机构暂时没有订单！");
		return false;
	}
	strSQL = "select Note from LZOrder where SerialNo = '"+serialNo+"'";
	arrResult = easyExecSql(strSQL);
	fm.Note.value = arrResult[0];
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
  	CertifyTypeGrid.clearData();
  	//SerialGrid.clearData();
	  //initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function requestClick()
{
  showInfo = window.open("./CertifyTypeQuery.jsp");
}

