//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量


//毕录项校验
function chkMustInput()
{
   var strReturn ="";   
   //单证类型码校验  
   if( verifyInput() == false){
   	return false;
  }
  return true;
}
//批次号码生成校验
function chkExistPrt()
{
   var strReturn ="";   
   //单证类型码校验 
  
   var sql = "select count(*) from lzcardnumber where  CardType='" + fm.all('CardType').value + "' and PRTNO='" +fm.all('PrtNo').value + "'"  ;
   var rs = easyExecSql(sql);   

  if(rs && rs[0][0] != "" && rs[0][0] != "null" && rs[0][0] != 0)  { 
  	
  	 strReturn ="该批次的号码已生成！";	    
  }  	
  return strReturn;    
}
//毕录项校验
function chkNotExistPrt()
{
   var strReturn ="";   
   //单证类型码校验 
  
   var sql = "select count(*) from lzcardnumber where  CardType='" + fm.all('CardType').value + "' and PRTNO='" +fm.all('PrtNo').value + "'"  ;
   var rs = easyExecSql(sql);   

  if(rs && rs[0][0] != "" && rs[0][0] != "null" && rs[0][0] != 0)  {  	
  }else{
  	 strReturn ="该批次的号码未生成！";	    
  }  	
  return strReturn;    
}

//毕录项校验
function chkConfirm(){
   
   //单证类型码校验
  var strReturn = ""; 
  var sql = "select count(*) from lzcardnumber where  CardType='" + fm.all('CardType').value + "' and PRTNO='" +fm.all('PrtNo').value + "'"
  + " and PrintFlag ='1'"  ;
  var rs = easyExecSql(sql);   

  if(rs && rs[0][0] != "" && rs[0][0] != "null" && rs[0][0] != 0)  { 
  	
  	 strReturn="该批次的号码成功下载过！";	   
  }  	
  return strReturn;    
}

/**
 * 将字符串补数,将sourString的<br>前面</br>用cChar补足cLen长度的字符串,如果字符串超长，则不做处理
 * <p><b>Example: </b><p>
 * <p>LCh("Minim", "0", 10) returns "00000Minim"<p>
 * @param sourString 源字符串
 * @param cChar 补数用的字符
 * @param cLen 字符串的目标长度
 * @return 字符串
 */
function LCh(sourString, cChar,cLen) {
	
	 var sourString = sourString+ "";
   var tLen = sourString.length;    
   
   var i=0;   
   var iMax=0;
   var tReturn = "";
   if (tLen >= cLen) {
       return sourString;
   }
   iMax = cLen - tLen;
   for (i = 0; i < iMax; i++) {
       tReturn = cChar + tReturn;
   }
   tReturn = trim(tReturn) + trim(sourString);   
   return tReturn;
}

