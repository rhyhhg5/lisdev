/*********************************************************************
 *  程序名称：LRReInsureAnswerInput.js
 *  程序功能：再保回复
 *  创建日期：2007-09-13 
 *  创建人  ：huxl
 *  返回值：  无
 *  更新记录：  更新人    更新日期     更新原因/内容
 *********************************************************************
 */
var arrResult1 = new Array();
var arrResult2 = new Array();
var arrResult3 = new Array();
var arrResult4 = new Array();
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var temp = new Array();
var mOperate="";
var ImportPath;
var oldDataSet ; 
var InputFlag="0";
//alert(PrtNo);
window.onfocus=myonfocus;

/**
	*查询核保发起任务
	*/
function QueryRiskInfo()
{
 	var tPrtNo = fm.PrtNo.value;
 	
  mSQL = "select A,B,C,D,E,F,div(D,F),div(E,F),G,H from "
				+ "(select a.GrpName A,a.RiskCode B,a.Mult C,a.Amnt D,a.Prem E,(select count(*) from lcpol where PrtNo = a.PrtNo "
				+ "and GrpPolNo = a.GrpPolNo) F, "
				+ "(select case (select state from lcreinsurtask where uwno= (select max(uwno) from lcreinsurtask  "
				+ "where polno=a.GrpPolNo) and polno=a.GrpPolNo) when '00' then '待回复' when '01' then '已回复' "
 				+ "when '02' then '办结' end from dual) G, a.GrpPolNo H "
				+ "from lcgrppol a where a.Prtno ='"+ tPrtNo
				+ "' and exists (select 1 from lcpol where prtno = a.PrtNo and GrpPolNo = a.GrpPolNo and ReinsureFlag='2'))  "
				+ "as X with ur ";
	turnPage1.queryModal(mSQL, RiskInfoGrid);
}

/**
	*查询任务发起与回复信息
	*/
 function  QueryReInsureAudit()
 {
 	//这里复用个险的再保契约意见表和再保回复信息表，所以用PolNo存储GrpPolNo. made by huxl @ 20070913
 	var tSel=RiskInfoGrid.getSelNo();
	var tPrtNo = fm.PrtNo.value;
	mSql = " (select b.GrpName,b.Riskcode,a.uwno,a.uwoperator,a.uwidea,a.makedate,a.adjunctpath,a.PolNo,'1','核保发送',case when a.adjunctpath is null then '无' else '有' end "
		+" from lcreinsuruwidea a ,LCGrpPol b where a.PolNo='"+RiskInfoGrid.getRowColData(tSel-1,10)
		+"' and b.prtno='" + tPrtNo +"' and a.polno=b.GrpPolno "
		+" union "
		+" select b.GrpName,b.Riskcode,a.uwno,a.operator,a.uwidea,a.makedate,a.adjunctpath,a.PolNo,'2','再保回复',case when a.adjunctpath is null then '无' else '有' end "
		+" from lcreinsuridea a ,LCGrpPol b where a.polno='"+RiskInfoGrid.getRowColData(tSel-1,10)
		+"' and b.PrtNo='" + tPrtNo +"' and a.polno=b.GrpPolno ) order by uwno desc";	
	turnPage1.queryModal(mSql,ReInsureAuditGrid);
 }
 
 /**
	*查询发送回复信息
	*/
 function QueryAnswerIdea()
 {
 	var tSel=ReInsureAuditGrid.getSelNo();
 	var saFlag=ReInsureAuditGrid.getRowColData(tSel-1,9);
 	if(saFlag=="1") //如果‘核保发送’
 	{
	 	var mSql="select uwidea from lcreinsuruwidea where polno='"+ReInsureAuditGrid.getRowColData(tSel-1,8)+"'"
	 	+" and UWNo="+ReInsureAuditGrid.getRowColData(tSel-1,3)+""
	 	var arrResult=easyExecSql(mSql);
	 	if(arrResult!=null)
	 	{
	 		fm.SendAnswerRemark.value=arrResult[0];
	 	}
	}else
	{
		var mSql="select uwidea from lcreinsuridea where polno='"+ReInsureAuditGrid.getRowColData(tSel-1,8)+"'"
	 	+" and UWNo="+ReInsureAuditGrid.getRowColData(tSel-1,3)+""
	 	var arrResult=easyExecSql(mSql);
	 	if(arrResult!=null)
	 	{
	 		fm.SendAnswerRemark.value=arrResult[0];
	 	}
	}
 }
 	
function AutoReInsure()
{
	  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.action = "./AutoReInsureChk.jsp";
    fm.submit();
}
//本函数暂时没有用到 	
function 	ReInsureAudit(){
 var tContNo = fm.ContNo.value;
  mSQL ="select a.insuredname,(select riskcode from lcpol where polno=a.polno),a.uwerror from LCUWError a where a.contno ='" + tContNo +"' and SugPassFlag='R'";
	turnPage.queryModal(mSQL, ReInsureGrid);
}
 	
function SendUWReInsure()
{
	tSel = RiskInfoGrid.getSelNo();
	if(tSel==0||tSel==null)
	{
		alert("请先选择险种保单信息！");
		return false;
	}
  var tRemark = fm.Remark.value; //再保回复意见
  
  var	PolNo = RiskInfoGrid.getRowColData(tSel-1, 10);
  
  var tsql="select appflag from LCGrpPOL where GrpPolno='"+PolNo+"'";
  var arr=easyExecSql(tsql);
  if(arr[0]=="1")
  {
  	alert("该保单已经签单，不能进行再保回复！");
  	return false;
  }
	tsql ="select state from lcreinsurtask where uwno=(select max(uwno) from lcreinsurtask where polno='" +PolNo + "') and polno='" +PolNo + "'";
	arr=easyExecSql(tsql);	
	if(arr == null){
		alert("没有核保意见，不能进行再保回复!");
    return false;
	}
	if(arr=='01'||arr=='02') //如果是已回复状态
	{
    alert("该任务为已回复状态，不能再回复核保申请!");
     return false;
	}
	if(fmImport.all('FileName').value == null ||fmImport.all('FileName').value == ""){
		var showStr="正在保存再保回复意见……";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.action = "./AnswerUWReInsureChk.jsp?FilePath=&FileName=&PolNo="+PolNo+"&Remark="+fm.Remark.value+"&ContType=2";
		fm.submit(); 
	}else{ 
  	ReInsureUpload();
	}
}

function ReInsureUpload() {
  var i = 0;
  var tImportFile = fmImport.all('FileName').value;
  if ( tImportFile.indexOf("\\")>0 )
    tImportFile =tImportFile.substring(tImportFile.lastIndexOf("\\")+1);
  if ( tImportFile.indexOf("/")>0 )
    tImportFile =tImportFile.substring(tImportFile.lastIndexOf("/")+1);
  if ( tImportFile.indexOf("_")>0)
    tImportFile = tImportFile.substring( 0,tImportFile.indexOf("_"));
  if ( tImportFile.indexOf(".")>0)
    tImportFile = tImportFile.substring( 0,tImportFile.indexOf("."));

  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmImport.action = "./ReInsuUpLodeSave.jsp"; 
  fmImport.submit();
}

function DownLoad(){
	 tSel = ReInsureAuditGrid.getSelNo();
	 if(tSel==0||tSel==null)
	 {
			alert("请先选择再保审核任务信息！");
			return false;
	 }
	 
   var FilePath = ReInsureAuditGrid.getRowColData(tSel - 1, 7);  
   if (FilePath==""||FilePath==null){
	   alert("没有附件,不能进行下载操作！")	
	   return false;
   }   
   
   //alert(FilePath);
   //var showStr="正在下载数据……";
   //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   fmImport.action = "../uw/DownLoadSave.jsp?FilePath="+FilePath;
   fmImport.submit();
	}

function ReInsureOver(){
		  for (i=0; i<RiskInfoGrid.mulLineCount; i++)
  {
    if (RiskInfoGrid.getSelNo(i))
    {
      checkFlag = RiskInfoGrid.getSelNo();
      break;
    }
  }
  var	State = RiskInfoGrid.getRowColData(checkFlag - 1, 6);
  if (State!='已回复'){
  alert("该险种处于"+State+"状态不能进行办结");
  }
  
}
 		
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    location.reload(true);
  }
}

function UpLodeReInsure(FilePath,FileName){
	for (i=0; i<RiskInfoGrid.mulLineCount; i++)
  {
  	ImportFile = fmImport.all('FileName').value;
    if (RiskInfoGrid.getSelNo(i))
    {
      checkFlag = RiskInfoGrid.getSelNo();
      break;
    }
  }
	var	PolNo = RiskInfoGrid.getRowColData(checkFlag - 1,10);	
	fm.action = "./AnswerUWReInsureChk.jsp?FilePath="+FilePath+"&FileName="+FileName+"&PolNo="+PolNo+"&Remark="+fm.Remark.value+"&ContType=2";
	fm.submit(); 
}
   
function clearData(){ 
	fm.Remark.value=""; 
	InputFlag="1"; 
}

function showTaskInfo()
{
	QueryReInsureAudit();
}

function showAnswerIdea()
{
	QueryAnswerIdea();
}