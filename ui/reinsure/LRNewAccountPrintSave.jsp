<%
//程序名称：LRNewAccountPrintSave.jsp
//程序功能：
//创建日期：2008-11-15
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
 <%@page import="com.sinosoft.lis.f1print.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  GlobalInput globalInput = new GlobalInput( );
	globalInput=(GlobalInput)session.getValue("GI");
		//globalInput.ClientIP = request.getRemoteAddr();
	globalInput.ClientIP = request.getHeader("X-Forwarded-For");
		if(globalInput.ClientIP == null || globalInput.ClientIP.length() == 0) { 
		   globalInput.ClientIP = request.getRemoteAddr(); 
		}
	globalInput.ServerIP =globalInput.GetServerIP();
  PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
  CErrors tError = null;

  String FlagStr = "";
  String Content = "";
  String mYear= request.getParameter("Year");
  String mMonth= request.getParameter("Month");

  String tChk[]=request.getParameterValues("InpNewAccountGridChk");
  String tActuGetNo[] = request.getParameterValues("NewAccountGrid15");
  String tPayDate[] = request.getParameterValues("NewAccountGrid12");
  String tOperator[] = request.getParameterValues("NewAccountGrid13");
  LOPRTManagerSet tLOPRTManagerSet=new LOPRTManagerSet();
  if(tChk!=null){
  	for(int i=0;i<tChk.length;i++){
			if(tChk[i]!=null && tChk[i].equals("1")){
				LOPRTManagerSchema tLOPRTManagerSchema=new LOPRTManagerSchema();
				tLOPRTManagerSchema.setOtherNo(tActuGetNo[i]);//
				tLOPRTManagerSchema.setStandbyFlag1(tPayDate[i]);
				tLOPRTManagerSchema.setReqOperator(tOperator[i]);
                tLOPRTManagerSchema.setOtherNoType("19");
                tLOPRTManagerSchema.setCode("zbzddy01");
				tLOPRTManagerSet.add(tLOPRTManagerSchema);
			}
		}
	}
  

//	TransferData tTransferData = new TransferData();
 	
 	//查询条件
// 	tTransferData.setNameAndValue("Year",mYear);
// 	tTransferData.setNameAndValue("Month",mMonth);


    VData tVData = new VData();
//  	tVData.addElement(tTransferData);
  	tVData.addElement(globalInput);
  	tVData.addElement(tLOPRTManagerSet);
  if(!tPDFPrintBatchManagerBL.submitData(tVData,"batch")){
		Content = tPDFPrintBatchManagerBL.mErrors.getFirstError().toString();
    	FlagStr = "true";	
	}
	else{	
      Content = "打印成功！";
      FlagStr = "Fail";
    }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>


