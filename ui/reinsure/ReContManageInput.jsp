<html>
<%
//name :ReComManage.jsp
//function :ReComManage
//Creator :
//date :2006-08-14
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.reinsure.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "./ReContManageInput.js"></SCRIPT> 
<%@include file="./ReContManageInit.jsp"%>
</head>
<body  onload="initElementtype();initForm()" >
  <form action="" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  
   <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divLLReport1);"></td>
    	<td class= titleImg>基本信息</td></tr>
    </table>
  
  <Div id= "divLLReport1" style= "display: ''">
	<br>
   	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		合同编号
   			</TD>
   			<TD class= input>
   				<Input class= common name="ReContCode" id="ReContCodeId" verify="合同编号|NOTNULL" elementtype=nacessary> 
   			</TD>
   			<TD class= title>
          合同名称
        </TD>
        <TD class= input colspan=3>
        	<Input class= common name= ReContName id="ReContNameId" style="width:98%"> 
        </TD>
      </TR> 
      
      <TR>
      	<TD class= title>
   		 		再保公司编号
   			</TD>
   			<TD class= input >
   				<input class="code" name="ReComCode" 
	            ondblclick="return showCodeList('reinsurecomcode',[this,ReComName],[0,1],null,null,null,1,300);"
	            onkeyup="return showCodeListKey('reinsurecomcode', [this,ReComName],[0,1],null,null,null,1,300);" verify="再保公司编号|NOTNULL" elementtype=nacessary>
   			</TD>
   			<TD class= title>
          再保公司名称
        </TD>
        <TD class= input colspan=3>
        	<Input class="common" name= ReComName style="width:98%" readonly > 
        </TD>
      </TR>
      
      <TR>
      	<TD class= title>
   		 		合同生效日期
   			</TD>
   			<TD class= input >
   				<Input class= "coolDatePicker" name= RValidate verify="合同生效日期|DATE&NOTNULL" elementtype=nacessary>
   			</TD>
   			<TD class= title>
   				合同终止日期
        </TD>
        <TD class= input>
        	<Input class= "coolDatePicker" name= RInvalidate verify="合同终止日期|DATE">
        </TD>
        <td class="title">
        	合同录入日期
        </td>
        <td class="input">
        	<Input class= "coolDatePicker" name= InputDate verify="合同生效日期|DATE"> 
        </td>
      </TR>
      
       <TR>
      	<TD class= title>
   		 		险种类别
   			</TD>
   			<TD class= input>
   				<Input class="codeno" readOnly name= "DiskKind" CodeData="0|^1|短险成数^2|短险溢额^3|长险溢额^4|长险成数" 
          ondblClick="showCodeListEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);" verify="险种类别|NOTNULL"><Input 
          class= codename name= 'DiskKindName' elementtype=nacessary>
   			</TD>
   			<TD class= title>
   				分保方式
        </TD>
        <TD class= input>
        	<Input class="codeno" readOnly name= "CessionMode" CodeData="0|^1|成数^2|溢额" 
          ondblClick="showCodeListEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);" verify="分保方式|NOTNULL" ><Input 
          class= codename name= 'CessionModeName' elementtype=nacessary >
        </TD>
        <td class="title">
        	分保保费方式
        </td>
        <td class="input">
        	<Input class="codeno" readOnly name= "CessionFeeMode" CodeData="0|^1|风险保费方式^2|原始保费方式^3|修正共保方式" 
          ondblClick="showCodeListEx('CessionFeeMode',[this,CessionFeeModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionFeeMode',[this,CessionFeeModeName],[0,1],null,null,null,1);" verify="分保保费方式|NOTNULL"><Input 
          class= codename name= 'CessionFeeModeName' elementtype=nacessary>
        </td>
      </TR>
      
      <TR>
      	<TD class= title>
   		 		再保项目
   			</TD>
   			<TD class= input>
   				<Input class="codeno" readOnly name="ReinsurItem" CodeData="0|^L|法定分保^C|商业分保" 
          ondblClick="showCodeListEx('ReinsurItem',[this,ReinsurItemName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('ReinsurItem',[this,ReinsurItemName],[0,1],null,null,null,1);" verify="再保项目|NOTNULL" ><Input 
          class= "codename" name='ReinsurItemName' elementtype=nacessary>
   			</TD>
   			<TD class= title></TD>
        <TD class= input></TD>
        <td class="title"></td>
        <td class="input"></td>
      </TR>
   	</Table>
   	<br>
  
  <input class="cssButton" type="button" value="添加分保信息" onclick="addCessInfo()" >
  
  <input type="hidden" name="OperateType" >
   	
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>