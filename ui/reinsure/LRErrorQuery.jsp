<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRErrorQuery.jsp
//程序功能：退保减人查询
//创建日期：2007-04-8 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LRErrorQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRErrorQueryInit.jsp"%>
  <title>退保减人查询</title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "" >
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divErrorInfo);"></td>
          <td class="titleImg">请选择错误日志类型</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divErrorInfo" style= "display: ''">
    <table class=common>
     	<tr class=common>
  			<TD  class= title>
        	日志类型
        </TD>
        <TD  class= input>
        	<Input class=codeNo name=LogType CodeData="0|^1|批处理^2|新单提数^3|保全提数^4|理赔提数^5|新单计算^6|保全计算^7|理赔计算" 
          ondblclick="return showCodeListEx('LogType',[this,LogTypeName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKeyEx('LogType',[this,LogTypeName],[0,1]);"><input class=codename name=LogTypeName >
        </TD>
        <TD class= title>
          起始日期
        </TD>
        <TD class= input>
          <Input class=coolDatePicker name="StartDate" dateFormat='short' verify="起始日期|notnull&Date">
        </TD>
        <TD  class= title>
          终止日期
        </TD>
        <TD  class= input>
          <Input class=coolDatePicker name="EndDate" dateFormat='short' verify="终止日期|notnull&Date">
        </TD>
      </tr>
  	</table>   
  </Div>
  <INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
	<Div  id= "divErrorResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanErrorPolGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">  
	</Div>
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>