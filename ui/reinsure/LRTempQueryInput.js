//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
	var strSQL = "";
	strSQL="select * from LR"
	
	var arrResult = new Array();
		
	turnPage.queryModal(strSQL, ReContGrid);
  
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}

	parent.fraMain.rows = "0,0,0,0,*";
}

function ReturnData()
{
	if(fm.PageFlag.value=="CONT")
	{
		var tRow=ReContGrid.getSelNo();
  	if (tRow==0)
   	{
   		alert("请您先进行选择!");
  		return;
  	}
  	var strSQL = "select RecontCode, ReContName,a.ReComCode,b.ReComName,RValidate,RInvalidate, InputDate, "
			+ " DiskKind, case diskkind when '1' then '医疗' when '2' then '意外' when '3' then '重疾' end,"
			+ " CessionMode, case CessionMode when '1' then '成数' when '2' then '溢额' end,"
			+ " CessionFeeMode, case CessionFeeMode when '1' then '风险保费方式' when '2' then '原始保费方式' when '3' then '修正共保方式' end,"
			+ " ReinsurItem, case ReinsurItem when 'L' then '法定分保' when 'C' then '商业分保' end "
			+ " from LRContrInf a,LRReComInfo b where 1=1 and a.Recomcode=b.ReComCode "
  		+ " and RecontCode='"+ReContGrid.getRowColData(tRow-1,1)+"'"
  		;
  	
  	strArray=easyExecSql(strSQL);
  	
  	if (strArray==null)
  	{
  		alert("无法返回,该数据可能刚被删除!");
  		return false;
  	}
    top.opener.fm.all('RecontCode').value 				=strArray[0][0];
    top.opener.fm.all('ReContName').value 				=strArray[0][1];
    top.opener.fm.all('ReComCode').value	 				=strArray[0][2];
    top.opener.fm.all('ReComName').value	 				=strArray[0][3];
    top.opener.fm.all('RValidate').value 					=strArray[0][4];
    top.opener.fm.all('RInvalidate').value				=strArray[0][5];
    top.opener.fm.all('InputDate').value 					=strArray[0][6];
    top.opener.fm.all('DiskKind').value 					=strArray[0][7];
    top.opener.fm.all('DiskKindName').value 			=strArray[0][8];
    top.opener.fm.all('CessionMode').value 				=strArray[0][9];  
    top.opener.fm.all('CessionModeName').value 		=strArray[0][10];
    top.opener.fm.all('CessionFeeMode').value 		=strArray[0][11];
    top.opener.fm.all('CessionFeeModeName').value =strArray[0][12];
    top.opener.fm.all('ReinsurItem').value 				=strArray[0][13];
    top.opener.fm.all('ReinsurItemName').value 		=strArray[0][14];
	}else
	{
		var tRow=ReContGrid.getSelNo();
  	if (tRow==0)
   	{
   		alert("请您先进行选择!");
  		return;
  	}
		strSQL="select distinct FactorCode,FactorValue from LRCalFactorValue where RecontCode ='"+ReContGrid.getRowColData(tRow-1,1)+"'";
		strArray=easyExecSql(strSQL);
		
		if (strArray==null)
		{
			alert("无法返回,该数据可能刚被删除!");
  		return false;
		}
		
		for(var i=0;i<strArray.length;i++)
		{
			eval("top.opener.fm.all('"+"Fac"+strArray[i][0]+"').value="+strArray[i][1]);
		}
		
		strSQL="select distinct a.RiskCode,b.RiskName from LRCalFactorValue a,LMRisk b "
			+ " where RecontCode ='"+ReContGrid.getRowColData(tRow-1,1)+"' and a.RiskCode=b.RiskCode";
		strArray=easyExecSql(strSQL);
		
		top.opener.ContRiskGrid.clearData();
		
		if (strArray!=null)
		{
			for (var k=0;k<strArray.length;k++)
			{
				top.opener.ContRiskGrid.addOne("ContRiskGrid");
				top.opener.ContRiskGrid.setRowColData(k,1,strArray[k][0]);
				top.opener.ContRiskGrid.setRowColData(k,2,strArray[k][1]);
			}
		}
	}
  top.close();
}

function ClosePage()
{
	top.close();
}