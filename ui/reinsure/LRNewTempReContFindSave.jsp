<%
//程序名称：LRNewTempReContFindSave.jsp
//程序功能：
//创建日期：2008-08-17
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  ReNewTempReContFindUI mReNewTempReContFindUI = new ReNewTempReContFindUI();
  CErrors tError = null;

  String FlagStr = "";
  String Content = "";
  String mContType= request.getParameter("ContType");
  String mContNo= request.getParameter("ContNo");
  String mAppnt= request.getParameter("Appnt");
  String mStartDate= request.getParameter("StartDate");
  String mEndDate= request.getParameter("EndDate");
  String mReContNo= request.getParameter("ReContNo");
	String mOperateType = request.getParameter("OperateType");
  String tChk[]=request.getParameterValues("InpNewTempReContGridChk");
  String tTempContCode[] = request.getParameterValues("NewTempReContGrid1");
  LRTempCessContSet tLRTempCessContSet=new LRTempCessContSet();
  if(tChk!=null){
  	for(int i=0;i<tChk.length;i++){
			if(tChk[i]!=null && tChk[i].equals("1")){
				LRTempCessContSchema tLRTempCessContSchema=new LRTempCessContSchema();
				tLRTempCessContSchema.setTempContCode(tTempContCode[i]);
				tLRTempCessContSet.add(tLRTempCessContSchema);
			}
		}
	}

	TransferData tTransferData = new TransferData();
 	
 	//查询条件
 	tTransferData.setNameAndValue("ContType",mContType);
 	tTransferData.setNameAndValue("ContNo",mContNo);
 	tTransferData.setNameAndValue("Appnt",mAppnt);
 	tTransferData.setNameAndValue("StartDate",mStartDate);
 	tTransferData.setNameAndValue("EndDate",mEndDate);
 	tTransferData.setNameAndValue("ReContNo",mReContNo);

  VData tVData = new VData();
  try
  {
  	tVData.addElement(tTransferData);
  	tVData.addElement(globalInput);
  	tVData.addElement(tLRTempCessContSet);
    mReNewTempReContFindUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReNewTempReContFindUI.mErrors;
    if (!tError.needDealError())
    {
      Content = "保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<%
	if(mOperateType.equals("ONLOAD")){
		VData mResult = mReNewTempReContFindUI.getResult();
		String url=(String)mResult.get(0);
		System.out.println(url);
		%> 
		<HTML>
			<script language="javascript">
				parent.fraInterface.downAfterSubmit("<%=url%>");
				</script>
		</HTML>   
<%
 }else{
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
<%}%>

