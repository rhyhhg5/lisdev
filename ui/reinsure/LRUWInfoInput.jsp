<html>
<%
//程序名称：核保信息查询
//程序功能：
//创建日期：2006-09-22
//创建人  ：Zhang Bin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.bank.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="LRUWInfoInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<%@include file="LRUWInfoInit.jsp"%>
</head>

<body  onload="initForm();queryClick();">
<form action="" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
   <br>
   <table>
   	  <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWInfo);"></td>
    	<td class= titleImg>团体人工核保信息</td></tr>
    </table>

   <Div id= "divUWInfo" style= "display: ''">
   	<table  class= common>
    	<tr  class= common>
      	<td text-align: left colSpan=1>
  				<span id="spanGrpUWGrid" >
  				</span>
  			</td>
  		</tr>
   	</table>
   	<br>
   	<INPUT class=cssButton id="riskbutton" VALUE="关&nbsp;&nbsp;闭" TYPE=button onClick="ClosePage();">
  	<input type="hidden" name="PageFlag">
  	<input type="hidden" name="ProposalNos">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
