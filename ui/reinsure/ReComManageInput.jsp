<html>
<%
//name :ReComManage.jsp
//function :ReComManage
//Creator :
//date :2006-08-14
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.reinsure.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "./ReComManageInput.js"></SCRIPT> 
<%@include file="./ReComManageInit.jsp"%>
</head>
<body  onload="initElementtype();initForm()" >
  <form action="./ReComManageSave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  <Div id= "divLLReport1" style= "display: ''">
	<br>
   	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		公司代码
   			</TD>
   			<TD class= input>
   				<Input class= common name= ReComCode id="ReComCodeId" Readonly > 
   			</TD>
   			<TD class= title>
          公司名称
        </TD>
        <TD class= input colspan=3>
        	<Input class= common name= ReComName id="ReComNameId" style="width:96%" elementtype=nacessary> 
        </TD>
      </TR> 
      
      <TR>
      	<TD class= title>
   		 		公司简称
   			</TD>
   			<TD class= input >
   				<Input class= common name= SimpleName id="SimpleNameId"> 
   			</TD>
   			<TD class= title>
   				英文名
        </TD>
        <TD class= input>
        	<Input class= common name= EngName id="EngNameId"> 
        </TD>
        <td class="title">
        	邮政编码
        </td>
        <td class="input">
        	<Input class= common name= PostalCode id="PosttalCodeId"> 
        </td>
      </TR>
      
      <TR>
      	<TD class= title>
   		 		传真
   			</TD>
   			<TD class= input >
   				<Input class= common name= FaxNo id="FaxId"> 
   			</TD>
   			<TD class= title>
          通讯地址
        </TD>
        <TD class= input colspan=3>
        	<Input class= common name= Address id="AddressId" style="width:98%"> 
        </TD>
      </TR>
      
       <TR>
      	<TD class= title>
   		 		网址
   			</TD>
   			<TD class= input>
   				<Input class= common name= WebAddress id="WebAddressId"> 
   			</TD>
   			<TD class= title>
        </TD>
        <TD class= input>
        </TD>
        <td class="title"></td>
        <td class="input"></td>
      </TR>
   	</Table>
   	<br>
    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divCertifyType);"></td>
    	<td class= titleImg>联系人</td></tr>
    </table>
   	
   	<Div  id= "divCertifyType" style= "display: ''">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanRelateGrid" ></span>
        		</td>
      		</tr>
    	</table>
	</div>
	<br>
	<table>
    <tr>
      <td class=common>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSerialRemark);">
      </td>
  	<td class= titleImg>注释</td></tr>
  </table>
    
	<Div  id= "divSerialRemark" style= "display: ''">
		<TR  class= common>
			<TD  class= input colspan="6">
			    <textarea name="Note" cols="100%" rows="3"  class="common">
			    </textarea>
			</TD>
		</TR>
  </Div> 	
  
  <input type="hidden" name="OperateType" >
   	
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>