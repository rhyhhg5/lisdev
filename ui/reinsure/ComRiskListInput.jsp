<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：ComRiskListInput.jsp
//程序功能：
//创建日期：2008-9-1
//创建人  ：ll
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="ComRiskListInput.js"></SCRIPT>
	<%@include file="ComRiskListInit.jsp"%>
</head>

<body onload="initElementtype();initForm();">    
  <form action="" method=post name=fm target="fraSubmit" >
    <div style="width:200">
      <table class="common"> 
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">再保账单预估数据计算</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divCessGetData" style= "display: ''">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	        </TR>
	        <TR  class= common>
						<TD  class= title>机构代码</TD>
						<TD  class= input> 
	          	<Input class= "codeno"  name=MngCom  ondblclick="return showCodeList('hmhospitalmng',[this,MngCom_ch],[1,0],null,null,null,1,180);" onkeyup="return showCodeListKey('hmhospitalmng',[this,MngCom_ch],[1,0],null,null,null,1,180);" readonly verify="管理机构|NOTNULL" ><Input class=codename  name=MngCom_ch>
	          </TD>
	          <TD  class= title>再保合同</TD>
						<TD  class= input> 
	          	<Input class="codeno" readOnly name= "RecontCode" ondblClick="return showCodeList('RecontCode',[this,RecontCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RecontCode',[this,RecontCodeName],[0,1],null,null,null,1);"><Input class="codename" name= 'RecontCodeName' >
	          </TD>
	          <TD  class= title>再保险种</TD>
						<TD  class= input> 
	          	<Input class="codeno" readOnly name= "ReRiskCode" ondblClick="return showCodeList('ReRiskCode',[this,ReRiskCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('ReRiskCode',[this,ReRiskCodeName],[0,1],null,null,null,1);"><Input class="codename" name= 'ReRiskCodeName' >
	          </TD> 
	        </TR>  
	    </table>
	    <br>
		<INPUT class=cssButton  VALUE="分保数据计算" TYPE=button onClick="CalComRisk();">	
		</Div> 
  </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 