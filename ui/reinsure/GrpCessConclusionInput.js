var showInfo;

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		//if (fm.SaveFlag.value != "1")
		//{
		
		var tSel = PolGrid.getSelNo();
		if( tSel == 0 || tSel == null )
		{
			alert( "请先选择一条记录,再对整单确认。" );
			return false;
		}
		if( verifyInput() == true && PolGrid.checkValue("PolGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./GrpCessConclusionSave.jsp";
		  	fm.submit(); //提交
			}
			else
			{
			}
		}
		
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	var polCount = PolGrid.mulLineCount;
	var selNo=PolGrid.getSelNo()-1;
	if(PolGrid.getRowColData(selNo,10)==""||PolGrid.getRowColData(selNo,10)==null)
		{
			alert("没有保单险种号！");
			return false;
		}
	var strSQL="select appflag from lcgrppol where Grpproposalno='"+PolGrid.getRowColData(selNo,10)+"'";
	var arrResult=easyExecSql(strSQL);
	if(arrResult==null)
	{
			alert("没有查询到保单状态！");
			return false;
	}
	if(arrResult[0]=='0')
	{
			alert("保单未签单，不能下临分结论！");
			return false;
	}
	
	for(var i=0;i<polCount;i++)
	{
		if(selNo==i)
		{
			if(PolGrid.getRowColData(i,15)=="3")
			{
				if((PolGrid.getRowColData(i,11)==""||PolGrid.getRowColData(i,11)==null)&&(PolGrid.getRowColData(i,12)==""||PolGrid.getRowColData(i,12)==null))
				{
					alert("请录入第:"+PolGrid.getSelNo()+"行的自留额或分保比例!");
					return false;
				}
				if((PolGrid.getRowColData(i,11)!=""&&PolGrid.getRowColData(i,11)!=null)
						&&(PolGrid.getRowColData(i,12)!=""&&PolGrid.getRowColData(i,12)!=null))
				{
					alert("自留额与分保比例不能同时录入!"); 
					return false; 
				}
			}
			if(PolGrid.getRowColData(i,12)!=""&&PolGrid.getRowColData(i,12)!=null)
			{
				if (parseFloat(PolGrid.getRowColData(i,12))>1)
				{
					alert("分保比例不能大于1！");
					return false;
				}
			}
			if(PolGrid.getRowColData(i,13)!=""&&PolGrid.getRowColData(i,13)!=null)
			{
				if (parseFloat(PolGrid.getRowColData(i,13))>1)
				{
					alert("手续费比例不能大于1！");
					return false;
				}
			}
			if(PolGrid.getRowColData(i,15)==null||PolGrid.getRowColData(i,15)=="")
			{
				alert("再保结论不能为空！");
				return false;
			}
		}
	}
	return true;
}

/*********************************************************************
 *  查询团体单个人临分信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function indivTempCessQuery()
{
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录。" );
		return false;
	}
	var grpProposalNo = PolGrid.getRowColData(tSel-1, 10);
  window.open("./FrameMainIndivCessInfo.jsp?GrpProposalNo="+grpProposalNo+"&ModifyFlag="+fm.ModifyFlag.value,"true"); 
}

/*********************************************************************
 *  修改团体单个人临分信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function modifyIndivTempCessQuery()
{
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录。" );
		return false;
	}
	var grpProposalNo = PolGrid.getRowColData(tSel-1, 10);
	var modifyFlag = fm.ModifyFlag.value;
	var riskCode = PolGrid.getRowColData(tSel-1, 4);
  window.open("./FramCessConclusion.jsp?LoadFlag=6&GrpProposalNo="+grpProposalNo+"&ModifyFlag=2&RiskCode="+riskCode,"CessConclusion");
}

function easyQuery()
{	
	var strSQL = "select A 参保人数,div(A,B) 参保比例,div(D2,A) 退休比例, F 投保险种,G 投保险种名称,H 标准保费,I 录入保费, div(i,a) 人均保费,case when k=1 then 'N/A' else varchar(div(I,H)) end  折扣比例,L "
		+ " from ( "
		+ " select (select count(*) from LCpol where grpcontno=Y.grpcontno and riskcode=Y.riskcode) A "
		+ " ,(select count(*) from LCpol where grpcontno=Y.grpcontno and riskcode=Y.riskcode) B "
		+ " ,(select count(1) from LCInsured where GrpContNo=Y.GrpContNo and InsuredStat='2') D2 "
		+ " ,Y.riskcode F "
		+ " ,(select RiskName from lmrisk where riskcode=Y.riskcode) G "
		+ " ,(select sum(StandPrem) From lcpol where grppolno=Y.grppolno) H "
		+ " ,(select sum(prem) From lcpol where grppolno=Y.grppolno) I "
		+ " ,(select distinct 1 from LCDuty m where m.standprem = 0 and PolNo in (select PolNo from LCPol n where n.GrpContNo=Y.GrpContNo and RiskCode=Y.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Y.GrpContNo ))) k "
		+ " ,(select distinct grpproposalno from LCgrppol where ProposalGrpContNo=Z.ProposalGrpContNo and grpproposalno=Y.grpproposalno) L"
		+ " From lcgrppol Y,lcgrpcont Z "
		+ " where Z.ProposalGrpContNo='"+fm.ProposalContNo.value+"' and Z.grpcontno=Y.grpcontno "
		+ " ) as x "
	;
	turnPage.queryModal(strSQL, PolGrid); 
}

function UWQuery()
{
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录。" );
		return false;
	}
	
	var prtNo = fm.PrtNo.value;
	
  window.open("./FrameGrpUWInfo.jsp?PrtNo="+prtNo,"true"); 
}

function showGrpTempCess()
{
	var prtNo = fm.PrtNo.value;
	window.open("./FrameGrpUWAnswer.jsp?PrtNo="+prtNo,"true"); 
}

function queryClick()
{
	alert("只能显示本合同的分保信息"); 
  //fm.OperateType.value="QUERY"; 
  //window.open("./FrameReContQuery.jsp?Serial="+fm.ReContCode.value+"&PageFlag=CESS"); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  fm.SaveFlag.value = "1";
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  //resetForm();
  }
}

function updateClick()
{
	if(!confirm("你确定要修改该合同的分保信息吗？"))
	{
		return false;
	}
	
	fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true && PolGrid.checkValue("PolGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./CessInfoSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	return true;
}

function deleteClick()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该合同的分保信息吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true ) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.action="./CessInfoSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	return true;
}

function afterQuery()
{
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}     
      
function initModify() 
{
	if(fm.ModifyFlag.value=="1")
	{
		var tSel=PolGrid.getSelNo();
		var strSQL="select a.RecontCode,a.RecontName,a.RecomCode,(select RecomName from LRReComInfo where RecomCode=a.RecomCode) "
		+ " from LRTempCessCont a where a.RecontCode= "
		+ "(select distinct RecontCode from LRTempCessContInfo where PrtNo='"+fm.PrtNo.value+"' and RiskCode='"+PolGrid.getRowColData(tSel-1,4)+"' and CalMode is null)";
		var arrResult=easyExecSql(strSQL);
		if(arrResult==null)
		{
			fm.ReContCode.value	="";
			fm.ReContName.value	="";
			fm.ReComCode.value	="";
			fm.ReComName.value	="";
			alert("该团单中没有针对此险种的团体临分协议，请点击[个人临分自检信息]按钮查看特殊保单的临分协议！");
			return false;
		}
		fm.ReContCode.value=arrResult[0][0];
		fm.ReContName.value=arrResult[0][1];
		fm.ReComCode.value=arrResult[0][2];
		fm.ReComName.value=arrResult[0][3];
	}
}

function easyQueryClick()
{
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录,再对整单确认。" );
		return false;
	}
	
	var grpproposalNo = PolGrid.getRowColData(tSel-1,10);
 	window.open("./FrameGrpTempContInfo.jsp?GrpProposalNo="+grpproposalNo,"WindowsGrpTemp"); 
}

function returnClick()
{
	top.close();
}