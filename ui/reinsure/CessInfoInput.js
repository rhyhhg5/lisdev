var showInfo;

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true && ContRiskGrid.checkValue("ContRiskGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./CessInfoSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	if (fm.CessionMode.value=="1")
	{
		if (fm.FacCessionRate.value==null||fm.FacCessionRate.value=="")
		{
			alert("成数分保，分保比例不能为空！");
			return false;
		}
	}
	if (fm.CessionMode.value=="2")
	{
		if(fm.FacRetainAmnt.value==null||fm.FacRetainAmnt.value=="")
		{
			alert("溢额分保，自留额不能为空！");
			return false;
		}
	}
	if(fm.FacRetainAmnt.value!=""&&fm.FacRetainAmnt.value!=null)
	{
		if (!isNumeric(fm.FacRetainAmnt.value))
		{
			alert("录入自留额格式不对！");
			return false;
		}
	}
	if(fm.FacCessionRate.value!=""&&fm.FacCessionRate.value!=null)
	{
		if (!isNumeric(fm.FacCessionRate.value))
		{
			alert("录入分保比例格式不对！");
			return false;
		}
	}
	if(fm.FacAccQuot.value!=""&&fm.FacAccQuot.value!=null)
	{
		if (!isNumeric(fm.FacAccQuot.value))
		{
			alert("录入接受份额格式不对！");
			return false;
		}
		if (parseFloat(fm.FacAccQuot.value)>1)
		{
			alert("接受份额不能大于1！");
			return false;
		}
	}
	if(fm.FacAutoLimit.value!=""&&fm.FacAutoLimit.value!=null)
	{
		if (!isNumeric(fm.FacAutoLimit.value))
		{
			alert("录入自动接受限额格式不对！");
			return false;
		}
	}
	if(fm.FacAutoLimit.value!=""&&fm.FacAutoLimit.value!=null)
	{
		if (!isNumeric(fm.FacProfitCom.value))
		{
			alert("盈余佣金比例录入格式不对！");
			return false;
		}
		if (parseFloat(fm.FacProfitCom.value)>1)
		{
			alert("盈余佣金比例不能大于1！");
			return false;
		}
	}
	if(fm.FacLowReAmnt.value!=""&&fm.FacLowReAmnt.value!=null)
	{
		if (!isNumeric(fm.FacLowReAmnt.value))
		{
			alert("录入最低再保保额格式不对！");
			return false;
		}
	}
	if(fm.FacReinManFeeRate.value!=""&&fm.FacReinManFeeRate.value!=null)
	{
		if (!isNumeric(fm.FacReinManFeeRate.value))
		{
			alert("录入管理费比例格式不对！");
			return false;
		}
		if (parseFloat(fm.FacReinManFeeRate.value)>1)
		{
			alert("管理费比例不能大于1！");
			return false;
		}
	}
	if(fm.FacReinProcFeeRate.value!=""&&fm.FacReinProcFeeRate.value!=null)
	{
		if (!isNumeric(fm.FacReinProcFeeRate.value))
		{
			alert("录入手续费比例格式不对！");
			return false;
		}
		if (parseFloat(fm.FacReinProcFeeRate.value)>1)
		{
			alert("手续费比例不能大于1！");
			return false;
		}
	}
	if(fm.FacChoiRebaRate.value!=""&&fm.FacChoiRebaRate.value!=null)
	{
		if (!isNumeric(fm.FacChoiRebaRate.value))
		{
			alert("录入选择折扣比例格式不对！");
			return false;
		}
		if (parseFloat(fm.FacChoiRebaRate.value)>1)
		{
			alert("选择折扣比例不能大于1！");
			return false;
		}
	}
	if(fm.FacSpeComRate.value!=""&&fm.FacSpeComRate.value!=null)
	{
		if (!isNumeric(fm.FacSpeComRate.value))
		{
			alert("录入特殊佣金比例不对！");
			return false;
		}
		if (parseFloat(fm.FacSpeComRate.value)>1)
		{
			alert("特殊佣金比例不能大于1！");
			return false;
		}
	}
	if(fm.FacClaimAttLmt.value!=""&&fm.FacClaimAttLmt.value!=null)
	{
		if (!isNumeric(fm.FacClaimAttLmt.value))
		{
			alert("录入理赔参与限额不对！");
			return false;
		}
	}
	if(fm.FacNoticeLmt.value!=""&&fm.FacNoticeLmt.value!=null)
	{
		if (!isNumeric(fm.FacNoticeLmt.value))
		{
			alert("录入通知限额不对！");
			return false;
		}
	}
	if (ContRiskGrid.mulLineCount==0)
	{
		alert("请录入合同险种！");
		return false;
	}
	return true;
}

function queryClick()
{
	alert("只能显示本合同的分保信息"); 
  //fm.OperateType.value="QUERY"; 
  //window.open("./FrameReContQuery.jsp?Serial="+fm.ReContCode.value+"&PageFlag=CESS"); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  resetForm();
  }
}

function updateClick()
{
	if(!confirm("你确定要修改该合同的分保信息吗？"))
	{
		return false;
	}
	
	fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true && ContRiskGrid.checkValue("ContRiskGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./CessInfoSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	return true;
}

function deleteClick()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该合同的分保信息吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true ) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.action="./CessInfoSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	return true;
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function initInfo() 
{
  if (fm.ReContCode.value==null||fm.ReContCode.value=="")
  {
  	alert("没有合同编码信息!");
  	return;
  }
	strSQL="select distinct FactorCode,FactorValue from LRCalFactorValue where RecontCode ='"+fm.ReContCode.value+"'";
	strArray=easyExecSql(strSQL);
	if (strArray!=null)
	{
		for(var i=0;i<strArray.length;i++)
		{
			if(strArray[i][1]==null||strArray[i][1]=="")
			{
				continue;
			}
			eval("fm.all('"+"Fac"+strArray[i][0]+"').value="+strArray[i][1]);
		}	
		strSQL="select distinct a.RiskCode,b.RiskName from LRCalFactorValue a,LMRisk b "
			+ " where RecontCode ='"+fm.ReContCode.value+"' and a.RiskCode=b.RiskCode";

		strArray=easyExecSql(strSQL);
		ContRiskGrid.clearData(); 
		if (strArray!=null)
		{
			for (var k=0;k<strArray.length;k++)
			{
				ContRiskGrid.addOne("ContRiskGrid");
				ContRiskGrid.setRowColData(k,1,strArray[k][0]);
				ContRiskGrid.setRowColData(k,2,strArray[k][1]);
			}
		}
	}
}

function amntRisk()
{
	if (fm.ReContCode.value==''||fm.ReContCode.value==null)
	{
		alert("请先查询再保合同再添加分保信息！");
		return false;
	}
	var strSQL="select ReContCode from LRContInfo where ReContCode='"+fm.ReContCode.value+"'";
	var arrResult=easyExecSql(strSQL);
	if (arrResult==null||arrResult=="")
	{
		alert("合同编号为空！");
		return false;
	}
	
  var varSrc="&ReContCode="+ fm.ReContCode.value+"&CessionMode="+fm.CessionMode.value+"&RiskSort="+fm.RiskSort.value ;
  window.open("./FrameAmntRiskInfo.jsp?"+varSrc,"AmntRisk"); 
}

function returnClick()
{
	top.close();
}