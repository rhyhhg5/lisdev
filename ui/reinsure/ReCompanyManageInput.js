var showInfo;

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;


//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true ) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReCompanyManageSave.jsp";
		  	fm.submit(); //提交
	  	   }	  
	  }
  } catch(ex) {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	if(fm.ReComName.value==""||fm.ReComName.value==null)
	{
		alert("请录入公司名称！");
		return false;
	}
	if (!isTel(fm.FaxNo.value))
	{
		alert("传真录入格式不对！");
		return false;
	}
	if (!isInteger1(fm.PostalCode.value))
	{
		alert("邮政编码录入格式不对！");
		return false;
	}		
	if (''==fm.SAPAppntNo.value||null==fm.SAPAppntNo.value)
	{
		alert("SAP客户号不能为空！");
		return false;
	}	
	return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  if(fm.OperateType.value=="DELETE"){
	  	initElemnt();
	  }
	  initForm();
  }
}

//修改公司信息
function updateForm()
{
	fm.OperateType.value="UPDATE";
	try 
	{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReCompanyManageSave.jsp";
		  	fm.submit(); //提交
	  	 }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	if (fm.ReComCode.value==""||fm.ReComCode.value==null)
	{
		alert("请先查询公司信息！");
		return false;
	}
	var strSQL = "select * from LRReComInfo where ReComCode='"+fm.ReComCode.value+"'";
	var strResult = easyExecSql(strSQL);
	
	if (strResult == null)
	{
		alert("该再保公司不存在！");
		return false;
	}
	if(fm.ReComName.value==""||fm.ReComName.value==null)
	{
		alert("请录入公司名称！");
		return false;
	}
	if (!isTel(fm.FaxNo.value))
	{
		alert("传真录入格式不对！");
		return false;
	}
	if (!isInteger1(fm.PostalCode.value))
	{
		alert("邮政编码录入格式不对！");
		return false;
	}
	if (''==fm.SAPAppntNo.value||null==fm.SAPAppntNo.value)
	{
		alert("SAP客户号不能为空！");
		return false;
	}
	return true;
	
}
//删除选中的公司
function deleteForm()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该再保公司吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.action="./ReCompanyManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	if (fm.ReComCode.value==""||fm.ReComCode.value==null)
	{
		alert("请先查询需要删除的公司信息");
		return false;
	}
	
	//校验该再保公司是否存在再保合同。合同部分还未开发，验证SQL需要修改！！！！！！！！！！！！！！
	var recont = "select count(*) from LRContInfo where ReComCode = '"+fm.ReComCode.value+"' with ur";
	var result = easyExecSql(recont);
	if(result[0][0] != "0"){
		if(confirm("该再保公司存在有效再保合同，确认需要删除吗？")){
			alert("请先维护该公司下的再保合同！");
		}
	}	
	return true;
}


 /**
   * 对输入域是否是邮编的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isInteger1(strValue)
{
  var NUM="0123456789";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(NUM.indexOf(strValue.charAt(i))<0) return false;

  }
  return true;
}

 /**
   * 对输入域是否是电话的校验
   * <p><b>Example: </b><p>
   * <p>isInteger("Minim") returns false<p>
   * <p>isInteger("123") returns true<p>
   * @param strValue 输入数值表达式或字符串表达式
   * @return 布尔值（true--是整数, false--不是整数）
   */
function isTel(strValue)
{
  var NUM="0123456789-() ";
  var i;
  if(strValue==null || strValue=="") return true;
  for(i=0;i<strValue.length;i++)
  {
    if(NUM.indexOf(strValue.charAt(i))<0) return false;

  }
  return true;
}
/**
*再保公司详细信息显示
**/
function OneQuery(){
    var selno = CompanyGrid.getSelNo();
	var ReComCode = CompanyGrid.getRowColData(CompanyGrid.getSelNo()-1,1);	
	var showOne = "select recomcode,recomname,SimpName,postalcode,faxno,address,sapappntno from LRReComInfo where recomcode = '"+ReComCode+"' with ur";
	var result = easyExecSql(showOne);
	fm.ReComCode.value = result[0][0];
	fm.ReComName.value = result[0][1];
	fm.SimpleName.value = result[0][2];
	fm.PostalCode.value = result[0][3];
	fm.FaxNo.value = result[0][4];
	fm.Address.value = result[0][5];
	fm.SAPAppntNo.value = result[0][6];
}