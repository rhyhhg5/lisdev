var showInfo;
var turnPage = new turnPageClass(); 



//查询合同信息
function QueryInfo(){

	var strSQL = "select a.riskcode,a.riskname,a.startdate,a.enddate," +
			"case when(select count(1) from lrcalfactorvalue where riskcode=a.riskcode " +
			"or riskcode in " +
			"(select getdutycode from lmdutygetrela where dutycode " +
			"in (select dutycode from lmriskduty where riskcode=a.riskcode)) )=0" +
			"  then '未分保' else '已分保' end,a.riskperiod, " +
			"(select CessionMode from lrcontinfo where recontcode " +
			"in (select recontcode from lrcalfactorvalue where riskcode=a.riskcode or riskcode in " +
			"(select getdutycode from lmdutygetrela where dutycode " +
			"in (select dutycode from lmriskduty where riskcode=a.riskcode))) fetch first 1 rows only)," +
			"(select count(distinct (RecontCode)) from lrcalfactorvalue where riskcode=a.riskcode " +
			"or riskcode in " +
			"(select getdutycode from lmdutygetrela where dutycode " +
			"in (select dutycode from lmriskduty where riskcode=a.riskcode))) " +
			"from lmriskapp a where 1=1  " 
	  
	  + getWherePart("a.riskperiod","DiskKind")
	 
	  ;
	if(fm.ReContState.value=='01')
		strSQL = strSQL +" and a.riskcode in (select riskcode from lrcalfactorvalue where riskcode=a.riskcode " +
				"or riskcode in " +
				"(select getdutycode from lmdutygetrela where dutycode " +
				"in (select dutycode from lmriskduty where riskcode=a.riskcode))) ";
	else if (fm.ReContState.value=='02')
		strSQL = strSQL +" and a.riskcode not in (select riskcode from lrcalfactorvalue where riskcode=a.riskcode " +
				 "or riskcode in " +
				 "(select getdutycode from lmdutygetrela where dutycode " +
				 "in (select dutycode from lmriskduty where riskcode=a.riskcode))) ";
	
	if(fm.ReComCode.value!='')
		strSQL = strSQL +" and a.riskcode in (select riskcode " +
				"from lrcalfactorvalue where ReComCode='"+fm.ReComCode.value+"') ";
	
	if(fm.CessionMode.value!='')
		strSQL = strSQL +" and a.riskcode in (select riskcode from lrcalfactorvalue where riskcode=a.riskcode and recontcode in (select recontcode " +
				"from lrcontinfo where CessionMode='"+fm.CessionMode.value+"')" +
		" or riskcode in " +
		"(select getdutycode from lmdutygetrela where dutycode " +
		"in (select dutycode from lmriskduty where riskcode=a.riskcode)) and recontcode in (select recontcode " +
				"from lrcontinfo where CessionMode='"+fm.CessionMode.value+"')) ";
		
	
	strSQL = strSQL +" group by  a.riskcode,a.riskname,a.startdate,a.enddate,a.riskperiod with ur";
	
	turnPage.queryModal(strSQL, ContGrid);
}

function DownLoad(){

	 // 书写SQL语句
	var strSQL = "select a.riskcode,a.riskname,a.startdate,a.enddate," +
	"case when(select count(1) from lrcalfactorvalue where riskcode=a.riskcode " +
	"or riskcode in " +
	"(select getdutycode from lmdutygetrela where dutycode " +
	"in (select dutycode from lmriskduty where riskcode=a.riskcode)) )=0" +
	"  then '未分保' else '已分保' end,a.riskperiod, " +
	"(select CessionMode from lrcontinfo where recontcode " +
	"in (select recontcode from lrcalfactorvalue where riskcode=a.riskcode or riskcode in " +
	"(select getdutycode from lmdutygetrela where dutycode " +
	"in (select dutycode from lmriskduty where riskcode=a.riskcode))) fetch first 1 rows only)," +
	"(select count(distinct (RecontCode)) from lrcalfactorvalue where riskcode=a.riskcode " +
	"or riskcode in " +
	"(select getdutycode from lmdutygetrela where dutycode " +
	"in (select dutycode from lmriskduty where riskcode=a.riskcode))) " +
	"from lmriskapp a where 1=1  " 

	+ getWherePart("a.riskperiod","DiskKind")

	;
	if(fm.ReContState.value=='01')
		strSQL = strSQL +" and a.riskcode in (select riskcode from lrcalfactorvalue where riskcode=a.riskcode " +
				"or riskcode in " +
				"(select getdutycode from lmdutygetrela where dutycode " +
				"in (select dutycode from lmriskduty where riskcode=a.riskcode))) ";
	else if (fm.ReContState.value=='02')
		strSQL = strSQL +" and a.riskcode not in (select riskcode from lrcalfactorvalue where riskcode=a.riskcode " +
		 		"or riskcode in " +
		 		"(select getdutycode from lmdutygetrela where dutycode " +
		 		"in (select dutycode from lmriskduty where riskcode=a.riskcode))) ";

	if(fm.ReComCode.value!='')
		strSQL = strSQL +" and a.riskcode in (select riskcode " +
			"from lrcalfactorvalue where ReComCode='"+fm.ReComCode.value+"') ";

	if(fm.CessionMode.value!='')
		strSQL = strSQL +" and a.riskcode in (select riskcode from lrcalfactorvalue where riskcode=a.riskcode and recontcode in (select recontcode " +
			"from lrcontinfo where CessionMode='"+fm.CessionMode.value+"')" +
			" or riskcode in " +
			"(select getdutycode from lmdutygetrela where dutycode " +
			"in (select dutycode from lmriskduty where riskcode=a.riskcode)) and recontcode in (select recontcode " +
			"from lrcontinfo where CessionMode='"+fm.CessionMode.value+"')) ";


	strSQL = strSQL +" group by  a.riskcode,a.riskname,a.startdate,a.enddate,a.riskperiod with ur";
    fm.querySql.value = strSQL;
    fm.action = "ReRiskQuerySave.jsp";
    fm.submit();
}