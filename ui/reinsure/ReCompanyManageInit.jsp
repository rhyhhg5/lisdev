<html>
<%
//name :ReComManageInit.jsp
//function :Manage 
//Creator :liuli
//date :2008-10-16
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 		GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
 		String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
 %>
<script language="JavaScript">
function initForm()
{
  try
  {
    initCompanyGrid();
    initSelect();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCompanyGrid() 
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="公司编码";
    iArray[1][1]="30px";
    iArray[1][2]=100;
    iArray[1][3]=1;
    
    iArray[2]=new Array();
    iArray[2][0]="SAP客户号";         	//列名
    iArray[2][1]="40px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[3]=new Array();
    iArray[3][0]="公司名称";         	//列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
            
    iArray[4]=new Array();
    iArray[4][0]="公司简称";         			//列名
    iArray[4][1]="60px";            	//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=1;              			//2表示是代码选择

    iArray[5]=new Array();
    iArray[5][0]="通讯地址";         				//列名
    iArray[5][1]="200px";            //列宽
    iArray[5][2]=60;            		//列最大值
    iArray[5][3]=1;              		//是否允许输入,1表示允许，0表示不允许
          
    CompanyGrid = new MulLineEnter( "fm" , "CompanyGrid" );
    CompanyGrid.mulLineCount = 10;
//    CompanyGrid.displayTitle = 1;
    CompanyGrid.canSel = 1;
    CompanyGrid. hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CompanyGrid. hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    CompanyGrid.loadMulLine(iArray);
//    CompanyGrid.detailInfo="单击显示详细信息";
    CompanyGrid. selBoxEventFuncName = "OneQuery";  //查询详细再保公司信息
  }
  catch(ex)
  {
    alert("初始化时出错:"+ex);
  }
}

//初始化查询系统中的所有再保公司信息
function initSelect(){
	var comsql = "select ReComCode,SAPAppntNo,ReComName,SimpName,Address from LRReComInfo order by ReComCode with ur";
	turnPage.queryModal(comsql, CompanyGrid);
}

function initElemnt(){
	fm.ReComCode.value = "";
	fm.ReComName.value = "";
	fm.SimpleName.value = "";
	fm.PostalCode.value = "";
	fm.FaxNo.value = "";
	fm.Address.value = "";
}
</script>


