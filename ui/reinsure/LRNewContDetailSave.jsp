<%
//程序名称：ReComManageSave.jsp
//程序功能：
//创建日期：2006-08-17
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  ReNewContDetailUI mReNewContDetailUI = new ReNewContDetailUI();
  CErrors tError = null;

  String FlagStr = "";
  String Content = "";
  String mReComCode= request.getParameter("ReComCode");
  String mCessionMode= request.getParameter("CessionMode");
  String mDiskKind= request.getParameter("DiskKind");
  String mRiskCode= request.getParameter("RiskCode");
  String mYear= request.getParameter("Year");
  String mMonth= request.getParameter("Month");
  String mContNo= request.getParameter("ContNo");
  String mAppntName= request.getParameter("AppntName");
  String mInsuredName= request.getParameter("InsuredName");
  String mReContCode = request.getParameter("RecontCode");//新增查询条件，再保合同号
//  System.out.println("呵呵,我来了"+mReContCode);
  String mActuGetState = request.getParameter("ActuGetState");//新增查询条件，账单状态
  
  
  String mOperateType = request.getParameter("OperateType");
  String BState = request.getParameter("BState");
  String AState = request.getParameter("AState");
  String mCessionAmount = request.getParameter("CessionAmount");
  String mCessPrem = request.getParameter("CessPrem");
  String mReProcFee = request.getParameter("ReProcFee");
  String mChoiRebaFee = request.getParameter("ChoiRebaFee");
//  String mSpeCemm = request.getParameter("SpeCemm");
  
  //09/03/16修改复选框为单选框
  LRPolSet tLRPolSet=new LRPolSet();//存储保存按钮操作时的值
  if(mOperateType.equals("UPDATE")){
//  String tChk[]=request.getParameterValues("InpNewContDetailGridChk");
  String tReNewCount[] = request.getParameterValues("NewContDetailGrid15");
  String tPolNo[] = request.getParameterValues("NewContDetailGrid19");
  String tReContCode[] = request.getParameterValues("NewContDetailGrid2");//再保合同号
  String tReinsureItem[] = request.getParameterValues("NewContDetailGrid21");//再保项目，F为反冲数据，C合同分保，T临分
  String tActuGetState[] = request.getParameterValues("NewContDetailGrid20");
  
  String tRadio[] = request.getParameterValues("InpNewContDetailGridSel");  
  
   for(int index=0; index< tRadio.length;index++)
      {
         if(tRadio[index].equals("1")){
           System.out.println("kwg kwgk kwgk kwgk "+index);
           LRPolSchema tLRPolSchema=new LRPolSchema();
		   tLRPolSchema.setPolNo(tPolNo[index]);
		   tLRPolSchema.setReNewCount(tReNewCount[index]);
		   tLRPolSchema.setReContCode(tReContCode[index]);
		   tLRPolSchema.setReinsureItem(tReinsureItem[index]);
//		   tLRPolSchema.setActuGetNo(tActuGetState[index]);  
		   tLRPolSet.add(tLRPolSchema);//四个字段才可以唯一确定一条记录。
         }
      }
  }
//  if(tChk!=null){
//  	for(int i=0;i<tChk.length;i++){
//			if(tChk[i]!=null && tChk[i].equals("1")){
//				LRPolSchema tLRPolSchema=new LRPolSchema();
//				tLRPolSchema.setPolNo(tPolNo[i]);
//				tLRPolSchema.setReNewCount(tReNewCount[i]);
//				tLRPolSchema.setReContCode(tReContCode[i]);
//				tLRPolSchema.setReinsureItem(tReinsureItem[i]);
//				tLRPolSchema.setActuGetNo(tActuGetNo[i]);  
//				tLRPolSet.add(tLRPolSchema);//四个字段才可以唯一确定一条记录。
//			}
//		}
//	}
  

	TransferData tTransferData = new TransferData();
	//分保信息,只有当是保存操作时,才有值
 	tTransferData.setNameAndValue("CessionAmount",mCessionAmount);//分出保额
 	tTransferData.setNameAndValue("CessPrem",mCessPrem);
 	tTransferData.setNameAndValue("ReProcFee",mReProcFee);//手续费
 	tTransferData.setNameAndValue("ChoiRebaFee",mChoiRebaFee);//选择折扣比例
 	tTransferData.setNameAndValue("AState",AState);//账单状态，溢额
 	tTransferData.setNameAndValue("BState",BState);//账单状态，成数
 	
 	//查询条件  
 	tTransferData.setNameAndValue("ReComCode",mReComCode);
 	tTransferData.setNameAndValue("CessionMode",mCessionMode);
 	tTransferData.setNameAndValue("DiskKind",mDiskKind);
 	tTransferData.setNameAndValue("RiskCode",mRiskCode);
 	tTransferData.setNameAndValue("Year",mYear);
 	tTransferData.setNameAndValue("Month",mMonth);
 	tTransferData.setNameAndValue("ContNo",mContNo);
 	tTransferData.setNameAndValue("AppntName",mAppntName);
 	tTransferData.setNameAndValue("InsuredName",mInsuredName);
 	tTransferData.setNameAndValue("ReContCode",mReContCode);
 	tTransferData.setNameAndValue("ActuGetState",mActuGetState);
 	
 	if(mOperateType.equals("ONLOAD")){
 	  String sysPath = application.getRealPath("/")+"/" ;
      System.out.println("in AddressCaseList ............................sysPath:"+sysPath); 
      tTransferData.setNameAndValue("SysPath",sysPath);
 	}

  VData tVData = new VData();
  try
  {
  	tVData.addElement(tTransferData);
  	tVData.addElement(globalInput);
  	tVData.addElement(tLRPolSet);
    mReNewContDetailUI.submitData(tVData,mOperateType); //处理分保明细数据
    if(mOperateType.equals("CHECK")){  //只有当是审核操作时才去校验账单类型
    CreateAccountBL tCreateAccountBL=new CreateAccountBL(); 
    tCreateAccountBL.submitData(tVData,"CHECK");//处理账单状态
    }
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReNewContDetailUI.mErrors;
    if (!tError.needDealError())
    {
      Content = "操作成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = "操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<%
	if(mOperateType.equals("ONLOAD")){
		VData mResult = mReNewContDetailUI.getResult();
		String url=(String)mResult.get(0);
		System.out.println(url);
		%> 
		<HTML>
			<script language="javascript">
				parent.fraInterface.downAfterSubmit("<%=url%>");
			</script>
		</HTML>   
<%
 }else{
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
<%}%>

