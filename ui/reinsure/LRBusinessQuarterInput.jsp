<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LRBusinessListInput.jsp
//程序功能：
//创建日期：2007-03-30
//创建人  ：huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LRBusinessQuarter.js"></SCRIPT>
	<%@include file="LRBusinessQuarterInit.jsp"%>
</head>

<body onload="initElementtype();initForm();">    
  <form action="" method=post name=fm target="f1print" >
    <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">季度帐单打印</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divCessGetData" style= "display: ''">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	        </TR>
	        <TR  class= common>
	          <TD  class= title>帐单编号</TD>
						<TD  class= input> 
	          	<Input class="common" name= "FeeNo" verify="帐单编号|NOTNULL" elementtype=nacessary >
	          </TD>
						<TD  class= title>再保合同</TD>
						<TD  class= input> 
	          	<Input class="codeno" name= "RecontCode" ondblClick="return showCodeList('RecontCode',[this,RecontCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RecontCode',[this,RecontCodeName],[0,1],null,null,null,1);" verify="再保合同|NOTNULL"><Input class="codename" name= 'RecontCodeName' elementtype=nacessary>
	          </TD>
          </TR>
          <TR  class= common>
	          <TD class= title>
   		 		分入公司编号
   			  </TD>
   			<TD class= input >
   				<input class="code" name="ReComCode" 
	            ondblclick="return showCodeList('reinsurecomcode',[this,ReComName],[0,1],null,null,null,1,300);"
	            onkeyup="return showCodeListKey('reinsurecomcode', [this,ReComName],[0,1],null,null,null,1,300);" verify="分入公司编号|NOTNULL" elementtype=nacessary>
   			</TD>
   			    <TD class= title>
          分入公司名称
        </TD>
        <TD class= input colspan=3>
        	<Input class="common" name= ReComName style="width:98%" readonly > 
        </TD>
   			
          </TR>   
	    </table>
	    <br>
		<INPUT class=cssButton  VALUE="打印" TYPE=button onClick="QuarterTabPrint();">	

		</Div> 
  </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 