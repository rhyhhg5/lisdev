<%
/*
**程序名称：LRBillDetailSave.jsp
**程序功能：实现审核，修改，下载功能
**创建日期：2010-4-13
**创建人  ：石和平
**更新记录：  更新人    更新日期     更新原因/内容
*/
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
	GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
	BillDetailUI mBillDetailUI = new BillDetailUI();
	CErrors tError = null;

	String FlagStr = "";
	String Content = "";
  	
	//	再保合同号RecontCode
	//	投保人AppntName
	//	被保险人InsuredName
	//	年份Year
	//	月份Month
	//	分保类型TempCessFlag
	//	险种类别DiskKind
	//	分保方式RiskCalSort
	//	险种编码RiskCode
	//	保单号ContNo
	//	再保公司ReinsureCom
	String mOperateType = request.getParameter("OperateType");
	String mRecontCode = request.getParameter("RecontCode");
	String mAppntName = request.getParameter("AppntName");
	String mYear = request.getParameter("Year");
	String mMonth = request.getParameter("Month");
	String mTempCessFlag = request.getParameter("TempCessFlag");
	String mDiskKind = request.getParameter("DiskKind");
	String mRiskCalSort = request.getParameter("RiskCalSort");
	String mRiskCode = request.getParameter("RiskCode");
	String mContNo = request.getParameter("ContNo");
	String mReinsureCom = request.getParameter("ReinsureCom");
	String mSQL = request.getParameter("SQL");
	ArrayList arrayList = new ArrayList();
	System.out.println("传递给后台的SQL-------------》"+mSQL );
	String tContCode[] = request.getParameterValues("ContGrid1");	 
	String tChk[] = request.getParameterValues("InpContGridChk"); //参数格式=” Inp+MulLine对象名+Chk”
	int tCount = tContCode.length;
	for(int j=0; j<tCount; j++)
		{
		  if(tChk[j].equals("1"))  
		  { 
		    arrayList.add(tContCode[j]);
			System.out.println("---------ContCode: "+tContCode[j]);
		  }		
		}
	TransferData tTransferData = new TransferData();
 	
 	//查询条件  
 	tTransferData.setNameAndValue("OperateType",mOperateType);
 	tTransferData.setNameAndValue("RecontCode",mRecontCode);
 	tTransferData.setNameAndValue("AppntName",mAppntName);
 	tTransferData.setNameAndValue("Year",mYear);
 	tTransferData.setNameAndValue("Month",mMonth);
 	tTransferData.setNameAndValue("AppntName",mAppntName);
 	tTransferData.setNameAndValue("TempCessFlag",mTempCessFlag);
 	tTransferData.setNameAndValue("DiskKind",mDiskKind);
 	tTransferData.setNameAndValue("RiskCalSort",mRiskCalSort);
 	tTransferData.setNameAndValue("RiskCode",mRiskCode);
 	tTransferData.setNameAndValue("ContNo",mContNo);
 	tTransferData.setNameAndValue("ReinsureCom",mReinsureCom);
 	tTransferData.setNameAndValue("SQL",mSQL);
 	tTransferData.setNameAndValue("arrayList", arrayList);
 	
 	//获取mulline传输过来的值
 	String triskcode = request.getParameter("riskcode1");
 	String trecontcode = request.getParameter("recontcode1");
 	String ttempcessflag = request.getParameter("tempcessflag1");
 	String tcontno= request.getParameter("contno1");
 	String tappntname = request.getParameter("appntname1");
 	String tinsueredname = request.getParameter("insueredname1");
 	String tcvalidate = request.getParameter("cvalidate1");
 	String tsigndate = request.getParameter("signdate1");
 	String tamnt = request.getParameter("amnt1");
 	String tstandprem = request.getParameter("standprem1");
 	String tCessPrem = request.getParameter("CessPrem1");
 	String tsum = request.getParameter("sum1");
 	String tCessionAmount=request.getParameter("CessionAmount1");
 	String tActuGetNo = request.getParameter("ActuGetNo1");
 	String tReNewCount = request.getParameter("ReNewCount1");
 	String tpolno = request.getParameter("polno1");
 	String treinsureitem = request.getParameter("reinsureitem1");
	//c.ReNewCount,c.polno,c.reinsureitem
 	tTransferData.setNameAndValue("CessionAmount",tCessionAmount);
	System.out.println(tCessionAmount+"wo shi zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
 	tTransferData.setNameAndValue("sum",tsum);
 	//更新选中数据时贮备的数据
 	LRAccountsSchema tLRAccountsSchema=new LRAccountsSchema();
 	if(mOperateType.equals("CHECK")){
 		if(!"".equals(tActuGetNo)&&tActuGetNo!=null){
 			tLRAccountsSchema.setActuGetNo(tActuGetNo);
 		}
 	}
 	
 	
 	LRPolSchema tLRPolSchema = new LRPolSchema();
 	if(mOperateType.equals("UPDATE")){
 		if(!"".equals(tReNewCount)&&tReNewCount!=null){
 			tLRPolSchema.setReNewCount(tReNewCount);
 		}
		if(!"".equals(tpolno)&&tpolno!=null){
			tLRPolSchema.setPolNo(tpolno);
 		}
		if(!"".equals(treinsureitem)&&treinsureitem!=null){
			tLRPolSchema.setReinsureItem(treinsureitem);
		}
 		if(!"".equals(triskcode)&&triskcode != null ){
 			tLRPolSchema.setRiskCode(triskcode);
 		}
 		if(!"".equals(trecontcode)&&trecontcode!=null){
 			tLRPolSchema.setReContCode(trecontcode);
 		}
 		if(!"".equals(ttempcessflag)||ttempcessflag!=null){
 			tLRPolSchema.setTempCessFlag(ttempcessflag);
 		}
 		if(!"".equals(tcontno)||tcontno!=null){
 			tLRPolSchema.setContNo(tcontno);
 		}
 		if(!"".equals(tappntname)||tappntname!=null){
 			tLRPolSchema.setAppntName(tappntname);
 		}
 		if(!"".equals(tinsueredname)||tinsueredname!=null){
 			tLRPolSchema.setInsuredName(tinsueredname);
 		}
 		if(!"".equals(tcvalidate)||tcvalidate!=null){
 			tLRPolSchema.setCValiDate(tcvalidate);
 		}
 		if(!"".equals(tsigndate)||tsigndate!=null){
 			tLRPolSchema.setSignDate(tsigndate);
 		}
 		if(!"".equals(tamnt)||tamnt!=null){
 			tLRPolSchema.setAmnt(tamnt);
 		}
 		if(!"".equals(tstandprem)||tstandprem!=null){
 			tLRPolSchema.setStandPrem(tstandprem);
 		}
 		if(!"".equals(tCessPrem)||tCessPrem!=null){
 			tLRPolSchema.setCessAmnt(tCessPrem);
 		}
 		if(!"".equals(tActuGetNo)||tActuGetNo!=null){
 			tLRPolSchema.setActuGetNo(tActuGetNo);
 		}
 	}
 	
 	//下载清单压缩包时准备的数据
 	if(mOperateType.equals("ONLOAD")){
	 	  String sysPath = application.getRealPath("/") ;
	      System.out.println("in AddressCaseList ............................sysPath:"+sysPath); 
	      tTransferData.setNameAndValue("SysPath",sysPath);
 	}

  	VData tVData = new VData();
  	try{
  		tVData.addElement(tTransferData);
  		tVData.addElement(globalInput);
  		tVData.addElement(tLRPolSchema);
  		tVData.addElement(tLRAccountsSchema);
    	mBillDetailUI.submitData(tVData,mOperateType); //处理分保明细数据
    	if(mOperateType.equals("CHECK")){  //只有当是审核操作时才去校验账单类型
    	    CreateAccountBL tCreateAccountBL=new CreateAccountBL(); 
    	    tCreateAccountBL.submitData(tVData,"CHECK");//处理账单状态
    	    }
  	}
  	catch(Exception ex){
	    Content = "操作失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
  	}

  	//如果在Catch中发现异常，则不从错误类中提取错误信息
 	if (FlagStr=="") {
	    tError = mBillDetailUI.mErrors;
	    if (!tError.needDealError()){
      		Content = "操作成功";
    		FlagStr = "Succ";
		} else{
	    	Content = "操作失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
    	}
 	 }
%>
<%
	if(mOperateType.equals("ONLOAD")){
		VData mResult = mBillDetailUI.getResult();
		String url=(String)mResult.get(0);
		System.out.println(url);
		%> 
		<HTML>
			<script language="javascript">
				parent.fraInterface.downAfterSubmit("<%=url%>");
			</script>
		</HTML>   
<%
 	}else{
%>
<html>
<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
<%	}	%>