<html>
<%
//name :CessInfoInit.jsp
//function :Manage CessInfo
//Creator :
//date :2008-10-11
%>

<!--用户校验类-->

<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>

<script language="JavaScript">
function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在LRNewContDetailInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initPolGrid();
  }
  catch(re)
  {
    alert("3LRNewContDetailInit.jsp->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
      iArray[2]=new Array();
      iArray[2][0]="再保合同号";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="分保类型";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许            

      iArray[4]=new Array();                                                       
      iArray[4][0]="保单号";         		//列名                                     
      iArray[4][1]="100px";            		//列宽                                   
      iArray[4][2]=100;            			//列最大值                                 
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
            
      iArray[5]=new Array();
      iArray[5][0]="投保人";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[6]=new Array();
      iArray[6][0]="被保险人";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      
      iArray[7]=new Array();
      iArray[7][0]="生效日期";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="签单日期";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="保额";         		//列名
      iArray[9][1]="40px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="标准保费";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="实收保费";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="分出保额";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[13]=new Array();
      iArray[13][0]="分出保费";         		//列名
      iArray[13][1]="60px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="再保手续费";         		//列名
      iArray[14][1]="70px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[15]=new Array();
      iArray[15][0]="续期次数";         		//列名
      iArray[15][1]="70px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[16]=new Array();
      iArray[16][0]="手续费";         		//列名
      iArray[16][1]="70px";            		//列宽
      iArray[16][2]=100;            			//列最大值
      iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[17]=new Array();
      iArray[17][0]="选择折扣";         		//列名
      iArray[17][1]="70px";            		//列宽
      iArray[17][2]=100;            			//列最大值
      iArray[17][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[18]=new Array();
      iArray[18][0]="特殊佣金";         		//列名
      iArray[18][1]="70px";            		//列宽
      iArray[18][2]=100;            			//列最大值
      iArray[18][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[19]=new Array();
      iArray[19][0]="PolNo";         		//列名
      iArray[19][1]="70px";            		//列宽
      iArray[19][2]=100;            			//列最大值
      iArray[19][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[20]=new Array();
      iArray[20][0]="ActuGetState";         		//列名
      iArray[20][1]="70px";            		//列宽
      iArray[20][2]=100;            			//列最大值
      iArray[20][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[21]=new Array();
      iArray[21][0]="ReinsureItem";         		//列名
      iArray[21][1]="0px";            		//列宽
      iArray[21][2]=100;            			//列最大值
      iArray[21][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      
      NewContDetailGrid = new MulLineEnter( "fm" , "NewContDetailGrid" ); 
      //这些属性必须在loadMulLine前
      NewContDetailGrid.mulLineCount = 0;   
      NewContDetailGrid.displayTitle = 1;
      NewContDetailGrid.locked = 1;
      NewContDetailGrid.canSel = 1;
      NewContDetailGrid.canChk =0; 
      NewContDetailGrid.hiddenPlus = 1;
      NewContDetailGrid.hiddenSubtraction = 1;
      NewContDetailGrid.loadMulLine(iArray);     
      
      NewContDetailGrid. selBoxEventFuncName  = "getinfo";
      
      //这些操作必须在loadMulLine后面
      //NewContDetailGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>
