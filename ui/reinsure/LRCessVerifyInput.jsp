<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LRCalculateInput.jsp
//程序功能：
//创建日期：2006-10-31
//创建人  ：JavaBean
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LRCessVerifyInput.js"></SCRIPT>
	<%@include file="LRCessVerifyInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
</head>

<body onload="initElementtype();initForm();">    
  <form action="" method=post name=fm target="fraSubmit" >
    <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">理赔摊回校验</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divCessGetData" style= "display: ''" style="float: right">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	         
	        </TR>
	        
	    </table>
	    <br>
	    <INPUT class=cssButton  VALUE="数据量校验" TYPE=button onClick="qureyData();">
		<INPUT class=cssButton  VALUE="数据提取" TYPE=button onClick="calCessData();">	
		
		</Div> 
    <Div  id= "divResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanLRCessGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">  
	</Div>
	
	<Div  id= "divBQResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanLRBQCessGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div2" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">  
	</Div>
	
	<Div  id= "divClaimResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanLRClaimGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div3" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();">  
	</Div>
	
	<Div  id= "divLRListGrid" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanLRListGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div4" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage();">  
	</Div>
    <input type="hidden" name="Operator" value="<%=Operator%>">
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 