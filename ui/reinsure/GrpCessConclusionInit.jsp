<html>
<%
//name :LRIndiCessInit.jsp
//function :Manage CessInfo
//Creator :
//date :2006-08-15
%>

<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 		GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator			= tG.Operator;
  	String Comcode			= tG.ManageCom;
 		String CurrentDate	= PubFun.getCurrentDate();   
    String tCurrentYear	=	StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=	StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate	=	StrTool.getVisaDay(CurrentDate);   
    String modifyFlag		= request.getParameter("ModifyFlag");
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.PrtNo.value 					= '<%=request.getParameter("PrtNo")%>';
  	fm.ProposalContNo.value = '<%=request.getParameter("ContNo")%>';
  	fm.ManageCom.value 			= '<%=request.getParameter("ManageCom")%>';
  	fm.InputDate.value			= '<%=CurrentDate%>';
  	fm.ModifyFlag.value 		= '<%=request.getParameter("ModifyFlag")%>';
  	
  	//fm.ReContCode.value='TD005'; //测试用
  	//fm.ReComCode.value='054';
  	
  	
  	
  	var strSQL="select distinct GrpName,GrpNature from LCGrpCont where PrtNo='"+fm.PrtNo.value+"'"
  	;
  	var arrResult=easyExecSql(strSQL);
  	if (arrResult==null)
  	{
  		fm.AppntName.value="";
  	}
  	else
  	{
  		fm.AppntName.value	=	arrResult[0][0];
  		fm.GrpName.value		= arrResult[0][1];
  	}
  	
  	strSQL = "select AppFlag,(Case AppFlag when '0' then '未结案' ELSE '已结案' End) from lcgrpcont where prtno='"+fm.PrtNo.value+"'";
  	arrResult=easyExecSql(strSQL);
  	if(arrResult==null)
  	{
  		fm.StateName.value="未知状态";
			fm.State.value="";
  	}
  	else
  	{
  		fm.State.value=arrResult[0][0];
  		fm.StateName.value=arrResult[0][1];
  	}
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CessConclusionInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initPolGrid();
    easyQuery();
  }
  catch(re)
  {
    alert("3在CessConclusionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         				//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            				//列最大值
      iArray[0][3]=0;              				//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="参保人数";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            				//列最大值
      iArray[1][3]=0;              				//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="参保比例";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            				//列最大值
      iArray[2][3]=0;              				//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="退休比例";         		//列名
      iArray[3][1]="70px";            		//列宽
      iArray[3][2]=100;            				//列最大值
      iArray[3][3]=0;              				//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="险种代码";        //列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            				//列最大值
      iArray[4][3]=0;              				//是否允许输入,1表示允许，0表示不允许            

      iArray[5]=new Array();                                                       
      iArray[5][0]="投保险种名称";        //列名                                     
      iArray[5][1]="80px";            		//列宽                                   
      iArray[5][2]=100;            				//列最大值                                 
      iArray[5][3]=0;              				//是否允许输入,1表示允许，0表示不允许   
      
      iArray[6]=new Array();                                                       
      iArray[6][0]="标准保费";        		//列名                                     
      iArray[6][1]="80px";            		//列宽                                   
      iArray[6][2]=100;            				//列最大值                                 
      iArray[6][3]=0;              				//是否允许输入,1表示允许，0表示不允许 
            
      iArray[7]=new Array();
      iArray[7][0]="录入保费";        		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            				//列最大值
      iArray[7][3]=0;              				//是否允许输入,1表示允许，0表示不允许 
	          
      iArray[8]=new Array();
      iArray[8][0]="人均保费";         		//列名
      iArray[8][1]="50px";            		//列宽
      iArray[8][2]=200;            				//列最大值
      iArray[8][3]=0;              				//是否允许输入,1表示允许，0表示不允许 
	    
	    iArray[9]=new Array();
      iArray[9][0]="折扣比例";         		//列名
      iArray[9][1]="50px";            		//列宽
      iArray[9][2]=200;            				//列最大值
      iArray[9][3]=0;              				//是否允许输入,1表示允许，0表示不允许 	
      
      iArray[10]=new Array();
      iArray[10][0]="Grpproposalno";       //列名
      iArray[10][1]="80px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许 					
      
      //iArray[10]=new Array();
      //iArray[10][0]="累计风险保额";       //列名
      //iArray[10][1]="50px";            		//列宽
      //iArray[10][2]=200;            			//列最大值
      //iArray[10][3]=1;              			//是否允许输入,1表示允许，0表示不允许 				
      
      iArray[11]=new Array();
      iArray[11][0]="自留额";         		//列名
      iArray[11][1]="80px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=1;              			//是否允许输入,1表示允许，0表示不允许
           
      iArray[12]=new Array();
      iArray[12][0]="分保比例";         	//列名
      iArray[12][1]="50px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许        
      
      iArray[13]=new Array();
      iArray[13][0]="手续费比例";         //列名
      iArray[13][1]="70px";            		//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
                  
      iArray[14]=new Array();
      iArray[14][0]="再保核保结论";       //列名
      iArray[14][1]="80px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=1;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[15]=new Array();
      iArray[15][0]="临分结论";         	//列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][2]=200;            			//列最大值
      iArray[15][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[15][10]="CessConclusion"; 		  			
	    iArray[15][11]="0|^3|临分标志|^4|合同分保标志|^5|临分未实现标志" 
	    iArray[15][13]="1|0"	
	    
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 3;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);     
      
      PolGrid. selBoxEventFuncName = "initModify";
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>
