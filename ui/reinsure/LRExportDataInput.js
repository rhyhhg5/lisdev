//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var DealWithNam ;

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function exportCessData()
{
	var startDate = fm.StartDate.value;
	var endDate = fm.EndDate.value;
	
	if (startDate.length!=10)
	{
		if(startDate.length==8)
		{
			fm.StartDate.value=modifydate(startDate)
		}
		else
		{
			alert("输入统计起期有误！");
			return false;
		}
	}
	if (endDate.length!=10)
	{
		if(endDate.length==8)
		{
			fm.EndDate.value=modifydate(endDate)
		}
		else
		{
			alert("输入统计止期有误！");
			return false;
		}
	}
	
	if (verifyInput() == false)
    return false;
    
  if(dateDiff(fm.StartDate.value,fm.EndDate.value,"M")>12||dateDiff(fm.StartDate.value,fm.EndDate.value,"M")<1)
	{
		alert("输入统计日期范围有误，日期范围必须在12个月以内，起期不能大于止期!");
		return false;
	}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "importCessData"; 
	fm.action = "LRExportDataSub.jsp"
	fm.submit();
	showInfo.close();
}

function exportEdorData()
{
	var startDate = fm.StartDate.value;
	var endDate = fm.EndDate.value;
	
	if (startDate.length!=10)
	{
		if(startDate.length==8)
		{
			fm.StartDate.value=modifydate(startDate)
		}
		else
		{
			alert("输入统计起期有误！");
			return false;
		}
	}
	if (endDate.length!=10)
	{
		if(endDate.length==8)
		{
			fm.EndDate.value=modifydate(endDate)
		}
		else
		{
			alert("输入统计止期有误！");
			return false;
		}
	}
	
	if (verifyInput() == false)
    return false;
    
  if(dateDiff(fm.StartDate.value,fm.EndDate.value,"M")>12||dateDiff(fm.StartDate.value,fm.EndDate.value,"M")<1)
	{
		alert("输入统计日期范围有误，日期范围必须在12个月以内，起期不能大于止期!");
		return false;
	}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "importEdorData"; 
	fm.action = "LRExportEdorSub.jsp"
	fm.submit();
	showInfo.close();
}

function exportClaimData()
{
	var startDate = fm.StartDate.value;
	var endDate = fm.EndDate.value;
	
	if (startDate.length!=10)
	{
		if(startDate.length==8)
		{
			fm.StartDate.value=modifydate(startDate)
		}
		else
		{
			alert("输入统计起期有误！");
			return false;
		}
	}
	if (endDate.length!=10)
	{
		if(endDate.length==8)
		{
			fm.EndDate.value=modifydate(endDate)
		}
		else
		{
			alert("输入统计止期有误！");
			return false;
		}
	}
	
	if (verifyInput() == false)
    return false;
    
  if(dateDiff(fm.StartDate.value,fm.EndDate.value,"M")>12||dateDiff(fm.StartDate.value,fm.EndDate.value,"M")<1)
	{
		alert("输入统计日期范围有误，日期范围必须在12个月以内，起期不能大于止期!");
		return false;
	}
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "importClaimData"; 
	fm.action = "LRExportClaimSub.jsp"
	fm.submit();
	showInfo.close();
}

function afterCodeSelect( cCodeName, Field )
{
}
