<%
//程序名称：LRBatchSave.jsp
//程序功能：
//创建日期：2007-08-07
//创建人  ：huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.taskservice.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  System.out.println("开始执行批处理打标记页面");
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
	
  CErrors tError = null;
  String FlagStr 		= "";
  String Content 		= "";
  
	String startDate=request.getParameter("StartDate"); 
	String endDate=request.getParameter("EndDate"); 
	
  try
  {
   	ReContCessTask tReContCessTask = new ReContCessTask();
   	tReContCessTask.submitData(startDate,endDate);
  }
  catch(Exception ex)
  {
    Content = "失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  
	if (FlagStr=="")
	{  
	  if (!tError.needDealError())
	  {
	    Content = "完成";
	  	FlagStr = "Succ";
	  }
	  else
	  {
	  	Content = " 失败，原因是:" + tError.getFirstError();
	  	FlagStr = "Fail";
	  }
	} 
%>

<html>
	<script language="javascript">
			parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>