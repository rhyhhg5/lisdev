<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRNewAccountCheckInput.jsp
//程序功能：账单审核
//创建日期：2008-11-11
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LRNewNoLongerReInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRNewNoLongerReInit.jsp"%>
  <title>不再分保保单录入</title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "./LRNewNoLongerReSave.jsp" enctype="multipart/form-data">
 
         
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInfo);"></td>
          <td class="titleImg">请输入数据信息</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divInfo" style= "display: ''">
    <table class=common>
    	<tr class=common>
    		 <td class="title">再保合同号</td>
				<td class="input">
					<input class="code" name="RecontCode"  verify="再保合同号|notnull" elementtype=nacessary
						ondblClick="return showCodeList('RecontCode',[this,null],[0,1],null,null,null,1);" 
						onkeyup="return showCodeListKey('RecontCode',[this,null],[0,1],null,null,null,1);">
				</td>	
   			<td class="title">保单号</td>
	       		<td class="input">
	   				<input class="common"  name= "Contno"  verify="保单号|NOTNULL" elementtype=nacessary>
	   			</td> 
   			<TD  class= title>数据是否已删除</TD>
        <TD  class= input> <Input class="codeno" value="01" readOnly name= "Standby1" CodeData="0|^01|导入待删除^02|已删除" 
          ondblClick="showCodeListEx('Standby1',[this,Standby1Name],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('Standby1',[this,Standby1Name],[0,1],null,null,null,1);"><Input 
          class= codename name= 'Standby1Name'  value="导入待删除"> 
        </TD>  			
        </tr>
    	
     	
  	</table>   
  </Div>


  <INPUT class=cssButton VALUE="查   询" TYPE=button onClick="queryDetailData()">
  <INPUT class=cssButton VALUE="单一数据导入" TYPE=button onClick="SaveData()">
  <Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFileImport);"></td>
          <td class="titleImg">数据批量删除</td>
        </tr>
      </table>
  </Div>
 <div id="divFileImport" name="divFileImport" style="display:''">
           
            <table class="common">
                <tr class="common">
                    <td class="title">批量导入文件</td>
                    <td class="common">
                        <input class="cssfile" name="FileName" type="file" />
                    </td>
                    
                </tr>
            </table>
            <font color="red" >批量导入模板说明：</font><br>
            <font color="red" >1.模板中sheet页的现有名称（LRNoLongerCont）和第一行的抬头名称不能修改； </font>  
 		<br>
 		<font color="red" >2.“保单号”列需保持现有文本格式，以防止保单号起始部分数字“0”的丢失，确保保单号完整。 </font>   
            <br><input class="cssButton" type="button"  value="批量导入模板下载" onclick="downLoad();" />
                    
            <input class="cssButton" type="button" id="btnImport" name="btnImport" value="批量待删除数据导入" onclick="importList();" />
       		
        </div>
         
        <hr />
  <input type="hidden" name="OperateType" >
 <Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divList);"></td>
          <td class="titleImg">已删除数据信息</td>
        </tr>
      </table>
  </Div>
  <div  id= "divList" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
	<center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
	</center>  	
  	</div>
  	<INPUT class=cssButton VALUE="确认删除分保数据" TYPE=button onClick="deleteImpData()">
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>