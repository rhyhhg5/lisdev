<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ReinsureCessList.jsp
//程序功能：
//创建日期：2007-03-31
//创建人  ：huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%
	System.out.println("start");
	String FlagStr = "";
	String Content = "";
	String FullPath = "";
	
	String mYear= request.getParameter("Year");
	  String mMonth= request.getParameter("Month");

	  String tChk[]=request.getParameterValues("InpNewAccountPrintGridChk");
	  String tActuGetNo[] = request.getParameterValues("NewAccountPrintGrid9");
	  LRAccountsSet tLRAccountsSet=new LRAccountsSet();
	  if(tChk!=null){
	  	for(int i=0;i<tChk.length;i++){
				if(tChk[i]!=null && tChk[i].equals("1")){
					LRAccountsSchema tLRAccountsSchema=new LRAccountsSchema();
					tLRAccountsSchema.setActuGetNo(tActuGetNo[i]);//
	                //tLOPRTManagerSchema.setOtherNoType("19");
	                //tLOPRTManagerSchema.setCode("zbzddy01");
	                tLRAccountsSet.add(tLRAccountsSchema);
				}
			}
		}
	

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	
	TransferData tTransferData = new TransferData();
	 	tTransferData.setNameAndValue("Year",mYear);
 	tTransferData.setNameAndValue("Month",mMonth);
  
  tVData.addElement(tG);
  tVData.addElement(tTransferData);
  tVData.addElement(tLRAccountsSet);
  tVData.addElement(tActuGetNo);
  
  String sysPath = application.getRealPath("/")+"/" ;         
  ReinsureTNewListBL tReinsureTNewListBL = new ReinsureTNewListBL();
  if(!tReinsureTNewListBL.submitData(tVData,sysPath))
  {
      FlagStr = "Fail";
      Content = tReinsureTNewListBL.mErrors.getFirstError().toString();                 
  }else{    
			FullPath = tReinsureTNewListBL.getResult();			
	   	Content="导出数据成功";	 
	}
%>
<html>
<script language="javascript">	
	 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>"); 	
	  <%if(Content.equals("导出数据成功")){%>  
          window.open("<%=FullPath%>");
    <%}else{%>	
	  	alert("导出数据失败！");
    <%}%>	
</script>
</html>