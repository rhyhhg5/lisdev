<%
//程序名称：ReNewComManageSave.jsp
//程序功能：
//创建日期：2008-10-27
//创建人  ：liuli
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行合同保存Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String mDescType = "";//将操作标志的英文转换成汉字的形式  
  System.out.println("开始进行获取数据的操作！！！");
  
  //处理再保合同信息
  LRContInfoSchema mLRContInfoSchema = new LRContInfoSchema();
  mLRContInfoSchema.setReContCode(		request.getParameter("ReContCode"));
  mLRContInfoSchema.setReContName(		request.getParameter("ReContName"));
  mLRContInfoSchema.setReComCode(			request.getParameter("ReComCode"));
  mLRContInfoSchema.setRValiDate(			request.getParameter("RValidate"));
  mLRContInfoSchema.setRInvaliDate(		request.getParameter("RInvalidate"));
  mLRContInfoSchema.setReContState(			request.getParameter("ReContState"));//合同状态
  mLRContInfoSchema.setDiskKind(			request.getParameter("DiskKind"));
  mLRContInfoSchema.setCessionMode(		request.getParameter("CessionMode"));//分保方式。成数，溢额
  mLRContInfoSchema.setCessionFeeMode(request.getParameter("CessionFeeMode"));
  mLRContInfoSchema.setReType(		request.getParameter("ReType"));//分保规则，按责任/按险种
  mLRContInfoSchema.setReinsurItem("C");//商业分保。这个字段为非空，但是新合同管理这个字段无实际意义。
  mLRContInfoSchema.setCessionDetailType(request.getParameter("CessionDetailType"));//溢额合同细分类型
  System.out.println("合同信息获取完成！");
  //处理再保计算要素值信息
  HashMap facMap = new HashMap(); //存放要素名与值
  ArrayList facList = new ArrayList(); //存放要素
  ArrayList namList = new ArrayList(); //存放要素名称
  ArrayList valList = new ArrayList(); //存放要素类型
  
  Enumeration enu = request.getParameterNames();
  String factor = "";
  
  while(enu.hasMoreElements())
	{
		factor = (String)enu.nextElement();
		
		if(factor.substring(0,3).equals("Fac"))
		{
			facList.add(factor);
			facMap.put(factor,request.getParameter(factor));
		}
		if(factor.substring(0,3).equals("Nam"))
		{
			namList.add(factor);
		}
		if(factor.substring(0,3).equals("Val"))
		{
			valList.add(factor);
		}
	}
  
  int xx = 0;
  if(request.getParameter("ReType").equals("01")){
     xx = Integer.parseInt(request.getParameter("risk"));  
  }else if(request.getParameter("ReType").equals("02")){
     xx = Integer.parseInt(request.getParameter("duty"));	
  }
  String[] strNumber = new String[xx] ;	
  String[] strRiskCode= new String[xx] ;
  String[] strRiskName= new String[xx] ;
  if(request.getParameter("ReType").equals("01")){//按险种分保，获取险种信息
//  System.out.println("fdsffffffffffffffafdsfdasa"+xx);
   strNumber 		= request.getParameterValues("ContRiskGridNo");
   strRiskCode 		= request.getParameterValues("ContRiskGrid1");
   strRiskName	 	= request.getParameterValues("ContRiskGrid2");
  }else if(request.getParameter("ReType").equals("02")){//按责任分保，获取责任信息
   strNumber 		= request.getParameterValues("ContGetDutyGridNo");
   strRiskCode 		= request.getParameterValues("ContGetDutyGrid1");
   strRiskName	 	= request.getParameterValues("ContGetDutyGrid2");	
  }
  String factorCode = "";
  String factorName = "";
  String factorValue = "";
  LRCalFactorValueSet  mLRCalFactorValueSet = new LRCalFactorValueSet();
  if(strNumber!=null)
  {  
  	int tLength = strNumber.length;	
    for(int i = 0 ;i < tLength ;i++)
    {
	  	Iterator facIte = facList.iterator();
	  	
    	while(facIte.hasNext())
		  {
		  	factorCode = (String)facIte.next();
		  	
		  	LRCalFactorValueSchema tLRCalFactorValueSchema = new LRCalFactorValueSchema();
						  	
		    tLRCalFactorValueSchema.setReContCode(request.getParameter("ReContCode"));
		    tLRCalFactorValueSchema.setRiskSort(request.getParameter("CessionMode"));//存储分保方式。成数，溢额
		    tLRCalFactorValueSchema.setRiskCode(strRiskCode[i]); // 险种号
		    
		    tLRCalFactorValueSchema.setFactorCode(factorCode.substring(3)); //添加要素代码
		    tLRCalFactorValueSchema.setReComCode(request.getParameter("ReComCode"));//再保公司号。可以存储它用字段
		    tLRCalFactorValueSchema.setFactorValue(request.getParameter(factorCode));
		    Iterator namIte = namList.iterator(); //添加要素名称
		    while(namIte.hasNext())
		  	{  
		  		factorName = (String)namIte.next();
		  		if (factorName.substring(3).equals(factorCode.substring(3)))
		  		{
		  			tLRCalFactorValueSchema.setFactorName(request.getParameter(factorName));
		  		}
		  	}
		  			  			  	
		  	Iterator valIte = valList.iterator(); //添加要素数值类型
		    while(valIte.hasNext())
		  	{  
		  		factorValue = (String)valIte.next();
		  		
		  		if (factorValue.substring(3).equals(factorCode.substring(3)))
		  		{
		  			tLRCalFactorValueSchema.setValueType(request.getParameter(factorValue));
		  		}
		  	}
		    mLRCalFactorValueSet.add(tLRCalFactorValueSchema);
		  }
    }
  } 
  System.out.println("要素完成");
  //处理关联险种信息
  LRAmntRelRiskSet mLRAmntRelRiskSet = new LRAmntRelRiskSet();
  
  //按险种分保时才有关联险种的累计
  if(request.getParameter("ReType").equals("01")){
//  String[] strRelNumber 	= request.getParameterValues("RelDiskGridNo");
  String[] strRelRiskCode 	= request.getParameterValues("RelDiskGrid1"); 
  if(strRelRiskCode!=null)
  {
  	int tLength = strRelRiskCode.length;
    for(int i = 0 ;i < tLength ;i++)
    {
      LRAmntRelRiskSchema tLRAmntRelRiskSchema = new LRAmntRelRiskSchema();
	  tLRAmntRelRiskSchema.setReContCode(request.getParameter("ReContCode")); 
      tLRAmntRelRiskSchema.setGroupName("A"); 
      tLRAmntRelRiskSchema.setRiskCode(strRelRiskCode[i]);
      
      mLRAmntRelRiskSet.add(tLRAmntRelRiskSchema);
    }
  }
  System.out.println("关联险种完成");
  }
    
  System.out.println("in jsp0 ->before submitData..........");    
 
  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增再保合同";
  }else if(mOperateType.equals("UPDATE")){
  	 mDescType = "修改再保合同";
  }else if(mOperateType.equals("DELETE")){
  	mDescType = "删除再保合同";
  }else if(mOperateType.equals("CONFIRM")){
    mDescType = "再保合同审核";
  }
  
  ReNewContManageUI mReNewContManageUI = new ReNewContManageUI();
  VData tVData = new VData();
  try
  {
  	tVData.addElement(globalInput);
    tVData.addElement(mLRContInfoSchema);
    tVData.addElement(mLRCalFactorValueSet);
    tVData.addElement(mLRAmntRelRiskSet);
    mReNewContManageUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReNewContManageUI.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+"成功，"+" 合同编号："+mReNewContManageUI.getResult();
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mReNewContManageUI.getResult()%>");
</script>
</html>