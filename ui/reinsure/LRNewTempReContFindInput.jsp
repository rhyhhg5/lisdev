<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRNewTempReContFindInput.jsp
//程序功能：临分查询
//创建日期：2008-11-11
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LRNewTempReContFindInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRNewTempReContFindInit.jsp"%>
  <title>临分查询</title>
</head>
<body  onload="initForm(); initElementtype();">
<form method=post name=fm target="fraSubmit" action= "./LRNewTempReContFindSave.jsp" >
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInfo);"></td>
          <td class="titleImg">请输入查询条件</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divInfo" style= "display: ''">
    <table class=common>
    	<tr class=common>
      	<TD class= title>
   		 		保单类型
   			</TD>
   			<TD class= input >
   				<Input class="codeno" readOnly name= "ContType" CodeData="0|^1|个单^2|团单" 
          ondblClick="showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" ><Input class="codename" name= 'ContTypeName' readonly>
   			</TD>
   			<TD class= title>
   				保单号
        </TD>
        <TD class= input>
        	<Input type="common" name="ContNo" >
        </TD>
        <TD class= title>
   		 		投保人/单位名称
   			</TD>
   			<TD class= input>
   				<Input type="common" name="Appnt" >
   			</TD>
      </tr>
     	<tr class=common>
				<TD  class= title>签单日期</TD>
	      <TD  class= input> 
	         <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|Date">
	      </TD> 
	      <TD  class= title>至</TD>
	      <TD  class= input> 
	         <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|Date">
	      </TD> 
	      <TD  class= title>再保合同号</TD>
	      <TD  class= input><Input type="common" name="ReContNo" ></TD>
  		</tr>
  	</table>   
  </Div>
  <INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
	<Div  id= "divResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanNewTempReContGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">  
	</Div>

  <INPUT class=cssButton VALUE="下载清单" TYPE=button onClick="onloadlist()">
  <input type="hidden" name="OperateType" >
  <Div id=DivFileDownload style="display:'none'">
    <A id=fileUrl href=""></A>
  </Div>
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>