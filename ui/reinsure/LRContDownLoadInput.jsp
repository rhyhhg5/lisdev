<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRContDownLoadInput.jsp
//程序功能：账单审核
//创建日期：2014-8-1
//创建人   杨阳
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <script src="LRContDownLoad.js"></script>
  <title>合同信息下载</title>
</head>
<body>
<form method=post name=fm target="fraSubmit" action= "" >
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="titleImg">请输入查询条件</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divInfo" style= "display: ''">
    <table class=common>
    	    	
     	<TR  class= common>
				<tr class=common>
  			<TD  class= title>
  				合同类型
        </TD>
        <TD  class= input>
          <Input class=codeno name="ContCode" CodeData="0|^0|正常合同^1|临分合同" value=0
          onClick="return showCodeListEx('1',[this,ContCodeName],[0,1]);" 
       		onkeyup="return showCodeListKeyEx('1',[this,ContCodeName],[0,1]);" ><input class=codename name=ContCodeName readonly=true value ="正常合同" >
        </TD>
	        </TR>
  	</table>   
  </Div>
  <INPUT class=cssButton VALUE="下载文件" TYPE=button   onclick="contDownLoad();">
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>