<html>
<%
//name :ReComManage.jsp
//function :ReComManage
//Creator :liuli
//date :2008-10-27
%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.reinsure.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src = "./ReNewContManageInput.js"></SCRIPT> 
<%@include file="./ReNewContManageInit.jsp"%>
</head>
<body  onload="initElementtype();initForm()" >
  <form action="" method=post name=fm target="fraSubmit">  
   <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divLLReport1);"></td>
    	<td class= titleImg>基本信息</td></tr>
    </table>
  
  <Div id= "divLLReport1" style= "display: ''">
	<br>
   	<Table class= common>
   		<TR class= common>
   			<TD class= title>
   		 		合同编号
   			</TD>
   			<TD class= input>
   				<Input class= common name="ReContCode" id="ReContCodeId" verify="合同编号|NOTNULL" elementtype=nacessary> 
   			</TD>
   			<TD class= title>
          合同名称
        </TD>
        <TD class= input>
        	<Input class= common name= ReContName id="ReContNameId" verify="合同名称|NOTNULL" elementtype=nacessary> 
        </TD>
        <TD class= title>
          合同状态
        </TD>
        <TD class= input colspan=3>
        	<Input class="codeno" readOnly name= "ReContState" value="02" CodeData="0|^01|已审核^02|失效^03|待审核" 
          ondblClick="showCodeListEx('ReContState',[this,ReContStateName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('ReContState',[this,ReContStateName],[0,1],null,null,null,1);" verify="保单状态|NOTNULL" ><Input 
          class= codename name= 'ReContStateName' elementtype=nacessary value="失效">
        </TD>
        
      </TR> 
      <TR>
      	<TD class= title>
   		 		险种类别
   			</TD>
   			<TD class= input>
   				<Input class="codeno" readOnly name= "DiskKind" CodeData="0|^L|长险^M|短险" 
          ondblClick="showCodeListEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);" verify="险种类别|NOTNULL"><Input 
          class= codename name= 'DiskKindName' elementtype=nacessary>
   			</TD>
   			<TD class= title>
   				分保方式
        </TD>
        <TD class= input>
        	<Input class="codeno" readOnly name= "CessionMode" CodeData="0|^1|成数^2|风险溢额" 
          ondblClick="showCodeListEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);" verify="分保方式|NOTNULL" ><Input 
          class= codename name= 'CessionModeName' elementtype=nacessary >
        </TD>
        <td class="title">
        	分保保费方式
        </td>
        <td class="input">
        	<Input class="codeno" readOnly name= "CessionFeeMode" CodeData="0|^1|风险保费方式^2|原始保费方式^3|修正共保方式" 
          ondblClick="showCodeListEx('CessionFeeMode',[this,CessionFeeModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionFeeMode',[this,CessionFeeModeName],[0,1],null,null,null,1);"><Input 
          class= codename name= 'CessionFeeModeName'>
        </td>
      </TR>      
      <TR>
      	<TD class= title>
   		 		合同生效日期
   			</TD>
   			<TD class= input >
   				<Input class= "coolDatePicker" name= RValidate verify="合同生效日期|DATE&NOTNULL" elementtype=nacessary>
   			</TD>
   			<TD class= title>
   				合同终止日期
        </TD>
        <TD class= input>
        	<Input class= "coolDatePicker" name= RInvalidate verify="合同终止日期|DATE">
        </TD>
        <td class="title">
        	 分保规则
        </td>
        <td class="input">
        	<Input class="codeno" readOnly name= "ReType" CodeData="0|^01|按险种^02|按责任" 
          ondblClick="showCodeListEx('ReType',[this,ReTypeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('ReType',[this,ReTypeName],[0,1],null,null,null,1);" verify="保单状态|NOTNULL" ><Input 
          class= codename name= 'ReTypeName' elementtype=nacessary > 
        </td>
      </TR>  
      <TR>
      	<TD class= title>
   		 		再保公司编号
   			</TD>
   			<TD class= input >
   				<input class="code" name="ReComCode" 
	            ondblclick="return showCodeList('reinsurecomcode',[this,ReComName],[0,1],null,null,null,1,300);"
	            onkeyup="return showCodeListKey('reinsurecomcode', [this,ReComName],[0,1],null,null,null,1,300);" verify="再保公司编号|NOTNULL" elementtype=nacessary>
   			</TD>
   			<TD class= title>
          再保公司名称
        </TD>
        <TD class= input colspan=3>
        	<Input class="common" name= ReComName style="width:98%" readonly > 
        </TD>  
       
    </TR>
    <tr style="display: none" id ="cessionDetailTR">
    <TD class= title>
   		 		溢额合同类型细分
   			</TD>
   			<TD class= input>
   				<Input class="codeno" readOnly name= "CessionDetailType" CodeData="0|^01|按合同终止日分保^02|按保单终止日分保^03|按月分保" 
          ondblClick="showCodeListEx('cessiondetailtype',[this,CessionDetailTypeName],[0,1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('cessiondetailtype',[this,CessionDetailTypeName],[0,1],null,null,null,1);"  ><Input 
          class= codename name= 'CessionDetailTypeName' >
   			</TD>  
    </tr>
   	</Table>
  </div>
   	<br>
    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divContRisk);"></td>
    	<td class= titleImg>险种信息</td></tr>
    </table>
    <Div  id= "divContRisk" style= "display: ''">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanContRiskGrid" ></span>
        		</td>
      		</tr>
    	</table>
	</div>	
    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divContGetDuty);"></td>
    	<td class= titleImg>责任信息</td></tr>
    </table>
    <Div  id= "divContGetDuty" style= "display:'none'">
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanContGetDutyGrid" ></span>
        		</td>
      		</tr>
    	</table>
	</div>

	<table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" ></td>
    	<td class= titleImg>分保信息</td></tr>
    </table>
    <div id= "divCessMO" style= "">
    <Table class= common>
   		<TR class= common>
        <TD class= title>
   				最低再保保额
        </TD>
        <TD class= input>
        	<Input class= common name= FacLowReAmnt >
        	<input type="hidden" name="NamLowReAmnt" value="最低再保保额" >
        	<input type="hidden" name="ValLowReAmnt" value="Float" >
        </TD>
        <TD class= title>
   				最高再保保额
        </TD>
        <TD class= input>
        	<Input class= common name= FacAutoLimit >
        	<input type="hidden" name="NamAutoLimit" value="最高再保保额" >
        	<input type="hidden" name="ValAutoLimit" value="Float" >
        </TD>
        <td class="title">
        	盈余佣金比例
        </td>
        <td class="input">
        	<Input class= common name= FacProfitCom > 
        	<input type="hidden" name="NamProfitCom" value="盈余佣金比例" >
        	<input type="hidden" name="ValProfitCom" value="Float" >
        </td>
      </TR>  
      </Table>
      </div>
      <div id="divCessRate" style= ""> 
      <Table class= common>   
      <TR>
      <TD class= title>
             分保比例
        </TD>
        <TD class= input>
	        	<Input class= common name= FacCessionRate> 
	        	<input type="hidden" name="NamCessionRate" value="分保比例" >
	        	<input type="hidden" name="ValCessionRate" value="Float" >
        </TD>
      	<TD class= title>
   		 		接受份额
   			</TD>
   			<TD class= input >
   				<Input class= common name= FacAccQuot> 
   				<input type="hidden" name="NamAccQuot" value="接受份额" >
   				<input type="hidden" name="ValAccQuot" value="Float" >
   			</TD>
        <TD class= title>
   		 		选择折扣比例
   			</TD>
   			<TD class= input>
   				<Input class= common name= FacChoiRebaRate> 
   				<input type="hidden" name="NamChoiRebaRate" value="选择折扣比例" >
   				<input type="hidden" name="ValChoiRebaRate" value="Float" >
   			</TD>
       
      </TR>
      </Table>
       </div>
       <div id= "divCessMO2" style= "">
    <Table class= common>
      <TR> 
       <TD class= title>
   				管理费比例
        </TD>
        <TD class= input>
        	<Input class= common name= FacReinManFeeRate >
        	<input type="hidden" name="NamReinManFeeRate" value="管理费比例" >
        	<input type="hidden" name="ValReinManFeeRate" value="Float" >
        </TD>		
        <td class="title">
        	手续费比例
        </td>
        <td class="input">
        	<Input class= common name= FacReinProcFeeRate > 
        	<input type="hidden" name="NamReinProcFeeRate" value="手续费比例" >
        	<input type="hidden" name="ValReinProcFeeRate" value="Float" >
        </td>
      	
   			<TD class= title>
   				特殊佣金比例
        </TD>
        <TD class= input>
        	<Input class= common name= FacSpeComRate >
        	<input type="hidden" name="NamSpeComRate" value="特殊佣金比例" >
        	<input type="hidden" name="ValSpeComRate" value="Float" >
        </TD>
    </TR>
     <TR>   
        <td class="title">
        	理赔参与限额
        </td>
        <td class="input">
        	<Input class= common name= FacClaimAttLmt > 
        	<input type="hidden" name="NamClaimAttLmt" value="理赔参与限额" >
        	<input type="hidden" name="ValClaimAttLmt" value="Float" >
        </td>
      	<TD class= title>
   		 		理赔通知限额
   			</TD>
   			<TD class= input>
   				<Input class= common name= FacNoticeLmt > 
   				<input type="hidden" name="NamNoticeLmt" value="通知限额" >
   				<input type="hidden" name="ValNoticeLmt" value="Float" >
   			</TD>
   		<TD class= title>退保金提取</TD>
        <TD class= input>
        <Input type="hidden" name= FacTBLmt > 
        <input type="hidden" name="NamTBLmt" value="退保金提取" >
        <Input class="codeno" readOnly name= "ValTBLmt" value="1" CodeData="0|^1|否^2|是" 
          ondblClick="showCodeListEx('ValTBLmt',[this,TBName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('ValTBLmt',[this,TBName],[0,1],null,null,null,1);"><Input 
          class= codename name= 'TBName' elementtype=nacessary >
        </TD>
      </TR>
   	</Table>
   	</div>
   <div id= "divRelDiskM" style= "display: ''">
   	    <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divRelDisk);"></td>
    	<td class= titleImg>关联险种信息</td></tr>
    </table>
  
       	<Div  id= "divRelDisk" style= "display: ''">	
      <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
          		<span id="spanRelDiskGrid" ></span>
        		</td>
      		</tr>
    	</table>
	</div>
	</div>
  <input type=hidden name=OperateType >
 <input type=hidden name=riskcode >
  <input type=hidden name=flag >
   <input type=hidden name=recontcode >
  <Div id="savedisk" style="display:'none'">
  <input class=cssButton  VALUE="保 存" TYPE=button onClick="submitForm();">
  </Div>
  <Div id="updatedisk" style="display:'none'">
  	<input class=cssButton  VALUE="修 改" TYPE=button onClick="updateForm();">
  	&nbsp;&nbsp;&nbsp;&nbsp;
  	<input class=cssButton  VALUE="删 除" TYPE=button onClick="deleteForm();">
  </Div> 
   <Div id="confirmdisk" style="display:'none'">
  <input class=cssButton  VALUE="审核通过" TYPE=button onClick="confirmForm();">
  </Div>	
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>