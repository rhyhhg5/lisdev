<%
//程序名称：ReTempComManageSave.jsp
//程序功能：
//创建日期：2008-12-24
//创建人  ：liuli
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行合同保存Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  System.out.println("获取临分合同号码！！！");
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("StartDate",request.getParameter("StartDate"));
  tTransferData.setNameAndValue("EndDate",request.getParameter("EndDate"));
  tTransferData.setNameAndValue("ReContCode",request.getParameter("ReContCode"));


  ReTempGetDateBL mReTempGetDateBL= new ReTempGetDateBL();
  VData tVData = new VData();
  try
  {
  	tVData.addElement(globalInput);
    tVData.addElement(tTransferData);
    mReTempGetDateBL.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    Content = "临分合同提数失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReTempGetDateBL.mErrors;
    if (!tError.needDealError())
    {
      Content ="临分合同提数成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = "临分合同提数失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>