<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRNewAccountCheckInput.jsp
//程序功能：账单审核
//创建日期：2008-11-11
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LREstimateInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LREstimateInit.jsp"%>
  <title>账单审核</title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "./LREstimateSave.jsp" >
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInfo);"></td>
          <td class="titleImg">请输入查询条件</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divInfo" style= "display: ''">
    <table class=common>
    	<tr class=common>
    		 <td class="title">再保合同号</td>
				<td class="input">
					<Input class="code" name="RecontCode" 
						ondblClick="return showCodeList('RecontCode',[this,null],[0,1],null,null,null,1);" 
						onkeyup="return showCodeListKey('RecontCode',[this,null],[0,1],null,null,null,1);">
				</td>	
   			<td class="title">分保类型</td>
	       		<td class="input">
	   				<input class="codeno" readOnly name= "TempCessFlag" CodeData="0|^Y|临分^N|合同" 
	  					ondblClick="showCodeListEx('ReinsureType',[this,TempCessFlagName],[0,1],null,null,null,1);	"
	  					onkeyup="showCodeListKeyEx('ReinsureType',[this,TempCessFlagName],[0,1],null,null,null,1);"><input class="codename" name= "TempCessFlagName" readonly>
	   			</td> 
   			<TD  class= title>险种编码</TD>
        <TD  class= input> <Input class="codeno" name=RiskCode 
          ondblclick = "return showCodeList('RiskCode',[this,RiskName],[0,1],null,null,null,1,300);"
          onkeyup = "return showCodeListKey('RiskCode',[this,RiskName],[0,1],null,null,null,1,300);"><Input class="codename" name= 'RiskName' readonly> 
        </TD>  			
        </tr>
    	<tr class=common>
      	<TD class= title>
   		 		再保公司
   			</TD>
   			<TD class= input >
   				<input class="codeno" name="ReComCode" 
	            ondblclick="return showCodeList('reinsurecomcode',[this,ReComCodeName],[0,1],null,null,null,1,300);"
	            onkeyup="return showCodeListKey('reinsurecomcode', [this,ReComCodeName],[0,1],null,null,null,1,300);"  elementtype=nacessary><Input class="codename" name= 'ReComCodeName' readonly>
   			</TD>
   			<TD class= title>
   				分保方式
        </TD>
        <TD class= input>
        	<Input class="codeno" readOnly name= "CessionMode" CodeData="0|^1|成数^2|溢额" 
          ondblClick="showCodeListEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);" ><Input class="codename" name= 'CessionModeName' readonly>
        </TD>
       <TD class= title>
   				预估数类型
        </TD>
        <TD class= input>
        	<Input class="codeno" readOnly name= "EType" CodeData="0|^1|分保^2|理赔" 
          ondblClick="showCodeListEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);" ><Input class="codename" name= 'CessionModeName' readonly>
        </TD>
      </tr>
     	<TR  class= common>
						<TD  class= title>起始日期</TD>
	          <TD  class= input> 
	          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="起始日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	          <TD  class= title>终止日期</TD>
	          <TD  class= input> 
	          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="终止日期|notnull&Date" elementtype=nacessary> 
	          </TD> 
	         
	        </TR>
  	</table>   
  </Div>
  <INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
	<Div  id= "divResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanLRCessGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">  
	</Div>
	<Div  id= "divResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanLRClaimGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">  
	</Div>

  
  <INPUT class=cssButton VALUE="下载分保预估数" TYPE=button onClick="onloadCesslist()">
  <INPUT class=cssButton VALUE="下载理赔摊回预估数" TYPE=button onClick="onloadClmlist()">
  <input type="hidden" name="OperateType" >
  <input type=hidden class=Common name=querySql >
  <Div id=DivFileDownload style="display:'none'">
    <A id=fileUrl href=""></A>
  </Div>
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>