var showInfo;
var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if(beforeSubmit()== false)
		{
			return false;
		}
		
		if( verifyInput() == true) 
		{
			if (veriryInput3()==true)
			{
			var	risk = ContRiskGrid. mulLineCount;//险种记录数
			var duty = ContGetDutyGrid. mulLineCount;//责任记录数
			var rel = RelDiskGrid.mulLineCount;//关联险种记录数
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		    fm.action="./ReNewContManageSave.jsp?risk="+risk+"&duty="+duty+"&rel="+rel;
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  		alert("ddddddd");
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}
//合同修改
function updateForm(){
fm.OperateType.value="UPDATE";
	try 
	{
		if(beforeSubmit()== false)
		{
			return false;
		}
		if( verifyInput() == true) 
		{
			if (veriryInput3()==true)
			{
			var	risk = ContRiskGrid. mulLineCount;//险种记录数
			var duty = ContGetDutyGrid. mulLineCount;//责任记录数
			var rel = RelDiskGrid.mulLineCount;//关联险种记录数
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		    fm.action="./ReNewContManageSave.jsp?risk="+risk+"&duty="+duty+"&rel="+rel;
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

//合同删除
function deleteForm(){
 fm.OperateType.value="DELETE";
	try 
	{
		if( verifyInput() == true) 
		{
			if (veriryInput2()==true)
			{
			var	risk = ContRiskGrid. mulLineCount;//险种记录数
			var duty = ContGetDutyGrid. mulLineCount;//责任记录数
			var rel = RelDiskGrid.mulLineCount;//关联险种记录数
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		    fm.action="./ReNewContManageSave.jsp?risk="+risk+"&duty="+duty+"&rel="+rel;
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }	
}
//合同审核
function confirmForm(){
	 fm.OperateType.value="CONFIRM";
	try 
	{
		if(beforeSubmit()== false)
		{
			return false;
		}
		if( verifyInput() == true) 
		{
			var	risk = ContRiskGrid. mulLineCount;//险种记录数
			var duty = ContGetDutyGrid. mulLineCount;//责任记录数
			var rel = RelDiskGrid.mulLineCount;//关联险种记录数
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		    fm.action="./ReNewContManageSave.jsp?risk="+risk+"&duty="+duty+"&rel="+rel;
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }	
}

//删除合同的之前的校验
function veriryInput2()
{
	var sqla = "select count(*) from lrpol where recontcode='"+ fm.all('ReContCode').value+"' fetch first 10 rows only with ur";
	var ssrs = easyExecSql(sqla);
	if(ssrs[0][0]>"0"){
	    if(confirm("该合同下存在已分保的保单，不能删除本合同！")){
	       return false;
	    }	
	}
	return true;
	
}
//合同保存之前校验
function veriryInput3()
{
	if(compareDate(fm.RValidate.value,fm.RInvalidate.value)==1)
	{
		alert("起始日期不能大于终止日期!");
		return false;
	}
	return true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode)
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
  	
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  if (fm.OperateType.value=="DELETE")
	  {
	  //	resetForm();
	  // window.parent.location.reload(); 
       window.parent.close(); 
           
	  }
  }
}

function veriryInput5()
{
	
	return true;
	
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
	if(fm.CessionMode.value=="2"&&(fm.CessionDetailType.value==""||fm.CessionDetailType.value==null))
	{
		alert("分保方式为：风险溢额的再保合同必须选择【溢额合同类型细分】！");
		return false;
	}
	return true;
	
}           


function addCessInfo()
{
	if (fm.ReContCode.value==''||fm.ReContCode.value==null)
	{
		alert("请先查询再保合同再添加分保信息！");
		return false;
	}
	var strSQL="select ReContCode from LRContInfo where ReContCode='"+fm.ReContCode.value+"'";
	var arrResult=easyExecSql(strSQL);
	if (arrResult==null||arrResult=="")
	{
		alert("合同编号为空或未保存合同基本信息！");
		return false;
	}
	var reContCode = fm.all('ReContCode').value;
	var cessionMode = fm.all('CessionMode').value;
	var diskKind = fm.all('DiskKind').value;
  var varSrc="&ReContCode="+ reContCode+"&CessionMode="+cessionMode+"&DiskKind="+diskKind ;
  window.open("./FrameMainCessInfo.jsp?Interface=CessInfoInput.jsp"+varSrc,"true");                   
}  

//function getDutyCode(){
//   var rowNum = ContRiskGrid. mulLineCount;
//   var riskcode = "''";
//   for(var i=0;i<rowNum;i++){
//   	 code = ContRiskGrid.getRowColData(i,1);
//   	 riskcode = riskcode+",'"+code+"'";
//   }
  // 	alert(riskcode);
//   var sql = "select getdutycode,getdutyname from lmdutyget where getdutycode in (select getdutycode from lmdutygetrela where dutycode in "+
//                   "(select dutycode from lmriskduty where riskcode in ("+riskcode+"))) with ur";
   // alert(sql)
//   var ssrs = easyExecSql(sql);
//   alert(ssrs);              
                   
//}

function getRiskCode()
{
	var selno = ContRiskGrid.getSelNo();
	if (selno !=0){
     ContGetDutyGrid.setRowColData(0,3,ContRiskGrid.getRowColData(selno-1,1)); 
 //    alert(ContGetDutyGrid.getRowColData(0,3));
    }
}

function afterCodeSelect( cCodeName, Field ){
	//如果按责任分保，则显示责任信息。
	if(cCodeName=="ReType"){
	if(fm.ReType.value=="01"){
	  divContGetDuty.style.display = 'none';	
	}else if(fm.ReType.value=="02"){
	  divContGetDuty.style.display ='';
	  
	}
   }
   if(ContRiskGrid. mulLineCount > 0){
   	// alert("ffffffffffff")
   	// fm.riskcode.value=ContRiskGrid.getRowColData(0,1);
   	// ContGetDutyGrid.setRowColData(0,3,ContRiskGrid.getRowColData(0,1));
   	// alert("ddddddd"+ContGetDutyGrid.getRowColData(0,3)+fm.riskcode.value)
   }
   if(cCodeName=="CessionMode"){
	if(fm.CessionMode.value=="2"){
	  //divCessMO.style.display = 'none';	
	  //divCessMO2.style.display = 'none';
	  divRelDiskM.style.display = 'none';
	  document.getElementById('cessionDetailTR').style.display='';
	  //fm.FacLowReAmnt.disabled=true;
	  //fm.FacAutoLimit.disabled=true;
	  //fm.FacProfitCom.disabled=true;
	 // fm.FacSpeComRate.disabled=true;
	  //fm.FacClaimAttLmt.disabled=true;
	  //fm.FacNoticeLmt.disabled=true;
	  
	}else {
	  //divCessMO.style.display ='';
	  //divCessMO2.style.display ='';
	  divRelDiskM.style.display = '';
	  fm.FacLowReAmnt.disabled=false;
	  fm.FacAutoLimit.disabled=false;
	  fm.FacProfitCom.disabled=false;
	  fm.FacSpeComRate.disabled=false;
	  fm.FacClaimAttLmt.disabled=false;
	  fm.FacNoticeLmt.disabled=false;
	  document.getElementById('cessionDetailTR').style.display='none';
	  fm.all("CessionDetailType").value="";
	  fm.all("CessionDetailTypeName").value="";
	  
	}
	  
	
   }
}
