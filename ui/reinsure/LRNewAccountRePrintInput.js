var showInfo;

var turnPage = new turnPageClass(); 

function easyQueryClick()
{
	if(fm.Year.value==''){
		alert('年份和月份必须录入!');
		return false;
	}
	if(fm.Month.value==''){
 		alert('年份和月份必须录入!');
		return false;
 	}
	var day = showMonthLastDay();  //获取提取月份的最后一天
    var StartDate = fm.Year.value+"-"+fm.Month.value+"-1";  //格试化日期
    var EndDate =fm.Year.value+"-"+fm.Month.value+"-"+day;
    var strWhere = " and a.StartDate='"+StartDate+"' and a.endData='"+EndDate+"' ";
 
 var strSQL="select  a.recontcode,StartDate,enddata,sumloan,reprocfee,claimbackfee,Payflag,standby3,a.ActuGetNo "
 					+"from lraccounts a  where a.payflag in ('01','02','03') and a.standby3<>'0' "
 					+ strWhere
 					+ getWherePart("a.RecontCode","RecontCode")
 				    + getWherePart("a.Payflag","ActuGetState")
 					+"with ur";
	  turnPage.queryModal(strSQL, NewAccountPrintGrid); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	easyQueryClick();
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}


function checkNum(control){
   var a = control.value.match(/^(-?\d+)(\.\d+)?$/); 
   if (a == null) {
      alert('数值不符合');
			control.select();
			control.focus();
			return false;
   } 
}


function check(){
	if(vCheck()==false){
		return false;
	}
	try {
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
//				fm.OperateType.value="CHECK";
				fm.submit();
  	} catch(ex) 
  	{
  		showInfo.close();
  		alert(ex);
  	}
}

function vCheck(){
	var rowCount=0;
	for(var i=0;i<NewAccountPrintGrid.mulLineCount;i++){
		if(NewAccountPrintGrid.getChkNo(i)==true){
			if(NewAccountPrintGrid.getRowColData(i,7)!="03"){
                 alert("存在非结算的账单,请重新选择!");
                 return false;
			}
			rowCount++;
		}
	}
	if(rowCount==0){
		alert('至少选择一条信息');
		return false;
	}
}

function showMonthLastDay()
 {
        var tmpDate=new Date(fm.Year.value,fm.Month.value,1); 
        var MonthLastDay=new Date(tmpDate-86400000);
        return MonthLastDay.getDate();      
 }

        

