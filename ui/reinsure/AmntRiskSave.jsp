<%
//程序名称：AmntRiskSave.jsp
//程序功能：
//创建日期：2006-08-24
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 	<%@page import="com.sinosoft.lis.reinsure.*"%>
 
	<%@page contentType="text/html;charset=GBK" %>
<%
  System.out.println("开始执行Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  LRAmntRelRiskSchema mLRAmntRelRiskSchema = new LRAmntRelRiskSchema();
  LRAmntRelRiskSet mLRAmntRelRiskSet = new LRAmntRelRiskSet();
  
  LRContInfoSchema mLRContInfoSchema = new LRContInfoSchema();
    
  ReAmntRiskUI mReAmntRiskUI = new ReAmntRiskUI();
  
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String mDescType = "";//将操作标志的英文转换成汉字的形式
  
  System.out.println("开始进行获取数据的操作！！！");
  
  mLRContInfoSchema.setReContCode(request.getParameter("ReContCode"));
	
  String[] strNumber 		= request.getParameterValues("RelDiskGridNo");
  String[] strGroupName = request.getParameterValues("RelDiskGrid1");
  String[] strRiskCode 	= request.getParameterValues("RelDiskGrid2");
  
	System.out.println("in jsp1 ->before submitData..........");    
  if(strNumber!=null)
  {
  	int tLength = strNumber.length;
    for(int i = 0 ;i < tLength ;i++)
    {
      LRAmntRelRiskSchema tLRAmntRelRiskSchema = new LRAmntRelRiskSchema();
			tLRAmntRelRiskSchema.setReContCode(request.getParameter("ReContCode")); 
      tLRAmntRelRiskSchema.setGroupName(strGroupName[i]); 
      tLRAmntRelRiskSchema.setRiskCode(strRiskCode[i]);
      
      mLRAmntRelRiskSet.add(tLRAmntRelRiskSchema);
    }
  }

  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增再保公司";
  }
  if(mOperateType.equals("UPDATE"))
  {
    mDescType = "修改再保公司信息";
  }
  if(mOperateType.equals("DELETE"))
  {
    mDescType = "删除再保公司";
  }
  if(mOperateType.equals("QUERY"))
  {
    mDescType = "查询再保公司";
  }

  VData tVData = new VData();
  try
  {
  	tVData.addElement(globalInput);
    tVData.addElement(mLRContInfoSchema);
    tVData.addElement(mLRAmntRelRiskSet);
    mReAmntRiskUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReAmntRiskUI.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+"成功，"+" 合同编号："+mReAmntRiskUI.getResult();
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mReAmntRiskUI.getResult()%>");
</script>
</html>