<%
//程序名称：ReTempComManageSave.jsp
//程序功能：
//创建日期：2008-12-24
//创建人  ：liuli
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行合同保存Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String mDescType = "";//将操作标志的英文转换成汉字的形式  
  System.out.println("开始进行获取数据的操作！！！");
  
  //处理再保合同信息
  LRTempCessContSchema mLRTempCessContSchema = new LRTempCessContSchema();
  mLRTempCessContSchema.setTempContCode(		request.getParameter("TempContCode"));
  mLRTempCessContSchema.setTempContName(		request.getParameter("TempContName"));
  mLRTempCessContSchema.setComCode(			request.getParameter("ReComCode"));
  mLRTempCessContSchema.setContno(			request.getParameter("Contno"));
  mLRTempCessContSchema.setContType(		request.getParameter("ContType"));
  mLRTempCessContSchema.setReContState(			request.getParameter("ReContState"));//合同状态
  mLRTempCessContSchema.setDiskKind(			request.getParameter("DiskKind"));
  mLRTempCessContSchema.setCessionMode(		request.getParameter("CessionMode"));//分保方式。成数，溢额
  mLRTempCessContSchema.setCessionFeeMode(request.getParameter("CessionFeeMode"));
  mLRTempCessContSchema.setReType(		request.getParameter("ReType"));//分保规则，按责任/按险种
  mLRTempCessContSchema.setContPlanCode("1");
  System.out.println("合同信息获取完成！");
  
  //处理计算要素值信息

  
  int xx = 0;
  if(request.getParameter("ReType").equals("01")){
     xx = Integer.parseInt(request.getParameter("risk"));  
  }
  
  String[] strRiskCode= new String[xx] ;
  String[] strRiskName= new String[xx] ;
  String[] strGetDutyCode = new String[1];
  String tRChk[]=request.getParameterValues("InpContRiskGridChk");//险种列表
  String tPChk[]=request.getParameterValues("InpPlanGridChk");//保障计划列表
  if(request.getParameter("ReType").equals("01")){//按险种分保，获取险种信息
//  System.out.println("fdsffffffffffffffafdsfdasa"+xx);
	strRiskCode 		= request.getParameterValues("ContRiskGrid1");
	
  }
  String factorCode = "";
  String factorName = "";
  String factorValue = "";
   LRTempCessContInfoSet mLRTempCessContInfoSet = new LRTempCessContInfoSet();
  if(tRChk!=null)
  {      
  	int tRLength=tRChk.length;
    for(int i=0;i<tRLength;i++)
    {
    	if(tRChk[i]!=null && tRChk[i].equals("1")){
	  		LRTempCessContInfoSchema mLRTempCessContInfoSchema = new LRTempCessContInfoSchema();
	  		mLRTempCessContInfoSchema.setTempContCode(request.getParameter("TempContCode"));
	  		mLRTempCessContInfoSchema.setRiskCode(strRiskCode[i]);
	  		
	    		mLRTempCessContInfoSchema.setGetDutyCode("A");
	    	
	  		mLRTempCessContInfoSchema.setCessionMode(request.getParameter("CessionMode"));	  	
	  		mLRTempCessContInfoSchema.setCalMode(request.getParameter("CalMode")); //最低再保保额	
	  		String tRetainAmnt=request.getParameter("RetainAmnt");
	  		if(tRetainAmnt==null||"".equals(tRetainAmnt)||"0".equals(tRetainAmnt))
	  			tRetainAmnt="99999999";
	  		mLRTempCessContInfoSchema.setRetainAmnt(tRetainAmnt); //最高再保保额 
	  		mLRTempCessContInfoSchema.setCessionRate(request.getParameter("CessionRate")); //分保比例 
	  		String tAccQuot=request.getParameter("AccQuot");
	  		if(tAccQuot==null||"".equals(tAccQuot)||"0".equals(tAccQuot))
	  			tAccQuot="1";
	  		mLRTempCessContInfoSchema.setAccQuot(tAccQuot); //接受份额
	  		mLRTempCessContInfoSchema.setReinProcFeeRate(request.getParameter("ReinProcFeeRate")); //再保险手续费比例
	  		mLRTempCessContInfoSchema.setSpeComRate(request.getParameter("SpeComRate")); //特殊佣金比例
	  		mLRTempCessContInfoSchema.setChoiRebaRate(request.getParameter("ChoiRebaRate")); //选择佣金比例
	  		mLRTempCessContInfoSchema.setProfitCom(request.getParameter("ProfitCom")); //盈余佣金比例
	  		mLRTempCessContInfoSchema.setReinManFeeRate(request.getParameter("ReinManFeeRate")); //管理费比例
	  		String tClaimNoticeLmt=request.getParameter("ClaimNoticeLmt");
	  		if(tClaimNoticeLmt==null||"".equals(tClaimNoticeLmt)||"0".equals(tClaimNoticeLmt))
	  			tClaimNoticeLmt="50000";
	  		mLRTempCessContInfoSchema.setClaimNoticeLmt(tClaimNoticeLmt); //理赔通知限额
	  		String tClaimAttLmt=request.getParameter("ClaimAttLmt");
	  		if(tClaimAttLmt==null||"".equals(tClaimAttLmt)||"0".equals(tClaimAttLmt))
	  			tClaimAttLmt="50000";
	  		mLRTempCessContInfoSchema.setClaimAttLmt(tClaimAttLmt); //理赔参与限额
	  		mLRTempCessContInfoSchema.setReserve1(request.getParameter("CessPremRate")); //再保净费率
	  		if(tPChk!=null){
	  			int tCLength=tPChk.length;
	  			String tContPlanCode[] = request.getParameterValues("PlanGrid1");
	  			for(int t=0;t<tCLength;t++)
	  		    {
	  		    	if(tPChk[t]!=null && tPChk[t].equals("1")){
	  		    		System.out.println("*************"+tCLength+"******************"+tContPlanCode[t]);
	  		    		LRTempCessContInfoSchema tLRTempCessContInfoSchema = new LRTempCessContInfoSchema().getSchema();
	  		    		tLRTempCessContInfoSchema.setSchema(mLRTempCessContInfoSchema);
	  		    		tLRTempCessContInfoSchema.setContPlanCode(tContPlanCode[t]);
	  					mLRTempCessContInfoSet.add(tLRTempCessContInfoSchema);
	  		    	}
	  		    }
	  		}
	  		else{
	  			mLRTempCessContInfoSchema.setContPlanCode("1");
	  			mLRTempCessContInfoSet.add(mLRTempCessContInfoSchema);
	  		}
	  		 	
    		}
    }
    
  } 
  System.out.println("要素完成");
  //处理关联险种信息
  LRAmntRelRiskSet mLRAmntRelRiskSet = new LRAmntRelRiskSet();
  
  //按险种分保时才有关联险种的累计
  if(request.getParameter("ReType").equals("01")){
//  String[] strRelNumber 	= request.getParameterValues("RelDiskGridNo");
  String[] strRelRiskCode 	= request.getParameterValues("RelDiskGrid1"); 
  if(strRelRiskCode!=null)
  {
  	int tLength = strRelRiskCode.length;
    for(int i = 0 ;i < tLength ;i++)
    {
      LRAmntRelRiskSchema tLRAmntRelRiskSchema = new LRAmntRelRiskSchema();
	  tLRAmntRelRiskSchema.setReContCode(request.getParameter("TempContCode")); 
      tLRAmntRelRiskSchema.setGroupName("B");  //标记临分合同 
      tLRAmntRelRiskSchema.setRiskCode(strRelRiskCode[i]);
      
      mLRAmntRelRiskSet.add(tLRAmntRelRiskSchema);
    }
  }
  System.out.println("关联险种完成");
  }
    
 
  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增再保合同";
  }else if(mOperateType.equals("UPDATE")){
  	 mDescType = "修改再保合同";
  }else if(mOperateType.equals("DELETE")){
  	mDescType = "删除再保合同";
  }else if(mOperateType.equals("CONFIRM")){
    mDescType = "临分合同审核";
  }
  
  ReTempContManageBL mReTempContManageBL = new ReTempContManageBL();
  VData tVData = new VData();
  try
  {
  	tVData.addElement(globalInput);
    tVData.addElement(mLRTempCessContInfoSet);
    tVData.addElement(mLRTempCessContSchema);
    tVData.addElement(mLRAmntRelRiskSet);
    mReTempContManageBL.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReTempContManageBL.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+"成功，"+" 合同编号："+mReTempContManageBL.getResult();
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mReTempContManageBL.getResult()%>");
</script>
</html>