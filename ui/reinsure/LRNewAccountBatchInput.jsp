<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LRNewAccountBatchInput.jsp
//程序功能：
//创建日期：20068-10-31
//创建人  ：JavaBean
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LRNewAccountBatchInput.js"></SCRIPT> 
	<%@include file="LRNewAccountBatchInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
</head>

<body onload="initElementtype();initForm();">    
  <form action="" method=post name=fm target="fraSubmit" >
    <div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCessGetData);"></td>
          <td class="titleImg">再保账单</td>
        </tr>
      </table>
  	</div>
    <br>
    <Div  id= "divCessGetData" style= "display: ''">
	    <table class= common border=0 width=100%>
	      	<TR  class= common>
						<TD  class= title>年份</TD>
	          <TD  class= input> 
	          	<Input class="code" readOnly name= "Year"  CodeData="0|^1|2008^2|2009^3|2010^4|2011^5|2012^6|2013^7|2014^8|2015^9|2016^10|2017^11|2018^12|2019^13|2020^14|2021^15|2022^16|2023^17|2024^18|2025^19|2026^20|2027" 
          		ondblClick="showCodeListEx('Year',[this],[1],null,null,null,1);	"
          		onkeyup="showCodeListKeyEx('Year',[this],[1],null,null,null,1);" elementtype=nacessary>
	          </TD> 
	          <TD  class= title>月份</TD>
	          <TD  class= input> 
	          	<Input class="code" readOnly name= "Month"  CodeData="0|^1|月^2|月^3|月^4|月^5|月^6|月^7|月^8|月^9|月^10|月^11|月^12|月" 
          			ondblClick="showCodeListEx('Month',[this],[0],null,null,null,1);	"
          			onkeyup="showCodeListKeyEx('Month',[this],[0],null,null,null,1);" elementtype=nacessary>
	          </TD> 
	        </TR>
	    </table>
	    <br>
		<INPUT class=cssButton  VALUE="提数" TYPE=button onClick="getData();">	
		<!--INPUT class=cssButton  VALUE="批处理维护" TYPE=button onClick="BatchMaitenData();"-->	
		</Div>
    <input type="hidden" name=OperateType value="">
    <input type="hidden" name="Operator" value="<%=Operator%>">
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 