<%
//程序名称：ReComManageSave.jsp
//程序功能：
//创建日期：2006-08-17
//创建人  ：张斌
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  
  LRReComInfoSchema mLRReComInfoSchema = new LRReComInfoSchema();
  LRReComRelaSchema mLRReComRelaSchema = new LRReComRelaSchema();
  
  LRReComInfoSet mLRReComInfoSet = new LRReComInfoSet();
  LRReComRelaSet mLRReComRelaSet = new LRReComRelaSet();
    
  ReComManageUI mReComManageUI = new ReComManageUI();
  
  CErrors tError = null;
  String mOperateType = request.getParameter("OperateType");
  System.out.println("操作的类型是"+mOperateType);
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String mDescType = "";//将操作标志的英文转换成汉字的形式
  
  System.out.println("开始进行获取数据的操作！！！");
  mLRReComInfoSchema.setReComCode(	request.getParameter("ReComCode"));
  mLRReComInfoSchema.setReComName(	request.getParameter("ReComName"));
  mLRReComInfoSchema.setSimpName(		request.getParameter("SimpleName"));
  mLRReComInfoSchema.setEngName(		request.getParameter("EngName"));
  mLRReComInfoSchema.setPostalCode(	request.getParameter("PostalCode"));
  mLRReComInfoSchema.setAddress(		request.getParameter("Address"));
  mLRReComInfoSchema.setFaxNo(			request.getParameter("FaxNo"));
  mLRReComInfoSchema.setWebAddress(	request.getParameter("WebAddress"));
  mLRReComInfoSchema.setNote(				request.getParameter("Note"));
  
System.out.println("in jsp0 ->before submitData..........");    
  /****************************************************************************
   * 判断单证的类型，若是定额单证则要对定额单险种信息表进行描述
   ***************************************************************************/

  String[] strNumber 			= request.getParameterValues("RelateGridNo");
  String[] strRelaName 		= request.getParameterValues("RelateGrid1");
  String[] strDepartment 	= request.getParameterValues("RelateGrid2");
  String[] strDuty 				= request.getParameterValues("RelateGrid3");
  String[] strRelaTel 		= request.getParameterValues("RelateGrid4");
  String[] strMobileTel 	= request.getParameterValues("RelateGrid5");
  String[] strFaxNo 			= request.getParameterValues("RelateGrid6");
  String[] strEmail 			= request.getParameterValues("RelateGrid7");
  String[] strRelaCode		= request.getParameterValues("RelateGrid8");
	
System.out.println("in jsp1 ->before submitData..........");    
  if(strNumber!=null)
  {
  	int tLength = strNumber.length;
    for(int i = 0 ;i < tLength ;i++)
    {
      LRReComRelaSchema tLRReComRelaSchema = new LRReComRelaSchema();
			tLRReComRelaSchema.setReComCode(request.getParameter("ReComCode")); //需要setRelaCode
      tLRReComRelaSchema.setRelaName(strRelaName[i]); //需要setRelaCode
      tLRReComRelaSchema.setDepartment(strDepartment[i]);
      tLRReComRelaSchema.setDuty(strDuty[i]);
      tLRReComRelaSchema.setRelaTel(strRelaTel[i]);
      tLRReComRelaSchema.setMobileTel(strMobileTel[i]);
      tLRReComRelaSchema.setFaxNo(strFaxNo[i]);
      tLRReComRelaSchema.setEmail(strEmail[i]);
      tLRReComRelaSchema.setRelaCode(strRelaCode[i]);
      
      mLRReComRelaSet.add(tLRReComRelaSchema);
    }
  }

  if(mOperateType.equals("INSERT"))
  {
    mDescType = "新增再保公司";
  }
  if(mOperateType.equals("UPDATE"))
  {
    mDescType = "修改再保公司信息";
  }
  if(mOperateType.equals("DELETE"))
  {
    mDescType = "删除再保公司";
  }
  if(mOperateType.equals("QUERY"))
  {
    mDescType = "查询再保公司";
  }

  VData tVData = new VData();
  try
  {
  	tVData.addElement(globalInput);
    tVData.addElement(mLRReComInfoSchema);
    tVData.addElement(mLRReComRelaSet);
    mReComManageUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReComManageUI.mErrors;
    if (!tError.needDealError())
    {
      Content = mDescType+"成功，"+" 批次号："+mReComManageUI.getResult();
    	FlagStr = "Succ";
    }
    else
    {
    	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mReComManageUI.getResult()%>");
</script>
</html>