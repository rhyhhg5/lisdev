var showInfo;

var turnPage = new turnPageClass(); 
window.onfocus=myonfocus;

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.OperateType.value="INSERT";
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput3()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput3()
{
	return true;
}

function easyQueryClick()
{
	var strSQL="";
	if(fm.ModifyFlag.value=="1") //如果是临分协议修改
	{
		strSQL = "select prtno,AppntName,(Case appflag when '0' then '未签单' when '1' then '已签单' end ),'',ManageCom from LCCont a "
		+ "where ContType = '1' and exists (select 1 from LCPol where reinsureflag in('3','4','5') and ContNo = a.ContNo)" 
		+ getWherePart("PrtNo","PrtNo")
		+ getWherePart("AppntName","AppntName","like")
		+ getWherePart("ManageCom","ManageCom","like")
		+ getWherePart("AppFlag","State")
		+ " with ur "
		;
	} 
	else
	{
		strSQL = "select prtno,AppntName,(Case appflag when '0' then '未签单' when '1' then '已签单' end ),'',ManageCom from LCCont a "
		+ "where ContType = '1' and exists (select 1 from LCPol where reinsureflag = '2' and ContNo = a.ContNo)" 
		+ getWherePart("PrtNo","PrtNo")
		+ getWherePart("AppntName","AppntName","like")
		+ getWherePart("ManageCom","ManageCom","like")
		+ getWherePart("AppFlag","State")
		+ " with ur "
		;
	}
	turnPage.queryModal(strSQL, PolGrid); 
}

function cessConclusion() //临分结论
{
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert("请先选择保单！");
		return false;
	}
	
  var cPrtNo 	= PolGrid.getRowColData(tSel-1, 1); 
	//var cProposalNo = PolGrid.getRowColData(tSel-1, 2);
  var cAppntName = PolGrid.getRowColData(tSel-1, 2); 
  var cManageCom = PolGrid.getRowColData(tSel-1, 5); 
  var cModifyFlag= fm.ModifyFlag.value;
  window.open("./FramCessConclusion.jsp?LoadFlag=6&PrtNo="+cPrtNo+"&AppntName="+cAppntName+"&ManageCom="+cManageCom+"&ModifyFlag="+cModifyFlag,"CessConclusion");
}


/*********************************************************************
 *  投保信息列表中选择一条，查看投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showContInfo()
{
	//从客户投保信息列表中选择一条，查看投保信息
	var tSel = PolGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，或输入保单号，再点击保单信息查询按钮。" );
		return false;
	}
	var prtNo = PolGrid.getRowColData(tSel-1,1);
	var strSQL = "select ContNo from LCCONT where prtno='"+prtNo+"'";
	var arrResult=easyExecSql(strSQL);
	if(arrResult==null)
	{
		alert("没有要查询的保单！");
		return false;
	}
	var cContNo=arrResult[0];
	window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType=1");	
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, ReComCode, CertifyCode )
{
  showInfo.close();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  } else { 
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  resetForm();
  }
}

function updateClick()
{
	fm.OperateType.value="UPDATE";
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput4()==true)
			{
		  	var i = 0;
		  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput4() //UPDATE 校验
{
	return true;
	
}

function deleteClick()
{
	fm.OperateType.value="DELETE";
	if(!confirm("你确定要删除该批次吗？"))
	{
		return false;
	}
	try 
	{
		if( verifyInput() == true && RelateGrid.checkValue("RelateGrid")) 
		{
			if (veriryInput5()==true)
			{
		  	var i = 0;
		  	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
				fm.action="./ReComManageSave.jsp";
		  	fm.submit(); //提交
	  	}
	  	else
	  	{
	  	}
	  }
  } catch(ex) 
  {
  	showInfo.close( );
  	alert(ex);
  }
}

function veriryInput5()
{
	return true;
}

function afterQuery()
{
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
  	fm.reset();
  	PolGrid.clearData();
	  initForm();
  }
  catch(re)
  {
  	alert("在CertifySendOutInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

