//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var DealWithNam;

function cessDataDetail1() //分保计算结果明细
{
	if(verifyInput() == false){
  	return false;
  }
  var showStr="正在生成分保结果数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./ReinsureCessList.jsp?StartDate="+fm.StartDate.value+"&EndDate="+fm.EndDate.value+"&ReRiskSort="+fm.ReRiskSort.value+"&RecontCode="+fm.RecontCode.value+"&ReRiskCode="+fm.ReRiskCode.value;
	fm.submit();
}

function edorDataDetail() //保全计算结果明细
{
	if(verifyInput() == false)
    return false;
  var showStr="正在生成保全结果数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./ReinsureEdorList.jsp?StartDate="+fm.StartDate.value+"&EndDate="+fm.EndDate.value+"&ReRiskSort="+fm.ReRiskSort.value+"&RecontCode="+fm.RecontCode.value+"&ReRiskCode="+fm.ReRiskCode.value;
	fm.submit();
}

function claimDataDetail() //理赔计算结果明细
{
	if(verifyInput() == false)
    return false;
  var showStr="正在生成理赔结果数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./ReinsureClaimList.jsp?StartDate="+fm.StartDate.value+"&EndDate="+fm.EndDate.value+"&ReRiskSort="+fm.ReRiskSort.value+"&RecontCode="+fm.RecontCode.value+"&ReRiskCode="+fm.ReRiskCode.value;
	fm.submit();
}
function afterSubmit(FlagStr,content)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}
