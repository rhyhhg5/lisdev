//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
 	var strSQL = "";
	//if(fm.PageFlag.value=="CONT")
	//{
	  strSQL = "select Operator,(select username from lduser where usercode=a.operator),CHAR(MAKEDATE)||' '||CHAR(MAKETIME), "
			+ " (select codename from LDCODE WHERE CODETYPE = 'uwflag' and code='9'), uwidea, GrpcontNo "
			+ " From lcgcuwmaster a where 1=1 and a.Proposalgrpcontno=(select proposalgrpcontno from lcgrpcont where prtno='"+fm.PrtNo.value+"')"  //
	  ;
	  
	  var arrResult = new Array();
		arrResult = easyExecSql(strSQL);
		if(arrResult==null)
		{
			alert("没有相应核保信息！");
			return false;
		}
	  
	//}else
	//{
	//	strSQL = "select RecontCode,ReContName,a.ReComCode,b.ReComName from LRContrInf a,LRReComInfo b where 1=1 and a.Recomcode=b.ReComCode "
	//	+ " and RecontCode in (select distinct RecontCode from LRCalFactorValue )"
	//  ;
	//  strSQL = strSQL +" order by ReComCode";
	//}
	displayMultiline(arrResult,GrpUWGrid);
	//turnPage.queryModal(strSQL, GrpUWGrid);
  
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}

	parent.fraMain.rows = "0,0,0,0,*";
}

function ClosePage()
{
	top.close();
}