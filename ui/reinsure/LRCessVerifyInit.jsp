<%
//Creator :张斌
//Date :2006-10-24
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 GlobalInput tG = new GlobalInput();
 tG=(GlobalInput)session.getValue("GI");
 String CurrentDate	= PubFun.getCurrentDate();   
 String CurrentTime	= PubFun.getCurrentTime();
 String Operator   	= tG.Operator;
%>

<script language="JavaScript">
function initInpBox()
{
  try
  { 
		fm.StartDate.value='';
		fm.EndDate.value='';
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initCessGrid();
    initBQCessGrid();
    initClaimGrid();
    initListGrid();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCessGrid()
{                               
  var iArray = new Array();
    
    try
    {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="lrpol数据量";         		//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
    iArray[2]=new Array();
    iArray[2][0]="lrpolresult数据量";        //列名
    iArray[2][1]="60px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="lrpolresult不为0数据量";        //列名
    iArray[3][1]="60px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    
 
    
          
    LRCessGrid = new MulLineEnter( "fm" , "LRCessGrid" ); 
    //这些属性必须在loadMulLine前
    LRCessGrid.mulLineCount = 0;   
    LRCessGrid.displayTitle = 1;
    LRCessGrid.locked = 1;
    LRCessGrid.canSel = 0;
    LRCessGrid.canChk =1; 
    LRCessGrid.hiddenPlus = 1;
    LRCessGrid.hiddenSubtraction = 1;
    LRCessGrid.loadMulLine(iArray);     

    //这些操作必须在loadMulLine后面
    }
    catch(ex)
    {
      alert(ex);
    }
}

function initBQCessGrid()
{                               
  var iArray = new Array();
    
    try
    {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="lrpoledor数据量";         		//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
    iArray[2]=new Array();
    iArray[2][0]="lrpoledorresult数据量";        //列名
    iArray[2][1]="60px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="lrpoledorresult不为0数据量";        //列名
    iArray[3][1]="60px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    
 
    
          
    LRBQCessGrid = new MulLineEnter( "fm" , "LRBQCessGrid" ); 
    //这些属性必须在loadMulLine前
    LRBQCessGrid.mulLineCount = 0;   
    LRBQCessGrid.displayTitle = 1;
    LRBQCessGrid.locked = 1;
    LRBQCessGrid.canSel = 0;
    LRBQCessGrid.canChk =1; 
    LRBQCessGrid.hiddenPlus = 1;
    LRBQCessGrid.hiddenSubtraction = 1;
    LRBQCessGrid.loadMulLine(iArray);     

    //这些操作必须在loadMulLine后面
    }
    catch(ex)
    {
      alert(ex);
    }
}

function initClaimGrid()
{                               
  var iArray = new Array();
    
    try
    {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="lrpolclm数据量";         		//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
    iArray[2]=new Array();
    iArray[2][0]="lrpolclmresult数据量";        //列名
    iArray[2][1]="60px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="lrpolclmresult不为0数据量";        //列名
    iArray[3][1]="60px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    
 
    
          
    LRClaimGrid = new MulLineEnter( "fm" , "LRClaimGrid" ); 
    //这些属性必须在loadMulLine前
    LRClaimGrid.mulLineCount = 0;   
    LRClaimGrid.displayTitle = 1;
    LRClaimGrid.locked = 1;
    LRClaimGrid.canSel = 0;
    LRClaimGrid.canChk =1; 
    LRClaimGrid.hiddenPlus = 1;
    LRClaimGrid.hiddenSubtraction = 1;
    LRClaimGrid.loadMulLine(iArray);     

    //这些操作必须在loadMulLine后面
    }
    catch(ex)
    {
      alert(ex);
    }
}

function initListGrid()
{                               
  var iArray = new Array();
    
    try
    {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="lrcesslist数据量新单";         		//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="lrcesslist数据量保全";         		//列名
    iArray[2][1]="60px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
    iArray[3]=new Array();
    iArray[3][0]="lrclaimlist数据量";        //列名
    iArray[3][1]="60px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    
 
    
          
    LRListGrid = new MulLineEnter( "fm" , "LRListGrid" ); 
    //这些属性必须在loadMulLine前
    LRListGrid.mulLineCount = 0;   
    LRListGrid.displayTitle = 1;
    LRListGrid.locked = 1;
    LRListGrid.canSel = 0;
    LRListGrid.canChk =1; 
    LRListGrid.hiddenPlus = 1;
    LRListGrid.hiddenSubtraction = 1;
    LRListGrid.loadMulLine(iArray);     

    //这些操作必须在loadMulLine后面
    }
    catch(ex)
    {
      alert(ex);
    }
}

</script>