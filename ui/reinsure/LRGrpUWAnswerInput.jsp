<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRReinsureAnswerInput.jsp
//程序功能：再保审核功能
//创建日期：2006-11-29 11:10:36
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <!--<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>-->
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="LRGrpUWAnswerInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRGrpUWAnswerInputInit.jsp"%>
  <title>再保信息</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./ReInsureSave.jsp" ENCTYPE="multipart/form-data">
    <%@include file="../common/jsp/InputButton.jsp"%>
      
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskInfo);">
    	   	</td>
    		  <td class= titleImg>
    			 团体险种保单信息 
    	   	</td>
    	</tr>
  </table>
	<Div  id= "divRiskInfo" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					  <span id="spanRiskInfoGrid" >
					  </span> 
				    </td>
			  </tr>
		</table>   
  </div>   

      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAnswerIdea);">
    		</td>
    		<td class= titleImg>
    			 回复意见 
    		</td>
    	</tr>
    </table>   
    <Div  id= "divAnswerIdea" style= "display: ''">
	    <table> 
	    	<tr>   
	           <TD  class= common>
	             <textarea name="Remark" value="" cols="120" rows="3" class="common" onfocus=clearData() verify="回复意见|len<=1000">
	             </textarea>
	           </TD>               
	      </tr>
	    </table>
  	</Div>
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divReInsureTask);">
    		</td>
    		<td class= titleImg>
    			 再保审核任务 
    		</td>
    	</tr>
    </table>
	<Div  id= "divReInsureTask" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					<span id="spanReInsureAuditGrid" >
					</span> 
				</td>
			</tr>
		</table>
		<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">  
		</Div>
	  
	  <table>
	  	<tr>
	      	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSendAnswerInfo);">
	  		</td>
	  		<td class= titleImg>
	  			 发送/回复意见信息
	  		</td>
	  	</tr>
	  </table>
	  <Div  id= "divSendAnswerInfo" style= "display: ''">
		  <table> 
		    <tr>   
		      <TD  class= common>
		        <textarea name="SendAnswerRemark" value="" cols="120" rows="3" class="common" verify="审核意见|len<=1000" ReadOnly >
		        </textarea>
		      </TD>               
		    </tr>
		  </table>
		</Div>
  </Div>  
  
  <input type="hidden" name="ProposalNo">
  <input type="hidden" name="PrtNo">
</form>  

<form action="./UpLodeSave.jsp" method=post name=fmImport target="fraSubmit" ENCTYPE="multipart/form-data">
	<div id="divDiskInput" style="display:''">
	  <table class = common >    	
		  <TR>
		    <TD  width='8%' style="font:9pt">
		      选择导入的文件：
		    </TD>     
		    <TD  width='80%'>
		      <Input  type="file"　width="50%" name=FileName class= common>
		     <INPUT VALUE="上载附件" class=cssButton TYPE=hidden onclick="ReInsureUpload();">
		    </TD>
		   </TR>
	  </table>
	</div>
	<br> 
	<table class = common>  
	  <TR class = common >
	    <td class = common align="left">
				<INPUT VALUE="回复意见" class=cssButton TYPE=button onclick="SendUWReInsure();">
		    <input type="hidden" class= Common name= FilePath value= "">
		    <INPUT VALUE="&nbsp;下&nbsp;&nbsp;载&nbsp;" class=cssButton TYPE=button onclick="DownLoad();">
				<INPUT VALUE="办 结" class=cssButton onclick="ReInsureOver();" type=hidden >
	    </td>
	  </TR>
	</table>
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>