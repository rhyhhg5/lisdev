<%
//Creator :张斌
//Date :2006-08-17
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
 		String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);       
    
    String reContCode = request.getParameter("ReContCode");
    String cessionMode = request.getParameter("CessionMode");
    String riskSort = request.getParameter("RiskSort");
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.ReContCode.value  = "<%=reContCode%>";
    fm.CessionMode.value = "<%=cessionMode%>";
    fm.RiskSort.value 	 = "<%=riskSort%>";
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("2在CertifyDescInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initRelDiskGrid();
    initInfo();
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initRelDiskGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;
    
    iArray[1]=new Array();
    iArray[1][0]="组名";         			//列名
    iArray[1][1]="60px";            	//列宽
    iArray[1][2]=60;            			//列最大值
    iArray[1][3]=2;              			//2表示是代码选择
    iArray[1][10]="PremProp";					//保费特性
    iArray[1][11]="0|^1|A|^2|B|^3|C^4|D";
    iArray[1][12]="1|0"		  //引用代码对应第几列，'|'为分割符
    iArray[1][13]="1|0"		  //上面的列中放置引用代码中第几位值,即:结果的值的位数
    
    iArray[2]=new Array();
    iArray[2][0]="险种代码";
    iArray[2][1]="150px";
    iArray[2][2]=100;
    iArray[2][3]=2;
    iArray[2][4]="RiskCode";
    iArray[2][5]="2|3";             //引用代码对应第几列，'|'为分割符
    iArray[2][6]="0|1";             //上面的列中放置引用代码中第几位值

    iArray[3]=new Array();
    iArray[3][0]="单证名称";         	//列名
    iArray[3][1]="250px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    //iArray[3]=new Array();
    //iArray[3][0]="保费份额";         	//列名
    //iArray[3][1]="60px";            	//列宽
    //iArray[3][2]=60;            			//列最大值
    //iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    RelDiskGrid = new MulLineEnter( "fm" , "RelDiskGrid" );
    RelDiskGrid.mulLineCount = 0;
    RelDiskGrid.displayTitle = 1;
    //RelDiskGrid.canSel=1;
    RelDiskGrid.loadMulLine(iArray);
    RelDiskGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert("初始化时出错2003-05-21"+ex);
  }
}

</script>
