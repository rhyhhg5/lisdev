//               该文件中包含客户端需要处理的函数和事件
var DealWithNam ;
var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var mDebug="0";
var mOperate="";
var arrDataSet;
var ImportPath;

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function importCessData()
{
	if(fm.FileName.value==""||fm.FileName.value==null)
	{
		alert("录入导入文件路径！");
		return false;
	}
	var i = 0;
  getImportPath();
  ImportFile = fm.all('FileName').value;  
  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LRImportSave.jsp?ImportPath="+ImportPath+"&OperateType=CESS";
  fm.submit(); //提交
}

function importEdorData()
{
	if(fm.FileName.value==""||fm.FileName.value==null)
	{
		alert("录入导入文件路径！");
		return false;
	}
	var i = 0;
  getImportPath();
  ImportFile = fm.all('FileName').value;  
  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LRImportSave.jsp?ImportPath="+ImportPath+"&OperateType=EDOR";
  fm.submit(); //提交
}

function importClaimData()
{
	if(fm.FileName.value==""||fm.FileName.value==null)
	{
		alert("录入导入文件路径！");
		return false;
	}
	var i = 0;
  getImportPath();
  ImportFile = fm.all('FileName').value;  
  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LRImportSave.jsp?ImportPath="+ImportPath+"&OperateType=CLAIM";
  fm.submit(); //提交
}

////提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr,content)
{
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
	  //content="保存成功！";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

function getImportPath () {
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未找到上传路径");
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  ImportPath = turnPage.arrDataCacheSet[0][0];
	
}

function afterCodeSelect( cCodeName, Field )
{
}
