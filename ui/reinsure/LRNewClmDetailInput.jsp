<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LRNewClmDetailInput.jsp
//程序功能：理赔明细
//创建日期：2008-11-11
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LRNewClmDetailInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LRNewClmDetailInit.jsp"%>
  <title>保全明细</title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "./LRNewClmDetailSave.jsp" >
	<Div style="width:200">
      <table class="common">
        <tr class="common">
          <td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInfo);"></td>
          <td class="titleImg">请输入查询条件</td>
        </tr>
      </table>
  </Div>
	<Div  id= "divInfo" style= "display: ''">
    <table class=common>
    	<tr class=common>
    		 <TD class= title>
   		 		再保合同号
   			</TD>
   			<TD class= input>
   				<Input class="code" name= RecontCode 
   				ondblClick="return showCodeList('RecontCode',[this,RecontCodeName],[0,1],null,null,null,1);" 
   				onkeyup="return showCodeListKey('RecontCode',[this,RecontCodeName],[0,1],null,null,null,1);">
   			</TD>
   			<TD  class= title>再保合同名称</TD>
           <TD  class= input> 
          	<Input class="common" name= 'RecontCodeName' >
              </TD>
   			 <TD class= title>
   		 		账单状态
   			</TD>
   			<TD class= input>
   				<Input class="codeno" readOnly name= "ActuGetState" CodeData="0|^00|未审核^01|清单已审核^02|账单已审核^03|结算" 
          ondblClick="showCodeListEx('ActuGetState',[this,ActuGetStateName],[0,1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('ActuGetState',[this,ActuGetStateName],[0,1],null,null,null,1);"><Input class="codename" name= 'ActuGetStateName' readonly>
   			</TD>
   			
        </tr>
    	<tr class=common>
      	<TD class= title>
   		 		再保公司
   			</TD>
   			<TD class= input >
   				<input class="codeno" name="ReComCode" 
	            ondblclick="return showCodeList('reinsurecomcode',[this,ReComCodeName],[0,1],null,null,null,1,300);"
	            onkeyup="return showCodeListKey('reinsurecomcode', [this,ReComCodeName],[0,1],null,null,null,1,300);"  elementtype=nacessary><Input class="codename" name= 'ReComCodeName' readonly>
   			</TD>
   			<TD class= title>
   				分保方式
        </TD>
        <TD class= input>
        	<Input class="codeno" readOnly name= "CessionMode" CodeData="0|^1|成数^2|溢额" 
          ondblClick="showCodeListEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('CessionMode',[this,CessionModeName],[0,1],null,null,null,1);" ><Input class="codename" name= 'CessionModeName' readonly>
        </TD>
        <TD class= title>
   		 		险种类别
   			</TD>
   			<TD class= input>
   				<Input class="codeno" readOnly name= "DiskKind" CodeData="0|^L|长险^M|短险" 
          ondblClick="showCodeListEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('DiskKind',[this,DiskKindName],[0,1],null,null,null,1);"><Input class="codename" name= 'DiskKindName' readonly>
   			</TD>
      </tr>
     	<tr class=common>     	
        <TD  class= title>
          年份
        </TD>
        <TD class= input>
   				<Input class="code" readOnly name= "Year" CodeData="0|^1|2008^2|2009^3|2010^4|2011^5|2012^6|2013^7|2014^8|2015^9|2016^10|2017^11|2018^12|2019^13|2020^14|2021^15|2022^16|2023^17|2024^18|2025^19|2026^20|2027" 
          ondblClick="showCodeListEx('Year',[this],[1],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('Year',[this],[1],null,null,null,1);" elementtype=nacessary><font color=red>*</font>
   			</TD>
          
        <TD class= title>
   		 		月份
   			</TD>
   			<TD class= input>
   				<Input class="code" readOnly name= "Month" CodeData="0|^1|月^2|月^3|月^4|月^5|月^6|月^7|月^8|月^9|月^10|月^11|月^12|月" 
          ondblClick="showCodeListEx('Month',[this],[0],null,null,null,1);	"
          onkeyup="showCodeListKeyEx('Month',[this],[0],null,null,null,1);" elementtype=nacessary><font color=red>*</font>
   			</TD>
   	   <TD  class= title>险种编码</TD>
        <TD  class= input> <Input class="codeno" name=RiskCode 
          ondblclick = "return showCodeList('RiskCode',[this,RiskName],[0,1],null,null,null,1,300);"
          onkeyup = "return showCodeListKey('RiskCode',[this,RiskName],[0,1],null,null,null,1,300);"><Input class="codename" name= 'RiskName' readonly> 
        </TD>
  		</tr>
  		<tr class=common>
  			<TD  class= title>
  				保单号
        </TD>
       
        	<td class="input"><input class="common" name="ContNo"/></td>
       
        <TD  class= title>
          投保人
        </TD>
        <TD  class= input>
        	<Input class="common" name="AppntName" >
        </TD>
        <TD  class= title>
        	被保险人
        </TD>
        <TD  class= input>
        	<Input class="common" name="InsuredName" >
        </TD>
  		</tr>
  	</table>   
  </Div>
  <INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
  
  <!--  div style="width:200">
	    	<table class="common">
	      		<tr class="common">
	      			<td class="common"><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divResult);"></td>
	      			<td class="titleImg">下载合同选择</td>
	      		</tr>
	    	</table>
		</div>
		<div id="divCont" style="display:''">
    		<table class="common">
        		<tr class="common">
    	  			<td text-align:left colSpan="1">
					 	<span id="spanContGrid" >
					 </span> 
				  </td>
			 	</tr>
			</table>   
  		</div>
  		<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">  
	</Div-->
	<Div  id= "divResult" style= "display: ''">
    <table  class= common>
        <tr  class= common>
    	  	<td text-align: left colSpan=1>
					 <span id="spanNewClmDetailGrid" >
					 </span> 
				  </td>
			 </tr>
		</table>   
  </Div>
	<Div id= "div1" align="center" style= "display: '' ">
			<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">  
	</Div>

  <INPUT class=cssButton VALUE="审核通过" TYPE=hidden onClick="check()">
  <INPUT class=cssButton VALUE="下载清单" TYPE=hidden onClick="onloadlist()">
  <input type="hidden" name="OperateType" >
  <Div id=DivFileDownload style="display:'none'">
    <A id=fileUrl href=""></A>
  </Div>
</form>	  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>