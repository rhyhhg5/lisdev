<%
//程序名称：LRNewReSureSave.jsp
//程序功能：
//创建日期：2008-11-15
//创建人  ：sunyu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.reinsure.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行Save页面");
  GlobalInput globalInput = new GlobalInput( );
	globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  ReNewReSureUI mReNewReSureUI = new ReNewReSureUI();
  CErrors tError = null;

  String FlagStr = "";
  String Content = "";
  String mRecontCode= request.getParameter("RecontCode");
  String mActuGetState= request.getParameter("ActuGetState");
  String mYear= request.getParameter("Year");
  String mMonth= request.getParameter("Month");

	String mOperateType = request.getParameter("OperateType");
  String tChk[]=request.getParameterValues("InpNewReSureGridChk");
  String tActuGetNo[] = request.getParameterValues("NewReSureGrid9");
  LRAccountsSet tLRAccountsSet=new LRAccountsSet();
  if(tChk!=null){
  	for(int i=0;i<tChk.length;i++){
			if(tChk[i]!=null && tChk[i].equals("1")){
				LRAccountsSchema tLRAccountsSchema=new LRAccountsSchema();
//				tLRAccountsSchema.setRiskCode(tRiskCode[i]);
//				tLRAccountsSchema.setReContCode(tRecontCode[i]);
				tLRAccountsSchema.setActuGetNo(tActuGetNo[i]);
				tLRAccountsSet.add(tLRAccountsSchema);
			}
		}
	}
  

	TransferData tTransferData = new TransferData();
 	
 	//查询条件
 	tTransferData.setNameAndValue("ReContCode",mRecontCode);
 	tTransferData.setNameAndValue("PayFlag",mActuGetState);
 	tTransferData.setNameAndValue("Year",mYear);
 	tTransferData.setNameAndValue("Month",mMonth);

  VData tVData = new VData();
  try
  {
  	tVData.addElement(tTransferData);
  	tVData.addElement(globalInput);
  	tVData.addElement(tLRAccountsSet);
    mReNewReSureUI.submitData(tVData,mOperateType);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = mReNewReSureUI.mErrors;
    if (!tError.needDealError())
    {
      Content = "保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

