<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：BriefContDeleteInit.jsp
//程序功能：简易保单整单删除初始化
//创建日期：2007-11-22
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	String strManageCom = globalInput.ComCode;
	String strOperator = globalInput.Operator;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
    // 保单查询条件  
    fm.all('fileName').value = '';
    //fm.all('fileType').value = '';  
   // fm.all('fileTypeName').value = ''; 
    //fm.all('fileDetailType').value = '';  
    //fm.all('fileDetailTypeName').value = '';                              
  }
  catch(ex)
  {
    alert("在downLoadInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
	  initFileGrid();
    initInpBox();
    
  }
  catch(ex)
  {
    alert("在downLoadInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }  
}


// 保单信息列表的初始化
function initFileGrid()
{                               
	var iArray = new Array();
	
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=30;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="文件编号";         			//列名
		iArray[1][1]="100px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="文件名";         			//列名
		iArray[2][1]="100px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
			
		iArray[3]=new Array();
		iArray[3][0]="文件描述";         			//列名
		iArray[3][1]="200px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="相对路径";         			//列名
		iArray[4][1]="50px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		
		FileGrid = new MulLineEnter( "fm" , "FileGrid" ); 
		//这些属性必须在loadMulLine前
		FileGrid.mulLineCount = 5;   
		FileGrid.displayTitle = 1;
		FileGrid.locked = 1;
		FileGrid.canSel = 1;
		FileGrid.hiddenPlus = 1;
		FileGrid.hiddenSubtraction = 1;
		FileGrid.loadMulLine(iArray);
		
		FileGrid.selBoxEventFuncName = "FileSelect"; 
	}
  catch(ex)
  {
    alert(ex);
  }
}


</script>