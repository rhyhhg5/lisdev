//程序名称：ScanContInput.js
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
//通过数据库同一个机构查询单号最大值
function testReturn(){
	var strSQL = "";
			 strSQL = "select max(missionprop1) from lwmission where 1=1 "
				 + " and activityid = '0000006024' "
				 + " and processid = '0000000006'"
				 + " and missionprop2 = '"+fm.InputDate.value+" '"
				 +"and missionprop3 = '"+fm.ManageCom.value+"'" 
			 var arr=easyExecSql(strSQL);
		//返回最大值
		return arr;
}
/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{
	var ManageCom=fm.ManageCom.value;
	var length=ManageCom.length;
	//调用定义的函数返回数据库查询的最大值（单号最大值）
	var arr = testReturn();
	//机构不足8位补全8位
	if(length!=8){
		for(i=1;i<=8-length;i++)
		ManageCom+=0;
	}
	var date=fm.InputDate.value;
	var date1=date.replace(/-/g,"");
	//拼接流水号
	var length2 = fm.PrtNo.value.length; 
	fm.PrtNo.value=ManageCom+date1+"001";	
	
	cPrtNo = fm.PrtNo.value;

	cManageCom = fm.ManageCom.value;
	
	 var ApplyDate = fm.InputDate.value;
	if(cPrtNo == "")
	{
		alert("请录入印刷号！");
		return;
	}
	
  if(strCurDay!==ApplyDate)
  {
    alert("申请日期不是当天！");
    return false;
  }
//	if(cManageCom == ""||cManageCom.length!=8)
//	{
//		alert("请录入8位管理机构！");
//		return;		
//	}
//	if(cPrtNo.length!=19)
//	{
//		alert("请输入19位的询价单号");
//		return;
//	}
  
  

  //将数据库查出来的最大值拼接成字符串 
  arr1 = arr+""; 
//截取数据库查出来单号的后三位流水转成int（十进制）
  var subArr = parseInt(arr1.substring(16,19),10);
	var tPrtNo=cPrtNo+"";
	//截取默认的单号流水后三位
	var subtPrtNo = parseInt(tPrtNo.substring(16,19),10);
	//定义变量
	var mPrtNo;
	//如果该天单数大于999
	if(subArr >= 999){
		alert("申请达到上限");
	}else{
		//判断数据库查出来的单号长度是否为19
		if(arr1.length!=19){
			//不是19也就是说明没有单号
			mPrtNo = cPrtNo;
			fm.PrtNo.value=mPrtNo;
			//提交保存直接结束此函数
			fm.submit();
			return;
		}
		
				//长促是19那么就+1
				subtPrtNo = subArr + 1;
		
	}
	
	
	//将流水转成字符串
	var subtPrtNo1 = subtPrtNo+"";
	//截取的数字为001的话会显示1判断长度是否为3不是的话在前面用0补全
	var length1 = subtPrtNo1.length;
	if(length1!=3){
		var temp;
		for(i=1;i<=3 -length1;i++)
			
		if(i == 1){
			temp = "0"+subtPrtNo;
		}else{
			temp = "0" + temp;
		}
	}
	//截取流水号前面的数字
	var sub = tPrtNo.substring(0,16,10);
	//alert(sub);
	 //拼接单号提交
	 mPrtNo = sub + temp;
	
	if(mPrtNo.length!=19)
	{
//	alert("印刷号必须为数字");
//	return;
	}
		//if (beforeSubmit() == false)
		//{
//		    alert("已存在该印刷号，请选择其他值!");
//		    return false;		
		//}
	fm.PrtNo.value=mPrtNo;
	fm.submit();
	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
	easyQueryClick();
}


/*********************************************************************
 *  执行新契约扫描的EasyQuery
 *  描述:查询显示对象是扫描件.显示条件:扫描件已上载成功
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	initGrpGrid();

	// 书写SQL语句
	var strSQL = "";
 	strSQL = "select lwmission.missionprop1,lwmission.missionprop3,lwmission.missionprop2,lwmission.missionprop4,lwmission.missionid,lwmission.submissionid from lwmission where 1=1 "
				 + " and activityid = '0000006024' "
				 + " and processid = '0000000006'"
				 + getWherePart('missionprop1','PrtNo')
				 + getWherePart('missionprop2','InputDate')
				 + getWherePart('missionprop3','ManageCom','=')
				 + " order by lwmission.missionprop1"
				 ;	 		 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有已申请的投保单，请录入印刷号开始录入！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}
function GoToInput()
{
  var i = 0;
  var checkFlag = 0;
  var state = "0";
  
  for (i=0; i<GrpGrid.mulLineCount; i++) {
    if (GrpGrid.getSelNo(i)) { 
      checkFlag = GrpGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
    var	prtNo = GrpGrid.getRowColData(checkFlag - 1, 1); 	
    var ManageCom = GrpGrid.getRowColData(checkFlag - 1, 2); 
    var MissionID = GrpGrid.getRowColData(checkFlag - 1, 5);
    var SubMissionID = GrpGrid.getRowColData(checkFlag - 1, 6);
    var InputDate = GrpGrid.getRowColData(checkFlag - 1, 3);
    //var GrpGrid4 =  GrpGrid.getRowColData(checkFlag 0, 6);
	  
    //var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&operator=" + operator + "&state=" + state;
    //var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    //var strReturn = window.showModalDialog(urlStr, "", sFeatures);
    var strReturn="1";
    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";
    if (strReturn == "1")
      window.open("./GrpContPolInputNoScanMain.jsp?ScanFlag=0&LoadFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&InputDate="+InputDate+"&SubMissionID="+SubMissionID, "", sFeatures);   
  }
//  +"&GrpGrid4="+GrpGrid4
  else {
    alert("请先选择一条保单信息！"); 
  }
  
}
function beforeSubmit()
{
	var strSQL = "";
	strSQL = "select missionprop1 from lwmission where 1=1 "
				 + " and activityid = '0000006024' "
				 + " and processid = '0000000006'" 
				 + " and missionprop1='"+fm.PrtNo.value+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
	//判断是否查询成功
	if (turnPage.strQueryResult) {
	    return false;
	}

	strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
			 "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
	//判断是否查询成功
	if (turnPage.strQueryResult) {
	    return false;
	}	   	
}
function GrpbeforeSubmit()
{
	var strSQL = "";
	strSQL = "select missionprop1 from lwmission where 1=1 "
				 + " and activityid = '0000002098' "
				 + " and processid = '0000000004'" 
				 + " and missionprop1='"+fm.PrtNo.value+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
	//判断是否查询成功
	if (turnPage.strQueryResult) {
		  alert('1');
	    return false;
	}

	strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
			 "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
	//判断是否查询成功
	if (turnPage.strQueryResult) {
		  alert('2');
	    return false;
	}	   	
}