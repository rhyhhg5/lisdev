<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<%  
	String tLoadFlag = "";
	String tGrpContNo = "";
	
	try
	{
		tLoadFlag = request.getParameter( "LoadFlag" );
		tGrpContNo = request.getParameter( "GrpContNo" );
		//默认情况下为个人保单直接录入
		if( tLoadFlag == null || tLoadFlag.equals( "" ))
			tLoadFlag = "2";//LoadFlag本身的 意义关键就在于个单部分
	}
	catch( Exception e1 )
	{
		tLoadFlag = "2";
	}
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
        System.out.println("LoadFlag:" + tLoadFlag);
         System.out.println("扫描类型:" + request.getParameter("scantype"));   
         System.out.println("扫描类型:" + request.getParameter("prtNo")); 
%> 
<script>
    var operator = "<%=tGI.Operator%>";
	var prtNo111 = "<%=request.getParameter("prtNo")%>";
	var polNo = "<%=request.getParameter("polNo")%>";
	var ManageCom = "<%=request.getParameter("ManageCom")%>";
	var scantype = "<%=request.getParameter("scantype")%>";
	var MissionID = "<%=request.getParameter("MissionID")%>";
	var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
	var InputDate="<%=request.getParameter("InputDate")%>";
	<%-- var CreatOperator="<%=request.getParameter("CreatOperator")%>"; --%>
        var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
        if (ScanFlag == null||ScanFlag=="null") 
        ScanFlag="0";
	if (polNo == "null") polNo = "";
	if (prtNo111 == "null") prtNo111 = "";
	var LoadFlag ="<%=tLoadFlag%>";
	//保全调用会传2过来，否则默认为0，将付值于保单表中的appflag字段
	var BQFlag = "<%=request.getParameter("BQFlag")%>";
	if (BQFlag == null||BQFlag=="null") 
	BQFlag = "0";
	//保全调用会传险种过来
	var BQRiskCode = "<%=request.getParameter("riskCode")%>";
	var currentDate = "<%=PubFun.getCurrentDate()%>";
</script>
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="ContPolLCImpart.js"></SCRIPT> <!--保障状况告知 ＆ 健康状况告知 引用函数-->
  <%@include file="GrpPriceQueryInit.jsp"%>
  <SCRIPT src="GrpPriceQueryInput.js"></SCRIPT>
  <SCRIPT src="../baojiaapp/Duty.js"></SCRIPT>
  
  
  
  <SCRIPT src="../app/ProposalAutoMove.js"></SCRIPT> 
  
  
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  
   
</head>

<body  onload="initForm();initElementtype();" >
  <form action="./AskContPolSave.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">

    <!-- 合同信息部分 AskGroupPolSave.jsp-->

  <DIV id=DivLCContButton STYLE="display:''">
  <table id="table1">
  		<tr>
  			<td>
  			<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroupPol1);">
  			</td>
  			<td class="titleImg">业务信息
  			</td>
  		</tr>
  </table>
</DIV>
<Div  id= "divGroupPol1" style= "display: ''">
   <table  class= common>
      <TR  class= common>
          <TD  class= title8>
            询价单号
          </TD>
          <TD  class= input8>
            <Input class= common name=PrtNo111  >
          </TD>
            <TD  class= title8>
            创建时间 
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker" elementtype=nacessary dateFormat="short" name=CreatDate    verify="创建时间|notnull&date" >
          </TD>
          <TD  class= title8>
            创建人
            </TD>
            <TD  class= input8>
            <Input class= common8  name=CreatOperator  readonly=true>
            </TD> 
          <TD  class= title8>
            管理机构
          </TD>
          <TD  class= input8>
            <Input class="codeNo" name=ManageCom  verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
          </TD> 
       
        <TR  class= common8>
        <TD  class= title8>
            销售渠道
            </TD>
            <TD  class= input8>
            <Input class="codeNo" name=AgentType ondblclick="return showCodeList('salechnl',[this,AgentTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('salechnl',[this,AgentTypeName],[0,1],null,null,null,1);" verify="销售渠道|notnull"><input class=codename name=AgentTypeName readonly=true elementtype=nacessary>
            </TD>
            <TD  class= title8>
            业务类型 
            </TD>
            <TD  class= input>
            <Input class=codeno name=MarketType CodeData="0|^1|直销业务^2|中介业务" ondblclick="return showCodeListEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);"verify="市场类型|notnull"><input class=codename name=MarketTypeName readonly=true elementtype=nacessary>
            </TD>
           
            <TD  class= title8>
            业务员代码
            </TD>
            <TD  class= input8>
      			<Input NAME=AgentCode  MAXLENGTH=8 CLASS=code8 elementtype=nacessary ondblclick="return queryAgent();"onkeyup="return queryAgent2();" verify="代理人编码|notnull">
            </TD>
            <TD  class= title8>
            业务员姓名
            </TD>
            <TD  class= input8>
      			<Input NAME=AgentName   CLASS=common8 readonly=true>
            </TD>
             <TR  class= common>
         	 <TD  class= title8>
            中介公司代码
            </TD>
            <TD  class= input8>
            <Input class=code8 name=AgentCom   ondblclick="return showCodeList('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey('AgentCom',[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" >
            </TD>
        	  <TD  class= title8>
            中介代理公司名称
            </TD>
            <TD  class= input8>
            <Input class=common8   name=AgentComName  >
            </TD>
            <TD  class= title8>
            业务提奖
            </TD>
            <TD  class= input8>
            <Input class="common"  name=AgentAward>（%）
            </TD>  
             <TD  class= title8>
            中介手续费率
            </TD>
            <TD  class= input8>
            <Input class="common"  name=AgentFee>（%）
            </TD>  
          </TR>
           <TR> 
           <TD  class= title8>
            投保类型 
            </TD>
            <TD  class= input>
            <Input class=codeno name=ApplyType CodeData="0|^1|新保^2|续保^3|转保" ondblclick="return showCodeListEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);"verify="市场类型|notnull"><input class=codename name=MarketTypeName readonly=true elementtype=nacessary>
            </TD>
        	  
        	   <TD  class= title8>
            保单生效日期 
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker" elementtype=nacessary dateFormat="short" name=CValiDate    verify="保单生效日期|notnull&date" >
          </TD>
           <TD  class= title8>
            保单终止日期 
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker" elementtype=nacessary dateFormat="short" name=CInValiDate    verify="保单终止日期|notnull&date" >
          </TD>
           <TD  class= title8>
            参保形式 
            </TD>
            <TD  class= input>
            <Input class=codeno name=ApplyType CodeData="0|^1|团体统一投保^2|成员自愿投保" ondblclick="return showCodeListEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);"verify="市场类型|notnull"><input class=codename name=MarketTypeName readonly=true >
            </TD>
         </TR>
      <tr class="common">
      	<td class= title8>是否为共保保单</td>
            <td class= input>
                <input class="codeNo" name="CoInsuranceFlag" value="0" verify="共保标志|notnull" CodeData="0|^0|非共保保单^1|共保保单" ondblclick="return showCodeListEx('CoInsuranceFlag',[this,CoInsuranceFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CoInsuranceFlag',[this,CoInsuranceFlagName],[0,1],null,null,null,1);"><input class="codename" name="CoInsuranceFlagName" readonly="readonly" elementtype="nacessary" />
            </td>
      </tr>  
     </table> 
</Div>
<table>
    <tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupPol2);">
    		  </td>
    		  <td class= titleImg>
<!--     		投保团体基本信息(团体保障号)(<INPUT id="butGrpNoQuery" class=cssButton VALUE="查  询" TYPE=button onclick="showAppnt();">	<Input class= common8 name=GrpNo>  )(首次投保单位无需填写团体保障号) -->
    		投保团体基本信息（团体客户号<INPUT id="butGrpNoQuery" class=cssButton VALUE="查  询"  name="Query" TYPE=button onclick="showAppnt();"><Input class= common8 name=GrpNo  verify="查询|int"> ）(首次报价单位无需填写客户号)
    		  </td>
    </tr>
</table>
    
<Div  id= "divGroupPol2" style= "display: ''">
    <table  class= common>
       <TR>
          <TD  class= title8>
            投保人名称
          </TD>
          <TD  class= input8>
            <Input class= common8 name=GrpName verify="投保人名称|notnull" elementtype=nacessary onchange=checkuseronly(this.value) verify="单位名称|notnull&len<=60">
          </TD>  
         <TD  class= title8>
            单位性质
          </TD>
          <TD  class= input8>
            <Input class=codeNo name=GrpNature  verify="单位性质|notnull&code:grpNature&len<=10" ondblclick="showCodeList('GrpNature',[this,GrpNatureName],[0,1]);" onkeyup="showCodeListKey('GrpNature',[this,GrpNatureName],[0,1]);"><Input class=codeName name=GrpNatureName  readonly elementtype=nacessary>          
          </TD> 
       <TD  class= title8>
            行业
          </TD>
          <TD  class= input8>
            <Input class="codeno" name=BusinessType  verify="行业|notnull&code:BusinessType&len<=20" ondblclick="return showCodeList('BusinessType',[this,BusinessTypeName],[0,1],null,null,null,null,300);" onkeyup="return showCodeListKey('BusinessType',[this,BusinessTypeName],[0,1],null,null,null,null,300);"><input class=codename name=BusinessTypeName elementtype=nacessary>
          </TD> 
           
        </TR>
        
     <TR  class= common>
           <TD  class= title8>
            单位所在地/单位注册地
          </TD>
          <TD  class= input8>
            <Input class= common8 name=LinkMan1  value="中国">
          </TD>  
           <TD  class= title8>
            省（自治区直辖市）
          </TD>
          <TD  class= input8>
      		<Input name=ProvinceID class=codeNo ondblclick="return showCodeList('Province1',[this,Province],[0,1],null,'0','Code1',1);"  onkeyup="return showCodeListKey('Province1',[this,Province],[0,1],null,'0','Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=Province class= codeName   readonly="readonly">
          </TD>
          <TD  class= title8>
            市
          </TD>
          <TD  class= input8>
              <Input name=CityID class=codeNo ondblclick="return showCodeList('City1',[this,City],[0,1],null,fm.ProvinceID.value,'Code1',1);"  onkeyup="return showCodeListKey('City1',[this,City],[0,1],null,fm.ProvinceID.value,'Code1',1);" onblur="FillAddress()" readonly="readonly"><Input name=City class= codeName   readonly="readonly">
          </TD>
      </TR>
        <TR  class= common>
        	  <TD  class= title8>
            员工总人数
            </TD>
            <TD  class= input8>
            <Input class= common8 name=Peoples  elementtype=nacessary verify="单位总人数|int&notnull">
            </TD>
            
           
            <TD  class= title8>
            在职人数
            </TD>
            <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=AppntOnWorkPeoples   verify="在职人数|int&notnull">
            </TD>
            <TD  class= title8>
            退休人数
            </TD>
            <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=AppntOffWorkPeoples   verify="退休人数|int&notnull">
            </TD>                                       
        </TR>
        <TR class= common>
        <TD  class= title8>
        	   其它人员人数
             </TD>
             <TD  class= input8>
             <Input class= common8  name=AppntOtherPeoples   verify="其它人员人数|int&notnull">
             </TD>  
             </TR>
        <TR class= common>
       	  <TD  class= title8>
            被保险人数（成员）
          </TD>
          <TD  class= input8>
          <Input name=Peoples3 class= common8 elementtype=nacessary verify="被保险人数（成员）|int&notnull">
          </TD>           
          <TD  class= title8>
            在职人数
          </TD>
          <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=OnWorkPeoples  verify="在职人数|int&notnull">
          </TD>
          <TD  class= title8>
            退休人数
          </TD>
          <TD  class= input8>
            <Input class= common8 elementtype=nacessary name=OffWorkPeoples   verify="退休人数|int&notnull">
          </TD>
        </TR>
       <TR  class= common>

        	 <TD  class= title8>
        	 其它人员人数
           </TD>
           <TD  class= input8>
            <Input class= common8  name=OtherPeoples   verify="其它人员人数|int&notnull">
           </TD>
           <TD  class= title8>
            连带被保险人数（家属）
           </TD>
           <TD  class= input8>
           <Input name=RelaPeoples  class= common8 verify="连带被保险人数（家属）|int&notnull">
           </TD>  
           <TD  class= title8>
            配偶人数
           </TD>
           <TD  class= input8>
           <Input name=RelaMatePeoples  class= common8 verify="配偶人数|int&notnull">
           </TD>                  
         </TR>  
         <TR  class= common>             
            <TD  class= title8>
            子女人数
            </TD>
            <TD  class= input8>
            <Input name=RelaYoungPeoples  class= common8 verify="子女人数|int&notnull">
            </TD>                                                    
            <TD  class= title8>
            其它人员人数
            </TD>
            <TD  class= input8>
            <Input name=RelaOtherPeoples  class= common8 verify="其它人员人数|int&notnull">
            </TD>
            </TR>
            <TR  class= common>             
            <TD  class= title8>
            平均年龄
            </TD>
            <TD  class= input8>
            <Input name=RelaYoungPeoples elementtype=nacessary class= common8 verify="子女人数|int&notnull">
            </TD>                                                    
            <TD  class= title8>
            最高年龄
            </TD>
            <TD  class= input8>
            <Input name=RelaOtherPeoples elementtype=nacessary class= common8 verify="其它人员人数|int&notnull">
            </TD>
             <TD  class= title8>
            最低年龄
            </TD>
            <TD  class= input8>
            <Input name=RelaOtherPeoples elementtype=nacessary class= common8 verify="其它人员人数|int&notnull">
            </TD>
            </TR>
      </table>
<div id ="div1" style="display: ''">
<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart5);">
					</td>
					<td class= titleImg>
					健康告知
				</td>
			</tr>
		</table>
		 <TD  class= title8>
		   客户告知状况
            </TD>
            <TD  class= input>
            <Input class=codeno name=CusNoteState CodeData="0|^1|能够获取客户告知^2|暂时无法获取客户告知" ondblclick="return showCodeListEx('CusNoteState',[this,CusNoteStatename],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CusNoteState',[this,CusNoteStatename],[0,1],null,null,null,1);""><input class=codename name=CusNoteStatename readonly=true elementtype=nacessary>
            </TD>
            <TD>
            <INPUT id="OneAllNot" class=cssButton VALUE="一键全否" TYPE=button onclick="OneAllNo();"> 
            </TD>
		<div  id= "divLCImpart5" style= "display:'';float:left;">
			<table width="942" border="0" align="center" cellpadding="0" cellspacing="0" class="muline">
  				<tr>
					<td colspan="7" class="mulinetitle2" height="20">&nbsp;</td>
				</tr>
  				<tr> 
    				<td colspan="7" class="mulinetitle3">1.是否定期组织被保险人或员工体检?&nbsp&nbsp&nbsp&nbsp<input type="checkbox" name="Detail201" class="box">&nbsp&nbsp 是 <input id="checkbox1" type="checkbox" name="Detail201" class="box">&nbsp&nbsp 否&nbsp&nbsp频次：<input name="Detail201" class = "common66" style="text-align:center;">次/年&nbsp&nbsp最近一次体检医院为： <input name="Detail201" class = "common66" style="width:200px;"></td>
  				</tr>  
  				<tr> 
    				<td colspan="6" class="mulinetitle3">2.被保险人在体检中是否有因异常情形而被建议接受其它检查或治疗的情况?&nbsp&nbsp&nbsp&nbsp</td>
    				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail202" class="box" onclick="InputDetailToMuline202();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" id="checkbox2" name="Detail202" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  				</tr>
  				<tr> 
    				<td colspan="6" class="mulinetitle3">3.目前被保险人中是否有正在住院治疗的情况?&nbsp&nbsp&nbsp&nbsp</td>
    				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail203" class="box" onclick="InputDetailToMuline203();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" id="checkbox3" name="Detail203" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  				</tr>
  				<tr> 
    				<td colspan="6" class="mulinetitle3">4.近一年内，被保险人中是否有因病不在工作岗位累计超过10天者？&nbsp&nbsp&nbsp&nbsp</td>
    				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail204" class="box" onclick="InputDetailToMuline204();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" id="checkbox4" id="checkbox1" name="Detail204" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  				</tr>
  				<tr> 
    				<td colspan="6" class="mulinetitle3">5.被保险人中是否有遭受意外伤害后，至今未愈或留有残疾后遗症者?&nbsp&nbsp&nbsp&nbsp</td>
    				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail205" class="box" onclick="InputDetailToMuline205();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" id="checkbox5" name="Detail205" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  				</tr>
  				<tr> 
    				<td colspan="6" class="mulinetitle3">6.过去五年内，被保险人中是否有曾患下列疾病而住院治疗或接受医师诊治者？
    													<br/>(1)高血压、冠心病、心律失常、心肌梗塞、主动脉瘤、心脏瓣膜疾病；
    													<br/>(2)脑中风(脑出血、脑栓塞、脑梗塞)、脑瘤、癫痫、重症肌无力、精神病；
    													<br/>(4)肝炎、肝硬化、胆石症、胰腺炎、消化性溃疡；
    													<br/>(5)肾炎、肾病综合症、肾功能不全、尿毒症、泌尿系统结石、肾囊肿；
    													<br/>(6)癌症(恶性肿瘤)或未经证实为良性或恶性之肿瘤、肿物、息肉或硬块；
    													<br/>(7)糖尿病、甲状腺功能亢进或低下；
    													<br/>(8)系统性红斑狼疮、类风湿性关节炎、强直性脊柱炎；
    													<br/>(9)艾滋病或艾滋病病毒携带者；
    													<br/>(10)慢性支气管炎、肺气肿、呼吸衰竭；
    													<br/>(11)白血病、再生障碍性贫血、淋巴瘤；
    													<br/>(12)先天性疾病、职业病、酒精或药物滥用成瘾；
    													<br/>(13)乳腺疾病、子宫肌瘤、卵巢囊肿等妇科疾病；
    													<br/>(14)以上未提及的其它疾病。
    													</td>
    				<td colspan="2" class="mulinetitle3"><input type="checkbox" name="Detail206" class="box" onclick="InputDetailToMuline206();">&nbsp&nbsp 是 <input class="common7"> <input type="checkbox" id="checkbox6" name="Detail206" class="box">&nbsp&nbsp 否 <input class="common7"></td>
  				</tr>
			</table>
			</div>
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanHealthImpartDetailGrid" >
						</span>
					</td>
				</tr>
			</table>
</Div>

<div>
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" />
                </td>
                <td class= titleImg>共保信息</td>
            </tr>
        </table>
        
        <table class="common">
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnCoInsuranceParam" name="btnCoInsuranceParam" value="录入共保要素信息" onclick="showCoInsuranceParamWin();" disabled="disabled" />
                </td>
            </tr>
        </table>
    </div>
<Div>
<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart5);">
					</td>
					<td class= titleImg>
					责任
				</td>
			</tr>
		</table>
    </Div>
    <Div>
    <table  class=common>
     <tr  class=common>
     <td text-align: left colSpan=1>
      <span id="spanRiskGrid1" >
      </span> 
      </td>
     </tr>
    </table>
</Div>

<div id ="div2" style="display: ''">
<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart5);">
					</td>
					<td class= titleImg>
					  责任信息 
				</td>
			</tr>
<Div  id= "divRiskCode0">

      <table class=common>
           <tr class=common>
          <TD  class= title>
            责任编码
          </TD>
          <TD  class= input>
                 <!--<Input class=codeNo  name="RiskCode1"  CodeData="0|^111111|一般意外身故^222222|一般意外残疾^333333|交通意外伤害^444444|意外医疗^555555|意外津贴"  ondblclick="return showCodeListEx('RiskCode1',[this,RiskCode1Name],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('RiskCode1',[this,RiskCode1Name],[0,1],null,null,null,1);"><input class=codename name=RiskCode1Name readonly=true elementtype=nacessary>-->           
                 <Input class=codeNo name="TxbjDutycode" verify="投保人证件类型|code:TxbjDutycode" ondblclick="return showCodeList('TxbjDutycode',[this,DutyCode1Name],[0,1]);" onkeyup="return showCodeListKey('TxbjDutycode',[this,DutyCode1Name],[0,1]);"><input class=codename name=DutyCode1Name readonly=true elementtype=nacessary>
          </TD>
       </tr>
      </table>  
       </table>
  
</Div>
<div>
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" />
                </td>
                <td class= titleImg>方案补充</td>
            </tr>
        </table>
        
        <table class="common">
        	<tr>
        		<td class=title>补充说明</td>
        	</tr>
            <tr>
               <td class=input>
						<textarea class=common name="discr" verify="补充说明|notnull" rows="5" cols="150"></textarea>
					</td>
            </tr>
        </table>
    </div>

 

<div id ="div3" style="display: ''">
<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart5);">
					</td>
					<td class= titleImg>
					  附件信息
				</td>
			</tr>
			
       </table>
       <table>
       		<tr class=common>
					<td class=titleImg colspan = "1">
						请选择要上传的附件:
					</td>
				</tr>
				<tr class=common>
				<td class=input >
						<input class=common type="file" name="fileName1" size="100">
					</td>
				</tr>	
				<tr>
					<td class=title colspan = "4">
						文件描述:
					</td>
				</tr>
				<tr>
					<td class=input colspan = "4">
						<textarea class=common name="discription" verify="文件描述|notnull" rows="5" cols="50"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						<input type="hidden" name=filePath>
						<input value="提  交" class=cssButton id="submitbutton" type=button name = submitbutton onclick="submitData();">
					</td>
				</tr>
       </table>
  
</Div>
<div>
	<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输入查询条件：
					</td>
				</tr>
			</table>
			<table class=common align=center border="0" width="30%">
				<tr>
					<td class=title>
						文件名称:
					</td>
					<td class=input>
						<input class=common type="text" name="fileName">
					</td>
					
				</tr>
			</table>
			<input value="查  询" class=cssButton type=button onclick="query111();">
			<input value="重  置" class="cssButton" type=button
				onclick="initForm();">
			<input class=common type=hidden name="FileCode">
			<!-- 查询结果 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divfile);">
					</td>
					<td class=titleImg>
						查询结果列表
					</td>
				</tr>
			</table>
		<table class=common>
			<!-- 信息（列表） -->
			<tr class="common">
			<td id="divFileGrid" style="display:''">
				<span id="spanFileGrid"></span>
			</td>
			</tr>
			</table>
			<table class=common>
			<tr><td>
				<!--div id="filesList" style="margin-top:10px;"></div-->	
				<input value="下  载" class=cssButton type=button onclick="downLoad();">
			</tr></td>
			</table>

</div>
<hr/>
<div id= "divnormalbtn" align= right>
<tr>
   <INPUT VALUE="录入完毕" class=cssButton TYPE=button onclick="submitClick();">
    <INPUT VALUE="修  改" class=cssButton TYPE=button onclick="updateClick();">
    </tr>
   </div>
  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
