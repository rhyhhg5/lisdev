<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：LaratecommisionSetInput.jsp
		//程序功能：个团保单明细查询
		//创建时间：2009-3-16
		//创建人  ：miaoxiangzheng
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<script language="JavaScript">
//var StrSql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode)";
</script>
	<script language="JavaScript">
   var msql=" 1 and   char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000#  ";
   var StrSql="1 and code not in (select riskcode from lmriskapp where risktype4=#4#)";
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="BJGrpCDQListRiskInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="BJGrpCDQListRiskInit.jsp"%>
		<%@include file="../common/jsp/ManageComLimit.jsp"%>

	</head>

	<body onload="initForm();initElementtype();">
		<form action="./GrpCDQListReport.jsp" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divQryModify);">
					</td>
					<td class=titleImg>
						查询条件
					</td>
				</tr>
			</table>
			<div id="divQryModify" style="display:''">
				<table class=common>
					<tr class=common>
						<td class=title>
							管理机构
						</td>
						<TD class=input>
							<Input class='codeno' name=ManageCom
								verify="管理机构|notnull&code:ComCode "
								ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,1,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,1,1);"><Input class=codename name=ManageComName readOnly elementtype=nacessary>
						</TD>

						<td class=title>
							代理人编码
						</td>
						<TD class=input>
							<Input class="common" name=AgentCode>
						</TD>
					</tr>
					<tr>
						<TD class=title>
							报价单录入起期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=StartDate>
						</TD>
						<TD class=title>
							报价单录入止期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=EndDate>
						</TD>
					</tr>
					<tr>
						<TD class=title>
							报价单核保完成起期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=StartCvaliDate>
						</TD>
						<TD class=title>
							报价单核保完成止期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" dateFormat="short" name=EndCvaliDate>
						</TD>
					</tr>
					
				</table>
			</div>
			<table>
				<tr>
					<td>
						<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
						<input type=button value="下  载" class=cssButton onclick="DoNewDownload();">
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSetGrid);">
					</td>
					<td class=titleImg>
						报价单明细查询结果显示
					</td>
				</tr>
			</table>

			<div id="divSetGrid" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanSetGrid"></span>
						</td>
					</tr>
				</table>
				<INPUT VALUE=" 首页 " TYPE="button" class=cssButton onclick="turnPage.firstPage();">
				<INPUT VALUE="上一页" TYPE="button" class=cssButton onclick="turnPage.previousPage();">
				<INPUT VALUE="下一页" TYPE="button" class=cssButton onclick="turnPage.nextPage();">
				<INPUT VALUE=" 尾页 " TYPE="button" class=cssButton onclick="turnPage.lastPage();">
				<br>
				<hr>
			</div>
			<input type=hidden id="sql_where" name="sql_where">
			<input type=hidden id="fmAction" name="fmAction">
			<input type=hidden class=Common name=querySql>
			<Input class="readonly" type=hidden name=diskimporttype>
		</form>
		<table>
			<br />
			<tr class=common>
				<td>
					<font color=red size=2> 说明：<br />
						1、报价单录入起期日期，报价单核保完成起期 中，需要至少录入一项。<br>
							</td>
			</tr>
		</table>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>