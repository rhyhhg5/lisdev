//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
parent.fraMain.rows = "0,0,0,0,*";

var turnPage = new turnPageClass();

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  fm.fmtransact.value = "INSERT||MAIN" ;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	window.focus();
  showInfo.close();
  disabledButton(false);
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
    queryCIParams();
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在TbGrpConractorInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“修改”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}
//初始化信息
function initDutyFactor(){
		var strSql = "select distinct CalFactor,FactorName,FactorNoti,(select distinct CalFactorValue from LCContPlanDutyParam where GrpContNo='"+GrpContNo+"' and ContPlanCode='11' and CalFactor=lmriskdutyfactor.CalFactor ),CalFactorType from lmriskdutyfactor where "
		+" RiskCode in (select RiskCode from lmriskapp where risktype8='4') and chooseflag='1' order by CalFactor";
		//turnPage.queryModal(strSql,ConractorGrid);
}

/**
 * 查询团体保单信息摘要。
 */
function queryGrpInfo()
{
    var tStrSql = ""
        + " select lgc.GrpContNo, lgc.ProposalGrpContNo, lgc.PrtNo, lgc.AppntNo, lgc.GrpName, lgc.ManageCom "
        + " from LCGrpCont lgc "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" + mGrpContNo + "' "
        ;

    var tArrResult = easyExecSql(tStrSql);
    if(tArrResult)
    {
        fm.PrtNo.value = tArrResult[0][2];
        fm.GrpContNo.value = tArrResult[0][0];
        fm.ProposalGrpContNo.value = tArrResult[0][1];
        fm.GrpAppntNo.value = tArrResult[0][3];
        fm.GrpAppntName.value = tArrResult[0][4];
        fm.ManageCom.value = tArrResult[0][5];
    }
}

function saveCIInfo()
{
    if(!CoInsuranceParamGrid.checkValue())
    {
        return false;
    }
    
    if(!checkCoInsuranceRate())
    {
        return false;
    }
    
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    fm.action = "CoInsuranceParamSave.jsp";
    fm.fmtransact.value = "Create";
    disabledButton(true);
    fm.submit();
}

function delCIInfo()
{
    if(CoInsuranceParamGrid.mulLineCount == 0)
    {
        alert("尚未保存信息。");
        return false;
    }
    
    if (confirm("您确实想删除该记录吗?"))
    {
        var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    
        fm.action = "CoInsuranceParamSave.jsp";
        fm.fmtransact.value = "Delete";
        disabledButton(true);
        fm.submit();
    }
}

function queryCIParams()
{
    var tStrSql = ""
        + " select lccip.AgentCom, lccip.AgentComName, lccip.Rate "
        + " from LCCoInsuranceParam lccip "
        + " where 1 = 1 "
        + " and lccip.GrpContNo = '" + mGrpContNo + "' "
        ;
    turnPage.queryModal(tStrSql, CoInsuranceParamGrid);
}

function disabledButton(flag)
{
    if(flag == false)
    {
        fm.btnSaveCIInfo.disabled = false;
        fm.btnDelCIInfo.disabled = false;
    }
    else
    {
        fm.btnSaveCIInfo.disabled = true;
        fm.btnDelCIInfo.disabled = true;
    }
}

function checkCoInsuranceRate()
{
    var tSumRate = 0;
    
    var tRows = CoInsuranceParamGrid.mulLineCount;
    for(var i = 0; i < tRows; i++)
    {
        var tTmpRateOfOne = CoInsuranceParamGrid.getRowColData(i, 3);
        //tSumRate += (tTmpRateOfOne * 1);
        tSumRate = accAdd(tSumRate, tTmpRateOfOne);
    }
    
    if(tSumRate > 1)
    {
        alert("所有负担比例累加之和不能超过1。");
        return false;
    }
    return true;
}

function accAdd(arg1,arg2)
{
    var r1,r2,m;
    try{r1=arg1.toString().split(".")[1].length;}catch(e){r1=0;}
    try{r2=arg2.toString().split(".")[1].length;}catch(e){r2=0;}
    m=Math.pow(10,Math.max(r1,r2));
    return (arg1*m+arg2*m)/m;
} 


function BtnBack(){
	location.href="../baojiaapp/GrpPriceQueryInput.jsp";
	
}
