<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ManuUWAllInit.jsp
//程序功能：个人人工核保  在线报价
//创建日期：2005-01-29 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	
	String strOperator = globalInput.Operator;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {            
  	if(operFlag == "1")
  	{
  		fm.ApplyNo.value = "5";    
  		fm.ApplyType.value = "1";  				//个单申请
  	}
  	if(operFlag == "2")
  	{
  		fm.ApplyNo.value = "2";  
  		fm.ApplyType.value = "2";    		  //团单申请
  	}  	      
  	if(operFlag == "3")
  	{
  		fm.ApplyNo.value = "2";  
  		fm.ApplyType.value = "3";    			//询价申请
  	}            
  }
  catch(ex)
  {
    alert("在UWInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
  	
    initInpBox();
        fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
    if(operFlag == "1")
    {
    	initPolGrid();   
    }
    if(operFlag == "2"||operFlag == "3")
    {
    	initGrpPolGrid();
    	initGrpRePolGrid();
    	initGrpHistoryPolGrid();  
    }
    easyQueryClick();
  }
  catch(re)
  {
    alert("在UWInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

// 保单信息列表的初始化
function initGrpPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="投保单位";         		//列名
      iArray[2][1]="300px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="生效日期";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保费";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="来源";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="管理机构";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
       
                  
      iArray[7]=new Array();
      iArray[7][0]="团体合同单号";         		//列名
      iArray[7][1]="160px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="申请日期";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="工作流任务号";         		//列名
      iArray[9][1]="100px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[10]=new Array();
      iArray[10][0]="工作流活动Id";         		//列名
      iArray[10][1]="100px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="工作流子任务号";         		//列名
      iArray[11][1]="100px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许
            
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);     
      
      PolGrid. selBoxEventFuncName = "easyQueryAddClick";
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
function initGrpRePolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="投保单位";         		//列名
      iArray[2][1]="300px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="生效时间";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保费";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
                                                                                            
      iArray[5]=new Array();                                                               
      iArray[5][0]="状态";         		//列名                                              
      iArray[5][1]="80px";            		//列宽                                          
      iArray[5][2]=100;            			//列最大值                                        
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许             
                                                                                            
      iArray[6]=new Array();                                                               
      iArray[6][0]="管理机构";         		//列名                                          
      iArray[6][1]="100px";            		//列宽                                          
      iArray[6][2]=100;            			//列最大值                                        
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许             
                                                                                            
                                                                                            
      iArray[7]=new Array();                                                               
      iArray[7][0]="团体合同单号";         		//列名                                      
      iArray[7][1]="160px";            		//列宽                                          
      iArray[7][2]=100;            			//列最大值                                        
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许             
                                                                                            
      iArray[8]=new Array();                                                                  
      iArray[8][0]="工作流任务号";         		//列名                                          
      iArray[8][1]="100px";            		//列宽                                              
      iArray[8][2]=100;            			//列最大值                                            
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许                 
                                                                                              
                                                                                              
      iArray[9]=new Array();                                                                 
      iArray[9][0]="工作流活动Id";         		//列名                                        
      iArray[9][1]="100px";            		//列宽                                            
      iArray[9][2]=100;            			//列最大值                                          
      iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许               
                                                                                              
      iArray[10]=new Array();                                                                 
      iArray[10][0]="工作流子任务号";         		//列名                                      
      iArray[10][1]="100px";            		//列宽                                            
      iArray[10][2]=100;            			//列最大值                                          
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许               
      
      RePolGrid = new MulLineEnter( "fm" , "RePolGrid" ); 
      //这些属性必须在loadMulLine前
      RePolGrid.mulLineCount = 0;   
      RePolGrid.displayTitle = 1;
      RePolGrid.locked = 1;
      RePolGrid.canSel = 1;
      RePolGrid.hiddenPlus = 1;
      RePolGrid.hiddenSubtraction = 1;
      RePolGrid.loadMulLine(iArray);     
      
      RePolGrid. selBoxEventFuncName = "easyQueryAddClick1";
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initGrpHistoryPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="投保单位";         		//列名
      iArray[2][1]="300px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="生效时间";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保费";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
                                                                                            
      iArray[5]=new Array();                                                               
      iArray[5][0]="状态";         		//列名                                              
      iArray[5][1]="80px";            		//列宽                                          
      iArray[5][2]=100;            			//列最大值                                        
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许             
                                                                                            
      iArray[6]=new Array();                                                               
      iArray[6][0]="管理机构";         		//列名                                          
      iArray[6][1]="100px";            		//列宽                                          
      iArray[6][2]=100;            			//列最大值                                        
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许             
                                                                                            
                                                                                            
      iArray[7]=new Array();                                                               
      iArray[7][0]="团体合同单号";         		//列名                                      
      iArray[7][1]="160px";            		//列宽                                          
      iArray[7][2]=100;            			//列最大值                                        
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许             
                                                                                                        
      
      HistoryPolGrid = new MulLineEnter( "fm" , "HistoryPolGrid" ); 
      //这些属性必须在loadMulLine前
      HistoryPolGrid.mulLineCount = 0;   
      HistoryPolGrid.displayTitle = 1;
      HistoryPolGrid.locked = 1;
      HistoryPolGrid.canSel = 1;
      HistoryPolGrid.hiddenPlus = 1;
      HistoryPolGrid.hiddenSubtraction = 1;
      HistoryPolGrid.loadMulLine(iArray);     
      
      HistoryPolGrid. selBoxEventFuncName = "easyQueryHistory";
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>