<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = "";
	try
	{
		tContNo = request.getParameter( "ContNo" );
		
		//默认情况下为团体投保单
		if( tContNo == null || tContNo.equals( "" ))
			tContNo = "";
	}
	
	catch( Exception e1 )
	{
		tContNo = "";
			System.out.println("---contno:"+tContNo);

	}
	System.out.println("---contno:"+tContNo);
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var contNo = "<%=tContNo%>";  //个人单的查询条件.
		var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <script src="../common/Calendar/Calendar.js"></script>
  <SCRIPT src="BJScan.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BJScaninit.jsp"%>
  <title>报价查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <!--<TD  class= title>
            团体保单号码
          </TD>
          <TD  class= input>-->
            <Input class= common name=GrpProposalNo type=hidden>
          <TD  class= title>
            报价单号码
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>
          <TD  class= title>
            销售渠道
          </TD>
          <TD  class= input>
          	<Input class=codeNo name=SaleChnl verify="销售渠道|code:SaleChnl" ondblclick="return showCodeList('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKey('SaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName readonly=true >
            
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          	<Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
            
          </TD>
        </TR>
         <TR  class= common>
          <TD  class= title>
            业务员代码
          </TD>
          <TD  class= input>
          	<Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
            
          </TD>
          <TD  class= title style="display:'none'">
            业务员组别
          </TD>
          <TD  class= input style="display:'none'">
          	<Input class="codeNo" name=AgentGroup  ondblclick="return showCodeList('agentgroup2',[this,AgentGroupName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('agentgroup2',[this,AgentGroupName],[0,1]);"><input class=codename name=AgentGroupName readonly=true >
            
          </TD>  
          <td class="title8">投保单位名称</td>
            <td class="input8">
                <input class="common" name="GrpName" />
            </td>
          <TD  class= title>
            报价单状态
          </TD>
          <TD  class= input>
            <Input class=codeno name=bjdzt CodeData="0|^1|录入^2|核保^3|报价完成" ondblclick="return showCodeListEx('bjdzt',[this,bjdztName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('bjdzt',[this,bjdztName],[0,1],null,null,null,1);"><input class=codename name=bjdztName readonly=true >
          </TD>
        </TR>
        <tr class="common">
<!--             <td class="title8">投保单位名称</td> -->
<!--             <td class="input8"> -->
<!--                 <input class="common" name="GrpName" /> -->
<!--             </td> -->
            <td class="title8">报价起始日期</td>
            <td class="input8">
                <input class="coolDatePicker" dateFormat="short" name="StartPolApplyDate" verify="投保起始日期|date" />
            </td>
            <td class="title8">报价终止日期</td>
            <td class="input8">
                <input class="coolDatePicker" dateFormat="short" name="EndPolApplyDate" verify="投保终止日期|date" />
            </td>
        </tr>
    </table>
    
    <br />
    
    <input value="查  询" class="cssbutton" type="button" onclick="easyQueryClick();" />
    <input value="下  载" class="cssButton" type="button" onclick="downloadQryResults();" />
        
    <br />
    <br />
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" Class=cssbutton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" Class=cssbutton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" Class=cssbutton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" Class=cssbutton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<P>
  	<INPUT VALUE="报价单明细" Class=cssbutton TYPE=button onclick="returnParent();"> 
            	</P>
  	</div>
  	  	<Div  id= "divLCPol2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanPolStatuGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>		
  	</div>
    
    <input name="querySql" type="hidden" />
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
