<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ManuUWAllChk.jsp
//程序功能：申请核保
//创建日期：2005-01-19 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  if(tG == null) 
  {
		System.out.println("session has expired");
		return;
  }
     
 	// 投保单列表
  String tApplyNo = request.getParameter("ApplyNo");
  System.out.println("tApplyNo="+request.getParameter("ApplyNo"));
  
  String tApplyType = request.getParameter("ApplyType");
  System.out.println("tApplyType="+request.getParameter("ApplyType"));

  String tContSaleChnlType = request.getParameter("ContSaleChnlType");
  System.out.println("tContSaleChnlType+" + tContSaleChnlType);

  TransferData mTransferData = new TransferData();
  
  mTransferData.setNameAndValue("ApplyNo",tApplyNo);  
  mTransferData.setNameAndValue("ApplyType",tApplyType);	
  mTransferData.setNameAndValue("ContSaleChnlType",tContSaleChnlType);

  
try{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( mTransferData );
		tVData.add( tG );
		
		// 数据传输
		UWApplyUI tUWApplyUI = new UWApplyUI();
		if (tUWApplyUI.submitData(tVData,"") == false)
		{
			int n = tUWApplyUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			{
			  System.out.println("Error: "+tUWApplyUI.mErrors.getError(i).errorMessage);
			  Content = " 申请核保失败，原因是: " + tUWApplyUI.mErrors.getError(0).errorMessage;
			}
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tUWApplyUI.mErrors;
		    System.out.println("tError.getErrorCount:"+tError.getErrorCount());
		    if (!tError.needDealError())
		    {                          
		    	Content = " 申请核保成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 申请核保失败，原因是:";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() + tError.getError(i).errorMessage.trim();
			      }
					}
		    	FlagStr = "Fail";
		    }
		}
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
  
%>                      
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
