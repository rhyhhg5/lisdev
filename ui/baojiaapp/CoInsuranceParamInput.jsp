<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp" %>
    
<%
//程序名称：HealthArchiveInput.jsp
//程序功能：F1报表生成
//创建日期：2005-07-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="CoInsuranceParamInput.js"></script>
    
    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">
    
    <%@include file="CoInsuranceParamInit.jsp"%>
</head>

<script>
	var mGrpContNo = "<%=request.getParameter("GrpContNo")%>";
	var LoadFlag = "<%=request.getParameter("LoadFlag")%>";
</script>

<body onload="initForm();initElementtype();">    
<form action="./TbGrpContractorSave.jsp" method=post name=fm target="fraSubmit">
    <div id="divGrpInfo">
        <table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">团体信息</td>
            </tr>
        </table>
            
        <table class="common">
            <tr>
                <td class="title8">投保单印刷号</td>
                <td class="input8">
                    <input class="readonly" name="PrtNo" readonly="readonly" />
                </td>
                <td class="title8">&nbsp;</td>
                <td class="input8">
                    <input class="readonly" type="hidden" name="GrpContNo" readonly="readonly" />
                </td>
                <td class="title8">&nbsp;</td>
                <td class="input8">
                    <input class="readonly" type="hidden" name="ProposalGrpContNo" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td class="title8">投保单位客户号</td>
                <td class="input8">
                    <input class="readonly" name="GrpAppntNo" readonly="readonly" />
                </td>
                <td class="title8">投保单位名称</td>
                <td class="input8">
                    <input class="readonly" name="GrpAppntName" readonly="readonly" />
                </td>
                <td class="title8">&nbsp;</td>
                <td class="input8">&nbsp;</td>
            </tr>
            <tr style="display:'none';">
                <td class="title8">&nbsp;</td>
                <td class="input8">
                    <input class="readonly" name="ManageCom" readonly="readonly" />
                </td>
                <td class="title8">&nbsp;</td>
                <td class="input8">&nbsp;</td>
                <td class="title8">&nbsp;</td>
                <td class="input8">&nbsp;</td>
            </tr>
        </table>
    </div>
    
    <br />
    
    <div id="DivLCContButton" style="display:''">
        <table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">共保信息要素<font color="red">（共保负担比例之和不能大于1）</font></td>
            </tr>
        </table>
    </div>
    
    <div id="divCoInsuranceParam" style="display: ''">
        <table class="common">
            <tr class="common">
                <td>
                    <span id="spanCoInsuranceParamGrid"></span> 
                </td>
            </tr>
        </table>
    </div>
    <div id="ButtonDiv" style="display: ''">
        <table class="common">
            <tr class="common">
                <td class="common">
                <input value="保  存" class="cssButton" type="button" id="btnSaveCIInfo" name="btnSaveCIInfo" onclick="saveCIInfo();" />
                <input value="删  除" class="cssButton" type="button" id="btnDelCIInfo" name="btnDelCIInfo" onclick="delCIInfo();" />
                <input value="返  回" class="cssButton" type="button" id="btnBack" name="btnBack" onclick="BtnBack()" />
                </td>
            </tr>
        </table>
    </div>
        
	<input type="hidden" name="fmtransact" />

</form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
