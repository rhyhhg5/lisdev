<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="AuditTrajectInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AuditTrajectInputInit.jsp"%>
  <title>核保轨迹查询</title>
</head>
<body  onload="initForm();initElementtype();">
<form  action="" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入询价单号：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            询价单号
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo elementtype=nacessary MAXLENGTH="11" verify="询价单号|notnull&len=11">
          </TD>
         <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          	<Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true > 
            
          </TD>
          <TD  class= title>
            核保日期
          </TD>
          <TD  class= input>
            <Input class=coolDatePicker  dateFormat="short" name=UWDate verify="核保日期|date" >
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrp1);">
    		</td>
    		<td class= titleImg>
    			 核保轨迹
    		</td>
    	</tr>
    	<tr>
  
          <INPUT  type= "hidden" class= Common name= MissionID  value="">
          <INPUT  type= "hidden" class= Common name= SubMissionID  value="">
    	</tr>
    </table>
             <div id="divUWInfo" >
	            <table class=common align=center>
	            <tr class=common>
	            <td text-align: left colSpan=1>
	              <span id="spanUWInfoGrid">  </span>
	             </td>
	            </tr>
	            </table>
	          
      <INPUT VALUE="首  页"
        class =  cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
<%--   	        <input class= common type=hidden name=tFlag value="<%=tFlag%>"> --%>
            <Input class= common type=hidden name=Operator >  	      
  </form>
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>