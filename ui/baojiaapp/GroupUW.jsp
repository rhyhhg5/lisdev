<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
  //程序名称：GroupUW.jsp
  //程序功能：团体保单人工核保
  //创建日期：2002-06-19 11:10:36
  //创建人  ：WHN
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
  String tContNo = "";
  String tPrtNo = "";
  String tGrpContNo = "";
  String tMissionID = "";
  String tSubMissionID = "";
  String tLoadFlag="";
  String tResource="";
  tPrtNo = request.getParameter("PrtNo");
  tGrpContNo = request.getParameter("GrpContNo");
  tMissionID = request.getParameter("MissionID");
  tSubMissionID = request.getParameter("SubMissionID");
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput) session.getValue("GI");
  tLoadFlag = request.getParameter("LoadFlag");
  tResource =request.getParameter("Resource");
  System.out.println(tGrpContNo + "asdf" + tPrtNo);
%>
<script>
	var PrtNo = "<%=tPrtNo%>";
	var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
	var MissionID = "<%=tMissionID%>";
	var SubMissionID = "<%=tSubMissionID%>";	
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var LoadFlag= "<%=tLoadFlag%>";
	var Resource= "<%=tResource%>";
	//alert(LoadFlag);
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="GroupUW.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="GroupUWInit.jsp"%>
</head>
<body onload="initForm();">
<form action="./GroupUWCho.jsp" method=post name=fmQuery target="fraSubmit">
<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGrpCont);">
    </td>
    <td class=titleImg>报价单信息    </td>
  </tr>
</table>
<Div id="divGrpCont" style="display: ''">
  <table class=common>
    <TR class=common>
      <!--  团体投保单号码-->
      <Input class="readonly" readonly name=GrpContNo type="hidden">
      <TD class=title8>报价单号      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=PrtNo>
      </TD>
      <TD class=title8>管理机构      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=ManageCom_ch>
        <input type=hidden name=ManageCom>
      </TD>
      <TD class=title8>业务类型      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name="BusType_ch" value="直销">
        
      </TD>
      
    </TR>
    <TR class=common8>
          <TD class=title8>销售渠道      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=SaleChnl_ch>
        <input type=hidden name=SaleChnl>
      </TD>
      <TD class=title8>中介机构      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=AgentCom_ch>
        <input type=hidden name=AgentCom>
      </TD>
      <TD class=title8>中介代理公司名称      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name="GrpName">
        
      </TD>
      
      
    </TR>
    <TR class=common>
    <TD class=title8>业务员代码      </TD>
      <TD class=input8>
        <input class="readonly" readonly name=AgentCode>
      </TD>
    <TD class=title8>业务员姓名      </TD>
      <TD class=input8>
        <Input class="readonly" readonly NAME=Name>
        
      </TD>
      <TD class=title8>业务员组别      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name=AgentGroup>
      
      </TD>
     
    </tr>
    <tr class=common>
    	 <TD class=title8>申请日期</TD>
      <TD class=input8>
        <Input class="readonly" readonly name=PolApplyDate>
      </TD>
      <TD class=title8>大项目      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name="dxm" value="否">
      </TD>
      <TD class=title8></TD><TD class=title8></TD>
    </tr>
    <tr class=common>
      <TD class=title8>特别约定</TD>
      <TD class=input8 COLSPAN="6">
        <Input class="readonly4" readonly name=Remark>
      </TD>
    </TR>
  </table>
</Div>
  <!-- 团体查询条件 -->
  <!--
    TR  class= common>
    <TD  class= titles>
    团体投保单号码
    </TD>
    <TD  class= input>
    <Input class= common name=GrpProposalNo >
    <Input type= "hidden" class= common name=GrpMainProposalNo >
    <INPUT type= "hidden" name= "Operator" value= "">
    </TD>
    </TR
  -->
  <!-- 查询未过团体保单（列表） -->
<!-- <table> -->
<!--   <tr> -->
<!--     <td class=common> -->
<!--       <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);"> -->
<!--     </td> -->
<!--     <td class=titleImg>业务信息    </td> -->
<!--   </tr> -->
<!-- </table> -->
<!-- <Div id="divywxx" style="display: ''"> -->
<!--   <table class=common> -->
<!--     <TR class=common> -->
<!--       <TD class=title8>销售类型      </TD> -->
<!--       <TD class=input8> -->
<!--         <Input class="readonly" readonly name="xslx" value="直销"> -->
<!--       </TD> -->
<!--       <TD class=title8>大项目      </TD> -->
<!--       <TD class=input8> -->
<!--         <Input class="readonly" readonly name="dxm" value="否"> -->
<!--       </TD> -->
<!--       <TD class=title8></TD> -->
<!--       <TD class=input8> -->
<!--         <Input class="readonly" readonly name=""> -->
<!--       </TD> -->
<!--     </TR> -->
<!--     <TR class=common> -->
<!--       <TD class=title8>销售渠道      </TD> -->
<!--       <TD class=input8> -->
<!--         <Input class="readonly" readonly name="xsqd" value="团险直销"> -->
<!--       </TD> -->
<!--       <TD class=title8>业务员姓名      </TD> -->
<!--       <TD class=input8> -->
<!--         <Input class="readonly" readonly name="xsqd" value="李英文"> -->
<!--       </TD> -->
<!--       <TD class=title8>中介代理公司名称      </TD> -->
<!--       <TD class=input8> -->
<!--         <Input class="readonly" readonly name="zjdlgs"> -->
<!--       </TD> -->
<!--     </TR> -->
<!--     <TR> -->
<!-- 		<TD class=title>上报原因      </TD> -->
<!-- 		<TD class=input colspan="5"> -->
<!-- 			<input type="checkbox" name="MarketType" value="1" checked="checked"/><span>保费规模超授权额度</span><br> -->
<!-- 			<input type="checkbox" name="MarketType" value="2"/><span>单个被保险人保险金额超授权额度</span><br> -->
<!-- 			<input type="checkbox" name="MarketType" value="3"/><span>新增保险责任须总公司定价支持</span><br> -->
<!-- 			<input type="checkbox" name="MarketType" value="4"/><span>扩大的保险责任属于公司现有已报备或报批的条款覆盖的责任，需要总公司定价支持</span><br> -->
<!-- 			<input type="checkbox" name="MarketType" value="5"/><span>无名单业务</span><br> -->
<!-- 			<input type="checkbox" name="MarketType" value="6"/><span>开业3个月以内的分公司核保件</span><br> -->
<!-- 			<input type="checkbox" name="MarketType" value="7" checked="checked"/><span>其他 &nbsp&nbsp<input name="Detail101" class = "common66" style="width:400px;" value="上报原因上报原因"></span> -->
<!-- 			<br> -->
<!-- 		</TD> -->
<!-- 	</TR> -->
<!--   </table> -->
<!-- </Div> -->
      
<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);">
    </td>
    <td class=titleImg>投保单位信息    </td>
  </tr>
</table>
<Div id="divtbxx" style="display: ''">
  <table class=common>
    <TR class=common>
      	<TD class=title8>投保单位全称</TD>
    	<TD class=input8 >
    	 <Input class="readonly" readonly name=GrpName1>
       
      	</TD>
      	<TD class=title8></TD>
    	<TD class=input8></TD>
    	<TD class=title8></TD>
    	<TD class=input8></TD>
    </TR>
    <TR class=common>
<!--     	<TD class=title8></TD> -->
      	<TD class=title8>主要职业</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="BusinessType" value="">
      	</TD>
      	<TD class=title8>行业性质</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="GrpNature" value="">
      	</TD>
      	<TD class=title8></TD>
    	<TD class=input8></TD>
    </TR>
    <TR class=common>
<!--     	<TD class=title8></TD> -->
      	<TD class=title8>单位总人数</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="Peoples">
      	</TD>
      	<TD class=title8>投保人数</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="Peoples3" value="10">
      	</TD>
      	<TD class=title8>连带被保险人数</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="RelaPeoples" value="0">
    	</TD>
    </TR>
    </table>
    <table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);">
    </td>
    <td class=titleImg>被保险人信息    </td>
  </tr>
</table>
	<table class=common>
    <TR class=common>
      	<TD class=title8>平均年龄</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="AvgAge">
      	</TD>
      	<TD class=title8>最大年龄</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="MaxAge" >
    	</TD>
    	<TD class=title8>最小年龄</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="MinAge" >
    	</TD>
    </TR>
    <TR class=common>
<!--     	<TD class=title8></TD> -->
      	<TD class=title8>职业类别</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="BusinessType1">
      	</TD>
      	<TD class=title8>男女比例</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="Fproportion" >
      	</TD>
      	<TD class=title8>退休人员占比</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="Retirement">
    	</TD>
    </TR>
  </table>
</Div>
    
<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);">
    </td>
    <td class=titleImg>保险方案信息    </td>
  </tr>
</table>
<Div id="divbxfa" style="display: ''">
  <table class=common>
  	<TR class=common >
  		<TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD>
  	</TR>
    <TR class=common>
      <TD class=title8>保险责任生效日      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name="sxr" value="">
      </TD>
      <TD class=title8>保险责任终止日      </TD>
      <TD class=input8>
        <Input class="readonly" readonly name="exr" value="">
      </TD>
<!--       <TD class=title8>保障计划区分</TD> -->
<!--       <TD class=input8 colspan="3"> -->
<!--       	<input type="checkbox" name="MarketType" value="1"/><span>统一</span> -->
<!--       	<input type="checkbox" name="MarketType" value="2"/><span>职务</span> -->
<!--       	<input type="checkbox" name="MarketType" value="3"/><span>薪酬</span> -->
<!--       	<input type="checkbox" name="MarketType" value="4"/><span>工龄</span><br> -->
<!--       	<input type="checkbox" name="MarketType" value="5" /><span>其他<input name="Detail101" class = "common66" style="width:80px;" value=""></span> -->
<!--       </TD> -->
    </TR>
  </table>
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanGrpAppntGrid">  </span>
      </td>
    </tr>
  </table>
<!--   <table class=common> -->
<!--   	<TR class=common > -->
<!--   		<TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD> -->
<!--   	</TR> -->
<!--     <TR class=common> -->
<!--     	<TD class=title8 rowspan="2">费率折扣</TD> -->
<!--     	<TD class=input8 colspan="7"> -->
<!--     		<input type="checkbox" name="fvzk" /><span>根据标准费率计算的应收保费为<input name="Detail101" class = "common66" style="width:130px;"></span> -->
<!--     		<input type="checkbox" name="fvzk" /><span>折扣率（实际保费/标费）<input name="Detail101" class = "common66" style="width:130px;"></span> -->
<!--       	</TD> -->
<!--     </TR> -->
<!--     <TR class=common> -->
<!--       	<TD class=input8 colspan="7"> -->
<!--     		<input type="checkbox" name="fvzk2" /><span>根据报价工具计算的应收保费为<input name="Detail101" class = "common66" style="width:130px;"></span> -->
<!--     		<input type="checkbox" name="fvzk2" /><span>折扣率（实际保费/报价工具生成保费）<input name="Detail101" class = "common66" style="width:130px;"></span> -->
<!--       	</TD> -->
<!--     </TR> -->
    
<!--     <TR class=common> -->
<!--     	<TD class=title8 rowspan="7">非标准业务管理</TD> -->
<!--       	<td class=title8 >1.是否为非标准业务：</td> -->
<!--       	<td class=title8 colspan="6"> -->
<!--       		<input type="checkbox" name="Detail207" class="box" onclick="InputDetailToMuline207();">&nbsp&nbsp 是 <input class="common7"> -->
<!--       		<input type="checkbox" name="Detail207" class="box">&nbsp&nbsp 否  -->
<!--       	</td> -->
<!--     </TR> -->
<!--     <TR class=common> -->
<!--     	<td class=title8 rowspan="6">2.非标准业务分类：&nbsp&nbsp&nbsp&nbsp -->
<!--       	</td> -->
<!--     </TR> -->
<!--     <TR class=common> -->
<!--       	<TD class=input8> -->
<!--       		<input type="checkbox" name="bzyw" /><span>新增保险责任</span> -->
<!--       	</TD> -->
<!--     	<TD class=input8 colspan="5"> -->
<!--     		新增责任说明：<input name="Detail101" class = "common66" style="width:200px;"> -->
<!--       	</TD> -->
<!--     </TR> -->
<!--     	<TD class=input8> -->
<!--       		<input type="checkbox" name="bzyw" /><span>变更保险责任</span> -->
<!--       	</TD> -->
<!--     	<TD class=input8 colspan="5"> -->
<!--     		变更责任代码及变更说明：<input name="Detail101" class = "common66" style="width:200px;"> -->
<!--       	</TD> -->
<!--     <TR> -->
<!--     	<TD class=input8> -->
<!--       		<input type="checkbox" name="bzyw" /><span>变更除外责任</span> -->
<!--       	</TD> -->
<!--     	<TD class=input8 colspan="5"> -->
<!--     		变更说明：<input name="Detail101" class = "common66" style="width:200px;"> -->
<!--       	</TD> -->
<!--     </TR> -->
<!--     	<TD class=input8> -->
<!--       		<input type="checkbox" name="bzyw" /><span>变更关键参数</span> -->
<!--       	</TD> -->
<!--     	<TD class=input8 colspan="5"> -->
<!--     		<input type="checkbox" name="MarketType" value="1"/><span>免赔额、</span> -->
<!--       		<input type="checkbox" name="MarketType" value="2"/><span>给付比例、</span> -->
<!--       		<input type="checkbox" name="MarketType" value="3"/><span>费用限额、</span> -->
<!--       		<input type="checkbox" name="MarketType" value="4"/><span>保险金额、</span> -->
<!--       		<input type="checkbox" name="MarketType" value="4"/><span>被保险人范围、</span> -->
<!--       		<input type="checkbox" name="MarketType" value="4"/><span>等待期、</span> -->
<!--       		<input type="checkbox" name="MarketType" value="4"/><span>免责期、</span> -->
<!--       		<input type="checkbox" name="MarketType" value="5" /><span>其他 <input name="Detail101" class = "common66" style="width:100px;" value=""></span> -->
<!--       	</TD> -->
<!--     <TR> -->
<!--     	<TD class=input8> -->
<!--       		<input type="checkbox" name="bzyw" /><span>非实质性变更</span> -->
<!--       	</TD> -->
<!--     	<TD class=input8 colspan="5"> -->
<!--     		变更说明：<input name="Detail101" class = "common66" style="width:200px;"> -->
<!--       	</TD> -->
<!--     </TR> -->
<!--     <TR> -->
<!--     </TR> -->
<!--   </table> -->
</Div>

<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);">
    </td>
    <td class=titleImg>其他相关信息    </td>
  </tr>
</table>
<Div id="divtbxx" style="display: ''">
  <table class=common>
    <TR class=common>
    	<TD class=title8>预计保费收入（万元）      </TD>
    	<TD class=input8 colspan="2">
        	<Input class="readonly" readonly name="Prem" value="">
      	</TD>
      	<TD class=title8>中介手续费率 %      </TD>
    	<TD class=input8 colspan="2">
        	<Input class="readonly" readonly name="IntermediaryFeerate" value="">
      	</TD>
    </TR>
    <TR class=common>
      	<TD class=title8>销售人员提奖 %</TD>
    	<TD class=input8 colspan="2">
    		<Input class="readonly" readonly name="xsrytj" value="">
      	</TD>
      	<TD class=title8>业务监管费用及保险保障基金  %</TD>
    	<TD class=input8 colspan="2">
    		<Input class="readonly" readonly name="ywjgfy" value="">
      	</TD>
    </TR>
    <TR class=common>
      	<TD class=title8>预估项目费用分摊比率%</TD>
    	<TD class=input8 >
    		<Input class="readonly" readonly name="ygxmfy" value="">
      	</TD>
      	<TD class=title8>预估赔付率%</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="ygpfl" value="">
      	</TD>
      	<TD class=title8>预估综合成本率%</TD>
    	<TD class=input8>
    		<Input class="readonly" readonly name="ygzhcbl" value="">
    	</TD>
    </TR>
    <TR class=common>
      	<TD class=title8>是否为统括保单</TD>
    	<TD class=input8 colspan="2">
    		<Input class="readonly" readonly name="tkbd" value="否">
      	</TD>
      	<TD class=title8>是否需签署协议</TD>
    	<TD class=input8 colspan="2">
    		<Input class="readonly" readonly name="qsxy" value="否">
      	</TD>
    </TR>
    <TR class=common>
      	<TD class=title8>是否已参加基本医疗保险</TD>
    	<TD class=input8 colspan="2">
    		<Input class="readonly" readonly name="ylbx" value="是">
      	</TD>
      	<TD class=title8>是否已参加大额医疗保险</TD>
    	<TD class=input8 colspan="2">
    		<Input class="readonly" readonly name="deylbx" value="是">
    	</TD>
    </TR>
    <TR class=common>
      	<TD class=title8>同业竞争情况</TD>
    	<TD class=input8 colspan="5">
    		<Input class="readonly" readonly name="tyjz" value="">
      	</TD>
      	
    </TR>
  </table>
</Div>
    
<!-- <table> -->
<!--   <tr> -->
<!--     <td class=common> -->
<!--       <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);"> -->
<!--     </td> -->
<!--     <td class=titleImg>团体投保资料    </td> -->
<!--   </tr> -->
<!-- </table> -->
<!-- <Div id="divLCPol1" style="display: ''"> -->
<!--   <table class=common> -->
<!--     <tr class=common> -->
<!--       <td text-align: left colSpan=1> -->
<!--         <span id="spanGrpAppntGrid">  </span> -->
<!--       </td> -->
<!--     </tr> -->
<!--   </table> -->
<!-- </div> -->

<!-- <table> -->
<!--   <tr> -->
<!--     <td class=common> -->
<!--       <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol2);"> -->
<!--     </td> -->
<!--     <td class=titleImg>报价单查询结果    </td> -->
<!--   </tr> -->
<!-- </table> -->
<!-- <Div id="divLCPol2" style="display: ''"> -->
<!--   <table class=common> -->
<!--     <tr class=common> -->
<!--       <td text-align: left colSpan=1> -->
<!--         <span id="spanGrpGrid">  </span> -->
<!--       </td> -->
<!--     </tr> -->
<!--   </table> -->
<!-- </div> -->   
<!--     <table> -->
<!--     <tr> -->
<!--     <td class=common> -->
<!--     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPolFee);"> -->
<!--     </td> -->
<!--     <td class= titleImg> -->
<!--     险种保费信息 -->
<!--     </td> -->
<!--     </tr> -->
<!--     </table -->
<!--   --> 
<!-- <Div id="divLCPolFee" style="display: 'none'"> -->
<!--   <table class=common> -->
<!--     <tr class=common> -->
<!--       <td text-align: left colSpan=1> -->
<!--         <span id="spanGrpPolFeeGrid">  </span> -->
<!--       </td> -->
<!--     </tr> -->
<!--   </table> -->
<!-- </div> -->
<br>
<hr></hr>
<!-- <INPUT VALUE="团体扫描件查询" Class="cssButton" TYPE=button onclick="ScanQuery2();"> -->
<!-- <INPUT VALUE="团体自动核保信息" Class="cssButton" TYPE=button onclick="showGNewUWSub();"> -->
<INPUT VALUE="报价单保单明细" Class="cssButton" TYPE=button onclick="showGrpCont();">
 <INPUT VALUE="附件信息" Class="cssButton" TYPE=button onclick="Enclosure();">
 
  <!--
    <INPUT VALUE = "团体既往询价信息" Class="cssButton" TYPE=button onclick= "showAskApp();">
    <INPUT VALUE = "团体既往保障信息" Class="cssButton" TYPE=button onclick= "showHistoryCont();">
  -->
<!-- <INPUT VALUE="团体既往信息" Class="cssButton" TYPE=button onclick="showHistoryContI();"> -->
<%-- <INPUT VALUE="个人自动核保信息" Class="cssButton" TYPE=button onclick="GrpContQuery('<%=tGrpContNo%>');" id="PerAutoUWInfo" name = "PerAutoUWInfo"> --%>
  <!--INPUT VALUE="团体保单承保特约录入" Class=Common TYPE=button onclick="showGSpec();"-->
  <!--INPUT VALUE = "团体保单问题件查询" Class="cssButton" TYPE=button onclick="GrpQuestQuery();"-->
<!-- <INPUT VALUE="团体保单问题件处理" Class="cssButton" TYPE=button onclick="GrpQuestInput();"> -->
<!-- <span  id= "GrpQuest13" style= "display: ''"> -->
<!-- <INPUT VALUE="团单契调信息录入" Class="cssButton" TYPE=button onclick="showRReport();"> -->
<!-- <INPUT VALUE="记事本" class="cssButton" TYPE=button onclick="showNotePade();"> -->
<!-- <INPUT VALUE="发送再保审核" class="cssButton" TYPE=button onclick="ReInsure();" id="SendReUW" name = "SendReUW"> -->
<!-- <input value="查看约定缴费计划" class="cssButton" type="button" onclick="showPayPlanInfo();"> -->
<!-- <input value="查看社保项目要素信息" class="cssButton" type="button" onclick="initBalance();"> -->
<!-- <input value="查看汇交件投保人信息" class="cssButton" type="button" onclick="HJAppnt();"> -->
<!-- </span> -->
  <!--INPUT VALUE = "记事本" Class="cssButton" TYPE=button onclick="showNotePad();"-->
<!-- <hr></hr> -->
<!-- <span  id= "GrpQuest11" style= "display: ''"> -->
<!-- <INPUT VALUE="发团体体检通知书" Class="cssButton" TYPE=button onclick="checkBody();" id="SendCheckBody" name = "SendCheckBody"> -->
<!-- <INPUT VALUE="发团单契调信息通知书" Class="cssButton" TYPE=button onclick="SendRReport();"> -->
<!-- <INPUT VALUE="发团体核保调知书" Class="cssButton" TYPE=hidden onclick="SendNotice();"> -->
<!-- <INPUT VALUE="发团体问题件通知书" Class="cssButton" TYPE=hidden onclick="SendIssueNotice();"> -->
<!-- <INPUT VALUE="承保过程记录" Class="cssButton" TYPE=button onclick="showMarkPrice();"> -->

  <!--input value="发核保通知书" class=cssButton type=button onclick="SendNotice();"-->
<hr></hr>
<!-- </span> -->
<!-- <span  id= "GrpQuest111" style= "display: ''"> -->
<!-- <input value="承保计划变更" class=cssButton type=button onclick="showChangePlan();" id="ChangePlan" name = "ChangePlan"> -->
<!-- <input value="承保计划变更结论录入" class=cssButton type=button onclick="showChangeResultView();"> -->
<!-- <input value="中介手续费调整" class=cssButton type=button onclick="showAgentDiv();"> -->
<!-- </span> -->
<!--input value="费用率调整" class=cssButton type=button onclick="showFeeRate();"-->
<!-- <div id="divFeeRate" style="display: ''"> -->
<!-- 	  <table class=common align=center> -->
<!-- 	    <table class=common> -->
<!-- 	    <tr class=common> -->
<!-- 	      <td text-align: left colSpan=1> -->
<!-- 	        <span id="spanFeeRateGrid">  </span> -->
<!-- 	      </td> -->
<!-- 	    </tr> -->
<!-- 	  </table> -->
<!--     <td class=common> -->
<!--       <INPUT VALUE="确  定" class=cssButton TYPE=button onclick="FeeRateSave();"> -->
<!--       <INPUT VALUE="取  消" class=cssButton TYPE=button onclick="showFeeRate();"> -->
<!--     </td> -->
<!--   </table> -->
<!-- </div> -->
<div id="divAgent" style="display: 'none'">
  <table class=common align=center>
    <TD class=title8>中介手续费率（该费率值不能大过1）    </TD>
    <TD class=input8>
      <Input class="common" name=AgentcyRate>
    </TD>
    <input class="common" type=hidden name=PrtNoInput>
    <td class=common>
      <INPUT VALUE="确  定" class=cssButton TYPE=button onclick="AgentcySave();">
      <!-- <INPUT VALUE="取  消" class=cssButton TYPE=button onclick="showAgentDiv();"> -->
    </td>
  </table>
</div>
  <!--INPUT VALUE="问题件录入" class=cssButton TYPE=button onclick="QuestInput();"-->
  <!--INPUT VALUE = "发送问题件" Class="cssButton" TYPE=button onclick="SendQuest();"-->
  <!--INPUT VALUE = "发送加费核保通知书" Class="cssButton" TYPE=button onclick="SendNotice();"-->
  <!-- 集体单核保结论 -->
  
<!-- <div id="divHisUWInfo" style="display: ''"> -->
<!-- 	  <table class=common align=center> -->
<!-- 	    <tr class=common> -->
<!-- 	      <td text-align: left colSpan=1> -->
<!-- 	        <span id="spanHisUWInfoGrid">  </span> -->
<!-- 	      </td> -->
<!-- 	    </tr> -->
<!-- 	  </table> -->
<!-- </div> -->
<table>
  <tr>
    <td class=common>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);">
    </td>
    <td class=titleImg>核保轨迹   </td>
  </tr>
</table>
<div id="divUWInfo" >
	            <table class=common align=center>
	            <tr class=common>
	            <td text-align: left colSpan=1>
	              <span id="spanUWInfoGrid">  </span>
	             </td>
	            </tr>
	            </table>
	          
<!--       <INPUT VALUE="首  页" -->
<!--         class =  cssButton TYPE=button onclick="getFirstPage();">  -->
<!--       <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="getPreviousPage();"> 					 -->
<!--       <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="getNextPage();">  -->
<!--       <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="getLastPage();"> 					 -->
  	</div>

<!--add by zhangxing-->  
<!-- <table> -->
<!--   <tr> -->
<!--     <td class=common> -->
<!--       <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);"> -->
<!--     </td> -->
<!--     <td class=titleImg>上报原因   </td> -->
<!--   </tr> -->
<!-- </table> -->
<!-- <div id="divUpSendReason" style="display: ''"> -->
<!-- 	  <table class=common align=center> -->
<!-- 	    <tr class=common> -->
<!-- 	      <td text-align: left colSpan=1> -->
<!-- 	        <span id="spanUpSendReasonGrid">  </span> -->
<!-- 	      </td> -->
<!-- 	    </tr> -->
<!-- 	  </table> -->
<!-- </div> -->


<table class=common border=0 width=100%>
  <tr>
    <td class=titleImg align=center>报价单核保结论：</td>
  </tr>
</table>
<table class=common border=0 width=100%>
  <TR class=common>
    <TD height="29" class=title>      报价单核保结论
      <Input class=codeno name=GUWState CodeData="0|^1|拒保^4|通融承保^9|正常承保^a|撤销投保单" ondblclick="showCodeListEx('cond',[this,GUWStateName],[0,1]);" onkeyup="showCodeListKeyEx('cond',[this,GUWStateName],[0,1]);"><input class=codename name=GUWStateName readonly=true>
      <!--input class="code" name=t ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);"-->
    </TD>
  </TR>
  <tr>  </tr>
  <TD class=title>报价单核保意见  </TD>
  <tr>  </tr>
  <TD class=input>
    <textarea name="GUWIdea" cols="100%" rows="5" witdh=100% class="common">    </textarea>
  </TD>
</table>
<div>
        <table>
            <tr>
                
                <td class= titleImg>方案补充:</td>
            </tr>
        </table>
        
        <table class="common">
        	<tr>
        		<td class=title>补充说明</td>
        	</tr>
            <tr>
               <td class=input>
						<textarea class=common name="discr" verify="补充说明|notnull" rows="5" cols="100"></textarea>
					</td>
            </tr>
        </table>
    </div>

<div id=divUWSave style="display:''">
  <INPUT VALUE="报价单整单确认" Class="cssButton" TYPE=button onclick="">
  <INPUT VALUE="上报上级" Class="cssButton" TYPE=button onclick="">
  <INPUT VALUE="回  退" Class="cssButton" TYPE=button onclick="">
  <INPUT VALUE="返  回" Class="cssButton" TYPE=button onclick="GoBack();">
</div>
<div id="divUWAgree" style="display: 'none'">
  <INPUT VALUE="同  意" class=cssButton TYPE=button onclick="gmanuchk(1);">
  <INPUT VALUE="不同意" class=cssButton TYPE=button onclick="gmanuchk(2);">
  <INPUT VALUE="上报上级" Class="cssButton" TYPE=button onclick="gmanuchk(3);">
  <INPUT VALUE="回  退" Class="cssButton" TYPE=button onclick="">
  <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="GoBack();">
</div>
<input type="hidden" name="WorkFlowFlag" value="">
<input type="hidden" name="MissionID" value="">
<input type="hidden" name="SubMissionID" value="">
<input type="hidden" name="PrtNoHide" value="">
<input type="hidden" name="GrpProposalContNo" value="">
<INPUT type="hidden" class=Common name="YesOrNo" value="">
<INPUT type="hidden" class=Common name="GrpSendUpFlag" value="">
<!-- </div> -->
<div id="divChangeResult" style="display: 'none'">
  <table class=common align=center>
    <TD height="24" class=title>承保计划变更结论录入:    </TD>
    <tr>    </tr>
    <TD class=input>
      <textarea name="ChangeIdea" cols="100%" rows="5" witdh=100% class="common">      </textarea>
    </TD>
  </table>
  
  <INPUT VALUE="确  定" class=cssButton TYPE=button onclick="showChangeResult();">
  <INPUT VALUE="取  消" class=cssButton TYPE=button onclick="HideChangeResult();">
</div>
<div id=hidden style="display : 'none'">
  <!--
    <TD  class= title8>
    投保单客户号码
    </TD>
    <TD  class= input8>
  -->
  <Input class="readonly" readonly name=AppntNo type="hidden">
</TD>  <TD class=title8>VIP标记  </TD>
  <TD class=input8>
    <Input class="readonly" readonly name=VIPValue>
    <INPUT type="hidden" name="Operator" value="">
  </TD>
  <TD class=title8>黑名单标记  </TD>
  <TD class=input8>
    <Input class="readonly" readonly name=BlacklistFlag>
  </TD>
</div>
<span id="LongSqlDiv"  style= "display: ''">
</span>
    <INPUT type=hidden name="LoadFlag" value="">
		<INPUT type=hidden name="Resource" value="">
</form>
  <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
<body name="LongSqlDiv">
</body>
</html>
