<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.ygz.*"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String tOperator = tG.Operator;
	String tComCode = tG.ComCode;
	
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	
	//得到前台页面的数据并进行封装
	String PolicyType = request.getParameter("PolicyType");
	String tChk[] = request.getParameterValues("InpContGridChk");
	String tPrtNos[] = request.getParameterValues("ContGrid2");
	String tPolicyNos[] = request.getParameterValues("ContGrid1");
	//LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
	Map tHashMap = new HashMap();
	try{
		for(int i=0; i<tChk.length; i++){
			if(tPrtNos[i] != null && "1".equals(tChk[i])){
				System.out.println("PtrNos[" + i + "]:" + tPrtNos[i]);
				tHashMap.put(i,tPrtNos[i]);
			}
		}
		
		VData tVData = new VData();
		tVData.add(tG);
		tVData.add(tHashMap);
		
		GetPayMoneyUI tGetPayMoneyUI = new GetPayMoneyUI();
		if(!tGetPayMoneyUI.submitData(tVData,PolicyType)){
			Content = " 处理失败，原因是: " + tGetPayMoneyUI.mErrors.getFirstError();
            FlagStr = "Fail";
		}else{
            Content = " 处理成功！";
            FlagStr = "Succ";
        }
		
	}catch(Exception e){
		Content = "数据提交失败";
		e.printStackTrace();
	}
	
%>

<html>
	<script language="javascript">
    	parent.fraInterface.afterGenerateData("<%=FlagStr%>","<%=Content%>");
  	</script>
</html>
