<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.ygz.*"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String tOperator = tG.Operator;
	String tComCode = tG.ComCode;
	
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	
	//得到前台页面的数据并进行封装

    String tStartDate=request.getParameter("StartDate");
    String tEndDate=request.getParameter("EndDate");
	String PolicyType = request.getParameter("PolicyType");
	String tChk[] = request.getParameterValues("InpContGridChk");
	String tTempFeeNo[] = request.getParameterValues("ContGrid2");//第二列
	
	LYPremSeparateDetailSet tLYPremSeparateDetailSet = new LYPremSeparateDetailSet();
	try{
		for(int i=0; i<tChk.length; i++){
			if(tTempFeeNo[i] != null && "1".equals(tChk[i])){//1是被选中		
				LYPremSeparateDetailSchema tLYPremSeparateDetailSchema = new LYPremSeparateDetailSchema();			
				tLYPremSeparateDetailSchema.setTempFeeNo(tTempFeeNo[i]);
				System.out.println("tTempFeeNo[" + i + "]:" + tTempFeeNo[i]);
				tLYPremSeparateDetailSet.add(tLYPremSeparateDetailSchema);
				
			}
		}

		TransferData tTransferData=new TransferData();
	    tTransferData.setNameAndValue("StartDate",tStartDate);
	    tTransferData.setNameAndValue("EndDate",tEndDate);
	    tTransferData.setNameAndValue("HandWorkFlag","1");
	    
		VData tVData = new VData();
		
		tVData.add(tG);
		tVData.add(tLYPremSeparateDetailSet);
		tVData.add(tTransferData);
		
		XqPremBillSendUI tXqPremBillSendUI = new XqPremBillSendUI();
		if(!tXqPremBillSendUI.submitData(tVData,PolicyType)){
			Content = " 处理失败，原因是: " + tXqPremBillSendUI.mErrors.getFirstError();
            FlagStr = "Fail";
		}else{
            Content = " 处理完毕！";
            FlagStr = "Succ";
        }
		
	}catch(Exception e){
		Content = "数据提交失败";
		e.printStackTrace();
	}
	
%>

<html>
	<script language="javascript">
    	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  	</script>
</html>
