var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;

/* 查询 */
function EasyQueryClick(){
	
	if(!verifyInput2()){
		return false;
	}
	
	var policyType = fm.PolicyType.value;
	
	var strSQL = "";
	
	var tStartDate = fm.CvaliStartDate.value;
	var tEndDate = fm.CvaliEndDate.value;
	var tPrtNo = fm.PrtNo.value;
	var tContNo = fm.ContNo.value;
//	var tAgentCom = fm.AgentCom.value;
	if((tPrtNo != null && !"" == tPrtNo) || (tContNo !=null && !"" == tContNo)){
		
	}else if((tContNo == null || "" == tContNo) && (tPrtNo == null || "" == tPrtNo)){
		if(tStartDate==null || ""==tStartDate){
			alert("起始日期必录项！");
			return false;
		}
		if(tEndDate==null || ""==tEndDate){
			alert("终止日期必录项！");
			return false;
		}
	}
	if((tStartDate != null && tStartDate != "") 
			&& (tEndDate != null && tEndDate != ""))
		{
			if(dateDiff(tStartDate,tEndDate,"M") > 1)
			{
				alert("查询间隔最多为一个月！");
				return false;
			}
		}
	
	if(policyType!=null && policyType=="2"){
		strSQL = "select grpcontno,prtno,'团单', "
			   + "( select codename as ygzstate from ldcode where codetype='ygzstate' and code = (select  nvl(( select state from lypremseparatedetail where prtno=lcgrpcont.prtno and otherstate in ('01','03','09') and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') fetch first rows only),'00') from dual)) ,"
			   + "( select codename as ygzfpstate from ldcode where codetype='ygzfpstate' and code = (select  nvl(( select lyo.state from LYOutPayDetail lyo,lypremseparatedetail lyp where lyo.insureno=lcgrpcont.prtno and lyo.busino = lyp.busino and lyp.otherstate in ('01','03','09') and lyp.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') fetch first rows only),'01') from dual)) "
			   + "from lcgrpcont "
			   + "where ManageCom like '"+fm.ManageCom.value+"%' "
			   + "and uwflag is not null and uwflag in ('4','9') "//已经核保且核保同意承保
//			   + "and appflag='0' "//未签单
			   + "and (cvalidate>='2016-05-01' or signdate>='2016-05-01') "
//			   + "and not exists (select 1 from LYOutPayDetail where insureno=lcgrpcont.prtno and state<>'03')"
			   + getWherePart('PrtNo','PrtNo')
			   + getWherePart('grpcontno','ContNo')
			   + getWherePart('Cvalidate','CvaliStartDate',">=")
			   + getWherePart('Cvalidate','CvaliEndDate',"<=")
			   + getWherePart('AgentCom','AgentCom')
			   + getWherePart('AgentCode','AgentCode')
			   //add by liyt 2016-07-15 union b表数据
			   + " union "
			   + "select grpcontno,prtno,'团单', "
			   + "( select codename as ygzstate from ldcode where codetype='ygzstate' and code = (select  nvl(( select state from lypremseparatedetail where prtno=lbgrpcont.prtno and otherstate in ('01','03','09') and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') fetch first rows only),'00') from dual)) ,"
			   + "( select codename as ygzfpstate from ldcode where codetype='ygzfpstate' and code = (select  nvl(( select lyo.state from LYOutPayDetail lyo,lypremseparatedetail lyp where lyo.insureno=lbgrpcont.prtno and lyo.busino = lyp.busino and lyp.otherstate in ('01','03','09') and lyp.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') fetch first rows only),'01') from dual)) "
			   + "from lbgrpcont "
			   + "where ManageCom like '"+fm.ManageCom.value+"%' "
			   + "and uwflag is not null and uwflag in ('4','9') "//已经核保且核保同意承保
			   + "and (cvalidate>='2016-05-01' or signdate>='2016-05-01') "
			   + getWherePart('PrtNo','PrtNo')
			   + getWherePart('grpcontno','ContNo')
			   + getWherePart('Cvalidate','CvaliStartDate',">=")
			   + getWherePart('Cvalidate','CvaliEndDate',"<=")
			   + getWherePart('AgentCom','AgentCom')
			   + getWherePart('AgentCode','AgentCode')
			   + " order by grpcontno ";
	}else if(policyType!=null && policyType=="1"){
		strSQL = "select contno,prtno,'个单', "
			   + "( select codename as ygzstate from ldcode where codetype='ygzstate' and code = (select  nvl(( select state from lypremseparatedetail where prtno=lccont.prtno and otherstate in ('01','03','09') and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') fetch first rows only),'00') from dual)) ,"
			   + "( select codename as ygzfpstate from ldcode where codetype='ygzfpstate' and code = (select  nvl(( select lyo.state from LYOutPayDetail lyo,lypremseparatedetail lyp where lyo.insureno=lccont.prtno and lyo.busino = lyp.busino and lyp.otherstate in ('01','03','09') and lyp.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') fetch first rows only),'01') from dual)) "
			   + "from lccont "
			   + "where ManageCom like '"+fm.ManageCom.value+"%' "
			   + "and conttype='1' " //个单
			   + "and appflag='1' "	 //已承保
			   //+ "and customgetpoldate is not null " //客户已签收
			   //2016-05-01之后收费的
			   + "and exists (select 1 from ljtempfee where otherno=lccont.contno and enteraccdate>='2016-05-01' and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype')) "
//			   + "and not exists (select 1 from LYOutPayDetail where insureno=lccont.prtno and state<>'03')"
			   + getWherePart('PrtNo','PrtNo')
			   + getWherePart('ContNo','ContNo')
			   + getWherePart('Cvalidate','CvaliStartDate',">=")
			   + getWherePart('Cvalidate','CvaliEndDate',"<=")
			   + getWherePart('AgentCom','AgentCom')
			   + getWherePart('AgentCode','AgentCode')
			   //add by liyt 2016-07-15 union b表数据
			   + " union "
			   + "select contno,prtno,'个单', "
			   + "( select codename as ygzstate from ldcode where codetype='ygzstate' and code = (select  nvl(( select state from lypremseparatedetail where prtno=lbcont.prtno and otherstate in ('01','03','09') and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') fetch first rows only),'00') from dual)) ,"
			   + "( select codename as ygzfpstate from ldcode where codetype='ygzfpstate' and code = (select  nvl(( select lyo.state from LYOutPayDetail lyo,lypremseparatedetail lyp where lyo.insureno=lbcont.prtno and lyo.busino = lyp.busino and lyp.otherstate in ('01','03','09') and lyp.tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') fetch first rows only),'01') from dual)) "
			   + "from lbcont "
			   + "where ManageCom like '"+fm.ManageCom.value+"%' "
			   + "and conttype='1' " //个单
			   + "and exists (select 1 from ljtempfee where otherno=lbcont.contno and enteraccdate>='2016-05-01' and tempfeetype in (select code from ldcode where codetype = 'ygzqyttype')) "
			   + getWherePart('PrtNo','PrtNo')
			   + getWherePart('ContNo','ContNo')
			   + getWherePart('Cvalidate','CvaliStartDate',">=")
			   + getWherePart('Cvalidate','CvaliEndDate',"<=")
			   + getWherePart('AgentCom','AgentCom')
			   + getWherePart('AgentCode','AgentCode')
			   + " order by contno ";
	}
	
	turnPage.pageLineNum = 50;
	turnPage.queryModal(strSQL,ContGrid);
	if(!turnPage.strQueryResult){
		alert("没有查询到相关数据！");
		return false;
	}
}

/* 调用接口，生成价税分离数据 */
function generateData(){
	
	fm.btnGen.disabled = true;
	
	var checkedRowNum = 0;
    var rowNum = ContGrid.mulLineCount;
    for (var i = 0; i < rowNum; i++){
        
        if(ContGrid.getChkNo(i)){
            checkedRowNum += 1;
            //按照讨论的意思，已经分离过费用的保单不需要传到后台，那就在这里阻断咯。
            var tPrtNo = ContGrid.getRowColData(i,2);
            var tSQL = "select state from lypremseparatedetail where tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and otherstate in ('01','03','09') and prtno = '"+tPrtNo+"' ";
            var arr = easyExecSql(tSQL);
            if(arr && arr[0][0]=="02"){
            	alert("印刷号为:"+tPrtNo+"的单子，已经做过价税分离了，不用再次推送！");
            	fm.btnGen.disabled = false;
            	return;
            }
        }
    }

    if(checkedRowNum < 1){
        alert("请至少选择一条记录!");
        fm.btnGen.disabled = false;
        return false;
    }
	
    var PolicyType = fm.PolicyType.value;    
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GetPayMoneySave.jsp?PolicyType="+PolicyType;
	fm.submit();
	
}

/* 对选中的数据进行判断，打印 */
function printInvoice(){
	
	fm.btnPrt.disabled = true;
	
	var checkedRowNum = 0;
    var rowNum = ContGrid.mulLineCount;
    for (var i = 0; i < rowNum; i++){
        
        if(ContGrid.getChkNo(i)){
            checkedRowNum += 1;
            //按照讨论的意思，已经分离过费用的保单不需要传到后台，那就在这里阻断咯。
            var tPrtNo = ContGrid.getRowColData(i,2);
            var tSQL = "select state from lypremseparatedetail where tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and otherstate in ('01','03','09') and prtno = '"+tPrtNo+"' ";
            var arr = easyExecSql(tSQL);
            if(arr && arr[0][0]!="02"){
            	alert("印刷号为:"+tPrtNo+"的单子，尚没有做价税分离，不能推送打印数据！");
            	fm.btnPrt.disabled = false;
            	return;
            }
            //add by liyt 2016-07-01 添加价税分离表状态为空的情况
            else if(arr == null||arr == ""){
            	alert("印刷号为:"+tPrtNo+"的单子，尚没有提取数据，不能推送打印数据！");
            	fm.btnPrt.disabled = false;
            	return;
            }
            //add by liyt 2016-07-01 添加发票已推送成功的话，不能再次推送
            var tSQL1 = "select lyo.state from LYOutPayDetail lyo where exists (select 1 from lypremseparatedetail where tempfeetype in (select code from ldcode where codetype = 'ygzqyttype') and otherstate in ('01','03','09') and busino = lyo.busino) and lyo.insureno = '"+tPrtNo+"' ";
            var arr1 = easyExecSql(tSQL1);
            if(arr1 && arr1[0][0] == "02"){
            	alert("印刷号为:"+tPrtNo+"的单子，已经推送成功了，不用再次推送！");
            	fm.btnPrt.disabled = false;
            	return;
            }
            
        }
    }

    if(checkedRowNum < 1){
        alert("请至少选择一条记录!");
        fm.btnPrt.disabled = false;
        return false;
    }
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GetInvoiceSave.jsp";
	fm.submit();
	
}

/* 生成价税分离数据后，save页面回调该方法 */
function afterGenerateData(FlagStr,Content ){
	
	EasyQueryClick();
	fm.btnGen.disabled = false;
	showInfo.close();
	
	if(FlagStr == "Fail"){             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

/* 报送打印发票数据后，save页面回调该方法 */
function afterPrintInvoice(FlagStr,Content ){
	
	EasyQueryClick();
	fm.btnPrt.disabled = false;
	showInfo.close();
	
	if(FlagStr == "Fail"){             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

//查询业务员信息
function queryAgent()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        return ;
    }
    if(fm.all('PolicyType').value == ""){
    	alert("请先录入保单类型！");
        return ;
    }
    var saleChnl = "";
    var agentCom = (fm.AgentCom != null && fm.AgentCom != "undefined") ? fm.AgentCom.value : "";
    var tPolicyType = fm.PolicyType.value;
    var branchType = "";
    if(tPolicyType == '2'){
    	branchType = 2;
    }
    if(tPolicyType == '1'){
    	branchType = 1;
    }

    var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
        + "&SaleChnl=" + saleChnl + "&AgentCom=" + agentCom + "&branchtype=" + branchType;
    var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
}

function afterQuery2(arrResult) {
    if(arrResult != null) {
        fm.AgentCode.value = arrResult[0][0];
        fm.AgentName.value = arrResult[0][5];
        fm.GroupAgentCode.value=arrResult[0][95];
    }
}
