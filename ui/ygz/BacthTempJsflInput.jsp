<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：获得
//创建日期：2016-04-22 19:53:28
//创建人  ：ZC
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="BacthTempJsflInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body onload="initForm(); initElementtype();">
	<form action="" method="post" name="fm" target="fraSubmit">
		<table>
    		<tr>
    			<td>
    		    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,forQuery);">
    			</td>
    			<td class= titleImg>查询条件</td>
    		</tr>
    	</table>
    	<div id="forQuery" style="display:''">
    		<table class=common>
    			<tr class=common>
    				<td class="title">生效日期起期</td>
					<td class="input">
						<input class=coolDatePicker dateFormat="short" name=StartDate>
					</td>
					<td class="title">生效日期止期</td>
					<td class="input">
						<input class=coolDatePicker dateFormat="short" name=EndDate>
					</td>
					<td class="title">每次处理数量</td>
    				<td class="input">
    					<input name="SL" class=common>
    				</td>
    			</tr>
    		</table>
    	</div>
    	<hr />
    	<Input type=button value=" 生成价税分离数据 " name="btnGen" class=cssbutton onclick="generateData();">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>