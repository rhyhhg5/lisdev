var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;

/* 查询 */
function EasyQueryClick(){
	
	if(!verifyInput2()){
		return false;
	}
	
	var policyType = fm.PolicyType.value;
	
	var strSQL = "";
	if(""==policyType){
		alert("请选择保单类型");
		return false;
	}
	if(policyType=="2"){
		strSQL = "select a.edorno,c.grpcontno,"
			   + " (select distinct edorname from lmedoritem where edorcode=a.edortype),"
			   + " c.cvalidate,b.confdate,a.getmoney "
			   + " from lpgrpedoritem a,lpedorapp b,lbgrpcont c "
			   + " where a.edorno=b.edoracceptno "
			   + " and a.grpcontno=c.grpcontno "
			   + " and a.edorno=c.edorno "
//			   + " and a.edortype in ('CT','WT','XT') "
			   + " and a.ManageCom like '"+fm.ManageCom.value+"%' "
			   + " and not exists (select 1 from lypremseparatedetail where policyno=c.grpcontno and otherno=a.edorno)"
			   + ""
			   + getWherePart('a.EdorNo','EdorNo')
			   + getWherePart('c.Cvalidate','CvaliStartDate',">=")
			   + getWherePart('c.Cvalidate','CvaliEndDate',"<=")
			   + getWherePart('b.ConfDate','EdorValiStartDate',"<=")
			   + getWherePart('b.ConfDate','EdorValiEndDate',"<=");
	}else if(policyType=="1"){
		strSQL = "select a.edorno,c.contno,"
			   + " (select distinct edorname from lmedoritem where edorcode=a.edortype),"
			   + " c.cvalidate,b.confdate,a.getmoney "
			   + " from lpedoritem a,lpedorapp b,lbcont c "
			   + " where a.edorno=b.edoracceptno "
			   + " and a.contno=c.contno "
			   + " and a.edorno=c.edorno "
//			   + " and a.edortype in ('CT','WT','XT') "
			   + " and c.conttype='1' "
			   + " and a.ManageCom like '"+fm.ManageCom.value+"%' "
			   + " and not exists (select 1 from lypremseparatedetail where policyno=c.contno and otherno=a.edorno)"
			   + getWherePart('a.EdorNo','EdorNo')
			   + getWherePart('c.Cvalidate','CvaliStartDate',">=")
			   + getWherePart('c.Cvalidate','CvaliEndDate',"<=")
			   + getWherePart('b.ConfDate','EdorValiStartDate',"<=")
			   + getWherePart('b.ConfDate','EdorValiEndDate',"<=");
	}
	
	turnPage.queryModal(strSQL,ContGrid);
	if(!turnPage.strQueryResult){
		alert("没有查询到相关数据！");
		return false;
	}
}

/* 调用接口 */
function callInterface(){
	
	fm.btnCall.disabled = true;
	
	var checkedRowNum = 0;
    var rowNum = ContGrid.mulLineCount;

    for (var i = 0; i < rowNum; i++){
        if(checkedRowNum > 1){
            break;
        }

        if(ContGrid.getChkNo(i)){
            checkedRowNum += 1;
        }
    }

    if(checkedRowNum < 1){
        alert("请至少选择一条记录!");
        return false;
    }
	
    var PolicyType = fm.PolicyType.value
    
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./GetTBMoneySave.jsp?PolicyType="+PolicyType;
	fm.submit();
	
}

function afterSubmit(FlagStr,Content ){
	
	EasyQueryClick();
	fm.btnCall.disabled = false;
	showInfo.close();
	
	if(FlagStr == "Fail"){
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}else{
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}