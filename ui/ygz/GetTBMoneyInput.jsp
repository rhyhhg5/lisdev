<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：保全退保数据价税分离
//创建日期：20160504
//创建人  ：lzy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="GetTBMoneyInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="GetTBMoneyInit.jsp"%>
</head>
<body onload="initForm(); initElementtype();">
	<form action="GetTBMoneySave.jsp" method="post" name="fm" target="fraSubmit">
		<table>
    		<tr>
    			<td>
    		    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,forQuery);">
    			</td>
    			<td class= titleImg>查询条件</td>
    		</tr>
    	</table>
    	<div id="forQuery" style="display:''">
    		<table class=common>
    			<tr class=common>
    				<td class="title">管理机构 </td>
					<td class="input">
						<input name="ManageCom" class=codeNo ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" verify="管理机构|notnull&code:comcode"><input class=codename name=ManageComName readonly=true elementtype=nacessary>  
    				</td>
    				<td class="title">保全受理号</td>
    				<td class="input">
    					<input name="EdorNo" class=common>
    				</td>
    				<td class="title">保单类型</td>
    				<td class="input">
            			<input class="codeNo" name=PolicyType CodeData="0^1|个单^2|团单" ondblclick="showCodeListEx('PolicyType',[this,PolicyTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PolicyType',[this,PolicyTypeName],[0,1],null,null,null,1);"><input class=codename name=PolicyTypeName readonly=true elementtype=nacessary verify="保单类型|notnull">
          			</td>
    			</tr>
    			<tr class=common>
    				<td class="title">保单生效日期起期</td>
					<td class="input">
						<input class=coolDatePicker dateFormat="short" name=CvaliStartDate>
					</td>
					<td class="title">保单生效日期止期</td>
					<td class="input">
						<input class=coolDatePicker dateFormat="short" name=CvaliEndDate>
					</td>
					<td class="title"></td>
					<td class="input"></td>
    			</tr>
    			<tr class=common>
    				<td class="title">保全结案日期起期</td>
					<td class="input">
						<input class=coolDatePicker dateFormat="short" name=EdorValiStartDate>
					</td>
					<td class="title">保全结案日期止期</td>
					<td class="input">
						<input class=coolDatePicker dateFormat="short" name=EdorValiEndDate>
					</td>
					<td class="title"></td>
					<td class="input"></td>
    			</tr>
    		</table>
    	</div>
    	<Input type=button value=" 查  询 " class=cssbutton onclick="EasyQueryClick();">
    	<br/>
    	<br/>
    	<table>
    		<tr>
        		<td class=common>
			    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,QueryResult);">
    			</td>
    			<td class= titleImg>查询结果</td>
    		</tr>
    	</table>
    	<div id="QueryResult" style="display:''">
    		<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1>
						<span id="spanContGrid"></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
    	</div>
    	<hr />
    	<Input type=button value="价税分离处理 " name="btnCall" class=cssbutton onclick="callInterface();">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>