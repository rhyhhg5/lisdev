

var showInfo;
var mDebug="0";
var arrGrid;
var ImpartGridobj;
var mSwitch = parent.VD.gVSwitch;


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}




function getQueryDetail1()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = GrpPolGrid.getRowColData(tSel - 1,1);		
	    if (cPolNo == "")
		    return;
		window.open("../app/AppGrpPolQuery.jsp");
		
	}
}




// 保单明细查询
function PolClick()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cPolNo = GrpPolGrid.getRowColData(tSel - 1,2);				
		
		if (cPolNo == "")
		    return;		    
 
		    window.open("../sys/PolDetailQueryMain.jsp?PolNo=" + cPolNo);	
	}
}



//扫描件查询
function ScanQuery()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var prtNo = GrpPolGrid.getRowColData(tSel - 1,2);				

		
		if (prtNo == "")
		    return;
		   
//		  window.open("../sys/ClaimGetQueryMain.jsp?PolNo=" + cPolNo);		
		  window.open("./ProposalEasyScan.jsp?prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");								
	}	     
}

function GoBack(){
	
	top.opener.easyQueryClick();
	top.close();
	
}

var turnPage = new turnPageClass(); 
function easyQueryClick()
{
	
	// 书写SQL语句
	var strSQL = "";
		
	strSQL = "select grppolno,PrtNo,GrpContNo,GrpName,RiskCode,ManageCom,Prem   from LCGrpPol where prtno='" + tPrtNo + "' order by makedate,maketime";			 
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  arrGrid=turnPage.strQueryResult;
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	GrpPolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = GrpPolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}

function GrpPolDefine()
{
	// 初始化表格
	var str = "";
	var tSelNo=GrpPolGrid.getSelNo()-1;
	if(tSelNo==-1)
	{
		alert("请选择集体保单信息后再点击 团单整单要约 按钮");
		return ;
	}
	
    var tRiskType = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo()-1, 5);
	var tFactoryType=initFactoryType(tRiskType);
	alert(tFactoryType);
	initImpartGrid(tFactoryType);
	
	divLCImpart1.style.display= "";
   var GrpPolNo = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo()-1, 1);
   
   fm.GrpPolNo.value = GrpPolNo;

   var strSQL = "select a.FactoryType,a.OtherNo,a.FactoryCode||a.FactorySubCode,a.CalRemark,a.Params,a.FactoryType||"+tRiskType+" from LCFactory a where   "
              +"  GrpPolNo='" +fm.GrpPolNo.value+ "'"
              +"  and PolNo='00000000000000000000'"
              +"  order by a.FactoryType, a.OtherNo,a.FactoryCode,a.FactorySubCode ";
  
   turnPage = new turnPageClass();
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (turnPage.strQueryResult) {
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = ImpartGrid;            
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;   
  //设置查询起始位置
  turnPage.pageIndex = 0;   
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES); 
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
	
}


function GrpPerPolDefine()
{
	var str = "";
	var tSelNo=GrpPolGrid.getSelNo()-1;
	if(tSelNo==-1)
	{
		alert("请选择集体保单信息后再点击 团单下个单要约 按钮");
		return ;
	}
	var GrpPolNo = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo()-1, 1);   
     fm.GrpPolNo.value = GrpPolNo;
    
    var cGrpPrtNo=GrpPolGrid.getRowColData(GrpPolGrid.getSelNo()-1, 2);
   
 window.open("./HealthFactoryDetailQueryMain.jsp?GrpPrtNo="+ cGrpPrtNo+"&GrpPolNo="+ GrpPolNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");  	

}

function GrpPolDefineSave()
{
	// 初始化表格
	var str = "";
	var tSelNo=GrpPolGrid.getSelNo()-1;
	if(tSelNo==-1)
	{
		alert("请选择集体保单信息，录入要素信息后，再点击 团单整单要约保存 按钮");
		return ;
	}

   fm.RiskCode.value = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo()-1, 5); 
   fm.GrpPolNo.value = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo()-1,1);
   fm.all('PolNo').value = "00000000000000000000";
   fm.all('ContNo').value = "00000000000000000000";
   fm.all('flag').value = "save";
   
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
   fm.submit(); //提交
}

function GrpPolDefineDelete()
{
	// 初始化表格
	var str = "";
	var tSelNo=GrpPolGrid.getSelNo()-1;
	if(tSelNo==-1)
	{
		alert("请选择集体保单信息，录入要素信息后，再点击 团单整单要约保存 按钮");
		return ;
	}

   fm.RiskCode.value = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo()-1, 5); 
   fm.GrpPolNo.value = GrpPolGrid.getRowColData(GrpPolGrid.getSelNo()-1,1);
   fm.all('PolNo').value = "00000000000000000000";
   fm.all('ContNo').value = "00000000000000000000";
    fm.all('flag').value = "delete";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
   fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showInfo.close();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
  }
  else
  { 
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  	showInfo.close();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  GrpPolDefine();
}

function easyQueryAddClick()
{

	divLCImpart1.style.display= "none";
}

function initFactoryType(tRiskCode)
{
	// 书写SQL语句
	var k=0;
	var strSQL = "";
	strSQL = "select distinct a.FactoryType,b.FactoryTypeName,a.FactoryType||"+tRiskCode+" from LMFactoryMode a ,LMFactoryType b  where 1=1 "
		   + " and a.FactoryType= b.FactoryType "
		   + " and (RiskCode = '"+tRiskCode+"' or RiskCode ='000000' )";
		   
    var str  = easyQueryVer3(strSQL, 1, 0, 1); 
    return str;
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = GrpPolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	arrSelected[0]= new Array();
	//设置需要返回的数组
	//alert("4:"+arrGrid);
	arrSelected[0][0] =  GrpPolGrid.getRowColData(GrpPolGrid.getSelNo()-1, 1); 
	return arrSelected;
}

//动态显示div
function showdyDiv(parm1){
  var ex,ey;
  ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  var str=fm.all( parm1 ).all('ImpartGrid5').value;
  ImpartGridobj=fm.all( parm1 ).all('ImpartGrid5');
  divDownList.innerHTML='<table id="tabList" style="width:100%" border="0" cellpadding="0" cellspacing="0"><tr><td align="right"><input type="button" value="确定" onclick="doSure()"></td></tr></table>';
  var arr=str.split(",");	
  for (var i=0;i<arr.length;i++){
	var newRow=tabList.insertRow(0);
	var newCell=newRow.insertCell();
	newCell.innerHTML='<input type="text" id="txtValue" name="txtValue" style="width:100%; background-color:#FFFFEF">';
  }
  divDownList.style.left=ex;
  divDownList.style.top =ey;
  divDownList.style.display="";		
}

function doSure(){
	var str="";
	for(var i=0;i<txtValue.length;i++){
		str+=txtValue[i].value;
		str+=",";
	}
	str=str.substring(0,str.length-1);
	ImpartGridobj.value=str;
	divDownList.style.display="none";
}	