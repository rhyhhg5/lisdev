//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var submitFlag="0";
var queryFlag="0";
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var tSelNum = SubPayGrid.mulLineCount;
	var tFlag = false;
	//alert('aaa'+tSelNum);
	for (i=0;i<=tSelNum-1;i++)
	{
		if (SubPayGrid.getChkNo(i))
		{
			tFlag = true;
		}
	}	
	if (tFlag)
	{
		submitFlag="1";
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  //showSubmitFrame(mDebug);
	  fm.action="./LFGetPaySave.jsp"
	  fm.submit(); //提交
	}
	else
	{
		alert("请选择给付信息!");
		return;
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail")
  {             
   	if (queryFlag=="1")
   	{
   		initGetDrawGrid();
   	}
   	else
   	{
   		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	   	initForm();
	  }
  }
  else
  { 
  	//alert('aaa');
  	emptyUndefined();
  	//alert('bbb');
	  SubPayGrid.delBlankLine('SubPayGrid');
   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
   // alert(submitFlag);
    if (submitFlag=="1")
    {
    	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  	//parent.fraInterface.initForm();
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    	queryClick();
    	queryFlag="1";
    }
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LJSGetDraw.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  var tNoticeNo = fm.all('bmtz').value;
  var tPolNo = fm.all('bmcert').value;
  if ((tNoticeNo==null||tNoticeNo=='')&&(tPolNo==null||tPolNo==''))
  {
  	alert("请录入保单号或通知书号查询!");
  	return;
  }	
	submitFlag="0";
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
 // showSubmitFrame(mDebug);
  fm.action="./LFGetPayQueryOut.jsp"
  fm.submit(); //提交
 }           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//计算合计金额
function CalSumMoney(parm1,parm2)
{
	var tSelNum = SubPayGrid.mulLineCount;
	var i;
	var aGetMoney=0;
	//alert('aaa'+tSelNum);
	for (i=0;i<=tSelNum-1;i++)
	{
		if (SubPayGrid.getChkNo(i))
		{
			var tMoney =0;
			if (SubPayGrid.getRowColData(i,7)!=null&&SubPayGrid.getRowColData(i,7)!='')
				tMoney = parseFloat(SubPayGrid.getRowColData(i,7));
				
			aGetMoney = aGetMoney + tMoney;
			//alert(aGetMoney);	
		}
	}	
	fm.all('SumGetMoney').value = aGetMoney;	
}