//程序名称：PolStatus.js
//程序功能：保单状态查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,PrtSetStr)
{	

  showInfo.close();

  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

//	alert(fm.all('fmtransact').value);
  	
    if (fm.all('fmtransact').value == "INSERT") {
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
        //alert("success!");
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initPolGrid();
		easyQueryClick();
		if (PrtSetStr != "") {		  
		  window.open("./LCAGetPrt.jsp?PrtSetStr=" + PrtSetStr);	  	
		}      
    }
    if (fm.all('fmtransact').value == "CalCurrentMoney") {
  	  fm.all('CurrentMoney').value=PrtSetStr;
    }
    if (fm.all('fmtransact').value == "CalValiCurrentMoney") {
  	   fm.all('ValiCurrentMoney').value=PrtSetStr;
    }
 }
  	 	
}

//确认整次申请
function ApplyConfirm()
{
	fm.all('fmtransact').value = "INSERT";
	if(fm.all('GetMoney').value == ""||fm.all('GetMoney').value == "0")
	{
		alert("请录入领取金额");
		return ;
	}
	if (window.confirm("是否确认本次申请?"))
	{
		 fm.all('GetMoneyHide').value = fm.all('GetMoney').value  
		　　PrtTimes='0';
		  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		  fm.submit();
		
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



// 查询按钮
function easyQueryClick()
{
	
	// 书写SQL语句
	k++;
	var strSQL = "";
	strSQL = "select SerialNo,InsuAccNo,PolNo,RiskCode,Money,MoneyType,OtherNo,OtherType,Operator,MakeDate,State from LCInsureAccTrace  where "+k+" = "+k
				 + getWherePart( 'PolNo','PolNo' )
				 + getWherePart( 'OtherNo','OtherNo' )
				 + getWherePart( 'InsuAccNo','InsuAccNo' )
				 + " and ManageCom like '" + manageCom + "%%'"
				 + " order  by SerialNo";

	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function getStatus()
{
  var i = 0;
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,*,0,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function CalCurrentMoney()
{
	fm.all('fmtransact').value = "CalCurrentMoney";
	fm.all('GetMoneyHide').value="0";
	var i = 0;
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

function CalValiCurrentMoney()
{
	fm.all('fmtransact').value = "CalValiCurrentMoney";
	fm.all('GetMoneyHide').value="0";
	var i = 0;
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}



