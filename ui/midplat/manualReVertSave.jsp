<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.sinosoft.midplat.kernel.management.YBTAutoBack"%>

<%
	Logger mLogger = Logger.getLogger("ui.midplat.manualReVertSave_jsp");
	
	GlobalInput mGlobalInput = (GlobalInput) session.getValue("GI");
	
	String mContNo = request.getParameter("ContNo");
	mLogger.info("ContNo = " + mContNo);
	String mContent = null;
	try {
		YBTAutoBack tYBTAutoBack = new YBTAutoBack(mContNo);
		tYBTAutoBack.deal();
		mContent = "数据恢复成功！";
	} catch (Exception ex) {
		mLogger.error("数据恢复(manualReVertSave.jsp)失败！", ex);
		
		mContent = "数据恢复失败！原因：" + ex.getMessage();
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=mContent%>");
</script>
</html>