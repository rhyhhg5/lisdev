
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var turnPage = new turnPageClass();

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}

/**
 * 查询地区代码
 */
function getComcode() {
	
		
	var QuerySQL, arrResult;
	QuerySQL = "select Code, CodeName from ldcode where codetype = 'station' and length(trim(code)) = 4 order by Code with ur";
	try {
		arrResult = easyExecSql(QuerySQL, 1, 0);
	} catch (ex) {
		alert("查询产品代码出现异常！ ");
		return;
	}
	sCodeData = "0|"
	for ( var i = 0; i < arrResult.length; i++) {
		sCodeData += "^" + arrResult[i][0] + "|" + arrResult[i][1];
	}
	// alert(sCodeData);
	try {
		document.getElementsByName("Comcode")[0].CodeData = sCodeData;
	} catch (ex) {
	}
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick() {
	fm.updateSubmit.disabled = true;
	initybtBQPolGrid();
	var cardFlag = "";
	if("0" == fm.Channel.value){
		cardFlag = " and channel = '9'";
	}else if ("1" == fm.Channel.value){
		cardFlag = " and channel = 'c'";
	}else if ("8" == fm.Channel.value){
		cardFlag = " and channel = 'd'";
	}else if ("e" == fm.Channel.value){
		cardFlag = " and channel = 'e'";
	}else if ("f" == fm.Channel.value){
		cardFlag = " and channel = 'f'";
	}else if ("00" == fm.Channel.value){
		cardFlag = " and channel = '00'";
	}
	var strSQL;
	strSQL = "select id,bankcode,comcode,riskcode,case channel when '00' then '所有渠道' when '9' then '柜面' when 'c' then '网银' when 'd' then 'ATM' when 'e' then '手机银行' when 'f' then '智慧柜员机' end,case status when '0' then '已关闭' else '已开通' end,operator," +
			"changedate from ybtriskswtich where datatype = 'BQ'"
	strSQL += getWherePart('BankCode','BankCode')
	strSQL += getWherePart('ComCode','ComCode')
	strSQL += getWherePart('Status','Status')
	strSQL += getWherePart('RiskCode','RiskCode')
	strSQL += cardFlag
	strSQL += ' order by bankcode';
	turnPage.pageLineNum = 20;
	turnPage.queryModal(strSQL, ybtBQPolGrid);

	return true;
}
// Click事件，当点击“增加”图片时触发该函数
function addClick() {
	fm.updateSubmit.disabled=true;
	var cardFlag = "";
	var cardFlag1 = "";
	if("0" == fm.Channel.value){
		cardFlag = " and channel = '9'";
		cardFlag1 = "9";
	}else if ("1" == fm.Channel.value){
		cardFlag = " and channel = 'c'";
		cardFlag1 = "c";
	}else if ("8" == fm.Channel.value){
		cardFlag = " and channel = 'd'";
		cardFlag1 = "d";
	}else if ("e" == fm.Channel.value){
		cardFlag = " and channel = 'e'";
		cardFlag1 = "e";
	}else if ("f" == fm.Channel.value){
		cardFlag = " and channel = 'f'";
		cardFlag1 = "f";
	}else if ("00" == fm.Channel.value){
		cardFlag = " and channel = '00'";
		cardFlag1 = "00";
	}
	if("" == fm.ComCode.value)
		fm.ComCode.value = "86";
	if (verifyInput() == false)
		return false;
	var tSQLStr = "select * from YBTRiskSwtich where datatype = 'BQ' and comcode in ('86','" + fm.ComCode.value + "')" 
		tSQLStr += getWherePart('BankCode', 'BankCode')
		tSQLStr += getWherePart('RiskCode', 'RiskCode')
		tSQLStr += cardFlag
		tSQLStr += " with ur";
	var tSQLResultStr  = easyQueryVer3(tSQLStr, 1, 1, 1);
	if (tSQLResultStr) {
		alert("该保全配置已经存在或已配置全国权限！");
		fm.ComCode.value = "86";
		return queryClick();
	}
	var tSQL = "select * from YBTRiskSwtich where datatype = 'BQ' and channel in ('00','" + cardFlag1 + "')" 
		tSQL += getWherePart('BankCode', 'BankCode')
		tSQL += getWherePart('RiskCode', 'RiskCode')
		tSQL += getWherePart('ComCode','ComCode')
		tSQL += " with ur";
	var tSQLResult  = easyQueryVer3(tSQL, 1, 1, 1);
	if (tSQLResult) {
		fm.Channel.value = "00";
		alert("该保全配置已经存在或已配置所有渠道权限！");
		return queryClick();
	}
	fm.DataType.value = "BQ";
	fm.hideOperate.value="INSERT||MAIN";
	if (!confirm("您确实添加改该记录吗?"))return;
	submitForm();
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick(){
	if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
	var rowArr = null;
	n = checkedRowNo("ybtBQPolGridSel");
	if (n == -1) {
		alert('请先选中一条记录!');
		return;
	}
	fm.updateSubmit.disabled = false;
	if (n != -1) {
		rowArr = ybtBQPolGrid.getRowData(n);
		if (rowArr != null) {
			fm.BankCode.value = rowArr[1];
			fm.ComCode.value = rowArr[2];
			fm.RiskCode.value = trim(rowArr[3]);
			fm.ChannelName.value = trim(rowArr[4]);
			if(fm.ChannelName.value == "柜面")
				fm.Channel.value = "0";
			if(fm.ChannelName.value == "网银")
				fm.Channel.value = "1";
			if(fm.ChannelName.value == "ATM")
				fm.Channel.value = "8";
			if(fm.ChannelName.value == "手机银行")
				fm.Channel.value = "e";
			if(fm.ChannelName.value == "智慧柜员机")
				fm.Channel.value = "f";
			if(fm.ChannelName.value == "所有渠道")
				fm.Channel.value = "00";
			fm.StatusName.value = trim(rowArr[5]);
			if(fm.StatusName.value == "已关闭"){
				fm.Status.value = "0";
				fm.StatusName.value = "关闭";
			}
			if(fm.StatusName.value == "已开通"){
				fm.Status.value = "1";
				fm.StatusName.value = "开通";
			}
			fm.StartDate.value = rowArr[7];
		}
		fm.ID.value = rowArr[0];
	}
}

//Click事件，当点击“保存”图片时触发该函数
function updateSubmitForm() {
	if (turnPage.queryAllRecordCount <= 0) {
		alert('请先查询!');
		return;
	}
	// 下面增加相应的代码
	if (verifyInput() == false)
		return false;
	if (confirm("您确实想修改该记录吗?")) {
		fm.DataType.value = "BQ";
		fm.hideOperate.value = "UPDATE||MAIN";
		submitForm();
		fm.updateSubmit.disabled = true;
	} else {
		fm.hideOperate.value = "";
		alert("您取消了修改操作！");
		return;
	}
}
// 返回选中的行号,这对radio button
function checkedRowNo(name) {
	obj = document.all[name];

	if (typeof (obj.length) != 'undefined') {
		for (i = 0; i < obj.length; i++) {
			if (obj[i].checked)
				return i;
		}
	} else {
		if (obj.checked)
			return 0;
	}
	return -1;
}


// Click事件，当点击“删除”图片时触发该函数
function delClick() {
	fm.updateSubmit.disabled = true;
	if (turnPage.queryAllRecordCount <= 0) {
		alert('请先查询!');
		return;
	}
	// 下面增加相应的代码
	fm.hideOperate.value = "DEL||MAIN";
	if (!confirm("您确实删除改该记录吗?"))
		return;
	var rowArr = null;
	n = checkedRowNo("ybtBQPolGridSel");
	if (n != -1) {
		rowArr = ybtBQPolGrid.getRowData(n);
		if (rowArr != null) {
			fm.ID.value = rowArr[0];
		}
	}
	submitForm();
	initInpBox();
}

// Click事件，当点击“重置”图片时触发该函数
function resetForm() {
	try {
		initForm();
	} catch (re) {
		alert("在LAComInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

// 提交，保存按钮对应操作
function submitForm() {
	var i = 0;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	if (fm.hideOperate.value == "") {
		alert("操作控制数据丢失！");
	}
	fm.submit(); // 提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    if(fm.hideOperate.value=="DEL||MAIN")content="删除成功!";
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
          //parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   // if(fm.hideOperate.value.length>0&&fm.hideOperate.value!="DEL||MAIN")
    queryClick();//提交后自动查询
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}