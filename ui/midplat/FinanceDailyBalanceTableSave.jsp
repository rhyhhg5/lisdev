<%@page contentType="text/html;charset=gb2312" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>

<%
    //在此设置导出Excel的列名，应与sql语句取出的域相对应

    ExportExcel.Format format = new ExportExcel.Format();
    ArrayList listCell = new ArrayList();
    ArrayList listLB = new ArrayList();
    ArrayList listColWidth = new ArrayList();
    format.mListCell=listCell;
    format.mListBL=listLB;
    format.mListColWidth=listColWidth;

    ExportExcel.Cell tCell=null;
    ExportExcel.ListBlock tLB=null;

       //管理机构
       String BankBranch = request.getParameter("BankBranch");
       System.out.println("地区代码=============="+BankBranch);
       //交费开始日期
       String BankNode = request.getParameter("BankNode");
       System.out.println("网点代码=============="+BankNode);
       //交费结束日期
       String StartTransDate = request.getParameter("StartTransDate");
       System.out.println("交费开始日期==============="+StartTransDate);       
       String EndTransDate = request.getParameter("EndTransDate");
       String BankCode = request.getParameter("BankCode");

   listColWidth.add(new String[]{"0","5000"});

    String sql = "select c.riskcode,d.riskname,sum(c.prem) from lccont a,lktransstatus b,lcpol c,lmrisk d "
			+ "where a.contno = b.polno and c.contno = a.contno and c.riskcode = d.riskcode and a.salechnl = '04' and a.cardflag = '9' and a.appflag = '1' and " 
			+"b.bankcode='" + BankCode + "' and a.PolApplyDate between '" + StartTransDate + "' and  '" + EndTransDate + "' ";
    if ((null!=BankBranch) && (!BankBranch.equals(""))) {
    	sql += "and b.bankbranch='" + BankBranch + "' ";
    }
    if ((null!=BankNode) && (!BankNode.equals(""))) {
    	sql += "and b.banknode='" + BankNode + "' ";
    }
    sql += "group by c.riskcode, d.riskname";
    sql += " union all "
    	+ "select c.riskcode,d.riskname,sum(c.prem) from lbcont a,lktransstatus b,lbpol c,lmrisk d "
			+ "where a.contno = b.polno and c.contno = a.contno and c.riskcode = d.riskcode and a.salechnl = '04' and a.cardflag = '9' and a.appflag = '3' and " 
			+"b.bankcode='" + BankCode + "' and a.PolApplyDate between '" + StartTransDate + "' and  '" + EndTransDate + "' ";
    if ((null!=BankBranch) && (!BankBranch.equals(""))) {
    	sql += "and b.bankbranch='" + BankBranch + "' ";
    }
    if ((null!=BankNode) && (!BankNode.equals(""))) {
    	sql += "and b.banknode='" + BankNode + "' ";
    }
    sql += "group by c.riskcode, d.riskname with ur";
    
         
    System.out.println("新契约承保明细sql："+sql);
    tLB = new ExportExcel.ListBlock("001");
    tLB.colName=new String[]{"险种代码","险种名称","保费收入"};
    tLB.sql=sql;
    tLB.row1=0;
    tLB.col1=0;
    tLB.InitData();
    listLB.add(tLB);

    try{

      response.reset();
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition","attachment; filename=YBT_"+StartTransDate+"_"+EndTransDate+"_List.xls");
      OutputStream outOS=response.getOutputStream();

      BufferedOutputStream bos=new BufferedOutputStream(outOS);

      ExportExcel excel = new ExportExcel();

      excel.write(format,bos);

      bos.flush();
      bos.close();
    }
    catch(Exception e){
      System.out.println("导出Excel失败！");
    };

%>
