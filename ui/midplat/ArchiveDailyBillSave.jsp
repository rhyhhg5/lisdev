<%@page contentType="text/html;charset=gb2312" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>

<%
    //在此设置导出Excel的 列名， 应与sql语句取出的域相对应

    ExportExcel.Format format = new ExportExcel.Format();
    ArrayList listCell = new ArrayList();
    ArrayList listLB = new ArrayList();
    ArrayList listColWidth = new ArrayList();
    format.mListCell=listCell;
    format.mListBL=listLB;
    format.mListColWidth=listColWidth;

    ExportExcel.Cell tCell=null;
    ExportExcel.ListBlock tLB=null;

			 GlobalInput tG = (GlobalInput) session.getValue("GI");
       String BankCode = request.getParameter("BankCode");
       String ManageCom = request.getParameter("ManageCom");
       String PrtNo = request.getParameter("PrtNo");
       String ProposalContNo = request.getParameter("ProposalContNo");
       String Flag = request.getParameter("Flag");
       String StartTransDate = request.getParameter("StartTransDate");
       String EndTransDate = request.getParameter("EndTransDate");
       String ContFlag = request.getParameter("ContFlag");
       if (ManageCom == null || ManageCom.equals("")) {
       			ManageCom = tG.ManageCom;
       }

    listColWidth.add(new String[]{"0","5000"});
    
    String sql = "";
    if (Flag.equals("1")) {
  	sql = "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通柜面出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END,(days(a.PolApplyDate) - days(h.Makedate)) "
			+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%' ";		
			if (null != BankCode && !BankCode.equals("")) {
    	    	sql += "and b.BankCode='" + BankCode + "' ";
    	    }else{
    	    	sql+= " and b.bankcode in ('01','02','03','04','10','16')";
    	    }
    	    if(null != ContFlag  && !ContFlag.equals("")){
  				sql+= " and a.cardflag= '"+ ContFlag +"'";
  			}
			if (null != PrtNo && !PrtNo.equals("")) {
	    		sql += "and a.PrtNo='" + PrtNo + "' ";
	    	}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		sql += "and a.ProposalContNo='" + ProposalContNo + "' ";
	    	}
	    	
	    	//add by zengzm 2016-03-23
	    	sql += " union all "
	      		+ "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通柜面出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END,(days(a.PolApplyDate) - days(h.Makedate)) "
	    			+ "from lbcont a,lktransstatus b,lbpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
	    			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
	    			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%' ";
	    			if (null != BankCode && !BankCode.equals("")) {
	        	     	sql += "and b.BankCode='" + BankCode + "' "; 
	        	   	}else{
	        	      	sql+= " and b.bankcode in ('01','02','03','04','10','16')";
	        	   	}
	    			if(null != ContFlag  && !ContFlag.equals("")){
	      				sql+= " and a.cardflag= '"+ContFlag+"'";
	      			}
	    			if (null != PrtNo && !PrtNo.equals("")) {
	        			sql += "and a.PrtNo='" + PrtNo + "' ";
	        		}
	    	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    	    		sql += "and a.ProposalContNo='" + ProposalContNo + "' "; 
	    	    	}
	    	
			sql += "order by bankcode,PolApplyDate, ContNo with ur";
  } else if (Flag.equals("0")) {
  	sql = "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, '', '否',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通柜面出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END,'0'"
			+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f,lacom g "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%' "
			+ "and not exists (select 1 from ES_DOC_Main h where h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04')) ";
			if (null != BankCode && !BankCode.equals("")) {
    	     	sql += "and b.BankCode='" + BankCode + "' ";
    	    }else{
    	      	sql+= " and b.bankcode in ('01','02','03','04','10','16')";
    	    }
    	    if(null != ContFlag  && !ContFlag.equals("")){
  				sql+= " and a.cardflag= '"+ContFlag+"'";
  			}
			if (null != PrtNo && !PrtNo.equals("")) {
	    		sql += "and a.PrtNo='" + PrtNo + "' ";
	    	}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		sql += "and a.ProposalContNo='" + ProposalContNo + "' ";
	    	}
	    	
	    	//add by zengzm 2016-03-23
	    	sql += " union all "
	      		+ "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通柜面出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END,(days(a.PolApplyDate) - days(h.Makedate)) "
	    			+ "from lbcont a,lktransstatus b,lbpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
	    			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
	    			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%' ";
	    			if (null != BankCode && !BankCode.equals("")) {
	        	     	sql += "and b.BankCode='" + BankCode + "' "; 
	        	   	}else{
	        	      	sql+= " and b.bankcode in ('01','02','03','04','10','16')";
	        	   	}
	    			if(null != ContFlag  && !ContFlag.equals("")){
	      				sql+= " and a.cardflag= '"+ContFlag+"'";
	      			}
	    			if (null != PrtNo && !PrtNo.equals("")) {
	        			sql += "and a.PrtNo='" + PrtNo + "' ";
	        		}
	    	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    	    		sql += "and a.ProposalContNo='" + ProposalContNo + "' "; 
	    	    	}
	    	
			sql += "order by bankcode,PolApplyDate, ContNo with ur"; 
  } else {
  	sql = "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, '', '否',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通柜面出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END,'0'"
			+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f,lacom g  "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%' "
			+ "and not exists (select 1 from ES_DOC_Main h where h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04')) ";
			if (null != BankCode && !BankCode.equals("")) { 
    	     	sql += "and b.BankCode='" + BankCode + "' ";
    	    }else{
    	      	sql+= " and b.bankcode in ('01','02','03','04','10','16')";
    	    }
			if (null != PrtNo && !PrtNo.equals("")) {
    			sql += "and a.PrtNo='" + PrtNo + "' ";
	    	}
	    	if(null != ContFlag  && !ContFlag.equals("")){
	  			sql+= " and a.cardflag= '"+ ContFlag +"'";
	  		}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		sql += "and a.ProposalContNo='" + ProposalContNo + "' ";
	    	}
  		sql += " union all "
  		+ "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通柜面出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END,(days(a.PolApplyDate) - days(h.Makedate)) "
			+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%' ";
			if (null != BankCode && !BankCode.equals("")) {
    	     	sql += "and b.BankCode='" + BankCode + "' "; 
    	   	}else{
    	      	sql+= " and b.bankcode in ('01','02','03','04','10','16')";
    	   	}
			if(null != ContFlag  && !ContFlag.equals("")){
  				sql+= " and a.cardflag= '"+ContFlag+"'";
  			}
			if (null != PrtNo && !PrtNo.equals("")) {
    			sql += "and a.PrtNo='" + PrtNo + "' ";
    		}
	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
	    		sql += "and a.ProposalContNo='" + ProposalContNo + "' "; 
	    	}  
	    	
	   	//add by zengzm 2016-03-23
    	sql += " union all "
      		+ "select e.name, a.managecom, g.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, d.riskname, c.prem, char(h.Makedate), '是',b.bankcode , CASE  WHEN a.cardflag = '9' THEN '银保通柜面出单' WHEN a.cardflag = 'c' THEN '网银出单' WHEN a.cardflag = 'd' then '终端出单' ELSE '未知状态' END,(days(a.PolApplyDate) - days(h.Makedate)) "
    			+ "from lbcont a,lktransstatus b,lbpol c,lmrisk d,ldcom e,laagent f,lacom g, ES_DOC_Main h "
    			+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode and h.DocCode = a.prtno and h.subtype in ('TB24', 'TB04') "
    			+ "and a.appflag='1' and g.agentcom = a.agentcom and g.managecom = a.managecom and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' and a.managecom like '" + ManageCom + "%' ";
    			if (null != BankCode && !BankCode.equals("")) {
        	     	sql += "and b.BankCode='" + BankCode + "' "; 
        	   	}else{
        	      	sql+= " and b.bankcode in ('01','02','03','04','10','16')";
        	   	}
    			if(null != ContFlag  && !ContFlag.equals("")){
      				sql+= " and a.cardflag= '"+ContFlag+"'";
      			}
    			if (null != PrtNo && !PrtNo.equals("")) {
        			sql += "and a.PrtNo='" + PrtNo + "' ";
        		}
    	    	if (null!=ProposalContNo && !ProposalContNo.equals("")) {
    	    		sql += "and a.ProposalContNo='" + ProposalContNo + "' "; 
    	    	}
	    	
			sql += " order by bankcode,PolApplyDate, ContNo with ur";  
  }
         
    System.out.println("新契约承保明细sql："+sql);   
    tLB = new ExportExcel.ListBlock("001");
    tLB.colName = new String[]{"管理机构名称","机构代码","银行机构名称","银行网点代码","公司网点代码","客户经理","银行柜员代码","投保单号","保单号","投保人","承保日期","险种名称","保费收入","归档日期","归档情况","银行代码","保单类型","归档时效(天)"};
    tLB.sql=sql;
    tLB.row1=0;
    tLB.col1=0;
    tLB.InitData();
    listLB.add(tLB); 
 
    try{
	  //将输出流置空,否则会因为流的冲突造成websphere报错.  
	  // out.clear();  
	  //out = pageContext.pushBody();
	  	
      response.reset();
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition","attachment; filename=YBT_"+StartTransDate+"_"+EndTransDate+"_List.xls");
      OutputStream outOS=response.getOutputStream();

      BufferedOutputStream bos=new BufferedOutputStream(outOS);

      ExportExcel excel = new ExportExcel();

      excel.write(format,bos);

      bos.flush();
      bos.close();
    }
    catch(Exception e){
      System.out.println("导出Excel失败！");
    };

%>
