
var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var turnPage = new turnPageClass();

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick() {
//	alert("查询");
	fm.updateSubmit.disabled = true;
	initybtPolGrid();
	var cardFlag = "";
	if("0" == fm.Channel.value){
		cardFlag = " and yy.channel = '9'";
	}else if ("1" == fm.Channel.value){
		cardFlag = " and yy.channel = 'c'";
	}else if ("8" == fm.Channel.value){
		cardFlag = " and yy.channel = 'd'";
	}else if ("e" == fm.Channel.value){
		cardFlag = " and yy.channel = 'e'";
	}else if ("f" == fm.Channel.value){
		cardFlag = " and yy.channel = 'f'";
	}else if ("00" == fm.Channel.value){
		cardFlag = " and yy.channel = '00'";
	}else if ("a" == fm.Channel.value){
		cardFlag = " and yy.channel = 'a'";
	}
	var strSQL;
	strSQL = "select yy.id,yy.bankcode,yy.comcode,lm.riskcode,lm.riskname,case yy.datatype when 'ST' then '停售' when 'GX' then '个险' when 'GWN' then '团险' when 'WN' then '特有流程' end" 
		+ ",yy.operator,case  yy.status when '0' then '失效' else '有效' end" 
		+ ",case yy.channel when '00' then '所有渠道' when '9' then '柜面' when 'c' then '网银' when 'd' then '自助终端' when 'e' then '手机银行' when 'f' then '智慧柜员机' when 'a' then '信保通' end," 
		+ "yy.changedate from ybtriskswtich yy,lmrisk lm where yy.riskcode = lm.riskcode and yy.datatype in ('ST','GX','GWN','WN')"
		strSQL += getWherePart('yy.BankCode','BankCode')
		strSQL += getWherePart('yy.RiskCode','RiskCode')
		strSQL += getWherePart('yy.ComCode','ComCode')
		strSQL += getWherePart('yy.DataType','DataType')
		strSQL += getWherePart('yy.Status','Status')
		strSQL += getWherePart('yy.changedate','StartDate')
		strSQL += cardFlag
		+ "union select yy.id,yy.bankcode,yy.comcode,lm.riskwrapcode,lm.riskwrapname,case yy.datatype when 'ST' then '停售' when 'GX' then '个险' when 'GWN' then '团险' when 'WN' then '特有流程' end"
		+ ",yy.operator,case  yy.status when '0' then '失效' else '有效' end"
		+ ",case yy.channel when '00' then '所有渠道' when '9' then '柜面' when 'c' then '网银' when 'd' then '自助终端' when 'e' then '手机银行' when 'f' then '智慧柜员机' when 'a' then '信保通' end," 
		+ "yy.changedate from ybtriskswtich yy,ldriskwrap lm where yy.riskcode = lm.riskwrapcode and yy.datatype in ('ST','GX','GWN','WN')"
		strSQL += getWherePart('yy.BankCode','BankCode')
		strSQL += getWherePart('yy.RiskCode','RiskCode')
		strSQL += getWherePart('yy.ComCode','ComCode')
		strSQL += getWherePart('yy.DataType','DataType')
		strSQL += getWherePart('yy.Status','Status')
		strSQL += getWherePart('yy.changedate','StartDate')
		strSQL += cardFlag
		strSQL += ' fetch first 3000 rows only with ur';
	turnPage.pageLineNum = 20;
	turnPage.queryModal(strSQL, ybtPolGrid);

	return true;
}

// Click事件，当点击“增加”图片时触发该函数
function addClick() {
	fm.updateSubmit.disabled=true;
	if("" == fm.BankCode.value.trim()){
		fm.BankCode.value = "00";
	}
	if("" == fm.ComCode.value.trim()){
		fm.ComCode.value = "86";
	}
	if (verifyInput() == false)
		return false;
	fm.hideOperate.value="INSERT||MAIN";
	if (!confirm("您确实添加改该记录吗?"))return;
	submitForm();
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick(){
	if(turnPage.queryAllRecordCount<=0){alert('请先查询!');return ;}
	var rowArr = null;
	n = checkedRowNo("ybtPolGridSel");
	if (n == -1) {
		alert('请先选中一条记录!');
		return;
	}
	fm.updateSubmit.disabled = false;
	if (n != -1) {
		rowArr = ybtPolGrid.getRowData(n);
		if (rowArr != null) {
			fm.BankCode.value = rowArr[1];
			fm.ComCode.value = rowArr[2];
			fm.RiskCode.value = rowArr[3];
			fm.RiskName.value = rowArr[4];
			fm.DataTypeName.value = trim(rowArr[5]);
			if(fm.DataTypeName.value == "停售")
				fm.DataType.value = "ST";
			if(fm.DataTypeName.value == "个险")
				fm.DataType.value = "GX";
			if(fm.DataTypeName.value == "特有流程")
				fm.DataType.value = "WN";
			if(fm.DataTypeName.value == "团险")
				fm.DataType.value = "GWN";
			fm.StatusName.value = trim(rowArr[7]);
			if(fm.StatusName.value == "失效")
				fm.Status.value = "0";
				if(fm.StatusName.value == "有效")
					fm.Status.value = "1";
			fm.ChannelName.value = trim(rowArr[8]);
			if(fm.ChannelName.value == "柜面")
				fm.Channel.value = "0";
			if(fm.ChannelName.value == "网银")
				fm.Channel.value = "1";
			if(fm.ChannelName.value == "自助终端")
				fm.Channel.value = "8";
			if(fm.ChannelName.value == "手机银行")
				fm.Channel.value = "e";
			if(fm.ChannelName.value == "智慧柜员机")
				fm.Channel.value = "f";
			if(fm.ChannelName.value == "所有渠道")
				fm.Channel.value = "00";
			if(fm.ChannelName.value == "信保通")
				fm.Channel.value = "a";
			fm.StartDate.value = rowArr[9];
			fm.ID.value = rowArr[0];
		}
	}
}

//Click事件，当点击“保存”图片时触发该函数
function updateSubmitForm() {
	if (turnPage.queryAllRecordCount <= 0) {
		alert('请先查询!');
		return;
	}
	// 下面增加相应的代码
	if (verifyInput() == false)
		return false;
	if (confirm("您确实想修改该记录吗?")) {
		fm.hideOperate.value = "UPDATE||MAIN";
		submitForm();
		fm.updateSubmit.disabled = true;
	} else {
		fm.hideOperate.value = "";
		alert("您取消了修改操作！");
		return;
	}
}
// 返回选中的行号,这对radio button
function checkedRowNo(name) {
	obj = document.all[name];

	if (typeof (obj.length) != 'undefined') {
		for (i = 0; i < obj.length; i++) {
			if (obj[i].checked)
				return i;
		}
	} else {
		if (obj.checked)
			return 0;
	}
	return -1;
}


// Click事件，当点击“删除”图片时触发该函数
function delClick() {
	fm.updateSubmit.disabled = true;
	if (turnPage.queryAllRecordCount <= 0) {
		alert('请先查询!');
		return;
	}
	// 下面增加相应的代码
	fm.hideOperate.value = "DEL||MAIN";
	if (!confirm("您确实删除改该记录吗?"))
		return;
	var rowArr = null;
	n = checkedRowNo("ybtPolGridSel");
	if (n != -1) {
		rowArr = ybtPolGrid.getRowData(n);
		if (rowArr != null) {
			fm.ID.value = rowArr[0];
		}
	}
	submitForm();
	initInpBox();
}

// Click事件，当点击“清空”图片时触发该函数
function clearForm() {
	try {
		typeFlag = "";
		initForm();
	} catch (re) {
		alert("在LAComInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}
// Click事件，当点击“重置”图片时触发该函数
function resetForm() {
	try {
		typeFlag = fm.hideReset.value;
		initForm();
	} catch (re) {
		alert("在LAComInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

// 提交，保存按钮对应操作
function submitForm() {
	var i = 0;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	if (fm.hideOperate.value == "") {
		alert("操作控制数据丢失！");
	}
	fm.submit(); // 提交
}

/**
 * 查询产品代码
 */
function getProductCode() {

	var sRiskCode;
	try {
		sRiskCode = fm.RiskCode.value;
//		alert(sRiskCode);
	} catch (ex) {
	}
	if (sRiskCode == null || sRiskCode == "") {
		alert("请输入险种代码!");
		return;
	} else {

		var QuerySQL, arrResult;
		QuerySQL = "select riskwrapcode,riskwrapname from ldriskwrap where riskcode = '"+sRiskCode+"' with ur";
//		alert(QuerySQL);
		try {
			arrResult = easyExecSql(QuerySQL, 1, 0);
		} catch (ex) {
			alert("查询产品代码出现异常！ ");
			return;
		}
		if (arrResult != null) {
			sCodeData = "0|"
			for ( var i = 0; i < arrResult.length; i++) {
				sCodeData += "^" + arrResult[i][0] + "|" + arrResult[i][1];
			}
			//alert(sCodeData);
			try {
				document.getElementsByName("ProductCode")[0].CodeData = sCodeData;
			} catch (ex) {
			}
		} else {
			try {
				alert("没有选出此险种的产品代码，请手工输入！");
				document.getElementsByName("ProductCode")[0].CodeData = "";
			} catch (ex) {
			}
		}
	}
}
/**
 * 查询地区代码
 */
function getComcode() {
	
		
	var QuerySQL, arrResult;
	QuerySQL = "select Code, CodeName from ldcode where codetype = 'station' and length(trim(code)) = 4 order by Code with ur";
	try {
		arrResult = easyExecSql(QuerySQL, 1, 0);
	} catch (ex) {
		alert("查询地区代码出现异常！ ");
		return;
	}
	sCodeData = "0|"
	for ( var i = 0; i < arrResult.length; i++) {
		sCodeData += "^" + arrResult[i][0] + "|" + arrResult[i][1];
	}
	// alert(sCodeData);
	try {
		document.getElementsByName("Comcode")[0].CodeData = sCodeData;
	} catch (ex) {
	}
}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    if(fm.hideOperate.value=="DEL||MAIN")content="删除成功!";
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
          //parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   // if(fm.hideOperate.value.length>0&&fm.hideOperate.value!="DEL||MAIN")
    queryClick();//提交后自动查询
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}

String.prototype.trim = function() {
	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}