var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if(verifyInput()==false){
		return false;
	}
  var DateSql = "select case when  TO_DATE('"+fm.EffectiveDate.value+"', 'YYYY-MM-DD') between current date and  current date + 1 month then '"+fm.EffectiveDate.value+"'   else '1' end   from dual ";
  var resultDate = easyExecSql(DateSql);
  if (resultDate!=null && resultDate==1) {
    alert("录入有效期不符合规定,请重新录入！");
    return false;
  }
  var strSql = "select count(1) from LCYBTCustomer where authorizeno='"+fm.AuthorizeNo.value+"'";
  var count = easyExecSql(strSql);
  if(count>0){
	  alert("已经存在的数据只能进行修改！");
	  return false;
  }
  
  if(!checkAllapp()){
	   return false;
	}

  fm.OperateType.value="INSERT";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = './YBTUserSave.jsp';
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    initForm();
  }
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
    alert("初始化页面错误，重置出错");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
      parent.fraMain.rows = "0,0,0,0,*";
  }
   else
   {
      parent.fraMain.rows = "0,0,0,0,*";
   }
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if(!fm.AuthorizeNo.value){
		  alert("请先选择一条记录！")
		  return;
	}
    if (confirm("您确实想修改该记录吗?"))
    {
    	if(!checkAllapp()){
    		return ;
    	}
        fm.OperateType.value = "UPDATE";
        var i = 0;
        var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        showSubmitFrame(mDebug);
        fm.action = './YBTUserSave.jsp';
        
        fm.submit(); //提交
    }
    else
    {
        fm.OperateType.value = "";
        alert("您取消了修改操作！");
    }
    
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  fm.OperateType.value="QUERY";
  window.open("./FrameYBTUserQuery.jsp");
}

function deleteClick()
{
  if(!fm.AuthorizeNo.value){
	  alert("请先选择一条记录！")
	  return;
  }
  if (confirm("您确实要删除该记录吗？"))
  {
    fm.OperateType.value = "DELETE";
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或连接其他的界面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    showSubmitFrame(mDebug);
    fm.action = './YBTUserSave.jsp';

    fm.submit();//提交
  }
  else
  {
    fm.OperateType.value = "";
    alert("您已经取消了删除操作！");
  }
}



function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function afterQueryUserInfo(tUserCode)
{
    var strSQL = "select authorizeno,name,sex,idtype,idno,effectivedate,birthday "
        + "from LCYBTCustomer where authorizeno = '" + tUserCode + "'";
    var arr = easyExecSql(strSQL);
    if(arr)
    {
        fm.AuthorizeNo.value = arr[0][0];
        fm.Name.value = arr[0][1];
        fm.Sex.value = arr[0][2];
        if(fm.Sex.value == "0")
            fm.SexName.value = "男";
        else if(fm.Sex.value == "1")
        	fm.SexName.value = "女";
        fm.IDType.value = arr[0][3];
        if(fm.IDType.value == "0")
        	fm.IDTypeName.value = "身份证";
        else
        	fm.IDTypeName.value = "其他";
        fm.IDNo.value = arr[0][4];
        fm.EffectiveDate.value = arr[0][5];
        fm.Birthday.value=arr[0][6];
       
    }
}

function checkAllapp(){
	   var idtype = fm.IDType.value;
	   var idno = fm.IDNo.value;
	   var sex = fm.Sex.value;
	   var birthday = fm.Birthday.value;
	   var checkResult = checkIdNo(idtype,idno,birthday,sex);
		if (checkResult != null && checkResult != "") {
			alert(checkResult);
			return false;
		}
	   return true;
}


