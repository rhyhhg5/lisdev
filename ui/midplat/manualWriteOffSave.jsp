<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.sinosoft.midplat.kernel.management.ManualWriteOffUI"%>

<%
	Logger mLogger = Logger.getLogger("ui.midplat.manualWriteOffSave_jsp");
	
	GlobalInput mGlobalInput = (GlobalInput) session.getValue("GI");
	
	String mContNo = request.getParameter("ContNo");
	mLogger.info("ContNo = " + mContNo);
	String mContent = null;
	try {
		ManualWriteOffUI tManualWriteOffUI = new ManualWriteOffUI(mContNo, mGlobalInput);
		tManualWriteOffUI.deal();
		mContent = "冲正成功！";
	} catch (Exception ex) {
		mLogger.error("补冲正(manualWriteOffSave.jsp)失败！", ex);
		
		mContent = "冲正失败！原因：" + ex.getMessage();
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=mContent%>");
</script>
</html>