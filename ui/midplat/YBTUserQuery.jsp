<html>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.bank.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="YBTUserQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="YBTUserQueryInit.jsp"%>
</head>

<body onload="initForm();" >
  <form action="./YBTUserQuerySave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 显示或隐藏LLReport1的信息 -->

    <div id="divLLReport1" style= "display: ''">
      <table class=common>
        <tr class=common>
                <td class=title>客户姓名</td>
                <td class=input><input class=common name=Name id=NameID></td>
                <td class=title>性别</td>
                <td class=input><input class=codeNo name=Sex verify="性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input class=codename name=SexName readonly=true></td>
            </tr>
            <tr class=common>
                <td class=title>证件类型</td>
                <td class=input><input class=codeno name=IDType CodeData="0|1^0|身份证^4|其他" ondblclick="return showCodeListEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('IDType',[this,IDTypeName],[0,1],null,null,null,1);"><input class=codename name=IDTypeName readonly=true ></td>
                <td class=title>证件号码</td>
                <td class=input><input class= common name=IDNo verify="证件号码|NUM&len<=20" ></td>
            </tr>
      </table>

      <input class=common name="CertifyClass" type="hidden">
      <input class=common name="State" type="hidden">
      <input class=common name="HaveNumber" type="hidden">
      <input class=common name="OperateType" type="hidden" >
      <input class=common name="ManageCom" type="hidden">
      <input class="cssButton" type=button value="查  询" onclick="submitForm()">
      <input class="cssButton" type=button value="返  回" onclick="ReturnData()">

      <table>
        <tr>
          <td class=common>
            <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
          </td>
          <td class=titleImg>清单列表</td>
        </tr>
      </table>
      <div id="divLLReport2" style= "display: ''">
        <table class=common>
          <tr class=common>
            <td text-align: left colSpan=1>
              <span id="spanYBTUserGrid" >
              </span>
            </td>
          </tr>
        </table>
        <div id="divPage" align=center style= "display: 'none' ">
          <input class=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
          <input class=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
          <input class=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
          <input class=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
        </div>
      </div>
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
  </body>
</html>
