<%

//ArchiveDailyBillInput.jsp

//程序功能：银保通归档日清单

//创建日期：2008-01-02 

//创建人  ：WangHaiBing

%>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<html> 

<head>	

<meta http-equiv="Content-Type" content="text/html; charset=GBK">

  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>

  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>

	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>

	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>



  <SCRIPT src="ArchiveDailyBill.js"></SCRIPT>

	<%@include file="ArchiveDailyBillInit.jsp"%>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>



  <title>银保通归档日清单</title>
</head>



<%

     //添加页面控件的初始化。

  GlobalInput tG =new GlobalInput();

     tG.Operator ="001";

     tG.ComCode = "8600";

     tG.ManageCom ="86";

    

	if( session.getValue("GI") == null ) {		

		session.putValue("GI", tG);

	}

//	System.out.println(String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom.length()));

	String temp =String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom);

%>

<body onload="initForm();initElementtype();">  

  <form action="./ArchiveDailyBillSave.jsp" method=post name=fm target="fraSubmit">

    <table class= common >

    	<tr>

				<td class= titleImg >请输入查询条件：</td>

			</tr>

    </table>

    <table  class= common >

	   	<tr  class= common>

				<td class=title>银行代码</td>

	      <td class=input>	      

	        <Input class=codeno name=BankCode ondblclick="return showCodeList('ybtbank',[this,BankName],[0,1]);"
	         onkeyup="return showCodeList('ybtbank',[this,BankName],null,null,['2'],['acctype'],[0,1]);"><input class=codename name=BankName readonly=true >

	      </td>

	    	<td  class= title>投保单号</td>

	    	<td class= input>

	    		<input class=common verify="投保单号|NotNull" name=PrtNo>

	    	</td>

	    	<td  class= title>保单印刷号</td>

	    	<td  class= input>

	       <input class=common verify="保单印刷号|NotNull" name=ProposalContNo>

	      </td>	

	    </tr>    
		<tr>
	     	<td  class= title>开始日期</td>

	      <td  class= input> <Input class= "coolDatePicker" dateFormat="short" name=StartTransDate elementtype=nacessary > </td>

	    	<td  class= title>结束日期</td>

	    	<td  class= input><Input class= "coolDatePicker" dateFormat="short" name=EndTransDate elementtype=nacessary > </td>
	    	
	    	 <td  class= title>保单类型</td>
	   		<td  class= input><input class='codeno' verify="保单类型|NotNull" name=ContFlag CodeData="0|^9|银保通柜面出单^c|网银出单^d|ATM出单" 
	      	ondblclick="return showCodeListEx('Flag',[this,ContFlagIn],[0,1]);" 
	      	onkeyup="return showCodeListKeyEx('Flag',[this,ContFlagIn],[0,1]);"><input class=codename name=ContFlagIn readonly=true>
	    	</tr>
	   
	   <tr>
	   		<td class=title>管理机构</td>

	      <td class=input>	      

	        <input name=ManageCom class=codeno   ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true>

	      </td>
	      
	     	<td  class= title>归档标志</td>

	      <td  class= input>
	      	<input class='codeno' verify="归档标志|NotNull" name=Flag CodeData="0|^0|否^1|是^2|全部" 
	      	ondblclick="return showCodeListEx('NodeType',[this,NodeType],[0,1]);" 
	      	onkeyup="return showCodeListKeyEx('NodeType',[this,NodeType],[0,1]);"><input class=codename name=NodeType readonly=true>
	      </td>
	      
	   </tr>
	    <td>

	    	<input type=hidden  name="Mng" value="<%=temp%>">

	    </td>

    </table>

    <table  class= common >

      <tr> 

				<td>

					<input class= cssbutton type=Button name="QueryButton" value="查  询" onclick="easyQueryClick();">

					<input class= cssbutton type=button name="ExportButton" disabled="disabled" value="导  出" onclick="NCExecel();">

				</td>			

			</tr>    

		</table>

<Div id="divIssuedoc3">

  <table class=common>

    <tr class=common>

      <td text-align:left colSpan=1>

        <span id="spanPremQueryGrid3">        

        	</span>

      </td>

    </tr>

  </table>

</div>



<center>      

      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 

      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">           

      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 

      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">      

</center> 

<input type=hidden id="fmtransact" name="fmtransact">

  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>

</html>

