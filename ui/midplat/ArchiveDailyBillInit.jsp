<script language="JavaScript">

function initInpBox()
{
  try
  {
  	fm.all('BankCode').value = '';
    fm.all('ManageCom').value = '';
    fm.all('PrtNo').value = '';
    fm.all('ProposalContNo').value = '';
    fm.all('StartTransDate').value = '';
    fm.all('EndTransDate').value = '';
    fm.all('Flag').value = '';
  }
  catch(ex)
  {
    alert("在ArchiveDailyBillInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initForm() 
{	
	initInpBox();
	initPremQueryGrid3();
}


function initPremQueryGrid3()
{

  var iArray = new Array();

  try {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         		//列名
	  iArray[0][1]="25px";
	  iArray[0][2]=80;                		
	  iArray[0][3]=0;
	  
	  iArray[1]=new Array();
    iArray[1][0]="管理机构名称";         		//列名
    iArray[1][1]="150px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;
	//update by zzm 2015-09-16
    iArray[2]=new Array();
	iArray[2][0]="机构代码";         		//列名
	iArray[2][1]="60px";
	iArray[2][2]=40;         		
	iArray[2][3]=0;
	  
	iArray[3]=new Array();
    iArray[3][0]="银行机构名称";         		//列名
    iArray[3][1]="150px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;
		
	iArray[4]=new Array();
    iArray[4][0]="银行网点代码";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;  
    
    iArray[5]=new Array();
    iArray[5][0]="公司网点代码";         		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;  
    
    iArray[6]=new Array();
    iArray[6][0]="客户经理";         		//列名
    iArray[6][1]="60px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0; 
    
    iArray[7]=new Array();
    iArray[7][0]="银行柜员代码";         		//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0; 
    			  	  
	  iArray[8]=new Array();
	  iArray[8][0]="投保单号";         		//列名
	  iArray[8][1]="60px";    
	  iArray[8][2]=40;      		
	  iArray[8][3]=0;  
	  
	  iArray[9]=new Array();
	  iArray[9][0]="保单号";         		//列名
	  iArray[9][1]="50px";
	  iArray[9][2]=80;	           		
	  iArray[9][3]=0;    
	          
	  iArray[10]=new Array();
	  iArray[10][0]="投保人";         	  //列名
	  iArray[10][1]="40px";            	//列宽
	  iArray[10][2]=80;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
		iArray[11]=new Array();
		iArray[11][0]="承保日期";
		iArray[11][1]="60px";
		iArray[11][2]=100;
		iArray[11][3]=0;
		
		iArray[12]=new Array();
		iArray[12][0]="险种名称";         	//列名    
		iArray[12][1]="160px";            	//列宽
		iArray[12][2]=60;                   //列最大值
		iArray[12][3]=0;              		//是否允许输入,1表示允许，0表示不允许
		
		iArray[13]=new Array();
		iArray[13][0]="保费收入";         	//列名
		iArray[13][1]="60px";            	//列宽
		iArray[13][2]=60;                   //列最大值
		iArray[13][3]=0;              		//是否允许输入,1表示允许，0表示不允许
		
 	  iArray[14]=new Array();
	  iArray[14][0]="归档日期";         		//列名
	  iArray[14][1]="60px";
	  iArray[14][2]=40;         		
	  iArray[14][3]=0;
	  
	  iArray[15]=new Array();
	  iArray[15][0]="归档情况";         		//列名
	  iArray[15][1]="60px";
	  iArray[15][2]=40;         		
	  iArray[15][3]=0;
	  
	  iArray[16]=new Array();
	  iArray[16][0]="银行代码";         		//列名
	  iArray[16][1]="60px";
	  iArray[16][2]=40;         		
	  iArray[16][3]=0;
	  
	  iArray[17]=new Array();
	  iArray[17][0]="保单类型";         		//列名
	  iArray[17][1]="60px";
	  iArray[17][2]=40;         		
	  iArray[17][3]=0;
	  
	  iArray[18]=new Array();
	  iArray[18][0]="归档时效(天)";         		//列名
	  iArray[18][1]="100px";
	  iArray[18][2]=40;         		
	  iArray[18][3]=0;
    		
		PremQueryGrid3 = new MulLineEnter("fm", "PremQueryGrid3");
		PremQueryGrid3.mulLineCount = 6;
		PremQueryGrid3.displayTitle = 1;
		PremQueryGrid3.locked = 1;
		PremQueryGrid3.canSel = 0;
		PremQueryGrid3.canChk = 0;
		PremQueryGrid3.hiddenSubtraction = 1;
		PremQueryGrid3.hiddenPlus = 1;
		PremQueryGrid3.loadMulLine(iArray);
  } catch(ex) {
    alert(ex);
  }
}
</script>
