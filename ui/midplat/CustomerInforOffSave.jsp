<%@page import="com.sinosoft.midplat.kernel.util.CustomerInforUI"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：CustomerInforOffSave.jsp
//程序功能：
//创建日期：2019-03-18 11:10:36
//创建人  ：zxs
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Succ";
	String Content = "";

	try
	{
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
		String prtno = request.getParameter("PrtNo");
		CustomerInforUI customerInforUI = new CustomerInforUI();
		if (!customerInforUI.submitData(prtno)){
			Content = "保单客户信息回销失败，原因是: " + customerInforUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		Content = "核销成功！";
	   	FlagStr = "Succ";	
	  
	} // end of try
	catch ( Exception e1 )
	{
	 e1.printStackTrace();
		Content = "保单客户信息回销失败，原因是: " + e1.toString();
		FlagStr = "Fail";
	}
%>                      
<html>
<script language="javascript">
    
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");


</script>
</html>
