
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput) session.getValue("GI");
	String strOperator = globalInput.Operator;
	String strCurTime = PubFun.getCurrentDate();
	String strCom = globalInput.ComCode;


%>

<script language="JavaScript">
//返回按钮初始化
var str = "";
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('BankCode').value = '';
    fm.all('BankCodeName').value = '';
    fm.all('ComCode').value = '';
    fm.all('ComCodeName').value = '';
    fm.all('RiskCode').value = '';
    fm.all('RiskName').value = '';
    fm.all('Status').value = '';
    fm.all('StatusName').value = '';
    fm.all('Channel').value = '';
    fm.all('ChannelName').value = '';
    fm.all('StartDate').value = '';
    fm.all('Operator').value = '<%= strOperator %>';
    fm.all('ID').value = '';
    
  }
  catch(ex)
  {
    alert("在ybtStopSaleInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在ybtStopSaleInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initybtBQPolGrid();
	//initybtStopSaleGrid();
  }
  catch(re)
  {
    alert("ybtStopSaleInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var ybtPolGrid;
// 保单信息列表的初始化
function initybtBQPolGrid() {                               
	var iArray = new Array();
	try {
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="ID";         		//列名
		iArray[1][1]="0px";            		//列宽
		iArray[1][2]=10;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="银行代码";         			//列名
		iArray[2][1]="50px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="机构代码";         			//列名
		iArray[3][1]="50px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="产品代码";         			//列名
		iArray[4][1]="50px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="渠道";         		//列名
		iArray[5][1]="50px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		       
		iArray[6]=new Array();
		iArray[6][0]="保全状态";         		//列名
		iArray[6][1]="50px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="操作员";         		//列名
		iArray[7][1]="50px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="生效日期";         		//列名
		iArray[8][1]="50px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		ybtBQPolGrid = new MulLineEnter( "fm" , "ybtBQPolGrid" ); 
		//这些属性必须在loadMulLine前
		ybtBQPolGrid.mulLineCount = 10;   
		ybtBQPolGrid.displayTitle = 1;
		ybtBQPolGrid.locked = 1;
		ybtBQPolGrid.canSel = 1;
		ybtBQPolGrid.hiddenPlus = 1;
		ybtBQPolGrid.hiddenSubtraction = 1;
		ybtBQPolGrid.loadMulLine(iArray); 
		
		//这些操作必须在loadMulLine后面
		//divLCPol1.setRowColData(7,7,"","divLCPol1"); 
	}
	catch(ex) { alert(ex); }
}

</script>
