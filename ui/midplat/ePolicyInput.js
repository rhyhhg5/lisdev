var turnPage = new turnPageClass();

// 查询按钮
var showInfo;
window.onfocus = myonfocus;// 使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) 
	{
		try {
			showInfo.focus();
		} 
		catch (ex) 
		{
			showInfo = null;
		}
	}
}
function easyQueryClick() {
	var BankCode = fm.all('BankCode').value;

	var ManageCom = fm.all('ManageCom').value;
	var TransDate = fm.all('TransDate').value;
	var ContNo = fm.all('ContNo').value;
	var ContFlag = fm.all('ContFlag').value;
	var strSQL = '';
	var strsql = '';
	
	if((null==BankCode) || (""==BankCode)) {
	   alert("请选择银行代码！");
	   return false;
	}
	if((null==ManageCom) || (""==ManageCom)) {
	   alert("请选择管理机构！");
	   return false;
	}
	
	if((null==TransDate) || (""==TransDate)) {
		alert("请输入查询日期！");
		return false;
	}
	
//	 add by  mxk  2016-01-14   全部  (03)
	if((null ==ContFlag || ""==ContFlag) || (null != ContFlag && "3"==ContFlag)){
		strSQL = "select lc.appntName,(select codename from  ldcode where  codetype ='banknum' and code ='"+ BankCode+"'),"
        +"lc.contNo,lc.signdate,lc.appntidno,ls.email," 
        +"(select name  from lacom where  agentcom=lc.agentcom)," 
        +"case  lr.temp1 when 0 then '未生成'   when   1 then '已发送 '  when  2  then '未发送 ' when  3  then '全部 ' else '其他状态' end  From lktransstatus lk,lccont lc "
        +"left join lcappnt lp on lc.contno = lp.contno " 
        +"left join lcaddress ls on ls.customerno = lp.appntno left join LKElectronCont lr on lr.contno = lc.contno where "
		+"  lk.funcflag = '01' " 
		+"and lk.riskcode in ('JZJ001','333501','JZJ002','JZJ003','JZJ004','333701','XBT009','XBT010','XBT011','XBT012','XBT013','XBT014','340401','334901','232401','335301','730301') " 
		+"and lp.addressno = ls.addressno "
		+"and lr.contno = lc.contno "
		+"and lk.rcode = '1'  " 
		+"and lk.Prtno = lk.Proposalno  "
		+"and lk.status = '1' "
		+"and lk.state_code = '03' "
		+"and lk.rmpvskernel = '00' "
		+"and lk.resultbalance = '0' "
		+"and lc.contno =lk.polno  "
		+"and lc.cardflag in ('c','d','a','9','e') "
		+"and lk.bankcode ='"+BankCode+"' "
		+"and lc.managecom like '"+ManageCom+"%'"   //"and lc.managecom = '86530100' "
		+"and lc.makedate  = '"+TransDate+"'";  
		if((null!=ContNo) && (""!=ContNo)) {
			strSQL +=(" and lc.contno = '"+ContNo+"' ");
		}
		strSQL += " fetch  first 200 rows only";
	}
	else
	{
		strSQL = "select lc.appntName,(select codename from  ldcode where  codetype ='banknum' and code ='"+ BankCode+"'),"
        +"lc.contNo,lc.signdate,lc.appntidno,ls.email," 
        +"(select name  from lacom where  agentcom=lc.agentcom)," 
        +"case  lr.temp1 when 0 then '未生成'   when   1 then '已发送 '  when  2  then '未发送 ' when  3  then '全部 ' else '其他状态' end  From lktransstatus lk,lccont lc "
        +"left join lcappnt lp on lc.contno = lp.contno " 
        +"left join lcaddress ls on ls.customerno = lp.appntno left join LKElectronCont lr on lr.contno = lc.contno where "
		+"  lk.funcflag = '01' " 
		+"and lk.riskcode in ('JZJ001','333501','JZJ002','JZJ003','JZJ004','333701','XBT009','XBT010','XBT011','XBT012','XBT013','XBT014','340401','334901','232401','335301','730301') " 
		+"and lp.addressno = ls.addressno "
		+"and lr.contno = lc.contno "
		+"and lk.rcode = '1'  " 
		+"and lk.Prtno = lk.Proposalno  "
		+"and lk.status = '1' "
		+"and lk.state_code = '03' "
		+"and lk.rmpvskernel = '00' "
		+"and lk.resultbalance = '0' "
		+"and lc.contno =lk.polno  "
		+"and lc.cardflag in ('c','d','a','9','e') "
		+"and lk.bankcode ='"+BankCode+"' "
		+"and lc.managecom like '"+ManageCom+"%'"   //"and lc.managecom = '86530100' "
		+"and lc.makedate  = '"+TransDate+"'";  
		if((null!=ContNo) && (""!=ContNo)) {
			strSQL +=(" and lc.contno = '"+ContNo+"' ");
		}
		strSQL +="and lr.temp1 = '"+ContFlag+"'";
		
		strSQL += " fetch  first 200 rows only";
	}
	turnPage.queryModal(strSQL, YBTPolGrid, 1, 1);
	fm.epolicySend.disabled="";  //将电子保单发送按钮置为可用状态
}

function  sendPolicy() {
	if(0 == YBTPolGrid.getSelNo()){
		alert('请选择一张需发送电子保单的保单号!');
		return false;
	}
	var ContNo = YBTPolGrid.getRowColData(YBTPolGrid.getSelNo()-1,3);
	if( null == ContNo || "" ==ContNo){
		alert("请先查询,然后再发送电子保单!");
		return false;
	}
	
	var showStr = "后台数据正在处理,请勿重复提交!";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:220px");
	var  urlofContNo = "./ePolicySave.jsp?ContNo1="+ContNo;
	fm.action = urlofContNo;
	fm.epolicySend.disabled="disabled";//导出一次后将按钮置灰
	fm.submit(); //提交
}

//add by zengzm 2017-03-01 针对电子保单补生成 
function  reCreateEpolicy() {
	
	if(0 == YBTPolGrid.getSelNo()){
		alert('请选择一张需生成电子保单的保单号!');
		return false;
	}
	var ContNo = YBTPolGrid.getRowColData(YBTPolGrid.getSelNo()-1,3);
	var SignDate = YBTPolGrid.getRowColData(YBTPolGrid.getSelNo()-1,4);
	if( null == ContNo || "" ==ContNo){
		alert("请先查询,然后再生成电子保单!");
		return false;
	}
	
	var showStr = "后台数据正在处理,请勿重复提交!";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:220px");
	var  urlofContNo = "./reCreateEpolicy.jsp?ContNo1="+ContNo+"&SignDate="+SignDate;
	fm.action = urlofContNo;
	fm.epolicySend.disabled="disabled";//导出一次后将按钮置灰
	fm.submit(); //提交
}

function afterSubmit(FlagStr, Content,ContNO3) {
	showInfo.close();
	var FlagDel = FlagStr;
	var info = Content;
	if (FlagStr == "发送失败" ) {
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content;
		 showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "操作成功!";
		showInfo =  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		easyQueryClick();
	}
}