var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var arrDataSet;

  var turnPage = new turnPageClass();

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
    initForm();
  }
  catch(re)
  {
    alert("在LAComInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
// 查询按钮
function easyQueryClick()
{
    
	// 初始化表格
	initybtPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var ContType = fm.ContType.value
	var AgentCom = fm.AgentCom.value
	if(ContType==null ||ContType =="")
	{
	    alert("请选择保单类别");
	    return false;
	}
	if(ContType == '1')
	{
	strSQL ="select prtno, proposalcontno, contno, signdate, cvalidate, prem "
          + ", (select max(amnt) from lcpol where contNo=a.contNo) "          
          + ", (select codename from ldcode where codetype='ybtbank' and code=a.bankcode) "
          + "from lccont a where salechnl = '04' and cardFlag='9' And appflag = '1' and bankcode like '"
          + AgentCom + "%'"
//          + getWherePart("bankcode", "AgentCom")
          + getWherePart("signdate", "StartDate", ">=")
          + getWherePart("signdate", "StartDate", ">=")
          + getWherePart("signdate", "EndDate", "<=")
          + getWherePart("signdate", "EndDate", "<=")
          + "order by prtno with ur";
/*     if(fm.all('AgentCom').value != null&&fm.all('AgentCom').value != "")
     {
     	strSQL += " and a.bankcode = '"+fm.all('AgentCom').value+"'"	
     	}    
     if(fm.all('StartDate').value != null&&fm.all('StartDate').value != "")
     {
     strSQL += " and a.signdate>='"+fm.all('StartDate').value+"'"	
     }
       
     if(fm.all('EndDate').value != null&& fm.all('EndDate').value != "")
     {
     	strSQL += " and a.signdate<='"+fm.all('EndDate').value+"'"
    }
     strSQL += " order by a.proposalcontno";
         */
    }


    if(ContType=='0')
    {
    	strSQL ="select prtno, proposalcontno, contno, signdate, cvalidate, prem "
          + ", (select max(amnt) from lcpol where contNo=a.contNo) "          
          + ", (select codename from ldcode where codetype='ybtbank' and code=a.bankcode) "
          + "from lccont a where salechnl = '04' and cardFlag='9' And appflag = '1' and bankcode like '"
          + AgentCom + "%'"
//          + getWherePart("bankcode", "AgentCom")
          + getWherePart("signdate", "StartDate", ">=")
          + getWherePart("signdate", "StartDate", ">=")
          + getWherePart("signdate", "EndDate", "<=")
          + getWherePart("signdate", "EndDate", "<=")
          + " union "
          + "select prtno, proposalcontno, contno, signdate, cvalidate, prem "
          + ", (select max(amnt) from lbpol where contNo=a.contNo) "          
          + ", (select codename from ldcode where codetype='ybtbank' and code=a.bankcode) "
          + "from lbcont a where salechnl = '04' and cardFlag='9' And appflag = '1' and bankcode like '"
          + AgentCom + "%'"
//          + getWherePart("bankcode", "AgentCom")
          + getWherePart("signdate", "StartDate", ">=")
          + getWherePart("signdate", "StartDate", ">=")
          + getWherePart("signdate", "EndDate", "<=")
          + getWherePart("signdate", "EndDate", "<=")
          + "order by prtno with ur";
/*     if(fm.all('bankcode').value != null&&fm.all('AgentCom').value != "")
     {
     	strSQL += " and a.bankcode = '"+fm.all('AgentCom').value+"'"	
     	}    
     if(fm.all('StartDate').value != null&&fm.all('StartDate').value != "")
     {
     strSQL += " and a.signdate>='"+fm.all('StartDate').value+"'"	
     }
       
     if(fm.all('EndDate').value != null&& fm.all('EndDate').value != "")
     {
     	strSQL += " and a.signdate<='"+fm.all('EndDate').value+"'"
    }

    strSQL += " union ";
    strSQL +="select distinct a.prtno,a.proposalcontno,a.contno,a.signdate,a.cvalidate,a.prem,a.amnt,b.codename"
          + " from lbcont a,ldcode b where a.salechnl = '04' and a.cardFlag='9' And a.appflag = '1'"          
          + " and a.bankcode = b.code and b.codetype = 'ybtbank' and b.codetype = 'ybtbank'"
     if(fm.all('AgentCom').value != null&&fm.all('AgentCom').value != "")
     {
     	strSQL += " and a.bankcode = '"+fm.all('AgentCom').value+"'"	
     	}    
     if(fm.all('StartDate').value != null&&fm.all('StartDate').value != "")
     {
     strSQL += " and a.signdate>='"+fm.all('StartDate').value+"'"	
     }
       
     if(fm.all('EndDate').value != null&& fm.all('EndDate').value != "")
     {
     	strSQL += " and a.signdate<='"+fm.all('EndDate').value+"'"
    }
     strSQL += " order by proposalcontno";
         */
    }
       
     turnPage.queryModal(strSQL, ybtPolGrid);
     
   }