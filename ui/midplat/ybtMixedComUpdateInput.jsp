<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LaratecommisionSetInput.jsp
//程序功能：佣金基数表设置录入界面
//创建时间：2006-08-22
//创建人  ：miaoxz

%>
<%
  	GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
		String Managecom2 = tG.ManageCom;
		System.out.println("tManageCom"+Managecom2);
%>
  <script>
 var strsql =" 1 and  comcode Like #"+'<%=Managecom2%>'+"%# " ;

</script>
<%@page contentType="text/html;charset=GBK" %>
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
     <SCRIPT src="ybtMixedComUpdateInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 

   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
      <%@include file="ybtMixedComUpdateInit.jsp"%>

</head>　
　
<body  onload="initForm();initElementtype();" >
 <form action="./ybtMixedComUpdateSave.jsp" method=post name=fm target="fraSubmit">
  
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
   <tr class=common>
   	<TD  class= title>管理机构</TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom     verify="管理机构|code:comcode&NOTNULL"
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,strsql,null,1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,strsql,null,1);"><input class=codename name=ManageComName readonly=true   elementtype=nacessary>
          </TD> 
           <td  class= title> 级别 </td>
         <td  class= input>
		      <input name=BankType2 class=codeno verify="级别|code:BankType2"  
		             ondblClick="showCodeList('BankType2',[this,BankType2Name],[0,1])" 
		             onkeyup="showCodeListKey('BankType2',[this,BankType2Name],[0,1])" ><Input class=codename name=BankType2Name readOnly >        				     
		     </td>
         <td  class= title> 银行机构 </td>
        <td  class= input><input class=codeno name=AgentCom verify="银行机构编码"
         ondblclick="return getAgentCom(this,AgentComName);"
         onkeyup="return getAgentCom(this,AgentComName);"><input class=codename readonly  name=AgentComName > </td>
      </TR> 
       <tr>
            <td colspan="6"><font color="black">如果是交叉销售,请选择</font><INPUT TYPE="checkbox" NAME="MixComFlag" onclick="isMixCom();"></td>
        </tr>
       <%@include file="../sys/MixedSalesAgent.jsp"%>
       
      <!--     
    <tr class=common>	
    	  
          <td  class= title> 交叉销售业务类型 </td>
          <TD  class= input> 
          <Input class="codeno" name=Crs_BussType 	ondblclick="return showCodeList('JCSaleType',[this,Crs_BussTypeName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('JCSaleType',[this,Crs_BussTypeName],[0,1],null,null,null,1,null,1);"><input class=codename name=Crs_BussTypeName readonly=true >
          </TD>
   		  <td  class= title>对方代理机构编码</td>
          <TD  class= input> <Input class= "common" name=GrpAgentCom onchange="return changeGrpAgentCom();"></TD>
          <td  class= title>对方代理机构名称</td>
          <TD  class= input> <Input class= "readonly" name=GrpAgentComName readonly></TD>
          
  </tr>
  <tr class=common>		
          <td  class= title>对方业务员编码</td>
          <TD  class= input> <Input class= "common" name=GrpAgentCode></TD>
   		  <td  class= title>对方业务员姓名</td>
          <TD  class= input> <Input class= "common" name=GrpAgentName></TD>
          <td  class= title>对方业务员身份证</td>
          <TD  class= input> <Input class= "common" name=GrpAgentIDNo></TD>       		
  </tr>
  --> 
  <tr class=common>	
  		<!-- 	
          <td  class= title>交叉销售渠道</td>
          <TD  class= input>
          <Input class="codeno" name=Crs_SaleChnl ondblclick="return showCodeList('JCSaleChnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('JCSaleChnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1,null,1);"><input class=codename name=Crs_SaleChnlName readonly=true >
          </TD>
           -->
   		  <td  class= title>失效日期</td>
          <TD  class= input><Input class='coolDatePicker' name=InvalidDate dateFormat='short' > </TD>       		
  </tr>
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();"> 
     		<input type=button value="修  改" class=cssButton onclick="return DoUpdate();">  
    	</td>
   
    </tr>      
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
   <!--table class=common>
    <tr class=common>
     <td class=input colspan=4 align=center><input type=button value="增     加" class=common onclick="addOneRow();">
     </td>
    </tr>
   </table-->      
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>  
  <input type=hidden id="State" name="State">  
  <input type=hidden id="fmAction" name="fmAction"> 
  <input type=hidden id="OldBandCode" name="OldBandCode">
  <input type=hidden id="OldBankNode" name="OldBankNode">
  <input type=hidden id="OldZoneNo" name="OldZoneNo"> 

 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




