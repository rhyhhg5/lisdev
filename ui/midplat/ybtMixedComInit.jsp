<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2006-08-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
 var  mSQL=" 1 and  char(length(trim(comcode))) = #8# and sign=#1#" ;
  </SCRIPT>       


<script language="JavaScript">
function initInpBox()
{
  try
  {
  	//alert('aaa');
  	fm.all('ManageCom').value='';
    fm.all('ManageComName').value='';
    fm.all('BankType2').value='';
    fm.all('BankType2Name').value='';
    fm.all('AgentCom').value='';
    fm.all('AgentComName').value='';
    fm.all('Crs_SaleChnl').value='01';
    fm.all('Crs_BussType').value='03';
    fm.all('Crs_SaleChnlName').value='财代健';
    fm.all('Crs_BussTypeName').value='渠道共享';
    fm.all('GrpAgentCom').value='';
    fm.all('GrpAgentComName').value='';
    fm.all('GrpAgentCode').value='';
    fm.all('GrpAgentName').value='';
    fm.all('GrpAgentIDNo').value='';
    fm.all('InvalidDate').value='';
    fm.all('State').value='';   
    
    fm.all('OldBandCode').value='';
    fm.all('OldBankNode').value='';
    fm.all('OldZoneNo').value='';
          
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    iArray[1]=new Array();
	  iArray[1][0]="管理机构编码";          		//列名
	  iArray[1][1]="60px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[2]=new Array();
	  iArray[2][0]="管理机构名称";          		//列名
	  iArray[2][1]="80px";      	      		//列宽
	  iArray[2][2]=20;            			//列最大值
	  iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[3]=new Array();
	  iArray[3][0]="级别编码";          		//列名
	  iArray[3][1]="60px";      	      		//列宽
	  iArray[3][2]=20;            			//列最大值
	  iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  
	  iArray[4]=new Array();
	  iArray[4][0]="级别名称";          		//列名
	  iArray[4][1]="80px";      	      		//列宽
	  iArray[4][2]=20;            			//列最大值
	  iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[5]=new Array();
	  iArray[5][0]="银行代码";          		//列名
	  iArray[5][1]="60px";      	      		//列宽
	  iArray[5][2]=20;            			//列最大值
	  iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	  
    
    iArray[6]=new Array();
    iArray[6][0]="银行名称"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[7]=new Array();
    iArray[7][0]="交叉销售渠道"; //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许        

    
    iArray[8]=new Array();
    iArray[8][0]="销售业务类型"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    
       
    iArray[9]=new Array();
    iArray[9][0]="代理机构"; //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许

        
    iArray[10]=new Array();
    iArray[10][0]="代理人编码"; //列名
    iArray[10][1]="60px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=0;              //是否允许输入,1表示允许,0表示不允许 
    

    
    iArray[11]=new Array();
    iArray[11][0]="代理人姓名"; //列名
    iArray[11][1]="60px";        //列宽
    iArray[11][2]=100;            //列最大值
    iArray[11][3]=0;              //是否允许输入,1表示允许,0表示不允许    
   
      
    iArray[12]=new Array();
    iArray[12][0]="代理人身份证"; //列名
    iArray[12][1]="60px";        //列宽
    iArray[12][2]=100;            //列最大值
    iArray[12][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[13]=new Array();
    iArray[13][0]="失效日期"; //列名
    iArray[13][1]="80px";        //列宽
    iArray[13][2]=100;            //列最大值
    iArray[13][3]=0;              //是否允许输入,1表示允许,0表示不允许  
    
    iArray[14]=new Array();
    iArray[14][0]="状态"; //列名
    iArray[14][1]="0px";        //列宽
    iArray[14][2]=100;            //列最大值
    iArray[14][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[15]=new Array();
    iArray[15][0]="状态"; //列名
    iArray[15][1]="60px";        //列宽
    iArray[15][2]=100;            //列最大值
    iArray[15][3]=0;              //是否允许输入,1表示允许,0表示不允许  
    
    iArray[16]=new Array();
    iArray[16][0]="销售业务类型名字"; //列名
    iArray[16][1]="0px";        //列宽
    iArray[16][2]=100;            //列最大值
    iArray[16][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[17]=new Array();
    iArray[17][0]="集团交叉销售渠道名字"; //列名
    iArray[17][1]="0px";        //列宽
    iArray[17][2]=100;            //列最大值
    iArray[17][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[18]=new Array();
    iArray[18][0]="集团交叉销售渠道名字"; //列名
    iArray[18][1]="0px";        //列宽
    iArray[18][2]=100;            //列最大值
    iArray[18][3]=0;              //是否允许输入,1表示允许,0表示不允许    
	  
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canSel = 1;
  SetGrid.mulLineCount = 0;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenSubtraction =1;
    SetGrid.hiddenPlus=1;
    SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray); 
    SetGrid.selBoxEventFuncName = "queryGrpHistoryDetail";   
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    
  }
  catch(re)
  {
    alert("在LaratecommisionSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>