<%@page contentType="text/html;charset=gb2312" %>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>

<%
    //在此设置导出Excel的列名，应与sql语句取出的域相对应

    ExportExcel.Format format = new ExportExcel.Format();
    ArrayList listCell = new ArrayList();
    ArrayList listLB = new ArrayList();
    ArrayList listColWidth = new ArrayList();
    format.mListCell=listCell;
    format.mListBL=listLB;
    format.mListColWidth=listColWidth;

    ExportExcel.Cell tCell=null;
    ExportExcel.ListBlock tLB=null;

       //管理机构
       String BankCode = request.getParameter("BankCode");
       String BankBranch = request.getParameter("BankBranch");
       System.out.println("地区代码=============="+BankBranch);
       //交费开始日期
       String BankNode = request.getParameter("BankNode");
       System.out.println("网点代码=============="+BankNode);
       //交费结束日期
       String StartTransDate = request.getParameter("StartTransDate");
       System.out.println("交费开始日期==============="+StartTransDate);       
       String EndTransDate = request.getParameter("EndTransDate");

       

    listColWidth.add(new String[]{"0","5000"});
    
    String sql = "select e.name, b.bankbranch, a.agentcom, f.name, b.bankoperator, a.prtno, a.contno, a.appntname, a.PolApplyDate, c.riskcode, d.riskname, case when a.payintv = 0 then '趸交' else '期交' end,c.prem, b.transno "
		+ "from lccont a,lktransstatus b,lcpol c,lmrisk d,ldcom e,laagent f "
		+ "where b.funcflag='01' and b.polno=a.contno and c.contno=a.contno and c.riskcode=d.riskcode and a.managecom=e.comcode and a.agentcode=f.agentcode "
		+ "and a.appflag='1' and a.PolApplyDate between '" + StartTransDate + "' and '" + EndTransDate + "' "
		+ "and b.bankcode='" + BankCode + "' ";
    if ((null!=BankBranch) && (!BankBranch.equals(""))) {
    	sql += "and b.bankbranch='" + BankBranch + "' ";
    }
    if ((null!=BankNode) && (!BankNode.equals(""))) {
    	sql += "and b.banknode='" + BankNode + "' ";
    }
	//后面加上with ur,解决锁表问题.
	sql += "order by a.managecom with ur";
        
    System.out.println("新契约承保明细sql："+sql);
    tLB = new ExportExcel.ListBlock("001");
    tLB.colName=new String[]{"银行机构名称","银行网点代码","公司网点代码","客户经理","银行柜员代码","投保单号","保单号","投保人","承保日期","险种代码","险种名称","缴费方式","保费收入","交易流水号"};
    tLB.sql=sql;
    tLB.row1=0;
    tLB.col1=0;
    tLB.InitData();
    listLB.add(tLB);

    try{
	  //将输出流置空,否则会因为流的冲突造成websphere报错.
	  out.clear(); 
	  out = pageContext.pushBody();
	  	
      response.reset();
      response.setContentType("application/octet-stream");
      response.setHeader("Content-Disposition","attachment; filename=YBT_"+StartTransDate+"_"+EndTransDate+"_List.xls");
      OutputStream outOS=response.getOutputStream();

      BufferedOutputStream bos=new BufferedOutputStream(outOS);

      ExportExcel excel = new ExportExcel();

      excel.write(format,bos);

      bos.flush();
      bos.close();
 
    }
    catch(Exception e){
      System.out.println("导出Excel失败！");
    };

%>
