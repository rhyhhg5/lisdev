<%
//tertianBalanceInput.jsp
//程序功能：页面补对账
//创建日期：2008-04-24
//创建人  ：Wanghb
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>	
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

  <SCRIPT src="ePolicyInput.js"></SCRIPT>
	<%@include file="ePolicyInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <title>页面补对账</title>
</head>

<%
     //添加页面控件的初始化。
  GlobalInput tG =new GlobalInput();
     tG.Operator ="001";
     tG.ComCode = "8600";
     tG.ManageCom ="86";
    
	if( session.getValue("GI") == null ) {		
		session.putValue("GI", tG);
	}
//	System.out.println(String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom.length()));
	String temp =String.valueOf(((GlobalInput)session.getValue("GI")).ManageCom);
%>

<body onload="initForm();">  
  <form action="" method=post name=fm target="fraSubmit">
    <table class= common >
    	<tr>
				<td class= titleImg >请输入查询条件：</td>
			</tr>
    </table>
    <table  class= common >  
    	<tr  class= common>   		
          <td class=title>银行代码</td>
	      <td class=input>
	     		 <input class=codeno  verify="银行代码|NotNull" name=BankCode ondblclick="return showCodeList('ybtbank',[this,BankCodeName],[0,1],null,null,null,1,null,1);" onkeyup="return showCodeListKey('ybtbank',[this,BankCodeName],[0,1],null,null,null,1,null,1);"><input class=codename  name=BankCodeName readonly=true elementtype=nacessary>	   
	      </td>
	      <td  class= title>管理机构</td>
	      <td  class= input><input class=common verify="管理机构|NotNull" name=manageCom value="<%= temp%>" readonly="readonly"> </td>
	        <td  class= title>查询日期</td>
	      <td  class= input> <Input class= "coolDatePicker" dateFormat="short" name=TransDate elementtype=nacessary > </td>
	      <td  class= title>保单号</td>
	      <td  class= input><input class=common verify="保单号|NotNull" name=ContNo> </td>
	     
       </tr>  
       <tr class= common>
       		<td class=title>电子保单状态</td>
	       	<td  class= input><input class='codeno' verify="电子保单状态|NotNull" name=ContFlag CodeData="0|^0|未生成^1|已发送^2|未发送^3|全部" 
	      	ondblclick="return showCodeListEx('Flag',[this,ContFlag1],[0,1]);" 
	      	onkeyup="return showCodeListKeyEx('Flag',[this,ContFlag1],[0,1]);"><input class=codename name=ContFlag1 readonly=true>
	      	</td>
       </tr>
       
	    <td>
	    	<input type=hidden  name="Mng" value="<%=temp%>">    			
	    </td>      
    </table>
    
    <table  class= common >
      <tr> 
				<td>
					<input class= cssbutton name="epolicyCont" type=Button value="电子保单查询" onclick="return easyQueryClick();">
				</td>			
			</tr>    
		</table>
		
			<Div id="divIssuedoc3">   <!-- 展示数据 -->
			  <table class=common>
			    <tr class=common>
			      <td text-align:left colSpan=1>
			        <span id="spanYBTPolGrid1">        
			        	</span>
			      </td>
			    </tr>
			  </table>
			</div>	
		
		<table class= common>
			<tr>
				<td class=title>新邮箱地址 &nbsp;&nbsp;<input class=common verify="新邮箱地址|NotNull" name=MailAddress></td><td class=title style="color: red;">*:如录入新邮箱地址,即按照新邮箱地址发送邮件,否则按客户投保邮箱发送!</td>
			</tr>
		</table>
		
		  <table  class= common >
               <tr> 
				<td class=title><input name="epolicySend" class= cssbutton type=Button value="电子保单发送" onclick="return sendPolicy();"></td><td class=title style="color: red;">*:银保通发送电子保单请求时间较长,请在弹出成功之后再继续发送!</td>
			</tr>
               <tr> 
				<td class=title><input name="epolicyReCreate" class= cssbutton type=Button value="补生成电子保单" onclick="return reCreateEpolicy();"></td><td class=title style="color: red;">*:银保通重新生成电子保单请求时间较长,请在弹出成功之后再继续发送!</td>
			</tr>    
		</table>	
		
		<hr>
		<center>      

      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 

      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">           

      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 

      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">      

</center> 			
		
<input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
