<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ExcelReportYWImportSave.jsp
//程序功能：业务数据汇总提数
//创建日期：2010-11-24 11:10:36
//创建人  ：liuyp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.msreport.*"%>
  <%@page import="com.sinosoft.workflow.circ.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean flag = true;
  GlobalInput tG = new GlobalInput();  
  tG=(GlobalInput)session.getValue("GI");  
  if(tG == null) {
		out.println("session has expired");
		return;
   } 
  
  	//接收信息
  	TransferData tTransferData = new TransferData();
    String tStatYear = request.getParameter("StatYear");
	String tStatMonth = request.getParameter("StatMonth");
	String tRepType = request.getParameter("RepType");
	String tStartTime = "";
	String tEndTime   = "";
	String tYearDate=tStatYear+"-01-01";
	System.out.println("tStatYear:"+tStatYear);
	System.out.println("tStatMonth:"+tStatMonth);
	String tReportDate= "";
	LFDateDB tLFDateDB=new LFDateDB();
    LFDateSchema tLFDateSchema=new LFDateSchema();
    tLFDateSchema.setRepType(tRepType);
    tLFDateSchema.setStatMon(tStatYear+tStatMonth);
    tLFDateDB.setSchema(tLFDateSchema);
    if(!tLFDateDB.getInfo()){
        Content = "请先进行报表时间管理操作!";
		FlagStr = "Fail";
		flag = false;
	
    }
    LFDateSchema mLFDateSchema=new LFDateSchema();
    mLFDateSchema = tLFDateDB.getSchema();
    tStartTime =mLFDateSchema.getStartDate();
    tEndTime   =mLFDateSchema.getEndDate();
	System.out.println("报告期开始日期:"+tStartTime);
	System.out.println("报告期结束日期:"+tEndTime);
	if(tStartTime==null)tStartTime="";
	if(tEndTime==null)  tEndTime="";
	if(!tEndTime.equals(""))
	{
      
       String mDay[]=PubFun.calFLDate(tEndTime);
       tReportDate=mDay[0];
	} 
	
    String tItemCode="";
    String tItemNum="";
    String tItemType="";
	
	tItemType="  AND ItemType In ('X1') ";   
	//cancle those contion for there is err after talk with hezy 2004-8-10 18:19
	//String RepTypeSql="";
	//if(tRepType.equals("1")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsQuick='1')";
	//if(tRepType.equals("2")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsMon='1')";
	//if(tRepType.equals("3")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsQut='1')";
	//if(tRepType.equals("4")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsHalYer='1')";
	//if(tRepType.equals("5")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsYear='1')";
	String OrderBySql=" order by ItemNum"; 
	//tItemType=tItemType+RepTypeSql+OrderBySql;
    tItemType=tItemType+OrderBySql;
	String tWhereSQL=tItemCode+" ||"+tItemNum+"  ||"+tItemType;  
	System.out.println("tWhereSQL"+tWhereSQL);
	if (tStatYear== "" ||  tStatMonth== ""  )
	{
		Content = "请录入统计年统计月!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{     
	      if(tStatYear != null && tStatMonth != null  )
	      {
             //准备公共工作流传输信息
            tTransferData.setNameAndValue("RepType",tRepType);
            tTransferData.setNameAndValue("StatYear",tStatYear);
	        tTransferData.setNameAndValue("StatMon",tStatMonth) ;
	        tTransferData.setNameAndValue("sYearDate", tYearDate);//统计年初 
	        //准备公共传输信息
	        tTransferData.setNameAndValue("WhereSQL", tWhereSQL); 
	        tTransferData.setNameAndValue("NeedItemKey", "1"); 
	        tTransferData.setNameAndValue("ReportDate", tReportDate);
            tTransferData.setNameAndValue("makedate",PubFun.getCurrentDate());
            tTransferData.setNameAndValue("maketime",PubFun.getCurrentTime());
            tTransferData.setNameAndValue("sDate", tStartTime);
            tTransferData.setNameAndValue("eDate", tEndTime);	
	      
	      }// End of if
		  else
		  {
			Content = "传输数据失败!";
			flag = false;
		  }
	}

    System.out.println("tRepType:"+tRepType);
    System.out.println("tStatYear:"+tStatYear);
    System.out.println("tStatMonth:"+tStatMonth);
    System.out.println("tYearDate:"+tYearDate);

    System.out.println("tWhereSQL:"+tWhereSQL);
    System.out.println("tReportDate:"+tReportDate);
    System.out.println("makedate:"+PubFun.getCurrentDate());
    System.out.println("maketime:"+PubFun.getCurrentTime());
    System.out.println("sDate:"+tStartTime);
    System.out.println("eDate:"+tEndTime);


try
{
  	if (flag == true )
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
  		ExcelReportYWImportUI tExcelReportYWImportUI   = new ExcelReportYWImportUI();
  			System.out.println("before ExcelReportYWImportUI!!!!");			
		 if (!tExcelReportYWImportUI.submitData(tVData,""))
		{
			int n = tExcelReportYWImportUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tExcelReportYWImportUI.mErrors.getError(i).errorMessage);
			Content = "保监会报表业务数据汇总提数失败，原因是: " + tExcelReportYWImportUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tExcelReportYWImportUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 保监会报表业务数据汇总提数成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = "保监会报表业务数据汇总提数失败，原因是: " + tError.getError(0).errorMessage;
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
