//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug = "0";
var flag;
var mSwitch = parent.VD.gVSwitch;

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var k = 0;
var cflag = "1"; // 问题件操作位置 1.核保

function change() {
	if (!document.all)
		return;

	if (event.srcElement.type == "checkbox") {
		tArray = searchArray(event.srcElement.id);
		tagInArray(tArray, event.srcElement.id);
	}

	if (event.srcElement.name == "foldheader") {
		tArray = searchArray(event.srcElement.id);
		var srcIndex = event.srcElement.sourceIndex;
		var nested = document.all[srcIndex + 1];
		if (nested.style.display == "none") {
			nested.style.display = '';
		} else {
			nested.style.display = "none";
		}
		nested = document.all[srcIndex + 2];
		if (nested.style.display == "none") {
			nested.style.display = '';
		} else {
			nested.style.display = "none";
		}

		tagShowlistInArray(tArray, event.srcElement.id);
	}
}

document.onclick = change;

function searchArray(id) {
	for ( var i = 0; i < selectArray.length; i++) {
		var aryId1 = "chk_" + selectArray[i][8];
		var aryId2 = "fld_" + selectArray[i][8];
		if (aryId1 == id || aryId2 == id)
			return selectArray;
	}
}

// 提交，保存按钮对应操作
function submitForm() {
	var i = 0;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	// showSubmitFrame(mDebug);
	fm.submit(); // 提交
	// alert("submit");
}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	showInfo.close();

	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
	showModalDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

}

// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}

function applyBat() {
	fm.all('Operator').value = "AUTO";
	submitForm();
}

function applySin() {
	// fm.all('Operator').value="HANDWORK";
	// submitForm();
}

// 查询按钮
function easyQueryClick() {
	// 初始化表格
	initPolGrid();

	// 书写SQL语句
	k++;
	var strSQL = "";
	strSQL = "select a.ckerror,b.calformula,b.remark from LFCKError a,lfcheckfield b where b.serialno=a.CKRuleCode and "
			+ k
			+ " = "
			+ k
			+ getWherePart('a.SerialNo', 'SerialNo')
			+ getWherePart('a.MakeDate', 'MakeDate')
			// + " and ManageCom like '" + ComCode + "%%'"
			+ " order by a.SerialNo ";
	// + getWherePart( 'RiskVersion' );
	// 查询SQL，返回结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		// alert(turnPage.strQueryResult);
		alert("没有满足条件的校验错误日志信息！");
		return "";
	}

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	// 设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = PolGrid;

	// 保存SQL语句
	turnPage.strQuerySql = strSQL;

	// 设置查询起始位置
	turnPage.pageIndex = 0;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,
			turnPage.pageIndex, MAXSCREENLINES);
	// alert(arrDataSet);
	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	return true;
}

function easyQueryAddClick() {
	fm.Remark.value = "";
	var tSelNo = PolGrid.getSelNo() - 1;
	fm.Remark.value = PolGrid.getRowColData(tSelNo, 1);
	fm.Remark1.value = PolGrid.getRowColData(tSelNo, 2);
	fm.Remark2.value = PolGrid.getRowColData(tSelNo, 3);
	return true;
}

var selectArray = new Array();

function buildTree(){
	//左边
	var f = fm.Remark1.value;
	if(f == "" || f == null){
		alert("没有选择一条错误信息");
		return false;
	}
	var arr = new Array();
	if(f.indexOf("=")>-1){
		arr = f.split("=");
	}else if(f.indexOf(">")>-1){
		arr = f.split(">");
	}else if(f.indexOf("<")>-1){
		arr = f.split("<");
	}
	var left = arr[0];
	var riget = arr[1];
	
	showItemTree(getOutItemcode(left),"spanItemTreeLeft");
	showItemTree(getOutItemcode(riget),"spanItemTreeRight");
}


function getOutItemcode(strItemCode){
	var arr = new Array();
	var sign = new Array();
	sign[0]="+";
	var arrResult = new Array();
	var n = 0;
	var add = strItemCode.indexOf("+");
	var sub = strItemCode.indexOf("-");
	var theFirst = false;
	var theLastSign = "";
	if(add<=-1&sub<=-1){
		theFirst = true;
		theLastSign="+";
	}
	while((add<9999999||sub<9999999)&n<100){
		if(parseFloat(add)>parseFloat(sub)){
			arr[n] = strItemCode.substring(0,sub);
			sign[n+1] = "-";
			strItemCode = strItemCode.substring(sub+1,strItemCode.length);
			n++;
		}else{
			arr[n] = strItemCode.substring(0,add);
			sign[n+1] = "+";
			strItemCode = strItemCode.substring(add+1,strItemCode.length);
			n++;
		}
		add = strItemCode.indexOf("+");
		sub = strItemCode.indexOf("-");
		if(add <=-1){
			add=9999999;
			if(!theFirst){
				theFirst = true;
				theLastSign="-";
			}
		}
		if(sub <=-1){
			sub=9999999;
			if(!theFirst){
				theFirst = true;
				theLastSign="+";
			}
		}
	}
	arr[n] = strItemCode;
	sign[n] = theLastSign;
	var strResultAdd = "";
	var strResultSub = "";
	for(var i=0 ;i<arr.length;i++){
		if(sign[i] == "+"){
			strResultAdd = strResultAdd+"'"+arr[i]+"',";
		}
		if(sign[i] == "-"){
			strResultSub = strResultSub+"'"+arr[i]+"',";
		}
	}
	if(strResultAdd.lastIndexOf(",")>-1){
		strResultAdd = strResultAdd.substring(0,strResultAdd.length-1);
	}
	if(strResultSub.lastIndexOf(",")>-1){
		strResultSub = strResultSub.substring(0,strResultSub.length-1);
	}
	arrResult[0] = strResultAdd;
	arrResult[1] = strResultSub;
	return arrResult;
}

function showItemTree(OutItemCode,spanId) {
	
	var strSqlAdd = getSql(OutItemCode[0]);
	var strSqlSub = getSql(OutItemCode[1]);
	// 测试用

	if(strSqlAdd != ""){
		print(strSqlAdd,spanId+"Add");
	}
	if(strSqlSub != ""){
		print(strSqlSub,spanId+"Sub");
	}
	
}

function getSql(OutItemCode){
	if(OutItemCode.length < 3 ){
		return "";
	}
	var strSql = " WITH temptab(upcode,isleaf,itemname, itemcode ,level,layer) AS ( "
		+ " select '0',isleaf,outitemcode||'	 '||super.itemname||'		'||varchar(svalue) itemname,super.itemcode,1,super.layer "
		+ " from lfitemrela super,(select itemcode,sum(statvalue) svalue from LFXmlColl where statyear="+fm.StatYear.value+" and statmon="+fm.StatMonth.value+" and comcodeisc='000000' and reptype='"+fm.RepType.value+"'  group by itemcode) xml "
		+ " where super.outitemcode in ("+OutItemCode+") and layer=1 "
		+ " and super.itemcode=xml.itemcode "
		+ " UNION ALL "
		+ " select super.itemcode,sub.isleaf,sub.outitemcode||'	  '||sub.itemname||'		'||varchar(svalue) itemname ,sub.itemcode,level+1,sub.layer "
		+ " from lfitemrela sub ,temptab super,(select itemcode,sum(statvalue) svalue from LFXmlColl where statyear="+fm.StatYear.value+" and statmon="+fm.StatMonth.value+" and comcodeisc='000000' and reptype='"+fm.RepType.value+"' group by itemcode) xml "
		+ " where sub.upitemcode = super.itemcode and sub.layer=1 and sub.itemcode=xml.itemcode) "
		+ " SELECT * " + " FROM temptab a " + " where 1=1 ";
	return strSql;
}

function print(strSql,spanId){
	var tempArray = easyExecSql(strSql);
	// var selectArray = new Array();
	if (tempArray != null) {
		// 初始化selectArray
		for ( var i = 0; i < tempArray.length; i++) {
			selectArray[i] = new Array();
			selectArray[i][0] = 0;
			selectArray[i][1] = tempArray[i][0];// 父节点
			selectArray[i][2] = 0;
			selectArray[i][3] = tempArray[i][2];// 显示名称
			selectArray[i][4] = tempArray[i][3];// 节点代码
			selectArray[i][5] = 0;
			selectArray[i][6] = 1;
			selectArray[i][7] = 0;
			selectArray[i][8] = "sel_" + tempArray[i][1];// 节点代码
			selectArray[i][9] = tempArray[i][1];
		}

	}
	paintTree_ReadOnly(selectArray, spanId);
}

