<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ExcelReportYWImportCancelSave.jsp
//程序功能：保监会报表业务数据提数撤销
//创建日期：2010-11-26 14:10:36
//创建人  ：liuyp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.msreport.*"%>
  <%@page import="com.sinosoft.workflow.circ.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean flag = true;
  GlobalInput tG = new GlobalInput();  
  tG=(GlobalInput)session.getValue("GI");  
  if(tG == null) {
		out.println("session has expired");
		return;
   } 
  
  	//接收信息
  	TransferData tTransferData = new TransferData();
    String tStatYear = request.getParameter("StatYear");
	String tStatMonth = request.getParameter("StatMonth");
	System.out.println("tStatYear:"+tStatYear);
	System.out.println("tStatMonth:"+tStatMonth);

	if (tStatYear== "" ||  tStatMonth== ""  )
	{
		Content = "请录入统计年统计月!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{     
	      if(tStatYear != null && tStatMonth != null  )
	      {

            tTransferData.setNameAndValue("StatYear",tStatYear);
	        tTransferData.setNameAndValue("StatMon",tStatMonth) ;
	      
	      }// End of if
		  else
		  {
			Content = "传输数据失败!";
			flag = false;
		  }
	}

    System.out.println("tStatYear:"+tStatYear);
    System.out.println("tStatMonth:"+tStatMonth);

try
{
  	if (flag == true )
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
  		ExcelReportYWImportCancelUI tExcelReportYWImportCancelUI   = new ExcelReportYWImportCancelUI();
  	    System.out.println("before ExcelReportYWImportCancelUI!!!!");			
		if (!tExcelReportYWImportCancelUI.submitData(tVData,""))
		{
			int n = tExcelReportYWImportCancelUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tExcelReportYWImportCancelUI.mErrors.getError(i).errorMessage);
			Content = "保监会报表业务数据汇总提数撤销失败，原因是: " + tExcelReportYWImportCancelUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tExcelReportYWImportCancelUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 保监会报表业务数据汇总提数撤销成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = "保监会报表业务数据汇总提数撤销失败，原因是: " + tError.getError(0).errorMessage;
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
