<%
//程序名称：LFComToISCComQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-08-12 19:10:40
//创建人  ：yangtao
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ComCode').value = "";
    fm.all('ComCodeISC').value = "";
  }
  catch(ex) {
    alert("在LFComToISCComQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLFComToISCComGrid();  
  }
  catch(re) {
    alert("LFComToISCComQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LFComToISCComGrid;
function initLFComToISCComGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    iArray[0+1]=new Array();
    iArray[0+1][0]="机构编码";         		//列名
    iArray[0+1][1]="329px";         		//列名
    iArray[0+1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[0+1][4]="comcode";              	        //是否引用代码:null||""为不引用
    iArray[0+1][5]="3";              	                //引用代码对应第几列，'|'为分割符
    iArray[0+1][9]="机构编码|code:comcode&NOTNULL";
    iArray[0+1][18]=250;
    iArray[0+1][19]= 0 ;  
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="保监会机构编码";         		//列名
    iArray[1+1][1]="273px";         		//列名
    iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许        
    iArray[2][4]="comcodeisc";              	        //是否引用代码:null||""为不引用     
    iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符   
    iArray[2][9]="保监会机构编码|code:comcodeisc&NOTNULL";                                
    iArray[2][18]=250;                                                                    
    iArray[2][19]= 0 ;                                                                    
    
    LFComToISCComGrid = new MulLineEnter( "fm" , "LFComToISCComGrid" ); 
    LFComToISCComGrid.mulLineCount = 1; 
    LFComToISCComGrid.displayTitle = 1; 
    LFComToISCComGrid.locked = 1; 
 
    LFComToISCComGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LFComToISCComGrid.mulLineCount = 0;   
    LFComToISCComGrid.displayTitle = 1;
    LFComToISCComGrid.hiddenPlus = 1;
    LFComToISCComGrid.hiddenSubtraction = 1;
    LFComToISCComGrid.canSel = 1;
    LFComToISCComGrid.canChk = 0;
    LFComToISCComGrid.selBoxEventFuncName = "showOne";
*/
    LFComToISCComGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LFComToISCComGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
