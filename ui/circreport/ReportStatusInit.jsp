<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ReportStatusInit.jsp
//程序功能：报表状态查询
//创建日期：2002-06-19 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

 
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();   
    initSelBox();    
    initPolStatuGrid()
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


// 保单状态列表的初始化
function initPolStatuGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="状态明细";         		//列名
      iArray[1][1]="400px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      PolStatuGrid = new MulLineEnter( "fm" , "PolStatuGrid" ); 
      //这些属性必须在loadMulLine前
      PolStatuGrid.mulLineCount = 3;   
      PolStatuGrid.displayTitle = 1;
      PolStatuGrid.locked = 1;
      PolStatuGrid.canSel = 0;
      PolStatuGrid.canChk = 0;
      PolStatuGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolStatuGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>