<%
//程序名称：ReportJSImportChk.jsp
//程序功能：保监会报表精算数据导入
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.circ.*"%>
  <%@page import="com.sinosoft.lis.agentprint.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.io.*"%>

<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>


<%
  //接收信息，并作校验处理。
  //输入参数
  // Variables
	int count=0;
	String path = "";      //文件上传路径
	String fileName = "";  //上传文件名
	String ImportConfigFile = "";

	//String FileName=request.getParameter("ImportFile");
	//System.out.println("FileName"+FileName);
	path = request.getParameter("ImportPath");
	//String ImportConfigFile = request.getParameter("ImportConfigFile");
	//System.out.println("ImportPath"+ImportPath);
	//System.out.println("ImportConfigFile"+ImportConfigFile);
	// Initialization
	
	DiskFileUpload fu = new DiskFileUpload();
	// 设置允许用户上传文件大小,单位:字节
	fu.setSizeMax(10000000);
	// maximum size that will be stored in memory?
	// 设置最多只允许在内存中存储的数据,单位:字节
	fu.setSizeThreshold(4096);//4096
	// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
	fu.setRepositoryPath(path);
	//开始读取上传信息
	List fileItems=null;
	try
	{
	   fileItems = fu.parseRequest(request);
	}
	catch (Exception e){
		e.printStackTrace();
	}

	String ImportPath = "";
	
	System.out.println("...开始上载文件");
	
	// 依次处理每个上传的文件
	Iterator iter = fileItems.iterator();
	while (iter.hasNext())
	{
	  FileItem item = (FileItem) iter.next();
	  if(item.getFieldName().compareTo("ImportConfigFile")==0){
		  ImportConfigFile=item.getString();
	  	//System.out.println("........save:diskimporttype"+diskimporttype);
	  	//System.out.println("ImportConfigFile : " + ImportConfigFile);
	  }
	  //忽略其他不是文件域的所有表单信息
	  if (!item.isFormField())
	  {
	    String name = item.getName();
	    
	    System.out.println("name : " + name);
	    long size = item.getSize();
	
	    if((name==null||name.equals("")) && size==0)
	      continue;
	    ImportPath= path ;
	//    ImportPath= path + ImportPath;
		ImportPath = ImportPath.replace('\\','/');
	    fileName = name.replace('\\','/');
	    fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
	    System.out.println("fileName(ImportPath + fileName):"+ImportPath + fileName);
	    
	    //保存上传的文件到指定的目录
	    try
	    {
	    //System.out.println("..............savejsphere:once");
	      item.write(new File(ImportPath + fileName));
	      count++;
	     // System.out.println("count:"+count);
	    }
	    catch(Exception e){
	      System.out.println("upload file error ...");
	    }
	  }
	}
	
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "Fail";
	String Content = "";
	String Result="";	
	boolean flag=true;
	GlobalInput tG = new GlobalInput();  
    tG=(GlobalInput)session.getValue("GI");  
    if(tG == null) {
		out.println("session has expired");
		return;
    } 
    TransferData tTransferData = new TransferData();
    String tStatYear = request.getParameter("StatYearHide");
	String tStatMonth = request.getParameter("StatMonHide");
	String tSubMissionID = request.getParameter("SubMissionID");
	String tMissionID = request.getParameter("MissionID");
		
	System.out.println("tStatYear:"+tStatYear);
	System.out.println("tStatMonth:"+tStatMonth);
	if (tStatYear== "" ||  tStatMonth== ""  )
	{
		Content = "请录入相关信息!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{     
	      if(tStatYear != null && tStatMonth != null  )
	      {
		   //准备特约信息
		   	tTransferData.setNameAndValue("ItemType","04");
            tTransferData.setNameAndValue("StatYear",tStatYear);
	        tTransferData.setNameAndValue("StatMon",tStatMonth) ;
	        tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
	        tTransferData.setNameAndValue("MissionID",tMissionID) ;	  
	        tTransferData.setNameAndValue("FileName",fileName);	
	        tTransferData.setNameAndValue("ConfigFileName",ImportConfigFile);	
            flag = true;
		   }// End of if
		  else
		  {
			Content = "传输数据失败!";
			flag = false;
		  }
	}
  
  System.out.println("----flag:"+flag);
  System.out.println("----FileName:"+fileName);
  System.out.println("----tMissionID:"+tMissionID);
  System.out.println("----tSubMissionID:"+tSubMissionID);
  System.out.println("----ConfigFileName:"+ImportConfigFile);
  System.out.println("----StatYear:"+tStatYear);
  System.out.println("----StatMon:"+tStatMonth);

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
  		CircReportWorkFlowUI tCircReportWorkFlowUI   = new CircReportWorkFlowUI();
  			System.out.println("before CircReportWorkFlowUI!!!!");			
		 if (!tCircReportWorkFlowUI.submitData(tVData,"0000000213"))//执行保全核保工作流节点0000000000
		{
			int n = tCircReportWorkFlowUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tCircReportWorkFlowUI.mErrors.getError(i).errorMessage);
			Content = "  保监会精算报表数据导入失败，原因是: " + tCircReportWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tCircReportWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 保监会精算报表数据导入成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
