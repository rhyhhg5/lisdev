<%
//程序名称：LFItemRelaQueryInit.jsp
//程序功能：功能描述
//创建日期：2004-08-12 19:10:47
//创建人  ：yangtao
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
   fm.all('ItemCode').value = "";
    fm.all('ItemName').value = "";
    fm.all('OutItemCode').value = "";
    fm.all('UpItemCode').value = "";
    fm.all('ItemLevel').value = "";
    fm.all('CorpType').value = "";
    fm.all('ItemType').value = "";
    fm.all('IsQuick').value = "";
    fm.all('IsMon').value = "";
    fm.all('IsQut').value = "";
    fm.all('IsHalYer').value = "";
    fm.all('IsYear').value = "";
    fm.all('IsLeaf').value = "";
    fm.all('General').value = "";
    fm.all('Layer').value = "";
    fm.all('Description').value = "";
    fm.all('Remark').value = "";
    fm.all('OutputFlag').value = "";
    fm.all('ComFlag').value = "";
    fm.all('IsCalFlag').value = "";
    fm.all('IsYearAll').value = "";
  }
  catch(ex) {
    alert("在LFItemRelaQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLFItemRelaGrid();  
  }
  catch(re) {
    alert("LFItemRelaQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LFItemRelaGrid;
function initLFItemRelaGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[0+1]=new Array();
    iArray[0+1][0]="内部科目编码";         		//列名
    iArray[0+1][1]="60px";         		//列名
 
    iArray[1+1]=new Array();
    iArray[1+1][0]="科目名称";         		//列名
    iArray[1+1][1]="300px";         		//列名
 
    iArray[2+1]=new Array();
    iArray[2+1][0]="外部科目编码";         		//列名
    iArray[2+1][1]="60px";         		//列名
 
    iArray[3+1]=new Array();                        
    iArray[3+1][0]="上级内部科目编码";         		//
    iArray[3+1][1]="60px";         		//列名    
    
    iArray[4+1]=new Array();
    iArray[4+1][0]  ="科目级别";         		//列名
    iArray[4+1][1]  ="60px";         		//列名
    iArray[4+1][3]  =2;  
    iArray[4+1][10] = "ItemLevel";  
    iArray[4+1][11] = "0|^1|一级^2|二级^3|三级^4|四级^5|五级^6|六级^7|七级";   
    
    iArray[5+1]=new Array();                      
    iArray[5+1][0]="公司类型";          //列名
    iArray[5+1][1]="60px";         		//列名
    iArray[5+1][3]  =2;                                                        
    iArray[5+1][10] = "CorpType";                                             
    iArray[5+1][11] = "0|^1|产^2|寿^3|再^4|集";   
            
    iArray[6+1]=new Array();                      
    iArray[6+1][0]="类型";         		//列名
    iArray[6+1][1]="60px";         		//列名
    iArray[6+1][3]  =2;                                                        
    iArray[6+1][10] = "ItemType";                                             
    iArray[6+1][11] = "0|^1|资产^2|负债^3|权益^4|损益^5|现金流^6|统计^7|资金";   
   
    iArray[7+1]=new Array();                      
    iArray[7+1][0]="是否是快报";         		//列名
    iArray[7+1][1]="50px";         		//列名  
    iArray[7+1][3]  =2;  
    iArray[7+1][10] = "ISQUICK";  
    iArray[7+1][11] = "0|^0|否^1|是";   
    
    iArray[8+1]=new Array();                         
    iArray[8+1][0]="是否是月报";         		//列名
    iArray[8+1][1]="50px";         		//列名         
    iArray[8+1][3]  =2;                 
    iArray[8+1][10] = "ISMOM";        
    iArray[8+1][11] = "0|^0|否^1|是";   

    
    iArray[9+1]=new Array();                        
    iArray[9+1][0]="是否是季报";         		//列
    iArray[9+1][1]="50px";         		//列名  
    iArray[9+1][3]  =2;                 
    iArray[9+1][10] = "IsQut";        
    iArray[9+1][11] = "0|^0|否^1|是";                             
    
    iArray[10+1]=new Array();                        
    iArray[10+1][0]="是否是半年报";         		//列
    iArray[10+1][1]="50px";         		//列名 
    iArray[10+1][3]  =2;                 
    iArray[10+1][10] = "IsYear";        
    iArray[10+1][11] = "0|^0|否^1|是";   
    
    iArray[11+1]=new Array();                        
    iArray[11+1][0]="是否是年报";         		//列
    iArray[11+1][1]="50px";         		//列名 
    iArray[11+1][3]  =2;                 
    iArray[11+1][10] = "IsYear";        
    iArray[11+1][11] = "0|^0|否^1|是";   
    
    iArray[12+1]=new Array();                        
    iArray[12+1][0]="是否是叶子结点";         		//列
    iArray[12+1][1]="50px";         		//列名 
    iArray[12+1][3]  =2;                 
    iArray[12+1][10] = "IsLeaf";        
    iArray[12+1][11] = "0|^0|否^1|是";   
   
    iArray[13+1]=new Array();                        
    iArray[13+1][0]="总分汇总";         		//列
    iArray[13+1][1]="50px";         		//列名 
    
    iArray[14+1]=new Array();                        
    iArray[14+1][0]="层级汇总";         		//列
    iArray[14+1][1]="50px";         	//列名 
    iArray[15+1]=new Array();                        
    iArray[15+1][0]="定义口径描述";         		//列
    iArray[15+1][1]="0px";         	//列名 
    iArray[16+1]=new Array();                        
    iArray[16+1][0]="备注";         		//列
    iArray[16+1][1]="0px";         	//列名 
    
    iArray[17+1]=new Array();                        
    iArray[17+1][0]="是否上报标志";      //列
    iArray[17+1][1]="50px";         	//列名 
    iArray[17+1][3]  =2;                 
    iArray[17+1][10] = "OutputFlag";        
    iArray[17+1][11] = "0|^0|否^1|是";   
    
    iArray[18+1]=new Array();                        
    iArray[18+1][0]="上报数据的机构粒度";         		//列
    iArray[18+1][1]="50px";         		//列名 
    iArray[18+1][3]  =2;                 
    iArray[18+1][10] = "ComFlag";        
    iArray[18+1][11] = "0|^1|据明细到总公司^2|数据明细到分公司^3|明细到中心支公司，^4|明细到支公司"; 
    

    
    iArray[19+1]=new Array();                        
    iArray[19+1][0]="是否一级计算标志";         		//列
    iArray[19+1][1]="50px";         		//列名 
    iArray[19+1][3]  =2;                 
    iArray[19+1][10] = "IsCalFlag";        
    iArray[19+1][11] = "0|^0|否^1|是";  
    
    iArray[20+1]=new Array();                        
    iArray[20+1][0]="是否是年度报";         		//列
    iArray[20+1][1]="50px";         		//列名 
    iArray[20+1][3]  =2;                 
    iArray[20+1][10] = "IsYearAll";        
    iArray[20+1][11] = "0|^0|否^1|是";    
    
    
    
    LFItemRelaGrid = new MulLineEnter( "fm" , "LFItemRelaGrid" ); 
    LFItemRelaGrid.mulLineCount = 1; 
    LFItemRelaGrid.displayTitle = 1; 
    LFItemRelaGrid.locked = 1; 
 
    LFItemRelaGrid.canSel = 1; 
 
    //这些属性必须在loadMulLine前
/*
    LFItemRelaGrid.mulLineCount = 0;   
    LFItemRelaGrid.displayTitle = 1;
    LFItemRelaGrid.hiddenPlus = 1;
    LFItemRelaGrid.hiddenSubtraction = 1;
    LFItemRelaGrid.canSel = 1;
    LFItemRelaGrid.canChk = 0;
    LFItemRelaGrid.selBoxEventFuncName = "showOne";
*/
    LFItemRelaGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LFItemRelaGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
