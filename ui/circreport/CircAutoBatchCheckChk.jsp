<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PolStatusChk.jsp
//程序功能：保单状态查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.msreport.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //内容待填充
	String tCountSQL = request.getParameter("SQLCountHide");
	String tSQL = request.getParameter("SQLHide");
	
	boolean flag = false;
	if (!tSQL.equals("")&&!tCountSQL.equals("") )
	{
		System.out.println("tSQL:"+tSQL);
		flag = true;
	}
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tCountSQL );
		tVData.add( tSQL );
		tVData.add( tG );
		
		// 数据传输
		CircAutoBatchChkUI tPRnewPolStatusChkUI   = new CircAutoBatchChkUI();
		if (tPRnewPolStatusChkUI.submitData(tVData,"INSERT") == false)
		{
			int n = tPRnewPolStatusChkUI.mErrors.getErrorCount();
			Content = " 查询失败，原因是: " + tPRnewPolStatusChkUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPRnewPolStatusChkUI.mErrors;
		    //tErrors = tPRnewPolStatusChkUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	FlagStr = "Succ";
		    	Content = "操作成功，请到报表自动校验错误查询界面查看是否有校验失败信息。";
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    	Content = "操作失败，原因是："+tError.getError(0).errorMessage;
		    }
		}
		else
		{                          
		    	FlagStr = "Succ";
		    	Content = "操作成功，请到报表自动校验错误查询界面查看是否有校验失败信息。";
		}
	}
	else
	{
		Content = "前台传输信息出错！";
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
