//程序名称：ReportEngine.js
//程序功能：一级汇总提交
//创建日期：2004-6-9 20:30
//创建人  ：guoxiang
//更新记录：  更新人    更新日期     更新原因/内容
//该文件中包含客户端需要处理的函数和事件
 
//设置显示行数和页数
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";
var mOperate="";
//提交，保存按钮对应操作
function submitForm()
{
  
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}


function initContent(ItemClass)
{
  try
  {
	    
	    
	    //alert(ItemClass);
	    var vSQL = "select distinct itemtype from LFDesbMode where Dealtype='S' AND ItemType like '"+ItemClass+"%%'";
		turnPage.queryModal(vSQL, LFDesbModeGrid);           
  }
  catch(re)
  {
    alert("ReportEngine.js-->InitContent函数中发生异常:初始化界面错误!");
  }
}
//               该文件中包含客户端需要处理的函数和事件



window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm2()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  //showSubmitFrame(mDebug);
  fm2.submit(); //提交
}

//数据汇总的操作
function CollData()
{
	
	  //首先检验录入框
  if(!verifyForm("fm2")) return false;
  fm2.action="./LFCollDataSave.jsp";
  submitForm2();
}

//生成文件的操作
function MakeFile()
{
	  //首先检验录入框
  if(!verifyForm("fm2")) return false;
  fm2.action="./LFMakeFileSave.jsp";
  submitForm2();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  }
}
                
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
//function showSubmitFrame(cDebug)
//{
  //if(cDebug=="1")
  //{
			//parent.fraMain.rows = "0,0,50,82,*";
  //}
 	//else {
  		//parent.fraMain.rows = "0,0,0,82,*";
 	//}
//}
function calSumbit(){
   var rowNum=LFDesbModeGrid.mulLineCount;   
   var today=new Date();
   var month=today.getMonth()+1.0;
   var curTime=today.getYear()+"-"+month+"-"+today.getDate()+" "+today.getHours()+":"+today.getMinutes()+":"+today.getSeconds();  
   for(i=0;i<rowNum;i++)
   {                                                                   
     if(LFDesbModeGrid.getChkNo(i))
     {
          LFDesbModeGrid.setRowColData(i,2,curTime);
      }
   }
   submitForm();
   
}