//程序名称：ReportCWImportCancel.js
//程序功能：保监会报表财务数据导入撤销
//创建日期：2004-07-08 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content);     
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    alert(content);
    easyQueryClick();
   //执行下一步操作
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



// 查询按钮
function easyQueryClick()
{
	initPolGrid();
	// 书写SQL语句
	k++;
	var strSQL = "";
strSQL = "select LWMission.MissionProp2,LWMission.MissionProp3,LWMission.CreateOperator ,LWMission.Makedate ,LWMission.Maketime,LWMission.MissionID,LWMission.SubMissionID from LWMission where "+k+"="+k				 	
				 + " and LWMission.ProcessID = '0000000002' " //保监会报表工作流
				 + " and LWMission.ActivityID = '0000000206' "//保监会报表工作流财务数据导入撤销任务节点
				 + getWherePart( 'MissionProp2','StatYear')
				 + getWherePart( 'MissionProp3','StatMonth')
				 + " order by MissionProp2,MissionProp3";
	
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有待启动的保监会报表申请！");
    return "";
  }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

function autochk()
{
  var i = 0;
  var tSelNo = PolGrid.getSelNo();
  if( tSelNo　== -1)
  {
  	alert("请先选择一条记录信息，再点击“发选定的续期催收通知书”！");
  	return ;
  }
  fm.all('SubMissionID').value = PolGrid.getRowColData(tSelNo - 1,7);	
  fm.all('MissionID').value = PolGrid.getRowColData(tSelNo - 1,6);
  fm.all('StatYearHide').value = PolGrid.getRowColData(tSelNo - 1,1);
  fm.all('StatMonHide').value = PolGrid.getRowColData(tSelNo - 1,2);
  alert(fm.all('StatYearHide').value);
  if(fm.all('SubMissionID').value == ""||fm.all('MissionID').value == ""||fm.all('StatYearHide').value=="")
  {
  	alert("请先选择一条记录信息，点击提交按钮！");
  	return ;
}
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}


function allautochk()
{
  if(fm.all('SQLHide').value== "")
  {
  	alert("请先查询后，再点击“发所有的续期催收通知书”按钮！");
  	return ;
  }
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "./PRnewApplyAllChk.jsp";
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function easyQueryAddClick(parm1,parm2)
{
	
	if(fm.all(parm1).all('InpPolGridSel').value == '1' )
	{
	//当前行第1列的值设为：选中
   		fm.PrtNoHide.value =  fm.all(parm1).all('PolGrid2').value;
  	}
}