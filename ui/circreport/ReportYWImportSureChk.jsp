<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ReportYWImportSureChk.jsp
//程序功能：业务数据导入
//创建日期：2002-06-19 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
   <%@page import="com.sinosoft.msreport.*"%>
   <%@page import="com.sinosoft.workflow.circ.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean flag = true;
  GlobalInput tG = new GlobalInput();  
  tG=(GlobalInput)session.getValue("GI");  
  if(tG == null) {
		out.println("session has expired");
		return;
   } 
  
  	//接收信息
    //接收信息
  	TransferData tTransferData = new TransferData();
    String tStatYear = request.getParameter("StatYearHide");
	String tStatMonth = request.getParameter("StatMonHide");
	String tSubMissionID = request.getParameter("SubMissionID");
	String tMissionID = request.getParameter("MissionID");
		
	System.out.println("tStatYear:"+tStatYear);
	System.out.println("tStatMonth:"+tStatMonth);
	if (tStatYear== "" ||  tStatMonth== ""  )
	{
		Content = "请录入续保特别约定信息或续保备注信息!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{     
	      if(tStatYear != null && tStatMonth != null  )
	      {
	        tTransferData.setNameAndValue("ItemType","01");
            tTransferData.setNameAndValue("StatYear",tStatYear);
	        tTransferData.setNameAndValue("StatMon",tStatMonth) ;
	        tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
	        tTransferData.setNameAndValue("MissionID",tMissionID) ;	        
		   }// End of if
		  else
		  {
			Content = "传输数据失败!";
			flag = false;
		  }
	}


try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
  		CircReportWorkFlowUI tCircReportWorkFlowUI   = new CircReportWorkFlowUI();
  			System.out.println("before CircReportWorkFlowUI!!!!");			
		 if (!tCircReportWorkFlowUI.submitData(tVData,"0000000202"))//保监会报表工作流业务数据确认0000000202
		{
			int n = tCircReportWorkFlowUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tCircReportWorkFlowUI.mErrors.getError(i).errorMessage);
			Content = "  保监会报表工作流业务数据确认失败，原因是: " + tCircReportWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tCircReportWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = "保监会报表工作流业务数据确认成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
