<%
//程序名称：LFComFinInput.jsp
//程序功能：功能描述
//创建日期：2004-09-14 15:49:12
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LFComFinQueryInput.js"></SCRIPT> 
  <%@include file="LFComFinQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLFComFinGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      财务管理机构编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ComFin >
    </TD>
    <TD  class= title>
      机构名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Name >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      成立日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FoundDate >
    </TD>
    <TD  class= title>
      停业标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      停业日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndDate >
    </TD>
    <TD  class= title>
      机构属性
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Attribute >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      机构标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Sign >
    </TD>
    <TD  class= title>
      统计标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=calFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      机构地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Address >
    </TD>
    <TD  class= title>
      机构邮编
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ZipCode >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      机构电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Phone >
    </TD>
    <TD  class= title>
      机构传真
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Fax >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      EMail
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EMail >
    </TD>
    <TD  class= title>
      网址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WebAddress >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      主管人姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SatrapName >
    </TD>
    <TD  class= title>
      总公司正式批示时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ApproveDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      工商批准营业时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PassDate >
    </TD>
  </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=common onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLFComFin1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLFComFin1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLFComFinGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
