<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LFRiskSave.jsp
//程序功能：
//创建日期：2004-07-20 09:40:17
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.onetable.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LFRiskSchema tLFRiskSchema   = new LFRiskSchema();
  OLFRiskUI tOLFRiskUI   = new OLFRiskUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLFRiskSchema.setRiskCode(request.getParameter("RiskCode"));
    tLFRiskSchema.setRiskVer(request.getParameter("RiskVer"));
    tLFRiskSchema.setRiskName(request.getParameter("RiskName"));
    tLFRiskSchema.setRiskType(request.getParameter("RiskType"));
    tLFRiskSchema.setRiskPeriod(request.getParameter("RiskPeriod"));
    tLFRiskSchema.setSubRiskFlag(request.getParameter("SubRiskFlag"));
    tLFRiskSchema.setLifeType(request.getParameter("LifeType"));
    tLFRiskSchema.setRiskDutyType(request.getParameter("RiskDutyType"));
    tLFRiskSchema.setRiskYearType(request.getParameter("RiskYearType"));
    tLFRiskSchema.setHealthType(request.getParameter("HealthType"));
    tLFRiskSchema.setAccidentType(request.getParameter("AccidentType"));
    tLFRiskSchema.setEndowmentFlag(request.getParameter("EndowmentFlag"));
    tLFRiskSchema.setEndowmentType1(request.getParameter("EndowmentType1"));
    tLFRiskSchema.setEndowmentType2(request.getParameter("EndowmentType2"));
    tLFRiskSchema.setRiskSaleChnl(request.getParameter("RiskSaleChnl"));
    tLFRiskSchema.setRiskGrpFlag(request.getParameter("RiskGrpFlag"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLFRiskSchema);
  	tVData.add(tG);
    tOLFRiskUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLFRiskUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
