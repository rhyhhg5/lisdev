<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ExcelReportYWImportCancelInit.jsp
//程序功能：保监会报表业务数据提数撤销
//创建日期：2010-11-26 14:10:36
//创建人  ：liuyp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('StatYear').value = '';
    fm.all('StatMonth').value = '';
}
  catch(ex)
  {
    alert("在ExcelReportYWImportCancelInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ExcelReportYWImportCancelInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("ExcelReportYWImportCancelInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>