<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：CircXmlQuery.jsp
//程序功能：保监会报表自动校验
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="CircXmlQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CircXmlQueryInit.jsp"%>
  <title>保监会报表查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./CircAutoCheckChk.jsp">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      	　<TD  class= title>
            科目编码
          </TD>
          <TD  class= input>
            <Input class="codeno" name=OutItemCode ondblclick="return showCodeList('OutItemCode',[this,OutItemCodeName],[0,1]);" onkeyup="return showCodeListKey('OutItemCode',[this,OutItemCodeName],[0,1]);"><input class=codename name=OutItemCodeName>          
          </TD>
            <TD  class= title>
            保监会管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=comcodeisc ondblclick="return showCodeList('comcodeisc',[this,comcodeiscName],[0,1]);" onkeyup="return showCodeListKey('comcodeisc',[this,comcodeiscName],[0,1]);"><input class=codename name=comcodeiscName>                    
          <!--Input class="code" name=comcodeisc ondblclick="return showCodeList('comcodeisc',[this]);" onkeyup="return showCodeListKey('comcodeisc',[this]);"-->  
          </TD>
          <TD  class= title>
           科目级别
          </TD>  
          <TD class= input>
               <Input class="codeno" name=ItemLevel verify="科目级别|NOTNULL" CodeData="0|^1|一级^2|二级^3|三级^4|四级^5|五级^6|六级^7|七级" 
             ondblClick="showCodeListEx('ItemLevel',[this,ItemLevelName],[0,1]);" onkeyup="showCodeListKeyEx('ItemLevel',[this,ItemLevelName],[0,1]);"><input class=codename name=ItemLevelName>          
          </TD>            
          </TR>      
          <TD  class= title>
            统计年
          </TD>
          <TD  class= input>
           <Input class="common"  name=StatYear >
          </TD>
          <TD  class= title>
           统计月
          </TD>
          <TD  class= input>
           <Input class="common"  name=StatMon >
          </TD>
          <TD  class= title>
            统计类型
          </TD>  
          <TD class= input>
               <Input class="codeno" name=RepType verify="统计类型|NOTNULL" CodeData="0|^1|快报^2|月报^3|季报^4|半年报^5|年报" 
             ondblClick="showCodeListEx('RepType',[this,RepTypeName],[0,1]);" onkeyup="showCodeListKeyEx('RepType',[this,RepTypeName],[0,1]);"><input class=codename name=RepTypeName>          
          </TD>
        </TR>
    </table>
    <hr/>
          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 科目信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
  	
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
