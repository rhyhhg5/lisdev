<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LFIscPrintSave.jsp
//程序功能：生成保监会报表
//创建日期：2008-09-04 09:57:36
//创建人  ：lyp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.msreport.*"%>
  <%@page import="com.sinosoft.workflow.circ.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean flag = true;
  GlobalInput tG = new GlobalInput();  
  tG=(GlobalInput)session.getValue("GI");  
  if(tG == null) {
		out.println("session has expired");
		return;
   } 
  
  	//接收信息
    //接收信息
  	TransferData tTransferData = new TransferData();
  	String ManageCom = request.getParameter("ManageCom");
    String tStatYear = request.getParameter("StatYear");
	  String tStatMonth = request.getParameter("StatMon");
	  System.out.println("ManageCom:"+ManageCom);
	  System.out.println("tStatYear:"+tStatYear);
	  System.out.println("tStatMonth:"+tStatMonth);

	  if (tStatYear== "" ||  tStatMonth== ""  )
	  {
		  Content = "请录入统计报表年统计报表月!";
		  FlagStr = "Fail";
		  flag = false;
	  }
	  else
	  {     
	      if(tStatYear != null && tStatMonth != null  )
	      {
             //准备公共工作流传输信息
          tTransferData.setNameAndValue("ManageCom",ManageCom); 
          tTransferData.setNameAndValue("StatYear",tStatYear);
	        tTransferData.setNameAndValue("StatMon",tStatMonth) ;	      
	      }// End of if
		    else
		    {
			    Content = "传输数据失败!";
			    flag = false;
		    }
	}

          System.out.println("ManageCom:"+ManageCom);
          System.out.println("tStatYear:"+tStatYear);
          System.out.println("tStatMonth:"+tStatMonth);
try
{
  	if (flag == true )
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
  		LFCircHealthReportBL tLFCircHealthReportBL   = new LFCircHealthReportBL();
		 if (!tLFCircHealthReportBL.submitData(tVData,"select"))//执行保监会报表打印
		{
			int n = tLFCircHealthReportBL.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tLFCircHealthReportBL.mErrors.getError(i).errorMessage);
			Content = "生成保监会报表excel文件失败，原因是: " + tLFCircHealthReportBL.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tLFCircHealthReportBL.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 生成保监会报表excel文件成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = "生成保监会报表excel文件失败，原因是: " + tError.getError(0).errorMessage;
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
