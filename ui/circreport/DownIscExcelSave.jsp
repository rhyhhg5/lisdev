<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：DownIscExcelSave.jsp
	//程序功能：保监会报表下载
	//创建日期：2005-10-12 11:10:36
	//创建人  ：sxy
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<%
	String FlagStr = "";
	String Content = "";
	String tOperate = "";

	//获得mutline中的数据信息
	int i = 0;
	System.out.println("**************************DownIscExcelSave BEGIN********************************");
	String tChecks[] = request.getParameterValues("Checks");
	String tChecksValues[] = request.getParameterValues("ChecksValues");
	System.out.println("tChecks="+tChecks);
	System.out.println("tChecksValues="+tChecksValues);
	
	
	List mChecksValuesPath = new ArrayList();
	List mChecksValues = new ArrayList();
	if (tChecks == null) 
	{
		FlagStr = "Fail";
		Content = "没有选中的文件";
	} 
	else 
	{
		for (i = 0;i < tChecks.length; i++) 
		{
			System.out.println("tChecks"+"["+i+"]="+tChecks[i]);
			int index = Integer.parseInt(tChecks[i]);
			System.out.println("index= "+ index);
			System.out.println("tChecksValues["+index+"]= "+tChecksValues[index]);
			mChecksValuesPath.add(tChecksValues[index]);
			mChecksValues.add(tChecksValues[index].substring(tChecksValues[index].lastIndexOf("/") + 1));
		}
		String[] cChecksValuesPath = (String[]) mChecksValuesPath.toArray(new String[0]);
		String[] cChecksValues = (String[]) mChecksValues.toArray(new String[0]);
		System.out.println("cChecksValuesPath.length is "+ cChecksValuesPath.length);
		if (cChecksValuesPath.length > 0 && cChecksValues.length > 0) 
		{
			ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = new SSRS();
			String tSql = "select sysvarvalue from ldsysvar where sysvar='ZipFilePath'";
			tSSRS = tExeSQL.execSQL(tSql);
			String zipfilepath = tSSRS.GetText(1, 1)+ "downloadexcel.zip";
			if (!tProposalDownloadBL.CreatZipFile(cChecksValuesPath,cChecksValues, zipfilepath)) 
			{
				System.out.println("生成压缩文件失败!");
			} 
			else 
			{
				FlagStr = "Succ";
				Content = "生成文件成功";
			}
		}
		else 
		{
			System.out.println("没有选中的文件");
			FlagStr = "Fail";
			Content = "没有选中的文件";
		}
	}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
 	var intPageWidth=screen.availWidth;
	var intPageHeight=screen.availHeight;
	var w=(intPageWidth-300)/2;
	var h=(intPageHeight-200)/2;
	var Succ="Succ";
	var Fail="Fail";
	if (<%=FlagStr%> == Succ)
	{
	window.open("../easyscan/PicDownload.jsp?&prtNo=downloadexcel","print", "height=50,width=300,top="+h+",left="+w+",toolbar=yes,status=no,menubar=no,resizable=no,z-look=no");
}
</script>
</html>
