<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ReportEngineChk.jsp
//程序功能：汇总体数处理页
//创建日期：2004-6-9 20:36
//创建人  ：guoxiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->	
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.msreport.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bl.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>

  
<%
  //输出参数
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";
    GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
     
  	//接收信息

    TransferData tTransferData = new TransferData();
    boolean flag = true;  
    //公共需要传输的条件
    String tStartTime = "";
	String tEndTime   = "";
	//一级汇总需要提交的
	String tReportDate= "";
	//xml汇总需要提交的                    
	String tYear      = "";
	String tMonth     = "";
	String tRepType   = "";
	String tYearDate  = "";
    String tCalType=request.getParameter("calType");
    System.out.println("提数类型:"+tCalType);
	tYear      = request.getParameter("Year1");
	tMonth     = request.getParameter("Month1");
	tYearDate=tYear+"-01-01";
    System.out.println("报告月:"+tMonth);
    System.out.println("报告年:"+tYear);
	if(tCalType.equals("0"))
	{
       tRepType   = "1";
	} 
	if(tCalType.equals("X"))   
	{                   
	   
	   tRepType   = request.getParameter("RepType1");
       System.out.println("报告类型:"+tRepType);
    } 
    LFDateDB tLFDateDB=new LFDateDB();
    LFDateSchema tLFDateSchema=new LFDateSchema();
    tLFDateSchema.setRepType(tRepType);
    tLFDateSchema.setStatMon(tYear+tMonth);
    tLFDateDB.setSchema(tLFDateSchema);
    if(!tLFDateDB.getInfo()){
     flag = false;
	
    }
    LFDateSchema mLFDateSchema=new LFDateSchema();
    mLFDateSchema = tLFDateDB.getSchema();
    tStartTime =mLFDateSchema.getStartDate();
    tEndTime   =mLFDateSchema.getEndDate();
	System.out.println("报告期开始日期:"+tStartTime);
	System.out.println("报告期结束日期:"+tEndTime);
	if(tStartTime==null)tStartTime="";
	if(tEndTime==null)  tEndTime="";
	//一月二月会有问题
	//if(tCalType.equals("0")&&!tEndTime.equals(""))
	//{
      
       //String mDay[]=PubFun.calFLDate(tEndTime);
        
       //tReportDate=mDay[0];
	//} 
	tReportDate=tYear+"-"+tMonth+"-01";	
    //选择报表类型
	String tSql="";
    String tGrid1 [] = request.getParameterValues("LFDesbModeGrid1"); //得到第1列的所有值
    String tChk[] = request.getParameterValues("InpLFDesbModeGridChk"); //参数格式=" Inp+MulLine对象名+Chk"  
    int checkCount=0;
    for(int index=0;index<tChk.length;index++)
    {
          if(tChk[index].equals("1"))
          {           
            System.out.println("该行被选中");
            tSql+=",'"+tGrid1 [index].substring(0,2)+"'";
            checkCount++;
          }
          if(tChk[index].equals("0"))   
          {   
            System.out.println("该行未被选中");
          }
    }                                                 
    System.out.println("选中的的记录数:"+checkCount);    
    if (checkCount==0)
	{
        flag = false;
		Content = "数据提交不完整，必须至少选中一列提数类型！";
    } 
    String tItemCode="";
    String tItemNum="";
    String tItemType="";
    System.out.println("查询描述表准备的sql语句条件:"+tItemType);
    // 准备传输数据 VData
	VData tVData = new VData();
	tVData.add( tG );
    if (!tCalType.equals("")&&!tStartTime.equals("")&&!tEndTime.equals(""))
	{
	  //一级汇总	
	  if(tCalType.equals("0")&&!tReportDate.equals(""))
	  { 
	
		 tItemType=" AND Dealtype='S' AND ItemType In ('&'"+tSql+")  order by ItemNum";    
         //准备公共传输信息
         tTransferData.setNameAndValue("ReportDate", tReportDate);
         tTransferData.setNameAndValue("makedate",PubFun.getCurrentDate());
         tTransferData.setNameAndValue("maketime",PubFun.getCurrentTime());
         tTransferData.setNameAndValue("StatYear",tYear);	
         tTransferData.setNameAndValue("StatMon", tMonth);
         tTransferData.setNameAndValue("sDate", tStartTime);
         tTransferData.setNameAndValue("eDate", tEndTime);	
	     tVData.add("0");
	     tVData.add( tTransferData);
	  }
	  //xml汇总
	  if(tCalType.equals("X")&&!tRepType.equals("")&&!tYear.equals("")&&!tStartTime.equals(""))   
	  {
        
		tItemType=" AND ItemType In ('&'"+tSql+")"; 
		String OrderBySql=" order by ItemNum"; 
		//cancle those contion for there is err after talk with hezy
		//String RepTypeSql="";
		//if(tRepType.equals("1")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsQuick='1')";
		//if(tRepType.equals("2")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsMon='1')";
		//if(tRepType.equals("3")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsQut='1')";
		//if(tRepType.equals("4")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsHalYer='1')";
	    //if(tRepType.equals("5")) RepTypeSql=" AND ItemCode in(select itemcode from LFItemRela where OutPutFlag='1' and IsYear='1')";
	    //tItemType=tItemType+RepTypeSql+OrderBySql;
		tItemType=tItemType+OrderBySql;
		//准备公共传输信息
        tTransferData.setNameAndValue("RepType", tRepType);//统计类型
        tTransferData.setNameAndValue("StatYear",tYear);	//统计年
        tTransferData.setNameAndValue("StatMon", tMonth);   //统计月
        tTransferData.setNameAndValue("sYearDate", tYearDate);//统计年初
        tTransferData.setNameAndValue("sDate", tStartTime);   //统计开始时间
        tTransferData.setNameAndValue("eDate", tEndTime);    //统计结束时间
		tVData.add("1");	  
	    tVData.add(tTransferData);
      
	  }
	}
	else
	{
		flag = false;
		Content = "数据不完整，必须提交开始日期，结束日期和报表提数日期或者由年月查询时间描述表得开始日期，结束日期失败！";
		
	}	


		
	String tOperate=tItemCode+" ||"+tItemNum+"  ||"+tItemType;
	System.out.println("准备的查询条件:"+tOperate);
    System.out.println("flag:"+flag);
try
{
  	if (flag == true)
  	{
		
	
		// 数据传输
	    ReportEngineUI tReportEngineUI   = new ReportEngineUI();
		if (!tReportEngineUI.submitData(tVData,tOperate))
		{
			int n = tReportEngineUI.mErrors.getErrorCount();
			System.out.println("ErrorCount:"+n ) ;
			Content = " 提交汇总失败，原因是: " + tReportEngineUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tReportEngineUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 提交汇总成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 提交汇总失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		     }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>