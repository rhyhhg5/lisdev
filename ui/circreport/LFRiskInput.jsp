<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2004-07-20 09:40:16
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LFRiskInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LFRiskInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LFRiskSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 险种报表描述基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLFRisk1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      险种编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskCode >
    </TD>
    <TD  class= title>
      险种版本
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskVer >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      险种名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RiskName  >
    </TD>
    <TD  class= title>
      险种分类
    </TD>
    <TD  class= input>
      <Input class= 'code' name=RiskType  CodeData="0|^L|寿险^A|意外险^H|健康险"  ondblClick="showCodeListEx('RiskType',[this],[0]);" onkeyup="showCodeListKeyEx('RiskType',[this],[0]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      险种类别
    </TD>
    <TD  class= input>
      <Input class= 'code' name=RiskPeriod CodeData="0|^L|长险^M|一年期险^S|极短期险"  ondblClick="showCodeListEx('RiskPeriod',[this],[0]);" onkeyup="showCodeListKeyEx('RiskPeriod',[this],[0]);">
    </TD>
    <TD  class= title>
      主附险标记
    </TD>
    <TD  class= input>
      <Input class= 'code' name=SubRiskFlag CodeData="0|^M|主险^S|附险^A|两者都可以"  ondblClick="showCodeListEx('SubRiskFlag',[this],[0]);" onkeyup="showCodeListKeyEx('SubRiskFlag',[this],[0]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      寿险分类
    </TD>
    <TD  class= input>
      <Input class= 'code' name=LifeType CodeData="0|^1|传统险（普通寿险）^2|分红^3|投连^4|万能^5|其他"  ondblClick="showCodeListEx('LifeType',[this],[0]);" onkeyup="showCodeListKeyEx('LifeType',[this],[0]);">
    </TD>
    <TD  class= title>
      险种事故责任分类（寿险）
    </TD>
    <TD  class= input>
      <Input class= 'code' name=RiskDutyType CodeData="0|^1|终身^2|两全及生存^3|定期^4|年金^5|其他"  ondblClick="showCodeListEx('RiskDutyType',[this],[0]);" onkeyup="showCodeListKeyEx('RiskDutyType',[this],[0]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      健康险和意外险期限分类
    </TD>
    <TD  class= input>
      <Input class= 'code' name=RiskYearType CodeData="0|^0|一年期以内^1|一年期^2|一年期以上"  ondblClick="showCodeListEx('RiskYearType',[this],[0]);" onkeyup="showCodeListKeyEx('RiskYearType',[this],[0]);">
    </TD>
    <TD  class= title>
      健康险细分
    </TD>
    <TD  class= input>
      <Input class= 'code' name=HealthType CodeData="0|^1|医疗保险 ^2|普通疾病保险^3|重大疾病保^4|失能保险^5|护理保险"  ondblClick="showCodeListEx('HealthType',[this],[0]);" onkeyup="showCodeListKeyEx('HealthType',[this],[0]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      意外险细分
    </TD>
    <TD  class= input>
      <Input class= 'code' name=AccidentType CodeData="0|^1|航空意外险 ^2|学生平安险^3|建筑工人意外伤害险"  ondblClick="showCodeListEx('AccidentType',[this],[0]);" onkeyup="showCodeListKeyEx('AccidentType',[this],[0]);">
    </TD>
    <TD  class= title>
      养老险标记
    </TD>
    <TD  class= input>
      <Input class= 'code' name=EndowmentFlag CodeData="0|^0|不是养老险 ^1|是养老险"  ondblClick="showCodeListEx('EndowmentFlag',[this],[0]);" onkeyup="showCodeListKeyEx('EndowmentFlag',[this],[0]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      养老险细分1
    </TD>
    <TD  class= input>
      <Input class= 'code' name=EndowmentType1 CodeData="0|^0|无税优计划 ^1|有税优计划"  ondblClick="showCodeListEx('EndowmentType1',[this],[0]);" onkeyup="showCodeListKeyEx('EndowmentType1',[this],[0]);">
    </TD>
    <TD  class= title>
      养老险细分2
    </TD>
    <TD  class= input>
      <Input class= 'code' name=EndowmentType2 CodeData="0|^0|无税优计划 ^1|有税优计划"  ondblClick="showCodeListEx('EndowmentType2',[this],[0]);" onkeyup="showCodeListKeyEx('EndowmentType2',[this],[0]);">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      险种的销售渠道
    </TD>
    <TD  class= input>
      <Input class= 'code' name=RiskSaleChnl CodeData="0|^01|个人代理^02|公司直销^03|保险专业代理^04|银行邮政代理^05|其他兼业代理^06|保险经纪业务"  ondblClick="showCodeListEx('RiskSaleChnl',[this],[0]);" onkeyup="showCodeListKeyEx('RiskSaleChnl',[this],[0]);">
    </TD>
    <TD  class= title>
      险种的团个单标志
    </TD>
    <TD  class= input>
      <Input class= 'code' name=RiskGrpFlag CodeData="0|^1|个人 ^2|团体"  ondblClick="showCodeListEx('RiskGrpFlag',[this],[0]);" onkeyup="showCodeListKeyEx('RiskGrpFlag',[this],[0]);">
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
