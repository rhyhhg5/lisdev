<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ReportYWExport.jsp
//程序功能：保监会报表业务数据导入
//创建日期：2010-11-23 11:10:36
//创建人  ：liuyp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="ReportYWExport.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ReportYWExportInit.jsp"%>
  <title>业务数据导出</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./ReportYWExportSave.jsp">
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
     	<TR  class= common>
        
          	<TD  class= title>
            	统计报表年
          	</TD>
          	<TD  class= input>
            	<Input class=codeNo name=StatYear  verify="统计报表年|notnull" ondblclick="return showCodeList('startyear',[this,StatYearName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('startyear',[this,StatYearName],[0,1],null,null,null,1);"><input class=codename name=StatYearName readonly=true>
            		</TD>           
          	<TD  class= title>
            	统计报表月
          	</TD>
          	<TD  class= input>
            	<Input class= codeno name=StatMonth verify="统计报表月|notnull" CodeData="0|^01|一月|M^02|二月|M^03|三月|M^04|四月|M^05|五月|M^06|六月|M^07|七月|M^08|八月|M^09|九月|M^10|十月|M^11|十一月|M^12|十二月|M" ondblClick="showCodeListEx('ReportMonth',[this,StatMonthName],[0,1]);" onkeyup="showCodeListKeyEx('ReportMonth',[this,StatMonthName],[0,1]);"><input class= codename name=StatMonthName>
          	</TD>
    	</TR> 
    </table>
  	 <hr/>
      <INPUT VALUE="业务数据导出" class= common TYPE=button onclick = "autochk();"> 
      <Div id=DivFileDownload style="display:'none'">
        <A id=fileUrl href=""></A>
      </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
