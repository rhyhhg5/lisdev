<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ReportTZImport.jsp
//程序功能：保监会报表投资数据导入
//创建日期：2004-07-08 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    String Branch =tG.ComCode;
    String strCurTime = PubFun.getCurrentDate();   
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="ReportTZImport.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ReportTZImportInit.jsp"%>  

</head>
<body  onload="initForm();" >
  <form action="./ReportTZImportChk.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
	<table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
     	<TR  class= common>
        
          	<TD  class= title>
            	统计报表年
          	</TD>
          	<TD  class= input>
            	<Input class=codeNo name=StatYear  verify="统计报表年|notnull" ondblclick="return showCodeList('startyear',[this,StatYearName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('startyear',[this,StatYearName],[0,1],null,null,null,1);"><input class=codename name=StatYearName readonly=true>
            </TD>           
          	<TD  class= title>
            	统计报表月(快报)
          	</TD>
          	<TD  class= input>
            	<Input class= codeno name=StatMonth verify="统计报表月|notnull" CodeData="0|^01|一月|M^02|二月|M^03|三月|M^04|四月|M^05|五月|M^06|六月|M^07|七月|M^08|八月|M^09|九月|M^10|十月|M^11|十一月|M^12|十二月|M" ondblClick="showCodeListEx('ReportMonth',[this,StatMonthName],[0,1]);" onkeyup="showCodeListKeyEx('ReportMonth',[this,StatMonthName],[0,1]);"><input class= codename name=StatMonthName>          
          	</TD>
    	</TR> 
    </table>
    	<hr/>

          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 

	     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保监会报表信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<hr/>
  	 <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入上传数据信息</td>
		</tr>
	 </table>
 	 <table class= common>
	    <TR  class= common>
	       <TD  class= title>
            模块类型
          </TD>  
          <TD class= input>
            <Input class="codeno" name=circreport_config ondblclick="return showCodeList('circreport_config',[this,circreport_configName,ImportConfigFile],[0,1,2]);" onkeyup="return showCodeListKey('circreport_config',[this,circreport_configName,ImportConfigFile],[0,1,2]);"><input class= codename name=circreport_configName>          
          </TD>
	      <TD class = title width=20%>
	        文件地址：
	      </TD>
	      <TD class = common width=80%>
	        <Input  class = common type="file" name=FileName>
	      </TD>	 
	   </TR>     
	</table>
		    <input type=hidden name=ImportFile>
	    <input type=hidden name=ImportConfigFile>
	 <input type="hidden" name= "SubMissionID" value= "">
  	 <input type="hidden" name= "MissionID" value= "">
  	 <input type="hidden" name= "StatYearHide" value= "">
  	 <input type="hidden" name= "StatMonHide" value= "">
 
      <INPUT VALUE="投资数据导入" class= common TYPE=button onclick = "autochk();"> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
