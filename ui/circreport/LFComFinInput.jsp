<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2004-09-14 15:49:05
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LFComFinInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LFComFinInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LFComFinSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg>
        		 财务机构信息表基本信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLFComFin1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      财务管理机构编码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ComFin >
    </TD>
    <TD  class= title>
      机构名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Name >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      成立日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FoundDate >
    </TD>
    <TD  class= title>
      停业标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      停业日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndDate >
    </TD>
    <TD  class= title>
      机构属性
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Attribute >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      机构标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Sign >
    </TD>
    <TD  class= title>
      统计标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=calFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      机构地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Address >
    </TD>
    <TD  class= title>
      机构邮编
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ZipCode >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      机构电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Phone >
    </TD>
    <TD  class= title>
      机构传真
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Fax >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      EMail
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EMail >
    </TD>
    <TD  class= title>
      网址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=WebAddress >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      主管人姓名
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SatrapName >
    </TD>
    <TD  class= title>
      总公司正式批示时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ApproveDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      工商批准营业时间
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PassDate >
    </TD>
  </TR>
</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
