//程序名称：CircMidQuery.js
//程序功能：保监会导入数据查询 
//创建日期：2002-06-19 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    //alert(content); 
    content="无校验错误信息！"
    alert(content);
  }

}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryAddClick()
{
	initPolStatuGrid();
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	var tDisType=fm.all('DisType').value;
	// 书写SQL语句
	k++;
	var strSQL = "";
	if(tDisType=="CW")
	{
		strSQL = "select LFFinXml.ItemCode,LFFinXml.ComCode,'',LFFinXml.StatValue,'2',Year(LFFinXml.reportdate),Month(LFFinXml.reportdate) from LFFinXml  where "+k+" = "+k
				 + getWherePart( 'LFFinXml.ItemCode','itemcode' )   
				 +" and Year(LFFinXml.reportdate)=" +fm.all('StatYear').value
				 +" and Month(LFFinXml.reportdate)=" +fm.all('StatMon').value
				 + getWherePart( 'LFFinXml.ComCode','ManageCom' )
				 + " order by LFFinXml.ComCode,LFFinXml.ItemCode";
     }
     else if(tDisType=="RL")
	{
		strSQL = "select ItemCode,ComCodeISC,'',StatValue,RepType,StatYear,StatMon from LFRLXml  where "+k+" = "+k
				 + getWherePart( 'LFRLXml.ItemCode','itemcode' )         
				 +" and StatYear=" +fm.all('StatYear').value
				 +" and StatMon=" +fm.all('StatMon').value
				 + getWherePart( 'ComCodeISC','ManageCom' )
				 + " order by ComCodeISC,ItemCode";
     }
     else if(tDisType=="TZ")
	{
		strSQL = "select ItemCode,ComCode,'',StatValue,RepType,StatYear,StatMon from LFTZXml  where "+k+" = "+k
		          + getWherePart( 'LFTZXml.ItemCode','itemcode' )         
				 +" and StatYear=" +fm.all('StatYear').value
				 +" and StatMon=" +fm.all('StatMon').value
				 + getWherePart( 'ComCode','ManageCom' )
				 + " order by ComCode,ItemCode";
     }
    else if(tDisType=="JS")
	{
		strSQL = "select ItemCode,ComCode,'',StatValue,RepType,StatYear,StatMon from LFActuaryXml  where "+k+" = "+k
		         + getWherePart( 'LFActuaryXml.ItemCode','itemcode' )       
				 +" and StatYear=" +fm.all('StatYear').value
				 +" and StatMon=" +fm.all('StatMon').value
				 + getWherePart( 'ComCode','ManageCom' )
				 + " order by ComCode,ItemCode";
     }
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有满足条件数据！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

function getStatus()
{
  var i = 0;
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,*,0,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}