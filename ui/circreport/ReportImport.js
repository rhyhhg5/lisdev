//               该文件中包含客户端需要处理的函数和事件

var mDebug="1";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
window.onfocus=myonfocus;
var ImportPath;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var ImportConfigFile = fm.all('ImportConfigFile').value;
  if(fm.all('ImportConfigFile').value=="")
  {
  	alert("请选则上传文件数据类型！");
  		return ;
  }
  
    if(fm.all('FileName').value=="")
  {
  	alert("请上传Excel文件！");
  	return ;
  }
  getImportPath();
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
	//showSubmitFrame(mDebug);
	//divImport.style.display='none';
	ImportFile = fm.all('FileName').value;
	parent.fraInterface.fm.action = "./ReportImportChk.jsp?ImportPath="+ImportPath+"&ImportFile="+ImportFile+"&ImportConfigFile="+ImportConfigFile;
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Result )
{
  showInfo.close();               
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在AutoSql.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function getImportPath ()
{
		// 书写SQL语句
	var strSQL = "";

	strSQL = "select SysvarValue from ldsysvar where sysvar ='TranDataPath'";
			 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	alert("未找到上传路径，请到ldsysvar表查看sysvar ='TranDataPath'的数据记录信息");
    return;
	}
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  ImportPath = turnPage.arrDataCacheSet[0][0];
}
