//程序名称：ReportCWImport.js
//程序功能：保监会报表财务数据导入
//创建日期：2004-07-08 11:10:36
//创建人  :ck
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass();
turnPage.pageLineNum += 7;

//初始化mulline中的内容
//function checkData() {
//	if((fm.all('StartDate').value == null || fm.all('StartDate').value == "")){
//		alert("请输入开始日期!");
//		return false;
//	}
//	if((fm.all('EndDate').value == null || fm.all('EndDate').value == "")){
//		alert("请输入结束日期!");
//		return false;
//	}
//	var startdate = fm.all("StartDate").value;
//	var enddate = fm.all("EndDate").value;
//	var flag = compareDate(startdate, enddate);
//	if (flag == 1) {
//		alert("开始日期不能晚于结束日期");
//		return false;
//	}
//	if(!verifyInput()){
//		alert("输入日期类型校验失败");
//		return false;
//	}
//	if((startdate != null && enddate != "") 
//			&& (startdate != null && enddate != ""))
//		{
//			if(dateDiff(startdate,enddate,"M") > 1)
//			{
//				alert("提数统计最多为一个月！");
//				return false;
//			}
//		}
//	
//	return true;
//}
function submitForm() {
		var showStr = "\u6b63\u5728\u6267\u884c\u63d0\u53d6\u6570\u636e\u64cd\u4f5c\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit();
	
}
/*
function afterSubmit(flag, Content) {	//返回提取数据执行结果
	if (flag == "fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}
*/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {     
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

