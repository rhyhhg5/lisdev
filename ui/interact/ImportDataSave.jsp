<%@ page contentType="text/html; charset=GBK" import="com.sinosoft.lis.xpath.*" import="java.io.*"%>
<%@page import="javax.xml.parsers.*"%>
<%@page import="org.w3c.dom.*"%>
<%@page import="java.net.*"%>
<%@page import="java.util.*"%>
<%@page import="org.xml.sax.InputSource"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.net.*"%>
<%@page import="com.sinosoft.jkxpt.datacatch.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	GlobalInput tG = (GlobalInput) session.getValue("GI");			//GlobalInput
	String StartDate = request.getParameter("StartDate");			
	String EndDate = request.getParameter("EndDate");
	String flag = "";												//执行结果返回标志
	String Content = "";											//返回结果内容
	CErrors tError = null;
	if(!checkInputInfo(StartDate,EndDate,Content)){
		flag = "fail";
	}else{//通过校验准备向UI提交数据
		VData vData = new VData();
		vData.add(tG);
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("StartDate",StartDate);
		transferData.setNameAndValue("EndDate",EndDate);

		vData.add(transferData);
		ImportDataCatchUI tImportDataCatchUI = new ImportDataCatchUI();
		if(!tImportDataCatchUI.submitData(vData, "")){
			tError = tImportDataCatchUI.mErrors;
			flag = "fail";
			Content = "历史数据导出失败，原因是：" + tError.getFirstError();
		}else{
			flag = "success";
			Content = "历史数据导出成功";
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>","<%=Content%>");
	</script>
</html>

<!-- 下面是方法定义 -->
<%!
	public static boolean checkInputInfo(String StartDate, String EndDate, String content){
		if(StrTool.cTrim(StartDate).equals("")){
			content="开始时间参数不能为空";
			return false;
		}
		if(StrTool.cTrim(EndDate).equals("")){
			content="结束时间参数不能为空";
			return false;
		}
		return true;
	}
%>
