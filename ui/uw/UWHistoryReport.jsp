<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：UWHistoryReport.jsp
//程序功能：既往契调件查询
//创建日期：2012-10-19
//创建人  ：张成轩
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	
	  String tFlag = "";

  tContNo = request.getParameter("ContNo");
  tFlag = request.getParameter("Flag");
  String tLoadFlag = request.getParameter("LoadFlag");

  System.out.println("ContNo:"+tContNo);
  System.out.println("Flag:"+tFlag);
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var customers = "";                   //生调人选
</script>
<head >
<title> </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="UWHistoryReport.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UWHistoryReportInit.jsp"%>
  
</head>
<body  onload="initForm('<%=tContNo%>','<%=tLoadFlag%>');" >
  <form method=post name=fm target="fraSubmit">
  	 <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
    			 契调待回销队列
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCRReport" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanLCRReportGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    </div>

    <div id="divHidden" style="display :'none'">
	 <table class= common align=center>
    	<tr class= common>
			<td  class= title> 姓名 </td>
			<td  class= input>
				<Input class=codeNo name=CustomerName ondblclick="showCodeListEx('CustomerName',[this,CustomerNameName,CustomerNo],[0,1,2],'', '', '', true);" onkeyup="showCodeListKeyEx('CustomerName',[this,CustomerNameName,CustomerNo],[0,1,2],'', '', '', true);"><input class=codename name=CustomerNameName readonly=true>  </td>   	
			<td  class= title> 客户号 </td>
			<td  class= input><Input readonly class=common name=CustomerNo>  </td>   	
		</tr>
    </table>
  </div>
  
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
    			 契约调查项目明细报告
    		</td>
    	</tr>
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanQuestGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    </div>
<BR>
     <table class=common>
         <TR class= common> 
           <TD  class= common> 契调结果 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Contente" cols="120" rows="3" class="common" >
             </textarea>
           </TD>
         </TR>
      </table>
       <table class=common>
         <TR  class= common> 
           <TD  class= common> 契调结论 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="ReplyContente" cols="120" rows="3" class="common" >
             </textarea>
           </TD>
         </TR>
      </table>
  <p> 
    <!--读取信息-->
    <input type= "hidden" name= "Flag" value="">
    <input type= "hidden" name= "ContNo" value= "">
    <input type= "hidden" name= "Type" value="">
    <input type= "hidden" name= "PrtSeq" value="">
    <input type= "hidden" name= "customer" value="">   
    <input type=hidden id="fmAction" name="fmAction">
    <INPUT  type= "hidden" class= Common name=RReportItemCode>
    <INPUT  type= "hidden" class= Common name=RReportItemName>
    <INPUT  type= "hidden" class= Common name=RRItemContent>
    <INPUT  type= "hidden" class= Common name=RRItemResult>
    <INPUT  type= "hidden" class= Common name=LoadFlag>
  </p>
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>