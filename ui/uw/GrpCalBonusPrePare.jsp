 <%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：CutBonus.jsp
//程序功能：分红处理
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<%
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="GrpCalBonusPrePare.js"></SCRIPT>
    <%@include file="GrpCalBonusPrePareInit.jsp"%>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>分红处理</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./GrpCalBonusPrePareChk.jsp">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入计算参数：</td>
		</tr>
	</table>
    <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>
	     	红利分配会计年度	     
	      </TD> 
	      <TD  class= input>
	          <Input class=common name=FiscalYear value="2003">
	      </TD>
          <TD  class= title>
	     	分红比率	     
	      </TD>
	        <TD  class= input>
	          <Input class=common  name=AssignRate value="0.7">
	        </TD>  
        </TR>
        <TR  class= common>
		  <TD  class= title>
	     	实际投资收益率	     
	      </TD>
	      <TD  class= input>
	         <Input class=common name=ActuRate value="0.05" >
	      </TD>
			<TD  class= title>
	     	默认保证年收益率	     
	        </TD>
	        <TD  class= input>
	          <Input  class=common name=EnsuRateDefault value="0.025">
	        </TD>     		
		</TR>
		<TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="code" name=RiskCode value='212401' ondblclick="return showCodeList('RiskCode',[this]);" onkeyup="return showCodeListKey('RiskCode',[this]);">
          </TD>		
		</TR> 	 		  
    </table>
    
     <!-- 
        <td class= title>
     查询会计年度内需要分红的保单(请填入会计年度)
    </td>
     --> 

    <INPUT VALUE="查询" class=common TYPE=button onclick="easyQueryClick();"> 	
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class=common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=common TYPE=button onclick="getLastPage();"> 					
  	</div>      

   <INPUT VALUE="保存" class=common TYPE=button onclick="submitForm();" name='save'> 
   
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
