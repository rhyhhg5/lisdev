<%@page contentType="text/html;charset=gb2312"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：UWHistoryHealth.jsp
	//程序功能：既往体检件查询
	//创建日期：2012-10-19
	//创建人  ：张成轩
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="./UWHistoryHealth.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="UWHistoryHealthInit.jsp"%>
		<title>承保体检资料录入</title>

	</head>
	<body
		onload="initForm('<%=tContNo%>','<%=tMissionID%>','<%=tSubMissionID%>','<%=tPrtNo%>','<%=tLoadFlag%>');">
		<form method=post name=fm target="fraSubmit"
			action="./UWManuHealthChk.jsp">
			<!-- 非列表 -->
			<table>
				<TR class=common>
					<TD class=title id="ff" style="display: 'none'">
						合同号码
					</TD>
					<TD class=input id="ff" style="display: 'none'">
						<Input class="readonly" name=ContNo>
					</TD>
				</TR>

			</table>

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
							OnClick="showPage(this,divMainUWSpec1);">
					</td>
					<td class=titleImg>
						体检履历
					</td>
				</tr>
			</table>
			<Div id="divMainUWSpec1" style="display: ''">
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanMainHealthGrid"> </span>
						</td>
					</tr>
				</table>
			</div>

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
							OnClick="showPage(this,divUWSpec1);">
					</td>
					<td class=titleImg>
						体检项目
					</td>
				</tr>
			</table>
			<Div id="divUWSpec1" style="display: ''">
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanHealthGrid"> </span>
						</td>
					</tr>
				</table>
				<Div id="divPage" align=center style="display: ''">
					<INPUT VALUE="首  页" class=cssButton TYPE=button
						onclick="getFirstPage();">
					<INPUT VALUE="上一页" class=cssButton TYPE=button
						onclick="getPreviousPage();">
					<INPUT VALUE="下一页" class=cssButton TYPE=button
						onclick="getNextPage();">
					<INPUT VALUE="尾  页" class=cssButton TYPE=button
						onclick="getLastPage();">
				</Div>
				<table>
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
								OnClick="showPage(this,divUWSpec1);">
						</td>
						<td class=titleImg>
							疾病结果
						</td>
					</tr>
				</table>
				<Div id="divUWDis" style="display: ''">
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanDisDesbGrid"> </span>
							</td>
						</tr>
					</table>
				</div>

				<table class=common>
					<TR class=common>
						<TD class=common>
							其他体检信息
						</TD>
					</TR>
					<TR class=common>
						<TD class=common>
							<textarea name="Note" cols="120" rows="3" class="common"></textarea>
						</TD>
					</TR>
				</table>
				<INPUT type="hidden" class=Common name=MissionID value="">
				<!-- 工作流任务编码 -->
				<INPUT type="hidden" class=Common name=SubMissionID value="">
				<INPUT type="hidden" class=Common name=PrtNo value="">
				<INPUT type="hidden" class=Common name=PrtSeq value="">
				<INPUT type="hidden" class=Common name=LoadFlag value="">
				<INPUT type="hidden" class=Common name=InsureNo value="">
				<!--读取信息-->
		</form>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</body>
</html>