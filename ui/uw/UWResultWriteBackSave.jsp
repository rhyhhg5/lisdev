<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWResultWriteBackSave.jsp
//程序功能：核保通知书回销
//创建日期：2005-9-27 15:06
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
CErrors tError = null;
String FlagStr = "Fail";
String Content = "";
GlobalInput tG = (GlobalInput)session.getValue("GI");

LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();

String tPolNo=request.getParameter("PolNo");
String tUWIdea=request.getParameter("UWIdea");

String tPassFlag=request.getParameter("PassFlag");
String tSugUWFlag = request.getParameter("SugUWFlag");
String tLoadFlag = request.getParameter("LoadFlag");
String tCustomerIdea = request.getParameter("CustomerIdea");

System.out.println("===============------"+tLoadFlag);
tLCUWMasterSchema.setPolNo(tPolNo);
tLCUWMasterSchema.setProposalNo(tPolNo);
tLCUWMasterSchema.setPassFlag(tPassFlag);
tLCUWMasterSchema.setUWIdea(tUWIdea);
tLCUWMasterSchema.setSugPassFlag(tSugUWFlag);
tLCUWMasterSchema.setCustomerReply(tCustomerIdea);
if(StrTool.cTrim(tCustomerIdea).equals("0")){
	tLCUWMasterSchema.setDisagreeDeal(tPassFlag);
}

TransferData mTransferData = new TransferData();
// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tG);
tVData.add(tLCUWMasterSchema);
tVData.add(mTransferData);

ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
try
{
  System.out.println("this will save the data!!!");
  tManuUWRiskSaveUI.submitData(tVData,"");
}
catch(Exception ex)
{
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}

if (!FlagStr.equals("Fail"))
{
  tError = tManuUWRiskSaveUI.mErrors;
  if (!tError.needDealError())
  {
    Content = " 保存成功! ";
    FlagStr = "Succ";
  }
  else
  {
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
