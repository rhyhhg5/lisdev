//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


/*********************************************************************
 *  集体投保单复核的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	//showInfo.close();
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	if( FlagStr == "Fail" )
	{             
		
		showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		// 刷新查询结果
		showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		easyQueryClick();		
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpPolGrid.getSelNo();
	var i = 0;
	var checkFlag = 0;
	var cGrpProposalNo = "";
	var cGrpPrtNo = "";
	var cflag = "2";
	  
	  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
	    if (GrpPolGrid.getSelNo(i)) { 
	      checkFlag = GrpPolGrid.getSelNo();
	      break;
	    }
	  }
	 
	  if (checkFlag) { 
	  	cGrpProposalNo = GrpPolGrid.getRowColData(checkFlag - 1, 1);
	  	cGrpPrtNo = GrpPolGrid.getRowColData(checkFlag - 1, 2);
	  }
	  else {
	    alert("请先选择一条保单信息！"); 
	    return false;
	  }
	  if(trim(cGrpPrtNo)==''||trim(cGrpProposalNo)=='')
	  {
	    alert("请先选择一条有效保单信息！"); 
	    return false;
	  }
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	 window.open("./GrpQuestModifyDetailMain.jsp?GrpProposalNo="+cGrpProposalNo+"&GrpPrtNo="+cGrpPrtNo,"window1");
	
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = GrpPolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

// 查询按钮
var queryBug = 1;
function easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid();
	if (contNo==null||contNo=='')
		contNo = "00000000000000000000";
	//alert(contNo);
	// 书写SQL语句
	var strSQL = "";
	var strSQL = "select LCGrpPol.GrpProposalNo,LCGrpPol.PrtNo,LCGrpPol.RiskCode,LCGrpPol.GrpName,LCGrpPol.Cvalidate,LCGrpPol.peoples,LCGrpPol.prem from LCGrpPol where " + queryBug + "=" + queryBug
    				 + " and LCGrpPol.AppFlag='0' "                 
    				 + " and LCGrpPol.Operator ='" + operator + "'"
    				 + " and LCGrpPol.ManageCom like '" + manageCom + "%%'"    				   				 
 				   	 + " and LCGrpPol.approveFlag = '2'"
    				 + " and LCGrpPol.uwflag not in ('1','4','9')"
    				 + " and LCGrpPol.riskcode in(select riskcode from lmriskapp where subriskflag='M')"
    				 + " and LCGrpPol.GrpProposalNo not in (select Proposalno from LCIssuePol where replyman is null and backobjtype in ('4'))"
    				 + " order by LCGrpPol.MakeDate ";
    			
                    // + getWherePart( 'RiskVersion' );
                   //alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpPolGrid();
		//HZM 到此修改
		GrpPolGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpPolGrid.loadMulLine(GrpPolGrid.arraySave);		
		//HZM 到此修改
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			GrpPolGrid.addOne();
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
			  GrpPolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//GrpPolGrid.delBlankLine();
	} // end of if
}

/*********************************************************************
 *  团体单的问题件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestQuery()
{
	  var i = 0;
	  var checkFlag = 0;
	  var cGrpProposalNo = "";
	  var cflag = "3";
	  
	  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
	    if (GrpPolGrid.getSelNo(i)) { 
	      checkFlag = GrpPolGrid.getSelNo();
	      break;
	    }
	  }
	 
	  if (checkFlag) { 
	  	cGrpProposalNo = GrpPolGrid.getRowColData(checkFlag - 1, 1);
	  }
	  else {
	    alert("请先选择一条保单信息！"); 
	    return false;
	  }
	  if(cGrpProposalNo==null||trim(cGrpProposalNo)=='')
	  {
	    alert("请先选择一条有效保单信息！"); 
	    return false;
	  }
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	 window.open("../uw/QuestQueryMain.jsp?ProposalNo1="+cGrpProposalNo+"&Flag="+cflag,"window1");
	
}


/*********************************************************************
 *  复核修改完成后确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ModifyMakeSure(){
	  var i = 0;
	  var checkFlag = 0;
	  var cGrpProposalNo = "";
	  var cGrpPrtNo="";
	  var cflag = "2";
	  
	  for (i=0; i<GrpPolGrid.mulLineCount; i++) {
	    if (GrpPolGrid.getSelNo(i)) { 
	      checkFlag = GrpPolGrid.getSelNo();
	      break;
	    }
	  }
	 
	  if (checkFlag) { 
	  	cGrpProposalNo = GrpPolGrid.getRowColData(checkFlag - 1, 1);
	  	cGrpPrtNo = GrpPolGrid.getRowColData(checkFlag - 1, 2);
	  }
	  else {
	    alert("请先选择一条保单信息！"); 
	    return false;
	  }
	  if(trim(cGrpPrtNo)==''||trim(cGrpProposalNo)=='')
	  {
	    alert("请先选择一条有效保单信息！"); 
	    return false;
	  }
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	 window.open("./GrpQuestModifyMakeSure.jsp?GrpProposalNo="+trim(cGrpProposalNo)+"&GrpPrtNo="+trim(cGrpPrtNo),"window1");
	
	}