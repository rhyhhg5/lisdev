<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWNotePadInit.jsp
//程序功能：新契约信息修改
//创建日期：2002-09-24 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
 <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
 
           
<script language="JavaScript">

function initForm() {
  try {
  initHidden();
	initInsuredGrid();
	  initBnfGrid();
	easyQueryClick();
  
  }
  catch(re) {
    alert("ProposalInfoModifyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initHidden()
{
   fm.ContNo.value = ContNo;
   fm.PrtNo.value  = prtNo;   
}

function initInsuredGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="被保险人客户号";          		//列名
      iArray[1][1]="80px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="姓名";         			//列名
      iArray[2][1]="60px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="性别";      	   		//列名
      iArray[3][1]="40px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              	//是否允许输入,1表示允许，0表示不允许
      iArray[3][4]="Sex"; 


      iArray[4]=new Array();
      iArray[4][0]="出生日期";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0; 
      
      iArray[5]=new Array();
      iArray[5][0]="与第一被保险人关系";      	   		//列名
      iArray[5][1]="100px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=0; 
      iArray[5][4]="Relation";              	        //是否引用代码:null||""为不引用
      iArray[5][9]="与主被保险人关系|code:Relation&NOTNULL";
      //iArray[5][18]=300;
      
      iArray[6]=new Array();
      iArray[6][0]="客户内部号码";      	   		//列名
      iArray[6][1]="80px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0; 
      iArray[6][10]="MutiSequenceNo";
      iArray[6][11]="0|^1|第一被保险人 ^2|第二被保险人 ^3|第三被保险人";
            

      InsuredGrid = new MulLineEnter( "fm" , "InsuredGrid" ); 
      //这些属性必须在loadMulLine前
      InsuredGrid.mulLineCount = 0;   
      InsuredGrid.displayTitle = 1;
      InsuredGrid.canSel =1;
      InsuredGrid. selBoxEventFuncName ="getInsuredDetail" ;     //点击RadioBox时响应的JS函数
      InsuredGrid. hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      InsuredGrid. hiddenSubtraction=1;
      InsuredGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}

//被保人险种信息列表初始化
function initPolGrid(){
    var iArray = new Array();
      
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		        //列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保险单险种号码";         		//列名
      iArray[1][1]="60px";            		        //列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]=" 险种代码";         		//列名
      iArray[2][1]="60px";            		        //列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
           
      
      iArray[3]=new Array();
      iArray[3][0]=" 保费(元)";         		//列名
      iArray[3][1]="60px";            		        //列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="保额(元)";         		//列名
      iArray[4][1]="60px";            		        //列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[5]=new Array();
      iArray[5][0]="被保人姓名";         		//列名
      iArray[5][1]="60px";            		        //列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许         
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.canSel =1;
      PolGrid. selBoxEventFuncName ="getBnfDetail";
      PolGrid. hiddenPlus=1;
      PolGrid. hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);  
    }
    catch(ex) {
      alert(ex);
    }
}

//受益人信息
function initBnfGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";		//列宽
    iArray[0][2]=10;			//列最大值
    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="险种代码"; 	//列名
    iArray[1][1]="40px";		//列宽
    iArray[1][2]=30;			//列最大值
    iArray[1][3]=0;			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();                                                   
    iArray[2][0]="被保人姓名";         		//列名                        
    iArray[2][1]="45px";            		        //列宽                        
    iArray[2][2]=60;            			//列最大值                              
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
       
    iArray[3]=new Array();
    iArray[3][0]="受益人姓名"; 	//列名
    iArray[3][1]="45px";		//列宽
    iArray[3][2]=30;			//列最大值
    iArray[3][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[3][9]="姓名|notnull&len<=20";//校验

    iArray[4]=new Array();
    iArray[4][0]="证件类型"; 		//列名
    iArray[4][1]="40px";		//列宽
    iArray[4][2]=40;			//列最大值
    iArray[4][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[4][4]="IDType";
    iArray[4][9]="证件类型|notnull&code:IDType";

    iArray[5]=new Array();
    iArray[5][0]="证件号码"; 		//列名
    iArray[5][1]="120px";		//列宽
    iArray[5][2]=80;			//列最大值
    iArray[5][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[5][9]="证件号码|notnull&len<=20";

    iArray[6]=new Array();
    iArray[6][0]="与被保人关系"; 	//列名
    iArray[6][1]="60px";		//列宽
    iArray[6][2]=60;			//列最大值
    iArray[6][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[6][4]="Relation";
    iArray[6][9]="与被保人关系|code:Relation";

    iArray[7]=new Array();
    iArray[7][0]="受益比例"; 		//列名
    iArray[7][1]="40px";		//列宽
    iArray[7][2]=40;			//列最大值
    iArray[7][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[7][9]="受益比例|Decimal&len<=10";

    iArray[8]=new Array();
    iArray[8][0]="受益顺序"; 		//列名
    iArray[8][1]="40px";		//列宽
    iArray[8][2]=40;			//列最大值
    iArray[8][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[8][4]="OccupationType";
    iArray[8][9]="受益顺序|code:OccupationType";
    
    iArray[9]=new Array();
    iArray[9][0]="受益人类别"; 		//列名
    iArray[9][1]="60px";		//列宽
    iArray[9][2]=40;			//列最大值
    iArray[9][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[9][4]="BnfType";
    iArray[9][9]="受益人类别|notnull&code:BnfType";
   
    iArray[10]=new Array();
    iArray[10][0]="保险单险种号码"; 		//列名
    iArray[10][1]="60px";		//列宽
    iArray[10][2]=40;			//列最大值
    iArray[10][3]=3;			//是否允许输入,1表示允许，0表示不允许
    

    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" );
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 0;
    BnfGrid.displayTitle = 1;
  //  BnfGrid.addEventFuncName="setFocus";
 
    BnfGrid.addEventFuncName="AddOne";
   BnfGrid.loadMulLine(iArray);		
    //这些操作必须在loadMulLine后面
    //BnfGrid.setRowColData(0,8,"1");
    //BnfGrid.setRowColData(0,9,"1");
  }
  catch(ex)
  {
    alert("在ProposalInit.jsp-->initBnfGrid函数中发生异常:初始化界面错误!");
  }
}
</script>


