//程序名称：FinFeeSure.js
//程序功能：到帐确认
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var k = 0;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
    
    //showInfo.close(); 
    alert(content); 
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    alert(content);
    
    //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showInfo.close();  
    
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	k++;
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select distinct a.TempFeeNo, a.ChequeNo, a.PayMode, a.PayMoney, " 
				 + "       a.AppntName, a.MakeDate, b.EnterAccDate, '', b.TempFeeType " 
	       + "from LJTempFeeClass a, LJTempFee b "
	       + "where "+k+" = "+k+" "
	       + "and a.tempfeeno = b.tempfeeno "
				 + "and a.EnterAccDate is null "
				 + "and a.ManageCom like '" + manageCom + "%%'"
				 + " and a.paymode <> '4' "
				 + " and not exists (select 1 from ljspayb where getnoticeno=a.tempfeeno and DealState in ('2','3'))"
				 + getWherePart( 'a.AppntName','AppntName')
				 + getWherePart( 'a.ManageCom','ManageCom')
				 + getWherePart( 'a.TempFeeNo','TempFeeNo')
				 + getWherePart( 'a.PayMode','PayMode')
				 + getWherePart( 'a.ChequeNo','ChequeNo');
	//fm.sqltext.value = 	strSQL;		 
	//查询金额合计
  var strSql = "select sum(b.PayMoney) " + strSQL.substring(strSQL.indexOf("from"));
  var arrResult = easyExecSql(strSql);
  fm.sumMoney.value = arrResult;
  	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有未到帐数据！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);	 
				 
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  return true;
}

function autochk()
{
  var haveCheck = false;
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getChkNo(i)) {
      haveCheck = true;
      break;
    }
  }
  
  if (haveCheck) {
    var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
    //showSubmitFrame(mDebug);
    fm.submit(); //提交
  }
  else {
    alert("请先选择一条暂交费信息，在选择框中打钩！");
  }
}

function insertCurrentDate() {
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getChkNo(i)) {
      PolGrid.setRowColData(i, 7, currentDate);
    }
  }
}