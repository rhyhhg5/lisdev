var arrDataSet
var showInfo;
var turnPage = new turnPageClass();

// 查询按钮
function easyQueryClick()
{
	
    initPolGrid();
    
    if(!verifyInput2())
    {
		return false;
    }
    //修改日期:2014-10-31   杨阳
    //业务员代码显示laagent表中的GroupAgentCode，
      //契约
    	if(fm.PiType.value== 2)
    		{ 
    		strSQL = "select distinct  lcc.PrtNo,lcc.PrtNo,lcc.AppntName,CHAR(lcc.Prem),( select getUniteCode(lcc.agentcode) from dual),lagt.Name,"
            + "lcc.ManageCom,lwm.DefaultOperator,lcc.Operator,lcc.ContType,lcc.SaleChnl,lwm.Activityid "
            + " from lccont lcc, lwmission lwm, laagent lagt "
            + "where lcc.PrtNo = lwm.MissionProp1 and lcc.AgentCode = lagt.AgentCode "
            + "and lwm.Activityid = '0000001100' and lcc.ContType = '1' " 
            + "and (lwm.defaultoperator is null or trim(lwm.defaultoperator)='') "
            + getWherePart('lcc.AppntName','AppntName')
            + getWherePart('lcc.PrtNo','Otherno')
            + getWherePart('lcc.ManageCom','ManageCom','like')
            + " order by lcc.ManageCom,lcc.PrtNo ";
            }
        //保全
        if(fm.PiType.value== 3)
    		{ 
    		strSQL = "select distinct  lpe.EdorNo,lcc.PrtNo,lcc.AppntName,CHAR(lcc.Prem),( select getUniteCode(lcc.agentcode) from dual),lagt.Name,"
            + "lcc.ManageCom,lwm.DefaultOperator,lcc.Operator,lcc.ContType,lcc.SaleChnl,lwm.Activityid "
            + " from lccont lcc, lwmission lwm, laagent lagt ,lpedoritem lpe "
            + "where lpe.ContNo=lcc.Contno and  lwm.missionprop1=lpe.edoracceptno and lcc.AgentCode = lagt.AgentCode "
            + "and lwm.Activityid = '0000001180' and lcc.ContType = '1' "
            + "and (lwm.defaultoperator is null or trim(lwm.defaultoperator)='') "
            + getWherePart('lcc.AppntName','AppntName')
            + getWherePart('lpe.EdorNo','Otherno')
            + getWherePart('lcc.ManageCom','ManageCom','like')
            + " order by lcc.ManageCom,lpe.EdorNo ";
            }
        //理赔二核/理赔续保二核
        if(fm.PiType.value== 4)
       // distinct llc.Caseno, null,llc.CustomerName,null,null,null,llc.MngCom,lwm.DefaultOperator,null,null,null,null
       //distinct llc.Caseno, lcc.PrtNo,llc.CustomerName,lcc.Prem,( select getUniteCode(lcc.agentcode) from dual),lagt.Name,"
       //   + "llc.MngCom,lwm.DefaultOperator,lcc.Operator,lcc.ContType,lcc.SaleChnl,lwm.Activityid "
    		{   
    		strSQL = "select  distinct llc.Caseno, '',llc.CustomerName,'','','',llc.MngCom,lwm.DefaultOperator,'','','',lwm.Activityid"
            + " from  lwmission lwm,llcase llc "
            + "where  lwm.missionprop1=llc.caseno "
            + "and lwm.Activityid = '0000001182' "
            + "and (lwm.defaultoperator is null or trim(lwm.defaultoperator)='') "
            + getWherePart('llc.Caseno','Otherno')
            + getWherePart('llc.CustomerName','AppntName')
            + getWherePart('llc.MngCom','ManageCom','like')
            +" union "
            + "select distinct  llc.Caseno,lcc.PrtNo,lcc.AppntName,CHAR(lcc.Prem),( select getUniteCode(lcc.agentcode) from dual),lagt.Name,"
            + "lcc.ManageCom,lwm.DefaultOperator,lcc.Operator,lcc.ContType,lcc.SaleChnl,lwm.Activityid "
            + " from lccont lcc, lwmission lwm, laagent lagt ,llcase llc "
            + "where lwm.missionprop2=lcc.Contno and  lwm.missionprop5=llc.caseno and lcc.AgentCode = lagt.AgentCode "
            + "and lwm.Activityid = '0000001181' and lcc.ContType = '1' "
            + "and (lwm.defaultoperator is null or trim(lwm.defaultoperator)='') "
            + getWherePart('lcc.AppntName','AppntName')
            + getWherePart('llc.caseno','Otherno')
            + getWherePart('lcc.ManageCom','ManageCom','like')
            }
        //全部
        if(fm.PiType.value== 1)
       
    		{   
    		strSQL = "select distinct  lcc.PrtNo as Otherno,lcc.PrtNo,lcc.AppntName,CHAR(lcc.Prem),( select getUniteCode(lcc.agentcode) from dual),lagt.Name,"
            + "lcc.ManageCom,lwm.DefaultOperator,lcc.Operator,lcc.ContType,lcc.SaleChnl,lwm.Activityid "
            + " from lccont lcc, lwmission lwm, laagent lagt "
            + "where lcc.PrtNo = lwm.MissionProp1 and lcc.AgentCode = lagt.AgentCode "
            + "and lwm.Activityid = '0000001100' and lcc.ContType = '1' "
            + "and (lwm.defaultoperator is null or trim(lwm.defaultoperator)='') "
            + getWherePart('lcc.AppntName','AppntName')
            + getWherePart('lcc.PrtNo','Otherno')
            + getWherePart('lcc.ManageCom','ManageCom','like')
            +" union "
            +"select distinct  lpe.EdorNo as Otherno,lcc.PrtNo,lcc.AppntName,CHAR(lcc.Prem),( select getUniteCode(lcc.agentcode) from dual),lagt.Name,"
            + "lcc.ManageCom,lwm.DefaultOperator,lcc.Operator,lcc.ContType,lcc.SaleChnl,lwm.Activityid "
            + " from lccont lcc, lwmission lwm, laagent lagt ,lpedoritem lpe "
            + "where lpe.ContNo=lcc.Contno and  lwm.missionprop1=lpe.edoracceptno and lcc.AgentCode = lagt.AgentCode "
            + "and lwm.Activityid = '0000001180' and lcc.ContType = '1' "
            + "and (lwm.defaultoperator is null or trim(lwm.defaultoperator)='') "
            + getWherePart('lcc.AppntName','AppntName')
            + getWherePart('lpe.EdorNo','Otherno')
            + getWherePart('lcc.ManageCom','ManageCom','like')
            +" union  "
            +"select distinct  llc.Caseno as Otherno,lcc.PrtNo,lcc.AppntName,CHAR(lcc.Prem),( select getUniteCode(lcc.agentcode) from dual),lagt.Name,"
            + "lcc.ManageCom,lwm.DefaultOperator,lcc.Operator,lcc.ContType,lcc.SaleChnl,lwm.Activityid "
            + " from lccont lcc, lwmission lwm, laagent lagt ,llcase llc "
            + "where lwm.missionprop2=lcc.Contno and  lwm.missionprop5=llc.caseno and lcc.AgentCode = lagt.AgentCode "
            + "and lwm.Activityid = '0000001181' and lcc.ContType = '1' "
            + "and (lwm.defaultoperator is null or trim(lwm.defaultoperator)='') "
            + getWherePart('lcc.AppntName','AppntName')
            + getWherePart('llc.caseno','Otherno')
            + getWherePart('lcc.ManageCom','ManageCom','like')
            +" union  "
            + "select  distinct llc.Caseno as Otherno , '',llc.CustomerName,'','','',llc.MngCom,lwm.DefaultOperator,'','','',lwm.Activityid"
            + " from  lwmission lwm ,llcase llc "
            + "where   lwm.missionprop1=llc.caseno "
            + "and lwm.Activityid = '0000001182'  "
            + "and (lwm.defaultoperator is null or trim(lwm.defaultoperator)='') "
            + getWherePart('llc.CustomerName','AppntName')
            + getWherePart('llc.Caseno','Otherno')
            + getWherePart('llc.MngCom','ManageCom','like')
            }            
    
    
    
  /*  // 书写SQL语句
    var strSQL = "";
    
    var strS = "where lcc.PrtNo = lwm.MissionProp1 and lcc.AgentCode = lagt.AgentCode "
            + "and lwm.Activityid = '0000001100' and lcc.ContType = '1' "
            + "and (lwm.defaultoperator is null or trim(lwm.defaultoperator)='') "
            + getWherePart('lcc.PrtNo','PrtNo')
            + getWherePart('lcc.appntname','AppntName')
            + getWherePart('lcc.ManageCom','ManageCom','like');

        strSQL = "select lcc.ContNo,lcc.PrtNo,lcc.AppntName,lcc.Prem,( select getUniteCode(lcc.agentcode) from dual),lagt.Name,"
            + "lcc.ManageCom,lwm.DefaultOperator,lcc.Operator,lcc.ContType,lcc.SaleChnl "
            + " from lccont lcc, lwmission lwm, laagent lagt "
            + strS
            + " order by lcc.ManageCom,lcc.ContNo ";*/
    
    //fm.mSQL.value=strS;
    fm.all("mModifyUWOperator").value = "";
    fm.all("UserCodeName").value = "";
    
    turnPage.pageLineNum = 10;
    turnPage.strQueryResult  = easyQueryVer3(strSQL);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到要处理的业务数据！");
        return false;
    }
   
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
   
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 10);
    //调用MULTILINE对象显示查询结果
    	 displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
   /* 	 for( i=0;i<=turnPage.arrDataCacheSet.length;i++)
    	{
    			if(turnPage.arrDataCacheSet[i][12]=="1")
    			{
    				PolGrid.setRowColData(i,4,'')
    			}
    		
    	}
    */
   
   
}

//初始化查询数据
function Polinit()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) 
  {
  	if (PolGrid.getChkNo(i)) 
  	{
  		checkFlag = PolGrid.getChkNo();
		break;
	}
  }
  alert(checkFlag);
  if (checkFlag) 
  {
  	
  	var tContNo=PolGrid.getRowColData(checkFlag-1,1);
  	var tOtherno=PolGrid.getRowColData(checkFlag-1,1);
	var tPrtNo=PolGrid.getRowColData(checkFlag-1,2);
	var tNowUWOperator=PolGrid.getRowColData(checkFlag-1,8);
	var tOperator=PolGrid.getRowColData(checkFlag-1,9);
	var tContType=PolGrid.getRowColData(checkFlag-1,10);
	var tManageCom=PolGrid.getRowColData(checkFlag-1,7);

	
	fm.all('mContNo').value = tContNo;
	fm.all('mOtherno').value = tOtherno;
	fm.all('mPrtNo').value = tPrtNo;
	fm.all('mNowUWOperator').value = tNowUWOperator;
	fm.all('mOperator').value = tOperator;
	fm.all('mContType').value = tContType;
	fm.all('mPiType').value=fm.PiType.value;
	fm.all("modifybtn").disabled=false;
	fm.all("returnbtn").disabled=false;
  }
  else
  {
	alert("请先选择一条保单信息！"); 
  }
  
  
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		//如果提交失败，显示错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//如果提交成功，则根据已有条件重新查询需要处理的数据
		easyQueryClick();
	}
}

//修改保单核保师
function modifyUWOperator()
{
    if(fm.all("mModifyUWOperator").value == null || fm.all("mModifyUWOperator").value == "")
    {
        alert("请录入分配核保师");
        return;
    }
    
    if(!verifyInput2())
    {
		return false;
    }
    
    var haveCheck = false;
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getChkNo(i)) {
      haveCheck = true;
      break;
    }
  }
  
  if (haveCheck) {
    fm.all('fmtransact').value = "UPDATE";
    fm.all("mAction").value = "ModifyUWOperator";
    var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
    //showSubmitFrame(mDebug);
    fm.submit(); //提交
  }
  else {
    alert("请先选择一条要分配的保单信息，在选择框中打钩！");
  }
  
}

//将保单放回核保池
function returnUWPol()
{
	if(!verifyInput2())
    {
		return false;
    }
	
    fm.all('fmtransact').value = "UPDATE";
    fm.all("mAction").value = "ReturnUWPol";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.submit();
}

function afterCodeSelect( cCodeName, Field ){
    if(cCodeName=="comcode"){
       if(Field.value!=""||Field.vale!=null){
          PolGrid.clearData();
          fm.all("mModifyUWOperator").value = "";
          fm.all("UserCodeName").value = "";
       }
    }
    return true;
}
