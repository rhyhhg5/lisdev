<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：CutBonus.jsp
//程序功能：分红处理
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="CutBonus.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CutBonusInit.jsp"%>
  <title>分红处理</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./CutBonusChk.jsp">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            被保人
          </TD>
          <TD  class= input>
            <Input class= common name=AppntName >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input><Input class="codeno" name=ManageCom ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1]);"><input class= codename name = ManageComName> </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保单号码  
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo>
          </TD>
          <TD  class= title>
            保单类型
          </TD>
          <TD  class= input><Input class="codeno" name=PolType value="1" CodeData="0|^1|个人保单^2|集体保单" ondblclick="return showCodeListEx('PolType',[this,PolTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('PolType',[this,PolTypeName],[0,1]);"><input class= codename name = PolTypeName> </TD>
        </TR>
        <TR  class= common>
			<TD  class= title>
	     	会计年度	     
	        </TD> 
	        <TD  class= input>
	          <Input class=common name=FiscalYear>
	        </TD>
			<TD  class= title>
	     	领取方式	     
	        </TD>
	        <TD  class= input>
	          <Input class="codeno" name=LiveGetMode ondblclick="return showCodeList('LiveGetMode', [this,LiveGetModeName],[0,1]);" onkeyup="return showCodeListKey('LiveGetMode', [this,LiveGetModeName],[0,1]);"><input class= codename name = LiveGetModeName>
	        </TD>
		
		</TR> 
        <TR  class= common>
			<TD  class= title>
	     	红利应该分配日期	     
	        </TD>
	        <TD  class= input>
	          <Input class="coolDatePicker" dateFormat="short" name=GetDate>
	        </TD>
			<TD  class= title>
	     	红利实际分配日期	     
	        </TD>
	        <TD  class= input>
	          <Input class="coolDatePicker" dateFormat="short" name=AGetDate>
	        </TD>        		
		</TR>
        <TR  class= common>
			<TD  class= title>
	     	险种编码	     
	        </TD>
	        <TD  class= input>
	          <Input class="codeno" name=RiskCode ondblclick="return showCodeList('RiskCode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('RiskCode',[this,RiskCodeName],[0,1]);"><input class= codename name = RiskCodeName>
	        </TD>
			<TD  class= title>
	     	红利领取标记	     
	        </TD>
	        <TD  class= input>
            <Input class="codeno" name=BonusFlag value="0" CodeData="0|^0|未领取^1|已经领取" ondblclick="return showCodeListEx('BonusFlag',[this,BonusFlagName],[0,1]);" onkeyup="return showCodeListKeyEx('BonusFlag',[this,BonusFlagName],[0,1]);"><input class= codename name = BonusFlagName>
	        </TD>
	        		
		</TR> 		 		  
        <TR  class= common>		     
          <TD  class= input>
            <Input class=common name=RqStart type=hidden>
          </TD>
          <TD  class= input>
            <Input class=common name=RqEnd  type=hidden>
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" class=common TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 分红数据
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class=common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=common TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<p>
      <INPUT VALUE="领取分红确认" class=common TYPE=button name=getbonusbutton onclick = "autochk();"> 
  	</p>
   <p>
   <INPUT VALUE="查看红利分配错误日志表" class=common TYPE=button onclick="viewErrLog();"> 	
  </p>
  	
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
