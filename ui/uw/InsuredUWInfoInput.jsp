<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
  //程序名称：InsuredUWInfoInput.jsp
  //程序功能：被保人核保信息界面
  //创建日期：2005-01-06 11:10:36
  //创建人  ：HYQ
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput) session.getValue("GI");
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=tGI.ComCode%>";//记录登陆机构
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var SelNo = "<%=request.getParameter("SelNo")%>";	
	var InsuredNo = "<%=request.getParameter("InsuredNo")%>";
	var PrtNo = "<%=request.getParameter("PrtNo")%>";
	var OperatePos = "<%=request.getParameter("OperatePos")%>";
	var MissionID = "<%=request.getParameter("MissionID")%>";
	var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
	var PolNo = "<%=request.getParameter("PolNo")%>";
	//alert(PolNo);
	var SendFlag = "<%=request.getParameter("SendFlag")%>";
	
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="InsuredUWInfo.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT><%@include file="InsuredUWInfoInit.jsp"%>
<title>被保人信息</title>
</head>
<body onload="initForm();showRiskResult();initElementtype();">
<form method=post name=fm target="fraSubmit" action="./InsuredUWInfoChk.jsp">
<DIV id=DivLCInsured STYLE="display:''">
  <table class=common border=0 width=100%>
    <tr>
      <td class=titleImg align=center>被保人信息：</td>
    </tr>
  </table>
  <table class=common>
    <TR class=common>
      <TD class=title>客户号码</TD>
      <TD class=input>
        <Input class=common name=InsuredNo readonly elementtype=nacessary verify="客户号码|notnull&len<=20">
      </TD>
      <TD class=title>姓名</TD>
      <TD class=input>
        <Input class=common name=Name elementtype=nacessary readonly verify="被保险人姓名|notnull">
      </TD>
      <TD class=title>性别</TD>
      <TD class=input>
        <Input class=codeNo name=Sex readonly=true verify="被保险人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"><input class=codename name=SexName readonly=true elementtype=nacessary>
      </TD>
      <TD CLASS=title>被保人年龄</TD>
      <TD CLASS=input COLSPAN=1>
        <Input NAME=InsuredAppAge VALUE="" readonly=true CLASS=common verify="被保人年龄|num">
      </TD>
    </TR>
    <TR class=common>
      <TD class=title>国籍</TD>
      <TD class=input>
        <input class=codeNo name="NativePlace" readonly=true verify="投保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,AppntNativePlaceName],[0,1]);" onkeyup="return showCodeListKey('NativePlace',[this,AppntNativePlaceName],[0,1]);"><input class=codename name=AppntNativePlaceName readonly=true elementtype=nacessary>
      </TD>
      <TD class=title id=MainInsured style="display:none">与第一被保险人关系</TD>
      <TD class=input id=MainInsuredInput style="display:none">
        <Input class="codeNo" name="RelationToMainInsured" readonly=true verify="主被保险人关系|code:Relation" ondblclick="return showCodeList('Relation', [this,RelationToMainInsuredName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,RelationToMainInsuredName],[0,1]);"><input class=codename name=RelationToMainInsuredName readonly=true>
      </TD>
      <TD class=title id=MainAppnt style="display:none">与投保人关系</TD>
      <TD class=input id=MainAppntInput style="display:none">
        <Input class="codeNo" name="RelationToAppnt" readonly=true ondblclick="return showCodeList('Relation', [this,RelationToAppntName],[0,1]);" onkeyup="return showCodeListKey('Relation', [this,RelationToAppntName],[0,1]);"><input class=codename name=RelationToAppntName readonly=true>
      </TD>
      <TD class=title>职业代码</TD>
      <TD class=input>
        <Input class="codeNo" name="OccupationCode" readonly=true ondblclick="return showCodeList('OccupationCode',[this,OccupationCodeName],[0,1]);" onkeyup="return showCodeListKey('OccupationCode',[this,OccupationCodeName],[0,1]);" onfocus="getdetailwork();"><input class=codename name=OccupationCodeName readonly=true>
      </TD>
      <TD class=title>风险等级</TD>
      <TD class=input>
        <Input class="common" name="RiskGrade" readonly=true>
      </TD>
    </TR>
    <TR class=common>
      <TD class=title>身高</TD>
      <TD class=input>
        <Input class="common" readonly=true name="Stature">
      </TD>
      <TD class=title>体重</TD>
      <TD class=input>
        <Input class="common" readonly=true name="Avoirdupois">
      </TD>
      <TD class=title>心率</TD>
      <TD class=input>
        <Input class="common" readonly=true name="Pulse">
      </TD>
      <TD class=title>血压</TD>
      <TD class=input>
        <Input class="common" readonly=true name="BloodPressure">
      </TD>    
    </TR>
    <TR class=common>
      <TD class=title>年收入</TD>
      <TD class=input>
        <Input class="common" readonly=true name="Income">
      </TD>
      <TD class=title>医疗费用支付方式</TD>
      <TD class=input>
        <Input class="common" readonly=true name="MedicalPayMode">
      </TD>
      <TD class=title>联系地址</TD>
      <TD class=input>
        <Input class="readonly" readonly=true name="AddressNo_ch"><input type=hidden name=AddressNo>
      </TD>
    </TR>       
    <TR class=common>
      <TD class=title>BMI</TD>
      <TD class=input>
        <Input class="common" readonly=true name="BMI">
      </TD>
      <TD class=title>累计保额</TD>
      <TD class=input>
        <Input class="common" readonly=true name="BaseAmnt">
      </TD>
      <TD class=title>累计风险保额</TD>
      <TD class=input>
        <Input class="common" readonly=true name="AccumRiskAmnt">
      </TD>
      <TD class=title>累计意外险风险保额</TD>
      <TD class=input>
        <Input class="common" readonly=true name="AccumAcciAmnt">
      </TD>
    </TR>	
    <TR class=common>
      <TD class=title>累计重疾险风险保额</TD>
      <TD class=input>
        <Input class="common" readonly=true name="AccumDiseaRiskAmnt">
      </TD>
      <TD class=title>累计护理险风险保额</TD>
      <TD class=input>
        <Input class="common" readonly=true name="AccumCareRiskAmnt">
      </TD>
      <TD class=title>累计身故保额</TD>
      <TD class=input>
        <Input class="common" readonly=true name="AccumDeaRiskAmnt">
      </TD>
      <TD class=title>既往累计理赔次数</TD>
      <TD class=input>
        <Input class="common" readonly=true name="ClaimNumber">
      </TD>
    </TR>
    <TR class=common>
      <TD class=title>累计理赔金额</TD>
      <TD class=input>
        <Input class="common" readonly=true name="ClaimMoney">
      </TD>
    </TR>		
    <TR class=common style="display: 'none'">
      <TD class=title>职业类别</TD>
      <TD class=input>
        <Input class="codeNo" name="OccupationType" readonly=true ondblclick="return showCodeList('OccupationType',[this,OccupationTypeName],[0,1]);" onkeyup="return showCodeListKey('OccupationType',[this,OccupationTypeName],[0,1]);"><input class=codename name=OccupationTypeName readonly=true>
      </TD>
    </TR>
  </Table>
</DIV>
<TABLE class=common>
  <TR class=common>
    <TD class=title>
      <DIV id="divContPlan" style="display:'none'">
        <TABLE class=common>
          <TR class=common>
            <TD class=title>保险计划</TD>
            <TD class=input>
              <Input class="code" name="ContPlanCode" ondblclick="showCodeListEx('ContPlanCode',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ContPlanCode',[this],[0],'', '', '', true);">
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </TD>
    <TD class=title>
      <DIV id="divExecuteCom" style="display:'none'">
        <TABLE class=common>
          <TR class=common>
            <TD class=title>处理机构</TD>
            <TD class=input>
              <Input class="code" name="ExecuteCom" ondblclick="showCodeListEx('ExecuteCom',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ExecuteCom',[this],[0],'', '', '', true);">
            </TD>
          </TR>
        </TABLE>
      </DIV>
    </TD>
    <TD class=title>    </TD>
  </TR>
</TABLE>
<DIV id="divLCInsuredPerson" style="display:'none'">
  <TABLE class=common>
    <TR class=common>
      <TD class=title>婚姻状况</TD>
      <TD class=input>
        <Input class="code" name="Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);">
      </TD>
      <!-- <Input class="code" name="Marriage" verify="被保险人婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);"></TD>-->
      <TD class=title>与投保人关系</TD>
      <TD class=input>
        <Input class="code" name="RelationToAppnt_" ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);">
      </TD>
      <!-- <Input class="code" name="RelationToAppnt" verify="与投保人关系|code:Relation" ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);"></TD>-->
      <TD class=title>国籍</TD>
      <TD class=input>
        <input class="code" name="NativePlace1" ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">
        <!--<input class="code" name="NativePlace" verify="被保险人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this]);" onkeyup="return showCodeListKey('NativePlace',[this]);">-->
      </TD>
    </TR>
    <TR class=common>
      <TD class=title>户口所在地</TD>
      <TD class=input>
        <Input class=common name="RgtAddress">
        <!--<Input class= common name="RgtAddress" verify="被保险人户口所在地|len<=80" >-->
      </TD>
      <TD class=title>民族</TD>
      <TD class=input>
        <input class="code" name="Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">
        <!-- <input class="code" name="Nationality" verify="被保险人民族|code:Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">-->
      </TD>
      <TD class=title>学历</TD>
      <TD class=input>
        <Input class="code" name="Degree" ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">
        <!--<Input class="code" name="Degree" verify="被保险人学历|code:Degree" ondblclick="return showCodeList('Degree',[this]);" onkeyup="return showCodeListKey('Degree',[this]);">-->
      </TD>
    </TR>
    <TR class=common>
      <TD class=title>工作单位</TD>
      <TD class=input colspan=3>
        <Input class=common3 name="GrpName">
        <!--<Input class= common3 name="GrpName" verify="被保险人工作单位|len<=60" >-->
      </TD>
      <TD class=title>单位电话</TD>
      <TD class=input>
        <Input class=common name="GrpPhone">
        <!--<Input class= common name="GrpPhone" verify="被保险人单位电话|len<=18" >-->
      </TD>
    </TR>
    <TR class=common>
      <TD class=title>单位地址</TD>
      <TD class=input colspan=3>
        <Input class=common3 name="GrpAddress">
        <!--<Input class= common3 name="GrpAddress" verify="被保险人单位地址|len<=80" >-->
      </TD>
      <TD class=title>单位邮政编码</TD>
      <TD class=input>
        <Input class=common name="GrpZipCode">
        <!-- <Input class= common name="GrpZipCode" verify="被保险人单位邮政编码|zipcode" >-->
      </TD>
    </TR>
    <TR class=common>
      <TD class=title>职业（工种）</TD>
      <TD class=input>
        <Input class=common name="WorkType">
        <!--<Input class= common name="WorkType" verify="被保险人职业（工种）|len<=10" >-->
      </TD>
      <TD class=title>兼职（工种）</TD>
      <TD class=input>
        <Input class=common name="PluralityType">
        <!--<Input class= common name="PluralityType" verify="被保险人兼职（工种）|len<=10" >-->
      </TD>
      <TD class=title>是否吸烟</TD>
      <TD class=input>
        <Input class="code" name="SmokeFlag" ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">
        <!--<Input class="code" name="SmokeFlag" verify="被保险人是否吸烟|code:YesNo" ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">-->
      </TD>
    </TR>
    <Input name=PolNo type=hidden>
    <Input name=ContNo type=hidden>
    <Input name=MissionID type=hidden>
    <Input name=SubMissionID type=hidden>
    <Input name=flag type=hidden>
    <Input name=PrtNo type=hidden>
    <input name=OperatePos type=hidden   >
    <Input name=AddFeeFlag type=hidden>
    <Input name=AddFeeValue type=hidden>
    <Input name=AddFeeRate type=hidden>
    <Input name=AddFeeStartDate type=hidden>
    <Input name=AddFeeEndDate type=hidden>
    <Input name=SpecFlag type=hidden>
    <Input name=LoadFlag type=hidden>
  </table>
</DIV>
<hr>
</hr><DIV id=DivButton STYLE="display:''">
  <!--
    table>
    <tr>
    <td>
    <input value="体检资料查询" class=cssButton type=button onclick="showHealthQ();" >
    <INPUT VALUE="生存调查查询" class=cssButton TYPE=button onclick="RReportQuery();">
    <input value="体检资料录入" class=cssButton type=button onclick="showHealth();" width="200">
    <input value="生调请求说明" class=cssButton type=button onclick="showRReport();">
    </td>
    </tr>
    </table
  -->
  <div id="buttonDiv" STYLE="display: ''" align=center>
    <table>
      <tr>
        <td class=common>
          <INPUT VALUE="既往投保信息" class=cssButton TYPE=button onclick="showApp(2);">
          <INPUT VALUE="既往保全查询" class=cssButton TYPE=button onclick="showEdor();">
          <INPUT VALUE="既往理赔查询" class=cssButton TYPE=button onclick="ClaimGetQuery();">
          <INPUT VALUE=" 扫描件查询" class=cssButton TYPE=button onclick="ScanQuery();">
          <INPUT VALUE="保障保全理赔查询" class=cssButton TYPE=hidden onclick="familyGetQuery();">
          <INPUT VALUE="加费承保录入" class=cssButton type=hidden name="AddFee" onclick="showAdd();">
          <INPUT VALUE="特约承保录入" class=cssButton type=hidden onclick="showSpec();">
        </td>
      </tr>
    </table>
  </div>
  <hr>
</DIV>
<DIV id=DivOldoldInfo STYLE="display:''">
  <table class=common border=0 width=100%>
    <tr>
      <td class=titleImg align=center>既往信息：</td>
    </tr>
  </table>
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanOldoldInfoGrid">        </span>
      </td>
    </tr>
  </table>
</DIV>
<hr>
</hr><DIV id=DivDisease STYLE="display:''">
  <table class=common border=0 width=100%>
    <tr>
      <td class=titleImg align=center>疾病信息：</td>
    </tr>
  </table>
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanDiseaseGrid">        </span>
      </td>
    </tr>
  </table>
</DIV>
<DIV id=DivOtherInfo STYLE="display:'none'">
  <table class=common border=0 width=100%>
    <tr>
      <td class=titleImg align=center>其他信息：</td>
    </tr>
  </table>
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanOtherInfoGrid">        </span>
      </td>
    </tr>
  </table>
</DIV>
<DIV id=DivLCPol STYLE="display:''">
  <table class=common border=0 width=100%>
    <tr>
      <td class=titleImg align=center>待审批险种：</td>
    </tr>
  </table>
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanRiskGrid">        </span>
      </td>
    </tr>
  </table>
</DIV>
<!--
<DIV id=DivUWAssess STYLE="display:''">
  <table class=common border=0 width=100%>
    <tr>
      <td class=titleImg align=center>核保评点：</td>
    </tr>
  </table>
  <table class=common>
    <tr class=common>
      <td text-align: left colSpan=1>
        <span id="spanUWRateGrid">        </span>
      </td>
    </tr>
  </table>
</DIV>


  	<div align=left>
  <tr>		
   评点汇总<input type='common' style='{border: 1px #9999CC solid;height: 18px}' name="SumAssess" size='8'>
    <td text-align: rigth>
        <INPUT VALUE="保 存" class=cssButton name="SaveAssess" TYPE=button onclick="submitForm(10);" align=right>
      </td>
   
  </TR>
    </div>
-->
	  <Div  id= "divEsDocPages1Title" style= "display: 'none'">
		 <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEsDocPages1);">
    		</td>
    		<td class= titleImg>
        		 扫描件基本信息
       	</td>   		 
    	</tr>
    </table>
  </div>

    <Div  id= "divEsDocPages1" style= "display: 'none'" align=center>
    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanEsDocPagesGrid">
	  				</span> 
			    </td>
				</tr>
		</table>
    </Div>


<div id="divUWResult" style="display: 'none'">
  <!-- 核保结论 -->
  <table class=common border=0 width=100%>
    <tr>
      <td class=titleImg align=center>险种核保结论：</td>
    </tr>
  </table>
  <table class=common>
  	<tr class=common>
  		<td class=common>正常承保<input class=box  type=checkbox name=UWCheck OnClick="initUWCheck(0);"></td>
  		<td class=common>延期承保<input class=box  type=checkbox name=UWCheck OnClick="initUWCheck(1);"></td>
  		<td class=common>谢绝承保<input class=box  type=checkbox name=UWCheck OnClick="initUWCheck(2);"></td>
  		<td class=common>撤销申请<input class=box  type=checkbox name=UWCheck OnClick="initUWCheck(3);"></td>
  		<td></td>
  	</tr>
  	<tr class=common>
  		<td class=common>加费承保<input class=box  type=checkbox name=UWCheck OnClick="initUWCheck(4);"></td>
  		<td class=common>免责承保<input class=box  type=checkbox name=UWCheck OnClick="initUWCheck(5);"></td>
  		<td class=common>降低档次<input class=box  type=checkbox name=UWCheck OnClick="initUWCheck(6);"></td>
  		<td class=common>降低保额<input class=box  type=checkbox name=UWCheck OnClick="initUWCheck(7);"></td>
  		<td class=common>变更缴费期<input class=box  type=checkbox name=UWCheck OnClick="initUWCheck(8);"></td>
  	</tr>
  </table>
  <table class=common align=center>
    <tr class=common>
      <!--
        td class=title >
        建议核保意见
        </td>
        <td class =input colspan=3>
        <Input class= common name="SugUWIdea"  >
        </td>
        <td class=title >
        </td>
        <td class =input>
        </td
      -->
      <TD class=title>核保结论</td>
      <td class=Input>
        <Input class="codeNo" name=uwstate verify="核保结论|code:uwstate" ondblclick="return showCodeList('uwstate',[this,uwstateName],[0,1]);" onkeyup="return showCodeListKey('uwstate',[this,uwstateName],[0,1]);"><input class=codename name=uwstateName readonly=true elementtype=nacessary>
      </TD>
      <TD id=divExplainName style="display:''" class=title>核保结论备注</TD>
        <td id=divExplain style="display:''" class=input>
        <Input VALUE="" class=common  name="inputExplain">
      </td>
      <td text-align: rigth>
        <INPUT VALUE="保 存" class=cssButton name="SaveExplain" TYPE=button onclick="submitForm(11);" align=right>
      </td>
      <TD id=divPropValueName style="display:none" class=title>属性值</TD>
      <td id=divPropValue style="display:none" class=input>
        <Input class=common name=PropValue>
      </td>
</div>
<span id=spanstopInsuredButton STYLE="display:'none'">
<td class=input>
  <INPUT VALUE="被保人暂停" name=stopInsuredButton class=cssButton TYPE=button type=hidden onclick="StopInsured();">
</td>
</span>
<td class=input></td>
</TR></table><!--存放加费或免责的信息-->
<DIV id=DivAddFee1 STYLE="display:'none'">
  <td class=title>建议结论</td>
  <td class=input>
    <Input class=common readonly=true name="SugUWFlag">
  </td>
</div>
<DIV id=DivAddFee STYLE="display:'none'">
  <table class=common border=0 width=100%>
    <tr>
      <td id=DivAddFee_1_1 style="display:'none'" class=titleImg align=center>加费信息 :</td>
      <td id=DivAddFee_2_1 style="display:'none'" class=titleImg align=center>免责信息 :</td>
    </tr>
  </table>
  <table class=common>
    <tr class=common>
      <td id=DivAddFee_1_2 style="display:'none'" text-align: left colSpan=1><!--使默认选择为不显示 2007-10-26 -->
        <span id="spanDiseaseCheckGrid"></span>
      </td>
      <td id=DivAddFee_1_2_1 style="display:'none'" text-align: left colSpan=1>
        <span id="spanAddFeeGrid">        </span>
      </td>
      <td id=DivAddFee_2_2 style="display:'none'" text-align: left colSpan=1>
        <span id="spanSpecGrid">        </span>
      </td>
    </tr>
    <tr class=common>
      <td id=DivAddFee_1_3 style="display:'none' " text-align: right>加费类型
        <INPUT type=input readonly=true name="AddFeeType" class=codeNo CodeData="0^1|评点加费^2|正常加费" ondblclick="showCodeListEx('AddFeeType',[this,AddFeeTypeName],[0,1],'', '', '', true);" onkeyup="showCodeListKeyEx('AddFeeType',[this,AddFeeTypeName],[0,1],'', '', '', true);"><input class=codename name=AddFeeTypeName readonly=true>
        <!-- 评点值% -->
       	<INPUT VALUE="" class=codeNo readonly=true name="AddFeeValue" TYPE=hidden >
        <INPUT VALUE="确  定" class=cssButton name="AddFeeButton" TYPE=button onclick="submitForm(3);" align=right>
      </td>
      <td id=DivAddFee_2_3 style="display:'none' " text-align: rigth>
        <INPUT VALUE="确  定" class=cssButton name="SpecButton" TYPE=button onclick="submitForm(4);" align=right>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <INPUT VALUE="查  询" class=cssButton name="QueryButton" TYPE=button onclick="QueryDisease();" align=middle>
     </td>
    </tr>
  </table>
  <div id="MultAndAmntDiv" STYLE="display: ''">
    <table class=common>
      <tr class=common>
        <td id=MultAndAmntDiv_1_1 style="display:'none' " class=titleImg align=left colSpan=4>降低档次 :</td>
        <td id=MultAndAmntDiv_2_1 style="display:'none' " class=titleImg align=left colSpan=4>降低保额 :</td>
      </tr>
      <tr class=common>
        <td id=MultAndAmntDiv_1_2 style="display:'none' " class=common align=left>原档次为 :</td>
        <td id=MultAndAmntDiv_1_3 style="display:'none' " class=common align=left>
          <INPUT VALUE="" class=common name="initMult">
        </td>
        <td id=MultAndAmntDiv_1_4 style="display:'none' " class=common align=left>降至 :</td>
        <td id=MultAndAmntDiv_1_5 style="display:'none' " class=common align=left>
          <INPUT VALUE="" class=common name="inputMult">
        </td>
        <td id=MultAndAmntDiv_2_2 style="display:'none' " class=common align=left>原保额为 :</td>
        <td id=MultAndAmntDiv_2_3 style="display:'none' " class=common align=left>
          <INPUT VALUE="" class=common name="initAmnt">
        </td>
        <td id=MultAndAmntDiv_2_4 style="display:'none' " class=common align=left>降至 :</td>
        <td id=MultAndAmntDiv_2_5 style="display:'none' " class=common align=left>
          <INPUT VALUE="" class=common name="inputAmnt">
        </td>
      </tr>
      <tr class=common>
        <td id=MultAndAmntDiv_1_6 style="display:'none' " class=common colSpan=4>
          <INPUT VALUE="确  定" class=cssButton name="MultButton" TYPE=button onclick="submitForm(5);" align=right>
        </td>
        <td id=MultAndAmntDiv_2_6 style="display:'none' " class=common colSpan=4>
          <INPUT VALUE="确  定" class=cssButton name="AmntButton" TYPE=button onclick="submitForm(6);" align=right>
        </td>
      </tr>
    </table>
  </div>
</DIV>
<div id="divDelay" style="display: 'none'">
  <table class="common">
    <TR class="common">
      <td class=title align=left>延期天数 :</td>
      <td class=input align=left>
        <INPUT VALUE="" class=common name="inputDelayDays">
      </td>
      <td class=title align=left>延期至:</td>
      
      <td class=input align=left>
        <INPUT VALUE="" class=coolDatePicker name="inputDelayDate" verify="延期至(日期使用)|DATE" >
        （日期使用）
      </td>
      <td class=title align=left>延期至:</td>
      <td class=input align=left>
        <INPUT VALUE="" class=common name="inputDelayDescribe">
        （文字使用）
      </td>
      <td>      </td>
      <td>      </td>
    </TR>
  </table>
</div>
<div id="divCancelReason" style="display: 'none'">
  <table class="common">
    <TR class="common">
      <td class=title align=left>撤销原因 :</td>
      <td class=input align=left>
        <INPUT VALUE="" class=code name="CancelReason" verify="核保结论|code:CancelReason" CodeData="0^01|变更承保不同意^02|客户主动撤销^03|问题件逾期未回复^04|其他" ondblclick="showCodeListEx('CancelReason',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('CancelReason',[this],[0],'', '', '', true);">
      </td>
      <td>      </td>
      <td>      </td>
      <td>      </td>
    </TR>
  </table>
</div>
<div id="divRejectReason" style="display: 'none'">
  <table class="common">
    <TR class="common">
      <td class=title align=left>拒保原因 :</td>
      <td class=input align=left>
       	<INPUT VALUE="" class="codeNo" name="RejectReason" verify="拒保原因|code:RejectReason" ondblclick="return showCodeList('rejectreason',[this,rejectreasonName],[0,1]);" onkeyup="return showCodeListKey('rejectreason',[this,rejectreasonName],[0,1]);"><input class=codename name=rejectreasonName readonly=true elementtype=nacessary>
      </td>
      <td>      </td>
      <td>      </td>
      <td>      </td>
    </TR>
  </table>
</div>
<div id="divChangePayEndYear" style="display: 'none'">
  <table class="common">
    <TR class="common">
      <td class=titleImg align=left colspan=1>变更缴费期 :</td>
    </TR>
    <tr>
    	<td class=common align=left>原缴费期间: 
    		<input type=input class=codeNo readonly=true name="oldPayEndYear" value="">
    		原缴费期间单位：
    		<input type=input class=codeNo readonly=true name="oldPayEndYearFlag" value="">
    		<b>调整为：</b>
    		新缴费期间:           
    		<input type=input class=codeNo name="newPayEndYear" value="">
    		新缴费期间单位：      
    		<input type=input class=codeNo name="newPayEndYearFlag" value="">
    	</td> 
    </tr>
    <tr>
    	<td class=common align=left colspan=1><INPUT VALUE="确  定" class=cssButton  TYPE=button onclick="submitForm(7);" align=right></td>
    </tr>
  </table>
</div>
<table class=common>
  <tr>
    <TD height="24" class=title>核保意见</TD>
    <td>    </td>
  </tr>
  <tr>
    <TD class=input>
      <textarea name="UWIdea" verify="核保结论|len<=255" cols="100%" rows="5" witdh=120% class="common"></textarea></TD>
      		<td></td>
      		
      		</tr>
	  </table>
	  <div id =divUWButton1 style="display:''">
	  <p>
    		<INPUT VALUE="确  定" class=cssButton TYPE=button onclick="submitForm(1);">
    		<INPUT VALUE="取  消" class=cssButton TYPE=button onclick="cancelchk();">
    		<INPUT VALUE="返  回" class=cssButton TYPE=button onclick="InitClick();">
  	</p>
  </div>
</div>

<div id = "divIndUWResult" style = "display: 'none'">
    	  <table class= common border=0 width=100%>
    	  	<tr>
			<td class= titleImg align= center>被保人核保结论：</td>
	  	</tr>
	  </table>
	   		 	   	
  	  <table  class= common align=center>
        	  				<tr class = common>
			<td class=title >
				建议结论
			</td>
			<td class =input>
				<Input class= common readonly=true name="SugIndUWFlag" >
			</td>
			<!--td class=title >
				建议核保意见
			</td>
			<td class =input colspan=3>
				<Input class= common name="SugIndUWIdea"  >
			</td>
			<td class=title >
			</td>
			<td class =input>
			</td-->
       	<TD class= title>
       	 	核保结论
       	 </td>
       	 <td class=input>
       	 <Input class="code" name=uwindstate ondblclick="return showCodeList('uwstate',[this]);" onkeyup="return showCodeListKey('uwstate',[this]);">
	   		 </TD>
	   		 <td class=title>
	   		 </td>
	   		 <td class=input>
	   		 </td>
       	</TR>
       </table>
      <table class=common>
		<tr>
		      	<TD height="24"  class= title>
            		核保意见
          	</TD>
          	<td></td>
          	</tr>
		<tr>
      		<TD  class= input> <textarea name="UWIndIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
      		<td></td>
      		</tr>
	  </table>
</div>
	<div id=divUWButton2 style="display:'none'">
  	<p>
    		<INPUT VALUE="确  定" class=cssButton TYPE=button onclick="submitForm(2);">
    		<INPUT VALUE="取  消" class=cssButton TYPE=button onclick="cancelchk();">
        <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="InitClick();">
  	</p>
  </div>
    	<Div id= "divPage" align=center style= "display: '' ">
      <INPUT VALUE="上一个" class = cssButton TYPE=button onclick="getPreviousRisk();"> 					
      <INPUT VALUE="下一个" class = cssButton TYPE=button onclick="getNextRisk();"> 
      </Div>					
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>






